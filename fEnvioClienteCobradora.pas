unit fEnvioClienteCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DB, DBCtrls, ADODB, StdCtrls, cxButtons, Mask;

type
  TfrmEnvioClienteCobradora = class(TfrmProcesso_Padrao)
    Label5: TLabel;
    edtCliente: TMaskEdit;
    btExibeTitulo: TcxButton;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    edtCobradora: TMaskEdit;
    Label1: TLabel;
    qryCobradora: TADOQuery;
    qryCobradoranCdCobradora: TIntegerField;
    qryCobradoracNmCobradora: TStringField;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    edtDtEnvio: TMaskEdit;
    Label3: TLabel;
    procedure preparaTela();
    procedure FormShow(Sender: TObject);
    procedure btExibeTituloClick(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtCobradoraExit(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCobradoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEnvioClienteCobradora: TfrmEnvioClienteCobradora;

implementation

uses fEnvioClienteCobradora_Titulos, fMenu, fLookup_Padrao;

{$R *.dfm}

{ TfrmEnvioClienteCobradora }

procedure TfrmEnvioClienteCobradora.preparaTela;
begin
    edtCliente.Text   := '' ;
    edtCobradora.Text := '' ;
    edtDtEnvio.Text   := DateToStr(Date) ;

    qryTerceiro.Close;
    qryCobradora.Close;

    edtCliente.SetFocus;
end;

procedure TfrmEnvioClienteCobradora.FormShow(Sender: TObject);
begin
  inherited;

  preparaTela;
  
end;

procedure TfrmEnvioClienteCobradora.btExibeTituloClick(Sender: TObject);
var
  objForm : TfrmEnvioClienteCobradora_Titulos;
begin
  inherited;

  if (qryTerceiro.Eof) then
  begin
      MensagemAlerta('Selecione um cliente.') ;
      edtCliente.SetFocus;
      abort ;
  end ;

  if (qryCobradora.Eof) then
  begin
      MensagemAlerta('Selecione uma cobradora.') ;
      edtCobradora.SetFocus;
      abort ;
  end ;

  if (Trim(edtDtEnvio.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de envio dos t�tulos para cobradora.') ;
      edtDtEnvio.SetFocus;
      abort ;
  end ;

  objForm := TfrmEnvioClienteCobradora_Titulos.Create(nil);

  objForm.qryTitulosPendentes.Close;
  objForm.qryTitulosPendentes.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(objForm.qryTitulosPendentes,qryTerceironCdTerceiro.AsString) ;

  if (objForm.qryTitulosPendentes.Eof) then
  begin
      MensagemAlerta('Cliente n�o tem nenhum t�tulo pendente ou todos j� enviados para cobradora.') ;
      edtCliente.SetFocus;
      abort ;
  end ;

  objForm.nCdCobradora      := qryCobradoranCdCobradora.Value;
  objForm.dDtEnvioCobradora := StrToDate(edtDtEnvio.Text) ;
  showForm(objForm,true);

  preparaTela;

end;

procedure TfrmEnvioClienteCobradora.edtClienteExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtCliente.Text) ;
  
end;

procedure TfrmEnvioClienteCobradora.edtCobradoraExit(Sender: TObject);
begin
  inherited;

  qryCobradora.Close;
  PosicionaQuery(qryCobradora, edtCobradora.Text) ;
  
end;

procedure TfrmEnvioClienteCobradora.edtClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        If (nPK > 0) then
            edtCliente.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmEnvioClienteCobradora.edtCobradoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(199,'cFlgAtivo = 1');

        If (nPK > 0) then
            edtCobradora.Text := intToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(tfrmEnvioClienteCobradora) ;
    
end.
