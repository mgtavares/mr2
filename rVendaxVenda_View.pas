unit rVendaxVenda_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TrptVendaxVenda_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRGroup4: TQRGroup;
    v: TQRDBText;
    QRLabel37: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRBand2: TQRBand;
    QRShape7: TQRShape;
    qryRelatorio: TADOQuery;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    qryRelatorionCdTerceiro: TIntegerField;
    qryRelatoriocNmTerceiro: TStringField;
    qryRelatoriodDtVenda: TStringField;
    qryRelatorionCdLoja: TIntegerField;
    qryRelatoriocNmLoja: TStringField;
    qryRelatorionCdPedido: TIntegerField;
    qryRelatorionValPedido: TBCDField;
    QRLabel6: TQRLabel;
    QRShape3: TQRShape;
    QRExpr2: TQRExpr;
    QRLabel4: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaxVenda_View: TrptVendaxVenda_View;

implementation

{$R *.dfm}

end.
