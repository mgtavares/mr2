inherited frmPDVItemGrade: TfrmPDVItemGrade
  Left = 492
  Top = 163
  Width = 395
  Height = 497
  BorderIcons = [biSystemMenu]
  Caption = 'Tamanho'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 379
    Height = 430
  end
  inherited ToolBar1: TToolBar
    Width = 379
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 379
    Height = 430
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      OnKeyPress = cxGrid1DBTableView1KeyPress
      DataController.DataSource = dsItemGrade
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.ConteudoCaixaGrande
      Styles.Header = frmMenu.HeaderPDV
      object cxGrid1DBTableView1cNmTamanho: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTamanho'
        Width = 97
      end
      object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
        DataBinding.FieldName = 'nCdProduto'
        Visible = False
      end
      object cxGrid1DBTableView1cEAN: TcxGridDBColumn
        Caption = 'C'#243'd. Barra'
        DataBinding.FieldName = 'cEAN'
      end
      object cxGrid1DBTableView1nQtdeEstoque: TcxGridDBColumn
        Caption = 'Estoque'
        DataBinding.FieldName = 'nQtdeEstoque'
        HeaderAlignmentHorz = taRightJustify
        Width = 81
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryItemGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT cNmTamanho'
      '      ,nCdProduto'
      '      ,cEAN'
      
        '      ,dbo.fn_PosicaoEstoqueLojaPDV(nCdProduto,@nCdLoja) as nQtd' +
        'eEstoque'
      '  FROM Produto'
      
        '       INNER JOIN ItemGrade  ON ItemGrade.nCdGrade = Produto.nCd' +
        'Grade'
      
        '                            AND ItemGrade.iTamanho = Produto.iTa' +
        'manho'
      ' WHERE nCdProdutoPai = :nPK'
      ' ORDER BY Produto.iTamanho')
    Left = 24
    Top = 104
    object qryItemGradecNmTamanho: TStringField
      DisplayLabel = 'Tamanho'
      FieldName = 'cNmTamanho'
      FixedChar = True
      Size = 5
    end
    object qryItemGradenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItemGradecEAN: TStringField
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryItemGradenQtdeEstoque: TIntegerField
      FieldName = 'nQtdeEstoque'
      ReadOnly = True
    end
  end
  object dsItemGrade: TDataSource
    DataSet = qryItemGrade
    Left = 24
    Top = 136
  end
  object PopupMenu1: TPopupMenu
    Left = 112
    Top = 168
    object PosioEstoque1: TMenuItem
      Caption = 'Posi'#231#227'o Estoque'
      OnClick = PosioEstoque1Click
    end
  end
end
