unit rPedidoItemCancelado_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg, StdCtrls;

type
  TrptPedidoItemCancelado_view = class(TForm)
    SPREL_PEDIDO_ITEM_CANCELADO: TADOStoredProc;
    SPREL_PEDIDO_ITEM_CANCELADOnCdPedido: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOdDtPedido: TDateTimeField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdTerceiro: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmTerceiro: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdTipoPedido: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmTipoPedido: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdProduto: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmProduto: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnQtdeCanc: TBCDField;
    SPREL_PEDIDO_ITEM_CANCELADOnValTotalCanc: TBCDField;
    SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed: TDateTimeField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdUsuarioCancelSaldoItemPed: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdMotivoCancSaldoPed: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmMotivoCancSaldoPed: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmUsuario: TStringField;
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBand2: TQRBand;
    QRLabel9: TQRLabel;
    QRExpr4: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand5: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel12: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel1: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  
var
  rptPedidoItemCancelado_view: TrptPedidoItemCancelado_view;

implementation

{$R *.dfm}

end.
