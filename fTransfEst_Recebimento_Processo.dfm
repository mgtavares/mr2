inherited frmTransfEst_Recebimento_Processo: TfrmTransfEst_Recebimento_Processo
  Left = 213
  Top = 207
  Width = 1130
  Height = 628
  BorderIcons = []
  Caption = 'Recebimento Transfer'#234'ncia'
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1114
    Height = 561
  end
  inherited ToolBar1: TToolBar
    Width = 1114
    ButtonWidth = 83
    TabOrder = 1
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 83
    end
    inherited ToolButton2: TToolButton
      Left = 91
    end
    object ToolButton4: TToolButton
      Left = 174
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 182
      Top = 0
      Caption = 'Excluir Itens'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1114
    Height = 561
    ActivePage = cxTabSheet1
    Align = alClient
    Focusable = False
    LookAndFeel.NativeStyle = True
    TabOrder = 0
    ClientRectBottom = 557
    ClientRectLeft = 4
    ClientRectRight = 1110
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens por C'#243'digo'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 1106
        Height = 533
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItens
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItem'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTransfEst'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'cEAN'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Width = 595
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Itens por EAN'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1106
        Height = 535
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItensEAN
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItem'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTransfEst'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cEAN'
            Footers = <>
            Width = 151
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Width = 595
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Resumo'
      ImageIndex = 2
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1106
        Height = 535
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsResumo
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh2DrawColumnCell
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 54
          end
          item
            EditButtons = <>
            FieldName = 'cEAN'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeTransf'
            Footers = <>
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            Width = 944
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      0000000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFF0030000
      FE00C003E001000000008001C001000000008001800100000000800180010000
      0000800100010000000080010001000000008001000100000000800100010000
      00008001000100000000800180030000000180018003000000038001C0070000
      0077C003E00F0000007FFFFFF83F000000000000000000000000000000000000
      000000000000}
  end
  object qryPrepara_Temp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_ItemTransfEst'#39') IS NULL)'
      'BEGIN'
      
        #9'CREATE TABLE #Temp_ItemTransfEst (nCdItem      int identity(1,1' +
        ')'
      #9#9#9#9#9#9#9#9#9'  ,nCdTransfEst int'
      #9#9#9#9#9#9#9#9#9'  ,nCdProduto   int'
      #9#9#9#9#9#9#9#9#9'  ,cEAN         varchar(20)'
      #9#9#9#9#9#9#9#9#9'  ,cNmProduto   varchar(150)'
      #9#9#9#9#9#9#9#9#9'  ,nQtde        decimal(12,2) default 0 not null'
      '                    ,cFlgDiverg   int'
      '                    ,cFlgInvalido int'
      '                    ,cFlgTipo     char(1))'
      'END'
      ''
      'TRUNCATE TABLE #Temp_ItemTransfEst'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_ResumoTransf'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_ResumoTransf (nCdProduto   int'
      #9#9#9#9#9#9#9#9#9#9' ,cEAN         varchar(20)'
      #9#9#9#9#9#9#9#9#9#9' ,cNmProduto   varchar(150)'
      #9#9#9#9#9#9#9#9#9#9' ,nQtde        decimal(12,2) default 0 not null'
      
        '                                         ,nQtdeTransf  decimal(1' +
        '2,2) default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_ResumoTransf'
      '')
    Left = 284
    Top = 173
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM #Temp_ItemTransfEst '
      'WHERE nCdTransfEst = :nPK'
      'AND cFlgTipo = '#39'C'#39)
    Left = 332
    Top = 173
    object qryItensnCdItem: TAutoIncField
      FieldName = 'nCdItem'
      ReadOnly = True
    end
    object qryItensnCdTransfEst: TIntegerField
      FieldName = 'nCdTransfEst'
    end
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItenscEAN: TStringField
      DisplayLabel = 'Produto|EAN'
      FieldName = 'cEAN'
    end
    object qryItenscNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItensnQtde: TBCDField
      DisplayLabel = 'Qtde|Receb.'
      FieldName = 'nQtde'
      Precision = 12
      Size = 2
    end
    object qryItenscFlgTipo: TStringField
      FieldName = 'cFlgTipo'
      FixedChar = True
      Size = 1
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 372
    Top = 173
  end
  object qryResumo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM #Temp_ResumoTransf')
    Left = 452
    Top = 221
    object qryResumonCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryResumocEAN: TStringField
      DisplayLabel = 'Produto|EAN'
      FieldName = 'cEAN'
    end
    object qryResumocNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryResumonQtde: TBCDField
      DisplayLabel = 'Quantidades|Recebida'
      FieldName = 'nQtde'
      Precision = 12
      Size = 2
    end
    object qryResumonQtdeTransf: TBCDField
      DisplayLabel = 'Quantidades|Enviada'
      FieldName = 'nQtdeTransf'
      Precision = 12
      Size = 2
    end
  end
  object dsResumo: TDataSource
    DataSet = qryResumo
    Left = 300
    Top = 357
  end
  object SP_CONFERE_RECEBIMENTO_TRANSFERENCIA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONFERE_RECEBIMENTO_TRANSFERENCIA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransfEst'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 484
    Top = 381
  end
  object qryZeraTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'TRUNCATE TABLE #Temp_ResumoTransf'
      'TRUNCATE TABLE #Temp_ItemTransfEst')
    Left = 516
    Top = 181
  end
  object SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransfEst'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 588
    Top = 293
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 332
    Top = 237
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryItensEAN: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensEANBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM #Temp_ItemTransfEst '
      'WHERE nCdTransfEst = :nPK'
      'AND cFlgTipo = '#39'E'#39)
    Left = 172
    Top = 229
    object qryItensEANnCdItem: TAutoIncField
      FieldName = 'nCdItem'
      ReadOnly = True
    end
    object qryItensEANnCdTransfEst: TIntegerField
      FieldName = 'nCdTransfEst'
    end
    object qryItensEANnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItensEANcEAN: TStringField
      DisplayLabel = 'Produto|C'#243'd. Barra'
      FieldName = 'cEAN'
    end
    object qryItensEANcNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItensEANnQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
      Size = 2
    end
    object qryItensEANcFlgDiverg: TIntegerField
      FieldName = 'cFlgDiverg'
    end
    object qryItensEANcFlgInvalido: TIntegerField
      FieldName = 'cFlgInvalido'
    end
    object qryItensEANcFlgTipo: TStringField
      FieldName = 'cFlgTipo'
      FixedChar = True
      Size = 1
    end
  end
  object dsItensEAN: TDataSource
    DataSet = qryItensEAN
    Left = 212
    Top = 229
  end
  object qryProdutoEAN: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmProduto'
      '      ,nCdProduto'
      '  FROM Produto'
      ' WHERE cEAN = LTRIM(RTRIM(:nPK))')
    Left = 332
    Top = 285
    object qryProdutoEANcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoEANnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
end
