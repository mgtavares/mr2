unit dcVendaProdutoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, ER2Lookup;

type
  TdcmVendaProdutoCliente = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    DBEdit7: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    qryRamoAtividade: TADOQuery;
    qryRamoAtividadenCdRamoAtividade: TIntegerField;
    qryRamoAtividadecNmRamoAtividade: TStringField;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    DataSource6: TDataSource;
    MaskEdit11: TMaskEdit;
    Label11: TLabel;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    DBEdit11: TDBEdit;
    DataSource7: TDataSource;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    qryAreaVenda: TADOQuery;
    qryAreaVendanCdAreaVenda: TIntegerField;
    qryAreaVendacNmAreaVenda: TStringField;
    qryDivisaoVenda: TADOQuery;
    qryDivisaoVendanCdDivisaoVenda: TIntegerField;
    qryDivisaoVendacNmDivisaoVenda: TStringField;
    qryCanalDistribuicao: TADOQuery;
    qryCanalDistribuicaonCdCanalDistribuicao: TIntegerField;
    qryCanalDistribuicaocNmCanalDistribuicao: TStringField;
    dsCanalDistribuicao: TDataSource;
    dsDivisaoVenda: TDataSource;
    dsAreaVenda: TDataSource;
    edtAreaVenda: TER2LookupMaskEdit;
    edtDivisaoVenda: TER2LookupMaskEdit;
    edtCanalDistrib: TER2LookupMaskEdit;
    edtGrupoTerceiro: TER2LookupMaskEdit;
    Label12: TLabel;
    qryGrupoTerceiro: TADOQuery;
    qryGrupoTerceironCdGrupoTerceiro: TIntegerField;
    qryGrupoTerceirocNmGrupoTerceiro: TStringField;
    DBEdit12: TDBEdit;
    DataSource8: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit11Exit(Sender: TObject);
    procedure MaskEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDivisaoVendaBeforeLookup(Sender: TObject);
    procedure edtDivisaoVendaBeforePosicionaQry(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaProdutoCliente: TdcmVendaProdutoCliente;

implementation

uses fMenu, fLookup_Padrao, dcVendaProdutoCliente_view;

{$R *.dfm}

procedure TdcmVendaProdutoCliente.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;


end;

procedure TdcmVendaProdutoCliente.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;


end;

procedure TdcmVendaProdutoCliente.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

procedure TdcmVendaProdutoCliente.ToolButton1Click(Sender: TObject);
var
   objRel : TdcmVendaProdutoCliente_view;
begin
  inherited;

  objRel := TdcmVendaProdutoCliente_view.Create(nil);

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  Try
      Try
         objRel.ADODataSet1.Close ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := frmMenu.ConvInteiro(MaskEdit9.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := frmMenu.ConvInteiro(MaskEdit10.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.ConvInteiro(MaskEdit11.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdAreaVenda').Value      := frmMenu.ConvInteiro(edtAreaVenda.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdDivisaoVenda').Value   := frmMenu.ConvInteiro(edtDivisaoVenda.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdCanalDistrib').Value   := frmMenu.ConvInteiro(edtCanalDistrib.Text) ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdGrupoTerceiro').Value  := frmMenu.ConvInteiro(edtGrupoTerceiro.Text) ;
         objRel.ADODataSet1.Open ;

         if (objRel.ADODataSet1.eof) then
         begin
             ShowMessage('Nenhuma informa��o encontrada.') ;
             exit ;
         end ;

         frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

         if objRel.PivotCube1.Active then
         begin
             objRel.PivotCube1.Active := False;
         end;

         objRel.PivotCube1.ExtendedMode := True;
         objRel.PVMeasureToolBar1.HideButtons := False;
         objRel.PivotCube1.Active := True;

         objRel.PivotMap1.Measures[2].ColumnPercent := True;
         objRel.PivotMap1.Measures[2].Value := False;

         objRel.PivotMap1.Measures[3].Rank := True ;
         objRel.PivotMap1.Measures[3].Value := False;

         objRel.PivotMap1.SortColumn(0,3,False) ;

         objRel.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
         objRel.PivotGrid1.RefreshData;

         frmMenu.StatusBar1.Panels[6].Text := '' ;

         objRel.ShowModal ;
         Self.Activate ;
      except
          MensagemErro('Erro ao gerar Data Analys');
          raise;
      end;
  Finally
      FreeAndNil(objRel);
  end;
end;

procedure TdcmVendaProdutoCliente.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryRamoAtividade.Close ;
  PosicionaQuery(qryRamoAtividade, Maskedit10.Text) ;
  
end;

procedure TdcmVendaProdutoCliente.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(158);

        If (nPK > 0) then
        begin
            Maskedit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRamoAtividade, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.MaskEdit11Exit(Sender: TObject);
begin
  inherited;

  qryTerceiroRepres.Close ;
  PosicionaQuery(qryTerceiroRepres, MaskEdit11.Text) ;
  
end;

procedure TdcmVendaProdutoCliente.MaskEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(103);

        If (nPK > 0) then
        begin
            Maskedit11.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoCliente.edtDivisaoVendaBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
  begin
      MensagemAlerta('Para selecionar a Divis�o de Venda, selecione a �rea de Venda.');
      edtAreaVenda.SetFocus;
      abort;
  end;

  edtDivisaoVenda.WhereAdicional.Text := 'nCdStatus = 1 AND nCdAreaVenda = ' + edtAreaVenda.Text;

end;

procedure TdcmVendaProdutoCliente.edtDivisaoVendaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
      exit;

  qryDivisaoVenda.Parameters.ParamByName('nCdAreaVenda').Value := edtAreaVenda.Text;
end;

initialization
    RegisterClass(TdcmVendaProdutoCliente) ;

end.

