inherited frmGeracaoBorderoRecebimento_titulos: TfrmGeracaoBorderoRecebimento_titulos
  Left = 44
  Top = 79
  Width = 1152
  Height = 763
  Caption = 'Gera'#231#227'o Border'#244' Recebimento'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 65
    Width = 1136
    Height = 662
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 65
    Width = 1136
    Height = 662
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsTemp
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '##,#00.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
          Column = cxGrid1DBTableView1nSaldoTit
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnHiding = True
      OptionsCustomize.ColumnMoving = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Selection = frmMenu.LinhaAzul
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1cFlgOK: TcxGridDBColumn
        DataBinding.FieldName = 'cFlgOK'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.DisplayChecked = '1'
        Properties.DisplayUnchecked = '0'
        Properties.ValueChecked = '1'
        Properties.ValueUnchecked = '0'
        Width = 50
      end
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTitulo'
        Options.Editing = False
      end
      object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
        DataBinding.FieldName = 'cNrTit'
        Options.Editing = False
        Width = 85
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        DataBinding.FieldName = 'cNrNF'
        Options.Editing = False
        Width = 73
      end
      object cxGrid1DBTableView1iParcela: TcxGridDBColumn
        DataBinding.FieldName = 'iParcela'
        Options.Editing = False
      end
      object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
        DataBinding.FieldName = 'dDtEmissao'
        Options.Editing = False
        Width = 97
      end
      object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
        DataBinding.FieldName = 'dDtVenc'
        Options.Editing = False
      end
      object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
        DataBinding.FieldName = 'nSaldoTit'
        Options.Editing = False
        Width = 87
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTerceiro'
        Options.Editing = False
        Width = 194
      end
      object cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn
        DataBinding.FieldName = 'cNmFormaPagto'
        Options.Editing = False
        Width = 219
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 29
    Width = 1136
    Height = 36
    Align = alTop
    TabOrder = 2
    object cxButton1: TcxButton
      Left = 0
      Top = 0
      Width = 129
      Height = 33
      Caption = 'Gerar Border'#244
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFD6D9D868513A60504060483070504070605070
        5040604830604830604830604830604830A6A6A9FFFFFF000000FFFFFFC0A8A0
        F0F0F0E0D8D0E0D0C0E0C8C09090B0D0C0B0E0B8A0D0B0A0D0B0A0D0A890D0A0
        90604830FFFFFF000000FFFFFFC0A8A0FFF0F0F0F0F0F0E8E03050C01038B070
        78C0E0D0D0F0D0C0E0D0C0E0C8B0D0A890604830FFFFFF000000FFFFFFC0A8A0
        FFF0F0D0D0E03050C03058F02048E01038B0A098C0F0D0C0F0D0C0E0C8B0D0A8
        90604830FFFFFF000000FFFFFFC0B0A0FFF8F02040C03058F06080FF5078F040
        60F02040B0D0C0C0F0D8D0E0C8C0D0B0A0604830FFFFFF000000FFFFFFC0B0A0
        FFF8F080A0FF8098FF8090F0D0D0E08098F04060E04058B0F0D8D0F0D8D0D0B8
        A0605040FFFFFF000000FFFFFFD0B0A0FFF8FFE0E8FFC0C8F0F0F0F0F0F0E0E0
        D8E08090F03058E05068B0F0E0D0E0C8B0705840FFFFFF000000FFFFFFD0B8A0
        FFFFFFFFF8FFFFF8F0FFF8F0FFF0F0F0F0E0F0E0E07088F02050D09090C0E0D0
        C0807060FFFFFF000000FFFFFFD0B8B0FFFFFFFFFFFFFFF8FFFFF8F0FFF8F0F0
        F0F0F0E0E0F0E8E08090F02048D0A098C0A09080FFFFFF000000FFFFFFD0C0B0
        FFFFFFFFFFFF80A0B060889060889060789060788070809090A0B090A0F03050
        D0B09890FFFFFF000000FFFFFFD0C0B0FFFFFFFFFFFF80A8B090D8E090E8F080
        D8F060C8E05098B0708090F0E8E0E0D8D0A09890FFFFFF000000FFFFFFD1C2B3
        FFFFFFFFFFFFF0F8FF80A8B0A0A8A095867780C8D0507080F0F0F0F0E0E0F0E0
        E0807060FFFFFF000000FFFFFFE2E5E1D1C2B3D0C0B0D0C0B070A8B0A0E8F0A0
        E8F090D0E0406870C0A8A0C0A8A0C0A890D6DAD6FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDCE3E180B0C080A0B07090A0D8DDD9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 104
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTitulosBordero'#39') IS NULL)'#13#10'BEGIN'#13#10#9'C' +
      'REATE TABLE #TempTitulosBordero(nCdTitulo int PRIMARY KEY'#13#10'     ' +
      '                              ,cFlgOK         int default 0 not ' +
      'null'#13#10'                                   ,cNrTit         char(17' +
      ')'#13#10'                                   ,cNrNF          char(15)'#13#10 +
      '                                   ,iParcela       int'#13#10'        ' +
      '                           ,dDtEmissao     datetime'#13#10'           ' +
      '                        ,dDtVenc        datetime'#13#10'              ' +
      '                     ,nSaldoTit      decimal(12,2) default 0 not' +
      ' null'#13#10'                                   ,nCdTerceiro    int'#13#10' ' +
      '                                  ,cNmTerceiro    varchar(50)'#13#10' ' +
      '                                  ,nCdFormaPagto  int'#13#10'         ' +
      '                          ,cNmFormaPagto  varchar(50)'#13#10'         ' +
      '                          ,nCdEspTit      int)                  ' +
      '                                                                ' +
      '       '#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 904
    Top = 129
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 936
    Top = 160
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempTitulosBordero'
      ' ORDER BY cNmTerceiro,dDtEmissao,cNrNf,iParcela')
    Left = 904
    Top = 161
    object qryTempnCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTempcFlgOK: TIntegerField
      DisplayLabel = 'OK'
      FieldName = 'cFlgOK'
    end
    object qryTempcNrTit: TStringField
      DisplayLabel = 'Nr.T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTempcNrNF: TStringField
      DisplayLabel = 'Nr NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTempiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTempdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTempdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTempnSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      DisplayFormat = '##,#00.00'
      EditFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
    object qryTempnCdTerceiro: TIntegerField
      DisplayLabel = 'Cod. Terceiro'
      FieldName = 'nCdTerceiro'
    end
    object qryTempcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTempnCdFormaPagto: TIntegerField
      DisplayLabel = 'C'#243'd. Forma Pagto'
      FieldName = 'nCdFormaPagto'
    end
    object qryTempcNmFormaPagto: TStringField
      DisplayLabel = 'Forma Pagto'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryTempnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
  end
  object SP_GERA_BORDERO_RECEBIMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_BORDERO_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 744
    Top = 320
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdFormaPagto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtEmissaoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtEmissaoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cFlgEmBordero'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cNRNF'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa         int'
      '       ,@nCdTerceiro        int'
      '       ,@nCdFormaPagto      int'
      '       ,@dDtEmissaoInicial  varchar(10)'
      '       ,@dDtEmissaoFinal    varchar(10)'
      '       ,@dDtVenctoInicial   varchar(10)'
      '       ,@dDtVenctoFinal     varchar(10)'
      '       ,@cFlgEmBordero      int'
      '       ,@cNRNF              varchar(15)'
      ''
      'SET @nCdEmpresa         = :nCdEmpresa'
      'SET @nCdTerceiro        = :nCdTerceiro'
      'SET @nCdFormaPagto      = :nCdFormaPagto'
      'SET @dDtEmissaoInicial  = :dDtEmissaoInicial'
      'SET @dDtEmissaoFinal    = :dDtEmissaoFinal'
      'SET @dDtVenctoInicial   = :dDtVenctoInicial'
      'SET @dDtVenctoFinal     = :dDtVenctoFinal'
      'SET @cFlgEmBordero      = :cFlgEmBordero'
      'SET @cNRNF              = :cNRNF'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempTitulosBordero'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #TempTitulosBordero(nCdTitulo int PRIMARY KEY'
      
        '                                   ,cFlgOK         int default 0' +
        ' not null'
      '                                   ,cNrTit         char(17)'
      '                                   ,cNrNF          char(15)'
      '                                   ,iParcela       int'
      '                                   ,dDtEmissao     datetime'
      '                                   ,dDtVenc        datetime'
      
        '                                   ,nSaldoTit      decimal(12,2)' +
        ' default 0 not null'
      '                                   ,nCdTerceiro    int'
      '                                   ,cNmTerceiro    varchar(50)'
      '                                   ,nCdFormaPagto  int'
      '                                   ,cNmFormaPagto  varchar(50)'
      
        '                                   ,nCdEspTit      int)         ' +
        '                                                                ' +
        '                '
      'END'
      ''
      'TRUNCATE TABLE #TempTitulosBordero'
      ''
      'INSERT INTO #TempTitulosBordero (nCdTitulo'
      '                               ,cNrTit'
      '                               ,cNrNF'
      '                               ,iParcela'
      '                               ,dDtEmissao'
      '                               ,dDtVenc'
      '                               ,nSaldoTit'
      '                               ,nCdTerceiro'
      '                               ,cNmTerceiro'
      '                               ,nCdFormaPagto'
      '                               ,cNmFormaPagto'
      '                               ,nCdEspTit)'
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNf'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nSaldoTit'
      '      ,Terceiro.nCdTerceiro'
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,50)'
      '      ,FormaPagto.nCdFormaPagto'
      '      ,FormaPagto.cNmFormaPagto'
      '      ,Titulo.nCdEspTit'
      '  FROM Titulo'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Titul' +
        'o.nCdTerceiro'
      
        '       LEFT  JOIN FormaPagto ON FormaPagto.nCdFormaPagto = Titul' +
        'o.nCdFormaPagtoTit'
      
        ' WHERE ((nCdEmpresa         = @nCdEmpresa)     OR (@nCdEmpresa  ' +
        '  = 0))'
      
        '   AND ((Titulo.nCdTerceiro = @nCdTerceiro)    OR (@nCdTerceiro ' +
        '  = 0))'
      
        '   AND ((nCdFormaPagtoTit   = @nCdFormaPagto)  OR (@nCdFormaPagt' +
        'o = 0))'
      
        '   AND ((dDtEmissao        >= Convert(DATETIME,@dDtEmissaoInicia' +
        'l,103)) OR (@dDtEmissaoInicial = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtEmissao         < Convert(DATETIME,@dDtEmissaoFinal,' +
        '103)+1) OR (@dDtEmissaoFinal   = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtVenc           >= Convert(DATETIME,@dDtVenctoInicial' +
        ',103))  OR (@dDtVenctoInicial  = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtVenc            < Convert(DATETIME,@dDtVenctoFinal,1' +
        '03)+1)  OR (@dDtVenctoFinal    = '#39'01/01/1900'#39'))'
      '   AND dDtLiq              IS NULL'
      '   AND cSenso               = '#39'C'#39
      '   AND dDtCancel           IS NULL'
      '   AND nSaldoTit            > 0'
      
        '   AND ((@cFlgEmBordero     = 1)      OR (Titulo.nCdBorderoFinan' +
        'ceiro IS NULL))'
      
        '   AND ((Titulo.cNrNF       = @cNRNF) OR (@cNRNF                ' +
        '       = '#39#39'))')
    Left = 936
    Top = 128
  end
  object qryVerificaSelecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM #TempTitulosBordero'
      ' WHERE cFlgOK = 1')
    Left = 920
    Top = 248
  end
  object qryValidaTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM #TempTitulosBordero'
      ' WHERE cFlgOK = 1'
      '   AND EXISTS (SELECT TOP 1 1'
      '                 FROM Titulo'
      
        '                WHERE Titulo.nCdTitulo = #TempTitulosBordero.nCd' +
        'Titulo'
      '                  AND Titulo.nCdBorderoFinanceiro IS NOT NULL)')
    Left = 952
    Top = 248
  end
end
