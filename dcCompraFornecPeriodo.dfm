inherited dcmCompraFornecPeriodo: TdcmCompraFornecPeriodo
  Left = 232
  Top = 91
  Width = 878
  Height = 362
  Caption = 'Data Analysis - Compras por Fornecedor'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 862
    Height = 295
  end
  object Label1: TLabel [1]
    Left = 66
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 31
    Top = 216
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Compra'
  end
  object Label6: TLabel [3]
    Left = 196
    Top = 216
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [4]
    Left = 38
    Top = 120
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label7: TLabel [5]
    Left = 78
    Top = 144
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label8: TLabel [6]
    Left = 82
    Top = 168
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label9: TLabel [7]
    Left = 76
    Top = 208
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label10: TLabel [8]
    Left = 52
    Top = 96
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fornecedor'
  end
  object Label2: TLabel [9]
    Left = 25
    Top = 72
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Econ'#244'mico'
  end
  inherited ToolBar1: TToolBar
    Width = 862
    TabOrder = 16
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 180
    Top = 40
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 10
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 243
    Top = 40
    Width = 587
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 11
  end
  object MaskEdit1: TMaskEdit [13]
    Left = 112
    Top = 208
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [14]
    Left = 224
    Top = 208
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [15]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit5: TMaskEdit [16]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [17]
    Tag = 1
    Left = 180
    Top = 112
    Width = 650
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 12
  end
  object MaskEdit7: TMaskEdit [18]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [19]
    Tag = 1
    Left = 180
    Top = 136
    Width = 650
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 13
  end
  object MaskEdit8: TMaskEdit [20]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [21]
    Tag = 1
    Left = 180
    Top = 160
    Width = 650
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 14
  end
  object MaskEdit9: TMaskEdit [22]
    Left = 112
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [23]
    Tag = 1
    Left = 180
    Top = 184
    Width = 650
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 9
  end
  object DBEdit8: TDBEdit [24]
    Tag = 1
    Left = 180
    Top = 88
    Width = 650
    Height = 21
    DataField = 'Nome/Raz'#227'o Social'
    DataSource = dsTerceiro
    TabOrder = 15
  end
  object MaskEdit10: TMaskEdit [25]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [26]
    Tag = 1
    Left = 180
    Top = 64
    Width = 650
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 17
  end
  object MaskEdit4: TMaskEdit [27]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  inherited ImageList1: TImageList
    Left = 648
    Top = 288
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 392
    Top = 256
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro as "C'#243'digo"  '
      '      ,cCNPJCPF    as "CNPJ / CPF"  '
      '      ,cNmTerceiro as "Nome/Raz'#227'o Social"  '
      '      ,cNmFantasia as "Nome Fantasia"  '
      '  FROM Terceiro'
      ' WHERE EXISTS(SELECT 1 '
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 1)  '
      '   AND nCdTerceiro = :nPK')
    Left = 456
    Top = 256
    object qryTerceiroCdigo: TIntegerField
      FieldName = 'C'#243'digo'
    end
    object qryTerceiroCNPJCPF: TStringField
      FieldName = 'CNPJ / CPF'
      Size = 14
    end
    object qryTerceiroNomeRazoSocial: TStringField
      FieldName = 'Nome/Raz'#227'o Social'
      Size = 50
    end
    object qryTerceiroNomeFantasia: TStringField
      FieldName = 'Nome Fantasia'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 392
    Top = 288
  end
  object DataSource2: TDataSource
    Left = 616
    Top = 288
  end
  object DataSource3: TDataSource
    Left = 648
    Top = 256
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 456
    Top = 288
  end
  object DataSource5: TDataSource
    Left = 616
    Top = 256
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 488
    Top = 256
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 520
    Top = 256
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 552
    Top = 256
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 584
    Top = 256
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 488
    Top = 288
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 520
    Top = 288
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 552
    Top = 288
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 584
    Top = 288
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoEconomico'
      ',cNmGrupoEconomico'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 256
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 424
    Top = 288
  end
end
