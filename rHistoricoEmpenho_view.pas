unit rHistoricoEmpenho_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, jpeg, DB, ADODB;

type
  TrptHistoricoEmpenho_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    DetailBand1: TQRBand;
    qryEstoqueEmpenhado: TADOQuery;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    qryEstoqueEmpenhadonCdEstoqueEmpenhado: TIntegerField;
    qryEstoqueEmpenhadonCdLocalEstoque: TIntegerField;
    qryEstoqueEmpenhadocNmLocalEstoque: TStringField;
    qryEstoqueEmpenhadonCdLocalEstoqueOrigem: TIntegerField;
    qryEstoqueEmpenhadocNmLocalEstoqueOrigem: TStringField;
    qryEstoqueEmpenhadonCdTabTipoOrigemEmpenho: TIntegerField;
    qryEstoqueEmpenhadocNmTabTipoOrigemEmpenho: TStringField;
    qryEstoqueEmpenhadodDtEmpenho: TDateTimeField;
    qryEstoqueEmpenhadonQtdeEstoqueEmpenhado: TBCDField;
    qryEstoqueEmpenhadonQtdeReal: TBCDField;
    qryEstoqueEmpenhadonQtdeConf: TBCDField;
    qryEstoqueEmpenhadodDtCancel: TDateTimeField;
    qryEstoqueEmpenhadonCdUsuarioCancel: TIntegerField;
    qryEstoqueEmpenhadocNmUsuario: TStringField;
    qryEstoqueEmpenhadocMotivoCancel: TStringField;
    QRDBText15: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape2: TQRShape;
    qryEstoqueEmpenhadonCdProduto: TIntegerField;
    qryEstoqueEmpenhadocNmProduto: TStringField;
    QRDBText5: TQRDBText;
    QRLabel19: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptHistoricoEmpenho_view: TrptHistoricoEmpenho_view;

implementation

uses
  fMenu;
{$R *.dfm}

end.
