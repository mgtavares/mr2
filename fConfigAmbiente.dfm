object frmConfigAmbiente: TfrmConfigAmbiente
  Left = 402
  Top = 167
  Width = 600
  Height = 648
  BorderIcons = []
  Caption = 'Configura'#231#227'o de Ambiente'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 592
    Height = 567
    Align = alTop
    Color = clWhite
    ParentColor = False
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 58
      Height = 13
      Caption = 'Workstation'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 32
      Width = 567
      Height = 21
      DataField = 'cNmComputador'
      DataSource = dsAmbiente
      Enabled = False
      TabOrder = 0
    end
    object cxPageControl1: TcxPageControl
      Left = 8
      Top = 56
      Width = 569
      Height = 505
      ActivePage = tabTEF
      LookAndFeel.NativeStyle = True
      TabOrder = 1
      ClientRectBottom = 505
      ClientRectRight = 569
      ClientRectTop = 23
      object tabPDV: TcxTabSheet
        Caption = 'PDV/Caixa'
        ImageIndex = 0
        object GroupBox2: TGroupBox
          Left = 11
          Top = 3
          Width = 538
          Height = 144
          Caption = ' Impressoras '
          TabOrder = 0
          object Label2: TLabel
            Left = 8
            Top = 16
            Width = 105
            Height = 13
            Caption = 'Porta Impressora PDV'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 144
            Top = 16
            Width = 160
            Height = 13
            Caption = 'Tipo de Impressora do Pr'#233'-Venda'
            FocusControl = DBEdit2
          end
          object Label29: TLabel
            Left = 392
            Top = 16
            Width = 116
            Height = 13
            Caption = 'Imp. PDV Compartilhada'
          end
          object Label6: TLabel
            Left = 8
            Top = 56
            Width = 126
            Height = 13
            Caption = 'Porta Impressora Etiqueta'
            FocusControl = DBEdit5
          end
          object Label26: TLabel
            Left = 144
            Top = 56
            Width = 137
            Height = 13
            Caption = 'Tipo de Impressora do Caixa'
            FocusControl = DBEdit2
          end
          object Label28: TLabel
            Left = 392
            Top = 56
            Width = 137
            Height = 13
            Caption = 'Imp. Etiqueta Compartilhada'
          end
          object DBEdit2: TDBEdit
            Left = 8
            Top = 32
            Width = 129
            Height = 21
            DataField = 'cPortaMatricial'
            DataSource = dsAmbiente
            TabOrder = 0
          end
          object DBLookupComboBox1: TDBLookupComboBox
            Left = 144
            Top = 32
            Width = 241
            Height = 21
            DataField = 'nCdTipoImpressoraPDV'
            DataSource = dsAmbiente
            KeyField = 'nCdTipoImpressoraPDV'
            ListField = 'cNmTipoImpressoraPDV'
            ListSource = dsImpressoraPDV
            TabOrder = 1
          end
          object DBEdit25: TDBEdit
            Left = 392
            Top = 32
            Width = 137
            Height = 21
            DataField = 'cNmImpCompartilhadaPDV'
            DataSource = dsAmbiente
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 72
            Width = 129
            Height = 21
            DataField = 'cPortaEtiqueta'
            DataSource = dsAmbiente
            TabOrder = 3
          end
          object DBLookupComboBox2: TDBLookupComboBox
            Left = 144
            Top = 72
            Width = 241
            Height = 21
            DataField = 'nCdTipoImpressoraPDVCaixa'
            DataSource = dsAmbiente
            KeyField = 'nCdTipoImpressoraPDV'
            ListField = 'cNmTipoImpressoraPDV'
            ListSource = dsTipoImpressoraCaixa
            TabOrder = 4
          end
          object DBEdit26: TDBEdit
            Left = 392
            Top = 72
            Width = 137
            Height = 21
            DataField = 'cNmImpCompartilhadaEtiqueta'
            DataSource = dsAmbiente
            TabOrder = 5
          end
          object DBCheckBox6: TDBCheckBox
            Left = 16
            Top = 103
            Width = 145
            Height = 33
            Caption = 'Terminal com Impressora PDV Mapeada'
            DataField = 'cFlgUsaMapeamentoAutPDV'
            DataSource = dsAmbiente
            TabOrder = 6
            ValueChecked = '1'
            ValueUnchecked = '0'
            WordWrap = True
          end
          object DBCheckBox7: TDBCheckBox
            Left = 176
            Top = 103
            Width = 145
            Height = 33
            Caption = 'Terminal com Impressora Etiqueta Mapeada'
            DataField = 'cFlgUsaMapeamentoAutEtiqueta'
            DataSource = dsAmbiente
            TabOrder = 7
            ValueChecked = '1'
            ValueUnchecked = '0'
            WordWrap = True
          end
        end
        object GroupBox3: TGroupBox
          Left = 11
          Top = 149
          Width = 538
          Height = 73
          Caption = ' ECF '
          TabOrder = 1
          object Label4: TLabel
            Left = 128
            Top = 16
            Width = 104
            Height = 13
            Caption = '# Vias Impressas ECF'
          end
          object Label35: TLabel
            Left = 158
            Top = 45
            Width = 73
            Height = 13
            Caption = 'Velocidade ECF'
          end
          object DBCheckBox1: TDBCheckBox
            Left = 16
            Top = 14
            Width = 105
            Height = 22
            Caption = 'Terminal com Impressora ECF'
            DataField = 'cFlgUsaECF'
            DataSource = dsAmbiente
            TabOrder = 0
            ValueChecked = '1'
            ValueUnchecked = '0'
            WordWrap = True
          end
          object DBEdit3: TDBEdit
            Left = 236
            Top = 13
            Width = 66
            Height = 21
            DataField = 'iViasECF'
            DataSource = dsAmbiente
            TabOrder = 1
          end
          object btResetaConfigECF: TcxButton
            Left = 368
            Top = 19
            Width = 153
            Height = 33
            Caption = 'Resetar Config. da ECF'
            TabOrder = 2
            OnClick = btResetaConfigECFClick
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              18000000000000030000C40E0000C40E00000000000000000000FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
              FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC4C6F3656BE05259DC53
              5BDC5058DA4952DA5960DCB7BAF0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
              FF00FFC6C8F3747CE78997FA8495FF7F90FC7D8EFA7D8DF77D8BF25159DD9B9F
              EAFF00FFFF00FFFF00FFFF00FFFF00FFCACCF47E86E796A5FA5A74FF3250FF30
              4FFF2C49FE2542FA4860F98694F46970E2BDC0F1FF00FFFF00FFFF00FFD0D2F5
              878EE99FADFB6781FF405EFF405EFF3C59FF3755FF3350FF2846FD4A65FD8996
              F6656EE1C1C3F2FF00FFFF00FF888EE8A2B2FC738FFF4F70FF4F6EFF4B69FF46
              64FF415EFF3C5AFF3755FF2C4BFF4E67FF8493FA5F66DEFF00FFFF00FF848AE6
              A6BBFF5F7FFF5F7EFF5A79FF5573FF506EFF4B69FF4664FF415EFF3B59FF314F
              FF8799FF565EDDFF00FFFF00FF9094E8ABBEFF6D8DFF6989FF6583FF5F7EFF5A
              79FF5574FF506FFF4B69FF4663FF3F5CFF8A9BFF6269DFFF00FFFF00FF959AE9
              B1C4FF7698FF7393FF6E8EFF6989FF6583FF5F7EFF5A79FF5573FF4F6EFF4867
              FF90A1FF686FE1FF00FFFF00FF989DEAB8CDFF7DA0FF7C9DFF7899FF7393FF6E
              8EFF6989FF6583FF607EFF5978FF4F70FF98AAFF6B72E2FF00FFFF00FFA7ABEE
              BCCDFC9CBBFF81A5FF81A2FF7C9EFF7899FF7493FF6F8EFF6989FF6080FF7893
              FF9EADFB7F85E5FF00FFFF00FFDFE1F8AFB5F1C1D1FCA0BFFF86AAFF85A7FF81
              A2FF7C9DFF7898FF6F90FF85A1FFACBAFB9097EAD2D3F6FF00FFFF00FFFF00FF
              E0E1F8B1B7F0C4D4FCA3C2FF8BAFFF89ABFF84A7FF7EA0FF91AEFFB4C3FB9CA2
              EDD5D6F6FF00FFFF00FFFF00FFFF00FFFF00FFE0E1F8B2B9F2C2D3FCC1D6FFBD
              D1FFBBCFFFB9CEFFB7C8FC989FEDC3C5F2FF00FFFF00FFFF00FFFF00FFFF00FF
              FF00FFFF00FFE1E2F9AFB3EFA6AAEDA5AAEDA4A8ED9EA4EBA5A9EDD8D9F7FF00
              FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
              00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
            LookAndFeel.NativeStyle = True
          end
          object dbCmbVeloECF: TDBComboBox
            Left = 236
            Top = 38
            Width = 67
            Height = 19
            Style = csOwnerDrawFixed
            DataField = 'iBaudECF'
            DataSource = dsAmbiente
            ItemHeight = 13
            Items.Strings = (
              '9600'
              '115200')
            TabOrder = 3
          end
        end
        object GroupBox4: TGroupBox
          Left = 11
          Top = 225
          Width = 538
          Height = 72
          Caption = ' Gaveta Eletr'#244'nica '
          TabOrder = 2
          object Label27: TLabel
            Left = 176
            Top = 22
            Width = 72
            Height = 13
            Caption = 'Modelo Gaveta'
          end
          object Label30: TLabel
            Left = 408
            Top = 22
            Width = 26
            Height = 13
            Caption = 'Porta'
          end
          object DBLookupComboBox3: TDBLookupComboBox
            Left = 176
            Top = 38
            Width = 222
            Height = 21
            DataField = 'nCdTipoGavetaPDV'
            DataSource = dsAmbiente
            KeyField = 'nCdTipoGavetaPDV'
            ListField = 'cNmTipoGavetaPDV'
            ListSource = dsTipoGavetaPDV
            TabOrder = 0
          end
          object DBComboBox2: TDBComboBox
            Left = 408
            Top = 38
            Width = 121
            Height = 19
            Style = csOwnerDrawFixed
            DataField = 'cNmPortaGaveta'
            DataSource = dsAmbiente
            ItemHeight = 13
            Items.Strings = (
              'COM1'
              'COM2'
              'COM3'
              'COM4'
              'COM5'
              'LPT1'
              'LPT2'
              'LPT3')
            TabOrder = 1
          end
          object DBCheckBox8: TDBCheckBox
            Left = 16
            Top = 32
            Width = 122
            Height = 33
            Caption = 'Terminal com Gaveta Eletr'#244'nica'
            DataField = 'cFlgUsaGaveta'
            DataSource = dsAmbiente
            TabOrder = 2
            ValueChecked = '1'
            ValueUnchecked = '0'
            WordWrap = True
          end
        end
        object GroupBox5: TGroupBox
          Left = 11
          Top = 300
          Width = 538
          Height = 112
          Caption = ' Imagem '
          TabOrder = 3
          object Label31: TLabel
            Left = 8
            Top = 16
            Width = 70
            Height = 13
            Caption = 'Path Logo PDV'
            FocusControl = DBEdit27
          end
          object Label32: TLabel
            Left = 8
            Top = 56
            Width = 78
            Height = 13
            Caption = 'Path Logo Caixa'
            FocusControl = DBEdit28
          end
          object Label33: TLabel
            Left = 453
            Top = 54
            Width = 49
            Height = 11
            Alignment = taRightJustify
            Caption = '153 x 185px'
            FocusControl = DBEdit28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label34: TLabel
            Left = 453
            Top = 94
            Width = 49
            Height = 11
            Alignment = taRightJustify
            Caption = '237 x 105px'
            FocusControl = DBEdit28
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object btImgPDV: TcxButton
            Left = 508
            Top = 30
            Width = 25
            Height = 25
            TabOrder = 0
            OnClick = btImgPDVClick
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              1800000000000003000000000000000000000000000000000000FF00FF76B2E6
              3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
              DA85B9E9FF00FFFF00FF4799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
              D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBFF00FFFF00FF3B97DBEFFAFE
              A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
              FB3B97DBFF00FFFF00FF3C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
              E0F77CE0F762DAF554D6F347D3F2E8F9FD3594DAFF00FFFF00FF3BA3DBF6FCFE
              C8F2FCB9EFFB91E5F88CE4F88AE3F882E1F781E2F86DDDF661DAF557D7F4E7F8
              FD3594DAFF00FFFF00FF3BA8DBFEFFFFF8FDFFF6FDFFF6FDFFE8FAFEAFECFA8E
              E4F889E4F87DE0F772DDF668DBF5E9F9FD3594DAFF00FFFF00FF39ADDBE8F6FB
              7EC5EA4AA3DF459FDE4CA3DFEDEFEDECEBE9E7E4E2E7E5E2E7E3E0E7E5E2F2E9
              E33594DAC69F85D7BFAF40AEDCF1FAFD94DEF593DCF481D5F260C0E95696C535
              94DA3594DA3594DA3594DA3594DA3594DA3594DAF2EEEAC89F8641B4DCF7FCFE
              8EE4F891DEF59FE0F5ACE1F6C29579F6F1ED47A270429D6B3C9766369160318B
              5A2C8655F7EDE6C59A7F3CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFC39679F7
              F1ECDCBE9EDEBD9B4F9ECC3178A1DAB791D6B28DF6EEE6C59B8059C2E061C3E2
              63C4E363C4E363C4E362C4E3BD987EF7F3EEDFC7AE65A66C39AD6A2FA85E4E97
              4ED2AE89F6EEE7C7A086FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
              F4EFE1CFBB40C08B4FC79948C28D31AA63CBAA89F6EEE7C7A086FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFC3967AF7F5F0E7D9C848C89C5ACFA853CA9D39B4
              75D2B79AF5EFE7C49B80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
              F5F4E9DBCC5EBC904ACA9F45C49558AE77DCC8B1F7F0E9C8A086FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFC8A791EBEBEAF7F5F4F7F5F4F7F5F4F7F5F4F7F5
              F4F7F5F4F2EEE8C69F85FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD8C3B5C8
              A692C69A7EC79B7FC39678C79A7FC79A7FC79A7FC59B80D7BFAF}
            LookAndFeel.NativeStyle = True
          end
          object btImgCaixa: TcxButton
            Left = 508
            Top = 70
            Width = 25
            Height = 25
            TabOrder = 1
            OnClick = btImgCaixaClick
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              1800000000000003000000000000000000000000000000000000FF00FF76B2E6
              3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
              DA85B9E9FF00FFFF00FF4799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
              D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBFF00FFFF00FF3B97DBEFFAFE
              A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
              FB3B97DBFF00FFFF00FF3C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
              E0F77CE0F762DAF554D6F347D3F2E8F9FD3594DAFF00FFFF00FF3BA3DBF6FCFE
              C8F2FCB9EFFB91E5F88CE4F88AE3F882E1F781E2F86DDDF661DAF557D7F4E7F8
              FD3594DAFF00FFFF00FF3BA8DBFEFFFFF8FDFFF6FDFFF6FDFFE8FAFEAFECFA8E
              E4F889E4F87DE0F772DDF668DBF5E9F9FD3594DAFF00FFFF00FF39ADDBE8F6FB
              7EC5EA4AA3DF459FDE4CA3DFEDEFEDECEBE9E7E4E2E7E5E2E7E3E0E7E5E2F2E9
              E33594DAC69F85D7BFAF40AEDCF1FAFD94DEF593DCF481D5F260C0E95696C535
              94DA3594DA3594DA3594DA3594DA3594DA3594DAF2EEEAC89F8641B4DCF7FCFE
              8EE4F891DEF59FE0F5ACE1F6C29579F6F1ED47A270429D6B3C9766369160318B
              5A2C8655F7EDE6C59A7F3CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFC39679F7
              F1ECDCBE9EDEBD9B4F9ECC3178A1DAB791D6B28DF6EEE6C59B8059C2E061C3E2
              63C4E363C4E363C4E362C4E3BD987EF7F3EEDFC7AE65A66C39AD6A2FA85E4E97
              4ED2AE89F6EEE7C7A086FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
              F4EFE1CFBB40C08B4FC79948C28D31AA63CBAA89F6EEE7C7A086FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFC3967AF7F5F0E7D9C848C89C5ACFA853CA9D39B4
              75D2B79AF5EFE7C49B80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
              F5F4E9DBCC5EBC904ACA9F45C49558AE77DCC8B1F7F0E9C8A086FF00FFFF00FF
              FF00FFFF00FFFF00FFFF00FFC8A791EBEBEAF7F5F4F7F5F4F7F5F4F7F5F4F7F5
              F4F7F5F4F2EEE8C69F85FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD8C3B5C8
              A692C69A7EC79B7FC39678C79A7FC79A7FC79A7FC59B80D7BFAF}
            LookAndFeel.NativeStyle = True
          end
          object DBEdit27: TDBEdit
            Left = 8
            Top = 32
            Width = 497
            Height = 21
            DataField = 'cPathLogoPDV'
            DataSource = dsAmbiente
            TabOrder = 2
          end
          object DBEdit28: TDBEdit
            Left = 8
            Top = 72
            Width = 497
            Height = 21
            DataField = 'cPathLogoCaixa'
            DataSource = dsAmbiente
            TabOrder = 3
          end
        end
        object GroupBox8: TGroupBox
          Left = 11
          Top = 415
          Width = 538
          Height = 58
          Caption = ' WideScreen '
          TabOrder = 4
          Visible = False
          object DBCheckBox10: TDBCheckBox
            Left = 16
            Top = 24
            Width = 97
            Height = 17
            Caption = 'PDV/Caixa'
            DataField = 'cFlgWidePdvCaixa'
            DataSource = dsAmbiente
            TabOrder = 0
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
      end
      object tabTEF: TcxTabSheet
        Caption = 'TEF'
        ImageIndex = 3
        object Label16: TLabel
          Left = 8
          Top = 40
          Width = 60
          Height = 13
          Caption = 'Arquivo LOG'
          FocusControl = DBEdit16
        end
        object Label18: TLabel
          Left = 8
          Top = 80
          Width = 61
          Height = 13
          Caption = 'Arquivo REQ'
          FocusControl = DBEdit17
        end
        object Label19: TLabel
          Left = 8
          Top = 120
          Width = 65
          Height = 13
          Caption = 'Arquivo RESP'
          FocusControl = DBEdit18
        end
        object Label20: TLabel
          Left = 8
          Top = 160
          Width = 58
          Height = 13
          Caption = 'Arquivo STS'
          FocusControl = DBEdit19
        end
        object Label21: TLabel
          Left = 8
          Top = 200
          Width = 66
          Height = 13
          Caption = 'Arquivo TEMP'
          FocusControl = DBEdit20
        end
        object Label22: TLabel
          Left = 8
          Top = 240
          Width = 294
          Height = 13
          Caption = 'Execut'#225'vel Gerenciador Padr'#227'o TEF DIAL (caminho completo)'
          FocusControl = DBEdit21
        end
        object Label23: TLabel
          Left = 8
          Top = 280
          Width = 111
          Height = 13
          Caption = 'Patch Backup TEF DIAL'
          FocusControl = DBEdit22
        end
        object Label24: TLabel
          Left = 8
          Top = 320
          Width = 54
          Height = 13
          Caption = 'Espera STS'
          FocusControl = DBEdit23
        end
        object Label25: TLabel
          Left = 152
          Top = 320
          Width = 119
          Height = 13
          Caption = '# Vias Comprovante TEF'
          FocusControl = DBEdit24
        end
        object Label12: TLabel
          Left = 8
          Top = 376
          Width = 97
          Height = 13
          Caption = 'Chave Autentica'#231#227'o'
          FocusControl = DBEdit23
        end
        object Label69: TLabel
          Left = 8
          Top = 416
          Width = 25
          Height = 13
          Caption = 'CNPJ'
          FocusControl = DBEdit23
        end
        object Label70: TLabel
          Left = 192
          Top = 416
          Width = 19
          Height = 13
          Caption = 'PDV'
          FocusControl = DBEdit23
        end
        object DBEdit16: TDBEdit
          Left = 8
          Top = 56
          Width = 430
          Height = 21
          DataField = 'cTefDialArqLog'
          DataSource = dsAmbiente
          TabOrder = 1
        end
        object DBEdit17: TDBEdit
          Left = 8
          Top = 96
          Width = 430
          Height = 21
          DataField = 'cTefDialArqReq'
          DataSource = dsAmbiente
          TabOrder = 2
        end
        object DBEdit18: TDBEdit
          Left = 8
          Top = 136
          Width = 430
          Height = 21
          DataField = 'cTefDialArqResp'
          DataSource = dsAmbiente
          TabOrder = 3
        end
        object DBEdit19: TDBEdit
          Left = 8
          Top = 176
          Width = 430
          Height = 21
          DataField = 'cTefDialArqSTS'
          DataSource = dsAmbiente
          TabOrder = 4
        end
        object DBEdit20: TDBEdit
          Left = 8
          Top = 216
          Width = 430
          Height = 21
          DataField = 'cTefDialArqTemp'
          DataSource = dsAmbiente
          TabOrder = 5
        end
        object DBEdit21: TDBEdit
          Left = 8
          Top = 256
          Width = 430
          Height = 21
          DataField = 'cTefDialGPExeName'
          DataSource = dsAmbiente
          TabOrder = 6
        end
        object DBEdit22: TDBEdit
          Left = 8
          Top = 296
          Width = 430
          Height = 21
          DataField = 'cTefDialPatchBackup'
          DataSource = dsAmbiente
          TabOrder = 7
        end
        object DBEdit23: TDBEdit
          Left = 8
          Top = 336
          Width = 134
          Height = 21
          DataField = 'iTefDialEsperaSTS'
          DataSource = dsAmbiente
          TabOrder = 8
        end
        object DBEdit24: TDBEdit
          Left = 152
          Top = 336
          Width = 134
          Height = 21
          DataField = 'iTefDialNumVias'
          DataSource = dsAmbiente
          TabOrder = 9
        end
        object DBCheckBox3: TDBCheckBox
          Left = 296
          Top = 320
          Width = 129
          Height = 33
          Caption = 'AutoAtivar Gerenciador Padr'#227'o'
          DataField = 'cTefDialAutoAtivarGP'
          DataSource = dsAmbiente
          TabOrder = 10
          ValueChecked = '1'
          ValueUnchecked = '0'
          WordWrap = True
        end
        object DBCheckBox5: TDBCheckBox
          Left = 8
          Top = 0
          Width = 129
          Height = 33
          Caption = 'TEF Ativado'
          DataField = 'cFlgTEFAtivo'
          DataSource = dsAmbiente
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
          WordWrap = True
        end
        object DBEdit11: TDBEdit
          Left = 8
          Top = 392
          Width = 430
          Height = 21
          DataField = 'cChaveAutTEF'
          DataSource = dsAmbiente
          TabOrder = 11
        end
        object DBEdit30: TDBEdit
          Left = 8
          Top = 432
          Width = 177
          Height = 21
          DataField = 'cCNPJTEF'
          DataSource = dsAmbiente
          TabOrder = 12
        end
        object DBEdit47: TDBEdit
          Left = 192
          Top = 432
          Width = 94
          Height = 21
          DataField = 'nPDVTEF'
          DataSource = dsAmbiente
          TabOrder = 13
        end
      end
      object tabNFeNFCe: TcxTabSheet
        Caption = 'NF-e / NFC-e'
        ImageIndex = 2
        object Label10: TLabel
          Left = 8
          Top = 152
          Width = 70
          Height = 13
          Caption = 'Path Logo NFe'
          FocusControl = DBEdit9
        end
        object Image1: TImage
          Left = 8
          Top = 200
          Width = 339
          Height = 144
        end
        object Label11: TLabel
          Left = 297
          Top = 348
          Width = 49
          Height = 11
          Alignment = taRightJustify
          Caption = '338 x 144px'
          FocusControl = DBEdit28
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBCheckBox2: TDBCheckBox
          Left = 8
          Top = 0
          Width = 129
          Height = 33
          Caption = 'Emissor NFe'
          DataField = 'cFlgEmiteNFe'
          DataSource = dsAmbiente
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
          WordWrap = True
        end
        object DBEdit9: TDBEdit
          Left = 8
          Top = 168
          Width = 338
          Height = 21
          DataField = 'cPathLogoNFe'
          DataSource = dsAmbiente
          TabOrder = 2
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 8
          Top = 376
          Width = 185
          Height = 37
          Caption = ' Tipo Ambiente NF-e / NFC-e '
          Columns = 2
          DataField = 'nCdTipoAmbienteNFe'
          DataSource = dsAmbiente
          Items.Strings = (
            'Produ'#231#227'o'
            'Homologa'#231#227'o')
          TabOrder = 3
          Values.Strings = (
            '0'
            '1')
        end
        object gbCertificadoDigital: TGroupBox
          Left = 8
          Top = 32
          Width = 544
          Height = 113
          Caption = ' Certificado Digital (Utilizado para NF-e e NFC-e) '
          TabOrder = 1
          object Label5: TLabel
            Left = 11
            Top = 24
            Width = 292
            Height = 13
            Caption = 'N'#250'mero S'#233'rie Certificado Padr'#227'o (Utilizado para NFe e NFCe)'
          end
          object Label7: TLabel
            Left = 11
            Top = 64
            Width = 103
            Height = 13
            Caption = 'C'#243'd. Munic'#237'pio (IBGE)'
            FocusControl = DBEdit6
          end
          object Label8: TLabel
            Left = 147
            Top = 64
            Width = 93
            Height = 13
            Caption = 'C'#243'd. Estado (IBGE)'
            FocusControl = DBEdit7
          end
          object Label9: TLabel
            Left = 323
            Top = 64
            Width = 54
            Height = 13
            Caption = 'UF Emiss'#227'o'
            FocusControl = DBEdit8
          end
          object DBEdit4: TDBEdit
            Left = 11
            Top = 38
            Width = 494
            Height = 21
            DataField = 'cNrSerieCertificadoNFe'
            DataSource = dsAmbiente
            TabOrder = 0
            OnExit = DBEdit4Exit
          end
          object btSelCertificadoDigPadrao: TcxButton
            Left = 506
            Top = 36
            Width = 25
            Height = 25
            Hint = 'Selecionar certificado digital padr'#227'o'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btSelCertificadoDigPadraoClick
            Glyph.Data = {
              D6020000424DD6020000000000003600000028000000100000000E0000000100
              180000000000A002000000000000000000000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE26632E97E53FFFFFFFFCB9DE35B
              21FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF79759EC3F03E5F6FDD8773CE35B21FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7A070009AE900A8EC00ACEFA855
              31FFFFFFFFFFFFFFFFFF7C9BAF38586D38586D38586D38586D38586D38586D38
              586D0986C7007BB90088C400B3E803A4E138586D38586D38586D7C9BAFC2CFD7
              CBD8DECBD8DECBD8DECBD8DECBD8DECBD8DE007BAE178BC264D1FA0088C40095
              D7CBD8DECBD8DE38586D7C9BAFCBD8DEFEFEFDF5F1EEF2EFF0F2F1F1FFFFFFFF
              FFFF0383B000ABDF1C99C80095D70077B7FFFFFFCBD8DE38586D7C9BAFCAD7DE
              FDFDFCD5B5B1D5B1ACD4B0ABFFFFFFFFFFFFCDE6EF0383B00086BD0086BDCDE6
              EFFFFFFFCBD8DE38586D7C9BAFCAD7DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBD8DE38586D7C9BAFCBD7DE
              FFFFFFFFFFFFFFFFFFFFFFFFE3D3D1DEC7C3DEC5C2E0CAC6F1E9E7FFFFFFFFFF
              FFFFFFFFDAE5EA38586D7C9BAFCCD8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCE6EB38586D7C9BAFCDD9DF
              FFFFFFCE9E97D8B1ACCE9E97CE9E97CE9E97CE9E97CE9E97E0C1BDCE9E97CE9E
              97FFFFFFDEE7EC38586D7C9BAFD2DDE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1EAEF38586D7C9BAFB9C8D3
              D5DFE5D3DCE2D2D8DED3DADFD4DCE1D7DFE3D7DFE3D8DFE3DAE1E5DBE1E5DFE7
              EBE5ECF0D1DFE638586D7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C
              9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF7C9BAF38586D}
            LookAndFeel.NativeStyle = True
          end
          object DBEdit6: TDBEdit
            Left = 11
            Top = 80
            Width = 65
            Height = 21
            DataField = 'nCdMunicipioEmissaoNFe'
            DataSource = dsAmbiente
            MaxLength = 7
            TabOrder = 2
          end
          object DBEdit7: TDBEdit
            Left = 147
            Top = 80
            Width = 65
            Height = 21
            DataField = 'nCdUFEmissaoNFe'
            DataSource = dsAmbiente
            MaxLength = 2
            TabOrder = 3
          end
          object DBEdit8: TDBEdit
            Left = 323
            Top = 80
            Width = 65
            Height = 21
            CharCase = ecUpperCase
            DataField = 'cUFEmissaoNfe'
            DataSource = dsAmbiente
            TabOrder = 4
          end
        end
      end
      object tabEmail: TcxTabSheet
        Caption = 'Email'
        ImageIndex = 1
        object Label13: TLabel
          Left = 8
          Top = 56
          Width = 197
          Height = 13
          Caption = 'Servidor SMTP (ex.: mail.er2soft.com.br)'
        end
        object Label14: TLabel
          Left = 8
          Top = 96
          Width = 28
          Height = 13
          Caption = 'E-Mail'
        end
        object Label15: TLabel
          Left = 200
          Top = 96
          Width = 30
          Height = 13
          Caption = 'Senha'
        end
        object Label17: TLabel
          Left = 304
          Top = 56
          Width = 55
          Height = 13
          Caption = 'Porta SMTP'
        end
        object DBEdit12: TDBEdit
          Left = 8
          Top = 72
          Width = 289
          Height = 21
          DataField = 'cServidorSMTP'
          DataSource = dsAmbiente
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 304
          Top = 72
          Width = 73
          Height = 21
          DataField = 'nPortaSMTP'
          DataSource = dsAmbiente
          TabOrder = 1
        end
        object DBEdit14: TDBEdit
          Left = 8
          Top = 112
          Width = 185
          Height = 21
          DataField = 'cEmail'
          DataSource = dsAmbiente
          TabOrder = 2
        end
        object DBEdit15: TDBEdit
          Left = 200
          Top = 112
          Width = 177
          Height = 21
          DataField = 'cSenhaEmail'
          DataSource = dsAmbiente
          PasswordChar = '*'
          TabOrder = 3
        end
        object DBCheckBox4: TDBCheckBox
          Left = 8
          Top = 136
          Width = 153
          Height = 17
          Caption = 'Usar conex'#227'o segura SSL'
          DataField = 'cFlgUsaSSL'
          DataSource = dsAmbiente
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object btnVerificaConexao: TcxButton
          Left = 8
          Top = 160
          Width = 137
          Height = 33
          Caption = 'Testar configura'#231#227'o...'
          TabOrder = 5
          OnClick = btnVerificaConexaoClick
          LookAndFeel.NativeStyle = True
        end
        object rgTipoConta: TRadioGroup
          Left = 8
          Top = 3
          Width = 544
          Height = 41
          Caption = ' Tipo da Conta '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Pessoal'
            'Gmail'
            'Hotmail'
            'Yahoo')
          TabOrder = 6
          OnClick = rgTipoContaClick
        end
      end
      object tabCFe: TcxTabSheet
        Caption = 'SAT-CF-e'
        ImageIndex = 4
        object Label42: TLabel
          Left = 367
          Top = 9
          Width = 185
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nota: CF-e-SAT substitui o uso da ECF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object rgModoEnvioCFe: TDBRadioGroup
          Left = 8
          Top = 32
          Width = 544
          Height = 33
          Caption = ' Modo de Transmiss'#227'o '
          Columns = 3
          DataField = 'cModoEnvioCFe'
          DataSource = dsAmbiente
          Items.Strings = (
            'NFC-e'
            'SAT'
            'NFC-e e Conting. SAT')
          TabOrder = 1
          Values.Strings = (
            'N'
            'S'
            'C')
          Visible = False
        end
        object DBCheckBox9: TDBCheckBox
          Left = 8
          Top = 0
          Width = 201
          Height = 33
          Caption = 'Cupom Fiscal Eletr'#244'nico Ativo (CF-e)'
          DataField = 'cFlgCFeAtivo'
          DataSource = dsAmbiente
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
          WordWrap = True
        end
        object cxPageControl2: TcxPageControl
          Left = 0
          Top = 75
          Width = 569
          Height = 407
          ActivePage = tabSATConfig
          Align = alBottom
          LookAndFeel.NativeStyle = True
          TabOrder = 2
          ClientRectBottom = 407
          ClientRectRight = 569
          ClientRectTop = 23
          object tabSATConfig: TcxTabSheet
            Caption = 'Configura'#231#245'es'
            ImageIndex = 1
            object Label44: TLabel
              Left = 8
              Top = 8
              Width = 55
              Height = 13
              Caption = 'N'#250'm. Caixa'
              FocusControl = DBEdit32
            end
            object Label36: TLabel
              Left = 8
              Top = 56
              Width = 130
              Height = 13
              Caption = 'C'#243'digo de Ativa'#231#227'o do SAT'
              FocusControl = DBEdit29
            end
            object Label38: TLabel
              Left = 8
              Top = 104
              Width = 192
              Height = 13
              Caption = 'C'#243'digo de Assinatura do SAT (ER2 Soft)'
              FocusControl = dbeAssAC
            end
            object lblCNPJAC: TLabel
              Left = 8
              Top = 152
              Width = 78
              Height = 13
              Caption = 'CNPJ (ER2 Soft)'
              FocusControl = dbeCNPJAC
            end
            object Label39: TLabel
              Tag = 1
              Left = 447
              Top = 96
              Width = 95
              Height = 13
              Alignment = taRightJustify
              Caption = 'm'#237'nimo 8 caracteres'
            end
            object Label37: TLabel
              Left = 288
              Top = 56
              Width = 193
              Height = 13
              Caption = 'Confirma'#231#227'o C'#243'digo de Ativa'#231#227'o do SAT'
              FocusControl = DBEdit29
            end
            object Label40: TLabel
              Left = 119
              Top = 8
              Width = 93
              Height = 13
              Caption = 'C'#243'd. Estado (IBGE)'
              FocusControl = DBEdit31
            end
            object Label41: TLabel
              Left = 247
              Top = 8
              Width = 56
              Height = 13
              Caption = 'Modelo SAT'
            end
            object Label68: TLabel
              Left = 424
              Top = 8
              Width = 57
              Height = 13
              Caption = 'Arquivo DLL'
              FocusControl = DBEdit34
            end
            object DBEdit32: TDBEdit
              Left = 8
              Top = 24
              Width = 81
              Height = 21
              Hint = 
                'N'#250'mero de 1 a 999 referente ao caixa em que o SAT est'#225' conectado' +
                '.'
              DataField = 'nCdCodPDVSAT'
              DataSource = dsAmbiente
              MaxLength = 3
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
            end
            object DBEdit29: TDBEdit
              Left = 8
              Top = 72
              Width = 265
              Height = 21
              DataField = 'cCdAtivacaoSAT'
              DataSource = dsAmbiente
              MaxLength = 32
              PasswordChar = '*'
              TabOrder = 4
            end
            object dbeAssAC: TDBEdit
              Left = 8
              Top = 120
              Width = 537
              Height = 21
              DataField = 'cAssinaturaACSAT'
              DataSource = dsAmbiente
              MaxLength = 344
              TabOrder = 6
            end
            object dbeCNPJAC: TDBEdit
              Tag = 1
              Left = 8
              Top = 168
              Width = 180
              Height = 21
              DataField = 'cCNPJACSAT'
              DataSource = dsAmbiente
              TabOrder = 7
            end
            object GroupBox6: TGroupBox
              Left = 8
              Top = 200
              Width = 273
              Height = 125
              Caption = '     '
              TabOrder = 9
              object Label45: TLabel
                Left = 88
                Top = 20
                Width = 74
                Height = 13
                Hint = 'Modelo da impressora'
                Caption = 'Modelo Impres.'
              end
              object Label43: TLabel
                Left = 8
                Top = 20
                Width = 66
                Height = 13
                Hint = 'Porta da impressora'
                Caption = 'Porta Impres.'
              end
              object Label72: TLabel
                Left = 56
                Top = 64
                Width = 38
                Height = 13
                Caption = 'Colunas'
                FocusControl = DBEdit44
              end
              object Label74: TLabel
                Left = 8
                Top = 64
                Width = 30
                Height = 13
                Caption = 'Linhas'
                FocusControl = DBEdit46
              end
              object Label75: TLabel
                Left = 104
                Top = 64
                Width = 34
                Height = 13
                Caption = 'Espa'#231'o'
                FocusControl = DBEdit44
              end
              object Label73: TLabel
                Left = 168
                Top = 20
                Width = 73
                Height = 13
                Hint = 'Modelo da impressora'
                Caption = 'P'#225'g. de C'#243'digo'
              end
              object DBComboBox3: TDBComboBox
                Left = 88
                Top = 36
                Width = 73
                Height = 22
                Style = csOwnerDrawFixed
                DataField = 'cModeloImpSAT'
                DataSource = dsAmbiente
                ItemHeight = 16
                Items.Strings = (
                  'Epson'
                  'Bematech'
                  'Daruma'
                  'Elgin'
                  'Diebold')
                TabOrder = 1
              end
              object DBCheckBox11: TDBCheckBox
                Left = 160
                Top = 82
                Width = 97
                Height = 17
                Caption = 'Imp. item 1 linha'
                DataField = 'cFlgImpUmaLinha'
                DataSource = dsAmbiente
                TabOrder = 6
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit44: TDBEdit
                Left = 56
                Top = 80
                Width = 40
                Height = 21
                DataField = 'iColunaImpSAT'
                DataSource = dsAmbiente
                TabOrder = 4
              end
              object DBEdit45: TDBEdit
                Left = 104
                Top = 80
                Width = 40
                Height = 21
                DataField = 'iEspacoImpSAT'
                DataSource = dsAmbiente
                TabOrder = 5
              end
              object DBEdit46: TDBEdit
                Left = 8
                Top = 80
                Width = 40
                Height = 21
                DataField = 'iLinhaImpSAT'
                DataSource = dsAmbiente
                TabOrder = 3
              end
              object DBComboBox5: TDBComboBox
                Left = 168
                Top = 36
                Width = 97
                Height = 22
                Style = csOwnerDrawFixed
                DataField = 'cPageCodImpSAT'
                DataSource = dsAmbiente
                ItemHeight = 16
                Items.Strings = (
                  'pcNone'
                  'pc437'
                  'pc850'
                  'pc852'
                  'pc860'
                  'pcUTF8'
                  'pc1252')
                TabOrder = 2
              end
              object DBEdit10: TDBEdit
                Left = 8
                Top = 36
                Width = 73
                Height = 21
                DataField = 'cPortaImpSAT'
                DataSource = dsAmbiente
                TabOrder = 0
              end
            end
            object GroupBox7: TGroupBox
              Left = 288
              Top = 200
              Width = 257
              Height = 125
              Caption = '     '
              TabOrder = 11
              object Label46: TLabel
                Left = 8
                Top = 16
                Width = 37
                Height = 13
                Caption = 'Largura'
                FocusControl = DBEdit35
              end
              object Label47: TLabel
                Left = 56
                Top = 16
                Width = 24
                Height = 13
                Caption = 'Topo'
                FocusControl = DBEdit36
              end
              object Label48: TLabel
                Left = 104
                Top = 16
                Width = 30
                Height = 13
                Caption = 'Fundo'
                FocusControl = DBEdit37
              end
              object Label49: TLabel
                Left = 200
                Top = 16
                Width = 31
                Height = 13
                Caption = 'Direita'
                FocusControl = DBEdit38
              end
              object Label50: TLabel
                Left = 152
                Top = 16
                Width = 45
                Height = 13
                Caption = 'Esquerda'
                FocusControl = DBEdit39
              end
              object Label51: TLabel
                Left = 8
                Top = 80
                Width = 91
                Height = 13
                Caption = 'Impressora Padr'#227'o'
                FocusControl = DBEdit33
              end
              object DBEdit35: TDBEdit
                Left = 8
                Top = 32
                Width = 40
                Height = 21
                DataField = 'iLarguraImpSAT'
                DataSource = dsAmbiente
                TabOrder = 0
              end
              object DBEdit36: TDBEdit
                Left = 56
                Top = 32
                Width = 40
                Height = 21
                DataField = 'iTopoImpSAT'
                DataSource = dsAmbiente
                TabOrder = 1
              end
              object DBEdit37: TDBEdit
                Left = 104
                Top = 32
                Width = 40
                Height = 21
                DataField = 'iFundoImpSAT'
                DataSource = dsAmbiente
                TabOrder = 2
              end
              object DBEdit38: TDBEdit
                Left = 200
                Top = 32
                Width = 40
                Height = 21
                DataField = 'iDireitoImpSAT'
                DataSource = dsAmbiente
                TabOrder = 4
              end
              object DBEdit39: TDBEdit
                Left = 152
                Top = 32
                Width = 40
                Height = 21
                DataField = 'iEsquerdoImpSAT'
                DataSource = dsAmbiente
                TabOrder = 3
              end
              object dbCheckPreviewImpSAT: TDBCheckBox
                Left = 8
                Top = 58
                Width = 97
                Height = 17
                Caption = 'Preview de imp.'
                DataField = 'cFlgPreviewImpSAT'
                DataSource = dsAmbiente
                TabOrder = 5
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object btnImpSATDefault: TcxButton
                Left = 216
                Top = 94
                Width = 25
                Height = 25
                TabOrder = 7
                OnClick = btnImpSATDefaultClick
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
                  FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
                  5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
                  FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
                  B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
                  5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
                  FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
                  B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
                  57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
                  C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
                  4742410101010202020202020101010101010101010101010101010101010101
                  01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
                  5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
                  B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
                  34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
                  B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
                  6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
                  535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
                  E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
                  5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
                  EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
                  5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
                  96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
                LookAndFeel.NativeStyle = True
              end
              object DBEdit33: TDBEdit
                Tag = 1
                Left = 8
                Top = 96
                Width = 205
                Height = 21
                DataField = 'cNmImpSATDefault'
                DataSource = dsAmbiente
                TabOrder = 6
              end
            end
            object checkEscPOS: TCheckBox
              Left = 24
              Top = 197
              Width = 121
              Height = 17
              Caption = ' Extrato em EscPOS '
              Checked = True
              State = cbChecked
              TabOrder = 8
              OnClick = checkEscPOSClick
            end
            object edtConfCodigoAtivacaoSAT: TEdit
              Left = 287
              Top = 72
              Width = 258
              Height = 21
              MaxLength = 32
              PasswordChar = '*'
              TabOrder = 5
            end
            object DBEdit31: TDBEdit
              Left = 119
              Top = 24
              Width = 65
              Height = 21
              CharCase = ecUpperCase
              DataField = 'nCdUFEmissaoCFe'
              DataSource = dsAmbiente
              MaxLength = 2
              TabOrder = 1
            end
            object DBComboBox1: TDBComboBox
              Left = 247
              Top = 25
              Width = 146
              Height = 19
              Style = csOwnerDrawFixed
              DataField = 'cModeloSAT'
              DataSource = dsAmbiente
              ItemHeight = 13
              Items.Strings = (
                'satNenhum'
                'satDinamico_cdecl'
                'satDinamico_stdcall')
              TabOrder = 2
            end
            object checkFortes: TCheckBox
              Left = 304
              Top = 197
              Width = 153
              Height = 17
              Caption = ' Extrato em Fortes Report '
              TabOrder = 10
              OnClick = checkFortesClick
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 8
              Top = 336
              Width = 185
              Height = 37
              Caption = ' Tipo Ambiente CF-e-SAT '
              Columns = 2
              DataField = 'cFlgTipoAmbienteCFe'
              DataSource = dsAmbiente
              Items.Strings = (
                'Produ'#231#227'o'
                'Homologa'#231#227'o')
              TabOrder = 12
              Values.Strings = (
                '0'
                '1')
            end
            object DBEdit34: TDBEdit
              Left = 424
              Top = 24
              Width = 121
              Height = 21
              Hint = 'Nome arquivo *.dll disponibilizado pelo equipamento'
              DataField = 'cArquivoDllSAT'
              DataSource = dsAmbiente
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
            end
          end
          object tabSATRede: TcxTabSheet
            Caption = 'Rede SAT'
            ImageIndex = 2
            object rgRedeTipoInter: TRadioGroup
              Left = 9
              Top = 10
              Width = 193
              Height = 41
              Caption = ' Tipo de Conex'#227'o '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'ETHE'
                'WIFI')
              TabOrder = 0
              OnClick = rgRedeTipoInterClick
            end
            object rgRedeTipoLan: TRadioGroup
              Left = 209
              Top = 10
              Width = 331
              Height = 41
              Caption = ' Tipo de Rede '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'DHCP'
                'PPPoE'
                'IPFIX')
              TabOrder = 1
              OnClick = rgRedeTipoLanClick
            end
            object gbWiFi: TGroupBox
              Left = 9
              Top = 52
              Width = 194
              Height = 113
              Caption = ' Wi Fi '
              TabOrder = 2
              object Label53: TLabel
                Left = 24
                Top = 24
                Width = 23
                Height = 13
                Alignment = taRightJustify
                Caption = 'SSID'
              end
              object Label54: TLabel
                Left = 15
                Top = 56
                Width = 32
                Height = 13
                Alignment = taRightJustify
                Caption = 'Segur.'
              end
              object Label52: TLabel
                Left = 17
                Top = 88
                Width = 30
                Height = 13
                Alignment = taRightJustify
                Caption = 'Senha'
              end
              object cmbWifiSeg: TComboBox
                Left = 55
                Top = 48
                Width = 126
                Height = 19
                Style = csOwnerDrawFixed
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 0
                Text = 'NONE'
                Items.Strings = (
                  'NONE'
                  'WEP'
                  'WPA-PERSONAL'
                  'WPA-ENTERPRISE')
              end
              object edtWifiSenha: TEdit
                Left = 55
                Top = 80
                Width = 126
                Height = 21
                TabOrder = 1
              end
              object edtWifiSSID: TEdit
                Left = 56
                Top = 16
                Width = 126
                Height = 21
                TabOrder = 2
              end
            end
            object gbIPFix: TGroupBox
              Left = 209
              Top = 52
              Width = 331
              Height = 113
              Caption = ' IPFIX '
              TabOrder = 3
              object Label57: TLabel
                Left = 10
                Top = 24
                Width = 45
                Height = 13
                Alignment = taRightJustify
                Caption = 'Ender. IP'
              end
              object Label58: TLabel
                Left = 15
                Top = 56
                Width = 40
                Height = 13
                Alignment = taRightJustify
                Caption = 'M'#225'scara'
              end
              object Label59: TLabel
                Left = 12
                Top = 88
                Width = 43
                Height = 13
                Alignment = taRightJustify
                Caption = 'Gateway'
              end
              object Label60: TLabel
                Left = 199
                Top = 56
                Width = 29
                Height = 13
                Alignment = taRightJustify
                Caption = 'DNS 2'
              end
              object Label61: TLabel
                Left = 199
                Top = 24
                Width = 29
                Height = 13
                Alignment = taRightJustify
                Caption = 'DNS 1'
              end
              object edtIPFixIP: TEdit
                Left = 64
                Top = 16
                Width = 120
                Height = 21
                TabOrder = 0
              end
              object edtIPFixMasc: TEdit
                Left = 64
                Top = 48
                Width = 120
                Height = 21
                TabOrder = 1
              end
              object edtIPFixGateway: TEdit
                Left = 64
                Top = 80
                Width = 120
                Height = 21
                TabOrder = 2
              end
              object edtIPFixDNS2: TEdit
                Left = 237
                Top = 48
                Width = 84
                Height = 21
                TabOrder = 3
              end
              object edtIPFixDNS1: TEdit
                Left = 237
                Top = 16
                Width = 84
                Height = 21
                TabOrder = 4
              end
            end
            object gbProxy: TGroupBox
              Left = 9
              Top = 165
              Width = 194
              Height = 180
              Caption = ' Proxy '
              TabOrder = 4
              object Label62: TLabel
                Left = 25
                Top = 152
                Width = 30
                Height = 13
                Alignment = taRightJustify
                Caption = 'Senha'
              end
              object Label63: TLabel
                Left = 29
                Top = 24
                Width = 26
                Height = 13
                Alignment = taRightJustify
                Caption = 'Modo'
              end
              object Label64: TLabel
                Left = 10
                Top = 56
                Width = 45
                Height = 13
                Alignment = taRightJustify
                Caption = 'Ender. IP'
              end
              object Label65: TLabel
                Left = 19
                Top = 120
                Width = 36
                Height = 13
                Alignment = taRightJustify
                Caption = 'Usu'#225'rio'
              end
              object Label66: TLabel
                Left = 29
                Top = 88
                Width = 26
                Height = 13
                Alignment = taRightJustify
                Caption = 'Porta'
              end
              object cmbProxy: TComboBox
                Left = 64
                Top = 16
                Width = 119
                Height = 19
                Style = csOwnerDrawFixed
                ItemHeight = 13
                ItemIndex = 0
                TabOrder = 0
                Text = 'N'#227'o utiliza proxy'
                OnChange = cmbProxyChange
                Items.Strings = (
                  'N'#227'o utiliza proxy'
                  'Proxy com configura'#231#227'o'
                  'Proxy transparente')
              end
              object edtProxyIP: TEdit
                Left = 64
                Top = 48
                Width = 119
                Height = 21
                TabOrder = 1
              end
              object edtProxyUsuario: TEdit
                Left = 64
                Top = 112
                Width = 119
                Height = 21
                TabOrder = 2
              end
              object edtProxySenha: TEdit
                Left = 64
                Top = 144
                Width = 119
                Height = 21
                TabOrder = 3
              end
              object mskProxyPorta: TMaskEdit
                Left = 64
                Top = 80
                Width = 119
                Height = 21
                EditMask = '#######;0; '
                MaxLength = 7
                TabOrder = 4
                Text = '0'
              end
            end
            object gbPPPoE: TGroupBox
              Left = 209
              Top = 165
              Width = 331
              Height = 84
              Caption = ' PPPoE '
              TabOrder = 5
              object Label55: TLabel
                Left = 11
                Top = 24
                Width = 36
                Height = 13
                Alignment = taRightJustify
                Caption = 'Usu'#225'rio'
              end
              object Label56: TLabel
                Left = 17
                Top = 56
                Width = 30
                Height = 13
                Alignment = taRightJustify
                Caption = 'Senha'
              end
              object edtPPPoEUsuario: TEdit
                Left = 56
                Top = 16
                Width = 265
                Height = 21
                TabOrder = 0
              end
              object edtPPPoESenha: TEdit
                Left = 55
                Top = 48
                Width = 266
                Height = 21
                TabOrder = 1
              end
            end
            object btGerarXMLRede: TcxButton
              Left = 248
              Top = 259
              Width = 140
              Height = 37
              Caption = 'Gerar XML de Rede'
              TabOrder = 6
              OnClick = btGerarXMLRedeClick
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                E2E2E2CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
                C9C9C9C9CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFC
                FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF9F9F9CCCCCCFFFFFFFEFEFE
                C9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
                FCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFC
                FCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCFCFCC9C9C9FFFFFFFEFEFE
                BEC1CA5773EA4662EAFCFCFC495CED5972ECEDF0F9FAFAFAFAFAFAFAFAFAFAFA
                FAFAFAFAFCFCFCC9C9C9FFFFFFEDF0FC4E67E15565EE4E6AEBFCFCFC5264EF59
                64EF5972ECEAEEF9FAFAFAFAFAFAF8F8F8F8F8F8FCFCFCC9C9C9AEB9F74A55F0
                5B65F05065EDE2E8F9FCFCFCE3E7FA5362EF5B65F04755EDA8B9F2F9F9F9F9F9
                F9F8F8F8FCFCFCC9C9C95054F26B70F34950EECDD5F7FCFCFCFCFCFCFCFCFCCE
                D4F84A51EF676EF04A59EFF3F5F8F6F6F6F6F6F6FCFCFCC9C9C9B0B7F85053F2
                6265F15562F2E3E7FAFCFCFCE4E6FA575CF26265F14D53F1A9B5F2F6F6F6F3F3
                F3F2F2F2FCFCFCC9C9C9FFFFFFEEEFFD5860E86366F2565FF2FCFCFC5A5BF468
                68F3636BF3E8EAF7F5F5F5F2F2F2EFEFEFEDEDEDFCFCFCC9C9C9FFFFFFFEFEFE
                BFC0CB646AF35253F2FCFCFC5A57F46568F4EBECF7F5F5F5F1F1F1ECECECEAEA
                EAE6E6E6FCFCFCC9C9C9FFFFFFFEFEFEC9C9C9EFF0FBF9F9F9F9F9F9F9F9F9F7
                F7F7F6F6F6F2F2F2EBEBEBFCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFE
                C9C9C9FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6
                F6F4F4F4C5C5C5DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5
                F5F5F1F1F1EFEFEFE9E9E9FCFCFCE7E7E7C3C3C3DFDFDFFDFDFDFFFFFFFFFFFF
                CCCCCCF8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2
                C2DFDFDFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9
                C9C9C9C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFF}
              LookAndFeel.NativeStyle = True
            end
            object btLerXMLRede: TcxButton
              Left = 400
              Top = 259
              Width = 140
              Height = 37
              Caption = 'Ler XML de Rede'
              TabOrder = 7
              OnClick = btLerXMLRedeClick
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                E2E2E2CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
                C9C9C9C9CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFC
                FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF9F9F9CCCCCCFFFFFFFEFEFE
                C9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
                FCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFC
                FCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCFCFCC9C9C9FFFFFFFEFEFE
                C9C2BFE19565E09055FCFCFCE39A59E39967F8F1EEFAFAFAFAFAFAFAFAFAFAFA
                FAFAFAFAFCFCFCC9C9C9FFFFFFFBF3EED7905CE5A365E1945DFCFCFCE5A061E7
                A868E39B67F8F0EBFAFAFAFAFAFAF8F8F8F8F8F8FCFCFCC9C9C9F2CFB4E6A25A
                E8AA6AE49C5FF8EBE5FCFCFCF8EDE5E5A262E8AA6AE39C57EDC5AFF9F9F9F9F9
                F9F8F8F8FCFCFCC9C9C9E8AC60ECB879E5A55AF4DFD1FCFCFCFCFCFCFCFCFCF5
                E1D2E6A65BE9B275E49E5AF8F5F4F6F6F6F6F6F6FCFCFCC9C9C9F4D5B7EAAE60
                EAB571E8A765F8EDE5FCFCFCF9EFE6E9AE66EAB571E8A85DEDC9B0F6F6F6F3F3
                F3F2F2F2FCFCFCC9C9C9FFFFFFFCF5EFDFA565EBB672E8A965FCFCFCEBB56AEC
                BB76EAB070F6EFEAF5F5F5F2F2F2EFEFEFEDEDEDFCFCFCC9C9C9FFFFFFFEFEFE
                CAC5C0EAB371E8AF62FCFCFCECB767EBB772F7F1ECF5F5F5F1F1F1ECECECEAEA
                EAE6E6E6FCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FAF5F0F9F9F9F9F9F9F9F9F9F7
                F7F7F6F6F6F2F2F2EBEBEBFCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFE
                C9C9C9FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6
                F6F4F4F4C5C5C5DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5
                F5F5F1F1F1EFEFEFE9E9E9FCFCFCE7E7E7C3C3C3DFDFDFFDFDFDFFFFFFFFFFFF
                CCCCCCF8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2
                C2DFDFDFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9
                C9C9C9C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFF}
              LookAndFeel.NativeStyle = True
            end
            object btResetarConfigRedeSAT: TcxButton
              Left = 400
              Top = 307
              Width = 140
              Height = 37
              Caption = 'Resetar Config. Rede'
              TabOrder = 8
              OnClick = btResetarConfigRedeSATClick
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC4C6F3656BE05259DC53
                5BDC5058DA4952DA5960DCB7BAF0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFC6C8F3747CE78997FA8495FF7F90FC7D8EFA7D8DF77D8BF25159DD9B9F
                EAFF00FFFF00FFFF00FFFF00FFFF00FFCACCF47E86E796A5FA5A74FF3250FF30
                4FFF2C49FE2542FA4860F98694F46970E2BDC0F1FF00FFFF00FFFF00FFD0D2F5
                878EE99FADFB6781FF405EFF405EFF3C59FF3755FF3350FF2846FD4A65FD8996
                F6656EE1C1C3F2FF00FFFF00FF888EE8A2B2FC738FFF4F70FF4F6EFF4B69FF46
                64FF415EFF3C5AFF3755FF2C4BFF4E67FF8493FA5F66DEFF00FFFF00FF848AE6
                A6BBFF5F7FFF5F7EFF5A79FF5573FF506EFF4B69FF4664FF415EFF3B59FF314F
                FF8799FF565EDDFF00FFFF00FF9094E8ABBEFF6D8DFF6989FF6583FF5F7EFF5A
                79FF5574FF506FFF4B69FF4663FF3F5CFF8A9BFF6269DFFF00FFFF00FF959AE9
                B1C4FF7698FF7393FF6E8EFF6989FF6583FF5F7EFF5A79FF5573FF4F6EFF4867
                FF90A1FF686FE1FF00FFFF00FF989DEAB8CDFF7DA0FF7C9DFF7899FF7393FF6E
                8EFF6989FF6583FF607EFF5978FF4F70FF98AAFF6B72E2FF00FFFF00FFA7ABEE
                BCCDFC9CBBFF81A5FF81A2FF7C9EFF7899FF7493FF6F8EFF6989FF6080FF7893
                FF9EADFB7F85E5FF00FFFF00FFDFE1F8AFB5F1C1D1FCA0BFFF86AAFF85A7FF81
                A2FF7C9DFF7898FF6F90FF85A1FFACBAFB9097EAD2D3F6FF00FFFF00FFFF00FF
                E0E1F8B1B7F0C4D4FCA3C2FF8BAFFF89ABFF84A7FF7EA0FF91AEFFB4C3FB9CA2
                EDD5D6F6FF00FFFF00FFFF00FFFF00FFFF00FFE0E1F8B2B9F2C2D3FCC1D6FFBD
                D1FFBBCFFFB9CEFFB7C8FC989FEDC3C5F2FF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFE1E2F9AFB3EFA6AAEDA5AAEDA4A8ED9EA4EBA5A9EDD8D9F7FF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
              LookAndFeel.NativeStyle = True
            end
          end
          object tabSATCompartilhado: TcxTabSheet
            Caption = 'SAT Compartilhado'
            ImageIndex = 2
            object Label71: TLabel
              Left = 8
              Top = 8
              Width = 55
              Height = 13
              Caption = 'N'#250'm. Caixa'
              FocusControl = DBEdit43
            end
            object gbDadosSAT: TGroupBox
              Left = 8
              Top = 56
              Width = 537
              Height = 81
              Caption = ' Dados de Acesso ao Servidor SAT '
              TabOrder = 1
              object Label67: TLabel
                Left = 16
                Top = 24
                Width = 40
                Height = 13
                Caption = 'Servidor'
                FocusControl = DBEdit2
              end
              object DBEdit40: TDBEdit
                Left = 16
                Top = 40
                Width = 65
                Height = 21
                DataField = 'nCdServidorSAT'
                DataSource = dsAmbiente
                TabOrder = 0
                OnExit = DBEdit40Exit
              end
              object DBEdit41: TDBEdit
                Left = 84
                Top = 40
                Width = 335
                Height = 21
                CharCase = ecUpperCase
                DataField = 'cNmServidorSAT'
                DataSource = dsServidorSAT
                TabOrder = 1
              end
              object DBEdit42: TDBEdit
                Left = 422
                Top = 40
                Width = 100
                Height = 21
                CharCase = ecUpperCase
                DataField = 'cNmStatus'
                DataSource = dsServidorSAT
                TabOrder = 2
              end
            end
            object rgServidorSat: TDBRadioGroup
              Left = 96
              Top = 8
              Width = 201
              Height = 41
              Caption = ' Tipo Terminal '
              Columns = 2
              DataField = 'cFlgServidorSAT'
              DataSource = dsAmbiente
              Items.Strings = (
                'Servidor'
                'Cliente')
              TabOrder = 0
              Values.Strings = (
                '1'
                '0')
            end
            object DBEdit43: TDBEdit
              Left = 8
              Top = 24
              Width = 81
              Height = 21
              Hint = 
                'N'#250'mero de 1 a 999 referente ao caixa em que o SAT est'#225' conectado' +
                '.'
              DataField = 'nCdCodPDVSAT'
              DataSource = dsAmbiente
              MaxLength = 3
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
            end
            object rgSatCompartilhado: TDBRadioGroup
              Left = 304
              Top = 8
              Width = 241
              Height = 41
              Caption = ' Modo de Utiliza'#231#227'o '
              Columns = 2
              DataField = 'cFlgSATCompartilhado'
              DataSource = dsAmbiente
              Items.Strings = (
                'Local'
                'Compartilhado')
              TabOrder = 3
              Values.Strings = (
                '0'
                '1')
            end
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 567
    Width = 592
    Height = 54
    Align = alClient
    Color = clWhite
    TabOrder = 1
    object btGravar: TcxButton
      Left = 4
      Top = 4
      Width = 81
      Height = 33
      Caption = 'Gravar'
      TabOrder = 0
      OnClick = btGravarClick
      Glyph.Data = {
        E6010000424DE60100000000000036000000280000000C0000000C0000000100
        180000000000B001000000000000000000000000000000000000CCCCCCAAA39F
        6D6C6B656464575D5E6564646564646564646D6C6B656464898A89C6C6C6B6B6
        B63D42420405065157577172726D6C6B575D5E515757191D23191D2331333289
        8A89484E4E0599CF092369DAD2C9FCFEFEFCFEFEEEEEEED5D5D5484E4E0599CF
        0923695157573D424231CDFD09236991846EB3AD9EAAA39F93929291846E3133
        320599CF0923695157573D424231CDFD10708209236909236909236909236909
        23691070820599CF0923695157573D424231CDFD0599CF107082092369092369
        092369092369479EC00599CF0923695157573D424231CDFD092369B7A68ACCC5
        C0C2BEB9C2BEB9DBC8C35157570599CF0923695157573D424231CDFD092369CB
        B9B1E6E6E6DEDEDEDEDEDEEEEEEA5157570599CF0923695157573D424231CDFD
        092369C2B9AEDEDEDEDADDD7DADDD7E6E6E65157570599CF0923695157573D42
        4231CDFD092369B8B1AADEDEDED5D5D5D5D5D5E6E6E6484E4E0599CF09236951
        57573D424231CDFD092369DAD2C9FCFEFEF9FAFAF9FAFAFCFEFE575D5E0599CF
        107082515757777B7B3D4242191D23484E4E575D5E515757515757575D5E191D
        23191D23575D5EA4A8A8}
      LookAndFeel.NativeStyle = True
    end
    object btFechar: TcxButton
      Left = 87
      Top = 4
      Width = 81
      Height = 33
      Cancel = True
      Caption = 'Fechar'
      TabOrder = 1
      OnClick = btFecharClick
      Glyph.Data = {
        06030000424D06030000000000003600000028000000100000000F0000000100
        180000000000D002000000000000000000000000000000000000C6D6DEC6D6DE
        C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE0000000000000000000000000000000000
        00000000000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
        0000084A21084A21084A21084A21084A21084A21084A21000000000000000000
        00000000000000000000000000000000000000000000000018A54A18A54A18A5
        4A18A54A084A21000000000000D6A58CD6A58CD6A58CD6A58CD6A58CD6A58CD6
        A58C00000052B5F700000018A54A18A54A18A54A084A21000000000000EFFFFF
        FFEFDEFFEFDE00000000000000000000000000000000FFFF52B5F700000018A5
        4A18A54A084A21000000000000EFFFFFFFEFDEFFEFDE00000000FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF52B5F700000018A54A084A21000000000000EFFFFF
        FFEFDEFFEFDE000000EFFFFFEFFFFFEFFFFFEFFFFF00FFFF00FFFFEFFFFF0000
        0018A54A084A21000000000000EFFFFFFFEFDEFFEFDE00000000000000000000
        000000000000FFFFEFFFFF00000018A54A18A54A084A21000000000000EFFFFF
        FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C000000EFFFFF00000018A54A18A5
        4A18A54A084A21000000000000EFFFFFFFEFDEFFEFDEFFEFDEA56B5AA56B5AD6
        A58C00000000000018A54A18A54A18A54A18A54A084A21000000000000EFFFFF
        FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C00000018A54A18A54A18A54A18A5
        4A316363316363000000000000EFFFFFFFEFDEFFEFDEFFEFDEA56B5AA56B5AD6
        A58C0000009CD6B59CD6B59CD6B59CD6B5316363000000C6D6DE000000EFFFFF
        FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C0000000000000000000000000000
        00000000C6D6DEC6D6DE000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
        FFFF000000C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000000000
        000000000000000000000000000000000000000000C6D6DEC6D6DEC6D6DEC6D6
        DEC6D6DEC6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
  end
  object TcxTabSheet
  end
  object TcxTabSheet
  end
  object qryAmbiente: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryAmbienteBeforePost
    AfterScroll = qryAmbienteAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ConfigAmbiente'
      'WHERE cNmComputador = :nPK')
    Left = 368
    Top = 8
    object qryAmbientecNmComputador: TStringField
      FieldName = 'cNmComputador'
      Size = 50
    end
    object qryAmbientecPortaMatricial: TStringField
      FieldName = 'cPortaMatricial'
      Size = 100
    end
    object qryAmbientecFlgUsaECF: TIntegerField
      FieldName = 'cFlgUsaECF'
    end
    object qryAmbientecModeloECF: TStringField
      FieldName = 'cModeloECF'
    end
    object qryAmbientecPortaECF: TStringField
      FieldName = 'cPortaECF'
      Size = 10
    end
    object qryAmbientenCdTipoImpressoraPDV: TIntegerField
      FieldName = 'nCdTipoImpressoraPDV'
    end
    object qryAmbienteiViasECF: TIntegerField
      FieldName = 'iViasECF'
    end
    object qryAmbientecPortaEtiqueta: TStringField
      FieldName = 'cPortaEtiqueta'
      Size = 10
    end
    object qryAmbientecFlgEmiteNFe: TIntegerField
      FieldName = 'cFlgEmiteNFe'
    end
    object qryAmbientecNrSerieCertificadoNFe: TStringField
      FieldName = 'cNrSerieCertificadoNFe'
      Size = 50
    end
    object qryAmbientedDtUltAcesso: TDateTimeField
      FieldName = 'dDtUltAcesso'
    end
    object qryAmbientecNmUsuarioUltAcesso: TStringField
      FieldName = 'cNmUsuarioUltAcesso'
      Size = 50
    end
    object qryAmbientecUFEmissaoNfe: TStringField
      FieldName = 'cUFEmissaoNfe'
      Size = 2
    end
    object qryAmbientecPathLogoNFe: TStringField
      FieldName = 'cPathLogoNFe'
      Size = 100
    end
    object qryAmbientenCdTipoAmbienteNFe: TIntegerField
      FieldName = 'nCdTipoAmbienteNFe'
    end
    object qryAmbientenCdUFEmissaoNFe: TIntegerField
      FieldName = 'nCdUFEmissaoNFe'
    end
    object qryAmbientenCdMunicipioEmissaoNFe: TIntegerField
      FieldName = 'nCdMunicipioEmissaoNFe'
    end
    object qryAmbientecPathArqNFeProd: TStringField
      FieldName = 'cPathArqNFeProd'
      Size = 100
    end
    object qryAmbientecPathArqNFeHom: TStringField
      FieldName = 'cPathArqNFeHom'
      Size = 100
    end
    object qryAmbientecServidorSMTP: TStringField
      FieldName = 'cServidorSMTP'
      Size = 50
    end
    object qryAmbientecEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryAmbientecSenhaEmail: TStringField
      FieldName = 'cSenhaEmail'
      Size = 50
    end
    object qryAmbientenPortaSMTP: TIntegerField
      FieldName = 'nPortaSMTP'
    end
    object qryAmbientecFlgUsaSSL: TIntegerField
      FieldName = 'cFlgUsaSSL'
    end
    object qryAmbientecTefDialArqLog: TStringField
      FieldName = 'cTefDialArqLog'
      Size = 100
    end
    object qryAmbientecTefDialArqReq: TStringField
      FieldName = 'cTefDialArqReq'
      Size = 100
    end
    object qryAmbientecTefDialArqResp: TStringField
      FieldName = 'cTefDialArqResp'
      Size = 100
    end
    object qryAmbientecTefDialArqSTS: TStringField
      FieldName = 'cTefDialArqSTS'
      Size = 100
    end
    object qryAmbientecTefDialArqTemp: TStringField
      FieldName = 'cTefDialArqTemp'
      Size = 100
    end
    object qryAmbientecTefDialGPExeName: TStringField
      FieldName = 'cTefDialGPExeName'
      Size = 100
    end
    object qryAmbientecTefDialPatchBackup: TStringField
      FieldName = 'cTefDialPatchBackup'
      Size = 100
    end
    object qryAmbientecTefDialAutoAtivarGP: TIntegerField
      FieldName = 'cTefDialAutoAtivarGP'
    end
    object qryAmbienteiTefDialEsperaSTS: TIntegerField
      FieldName = 'iTefDialEsperaSTS'
    end
    object qryAmbienteiTefDialNumVias: TIntegerField
      FieldName = 'iTefDialNumVias'
    end
    object qryAmbientecFlgTEFAtivo: TIntegerField
      FieldName = 'cFlgTEFAtivo'
    end
    object qryAmbientenCdTipoImpressoraPDVCaixa: TIntegerField
      FieldName = 'nCdTipoImpressoraPDVCaixa'
    end
    object qryAmbientecNmImpCompartilhadaPDV: TStringField
      FieldName = 'cNmImpCompartilhadaPDV'
    end
    object qryAmbientecNmImpCompartilhadaEtiqueta: TStringField
      FieldName = 'cNmImpCompartilhadaEtiqueta'
    end
    object qryAmbientecFlgUsaMapeamentoAutPDV: TIntegerField
      FieldName = 'cFlgUsaMapeamentoAutPDV'
    end
    object qryAmbientecFlgUsaMapeamentoAutEtiqueta: TIntegerField
      FieldName = 'cFlgUsaMapeamentoAutEtiqueta'
    end
    object qryAmbientecFlgUsaGaveta: TIntegerField
      FieldName = 'cFlgUsaGaveta'
    end
    object qryAmbientenCdTipoGavetaPDV: TIntegerField
      FieldName = 'nCdTipoGavetaPDV'
    end
    object qryAmbientecNmPortaGaveta: TStringField
      FieldName = 'cNmPortaGaveta'
    end
    object qryAmbientecPathLogoPDV: TStringField
      FieldName = 'cPathLogoPDV'
      Size = 100
    end
    object qryAmbientecPathLogoCaixa: TStringField
      FieldName = 'cPathLogoCaixa'
      Size = 100
    end
    object qryAmbienteiBaudECF: TIntegerField
      FieldName = 'iBaudECF'
    end
    object qryAmbientecFlgCFeAtivo: TIntegerField
      FieldName = 'cFlgCFeAtivo'
    end
    object qryAmbientecModoEnvioCFe: TStringField
      FieldName = 'cModoEnvioCFe'
      FixedChar = True
      Size = 1
    end
    object qryAmbientecCdAtivacaoSAT: TStringField
      FieldName = 'cCdAtivacaoSAT'
      Size = 50
    end
    object qryAmbientecAssinaturaACSAT: TStringField
      FieldName = 'cAssinaturaACSAT'
      Size = 500
    end
    object qryAmbientenCdUFEmissaoCFe: TIntegerField
      FieldName = 'nCdUFEmissaoCFe'
    end
    object qryAmbientecCNPJACSAT: TStringField
      FieldName = 'cCNPJACSAT'
      Size = 14
    end
    object qryAmbientecFlgTipoAmbienteCFe: TIntegerField
      FieldName = 'cFlgTipoAmbienteCFe'
    end
    object qryAmbientecModeloSAT: TStringField
      FieldName = 'cModeloSAT'
      Size = 50
    end
    object qryAmbientenCdCodPDVSAT: TIntegerField
      FieldName = 'nCdCodPDVSAT'
    end
    object qryAmbientecFlgCodSeguranca: TIntegerField
      FieldName = 'cFlgCodSeguranca'
    end
    object qryAmbientecFlgSATBloqueado: TIntegerField
      FieldName = 'cFlgSATBloqueado'
    end
    object qryAmbientecFlgTipoImpSAT: TIntegerField
      FieldName = 'cFlgTipoImpSAT'
    end
    object qryAmbienteiLarguraImpSAT: TIntegerField
      FieldName = 'iLarguraImpSAT'
    end
    object qryAmbienteiTopoImpSAT: TIntegerField
      FieldName = 'iTopoImpSAT'
    end
    object qryAmbienteiFundoImpSAT: TIntegerField
      FieldName = 'iFundoImpSAT'
    end
    object qryAmbienteiEsquerdoImpSAT: TIntegerField
      FieldName = 'iEsquerdoImpSAT'
    end
    object qryAmbienteiDireitoImpSAT: TIntegerField
      FieldName = 'iDireitoImpSAT'
    end
    object qryAmbientecFlgPreviewImpSAT: TIntegerField
      FieldName = 'cFlgPreviewImpSAT'
    end
    object qryAmbientecPortaImpSAT: TStringField
      FieldName = 'cPortaImpSAT'
      Size = 50
    end
    object qryAmbientecNmImpSATDefault: TStringField
      FieldName = 'cNmImpSATDefault'
      Size = 50
    end
    object qryAmbientecFlgSATCompartilhado: TIntegerField
      FieldName = 'cFlgSATCompartilhado'
    end
    object qryAmbientecArquivoXMLRede: TMemoField
      FieldName = 'cArquivoXMLRede'
      BlobType = ftMemo
    end
    object qryAmbientenCdServidorSAT: TIntegerField
      FieldName = 'nCdServidorSAT'
    end
    object qryAmbientecPathLogoCFe: TStringField
      FieldName = 'cPathLogoCFe'
      Size = 150
    end
    object qryAmbientecModeloImpSAT: TStringField
      FieldName = 'cModeloImpSAT'
      Size = 50
    end
    object qryAmbientecFlgRemoto: TIntegerField
      FieldName = 'cFlgRemoto'
    end
    object qryAmbientecIPServidor: TStringField
      FieldName = 'cIPServidor'
      Size = 15
    end
    object qryAmbienteiPortaServidor: TIntegerField
      FieldName = 'iPortaServidor'
    end
    object qryAmbientecPortaPinPad: TStringField
      FieldName = 'cPortaPinPad'
      Size = 6
    end
    object qryAmbientecIPSiTef: TStringField
      FieldName = 'cIPSiTef'
      Size = 15
    end
    object qryAmbientecIDLojaTef: TStringField
      FieldName = 'cIDLojaTef'
    end
    object qryAmbientecIDTerminalTef: TStringField
      FieldName = 'cIDTerminalTef'
    end
    object qryAmbientecMsgPinPad: TStringField
      FieldName = 'cMsgPinPad'
      Size = 30
    end
    object qryAmbientecNumSerieSAT: TStringField
      FieldName = 'cNumSerieSAT'
      Size = 50
    end
    object qryAmbientecFlgWidePdvCaixa: TIntegerField
      FieldName = 'cFlgWidePdvCaixa'
    end
    object qryAmbientecArquivoDllSAT: TStringField
      FieldName = 'cArquivoDllSAT'
      Size = 25
    end
    object qryAmbientecFlgUpdateER2: TIntegerField
      FieldName = 'cFlgUpdateER2'
    end
    object qryAmbientecPageCodImpSAT: TStringField
      FieldName = 'cPageCodImpSAT'
      Size = 30
    end
    object qryAmbienteiLinhaImpSAT: TIntegerField
      FieldName = 'iLinhaImpSAT'
    end
    object qryAmbienteiColunaImpSAT: TIntegerField
      FieldName = 'iColunaImpSAT'
    end
    object qryAmbienteiEspacoImpSAT: TIntegerField
      FieldName = 'iEspacoImpSAT'
    end
    object qryAmbientecFlgImpUmaLinha: TIntegerField
      FieldName = 'cFlgImpUmaLinha'
    end
    object qryAmbientecFlgServidorSAT: TIntegerField
      FieldName = 'cFlgServidorSAT'
    end
    object qryAmbientecCNPJTEF: TStringField
      FieldName = 'cCNPJTEF'
    end
    object qryAmbientenPDVTEF: TIntegerField
      FieldName = 'nPDVTEF'
    end
    object qryAmbientecChaveAutTEF: TStringField
      FieldName = 'cChaveAutTEF'
      Size = 100
    end
  end
  object dsAmbiente: TDataSource
    DataSet = qryAmbiente
    Left = 368
    Top = 40
  end
  object qryTipoImpressoraPDV: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      '  FROM TipoImpressoraPDV'
      ' ORDER BY cNmTipoImpressoraPDV DESC')
    Left = 400
    Top = 8
    object qryTipoImpressoraPDVnCdTipoImpressoraPDV: TIntegerField
      FieldName = 'nCdTipoImpressoraPDV'
    end
    object qryTipoImpressoraPDVcNmTipoImpressoraPDV: TStringField
      FieldName = 'cNmTipoImpressoraPDV'
      Size = 50
    end
    object qryTipoImpressoraPDVcNomeDLL: TStringField
      FieldName = 'cNomeDLL'
      Size = 50
    end
  end
  object dsImpressoraPDV: TDataSource
    DataSet = qryTipoImpressoraPDV
    Left = 400
    Top = 40
  end
  object qryTipoImpressoraCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      'FROM TipoImpressoraPDV')
    Left = 432
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'nCdTipoImpressoraPDV'
    end
    object StringField1: TStringField
      FieldName = 'cNmTipoImpressoraPDV'
      Size = 50
    end
    object StringField2: TStringField
      FieldName = 'cNomeDLL'
      Size = 50
    end
  end
  object dsTipoImpressoraCaixa: TDataSource
    DataSet = qryTipoImpressoraCaixa
    Left = 432
    Top = 40
  end
  object qryTipoGavetaPDV: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *  FROM TipoGavetaPDV')
    Left = 468
    Top = 8
    object qryTipoGavetaPDVnCdTipoGavetaPDV: TIntegerField
      FieldName = 'nCdTipoGavetaPDV'
    end
    object qryTipoGavetaPDVcNmTipoGavetaPDV: TStringField
      FieldName = 'cNmTipoGavetaPDV'
      Size = 40
    end
  end
  object dsTipoGavetaPDV: TDataSource
    DataSet = qryTipoGavetaPDV
    Left = 468
    Top = 40
  end
  object OpenDialog: TOpenDialog
    Left = 334
    Top = 9
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 543
    Top = 9
  end
  object qryVerificaNumCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = '0'
      end
      item
        Name = 'nCdCodPDVSAT'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdServidorSAT'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmComputador'
      '  FROM ConfigAmbiente'
      ' WHERE UPPER(cNmComputador) <> UPPER(:cNmComputador)'
      '   AND nCdCodPDVSAT         =  :nCdCodPDVSAT'
      '   AND nCdServidorSAT       =  :nCdServidorSAT')
    Left = 543
    Top = 41
    object qryVerificaNumCaixacNmComputador: TStringField
      FieldName = 'cNmComputador'
      Size = 50
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 336
    Top = 45
  end
  object SaveDialog1: TSaveDialog
    Left = 304
    Top = 8
  end
  object qryServidorSAT: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ServidorSAT.*'
      '      ,cNmStatus'
      '  FROM ServidorSAT'
      
        '       INNER JOIN Status ON Status.nCdStatus = ServidorSAT.nCdSt' +
        'atus'
      ' WHERE nCdServidorSAT = :nPK')
    Left = 504
    Top = 8
    object qryServidorSATnCdServidorSAT: TIntegerField
      FieldName = 'nCdServidorSAT'
    end
    object qryServidorSATcNmServidorSAT: TStringField
      FieldName = 'cNmServidorSAT'
      Size = 100
    end
    object qryServidorSATdDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryServidorSATnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryServidorSATcNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsServidorSAT: TDataSource
    DataSet = qryServidorSAT
    Left = 504
    Top = 40
  end
  object IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket
    SSLOptions.Method = sslvSSLv2
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 236
    Top = 16
  end
  object IdSMTP: TIdSMTP
    IOHandler = IdSSLIOHandlerSocket1
    MaxLineAction = maException
    ReadTimeout = 0
    Port = 25
    AuthenticationType = atNone
    Left = 268
    Top = 16
  end
end
