inherited frmConfirma_Numerarios: TfrmConfirma_Numerarios
  Left = 30
  Top = 12
  Width = 1024
  Height = 768
  Caption = 'Recebimento de Numer'#225'rios'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1008
    Height = 703
  end
  object Label1: TLabel [1]
    Left = 64
    Top = 48
    Width = 79
    Height = 13
    Caption = 'C'#243'digo Remessa'
  end
  object Label2: TLabel [2]
    Left = 76
    Top = 128
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  object Label3: TLabel [3]
    Left = 60
    Top = 104
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Dinheiro'
  end
  object Label4: TLabel [4]
    Left = 62
    Top = 160
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Cheque'
  end
  object Label5: TLabel [5]
    Left = 76
    Top = 184
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  object Label6: TLabel [6]
    Left = 16
    Top = 216
    Width = 127
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Comprov. Cart'#227'o'
  end
  object Label7: TLabel [7]
    Left = 76
    Top = 240
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  object Label8: TLabel [8]
    Left = 6
    Top = 272
    Width = 137
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Comprov. Credi'#225'rio'
  end
  object Label9: TLabel [9]
    Left = 76
    Top = 296
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  object Label10: TLabel [10]
    Left = 75
    Top = 72
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Destino'
  end
  inherited ToolBar1: TToolBar
    Width = 1008
    ButtonWidth = 145
    TabOrder = 14
    inherited ToolButton1: TToolButton
      Caption = 'Confirmar Transfer'#234'ncia'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 145
    end
    inherited ToolButton2: TToolButton
      Left = 153
    end
  end
  object EdtCodRemessa: TMaskEdit [12]
    Left = 146
    Top = 40
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 0
    Text = '        '
    OnKeyDown = EdtCodRemessaKeyDown
  end
  object edtContaDinheiro: TMaskEdit [13]
    Left = 146
    Top = 120
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 3
    Text = '        '
    OnExit = edtContaDinheiroExit
    OnKeyDown = edtContaDinheiroKeyDown
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 218
    Top = 120
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource1
    TabOrder = 4
  end
  object edtValDinheiro: TMaskEdit [15]
    Left = 146
    Top = 96
    Width = 87
    Height = 21
    TabOrder = 2
    OnExit = edtValDinheiroExit
    OnKeyDown = edtValDinheiroKeyDown
  end
  object edtValCheque: TMaskEdit [16]
    Left = 146
    Top = 152
    Width = 87
    Height = 21
    TabOrder = 5
    OnExit = edtValChequeExit
    OnKeyDown = edtValChequeKeyDown
  end
  object edtContaCheque: TMaskEdit [17]
    Left = 146
    Top = 176
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 6
    Text = '        '
    OnExit = edtContaChequeExit
    OnKeyDown = edtContaChequeKeyDown
  end
  object edtContaCartao: TMaskEdit [18]
    Left = 146
    Top = 232
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 8
    Text = '        '
    OnExit = edtContaCartaoExit
    OnKeyDown = edtContaCartaoKeyDown
  end
  object edtValCartao: TMaskEdit [19]
    Left = 146
    Top = 208
    Width = 87
    Height = 21
    TabOrder = 7
    OnExit = edtValCartaoExit
    OnKeyDown = edtValCartaoKeyDown
  end
  object edtContaCrediario: TMaskEdit [20]
    Left = 146
    Top = 288
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 10
    Text = '        '
    OnExit = edtContaCrediarioExit
    OnKeyDown = edtContaCrediarioKeyDown
  end
  object edtValCrediario: TMaskEdit [21]
    Left = 146
    Top = 264
    Width = 87
    Height = 21
    TabOrder = 9
    OnExit = edtValCrediarioExit
    OnKeyDown = edtValCrediarioKeyDown
  end
  object DBEdit2: TDBEdit [22]
    Tag = 1
    Left = 218
    Top = 288
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource2
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [23]
    Tag = 1
    Left = 218
    Top = 232
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource3
    TabOrder = 12
  end
  object DBEdit4: TDBEdit [24]
    Tag = 1
    Left = 218
    Top = 176
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource4
    TabOrder = 13
  end
  object edtContaDestino: TMaskEdit [25]
    Left = 146
    Top = 64
    Width = 65
    Height = 21
    EditMask = '99999999;1; '
    MaxLength = 8
    TabOrder = 1
    Text = '        '
    OnExit = edtContaDestinoExit
    OnKeyDown = edtContaDestinoKeyDown
  end
  object DBEdit5: TDBEdit [26]
    Tag = 1
    Left = 218
    Top = 64
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource5
    TabOrder = 15
  end
  inherited ImageList1: TImageList
    Left = 416
  end
  object qryContaBancariaDin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 344
    Top = 160
    object qryContaBancariaDinnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDinnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancariaDin
    Left = 376
    Top = 248
  end
  object qryContaBancariaCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 384
    Top = 160
    object qryContaBancariaChequenCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaChequenCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object qryContaBancariaCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 424
    Top = 160
    object qryContaBancariaCartaonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaCartaonCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object qryContaBancariaCrediario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 456
    Top = 152
    object qryContaBancariaCrediarionCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaCrediarionCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancariaCrediario
    Left = 384
    Top = 248
  end
  object DataSource3: TDataSource
    DataSet = qryContaBancariaCartao
    Left = 392
    Top = 256
  end
  object DataSource4: TDataSource
    DataSet = qryContaBancariaCheque
    Left = 400
    Top = 264
  end
  object qryContaDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 488
    Top = 152
    object qryContaDestinonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaDestinonCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource5: TDataSource
    DataSet = qryContaDestino
    Left = 392
    Top = 248
  end
  object qryRemessaNum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT RemessaNum.*'
      '      ,IsNull((SELECT Sum(nValCheque)'
      '                 FROM ChequeRemessaNum'
      
        '                      INNER JOIN Cheque ON Cheque.nCdCheque = Ch' +
        'equeRemessaNum.nCdCheque'
      
        '                WHERE ChequeRemessaNum.nCdRemessaNum = RemessaNu' +
        'm.nCdRemessaNum),0) nValCheque'
      '      ,IsNull((SELECT Sum(nValTransacao)'
      '                 FROM CartaoRemessaNum'
      
        '                      INNER JOIN TransacaoCartao ON TransacaoCar' +
        'tao.nCdTransacaoCartao = CartaoRemessaNum.nCdTransacaoCartao'
      
        '                WHERE CartaoRemessaNum.nCdRemessaNum = RemessaNu' +
        'm.nCdRemessaNum),0) nValCartao'
      '      ,IsNull((SELECT Sum(nValCrediario)'
      '                 FROM CrediarioRemessaNum'
      
        '                      INNER JOIN Crediario ON Crediario.nCdCredi' +
        'ario = CrediarioRemessaNum.nCdCrediario'
      
        '                WHERE CrediarioRemessaNum.nCdRemessaNum = Remess' +
        'aNum.nCdRemessaNum),0) nValCrediario'
      'FROM RemessaNum'
      'WHERE nCdRemessaNum = :nPK')
    Left = 504
    Top = 248
    object qryRemessaNumnCdRemessaNum: TIntegerField
      FieldName = 'nCdRemessaNum'
    end
    object qryRemessaNumnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryRemessaNumnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryRemessaNumnCdContaOrigem: TIntegerField
      FieldName = 'nCdContaOrigem'
    end
    object qryRemessaNumnCdContaDestino: TIntegerField
      FieldName = 'nCdContaDestino'
    end
    object qryRemessaNumnValDinheiro: TBCDField
      FieldName = 'nValDinheiro'
      Precision = 12
      Size = 2
    end
    object qryRemessaNumnCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryRemessaNumdDtRemessa: TDateTimeField
      FieldName = 'dDtRemessa'
    end
    object qryRemessaNumdDtFech: TDateTimeField
      FieldName = 'dDtFech'
    end
    object qryRemessaNumdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryRemessaNumcOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryRemessaNumdDtConfirma: TDateTimeField
      FieldName = 'dDtConfirma'
    end
    object qryRemessaNumnCdUsuarioConfirma: TIntegerField
      FieldName = 'nCdUsuarioConfirma'
    end
    object qryRemessaNumnValCheque: TBCDField
      FieldName = 'nValCheque'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryRemessaNumnValCartao: TBCDField
      FieldName = 'nValCartao'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryRemessaNumnValCrediario: TBCDField
      FieldName = 'nValCrediario'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object SP_CONFIRMA_NUMERARIO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONFIRMA_NUMERARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRemessaNum'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaDestino'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaDin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaCrediario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 512
    Top = 296
  end
end
