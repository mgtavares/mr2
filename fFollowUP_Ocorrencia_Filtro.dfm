inherited frmFollowUp_Ocorrencia_Filtro: TfrmFollowUp_Ocorrencia_Filtro
  Left = 331
  Top = 323
  Width = 716
  Height = 195
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Filtro FollowUp'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 700
    Height = 128
  end
  object Label6: TLabel [1]
    Left = 196
    Top = 112
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label8: TLabel [2]
    Left = 196
    Top = 136
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [3]
    Left = 14
    Top = 136
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Ocorr'#234'ncia'
  end
  object Label3: TLabel [4]
    Left = 29
    Top = 112
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Atend.'
  end
  object Label1: TLabel [5]
    Left = 65
    Top = 40
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [6]
    Left = 86
    Top = 64
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label5: TLabel [7]
    Left = 32
    Top = 88
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Ocorr'#234'ncia'
  end
  inherited ToolBar1: TToolBar
    Width = 700
    TabOrder = 7
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Enabled = False
      Visible = False
    end
  end
  object mskDtOcorrenciaFinal: TMaskEdit [9]
    Left = 216
    Top = 128
    Width = 62
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object mskDtAtendFinal: TMaskEdit [10]
    Left = 216
    Top = 104
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object mskDtOcorrenciaInicial: TMaskEdit [11]
    Left = 112
    Top = 128
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object mskDtAtendInicial: TMaskEdit [12]
    Left = 112
    Top = 104
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtEmpresa: TDBEdit [13]
    Tag = 1
    Left = 112
    Top = 32
    Width = 65
    Height = 21
    DataField = 'nCdEmpresa'
    DataSource = dsEmpresa
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [14]
    Tag = 1
    Left = 180
    Top = 32
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 8
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 243
    Top = 32
    Width = 446
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 9
  end
  object er2LkpLoja: TER2LookupMaskEdit [16]
    Left = 112
    Top = 56
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 180
    Top = 56
    Width = 509
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 10
  end
  object er2LkpTipoOcorrencia: TER2LookupMaskEdit [18]
    Left = 112
    Top = 80
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 777
    QueryLookup = qryTipoOcorrencia
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 180
    Top = 80
    Width = 509
    Height = 21
    DataField = 'cNmTipoOcorrencia'
    DataSource = dsTipoOcorrencia
    TabOrder = 11
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      '   AND nCdEmpresa = :nPK')
    Left = 440
    Top = 64
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 440
    Top = 96
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 472
    Top = 96
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM Loja'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioLoja'
      '               WHERE UsuarioLoja.nCdLoja    = Loja.nCdLoja'
      '                 AND UsuarioLoja.nCdUsuario = :nPK)')
    Left = 472
    Top = 64
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTipoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryTipoOcorrenciaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTipoOcorrencia int'
      ''
      'SET @nCdTipoOcorrencia = :nPK'
      ''
      'SELECT nCdTipoOcorrencia'
      '      ,cNmTipoOcorrencia'
      '  FROM TipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = @nCdTipoOcorrencia'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioAtendTipoOcorrencia'
      '                WHERE nCdUsuario = :nCdUsuario'
      '                  AND nCdTipoOcorrencia = @nCdTipoOcorrencia)')
    Left = 408
    Top = 64
    object qryTipoOcorrencianCdTipoOcorrencia: TIntegerField
      FieldName = 'nCdTipoOcorrencia'
    end
    object qryTipoOcorrenciacNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
  end
  object dsTipoOcorrencia: TDataSource
    DataSet = qryTipoOcorrencia
    Left = 408
    Top = 96
  end
end
