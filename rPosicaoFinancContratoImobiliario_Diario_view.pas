unit rPosicaoFinancContratoImobiliario_Diario_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoFinancContratoImobiliario_Diario_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryTempParcelas: TADOQuery;
    qryTempParcelasnCdTitulo: TIntegerField;
    qryTempParcelascNmTerceiro: TStringField;
    qryTempParcelascNmEmpreendimentoBloco: TStringField;
    qryTempParcelasdDtVenc: TDateTimeField;
    qryTempParcelasiParcela: TIntegerField;
    qryTempParcelascNmTipoParcela: TStringField;
    qryTempParcelasnValTit: TBCDField;
    qryTempParcelasnValCorrecao: TBCDField;
    qryTempParcelasnValJuroMulta: TBCDField;
    qryTempParcelasnSaldoFinal: TBCDField;
    qryTempParcelascNumContrato: TStringField;
    qryTempParcelascNrUnidade: TStringField;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape3: TQRShape;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoFinancContratoImobiliario_Diario_view: TrptPosicaoFinancContratoImobiliario_Diario_view;

implementation

{$R *.dfm}

end.
