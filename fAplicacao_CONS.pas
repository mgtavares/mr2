unit fAplicacao_CONS;

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fConsulta_Template, DB, ADODB, ImgList, StdCtrls, GridsEh,
  DBGridEh, ComCtrls, ToolWin, ExtCtrls, Mask;

type
  TfrmAplicacao_CONS = class(TfrmConsulta_Template)
    ADOQuery2: TADOQuery;
    ADOQuery2nCdModulo: TIntegerField;
    ADOQuery2cNmModulo: TStringField;
    ADOQuery2nCdStatus: TIntegerField;
    ADOQuery1nCdAplicacao: TIntegerField;
    ADOQuery1cNmAplicacao: TStringField;
    ADOQuery1nCdStatus: TIntegerField;
    ADOQuery1nCdModulo: TIntegerField;
    ADOQuery1cNmModulo: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure ExecutaConsulta();
    procedure ToolButton1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton3Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAplicacao_CONS: TfrmAplicacao_CONS;

implementation

uses fAplicacao_CAD, fMenu;

{$R *.dfm}


procedure TfrmAplicacao_CONS.FormCreate(Sender: TObject);
begin
  inherited;
RegisterClass(TfrmAplicacao_CONS);

end;

procedure TfrmAplicacao_CONS.ExecutaConsulta;
begin
    ADOQuery1.SQL.Clear;

    If (Trim(QtdeRegistros.Text) <> '') And (StrToInt(Trim(qtdeRegistros.Text)) > 0) then
      ADOQuery1.SQL.Add('SELECT TOP ' + Trim(QtdeRegistros.Text) + ' * FROM APLICACAO')
    else 
      ADOQuery1.SQL.Add('SELECT * FROM APLICACAO') ;

    If (Trim(Edit1.Text) <> '') then
    begin
        ADOQuery1.SQL.Add('WHERE cNmAplicacao Like ' + Chr(39) + Trim(Edit1.Text) + '%' + Chr(39)) ;
    end;

    ADOQuery1.Close ;
    ADOQuery1.Open ;
end;

procedure TfrmAplicacao_CONS.ToolButton1Click(Sender: TObject);
begin
  inherited;
  ExecutaConsulta;
end;

procedure TfrmAplicacao_CONS.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin
    key := #0;
    ExecutaConsulta;
  end;

end;

procedure TfrmAplicacao_CONS.ToolButton3Click(Sender: TObject);
var
    frm : TForm ;
begin
  Try
    frm := TfrmAplicacao_CAD.Create (self); // Cria��o do formul�rio na mem�ria
    //frm.ManualDock(frmMenu.PageControlPageDock.ActivePage , nil, alClient) ;
    frm.ClientHeight := frmMenu.PageControlPageDock.ActivePage.Height;
    frm.Top := frmMenu.PageControlPageDock.Top ;
    frm.Left := frmMenu.PageControlPageDock.Left;
    frm.Width := frmMenu.PageControlPageDock.Width;
    frm.ShowModal ; //.Show; // Exibi��o para o usu�rio
  Finally
    FreeAndNil (frm) // Libera dos recursos alocados na mem�ria
  End; // Try

end;

procedure TfrmAplicacao_CONS.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  //frmAplicacao_Cad.PreparaTabelas();

    frmAplicacao_Cad.qryMaster.Locate('nCdAplicacao',ADOQuery1nCdAplicacao.Value,[loCaseInsensitive]) ;
    //frmAplicacao_Cad.ManualDock(frmMenu.PageControlPageDock, nil, alClient);
    //frmAplicacao_Cad.Visible := True;

    frmMenu.OpenForm(frmAplicacao_Cad,frmAplicacao_Cad.Caption) ;

end;

end.
