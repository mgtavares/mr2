object frmEmiteNFe: TfrmEmiteNFe
  Left = 157
  Top = 122
  Width = 914
  Height = 563
  Caption = 'Emiss'#227'o NFe'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ACBrNFe1: TACBrNFe
    Configuracoes.Geral.PathSalvar = 'C:\Program Files\Borland\Delphi7\Bin\'
    Configuracoes.WebServices.Visualizar = True
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.IntervaloTentativas = 0
    Configuracoes.WebServices.AjustaAguardaConsultaRet = False
    DANFE = ACBrNFeDANFEQR1
    Left = 272
    Top = 120
  end
  object ACBrNFeDANFEQR1: TACBrNFeDANFEQR
    ACBrNFe = ACBrNFe1
    Sistema = 'www.er2soft.com.br'
    PathPDF = 'C:\Program Files\Borland\Delphi7\Bin\'
    ImprimirHoraSaida = False
    MostrarPreview = False
    MostrarStatus = True
    TipoDANFE = tiRetrato
    NumCopias = 1
    ImprimirDescPorc = False
    ImprimirTotalLiquido = False
    MargemInferior = 0.800000000000000000
    MargemSuperior = 0.800000000000000000
    MargemEsquerda = 0.600000000000000000
    MargemDireita = 0.510000000000000000
    CasasDecimais._qCom = 4
    CasasDecimais._vUnCom = 4
    ExibirResumoCanhoto = False
    FormularioContinuo = False
    TamanhoFonte_DemaisCampos = 10
    ProdutosPorPagina = 0
    Left = 312
    Top = 120
  end
  object qryConfigAmbiente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cUFEmissaoNfe'
      
        '      ,cPathLogoNFe                                             ' +
        '                                            '
      '      ,nCdTipoAmbienteNFe'
      '      ,cFlgEmiteNFe '
      '      ,cNrSerieCertificadoNFe'
      '      ,nCdUFEmissaoNFe        '
      '      ,nCdMunicipioEmissaoNFe'
      '      ,CASE WHEN nCdTipoAmbienteNFe = 0 THEN cPathArqNFeProd'
      '            ELSE cPathArqNFeHom'
      '       END cPathArqNFe'
      '      ,cServidorSMTP'
      '      ,nPortaSMTP'
      '      ,cEmail'
      '      ,cSenhaEmail'
      '      ,cFlgUsaSSL'
      '  FROM ConfigAmbiente'
      ' WHERE cNmComputador = :cNmComputador')
    Left = 316
    Top = 185
    object qryConfigAmbientecUFEmissaoNfe: TStringField
      FieldName = 'cUFEmissaoNfe'
      Size = 2
    end
    object qryConfigAmbientecPathLogoNFe: TStringField
      FieldName = 'cPathLogoNFe'
      Size = 100
    end
    object qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField
      FieldName = 'nCdTipoAmbienteNFe'
    end
    object qryConfigAmbientecFlgEmiteNFe: TIntegerField
      FieldName = 'cFlgEmiteNFe'
    end
    object qryConfigAmbientecNrSerieCertificadoNFe: TStringField
      FieldName = 'cNrSerieCertificadoNFe'
      Size = 50
    end
    object qryConfigAmbientenCdUFEmissaoNFe: TIntegerField
      FieldName = 'nCdUFEmissaoNFe'
    end
    object qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField
      FieldName = 'nCdMunicipioEmissaoNFe'
    end
    object qryConfigAmbientecPathArqNFe: TStringField
      FieldName = 'cPathArqNFe'
      ReadOnly = True
      Size = 100
    end
    object qryConfigAmbientecServidorSMTP: TStringField
      FieldName = 'cServidorSMTP'
      Size = 50
    end
    object qryConfigAmbientenPortaSMTP: TIntegerField
      FieldName = 'nPortaSMTP'
    end
    object qryConfigAmbientecEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryConfigAmbientecSenhaEmail: TStringField
      FieldName = 'cSenhaEmail'
      Size = 50
    end
    object qryConfigAmbientecFlgUsaSSL: TIntegerField
      FieldName = 'cFlgUsaSSL'
    end
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '      ,(SELECT TOP 1 1'
      '          FROM PrazoDoctoFiscal PDF'
      '         WHERE PDF.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFiscal'
      
        '           AND PDF.dDtVenc        > DoctoFiscal.dDtEmissao) cFlg' +
        'APrazo'
      '      ,(SELECT DFC.cChaveNFe'
      '          FROM DoctoFiscal DFC'
      
        '         WHERE DFC.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFiscalOr' +
        'igemCompl) cNFeRef'
      '  FROM DoctoFiscal'
      '  WHERE nCdDoctoFiscal =:nCdDoctoFiscal')
    Left = 360
    Top = 184
    object qryDoctoFiscalnCdDoctoFiscal: TAutoIncField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryDoctoFiscalnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryDoctoFiscalnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryDoctoFiscalnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryDoctoFiscalcFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
    object qryDoctoFiscalcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoFiscalcTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoFiscalnCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscaldDtSaida: TDateTimeField
      FieldName = 'dDtSaida'
    end
    object qryDoctoFiscaldDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
    object qryDoctoFiscalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoFiscalcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryDoctoFiscalcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryDoctoFiscalcIE: TStringField
      FieldName = 'cIE'
      Size = 14
    end
    object qryDoctoFiscalcTelefone: TStringField
      FieldName = 'cTelefone'
    end
    object qryDoctoFiscalnCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
    end
    object qryDoctoFiscalcEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 50
    end
    object qryDoctoFiscaliNumero: TIntegerField
      FieldName = 'iNumero'
    end
    object qryDoctoFiscalcBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryDoctoFiscalcCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryDoctoFiscalcCep: TStringField
      FieldName = 'cCep'
      FixedChar = True
      Size = 8
    end
    object qryDoctoFiscalcUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValProduto: TBCDField
      FieldName = 'nValProduto'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValICMS: TBCDField
      FieldName = 'nValICMS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValIsenta: TBCDField
      FieldName = 'nValIsenta'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValOutras: TBCDField
      FieldName = 'nValOutras'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValDespesa: TBCDField
      FieldName = 'nValDespesa'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValFrete: TBCDField
      FieldName = 'nValFrete'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscaldDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryDoctoFiscalnCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryDoctoFiscalnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryDoctoFiscalcFlgFaturado: TIntegerField
      FieldName = 'cFlgFaturado'
    end
    object qryDoctoFiscalnCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryDoctoFiscalcNmTransp: TStringField
      FieldName = 'cNmTransp'
      Size = 50
    end
    object qryDoctoFiscalcCNPJTransp: TStringField
      FieldName = 'cCNPJTransp'
      Size = 14
    end
    object qryDoctoFiscalcIETransp: TStringField
      FieldName = 'cIETransp'
      Size = 14
    end
    object qryDoctoFiscalcEnderecoTransp: TStringField
      FieldName = 'cEnderecoTransp'
      Size = 50
    end
    object qryDoctoFiscalcBairroTransp: TStringField
      FieldName = 'cBairroTransp'
      Size = 35
    end
    object qryDoctoFiscalcCidadeTransp: TStringField
      FieldName = 'cCidadeTransp'
      Size = 35
    end
    object qryDoctoFiscalcUFTransp: TStringField
      FieldName = 'cUFTransp'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscaliQtdeVolume: TIntegerField
      FieldName = 'iQtdeVolume'
    end
    object qryDoctoFiscalnPesoBruto: TBCDField
      FieldName = 'nPesoBruto'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnPesoLiq: TBCDField
      FieldName = 'nPesoLiq'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcPlaca: TStringField
      FieldName = 'cPlaca'
      Size = 10
    end
    object qryDoctoFiscalcUFPlaca: TStringField
      FieldName = 'cUFPlaca'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalnValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValFatura: TBCDField
      FieldName = 'nValFatura'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryDoctoFiscalnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryDoctoFiscalnCdUsuarioImpr: TIntegerField
      FieldName = 'nCdUsuarioImpr'
    end
    object qryDoctoFiscalnCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryDoctoFiscaldDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryDoctoFiscalnCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryDoctoFiscalnCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryDoctoFiscalcFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryDoctoFiscalnValCredAdiant: TBCDField
      FieldName = 'nValCredAdiant'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValIcentivoFiscal: TBCDField
      FieldName = 'nValIcentivoFiscal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnCdLanctoFinDoctoFiscal: TIntegerField
      FieldName = 'nCdLanctoFinDoctoFiscal'
    end
    object qryDoctoFiscaldDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryDoctoFiscaldDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryDoctoFiscalnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryDoctoFiscaldDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryDoctoFiscalcFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
    object qryDoctoFiscalcNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryDoctoFiscalcNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
    object qryDoctoFiscalcFlgAPrazo: TIntegerField
      FieldName = 'cFlgAPrazo'
      ReadOnly = True
    end
    object qryDoctoFiscalcComplEnderDestino: TStringField
      FieldName = 'cComplEnderDestino'
      Size = 35
    end
    object qryDoctoFiscalnCdMunicipioDestinoIBGE: TIntegerField
      FieldName = 'nCdMunicipioDestinoIBGE'
    end
    object qryDoctoFiscalnCdPaisDestino: TIntegerField
      FieldName = 'nCdPaisDestino'
    end
    object qryDoctoFiscalcNmPaisDestino: TStringField
      FieldName = 'cNmPaisDestino'
      Size = 50
    end
    object qryDoctoFiscalcCNPJCPFEmitente: TStringField
      FieldName = 'cCNPJCPFEmitente'
      Size = 14
    end
    object qryDoctoFiscalcIEEmitente: TStringField
      FieldName = 'cIEEmitente'
      Size = 14
    end
    object qryDoctoFiscalcNmRazaoSocialEmitente: TStringField
      FieldName = 'cNmRazaoSocialEmitente'
      Size = 50
    end
    object qryDoctoFiscalcNmFantasiaEmitente: TStringField
      FieldName = 'cNmFantasiaEmitente'
      Size = 50
    end
    object qryDoctoFiscalcTelefoneEmitente: TStringField
      FieldName = 'cTelefoneEmitente'
    end
    object qryDoctoFiscalnCdEnderecoEmitente: TIntegerField
      FieldName = 'nCdEnderecoEmitente'
    end
    object qryDoctoFiscalcEnderecoEmitente: TStringField
      FieldName = 'cEnderecoEmitente'
      Size = 50
    end
    object qryDoctoFiscaliNumeroEmitente: TIntegerField
      FieldName = 'iNumeroEmitente'
    end
    object qryDoctoFiscalcComplEnderEmitente: TStringField
      FieldName = 'cComplEnderEmitente'
      Size = 35
    end
    object qryDoctoFiscalcBairroEmitente: TStringField
      FieldName = 'cBairroEmitente'
      Size = 35
    end
    object qryDoctoFiscalnCdMunicipioEmitenteIBGE: TIntegerField
      FieldName = 'nCdMunicipioEmitenteIBGE'
    end
    object qryDoctoFiscalcCidadeEmitente: TStringField
      FieldName = 'cCidadeEmitente'
      Size = 35
    end
    object qryDoctoFiscalcUFEmitente: TStringField
      FieldName = 'cUFEmitente'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalcCepEmitente: TStringField
      FieldName = 'cCepEmitente'
      FixedChar = True
      Size = 8
    end
    object qryDoctoFiscalnCdPaisEmitente: TIntegerField
      FieldName = 'nCdPaisEmitente'
    end
    object qryDoctoFiscalcNmPaisEmitente: TStringField
      FieldName = 'cNmPaisEmitente'
      Size = 50
    end
    object qryDoctoFiscalcSuframa: TStringField
      FieldName = 'cSuframa'
    end
    object qryDoctoFiscalcXMLNFe: TMemoField
      FieldName = 'cXMLNFe'
      BlobType = ftMemo
    end
    object qryDoctoFiscalcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryDoctoFiscalcNrProtocoloCancNFe: TStringField
      FieldName = 'cNrProtocoloCancNFe'
      Size = 100
    end
    object qryDoctoFiscalcCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryDoctoFiscalcArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryDoctoFiscalnValSeguro: TBCDField
      FieldName = 'nValSeguro'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcFlgComplementar: TIntegerField
      FieldName = 'cFlgComplementar'
    end
    object qryDoctoFiscalnCdDoctoFiscalOrigemCompl: TIntegerField
      FieldName = 'nCdDoctoFiscalOrigemCompl'
    end
    object qryDoctoFiscalcNmMotivoComplemento: TStringField
      FieldName = 'cNmMotivoComplemento'
      Size = 150
    end
    object qryDoctoFiscalcNFeRef: TStringField
      FieldName = 'cNFeRef'
      ReadOnly = True
      Size = 100
    end
  end
  object qryItemDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT CASE WHEN ((nCdTipoItemPed <> 5) OR (nCdTipoItemPed IS NU' +
        'LL)) THEN Convert(VARCHAR,ItemDoctoFiscal.nCdProduto)'
      '            ELSE '#39'CFOP'#39' + cCFOP'
      '       END nCdProduto'
      '      ,ItemDoctoFiscal.cNmItem'
      '      ,cDescricaoAdicional'
      '      ,cCFOP'
      '      ,GRI.cNCM'
      '      ,ItemDoctoFiscal.cCdST'
      '      ,ItemDoctoFiscal.cCdSTIPI'
      '      ,GRI.cUnidadeMedida'
      '      ,GRI.nCdTabTipoOrigemMercadoria'
      '      ,ItemDoctoFiscal.nValUnitario'
      '      ,nAliqICMS'
      '      ,ItemDoctoFiscal.nAliqIPI'
      '      ,ItemDoctoFiscal.nPercIVA'
      '      ,ItemDoctoFiscal.nPercBCICMS'
      '      ,ItemDoctoFiscal.nPercBCIVA'
      '      ,ItemDoctoFiscal.nPercBCICMSST'
      '      ,Sum(ItemDoctoFiscal.nQtde)                    nQtde'
      '      ,Sum(ItemDoctoFiscal.nValTotal)                nValTotal'
      
        '      ,Sum(ItemDoctoFiscal.nValBaseICMS)             nValBaseICM' +
        'S'
      '      ,Sum(ItemDoctoFiscal.nValICMS)                 nValICMS'
      '      ,Sum(ItemDoctoFiscal.nValIPI)                  nValIPI'
      '      ,Sum(ItemDoctoFiscal.nValBaseIPI)              nValBaseIPI'
      
        '      ,Sum(ItemDoctoFiscal.nValBASEICMSSub)          nValBASEICM' +
        'SSub'
      '      ,Sum(ItemDoctoFiscal.nValICMSSub)              nValICMSSub'
      '      ,Sum(ItemDoctoFiscal.nValIsenta)               nValIsenta'
      '      ,Sum(ItemDoctoFiscal.nValOutras)               nValOutras'
      '      ,Sum(ItemDoctoFiscal.nValDespesa)              nValDespesa'
      '      ,Sum(ItemDoctoFiscal.nValFrete)                nValFrete'
      
        '      ,itemDoctoFiscal.nValDesconto                  nValDescont' +
        'o'
      '  FROM ItemDoctoFiscal'
      
        '       INNER JOIN GrupoImposto GRI ON GRI.nCdGrupoImposto      =' +
        ' ItemDoctoFiscal.nCdGrupoImposto'
      
        '       LEFT  JOIN ItemPedido       ON ItemPedido.nCdItemPedido =' +
        ' ItemDoctoFiscal.nCdItemPedido'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      
        ' GROUP BY CASE WHEN ((nCdTipoItemPed <> 5) OR (nCdTipoItemPed IS' +
        ' NULL)) THEN Convert(VARCHAR,ItemDoctoFiscal.nCdProduto)'
      '            ELSE '#39'CFOP'#39' + cCFOP'
      '       END'
      '      ,ItemDoctoFiscal.cNmItem'
      '      ,cDescricaoAdicional'
      '      ,cCFOP'
      '      ,GRI.cNCM'
      '      ,ItemDoctoFiscal.cCdST'
      '      ,ItemDoctoFiscal.cCdSTIPI'
      '      ,GRI.cUnidadeMedida'
      '      ,GRI.nCdTabTipoOrigemMercadoria'
      '      ,ItemDoctoFiscal.nValUnitario'
      '      ,nAliqICMS'
      '      ,ItemDoctoFiscal.nAliqIPI'
      '      ,ItemDoctoFiscal.nPercIVA'
      '      ,ItemDoctoFiscal.nPercBCICMS'
      '      ,ItemDoctoFiscal.nPercBCIVA'
      '      ,ItemDoctoFiscal.nPercBCICMSST'
      '      ,ItemDoctoFiscal.nValDesconto '
      ' ORDER BY ItemDoctoFiscal.cNmItem'
      '         ,cCFOP'
      '')
    Left = 320
    Top = 232
    object qryItemDoctoFiscalnCdProduto: TStringField
      FieldName = 'nCdProduto'
      ReadOnly = True
      Size = 30
    end
    object qryItemDoctoFiscalcNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemDoctoFiscalcDescricaoAdicional: TStringField
      FieldName = 'cDescricaoAdicional'
      Size = 100
    end
    object qryItemDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryItemDoctoFiscalcNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryItemDoctoFiscalcCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalcCdSTIPI: TStringField
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalcUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField
      FieldName = 'nCdTabTipoOrigemMercadoria'
    end
    object qryItemDoctoFiscalnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryItemDoctoFiscalnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 4
      Size = 2
    end
    object qryItemDoctoFiscalnAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      Precision = 4
      Size = 2
    end
    object qryItemDoctoFiscalnPercIVA: TBCDField
      FieldName = 'nPercIVA'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCICMS: TBCDField
      FieldName = 'nPercBCICMS'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCIVA: TBCDField
      FieldName = 'nPercBCIVA'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCICMSST: TBCDField
      FieldName = 'nPercBCICMSST'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnQtde: TBCDField
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMS: TBCDField
      FieldName = 'nValICMS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValIPI: TBCDField
      FieldName = 'nValIPI'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseIPI: TBCDField
      FieldName = 'nValBaseIPI'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBASEICMSSub: TBCDField
      FieldName = 'nValBASEICMSSub'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValIsenta: TBCDField
      FieldName = 'nValIsenta'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValOutras: TBCDField
      FieldName = 'nValOutras'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValDespesa: TBCDField
      FieldName = 'nValDespesa'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValFrete: TBCDField
      FieldName = 'nValFrete'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      ReadOnly = True
      Precision = 32
      Size = 6
    end
  end
  object qryPrazoDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT iParcela'
      '      ,dDtVenc'
      '      ,nValPagto'
      '  FROM PrazoDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ORDER BY iParcela')
    Left = 360
    Top = 232
    object qryPrazoDoctoFiscaliParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryPrazoDoctoFiscaldDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryPrazoDoctoFiscalnValPagto: TBCDField
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
  end
  object qryMsgDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cMsg'
      '  FROM MsgDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ORDER BY iOrdem')
    Left = 320
    Top = 272
    object qryMsgDoctoFiscalcMsg: TStringField
      FieldName = 'cMsg'
      Size = 150
    end
  end
  object qryDescontoTotal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      'SUM(nQtde * nValDesconto) nValDescontoItens'
      ' FROM ItemDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ')
    Left = 408
    Top = 184
    object qryDescontoTotalnValDescontoItens: TBCDField
      FieldName = 'nValDescontoItens'
      ReadOnly = True
      Precision = 32
      Size = 8
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      'cFlgTipoDescontoDanfe'
      ' FROM Terceiro'
      ' WHERE nCdTerceiro = :nCdTerceiro'
      ' '
      '')
    Left = 440
    Top = 184
    object qryTerceirocFlgTipoDescontoDanfe: TIntegerField
      FieldName = 'cFlgTipoDescontoDanfe'
    end
  end
end
