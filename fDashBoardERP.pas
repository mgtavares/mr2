unit fDashBoardERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, StdCtrls, cxLookAndFeelPainters, cxButtons, DB, ADODB,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, Mask,
  DBCtrls, TeEngine, Series, TeeProcs, Chart, DbChart;

type
  TfrmDashBoardERP = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTop10: TcxTabSheet;
    GroupBox1: TGroupBox;
    Top10dDtInicial: TDateTimePicker;
    Top10dDtFinal: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    btAtualizarTop10: TcxButton;
    cxPageControl2: TcxPageControl;
    cxTop10Clientes: TcxTabSheet;
    cxPageControl3: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControl4: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    cxPageControl5: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    qryTop10Clientes: TADOQuery;
    qryTop10ClientesnCdTerceiro: TIntegerField;
    qryTop10ClientescNmTerceiro: TStringField;
    qryTop10ClientesnValTotalPedido: TBCDField;
    cxPageControl6: TcxPageControl;
    cxTop10ClienteDados: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsTop10Clientes: TDataSource;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotalPedido: TcxGridDBColumn;
    cxPageControl7: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryTop10Fornecedores: TADOQuery;
    dsTop10Fornecedores: TDataSource;
    qryTop10FornecedoresnCdTerceiro: TIntegerField;
    qryTop10FornecedorescNmTerceiro: TStringField;
    qryTop10FornecedoresnValTotalReceb: TBCDField;
    cxGridDBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1nValTotalReceb: TcxGridDBColumn;
    qryTop10Produtos: TADOQuery;
    dsTop10Produtos: TDataSource;
    qryTop10ProdutosnCdProduto: TIntegerField;
    qryTop10ProdutoscNmProduto: TStringField;
    qryTop10ProdutosnValTotalPedido: TBCDField;
    cxPageControl8: TcxPageControl;
    cxTabSheet7: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2nCdProduto: TcxGridDBColumn;
    cxGridDBTableView2cNmProduto: TcxGridDBColumn;
    cxGridDBTableView2nValTotalPedido: TcxGridDBColumn;
    qryTop10Representantes: TADOQuery;
    StringField1: TStringField;
    BCDField1: TBCDField;
    dsTop10Representantes: TDataSource;
    cxPageControl9: TcxPageControl;
    cxTabSheet9: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView3nValTotalPedido: TcxGridDBColumn;
    qryTop10RepresentantesnCdTerceiroRepres: TIntegerField;
    cxTabFinanceiro: TcxTabSheet;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    FinanceirodDtInicial: TDateTimePicker;
    FinanceirodDtFinal: TDateTimePicker;
    btAtualizarFinanceiro: TcxButton;
    cxPageControl10: TcxPageControl;
    cxTabSheet10: TcxTabSheet;
    qryDispCaixa: TADOQuery;
    dsDispCaixa: TDataSource;
    qryDispCaixacBanco: TStringField;
    qryDispCaixacAgencia: TIntegerField;
    qryDispCaixanCdConta: TStringField;
    qryDispCaixanSaldoConta: TBCDField;
    qryDispCaixadDtUltConciliacao: TDateTimeField;
    qryDispCaixanValLimiteCredito: TBCDField;
    qryDispCaixanSaldoTotal: TBCDField;
    btFluxoCaixa: TcxButton;
    btOrcamento: TcxButton;
    btDRE: TcxButton;
    cxPageControl11: TcxPageControl;
    cxTabSheet11: TcxTabSheet;
    cxGrid6: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    cxTabSheet12: TcxTabSheet;
    cxPageControl12: TcxPageControl;
    cxPagarHoje: TcxTabSheet;
    cxPagarAtraso: TcxTabSheet;
    cxPagarTotal: TcxTabSheet;
    qryPagarHoje: TADOQuery;
    qryPagarHojedDtVenc: TDateTimeField;
    qryPagarHojenCdTitulo: TIntegerField;
    qryPagarHojecNrTit: TStringField;
    qryPagarHojecNrNF: TStringField;
    qryPagarHojeiParcela: TIntegerField;
    qryPagarHojecNmEspTit: TStringField;
    qryPagarHojecNmTerceiro: TStringField;
    qryPagarHojecNmCategFinanc: TStringField;
    qryPagarHojenSaldoTit: TBCDField;
    qryPagarHojecSenso: TStringField;
    cxGrid5: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    dsPagarHoje: TDataSource;
    cxGridDBTableView4dDtVenc: TcxGridDBColumn;
    cxGridDBTableView4nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView4cNrTit: TcxGridDBColumn;
    cxGridDBTableView4cNrNF: TcxGridDBColumn;
    cxGridDBTableView4iParcela: TcxGridDBColumn;
    cxGridDBTableView4cNmEspTit: TcxGridDBColumn;
    cxGridDBTableView4cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView4cNmCategFinanc: TcxGridDBColumn;
    cxGridDBTableView4nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView4cSenso: TcxGridDBColumn;
    cxGrid7: TcxGrid;
    cxGridDBTableView6: TcxGridDBTableView;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridLevel6: TcxGridLevel;
    qryPagarAtraso: TADOQuery;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    BCDField2: TBCDField;
    StringField7: TStringField;
    dsPagarAtraso: TDataSource;
    cxGrid8: TcxGrid;
    cxGridDBTableView7: TcxGridDBTableView;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridDBColumn22: TcxGridDBColumn;
    cxGridDBColumn23: TcxGridDBColumn;
    cxGridDBColumn24: TcxGridDBColumn;
    cxGridDBColumn25: TcxGridDBColumn;
    cxGridDBColumn26: TcxGridDBColumn;
    cxGridDBColumn27: TcxGridDBColumn;
    cxGridLevel7: TcxGridLevel;
    qryPagarPeriodo: TADOQuery;
    DateTimeField2: TDateTimeField;
    IntegerField3: TIntegerField;
    StringField8: TStringField;
    StringField9: TStringField;
    IntegerField4: TIntegerField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    BCDField3: TBCDField;
    StringField13: TStringField;
    dsPagarPeriodo: TDataSource;
    cxPageControl13: TcxPageControl;
    cxTabSheet13: TcxTabSheet;
    cxGrid9: TcxGrid;
    cxGridDBTableView8: TcxGridDBTableView;
    cxGridDBColumn28: TcxGridDBColumn;
    cxGridDBColumn29: TcxGridDBColumn;
    cxGridDBColumn30: TcxGridDBColumn;
    cxGridDBColumn31: TcxGridDBColumn;
    cxGridDBColumn32: TcxGridDBColumn;
    cxGridDBColumn33: TcxGridDBColumn;
    cxGridDBColumn34: TcxGridDBColumn;
    cxGridDBColumn35: TcxGridDBColumn;
    cxGridDBColumn36: TcxGridDBColumn;
    cxGridDBColumn37: TcxGridDBColumn;
    cxGridLevel8: TcxGridLevel;
    cxTabSheet14: TcxTabSheet;
    cxGrid10: TcxGrid;
    cxGridDBTableView9: TcxGridDBTableView;
    cxGridDBColumn38: TcxGridDBColumn;
    cxGridDBColumn39: TcxGridDBColumn;
    cxGridDBColumn40: TcxGridDBColumn;
    cxGridDBColumn41: TcxGridDBColumn;
    cxGridDBColumn42: TcxGridDBColumn;
    cxGridDBColumn43: TcxGridDBColumn;
    cxGridDBColumn44: TcxGridDBColumn;
    cxGridDBColumn45: TcxGridDBColumn;
    cxGridDBColumn46: TcxGridDBColumn;
    cxGridDBColumn47: TcxGridDBColumn;
    cxGridLevel9: TcxGridLevel;
    cxTabSheet15: TcxTabSheet;
    cxGrid11: TcxGrid;
    cxGridDBTableView10: TcxGridDBTableView;
    cxGridDBColumn48: TcxGridDBColumn;
    cxGridDBColumn49: TcxGridDBColumn;
    cxGridDBColumn50: TcxGridDBColumn;
    cxGridDBColumn51: TcxGridDBColumn;
    cxGridDBColumn52: TcxGridDBColumn;
    cxGridDBColumn53: TcxGridDBColumn;
    cxGridDBColumn54: TcxGridDBColumn;
    cxGridDBColumn55: TcxGridDBColumn;
    cxGridDBColumn56: TcxGridDBColumn;
    cxGridDBColumn57: TcxGridDBColumn;
    cxGridLevel10: TcxGridLevel;
    qryReceberHoje: TADOQuery;
    DateTimeField3: TDateTimeField;
    IntegerField5: TIntegerField;
    StringField14: TStringField;
    StringField15: TStringField;
    IntegerField6: TIntegerField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    BCDField4: TBCDField;
    StringField19: TStringField;
    dsReceberHoje: TDataSource;
    qryReceberAtraso: TADOQuery;
    DateTimeField4: TDateTimeField;
    IntegerField7: TIntegerField;
    StringField20: TStringField;
    StringField21: TStringField;
    IntegerField8: TIntegerField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    BCDField5: TBCDField;
    StringField25: TStringField;
    dsReceberAtraso: TDataSource;
    qryReceberPeriodo: TADOQuery;
    DateTimeField5: TDateTimeField;
    IntegerField9: TIntegerField;
    StringField26: TStringField;
    StringField27: TStringField;
    IntegerField10: TIntegerField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    BCDField6: TBCDField;
    StringField31: TStringField;
    dsReceberPeriodo: TDataSource;
    cxTabVendas: TcxTabSheet;
    qryPreparaTempMeta: TADOQuery;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    btAtualizarMetaVenda: TcxButton;
    edtMesAnoMeta: TMaskEdit;
    qryTempVenda: TADOQuery;
    qryTempVendaiDiasUteis: TIntegerField;
    qryTempVendaiDiasDecorridos: TIntegerField;
    qryTempVendanPercDecorridos: TBCDField;
    qryTempVendanValMeta: TBCDField;
    qryTempVendanValMetaDia: TBCDField;
    qryTempVendanPercMetaAting_Venda: TBCDField;
    qryTempVendanPercMetaAting_Fat: TBCDField;
    qryTempVendanProjecao_Venda: TBCDField;
    qryTempVendanProjecao_Fat: TBCDField;
    qryTempVendanValVendaDia_AtMeta: TBCDField;
    qryTempVendanValFatDia_AtMeta: TBCDField;
    qryTempVendaDia: TADOQuery;
    qryTempVendaDiadData: TDateTimeField;
    qryTempVendaDianQtdePedido: TIntegerField;
    qryTempVendaDianValVenda: TBCDField;
    qryTempVendaDianValFaturamento: TBCDField;
    qryTempVendaDianValMeta: TBCDField;
    qryTempVendaDianValDiferenca: TBCDField;
    qryTempVendaDianValVendaAcum: TBCDField;
    qryTempVendaDianValFaturamentoAcum: TBCDField;
    qryTempVendaDianValMetaAcum: TBCDField;
    qryTempVendaDianValDiferencaAcum: TBCDField;
    qryTempVendaDiacFlgDiaUtil: TIntegerField;
    SPREL_VENDA_DIARIA: TADOStoredProc;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    cxPageControl14: TcxPageControl;
    cxTabSheet16: TcxTabSheet;
    cxGrid12: TcxGrid;
    cxGridDBTableView11: TcxGridDBTableView;
    cxGridLevel11: TcxGridLevel;
    dsTempVendaDia: TDataSource;
    cxGridDBTableView11dData: TcxGridDBColumn;
    cxGridDBTableView11nQtdePedido: TcxGridDBColumn;
    cxGridDBTableView11nValVenda: TcxGridDBColumn;
    cxGridDBTableView11nValFaturamento: TcxGridDBColumn;
    cxGridDBTableView11nValMeta: TcxGridDBColumn;
    cxGridDBTableView11nValDiferenca: TcxGridDBColumn;
    cxGridDBTableView11nValVendaAcum: TcxGridDBColumn;
    cxGridDBTableView11nValFaturamentoAcum: TcxGridDBColumn;
    cxGridDBTableView11nValMetaAcum: TcxGridDBColumn;
    cxGridDBTableView11nValDiferencaAcum: TcxGridDBColumn;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    DBChart1: TDBChart;
    dsTempVenda: TDataSource;
    Series1: TBarSeries;
    qrySomaVendaDia: TADOQuery;
    qrySomaVendaDianValTotalVenda: TBCDField;
    qrySomaVendaDianValFaturamento: TBCDField;
    dsSomaVendaDia: TDataSource;
    Series2: TBarSeries;
    Series3: TBarSeries;
    cxTabEstoque: TcxTabSheet;
    cxPageControl15: TcxPageControl;
    cxTabSheet17: TcxTabSheet;
    cxTabSheet18: TcxTabSheet;
    cxGrid13: TcxGrid;
    cxGridDBTableView12: TcxGridDBTableView;
    cxGridLevel12: TcxGridLevel;
    qryProdutoAbaixoMinimo: TADOQuery;
    qryProdutoAbaixoMinimonCdProduto: TIntegerField;
    qryProdutoAbaixoMinimocNmProduto: TStringField;
    qryProdutoAbaixoMinimocReferencia: TStringField;
    qryProdutoAbaixoMinimocUnidadeMedida: TStringField;
    qryProdutoAbaixoMinimocNmGrupoProduto: TStringField;
    qryProdutoAbaixoMinimonQtdeEstoque: TBCDField;
    qryProdutoAbaixoMinimonEstoqueMinimo: TBCDField;
    qryProdutoAbaixoMinimoiLeadTime: TIntegerField;
    qryProdutoAbaixoMinimonQtdeEmPedido: TBCDField;
    dsProdutoAbaixoMinimo: TDataSource;
    cxGridDBTableView12nCdProduto: TcxGridDBColumn;
    cxGridDBTableView12cNmProduto: TcxGridDBColumn;
    cxGridDBTableView12cReferencia: TcxGridDBColumn;
    cxGridDBTableView12cUnidadeMedida: TcxGridDBColumn;
    cxGridDBTableView12cNmGrupoProduto: TcxGridDBColumn;
    cxGridDBTableView12nQtdeEstoque: TcxGridDBColumn;
    cxGridDBTableView12nEstoqueMinimo: TcxGridDBColumn;
    cxGridDBTableView12iLeadTime: TcxGridDBColumn;
    cxGridDBTableView12nQtdeEmPedido: TcxGridDBColumn;
    qryProdutoAbaixoRessuprimento: TADOQuery;
    IntegerField11: TIntegerField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    BCDField7: TBCDField;
    BCDField8: TBCDField;
    IntegerField12: TIntegerField;
    BCDField9: TBCDField;
    dsProdutoAbaixoRessuprimento: TDataSource;
    cxGrid14: TcxGrid;
    cxGridDBTableView13: TcxGridDBTableView;
    cxGridDBColumn58: TcxGridDBColumn;
    cxGridDBColumn59: TcxGridDBColumn;
    cxGridDBColumn60: TcxGridDBColumn;
    cxGridDBColumn61: TcxGridDBColumn;
    cxGridDBColumn62: TcxGridDBColumn;
    cxGridDBColumn63: TcxGridDBColumn;
    cxGridDBColumn64: TcxGridDBColumn;
    cxGridDBColumn65: TcxGridDBColumn;
    cxGridDBColumn66: TcxGridDBColumn;
    cxGridLevel13: TcxGridLevel;
    qryTotalEstoqueGrupo: TADOQuery;
    qryTotalEstoqueGrupocNmGrupoProduto: TStringField;
    qryTotalEstoqueGruponValTotalCusto: TBCDField;
    dsTotalEstoqueGrupo: TDataSource;
    DBChart2: TDBChart;
    BarSeries3: TPieSeries;
    GroupBox6: TGroupBox;
    btAtualizaEstoque: TcxButton;
    cxTabProducao: TcxTabSheet;
    GroupBox7: TGroupBox;
    cxButton1: TcxButton;
    cxPageControl16: TcxPageControl;
    cxTabSheet19: TcxTabSheet;
    cxTabSheet20: TcxTabSheet;
    cxTabSheet21: TcxTabSheet;
    qryOPAberta: TADOQuery;
    IntegerField13: TIntegerField;
    StringField36: TStringField;
    StringField37: TStringField;
    DateTimeField6: TDateTimeField;
    DateTimeField7: TDateTimeField;
    DateTimeField8: TDateTimeField;
    StringField38: TStringField;
    IntegerField14: TIntegerField;
    StringField39: TStringField;
    StringField40: TStringField;
    BCDField10: TBCDField;
    IntegerField15: TIntegerField;
    StringField41: TStringField;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    IntegerField18: TIntegerField;
    BCDField11: TBCDField;
    StringField42: TStringField;
    IntegerField19: TIntegerField;
    qryOPAbertacNmTabTipoStatusOP: TStringField;
    dsOPAberta: TDataSource;
    cxGrid15: TcxGrid;
    cxGridDBTableView14: TcxGridDBTableView;
    cxGridLevel14: TcxGridLevel;
    cxGridDBTableView14nCdOrdemProducao: TcxGridDBColumn;
    cxGridDBTableView14cNumeroOP: TcxGridDBColumn;
    cxGridDBTableView14cNmTabTipoOrigemOP: TcxGridDBColumn;
    cxGridDBTableView14dDtAbertura: TcxGridDBColumn;
    cxGridDBTableView14dDtInicioPrevisto: TcxGridDBColumn;
    cxGridDBTableView14dDtPrevConclusao: TcxGridDBColumn;
    cxGridDBTableView14cNmTipoOP: TcxGridDBColumn;
    cxGridDBTableView14nCdProduto: TcxGridDBColumn;
    cxGridDBTableView14cReferencia: TcxGridDBColumn;
    cxGridDBTableView14cNmProduto: TcxGridDBColumn;
    cxGridDBTableView14nQtdePlanejada: TcxGridDBColumn;
    cxGridDBTableView14nCdPedido: TcxGridDBColumn;
    cxGridDBTableView14cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView14nQtdeCancelada: TcxGridDBColumn;
    cxGridDBTableView14nQtdeRetrabalho: TcxGridDBColumn;
    cxGridDBTableView14nQtdeRefugo: TcxGridDBColumn;
    cxGridDBTableView14nSaldoProduzir: TcxGridDBColumn;
    cxGridDBTableView14cFlgAtraso: TcxGridDBColumn;
    cxGridDBTableView14nQtdeConcluida: TcxGridDBColumn;
    cxGridDBTableView14cNmTabTipoStatusOP: TcxGridDBColumn;
    qryOPAguardaProduto: TADOQuery;
    IntegerField20: TIntegerField;
    StringField43: TStringField;
    StringField44: TStringField;
    DateTimeField9: TDateTimeField;
    DateTimeField10: TDateTimeField;
    DateTimeField11: TDateTimeField;
    StringField45: TStringField;
    IntegerField21: TIntegerField;
    StringField46: TStringField;
    StringField47: TStringField;
    BCDField12: TBCDField;
    IntegerField22: TIntegerField;
    StringField48: TStringField;
    IntegerField23: TIntegerField;
    IntegerField24: TIntegerField;
    IntegerField25: TIntegerField;
    BCDField13: TBCDField;
    StringField49: TStringField;
    IntegerField26: TIntegerField;
    StringField50: TStringField;
    dsOPAguardaProduto: TDataSource;
    cxGrid16: TcxGrid;
    cxGridDBTableView15: TcxGridDBTableView;
    cxGridDBColumn67: TcxGridDBColumn;
    cxGridDBColumn68: TcxGridDBColumn;
    cxGridDBColumn69: TcxGridDBColumn;
    cxGridDBColumn70: TcxGridDBColumn;
    cxGridDBColumn71: TcxGridDBColumn;
    cxGridDBColumn72: TcxGridDBColumn;
    cxGridDBColumn73: TcxGridDBColumn;
    cxGridDBColumn74: TcxGridDBColumn;
    cxGridDBColumn75: TcxGridDBColumn;
    cxGridDBColumn76: TcxGridDBColumn;
    cxGridDBColumn77: TcxGridDBColumn;
    cxGridDBColumn78: TcxGridDBColumn;
    cxGridDBColumn79: TcxGridDBColumn;
    cxGridDBColumn80: TcxGridDBColumn;
    cxGridDBColumn81: TcxGridDBColumn;
    cxGridDBColumn82: TcxGridDBColumn;
    cxGridDBColumn83: TcxGridDBColumn;
    cxGridDBColumn84: TcxGridDBColumn;
    cxGridDBColumn85: TcxGridDBColumn;
    cxGridDBColumn86: TcxGridDBColumn;
    cxGridLevel15: TcxGridLevel;
    qryOPAguardaServico: TADOQuery;
    IntegerField27: TIntegerField;
    StringField51: TStringField;
    StringField52: TStringField;
    DateTimeField12: TDateTimeField;
    DateTimeField13: TDateTimeField;
    DateTimeField14: TDateTimeField;
    StringField53: TStringField;
    IntegerField28: TIntegerField;
    StringField54: TStringField;
    StringField55: TStringField;
    BCDField14: TBCDField;
    IntegerField29: TIntegerField;
    StringField56: TStringField;
    IntegerField30: TIntegerField;
    IntegerField31: TIntegerField;
    IntegerField32: TIntegerField;
    BCDField15: TBCDField;
    StringField57: TStringField;
    IntegerField33: TIntegerField;
    StringField58: TStringField;
    dsOPAguardaServico: TDataSource;
    cxGrid17: TcxGrid;
    cxGridDBTableView16: TcxGridDBTableView;
    cxGridDBColumn87: TcxGridDBColumn;
    cxGridDBColumn88: TcxGridDBColumn;
    cxGridDBColumn89: TcxGridDBColumn;
    cxGridDBColumn90: TcxGridDBColumn;
    cxGridDBColumn91: TcxGridDBColumn;
    cxGridDBColumn92: TcxGridDBColumn;
    cxGridDBColumn93: TcxGridDBColumn;
    cxGridDBColumn94: TcxGridDBColumn;
    cxGridDBColumn95: TcxGridDBColumn;
    cxGridDBColumn96: TcxGridDBColumn;
    cxGridDBColumn97: TcxGridDBColumn;
    cxGridDBColumn98: TcxGridDBColumn;
    cxGridDBColumn99: TcxGridDBColumn;
    cxGridDBColumn100: TcxGridDBColumn;
    cxGridDBColumn101: TcxGridDBColumn;
    cxGridDBColumn102: TcxGridDBColumn;
    cxGridDBColumn103: TcxGridDBColumn;
    cxGridDBColumn104: TcxGridDBColumn;
    cxGridDBColumn105: TcxGridDBColumn;
    cxGridDBColumn106: TcxGridDBColumn;
    cxGridLevel16: TcxGridLevel;
    cxTabCompras: TcxTabSheet;
    GroupBox8: TGroupBox;
    cxButton2: TcxButton;
    cxPageControl17: TcxPageControl;
    cxTabSheet22: TcxTabSheet;
    cxTabSheet23: TcxTabSheet;
    cxTabSheet24: TcxTabSheet;
    qryPDCAguardAutoriz: TADOQuery;
    qryPDCAguardAutoriznCdPedido: TIntegerField;
    qryPDCAguardAutorizdDtPedido: TDateTimeField;
    qryPDCAguardAutorizcNmTipoPedido: TStringField;
    qryPDCAguardAutorizcNmTerceiro: TStringField;
    qryPDCAguardAutoriznValPedido: TBCDField;
    dsPDCAguardAutoriz: TDataSource;
    cxGrid18: TcxGrid;
    cxGridDBTableView17: TcxGridDBTableView;
    cxGridLevel17: TcxGridLevel;
    cxGridDBTableView17nCdPedido: TcxGridDBColumn;
    cxGridDBTableView17dDtPedido: TcxGridDBColumn;
    cxGridDBTableView17cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView17cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView17nValPedido: TcxGridDBColumn;
    qryPDCAtraso: TADOQuery;
    dsPDCAtraso: TDataSource;
    qryPDCAtrasodDtEntregaFim: TDateTimeField;
    qryPDCAtrasonCdPedido: TIntegerField;
    qryPDCAtrasodDtPedido: TDateTimeField;
    qryPDCAtrasocNmTipoPedido: TStringField;
    qryPDCAtrasocNmTerceiro: TStringField;
    qryPDCAtrasocCdProduto: TStringField;
    qryPDCAtrasocNmItem: TStringField;
    qryPDCAtrasonSaldoReceber: TBCDField;
    qryPDCAtrasonValTotalItem: TBCDField;
    cxGrid19: TcxGrid;
    cxGridDBTableView18: TcxGridDBTableView;
    cxGridDBColumn107: TcxGridDBColumn;
    cxGridDBColumn108: TcxGridDBColumn;
    cxGridDBColumn109: TcxGridDBColumn;
    cxGridDBColumn110: TcxGridDBColumn;
    cxGridDBColumn111: TcxGridDBColumn;
    cxGridLevel18: TcxGridLevel;
    cxGridDBTableView18dDtEntregaFim: TcxGridDBColumn;
    cxGridDBTableView18cCdProduto: TcxGridDBColumn;
    cxGridDBTableView18cNmItem: TcxGridDBColumn;
    cxGridDBTableView18nSaldoReceber: TcxGridDBColumn;
    qryPDCProx15D: TADOQuery;
    DateTimeField15: TDateTimeField;
    IntegerField34: TIntegerField;
    DateTimeField16: TDateTimeField;
    StringField59: TStringField;
    StringField60: TStringField;
    StringField62: TStringField;
    BCDField16: TBCDField;
    BCDField17: TBCDField;
    dsPDCProx15D: TDataSource;
    qryPDCProx15DnCdProduto: TIntegerField;
    cxGrid20: TcxGrid;
    cxGridDBTableView19: TcxGridDBTableView;
    cxGridDBColumn112: TcxGridDBColumn;
    cxGridDBColumn113: TcxGridDBColumn;
    cxGridDBColumn114: TcxGridDBColumn;
    cxGridDBColumn115: TcxGridDBColumn;
    cxGridDBColumn116: TcxGridDBColumn;
    cxGridDBColumn117: TcxGridDBColumn;
    cxGridDBColumn118: TcxGridDBColumn;
    cxGridDBColumn119: TcxGridDBColumn;
    cxGridDBColumn120: TcxGridDBColumn;
    cxGridLevel19: TcxGridLevel;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure btAtualizarTop10Click(Sender: TObject);
    procedure btAtualizarFinanceiroClick(Sender: TObject);
    procedure btFluxoCaixaClick(Sender: TObject);
    procedure btOrcamentoClick(Sender: TObject);
    procedure btDREClick(Sender: TObject);
    procedure btAtualizarMetaVendaClick(Sender: TObject);
    procedure btAtualizaEstoqueClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure exibeOP( nCdOrdemProducao : integer ) ;
    procedure cxGridDBTableView14DblClick(Sender: TObject);
    procedure cxGridDBTableView15DblClick(Sender: TObject);
    procedure cxGridDBTableView16DblClick(Sender: TObject);
    procedure exibePedidoCompra( nCdPedido : integer );
    procedure cxGridDBTableView17DblClick(Sender: TObject);
    procedure cxGridDBTableView18DblClick(Sender: TObject);
    procedure cxGridDBTableView19DblClick(Sender: TObject);
    procedure exibeCadastroProduto( nCdProduto : integer ) ;
    procedure cxGridDBTableView12DblClick(Sender: TObject);
    procedure cxGridDBTableView13DblClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    function UltimoDiaMes(dUltimoDiaMes: TDateTime): TDateTime; 
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDashBoardERP: TfrmDashBoardERP;

implementation

{$R *.dfm}

uses fMenu, rFluxoCaixaNovo, rOrcamentoPrevReal, rDRE, fSeparaEmbalaPedido, fOP, fPedidoComercial, fProdutoERP, dcVendaProdutoCliente_view ;

procedure TfrmDashBoardERP.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  Top10dDtFinal.Date        := Date + 30 ;
  Top10dDtInicial.Date      := Date - 30 ;

  FinanceirodDtFinal.Date   := Date + 30 ;
  FinanceirodDtInicial.Date := Date - 30 ;

  btAtualizarFinanceiro.Click;

end;

procedure TfrmDashBoardERP.btAtualizarTop10Click(Sender: TObject);
begin
  inherited;

  qryTop10Clientes.Close;
  qryTop10Clientes.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTop10Clientes.Parameters.ParamByName('dDtInicial').Value := DateToStr( Top10dDtInicial.Date ) ;
  qryTop10Clientes.Parameters.ParamByName('dDtFinal').Value   := DateToStr( Top10dDtFinal.Date ) ;
  qryTop10Clientes.Open;

  qryTop10Fornecedores.Close;
  qryTop10Fornecedores.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTop10Fornecedores.Parameters.ParamByName('dDtInicial').Value := DateToStr( Top10dDtInicial.Date ) ;
  qryTop10Fornecedores.Parameters.ParamByName('dDtFinal').Value   := DateToStr( Top10dDtFinal.Date ) ;
  qryTop10Fornecedores.Open;

  qryTop10Produtos.Close;
  qryTop10Produtos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTop10Produtos.Parameters.ParamByName('dDtInicial').Value := DateToStr( Top10dDtInicial.Date ) ;
  qryTop10Produtos.Parameters.ParamByName('dDtFinal').Value   := DateToStr( Top10dDtFinal.Date ) ;
  qryTop10Produtos.Open;

  qryTop10Representantes.Close;
  qryTop10Representantes.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTop10Representantes.Parameters.ParamByName('dDtInicial').Value := DateToStr( Top10dDtInicial.Date ) ;
  qryTop10Representantes.Parameters.ParamByName('dDtFinal').Value   := DateToStr( Top10dDtFinal.Date ) ;
  qryTop10Representantes.Open;

end;

procedure TfrmDashBoardERP.btAtualizarFinanceiroClick(Sender: TObject);
begin
  inherited;

  qryDispCaixa.Close;
  qryDispCaixa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryDispCaixa.Open;

  qryPagarHoje.Close;
  qryPagarHoje.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPagarHoje.Open;

  qryPagarAtraso.Close;
  qryPagarAtraso.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPagarAtraso.Open;

  qryPagarPeriodo.Close;
  qryPagarPeriodo.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPagarPeriodo.Parameters.ParamByName('dDtInicial').Value := DateToStr( FinanceirodDtInicial.Date ) ;
  qryPagarPeriodo.Parameters.ParamByName('dDtFinal').Value   := DateToStr( FinanceirodDtFinal.Date ) ;
  qryPagarPeriodo.Open;

  qryReceberHoje.Close;
  qryReceberHoje.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryReceberHoje.Open;

  qryReceberAtraso.Close;
  qryReceberAtraso.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryReceberAtraso.Open;

  qryReceberPeriodo.Close;
  qryReceberPeriodo.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryReceberPeriodo.Parameters.ParamByName('dDtInicial').Value := DateToStr( FinanceirodDtInicial.Date ) ;
  qryReceberPeriodo.Parameters.ParamByName('dDtFinal').Value   := DateToStr( FinanceirodDtFinal.Date ) ;
  qryReceberPeriodo.Open;

end;

procedure TfrmDashBoardERP.btFluxoCaixaClick(Sender: TObject);
var
  objRel : TrptFluxoCaixaNovo ;
begin
  inherited;

  objRel := TrptFluxoCaixaNovo.Create( Self ) ;
  objRel.WindowState := wsMaximized ;

  showForm( objRel , TRUE ) ;

end;

procedure TfrmDashBoardERP.btOrcamentoClick(Sender: TObject);
var
  objRel : TrptOrcamentoPrevReal ;
begin
  inherited;

  objRel := TrptOrcamentoPrevReal.Create( Self ) ;
  objRel.WindowState := wsMaximized ;

  showForm( objRel , TRUE ) ;

end;

procedure TfrmDashBoardERP.btDREClick(Sender: TObject);
var
  objRel : TrptDRE ;
begin
  inherited;

  objRel := TrptDRE.Create( Self ) ;
  objRel.WindowState := wsMaximized ;

  showForm( objRel , TRUE ) ;

end;

procedure TfrmDashBoardERP.btAtualizarMetaVendaClick(Sender: TObject);
var
    dAux : TDateTime ;
begin
  inherited;

  if (Length(Trim(edtMesAnoMeta.Text)) = 1) then
  begin
      edtMesAnoMeta.Text := Copy( DateToStr( Date ),4,2 ) + '/' + Copy( DateToStr( Date ),7,4 ) ;
  end ;

  if (Length(Trim(edtMesAnoMeta.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inv�lida.') ;
      edtMesAnoMeta.SetFocus;
      abort ;
  end ;

  try
      dAux := StrToDateTime('01/' + edtMesAnoMeta.Text) ;
  except
      ShowMessage('Compet�ncia Inv�lida.') ;
      edtMesAnoMeta.SetFocus;
      abort ;
  end ;

  qryPreparaTempMeta.ExecSQL ;

  qryTempVenda.Close;
  qryTempVendaDia.Close;

  SPREL_VENDA_DIARIA.Close;
  SPREL_VENDA_DIARIA.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
  SPREL_VENDA_DIARIA.Parameters.ParamByName('@cCompetencia').Value := edtMesAnoMeta.Text ;
  SPREL_VENDA_DIARIA.ExecProc;

  qryTempVenda.Open;
  qryTempVendaDia.Open;

  qrySomaVendaDia.Close;
  qrySomaVendaDia.Open;

  DBChart1.Refresh;


end;

procedure TfrmDashBoardERP.btAtualizaEstoqueClick(Sender: TObject);
begin
  inherited;

  qryProdutoAbaixoMinimo.Close;
  qryProdutoAbaixoMinimo.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProdutoAbaixoMinimo.Open;

  qryProdutoAbaixoRessuprimento.Close;
  qryProdutoAbaixoRessuprimento.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProdutoAbaixoRessuprimento.Open;

  qryTotalEstoqueGrupo.Close;
  qryTotalEstoqueGrupo.Open;

end;

procedure TfrmDashBoardERP.cxButton1Click(Sender: TObject);
begin
  inherited;

  qryOPAberta.Close;
  PosicionaQuery( qryOPAberta , intToStr(frmMenu.nCdEmpresaAtiva) ) ;

  qryOPAguardaProduto.Close;
  PosicionaQuery( qryOPAguardaProduto , intToStr(frmMenu.nCdEmpresaAtiva) ) ;

  qryOPAguardaServico.Close;
  PosicionaQuery( qryOPAguardaServico , intToStr(frmMenu.nCdEmpresaAtiva) ) ;

end;

procedure TfrmDashBoardERP.cxButton2Click(Sender: TObject);
begin
  inherited;

  qryPDCAguardAutoriz.Close;
  qryPDCAguardAutoriz.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPDCAguardAutoriz.Open;

  qryPDCAtraso.Close;
  qryPDCAtraso.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPDCAtraso.Open;

  qryPDCProx15D.Close;
  qryPDCProx15D.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryPDCProx15D.Open;

end;

procedure TfrmDashBoardERP.cxButton3Click(Sender: TObject);
var
    objForm : TfrmSeparaEmbalaPedido ;
begin
  inherited;

  objForm := TfrmSeparaEmbalaPedido.Create( Self ) ;

  objForm.WindowState := wsMaximized ;

  showForm( objForm, TRUE ) ;

end;

procedure TfrmDashBoardERP.exibeOP(nCdOrdemProducao: integer);
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( nCdOrdemProducao );
  objForm.WindowState := wsMaximized ;

  showForm( objForm , True ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView14DblClick(Sender: TObject);
begin
  inherited;

  if qryOPAberta.Active and (qryOPAberta.RecordCount > 0) then
      exibeOP( IntegerField13.Value ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView15DblClick(Sender: TObject);
begin
  inherited;

  if qryOPAguardaProduto.Active and (qryOPAguardaProduto.RecordCount > 0) then
      exibeOP( IntegerField20.Value ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView16DblClick(Sender: TObject);
begin
  inherited;

  if qryOPAguardaServico.Active and (qryOPAguardaServico.RecordCount > 0) then
      exibeOP( IntegerField27.Value ) ;

end;

procedure TfrmDashBoardERP.exibePedidoCompra(nCdPedido: integer);
var
  objForm : TfrmPedidoComercial ;
begin
  inherited;

  objForm := TfrmPedidoComercial.Create( Self ) ;

  objForm.PosicionaPK( nCdPedido );
  objForm.WindowState := wsMaximized ;

  showForm( objForm , True ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView17DblClick(Sender: TObject);
begin
  inherited;

  if qryPDCAguardAutoriz.Active and (qryPDCAguardAutoriz.RecordCount > 0) then
      exibePedidoCompra( qryPDCAguardAutoriznCdPedido.Value ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView18DblClick(Sender: TObject);
begin
  inherited;

  if qryPDCAtraso.Active and (qryPDCAtraso.RecordCount > 0) then
      exibePedidoCompra( qryPDCAtrasonCdPedido.Value ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView19DblClick(Sender: TObject);
begin
  inherited;

  if qryPDCProx15D.Active and (qryPDCProx15D.RecordCount > 0) then
      exibePedidoCompra( IntegerField34.Value ) ;

end;

procedure TfrmDashBoardERP.exibeCadastroProduto(nCdProduto: integer);
var
  objForm : TfrmProdutoERP ;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self ) ;

  objForm.PosicionaPK( nCdProduto );
  objForm.WindowState := wsMaximized ;

  showForm( objForm , True ) ;

end;

procedure TfrmDashBoardERP.cxGridDBTableView12DblClick(Sender: TObject);
begin
  inherited;

  if qryProdutoAbaixoMinimo.Active and (qryProdutoAbaixoMinimo.RecordCount > 0) then
      exibeCadastroProduto( qryProdutoAbaixoMinimonCdProduto.Value ) ;


end;

procedure TfrmDashBoardERP.cxGridDBTableView13DblClick(Sender: TObject);
begin
  inherited;

  if qryProdutoAbaixoRessuprimento.Active and (qryProdutoAbaixoRessuprimento.RecordCount > 0) then
      exibeCadastroProduto( IntegerField11.Value ) ;

end;

procedure TfrmDashBoardERP.cxButton4Click(Sender: TObject);
var
   objRel : TdcmVendaProdutoCliente_view;
   dDtInicial, dDtFinal : TDateTime ;
begin
  inherited;

  if (Length(Trim(edtMesAnoMeta.Text)) = 1) then
  begin
      edtMesAnoMeta.Text := Copy( DateToStr( Date ),4,2 ) + '/' + Copy( DateToStr( Date ),7,4 ) ;
  end ;

  objRel := TdcmVendaProdutoCliente_view.Create(nil);

  dDtInicial := strToDate( '01/' + edtMesAnoMeta.Text ) ;
  dDtFinal   := UltimoDiaMes( dDtInicial ) ;

  Try
      Try
         objRel.ADODataSet1.Close ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := 0;
         objRel.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := dDtInicial ;
         objRel.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := dDtFinal ;
         objRel.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := 0;
         objRel.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := 0;
         objRel.ADODataSet1.Open ;

         if (objRel.ADODataSet1.eof) then
         begin
             ShowMessage('Nenhuma informa��o encontrada.') ;
             exit ;
         end ;

         frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

         if objRel.PivotCube1.Active then
         begin
             objRel.PivotCube1.Active := False;
         end;

         objRel.PivotCube1.ExtendedMode := True;
         objRel.PVMeasureToolBar1.HideButtons := False;
         objRel.PivotCube1.Active := True;

         objRel.PivotMap1.Measures[2].ColumnPercent := True;
         objRel.PivotMap1.Measures[2].Value := False;

         objRel.PivotMap1.Measures[3].Rank := True ;
         objRel.PivotMap1.Measures[3].Value := False;

         objRel.PivotMap1.SortColumn(0,3,False) ;

         objRel.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
         objRel.PivotGrid1.RefreshData;

         frmMenu.StatusBar1.Panels[6].Text := '' ;

         objRel.ShowModal ;
         Self.Activate ;
      except
          MensagemErro('Erro ao gerar Data Analys');
          raise;
      end;
  Finally
      FreeAndNil(objRel);
  end;

end;

function TfrmDashBoardERP.UltimoDiaMes(
  dUltimoDiaMes: TDateTime): TDateTime;
(*
  Retorna o ultimo dia o mes, de uma data fornecida - Fun��o criada por Adriano   para fins de calculos de dias para seguro desemprego.
*)
var
  ano, mes, dia: word;
begin
  DecodeDate(dUltimoDiaMes, ano, mes, dia);
  mes := mes + 1;
  if mes = 13 then
  begin
    mes := 1;
    ano := ano + 1;
  end;
  Result := EncodeDate(ano, mes, 1) - 1;
end;

initialization
    RegisterClass( TfrmDashBoardERP ) ;

end.
