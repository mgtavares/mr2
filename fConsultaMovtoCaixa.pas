unit fConsultaMovtoCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBCtrls, StdCtrls, Mask, DBGridEhGrouping,
  ToolCtrlsEh, ER2Excel;

type
  TfrmConsultaMovtoCaixa = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit1: TMaskEdit;
    edtConta: TMaskEdit;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    DataSource2: TDataSource;
    RadioGroup2: TRadioGroup;
    qryContaBancariaDebcFlgCaixa: TIntegerField;
    qryContaBancariaDebcFlgCofre: TIntegerField;
    DBEdit1: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    Label4: TLabel;
    edtLoja: TMaskEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    DataSource3: TDataSource;
    qryResultadonCdLanctoFin: TAutoIncField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadonCdTipoLancto: TIntegerField;
    qryResultadocNmTipoLancto: TStringField;
    qryResultadocNmFormaPagto: TStringField;
    qryResultadonValCredito: TBCDField;
    qryResultadonValDebito: TBCDField;
    qryResultadonCdUsuario: TIntegerField;
    qryResultadocNmUsuario: TStringField;
    qryResultadocFlgManual: TStringField;
    qryResultadocHistorico: TStringField;
    qryResultadocFlgEntrada: TStringField;
    CheckBox1: TCheckBox;
    qryResultadodDtEstorno: TDateTimeField;
    ToolButton6: TToolButton;
    ER2Excel1: TER2Excel;
    ToolButton7: TToolButton;
    RadioGroup1: TRadioGroup;
    procedure edtContaExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaMovtoCaixa: TfrmConsultaMovtoCaixa;

implementation

uses fMenu, fLookup_Padrao, fConsultaVendaProduto_Dados,
  fConsultaMovtoCaixa_Parcelas;

{$R *.dfm}

procedure TfrmConsultaMovtoCaixa.edtContaExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtConta.Text) = '') then
      Exit;

  if ((Trim(edtConta.Text) <> '') and (Trim(DBEdit2.Text) = '')) then
  begin
      MensagemAlerta('Selecione a loja.') ;
      edtConta.Clear;
      qryContaBancariaDeb.Close;
      edtLoja.SetFocus ;
      exit ;
  end
  else
  begin
      qryContaBancariaDeb.Close ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := edtLoja.Text ;
      PosicionaQuery(qryContaBancariaDeb, edtConta.Text) ;
  end;
end;

procedure TfrmConsultaMovtoCaixa.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione a loja.') ;
      edtLoja.SetFocus ;
      exit ;
  end ;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione a conta.') ;
      edtConta.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit1.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit2.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdContaBancaria').Value      := frmMenu.ConvInteiro(edtConta.Text) ;
  qryResultado.Parameters.ParamByName('cDataInicial').Value          := frmMenu.ConvData(MaskEdit1.Text)    ;
  qryResultado.Parameters.ParamByName('cDataFinal').Value            := frmMenu.ConvData(MaskEdit2.Text)    ;
  qryResultado.Parameters.ParamByName('cFlgManual').Value            := 'N' ;
  qryResultado.Parameters.ParamByName('cMostrarEstornados').Value    := 'N' ;
  qryResultado.Parameters.ParamByName('cMostrarAberturaFecha').Value := 'N' ;

  if (RadioGroup2.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cFlgManual').Value := 'S' ;

  if (RadioGroup1.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cMostrarAberturaFecha').Value := 'S' ;

  if (CheckBox1.Checked) then
      qryResultado.Parameters.ParamByName('cMostrarEstornados').Value := 'S' ;

  qryResultado.Open ;

  if (qryResultado.eof) then
  begin
      MensagemAlerta('Nenhum lan�amento encontrado para o crit�rio selecionado.') ;
  end ;

end;

procedure TfrmConsultaMovtoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[5].Font.Color := clBlue ;
  DBGridEh1.Columns[6].Font.Color := clRed ;

end;

procedure TfrmConsultaMovtoCaixa.edtContaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            if (DBEdit2.Text = '') then
            begin
                MensagemAlerta('Selecione a loja.') ;
                edtLoja.SetFocus;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.cFlgCofre = 0 AND ContaBancaria.cFlgCaixa = 1 AND ContaBancaria.nCdLoja = ' + edtLoja.Text);

            If (nPK > 0) then
            begin
                edtConta.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaMovtoCaixa.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Consulta Movimento Caixa');
end;

procedure TfrmConsultaMovtoCaixa.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TfrmConsultaMovtoCaixa.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(147,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja = Loja.nCdLoja AND UL.nCdUsuario = @@Usuario)');

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaMovtoCaixa.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if ((qryResultadocNmTipoLancto.Value = 'SALDO ANTERIOR') or (qryResultadocNmTipoLancto.Value = 'SALDO FINAL')) and (DataCol = 3) then
  begin

     DBGridEh1.Canvas.Brush.Color := clBlue;
     DBGridEh1.Canvas.Font.Color  := clWhite ;

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

  if (qryResultadocNmTipoLancto.Value = 'RESUMO MOVIMENTA��O') and (DataCol = 3) then
  begin

     DBGridEh1.Canvas.Brush.Color := clBlue;
     DBGridEh1.Canvas.Font.Color  := clWhite ;

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

procedure TfrmConsultaMovtoCaixa.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados;
  objFormParcelas : TfrmConsultaMovtoCaixa_Parcelas;
begin
  inherited;

  if (qryResultadonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCVDCX'))) then
  begin
      objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

      PosicionaQuery(objForm.qryItemPedido, qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCondicao  , qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryPrestacoes, qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCheques   , qryResultadonCdLanctoFin.AsString) ;
      showForm(objForm,true);

  end ;

  if (qryResultadonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCRPCX'))) then
  begin
      objFormParcelas := TfrmConsultaMovtoCaixa_Parcelas.Create(nil);

      PosicionaQuery(objFormParcelas.qryParcelas   , qryResultadonCdLanctoFin.AsString) ;
      showForm(objFormParcelas,true);

  end ;


end;

procedure TfrmConsultaMovtoCaixa.ToolButton6Click(Sender: TObject);
var
  iLinha   : integer ;
begin
  if (qryResultado.State = dsInactive) then
  begin
      MensagemAlerta('Por favor informe os par�metros e realize a consulta antes de exportar os dados.') ;
      edtLoja.SetFocus ;
      exit ;
  end;

  inherited;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  { -- formata t�tulo -- }
  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := 'Loja - Consulta de Movimentos do Caixa' ;
  ER2Excel1.Celula['A3'].Text := 'Loja: ' + qryLojacNmLoja.Value;
  ER2Excel1.Celula['C3'].Text := 'Conta Caixa/Cofre: ' + qryContaBancariaDebnCdConta.Value ;
  ER2Excel1.Celula['D3'].Text := 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text;
  ER2Excel1.Celula['A5'].Text := 'Lancto' ;
  ER2Excel1.Celula['B5'].Text := 'Data Lancto' ;
  ER2Excel1.Celula['C5'].Text := 'Tipo Lancto' ;
  ER2Excel1.Celula['D5'].Text := 'Forma Pagto' ;
  ER2Excel1.Celula['E5'].Text := 'Valor Cr�dito' ;
  ER2Excel1.Celula['F5'].Text := 'Valor D�bito' ;
  ER2Excel1.Celula['G5'].Text := 'Usr Respons�vel' ;
  ER2Excel1.Celula['H5'].Text := 'Manual' ;
  ER2Excel1.Celula['I5'].Text := 'Hist�rico' ;
  ER2Excel1.Celula['J5'].Text := 'Entrada Credi�rio' ;
  ER2Excel1.Celula['K5'].Text := 'Dt Estorno' ;    

  ER2Excel1.Celula['A1'].Background := RGB(221,221,221);
  ER2Excel1.Celula['A1'].Range('K1');
  ER2Excel1.Celula['A2'].Background := RGB(221,221,221);
  ER2Excel1.Celula['A2'].Range('K2');
  ER2Excel1.Celula['A3'].Background := RGB(221,221,221);
  ER2Excel1.Celula['A3'].Range('K3');
  ER2Excel1.Celula['A4'].Background := RGB(221,221,221);
  ER2Excel1.Celula['A4'].Range('K4');
  ER2Excel1.Celula['A5'].Background := RGB(221,221,221);
  ER2Excel1.Celula['A5'].Range('K5');

  ER2Excel1.Celula['E5'].HorizontalAlign := haRight ;
  ER2Excel1.Celula['F5'].HorizontalAlign := haRight ;

  ER2Excel1.Celula['A1'].Congelar('A6');

  iLinha := 7 ; {-- Linha Inicial --}

  qryResultado.DisableControls;
  qryResultado.First;

  while not qryResultado.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := qryResultadonCdLanctoFin.AsString ;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := qryResultadodDtLancto.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := qryResultadocNmTipoLancto.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := qryResultadocNmFormaPagto.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := qryResultadonValCredito.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := qryResultadonValDebito.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := qryResultadocNmUsuario.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := qryResultadocFlgManual.Value;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Text := qryResultadocHistorico.Value;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := qryResultadocFlgEntrada.Value;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].Text := qryResultadodDtEstorno.AsString;

      ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara := '#.##0,00' ;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara := '#.##0,00' ;

      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight ;

      inc( iLinha ) ;

      qryResultado.Next;
  end ;

  qryResultado.First;
  qryResultado.EnableControls;

  ER2Excel1.Celula['A6'].Width := 7;
  ER2Excel1.Celula['B6'].Width := 15;
  ER2Excel1.Celula['C6'].Width := 40;
  ER2Excel1.Celula['D6'].Width := 20;
  ER2Excel1.Celula['E6'].Width := 17;
  ER2Excel1.Celula['F6'].Width := 17;
  ER2Excel1.Celula['G6'].Width := 30;
  ER2Excel1.Celula['H6'].Width := 7;
  ER2Excel1.Celula['I6'].Width := 30;
  ER2Excel1.Celula['J6'].Width := 13;
  ER2Excel1.Celula['K6'].Width := 15;

  ER2Excel1.Celula['A4'].Height := 5;
  ER2Excel1.Celula['A5'].Height := 10;
  ER2Excel1.Celula['A6'].Height := 5;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;

  frmMenu.mensagemUsuario('');


end;

initialization
    RegisterClass(TfrmConsultaMovtoCaixa) ;

end.
