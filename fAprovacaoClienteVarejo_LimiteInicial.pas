unit fAprovacaoClienteVarejo_LimiteInicial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit,
  cxLookAndFeelPainters, cxButtons, DB, ADODB;

type
  TfrmAprovacaoClienteVarejo_LimiteInicial = class(TfrmProcesso_Padrao)
    edtRendaTrabalho: TcxCurrencyEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtRendaAddComprovada: TcxCurrencyEdit;
    edtRendaAddDeclarada: TcxCurrencyEdit;
    Label3: TLabel;
    edtRendaCjg: TcxCurrencyEdit;
    Label4: TLabel;
    edtLimiteCredito: TcxCurrencyEdit;
    Label5: TLabel;
    edtPercEntrada: TcxCurrencyEdit;
    Label6: TLabel;
    btCalcular: TcxButton;
    SP_GERA_LIMITE_CREDITO: TADOStoredProc;
    qryAux: TADOQuery;
    SP_LIMITE_CREDITO_INICIAL: TADOStoredProc;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceironValRendaBruta: TBCDField;
    qryTerceironValRendaBrutaCjg: TBCDField;
    qryTerceironValRendaComprovada: TBCDField;
    qryTerceironValRendaDeclarada: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLimiteCreditoKeyPress(Sender: TObject; var Key: Char);
    procedure edtPercEntradaKeyPress(Sender: TObject; var Key: Char);
    procedure edtPercEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLimiteCreditoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCalcularClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro   : integer ;
    cOBS          : string ;
    bAprovacao    : boolean ;
    bLimiteManual : boolean ;
  end;

var
  frmAprovacaoClienteVarejo_LimiteInicial: TfrmAprovacaoClienteVarejo_LimiteInicial;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.FormShow(
  Sender: TObject);
begin
  inherited;

  {-- Le o parametro que indica o nome da procedure --}
  SP_LIMITE_CREDITO_INICIAL.ProcedureName := Trim(frmMenu.LeParametroEmpresa('proc:SP_LIMITE_CREDITO_INICIAL')) + ';1' ;

  {-- Se a variavel bLimiteManual for true, independente do valor do parametro ALTLIMCADCLI, o limite pode ser informado manualmente --}
  
  if not bLimiteManual then
  begin
      if (frmMenu.LeParametro('ALTLIMCADCLI') = 'N') then
          edtLimiteCredito.Enabled := false ;
  end
  else if bLimiteManual then
      edtLimiteCredito.Enabled := True ;

  if (edtLimiteCredito.Enabled) then
      edtLimiteCredito.SetFocus
  else edtPercEntrada.setFocus;

  PosicionaQuery(qryTerceiro, intToStr(nCdTerceiro)) ;

  if not qryTerceiro.eof then
  begin
      edtRendaTrabalho.Value      := qryTerceironValRendaBruta.Value ;
      edtRendaCjg.Value           := qryTerceironValRendaBrutaCjg.Value ;
      edtRendaAddComprovada.Value := qryTerceironValRendaComprovada.Value ;
      edtRendaAddDeclarada.Value  := qryTerceironValRendaDeclarada.Value ;
  end ;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  edtLimiteCredito.PostEditValue;
  edtPercEntrada.PostEditValue;

  if (edtLimiteCredito.Value < 0) then
  begin
      MensagemErro('Limite de cr�dito inv�lido.') ;
      edtLimiteCredito.Value := 0 ;
      exit ;
  end ;

  if (edtPercEntrada.Value < 0) or (edtPercEntrada.Value > 100) then
  begin
      MensagemErro('Percentual de entrada inv�lido.') ;
      edtPercEntrada.Value := 0 ;
      exit ;
  end ;

  if (MessageDlg('Confirma a concess�o deste limite de cr�dito ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (edtLimiteCredito.Value = 0) then
      if (MessageDlg('Voc� n�o informou nenhum valor de limite. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans;

  try
  
      SP_GERA_LIMITE_CREDITO.Close ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nCdTerceiro').Value    := nCdTerceiro ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nValLimite').Value     := edtLimiteCredito.Value ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nPercEntrada').Value   := edtPercEntrada.Value ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cFlgAutomatico').Value := 0 ;

      if (not edtLimiteCredito.Enabled) then
          SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cFlgAutomatico').Value := 1 ;

      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cOBS').Value           := cOBS ;
      SP_GERA_LIMITE_CREDITO.ExecProc ;

      if (bAprovacao) then
      begin
          qryAux.Close ;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE PessoaFisica SET cOBSSituacaoCadastro = NULL, nCdUsuarioAprova = ' + intToStr(frmMenu.nCdUsuarioLogado)+ ', dDtusuarioAprova = GetDate() ,nCdTabTipoSituacao = 2 WHERE nCdTerceiro = ' + intToStr(nCdTerceiro)) ;
          qryAux.ExecSQL;
      end ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  close ;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.edtLimiteCreditoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if (Key = #13) then
      edtPercEntrada.SetFocus;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.edtPercEntradaKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if (Key = #13) then
      Toolbutton1.Click;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.edtPercEntradaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F5) then
      btCalcular.Click;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.edtLimiteCreditoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F5) then
      btCalcular.Click;

end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.btCalcularClick(
  Sender: TObject);
begin
  inherited;

  try
      SP_LIMITE_CREDITO_INICIAL.Close ;
      SP_LIMITE_CREDITO_INICIAL.Parameters.ParamByName('@nCdTerceiro').Value   := nCdTerceiro                 ;
      SP_LIMITE_CREDITO_INICIAL.Parameters.ParamByName('@nValRenda').Value     := edtRendaTrabalho.Value      ;
      SP_LIMITE_CREDITO_INICIAL.Parameters.ParamByName('@nValRendaCjg').Value  := edtRendaCjg.Value   ;
      SP_LIMITE_CREDITO_INICIAL.Parameters.ParamByName('@nValRendaComp').Value := edtRendaAddComprovada.Value ;
      SP_LIMITE_CREDITO_INICIAL.Parameters.ParamByName('@nValRendaDecl').Value := edtRendaAddDeclarada.Value  ;
      SP_LIMITE_CREDITO_INICIAL.ExecProc;
  except
      MensagemErro('Erro calculando o limite de cr�dito.') ;
      raise ;
  end ;

  edtLimiteCredito.Value := strToFloat(SP_LIMITE_CREDITO_INICIAL.Parameters.ParamValues['@nValLimiteInicial']) ;
  edtPercEntrada.SetFocus;
  
end;

procedure TfrmAprovacaoClienteVarejo_LimiteInicial.FormClose(
  Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  qryTerceiro.Close ;

end;

end.
