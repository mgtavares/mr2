unit fCadastro_Template;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, StdCtrls, Mask, ExtCtrls, DBCtrls, DB, ADODB,
  Grids, DBGrids, ToolWin, DBGridEh, cxLookAndFeelPainters,  cxButtons,
  cxGridDBTableView, cxGrid, cxGraphics, cxLookAndFeels, cxCurrencyEdit, cxPC,
  ER2Lookup;

type
  TfrmCadastro_Padrao = class(TForm)
    Image1: TImage;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
    ToolBar2: TToolBar;
    btIncluir: TToolButton;
    btSalvar: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    btAlterar: TToolButton;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    btCancelar: TToolButton;
    ToolButton11: TToolButton;
    btConsultar: TToolButton;
    qryID: TADOQuery;
    usp_ProximoID: TADOStoredProc;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton9: TToolButton;
    btnAuditoria: TToolButton;
    qryStat: TADOQuery;
    qryStatnCdStatus: TIntegerField;
    qryStatcNmStatus: TStringField;
    usp_ValidaPermissao: TADOStoredProc;
    ImageList1: TImageList;
    ToolButton50: TToolButton;
    btExcluir: TToolButton;
    procedure btnCloseClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryMasterAfterInsert(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure btAlterarClick(Sender: TObject);
    procedure dsMasterDataChange(Sender: TObject; Field: TField);
    procedure PosicionaPK(nCdPK : Integer) ;
    procedure btConsultarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure btnAuditoriaClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btExcluirClick(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure FormDeactivate(Sender: TObject);
    procedure TrataData(Sender: TField; const Text: string) ;
    function InputQuery(const ACaption, APrompt: string; var Value: string): Boolean;
    procedure NavegaEnter(Sender: TObject; var Key: Char);
    procedure showForm(objForm: TForm; bDestroirObjeto : boolean);
    procedure desativaDBEdit (obj : TDBEdit) ;
    procedure ativaDBEdit (obj : TDBEdit) ;
    procedure desativaMaskEdit (obj : TMaskEdit) ;
    procedure ativaMaskEdit (obj : TMaskEdit) ;
    procedure desativaER2LkpMaskEdit (obj : TER2LookupMaskEdit) ;
    procedure ativaER2LkpMaskEdit (obj : TER2LookupMaskEdit) ;
    function fnValidaEstado(cSiglaEstado : String) : Boolean;

  private
    { Private declarations }
    nCdPK :Integer;
    n : integer;
    nCdTipoAcao : integer ;

  public
    { Public declarations }
    cNmTabelaMaster         : String;
    nCdTabelaSistema        : Integer ;
    nCdConsultaPadrao       : Integer;
    cWhereAddConsultaPadrao : String ;
    bAlterarDados           : Boolean;
    bCodigoAutomatico       : Boolean ;
    bLimpaAposSalvar        : Boolean ;
    bAutoEdit               : Boolean ;
    bInclusao               : Boolean ;
  end;

var
  frmCadastro_Padrao: TfrmCadastro_Padrao;

implementation

uses fMenu, fLookup_Padrao, fMensagem;

function TfrmCadastro_Padrao.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free; 

end;

procedure TfrmCadastro_Padrao.ShowMessage(cTexto: string);
begin

    MessageDLG(cTexto,mtInformation,[mbOK],0) ;

    {frmMenu.nShowMessage(cTexto);}

end;

procedure TfrmCadastro_Padrao.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;

    {frmMenu.nMensagemErro(cTexto);}

end;

procedure TfrmCadastro_Padrao.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;

    {frmMenu.nMensagemAlerta(cTexto);}

end;

procedure TfrmCadastro_Padrao.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmCadastro_Padrao.btnCloseClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmCadastro_Padrao.btnNovoClick(Sender: TObject);
begin
    qryMaster.Close ;
    qryMaster.Open  ;
    qryMaster.Insert ;
end;

procedure TfrmCadastro_Padrao.btnGravarClick(Sender: TObject);
begin


    try
        qryMaster.Post;
    except
        raise ;
        exit ;
    end ;

    qryMaster.Insert ;

end;

procedure TfrmCadastro_Padrao.ToolButton2Click(Sender: TObject);
begin
    qryMaster.Close ;
    Close ;
end;

procedure TfrmCadastro_Padrao.btIncluirClick(Sender: TObject);
begin

    qryMaster.Close ;
    qryMaster.Parameters.ParamByName('nPK').Value := 0 ;
    qryMaster.Open  ;
    qryMaster.Insert ;

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btSalvar.Enabled   := True ;
    btCancelar.Enabled := True ;

end;

procedure TfrmCadastro_Padrao.btSalvarClick(Sender: TObject);
begin
    try
        qryMaster.Post;
    except
        raise ;
        exit ;
    end ;

    if (bLimpaAposSalvar) then
    begin
      qryMaster.Insert ;

      btIncluir.Enabled  := False ;
      btAlterar.Enabled  := False ;
      btSalvar.Enabled   := False ;
      btCancelar.Enabled := False ;
      btExcluir.Enabled  := False ;

      btIncluir.Enabled  := True ;
    end;

end;

procedure TfrmCadastro_Padrao.ToolButton5Click(Sender: TObject);
begin
    qryMaster.Close ;
    Close ;
end;

procedure TfrmCadastro_Padrao.qryMasterAfterCancel(DataSet: TDataSet);
begin

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btIncluir.Enabled := True ;

    if (qryMaster.RecNo <> -1) then
        btAlterar.Enabled := True ;

end;

procedure TfrmCadastro_Padrao.qryMasterAfterInsert(DataSet: TDataSet);
begin

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btSalvar.Enabled   := True ;
    btCancelar.Enabled := True ;

end;

procedure TfrmCadastro_Padrao.btCancelarClick(Sender: TObject);
begin
    qryMaster.Cancel ;
    qryMaster.Close ;

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btIncluir.Enabled   := True ;

end;

procedure TfrmCadastro_Padrao.qryMasterCalcFields(DataSet: TDataSet);
begin
    btAlterar.Enabled := True ;

end;


procedure TfrmCadastro_Padrao.btAlterarClick(Sender: TObject);
begin

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btSalvar.Enabled   := True ;
    btCancelar.Enabled := True ;
    btExcluir.Enabled  := True ;


    if (bAutoEdit) then
        qryMaster.Edit ;

end;

procedure TfrmCadastro_Padrao.dsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  btAlterar.Click ;

end;

procedure TfrmCadastro_Padrao.PosicionaPK(nCdPK : Integer) ;
begin
    qryMaster.Close ;
    qryMaster.DisableControls;

    qryMaster.Parameters.ParamByName('nPK').Value := nCdPK ;

    qryMaster.Open ;

    qryMaster.EnableControls ;

end ;


procedure TfrmCadastro_Padrao.btConsultarClick(Sender: TObject);
begin
  If (nCdConsultaPadrao = 0) then
  begin
      ShowMessage('Consulta padr�o n�o configurada, informe o ID da consulta na vari�vel nCdConsultaPadrao') ;
      exit ;
  end ;

  if (Trim(cWhereAddConsultaPadrao) = '') then
      nCdPK := frmLookup_Padrao.ExecutaConsulta(nCdConsultaPadrao)
  else nCdPK := frmLookup_Padrao.ExecutaConsulta2(nCdConsultaPadrao,cWhereAddConsultaPadrao) ;

  If (nCdPK > 0) then
  begin
      PosicionaPK(nCdPK) ;
  end ;

end;

procedure TfrmCadastro_Padrao.FormCreate(Sender: TObject);
var
    i : Integer;
    i2 : integer ;
    varTeste : Variant ;
begin

  frmCadastro_Padrao.KeyPreview := True ;
  Application.OnException       := frmMenu.TrataErros ;
  Screen.OnActiveControlChange  := frmMenu.ScreenActiveControlChange;

  bCodigoAutomatico := True ;
  bLimpaAposSalvar  := True ;
  bAutoEdit         := True ;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TcxPageControl) then
    begin

        (Components [I] as TcxPageControl).Font.Name  := 'Calibri' ;
        (Components [I] as TcxPageControl).Font.Size  := 9 ;
        (Components [I] as TcxPageControl).Font.Style := [] ;
        (Components [I] as TcxPageControl).LookAndFeel.NativeStyle := True ;

    end ;

    if (Components [I] is TGroupBox) then
    begin
       (Components [I] as TGroupBox).Font.Name    := 'Calibri' ;
       (Components [I] as TGroupBox).Font.Size    := 8 ;
       (Components [I] as TGroupBox).Font.Style   := [] ;
    end ;

    if (Components [I] is TCheckBox) then
    begin
       (Components [I] as TCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TCheckBox).Font.Size    := 8 ;
       (Components [I] as TCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDBCheckBox) then
    begin
       (Components [I] as TDBCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TDBCheckBox).Font.Size    := 8 ;
       (Components [I] as TDBCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDateTimeField) then
    begin
        (Components [I] as TDateTimeField).OnSetText := TrataData ;
    end ;

    if (Components [I] is TDBGridEh) and ((Components [I] as TDBGridEh).Tag < 2) then
    begin
      (Components [I] as TDBGridEh).Flat := True ;
      (Components [I] as TDBGridEh).FixedColor := clMoneyGreen ;
      //(Components [I] as TDBGridEh).OddRowColor := clAqua ;
      (Components [I] as TDBGridEh).UseMultiTitle := True ;
      (Components [I] as TDBGridEh).OptionsEh := [dghColumnResize,dghFixed3D,dghHighlightFocus,dghClearSelection,dghRowHighlight,dghDialogFind,dghEnterAsTab] ;
      //[dghFixed3D,dghHighlightFocus,dghClearSelection,dghEnterAsTab,dghDialogFind] ;
      (Components [I] as TDBGridEh).ParentFont := False ;

      if ((Components [I] as TDBGridEh).Tag = 1) then
      begin
          (Components [I] as TDBGridEh).ParentFont := False ;
          (Components [I] as TDBGridEh).ReadOnly   := True ;
          (Components [I] as TDBGridEh).Color      := clSilver ;
          (Components [I] as TDBGridEh).Font.Color := clYellow ;
          Update;
      end ;


      (Components [I] as TDBGridEh).Font.Name  := 'Calibri' ;
      (Components [I] as TDBGridEh).Font.Size  := 9 ;
      (Components [I] as TDBGridEh).Font.Style := [] ;


      (Components [I] as TDBGridEh).TitleFont.Name := 'Calibri' ;
      (Components [I] as TDBGridEh).TitleFont.Size := 9 ;

      (Components [I] as TDBGridEh).FooterFont.Name  := 'Calibri' ;

      if ((Components [I] as TDBGridEh).FooterFont.Color <> clWhite) then
          (Components [I] as TDBGridEh).FooterFont.Color := clTeal ;

      (Components [I] as TDBGridEh).FooterFont.Size  := 9 ;

      // padroniza as colunas

      with (Components[I] as TDBGridEH) do
      begin
          OptionsEH := OptionsEH + [dghAutoSortMarking] ;
          //Options := [dgTitles,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgConfirmDelete,dgCancelOnExit] ;

          with Columns do
          begin

            for i2 := 0 to Count - 1 do
            begin

                OnSortMarkingChanged := nil;

                If Items[0].ReadOnly then
                begin
                    Items[0].Title.Font.Color := clRed;
                    {Items[0].Font.Color := clTeal ;} //clGray ;
                    Items [0].Color := clSilver ;

                end ;

            end ;


          end ;

          if Columns.Count = 0 then
            exit ;

          //FooterRowCount := 1;
          //Columns.Items [0].Footer.ValueType := fvtCount;

          for i2 := 1 to Columns.Count - 1 do
          begin
              with Columns.Items[i2] do
              begin

                  try

                    Footer.Font.Name := 'Calibri' ;
                    Footer.Font.Size := 9 ;

                    if Field.DataType in [ftSmallint, ftInteger, ftLargeint, ftWord, ftFloat,ftCurrency, ftBCD] then
                    begin
                      Footer.ValueType := fvtSum;
                    end ;

                    if (Field.DataType in [ftFloat, ftCurrency, ftBCD]) and (Tag = 0) then
                    begin
                      Columns.Items [i2].DisplayFormat := '#,##0.00';
                      Footer.DisplayFormat := '#,##0.00';
                    end ;

                    if (Field.DataType in [ftDateTime]) and (Tag <> 2) then
                    begin
                      Columns.Items [i2].EditMask := '!99/99/9999;1;_';
                      Columns.Items [i2].Width    := 77 ;
                      ButtonStyle := cbsNone ;
                    end ;

                  except
                  end ;

                  If ReadOnly then
                  begin
                      Title.Font.Color := clRed ;
                      {Font.Color       := clTeal;} //clGray ;
                      Columns.Items [i2].Color := clSilver ;

                  end ;

                  {Font.Style := [fsBold] ;
                  Font.Name  := 'Arial' ;}

                  {Font.Name  := 'Tahoma' ;
                  Font.Size  := 7 ;}

              end ;

          end ;


      end ;

    end;

    if (Components [I] is TDBEdit) then
    begin
      //(Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := true    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TDBEdit).Font.Size   := 9 ; //7 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;
      {(Components [I] as TDBEdit).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;}

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBEdit).OnKeyPress  := frmMenu.NavegaEnter;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          //(Components [I] as TDBEdit).ParentFont := False ;
          //(Components [I] as TDBEdit).Enabled := False ;
          (Components [I] as TDBEdit).ReadOnly := True ;
          (Components [I] as TDBEdit).Color      := $00E9E4E4 ;
          (Components [I] as TDBEdit).Font.Color := clBlack ;
          (Components [I] as TDBEdit).TabStop  := False ;
          Update;
      end;

      if ((Components [I] as TDBEdit).Tag = 0) then
      begin
          if (Copy((Components [I] as TDBEdit).DataField,1,3) = 'dDt') then
             (Components [I] as TDBEdit).Width := 76 ;
      end ;


    end;

    if (Components [I] is TDBLookupComboBox) then
    begin

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
      begin
          (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;
      end ;

      (Components [I] as TDBLookupComboBox).Font.Size   := 9 ;
      (Components [I] as TDBLookupComboBox).Font.Name   := 'Calibri' ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;

    end;

    if (Components [I] is TDBMemo) then
    begin
      //(Components [I] as TDBMemo).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TDBMemo).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBMemo).Color       := clWhite ;
      (Components [I] as TDBMemo).BevelInner  := bvSpace ;
      (Components [I] as TDBMemo).BevelKind   := bkNone ;
      (Components [I] as TDBMemo).BevelOuter  := bvLowered ;
      (Components [I] as TDBMemo).Ctl3D       := True    ;
      (Components [I] as TDBMemo).BorderStyle := bsSingle ;
      (Components [I] as TDBMemo).Font.Name   := 'Calibri' ;
      (Components [I] as TDBMemo).Font.Size   := 9 ;
      (Components [I] as TDBMemo).Font.Style  := [] ;
      (Components [I] as TDBMemo).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if ((Components [I] as TDBMemo).Tag = 1) then
      begin
          (Components [I] as TDBMemo).ReadOnly   := True ;
          (Components [I] as TDBMemo).Color      := $00E9E4E4 ;
          (Components [I] as TDBMemo).Font.Color := clBlack ;
          (Components [I] as TDBMemo).TabStop    := False ;
          Update;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      //(Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 9 ;
      (Components [I] as TEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TEdit).OnKeyPress  := frmCadastro_Padrao.NavegaEnter ;

      if ((Components [I] as TEdit).Tag = 1) then
      begin
          (Components [I] as TEdit).ReadOnly   := True ;
          (Components [I] as TEdit).Color      := $00E9E4E4 ;
          (Components [I] as TEdit).Font.Color := clBlack ;
          (Components [I] as TEdit).TabStop    := False ;
          Update;
      end
          
    end;

    if (Components [I] is TMaskEdit) then
    begin
      //(Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TMaskEdit).Font.Size   := 9 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

      if Trim((Components[I] as TMaskEdit).EditMask) = '######;1;' then
          (Components[I] as TMaskEdit).EditMask := '##########;1; ' ;
      
      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TMaskEdit).OnKeyPress  := frmCadastro_Padrao.NavegaEnter ;

      if ((Components [I] as TMaskEdit).Tag = 1) then
      begin
          (Components [I] as TMaskEdit).ReadOnly := True ;
          (Components [I] as TMaskEdit).Color      := $00E9E4E4 ;
          (Components [I] as TMaskEdit).Font.Color := clBlack ;
          (Components [I] as TMaskEdit).TabStop  := False ;
          Update;
      end
      else
      begin
          if (Components[I] as TMaskEdit).EditMask = '!99/99/9999;1;_' then
              (Components[I] as TMaskEdit).Width := 76 ;
      end ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;

       if ((Components [I] as TLabel).Tag = 0) then
       begin
           (Components [I] as TLabel).Font.Color   := clWhite ;
       end ;

       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Calibri' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Style := [fsBold] ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    If (Components [I] is TDBCheckBox) then
      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBCheckBox).OnKeyPress  := frmCadastro_Padrao.NavegaEnter ;

    If (Components [I] is TcxGridDBTableView) then
    begin
        (Components [I] as TcxGridDBTableView).OptionsView.GridLineColor := clAqua ;
        (Components [I] as TcxGridDBTableView).OptionsView.GridLines     := glVertical ;
        (Components [I] as TcxGridDBTableView).Styles.Header             := frmMenu.Header ;
    end ;

    If (Components [I] is TcxGrid) then
    begin
        (Components [I] as TcxGrid).LookAndFeel.Kind        := lfFlat ;
        (Components [I] as TcxGrid).LookAndFeel.NativeStyle := True ;
        (Components [I] as TcxGrid).Font.Name               := 'Calibri' ;
    end ;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;

      (Components [I] as TDBLookupComboBox).Height      := 12 ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;
    end;

    if (Components [I] is TcxCurrencyEdit) then
    begin
        (Components [I] as TcxCurrencyEdit).Style.Font.Name := 'Calibri' ;
        (Components [I] as TcxCurrencyEdit).Style.Font.Size := 9 ;
    end ;
          
  end ;


  //ShowMessage(Sender.ClassName);

end;

procedure TfrmCadastro_Padrao.qryMasterBeforePost(DataSet: TDataSet);
begin

    nCdTipoAcao := 4 ;

    if (qryMaster.State = dsInsert) then
        nCdTipoAcao := 1 ;

    if (qryMaster.State = dsEdit) then
        nCdTipoAcao := 2 ;

    bInclusao := False ;

    if (qryMaster.State = dsInsert) then
    begin
        bInclusao := True ;

        If (cNmTabelaMaster = '') then
        begin
            ShowMessage('cNmTabelaMaster n�o configurada. N�o foi poss�vel gravar o registro.') ;
            exit ;
        end ;

        if (qryMaster.State = dsInsert) then
        begin

          case MessageDlg('Confirma a inclus�o deste registro ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:abort ;
          end ;

        end ;

        if (bCodigoAutomatico) then
        begin
            usp_ProximoID.Close;
            usp_ProximoID.Parameters.ParamByName('@cNmTabela').Value := cNmTabelaMaster ;
            usp_ProximoID.ExecProc ;

            qryMaster.FieldList[0].Value := usp_ProximoID.Parameters.ParamByName('@iUltimoCodigo').Value ;

            qryID.Close ;
        end ;

    end ;

end;

procedure TfrmCadastro_Padrao.ToolButton1Click(Sender: TObject);
var
  i:Integer ;
begin

    i := frmMenu.BuscaPK() ;

    If (i > 0) then
    begin
        PosicionaPK(i) ;
    end ;

end;

procedure TfrmCadastro_Padrao.btnAuditoriaClick(Sender: TObject);
begin
    if (qryMaster.Active) then
    begin
        if (qryMaster.FieldList[0].Value <> null) then
            frmMenu.ExibeAuditoria(nCdTabelaSistema , qryMaster.FieldList[0].Value) ;
    end ;
end;

procedure TfrmCadastro_Padrao.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmCadastro_Padrao.cxButton1Click(Sender: TObject);
begin
    if (qryMaster.Active) then
    begin
        if (qryMaster.FieldList[0].Value <> null) then
            frmMenu.ExibeAuditoria(nCdTabelaSistema , qryMaster.FieldList[0].Value) ;
    end ;

end;

procedure TfrmCadastro_Padrao.FormShow(Sender: TObject);
var
  i : integer ;
begin
  If (nCdConsultaPadrao = -1) then
  begin
      btConsultar.Enabled := False ;
  end ;

  if (cNmTabelaMaster = '') then
  begin
      ShowMessage('cNmTabelaMaster n�o configurado.') ;
      Self.Destroy;
  end ;

  if (nCdTabelaSistema = 0) then
  begin
      ShowMessage('nCdTabelaSistema n�o configurado.') ;
      Self.Destroy;
  end ;

  qryStat.Open ;

  bAlterarDados := False ;

  usp_ValidaPermissao.Close;
  usp_ValidaPermissao.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  usp_ValidaPermissao.Parameters.ParamByName('@cNmClasse').Value  := Sender.ClassName ;
  usp_ValidaPermissao.ExecProc;

  bAlterarDados := True ;

  if (usp_ValidaPermissao.Parameters.ParamByName('@cFlgAcesso').Value = 0) then
  begin
      btIncluir.Visible := False ;
      btSalvar.Visible  := False ;
      btExcluir.Enabled  := False ;
      bAlterarDados := False ;
  end ;

  if not bAlterarDados then
  begin
    for I := 0 to ComponentCount - 1 do
    begin

      if (Components [I] is TDBGridEh) then
      begin
        (Components [I] as TDBGridEh).ReadOnly   := True ;
      end ;

    end ;
  end ;

end;

procedure TfrmCadastro_Padrao.FormActivate(Sender: TObject);
begin

    Self.ToolBar2.Enabled := True ;
    frmMenu.StatusBar1.Panels[5].Text := Self.ClassName ;
    
end;

procedure TfrmCadastro_Padrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
    i : integer ;
begin
    ToolBar2.Enabled := False ;
    frmMenu.StatusBar1.Panels[5].Text := '' ;

    {for I := 0 to ComponentCount - 1 do
    begin
        try
            if (Components[I] is TcxButton) then
                (Components[I] as TcxButton).Caption := 'xxx' ;

        except
        end ;
    end ;}

end;

procedure TfrmCadastro_Padrao.btExcluirClick(Sender: TObject);
begin

  case MessageDlg('Confirma a exclus�o deste registro ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

    try
        qryMaster.Delete;
    except
        MensagemAlerta('Erro na tentativa de exclus�o, o registro pode ter depend�ncias.') ;
        raise ;
        exit ;
    end ;

    qryMaster.Insert ;

    btIncluir.Enabled  := False ;
    btAlterar.Enabled  := False ;
    btSalvar.Enabled   := False ;
    btCancelar.Enabled := False ;
    btExcluir.Enabled  := False ;

    btIncluir.Enabled := True ;


end;

procedure TfrmCadastro_Padrao.qryMasterAfterPost(DataSet: TDataSet);
begin
    frmMenu.LogAuditoria(nCdTabelaSistema, nCdTipoAcao, qryMaster.FieldList[0].Value,'') ;

end;

procedure TfrmCadastro_Padrao.FormDeactivate(Sender: TObject);
begin
    ToolBar2.Enabled := False ;
end;

procedure TfrmCadastro_Padrao.TrataData(Sender: TField; const Text: string) ;
begin

  if (Sender.DataType = ftInteger) then
  begin
      if (Trim(Text) = '') then
          Sender.AsString := ''
      else Sender.Value := Text ;
      exit ;
  end ;

  if (Sender.DataType = ftDate) or (Sender.DataType = ftDateTime) then
  begin
    if (Text = '  /  /    ') then
      Sender.asString := ''
    else
    begin
      try
          StrToDate(Text) ;
          Sender.Value := Text
      except
          MensagemErro('Data inv�lida. Utilize o formado dia/mes/ano.' +#13#13 + 'Exemplo: ' + DateToStr(Date)) ;
          abort ;
      end ;
    end;
  end
  else if (Sender.DataType = ftFloat) then
  begin
    if (Trim(Text) = '') then
      Sender.AsFloat := 0
    else
    begin
      try
          StrToFloat(Text) ;
          Sender.Value := Text
      except
          MensagemErro('Valor Inv�lido.' +#13#13+'N�o utilize pontos na digita��o de valores, apenas virgula para separar os centavos.') ;
          abort ;
      end ;
    end;
  end
  else
  begin
    Sender.Value := Text;
  end;

end;

function TfrmCadastro_Padrao.InputQuery(const ACaption, APrompt: string;
  var Value: string): Boolean;
begin
    Result := frmMenu.InputQuery(ACaption, APrompt, Value) ;

end;

procedure TfrmCadastro_Padrao.NavegaEnter(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin

    key := #0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmCadastro_Padrao.showForm(objForm: TForm; bDestroirObjeto : boolean);
begin

    frmMenu.showForm(objForm , bDestroirObjeto);

end;

procedure TfrmCadastro_Padrao.desativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TfrmCadastro_Padrao.ativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmCadastro_Padrao.ativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmCadastro_Padrao.desativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TfrmCadastro_Padrao.ativaER2LkpMaskEdit(obj : TER2LookupMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmCadastro_Padrao.desativaER2LkpMaskEdit(obj : TER2LookupMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

function TfrmCadastro_Padrao.fnValidaEstado(cSiglaEstado : String) : Boolean;
var strEstados : TstringList;
begin
  strEstados := TStringList.Create;

  strEstados.Add('AC') ;
  strEstados.Add('AL') ;
  strEstados.Add('AM') ;
  strEstados.Add('AP') ;
  strEstados.Add('BA') ;
  strEstados.Add('CE') ;
  strEstados.Add('DF') ;
  strEstados.Add('ES') ;
  strEstados.Add('EX') ;
  strEstados.Add('GO') ;
  strEstados.Add('MA') ;
  strEstados.Add('MG') ;
  strEstados.Add('MS') ;
  strEstados.Add('MT') ;
  strEstados.Add('PA') ;
  strEstados.Add('PB') ;
  strEstados.Add('PE') ;
  strEstados.Add('PI') ;
  strEstados.Add('PR') ;
  strEstados.Add('RJ') ;
  strEstados.Add('RN') ;
  strEstados.Add('RO') ;
  strEstados.Add('RR') ;
  strEstados.Add('RS') ;
  strEstados.Add('SC') ;
  strEstados.Add('SE') ;
  strEstados.Add('SP') ;
  strEstados.Add('TO') ;

  if (strEstados.IndexOf(cSiglaEstado) = -1) then
  begin
      MensagemAlerta('Estado informado inv�lido.');
      Result := False;
  end
  else
      Result := True;
end;

initialization
    RegisterClass(tfrmCadastro_Padrao) ;

{$R *.dfm}


end.


