unit fMonitorMapaCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, StdCtrls, DBGridEhGrouping;

type
  TfrmMonitorMapaCompra = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    qryResultadonCdMapaCompra: TAutoIncField;
    qryResultadocNmMapaCompra: TStringField;
    qryResultadodDtGeracao: TDateTimeField;
    qryResultadocNmUsuario: TStringField;
    qryResultadocNmStatus: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure AtualizaResultado();
    procedure FormShow(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorMapaCompra: TfrmMonitorMapaCompra;

implementation

uses fMenu, rMapaCompra, fMapaCompra;

{$R *.dfm}

procedure TfrmMonitorMapaCompra.AtualizaResultado();
begin

    qryResultado.Close ;

    qryResultado.Parameters.ParamByName('nCdusuario').Value := 0 ;

    if (RadioGroup1.ItemIndex = 0) then
        qryResultado.Parameters.ParamByName('nCdusuario').Value := frmMenu.nCdUsuarioLogado ;

    qryResultado.Parameters.ParamByName('nCdStatus').Value  := RadioGroup2.ItemIndex ;
    qryResultado.Open ;

    DBGridEh1.SetFocus ;

end ;

procedure TfrmMonitorMapaCompra.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  
  AtualizaResultado() ;
end;

procedure TfrmMonitorMapaCompra.RadioGroup1Click(Sender: TObject);
begin
  inherited;
  AtualizaResultado() ;

end;

procedure TfrmMonitorMapaCompra.RadioGroup2Click(Sender: TObject);
begin
  inherited;
  AtualizaResultado() ;

end;

procedure TfrmMonitorMapaCompra.ToolButton5Click(Sender: TObject);
var
   objRel : TrptMapaCompra;
begin
  inherited;

  if not qryResultado.Active then
  begin
      MensagemAlerta('Selecione um mapa.') ;
      exit ;
  end ;

  objRel := TrptMapaCompra.Create(nil);
  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMapaCompra').Value := qryResultadonCdMapaCompra.Value ;
          objRel.usp_Relatorio.Open ;

          objRel.SPREL_MAPA_HEADER.Close ;
          objRel.SPREL_MAPA_HEADER.Parameters.ParamByName('@nCdMapaCompra').Value := qryResultadonCdMapaCompra.Value ;
          objRel.SPREL_MAPA_HEADER.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.QuickRep1.Preview;
      except
          MensagemErro('Erro na cria�ao do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;


procedure TfrmMonitorMapaCompra.DBGridEh1DblClick(Sender: TObject);
var
   objForm : TfrmMapaCompra;
begin
  inherited;

  if (qryResultado.Eof) then
      exit ;

  objForm := TfrmMapaCompra.Create(nil);

  objForm.PosicionaPK(qryResultadonCdMapaCompra.Value);

  objForm.btSalvar.Enabled    := False ;
  objForm.btCancelar.Enabled  := False ;
  objForm.btConsultar.Enabled := False ;
  objForm.btExcluir.Enabled   := False ;
  objForm.DBGridEh1.ReadOnly  := True ;
  objForm.DBGridEh2.ReadOnly  := True ;

  ShowForm(objForm,True);

end;

initialization
    RegisterClass(TfrmMonitorMapaCompra) ;
end.
