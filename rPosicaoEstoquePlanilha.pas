unit rPosicaoEstoquePlanilha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP, ER2Lookup;

type
  TrptPosicaoEstoquePlanilha = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    edtGrupoEstoque: TMaskEdit;
    Label15: TLabel;
    qryGrupoEstoque: TADOQuery;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    DBEdit12: TDBEdit;
    DataSource12: TDataSource;
    Label7: TLabel;
    edtGrade: TMaskEdit;
    qryGrade: TADOQuery;
    qryGradenCdGrade: TIntegerField;
    qryGradecNmGrade: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA: TADOStoredProc;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    qryEmpresa: TADOQuery;
    DataSource9: TDataSource;
    edtEmpresa: TER2LookupMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEstoqueExit(Sender: TObject);
    procedure edtGrupoEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGradeExit(Sender: TObject);
    procedure edtGradeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoquePlanilha: TrptPosicaoEstoquePlanilha;

implementation

uses fMenu, fLookup_Padrao, rPosicaoEstoqueGrade_view;

{$R *.dfm}

procedure TrptPosicaoEstoquePlanilha.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      desativaMaskEdit(edtLoja);

end;

procedure TrptPosicaoEstoquePlanilha.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TrptPosicaoEstoquePlanilha.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptPosicaoEstoquePlanilha.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptPosicaoEstoquePlanilha.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptPosicaoEstoquePlanilha.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptPosicaoEstoquePlanilha.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptPosicaoEstoquePlanilha.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;

begin
  inherited;

{  if (DBEdit7.Text = '') then
  begin
      MensagemAlerta('Selecione a grade.') ;      ---Trecho comentado para que possa permitir o relatorio sem a grade
      edtGrade.SetFocus;
      abort ;
  end ;     }

  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Close ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdGrade').Value         := frmMenu.ConvInteiro(edtGrade.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdGrupoEstoque').Value  := frmMenu.ConvInteiro(edtGrupoEstoque.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(edtEmpresa.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
  SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Open ;

  if (SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.eof) then
  begin
      ShowMessage('Nenhum registro encontrado para o crit�rio selecionado.') ;
      edtGrade.SetFocus;
      abort ;
  end ;

    try
        planilha:= CreateoleObject('Excel.Application');
    except
        MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
        exit ;
    end ;

    LCID := GetUserDefaultLCID;
    planilha.DisplayAlerts  := False;
    planilha.WorkBooks.add(1);
    planilha.ScreenUpdating := true;

    planilha.caption := 'Posi��o de Estoque em Grade';

    for coluna := 1 to SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.FieldCount do
    begin

       valorcampo := SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Fields[coluna - 1].DisplayLabel;

       case frmMenu.StringToCaseSelect(valorCampo
                                      ,['cNmDepartamento'
                                       ,'cNmCategoria'
                                       ,'cNmMarca'
                                       ,'cReferencia'
                                       ,'cNmProduto'
                                       ,'cNmGrade'
                                       ,'cNmLocalEstoque'
                                       ,'cChaveCampanha'
                                       ,'dDtUltRecebEstoque'
                                       ,'nCdProduto'
                                       ,'nCdLoja'
                                       ,'nCdLocalEstoque'
                                       ,'nSaldoEstoque'
                                       ,'nTransito']) of
           0 : valorCampo  := 'DEPARTAMENTO' ;
           1 : valorCampo  := 'CATEGORIA' ;
           2 : valorCampo  := 'MARCA' ;
           3 : valorCampo  := 'REFERENCIA';
           4 : valorCampo  := 'DESCRICAO PRODUTO';
           5 : valorCampo  := 'GRADE';
           6 : valorCampo  := 'LOCAL ESTOQUE';
           7 : valorCampo  := 'CAMPANHA PROMOC.' ;
           8 : valorCampo  := 'DT. ULT. REC.' ;
           9 : valorCampo  := 'COD. PRODUTO';
           10 : valorCampo := 'LOJA';
           11: valorCampo  := 'COD. LOCAL ESTOQ.' ;
           12: valorCampo  := 'SALDO TT';
           13: valorCampo  := 'Qtde. Transito';
       end ;

       if (Pos('xxx',ValorCampo) > 0) then
           valorCampo := Copy(valorCampo,Pos('xxx',ValorCampo)+3,Length(ValorCampo)) ;
           
       planilha.cells[1,coluna] := valorcampo;

    end;

    for linha := 0 to SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.RecordCount - 1 do
    begin

       for coluna := 1 to SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.FieldCount do
       begin

         valorcampo := SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;

         if (SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Fields[coluna - 1].DataType = ftBCD) then
         begin
             planilha.Cells[Linha+2,coluna].NumberFormat        := '0,00' ;
             planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlRight ;
         end ;

         if (SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Fields[coluna - 1].DataType = ftDateTime) then
         begin
             planilha.Cells[Linha+2,coluna].NumberFormat        := 'dd/mm/yyyy' ;
             planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlLeft ;
         end ;

       end;

       SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA.Next;

    end;

    planilha.columns.Autofit;
    planilha.Visible := True;


end;

procedure TrptPosicaoEstoquePlanilha.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptPosicaoEstoquePlanilha.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtGrupoEstoqueExit(Sender: TObject);
begin
  inherited;

  qryGrupoEstoque.Close;
  PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;
  
end;

procedure TrptPosicaoEstoquePlanilha.edtGrupoEstoqueKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(85);

        If (nPK > 0) then
        begin

            edtGrupoEstoque.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoquePlanilha.edtGradeExit(Sender: TObject);
begin
  inherited;

  qryGrade.Close;
  PosicionaQuery(qryGrade, edtGrade.Text);
  
end;

procedure TrptPosicaoEstoquePlanilha.edtGradeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(51);

        If (nPK > 0) then
            edtGrade.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(TrptPosicaoEstoquePlanilha) ;

end.
