unit fMotivoHoraImprodutiva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmMotivoHoraImprodutiva = class(TfrmCadastro_Padrao)
    qryMasternCdMotivoHoraImprodutiva: TIntegerField;
    qryMastercNmMotivoHoraImprodutiva: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMotivoHoraImprodutiva: TfrmMotivoHoraImprodutiva;

implementation

{$R *.dfm}

procedure TfrmMotivoHoraImprodutiva.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MOTIVOHORAIMPRODUTIVA' ;
  nCdTabelaSistema  := 425 ;
  nCdConsultaPadrao := 1024 ;

end;

procedure TfrmMotivoHoraImprodutiva.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEDit2.SetFocus;
end;

procedure TfrmMotivoHoraImprodutiva.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe o Motivo de hora improdutiva.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;
  inherited;

end;

initialization
    RegisterClass(TfrmMotivoHoraImprodutiva) ;

end.
