unit fNotificaVencTitEmail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, DB, ADODB,
  IdBaseComponent, IdComponent, IdIOHandler, IdIOHandlerSocket,
  IdSSLOpenSSL, Mask, ER2Lookup, StdCtrls, DBCtrls, IdMessage, StrUtils,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxButtons;

type
  TfrmNotificaVencTitEmail = class(TfrmProcesso_Padrao)
    IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket;
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecUFEmissaoNfe: TStringField;
    qryConfigAmbientecPathLogoNFe: TStringField;
    qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField;
    qryConfigAmbientecFlgEmiteNFe: TIntegerField;
    qryConfigAmbientecNrSerieCertificadoNFe: TStringField;
    qryConfigAmbientenCdUFEmissaoNFe: TIntegerField;
    qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField;
    qryConfigAmbientecPathArqNFe: TStringField;
    qryConfigAmbientecServidorSMTP: TStringField;
    qryConfigAmbientenPortaSMTP: TIntegerField;
    qryConfigAmbientecEmail: TStringField;
    qryConfigAmbientecSenhaEmail: TStringField;
    qryConfigAmbientecFlgUsaSSL: TIntegerField;
    qryModeloDocumento: TADOQuery;
    qryModeloDocumentocDescricaoModeloDoc: TMemoField;
    qryModeloDocumentonCdTabTipoModeloDoc: TIntegerField;
    IdSMTP1: TIdSMTP;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    dsTerceiro: TDataSource;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    dsEspTit: TDataSource;
    qryMaster: TADOQuery;
    qryMasternCdTitulo: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMasternValTit: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValAbatimento: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLojaTit: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryTerceiroAux: TADOQuery;
    qryTerceiroAuxcEmail: TStringField;
    dsModeloDocumento: TDataSource;
    qryModeloDocumentocNmModeloDocumento: TStringField;
    qryMastercEmail: TStringField;
    GroupBox1: TGroupBox;
    edtDias: TEdit;
    ER2LookupMaskEdit5: TER2LookupMaskEdit;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    ER2LookupMaskEdit3: TER2LookupMaskEdit;
    ER2LookupMaskEdit4: TER2LookupMaskEdit;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    panel: TPanel;
    btEnviarLoteEmail: TcxButton;
    btMarcarTodos: TcxButton;
    btDesmarcarTodos: TcxButton;
    cxGridDoctoFiscal: TcxGrid;
    cxGridDoctoFiscalDBTableView1: TcxGridDBTableView;
    cxGridDoctoFiscalDBTableView1DBColumn_cFlgAtivo: TcxGridDBColumn;
    cxGridDoctoFiscalLevel1: TcxGridLevel;
    qryAux: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryTempTerceiro: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryTempTerceironCdTitulo: TIntegerField;
    qryTempTerceironCdTerceiro: TIntegerField;
    qryTempTerceirocNrTit: TStringField;
    qryTempTerceirocEmail: TStringField;
    qryTempTerceironCdEmpresa: TIntegerField;
    qryTempTerceironCdLojaTit: TIntegerField;
    qryTempTerceironCdEspTit: TIntegerField;
    qryTempTerceironValTit: TBCDField;
    qryTempTerceironValDesconto: TBCDField;
    qryTempTerceironValJuro: TBCDField;
    qryTempTerceironValAbatimento: TBCDField;
    qryTempTerceironSaldoTit: TBCDField;
    qryTempTerceirodDtVenc: TDateTimeField;
    qryTempTerceirocFlgAtivo: TIntegerField;
    dsTempTerceiro: TDataSource;
    qryTempTerceirocNmTerceiro: TStringField;
    qryTempTerceiroiParcela: TIntegerField;
    cxGridDoctoFiscalDBTableView1DBColumn_nCdTerceiro: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cNmTerceiro: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cNrTit: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_iParcela: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_nSaldoTit: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_dDtVenc: TcxGridDBColumn;
    Label7: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    qryTerceiroAuxcNmTerceiro: TStringField;
    GroupBox2: TGroupBox;
    mLog: TMemo;
    cxGridDoctoFiscalDBTableView1DBColumn_cNmTerceiro1: TcxGridDBColumn;
    Label5: TLabel;
    RadioGroup1: TRadioGroup;
    procedure ER2LookupMaskEdit1Exit(Sender: TObject);
    procedure ER2LookupMaskEdit2Exit(Sender: TObject);
    procedure ER2LookupMaskEdit4Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure prEnviaEmailHTML(cNrTit : String; nCdTerceiro, nCdEmpresa, nCdModDocumento : Integer);
    procedure qryMasterBeforeInsert(DataSet: TDataSet);
    procedure ER2LookupMaskEdit5Exit(Sender: TObject);
    function  fnValidaEmail(cEmail : String) : Boolean;
    procedure ER2LookupMaskEdit3Exit(Sender: TObject);
    procedure btEnviarLoteEmailClick(Sender: TObject);
    procedure btMarcarTodosClick(Sender: TObject);
    procedure btDesmarcarTodosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    iEmailEnviado : Integer;
  public
    { Public declarations }
  end;

var
  frmNotificaVencTitEmail: TfrmNotificaVencTitEmail;

implementation

{$R *.dfm}
uses
  fMenu, fModeloDocumento;

function TfrmNotificaVencTitEmail.fnValidaEmail(cEmail : String) : Boolean;
begin
    cEmail := Trim(UpperCase(cEmail));

    if Pos('@', cEmail) > 1 then
    begin
        Delete(cEmail, 1, Pos('@', cEmail));
        Result := (Length(cEmail) > 0) and (Pos('.', cEmail) > 1);
    end
    else
        Result := False;
end;
procedure TfrmNotificaVencTitEmail.prEnviaEmailHTML(cNrTit : String; nCdTerceiro, nCdEmpresa, nCdModDocumento : Integer);
var
  Assunto, Servidor, Usuario, Senha: String;
  Mensagem, Copia : TStrings;
  Porta           : Integer;
  SMTP            : TIdSMTP;
  idMessage       : TIdMessage;
  idTextPart      : TIdText;
  i               : Integer;
  arrParam        : Array of String;
  strArquivos     : TStringList;
  Busca           : TSearchRec;
  objModDoc       : TfrmModeloDocumento;
begin
  Mensagem  := TStringList.Create;
  //Copia     := TStringList.Create;
  SMTP      := TIdSMTP.Create(nil);
  idMessage := TIdMessage.Create(nil);
  {-- Verifica se tem parametro --}

  if (nCdModDocumento > 0) then
  begin
      try
          objModDoc := TfrmModeloDocumento.Create(Nil);

          qryModeloDocumento.Close;
          qryModeloDocumento.Parameters.ParamByName('nPK').Value := nCdModDocumento;
          qryModeloDocumento.Open;

          {-- Parametros de variaveis do Documento --}
          SetLength(arrParam,3);
          arrParam[0] := 'Titulo,'   + cNrTit;
          arrParam[1] := 'TerceiroOrigem,' + IntToStr(nCdTerceiro);
          arrParam[2] := 'Empresa,'  + IntToStr(nCdEmpresa);

          if (qryModeloDocumentonCdTabTipoModeloDoc.Value = 2) then
          begin
              strArquivos := TStringList.Create; //Pega os anexos

              if FindFirst('.\Arquivos\ModeloDocumento\' + IntToStr(nCdModDocumento) + '\*.jpg', faArchive, Busca) = 0 then
              begin
                  repeat
                      strArquivos.Add('.\Arquivos\ModeloDocumento\' + IntToStr(nCdModDocumento) + '\' + Busca.Name);
                  until FindNext(Busca) <> 0;
                  FindClose(Busca);
              end;

              {-- Cria o corpo do e-mail em HTML --}
              idTextPart             := TIdText.Create(idMessage.MessageParts, nil);
              idTextPart.ContentType := 'text/plain';
              idTextPart.Body.Add('E-mail');

              idTextPart             := TIdText.Create(idMessage.MessageParts, nil);
              idTextPart.ContentType := 'text/html';

              objModDoc.qryTemp.Close;
              objModDoc.qryTemp.Open;
              {-- Chama a fun��o da ModeloDocumento, fnSubistituiVariaveis, que retorna o conteudo do texto formatado --}
              idTextPart.Body.Text := objModDoc.fnSubistituiVariaveis(nCdModDocumento,nCdTerceiro, arrParam);
              Mensagem.add(idTextPart.Body.Text);
              {-- Anexa Arquivos -- }
              for i := 0 to strArquivos.Count - 1 do
                  TIdAttachment.Create(idMessage.MessageParts, strArquivos[i]);
          end;

      finally
          FreeAndNil(objModDoc);
      end;

      {-- Pega as Configura��es de ambiente --}
      qryConfigAmbiente.Close;
      qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
      qryConfigAmbiente.Open;
      
      Servidor := qryConfigAmbientecServidorSMTP.Value;
      Porta    := qryConfigAmbientenPortaSMTP.Value;
      Usuario  := qryConfigAmbientecEmail.Value;
      Senha    := qryConfigAmbientecSenhaEmail.Value;

      {-- Passa as configura��es de conex�o para o INDY --}

      SMTP.Host               := Servidor;
      SMTP.Port               := Porta;
      SMTP.Username           := Usuario;
      SMTP.Password           := Senha;
      SMTP.AuthenticationType := atLogin;

      if (qryConfigAmbientecFlgUsaSSL.Value = 1) then
      begin
          SMTP.IOHandler                          := IdSSLIOHandlerSocket1;
          IdSSLIOHandlerSocket1.SSLOptions.Method := sslvTLSv1;
          IdSSLIOHandlerSocket1.PassThrough       := true;
          IdSSLIOHandlerSocket1.SSLOptions.Mode   := sslmClient;
      end;
      {-- Pega o e-mail destinat�rio do terceiro --}
      qryTerceiroAux.Close;
      qryTerceiroAux.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
      qryTerceiroAux.Open;
      
      {-- prepara os dados da mensagem --}
      if (not fnValidaEmail(qryTerceiroAuxcEmail.AsString)) then
      begin
          mLog.Lines.Add('Erro: ' + qryTerceiroAuxcNmTerceiro.AsString + ' - ' + qryTerceiroAuxcEmail.AsString + ' e-mail inv�lido');
          Exit;
      end
      else
          mLog.Lines.Add('Enviando e-mail para ' + qryTerceiroAuxcNmTerceiro.AsString + ' em ' + qryTerceiroAuxcEmail.AsString);

      idMessage.From.Address              := qryTerceiroAuxcEmail.AsString;
      idMessage.Recipients.EMailAddresses := qryTerceiroAuxcEmail.AsString;
      idMessage.Subject                   := 'E-mail Cobran�a';
      idMessage.Body.Text                 := Mensagem.Text;
      idMessage.CharSet                   := 'iso-8859-1';
      {-- envia o email --}
      try
          frmMenu.mensagemUsuario('Enviando e-mail');
          SMTP.Connect(2000);
          if (qryConfigAmbientecFlgUsaSSL.Value = 1) then
          begin
              SMTP.SendCmd('STARTTLS', 220);
              IdSSLIOHandlerSocket1.PassThrough := false;
              SMTP.Authenticate;
          end;
          SMTP.Send(idMessage);
          mlog.Lines.Add('E-mail de notifica��o para o(a) ' + qryTerceiroAuxcNmTerceiro.AsString + ' (' + qryTerceiroAuxcEmail.AsString + ') enviado com sucesso');
          iEmailEnviado := iEmailEnviado + 1;
      except on E:Exception do
           begin
               frmMenu.MensagemErro('Erro: ' + E.Message) ;
               if SMTP.Connected then SMTP.Disconnect;
               frmMenu.mensagemUsuario('');
               raise;
          end;
      end;

      if SMTP.Connected then SMTP.Disconnect;

      frmMenu.mensagemUsuario('');

  end;

end;

procedure TfrmNotificaVencTitEmail.ER2LookupMaskEdit1Exit(Sender: TObject);
begin
  inherited;
  if trim(ER2LookupMaskEdit1.Text) = '' then
      exit;

  PosicionaQuery(qryEmpresa, ER2LookupMaskEdit1.Text);
end;

procedure TfrmNotificaVencTitEmail.ER2LookupMaskEdit2Exit(Sender: TObject);
begin
  inherited;
  if trim(ER2LookupMaskEdit2.Text) = '' then
      exit;

  PosicionaQuery(qryLoja, ER2LookupMaskEdit2.Text);
end;

procedure TfrmNotificaVencTitEmail.ER2LookupMaskEdit4Exit(Sender: TObject);
begin
  inherited;
  if trim(ER2LookupMaskEdit4.Text) = '' then
      exit;

  PosicionaQuery(qryEspTit, ER2LookupMaskEdit4.Text);
end;

procedure TfrmNotificaVencTitEmail.ToolButton1Click(Sender: TObject);
var
  cDias : String;
begin
  inherited;

  cmdPreparaTemp.Execute;

  cDias := Trim(edtDias.Text);
  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value  := Trim(ER2LookupMaskEdit1.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdLojaTit').Value  := Trim(ER2LookupMaskEdit2.Text);
  
  if Trim(DBEdit3.Text) <> '' then
      qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value  := Trim(ER2LookupMaskEdit3.Text)
  else
      qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value  := 0;

  qryPopulaTemp.Parameters.ParamByName('cFlgTipoVenc').Value  := RadioGroup1.ItemIndex;
  qryPopulaTemp.Parameters.ParamByName('nCdEspTit').Value   := Trim(ER2LookupMaskEdit4.Text);
  qryPopulaTemp.Parameters.ParamByName('iDias').Value       := cDias;
  qryPopulaTemp.Parameters.ParamByName('cFlgTipoVenc').Value := RadioGroup1.ItemIndex;
  qryPopulaTemp.ExecSQL;

  qryTempTerceiro.Close;
  qryTempTerceiro.Open;
end;

procedure TfrmNotificaVencTitEmail.qryMasterBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if trim(DBEdit1.Text) = '' then
  begin
      MensagemAlerta('Informe a empresa');
      Abort;
  end;

  if trim(DBEdit2.Text) = '' then
  begin
      MensagemAlerta('Informe a lojas');
      Abort;
  end;

  if trim(DBEdit4.Text) = '' then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo');
      Abort;
  end;

  if trim(edtDias.Text) = '' then
  begin
      MensagemAlerta('Informe a quantidade de dias');
      Abort;
  end;
  
end;

procedure TfrmNotificaVencTitEmail.ER2LookupMaskEdit5Exit(Sender: TObject);
begin
  inherited;
  if Trim(ER2LookupMaskEdit5.Text) = '' then
      exit;

  PosicionaQuery(qryModeloDocumento, ER2LookupMaskEdit5.Text);
end;

procedure TfrmNotificaVencTitEmail.ER2LookupMaskEdit3Exit(Sender: TObject);
begin
  inherited;
  if trim(ER2LookupMaskEdit3.Text) = '' then
      exit;

  PosicionaQuery(qryTerceiro, ER2LookupMaskEdit3.Text);
end;

procedure TfrmNotificaVencTitEmail.btEnviarLoteEmailClick(Sender: TObject);
var
  cNrTit           : String;
  cdEmpresa        : String;
  cdTerceiro       : String;
  nCdModDocumento  : Integer;
  cAux             : String;
  bTerceiro        : Boolean;
begin
  inherited;

  if (qryTempTerceiro.IsEmpty) then
      Exit;

  cNrTit          := '';
  cdEmpresa       := Trim(qryTempTerceironCdEmpresa.AsString);
  cAux            := Trim(ER2LookupMaskEdit5.Text);
  nCdModDocumento := StrToInt(cAux);

  try
      btEnviarLoteEmail.Enabled := False;
      
      mLog.Lines.Clear;
      mLog.Lines.Add('Iniciando envio de notifica��es por e-mail...');
  
      qryTempTerceiro.First;

      while (not qryTempTerceiro.Eof) do
      begin
          if (qryTempTerceirocFlgAtivo.Value = 1) then
          begin
              cNrTit := Trim(qryTempTerceirocNrTit.Text);
              cdTerceiro := Trim(qryTempTerceironCdTerceiro.AsString);
              prEnviaEmailHTML(cNrTit, StrToInt(cdTerceiro), StrToInt(cdEmpresa), nCdModDocumento);
          end;
          qryTempTerceiro.Next;
      end;

      if (iEmailEnviado = qryTempTerceiro.RecordCount) then
      begin
          ShowMessage('E-mail enviado com sucesso!');
          mLog.Lines.Add('E-mail' + #39 + 's enviados.');
          iEmailEnviado := 0;
      end;
  finally
      btEnviarLoteEmail.Enabled := True;
  end;
end;

procedure TfrmNotificaVencTitEmail.btMarcarTodosClick(Sender: TObject);
begin
  inherited;
  if (qryTempTerceiro.IsEmpty) then
      Exit;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempTerceiro SET cFlgAtivo = 1');
  qryAux.ExecSQL;

  qryTempTerceiro.Close;
  qryTempTerceiro.Open;
end;

procedure TfrmNotificaVencTitEmail.btDesmarcarTodosClick(Sender: TObject);
begin
  inherited;
  if (qryTempTerceiro.IsEmpty) then
      Exit;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempTerceiro SET cFlgAtivo = 0');
  qryAux.ExecSQL;

  qryTempTerceiro.Close;
  qryTempTerceiro.Open;
end;

procedure TfrmNotificaVencTitEmail.FormCreate(Sender: TObject);
begin
  inherited;
  iEmailEnviado := 0;
end;

procedure TfrmNotificaVencTitEmail.Button2Click(Sender: TObject);
begin
  inherited;
  while (not qryTempTerceiro.Eof) do
  begin
      if (qryTempTerceirocFlgAtivo.Value = 1) then
          showmessage(qryTempTerceirocEmail.AsString);

      qryTempTerceiro.Next;
  end;
end;

initialization
    RegisterClass(TfrmNotificaVencTitEmail);

end.
