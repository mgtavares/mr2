inherited rptPosicaoEstoquePlanilha: TrptPosicaoEstoquePlanilha
  Left = 58
  Top = 73
  Height = 665
  Caption = 'Relat'#243'rio Posi'#231#227'o de Estoque - Planilha'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 603
  end
  object Label1: TLabel [1]
    Left = 80
    Top = 120
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 31
    Top = 144
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label3: TLabel [3]
    Left = 53
    Top = 168
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label4: TLabel [4]
    Left = 32
    Top = 192
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
  end
  object Label5: TLabel [5]
    Left = 52
    Top = 216
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label6: TLabel [6]
    Left = 71
    Top = 240
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label11: TLabel [7]
    Left = 75
    Top = 264
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label15: TLabel [8]
    Left = 29
    Top = 72
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Estoque'
  end
  object Label7: TLabel [9]
    Left = 71
    Top = 48
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grade'
  end
  object Label8: TLabel [10]
    Left = 58
    Top = 96
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 17
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtLoja: TMaskEdit [12]
    Left = 104
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtDepartamento: TMaskEdit [13]
    Left = 104
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = edtDepartamentoExit
    OnKeyDown = edtDepartamentoKeyDown
  end
  object edtCategoria: TMaskEdit [14]
    Left = 104
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtCategoriaExit
    OnKeyDown = edtCategoriaKeyDown
  end
  object edtSubCategoria: TMaskEdit [15]
    Left = 104
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtSubCategoriaExit
    OnKeyDown = edtSubCategoriaKeyDown
  end
  object edtSegmento: TMaskEdit [16]
    Left = 104
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtSegmentoExit
    OnKeyDown = edtSegmentoKeyDown
  end
  object edtMarca: TMaskEdit [17]
    Left = 104
    Top = 232
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = edtMarcaExit
    OnKeyDown = edtMarcaKeyDown
  end
  object DBEdit1: TDBEdit [18]
    Tag = 1
    Left = 176
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit2: TDBEdit [19]
    Tag = 1
    Left = 176
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = DataSource2
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [20]
    Tag = 1
    Left = 176
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = DataSource3
    TabOrder = 12
  end
  object DBEdit4: TDBEdit [21]
    Tag = 1
    Left = 176
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = DataSource4
    TabOrder = 13
  end
  object DBEdit5: TDBEdit [22]
    Tag = 1
    Left = 176
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = DataSource5
    TabOrder = 14
  end
  object DBEdit6: TDBEdit [23]
    Tag = 1
    Left = 176
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource6
    TabOrder = 15
  end
  object edtLinha: TMaskEdit [24]
    Left = 104
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = edtLinhaExit
    OnKeyDown = edtLinhaKeyDown
  end
  object DBEdit8: TDBEdit [25]
    Tag = 1
    Left = 176
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = DataSource8
    TabOrder = 16
  end
  object edtGrupoEstoque: TMaskEdit [26]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtGrupoEstoqueExit
    OnKeyDown = edtGrupoEstoqueKeyDown
  end
  object DBEdit12: TDBEdit [27]
    Tag = 1
    Left = 176
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmGrupoEstoque'
    DataSource = DataSource12
    TabOrder = 18
  end
  object edtGrade: TMaskEdit [28]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtGradeExit
    OnKeyDown = edtGradeKeyDown
  end
  object DBEdit7: TDBEdit [29]
    Tag = 1
    Left = 176
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmGrade'
    DataSource = DataSource7
    TabOrder = 19
  end
  object DBEdit9: TDBEdit [30]
    Tag = 1
    Left = 176
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource9
    TabOrder = 20
  end
  object edtEmpresa: TER2LookupMaskEdit [31]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  inherited ImageList1: TImageList
    Left = 872
    Top = 144
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 664
    Top = 192
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 184
    Top = 296
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmDepartamento, nCdDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 712
    Top = 224
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryDepartamento
    Left = 232
    Top = 304
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategoria, nCdCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 824
    Top = 200
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
  end
  object DataSource3: TDataSource
    DataSet = qryCategoria
    Left = 272
    Top = 312
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSubCategoria, nCdSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 792
    Top = 200
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
  end
  object DataSource4: TDataSource
    DataSet = qrySubCategoria
    Left = 328
    Top = 320
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmSegmento, nCdSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 712
    Top = 264
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
  end
  object DataSource5: TDataSource
    DataSet = qrySegmento
    Left = 384
    Top = 304
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca, nCdMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 752
    Top = 224
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object DataSource6: TDataSource
    DataSet = qryMarca
    Left = 472
    Top = 312
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha '
      'WHERE nCdLinha = :nPK')
    Left = 816
    Top = 304
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryLinha
    Left = 392
    Top = 360
  end
  object qryGrupoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoEstoque '
      '      ,cNmGrupoEstoque      '
      '  FROM GrupoEstoque'
      ' WHERE nCdGrupoEstoque = :nPK')
    Left = 776
    Top = 336
    object qryGrupoEstoquenCdGrupoEstoque: TIntegerField
      FieldName = 'nCdGrupoEstoque'
    end
    object qryGrupoEstoquecNmGrupoEstoque: TStringField
      FieldName = 'cNmGrupoEstoque'
      Size = 50
    end
  end
  object DataSource12: TDataSource
    DataSet = qryGrupoEstoque
    Left = 560
    Top = 400
  end
  object qryGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrade, cNmGrade'
      'FROM Grade'
      'WHERE nCdGrade = :nPK')
    Left = 592
    Top = 312
    object qryGradenCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryGradecNmGrade: TStringField
      FieldName = 'cNmGrade'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryGrade
    Left = 496
    Top = 320
  end
  object SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SPREL_POSICAO_ESTOQUE_GRADE_PLANILHA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdGrade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdGrupoEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDepartamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSubCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMarca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLinha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 248
    Top = 408
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      '   FROM Empresa'
      ' WHERE nCdEmpresa = :nPK'
      '')
    Left = 136
    Top = 464
  end
  object DataSource9: TDataSource
    DataSet = qryEmpresa
    Left = 152
    Top = 496
  end
end
