inherited frmAutorizaPropostaReneg: TfrmAutorizaPropostaReneg
  Left = 442
  Top = 128
  Width = 859
  Height = 543
  Caption = 'Libera'#231#227'o Proposta Renegocia'#231#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 843
    Height = 476
  end
  inherited ToolBar1: TToolBar
    Width = 843
    ButtonWidth = 76
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 76
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 84
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 841
    Height = 476
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    DrawMemoText = True
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPropostaReneg'
        Footers = <>
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 224
      end
      item
        Alignment = taCenter
        EditButtons = <>
        FieldName = 'cRenegMultipla'
        Footers = <>
        Tag = 1
        Title.Caption = 'Propostas Aguardando Libera'#231#227'o|Reneg. M'#250'ltipla'
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'dDtProposta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidade'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuarioGeracao'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 224
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtraso'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'iParcelas'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nValOriginal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValJuros'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDescMaxOriginal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDescMaxJuros'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDescOriginal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDescJuros'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoNegociado'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValEntrada'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValEntradaMin'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValParcela'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValParcelaMin'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cOBS'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 216
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT PropostaReneg.nCdPropostaReneg '
      '      ,dbo.fn_ZeroEsquerda(PropostaReneg.nCdLoja,3) as nCdLoja'
      '      ,PropostaReneg.nCdTerceiro '
      '      ,Terceiro.cNmTerceiro'
      '      ,PropostaReneg.dDtProposta             '
      '      ,PropostaReneg.nCdUsuarioGeracao '
      '      ,Usuario.cNmUsuario'
      '      ,PropostaReneg.dDtValidade             '
      '      ,PropostaReneg.iDiasAtraso '
      '      ,PropostaReneg.iParcelas   '
      '      ,PropostaReneg.nValOriginal                            '
      '      ,PropostaReneg.nValJuros                               '
      '      ,PropostaReneg.nValDescMaxOriginal'
      '      ,PropostaReneg.nValDescOriginal                        '
      '      ,PropostaReneg.nValDescMaxJuros                        '
      '      ,PropostaReneg.nValDescJuros                           '
      '      ,PropostaReneg.nSaldoNegociado'
      '      ,PropostaReneg.nValEntrada'
      '      ,PropostaReneg.nValEntradaMin                          '
      '      ,PropostaReneg.nValParcela                             '
      '      ,PropostaReneg.nValParcelaMin'
      '      ,PropostaReneg.cOBS'
      '      ,CASE WHEN cFlgRenegMultipla = 1'
      '            THEN '#39'Sim'#39
      '            ELSE '#39'N'#227'o'#39
      '       END AS cRenegMultipla'
      '  FROM PropostaReneg'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = PropostaRen' +
        'eg.nCdTerceiro'
      
        '       INNER JOIN Usuario  ON Usuario.nCdUsuario   = PropostaRen' +
        'eg.nCdUsuarioGeracao'
      ' WHERE PropostaReneg.nCdEmpresa                = :nCdEmpresa'
      '   AND ((PropostaReneg.nCdLoja = @nCdLoja) OR (@nCdLoja = 0))'
      '   AND PropostaReneg.nCdTabStatusPropostaReneg = 1'
      ' ORDER BY PropostaReneg.dDtProposta')
    Left = 304
    Top = 216
    object qryMasternCdPropostaReneg: TAutoIncField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|C'#243'd.'
      FieldName = 'nCdPropostaReneg'
      ReadOnly = True
    end
    object qryMasternCdLoja: TStringField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Loja'
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryMasternCdTerceiro: TIntegerField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Cliente|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Cliente|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasterdDtProposta: TDateTimeField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Dt. Proposta'
      FieldName = 'dDtProposta'
    end
    object qryMasternCdUsuarioGeracao: TIntegerField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Usu'#225'rio Gera'#231#227'o|C'#243'd'
      FieldName = 'nCdUsuarioGeracao'
    end
    object qryMastercNmUsuario: TStringField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Usu'#225'rio Gera'#231#227'o|Nome'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryMasterdDtValidade: TDateTimeField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Dt. Validade'
      FieldName = 'dDtValidade'
    end
    object qryMasteriDiasAtraso: TIntegerField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Dias atraso'
      FieldName = 'iDiasAtraso'
    end
    object qryMasteriParcelas: TIntegerField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Parcelas'
      FieldName = 'iParcelas'
    end
    object qryMasternValOriginal: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Valor Original'
      FieldName = 'nValOriginal'
      Precision = 12
      Size = 2
    end
    object qryMasternValJuros: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Valor Juros'
      FieldName = 'nValJuros'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescMaxOriginal: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Desconto M'#225'ximo|Original'
      FieldName = 'nValDescMaxOriginal'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescOriginal: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Desconto Proposto|Original'
      FieldName = 'nValDescOriginal'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescMaxJuros: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Desconto M'#225'ximo|Juros'
      FieldName = 'nValDescMaxJuros'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescJuros: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Desconto Proposto|Juros'
      FieldName = 'nValDescJuros'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoNegociado: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Valor Negociado'
      FieldName = 'nSaldoNegociado'
      Precision = 12
      Size = 2
    end
    object qryMasternValEntrada: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Entrada|Proposto'
      FieldName = 'nValEntrada'
      Precision = 12
      Size = 2
    end
    object qryMasternValEntradaMin: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Entrada|M'#237'nimo'
      FieldName = 'nValEntradaMin'
      Precision = 12
      Size = 2
    end
    object qryMasternValParcela: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Valor Parcela|Proposto'
      FieldName = 'nValParcela'
      Precision = 12
      Size = 2
    end
    object qryMasternValParcelaMin: TBCDField
      DisplayLabel = 'Propostas Aguardando Libera'#231#227'o|Valor Parcela|M'#237'nimo'
      FieldName = 'nValParcelaMin'
      Precision = 12
      Size = 2
    end
    object qryMastercOBS: TMemoField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryMastercRenegMultipla: TStringField
      FieldName = 'cRenegMultipla'
      ReadOnly = True
      Size = 3
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 304
    Top = 248
  end
  object PopupMenu1: TPopupMenu
    Left = 384
    Top = 248
    object LiberarProposta1: TMenuItem
      Caption = 'Liberar Proposta'
      OnClick = LiberarProposta1Click
    end
    object CancelarProposta1: TMenuItem
      Caption = 'Cancelar Proposta'
      OnClick = CancelarProposta1Click
    end
    object CancelarPropostasMltiplas1: TMenuItem
      Caption = 'Cancelar Propostas M'#250'ltiplas'
      OnClick = CancelarPropostasMltiplas1Click
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 344
    Top = 216
  end
end
