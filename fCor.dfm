inherited frmCor: TfrmCor
  Left = -8
  Top = -8
  Width = 1296
  Height = 786
  Caption = 'Cores'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1280
    Height = 725
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 31
    Top = 70
    Width = 70
    Height = 13
    Caption = 'Descri'#231#227'o Cor'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 94
    Width = 93
    Height = 13
    Caption = 'Cor Predominante'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 1280
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCor'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 104
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmCor'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 104
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdCorPredom'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 176
    Top = 88
    Width = 577
    Height = 19
    DataField = 'cNmCor'
    DataSource = DataSource1
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM COR'
      'WHERE nCdCor = :nPK'
      'AND iNivel = 1')
    object qryMasternCdCor: TAutoIncField
      FieldName = 'nCdCor'
    end
    object qryMastercNmCor: TStringField
      FieldName = 'cNmCor'
      Size = 50
    end
    object qryMastercNmCorPredom: TStringField
      FieldName = 'cNmCorPredom'
      Size = 50
    end
    object qryMasteriNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryMasternCdCorPredom: TIntegerField
      FieldName = 'nCdCorPredom'
    end
  end
  object qryCorPredom: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCor'
      'FROM Cor'
      'WHERE nCdCor = :nPK'
      'AND nCdCorPredom IS NULL'
      'AND iNivel = 1')
    Left = 416
    Top = 288
    object qryCorPredomcNmCor: TStringField
      FieldName = 'cNmCor'
      Size = 100
    end
  end
  object DataSource1: TDataSource
    DataSet = qryCorPredom
    Left = 624
    Top = 368
  end
  object qryVerificaDup: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCor'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNmCor'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 100
        Size = 100
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM Cor'
      'WHERE nCdCor <> :nCdCor'
      'AND cNmCor = :cNmCor'
      'AND nCdCorPredom IS NOT NULL')
    Left = 400
    Top = 176
  end
end
