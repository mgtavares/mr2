inherited frmTipoBloqueioCliente: TfrmTipoBloqueioCliente
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Tipo Bloqueio Cliente'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 789
  end
  object Label1: TLabel [1]
    Left = 48
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 37
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 1152
    inherited ToolButton50: TToolButton
      Left = 465
      Wrap = False
    end
    inherited btCancelar: TToolButton
      Left = 473
      Top = 0
    end
    inherited ToolButton11: TToolButton
      Left = 562
      Top = 0
    end
    inherited btConsultar: TToolButton
      Left = 570
      Top = 0
    end
    inherited ToolButton8: TToolButton
      Left = 659
      Top = 0
    end
    inherited ToolButton5: TToolButton
      Left = 667
      Top = 0
    end
    inherited ToolButton4: TToolButton
      Left = 756
      Top = 0
    end
    inherited ToolButton9: TToolButton
      Left = 764
      Top = 0
    end
    inherited btnAuditoria: TToolButton
      Left = 772
      Top = 0
    end
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 89
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoRestricaoVenda'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 89
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoRestricaoVenda'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 24
    Top = 120
    Width = 713
    Height = 313
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    ClientRectBottom = 309
    ClientRectLeft = 4
    ClientRectRight = 709
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios que podem desbloquear este tipo de bloqueio'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 705
        Height = 285
        Align = alClient
        DataSource = dsUsuarioTipoRestricao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoDiverg'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 483
          end>
      end
    end
  end
  object DBCheckBox1: TDBCheckBox [7]
    Left = 89
    Top = 88
    Width = 272
    Height = 17
    Caption = 'Permite o gerente liberar no momento da venda'
    DataField = 'cFlgGerenteLibera'
    DataSource = dsMaster
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoRestricaoVenda'
      'WHERE nCdTabTipoRestricaoVenda = :nPK'
      'AND nCdTabTipoRestricaoVenda > 100')
    object qryMasternCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
    object qryMastercNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
    object qryMastercFlgGerenteLibera: TIntegerField
      FieldName = 'cFlgGerenteLibera'
    end
  end
  object qryUsuarioTipoRestricao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioTipoRestricaoBeforePost
    OnCalcFields = qryUsuarioTipoRestricaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UsuarioTabTipoRestricaoVenda'
      ' WHERE nCdTabTipoRestricaoVenda = :nPK')
    Left = 196
    Top = 224
    object qryUsuarioTipoRestricaonCdUsuarioTabTipoRestricaoVenda: TAutoIncField
      FieldName = 'nCdUsuarioTabTipoRestricaoVenda'
    end
    object qryUsuarioTipoRestricaonCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
    object qryUsuarioTipoRestricaonCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rios|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioTipoRestricaocNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rios|Nome Usu'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
  end
  object dsUsuarioTipoRestricao: TDataSource
    DataSet = qryUsuarioTipoRestricao
    Left = 228
    Top = 224
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      ',cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 432
    Top = 200
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
