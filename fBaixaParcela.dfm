inherited frmBaixaParcela: TfrmBaixaParcela
  Left = 83
  Top = 75
  Width = 1152
  Height = 812
  Caption = 'Baixa Parcela a Receber'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 121
    Width = 1136
    Height = 655
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 97
    inherited ToolButton1: TToolButton
      Caption = '&Exibir Parcela'
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 97
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 105
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 121
    Width = 1136
    Height = 655
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 651
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Parcelas deste Cliente'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 627
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource2
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValAbatimento'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValLiq'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1136
    Height = 92
    Align = alTop
    Caption = 'Cliente'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 24
      Top = 28
      Width = 33
      Height = 13
      Caption = 'Cliente'
    end
    object MaskEdit6: TMaskEdit
      Left = 60
      Top = 20
      Width = 93
      Height = 21
      TabOrder = 0
      OnExit = MaskEdit6Exit
      OnKeyDown = MaskEdit6KeyDown
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 159
      Top = 20
      Width = 124
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 285
      Top = 20
      Width = 649
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 2
    end
    object cxButton1: TcxButton
      Left = 60
      Top = 48
      Width = 156
      Height = 32
      Caption = 'Exibir Parcelas'
      TabOrder = 3
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
  end
  inherited ImageList1: TImageList
    Left = 520
    Top = 80
  end
  object qryParcelaAberto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cSenso'
        DataType = ftString
        Precision = 1
        Size = 1
        Value = 'R'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      ' SELECT TIT.nCdTitulo'
      '          ,TIT.nCdEspTit'
      '          ,cNmEspTit'
      '          ,TIT.nCdSP'
      '          ,TIT.cNrTit'
      '          ,TIT.iParcela'
      '          ,TIT.nCdTerceiro'
      '          ,cNmTerceiro'
      '          ,TIT.nCdMoeda'
      '          ,Moeda.cSigla'
      '          ,TIT.cSenso'
      '          ,TIT.dDtVenc'
      '          ,TIT.nValTit'
      '          ,TIT.nSaldoTit'
      '          ,Tit.dDtCancel'
      '          ,EMP.cSigla as cSiglaEmp'
      '          ,TIT.cNrNF'
      '          ,ProvisaoTit.nCdProvisaoTit'
      '          ,ProvisaoTit.dDtPagto'
      '          ,Tit.nCdLojaTit'
      '          ,nValAbatimento'
      '          ,nValDesconto'
      '          ,nValLiq'
      
        '           ,ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmp' +
        'Imobiliario'
      '      FROM Titulo TIT'
      
        '           LEFT  JOIN EspTit              ON EspTit.nCdEspTit   ' +
        '              = TIT.nCdEspTit'
      
        '           LEFT  JOIN Terceiro           ON Terceiro.nCdTerceiro' +
        '           = TIT.nCdTerceiro'
      
        '           LEFT  JOIN Moeda             ON Moeda.nCdMoeda       ' +
        '        = TIT.nCdMoeda'
      
        '           LEFT  JOIN Empresa EMP ON EMP.nCdEmpresa             ' +
        '   = TIT.nCdEmpresa'
      
        '           LEFT  JOIN ProvisaoTit      ON ProvisaoTit.nCdProvisa' +
        'oTit = Tit.nCdProvisaoTit'
      
        '           LEFT  JOIN ContratoEmpImobiliario                    ' +
        ' ON ContratoEmpImobiliario.nCdTerceiro         = Tit.nCdTerceiro' +
        ' AND ContratoEmpImobiliario.cNumContrato = TIT.cNrTit '
      
        '           LEFT  JOIN BlocoEmpImobiliario                       ' +
        '   ON BlocoEmpImobiliario.nCdBlocoEmpImobiliario = ContratoEmpIm' +
        'obiliario.nCdBlocoEmpImobiliario'
      
        '           LEFT  JOIN EmpImobiliario                            ' +
        '       ON EmpImobiliario.nCdEmpImobiliario           = BlocoEmpI' +
        'mobiliario.nCdEmpImobiliario'
      
        '           LEFT  JOIN ParcContratoEmpImobiliario              ON' +
        ' ParcContratoEmpImobiliario.nCdParcContratoEmpImobiliario       ' +
        '        = TIT.nCdParcContratoEmpImobiliario'
      
        '           LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTi' +
        'poParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliari' +
        'o = ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobilia' +
        'rio'
      ''
      '     WHERE Tit.dDtCancel IS NULL'
      '           AND dDtBloqTit    IS NULL'
      '           AND dDtLiq         IS NULL'
      '           AND nSaldoTit > 0'
      '           AND cSenso                  =:cSenso'
      '           AND TIT.nCdEmpresa    =:nCdEmpresa'
      '           AND TIT.nCdTerceiro    =:nCdTerceiro'
      
        '           AND ( (EspTit.cFlgPrev = 0) OR (EspTit.cFlgPrev = 1 A' +
        'ND TIT.nCdSP IS NULL))'
      '     ORDER BY TIT.dDtVenc'
      ' ')
    Left = 672
    Top = 232
    object qryParcelaAbertonCdTitulo: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|ID T'#237'tulo'
      FieldName = 'nCdTitulo'
    end
    object qryParcelaAbertonCdEspTit: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdEspTit'
    end
    object qryParcelaAbertocNmEspTit: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryParcelaAbertonCdSP: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdSP'
    end
    object qryParcelaAbertocNrTit: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|N'#250'mero T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryParcelaAbertoiParcela: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Parcela'
      FieldName = 'iParcela'
    end
    object qryParcelaAbertonCdTerceiro: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdTerceiro'
    end
    object qryParcelaAbertocNmTerceiro: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryParcelaAbertonCdMoeda: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdMoeda'
    end
    object qryParcelaAbertocSigla: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryParcelaAbertocSenso: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryParcelaAbertodDtVenc: TDateTimeField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Data Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryParcelaAbertonValTit: TBCDField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Valor T'#237'tulo'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryParcelaAbertonSaldoTit: TBCDField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryParcelaAbertodDtCancel: TDateTimeField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'dDtCancel'
    end
    object qryParcelaAbertocSiglaEmp: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cSiglaEmp'
      FixedChar = True
      Size = 5
    end
    object qryParcelaAbertocNrNF: TStringField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryParcelaAbertonCdProvisaoTit: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdProvisaoTit'
    end
    object qryParcelaAbertodDtPagto: TDateTimeField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'dDtPagto'
    end
    object qryParcelaAbertonCdLojaTit: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdLojaTit'
    end
    object qryParcelaAbertonValAbatimento: TBCDField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Abatimento'
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryParcelaAbertonValDesconto: TBCDField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Desconto'
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryParcelaAbertonValLiq: TBCDField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|Valor Pago'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryParcelaAbertonCdTabTipoParcContratoEmpImobiliario: TIntegerField
      DisplayLabel = 'Parcelas em Aberto deste Cliente|'
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryParcelaAberto
    Left = 712
    Top = 248
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 720
    Top = 72
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 760
    Top = 72
  end
  object qryParcelasPendentes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end
      item
        Name = 'iParcela'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT  Titulo.nCdTitulo'
      '       ,Titulo.iparcela'
      '       ,Titulo.nValIndiceCorrecao'
      '       ,Titulo.nValCorrecao'
      '       ,Titulo.nSaldoTit'
      '       ,Titulo.nValTit'
      
        '       ,ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImob' +
        'iliario'
      '  FROM Titulo'
      
        '  LEFT  JOIN ContratoEmpImobiliario            ON ContratoEmpImo' +
        'biliario.nCdTerceiro         = Titulo.nCdTerceiro AND ContratoEm' +
        'pImobiliario.cNumContrato = Titulo.cNrTit'
      
        '  LEFT  JOIN BlocoEmpImobiliario               ON BlocoEmpImobil' +
        'iario.nCdBlocoEmpImobiliario = ContratoEmpImobiliario.nCdBlocoEm' +
        'pImobiliario'
      
        '  LEFT  JOIN EmpImobiliario                    ON EmpImobiliario' +
        '.nCdEmpImobiliario           = BlocoEmpImobiliario.nCdEmpImobili' +
        'ario'
      
        '  LEFT  JOIN ParcContratoEmpImobiliario        ON ParcContratoEm' +
        'pImobiliario.nCdParcContratoEmpImobiliario               = Titul' +
        'o.nCdParcContratoEmpImobiliario'
      
        '  LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcC' +
        'ontratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      ''
      ' WHERE Titulo.dDtLiq is null'
      '       AND Titulo.nSaldoTit   > 0'
      '       AND Titulo.nCdterceiro = :nCdTerceiro'
      '       AND Titulo.cNrTit      = :cNrTit'
      '       AND Titulo.iparcela    < :iParcela'
      ' ORDER BY Titulo.iParcela'
      '')
    Left = 772
    Top = 289
    object qryParcelasPendentesnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryParcelasPendentesiparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryParcelasPendentesnValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryParcelasPendentesnValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryParcelasPendentesnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryParcelasPendentesnValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryParcelasPendentesnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
  end
end
