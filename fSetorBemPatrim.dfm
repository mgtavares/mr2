inherited frmSetorBemPatrim: TfrmSetorBemPatrim
  Left = 207
  Top = 121
  Caption = 'Setor Bem Patrimonial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 52
    Top = 39
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 11
    Top = 65
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Setor'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 36
    Width = 65
    Height = 19
    DataField = 'nCdSetorBemPatrim'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 60
    Width = 650
    Height = 19
    DataField = 'cNmSetorBemPatrim'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 8
    Top = 104
    Width = 593
    Height = 193
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 193
    ClientRectRight = 593
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Local'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 593
        Height = 170
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLocalBemPatrim
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLocalBemPatrim'
            Footers = <>
            Title.Caption = 'C'#243'd'
          end
          item
            EditButtons = <>
            FieldName = 'cNmLocalBemPatrim'
            Footers = <>
            Title.Caption = 'Descri'#231#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM SetorBemPatrim'
      'WHERE nCdSetorBemPatrim = :nPK'
      ''
      '')
    object qryMasternCdSetorBemPatrim: TIntegerField
      FieldName = 'nCdSetorBemPatrim'
    end
    object qryMastercNmSetorBemPatrim: TStringField
      FieldName = 'cNmSetorBemPatrim'
      Size = 50
    end
  end
  object qryLocalBemPatrim: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLocalBemPatrimBeforePost
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from LocalBemPatrim '
      'where nCdSetorBemPatrim = :nPk')
    Left = 624
    Top = 352
    object qryLocalBemPatrimnCdLocalBemPatrim: TAutoIncField
      FieldName = 'nCdLocalBemPatrim'
      ReadOnly = True
    end
    object qryLocalBemPatrimcNmLocalBemPatrim: TStringField
      FieldName = 'cNmLocalBemPatrim'
      Size = 50
    end
    object qryLocalBemPatrimnCdSetorBemPatrim: TIntegerField
      FieldName = 'nCdSetorBemPatrim'
    end
  end
  object dsLocalBemPatrim: TDataSource
    DataSet = qryLocalBemPatrim
    Left = 656
    Top = 352
  end
end
