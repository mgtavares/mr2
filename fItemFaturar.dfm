inherited frmItemFaturar: TfrmItemFaturar
  Left = 160
  Top = 166
  Width = 940
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Itens a Faturar'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 932
  end
  inherited ToolBar1: TToolBar
    Width = 932
    ButtonWidth = 136
    Font.Name = 'Tahoma'
    inherited ToolButton1: TToolButton
      Caption = 'Gerar Faturamento'
      Enabled = False
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 136
    end
    inherited ToolButton2: TToolButton
      Left = 144
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 932
    Height = 444
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnKeyDown = cxGrid1DBTableView1KeyDown
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1ItemdoPedidoCdItem: TcxGridDBColumn
        Caption = 'C'#243'd Item'
        DataBinding.FieldName = 'Item do Pedido|C'#243'd Item'
      end
      object cxGrid1DBTableView1ProdutoCd: TcxGridDBColumn
        Caption = 'C'#243'd Produto'
        DataBinding.FieldName = 'Produto|C'#243'd'
        Width = 94
      end
      object cxGrid1DBTableView1ProdutoDescrio: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'Produto|Descri'#231#227'o'
        Width = 439
      end
      object cxGrid1DBTableView1ItemdoPedidoQtdeAutorizada: TcxGridDBColumn
        Caption = 'Quantidade Liberada'
        DataBinding.FieldName = 'Item do Pedido|Qtde Autorizada'
        Width = 124
      end
      object cxGrid1DBTableView1ItemdoPedidoTipodeItem: TcxGridDBColumn
        Caption = 'Tipo de Item'
        DataBinding.FieldName = 'Item do Pedido|Tipo de Item'
        Width = 195
      end
      object cxGrid1DBTableView1ItemdoPedidoStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'Item do Pedido|Status'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 344
    Top = 128
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemPedido.nCdItemPedido        "Item do Pedido|C'#243'd Item"'
      '      ,ItemPedido.nCdProduto           "Produto|C'#243'd"'
      '      ,ItemPedido.cNmItem              "Produto|Descri'#231#227'o"'
      
        '      ,cNmTipoItemPed                  "Item do Pedido|Tipo de I' +
        'tem"'
      
        '      ,ItemPedido.nQtdeLibFat          "Item do Pedido|Qtde Auto' +
        'rizada"'
      '      ,cNmTabStatusItemPed             "Item do Pedido|Status"'
      '      ,TipoPedido.cFlgDevolucCompra'
      '      ,TipoPedido.cFlgDevolucVenda'
      '  FROM ItemPedido'
      
        '       INNER JOIN TabStatusItemPed TSIP ON TSIP.nCdTabStatusItem' +
        'Ped = ItemPedido.nCdTabStatusItemPed'
      
        '       INNER JOIN TipoItemPed      TIP  ON TIP.nCdTipoItemPed   ' +
        '    = ItemPedido.nCdTipoItemPed'
      
        '       INNER JOIN Pedido                ON Pedido.nCdPedido     ' +
        '    = ItemPedido.nCdPedido'
      
        '       INNER JOIN TipoPedido            ON TipoPedido.nCdTipoPed' +
        'ido = Pedido.nCdTipoPedido'
      ' WHERE ItemPedido.nCdPedido = :nPK'
      '   AND ItemPedido.nCdTipoItemPed IN (1,2,4,5,6)'
      '   AND nQtdeLibFat > 0')
    Left = 376
    Top = 112
    object qryItemPedidoItemdoPedidoCdItem: TAutoIncField
      FieldName = 'Item do Pedido|C'#243'd Item'
      ReadOnly = True
    end
    object qryItemPedidoProdutoCd: TIntegerField
      FieldName = 'Produto|C'#243'd'
    end
    object qryItemPedidoProdutoDescrio: TStringField
      FieldName = 'Produto|Descri'#231#227'o'
      Size = 150
    end
    object qryItemPedidoItemdoPedidoTipodeItem: TStringField
      FieldName = 'Item do Pedido|Tipo de Item'
      Size = 50
    end
    object qryItemPedidoItemdoPedidoStatus: TStringField
      FieldName = 'Item do Pedido|Status'
      Size = 50
    end
    object qryItemPedidoItemdoPedidoQtdeAutorizada: TBCDField
      FieldName = 'Item do Pedido|Qtde Autorizada'
      Precision = 12
      Size = 2
    end
    object qryItemPedidocFlgDevolucCompra: TIntegerField
      FieldName = 'cFlgDevolucCompra'
    end
    object qryItemPedidocFlgDevolucVenda: TIntegerField
      FieldName = 'cFlgDevolucVenda'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryItemPedido
    Left = 408
    Top = 112
  end
  object uSP_FATURA_PEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_FATURA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtFatur'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nPercC'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nPercN'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 376
    Top = 144
  end
  object SP_FATURA_PEDIDO_DEVOLUCAO: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_FATURA_PEDIDO_DEVOLUCAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtFatur'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nPercC'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nPercN'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 408
    Top = 144
  end
  object SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtFatur'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nPercC'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nPercN'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 440
    Top = 144
  end
end
