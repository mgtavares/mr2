unit fCaixa_LiberaRestricao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, jpeg, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxButtonEdit, cxCheckBox, cxHyperLinkEdit,
  cxMemo, StdCtrls;

type
  TfrmCaixa_LiberaRestricao = class(TfrmProcesso_Padrao)
    qryTemp_RestricaoVenda: TADOQuery;
    qryTemp_RestricaoVendanCdTemp_Pagto: TIntegerField;
    qryTemp_RestricaoVendanCdTabTipoRestricaoVenda: TIntegerField;
    qryTemp_RestricaoVendanCdUsuarioAutor: TIntegerField;
    qryTemp_RestricaoVendadDtAutor: TDateTimeField;
    qryTemp_RestricaoVendacJustificativa: TMemoField;
    dsTemp_RestricaoVenda: TDataSource;
    qryTabTipoRestricao: TADOQuery;
    qryTabTipoRestricaocNmTabTipoRestricaoVenda: TStringField;
    qryTemp_RestricaoVendacFlgLiberado: TIntegerField;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    Image2: TImage;
    cxGridDBTableView2nCdTemp_Pagto: TcxGridDBColumn;
    cxGridDBTableView2nCdTabTipoRestricaoVenda: TcxGridDBColumn;
    cxGridDBTableView2nCdUsuarioAutor: TcxGridDBColumn;
    cxGridDBTableView2dDtAutor: TcxGridDBColumn;
    cxGridDBTableView2cNmTabTipoRestricaoVenda: TcxGridDBColumn;
    cxGridDBTableView2cFlgLiberado: TcxGridDBColumn;
    qryTemp_RestricaoVendacTemporario: TStringField;
    qryTemp_RestricaoVendanCdTempRestricaoVenda: TAutoIncField;
    qryTemp_RestricaoVendacNmTabTipoRestricaoVenda: TStringField;
    qryTemp_RestricaoVendacFlgGerenteLibera: TIntegerField;
    cxGridDBTableView2DBColumn1: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    Image3: TImage;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure qryTemp_RestricaoVendaCalcFields(DataSet: TDataSet);
    function LiberaRestricoes(): boolean;
    procedure LiberaRestricao;
    procedure cxGridDBTableView2EditKeyPress(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      AEdit: TcxCustomEdit; var Key: Char);
    procedure cxGridDBTableView2StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTemp_Pagto   : integer ;
    nCdUsuarioAutor : integer ;
  end;

var
  frmCaixa_LiberaRestricao: TfrmCaixa_LiberaRestricao;

implementation

uses fCaixa_Justificativa, fMenu;

{$R *.dfm}

procedure TfrmCaixa_LiberaRestricao.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryTemp_RestricaoVenda,intToStr(nCdTemp_Pagto)) ;

  qryTemp_RestricaoVenda.First;
  
end;

procedure TfrmCaixa_LiberaRestricao.cxGridDBTableView2DblClick(
  Sender: TObject);
begin
  inherited;

  LiberaRestricao;
end;

procedure TfrmCaixa_LiberaRestricao.qryTemp_RestricaoVendaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTemp_RestricaoVendacNmTabTipoRestricaoVenda.Value = '') then
  begin
      qryTabTipoRestricao.Close ;
      PosicionaQuery(qryTabTipoRestricao, qryTemp_RestricaoVendanCdTabTipoRestricaoVenda.AsString) ;

      if not qryTabTipoRestricao.eof then
          qryTemp_RestricaoVendacNmTabTipoRestricaoVenda.Value := qryTabTipoRestricaocNmTabTipoRestricaoVenda.Value ;
          
  end ;

end;

function TfrmCaixa_LiberaRestricao.LiberaRestricoes: boolean;
var
    iQtdeSemLiberacao : integer ;
begin

    Self.ShowModal ;

    iQtdeSemLiberacao := 0;

    qryTemp_RestricaoVenda.First ;

    while not qryTemp_RestricaoVenda.eof do
    begin
        if (qryTemp_RestricaoVendacFlgLiberado.Value = 0) then
            iQtdeSemLiberacao := iQtdeSemLiberacao + 1;

        qryTemp_RestricaoVenda.Next ;
    end ;

    qryTemp_RestricaoVenda.First ;

    Result := (iQtdeSemLiberacao = 0) ;

end;

procedure TfrmCaixa_LiberaRestricao.LiberaRestricao;
var
  objCaixa_Justificativa : TfrmCaixa_Justificativa ;
begin

  if (qryTemp_RestricaoVendacFlgGerenteLibera.Value = 0) then
  begin
      MensagemErro('Voc� n�o tem permiss�o para liberar este tipo de restri��o.') ;
      exit ;
  end ;

  objCaixa_Justificativa := TfrmCaixa_Justificativa.Create( Self ) ;
  objCaixa_Justificativa.MemoJustificativa.Text := qryTemp_RestricaoVendacJustificativa.Value ;
  objCaixa_Justificativa.ShowModal ;

  if (objCaixa_Justificativa.MemoJustificativa.GetTextLen > 0) then
  begin
      qryTemp_RestricaoVenda.Edit;
      qryTemp_RestricaoVendadDtAutor.Value        := Now() ;
      qryTemp_RestricaoVendanCdUsuarioAutor.Value := nCdUsuarioAutor ;
      qryTemp_RestricaoVendacJustificativa.Value  := AnsiUpperCase(objCaixa_Justificativa.MemoJustificativa.Text) ;
      qryTemp_RestricaoVendacFlgLiberado.Value    := 1 ;
      qryTemp_RestricaoVenda.Post;

      qryTemp_RestricaoVenda.Next;
  end ;

  freeAndNil( objCaixa_Justificativa ) ;

end;

procedure TfrmCaixa_LiberaRestricao.cxGridDBTableView2EditKeyPress(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Char);
begin
  inherited;

  if (Key = #13) then
      LiberaRestricoes;
      
end;

procedure TfrmCaixa_LiberaRestricao.cxGridDBTableView2StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGridDBTableView2cNmTabTipoRestricaoVenda) then
  begin

      if (ARecord.Values[6] = 0) then
          AStyle := frmMenu.LinhaVermelha ;

  end ;

end;

end.
