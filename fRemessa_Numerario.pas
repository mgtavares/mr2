unit fRemessa_Numerario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, RDprint, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmRemessa_Numerarios = class(TfrmCadastro_Padrao)
    qryMasternCdRemessaNum: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdContaOrigem: TIntegerField;
    qryMasternCdContaDestino: TIntegerField;
    qryMasternValDinheiro: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtRemessa: TDateTimeField;
    qryMasterdDtFech: TDateTimeField;
    qryMastercOBS: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryContaOrigem: TADOQuery;
    qryContaOrigemnCdContaBancaria: TIntegerField;
    qryContaOrigemnCdConta: TStringField;
    qryContaDestino: TADOQuery;
    qryContaDestinonCdContaBancaria: TIntegerField;
    qryContaDestinonCdConta: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit7: TDBEdit;
    dsContaOrigem: TDataSource;
    DBEdit11: TDBEdit;
    dsContaDestino: TDataSource;
    DBEdit12: TDBEdit;
    dsEmpresa: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    DBEdit13: TDBEdit;
    dsLoja: TDataSource;
    qryChequeRemessaNum: TADOQuery;
    qryChequeRemessaNumnCdChequeRemessaNum: TAutoIncField;
    qryChequeRemessaNumnCdRemessaNum: TIntegerField;
    qryChequeRemessaNumnCdCheque: TIntegerField;
    qryChequeRemessaNumcFlgAux: TIntegerField;
    qryCrediarioRemessaNum: TADOQuery;
    qryCrediarioRemessaNumnCdCrediarioRemessaNum: TAutoIncField;
    qryCrediarioRemessaNumnCdRemessaNum: TIntegerField;
    qryCrediarioRemessaNumnCdCrediario: TIntegerField;
    qryCrediarioRemessaNumcFlgAux: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh3: TDBGridEh;
    dsChequeRemessaNum: TDataSource;
    dsCartaoRemessaNum: TDataSource;
    dsCrediarioRemessaNum: TDataSource;
    qryChequeRemessaNumnCdBanco: TStringField;
    qryChequeRemessaNumcAgencia: TStringField;
    qryChequeRemessaNumiNrCheque: TIntegerField;
    qryChequeRemessaNumnValCheque: TFloatField;
    qryAux: TADOQuery;
    qryCheque: TADOQuery;
    qryChequenCdBanco: TIntegerField;
    qryChequecAgencia: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryTransacaoCartao: TADOQuery;
    qryTransacaoCartaocNmOperadoraCartao: TStringField;
    qryTransacaoCartaoiNrDocto: TIntegerField;
    qryTransacaoCartaoiNrAutorizacao: TIntegerField;
    qryTransacaoCartaodDtTransacao: TDateTimeField;
    qryTransacaoCartaonValTransacao: TBCDField;
    qryCrediario: TADOQuery;
    qryCrediarionCdLanctoFin: TIntegerField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediariocNmTerceiro: TStringField;
    qryCrediarionValCrediario: TBCDField;
    qryCrediarioRemessaNumnCdLanctoFin: TIntegerField;
    qryCrediarioRemessaNumdDtVenda: TDateField;
    qryCrediarioRemessaNumcNmTerceiro: TStringField;
    qryCrediarioRemessaNumnValCrediario: TFloatField;
    qryUsuarioConfirma: TADOQuery;
    qryUsuarioConfirmacNmUsuario: TStringField;
    Label7: TLabel;
    DBEdit14: TDBEdit;
    dsUsuarioConfirma: TDataSource;
    Label11: TLabel;
    DBEdit15: TDBEdit;
    dsUsuario: TDataSource;
    qryMasterdDtCancel: TDateTimeField;
    qryMasterdDtConfirma: TDateTimeField;
    qryMasternCdUsuarioConfirma: TIntegerField;
    DBEdit16: TDBEdit;
    Label8: TLabel;
    DBEdit17: TDBEdit;
    qryGeraLanctoRemessaDinheiro: TADOQuery;
    btEncerrar: TcxButton;
    btImprimir: TcxButton;
    btCancelarRem: TcxButton;
    qryGerente: TADOQuery;
    qryGerentecFlgGerente: TIntegerField;
    qryContaDestinonCdLoja: TIntegerField;
    qryGeraEstornoDinheiro: TADOQuery;
    qryContaOrigemnSaldoConta: TBCDField;
    qryCartao: TADOQuery;
    qryCartaodDtTransacao: TDateTimeField;
    qryCartaocNmOperadoraCartao: TStringField;
    qryCartaoiParcelas: TIntegerField;
    qryCartaoiNrDocto: TIntegerField;
    qryCartaoiNrAutorizacao: TIntegerField;
    qryCartaonValTransacao: TBCDField;
    qryCartaocStatus: TStringField;
    qryCheques: TADOQuery;
    qryChequesnCdBanco: TIntegerField;
    qryChequescAgencia: TStringField;
    qryChequescConta: TStringField;
    qryChequescDigito: TStringField;
    qryChequescCNPJCPF: TStringField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequescNmTerceiro: TStringField;
    qryChequescStatus: TStringField;
    qryRemessa: TADOQuery;
    qryRemessanCdRemessaNum: TIntegerField;
    qryRemessacNmEmpresa: TStringField;
    qryRemessacNmLoja: TStringField;
    qryRemessanCdContaOrigem: TStringField;
    qryRemessanCdContaDestino: TStringField;
    qryRemessanValDinheiro: TBCDField;
    qryRemessacNmUsuarioCad: TStringField;
    qryRemessadDtRemessa: TDateTimeField;
    qryRemessadDtFech: TDateTimeField;
    qryRemessadDtCancel: TDateTimeField;
    qryRemessacOBS: TStringField;
    qryRemessadDtConfirma: TDateTimeField;
    qryRemessacNmUsuarioConfirma: TStringField;
    qryCrediarios: TADOQuery;
    qryCrediariosnCdCrediario: TIntegerField;
    qryCrediariosdDtVenda: TDateTimeField;
    qryCrediarioscNmTerceiro: TStringField;
    qryCrediariosnValCrediario: TBCDField;
    qryCrediarioscStatus: TStringField;
    qryCartaocParcelado: TStringField;
    btVeriDocPend: TcxButton;
    RDprint1: TRDprint;
    SP_INCLUI_DOCUMENTOS_REMESSA: TADOStoredProc;
    DBGridEh2: TDBGridEh;
    qryCartaoRemessaNum: TADOQuery;
    qryCartaoRemessaNumnCdCartaoRemessaNum: TAutoIncField;
    qryCartaoRemessaNumnCdRemessaNum: TIntegerField;
    qryCartaoRemessaNumnCdTransacaoCartao: TIntegerField;
    qryCartaoRemessaNumcFlgAux: TIntegerField;
    qryCartaoRemessaNumcNmOperadora: TStringField;
    qryCartaoRemessaNumiNrDocto: TStringField;
    qryCartaoRemessaNumiNrAutorizacao: TStringField;
    qryCartaoRemessaNumdDtTransacao: TDateField;
    qryCartaoRemessaNumnValTransacao: TFloatField;
    qryContaOrigemcFlgBloqDinheiroRemessa: TIntegerField;
    qryGeraRemAuto: TADOQuery;
    qryGeraRemAutonCdRemessaNum: TIntegerField;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBGridEh3Enter(Sender: TObject);
    procedure qryChequeRemessaNumCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryCrediarioRemessaNumCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarRemClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton2Click(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure btVeriDocPendClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure qryMasterBeforeDelete(DataSet: TDataSet);
    procedure qryCartaoRemessaNumCalcFields(DataSet: TDataSet);
    procedure prGeraRemessaAuto(nCdContaBancaria : Integer);
  private
    { Private declarations }
    iQtdeComprovantes : integer ;
  public
    { Public declarations }
    bNovoRegistro : Boolean ;
    procedure ExibeComprovante();
    procedure ImprimeCabecalho(cTipo : string);
    procedure VerificaOperadora();
    procedure VerificaParcelas();
    procedure VerificaDocPend();
  end;

var
  frmRemessa_Numerarios: TfrmRemessa_Numerarios;
  iLinha, iPagina              : integer ;
  cOperadoraCartao, cParcelado : string;
  nValCartao                   : currency;

implementation

uses fMenu, fLookup_Padrao, fPdvSupervisor,
  fCaixa_Recebimento, Math;

{$R *.dfm}

procedure TfrmRemessa_Numerarios.prGeraRemessaAuto(nCdContaBancaria : Integer);
begin
    {-- Insere a remessa autom�ticamente --}
    qryGeraRemAuto.Close;
    qryGeraRemAuto.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
    qryGeraRemAuto.Parameters.ParamByName('nCdLoja').Value        := frmMenu.nCdLojaAtiva;
    qryGeraRemAuto.Parameters.ParamByName('nCdContaOrigem').Value := nCdContaBancaria;
    qryGeraRemAuto.Parameters.ParamByName('nCdUsuarioCad').Value  := frmMenu.nCdUsuarioLogado;
    qryGeraRemAuto.Open;

    {-- pega o id gerado da remessa e abre a tela de remessa para o usu�rio --}
    PosicionaQuery(qryMaster, qryGeraRemAutonCdRemessaNum.AsString);
    qryContaOrigem.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
    PosicionaQuery(qryContaOrigem, qryMasternCdContaOrigem.AsString);

    VerificaDocPend();
end;

procedure TfrmRemessa_Numerarios.btIncluirClick(Sender: TObject);
begin
  inherited;

  DbGridEh1.ReadOnly := False ;
  DbGridEh2.ReadOnly := False ;
  DbGridEh3.ReadOnly := False ;

  ativaDBEdit(DbEdit4);
  ativaDBEdit(DBEdit5);

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      qryMasternCdLoja.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
  end ;

  DBEdit4.SetFocus ;

  // se n�o for gerente, posiciona a conta que o usuario � responsavel
  if (qryGerentecFlgGerente.Value = 0) then
  begin
  
      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdContaBancaria FROM ContaBancaria WHERE cFlgCaixa = 1 AND nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva) + ' AND nCdUsuarioOperador = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Open ;

      if qryAux.Eof then
      begin
          MensagemAlerta('Voc� n�o tem permiss�o para movimentar nenhuma conta caixa.') ;
          btCancelar.Click;
          exit ;
      end ;

      qryMasternCdContaOrigem.Value := qryAux.FieldList[0].Value ;

      if (qryMasternCdLoja.Value > 0) then
          qryContaOrigem.Parameters.ParamByName('nCdLoja').Value := qryMasternCdLoja.Value;

      PosicionaQuery(qryContaOrigem, qryMasternCdContaOrigem.AsString) ;

      // localiza a conta cofre da loja
      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT IsNull(nCdContaBancariaCofre,0) FROM Loja WHERE nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva)) ;
      qryAux.Open ;

      if qryAux.Eof or (qryAux.FieldList[0].Value = 0) then
      begin
          MensagemAlerta('Loja n�o tem conta cofre parametrizada.') ;
          btCancelar.Click;
          exit ;
      end ;

      qryMasternCdContaDestino.Value := qryAux.FieldList[0].Value ;
      PosicionaQuery(qryContaDestino, qryMasternCdContaDestino.AsString) ;

      DBEdit6.SetFocus;

      desativaDBEdit(DbEdit4);
      desativaDBEdit(DBEdit5);
  end;
end;

procedure TfrmRemessa_Numerarios.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'REMESSANUM' ;
  nCdTabelaSistema  := 57 ;
  nCdConsultaPadrao := 127 ;
  bLimpaAposSalvar  := false ;

end;

procedure TfrmRemessa_Numerarios.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryContaOrigem.Close ;
  qryContaDestino.Close ;
  qryChequeRemessaNum.Close ;
  qryCartaoRemessaNum.Close ;
  qryCrediarioRemessaNum.Close ;
  qryUsuario.Close ;

  btVeriDocPend.Enabled := True ;
  btEncerrar.Enabled    := True ;
  btImprimir.Enabled    := True ;

  DbGridEh1.ReadOnly := False ;
  DbGridEh2.ReadOnly := False ;
  DbGridEh3.ReadOnly := False ;

end;

procedure TfrmRemessa_Numerarios.qryMasterBeforePost(DataSet: TDataSet);
begin
  bNovoRegistro := False ;

  if (qryMasternCdContaOrigem.Value = 0) or (DBEdit7.Text = '') then
  begin
      MensagemAlerta('Informe a conta de origem.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdContaDestino.Value = 0) or (DBEdit11.Text = '') then
  begin
      MensagemAlerta('Informe a conta de destino.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdContaOrigem.Value = qryMasternCdContaDestino.Value) then
  begin
      MensagemAlerta('As contas de origem e destino devem ser diferentes.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternValDinheiro.Value < 0) then
  begin
      MensagemAlerta('Valor informado inv�lido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  { -- verifica se existe remessa em aberto e sugere utilizar a mesma -- }
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT nCdRemessaNum                                         ');
  qryAux.SQL.Add('  FROM RemessaNum                                            ');
  qryAux.SQL.Add( 'WHERE nCdContaOrigem  = ' + qryMasternCdContaOrigem.AsString );
  qryAux.SQL.Add('   AND nCdContaDestino = ' + qryMasternCdContaDestino.AsString);
  qryAux.SQL.Add('   AND dDtCancel       IS NULL                               ');
  qryAux.SQL.Add('   AND dDtFech         IS NULL                               ');
  qryAux.SQL.Add('   AND dDtConfirma     IS NULL                               ');
  qryAux.Open;

  if ((not qryAux.IsEmpty) and (qryMaster.State = dsInsert)) then
  begin
      case MessageDlg('Existe uma remessa de numer�rios em aberto para a mesma conta de origem e destino. Deseja visualizar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrYes : PosicionaPK(qryAux.Fields[0].Value);
      end;
  end;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      bNovoRegistro := True ;
      qryMasterdDtRemessa.Value    := StrToDate(DateToStr(Now())) ;
      qryMasternCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado;
  end ;
end;

procedure TfrmRemessa_Numerarios.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse)  and (qryMasternCdRemessaNum.Value = 0) then
      qryMaster.Post ;

end;

procedure TfrmRemessa_Numerarios.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
    exit ;

  if (qryMaster.State <> dsBrowse) and (qryMasternCdRemessaNum.Value = 0) then
      qryMaster.Post ;

end;

procedure TfrmRemessa_Numerarios.DBGridEh3Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) and (qryMasternCdRemessaNum.Value = 0) then
      qryMaster.Post ;

end;

procedure TfrmRemessa_Numerarios.qryChequeRemessaNumCalcFields(
  DataSet: TDataSet);
begin
  {--inherited;--}

  PosicionaQuery(qryCheque, qryChequeRemessaNumnCdCheque.AsString) ;

  if not qryCheque.eof then
  begin

      qryChequeRemessaNumnCdBanco.Value   := qryChequenCdBanco.asString ;
      qryChequeRemessaNumcAgencia.Value   := qryChequecAgencia.Value ;
      qryChequeRemessaNumiNrCheque.Value  := qryChequeiNrCheque.Value ;
      qryChequeRemessaNumnValCheque.Value := qryChequenValCheque.Value ;

  end ;

end;

procedure TfrmRemessa_Numerarios.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bNovoRegistro) then
  begin
      VerificaDocPend();
  end ;

  bNovoRegistro := False ;

end;

procedure TfrmRemessa_Numerarios.qryCrediarioRemessaNumCalcFields(
  DataSet: TDataSet);
begin
  {-- inherited;--}

  PosicionaQuery(qryCrediario, qryCrediarioRemessaNumnCdCrediario.AsString) ;

  if not qryCrediario.Eof then
  begin

      qryCrediarioRemessaNumnCdLanctoFin.Value  := qryCrediarionCdLanctoFin.Value ;
      qryCrediarioRemessaNumdDtVenda.Value      := qryCrediariodDtVenda.Value     ;
      qryCrediarioRemessaNumcNmTerceiro.Value   := qryCrediariocNmTerceiro.Value  ;
      qryCrediarioRemessaNumnValCrediario.Value := qryCrediarionValCrediario.Value ;

  end ;

end;

procedure TfrmRemessa_Numerarios.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  btVeriDocPend.Enabled := True ;
  btEncerrar.Enabled    := True ;
  btCancelarRem.Enabled := True ;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryContaOrigem.Close ;
  qryContaDestino.Close ;
  qryUsuario.Close ;
  qryUsuarioConfirma.Close ;
  qryChequeRemessaNum.Close ;
  qryCrediarioRemessaNum.Close ;
  qryCartaoRemessaNum.Close ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;

  if (qryMasternCdLoja.Value > 0) then
      qryContaOrigem.Parameters.ParamByName('nCdLoja').Value := qryMasternCdLoja.Value;

  PosicionaQuery(qryContaOrigem, qryMasternCdContaOrigem.AsString) ;
  PosicionaQuery(qryContaDestino, qryMasternCdContaDestino.AsString) ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString) ;
  PosicionaQuery(qryUsuarioConfirma, qryMasternCdUsuarioConfirma.AsString) ;
  PosicionaQuery(qryChequeRemessaNum, qryMasternCdRemessaNum.AsString) ;
  PosicionaQuery(qryCartaoRemessaNum, qryMasternCdRemessaNum.AsString) ;
  PosicionaQuery(qryCrediarioRemessaNum, qryMasternCdRemessaNum.AsString) ;

  desativaDBEdit(DbEdit2);
  desativaDBEdit(DbEdit3);
  desativaDBEdit(DbEdit4);

  ativaDBEdit(DBEdit6);

  if (qryMasterdDtFech.AsString <> '') then
      btEncerrar.Enabled := False ;

  if (qryMasternCdUsuarioCad.Value = frmMenu.nCdUsuarioLogado) then
      btCancelarRem.Enabled := True ;

  if (qryMasterdDtConfirma.AsString <> '') then
      btCancelarRem.Enabled := False ;

  if (qryMasterdDtCancel.AsString <> '') then
      btCancelarRem.Enabled := False ;

  btSalvar.Enabled   := True  ;
  DbGridEh1.ReadOnly := False ;
  DbGridEh2.ReadOnly := False ;
  DbGridEh3.ReadOnly := False ;

  if (qryMasterdDtConfirma.asString <> '') or (qryMasterdDtFech.asString <> '') or (qryMasterdDtCancel.asString <> '') then
  begin
      btVeriDocPend.Enabled := False;
      btSalvar.Enabled      := False ;
      DbGridEh1.ReadOnly    := True ;
      DbGridEh2.ReadOnly    := True ;
      DbGridEh3.ReadOnly    := True ;

      desativaDBEdit(DbEdit5);
      desativaDBEdit(DBEdit6);
  end ;

end;

procedure TfrmRemessa_Numerarios.btCancelarRemClick(Sender: TObject);
var
  objForm : TfrmCaixa_Recebimento;
begin
  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma remessa.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Remessa j� foi cancelada. Imposs�vel cancelar.') ;
      abort ;
  end;

  if (qryMasterdDtConfirma.asString <> '') then
  begin
      MensagemAlerta('Remessa j� foi confirmada pela conta de destino. Imposs�vel cancelar.') ;
      abort ;
  end ;

  if (qryMasterdDtFech.AsString = '') then
  begin
      MensagemAlerta('Remessa n�o foi finalizada impedindo seu cancelamento, efetue a exclus�o do registro.');
      Abort;
  end;

  if (qryMasternCdUsuarioCad.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Remessa realizada por outro usu�rio. Para cancelar � necess�rio a autoriza��o do gerente.') ;

      objForm := TfrmCaixa_Recebimento.Create(nil);

      if not (objForm.AutorizacaoGerente(51)) then
      begin
          MensagemAlerta('Autentica��o falhou.');

          FreeAndNil(objForm);

          Exit;
      end;

      FreeAndNil(objForm);
  end ;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT dDtCancel FROM RemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString + ' AND dDtCancel IS NOT NULL');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Remessa de num�rarios ativa foi cancelada anteriormente em ' + qryAux.Fields[0].AsString + '.');
      PosicionaPK(qryMasternCdRemessaNum.Value);
      Exit;
  end;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT dDtConfirma FROM RemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString + ' AND dDtConfirma IS NOT NULL');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Remessa de num�rarios ativa foi confirmada anteriormente em ' + qryAux.Fields[0].AsString + '.');
      PosicionaPK(qryMasternCdRemessaNum.Value);
      Exit;
  end;

  case MessageDlg('Confirma o cancelamento desta remessa?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasterdDtCancel.Value := Now() ;
      qryMaster.Post ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT dDtFech FROM RemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString + ' AND dDtFech IS NOT NULL');
      qryAux.Open;

      if (not qryAux.IsEmpty) then
      begin
          {-- gera um lan�amento financeiro da entrada de dinheiro no caixa (estorno) --}
          qryGeraEstornoDinheiro.Close;
          qryGeraEstornoDinheiro.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaOrigem.Value ;
          qryGeraEstornoDinheiro.Parameters.ParamByName('nValDinheiro').Value     := qryMasternValDinheiro.Value ;
          qryGeraEstornoDinheiro.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          qryGeraEstornoDinheiro.Parameters.ParamByName('nCdRemessaNum').Value    := qryMasternCdRemessaNum.Value;
          qryGeraEstornoDinheiro.ExecSQL ;
      end ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Remessa cancelada com sucesso.') ;

  btCancelar.Click;
end;

procedure TfrmRemessa_Numerarios.FormShow(Sender: TObject);
begin
  inherited;
  qryGerente.Close;
  PosicionaQuery(qryGerente, intToStr(frmMenu.nCdUsuarioLogado)) ;
end;

procedure TfrmRemessa_Numerarios.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  
  if (not qryMaster.Active) then
      Exit;

  qryContaOrigem.Close ;

  if (qryMasternCdLoja.Value > 0) then
      qryContaOrigem.Parameters.ParamByName('nCdLoja').Value := qryMasternCdLoja.Value;

  PosicionaQuery(qryContaOrigem, qryMasternCdContaOrigem.AsString);

  if (qryContaOrigem.IsEmpty) then
      Exit;

  {-- V�lida��o para conta origem --}
  if (qryContaOrigemcFlgBloqDinheiroRemessa.Value = 1) then
  begin
      desativaDBEdit(DBEdit6);
      {-- Caso o Usuario queira burlar o preenchimento, apagara o registro --}
      if (trim(DBEdit6.Text) <> '') then
      begin
          ShowMessage('Conta origem n�o permite que seja informado o valor em dinheiro atrav�s da remessa de numer�rios.' + #13 + 'O valor R$ ' + qryMasternValDinheiro.AsString + ' ser� removido.');
          qryMasternValDinheiro.Value := 0;
          DBEdit6.Text                := '';
      end;
  end
  else
      ativaDBEdit(DBEdit6);
end;

procedure TfrmRemessa_Numerarios.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if (not qryMaster.Active) then
      Exit;
      
  qryContaDestino.Close ;
  PosicionaQuery(qryContaDestino, DbEdit5.Text);

  if not (qryContaDestino.IsEmpty) then
  begin
      if (qryContaDestinonCdLoja.Value <> qryMasternCdLoja.Value) then
      begin
          case MessageDlg('O Cofre selecionado n�o pertence a loja ' + qryLojacNmLoja.Value + '. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo : begin
                  qryMasternCdContaDestino.Clear;
                  qryContaDestino.Close;
                  DBEdit5.SetFocus;
                  Abort;
              end;
          end;
      end;
  end;
end;

procedure TfrmRemessa_Numerarios.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if (DBEdit4.ReadOnly) then
      Exit;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'Loja.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdContaOrigem.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRemessa_Numerarios.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if (DBEdit5.ReadOnly) then
      Exit;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'cFlgCofre = 1');

            If (nPK > 0) then
            begin
                qryMasternCdContaDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRemessa_Numerarios.cxButton2Click(Sender: TObject);
var
    cFlgGerente : integer ;
    objForm     : TfrmCaixa_Recebimento;
begin
  cFlgGerente := 0 ;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma remessa.') ;
      exit ;
  end ;

  qryMaster.Post;

  if (qryMasterdDtFech.AsString <> '') then
  begin
      MensagemAlerta('Remessa j� foi finalizada.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Remessa Cancelada.') ;
      exit ;
  end ;

  if (qryMasternCdUsuarioCad.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Somente o usu�rio que cadastrou a remessa pode finalizar.') ;
      exit ;
  end ;

  if ((qryChequeRemessaNum.IsEmpty) and (qryCrediarioRemessaNum.IsEmpty) and (qryCartaoRemessaNum.IsEmpty) and (qryMasternValDinheiro.Value = 0)) then
  begin
      MensagemAlerta('� necess�rio informar pelo menos um comprovante ou valor para finalizar a remessa.');
      Abort;
  end;

  if (qryContaDestinonCdLoja.Value <> qryMasternCdLoja.Value) then
  begin

      if (MessageDlg('O Cofre Selecionado N�o Pertence a essa Loja. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      begin
          abort ;
      end ;

  end ;

  // descobre o saldo atual da conta de origem e verifica se o valor transferido � maior que o saldo
  PosicionaQuery(qryContaOrigem, qryMasternCdContaOrigem.AsString) ;

  if (frmMenu.TBRound(qryContaOrigemnSaldoConta.Value,2) < frmMenu.TBRound(qryMasternValDinheiro.Value,2)) then
  begin
      MensagemAlerta('O Saldo da conta de origem � menor que o valor em dinheiro a ser transferido.') ;
      abort ;
  end ;

  // descobre se o usu�rio � gerente
  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT cFlgGerente FROM Usuario WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
  qryAux.Open ;

  if not qryAux.Eof then
  begin
      cFlgGerente := qryAux.FieldList[0].Value ;
  end ;

  if (cFlgGerente = 0) then
  begin

      objForm := TfrmCaixa_Recebimento.Create(nil);

      try
          try
              if not objForm.AutorizacaoGerente(50) then
              begin
                  MensagemAlerta('Autentica��o Inv�lida.') ;
                  exit ;
              end ;
          except
              MensagemErro('Erro na cria��o da tela. Classe:  ' + objForm.ClassName);
              raise;
          end;
      finally
          FreeAndNil(objForm);
      end;


  end ;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT dDtCancel FROM RemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString + ' AND dDtCancel IS NOT NULL');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Remessa de num�rarios ativa foi cancelada anteriormente em ' + qryAux.Fields[0].AsString + '.');
      PosicionaPK(qryMasternCdRemessaNum.Value);
      Exit;
  end;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT dDtFech FROM RemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString + ' AND dDtFech IS NOT NULL');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Remessa de num�rarios ativa foi finalizada anteriormente em ' + qryAux.Fields[0].AsString + '.');
      PosicionaPK(qryMasternCdRemessaNum.Value);
      Exit;
  end;

  case MessageDlg('Confirma a finaliza��o desta remessa ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

      qryMaster.Edit ;
      qryMasterdDtFech.Value := Now() ;
      qryMaster.Post ;

      if (qryChequeRemessaNum.RecordCount > 0) then
          if (qryChequeRemessaNum.State <> dsBrowse) then
              qryChequeRemessaNum.Post ;

      if (qryCartaoRemessaNum.RecordCount > 0) then
          if (qryCartaoRemessaNum.State <> dsBrowse) then
              qryCartaoRemessaNum.Post ;

      if (qryCrediarioRemessaNum.RecordCount > 0) then
          if (qryCrediarioRemessaNum.State <> dsBrowse) then
              qryCrediarioRemessaNum.Post ;

      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ChequeRemessaNum WHERE cFlgAux = 0 AND nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
      qryAux.ExecSQL;

      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM CartaoRemessaNum WHERE cFlgAux = 0 AND nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
      qryAux.ExecSQL;

      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM CrediarioRemessaNum WHERE cFlgAux = 0 AND nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
      qryAux.ExecSQL;

      {-- gera um lan�amento financeiro da sa�da de dinheiro do caixa e debita a conta do caixa --}

      qryGeraLanctoRemessaDinheiro.Close;
      qryGeraLanctoRemessaDinheiro.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaOrigem.Value ;
      qryGeraLanctoRemessaDinheiro.Parameters.ParamByName('nValDinheiro').Value     := qryMasternValDinheiro.Value ;
      qryGeraLanctoRemessaDinheiro.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      qryGeraLanctoRemessaDinheiro.Parameters.ParamByName('nCdRemessaNum').Value    := qryMasternCdRemessaNum.Value;
      qryGeraLanctoRemessaDinheiro.ExecSQL ;

      {qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE ContaBancaria Set nSaldoConta = nSaldoConta - ' + StringReplace(qryMasternValDinheiro.AsString,',','.',[rfReplaceAll, rfIgnoreCase]) + ' WHERE nCdContaBancaria = ' + qryMasternCdContaOrigem.AsString) ;
      qryAux.ExecSQL;}

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  case MessageDlg('Deseja imprimir esta remessa ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: btImprimir.Click ;
  end ;

  btCancelar.Click;

end;

procedure TfrmRemessa_Numerarios.btImprimirClick(Sender: TObject);
begin
  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma remessa.') ;
      exit ;
  end ;

  if (qryMasterdDtFech.AsString = '') then
  begin
      MensagemAlerta('Finalize a remessa antes de imprimir.') ;
      exit ;
  end ;

  qryRemessa.Close ;
  qryCheques.Close ;
  qryCartao.Close  ;
  qryCrediarios.Close  ;

  PosicionaQuery(qryRemessa, qryMasternCdRemessaNum.AsString) ;
  PosicionaQuery(qryCheques, qryMasternCdRemessaNum.AsString) ;
  PosicionaQuery(qryCartao, qryMasternCdRemessaNum.AsString) ;
  PosicionaQuery(qryCrediarios, qryMasternCdRemessaNum.AsString) ;

  {-- Exibe o Comprovante de Remessa de Numer�rios -- }

  ExibeComprovante;

end;

procedure TfrmRemessa_Numerarios.ExibeComprovante;
var
  nValCheque, nValCrediario, nValTotalCartao : currency;
  iQtdeTotalCartao : integer;
begin
  nValCheque       := 0;
  nValCartao       := 0;
  nValTotalCartao  := 0;
  nValCrediario    := 0;
  iPagina          := 0;
  cParcelado       := '';
  cOperadoraCartao := '';
  RDprint1.TitulodoRelatorio := 'Remessa de Numer�rio No ' + qryMasternCdRemessaNum.AsString;
  
  rdPrint1.Abrir;

  rdPrint1.OpcoesPreview.PaginaZebrada := false ;
  rdPrint1.OpcoesPreview.Remalina      := false ;

  if not qryCheques.Eof then
  begin
      ImprimeCabecalho('CHEQUE');

      iQtdeComprovantes := 0 ;

      iLinha := iLinha + 2;

      qryCheques.First;

      while not qryCheques.Eof do
      begin
          rdPrint1.ImpF(iLinha,1, qryChequescStatus.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,3, qryChequesnCdBanco.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,13,qryChequescAgencia.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,19,qryChequescConta.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,30,qryChequescDigito.AsString,[comp17]);
          rdPrint1.ImpVal(iLinha,34,'#,##000',qryChequesiNrCheque.AsInteger,[comp17]);
          rdPrint1.ImpF(iLinha,40,qryChequescCNPJCPF.AsString,[comp17]);
          rdPrint1.ImpVal(iLinha,54,'###,##0.00',qryChequesnValCheque.Value,[comp17]);
          rdPrint1.ImpF(iLinha,61,qryChequescNmTerceiro.AsString,[comp17]);

          nValCheque := nValCheque + qryChequesnValCheque.Value;

          iLinha := iLinha + 1;

          if (iLinha > 64) then
          begin
              rdPrint1.Novapagina;
              ImprimeCabecalho('CHEQUE');
              iLinha := iLinha + 2;
          end;

          iQtdeComprovantes := iQtdeComprovantes + 1 ;
          qryCheques.Next;
      end;

      rdPrint1.ImpF(iLinha + 1, 1,'Quantidade: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeComprovantes),5),[comp17,negrito]);
      rdPrint1.ImpF(iLinha + 1, 37, 'Total de Cheques:',[comp17,negrito]);
      rdPrint1.ImpVal(iLinha + 1, 54,'###,##0.00', nValCheque,[comp17,negrito]);

      iLinha := iLinha + 3;
  end ;

  if not qryCartao.Eof then
  begin

      ImprimeCabecalho('CART�O');

      iQtdeComprovantes := 0 ;
      iQtdeTotalCartao  := 0 ;
      iLinha            := iLinha + 2;

      qryCartao.First;

      while not qryCartao.Eof do
      begin

          VerificaOperadora;
          VerificaParcelas;

          rdPrint1.ImpF(iLinha,1, qryCartaocStatus.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,3, qryCartaodDtTransacao.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,15,qryCartaocNmOperadoraCartao.AsString,[comp17]);
          rdPrint1.ImpD(iLinha,43,qryCartaoiParcelas.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,45,qryCartaoiNrAutorizacao.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,58,qryCartaoiNrDocto.AsString,[comp17]);
          rdPrint1.ImpVal(iLinha,70,'###,##0.00',qryCartaonValTransacao.Value,[comp17]);

          nValCartao := nValCartao + qryCartaonValTransacao.Value;
          nValTotalCartao := nValTotalCartao + qryCartaonValTransacao.Value;

          iLinha := iLinha + 1;

          if (iLinha > 64) then
          begin
              rdPrint1.Novapagina;
              ImprimeCabecalho('CART�O');
              iLinha := iLinha + 2;
          end;

          cOperadoraCartao := qryCartaocNmOperadoraCartao.Value;
          cParcelado       := qryCartaocParcelado.Value;

          iQtdeComprovantes := iQtdeComprovantes + 1 ;
          iQtdeTotalCartao  := iQtdeTotalCartao  + 1 ;
          qryCartao.Next;
      end;

      rdPrint1.ImpF(iLinha, 1,'Quantidade: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeComprovantes),5),[comp17,negrito]);
      rdPrint1.ImpF(iLinha, 47, 'Subtotal '+ cOperadoraCartao + ' - ' + cParcelado,[comp17,negrito]);
      rdPrint1.ImpVal(iLinha, 70,'###,##0.00', nValCartao,[comp17,negrito]);

      rdPrint1.ImpF(iLinha + 2, 1,'Quantidade Total: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeTotalCartao),5),[comp17,negrito]);
      rdPrint1.ImpF(iLinha + 2, 43, 'Total Geral de Comprovantes de Cart�o:',[comp17,negrito]);
      rdPrint1.ImpVal(iLinha + 2, 70,'###,##0.00', nValTotalCartao,[comp17,negrito]);

  end ;

  iLinha := iLinha + 4;

  if not qryCrediarios.Eof then
  begin
      ImprimeCabecalho('CREDI�RIO');

      iQtdeComprovantes := 0 ;

      iLinha := iLinha + 2;

      qryCrediarios.First;

      while not qryCrediarios.Eof do
      begin
          rdPrint1.ImpF(iLinha,1, qryCrediarioscStatus.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,3, qryCrediariosnCdCrediario.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,13,qryCrediariosdDtVenda.AsString,[comp17]);
          rdPrint1.ImpF(iLinha,26,qryCrediarioscNmTerceiro.AsString,[comp17]);
          rdPrint1.ImpVal(iLinha,49,'###,##0.00',qryCrediariosnValCrediario.Value,[comp17]);

          nValCrediario := nValCrediario + qryCrediariosnValCrediario.Value;

          iLinha := iLinha + 1;

          if (iLinha > 64) then
          begin
              rdPrint1.Novapagina;
              ImprimeCabecalho('CREDI�RIO');
              iLinha := iLinha + 2;
          end;

          iQtdeComprovantes := iQtdeComprovantes + 1 ;
          qryCrediarios.Next;
      end;

      rdPrint1.ImpF(iLinha + 1, 1,'Quantidade: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeComprovantes),5),[comp17,negrito]);
      rdPrint1.ImpF(iLinha + 1, 26, 'Total de Comprovantes de Credi�rio:',[comp17,negrito]);
      rdPrint1.ImpVal(iLinha + 1, 49,'###,##0.00', nValCrediario,[comp17,negrito]);

  end ;

  iQtdeComprovantes := 0 ;

  rdPrint1.Fechar;

end;

procedure TfrmRemessa_Numerarios.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  inherited;
  iPagina := iPagina+ 1 ;

  rdPrint1.impBox(01,01,'--------------------------------------------------------------------------------');
  rdPrint1.ImpF(2,1,frmMenu.cNmEmpresaAtiva,[comp17]);
  rdPrint1.ImpD(3,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
  rdPrint1.ImpF(3,1,'Comprovante de Remessa de Numer�rios',[comp17])  ;
  rdPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);
  rdPrint1.impBox(04,01,'--------------------------------------------------------------------------------');

  {-- Cabe�alho da remessa --}
  rdPrint1.ImpF(5,2,'C�digo Remessa',[comp17]);
  rdPrint1.ImpF(5,11,qryMasternCdRemessaNum.AsString,[comp17,negrito]);

  rdPrint1.ImpF(6,7,' Loja',[comp17]);
  rdPrint1.ImpF(6,11,qryRemessacNmLoja.AsString,[comp17,negrito]);

  rdPrint1.ImpF(7,2,'  Conta Origem',[comp17]);
  rdPrint1.ImpF(7,11,qryRemessanCdContaOrigem.AsString,[comp17,negrito]);

  rdPrint1.ImpF(7,30,' Data Remessa',[comp17]);
  rdPrint1.ImpF(7,38,qryRemessadDtRemessa.AsString,[comp17,negrito]);
  rdPrint1.ImpF(7,49,qryRemessacNmUsuarioCad.AsString,[comp17,negrito]);

  rdPrint1.ImpF(8,2,' Conta Destino',[comp17]);
  rdPrint1.ImpF(8,11,qryRemessanCdContaDestino.AsString,[comp17,negrito]);

  rdPrint1.ImpF(8,30,'Processamento',[comp17]);
  rdPrint1.ImpF(8,38,qryRemessadDtFech.AsString,[comp17,negrito]);

  rdPrint1.ImpF(9,1,'Valor em Esp�cie',[comp17]);
  rdPrint1.ImpVal(9,16,'#,##0.00',qryRemessanValDinheiro.Value,[comp17,negrito]);

  rdPrint1.ImpF(10,4,' Observa��o',[comp17]);
  rdPrint1.ImpF(10,11,qryRemessacOBS.AsString,[comp17,negrito]);

  rdPrint1.ImpF(12,1,'* Documentos Extraviados',[comp17]);

  iLinha := 14 ;

  {-- Rodap� --}
  rdPrint1.impBox(67, 1, '------------------------' );
  rdPrint1.impF(68, 1,'RESPONS�VEL CONTA ORIGEM',[comp17,negrito]);
  rdPrint1.impBox(67, 50, '------------------------' );
  rdPrint1.impF(68, 50,'RESPONS�VEL CONTA DESTINO',[comp17,negrito]);
  rdPrint1.impBox(69,01,'--------------------------------------------------------------------------------');
  rdPrint1.impBox(72,01,'--------------------------------------------------------------------------------');


end;

procedure TfrmRemessa_Numerarios.ImprimeCabecalho(cTipo : string);
begin
    if (cTipo = 'CHEQUE') then
    begin
        rdPrint1.ImpF(iLinha,1,'CHEQUES',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,3,'BANCO',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,13,'AG�NCIA',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,19,'CONTA',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,30,'DIG',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,35,'CHEQUE',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,40,'CNPJ/CPF',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,53,'VALOR CHEQUE',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,61,'CLIENTE',[comp17,negrito]);
    end
    else if (cTipo = 'CART�O') then
    begin
        rdPrint1.ImpF(iLinha,1,'COMPROVANTES DE CART�O',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,3,'DATA TRANSA��O',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,15,'OPERADORA',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,35,'N�M. PARCELAS',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,45,'N�M. AUTORIZA��O',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,58,'DOCUMENTO',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,67,'VALOR TRANSA��O',[comp17,negrito]);
    end
    else if (cTipo = 'CREDI�RIO') then
    begin
        rdPrint1.ImpF(iLinha,1,'COMPROVANTES CREDI�RIO',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,3,'CREDI�RIO',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,13,'DATA VENDA',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,26,'CLIENTE',[comp17,negrito]);
        rdPrint1.ImpF(iLinha + 1,46,'VALOR CREDI�RIO',[comp17,negrito]);
    end;

end;

procedure TfrmRemessa_Numerarios.VerificaOperadora;
begin

    if((cOperadoraCartao <> '') and (cOperadoraCartao <> qryCartaocNmOperadoraCartao.Value)) then
    begin

        rdPrint1.ImpF(iLinha, 1,'Quantidade: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeComprovantes),5),[comp17,negrito]);
        rdPrint1.ImpF(iLinha, 47, 'Subtotal '+ cOperadoraCartao + ' - ' + cParcelado,[comp17,negrito]);
        rdPrint1.ImpVal(iLinha, 70,'###,##0.00', nValCartao,[comp17,negrito]);
        nValCartao := 0;

        iLinha := iLinha + 2;

        iQtdeComprovantes := 0 ;

    end;

end;

procedure TfrmRemessa_Numerarios.VerificaParcelas;
begin

    if((cParcelado <> '') and (cOperadoraCartao <> '') and ((qryCartaocParcelado.Value = 'PARCELADO') and (qryCartaocParcelado.Value <> cParcelado)) and (cOperadoraCartao = qryCartaocNmOperadoraCartao.Value)) then
    begin

        rdPrint1.ImpF(iLinha, 1,'Quantidade: ' + frmMenu.ZeroEsquerda(intToStr(iQtdeComprovantes),5),[comp17,negrito]);
        rdPrint1.ImpF(iLinha, 47, 'Subtotal '+ cOperadoraCartao + ' - ' + cParcelado,[comp17,negrito]);
        rdPrint1.ImpVal(iLinha, 70,'###,##0.00', nValCartao,[comp17,negrito]);
        nValCartao := 0;

        iLinha := iLinha + 2;

        iQtdeComprovantes := 0 ;

    end;

end;

procedure TfrmRemessa_Numerarios.VerificaDocPend;
//var
  //cFlegar : string ;
begin

  try

     SP_INCLUI_DOCUMENTOS_REMESSA.Close;
     SP_INCLUI_DOCUMENTOS_REMESSA.Parameters.ParamByName('@nCdRemessaNum').Value          := qryMasternCdRemessaNum.Value;
     SP_INCLUI_DOCUMENTOS_REMESSA.Parameters.ParamByName('@nCdContaBancariaOrigem').Value := qryMasternCdContaOrigem.Value;
     SP_INCLUI_DOCUMENTOS_REMESSA.ExecProc;

  except
     MensagemErro('Erro no processamento.');
     raise;
  end;

  cxPageControl1.ActivePage := cxTabSheet1 ;

  qryCartaoRemessaNum.close;
  PosicionaQuery(qryCartaoRemessaNum,qryMasternCdRemessaNum.AsString);

  qryChequeRemessaNum.close;
  PosicionaQuery(qryChequeRemessaNum,qryMasternCdRemessaNum.AsString);

  qryCrediarioRemessaNum.close;
  PosicionaQuery(qryCrediarioRemessaNum,qryMasternCdRemessaNum.AsString);


  {cFlegar := frmMenu.LeParametro('FLGREMNUM') ;

  qryChequeRemessaNum.Close ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdCheque                                                                                           ');
  qryAux.SQL.Add('  FROM Cheque                                                                                              ');
  qryAux.SQL.Add(' WHERE nCdContaBancariaDep = ' + DbEdit4.Text                                                               );
  qryAux.SQL.Add('   AND NOT EXISTS(SELECT 1                                                                                 ');
  qryAux.SQL.Add('                    FROM ChequeRemessaNum                                                                  ');
  qryAux.SQL.Add('                         INNER JOIN RemessaNum ON RemessaNum.nCdRemessaNum = ChequeRemessaNum.nCdRemessaNum');
  qryAux.SQL.Add('                   WHERE ChequeRemessaNum.nCdCheque  = Cheque.nCdCheque                                    ');
  qryAux.SQL.Add('                     AND RemessaNum.nCdContaOrigem   = ' + DbEdit4.Text                                     );
  qryAux.SQL.Add('                     AND RemessaNum.dDtCancel       IS NULL)                                               ');
  qryAux.Open ;

  PosicionaQuery(qryChequeRemessaNum, qryMasternCdRemessaNum.AsString) ;

  while not qryAux.Eof do
  begin

      qryChequeRemessaNum.Insert ;
      qryChequeRemessaNumnCdChequeRemessaNum.Value := frmMenu.fnProximoCodigo('CHEQUEREMESSANUM') ;
      qryChequeRemessaNumnCdRemessaNum.Value       := qryMasternCdRemessaNum.Value ;
      qryChequeRemessaNumnCdCheque.Value           := qryAux.FieldList[0].Value ;

      if (cFlegar = 'S') then
          qryChequeRemessaNumcFlgAux.Value := 1 ;

      qryChequeRemessaNum.Post ;

      qryAux.Next ;

  end ;

  qryAux.Close ;

  PosicionaQuery(qryChequeRemessaNum, qryMasternCdRemessaNum.AsString) ;



  qryCartaoRemessaNum.Close ;

  1qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdTransacaoCartao                                                                                  ');
  qryAux.SQL.Add('  FROM TransacaoCartao                                                                                     ');
  qryAux.SQL.Add(' WHERE nCdContaBancaria = ' + DbEdit4.Text                                                                  );
  qryAux.SQL.Add('   AND NOT EXISTS(SELECT 1                                                                                 ');
  qryAux.SQL.Add('                    FROM CartaoRemessaNum                                                                  ');
  qryAux.SQL.Add('                         INNER JOIN RemessaNum ON RemessaNum.nCdRemessaNum = CartaoRemessaNum.nCdRemessaNum');
  qryAux.SQL.Add('                   WHERE CartaoRemessaNum.nCdTransacaoCartao  = TransacaoCartao.nCdTransacaoCartao         ');
  qryAux.SQL.Add('                     AND RemessaNum.nCdContaOrigem            = ' + DbEdit4.Text                            );
  qryAux.SQL.Add('                     AND RemessaNum.dDtCancel                IS NULL)                                      ');
  qryAux.Open ;

  PosicionaQuery(qryCartaoRemessaNum, qryMasternCdRemessaNum.AsString) ;

  while not qryAux.Eof do
  begin

      qryCartaoRemessaNum.Insert ;
      qryCartaoRemessaNumnCdCartaoRemessaNum.Value := frmMenu.fnProximoCodigo('CARTAOREMESSANUM') ;
      qryCartaoRemessaNumnCdRemessaNum.Value       := qryMasternCdRemessaNum.Value ;
      qryCartaoRemessaNumnCdTransacaoCartao.Value  := qryAux.FieldList[0].Value ;

      if (cFlegar = 'S') then
          qryCartaoRemessaNumcFlgAux.Value := 1 ;

      qryCartaoRemessaNum.Post ;

      qryAux.Next ;

  end ;

  qryAux.Close ;

  PosicionaQuery(qryCartaoRemessaNum, qryMasternCdRemessaNum.AsString) ;

  qryCrediarioRemessaNum.Close ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdCrediario                                                                                           ');
  qryAux.SQL.Add('  FROM Crediario                                                                                              ');
  qryAux.SQL.Add(' WHERE dDtEstorno IS NULL AND nCdContaBancaria = ' + DbEdit4.Text                                              );
  qryAux.SQL.Add('   AND NOT EXISTS(SELECT 1                                                                                    ');
  qryAux.SQL.Add('                    FROM CrediarioRemessaNum                                                                  ');
  qryAux.SQL.Add('                         INNER JOIN RemessaNum ON RemessaNum.nCdRemessaNum = CrediarioRemessaNum.nCdRemessaNum');
  qryAux.SQL.Add('                   WHERE CrediarioRemessaNum.nCdCrediario  = Crediario.nCdCrediario                           ');
  qryAux.SQL.Add('                     AND RemessaNum.nCdContaOrigem         = ' + DbEdit4.Text                                  );
  qryAux.SQL.Add('                     AND RemessaNum.dDtCancel             IS NULL)                                            ');
  qryAux.Open ;

  PosicionaQuery(qryCrediarioRemessaNum, qryMasternCdRemessaNum.AsString) ;

  while not qryAux.Eof do
  begin

      qryCrediarioRemessaNum.Insert ;
      qryCrediarioRemessaNumnCdCrediarioRemessaNum.Value := frmMenu.fnProximoCodigo('CREDIARIOREMESSANUM') ;
      qryCrediarioRemessaNumnCdRemessaNum.Value          := qryMasternCdRemessaNum.Value ;
      qryCrediarioRemessaNumnCdCrediario.Value           := qryAux.FieldList[0].Value ;

      if (cFlegar = 'S') then
          qryCrediarioRemessaNumcFlgAux.Value := 1 ;

      qryCrediarioRemessaNum.Post ;

      qryAux.Next ;

  end ;

  qryAux.Close ;}


end;

procedure TfrmRemessa_Numerarios.btVeriDocPendClick(Sender: TObject);
begin

  if (not qryMaster.Active) then
      Exit ;

  if (qryMaster.State in [dsInsert,dsEdit]) then
     qryMaster.Post;

  if (qryMasterdDtFech.AsString = '') and (qryMasterdDtCancel.AsString = '') and (qryMasternCdUsuarioConfirma.Value = 0) then
  begin
      VerificaDocPend();
  end;

end;

procedure TfrmRemessa_Numerarios.btExcluirClick(Sender: TObject);
begin
  if (qryMasterdDtFech.AsString = '') and (qryMasterdDtCancel.AsString = '') and (qryMasternCdUsuarioConfirma.Value = 0) then
      inherited
  else
      MensagemAlerta('Status da Remessa de Numer�rios n�o permite mais sua exclus�o.');
end;

procedure TfrmRemessa_Numerarios.qryMasterBeforeDelete(DataSet: TDataSet);
begin
  //inherited;
  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM ChequeRemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
  qryAux.ExecSQL;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM CartaoRemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
  qryAux.ExecSQL;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM CrediarioRemessaNum WHERE nCdRemessaNum = ' + qryMasternCdRemessaNum.AsString) ;
  qryAux.ExecSQL;
end;

procedure TfrmRemessa_Numerarios.qryCartaoRemessaNumCalcFields(
  DataSet: TDataSet);
begin
  {--inherited;--}

  PosicionaQuery(qryTransacaoCartao, qryCartaoRemessaNumnCdTransacaoCartao.AsString) ;
  
  if not qryTransacaoCartao.eof then
  begin

      qryCartaoRemessaNumcNmOperadora.Value    := qryTransacaoCartaocNmOperadoraCartao.Value ;
      qryCartaoRemessaNumiNrDocto.Value        := qryTransacaoCartaoiNrDocto.asString ;
      qryCartaoRemessaNumiNrAutorizacao.Value  := qryTransacaoCartaoiNrAutorizacao.asString ;
      qryCartaoRemessaNumdDtTransacao.Value    := qryTransacaoCartaodDtTransacao.Value ;
      qryCartaoRemessaNumnValTransacao.Value   := qryTransacaoCartaonValTransacao.Value ;

  end ;

end;

initialization
    RegisterClass(TfrmRemessa_Numerarios) ;

end.
