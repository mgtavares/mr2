unit fBaixaTituloDeposito_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit,
  DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid;

type
  TfrmBaixaTituloDeposito_Titulos = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtSaldoBaixar: TcxCurrencyEdit;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    qryTitulos: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryTitulo_NaoSelec: TADOQuery;
    qryTitulo_NaoSelecnCdTitulo: TIntegerField;
    qryTitulo_NaoSeleccNrNF: TStringField;
    qryTitulo_NaoSeleccNrTit: TStringField;
    qryTitulo_NaoSeleciParcela: TIntegerField;
    qryTitulo_NaoSelecdDtEmissao: TDateTimeField;
    qryTitulo_NaoSelecdDtVenc: TDateTimeField;
    qryTitulo_NaoSelecnSaldoTit: TBCDField;
    qryTitulo_NaoSeleccNmEspTit: TStringField;
    qryTitulo_NaoSeleccNmTerceiro: TStringField;
    qryTitulo_Selec: TADOQuery;
    qryTitulo_SelecnCdTitulo: TIntegerField;
    qryTitulo_SeleccNrNF: TStringField;
    qryTitulo_SeleccNrTit: TStringField;
    qryTitulo_SeleciParcela: TIntegerField;
    qryTitulo_SelecdDtEmissao: TDateTimeField;
    qryTitulo_SelecdDtVenc: TDateTimeField;
    qryTitulo_SelecnSaldoTit: TBCDField;
    qryTitulo_SeleccNmEspTit: TStringField;
    qryTitulo_SeleccNmTerceiro: TStringField;
    dsTitulo_NaoSelec: TDataSource;
    dsTitulo_Selec: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    qryAux: TADOQuery;
    qryTitulo_SelecnValLiq: TBCDField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1cNrNF: TcxGridDBColumn;
    cxGridDBTableView1cNrTit: TcxGridDBColumn;
    cxGridDBTableView1iParcela: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1nValLiq: TcxGridDBColumn;
    cxGridDBTableView1cNmEspTit: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    SP_PROCESSA_BAIXA_TITULO_DEPOSITO: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro  : integer ;
    nSaldo       : double ;
    dDtDeposito  : Tdatetime ;
    nCdLanctoFin : integer ;
  end;

var
  frmBaixaTituloDeposito_Titulos: TfrmBaixaTituloDeposito_Titulos;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmBaixaTituloDeposito_Titulos.FormShow(Sender: TObject);
begin
  inherited;

  edtSaldoBaixar.Value := nSaldo ;

  qryPreparaTemp.ExecSQL;

  qryPopulaTemp.Close ;
  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
  qryPopulaTemp.ExecSQL;

  qryTitulo_NaoSelec.Close ;
  qryTitulo_NaoSelec.Open  ;

  qryTitulo_Selec.Close ;
  qryTitulo_Selec.Open  ;

  if (qryTitulo_NaoSelec.Eof) then
      MensagemAlerta('Nenhum t�tulo em aberto para este terceiro credor.') ;

  cxGrid1.SetFocus ;

end;

procedure TfrmBaixaTituloDeposito_Titulos.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  if (not qryTitulo_NaoSelec.eof) and (qryTitulo_NaoSelecnCdTitulo.Value > 0) then
  begin

      if (edtSaldoBaixar.Value <= 0) then
      begin
          MensagemAlerta('Nenhum saldo dispon�vel para baixa.') ;
          abort ;
      end ;

      if (qryTitulo_NaoSelecdDtVenc.Value > Date) then
      begin
          case MessageDlg('Este t�tulo ainda n�o venceu. Confirma a sele��o ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;
      end ;

      qryAux.SQL.Clear ;

      try

          if (qryTitulo_NaoSelecnSaldoTit.Value <= edtSaldoBaixar.Value) then
          begin
              qryAux.SQL.Add('UPDATE #Temp_Titulos SET cFlgAux = 1, nValLiq = nSaldoTit WHERE nCdTitulo = ' + qryTitulo_NaoSelecnCdTitulo.AsString) ;
              qryAux.Parameters.Clear;
              edtSaldoBaixar.Value := edtSaldoBaixar.Value - qryTitulo_NaoSelecnSaldoTit.Value ;
          end
          else
          begin
              qryAux.Parameters.Clear;
              qryAux.Prepared := False ;
              qryAux.SQL.Add('UPDATE #Temp_Titulos SET cFlgAux = 1, nValLiq = :nValLiq WHERE nCdTitulo = ' + qryTitulo_NaoSelecnCdTitulo.AsString) ;
              qryAux.Prepared := True ;
              qryaux.Parameters.ParamByName('nValLiq').Value := edtSaldoBaixar.Value ;
              edtSaldoBaixar.Value := 0 ;
          end ;


          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      qryTitulo_NaoSelec.Close ;
      qryTitulo_NaoSelec.Open ;

      qryTitulo_Selec.Close ;
      qryTitulo_Selec.Open ;

  end ;

end;

procedure TfrmBaixaTituloDeposito_Titulos.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  if (not qryTitulo_Selec.eof) and (qryTitulo_SelecnCdTitulo.Value > 0) then
  begin

      qryAux.SQL.Clear ;

      try

          qryAux.SQL.Add('UPDATE #Temp_Titulos SET cFlgAux = 0, nValLiq = 0 WHERE nCdTitulo = ' + qryTitulo_SelecnCdTitulo.AsString) ;
          qryAux.Parameters.Clear;
          edtSaldoBaixar.Value := edtSaldoBaixar.Value + qryTitulo_SelecnValLiq.Value ;

          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      qryTitulo_NaoSelec.Close ;
      qryTitulo_NaoSelec.Open ;

      qryTitulo_Selec.Close ;
      qryTitulo_Selec.Open ;

  end ;

end;

procedure TfrmBaixaTituloDeposito_Titulos.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryTitulo_Selec.Eof) or (qryTitulo_Selec.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum t�tulo selecionado para baixa.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a baixa dos t�tulos selecionados ?' +#13#13 + 'Este processo n�o poder� ser estornado!!!',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_BAIXA_TITULO_DEPOSITO.Close ;
      SP_PROCESSA_BAIXA_TITULO_DEPOSITO.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
      SP_PROCESSA_BAIXA_TITULO_DEPOSITO.Parameters.ParamByName('@dDtDeposito').Value  := DateToStr(dDtDeposito) ;
      SP_PROCESSA_BAIXA_TITULO_DEPOSITO.Parameters.ParamByName('@nCdLanctoFin').Value := nCdLanctoFin ;
      SP_PROCESSA_BAIXA_TITULO_DEPOSITO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('T�tulos Baixados com Sucesso!') ;

  close ;

end;

end.
