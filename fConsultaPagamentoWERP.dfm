inherited frmConsultaPagamentoWERP: TfrmConsultaPagamentoWERP
  Left = -8
  Top = -8
  Width = 1456
  Height = 886
  Caption = 'Consulta de Pagamentos - WERP'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 105
    Width = 1440
    Height = 745
  end
  inherited ToolBar1: TToolBar
    Width = 1440
    ButtonWidth = 98
    inherited ToolButton1: TToolButton
      Caption = 'Exibir Consulta'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 98
    end
    inherited ToolButton2: TToolButton
      Left = 106
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1440
    Height = 76
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 56
      Top = 24
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 4
      Top = 48
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Pagamento'
    end
    object Label6: TLabel
      Tag = 1
      Left = 188
      Top = 48
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object MaskEdit1: TMaskEdit
      Left = 104
      Top = 40
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object MaskEdit2: TMaskEdit
      Left = 216
      Top = 40
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Tag = 1
      Left = 104
      Top = 16
      Width = 63
      Height = 21
      Enabled = False
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 176
      Top = 16
      Width = 617
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = DataSource1
      TabOrder = 3
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 105
    Width = 1440
    Height = 745
    Align = alClient
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsDoctoFiscal
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Position = spFooter
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValMov
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1dDtMov: TcxGridDBColumn
        Caption = 'Data Movto.'
        DataBinding.FieldName = 'dDtMov'
      end
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        Caption = 'ID T'#237'tulo'
        DataBinding.FieldName = 'nCdTitulo'
        Width = 90
      end
      object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
        Caption = 'N'#250'm. T'#237'tulo'
        DataBinding.FieldName = 'cNrTit'
      end
      object cxGrid1DBTableView1iParcela: TcxGridDBColumn
        Caption = 'Parcela'
        DataBinding.FieldName = 'iParcela'
        Width = 73
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        Caption = 'N'#250'm. NF'
        DataBinding.FieldName = 'cNrNF'
      end
      object cxGrid1DBTableView1cSigla: TcxGridDBColumn
        DataBinding.FieldName = 'cSigla'
        Visible = False
      end
      object cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn
        Caption = 'Categoria Financeira'
        DataBinding.FieldName = 'cNmCategFinanc'
        Width = 233
      end
      object cxGrid1DBTableView1cNmOperacao: TcxGridDBColumn
        Caption = 'Opera'#231#227'o Financeira'
        DataBinding.FieldName = 'cNmOperacao'
        Width = 181
      end
      object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
        Caption = 'N'#250'm. Doc.'
        DataBinding.FieldName = 'cNrDoc'
      end
      object cxGrid1DBTableView1nValMov: TcxGridDBColumn
        Caption = 'Valor Movto.'
        DataBinding.FieldName = 'nValMov'
        Width = 115
      end
      object cxGrid1DBTableView1cSenso: TcxGridDBColumn
        Caption = 'Senso'
        DataBinding.FieldName = 'cSenso'
        Width = 61
      end
      object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
        Caption = 'Esp'#233'cie de T'#237'tulo'
        DataBinding.FieldName = 'cNmEspTit'
        Width = 214
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 576
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryMovimentacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@iNrNf       int'
      '       ,@dDtInicial  varchar(10)'
      '       ,@dDtFinal    varchar(10)'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @dDtInicial  = :dDtInicial'
      'Set @dDtFinal    = :dDtFinal'
      ''
      'SELECT dbo.fn_OnlyDate(dDtMov) dDtMov'
      #9#9'  ,MTitulo.nCdTitulo'
      '          ,Titulo.cNrTit'
      '          ,Titulo.iParcela'
      '          ,cSigla'
      #9#9'  ,cNmCategFinanc'
      #9#9'  ,cNmOperacao'
      #9#9'  ,nValMov'
      #9#9'  ,CASE WHEN cSenso = '#39'C'#39' THEN'
      #9#9#9#9#9'CASE WHEN cTipoOper = '#39'P'#39' THEN '#39'C'#39
      #9#9#9#9#9#9' WHEN cTipoOper = '#39'J'#39' THEN '#39'C'#39
      #9#9#9#9#9#9' WHEN cTipoOper = '#39'D'#39' THEN '#39'D'#39
      #9#9#9#9#9'END'
      #9#9#9#9'ELSE'
      #9#9#9#9#9'CASE WHEN cTipoOper = '#39'P'#39' THEN '#39'D'#39
      #9#9#9#9#9#9' WHEN cTipoOper = '#39'J'#39' THEN '#39'D'#39
      #9#9#9#9#9#9' WHEN cTipoOper = '#39'D'#39' THEN '#39'C'#39
      #9#9#9#9#9'END'
      #9#9'   END AS cSenso'
      '          ,cNmEspTit'
      '          ,Titulo.cNrNF'
      '       ,MTitulo.nCdOperacao'
      '       ,MTitulo.cNrDoc'
      #9'  FROM MTitulo'
      
        #9#9'   LEFT  JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria ' +
        '= MTitulo.nCdContaBancaria'
      
        #9#9'   LEFT  JOIN FormaPagto    ON FormaPagto.nCdFormaPagto       ' +
        '= MTitulo.nCdFormaPagto'
      
        #9#9'   INNER JOIN Operacao      ON Operacao.nCdOperacao           ' +
        '= MTitulo.nCdOperacao'
      
        #9#9'   INNER JOIN Titulo        ON Titulo.nCdTitulo               ' +
        '= MTitulo.nCdTitulo'
      
        #9#9'   INNER JOIN Terceiro      ON Terceiro.nCdTerceiro           ' +
        '= Titulo.nCdTerceiro'
      
        #9#9'   INNER JOIN CategFinanc   ON CategFinanc.nCdCategFinanc     ' +
        '= Titulo.nCdCategFinanc'
      
        '           INNER JOIN Empresa       ON Empresa.nCdEmpresa       ' +
        '      = Titulo.nCdEmpresa'
      
        '           INNER JOIN EspTit        ON EspTit.nCdEspTit         ' +
        '      = Titulo.nCdEspTit'
      '     WHERE Titulo.nCdEmpresa         = :nCdEmpresa'
      '       AND Titulo.nCdTerceiro        = @nCdTerceiro'
      
        '       AND MTitulo.dDtMov           >= Convert(DATETIME,@dDtInic' +
        'ial,103)'
      
        '       AND MTitulo.dDtMov            < Convert(DATETIME,@dDtFina' +
        'l,103)+1'
      '       AND Operacao.cTipoOper = '#39'P'#39
      '       AND Titulo.cSenso      = '#39'C'#39
      '       AND MTitulo.dDtCancel IS NULL'
      '     ORDER BY dDtMov'
      '             ,nCdTitulo')
    Left = 560
    Top = 248
    object qryMovimentacaodDtMov: TDateTimeField
      FieldName = 'dDtMov'
      ReadOnly = True
    end
    object qryMovimentacaonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMovimentacaocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMovimentacaoiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryMovimentacaocSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryMovimentacaocNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryMovimentacaocNmOperacao: TStringField
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object qryMovimentacaonValMov: TBCDField
      FieldName = 'nValMov'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMovimentacaocSenso: TStringField
      FieldName = 'cSenso'
      ReadOnly = True
      Size = 1
    end
    object qryMovimentacaocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryMovimentacaocNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryMovimentacaonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMovimentacaocNrDoc: TStringField
      FieldName = 'cNrDoc'
      FixedChar = True
      Size = 15
    end
  end
  object dsDoctoFiscal: TDataSource
    DataSet = qryMovimentacao
    Left = 600
    Top = 248
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa '
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 424
    Top = 176
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 384
    Top = 240
  end
end
