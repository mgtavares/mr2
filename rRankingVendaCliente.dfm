inherited rptRankingVendaCliente: TrptRankingVendaCliente
  Caption = 'Ranking de Vendas - Cliente'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 37
    Top = 96
    Width = 69
    Height = 13
    Caption = 'Per'#237'odo Venda'
  end
  object Label6: TLabel [3]
    Left = 188
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [4]
    Left = 24
    Top = 72
    Width = 82
    Height = 13
    Caption = 'Grupo Economico'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [6]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [7]
    Tag = 1
    Left = 256
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 3
  end
  object MaskEdit1: TMaskEdit [8]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [9]
    Left = 208
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [10]
    Left = 112
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 64
    Width = 653
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 7
  end
  object MaskEdit4: TMaskEdit [12]
    Left = 112
    Top = 64
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 288
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 480
    Top = 280
  end
end
