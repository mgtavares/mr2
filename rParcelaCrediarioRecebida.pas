unit rParcelaCrediarioRecebida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, ER2Excel;

type
  TrptParcelaCrediarioRecebida = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit6: TMaskEdit;
    RadioGroup1: TRadioGroup;
    MaskEdit4: TMaskEdit;
    DBEdit4: TDBEdit;
    qryLojaEmissora: TADOQuery;
    qryLojaEmissoracNmLoja: TStringField;
    Image2: TImage;
    dsLoja: TDataSource;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    MaskEdit8: TMaskEdit;
    Label4: TLabel;
    MaskEdit9: TMaskEdit;
    Label5: TLabel;
    RadioGroup3: TRadioGroup;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    Label1: TLabel;
    MaskEdit5: TMaskEdit;
    qryLojaRecebedora: TADOQuery;
    qryCaixaRecebimento: TADOQuery;
    qryLojaRecebedoracNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLojaRecebedora: TDataSource;
    qryCaixaRecebimentonCdContaBancaria: TIntegerField;
    qryCaixaRecebimentonCdConta: TStringField;
    qryCaixaRecebimentonCdLoja: TStringField;
    MaskEdit7: TMaskEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    dsCaixaRecebimento: TDataSource;
    RadioGroup2: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptParcelaCrediarioRecebida: TrptParcelaCrediarioRecebida;

implementation

uses fMenu, rImpSP, fLookup_Padrao,rParcelaCrediarioRecebida_view;

{$R *.dfm}

procedure TrptParcelaCrediarioRecebida.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  qryLojaEmissora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit4.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLojaEmissora, MaskEdit4.text) ;
  end
  else
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;

end;


procedure TrptParcelaCrediarioRecebida.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptParcelaCrediarioRecebida.ToolButton1Click(Sender: TObject);
var
  objRel : TrptParcelaCrediarioRecebida_view ;
  iLinha : integer;
begin

  objRel := TrptParcelaCrediarioRecebida_view.Create(Self) ;

  objRel.usp_Relatorio.Close ;

  {--posiciona qryEmpresa para verificar se a empresa digitada � valida
   --e tem acesso permitido para o usu�rio logado--}

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Open;

  if qryEmpresa.Eof then
      MaskEdit3.Text := '';

  {--posiciona qryLojaEmissora para verificar se a loja digitada � valida
   --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

  qryLojaEmissora.Close ;

  if (Trim(MaskEdit4.Text) <> '') then
  begin

      qryLojaEmissora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryLojaEmissora.Parameters.ParamByName('nPK').Value := MaskEdit4.Text ;
      qryLojaEmissora.Open ;

      if qryLojaEmissora.Eof then
          MaskEdit4.Text := '';

  end;

  {--posiciona qryLojaRecebedora para verificar se a loja digitada � valida
   --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

  qryLojaRecebedora.Close ;

  if (Trim(MaskEdit5.Text) <> '') then
  begin
      qryLojaRecebedora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryLojaRecebedora.Parameters.ParamByName('nPK').Value := MaskEdit5.Text ;
      qryLojaRecebedora.Open ;

      if qryLojaRecebedora.Eof then
          MaskEdit5.Text := '';

  end;

  {-- verifica se uma Loja recebedora foi informada para utilizar um
   -- Caixa recebimento --}

  if((Trim(MaskEdit5.Text)='') and (Trim(MaskEdit7.Text)<>''))then
  begin
      MensagemAlerta('Para selecionar um Caixa Recebimento, selecione primeiro uma Loja Recebedora.');
      MaskEdit7.Text := '';
      MaskEdit5.SetFocus;
      Abort;
  end;

  {--aqui passa os valores para os parametros de entrada da procedure--}

  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLojaEmissora').Value   := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLojaRecebedora').Value := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdCaixaReceb').Value     := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado ;

  if (Trim(MaskEdit6.Text) <> '') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdCliente').Value        := frmMenu.ConvInteiro(MaskEdit6.Text)
  else objRel.usp_Relatorio.Parameters.ParamByName('@nCdCliente').Value       := 0;

  {-- faz verifica��o das flags --}

  if (RadioGroup3.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteReneg').Value := 1
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteReneg').Value := 0 ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteAtraso').Value := '1'
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteAtraso').Value := '0' ;

  {-- verifica a data e preenche(quando vazio) para listar tudo --}

  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoInicial').Value := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoFinal').Value   := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtLiquidaInicial').Value := frmMenu.ConvData(MaskEdit9.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtLiquidaFinal').Value   := frmMenu.ConvData(MaskEdit8.Text) ;

  {-- abre a procedure para exibi��o dos dados no relat�rio --}

  objRel.usp_Relatorio.Open ;

  {-- aqui preenche os filtros do relat�rio--}

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (Trim(DBedit3.Text) <> '') then
      objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

  if (Trim(MaskEdit4.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja Emissora : ' + trim(MaskEdit4.Text) + '-' + DbEdit4.Text ;

  if (Trim(MaskEdit5.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja Recebedora : ' + trim(MaskEdit5.Text) + '-' + DbEdit1.Text ;

  if (Trim(MaskEdit7.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Caixa Recebimento : ' + trim(MaskEdit7.Text) + '-' + DbEdit5.Text ;

  if (Trim(DBedit10.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Cliente: ' + trim(MaskEdit6.Text) + '-' + DbEdit10.Text ;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Emiss�o: ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

  if (((Trim(MaskEdit9.Text)) <> '/  /') or ((Trim(MaskEdit8.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Liquida��o: ' + trim(MaskEdit9.Text) + '-' + trim(MaskEdit8.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Recebidas em Atraso: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Recebidas em Atraso: N�o' ;

  if (RadioGroup3.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Renegocia��o Recebida: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Renegocia��o Recebida: N�o' ;

  try
      try
          {--visualiza o relat�rio--}
          case RadioGroup2.ItemIndex of
          0:begin
                objRel.QuickRep1.PreviewModal;
            end;
          1:begin
                frmMenu.mensagemUsuario('Exportando Planilha...');
                { -- formata c�lulas -- }
                ER2Excel.Celula['A1'].Mesclar('Q1');
                ER2Excel.Celula['A2'].Mesclar('Q2');
                ER2Excel.Celula['A3'].Mesclar('Q3');
                ER2Excel.Celula['A4'].Mesclar('C4');
                ER2Excel.Celula['A5'].Mesclar('C5');
                ER2Excel.Celula['D4'].Mesclar('Q4');

                ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A1'].Range('A4');
                ER2Excel.Celula['D4'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A5'].Range('Q5');
                ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A6'].Range('Q6');

                { -- inseri informa��es do cabe�alho -- }
                ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                ER2Excel.Celula['A2'].Text := 'Rel. Parcela Crediario Recebida.';
                ER2Excel.Celula['A4'].Text := 'Loja Emissora: ' + DBEdit4.Text;
                ER2Excel.Celula['A5'].Text := 'Loja Recebedora: ' + DBEdit1.Text;
                ER2Excel.Celula['D4'].Text := 'Cliente: ' + DBEdit10.Text;
                ER2Excel.Celula['A6'].Text := 'Cod. Titulo';
                ER2Excel.Celula['B6'].Text := 'Cod. Empresa';
                ER2Excel.Celula['C6'].Text := 'Cod. Loja Emissora';
                ER2Excel.Celula['D6'].Text := 'Carnet';
                ER2Excel.Celula['E6'].Text := 'Parcela';
                ER2Excel.Celula['F6'].Text := 'Cod. Cliente';
                ER2Excel.Celula['G6'].Text := 'Nome Cliente';
                ER2Excel.Celula['H6'].Text := 'Data Emissao';
                ER2Excel.Celula['I6'].Text := 'Data Vencimento';
                ER2Excel.Celula['J6'].Text := 'Data Liq';
                ER2Excel.Celula['K6'].Text := 'Hora Liq';
                ER2Excel.Celula['L6'].Text := 'Valor Parcela';
                ER2Excel.Celula['M6'].Text := 'Valor Juros';
                ER2Excel.Celula['N6'].Text := 'Valor Recebido';
                ER2Excel.Celula['O6'].Text := 'Cod. Loja Recebido';
                ER2Excel.Celula['P6'].Text := 'Cod. Caixa Recebido';
                ER2Excel.Celula['Q6'].Text := 'Nome Caixa Recebido';
                ER2Excel.Celula['R6'].Text := 'Renegocia��o';

                iLinha := 7;
                ER2Excel.Celula['A1'].Congelar('A7');

                objRel.usp_Relatorio.First;

                while (not objRel.usp_Relatorio.Eof) do
                begin
                    ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdTitulo.Value;
                    ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdEmpresa.Value;
                    ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdLojaEmi.Value;
                    ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNrCarnet.Value;
                    ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.usp_RelatorioiParcela.Value;
                    ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdCliente.Value;
                    ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNmCliente.Value;
                    ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtEmissao.Value;
                    ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtVenc.Value;
                    ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtLiq.Value;
                    ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocDtLiqHora.Value;
                    ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValParcela.Value;
                    ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValJuros.Value;
                    ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValRecebido.Value;
                    ER2Excel.Celula['O' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdLojaReceb.Value;
                    ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdCaixaReceb.Value;
                    ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNmCaixaReceb.Value;
                    ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocRenegociacao.Value;

                    objRel.usp_Relatorio.Next;

                    Inc(iLinha);
                end;

                ER2Excel.AutoSizeCol := true;

                { -- exporta planilha e limpa result do componente -- }
                ER2Excel.ExportXLS;
                ER2Excel.CleanupInstance;
            end;
          end;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptParcelaCrediarioRecebida.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(180);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioRecebida.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioRecebida.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryLojaEmissora.Close ;
  qryLojaEmissora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLojaEmissora, MaskEdit4.Text) ;

end;

procedure TrptParcelaCrediarioRecebida.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioRecebida.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptParcelaCrediarioRecebida.MaskEdit7Exit(Sender: TObject);
begin
  inherited;

  if((Trim(MaskEdit5.Text)='') and (Trim(MaskEdit7.Text)<>''))then
  begin
      MensagemAlerta('Para selecionar um Caixa Recebimento, selecione primeiro uma Loja Recebedora.');
      MaskEdit7.Text := '';
      MaskEdit5.SetFocus;
      Abort;
  end
  else
  begin
      qryCaixaRecebimento.Close ;
      qryCaixaRecebimento.Parameters.ParamByName('nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      qryCaixaRecebimento.Parameters.ParamByName('nCdLojaRecebedora').Value := frmMenu.ConvInteiro(MaskEdit5.Text);
      PosicionaQuery(qryCaixaRecebimento, MaskEdit7.Text) ;
  end;
end;

procedure TrptParcelaCrediarioRecebida.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if ((Trim(MaskEdit5.Text)='')) then
        begin
            MensagemAlerta('Para selecionar um Caixa Recebimento, selecione primeiro uma Loja Recebedora.');
            MaskEdit5.SetFocus;
            Abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND UL.nCdLoja = ContaBancaria.nCdLoja) AND EXISTS(SELECT 1 FROM UsuarioEmpresa UE WHERE UE.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND UE.nCdEmpresa = ContaBancaria.nCdEmpresa) AND ContaBancaria.nCdLoja = '+IntToStr(frmMenu.ConvInteiro(MaskEdit5.Text)));

            If (nPK > 0) then
            begin
                Maskedit7.Text := IntToStr(nPK) ;
            end ;
        end;
    end ;

  end ;
end;

procedure TrptParcelaCrediarioRecebida.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryLojaRecebedora.Close ;
  qryLojaRecebedora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLojaRecebedora, MaskEdit5.Text) ;
end;

procedure TrptParcelaCrediarioRecebida.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptParcelaCrediarioRecebida) ;

end.
