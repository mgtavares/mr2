unit fEnviaLoteXMLNFe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Mask,
  StdCtrls, DBCtrls, DB, ADODB, ER2Lookup, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxLookAndFeelPainters, cxButtons,
  cxCheckBox, tsqZip, IdSMTP, IdMessage, cxContainer, cxTextEdit,
  IdBaseComponent, IdComponent, IdIOHandler, IdIOHandlerSocket,
  IdSSLOpenSSL;

type
  TfrmEnviaLoteXMLNFe = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label9: TLabel;
    er2LkpLoja: TER2LookupMaskEdit;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryTerceiroDestino: TADOQuery;
    qryTempDoctoFiscal: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit4: TDBEdit;
    dsLoja: TDataSource;
    qryTerceiroDestinonCdTerceiro: TIntegerField;
    qryTerceiroDestinocNmTerceiro: TStringField;
    qryTerceiroDestinocCNPJCPF: TStringField;
    dsTerceiroDestino: TDataSource;
    mskMesAno: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    qryTempDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryTempDoctoFiscalnCdTerceiro: TIntegerField;
    qryTempDoctoFiscalcNmTerceiro: TStringField;
    qryTempDoctoFiscalcCNPJCPF: TStringField;
    qryTempDoctoFiscalcFlgEntSai: TStringField;
    qryTempDoctoFiscaliNrDocto: TIntegerField;
    qryTempDoctoFiscalcSerie: TStringField;
    qryTempDoctoFiscalcChaveNFe: TStringField;
    qryTempDoctoFiscalcNrProtocoloNFe: TStringField;
    qryTempDoctoFiscalcNrReciboNFe: TStringField;
    qryTempDoctoFiscalcCaminhoXML: TStringField;
    qryTempDoctoFiscalcArquivoXML: TStringField;
    qryTempDoctoFiscaldDtEmissao: TDateTimeField;
    qryTempDoctoFiscaldDtImpressao: TDateTimeField;
    qryTempDoctoFiscalnValTotal: TBCDField;
    qryTempDoctoFiscalnValFatura: TBCDField;
    dsTempDoctoFiscal: TDataSource;
    cxGridDoctoFiscal: TcxGrid;
    cxGridDoctoFiscalDBTableView1: TcxGridDBTableView;
    cxGridDoctoFiscalLevel1: TcxGridLevel;
    ToolButton4: TToolButton;
    btOpcao: TToolButton;
    menuOpcao: TPopupMenu;
    qryTerceiroDestinocEmailNFe: TStringField;
    qryTerceiroDestinocEmailCopiaNFe: TStringField;
    qryTerceiroDestinocFlgEnviaNFeEmail: TIntegerField;
    btTerceiroDestino: TMenuItem;
    cxGridDoctoFiscalDBTableView1DBColumn_nCdDoctoFiscal: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cNmTerceiro: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cCNPJCPF: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cFlgEntSai: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_iNrDocto: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cSerie: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cChaveNFe: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cNrProtocoloNFe: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_cNrReciboNFe: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_dDtEmissao: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_dDtImpressao: TcxGridDBColumn;
    cxGridDoctoFiscalDBTableView1DBColumn_nValTotal: TcxGridDBColumn;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    er2LkpTerceiroDestino: TER2LookupMaskEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    Label5: TLabel;
    DBEdit7: TDBEdit;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    dsTerceiro: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocEmailNFe: TStringField;
    qryTerceirocEmailCopiaNFe: TStringField;
    qryTerceirocFlgEnviaNFeEmail: TIntegerField;
    er2LkpTerceiro: TER2LookupMaskEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    cxGridDoctoFiscalDBTableView1DBColumn_cFlgAtivo: TcxGridDBColumn;
    qryTempDoctoFiscalcFlgAtivo: TIntegerField;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecServidorSMTP: TStringField;
    qryConfigAmbientenPortaSMTP: TIntegerField;
    qryConfigAmbientecEmail: TStringField;
    qryConfigAmbientecSenhaEmail: TStringField;
    qryConfigAmbientecFlgUsaSSL: TIntegerField;
    qryAux: TADOQuery;
    panel: TPanel;
    btEnviarLoteEmail: TcxButton;
    btMarcarTodos: TcxButton;
    btDesmarcarTodos: TcxButton;
    tsqZip1: TtsqZip;
    qryConfigAmbientecPathArqNFeProd: TStringField;
    btGerarZip: TcxButton;
    popGerarXML: TPopupMenu;
    GerarXMLNFe1: TMenuItem;
    cxStyleRepository1: TcxStyleRepository;
    NotFound: TcxStyle;
    Ok: TcxStyle;
    cxGridDoctoFiscalDBTableView1DBColumn_cArquivoXML: TcxGridDBColumn;
    SaveDialog1: TSaveDialog;
    btGerarXml: TcxButton;
    rgTipoDocto: TRadioGroup;
    IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket;
    cxGridDoctoFiscalDBTableView1DBColumn_cCaminhoNFe: TcxGridDBColumn;
    qryTempDoctoFiscalSel: TADOQuery;
    qryTempDoctoFiscalSelnCdDoctoFiscal: TIntegerField;
    qryTempDoctoFiscalSelcCaminhoXML: TStringField;
    qryTempDoctoFiscalSelcArquivoXML: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure btTerceiroDestinoClick(Sender: TObject);
    procedure btEnviarLoteEmailClick(Sender: TObject);
    procedure btMarcarTodosClick(Sender: TObject);
    procedure btDesmarcarTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure prVerificaCriaArquivoXML();
    procedure prGeraXML(bConfirma : boolean);
    procedure GerarXMLNFe1Click(Sender: TObject);
    procedure btGerarZipClick(Sender: TObject);
    procedure cxGridDoctoFiscalDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FormCreate(Sender: TObject);
    procedure testeClick(Sender: TObject);
    procedure btGerarXmlClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bStatus         : boolean;
    //cCaminhoArquivo : String;
  end;

var
  frmEnviaLoteXMLNFe: TfrmEnviaLoteXMLNFe;

implementation

{$R *.dfm}

uses
  fMenu, fTerceiro, fDoctoFiscal;

procedure TfrmEnviaLoteXMLNFe.prGeraXML(bConfirma : boolean);
var
  objDocto : TfrmDoctoFiscal;
begin
    try
        {-- Reaproveita o evento de gerar XML da DoctoFiscal --}
        objDocto := TfrmDoctoFiscal.Create(Nil);        
        objDocto.prGerarXMLNFe(qryTempDoctoFiscalSelnCdDoctoFiscal.Value, bConfirma);
    finally
        bStatus := True;
        FreeAndNil(objDocto);
    end;
end;
procedure TfrmEnviaLoteXMLNFe.prVerificaCriaArquivoXML();
begin
    {-- Fun��o para verificar se tem XML e criar um zip, e se n�o tiver, gerar um XML --}
    if (not FileExists(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value)) then
    begin
        frmMenu.mensagemUsuario('');

        if MessageDlg('O documento fiscal ' + qryTempDoctoFiscalSelnCdDoctoFiscal.AsString + ' n�o possui arquivo XML, deseja gerar o arquivo ?',mtConfirmation,[mbYes, mbNo],0) = mrYes then
        begin
            {-- Chama a procedure que cria o XML --}
            prGeraXML(false);
        end
        else
        begin
            MensagemAlerta('Arquivo ' + qryTempDoctoFiscalSelcArquivoXML.Value + ' n�o encontrado.' + #13#13 + 'Dir.: ' + frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value);
            qryTempDoctoFiscalSel.Last;
            bStatus := False;
        end;
    end
    else
    begin
        bStatus := true;
    end;
end;
procedure TfrmEnviaLoteXMLNFe.ToolButton1Click(Sender: TObject);
begin
  inherited;

  { -- valida compet�ncia -- }
  if (Trim(mskMesAno.Text) = '/') then
  begin
      mskMesAno.Text := Copy(DateToStr(Date),4,7);
  end;

  mskMesAno.Text := frmMenu.ZeroEsquerda(mskMesAno.Text,6);

  try
      StrToDate('01/' + mskMesAno.Text);
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa');
      mskMesAno.SetFocus;
      Abort;
  end;

  cmdPreparaTemp.Execute;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value   := qryEmpresanCdEmpresa.Value;
  qryPopulaTemp.Parameters.ParamByName('nCdLoja').Value      := frmMenu.ConvInteiro(er2LkpLoja.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value  := frmMenu.ConvInteiro(er2LkpTerceiro.Text);
  qryPopulaTemp.Parameters.ParamByName('cCompetencia').Value := mskMesAno.Text;
  qryPopulaTemp.Parameters.ParamByName('nCdTipoDocto').Value := rgTipoDocto.ItemIndex + 1;
  qryPopulaTemp.ExecSQL;

  qryTempDoctoFiscal.Close;
  qryTempDoctoFiscal.Open;
end;

procedure TfrmEnviaLoteXMLNFe.btTerceiroDestinoClick(Sender: TObject);
var
  objForm : TfrmTerceiro;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMTERCEIRO')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmTerceiro.Create(nil);

  if (not qryTerceiroDestino.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryTerceiroDestinonCdTerceiro.AsString);

  showForm(objForm,True);

  { -- da refresh no registro  -- }
  if (not qryTerceiroDestino.IsEmpty) then
  begin
      PosicionaQuery(qryTerceiroDestino,qryTerceiroDestinonCdTerceiro.AsString);
  end;
end;

procedure TfrmEnviaLoteXMLNFe.btEnviarLoteEmailClick(Sender: TObject);
var
  bStatus         : Boolean;
  cAssunto        : String;
  cServidor       : String;
  cUsuario        : String;
  cSenha          : String;
  cMensagem       : TStrings;
  iPorta          : Integer;
  idSMTP          : TIdSMTP;
  idMessage       : TIdMessage;
  cCaminhoArquivo : String;
  cTipoDocto      : String;
begin
  inherited;

  if (qryTempDoctoFiscal.IsEmpty) then
      Exit;

  if (qryTerceiroDestino.IsEmpty) then
  begin
      MensagemAlerta('Informe o terceiro destinat�rio.');
      er2LkpTerceiroDestino.SetFocus;
      Abort;
  end;

  if (qryTerceiroDestinocFlgEnviaNFeEmail.Value <> 1 ) then
  begin
      MensagemAlerta('Terceiro destinat�rio n�o configurado para envio de e-mail.');
      er2LkpTerceiroDestino.SetFocus;
      Abort;
  end;

  if (Trim(qryTerceiroDestinocEmailNFe.Value) = '') then
  begin
      MensagemAlerta('E-mail do terceiro destinat�rio n�o informado.');
      er2LkpTerceiroDestino.SetFocus;
      Abort;
  end;

  qryTempDoctoFiscalSel.Close;
  qryTempDoctoFiscalSel.Open;

  if (qryTempDoctoFiscalSel.IsEmpty) then
  begin
      MensagemAlerta('Lote deve conter no m�nimo um documento ativo.');
      cxGridDoctoFiscal.SetFocus;
      Abort;
  end;

  if (MessageDLG('Confirma envio de lote para o e-mail: ' + qryTerceiroDestinocEmailNFe.Value + ' ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
      Exit;

  if (Trim(qryConfigAmbientecEmail.Value) = '') then
  begin
      MensagemAlerta('Ambiente n�o configurado para envio de e-mail.');
      Exit;
  end;

  {-- valida tipo de document --}
  case rgTipoDocto.ItemIndex of
      0: cTipoDocto := 'NFe';
      1: cTipoDocto := 'CFe';
  end;
  
  try
      cCaminhoArquivo := frmMenu.cPathSistema + 'EMP' + frmMenu.ZeroEsquerda(qryEmpresanCdEmpresa.AsString,3) + '\Producao\' + cTipoDocto + '\Lote\LoteXML_' + StringReplace(mskMesAno.Text,'/','',[rfReplaceAll,rfIgnoreCase]) + '.zip';
      bStatus         := True;
      cMensagem       := TStringList.Create;
      idSMTP          := TIdSMTP.Create(Self);
      idMessage       := TIdMessage.Create(Self);

      tsqZip1.FilesToZip.Clear;
      tsqZip1.ZipOptions.RecurseSubDirs := rsdNotRecursive;
      tsqZip1.ZipOptions.ZipFlagOptions := [zfoJunkDir];

      frmMenu.mensagemUsuario('Processando lote...');

      { -- deleta lote se existir e gera novo lote -- }
      if (FileExists(cCaminhoArquivo)) then
          DeleteFile(cCaminhoArquivo);

      while (not qryTempDoctoFiscalSel.Eof) do
      begin
          if (not FileExists(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value)) then
              prGeraXML(false);

          tsqZip1.FilesToZip.Add(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value);

          qryTempDoctoFiscalSel.Next;
      end;

      { -- se ocorreu erro na gera��o do arquivo, finaliza processo -- }
      if (not bStatus) then
          Exit;

      {-- Zipa os documentos --}
      try
          tsqZip1.ZipFileName := cCaminhoArquivo;
          tsqZip1.Execute;
      finally
          tsqZip1.FilesToZip.Clear;
      end;

      try
          { -- prepara e-mail -- }
          frmMenu.mensagemUsuario('Preparando corpo do e-mail...');

          cAssunto := 'Lote Arquivo ' + cTipoDocto + ' (' + mskMesAno.Text + ')';

          cMensagem.Clear;
          cMensagem.Add(qryTerceiroDestinocNmTerceiro.AsString + #13#13);
      
          if (rgTipoDocto.ItemIndex = 0) then
              cMensagem.Add('Entrega de arquivos XML das notas fiscais eletr�nicas gerados em ' + mskMesAno.Text + '.')
          else
              cMensagem.Add('Entrega de arquivos XML dos cupons fiscais eletr�nicos gerados em ' + mskMesAno.Text + '.');
          
          cMensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
          cMensagem.Add('Conhe�a o Sistema de Gest�o Empresarial que integra sua empresa e seus parceiros de neg�cios com simplicidade.');
          cMensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades das empresas com baixo custo.');
          cMensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');

          frmMenu.mensagemUsuario('Enviando e-mail...');

          { -- configura e-mail -- }
          cServidor := qryConfigAmbientecServidorSMTP.Value;
          iPorta    := qryConfigAmbientenPortaSMTP.Value;
          cUsuario  := qryConfigAmbientecEmail.Value;
          cSenha    := qryConfigAmbientecSenhaEmail.Value;

          { -- configura conex�o para envio atrav�s do INDY -- }
          idSMTP.Host               := cServidor;
          idSMTP.Port               := iPorta;
          idSMTP.Username           := cUsuario;
          idSMTP.Password           := cSenha;
          idSMTP.AuthenticationType := atLogin;

          if (qryConfigAmbientecFlgUsaSSL.Value = 1) then
          begin
              idSMTP.IOHandler                        := IdSSLIOHandlerSocket1;
              IdSSLIOHandlerSocket1.SSLOptions.Method := sslvTLSv1;
              IdSSLIOHandlerSocket1.PassThrough       := true;
              IdSSLIOHandlerSocket1.SSLOptions.Mode   := sslmClient;
          end;

          { -- prepara os dados da mensagem -- }
          idMessage.From.Address              := cUsuario;
          idMessage.Recipients.EMailAddresses := qryTerceiroDestinocEmailNFe.AsString;
          idMessage.CCList.EMailAddresses     := qryTerceiroDestinocEmailCopiaNFe.AsString;
          idMessage.Subject                   := cAssunto;
          idMessage.Body                      := cMensagem;

          { -- anexa lote de xml � mensagem -- }
          TIdAttachment.Create(idMessage.MessageParts, cCaminhoArquivo);

          { -- envia o email -- }
          try
              idSMTP.Connect(2000);
              if(qryConfigAmbientecFlgUsaSSL.Value = 1) then
              begin
                  idSMTP.SendCmd('STARTTLS', 220);
                  IdSSLIOHandlerSocket1.PassThrough := false;
                  idSMTP.Authenticate;
              end;
              idSMTP.Send(idMessage);
          except on E:Exception do
              begin
                  frmMenu.MensagemErro('Erro: ' + E.Message);

              if (idSMTP.Connected) then idSMTP.Disconnect;
                  frmMenu.mensagemUsuario('');
                  Raise;
              end;
          end;

          if (idSMTP.Connected) then idSMTP.Disconnect;

          if (Trim(qryTerceiroDestinocEmailCopiaNFe.Value) <> '') then
              ShowMessage('Lote de arquivos enviado por e-mail com sucesso.' + #13#13 + 'E-mail de destino: ' + qryTerceiroDestinocEmailNFe.Value + ' e ' + qryTerceiroDestinocEmailCopiaNFe.Value + '.')
          else
              ShowMessage('Lote de arquivos enviado por e-mail com sucesso.' + #13#13 + 'E-mail de destino: ' + qryTerceiroDestinocEmailNFe.Value + '.');

          { -- exclui lote de arquivos -- }
          {if (FileExists(cCaminhoArquivo)) then
              DeleteFile(cCaminhoArquivo);}

          qryLoja.Close;
          qryTerceiro.Close;
          qryTerceiroDestino.Close;
          qryTempDoctoFiscal.Close;
          qryTempDoctoFiscalSel.Close;
          er2LkpLoja.Clear;
          er2LkpTerceiro.Clear;
          er2LkpTerceiroDestino.Clear;
          mskMesAno.Clear;

          mskMesAno.SetFocus;
      except
          MensagemErro('N�o foi poss�vel enviar o e-mail, verifique as configura��es de email e/ou sua conex�o.');
      end;
  finally
      FreeAndNil(cMensagem);
      FreeAndNil(idSMTP);
      FreeAndNil(idMessage);
  end;

  frmMenu.mensagemUsuario('');
end;

procedure TfrmEnviaLoteXMLNFe.btMarcarTodosClick(Sender: TObject);
begin
  inherited;

  if (qryTempDoctoFiscal.IsEmpty) then
      Exit;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempDoctoFiscal SET cFlgAtivo = 1');
  qryAux.ExecSQL;

  qryTempDoctoFiscal.Close;
  qryTempDoctoFiscal.Open;
end;

procedure TfrmEnviaLoteXMLNFe.btDesmarcarTodosClick(Sender: TObject);
begin
  inherited;

  if (qryTempDoctoFiscal.IsEmpty) then
      Exit;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempDoctoFiscal SET cFlgAtivo = 0');
  qryAux.ExecSQL;

  qryTempDoctoFiscal.Close;
  qryTempDoctoFiscal.Open;
end;

procedure TfrmEnviaLoteXMLNFe.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryConfigAmbiente, frmMenu.cNomeComputador);
  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      desativaER2LkpMaskEdit(er2LkpLoja);
  end;
end;

procedure TfrmEnviaLoteXMLNFe.GerarXMLNFe1Click(Sender: TObject);
begin
  inherited;
  prGeraXML(true);
end;

procedure TfrmEnviaLoteXMLNFe.btGerarZipClick(Sender: TObject);
var
  cCaminhoZip : String;
  cTipoDocto  : String;
begin
  inherited;

  qryTempDoctoFiscalSel.Close;
  qryTempDoctoFiscalSel.Open;

  if (qryTempDoctoFiscalSel.IsEmpty) then
  begin
      MensagemAlerta('Lote deve conter no m�nimo um documento ativo.');
      cxGridDoctoFiscal.SetFocus;
      Abort;
  end;

  tsqZip1.ZipOptions.RecurseSubDirs := rsdNotRecursive;
  tsqZip1.ZipOptions.ZipFlagOptions := [zfoJunkDir];
  qryTempDoctoFiscalSel.First;
  
  while (not qryTempDoctoFiscalSel.Eof) do
  begin
      if (not FileExists(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value)) then
      begin
          MensagemAlerta('O Documento ' + qryTempDoctoFiscalSelnCdDoctoFiscal.AsString + ' n�o possui arquivo XML, gere um arquivo para continuar a opera��o');
          tsqZip1.FilesToZip.Clear;
          abort;
      end;

      tsqZip1.FilesToZip.Add(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value);

      qryTempDoctoFiscalSel.Next;
  end;

  {-- valida tipo de document --}
  case rgTipoDocto.ItemIndex of
      0: cTipoDocto := 'NFe';
      1: cTipoDocto := 'CFe';
  end;

  {-- Local onde sera salvo o arquivo .zip --}
  SaveDialog1.Title      := 'Selecione o local onde ser� salvo o arquivo.';
  SaveDialog1.Filter     := 'Arquivos .Zip|*.zip';
  SaveDialog1.DefaultExt := 'zip';
  SaveDialog1.InitialDir := frmMenu.cPathSistema + 'EMP' + frmMenu.ZeroEsquerda(qryEmpresanCdEmpresa.AsString,3) + '\Producao\' + cTipoDocto + '\Lote\';
  SaveDialog1.FileName   := 'LoteXML_' + StringReplace(mskMesAno.Text,'/','',[rfReplaceAll,rfIgnoreCase]) + '.zip';

  if (SaveDialog1.Execute) then
      cCaminhoZip := SaveDialog1.FileName;
  
  {-- Zipa os documentos --}
  try
      tsqZip1.ZipFileName := cCaminhoZip;
      tsqZip1.Execute;
  finally
      ShowMessage('Os Documentos fiscais foram compactados com sucesso.');
      tsqZip1.FilesToZip.Clear;
  end;
end;

procedure TfrmEnviaLoteXMLNFe.cxGridDoctoFiscalDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if(not FileExists(ARecord.Values[13] + ARecord.Values[14]) ) then
  begin
      AStyle := NotFound;
  end;
end;

procedure TfrmEnviaLoteXMLNFe.FormCreate(Sender: TObject);
begin
  inherited;
  //cxGridDoctoFiscalDBTableView1.OptionsData.Editing := False;
end;

procedure TfrmEnviaLoteXMLNFe.testeClick(Sender: TObject);
begin
  inherited;
  ShowMessage(IntToStr(cxGridDoctoFiscalDBTableView1.ColumnCount));
end;

procedure TfrmEnviaLoteXMLNFe.btGerarXmlClick(Sender: TObject);
var
  bAux : boolean;
begin
  inherited;

  bAux := False;

  if (qryTempDoctoFiscal.IsEmpty) then
      Exit;

  try
      qryTempDoctoFiscalSel.Close;
      qryTempDoctoFiscalSel.Open;
      qryTempDoctoFiscalSel.First;

      while (not qryTempDoctoFiscalSel.Eof) do
      begin
          if (not FileExists(frmMenu.cPathSistema + qryTempDoctoFiscalSelcCaminhoXML.Value + qryTempDoctoFiscalSelcArquivoXML.Value)) then
          begin
              prGeraXML(false);
              bAux := True;
          end;

          qryTempDoctoFiscalSel.Next;
      end;
  finally
      qryTempDoctoFiscal.Requery();

      if(bAux) then
          ShowMessage('Arquivos gerados com succeso.')
      else
          ShowMessage('Todos os documentos j� possuem arquivo XML.');
  end;
end;

initialization
    RegisterClass(TfrmEnviaLoteXMLNFe);

end.
