inherited frmConsignacao_Leitor: TfrmConsignacao_Leitor
  Left = 189
  Top = 161
  Width = 856
  BorderIcons = [biMaximize]
  Caption = 'Item Consigna'#231#227'o - Leitor'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 24
    Width = 840
    Height = 438
  end
  inherited ToolBar1: TToolBar
    Width = 840
    Height = 24
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 24
    Width = 840
    Height = 438
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTempItemConsig
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyDown = DBGridEh1KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cEAN'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 424
      end
      item
        EditButtons = <>
        FieldName = 'cReferencia'
        Footers = <>
        ReadOnly = True
        Width = 98
      end
      item
        EditButtons = <>
        FieldName = 'nValVenda'
        Footers = <>
        ReadOnly = True
        Width = 89
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 32
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,cEAN'
      '      ,cReferencia'
      '      ,nValVenda'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 808
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutocEAN: TStringField
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryProdutocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryProdutonValVenda: TBCDField
      FieldName = 'nValVenda'
      Precision = 14
      Size = 6
    end
  end
  object dsTempItemConsig: TDataSource
    DataSet = qryTempItemConsig
    Left = 776
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempItemConsig'#39') IS NULL)'
      'BEGIN'
      
        '    CREATE TABLE #TempItemConsig(nCdProduto  int identity(1,1) P' +
        'RIMARY KEY'
      '                                ,cNmProduto  varchar(150)'
      '                                ,cEAN        char(20)'
      '                                ,cReferencia varchar(15)'
      '                                ,nValVenda   decimal(14,6))'
      'END'
      ''
      'TRUNCATE TABLE #TempItemConsig'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempItens'#39') IS NULL)'
      'BEGIN'
      '    CREATE TABLE #TempItens(nCdProduto    int'
      '                           ,nQtdeRetirada int)'
      'END'
      ''
      'TRUNCATE TABLE #TempItens')
    Left = 712
  end
  object cmdTempItemConsig: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempItemConsig'#39') IS NULL)'#13#10'BEGIN'#13#10'    CR' +
      'EATE TABLE #TempItemConsig(nCdTempItemConsig int identity(1,1) P' +
      'RIMARY KEY'#13#10'                               ,nCdProduto     int'#13#10 +
      '                                ,cEAN              char(20)'#13#10'   ' +
      '                             ,cNmProduto     varchar(150)'#13#10'     ' +
      '                           ,cReferencia    varchar(15)'#13#10'        ' +
      '                        ,nValVenda      decimal(14,6))'#13#10'END'#13#10#13#10'I' +
      'F (OBJECT_ID('#39'tempdb..#TempItens'#39') IS NULL)'#13#10'BEGIN'#13#10'    CREATE T' +
      'ABLE #TempItens(nCdProduto    int'#13#10'                             ' +
      '   ,nQtdeRetirada int)'#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 680
  end
  object qryTempItemConsig: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempItemConsigBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTempItemConsig'
      '      ,nCdProduto'
      '      ,cNmProduto'
      '      ,cEAN '
      '      ,cReferencia'
      '      ,nValVenda'
      '  FROM #TempItemConsig')
    Left = 744
    object qryTempItemConsignCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd.'
      FieldName = 'nCdProduto'
    end
    object qryTempItemConsigcNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryTempItemConsigcEAN: TStringField
      DisplayLabel = 'Produto|C'#243'd. Barras'
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryTempItemConsigcReferencia: TStringField
      DisplayLabel = 'Produto|Refer'#234'ncia'
      FieldName = 'cReferencia'
      Size = 15
    end
    object qryTempItemConsignValVenda: TBCDField
      DisplayLabel = 'Produto|Val.Unit.'
      FieldName = 'nValVenda'
      Precision = 14
      Size = 6
    end
    object qryTempItemConsignCdTempItemConsig: TAutoIncField
      FieldName = 'nCdTempItemConsig'
      ReadOnly = True
    end
  end
  object SP_ITENS_LEITOR_CONSIG: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ITENS_LEITOR_CONSIG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdConsignacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 648
  end
  object SP_DEVOLUCAO_ITEM_CONSIG: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_DEVOLUCAO_ITEM_CONSIG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdConsignacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
  end
  object qryTempItens: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,nQtdeRetirada'
      '  FROM #TempItens'
      ' WHERE nCdProduto = :nPK'
      ''
      ''
      '')
    Left = 584
    object qryTempItensnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryTempItensnQtdeRetirada: TIntegerField
      FieldName = 'nQtdeRetirada'
    end
  end
  object qryValidaQtde: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Sum(nQtdeRetirada) AS nQtdeRetirada'
      '  FROM #TempItens'
      ' WHERE nCdProduto = :nPK')
    Left = 520
    object qryValidaQtdenQtdeRetirada: TIntegerField
      FieldName = 'nQtdeRetirada'
      ReadOnly = True
    end
  end
  object qryQtdeProdutos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Count(1) AS nQtdeDevolvida'
      '  FROM #TempItemConsig'
      ' WHERE nCdProduto = :nPK')
    Left = 552
    object qryQtdeProdutosnQtdeDevolvida: TIntegerField
      FieldName = 'nQtdeDevolvida'
      ReadOnly = True
    end
  end
  object SP_BUSCAPRODUTO_ITEMPEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_BUSCAPRODUTO_ITEMPEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@cEAN'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = '0'
      end>
    Left = 528
    Top = 312
    object SP_BUSCAPRODUTO_ITEMPEDIDOnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object SP_BUSCAPRODUTO_ITEMPEDIDOcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
end
