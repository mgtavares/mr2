inherited rptRelPagtoCentroCusto: TrptRelPagtoCentroCusto
  Left = 12
  Top = 167
  Caption = 'Pagamentos por Centro de Custo'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 26
    Top = 198
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Pagamento'
  end
  object Label6: TLabel [2]
    Left = 203
    Top = 198
    Width = 16
    Height = 13
    Alignment = taRightJustify
    Caption = 'at'#233
  end
  object Label5: TLabel [3]
    Left = 80
    Top = 173
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label1: TLabel [4]
    Left = 5
    Top = 148
    Width = 114
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo N'#237'vel 3'
  end
  object Label2: TLabel [5]
    Left = 20
    Top = 98
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo N'#237'vel 1'
  end
  object Label4: TLabel [6]
    Left = 20
    Top = 123
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo N'#237'vel 2'
  end
  object Label7: TLabel [7]
    Left = 78
    Top = 46
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label8: TLabel [8]
    Left = 99
    Top = 72
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 8
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [10]
    Left = 121
    Top = 190
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [11]
    Left = 224
    Top = 190
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit6: TMaskEdit [12]
    Left = 121
    Top = 165
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
    OnKeyPress = MaskEdit6KeyPress
  end
  object DBEdit9: TDBEdit [13]
    Tag = 1
    Left = 191
    Top = 165
    Width = 129
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [14]
    Tag = 1
    Left = 324
    Top = 165
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 10
  end
  object MaskEdit3: TMaskEdit [15]
    Left = 121
    Top = 140
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit2: TDBEdit [16]
    Tag = 1
    Left = 191
    Top = 140
    Width = 97
    Height = 21
    DataField = 'cCdCC'
    DataSource = dsCentroCustoNivel3
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [17]
    Tag = 1
    Left = 293
    Top = 140
    Width = 649
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel3
    TabOrder = 12
  end
  object DBEdit11: TDBEdit [18]
    Tag = 1
    Left = 191
    Top = 90
    Width = 321
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel1
    TabOrder = 13
  end
  object DBEdit12: TDBEdit [19]
    Tag = 1
    Left = 261
    Top = 115
    Width = 321
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel2
    TabOrder = 14
  end
  object MaskEdit4: TMaskEdit [20]
    Left = 121
    Top = 38
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit13: TDBEdit [21]
    Tag = 1
    Left = 191
    Top = 38
    Width = 65
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 15
  end
  object DBEdit14: TDBEdit [22]
    Tag = 1
    Left = 260
    Top = 38
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 16
  end
  object MaskEdit5: TMaskEdit [23]
    Left = 121
    Top = 90
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnEnter = MaskEdit5Enter
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object MaskEdit7: TMaskEdit [24]
    Left = 121
    Top = 115
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit4: TDBEdit [25]
    Tag = 1
    Left = 191
    Top = 115
    Width = 65
    Height = 21
    DataField = 'cCdCC'
    DataSource = dsCentroCustoNivel2
    TabOrder = 17
  end
  object MaskEdit8: TMaskEdit [26]
    Left = 121
    Top = 64
    Width = 65
    Height = 21
    TabOrder = 1
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit1: TDBEdit [27]
    Tag = 1
    Left = 191
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 18
  end
  object RadioGroup1: TRadioGroup [28]
    Left = 120
    Top = 219
    Width = 197
    Height = 46
    Caption = 'Modo de Exibi'#231#227'o'
    TabOrder = 19
  end
  object RadioButton1: TRadioButton [29]
    Left = 132
    Top = 240
    Width = 113
    Height = 17
    Caption = 'Anal'#237'tico'
    TabOrder = 20
    OnClick = RadioButton1Click
  end
  object RadioButton2: TRadioButton [30]
    Left = 205
    Top = 240
    Width = 72
    Height = 17
    Caption = 'Sint'#233'tico'
    TabOrder = 21
    OnClick = RadioButton2Click
  end
  object GroupBox1: TGroupBox [31]
    Left = 120
    Top = 269
    Width = 229
    Height = 92
    Caption = 'Exibir Centro de Custo'
    TabOrder = 22
    object CheckBox1: TCheckBox
      Left = 16
      Top = 24
      Width = 161
      Height = 17
      Caption = 'Exibir Centro Custo N'#237'vel 2'
      TabOrder = 0
      OnClick = CheckBox1Click
    end
    object CheckBox2: TCheckBox
      Left = 16
      Top = 45
      Width = 153
      Height = 17
      Caption = 'Exibir Centro Custo N'#237'vel 3'
      TabOrder = 1
      OnClick = CheckBox2Click
    end
    object CheckBox3: TCheckBox
      Left = 16
      Top = 67
      Width = 195
      Height = 17
      Caption = 'Exibir Centro Custo sem Movimento?'
      TabOrder = 2
    end
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 296
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 792
    Top = 256
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryCentroCustoNivel3: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCC1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCC2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 3 '
      'AND nCdCC1 = :nCdCC1'
      'AND nCdCC2 = :nCdCC2'
      'AND nCdCC = :nPK'
      ''
      '')
    Left = 792
    Top = 288
    object qryCentroCustoNivel3nCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustoNivel3cCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustoNivel3cNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCentroCustoNivel3: TDataSource
    DataSet = qryCentroCustoNivel3
    Left = 824
    Top = 288
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nPK')
    Left = 792
    Top = 320
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryCentroCustoNivel1: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 1'
      'AND nCdCC = :nPK')
    Left = 792
    Top = 352
    object IntegerField1: TIntegerField
      FieldName = 'nCdCC'
    end
    object StringField1: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object StringField2: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryCentroCustoNivel2: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCC1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 2  AND nCdCC1 = :nCdCC1'
      'AND nCdCC = :nPK')
    Left = 792
    Top = 384
    object IntegerField2: TIntegerField
      FieldName = 'nCdCC'
    end
    object StringField3: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object StringField4: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 824
    Top = 256
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 824
    Top = 320
  end
  object dsCentroCustoNivel1: TDataSource
    DataSet = qryCentroCustoNivel1
    Left = 824
    Top = 352
  end
  object dsCentroCustoNivel2: TDataSource
    DataSet = qryCentroCustoNivel2
    Left = 824
    Top = 384
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select * from UnidadeNegocio where nCdUnidadeNegocio = :nPk')
    Left = 792
    Top = 416
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object dsUnidadeNegocio: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 824
    Top = 416
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 795
    Top = 450
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 826
    Top = 449
  end
end
