unit fAutoriza_Requisicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh, DBCtrls, StdCtrls,
  Mask, ER2Lookup, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmAutoriza_Requisicao = class(TfrmProcesso_Padrao)
    qryRequisicao: TADOQuery;
    qryRequisicaonCdRequisicao: TIntegerField;
    qryRequisicaocNmTipoRequisicao: TStringField;
    qryRequisicaodDtRequisicao: TDateTimeField;
    qryRequisicaocNmSetor: TStringField;
    qryRequisicaocNmSolicitante: TStringField;
    dsRequisicao: TDataSource;
    qryAux: TADOQuery;
    qryRequisicaonValrequisicao: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryRequisicaonCdTabTipoRequis: TIntegerField;
    SP_EMPENHA_REQUISICAO_CD: TADOStoredProc;
    Requisicao: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdRequisicao: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoRequisicao: TcxGridDBColumn;
    cxGridDBTableView1dDtRequisicao: TcxGridDBColumn;
    cxGridDBTableView1cNmSetor: TcxGridDBColumn;
    cxGridDBTableView1cNmSolicitante: TcxGridDBColumn;
    cxGridDBTableView1nValrequisicao: TcxGridDBColumn;
    cxGridDBTableView1nCdTabTipoRequis: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    qryRequisicaocNmLoja: TStringField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAutoriza_Requisicao: TfrmAutoriza_Requisicao;

implementation

uses fMenu, fRequisicao;

{$R *.dfm}


procedure TfrmAutoriza_Requisicao.FormShow(Sender: TObject);
begin
  inherited;

  qryRequisicao.Close ;
  qryRequisicao.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryRequisicao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryRequisicao.Open ;


end;

procedure TfrmAutoriza_Requisicao.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if (qryRequisicao.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhuma requisi��o ativa.') ;
      exit ;
  end ;

  { -- verifica status da requisi��o -- }
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT 1                                                          ');
  qryAux.SQL.Add('  FROM Requisicao                                                 ');
  qryAux.SQL.Add(' WHERE nCdRequisicao      = ' + qryRequisicaonCdRequisicao.AsString);
  qryAux.SQL.Add('   AND nCdTabStatusRequis = 1                                     ');
  qryAux.Open;

  if (qryAux.IsEmpty) then
  begin
      MensagemAlerta('Requisi��o No ' + qryRequisicaonCdRequisicao.AsString + ' j� processada anteriormente.');
      ToolButton1.Click;
      Exit;
  end;

  case MessageDlg('Confirma a autoriza��o desta requisi��o ?' + #13#13 + 'Ap�s este processo a requisi��o n�o poder� ser cancelada.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE Requisicao SET nCdTabStatusRequis = 2 , dDtAutor = GetDate() , nCdUsuarioAutor = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' WHERE nCdRequisicao = ' + qryRequisicaonCdRequisicao.AsString) ;
      qryAux.ExecSQL;

      if (qryRequisicaonCdTabTipoRequis.Value = 3) then
      begin
          SP_EMPENHA_REQUISICAO_CD.Close;
          SP_EMPENHA_REQUISICAO_CD.Parameters.ParamByName('@nCdRequisicao').Value := qryRequisicaonCdRequisicao.Value;
          SP_EMPENHA_REQUISICAO_CD.ExecProc;
      end;

      { -- gera alerta de requisi��o -- }
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 3; //Requisi��o de Compra
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'AUTOR. REQUISI��O No ' + qryRequisicaonCdRequisicao.AsString;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'Autoriza��o de Requisi��o No ' + qryRequisicaonCdRequisicao.AsString + ' R$ ' + FloatToStrF(qryRequisicaonValRequisicao.Value,ffNumber,12,2) + ' Solicitado por: ' + qryRequisicaocNmSolicitante.Value + ' - Tipo Req.: ' + qryRequisicaocNmTipoRequisicao.Value + ' / Setor: ' + qryRequisicaocNmSetor.Value + ' / Autorizado por ' + frmMenu.cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + '.';
      frmMenu.SP_GERA_ALERTA.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      Raise;
      Exit;
  end ;

  ShowMessage('Requisi��o autorizada com sucesso.');

  frmMenu.Connection.CommitTrans;

  ToolButton1.Click;
end;

procedure TfrmAutoriza_Requisicao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryRequisicao.Close;
  qryRequisicao.Open;
end;

procedure TfrmAutoriza_Requisicao.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmRequisicao;
begin
  inherited;

  objForm := TfrmRequisicao.Create(nil);

  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  objForm.PosicionaQuery(objForm.qryMaster, qryRequisicaonCdRequisicao.asString);

  objForm.btSalvar.Enabled    := false ;
  objForm.btExcluir.Enabled   := false ;
  objForm.btIncluir.Enabled   := false ;
  objForm.btConsultar.Enabled := false ;
  objForm.ToolButton1.Enabled := false ;
  showForm(objForm,true) ;
end;

initialization
    RegisterClass(TfrmAutoriza_Requisicao) ;
    
end.
