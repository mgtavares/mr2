unit fMetaApontamentoHora_Vendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxPC, cxControls, DBGridEhGrouping;

type
  TfrmMetaApontamentoHora_Vendedor = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryApontamentos: TADOQuery;
    dsApontamentos: TDataSource;
    qryApontamentosnCdUsuario: TIntegerField;
    qryApontamentosiTotalHorasReal: TBCDField;
    qryApontamentosiOportunidades: TIntegerField;
    qryApontamentoscNmVendedor: TStringField;
    qryVendedor: TADOQuery;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    qryInsereApontamento: TADOQuery;
    qryApontamentosdDtRef: TDateTimeField;
    qryApontamentosnCdMetaVendedor: TIntegerField;
    procedure qryApontamentosCalcFields(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure qryApontamentosBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    cFlgPrevisaoMeta : integer;
  end;

var
  frmMetaApontamentoHora_Vendedor: TfrmMetaApontamentoHora_Vendedor;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmMetaApontamentoHora_Vendedor.qryApontamentosCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryApontamentoscNmVendedor.Value = '') then
  begin
      qryVendedor.Close;
      PosicionaQuery(qryVendedor,qryApontamentosnCdUsuario.AsString);
      qryApontamentoscNmVendedor.Value := qryVendedorcNmUsuario.Value;
  end ;

end;

procedure TfrmMetaApontamentoHora_Vendedor.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  {inherited;}

end;

procedure TfrmMetaApontamentoHora_Vendedor.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmMetaApontamentoHora_Vendedor.ToolButton1Click(
  Sender: TObject);
begin
  inherited;
  
  Close;

end;

procedure TfrmMetaApontamentoHora_Vendedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  {-- registra na MetaVendedorDia os valores digitados --}

  frmMenu.Connection.BeginTrans;

  try
      qryApontamentos.First;

      while not qryApontamentos.Eof do
      begin
          qryInsereApontamento.Close;
          qryInsereApontamento.Parameters.ParamByName('nCdMetaVendedor').Value := qryApontamentosnCdMetaVendedor.Value;
          qryInsereApontamento.Parameters.ParamByName('dDtRef').Value          := qryApontamentosdDtRef.AsString;
          qryInsereApontamento.Parameters.ParamByName('iTotalHorasReal').Value := qryApontamentosiTotalHorasReal.Value;
          qryInsereApontamento.Parameters.ParamByName('iOportunidades').Value  := qryApontamentosiOportunidades.Value;
          qryInsereApontamento.ExecSQL;

          qryApontamentos.Next;
      end;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  qryApontamentos.Close;
end;

procedure TfrmMetaApontamentoHora_Vendedor.FormShow(Sender: TObject);
begin
  inherited;

  if (cFlgPrevisaoMeta = 1) then
  begin
      dbgrideh1.Columns[3].ReadOnly               := True;
      dbgrideh1.Columns.Items[3].Title.Font.Color := clRed;
      dbgrideh1.Columns.Items[3].Font.Color       := clTeal;
  end
  else
  begin
      dbgrideh1.Columns[3].ReadOnly               := false;
      dbgrideh1.Columns.Items[3].Title.Font.Color := clBlack;
      dbgrideh1.Columns.Items[3].Font.Color       := clBlack;
  end;
  
end;

procedure TfrmMetaApontamentoHora_Vendedor.qryApontamentosBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryApontamentosiTotalHorasReal.Value < 0) then
  begin
      MensagemAlerta('A quantidade de horas realizadas n�o pode ser negativa.') ;
      abort ;
  end ;

  if (qryApontamentosiOportunidades.Value < 0) then
  begin
      MensagemAlerta('A quantidade de oportunidades n�o pode ser negativa.') ;
      abort ;
  end ;

end;

end.
