unit fAutoriza_devolucao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmAutoriza_devolucao = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdPedido: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMastercNmTerceiro: TStringField;
    qryMasternValPedido: TBCDField;
    qryMasternSaldoVencido: TBCDField;
    qryMasternSaldoaVencer: TBCDField;
    DBGridEh1: TDBGridEh;
    dsMaster: TDataSource;
    SP_AUTORIZA_DEVOLUCAO_VENDA: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAutoriza_devolucao: TfrmAutoriza_devolucao;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAutoriza_devolucao.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMaster, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  
end;

procedure TfrmAutoriza_devolucao.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
end;

procedure TfrmAutoriza_devolucao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.Eof) or (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhuma devolu��o ativa.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a autoriza��o desta devolu��o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_AUTORIZA_DEVOLUCAO_VENDA.Close ;
      SP_AUTORIZA_DEVOLUCAO_VENDA.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
      SP_AUTORIZA_DEVOLUCAO_VENDA.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_AUTORIZA_DEVOLUCAO_VENDA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Devolu��o autorizada com sucesso.') ;

  qryMaster.Close ;
  qryMaster.Open ;

end;

initialization
    RegisterClass(TfrmAutoriza_devolucao) ;
    
end.
