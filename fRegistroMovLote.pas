unit fRegistroMovLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxPC, cxControls, DBGridEhGrouping,
  GridsEh, DBGridEh;

type
  TfrmRegistroMovLote = class(TfrmProcesso_Padrao)
    qryMovLoteTemporario: TADOQuery;
    qryTabTipoOrigemMovLote: TADOQuery;
    qryTabTipoOrigemMovLotenCdTabTipoOrigemMovLote: TIntegerField;
    qryTabTipoOrigemMovLotecNmTabTipoOrigemMovLote: TStringField;
    qryTabTipoOrigemMovLotecFlgEntradaSaida: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdTabTipoModoGestaoProduto: TIntegerField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DataSource2: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsTempLote: TDataSource;
    qryProdutocFlgControlaValidadeLote: TIntegerField;
    qryProdutoiDiasDisponibilidadeLote: TIntegerField;
    qryMovLoteTemporarionCdMovLoteTemporario: TAutoIncField;
    qryMovLoteTemporarionCdProduto: TIntegerField;
    qryMovLoteTemporariocNrLote: TStringField;
    qryMovLoteTemporariodDtFabricacao: TDateTimeField;
    qryMovLoteTemporariodDtValidade: TDateTimeField;
    qryMovLoteTemporariodDtDisponibilidade: TDateTimeField;
    qryMovLoteTemporarionQtdeMov: TBCDField;
    qryMovLoteTemporarionCdTabTipoOrigemMovLote: TIntegerField;
    qryMovLoteTemporarioiIDRegistro: TIntegerField;
    qryMovLoteTemporariocOBS: TStringField;
    qrySomaQtdeMov: TADOQuery;
    qryLocalizaLoteEstoque: TADOQuery;
    qryLocalizaLoteEstoquenCdLoteProduto: TAutoIncField;
    qryLocalizaLoteEstoquedDtFabricacao: TDateTimeField;
    qryLocalizaLoteEstoquedDtValidade: TDateTimeField;
    qryLocalizaLoteEstoquedDtDisponibilidade: TDateTimeField;
    qryLocalizaLoteEstoquenSaldoLote: TBCDField;
    qryMovLoteTemporariocFlgCriarLote: TIntegerField;
    qryLocalizaLote: TADOQuery;
    qryLocalizaLotenCdLoteProduto: TAutoIncField;
    qryLocalizaLotedDtFabricacao: TDateTimeField;
    qryLocalizaLotedDtValidade: TDateTimeField;
    qryLocalizaLotedDtDisponibilidade: TDateTimeField;
    qryLocalizaLotenSaldoLote: TBCDField;
    qrySomaQtdeMovnQtdeMov: TBCDField;
    qryMovLoteTemporariocFlgProcessado: TIntegerField;
    qryMovLoteTemporarionCdTerceiro: TIntegerField;
    qryListaSeriaisSaida: TADOQuery;
    qryListaSeriaisSaidacNrLote: TStringField;
    qryListaSeriaisSaidanCdProduto: TIntegerField;
    qryListaSeriaisSaidanQtde: TBCDField;
    qryMovLoteTemporarionCdLocalEstoque: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure qryMovLoteTemporarioBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryMovLoteTemporariocNrLoteChange(Sender: TField);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
    nCdProduto              : integer;
    nCdTabTipoOrigemMovLote : integer;
    iIDRegistro             : integer;
    nCdLocalEstoque         : integer;
    nQtdeTotal              : double;
    cOBS                    : string ;
    bReadOnly               : boolean;
    bCriarLote              : boolean;
    nCdTerceiro             : integer;
    bValidaLoteSerial       : boolean;

    dDtValidadeOld          : TDateTime ;
    dDtFabricacaoOld        : TDateTime ;
    dDtDisponibilidadeOld   : TDateTime ;

    procedure preparaDataSet;

  public
    { Public declarations }
    procedure registraMovLote(nCdProdutoAux, nCdTerceiroAux, nCdTabTipoOrigemMovLoteAux, iIDRegistroAux, nCdLocalEstoqueAux : integer ; nQtdeTotalAux : double ; cOBSAux : string ; bCriarLoteAux,bReadOnlyAux : boolean; bValidaLoteSerialAux : boolean) ;
    function retornaQuantidadeRegistradaTemporaria( nCdProdutoAux, nCdTabTipoOrigemMovLoteAux, iIDRegistroAux : integer) : double ;
    function validaSerialLote(iIDRegistroAux, nCdTabTipoOrigemMovLoteAux : integer; cDescricaoProduto : String) : boolean;
  end;

var
  frmRegistroMovLote: TfrmRegistroMovLote;

implementation

uses fMenu, fConsultaLoteSerial;

{$R *.dfm}

{ TfrmRegistroMovLote }

procedure TfrmRegistroMovLote.registraMovLote(nCdProdutoAux, nCdTerceiroAux,
  nCdTabTipoOrigemMovLoteAux, iIDRegistroAux, nCdLocalEstoqueAux: integer; nQtdeTotalAux : double; cOBSAux : string ;
  bCriarLoteAux, bReadOnlyAux: boolean; bValidaLoteSerialAux : boolean);
begin

    nCdProduto              := nCdProdutoAux;
    nCdTabTipoOrigemMovLote := nCdTabTipoOrigemMovLoteAux;
    iIDRegistro             := iIDRegistroAux;
    nCdLocalEstoque         := nCdLocalEstoqueAux;
    nQtdeTotal              := nQtdeTotalAux;
    cOBS                    := cOBSAux;
    bReadOnly               := bReadOnlyAux;
    bCriarLote              := bCriarLoteAux;
    nCdTerceiro             := nCdTerceiroAux;
    bValidaLoteSerial       := bValidaLoteSerialAux;

    preparaDataSet ;

    Self.ShowModal ;

end;

procedure TfrmRegistroMovLote.FormShow(Sender: TObject);
begin
  inherited;

  dDtValidadeOld          := 0 ;
  dDtFabricacaoOld        := 0 ;
  dDtDisponibilidadeOld   := 0 ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRegistroMovLote.qryMovLoteTemporarioBeforePost(DataSet: TDataSet);
begin

  if (qryMovLoteTemporarionQtdeMov.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  {-- quando a tela for utilizada para valida��o do numero de serie ou do lote
  -- n�o faz as consistencias--}
  if (not bValidaLoteSerial) then
  begin

      {-- localiza para ver se o lote do produto existe --}
      qryLocalizaLoteEstoque.Close;
      qryLocalizaLoteEstoque.Parameters.ParamByName('nCdProduto').Value      := nCdProduto ;
      qryLocalizaLoteEstoque.Parameters.ParamByName('nCdLocalEstoque').Value := nCdLocalEstoque ;
      qryLocalizaLoteEstoque.Parameters.ParamByName('cNrLote').Value         := Uppercase( qryMovLoteTemporariocNrLote.Value );
      qryLocalizaLoteEstoque.Open;

  end;

  {-- quando � movimenta��o do tipo serial o usu�rio n�o tem op��o de digitar a quantidade, pois � um serial para cada pe�a --}
  if (qryMovLoteTemporarionQtdeMov.Value = 0) then
      qryMovLoteTemporarionQtdeMov.Value := 1;

  {-- se n�o permite criar lote, ent�o tenta localizar um lote para este produto pois o usu�rio n�o poder� --}
  {-- movimentar um lote que n�o existe                                                                    --}
  if ((not bCriarLote) and (not bValidaLoteSerial)) then
  begin

      if qryLocalizaLoteEstoque.eof then
      begin
          MensagemAlerta('Lote/Serial n�o encontrado. Produto: ' + intToStr( nCdProduto ) + ' Local Estoque: ' + intToStr( nCdLocalEstoque ) ) ;
          abort ;
      end ;

      if (qryLocalizaLoteEstoquenSaldoLote.Value < qryMovLoteTemporarionQtdeMov.Value) then
      begin

          MensagemAlerta('Saldo do lote insuficiente para atender esta transa��o. Saldo do Lote: ' + floatToStr( qryLocalizaLoteEstoquenSaldoLote.Value ) ) ;

          if (frmMenu.LeParametro('PERMLOTENEG') = 'N') then
              abort ;

      end ;

      qryMovLoteTemporariodDtFabricacao.Value      := qryLocalizaLoteEstoquedDtFabricacao.Value ;
      qryMovLoteTemporariodDtDisponibilidade.Value := qryLocalizaLoteEstoquedDtDisponibilidade.Value ;
      qryMovLoteTemporariodDtValidade.Value        := qryLocalizaLoteEstoquedDtValidade.Value ;

  end ;

  if (not bValidaLoteSerial) then
  begin
      if (qryMovLoteTemporariodDtFabricacao.asString = '') then
      begin
          MensagemAlerta('Informe a data de fabrica��o.') ;
          abort ;
      end ;

      if (qryMovLoteTemporariodDtFabricacao.Value > Date) then
      begin
          MensagemAlerta('A data de fabrica��o n�o pode ser maior que hoje.') ;
          abort ;
      end ;

      {-- se o produto for controlado por lote --}
      if (qryProdutonCdTabTipoModoGestaoProduto.Value = 3) then
      begin

          {-- se o produto controlar data de validade --}
          if (qryProdutocFlgControlaValidadeLote.Value = 1) then
          begin

              if (qryMovLoteTemporariodDtValidade.asString = '') then
              begin
                  MensagemAlerta('Informe a data de validade.') ;
                  abort ;
              end ;

              if (qryMovLoteTemporariodDtValidade.Value < qryMovLoteTemporariodDtFabricacao.Value) and (qryProdutocFlgControlaValidadeLote.Value = 1) then
              begin
                  MensagemAlerta('A data de validade n�o pode ser menor que a data de fabrica��o.') ;
                  abort ;
              end ;

          end
          else qryMovLoteTemporariodDtValidade.AsString := '' ;

          {-- se a data de disponibilidade do lote n�o foi informada e no cadastro do produto estiver informado --}
          {-- qual o tempo de matura��o do lote, calcula automatico a partir da data de fabrica��o              --}

          if (qryMovLoteTemporariodDtDisponibilidade.AsString = '') and (qryProdutoiDiasDisponibilidadeLote.Value > 0) then
              qryMovLoteTemporariodDtDisponibilidade.Value := qryMovLoteTemporariodDtFabricacao.Value + qryProdutoiDiasDisponibilidadeLote.Value ;

          if (qryMovLoteTemporariodDtDisponibilidade.AsString = '') then
              qryMovLoteTemporariodDtDisponibilidade.Value := qryMovLoteTemporariodDtFabricacao.Value ;

          if (qryMovLoteTemporariodDtValidade.asString <> '') then
              if (qryMovLoteTemporariodDtDisponibilidade.Value > qryMovLoteTemporariodDtValidade.Value) then
              begin
                  MensagemAlerta('A data de disponibilidade n�o pode ser maior que a data de validade.') ;
                  abort ;
              end ;

      end ;

      {-- se o produto for controlado por n�mero de s�rie --}
      if (qryProdutonCdTabTipoModoGestaoProduto.Value = 2) then
      begin
          qryMovLoteTemporariodDtValidade.AsString     := '' ;
          qryMovLoteTemporariodDtDisponibilidade.Value := qryMovLoteTemporariodDtFabricacao.Value ;
      end ;


      {-- se for uma transa��o que est� criando um estoque de lotes, verifica se o lote/serial existe e tem saldo --}
      if (bCriarLote) then
      begin

          qryLocalizaLote.Close ;
          qryLocalizaLote.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
          qryLocalizaLote.Parameters.ParamByName('cNrLote').Value    := Uppercase( qryMovLoteTemporariocNrLote.Value );
          qryLocalizaLote.Open ;

          if not qryLocalizaLote.eof then
          begin

              {-- se o produto for controlado por n�mero de s�rie e esse numero de s�rie j� existir no estoque, n�o permite a cria��o --}
              if (qryProdutonCdTabTipoModoGestaoProduto.Value = 2) then
              begin

                  MensagemAlerta('Este n�mero de s�rie j� est� no estoque, imposs�vel realizar esta movimenta��o.') ;
                  abort ;

              end ;

              {-- se o produto for controlado por n�mero de lote, verifica se a data de fabrica��o � a mesma que j� tem no estoque --}
              if (qryProdutonCdTabTipoModoGestaoProduto.Value = 3) then
                  if (qryLocalizaLotedDtFabricacao.Value <> qryMovLoteTemporariodDtFabricacao.Value) then
                  begin
                      MensagemAlerta('Este lote j� existe no estoque e a data de fabrica��o � diferente da digitada. Verifique.') ;
                      abort ;
                  end ;

          end ;

      end ;

  end ;

  inherited;

  qryMovLoteTemporariocNrLote.Value                 := Uppercase( qryMovLoteTemporariocNrLote.Value ) ;
  qryMovLoteTemporarionCdProduto.Value              := nCdProduto;
  qryMovLoteTemporarionCdTabTipoOrigemMovLote.Value := nCdTabTipoOrigemMovLote;
  qryMovLoteTemporarioiIDRegistro.Value             := iIDRegistro;

  {-- se nao tiver local de estoque especificado insere null --}
  if (nCdLocalEstoque > 0) then
      qryMovLoteTemporarionCdLocalEstoque.Value := nCdLocalEstoque;

  qryMovLoteTemporariocOBS.Value                    := cOBS ;

  if (bValidaLoteSerial) then
  begin
      qryMovLoteTemporariodDtValidade.Value        := Now();
      qryMovLoteTemporariodDtFabricacao.Value      := Now();
      qryMovLoteTemporariodDtDisponibilidade.Value := Now();
  end;

  if (nCdTerceiro > 0) then
      qryMovLoteTemporarionCdTerceiro.Value := nCdTerceiro ;

  if (bCriarLote) then
  begin

      qryMovLoteTemporariocFlgCriarLote.Value := 1 ;

      if (qryMovLoteTemporariodDtValidade.asString <> '') then
          dDtValidadeOld        := qryMovLoteTemporariodDtValidade.Value ;

      if (qryMovLoteTemporariodDtFabricacao.asString <> '') then
          dDtFabricacaoOld      := qryMovLoteTemporariodDtFabricacao.Value ;

      if (qryMovLoteTemporariodDtDisponibilidade.asString <> '') then
          dDtDisponibilidadeOld := qryMovLoteTemporariodDtDisponibilidade.Value ;

  end ;

end;

procedure TfrmRegistroMovLote.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRegistroMovLote.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRegistroMovLote.ToolButton1Click(Sender: TObject);
var
  nQtdeInformada : double ;
begin
  inherited;

  nQtdeInformada := DBGridEh1.SumList.SumCollection.Items[2].SumValue ;

  if (nQtdeTotal <> nQtdeInformada) then
  begin

      if (MessageDlg('A quantidade especificada nos lotes/seriais n�o confere com a quantidade total movimentada. Deseja continuar ?' +#13#13+ 'Quantidade Movimentada : ' + floatToStr( nQtdeTotal ) + #13#13 + 'Quantidade Especificada : ' + FloatToStr( nQtdeInformada ),mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      begin
          DBGridEh1.SetFocus ;
          exit ;
      end ;

  end ;

  Close;
  
end;

function TfrmRegistroMovLote.retornaQuantidadeRegistradaTemporaria(
  nCdProdutoAux, nCdTabTipoOrigemMovLoteAux, iIDRegistroAux : integer): double;
begin

    qryProduto.Close;
    PosicionaQuery(qryProduto , intToStr( nCdProdutoAux ) ) ;

    if (qryProduto.eof) then
    begin
        MensagemErro('O Produto ' + intToStr( nCdProdutoAux ) + ' n�o est� cadastrado.') ;
        result := 0 ;
        exit ;
    end ;

    {--Nenhum--}
    if (qryProdutonCdTabTipoModoGestaoProduto.Value = 1) then
    begin
        Result := -1 ;
        exit ;
    end ;

    qrySomaQtdeMov.Close;
    qrySomaQtdeMov.Parameters.ParamByName('nCdTabTipoOrigemMovLote').Value := nCdTabTipoOrigemMovLoteAux;
    qrySomaQtdeMov.Parameters.ParamByName('iIDRegistro').Value             := iIDRegistroAux ;
    qrySomaQtdeMov.Open;

    if (qrySomaQtdeMov.eof) then
        Result := 0
    else Result := qrySomaQtdeMovnQtdeMov.Value ;

end;

procedure TfrmRegistroMovLote.preparaDataSet;
begin

    qryTabTipoOrigemMovLote.Close;
    PosicionaQuery(qryTabTipoOrigemMovLote , intToStr( nCdTabTipoOrigemMovLote ) ) ;

    qryProduto.Close;
    PosicionaQuery(qryProduto , intToStr( nCdProduto ) ) ;

    qryMovLoteTemporario.Close;
    qryMovLoteTemporario.Parameters.ParamByName('nCdTabTipoOrigemMovLote').Value := nCdTabTipoOrigemMovLote;
    qryMovLoteTemporario.Parameters.ParamByName('iIDRegistro').Value             := iIDRegistro;
    qryMovLoteTemporario.Open;

    if (qryProduto.eof) then
    begin
        MensagemErro('O Produto ' + intToStr( nCdProduto ) + ' n�o est� cadastrado.') ;
        close ;
        abort ;
    end ;

    {--Nenhum--}
    if (qryProdutonCdTabTipoModoGestaoProduto.Value = 1) then
    begin
        MensagemErro('O Produto ' + intToStr( nCdProduto ) + ' n�o � gerenciado por lote/serial.') ;
        close ;
        abort ;
    end ;

    {-- s� exibe as colunas de data de validade e data de disponiblidade qdo for lote --}
    DBGridEh1.Columns[5].Visible  := (qryProdutonCdTabTipoModoGestaoProduto.Value = 3) ;
    DBGridEh1.Columns[6].Visible  := (qryProdutonCdTabTipoModoGestaoProduto.Value = 3) ;
    DBGridEh1.Columns[7].ReadOnly := (qryProdutonCdTabTipoModoGestaoProduto.Value = 2) ; {-- se for serial, desativa a quantidade --}

    DBGridEh1.Columns[4].Width    := DBGridEh1.Columns[4].Width + 30 ;
    DBGridEh1.Columns[5].Width    := DBGridEh1.Columns[5].Width + 30 ;
    DBGridEh1.Columns[6].Width    := DBGridEh1.Columns[6].Width + 30 ;

    {-- se n�o permitir criar lote, desativa a edi��o das datas --}
    if (not bCriarLote) then
    begin

        DBGridEh1.Columns[4].ReadOnly := True ;
        DBGridEh1.Columns[5].ReadOnly := True ;
        DBGridEh1.Columns[6].ReadOnly := True ;

        DBGridEh1.Columns[4].Title.Font.Color := clRed ;
        DBGridEh1.Columns[5].Title.Font.Color := clRed ;
        DBGridEh1.Columns[6].Title.Font.Color := clRed ;

        DBGridEh1.Columns[4].Color := clSilver ;
        DBGridEh1.Columns[5].Color := clSilver ;
        DBGridEh1.Columns[6].Color := clSilver ;

    end ;

    if (DBGridEh1.Columns[7].ReadOnly) then
    begin
        DBGridEh1.Columns[7].Title.Font.Color := clRed    ;
        DBGridEh1.Columns[7].Color            := clSilver ;
    end
    else DBGridEh1.Columns[7].Title.Font.Color := clBlack ;

    if (qryProdutonCdTabTipoModoGestaoProduto.Value = 3) then
    begin
        ToolButton5.Caption                := 'Consultar Lotes' ;
        DBGridEh1.Columns[3].Title.Caption := 'Dados da Rastreabilidade|N�mero Lote' ;
    end
    else
    begin
        ToolButton5.Caption                := 'Consultar Seriais' ;
        DBGridEh1.Columns[3].Title.Caption := 'Dados da Rastreabilidade|N�mero de S�rie' ;
    end ;

    ToolButton1.Enabled := (not bReadOnly) ; {--se n�o estiver em somente leitura, ativa o bot�o processar --}
    ToolButton2.Enabled := bReadOnly ; {-- se estiver em somente leitura, ativa o bot�o fechar --}
    DBGridEh1.ReadOnly  := bReadOnly ; {-- se estiver em somente leitura, desativa a edi��o do grid --}

    if (bValidaLoteSerial) then
    begin
        DBGridEh1.Columns[4].Visible  := False;
        DBGridEh1.Columns[5].Visible  := False;
        DBGridEh1.Columns[6].Visible  := False;
    end;

end;

procedure TfrmRegistroMovLote.qryMovLoteTemporariocNrLoteChange(
  Sender: TField);
begin
  inherited;

  if (bCriarLote) and (qryMovLoteTemporariodDtFabricacao.asString = '') and (dDtFabricacaoOld <> 0) then
  begin

      if (dDtValidadeOld <> 0) then
          qryMovLoteTemporariodDtValidade.Value        := dDtValidadeOld        ;

      qryMovLoteTemporariodDtFabricacao.Value      := dDtFabricacaoOld      ;

      if (dDtDisponibilidadeOld <> 0) then
          qryMovLoteTemporariodDtDisponibilidade.Value := dDtDisponibilidadeOld ;

  end ;

end;

procedure TfrmRegistroMovLote.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmConsultaLoteSerial;
begin
  inherited;

  objForm := TfrmConsultaLoteSerial.Create( Self );

  objForm.qryProduto.Close;
  objForm.qryProduto.Parameters.ParamByName('nPK').Value              := nCdProduto;
  objForm.qryProduto.Open;

  objForm.qryTabTipoOrigemMovLote.Close;
  objForm.qryTabTipoOrigemMovLote.Parameters.ParamByName('nPK').Value := nCdTabTipoOrigemMovLote;
  objForm.qryTabTipoOrigemMovLote.Open;

  objForm.qryLocalEstoque.Close;
  objForm.qryLocalEstoque.Parameters.ParamByName('nPK').Value := nCdLocalEstoque;
  objForm.qryLocalEstoque.Open;

  objForm.cmdPreparaTemp.Execute;
  
  objForm.qryPopulaTemp.Close;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdProduto').Value      := nCdProduto;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdLocalEstoque').Value := nCdLocalEstoque;
  objForm.qryPopulaTemp.ExecSQL;

  objForm.qryLoteDisponivel.Close;
  objForm.qryLoteDisponivel.Open;

  objForm.qryLoteSelecionado.Close;
  objForm.qryLoteSelecionado.Open;

  objForm.bPermiteSelecionar := ((not bCriarLote) and (not bReadOnly));;
  objForm.nQtdeMov           := nQtdeTotal;

  showForm(objForm,false);

  objForm.qryLoteSelecionado.First;

  if ((objForm.qryLoteSelecionado.RecordCount <= 0) or not (objForm.bGravarSelecao)) then
      exit;

  while not (objForm.qryLoteSelecionado.Eof) do
  begin

      if not (qryMovLoteTemporario.Locate('cNrLote',objForm.qryLoteSelecionadocNrLote.Value,[])) then
      begin
          qryMovLoteTemporario.Insert;
          qryMovLoteTemporariocNrLote.Value  := objForm.qryLoteSelecionadocNrLote.Value;
          qryMovLoteTemporarionQtdeMov.Value := objForm.qryLoteSelecionadonQtdeSelecionado.Value;
          qryMovLoteTemporario.Post;
      end;

      objForm.qryLoteSelecionado.Next;
  end;

end;

function TfrmRegistroMovLote.validaSerialLote(iIDRegistroAux,
  nCdTabTipoOrigemMovLoteAux: integer; cDescricaoProduto : String): boolean;
begin
  {-- verifica se o serial/ lote � valido --}

  qryListaSeriaisSaida.Close;
  qryListaSeriaisSaida.Parameters.ParamByName('iIDRegistro').Value := iIDRegistroAux;
  qryListaSeriaisSaida.Open;

  qryMovLoteTemporario.Close;
  qryMovLoteTemporario.Parameters.ParamByName('nCdTabTipoOrigemMovLote').Value := nCdTabTipoOrigemMovLoteAux;
  qryMovLoteTemporario.Parameters.ParamByName('iIDRegistro').Value             := iIDRegistroAux;
  qryMovLoteTemporario.Open;

  qryMovLoteTemporario.First;

  {-- verifica se n�o foi digitado nenhum lote/serial ou se o numero de lotes/seriais digitados for diferente da quantidade transferida --}
  if (qryMovLoteTemporario.RecordCount <> qryListaSeriaisSaida.RecordCount) then
  begin
      MensagemAlerta('A quantidade especificada nos Lotes/Seriais n�o confere com o total transferido.'
                     + #13#13 + 'Produto: ' + qryListaSeriaisSaidanCdProduto.AsString + ' - ' + Trim(cDescricaoProduto));
      Result := False;
      exit;
  end;

  while not (qryMovLoteTemporario.Eof) do
  begin
      {-- verifica se o Lote/Serial para o produto existe na transferencia --}
      if (not qryListaSeriaisSaida.Locate('cNrLote',qryMovLoteTemporariocNrLote.Value,[])) then
      begin
          MensagemAlerta('Numero do Lote/Serial informado n�o confere com os Lotes/Serias transferidos.'
                         + #13#13 + 'Produto: ' + qryMovLoteTemporarionCdProduto.AsString + ' - ' + Trim(cDescricaoProduto));
          Result := False;
          break;
      end
      else begin
          {-- verifica a quantidade informada --}
          if (qryListaSeriaisSaidanQtde.Value <> qryMovLoteTemporarionQtdeMov.Value) then
          begin
              MensagemAlerta('Quantidade informada n�o confere com o total transferido.'
                             + #13#13 + 'Produto    : ' + qryMovLoteTemporarionCdProduto.AsString + ' - ' + Trim(cDescricaoProduto)
                             + #13#13 + 'Lote/Serial: ' + qryMovLoteTemporariocNrLote.Value);
              Result := False;
              break;
          end
          else Result := True;
      
      end;
      qryMovLoteTemporario.Next;
  end;

  
end;

end.
