unit fMotivoPerdaProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmMotivoPerdaProducao = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    qryMasternCdMotivoPerdaProducao: TIntegerField;
    qryMastercNmMotivoPerdaProducao: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMotivoPerdaProducao: TfrmMotivoPerdaProducao;

implementation

{$R *.dfm}

procedure TfrmMotivoPerdaProducao.FormCreate(Sender: TObject);
begin
  cNmTabelaMaster   := 'MOTIVOPERDAPRODUCAO' ;
  nCdTabelaSistema  := 422 ;
  nCdConsultaPadrao := 1015 ;
  inherited;

end;
procedure TfrmMotivoPerdaProducao.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do motivo de perda de produ��o.') ;
      DBedit2.SetFocus;
      abort ;
  end ;

  inherited;

end;

procedure TfrmMotivoPerdaProducao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;

end;

initialization
    RegisterClass(TfrmMotivoPerdaProducao) ;

end.
