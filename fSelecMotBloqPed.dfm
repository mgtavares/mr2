inherited frmSelecMotBloqPed: TfrmSelecMotBloqPed
  Left = 338
  Top = 206
  VertScrollBar.Range = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsSingle
  Caption = 'Selecione Motivo'
  ClientHeight = 267
  ClientWidth = 566
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 566
    Height = 238
  end
  object Label1: TLabel [1]
    Left = 16
    Top = 72
    Width = 58
    Height = 13
    Caption = 'Observa'#231#227'o'
  end
  inherited ToolBar1: TToolBar
    Width = 566
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBComboBoxEh1: TDBComboBoxEh [3]
    Left = 16
    Top = 40
    Width = 537
    Height = 19
    DataField = 'cNmMotBloqPed'
    DataSource = DataSource1
    EditButtons = <>
    Flat = True
    ReadOnly = True
    TabOrder = 1
    Visible = True
  end
  object Memo1: TMemo [4]
    Left = 16
    Top = 88
    Width = 537
    Height = 169
    TabOrder = 2
  end
  object qryMotivo: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 3
        Value = '156'
      end>
    SQL.Strings = (
      'SELECT MotBloqPed.nCdMotBloqPed'
      '      ,MotBloqPed.cNmMotBloqPed'
      '  FROM TipoPedidoMotBloqPed'
      
        '       INNER JOIN MotBloqPed ON MotBloqPed.nCdMotBloqPed = TipoP' +
        'edidoMotBloqPed.nCdMotBloqPed'
      ' WHERE nCdTipoPedido = (SELECT nCdTipoPedido'
      '                          FROM Pedido'
      '                         WHERE nCdPedido = :nPK)')
    Left = 160
    Top = 104
    object qryMotivonCdMotBloqPed: TIntegerField
      FieldName = 'nCdMotBloqPed'
    end
    object qryMotivocNmMotBloqPed: TStringField
      FieldName = 'cNmMotBloqPed'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryMotivo
    Left = 200
    Top = 160
  end
  object cmdBloquear: TADOCommand
    CommandText = 
      'UPDATE Pedido'#13#10'SET nCdTabStatusPed = 11'#13#10',dDtRejeicao = GetDate(' +
      ')'#13#10',nCdUsuarioRejeicao = :nCdUsuario'#13#10',nCdMotBloqPed = :nCdMotBl' +
      'oqPed'#13#10',cMotivoRejeicao = :cNmMotBloqPed'#13#10',nSaldoFat = 0'#13#10',cObsF' +
      'inanc = :cOBS'#13#10'WHERE nCdPedido = :nCdPedido'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdMotBloqPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNmMotBloqPed'
        Attributes = [paNullable, paLong]
        DataType = ftString
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'cOBS'
        Attributes = [paNullable, paLong]
        DataType = ftString
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 392
    Top = 16
  end
end
