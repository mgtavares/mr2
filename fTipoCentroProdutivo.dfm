inherited frmTipoOrdemProducao: TfrmTipoOrdemProducao
  Left = 260
  Top = 172
  Width = 922
  Height = 520
  Caption = 'Tipo Ordem de Produ'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 906
    Height = 459
  end
  object Label1: TLabel [1]
    Left = 96
    Top = 64
    Width = 58
    Height = 13
    Caption = 'nCdTipoOP'
    FocusControl = DBEdit1
  end
  inherited ToolBar2: TToolBar
    Width = 906
  end
  object DBEdit1: TDBEdit [3]
    Left = 96
    Top = 80
    Width = 130
    Height = 19
    DataField = 'nCdTipoOP'
    DataSource = dsMaster
    TabOrder = 1
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoOP'
      'WHERE nCdTipoOP = :nPK')
    object qryMasternCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryMastercNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      Size = 50
    end
    object qryMasternCdTipoOPRetrabalho: TIntegerField
      FieldName = 'nCdTipoOPRetrabalho'
    end
    object qryMasternCdOperacaoEstoqueCP: TIntegerField
      FieldName = 'nCdOperacaoEstoqueCP'
    end
    object qryMasternCdOperacaoEstoqueRE: TIntegerField
      FieldName = 'nCdOperacaoEstoqueRE'
    end
  end
  inherited ImageList1: TImageList
    Top = 280
  end
end
