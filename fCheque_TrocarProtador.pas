unit fCheque_TrocarProtador;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, DBCtrls, ADODB;

type
  TfrmCheque_TrocarProtador = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta_1: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    cmdAtualizaCheque: TADOCommand;
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdCheque : integer ;
  end;

var
  frmCheque_TrocarProtador: TfrmCheque_TrocarProtador;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCheque_TrocarProtador.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  MaskEdit1.Enabled := False ;
  MaskEdit6.Enabled := False ;

  MaskEdit1.Text := '' ;
  MaskEdit6.Text := '' ;

  qryContaBancaria.Close ;
  qryTerceiro.Close ;
  
  if (RadioGroup1.ItemIndex = 0) then
  begin
      MaskEdit1.Enabled := True ;
      MaskEdit1.SetFocus;
  end ;

  if (RadioGroup1.ItemIndex = 1) then
  begin
      MaskEdit6.Enabled := True ;
      MaskEdit6.SetFocus;
  end ;

end;

procedure TfrmCheque_TrocarProtador.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryTerceiro.Close ;

  MaskEdit1.Text := '' ;
  MaskEdit6.Text := '' ;
    
  RadioGroup1.ItemIndex := 0 ;
  MaskEdit1.Enabled := True ;
  MaskEdit6.Enabled := False ;
  MaskEdit1.SetFocus ;

end;

procedure TfrmCheque_TrocarProtador.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (RadioGroup1.ItemIndex = 0) and (qryContaBancaria.eof) then
  begin
      MensagemAlerta('Selecione a nova conta portadora.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (RadioGroup1.ItemIndex = 1) and (qryTerceiro.eof) then
  begin
      MensagemAlerta('Selecione o novo terceiro portador.') ;
      MaskEdit6.SetFocus ;
      exit ;
  end ;

  case MessageDlg('Confirma a troca do portador ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      cmdAtualizaCheque.Parameters.ParamByName('nCdCheque').Value           := nCdCheque ;
      cmdAtualizaCheque.Parameters.ParamByName('nCdContaBancariaDep').Value := frmMenu.ConvInteiro(MaskEdit1.Text) ;
      cmdAtualizaCheque.Parameters.ParamByName('nCdTerceiroPort').Value     := frmMenu.ConvInteiro(MaskEdit6.Text) ;
      cmdAtualizaCheque.Execute;
  except
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  close ;

end;

procedure TfrmCheque_TrocarProtador.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;

end;

procedure TfrmCheque_TrocarProtador.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
  
end;

procedure TfrmCheque_TrocarProtador.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva) + ' AND ((@@Loja = 0) OR (ContaBancaria.nCdLoja = @@Loja))');

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            qryContaBancaria.Close ;
            qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
            qryContaBancaria.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
            PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCheque_TrocarProtador.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
            qryTerceiro.Close ;
            PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

end.
