unit fMetaMensal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, cxPC, cxControls, DBCtrls, StdCtrls, Mask, DB, ADODB,
  cxLookAndFeelPainters, cxButtons,DateUtils, DBGridEhGrouping;

type
  TfrmMetaMensal = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    edtMesAno: TMaskEdit;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryMetaMes: TADOQuery;
    qryMetaMesnCdMetaMes: TIntegerField;
    qryMetaMesnCdLoja: TIntegerField;
    qryMetaMescNmLoja: TStringField;
    qryMetaMesnValMetaMinima: TBCDField;
    qryMetaMesnValMeta: TBCDField;
    qryMetaMesnValVendaMediaPrev: TBCDField;
    qryMetaMesnItensPorVendaPrev: TBCDField;
    qryMetaMesnValItemMedioPrev: TBCDField;
    qryMetaMesnValVendaHoraPrev: TBCDField;
    qryMetaMesnTxConversaoPrev: TBCDField;
    dsMetaMes: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    edtEmpresa: TMaskEdit;
    qryAux: TADOQuery;
    qryMetaMesdDtInicial: TDateTimeField;
    qryMetaMesdDtFinal: TDateTimeField;
    qryInsereSemana: TADOQuery;
    qryMetaMesnCdUsuarioUltAlt: TIntegerField;
    qryMetaMesdDtUltAlt: TDateTimeField;
    qryMetaMesiTotalHorasPrev: TBCDField;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEmpresaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure edtMesAnoChange(Sender: TObject);
    procedure edtEmpresaChange(Sender: TObject);
    procedure qryMetaMesBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function retornaDataInicialSemanaMeta(iSemana,iMes,iAno:integer) : TDateTime;
    function retornaDataFinalSemanaMeta(iSemana,iMes,iAno:integer)   : TDateTime;
    function retornaSemanasMes(iUltimoDataMes : TDateTime)           : integer;
  end;

var
  frmMetaMensal: TfrmMetaMensal;

implementation

uses fMenu, fLookup_Padrao,fMetaSemanal;

{$R *.dfm}

procedure TfrmMetaMensal.cxButton1Click(Sender: TObject);
var
  iMes, iAno, iSemanasMes, iSemana : integer;
begin
  inherited;

  if (trim(edtMesAno.Text) = '/') then
  begin
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
  end ;

  edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end ;

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  {-- se o campo da empresa estiver vazio preenche com a empresa ativa --}

  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMetaMes.Close;
      qryMetaMes.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(Trim(edtEmpresa.Text));
      qryMetaMes.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryMetaMes.Parameters.ParamByName('iMes').Value       := iMes;
      qryMetaMes.Parameters.ParamByName('iAno').Value       := iAno;
      qryMetaMes.Open;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  iSemanasMes := retornaSemanasMes(qryMetaMesdDtFinal.Value);

  qryMetaMes.First;

  while not qryMetaMes.Eof do
  begin
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM MetaSemana WHERE nCdMetaMes = ' + qryMetaMesnCdMetaMes.AsString);
      qryAux.Open;

      if (qryAux.Eof) then
      begin

          for iSemana := 1 to iSemanasMes do
          begin
              frmMenu.Connection.BeginTrans;

              try
                  qryInsereSemana.Close;
                  qryInsereSemana.Parameters.ParamByName('iSemana').Value    := iSemana;
                  qryInsereSemana.Parameters.ParamByName('dDtInicial').Value := DateToStr(retornaDataInicialSemanaMeta(iSemana,iMes,iAno));
                  qryInsereSemana.Parameters.ParamByName('dDtFinal').Value   := DateToStr(retornaDataFinalSemanaMeta(iSemana,iMes,iAno));
                  qryInsereSemana.Parameters.ParamByName('nCdMetaMes').Value := qryMetaMesnCdMetaMes.Value;
                  qryInsereSemana.ExecSQL;
              except
                  frmMenu.Connection.RollbackTrans;
                  MensagemErro('Erro no Processamento.');
                  raise;
              end;

              frmMenu.Connection.CommitTrans;

          end;
      end;

      qryMetaMes.Next;
  end;

  qryMetaMes.First;
  DBGridEh1.SetFocus;

end;

procedure TfrmMetaMensal.cxButton2Click(Sender: TObject);
begin
  inherited;
  qryMetaMes.Close;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtMesAno.Text  := '';

  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  
  edtMesAno.SetFocus;
  
end;

procedure TfrmMetaMensal.edtEmpresaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            qryEmpresa.Close;

            edtEmpresa.Text := IntToStr(nPK);
            PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMetaMensal.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

procedure TfrmMetaMensal.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

function TfrmMetaMensal.retornaDataFinalSemanaMeta(iSemana, iMes,
  iAno: integer): TDateTime;
var
    dDtInicialMes,dDtUltimoDiaMes,dDtInicialMesOriginal : TDateTime ;
    cMes,cMesFim : String;
    iSemanaCorrente:integer;
begin

    {-- preenche com zero a esquerda --}
    cMes := Copy('00'+intToStr(iMes),Length(intToStr(iMes))+1,2) ;

    {-- verifica se � o m�s de Dezembro --}
    if (iMes < 12) then
        cMesFim := Copy('00'+intToStr(iMes + 1),Length(intToStr(iMes + 1))+1,2)
    else cMesFim := '01';

    dDtInicialMes := StrToDate('01/' + cMes + '/' + intToStr(iAno));

    {-- quando for dezembro incrementa o ano --}
    if (iMes = 12) then
        iAno := iAno + 1;

    dDtUltimoDiaMes       := StrToDate('01/' + cMesFim + '/' + intToStr(iAno)) - 1;
    dDtInicialMesOriginal := dDtInicialMes ;
    iSemanaCorrente       := 1 ;

    while (dDtInicialMes <= dDtUltimoDiaMes) do
    begin

        {-- se n�o for o primeiro dia do m�s e for um domingo, soma 1 na semana --}
        if (dDtInicialMes > dDtInicialMesOriginal) then
            if (DayOfWeek(dDtInicialMes) = 1) then
                iSemanaCorrente := iSemanaCorrente + 1 ;

        if (iSemanaCorrente > iSemana) then
        begin
            Result := dDtInicialMes -1 ;
            exit ;
        end ;

        dDtInicialMes := dDtInicialMes + 1 ;

        if (MonthOfTheYear(dDtInicialMes) <> MonthOfTheYear(dDtInicialMesOriginal)) then
        begin
            Result := dDtInicialMes -1 ;
            exit ;
        end ;

    end ;

    Result := dDtInicialMes ;

end;

function TfrmMetaMensal.retornaDataInicialSemanaMeta(iSemana, iMes,
  iAno: integer): TDateTime;
var
    dDtInicialMes,dDtUltimoDiaMes,dDtInicialMesOriginal : TDateTime ;
    cMes,cMesFim : String ;
    iSemanaCorrente:integer;
begin

    {-- preenche com zero a esquerda --}
    cMes    := Copy('00'+intToStr(iMes),Length(intToStr(iMes))+1,2) ;

    {-- verifica se � o m�s de Dezembro --}
    if (iMes < 12) then
        cMesFim := Copy('00'+intToStr(iMes + 1),Length(intToStr(iMes + 1))+1,2)
    else cMesFim := '01';

    dDtInicialMes         := StrToDate('01/' + cMes + '/' + intToStr(iAno));

    {-- quando for dezembro incrementa o ano --}
    if (iMes = 12) then
        iAno := iAno + 1;

    dDtUltimoDiaMes       := StrToDate('01/' + cMesFim + '/' + intToStr(iAno)) - 1;
    dDtInicialMesOriginal := dDtInicialMes ;
    iSemanaCorrente       := 1 ;

    if (iSemana = 1) then
    begin
        Result := dDtInicialMes ;
        exit ;
    end ;

    while (dDtInicialMes <= dDtUltimoDiaMes) do
    begin

        {-- se n�o for o primeiro dia do m�s e for um domingo, soma 1 na semana --}
        if (dDtInicialMes > dDtInicialMesOriginal) then
            if (DayOfWeek(dDtInicialMes) = 1) then
                iSemanaCorrente := iSemanaCorrente + 1 ;

        if (iSemanaCorrente = iSemana) then
        begin
            Result := dDtInicialMes ;
            exit ;
        end ;

        dDtInicialMes := dDtInicialMes + 1 ;
    end ;

    Result := dDtInicialMes ;

end;

procedure TfrmMetaMensal.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmMetaSemanal;
begin
  inherited;

  if (qryMetaMes.Active) then
  begin
      if(qryMetaMes.RecordCount > 0) then
      begin
      
          objForm := TfrmMetaSemanal.Create(nil);

          objForm.qryMetaSemana.Close;
          objForm.qryMetaSemana.Parameters.ParamByName('nCdMetaMes').Value := qryMetaMesnCdMetaMes.Value;
          objForm.qryMetaSemana.Open;

          objForm.nValMetaMinima  := qryMetaMesnValMetaMinima.Value;
          objForm.nValMeta        := qryMetaMesnValMeta.Value;

          showForm(objForm,false);

          frmMenu.Connection.BeginTrans;

          try
              qryMetaMes.Edit;
              qryMetaMesiTotalHorasPrev.Value := objForm.iTotalHorasPrev;
              qryMetaMes.Post;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no Processamento.');
              raise;
          end;

          frmMenu.Connection.CommitTrans;

          qryMetaMes.Close;
          qryMetaMes.Open;
          qryMetaMes.First;

          DBGridEh1.SetFocus;
      end;
  end;
end;

function TfrmMetaMensal.retornaSemanasMes(iUltimoDataMes : TDateTime) : integer;
var
    iAno, iMes, iDia : word;
begin

    {-- pega o mes e o ano da data informada --}

    DecodeDate(iUltimoDataMes,iAno,iMes,iDia);

    {-- verifica em qual semana esta a ultima data do mes, de acordo com o mes e o ano --}
    
    if (retornaDataFinalSemanaMeta(4,iMes,iAno) = iUltimoDataMes) then
    begin
        Result := 4;
        exit;
    end;

    if (retornaDataFinalSemanaMeta(5,iMes,iAno) = iUltimoDataMes) then
    begin
        Result := 5;
        exit;
    end;
    
    Result := 6;
end;

procedure TfrmMetaMensal.edtMesAnoChange(Sender: TObject);
begin
  inherited;
  
  qryMetaMes.Close;
end;

procedure TfrmMetaMensal.edtEmpresaChange(Sender: TObject);
begin
  inherited;

  qryMetaMes.Close;
end;

procedure TfrmMetaMensal.qryMetaMesBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryMetaMes.State = dsEdit) then
  begin
      qryMetaMesdDtUltAlt.Value        := Now;
      qryMetaMesnCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;
  end;

end;

initialization
    RegisterClass(TfrmMetaMensal);
end.
