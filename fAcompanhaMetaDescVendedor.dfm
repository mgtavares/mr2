inherited frmAcompanhaMetaDescVendedor: TfrmAcompanhaMetaDescVendedor
  Left = 209
  Top = 72
  Width = 1009
  Height = 597
  Caption = 'Acompanhamento Desconto Vendedor'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 153
    Width = 993
    Height = 406
  end
  inherited ToolBar1: TToolBar
    Width = 993
    inherited ToolButton1: TToolButton
      Caption = '&Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 153
    Width = 993
    Height = 406
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 402
    ClientRectLeft = 4
    ClientRectRight = 989
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Acompanhamento do Vendedor'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 985
        Height = 378
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsMaster
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 3
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <
              item
                ValueType = fvtStaticText
              end
              item
                ValueType = fvtStaticText
              end
              item
              end>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|C'#243'd. Pedido'
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'dDtPedido'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|Data do Pedido'
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'nValDescontoAnt'
            Footer.FieldName = 'nValCotaDesconto'
            Footers = <
              item
                ValueType = fvtStaticText
              end
              item
                ValueType = fvtStaticText
              end
              item
              end>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|Saldo Anterior'
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'nValDescontoUtil'
            Footers = <
              item
                ValueType = fvtStaticText
              end
              item
                ValueType = fvtStaticText
              end
              item
              end>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|Desconto Utilizado'
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'nValDescontoPost'
            Footers = <
              item
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Value = 'Total Distribu'#237'do'
                ValueType = fvtStaticText
              end
              item
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Tahoma'
                Font.Style = []
                Value = 'Valor Utilizado'
                ValueType = fvtStaticText
              end
              item
                Value = 'Saldo Dispon'#237'vel'
                ValueType = fvtStaticText
              end>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|Saldo Posterior'
            Width = 120
          end
          item
            DisplayFormat = '#,##0.00'
            EditButtons = <>
            FieldName = 'nValPedido'
            Footers = <
              item
                Value = '0,00'
                ValueType = fvtStaticText
              end
              item
                DisplayFormat = '#,##0.00'
                Value = '0,00'
                ValueType = fvtStaticText
              end
              item
                DisplayFormat = '#,##0.00'
                Value = '0,00'
                ValueType = fvtStaticText
              end>
            Title.Caption = 'Hist'#243'rico de Utiliza'#231#227'o de Descontos|Valor do Pedido'
            Width = 120
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 993
    Height = 124
    Align = alTop
    TabOrder = 1
    object Label4: TLabel
      Tag = 1
      Left = 65
      Top = 47
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label2: TLabel
      Tag = 1
      Left = 39
      Top = 70
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Vendedor'
    end
    object Label1: TLabel
      Tag = 1
      Left = 44
      Top = 23
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 16
      Top = 96
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Meta'
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 161
      Top = 42
      Width = 432
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 5
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 161
      Top = 17
      Width = 432
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 160
      Top = 66
      Width = 433
      Height = 21
      DataField = 'cNmUsuario'
      DataSource = dsVendedor
      TabOrder = 6
    end
    object edtMesAno: TMaskEdit
      Left = 93
      Top = 91
      Width = 65
      Height = 21
      EditMask = '99/9999'
      MaxLength = 7
      TabOrder = 3
      Text = '  /    '
    end
    object er2LkpEmpresa: TER2LookupMaskEdit
      Left = 93
      Top = 17
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnExit = er2LkpEmpresaExit
      CodigoLookup = 8
      QueryLookup = qryEmpresa
    end
    object er2LkpLoja: TER2LookupMaskEdit
      Left = 93
      Top = 41
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnExit = er2LkpLojaExit
      CodigoLookup = 59
      QueryLookup = qryLoja
    end
    object er2LkpVendedor: TER2LookupMaskEdit
      Left = 93
      Top = 66
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      OnExit = er2LkpVendedorExit
      CodigoLookup = 703
      QueryLookup = qryVendedor
    end
  end
  inherited ImageList1: TImageList
    Left = 752
    Top = 88
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C00380030000000080018003000000008001800300000000800181030000
      0000800181030000000080010001000000008001000100000000800100010000
      000080010001000000008001C101000000018001F11F000000038001F11F0000
      0077C003FFFF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '      ,cFlgGerente'
      '      ,cFlgVendedor'
      '      ,nCdTerceiroResponsavel'
      '  FROM Usuario   '
      ' WHERE nCdUsuario    = :nPK'
      '   AND (cFlgVendedor = 1 OR cFlgGerente = 1)'
      '   AND EXISTS(SELECT 1                  '
      '                FROM UsuarioLoja                 '
      
        '               WHERE UsuarioLoja.nCdUsuario = Usuario.nCdUsuario' +
        '                   '
      '                 AND UsuarioLoja.nCdLoja    = :nCdLoja)')
    Left = 688
    Top = 56
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryVendedorcFlgGerente: TIntegerField
      FieldName = 'cFlgGerente'
    end
    object qryVendedorcFlgVendedor: TIntegerField
      FieldName = 'cFlgVendedor'
    end
    object qryVendedornCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 624
    Top = 56
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '  FROM Loja'
      
        '       INNER JOIN UsuarioLoja ON UsuarioLoja.nCdLoja = Loja.nCdL' +
        'oja'
      ' WHERE Loja.nCdLoja    = :nPK'
      '   AND Loja.nCdEmpresa = :nCdEmpresa'
      '   AND nCdUsuario      = :nCdUsuario')
    Left = 656
    Top = 56
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 656
    Top = 88
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 88
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 688
    Top = 88
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iMes'
        DataType = ftString
        Size = 2
        Value = '11'
      end
      item
        Name = 'iAno'
        DataType = ftString
        Size = 4
        Value = '2015'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '8'
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial  datetime'
      '       ,@dDtFinal    datetime'
      '       ,@iMes        int'
      '       ,@iAno        int'
      '       ,@nCdTerceiro int'
      '       '
      'SET @iMes        = :iMes'
      'SET @iAno        = :iAno'
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      'IF (@iMes < 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes + 1,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103) - 1)'
      'END'
      ''
      'IF (@iMes = 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(1,2) + '#39'/'#39' + (Convert(char(4),@iAno + 1)),103) - 1)'
      'END'
      ''
      'SELECT MVD.nCdMetaVendedorDesconto'
      '      ,MVD.nValCotaDesconto'
      '      ,Pedido.nCdPedido'
      '      ,Pedido.nValPedido'
      '      ,MDU.nValDescontoAnt'
      '      ,MDU.nValDescontoUtil'
      '      ,MDU.nValDescontoPost'
      '      ,MDU.dDtPedido'
      '  FROM MetaVendedorDesconto MVD'
      
        '       LEFT  JOIN MetaDescontoUtil MDU ON MDU.nCdMetaVendedorDes' +
        'conto = MVD.nCdMetaVendedorDesconto'
      
        '       LEFT  JOIN MetaDescontoMes MDM ON MDM.nCdMetaDescontoMes ' +
        '= MVD.nCdMetaDescontoMes'
      '       INNER JOIN Pedido ON Pedido.nCdPedido = MDU.nCdPedido'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = MVD.nCdTerc' +
        'eiroColab'
      ' WHERE MVD.nCdTerceiroColab = @nCdTerceiro '
      '   AND MDM.iMes             = @iMes'
      '   AND MDM.iAno             = @iAno')
    Left = 720
    Top = 53
    object qryMasternCdMetaVendedorDesconto: TIntegerField
      FieldName = 'nCdMetaVendedorDesconto'
    end
    object qryMasternValCotaDesconto: TBCDField
      FieldName = 'nValCotaDesconto'
      Precision = 12
      Size = 2
    end
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMasternValPedido: TBCDField
      FieldName = 'nValPedido'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescontoUtil: TBCDField
      FieldName = 'nValDescontoUtil'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryMasternValDescontoAnt: TBCDField
      FieldName = 'nValDescontoAnt'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescontoPost: TBCDField
      FieldName = 'nValDescontoPost'
      Precision = 12
      Size = 2
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 720
    Top = 85
  end
end
