inherited frmCentroDistribuicao: TfrmCentroDistribuicao
  Left = 218
  Top = 144
  Width = 922
  Height = 477
  Caption = 'Centro de Distribui'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 906
    Height = 414
  end
  object Label2: TLabel [1]
    Left = 98
    Top = 50
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'd.'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [2]
    Left = 73
    Top = 122
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [3]
    Left = 10
    Top = 145
    Width = 112
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local Est. Empenhado'
  end
  object Label5: TLabel [4]
    Left = 90
    Top = 169
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label1: TLabel [5]
    Left = 79
    Top = 73
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [6]
    Left = 101
    Top = 97
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit6
  end
  inherited ToolBar2: TToolBar
    Width = 906
    TabOrder = 11
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 127
    Top = 44
    Width = 65
    Height = 19
    DataField = 'nCdCentroDistribuicao'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit3: TDBEdit [9]
    Left = 127
    Top = 116
    Width = 498
    Height = 19
    DataField = 'cNmCentroDistribuicao'
    DataSource = dsMaster
    TabOrder = 5
  end
  object cxPageControl1: TcxPageControl [10]
    Left = 9
    Top = 198
    Width = 872
    Height = 241
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 10
    ClientRectBottom = 237
    ClientRectLeft = 4
    ClientRectRight = 868
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Locais Estoques - Centro de Distribui'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 864
        Height = 213
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLocalEstoqueCentroDistribuicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            Title.Caption = 'Local Estoque|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmLocalEstoque'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Local Estoque|Descri'#231#227'o'
            Width = 400
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Usu'#225'rio x Centro Distribui'#231#227'o'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 864
        Height = 213
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioCentroDistribuicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rio|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rio|Descri'#231#227'o'
            Width = 400
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Parametriza'#231#227'o Centro de Distribui'#231#227'o'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 864
        Height = 213
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLojaCentroDistribuicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh3Enter
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Title.Caption = 'Loja Destino|C'#243'digo'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Loja Destino|Descri'#231#227'o'
            Width = 164
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoPedidoSaida'
            Footers = <>
            Title.Caption = 'Tipo de Pedido de Sa'#237'da|C'#243'digo'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoPedidoSaida'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Pedido de Sa'#237'da|Descri'#231#227'o'
            Width = 175
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoPedidoEntrada'
            Footers = <>
            Title.Caption = 'Tipo de Pedido de Entrada|C'#243'digo'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoPedidoEntrada'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Pedido de Entrada|Descri'#231#227'o'
            Width = 175
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoReceb'
            Footers = <>
            Title.Caption = 'Tipo de Recebimento|C'#243'digo'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoReceb'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Recebimento|Descri'#231#227'o'
            Width = 175
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoFrequenciaDistribuicao'
            Footers = <>
            Title.Caption = 'Tipo de Frequ'#234'ncia Distribui'#231#227'o|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoFrequenciaDistribuicao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Frequ'#234'ncia Distribui'#231#227'o|Descri'#231#227'o'
            Width = 175
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit5: TDBEdit [11]
    Tag = 1
    Left = 195
    Top = 140
    Width = 430
    Height = 19
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstEmpenhadoPadrao
    TabOrder = 7
  end
  object ER2LookupDBEdit1: TER2LookupDBEdit [12]
    Left = 127
    Top = 164
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 8
    CodigoLookup = 2
    QueryLookup = qryStat
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 195
    Top = 164
    Width = 190
    Height = 19
    DataField = 'cNmStatus'
    DataSource = DataSource1
    TabOrder = 9
  end
  object ER2LookupDBEdit2: TER2LookupDBEdit [14]
    Left = 127
    Top = 140
    Width = 65
    Height = 19
    DataField = 'nCdLocalEstEmpenhoPadrao'
    DataSource = dsMaster
    TabOrder = 6
    CodigoLookup = 87
    QueryLookup = qryLocalEstEmpenhadoPadrao
  end
  object DBEdit4: TDBEdit [15]
    Tag = 1
    Left = 195
    Top = 68
    Width = 430
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object ER2LookupDBEdit3: TER2LookupDBEdit [16]
    Left = 127
    Top = 68
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 1
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object ER2LookupDBEdit4: TER2LookupDBEdit [17]
    Left = 127
    Top = 92
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 147
    QueryLookup = qryLojaCD
  end
  object DBEdit6: TDBEdit [18]
    Tag = 1
    Left = 195
    Top = 92
    Width = 430
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLojaCD
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdCentroDistribuicao'
      '      ,cNmCentroDistribuicao'
      '      ,nCdLocalEstEmpenhoPadrao'
      '      ,nCdStatus'
      '      ,nCdEmpresa'
      '      ,nCdLoja'
      '  FROM CentroDistribuicao'
      ' WHERE nCdCentroDistribuicao = :nPK')
    Left = 704
    Top = 40
    object qryMasternCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
    object qryMastercNmCentroDistribuicao: TStringField
      FieldName = 'cNmCentroDistribuicao'
      Size = 100
    end
    object qryMasternCdLocalEstEmpenhoPadrao: TIntegerField
      FieldName = 'nCdLocalEstEmpenhoPadrao'
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  inherited dsMaster: TDataSource
    Left = 704
    Top = 72
  end
  inherited qryID: TADOQuery
    Left = 736
    Top = 40
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 768
    Top = 40
  end
  inherited qryStat: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM STATUS'
      ' WHERE nCdStatus = :nPK')
    Left = 736
    Top = 72
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 768
    Top = 72
  end
  inherited ImageList1: TImageList
    Left = 800
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = qryStat
    Left = 736
    Top = 104
  end
  object qryLocalEstEmpenhadoPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 304
    Top = 304
    object qryLocalEstEmpenhadoPadraocNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsLocalEstEmpenhadoPadrao: TDataSource
    DataSet = qryLocalEstEmpenhadoPadrao
    Left = 304
    Top = 336
  end
  object qryLocalEstoqueCentroDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLocalEstoqueCentroDistribuicaoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoqueCentroDistribuicao'
      '      ,nCdCentroDistribuicao'
      '      ,nCdLocalEstoque'
      '  FROM LocalEstoqueCentroDistribuicao'
      ' WHERE nCdCentroDistribuicao = :nPK')
    Left = 400
    Top = 303
    object qryLocalEstoqueCentroDistribuicaonCdLocalEstoqueCentroDistribuicao: TIntegerField
      FieldName = 'nCdLocalEstoqueCentroDistribuicao'
    end
    object qryLocalEstoqueCentroDistribuicaonCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
    object qryLocalEstoqueCentroDistribuicaonCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoqueCentroDistribuicaocNmLocalEstoque: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmLocalEstoque'
      LookupDataSet = qryLocalEstoque
      LookupKeyFields = 'nCdLocalEstoque'
      LookupResultField = 'cNmLocalEstoque'
      KeyFields = 'nCdLocalEstoque'
      LookupCache = True
      Lookup = True
    end
  end
  object dsLocalEstoqueCentroDistribuicao: TDataSource
    DataSet = qryLocalEstoqueCentroDistribuicao
    Left = 400
    Top = 335
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT nCdLocalEstoque'
      '              ,cNmLocalEstoque'
      '   FROM LocalEstoque'
      '')
    Left = 464
    Top = 303
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
  end
  object qryUsuarioCentroDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioCentroDistribuicaoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuarioCentroDistribuicao'
      '      ,nCdCentroDistribuicao'
      '      ,nCdUsuario'
      '  FROM UsuarioCentroDistribuicao'
      ' WHERE nCdCentroDistribuicao = :nPK')
    Left = 368
    Top = 303
    object qryUsuarioCentroDistribuicaonCdUsuarioCentroDistribuicao: TIntegerField
      FieldName = 'nCdUsuarioCentroDistribuicao'
    end
    object qryUsuarioCentroDistribuicaonCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
    object qryUsuarioCentroDistribuicaonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioCentroDistribuicaocNmUsuario: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmUsuario'
      LookupDataSet = qryUsuario
      LookupKeyFields = 'nCdUsuario'
      LookupResultField = 'cNmUsuario'
      KeyFields = 'nCdUsuario'
      LookupCache = True
      Lookup = True
    end
  end
  object dsUsuarioCentroDistribuicao: TDataSource
    DataSet = qryUsuarioCentroDistribuicao
    Left = 368
    Top = 335
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdUsuario '
      '             ,cNmUsuario'
      '  FROM Usuario'
      '')
    Left = 496
    Top = 304
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 704
    Top = 104
  end
  object qryLojaCentroDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLojaCentroDistribuicaoBeforePost
    OnCalcFields = qryLojaCentroDistribuicaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LojaCentroDistribuicao'
      ' WHERE nCdCentroDistribuicao = :nPK')
    Left = 336
    Top = 304
    object qryLojaCentroDistribuicaonCdLojaCentroDistribuicao: TIntegerField
      FieldName = 'nCdLojaCentroDistribuicao'
    end
    object qryLojaCentroDistribuicaonCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
    object qryLojaCentroDistribuicaonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojaCentroDistribuicaocNmLoja: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmLoja'
      LookupDataSet = qryLoja
      LookupKeyFields = 'nCdLoja'
      LookupResultField = 'cNmLoja'
      KeyFields = 'nCdLoja'
      Size = 50
      Lookup = True
    end
    object qryLojaCentroDistribuicaonCdTipoPedidoSaida: TIntegerField
      FieldName = 'nCdTipoPedidoSaida'
    end
    object qryLojaCentroDistribuicaocNmTipoPedidoSaida: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTipoPedidoSaida'
      LookupDataSet = qryTipoPedidoSaida
      LookupKeyFields = 'nCdTipoPedido'
      LookupResultField = 'cNmTipoPedido'
      KeyFields = 'nCdTipoPedidoSaida'
      Size = 50
      Lookup = True
    end
    object qryLojaCentroDistribuicaonCdTipoPedidoEntrada: TIntegerField
      FieldName = 'nCdTipoPedidoEntrada'
    end
    object qryLojaCentroDistribuicaocNmTipoPedidoEntrada: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoPedidoEntrada'
      Size = 50
      Calculated = True
    end
    object qryLojaCentroDistribuicaonCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryLojaCentroDistribuicaocNmTipoReceb: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoReceb'
      Size = 50
      Calculated = True
    end
    object qryLojaCentroDistribuicaonCdTipoFrequenciaDistribuicao: TIntegerField
      FieldName = 'nCdTipoFrequenciaDistribuicao'
    end
    object qryLojaCentroDistribuicaocNmTipoFrequenciaDistribuicao: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTipoFrequenciaDistribuicao'
      LookupDataSet = qryTipoFrequenciaDistribuicao
      LookupKeyFields = 'nCdTipoFrequenciaDistribuicao'
      LookupResultField = 'cNmTipoFrequenciaDistribuicao'
      KeyFields = 'nCdTipoFrequenciaDistribuicao'
      Size = 50
      Lookup = True
    end
    object qryLojaCentroDistribuicaonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryLojaCentroDistribuicaodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object dsLojaCentroDistribuicao: TDataSource
    DataSet = qryLojaCentroDistribuicao
    Left = 336
    Top = 336
  end
  object qryTipoFrequenciaDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      ' SELECT nCdTipoFrequenciaDistribuicao'
      '              ,cNmTipoFrequenciaDistribuicao'
      '   FROM TipoFrequenciaDistribuicao'
      '')
    Left = 464
    Top = 335
    object qryTipoFrequenciaDistribuicaocNmTipoFrequenciaDistribuicao: TStringField
      FieldName = 'cNmTipoFrequenciaDistribuicao'
      Size = 50
    end
    object qryTipoFrequenciaDistribuicaonCdTipoFrequenciaDistribuicao: TIntegerField
      FieldName = 'nCdTipoFrequenciaDistribuicao'
    end
  end
  object qryTipoPedidoSaida: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '      ,cGerarFinanc'
      '  FROM TipoPedido'
      ' WHERE nCdTabTipoPedido = 1 -- sa'#237'da')
    Left = 528
    Top = 303
    object qryTipoPedidoSaidacNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryTipoPedidoSaidanCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidoSaidacGerarFinanc: TIntegerField
      FieldName = 'cGerarFinanc'
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdLoja'
      '             ,cNmLoja'
      '   FROM Loja'
      '')
    Left = 496
    Top = 335
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      '  FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 432
    Top = 304
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 432
    Top = 336
  end
  object qryTipoReceb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoReceb'
      '      ,cNmTipoReceb'
      '      ,cFlgRecebCego'
      '  FROM TipoReceb'
      ' WHERE nCdTipoReceb = :nPK'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM TipoPedidoTipoReceb TPT'
      '               WHERE TPT.nCdTipoReceb  = TipoReceb.nCdTipoReceb'
      '                 AND TPT.nCdTipoPedido = :nCdTipoPedido)')
    Left = 528
    Top = 336
    object qryTipoRecebnCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryTipoRecebcNmTipoReceb: TStringField
      FieldName = 'cNmTipoReceb'
      Size = 50
    end
    object qryTipoRecebcFlgRecebCego: TIntegerField
      FieldName = 'cFlgRecebCego'
    end
  end
  object qryTipoPedidoEntrada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido    = :nPK'
      '   AND nCdTabTipoPedido = 2 -- entrada')
    Left = 560
    Top = 303
    object qryTipoPedidoEntradanCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidoEntradacNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryLojaCD: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 272
    Top = 304
    object qryLojaCDcNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLojaCD: TDataSource
    DataSet = qryLojaCD
    Left = 272
    Top = 336
  end
  object qryVerificaEstoquePadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCentroDistribuicao'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLocalEstEmpenho'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdCentroDistribuicao int'
      ''
      'SET @nCdCentroDistribuicao = :nCdCentroDistribuicao'
      'SELECT TOP 1 1'
      '  FROM CentroDistribuicao'
      ' WHERE nCdLocalEstEmpenhoPadrao =  :nCdLocalEstEmpenho'
      
        '   AND ((nCdCentroDistribuicao    <> @nCdCentroDistribuicao) OR ' +
        '(@nCdCentroDistribuicao = 0))')
    Left = 776
    Top = 104
  end
end
