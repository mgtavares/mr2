unit fAgrupamentoCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, ER2Lookup, Mask;

type
  TfrmAgrupamentoCusto = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    edtEmpresa: TER2LookupDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    edtTipoAgrupamento: TER2LookupDBEdit;
    DBRadioGroup5: TDBRadioGroup;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    Label6: TLabel;
    qryTabTipoAgrupamentoContabil: TADOQuery;
    qryTabTipoAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField;
    qryTabTipoAgrupamentoContabilcNmTabTipoAgrupamentoContabil: TStringField;
    DBEdit5: TDBEdit;
    DataSource2: TDataSource;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    qryMasternCdAgrupamentoCusto: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmAgrupamentoCusto: TStringField;
    qryMastercMascara: TStringField;
    qryMasternCdTabTipoAgrupamentoContabil: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasteriMaxNiveis: TIntegerField;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAgrupamentoCusto: TfrmAgrupamentoCusto;

implementation

uses fMenu, fAgrupamentoCusto_Estrutura;

{$R *.dfm}

procedure TfrmAgrupamentoCusto.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  edtEmpresa.PosicionaQuery;

  qryMasternCdStatus.Value := 1 ;
  
  edtEmpresa.SetFocus;

end;

procedure TfrmAgrupamentoCusto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'AGRUPAMENTOCUSTO' ;
  nCdTabelaSistema  := 219 ;
  nCdConsultaPadrao := 222 ;

end;

procedure TfrmAgrupamentoCusto.qryMasterBeforePost(DataSet: TDataSet);
var
  iNiveis, i : integer;
begin

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione a empresa.') ;
      edtEmpresa.SetFocus;
      abort;
  end ;

  if (trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do agrupamento.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  if (trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Informe a m�scara do agrupamento.') ;
      DBEdit3.SetFocus;
      abort;
  end ;

  if (not frmMenu.fnValidaMascaraContabil( DBEdit3.Text )) then
  begin
      MensagemAlerta('M�scara inv�lida.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Selecione o tipo de agrupamento.') ;
      edtTipoAgrupamento.SetFocus;
      abort;
  end ;

  inherited;

  iNiveis := 1 ;

  for i := 0 to length( trim( DBEdit3.Text ) ) do
  begin

      if ( Copy( trim( DBEdit3.Text ) , i , 1 ) = '.') then
          inc( iNiveis ) ;

  end ;

  qryMasteriMaxNiveis.Value := iNiveis;


end;

procedure TfrmAgrupamentoCusto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryTabTipoAgrupamentoContabil.Close;
  
end;

procedure TfrmAgrupamentoCusto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryTabTipoAgrupamentoContabil.Close;

  PosicionaQuery( qryEmpresa , qryMasternCdEmpresa.asString ) ;
  PosicionaQuery( qryTabTipoAgrupamentoContabil , qryMasternCdTabTipoAgrupamentoContabil.AsString ) ;
   
end;

procedure TfrmAgrupamentoCusto.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmAgrupamentoCusto_Estrutura ;
begin
  inherited;

  if (qryMaster.isEmpty) then
  begin
      MensagemAlerta('Selecione um agrupamento de custos.') ;
      abort;
  end ;

  if (qryMaster.State in [dsInsert,dsEdit]) then
      qryMaster.Post; 

  objForm := TfrmAgrupamentoCusto_Estrutura.Create(Self) ;
  objForm.nCdAgrupamentoCusto := qryMasternCdAgrupamentoCusto.Value;
  objForm.cNmAgrupamentoCusto := qryMastercNmAgrupamentoCusto.Value;
  objForm.cFormatoMascara     := qryMastercMascara.Value;

  showForm( objForm, TRUE ) ;

end;

initialization
    registerClass(TfrmAgrupamentoCusto) ;
    
end.
