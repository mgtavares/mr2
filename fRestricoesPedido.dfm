inherited frmRestricoesPedido: TfrmRestricoesPedido
  Width = 915
  Height = 453
  Caption = 'Restri'#231#245'es Cr'#233'dito'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 899
    Height = 388
  end
  inherited ToolBar1: TToolBar
    Width = 899
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 899
    Height = 388
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = DataSource1
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cNmTabTipoRestricaoVenda'
        Footers = <>
        Title.Caption = 'Tipo de Diverg'#234'ncia'
        Width = 308
      end
      item
        EditButtons = <>
        FieldName = 'cPendenteLiberacao'
        Footers = <>
        Title.Caption = 'Pendente de Libera'#231#227'o'
        Width = 138
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryRestricao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRestricaoVendaTerceiro'
      '      ,TTRV.cNmTabTipoRestricaoVenda'
      '      ,nCdPedido'
      '      ,(CASE WHEN cFlgDesbloqueado = 0 '
      '             THEN '#39'SIM'#39
      '             ELSE NULL'
      '        END) cPendenteLiberacao'
      'FROM RestricaoVendaTerceiro'
      
        '     INNER JOIN TabTipoRestricaoVenda TTRV ON TTRV.nCdTabTipoRes' +
        'tricaoVenda = RestricaoVendaTerceiro.nCdTabTipoRestricaoVenda'
      'WHERE nCdPedido = :nPK')
    Left = 208
    Top = 168
    object qryRestricaonCdRestricaoVendaTerceiro: TIntegerField
      FieldName = 'nCdRestricaoVendaTerceiro'
    end
    object qryRestricaocNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
    object qryRestricaonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryRestricaocPendenteLiberacao: TStringField
      FieldName = 'cPendenteLiberacao'
      ReadOnly = True
      Size = 3
    end
  end
  object DataSource1: TDataSource
    DataSet = qryRestricao
    Left = 224
    Top = 200
  end
end
