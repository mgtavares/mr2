inherited frmRazaoAuxiliarFornecedores_View: TfrmRazaoAuxiliarFornecedores_View
  Left = 212
  Top = 168
  Caption = 'Raz'#227'o Auxiliar Fornecedores'
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1016
    Height = 514
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Position = spFooter
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValCredito'
          Column = cxGrid1DBTableView1nValCredito
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValDebito'
          Column = cxGrid1DBTableView1nValDebito
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1dDtMovto: TcxGridDBColumn
        DataBinding.FieldName = 'dDtMovto'
        Width = 86
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 227
      end
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTitulo'
        Width = 75
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        DataBinding.FieldName = 'cNrNF'
        Width = 93
      end
      object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
        DataBinding.FieldName = 'cNrTit'
        Width = 101
      end
      object cxGrid1DBTableView1nCdParc: TcxGridDBColumn
        DataBinding.FieldName = 'nCdParc'
        Width = 72
      end
      object cxGrid1DBTableView1cNmOperacao: TcxGridDBColumn
        DataBinding.FieldName = 'cNmOperacao'
        Width = 153
      end
      object cxGrid1DBTableView1nValCredito: TcxGridDBColumn
        DataBinding.FieldName = 'nValCredito'
        Width = 105
      end
      object cxGrid1DBTableView1nValDebito: TcxGridDBColumn
        DataBinding.FieldName = 'nValDebito'
        Width = 97
      end
      object cxGrid1DBTableView1nSaldo: TcxGridDBColumn
        DataBinding.FieldName = 'nSaldo'
        Width = 93
      end
      object cxGrid1DBTableView1cHistorico: TcxGridDBColumn
        DataBinding.FieldName = 'cHistorico'
        Width = 218
      end
      object cxGrid1DBTableView1cDocumento: TcxGridDBColumn
        DataBinding.FieldName = 'cDocumento'
        Width = 91
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        DataBinding.FieldName = 'cNmUsuario'
        Width = 166
      end
      object cxGrid1DBTableView1cContaBancaria: TcxGridDBColumn
        DataBinding.FieldName = 'cContaBancaria'
        Width = 200
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object uspRelatorio: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_RAZAO_AUXILIAR_FORNECEDORES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdGrupoEconomico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@nCdUsuario'
        DataType = ftInteger
        Value = 0
      end>
    Left = 840
    Top = 288
    object uspRelatorionCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object uspRelatoriocNmTerceiro: TStringField
      DisplayLabel = 'Fornecedor'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object uspRelatorionCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulo'
      FieldName = 'nCdTitulo'
    end
    object uspRelatoriodDtMovto: TDateTimeField
      DisplayLabel = 'Data Movto'
      FieldName = 'dDtMovto'
    end
    object uspRelatoriocNrNF: TStringField
      Alignment = taRightJustify
      DisplayLabel = 'N'#250'm. NF'
      FieldName = 'cNrNF'
    end
    object uspRelatorionCdParc: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'nCdParc'
    end
    object uspRelatoriocNrTit: TStringField
      Alignment = taRightJustify
      DisplayLabel = 'N'#250'm T'#237'tulo'
      FieldName = 'cNrTit'
    end
    object uspRelatoriocNmOperacao: TStringField
      DisplayLabel = 'Opera'#231#227'o'
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object uspRelatorionValCredito: TBCDField
      DisplayLabel = 'Valor Credito'
      FieldName = 'nValCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValDebito: TBCDField
      DisplayLabel = 'Valor Debito'
      FieldName = 'nValDebito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionSaldo: TBCDField
      DisplayLabel = 'Saldo'
      FieldName = 'nSaldo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatoriocDocumento: TStringField
      Alignment = taRightJustify
      DisplayLabel = 'Documento'
      FieldName = 'cDocumento'
    end
    object uspRelatoriocNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object uspRelatoriocContaBancaria: TStringField
      DisplayLabel = 'Conta Banc'#225'ria'
      FieldName = 'cContaBancaria'
      Size = 50
    end
    object uspRelatoriocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
    end
    object uspRelatoriocFlgExibeRazao: TIntegerField
      FieldName = 'cFlgExibeRazao'
    end
    object uspRelatoriocHistorico: TStringField
      DisplayLabel = 'Hist'#243'rico'
      FieldName = 'cHistorico'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = uspRelatorio
    Left = 880
    Top = 288
  end
end
