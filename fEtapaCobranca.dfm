inherited frmEtapaCobranca: TfrmEtapaCobranca
  Left = 291
  Top = 264
  Width = 881
  Height = 327
  Caption = 'Etapas de Cobran'#231'a'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 865
    Height = 264
  end
  object Label1: TLabel [1]
    Left = 64
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 53
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 28
    Top = 110
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias de Atraso'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 182
    Top = 110
    Width = 174
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtde M'#225'xima Itens Lista Cobran'#231'a'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 430
    Top = 110
    Width = 77
    Height = 13
    Caption = '(0 = sem limite)'
    FocusControl = DBEdit3
  end
  object Label6: TLabel [6]
    Left = 4
    Top = 134
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Contato Telef'#244'nico'
    FocusControl = DBEdit3
  end
  object Label7: TLabel [7]
    Left = 234
    Top = 134
    Width = 122
    Height = 13
    Alignment = taRightJustify
    Caption = 'Enviar Correspond'#234'ncia'
    FocusControl = DBEdit3
  end
  object Label8: TLabel [8]
    Left = 53
    Top = 158
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Negativar'
    FocusControl = DBEdit3
  end
  object Label9: TLabel [9]
    Left = 281
    Top = 158
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Liberar Acordo'
    FocusControl = DBEdit3
  end
  object Label10: TLabel [10]
    Left = 13
    Top = 182
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Enviar Cobradora'
    FocusControl = DBEdit3
  end
  object Label11: TLabel [11]
    Left = 330
    Top = 182
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ativo'
    FocusControl = DBEdit3
  end
  object Label12: TLabel [12]
    Left = 17
    Top = 86
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de titulo'
    FocusControl = DBEdit5
  end
  inherited ToolBar2: TToolBar
    Width = 865
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 105
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdEtapaCobranca'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [15]
    Left = 105
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmEtapaCobranca'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [16]
    Left = 105
    Top = 104
    Width = 65
    Height = 19
    DataField = 'iDiasVencto'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit4: TDBEdit [17]
    Left = 359
    Top = 104
    Width = 65
    Height = 19
    DataField = 'iQtdeMaxItensLista'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBLookupComboBox1: TDBLookupComboBox [18]
    Left = 105
    Top = 128
    Width = 55
    Height = 19
    Ctl3D = False
    DataField = 'cFlgTelefone'
    DataSource = dsMaster
    DropDownRows = 2
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsTelefone
    ParentCtl3D = False
    TabOrder = 6
  end
  object DBLookupComboBox2: TDBLookupComboBox [19]
    Left = 358
    Top = 128
    Width = 55
    Height = 19
    DataField = 'cFlgCarta'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsCarta
    TabOrder = 7
  end
  object DBLookupComboBox3: TDBLookupComboBox [20]
    Left = 105
    Top = 152
    Width = 55
    Height = 19
    DataField = 'cFlgNegativar'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsNegativar
    TabOrder = 8
  end
  object DBLookupComboBox4: TDBLookupComboBox [21]
    Left = 358
    Top = 152
    Width = 55
    Height = 19
    DataField = 'cFlgAcordo'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsAcordo
    TabOrder = 9
  end
  object DBLookupComboBox5: TDBLookupComboBox [22]
    Left = 105
    Top = 176
    Width = 55
    Height = 19
    DataField = 'cFlgCobradora'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsCobradora
    TabOrder = 10
  end
  object DBLookupComboBox6: TDBLookupComboBox [23]
    Left = 358
    Top = 176
    Width = 55
    Height = 19
    DataField = 'cFlgAtivo'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsAtivo
    TabOrder = 11
  end
  object DBEdit5: TDBEdit [24]
    Left = 105
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdEspTit'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [25]
    Tag = 1
    Left = 176
    Top = 80
    Width = 577
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTit
    TabOrder = 12
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ETAPACOBRANCA '
      'WHERE nCdEtapaCobranca = :nPK')
    Left = 128
    Top = 216
    object qryMasternCdEtapaCobranca: TIntegerField
      FieldName = 'nCdEtapaCobranca'
    end
    object qryMastercNmEtapaCobranca: TStringField
      FieldName = 'cNmEtapaCobranca'
      Size = 50
    end
    object qryMasteriDiasVencto: TIntegerField
      FieldName = 'iDiasVencto'
    end
    object qryMastercFlgTelefone: TIntegerField
      FieldName = 'cFlgTelefone'
    end
    object qryMastercFlgCarta: TIntegerField
      FieldName = 'cFlgCarta'
    end
    object qryMastercFlgAcordo: TIntegerField
      FieldName = 'cFlgAcordo'
    end
    object qryMastercFlgNegativar: TIntegerField
      FieldName = 'cFlgNegativar'
    end
    object qryMastercFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMasteriQtdeMaxItensLista: TIntegerField
      FieldName = 'iQtdeMaxItensLista'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
  end
  inherited dsMaster: TDataSource
    Left = 128
    Top = 248
  end
  inherited qryID: TADOQuery
    Left = 384
    Top = 216
  end
  inherited qryStat: TADOQuery
    Left = 320
    Top = 216
  end
  object dsTelefone: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 160
    Top = 248
  end
  object dsCarta: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 192
    Top = 248
  end
  object dsNegativar: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 224
    Top = 248
  end
  object dsAcordo: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 256
    Top = 248
  end
  object dsCobradora: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 288
    Top = 248
  end
  object dsAtivo: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 320
    Top = 248
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM EspTit '
      'WHERE nCdEspTit =:nPK'
      'AND cFlgCobranca = 1')
    Left = 352
    Top = 216
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
  object dsEspTit: TDataSource
    DataSet = qryEspTit
    Left = 352
    Top = 248
  end
end
