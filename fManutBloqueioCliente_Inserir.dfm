inherited frmManutBloqueioCliente_Inserir: TfrmManutBloqueioCliente_Inserir
  Left = 337
  Top = 298
  Width = 650
  Height = 135
  BorderIcons = [biSystemMenu]
  Caption = 'Inclus'#227'o de Bloqueio'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 634
    Height = 70
  end
  object Label5: TLabel [1]
    Left = 11
    Top = 48
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Bloqueio'
  end
  object Label1: TLabel [2]
    Left = 31
    Top = 72
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
  end
  inherited ToolBar1: TToolBar
    Width = 634
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtnCdTabTipoRestricaoVenda: TMaskEdit [4]
    Left = 92
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtnCdTabTipoRestricaoVendaExit
    OnKeyDown = edtnCdTabTipoRestricaoVendaKeyDown
  end
  object Edit1: TEdit [5]
    Left = 92
    Top = 64
    Width = 525
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 2
    OnKeyDown = Edit1KeyDown
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 160
    Top = 40
    Width = 457
    Height = 21
    DataField = 'cNmTabTipoRestricaoVenda'
    DataSource = DataSource1
    TabOrder = 3
  end
  object qryTabTipoRestricaoVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM TabTipoRestricaoVenda'
      'WHERE nCdTabTipoRestricaoVenda = :nPK'
      'AND nCdTabTipoRestricaoVenda > 100')
    Left = 472
    Top = 64
    object qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
    object qryTabTipoRestricaoVendacNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTabTipoRestricaoVenda
    Left = 504
    Top = 64
  end
  object SP_INSERI_BLOQUEIO_TERCEIRO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INSERI_BLOQUEIO_TERCEIRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdTabTipoRestricaoVenda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuarioBloqueio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdLojaBloqueio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cObs'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end>
    Left = 368
    Top = 40
  end
  object qryVerificaDup: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTabTipoRestricaoVenda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1 '
      '  FROM RestricaoVendaTerceiro'
      ' WHERE nCdTerceiro              = :nCdTerceiro'
      '   AND nCdTabTipoRestricaoVenda = :nCdTabTipoRestricaoVenda '
      '   AND cFlgDesbloqueado         = 0')
    Left = 240
    Top = 40
  end
end
