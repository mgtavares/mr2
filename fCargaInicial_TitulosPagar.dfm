inherited frmCargaInicial_TitulosPagar: TfrmCargaInicial_TitulosPagar
  Left = 36
  Top = 150
  Width = 1024
  Caption = 'Carga Inicial - T'#237'tulos a Pagar'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1008
  end
  inherited ToolBar1: TToolBar
    Width = 1008
    ButtonWidth = 104
    inherited ToolButton1: TToolButton
      Caption = 'Gravar T'#237'tulos'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 104
    end
    inherited ToolButton2: TToolButton
      Left = 112
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1008
    Height = 435
    Align = alClient
    DataSource = dsTitulos
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    ParentFont = False
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        ReadOnly = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEspTit'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footers = <>
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 64
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        Width = 39
      end
      item
        AutoDropDown = True
        EditButtons = <>
        FieldName = 'cFlgDocCobranca'
        Footers = <>
        KeyList.Strings = (
          '0'
          '1')
        PickList.Strings = (
          'N'#227'o'
          'Sim')
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 180
      end
      item
        EditButtons = <>
        FieldName = 'nCdCategFinanc'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmCategFinanc'
        Footers = <>
        ReadOnly = True
        Width = 180
      end
      item
        EditButtons = <>
        FieldName = 'nCdUnidadeNegocio'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmUnidadeNegocio'
        Footers = <>
        ReadOnly = True
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nCdCC'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmCC'
        Footers = <>
        ReadOnly = True
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'nCdMoeda'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cSenso'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
        Width = 79
      end
      item
        EditButtons = <>
        FieldName = 'dDtReceb'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'dDtCad'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 69
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cFlgInclusaoManual'
        Footers = <>
        Visible = False
      end>
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF00107082005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C003E003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000018001C003000000038001C0030000
      0077C003FFFF0000007FFFFFFFFF0000}
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    AfterInsert = qryTitulosAfterInsert
    BeforePost = qryTitulosBeforePost
    OnCalcFields = qryTitulosCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 nCdTitulo'
      '      ,nCdEmpresa '
      '      ,nCdEspTit'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,nCdTerceiro'
      '      ,nCdCategFinanc'
      '      ,nCdMoeda'
      '      ,cSenso'
      '      ,dDtEmissao'
      '      ,dDtReceb'
      '      ,dDtVenc'
      '      ,dDtCad'
      '      ,nValTit'
      '      ,nSaldoTit'
      '      ,cOBSTit'
      '      ,nCdUnidadeNegocio'
      '      ,nCdCC'
      '      ,cFlgDocCobranca'
      '      ,cNrNF'
      '      ,nCdLojaTit'
      '      ,cFlgInclusaoManual'
      '  FROM Titulo')
    Left = 280
    Top = 112
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTitulosnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'Parc.'
      FieldName = 'iParcela'
    end
    object qryTitulosnCdTerceiro: TIntegerField
      DisplayLabel = 'Credor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryTituloscNmTerceiro: TStringField
      DisplayLabel = 'Credor|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryTitulosnCdCategFinanc: TIntegerField
      DisplayLabel = 'Categoria Financeira|C'#243'd'
      FieldName = 'nCdCategFinanc'
    end
    object qryTituloscNmCategFinanc: TStringField
      DisplayLabel = 'Categoria Financeira|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCategFinanc'
      Size = 50
      Calculated = True
    end
    object qryTitulosnCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTituloscSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'Data Emiss'#227'o'
      FieldName = 'dDtEmissao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryTitulosdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'Vencimento'
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryTitulosdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'Valor T'#237'tulo'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnCdUnidadeNegocio: TIntegerField
      DisplayLabel = 'Unidade de Neg'#243'cio|C'#243'd'
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTituloscNmUnidadeNegocio: TStringField
      DisplayLabel = 'Unidade de Neg'#243'cio|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
      Calculated = True
    end
    object qryTitulosnCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryTituloscNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
    object qryTituloscFlgDocCobranca: TIntegerField
      DisplayLabel = 'Boleto Enviado'
      FieldName = 'cFlgDocCobranca'
    end
    object qryTituloscNrNF: TStringField
      DisplayLabel = 'N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosnCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTituloscFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 328
    Top = 112
  end
  object usp_ProximoID: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'usp_ProximoID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmTabela'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@iUltimoCodigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 392
    Top = 136
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 464
    Top = 216
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategFinanc, cNmCategFinanc'
      'FROM CategFinanc'
      'WHERE nCdCategFinanc = :nPK'
      'AND cFlgRecDes = '#39'D'#39)
    Left = 528
    Top = 216
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio, cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 536
    Top = 264
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      '  FROM Centrocusto'
      ' WHERE cFLgLanc = 1'
      'AND nCdCC = :nPK')
    Left = 544
    Top = 312
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
end
