inherited frmCancelaBorderoRecebimento: TfrmCancelaBorderoRecebimento
  Left = 81
  Top = 9
  Width = 1146
  Height = 755
  Caption = 'Cancelar Border'#244' Recebimento'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 65
    Width = 1130
    Height = 654
  end
  inherited ToolBar1: TToolBar
    Width = 1130
    ButtonWidth = 76
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 76
      Caption = '&Atualizar'
    end
    inherited ToolButton2: TToolButton
      Left = 84
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1130
    Height = 36
    Align = alTop
    TabOrder = 1
    object RadioGroup1: TRadioGroup
      Left = 0
      Top = 0
      Width = 625
      Height = 36
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Somente Remessas Pendentes'
        'Somente Remessas Canceladas'
        'Todas as Remessas')
      TabOrder = 0
      OnClick = RadioGroup1Click
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 65
    Width = 1130
    Height = 654
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = DataSource1
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBorderoFinanceiro'
        Footers = <>
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'dDtGeracao'
        Footers = <>
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuarioGeracao'
        Footers = <>
        Width = 88
      end
      item
        EditButtons = <>
        FieldName = 'cNmBanco'
        Footers = <>
        Width = 255
      end
      item
        EditButtons = <>
        FieldName = 'cAgencia'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTitular'
        Footers = <>
        Width = 217
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeTitulo'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValTotalTitulo'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmArquivoRemessa'
        Footers = <>
        Width = 165
      end
      item
        EditButtons = <>
        FieldName = 'cNmComputadorGeracao'
        Footers = <>
        Width = 188
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 656
  end
  object qryRemessas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cFlgListarRemessa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cFlgListarRemessa int'
      ''
      
        'SET @cFlgListarRemessa = :cFlgListarRemessa -- 0- Somente Penden' +
        'tes, 1- Somente Canceladas, 2- Todas'
      ''
      'SELECT BorderoFinanceiro.nCdBorderoFinanceiro'
      '      ,BorderoFinanceiro.dDtGeracao'
      '      ,BorderoFinanceiro.nCdUsuarioGeracao'
      '      ,ContaBancaria.nCdBanco '
      '      ,Banco.cNmBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.cNmTitular'
      '      ,BorderoFinanceiro.nQtdeTitulo'
      '      ,BorderoFinanceiro.nValTotalTitulo'
      '      ,BorderoFinanceiro.cNmArquivoRemessa'
      '      ,BorderoFinanceiro.cNmComputadorGeracao'
      '  FROM BorderoFinanceiro'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = BorderoFinanceiro.nCdContaBancaria'
      
        '       LEFT  JOIN Banco         ON Banco.nCdBanco               ' +
        '  = ContaBancaria.nCdBanco'
      
        ' WHERE (   ((@cFlgListarRemessa = 2) AND (BorderoFinanceiro.dDtC' +
        'ancelamento   IS     NULL))'
      
        '        OR ((@cFlgListarRemessa = 1) AND (BorderoFinanceiro.dDtC' +
        'ancelamento   IS NOT NULL)) '
      
        '        OR ((@cFlgListarRemessa = 0) AND (BorderoFinanceiro.cNmA' +
        'rquivoRemessa IS     NULL) AND (BorderoFinanceiro.dDtCancelament' +
        'o IS NULL)))')
    Left = 664
    Top = 192
    object qryRemessasnCdBorderoFinanceiro: TIntegerField
      DisplayLabel = 'C'#243'd. Border'#244
      FieldName = 'nCdBorderoFinanceiro'
    end
    object qryRemessasdDtGeracao: TDateTimeField
      DisplayLabel = 'Data Gera'#231#227'o'
      FieldName = 'dDtGeracao'
    end
    object qryRemessasnCdUsuarioGeracao: TIntegerField
      DisplayLabel = 'Usu'#225'rio Gera'#231#227'o'
      FieldName = 'nCdUsuarioGeracao'
    end
    object qryRemessasnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryRemessascNmBanco: TStringField
      DisplayLabel = 'Banco'
      FieldName = 'cNmBanco'
      Size = 50
    end
    object qryRemessascAgencia: TIntegerField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'cAgencia'
    end
    object qryRemessasnCdConta: TStringField
      DisplayLabel = 'Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryRemessascNmTitular: TStringField
      DisplayLabel = 'Titular'
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryRemessasnValTotalTitulo: TBCDField
      DisplayLabel = 'Valor Border'#244
      FieldName = 'nValTotalTitulo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRemessascNmArquivoRemessa: TStringField
      DisplayLabel = 'Arquivo Remessa'
      FieldName = 'cNmArquivoRemessa'
      Size = 50
    end
    object qryRemessascNmComputadorGeracao: TStringField
      DisplayLabel = 'Computador Gera'#231#227'o'
      FieldName = 'cNmComputadorGeracao'
      Size = 50
    end
    object qryRemessasnQtdeTitulo: TIntegerField
      DisplayLabel = 'Qt. Titulos'
      FieldName = 'nQtdeTitulo'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryRemessas
    Left = 680
    Top = 224
  end
  object qryVerificaBordero: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM BorderoFinanceiro'
      ' WHERE nCdBorderoFinanceiro = :nPK'
      '   AND dDtCancelamento IS NOT NULL '
      '')
    Left = 776
    Top = 208
  end
end
