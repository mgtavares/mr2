unit rPagamentoCentroCustoSint_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, jpeg, QuickRpt, ExtCtrls;

type
  TrptPagamentoCentroCustoSint_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape3: TQRShape;
    QRBand5: TQRBand;
    QRBand3: TQRBand;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    uspRelatorio: TADOStoredProc;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRLabel5: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel6: TQRLabel;
    uspRelatorionCdCC: TIntegerField;
    uspRelatorioiNivel: TIntegerField;
    uspRelatoriocNmCC: TStringField;
    uspRelatorionValCC: TBCDField;
    uspRelatorionCdOrdem: TIntegerField;
    uspRelatoriocCdCC: TStringField;
    uspRelatorionValPercNivelAnt: TBCDField;
    uspRelatorionValPercTotalGer: TBCDField;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel8: TQRLabel;
    procedure QRDBText5Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPagamentoCentroCustoSint_View: TrptPagamentoCentroCustoSint_View;

implementation

{$R *.dfm}

procedure TrptPagamentoCentroCustoSint_View.QRDBText5Print(sender: TObject;
  var Value: String);
begin
  if ((uspRelatorioInivel.Value <= 2)  OR (uspRelatorioInivel.Value = 9)) then  QRDBText5.Font.Style := QRDBText5.Font.Style + [fsBold]
  else QRDBText5.Font.Style := QRDBText5.Font.Style - [fsBold];
end;

end.
