unit fMotBaixaBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmMotBaixaBemPatrim = class(TfrmCadastro_Padrao)
    qryMasternCdMotBaixaBemPatrim: TIntegerField;
    qryMastercNmMotBaixaBemPatrim: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMotBaixaBemPatrim: TfrmMotBaixaBemPatrim;

implementation

{$R *.dfm}

procedure TfrmMotBaixaBemPatrim.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MOTBAIXABEMPATRIM' ;
  nCdTabelaSistema  := 502 ;
  nCdConsultaPadrao := 1006 ;

end;

procedure TfrmMotBaixaBemPatrim.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o do Motivo de Baixa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

end;
procedure TfrmMotBaixaBemPatrim.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
  
end;

initialization
    RegisterClass(TfrmMotBaixaBemPatrim) ;

end.
