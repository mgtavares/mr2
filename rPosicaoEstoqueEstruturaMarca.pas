unit rPosicaoEstoqueEstruturaMarca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, comObj, ExcelXP;

type
  TrptPosicaoEstoqueEstruturaMarca = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    RadioGroup4: TRadioGroup;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    RadioGroup7: TRadioGroup;
    RadioGroup8: TRadioGroup;
    edtCdCampanhaPromoc: TMaskEdit;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    DataSource11: TDataSource;
    RadioGroup1: TRadioGroup;
    qryGrupoEstoque: TADOQuery;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    Label15: TLabel;
    edtGrupoEstoque: TMaskEdit;
    DBEdit12: TDBEdit;
    DataSource12: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtCdCampanhaPromocExit(Sender: TObject);
    procedure edtCdCampanhaPromocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEstoqueExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueEstruturaMarca: TrptPosicaoEstoqueEstruturaMarca;

implementation

uses fMenu, fLookup_Padrao, rPosicaoEstoqueEstruturaMarca_view;

{$R *.dfm}

procedure TrptPosicaoEstoqueEstruturaMarca.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, edtLoja.text) ;
  end
  else
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptPosicaoEstoqueEstruturaMarca.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        :TrptPosicaoEstoqueEstruturaMarca_view;

begin
  inherited;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(StrToDate(edtDtFinal.Text)-30) ;

  if (frmMenu.ConvData(edtDtFinal.Text) > Date) then
  begin
      MensagemErro('A data final da venda n�o pode ser maior que hoje.') ;
      edtDtFinal.SetFocus;
      abort;
  end ;

  objRel := TrptPosicaoEstoqueEstruturaMarca_view.Create(nil);

  try
      try
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Close ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdGrupoEstoque').Value  := frmMenu.ConvInteiro(edtGrupoEstoque.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@cFlgSoProdAtivo').Value  := RadioGroup4.ItemIndex ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@nCdCampanhaPromoc').Value:= frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@dDtVendaIni').Value      := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@dDtVendaFim').Value      := frmMenu.ConvData(edtDtFinal.Text) ;

          if (RadioGroup7.ItemIndex = 0) then
              objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@cFlgDestPromocao').Value := 1
          else objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@cFlgDestPromocao').Value := 0 ;

          if (RadioGroup8.ItemIndex = 0) then
              objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@cFlgSoPromocao').Value   := 1
          else objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Parameters.ParamByName('@cFlgSoPromocao').Value   := 0 ;

          objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Open ;


          if (RadioGroup1.ItemIndex = 0) then
          begin
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '' ;

              if (DBEdit12.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Estoque: ' + trim(edtGrupoEstoque.Text) + '-' + DbEdit12.Text ;

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ Departamento: ' + trim(edtDepartamento.Text) + '-' + DbEdit2.Text ;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ Categoria: ' + trim(edtCategoria.Text) + '-' + DbEdit3.Text ;

              if (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + '/ SubCategoria: ' + trim(edtSubCategoria.Text) + '-' + DbEdit4.Text ;

              if (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + '/ Segmento: ' + trim(edtSegmento.Text) + '-' + DbEdit5.Text ;

              if (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + '/ Marca: ' + trim(edtMarca.Text) + '-' + DbEdit6.Text ;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ Linha: ' + trim(edtLinha.Text) + '-' + DbEdit8.Text ;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ Cole��o: ' + trim(edtColecao.Text) + '-' + DbEdit9.Text ;

              if (DBEdit7.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Produto: ' + trim(edtGrupoProduto.Text) + '-' + DbEdit7.Text ;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ Classe Produto: ' + trim(edtClasseProduto.Text) + '-' + DbEdit10.Text ;

              if (DBEdit11.Text <> '') then
                  cFiltro := cFiltro + '/ CampanhaPromoc: ' + trim(edtCdCampanhaPromoc.Text) + '-' + DbEdit11.Text ;

              cFiltro := cFiltro + '/ Per�odo Venda : ' + trim(edtDtInicial.Text) + ' -  ' + trim(edtDtFinal.Text);

              if (RadioGroup7.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Dest. Promo��o: Sim'
              else cFiltro := cFiltro + '/ Dest. Promo��o: N�o' ;

              if (RadioGroup8.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ S� em Promo��o: Sim'
              else cFiltro := cFiltro + '/ S� em Promo��o: N�o' ;

              objRel.lblFiltro1.Caption := cFiltro ;
              objRel.QRLabel11.Caption := 'Posi��o estoque em ' + trim(edtDtFinal.Text) ;

              objRel.QuickRep1.PreviewModal;
          end
          else
          begin

              try
              planilha:= CreateoleObject('Excel.Application');
              except
                  MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
                  exit ;
              end ;

              LCID := GetUserDefaultLCID;
              planilha.DisplayAlerts  := False;
              planilha.WorkBooks.add(1);
              planilha.ScreenUpdating := true;

              planilha.caption := 'Posi��o de Estoque - Estrutura x Marca';

              for linha := 0 to objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.RecordCount - 1 do
              begin

                  for coluna := 1 to objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.FieldCount do
                  begin

                      valorcampo := objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Fields[coluna - 1].AsString;
                      planilha.cells[linha + 2,coluna] := valorCampo;

                      if (objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Fields[coluna - 1].DataType = ftBCD) then
                      begin
                          planilha.Cells[Linha+2,coluna].NumberFormat        := '0,00' ;
                          planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlRight ;
                      end ;

                      if (objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Fields[coluna - 1].DataType = ftDateTime) then
                      begin
                          planilha.Cells[Linha+2,coluna].NumberFormat        := 'dd/mm/yyyy' ;
                          planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlLeft ;
                      end ;

                  end;

                  objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Next;

              end;

              for coluna := 1 to objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.FieldCount do
              begin

                  valorcampo := objRel.SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA.Fields[coluna - 1].DisplayLabel;
                  planilha.cells[1,coluna] := valorcampo;

              end;

              planilha.columns.Autofit;
              planilha.Visible := True;

          end ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtCdCampanhaPromocExit(Sender: TObject);
begin
  inherited;

  qryCampanhaPromoc.Close;
  PosicionaQuery(qryCampanhaPromoc, edtCdCampanhaPromoc.Text) ;
  
end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtCdCampanhaPromocKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(194);

        If (nPK > 0) then
            edtCdCampanhaPromoc.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtGrupoEstoqueKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(85);

        If (nPK > 0) then
        begin

            edtGrupoEstoque.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptPosicaoEstoqueEstruturaMarca.edtGrupoEstoqueExit(
  Sender: TObject);
begin
  inherited;

  qryGrupoEstoque.Close;
  PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;

end;

initialization
    RegisterClass(TrptPosicaoEstoqueEstruturaMarca) ;

end.
