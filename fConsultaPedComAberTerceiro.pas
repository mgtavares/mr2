unit fConsultaPedComAberTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, ExtCtrls, ImgList,
  ComCtrls, ToolWin, DB, ADODB, StdCtrls, Mask, DBCtrls, DBGridEhGrouping;

type
  TfrmConsultaPedComAberTerceiro = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    edtTerceiro: TMaskEdit;
    Label1: TLabel;
    edtGrupoEconomico: TMaskEdit;
    Label2: TLabel;
    edtMarca: TMaskEdit;
    Label3: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    edtPedido: TMaskEdit;
    Label4: TLabel;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    qryResultadonCdPedido: TIntegerField;
    qryResultadodDtPedido: TDateTimeField;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadocNrPedTerceiro: TStringField;
    qryResultadocNmContato: TStringField;
    qryTerceironCdGrupoEconomico: TIntegerField;
    qryResultadocNmGrupoEconomico: TStringField;
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtGrupoEconomicoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEconomicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure edtTerceiroChange(Sender: TObject);
    procedure edtGrupoEconomicoChange(Sender: TObject);
    procedure edtMarcaChange(Sender: TObject);
    procedure edtPedidoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPedComAberTerceiro: TfrmConsultaPedComAberTerceiro;

implementation

uses fLookup_Padrao, fMenu, fConsultaPedComAberTerceiro_Itens;

{$R *.dfm}

procedure TfrmConsultaPedComAberTerceiro.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

  if (qryTerceiro.Active) and (qryTerceironCdGrupoEconomico.Value > 0) then
  begin
  
      edtGrupoEconomico.Text := qryTerceironCdGrupoEconomico.asString;

      qryGrupoEconomico.Close ;
      PosicionaQuery(qryGrupoEconomico, edtGrupoEconomico.Text) ;

  end ;
  
end;

procedure TfrmConsultaPedComAberTerceiro.edtGrupoEconomicoExit(
  Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  PosicionaQuery(qryGrupoEconomico, edtGrupoEconomico.Text) ;

end;

procedure TfrmConsultaPedComAberTerceiro.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TfrmConsultaPedComAberTerceiro.edtTerceiroKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPedComAberTerceiro.edtGrupoEconomicoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            edtGrupoEconomico.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, edtGrupoEconomico.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPedComAberTerceiro.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPedComAberTerceiro.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') and (DBEdit2.Text = '') and (DBEdit3.Text = '') and (Trim(edtPedido.Text) = '') then
  begin
      MensagemAlerta('Informe algum filtro para prosseguir com a consulta.') ;
      edtTerceiro.SetFocus;
      abort;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('nCdLoja').Value           := frmMenu.nCdLojaAtiva;
  qryResultado.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(edtTerceiro.Text) ;
  qryResultado.Parameters.ParamByName('nCdGrupoEconomico').Value := frmMenu.ConvInteiro(edtGrupoEconomico.Text) ;
  qryResultado.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(edtMarca.Text) ;
  qryResultado.Parameters.ParamByName('nCdPedido').Value         := frmMenu.ConvInteiro(edtPedido.Text) ;
  qryResultado.open ;

  if qryResultado.eof then
      MensagemAlerta('Nenhum pedido encontrado para o crit�rio utilizado.') ;


end;

procedure TfrmConsultaPedComAberTerceiro.DBGridEh1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaPedComAberTerceiro_Itens ;
begin
  inherited;

  if qryResultado.eof then
      exit ;

  objForm := TfrmConsultaPedComAberTerceiro_Itens.Create( Self ) ;

  PosicionaQuery(objForm.qryItens , qryResultadonCdPedido.AsString) ;

  showForm( objForm , TRUE ) ;
  
end;

procedure TfrmConsultaPedComAberTerceiro.edtTerceiroChange(
  Sender: TObject);
begin
  inherited;

  qryResultado.Close;
end;

procedure TfrmConsultaPedComAberTerceiro.edtGrupoEconomicoChange(
  Sender: TObject);
begin
  inherited;
  qryResultado.Close;

end;

procedure TfrmConsultaPedComAberTerceiro.edtMarcaChange(Sender: TObject);
begin
  inherited;
  qryResultado.Close;

end;

procedure TfrmConsultaPedComAberTerceiro.edtPedidoChange(Sender: TObject);
begin
  inherited;
  qryResultado.Close;

end;

initialization
    RegisterClass(TfrmConsultaPedComAberTerceiro) ;
    
end.
