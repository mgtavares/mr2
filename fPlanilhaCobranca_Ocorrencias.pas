unit fPlanilhaCobranca_Ocorrencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, StdCtrls, DBCtrls, Menus, DBGridEhGrouping;

type
  TfrmPlanilhaCobranca_Ocorrencias = class(TfrmProcesso_Padrao)
    qryFollowUp: TADOQuery;
    DataSource1: TDataSource;
    qryFollowUpnCdFollowUp: TAutoIncField;
    qryFollowUpnCdPedido: TIntegerField;
    qryFollowUpnCdTitulo: TIntegerField;
    qryFollowUpdDtFollowUp: TDateTimeField;
    qryFollowUpnCdUsuario: TIntegerField;
    qryFollowUpcOcorrenciaResum: TStringField;
    qryFollowUpcOcorrencia: TMemoField;
    qryFollowUpdDtProxAcao: TDateTimeField;
    qryFollowUpcFlgOK: TIntegerField;
    qryFollowUpnCdTerceiro: TIntegerField;
    DBGridEh1: TDBGridEh;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryFollowUpcNmUsuario: TStringField;
    DBMemo1: TDBMemo;
    PopupMenu1: TPopupMenu;
    ExcluirOcorrencia1: TMenuItem;
    procedure ToolButton1Click(Sender: TObject);
    procedure qryFollowUpCalcFields(DataSet: TDataSet);
    procedure ExcluirOcorrencia1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro : integer ;
  end;

var
  frmPlanilhaCobranca_Ocorrencias: TfrmPlanilhaCobranca_Ocorrencias;

implementation

uses fNovo_FollowUp, fMenu;

{$R *.dfm}

procedure TfrmPlanilhaCobranca_Ocorrencias.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  frmNovo_FollowUp.qryFollow.Close ;
  frmNovo_FollowUp.qryFollow.Open ;
  frmNovo_FollowUp.qryFollow.Insert ;
  frmNovo_FollowUp.qryFollownCdTerceiro.Value := nCdTerceiro ;
  frmNovo_FollowUp.qryFollownCdUsuario.Value  := frmMenu.nCdUsuarioLogado ;
  frmNovo_FollowUp.qryFollowdDtFollowUp.Value := Now() ;
  frmNovo_FollowUp.ShowModal ;

  qryFollowUp.Close ;
  qryFollowUp.Open  ;

end;

procedure TfrmPlanilhaCobranca_Ocorrencias.qryFollowUpCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuario, qryFollowUpnCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryFollowUpcNmUsuario.Value := qryUsuariocNmUsuario.Value ;
      
end;

procedure TfrmPlanilhaCobranca_Ocorrencias.ExcluirOcorrencia1Click(
  Sender: TObject);
begin
  inherited;

  if not qryFollowUp.Eof then
  begin
      if (qryFollowUpnCdUsuario.Value <> frmMenu.nCdUsuarioLogado) then
      begin
          MensagemAlerta('Somente o usu�rio que incluiu pode excluir a ocorr�ncia.');
          exit ;
      end ;

      case MessageDlg('Confirma o cancelamento do saldo deste item?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          qryFollowUp.Delete;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryFollowUp.Close ;
      qryFollowUp.Open ;


  end ;

end;

end.
