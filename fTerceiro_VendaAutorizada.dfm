inherited frmTerceiro_VendaAutorizada: TfrmTerceiro_VendaAutorizada
  Left = 78
  Top = 176
  Width = 964
  Height = 520
  Caption = 'Vendas Autorizadas'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 477
    Width = 948
    Height = 7
  end
  inherited ToolBar1: TToolBar
    Width = 948
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 948
    Height = 448
    ActivePage = cxTabSheet1
    Align = alTop
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 444
    ClientRectLeft = 4
    ClientRectRight = 944
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Vendas Autorizadas'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 940
        Height = 420
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              Column = cxGrid1DBTableView1nValPedidoVendaAberto
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGrid1DBTableView1nValPedidoVendaAberto
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValPedidoVendaAberto'
              Column = cxGrid1DBTableView1nValPedidoVendaAberto
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
            DataBinding.FieldName = 'nCdPedido'
            Width = 93
          end
          object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
            DataBinding.FieldName = 'dDtPedido'
          end
          object cxGrid1DBTableView1Terceiro: TcxGridDBColumn
            DataBinding.FieldName = 'Terceiro'
          end
          object cxGrid1DBTableView1nValPedidoVendaAberto: TcxGridDBColumn
            DataBinding.FieldName = 'nValPedidoVendaAberto'
          end
          object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTipoPedido'
            Width = 191
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object qryVendaAutorizada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 14
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  Pedido.nCdPedido'
      '       ,Pedido.dDtPedido '
      
        '       ,(SELECT cNmTerceiro FROM Terceiro WHERE nCdTerceiro = Pe' +
        'dido.nCdTerceiro) as Terceiro'
      '       ,TipoPedido.cNmTipoPedido          '
      
        ' '#9'   ,IsNull((SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9'  '#9#9#9'  FROM ItemPedido (NOLOCK)'
      #9#9#9' '#9' WHERE ItemPedido.nCdPedido       = Pedido.nCdPedido'
      #9#9#9#9'   AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)'
      
        '                   AND (ItemPedido.nQtdePed - ItemPedido.nQtdeEx' +
        'pRec - ItemPedido.nQtdeCanc) > 0),0) as nValPedidoVendaAberto '
      '  FROM Pedido (NOLOCK)'
      
        ' INNER JOIN TipoPedido (NOLOCK) ON TipoPedido.nCdTipoPedido = Pe' +
        'dido.nCdTipoPedido'
      ' WHERE Pedido.nCdTerceiro     = :nCdTerceiro'
      '   AND Pedido.nCdTabStatusPed  IN (3,4,5)'
      '   AND TipoPedido.cGerarFinanc    = 1'
      '   AND TipoPedido.cFlgVenda       = 1'
      
        '   AND (SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtdeExpRec' +
        ' - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9'  '#9#9#9'  FROM ItemPedido (NOLOCK)'
      #9#9#9' '#9' WHERE ItemPedido.nCdPedido       = Pedido.nCdPedido'
      #9#9#9#9'   AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)) > 0')
    Left = 560
    Top = 128
    object qryVendaAutorizadanCdPedido: TIntegerField
      DisplayLabel = 'Nr. Pedido'
      FieldName = 'nCdPedido'
    end
    object qryVendaAutorizadadDtPedido: TDateTimeField
      DisplayLabel = 'Dt. Pedido'
      FieldName = 'dDtPedido'
    end
    object qryVendaAutorizadacNmTipoPedido: TStringField
      DisplayLabel = 'Tipo de Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryVendaAutorizadanValPedidoVendaAberto: TBCDField
      DisplayLabel = 'Valor em Aberto'
      FieldName = 'nValPedidoVendaAberto'
      ReadOnly = True
      DisplayFormat = '###,##0.00'
      Precision = 32
      Size = 10
    end
    object qryVendaAutorizadaTerceiro: TStringField
      FieldName = 'Terceiro'
      ReadOnly = True
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryVendaAutorizada
    Left = 608
    Top = 128
  end
end
