inherited frmAjusteEmpenho: TfrmAjusteEmpenho
  Left = 220
  Top = 173
  Caption = 'Ajuste de Empenho'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 52
    Top = 42
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 9
    Top = 66
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Estoque'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 58
    Top = 90
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedido'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [4]
    Left = 36
    Top = 114
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero OP'
    FocusControl = DBEdit2
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 164
    Top = 34
    Width = 337
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 164
    Top = 58
    Width = 337
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = DataSource2
    TabOrder = 2
  end
  object edtProduto: TER2LookupMaskEdit [8]
    Left = 96
    Top = 34
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object edtLocalEstoque: TER2LookupMaskEdit [9]
    Left = 96
    Top = 58
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object edtNumOP: TEdit [10]
    Left = 96
    Top = 106
    Width = 65
    Height = 21
    TabOrder = 6
  end
  object edtPedido: TMaskEdit [11]
    Left = 96
    Top = 82
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
  end
  object cxButton1: TcxButton [12]
    Left = 8
    Top = 138
    Width = 121
    Height = 33
    Caption = 'Exibir Empenhos'
    TabOrder = 7
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '              ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 56
    Top = 352
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '              ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 88
    Top = 352
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 72
    Top = 384
  end
  object DataSource2: TDataSource
    DataSet = qryLocalEstoque
    Left = 104
    Top = 384
  end
end
