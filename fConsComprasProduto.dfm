inherited frmConsComprasProduto: TfrmConsComprasProduto
  Left = 89
  Top = 131
  Caption = 'Consulta de Compras por Produto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 97
    Height = 367
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 68
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 48
      Top = 20
      Width = 38
      Height = 13
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Tag = 1
      Left = 10
      Top = 47
      Width = 76
      Height = 13
      Caption = 'Per'#237'odo Compra'
    end
    object Label6: TLabel
      Tag = 1
      Left = 180
      Top = 47
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object MaskEdit1: TMaskEdit
      Left = 96
      Top = 12
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit1Exit
      OnKeyDown = MaskEdit1KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 168
      Top = 12
      Width = 553
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource1
      TabOrder = 3
    end
    object MaskEdit2: TMaskEdit
      Left = 96
      Top = 39
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Left = 208
      Top = 39
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 97
    Width = 784
    Height = 367
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsResultado
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Dt. Pedido'
        DataBinding.FieldName = 'dDtPedido'
      end
      object cxGrid1DBTableView1dDtAutor: TcxGridDBColumn
        Caption = 'Dt. Autoriz.'
        DataBinding.FieldName = 'dDtAutor'
      end
      object cxGrid1DBTableView1dDtPrevEntIni: TcxGridDBColumn
        Caption = 'Dt. Entrega Ini.'
        DataBinding.FieldName = 'dDtPrevEntIni'
      end
      object cxGrid1DBTableView1dDtPrevEntFim: TcxGridDBColumn
        Caption = 'Dt. Entrega Fim'
        DataBinding.FieldName = 'dDtPrevEntFim'
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Fornecedor'
        DataBinding.FieldName = 'cNmTerceiro'
      end
      object cxGrid1DBTableView1nQtdePed: TcxGridDBColumn
        Caption = 'Qt. Pedida'
        DataBinding.FieldName = 'nQtdePed'
        Width = 92
      end
      object cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn
        Caption = 'Qt. Entregue'
        DataBinding.FieldName = 'nQtdeExpRec'
        Width = 103
      end
      object cxGrid1DBTableView1nSaldo: TcxGridDBColumn
        Caption = 'Saldo'
        DataBinding.FieldName = 'nSaldo'
      end
      object cxGrid1DBTableView1cNrPedTerceiro: TcxGridDBColumn
        Caption = 'Pedido do Fornecedor'
        DataBinding.FieldName = 'cNrPedTerceiro'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 224
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'SELECT Pedido.nCdPedido'
      '      ,dDtPedido'
      '      ,dDtAutor'
      '      ,dDtPrevEntIni'
      '      ,dDtPrevEntFim'
      '      ,cNmTerceiro'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,(nQtdePed - nQtdeExpRec - nQtdeCanc) nSaldo'
      '      ,cNrPedTerceiro'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido     ON Pedido.nCdPedido         = ItemP' +
        'edido.nCdPedido'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      ' WHERE ItemPedido.nCdProduto  = :nCdProduto'
      '   AND TipoPedido.cFlgCompra  = 1'
      '   AND Pedido.dDtAutor       IS NOT NULL'
      '   AND Pedido.nCdEmpresa      = :nCdEmpresa'
      
        '   AND Pedido.dDtPedido       >= Convert(DATETIME,:dDtInicial,10' +
        '3)'
      
        '   AND Pedido.dDtPedido       <  (Convert(DATETIME,:dDtFinal,103' +
        ')+1)'
      '   AND nQtdeExpRec            > 0'
      ' ORDER BY dDtPedido DESC')
    Left = 360
    Top = 392
    object qryResultadonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryResultadodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryResultadodDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryResultadodDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
    end
    object qryResultadodDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
    end
    object qryResultadocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryResultadonQtdePed: TBCDField
      FieldName = 'nQtdePed'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryResultadonQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryResultadonSaldo: TBCDField
      FieldName = 'nSaldo'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 14
    end
    object qryResultadocNrPedTerceiro: TStringField
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 424
    Top = 264
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 392
    Top = 400
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 368
    Top = 440
  end
end
