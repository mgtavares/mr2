inherited rptListaProdutoSimples: TrptListaProdutoSimples
  Left = 274
  Top = 175
  Width = 777
  Height = 296
  Caption = 'Listagem do Cadastro de Produtos - Resumida'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 761
    Height = 234
  end
  object Label1: TLabel [1]
    Left = 32
    Top = 48
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Produto'
  end
  object Label3: TLabel [2]
    Left = 11
    Top = 96
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo de Cadastro'
  end
  object Label6: TLabel [3]
    Left = 191
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [4]
    Left = 88
    Top = 72
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  inherited ToolBar1: TToolBar
    Width = 761
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [6]
    Left = 120
    Top = 40
    Width = 63
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit1Exit
    OnKeyDown = MaskEdit1KeyDown
  end
  object MaskEdit6: TMaskEdit [7]
    Left = 120
    Top = 88
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit7: TMaskEdit [8]
    Left = 216
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 185
    Top = 40
    Width = 401
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource1
    TabOrder = 5
  end
  object RadioGroup1: TRadioGroup [10]
    Left = 10
    Top = 117
    Width = 241
    Height = 49
    Caption = ' Status dos Produtos '
    Color = 13086366
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Ativo'
      'Inativo'
      'Todos')
    ParentColor = False
    TabOrder = 6
  end
  object rgTipoProd: TRadioGroup [11]
    Left = 258
    Top = 117
    Width = 487
    Height = 49
    Caption = ' Tipo Produto '
    Color = 13086366
    Columns = 5
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      '1 - Estrutur.'
      '2 - Intermed.'
      '3 - Final'
      '4 - Formulado')
    ParentColor = False
    TabOrder = 7
  end
  object MaskEdit2: TMaskEdit [12]
    Left = 120
    Top = 64
    Width = 63
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit2Exit
    OnKeyDown = MaskEdit2KeyDown
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 185
    Top = 64
    Width = 401
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource2
    TabOrder = 8
  end
  object rgProdWeb: TRadioGroup [14]
    Left = 10
    Top = 173
    Width = 241
    Height = 49
    Caption = ' Produto Web ? '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    ParentColor = False
    TabOrder = 9
    OnClick = rgProdWebClick
  end
  object rgModeloImp: TRadioGroup [15]
    Left = 258
    Top = 173
    Width = 257
    Height = 49
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 10
  end
  inherited ImageList1: TImageList
    Left = 600
    Top = 32
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 632
    Top = 32
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoProduto'
      ' WHERE nCdGrupoProduto = :nPK')
    Left = 632
    Top = 64
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryGrupoProdutocFlgExpPDV: TIntegerField
      FieldName = 'cFlgExpPDV'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryGrupoProduto
    Left = 664
    Top = 64
  end
  object DataSource2: TDataSource
    DataSet = qryMarca
    Left = 664
    Top = 32
  end
end
