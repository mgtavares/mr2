unit fFuncoesECF;

interface

uses
  ACBrECF, ACBrRFD, ACBrBase, ACBrDevice, ACBrECFClass, ACBrConsts,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, Menus, StdCtrls, cxButtons, DB, ADODB;

type
  TfrmFuncoesECF = class(TfrmProcesso_Padrao)
    mResp: TMemo;
    Label2: TLabel;
    MainMenu1: TMainMenu;
    Principal1: TMenuItem;
    Ativcar1: TMenuItem;
    Desativar1: TMenuItem;
    Testar1: TMenuItem;
    N2: TMenuItem;
    Sair1: TMenuItem;
    Variaveis1: TMenuItem;
    Equipamento1: TMenuItem;
    Estado1: TMenuItem;
    DataHora1: TMenuItem;
    NumECF1: TMenuItem;
    NumLoja1: TMenuItem;
    NSrie1: TMenuItem;
    NVerso1: TMenuItem;
    CNPJIE1: TMenuItem;
    IE1: TMenuItem;
    N19: TMenuItem;
    Aliquotas1: TMenuItem;
    AliquotasICMS1: TMenuItem;
    LerTotaisAliquotas1: TMenuItem;
    FormasdePagamento2: TMenuItem;
    FormasdePagamento1: TMenuItem;
    LerTotaisFormadePagamento1: TMenuItem;
    ComprovantesNaoFiscais1: TMenuItem;
    CarregaComprovantesNAOFiscais1: TMenuItem;
    LerTotaisComprovanetNaoFiscal1: TMenuItem;
    CarregaUnidadesdeMedida1: TMenuItem;
    Relatrios1: TMenuItem;
    LeituraX1: TMenuItem;
    ReduoZ1: TMenuItem;
    PularLinhas1: TMenuItem;
    CortaPapel1: TMenuItem;
    Utilitrios1: TMenuItem;
    ProgramaAliquota1: TMenuItem;
    ProgramaFormadePagamento1: TMenuItem;
    ProgramaUnidadeMedida1: TMenuItem;
    N13: TMenuItem;
    HorarioVerao1: TMenuItem;
    MudaArredondamento1: TMenuItem;
    ImpactoAgulhas1: TMenuItem;
    N7: TMenuItem;
    CorrigeEstadodeErro1: TMenuItem;
    N1: TMenuItem;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA: TADOStoredProc;
    SP_REGISTRO_ANALITICO_ECF_SINTEGRA: TADOStoredProc;
    qryUpdateMovtoECF: TADOQuery;
    MapaResumo1: TMenuItem;
    N3: TMenuItem;
    procedure Ativcar1Click(Sender: TObject);
    procedure Desativar1Click(Sender: TObject);
    procedure Testar1Click(Sender: TObject);
    procedure Estado1Click(Sender: TObject);
    procedure DataHora1Click(Sender: TObject);
    procedure NumECF1Click(Sender: TObject);
    procedure NumLoja1Click(Sender: TObject);
    procedure NSrie1Click(Sender: TObject);
    procedure NVerso1Click(Sender: TObject);
    procedure CNPJIE1Click(Sender: TObject);
    procedure IE1Click(Sender: TObject);
    procedure LerTotaisAliquotas1Click(Sender: TObject);
    procedure AliquotasICMS1Click(Sender: TObject);
    procedure FormasdePagamento1Click(Sender: TObject);
    procedure LerTotaisFormadePagamento1Click(Sender: TObject);
    procedure CarregaUnidadesdeMedida1Click(Sender: TObject);
    procedure LeituraX1Click(Sender: TObject);
    procedure ReduoZ1Click(Sender: TObject);
    procedure PularLinhas1Click(Sender: TObject);
    procedure CortaPapel1Click(Sender: TObject);
    procedure ProgramaAliquota1Click(Sender: TObject);
    procedure ProgramaFormadePagamento1Click(Sender: TObject);
    procedure HorarioVerao1Click(Sender: TObject);
    procedure ProgramaUnidadeMedida1Click(Sender: TObject);
    procedure MudaArredondamento1Click(Sender: TObject);
    procedure ImpactoAgulhas1Click(Sender: TObject);
    procedure CorrigeEstadodeErro1Click(Sender: TObject);
    procedure CarregaComprovantesNAOFiscais1Click(Sender: TObject);
    procedure LerTotaisComprovanetNaoFiscal1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure MapaResumo1Click(Sender: TObject);

  private
    { Private declarations }
    function GeraRegistroMestre : integer;
    procedure GeraRegistroAnalitico (nCdMovtoDiaECF : integer ;cCdSTAliq : String; nValAcumTotalParcial : Double);
  public
    { Public declarations }
  end;

const
    Estados : array[TACBrECFEstado] of string =
    ('N�o Inicializada', 'Desconhecido', 'Livre', 'Venda',
    'Pagamento', 'Relat�rio', 'Bloqueada', 'Requer Z', 'Requer X', 'Nao Fiscal' );

var
  frmFuncoesECF: TfrmFuncoesECF;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmFuncoesECF.Ativcar1Click(Sender: TObject);
begin
  inherited;
  try

    if not frmMenu.ACBrECF1.AcharECF(true,False) then
    begin
       MessageDlg('Nenhum ECF encontrado.',mtInformation,[mbOk],0) ;
       exit ;
    end ;

    frmMenu.ACBrECF1.Ativar ;

    mResp.Lines.Add( 'Ativar' );

  finally
    frmMenu.cModeloECF := frmMenu.ACBrECF1.ModeloStr;
  end ;

end;

procedure TfrmFuncoesECF.Desativar1Click(Sender: TObject);
begin
  inherited;
  frmMenu.ACBrECF1.Desativar ;
  mResp.Lines.Add( 'Desativar' );

end;

procedure TfrmFuncoesECF.Testar1Click(Sender: TObject);
begin
  inherited;
  frmMenu.ACBrECF1.TestarDialog ;

end;

procedure TfrmFuncoesECF.Estado1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'Estado: '+ Estados[ frmMenu.ACBrECF1.Estado ] );

end;

procedure TfrmFuncoesECF.DataHora1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'Data/Hora: '+ DateTimeToStr( frmMenu.ACBrECF1.DataHora ) ) ;

end;

procedure TfrmFuncoesECF.NumECF1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'N.ECF: ('+ frmMenu.ACBrECF1.NumECF+')' );

end;

procedure TfrmFuncoesECF.NumLoja1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'NUM Loja: ('+ frmMenu.ACBrECF1.NumLoja+')' );

end;

procedure TfrmFuncoesECF.NSrie1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'N.S�rie: ('+ frmMenu.ACBrECF1.NumSerie+')' );

end;

procedure TfrmFuncoesECF.NVerso1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'N.Vers�o: '+ frmMenu.ACBrECF1.NumVersao );

end;

procedure TfrmFuncoesECF.CNPJIE1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'CNPJ: ('+ frmMenu.ACBrECF1.CNPJ+')' );

end;

procedure TfrmFuncoesECF.IE1Click(Sender: TObject);
begin
  inherited;
  mResp.Lines.Add( 'IE: ('+ frmMenu.ACBrECF1.IE+')' );

end;

procedure TfrmFuncoesECF.LerTotaisAliquotas1Click(Sender: TObject);
Var A : Integer ;
begin
  inherited;
  frmMenu.ACBrECF1.LerTotaisAliquota ;

  for A := 0 to frmMenu.ACBrECF1.Aliquotas.Count -1 do
  begin
     mResp.Lines.Add( 'Aliquota: '+frmMenu.ACBrECF1.Aliquotas[A].Indice +' - '+
                      FloatToStr( frmMenu.ACBrECF1.Aliquotas[A].Aliquota ) + ' Tipo: '+
                      frmMenu.ACBrECF1.Aliquotas[A].Tipo+ ' -> '+
                      FloatToStr( frmMenu.ACBrECF1.Aliquotas[A].Total ) );
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.AliquotasICMS1Click(Sender: TObject);
Var A : Integer ;
begin
  inherited;
  frmMenu.ACBrECF1.CarregaAliquotas ;

  for A := 0 to frmMenu.ACBrECF1.Aliquotas.Count -1 do
  begin
     mResp.Lines.Add( 'Aliquota: '+ IntToStr(frmMenu.ACBrECF1.Aliquotas[A].Sequencia) +
                      ' Indice: '+frmMenu.ACBrECF1.Aliquotas[A].Indice +' -> '+
                      FloatToStr( frmMenu.ACBrECF1.Aliquotas[A].Aliquota ) + ' Tipo: '+
                      frmMenu.ACBrECF1.Aliquotas[A].Tipo );
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.FormasdePagamento1Click(Sender: TObject);
var A : Integer ;
begin
  frmMenu.ACBrECF1.CarregaFormasPagamento ;

  for A := 0 to frmMenu.ACBrECF1.FormasPagamento.Count -1 do
  begin
     if frmMenu.ACBrECF1.FormasPagamento[A].Descricao <> '' then
        mResp.Lines.Add( 'Forma Pagto: '+frmMenu.ACBrECF1.FormasPagamento[A].Indice+' -> '+ frmMenu.ACBrECF1.FormasPagamento[A].Descricao);
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.LerTotaisFormadePagamento1Click(Sender: TObject);
var A : Integer ;
begin
  frmMenu.ACBrECF1.LerTotaisFormaPagamento ;

  for A := 0 to frmMenu.ACBrECF1.FormasPagamento.Count -1 do
  begin
     if frmMenu.ACBrECF1.FormasPagamento[A].Descricao <> '' then
        mResp.Lines.Add( 'Forma Pagto: '+frmMenu.ACBrECF1.FormasPagamento[A].Indice+' - '+
           frmMenu.ACBrECF1.FormasPagamento[A].Descricao+'  -> '+
           FloatToStr(frmMenu.ACBrECF1.FormasPagamento[A].Total)) ;
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.CarregaUnidadesdeMedida1Click(Sender: TObject);
var
   A: Integer;
begin
  frmMenu.ACBrECF1.CarregaUnidadesMedida ;

  for A := 0 to frmMenu.ACBrECF1.UnidadesMedida.Count -1 do
  begin
     if frmMenu.ACBrECF1.UnidadesMedida[A].Descricao <> '' then
        mResp.Lines.Add( 'Unid Medida: '+frmMenu.ACBrECF1.UnidadesMedida[A].Indice+' -> '+
           frmMenu.ACBrECF1.UnidadesMedida[A].Descricao);
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.LeituraX1Click(Sender: TObject);
begin
  inherited;
  frmMenu.ACBrECF1.LeituraX ;
  mResp.Lines.Add( 'Leitura X');

end;

procedure TfrmFuncoesECF.ReduoZ1Click(Sender: TObject);
Var
    Resp           : TModalResult ;
    nCdMovtoDiaECF : integer;
    n              : integer;
begin

  if frmMenu.ACBrECF1.Estado <> estRequerZ then
  begin
     if MessageDlg('Esta opera��o ir� bloquear a impressora. Confirma ?',mtConfirmation,mbYesNoCancel,0) <> mrYes then
        exit ;

      frmMenu.Connection.BeginTrans;

      try
          nCdMovtoDiaECF := GeraRegistroMestre;

          //Registra as aliquotas praticadas

          frmMenu.ACBrECF1.LerTotaisAliquota;

          for n := 0 to frmMenu.ACBrECF1.Aliquotas.Count - 1 do
              if (frmMenu.ACBrECF1.Aliquotas.Objects[n].Total > 0) then
                  GeraRegistroAnalitico(nCdMovtoDiaECF,frmMenu.ZeroEsquerda(FloatToStr(frmMenu.ACBrECF1.Aliquotas.Objects[n].Aliquota * 100),4),frmMenu.ACBrECF1.Aliquotas.Objects[n].Total);

          if (frmMenu.ACBrECF1.TotalSubstituicaoTributaria > 0) then
              GeraRegistroAnalitico(nCdMovtoDiaECF,'F',frmMenu.ACBrECF1.TotalSubstituicaoTributaria);

          if (frmMenu.ACBrECF1.TotalIsencao > 0) then
              GeraRegistroAnalitico(nCdMovtoDiaECF,'I',frmMenu.ACBrECF1.TotalIsencao);

          if (frmMenu.ACBrECF1.TotalNaoTributado > 0) then
              GeraRegistroAnalitico(nCdMovtoDiaECF,'N',frmMenu.ACBrECF1.TotalNaoTributado);

          if (frmMenu.ACBrECF1.TotalCancelamentos > 0) then
              GeraRegistroAnalitico(nCdMovtoDiaECF,'CANC',frmMenu.ACBrECF1.TotalCancelamentos);

          if (frmMenu.ACBrECF1.TotalDescontos > 0) then
              GeraRegistroAnalitico(nCdMovtoDiaECF,'DESC',frmMenu.ACBrECF1.TotalDescontos);

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no Processamento.');
          raise;
      end;

      frmMenu.Connection.CommitTrans;

      frmMenu.ACBrECF1.ReducaoZ( ) ;

      qryUpdateMovtoECF.Close;
      qryUpdateMovtoECF.Parameters.ParamByName('iNrOrdemOperInicial').Value := frmMenu.ACBrECF1.NumCOOInicial;
      qryUpdateMovtoECF.Parameters.ParamByName('iNrOrdemOperFinal').Value   := frmMenu.ACBrECF1.NumCOO;
      qryUpdateMovtoECF.Parameters.ParamByName('iNrReducaoZ').Value         := frmMenu.ACBrECF1.NumCRZ;
      qryUpdateMovtoECF.Parameters.ParamByName('iNrReinicioOper').Value     := frmMenu.ACBrECF1.NumCRO;
      qryUpdateMovtoECF.Parameters.ParamByName('nCdMovtoDiaECF').Value      := nCdMovtoDiaECF;
      qryUpdateMovtoECF.ExecSQL;

      mResp.Lines.Add('Redu�ao Z');
  end ;
  
end;

procedure TfrmFuncoesECF.PularLinhas1Click(Sender: TObject);
Var Linhas : String ;
begin
  Linhas := IntToStr( frmMenu.ACBrECF1.LinhasEntreCupons ) ;
  if not InputQuery('Pular Linhas',
                    'Digite o Numero de Linhas a Pular', Linhas ) then
     exit ;

  frmMenu.ACBrECF1.PulaLinhas( StrToIntDef(Linhas,0) ) ;

end;

procedure TfrmFuncoesECF.CortaPapel1Click(Sender: TObject);
 Var Resp : TModalResult ;
begin
  Resp := MessageDlg('Corte Parcial ?',mtConfirmation,mbYesNoCancel,0) ;

  if Resp = mrCancel then
     exit ;

  frmMenu.ACBrECF1.CortaPapel( (Resp = mrYes) );

end;

procedure TfrmFuncoesECF.ProgramaAliquota1Click(Sender: TObject);
Var cAliq : String ;
    nAliq : Double ;
    Tipo  : Char ;
    Resp  : TModalResult ;
begin
  cAliq := '18,00' ;

  if not InputQuery('Programa�ao de Aliquotas',
                    'Entre com o valor da Aliquota:', cAliq ) then
     exit ;

  cAliq := StringReplace(StringReplace(cAliq,'.',DecimalSeparator,[]),
                                             ',',DecimalSeparator,[]) ;
  nAliq := StrToFloatDef(cAliq,0) ;
  if nAliq = 0 then
     exit ;

  Resp := MessageDlg('Aliquota do ICMS ?'+sLineBreak+'SIM = ICMS, NAO = ISS',
                mtConfirmation,mbYesNoCancel,0) ;
  case Resp of
    mrCancel : exit ;
    mrYes    : Tipo := 'T' ;
  else ;
    Tipo := 'S' ;
  end;

  if MessageDlg('A aliquota: ['+FloatToStr(nAliq)+'] do Tipo: ['+Tipo+
                '] ser� programada.'+sLineBreak+sLineBreak+
                'Cuidado !! A programa��o de Aliquotas � irreversivel'+sLineBreak+
                'Confirma a opera��o ?',mtConfirmation,mbYesNoCancel,0) <> mrYes then
     exit ;

  frmMenu.ACBrECF1.ProgramaAliquota(nAliq,Tipo);
  AliquotasICMS1Click(Sender);

end;

procedure TfrmFuncoesECF.ProgramaFormadePagamento1Click(Sender: TObject);
Var cDescricao : String ;
    Vinculado  : Boolean ;
    Resp       : TModalResult ;
begin
  cDescricao := 'CARTAO' ;
  Vinculado  := true ;

  if not InputQuery('Programa�ao de Formas de Pagamento (FPG)',
                    'Entre com a Descri�ao:', cDescricao ) then
     exit ;

  if not (frmMenu.ACBrECF1.Modelo in [ecfBematech, ecfNaoFiscal, ecfMecaf]) then
  begin
     Resp := MessageDlg('Permite Vinculado nessa Forma de Pagamento ?',
                   mtConfirmation,mbYesNoCancel,0) ;
     if Resp = mrCancel then
        exit
     else
        Vinculado := (Resp = mrYes) ;
  end ;

  if MessageDlg('A Forma de Pagamento: ['+cDescricao+'] '+
                'ser� programada.'+sLineBreak+sLineBreak+
                'Cuidado !! A programa��o de Formas de Pagamento � irreversivel'+sLineBreak+
                'Confirma a opera��o ?',mtConfirmation,mbYesNoCancel,0) <> mrYes then
     exit ;

  frmMenu.ACBrECF1.ProgramaFormaPagamento(cDescricao,Vinculado);
  LerTotaisFormadePagamento1Click(Sender);

end;

procedure TfrmFuncoesECF.HorarioVerao1Click(Sender: TObject);
begin
  inherited;
  frmMenu.ACBrECF1.MudaHorarioVerao ;
  mResp.Lines.Add( 'MudaHorarioVerao');

end;

procedure TfrmFuncoesECF.ProgramaUnidadeMedida1Click(Sender: TObject);
var
   um:String;
begin
  if not InputQuery('Programa�ao de Unidades de Medida',
                    'Entre com a Descri��o da Unidade de Medida:', um ) then
     exit ;

  if MessageDlg('A Unidade de Medida: ['+um+'] ser� programada.'+sLineBreak+sLineBreak+
                'Cuidado a programa��o de Unidades de Medida � irreversivel'+sLineBreak+
                'Confirma a opera��o ?',mtConfirmation,mbYesNoCancel,0) <> mrYes then
     exit ;

  frmMenu.ACBrECF1.ProgramaUnidadeMedida( um );

end;

procedure TfrmFuncoesECF.MudaArredondamento1Click(Sender: TObject);
Var Resp : TModalResult ;
begin
  Resp := MessageDlg('Arredondar ?',mtConfirmation,mbYesNoCancel,0) ;
  if Resp <> mrCancel then
  begin
    frmMenu.ACBrECF1.MudaArredondamento( (Resp = mrYes) ) ;
    mResp.Lines.Add( 'MudaArredondamento');
  end ;

end;

procedure TfrmFuncoesECF.ImpactoAgulhas1Click(Sender: TObject);
begin
  inherited;
  frmMenu.ACBrECF1.ImpactoAgulhas ;
  mResp.Lines.Add( 'ImpactoAgulhas');

end;

procedure TfrmFuncoesECF.CorrigeEstadodeErro1Click(Sender: TObject);
begin
  frmMenu.ACBrECF1.CorrigeEstadoErro ;
end;

procedure TfrmFuncoesECF.CarregaComprovantesNAOFiscais1Click(
  Sender: TObject);
var A : Integer ;
begin
  frmMenu.ACBrECF1.CarregaComprovantesNaoFiscais ;

  for A := 0 to frmMenu.ACBrECF1.ComprovantesNaoFiscais.Count -1 do
  begin
     if frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Descricao <> '' then
        mResp.Lines.Add( 'CNF: '+frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Indice+' -> '+
           frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Descricao +
           ' - FPG associada: '+frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].FormaPagamento);
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.LerTotaisComprovanetNaoFiscal1Click(
  Sender: TObject);
var A : Integer ;
begin
  frmMenu.ACBrECF1.LerTotaisComprovanteNaoFiscal ;

  for A := 0 to frmMenu.ACBrECF1.ComprovantesNaoFiscais.Count -1 do
  begin
     if frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Descricao <> '' then
        mResp.Lines.Add( 'CNF: '+frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Indice+' - '+
           frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Descricao+' ('+
           IntToStr(frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Contador)+') -> '+
           FloatToStr(frmMenu.ACBrECF1.ComprovantesNaoFiscais[A].Total)) ;
  end ;
  mResp.Lines.Add('---------------------------------');

end;

procedure TfrmFuncoesECF.Sair1Click(Sender: TObject);
begin
  inherited;
  close;
end;

function TfrmFuncoesECF.GeraRegistroMestre : integer;
begin

    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Close;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@nCdEmpresa').Value          := frmMenu.nCdEmpresaAtiva;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@nCdLoja').Value             := frmMenu.nCdLojaAtiva;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@cCdNumSerieECF').Value      := frmMenu.ACBrECF1.NumSerie;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@iNrECF').Value              := frmMenu.ACBrECF1.NumECF;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@cModelo').Value             := '2D';
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@iNrOrdemOperInicial').Value := 0 ;//frmMenu.ACBrECF1.NumCOOInicial;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@iNrOrdemOperFinal').Value   := 0 ;//frmMenu.ACBrECF1.NumCOO;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@iNrReducaoZ').Value         := 0 ;//frmMenu.ACBrECF1.NumCRZ;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@iNrReinicioOper').Value     := 0 ;//frmMenu.ACBrECF1.NumCRO;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@nValVendaBruta').Value      := frmMenu.ACBrECF1.VendaBruta;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@nValTotalGeral').Value      := frmMenu.ACBrECF1.GrandeTotal;
    SP_REGISTRO_MESTRE_ECF_SINTEGRA.ExecProc;

    Result := SP_REGISTRO_MESTRE_ECF_SINTEGRA.Parameters.ParamByName('@nCdMovtoDiaECF').Value ;

end;

procedure TfrmFuncoesECF.GeraRegistroAnalitico(nCdMovtoDiaECF: integer; cCdSTAliq: String;
  nValAcumTotalParcial: Double);
begin
        SP_REGISTRO_ANALITICO_ECF_SINTEGRA.Close;
        SP_REGISTRO_ANALITICO_ECF_SINTEGRA.Parameters.ParamByName('@nCdMovtoDiaECF').Value       := nCdMovtoDiaECF;
        SP_REGISTRO_ANALITICO_ECF_SINTEGRA.Parameters.ParamByName('@cCdSTAliq').Value            := cCdSTAliq;
        SP_REGISTRO_ANALITICO_ECF_SINTEGRA.Parameters.ParamByName('@nValAcumTotalParcial').Value := nValAcumTotalParcial;
        SP_REGISTRO_ANALITICO_ECF_SINTEGRA.ExecProc;
end;

procedure TfrmFuncoesECF.MapaResumo1Click(Sender: TObject);
var
  strRelatorio : TStrings;
begin
  inherited;

  mResp.Lines.Add('Recuperando informa��es da ECF...');

  // Exibe o relat�rio no Memo
  mResp.Lines.Add('---------------------------------' + sLineBreak + frmMenu.ACBrECF1.DadosReducaoZ);

  strRelatorio := TStringList.Create;

  // Recupera o relat�rio e insere o Pipe para quebra de linha na ECF
  strRelatorio.Text := StringReplace(mResp.Text,sLineBreak,'|',[rfReplaceAll,rfIgnoreCase]);

  // ShowMessage(strRelatorio.Text);

  frmMenu.ACBrECF1.AbreRelatorioGerencial;

  frmMenu.ACBrECF1.RelatorioGerencial(strRelatorio,1);

  frmMenu.ACBrECF1.FechaRelatorio;

  FreeAndNil(strRelatorio);
end;

end.
