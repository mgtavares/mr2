inherited frmRamoAtividade: TfrmRamoAtividade
  Left = 28
  Top = 109
  Caption = 'Ramo de Atividade'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 66
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ramo de Atividade'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 112
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdRamoAtividade'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 112
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmRamoAtividade'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RamoAtividade'
      'WHERE nCdRamoAtividade = :nPK')
    object qryMasternCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryMastercNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
  end
end
