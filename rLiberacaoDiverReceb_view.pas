unit rLiberacaoDiverReceb_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls;

type
  TrptLiberacaoDiverReceb_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO: TADOStoredProc;
    QRBand1: TQRBand;
    lblTitle: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    lblfiltros: TQRLabel;
    QRGroup1: TQRGroup;
    bandDetail: TQRBand;
    bandFooter: TQRBand;
    QRDBText1: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdRecebimento: TIntegerField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdEmpresa: TIntegerField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdLoja: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTerceiro: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdUsuarioFech: TIntegerField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmUsuarioFech: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtFech: TDateTimeField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTabStatusReceb: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTipoReceb: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtReceb: TDateTimeField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNrDocto: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcSerieDocto: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcModeloNF: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtDocto: TDateTimeField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdProduto: TIntegerField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmItem: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTabTipoDiverg: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdUsuario: TIntegerField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmUsuario: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtAutorizacao: TDateTimeField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcAutorizaAut: TStringField;
    SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcOBS: TStringField;
    QRLabel15: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape2: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptLiberacaoDiverReceb_view: TrptLiberacaoDiverReceb_view;

implementation

{$R *.dfm}

uses
  fMenu;

end.
