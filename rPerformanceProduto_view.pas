unit rPerformanceProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPerformanceProduto_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_PERFORMANCE_PRODUTO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel11: TQRLabel;
    SPREL_PERFORMANCE_PRODUTOnCdProduto: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOcReferencia: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmMarca: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmProduto: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmDepartamento: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmCategoria: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmSubCategoria: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmSegmento: TStringField;
    SPREL_PERFORMANCE_PRODUTOnCdGrade: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOdDtUltReceb: TDateTimeField;
    SPREL_PERFORMANCE_PRODUTOcTituloGrade: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeEI: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeMR: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeMV: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeEF: TStringField;
    SPREL_PERFORMANCE_PRODUTOnSaldoEI: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoMR: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoMV: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoEF: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnPerformance: TFloatField;
    SPREL_PERFORMANCE_PRODUTOnCdTerceiroComprador: TIntegerField;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel2: TQRLabel;
    QRGroup2: TQRGroup;
    QRDBText5: TQRDBText;
    QRDBText25: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel37: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel5: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptPerformanceProduto_view: TrptPerformanceProduto_view;

implementation

{$R *.dfm}

procedure TrptPerformanceProduto_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
