unit fManutencaoVales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmManutencaoVales = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    qryMasternCdVale: TAutoIncField;
    qryMastercCodigo: TStringField;
    qryMastercNmTabTipoVale: TStringField;
    qryMastercNmLoja: TStringField;
    qryMasterdDtVale: TDateTimeField;
    qryMasternValVale: TBCDField;
    qryMasternSaldoVale: TBCDField;
    qryMasterdDtBaixa: TDateTimeField;
    qryMasterdDtEstorno: TDateTimeField;
    qryMastercFlgLiqVenda: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdLanctoFinBaixa: TIntegerField;
    qryMasternCdContaBaixa: TStringField;
    qryMastercNmLojaBaixa: TStringField;
    qryMasternCdLanctoFinRecompra: TIntegerField;
    qryMasternCdContaRecompra: TStringField;
    qryMastercNmLojaRecompra: TStringField;
    qryMasterdDtRecompra: TDateTimeField;
    DBEdit1: TDBEdit;
    DBEdit14: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutencaoVales: TfrmManutencaoVales;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmManutencaoVales.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'VALE' ;
  nCdTabelaSistema  := 198 ;
  nCdConsultaPadrao := 704 ;
  bCodigoAutomatico := False ;
end;

procedure TfrmManutencaoVales.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternSaldoVale.Value > qryMasternValVale.Value) then
  begin
      MensagemAlerta('O Saldo n�o pode ser superior ao valor do vale.');
      abort;
  end;

  if (qryMasternSaldoVale.Value < 0) then
  begin
      MensagemAlerta('O Saldo n�o pode ser inferior a zero.');
      abort;
  end;

  inherited;

end;

initialization
    RegisterClass(TfrmManutencaoVales);

end.
