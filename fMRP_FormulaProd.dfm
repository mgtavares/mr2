inherited frmMRP_FormulaProd: TfrmMRP_FormulaProd
  Left = 156
  Top = 165
  Width = 764
  BorderIcons = [biSystemMenu]
  Caption = 'Bill of Material (BOM)'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 748
  end
  inherited ToolBar1: TToolBar
    Width = 748
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 748
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdProduto'
        Width = 91
      end
      object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
        Caption = 'Descri'#231#227'o'
        DataBinding.FieldName = 'cNmProduto'
        Width = 565
      end
      object cxGrid1DBTableView1nQtde: TcxGridDBColumn
        DataBinding.FieldName = 'nQtde'
        Visible = False
      end
      object cxGrid1DBTableView1cFornecHom: TcxGridDBColumn
        Caption = 'Fornecedor Homologado'
        DataBinding.FieldName = 'cFornecHom'
        Width = 66
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryFormula: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      ''
      'Set @nCdProduto = :nPK'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'SELECT Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,nQtde'
      
        '      ,CASE WHEN EXISTS(SELECT 1 FROM ProdutoFornecedor WHERE Pr' +
        'odutoFornecedor.nCdProduto = FormulaProduto.nCdProduto) THEN '#39'Si' +
        'm'#39
      '            ELSE '#39'N'#227'o'#39
      '       END cFornecHom'
      '  FROM FormulaProduto'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = FormulaProduto' +
        '.nCdProduto'
      ' WHERE FormulaProduto.nCdProdutoPai = @nCdProduto'
      'UNION'
      'SELECT Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,nQtde'
      
        '      ,CASE WHEN EXISTS(SELECT 1 FROM ProdutoFornecedor WHERE Pr' +
        'odutoFornecedor.nCdProduto = FormulaProduto.nCdProduto) THEN '#39'Si' +
        'm'#39
      '            ELSE '#39'N'#227'o'#39
      '       END cFornecHom'
      '  FROM FormulaProduto'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = FormulaProduto' +
        '.nCdProduto'
      ' WHERE FormulaProduto.nCdProdutoPai = @nCdProdutoPai')
    Left = 352
    Top = 168
    object qryFormulanCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryFormulacNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryFormulanQtde: TBCDField
      DisplayLabel = 'Produto|Quantidade'
      FieldName = 'nQtde'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryFormulacFornecHom: TStringField
      DisplayLabel = 'Fornecedor|Homologado'
      FieldName = 'cFornecHom'
      ReadOnly = True
      Size = 3
    end
  end
  object DataSource1: TDataSource
    DataSet = qryFormula
    Left = 384
    Top = 240
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 144
    object FornecedoresHomologados1: TMenuItem
      Caption = 'Fornecedores Homologados'
      OnClick = FornecedoresHomologados1Click
    end
  end
end
