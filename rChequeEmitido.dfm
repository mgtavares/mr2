inherited rptChequeEmitido: TrptChequeEmitido
  Left = 68
  Top = 129
  Caption = 'Relat'#243'rio de Cheque Emitido'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 33
    Top = 72
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria'
  end
  object Label3: TLabel [3]
    Left = 29
    Top = 96
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Emiss'#227'o'
  end
  object Label6: TLabel [4]
    Left = 191
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 12
    Top = 120
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Vencimento'
  end
  object Label4: TLabel [6]
    Left = 191
    Top = 120
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    TabOrder = 8
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 9
  end
  object DBEdit3: TDBEdit [9]
    Tag = 1
    Left = 256
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 10
  end
  object edtDtEmissaoIni: TMaskEdit [10]
    Left = 112
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtDtEmissaoFim: TMaskEdit [11]
    Left = 211
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtEmpresa: TMaskEdit [12]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtEmpresaExit
    OnKeyDown = edtEmpresaKeyDown
  end
  object edtContaBancaria: TMaskEdit [13]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtContaBancariaExit
    OnKeyDown = edtContaBancariaKeyDown
  end
  object edtDtVenctoIni: TMaskEdit [14]
    Left = 112
    Top = 112
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object edtDtVenctoFim: TMaskEdit [15]
    Left = 211
    Top = 112
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 184
    Top = 64
    Width = 69
    Height = 21
    DataField = 'nCdBanco'
    DataSource = DataSource6
    TabOrder = 11
  end
  object DBEdit4: TDBEdit [17]
    Tag = 1
    Left = 256
    Top = 64
    Width = 65
    Height = 21
    DataField = 'cAgencia'
    DataSource = DataSource6
    TabOrder = 12
  end
  object DBEdit5: TDBEdit [18]
    Tag = 1
    Left = 328
    Top = 64
    Width = 200
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource6
    TabOrder = 13
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 536
    Top = 64
    Width = 374
    Height = 21
    DataField = 'cNmTitular'
    DataSource = DataSource6
    TabOrder = 14
  end
  object RadioGroup1: TRadioGroup [20]
    Left = 112
    Top = 144
    Width = 185
    Height = 105
    Caption = 'Situacao'
    ItemIndex = 2
    Items.Strings = (
      'Aberto'
      'Compensado'
      'Todos')
    TabOrder = 6
  end
  object RadioGroup2: TRadioGroup [21]
    Left = 304
    Top = 144
    Width = 185
    Height = 105
    Caption = 'Ordena'#231#227'o'
    ItemIndex = 0
    Items.Strings = (
      'N'#250'mero de Cheque'
      'Data de Emiss'#227'o'
      'Data de Vencimento')
    TabOrder = 7
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 336
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdEmpresa'
      '      ,nCdBanco'
      '      ,cAgencia'
      '      ,nCdConta'
      '      ,cNmTitular '
      '  FROM ContaBancaria'
      ' WHERE cFlgEmiteCheque  = 1'
      '   AND nCdEmpresa       = :nCdEmpresa'
      '   AND nCdContaBancaria = :nPK')
    Left = 336
    Top = 288
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryContaBancaria
    Left = 496
    Top = 272
  end
end
