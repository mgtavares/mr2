unit fPedidoVendaSimples_SelecaoAgrupada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB, DBCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, ToolCtrlsEh;

type
  TfrmPedidoVendaSimples_SelecaoAgrupada = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    dsMarca: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdMarca: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMarca: TcxGridDBColumn;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    dsProduto: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmProduto: TcxGridDBColumn;
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    qryTempnCdProdutoPai: TIntegerField;
    qryTempnCdProduto: TIntegerField;
    qryTempcReferencia: TStringField;
    qryTempcNmProduto: TStringField;
    qryTempnQtdePed: TBCDField;
    qryTempnQtdeEstoque: TBCDField;
    qryTempnQtdeEmpenhado: TBCDField;
    qryTempnQtdeDisponivel: TBCDField;
    qryTempnValUnitario: TBCDField;
    qryTempnValDescontoUnit: TBCDField;
    qryTempnValTotalItem: TBCDField;
    GroupBox5: TGroupBox;
    DBGridEh3: TDBGridEh;
    GroupBox6: TGroupBox;
    DBGridEh1: TDBGridEh;
    qryResumo: TADOQuery;
    qryResumonCdProdutoPai: TIntegerField;
    qryResumocNmProduto: TStringField;
    qryResumonQtdePedTotal: TBCDField;
    qryResumonValorTotal: TBCDField;
    dsResumo: TDataSource;
    qryTempAnalitica: TADOQuery;
    qryTempAnaliticanCdProdutoPai: TIntegerField;
    qryTempAnaliticacNmProduto: TStringField;
    qryTempAnaliticanQtdePed: TBCDField;
    qryTempAnaliticanValUnitario: TBCDField;
    qryTempAnaliticanValDescontoUnit: TBCDField;
    qryTempAnaliticanValTotalItem: TBCDField;
    qryTempAnaliticanCdProduto: TIntegerField;
    qryPopulaTempInicial: TADOQuery;
    procedure preparaSelecaoAgrupada;
    procedure qryMarcaAfterScroll(DataSet: TDataSet);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTipoPedido : integer ;
    nCdPedido     : integer ;
  end;

var
  frmPedidoVendaSimples_SelecaoAgrupada: TfrmPedidoVendaSimples_SelecaoAgrupada;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmPedidoVendaSimples_SelecaoAgrupada }

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.preparaSelecaoAgrupada;
begin

    qryMarca.Close;
    qryMarca.Open;

    qryPreparaTemp.ExecSQL;

    qryPopulaTempInicial.Parameters.ParamByName('nCdPedido').Value := nCdPedido ;
    qryPopulaTempInicial.ExecSQL;

    qryResumo.Close;
    qryResumo.Open;

    Self.ShowModal;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.qryMarcaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close;
end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if (qryMarca.Active) and (qryMarcanCdMarca.Value > 0) then
  begin
      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := nCdTipoPedido ;
      qryProduto.Parameters.ParamByName('nCdMarca').Value      := qryMarcanCdMarca.Value ;
      qryProduto.Open;

      if not qryProduto.eof then
          cxGrid2.SetFocus;
  end ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  qryTemp.Close;

  if (qryProduto.Active) and (qryProdutonCdProduto.Value > 0) then
  begin
      qryPopulaTemp.Close;
      qryPopulaTemp.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutonCdProduto.Value ;
      qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value     := nCdPedido ;
      qryPopulaTemp.ExecSQL;

      qryTemp.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutonCdProduto.Value ;
      qryTemp.Open;

      DBGridEh3.SetFocus ;
      DBGridEh3.Col := 4 ;
  end ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh3.Columns[1].Width := 65 ;
  DBGridEh3.Columns[2].Width := 190 ;
  DBGridEh3.Columns[3].Width := 52 ;

  DBGridEh3.Columns[4].Width := 55 ;
  DBGridEh3.Columns[5].Width := 62 ;
  DBGridEh3.Columns[6].Width := 62 ;

  if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'S') then
  begin
      DBGridEh3.Columns[7].ReadOnly         := False ;
      DBGridEh3.Columns[7].Color            := clWhite ;
      DBGridEh3.Columns[7].Title.Font.Color := clBlack ;
  end ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.FormKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.qryTempBeforePost(
  DataSet: TDataSet);
begin

  if (qryTempnQtdePed.Value < 0) then
  begin
      MensagemAlerta('Quantidade Inv�lida.') ;
      abort ;
  end ;

  if (qryTempnValUnitario.Value <= 0) and (qryTempnQtdePed.Value > 0) then
  begin
      MensagemAlerta('Pre�o Unit�rio Inv�lido ou n�o informado.') ;
      abort ;
  end ;

  inherited;

  qryTempnValTotalItem.Value := (qryTempnQtdePed.Value * (qryTempnValUnitario.Value - qryTempnValDescontoUnit.Value) ) ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.qryTempAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  qryResumo.Close;
  qryResumo.Open;
  
end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
  begin

      if (qryMarca.Active) and (qryMarcanCdMarca.Value > 0) then
      begin
          qryProduto.Close;
          qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := nCdTipoPedido ;
          qryProduto.Parameters.ParamByName('nCdMarca').Value      := qryMarcanCdMarca.Value ;
          qryProduto.Open;

          if not qryProduto.eof then
              cxGrid2.SetFocus;
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.cxGridDBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
  begin

      qryTemp.Close;
      
      if (qryProduto.Active) and (qryProdutonCdProduto.Value > 0) then
      begin
          qryPopulaTemp.Close;
          qryPopulaTemp.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutonCdProduto.Value ;
          qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value     := nCdPedido ;
          qryPopulaTemp.ExecSQL;

          qryTemp.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutonCdProduto.Value ;
          qryTemp.Open;

          DBGridEh3.SetFocus ;
          DBGridEh3.Col := 4 ;
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.DBGridEh3KeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

  if (Key = VK_TAB) then
  begin
      if (qryTemp.State = dsEdit) then
          qryTemp.Post ;

      cxGrid1.SetFocus;
  end ;
  
end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.ToolButton2Click(
  Sender: TObject);
begin

  qryTempAnalitica.Close ;

  inherited;

end;

procedure TfrmPedidoVendaSimples_SelecaoAgrupada.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  qryTempAnalitica.Close;
  qryTempAnalitica.Open;

  Close;
end;

end.
