unit fLookup_Padrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, GridsEh, DBGridEh, Mask, DBCtrlsEh, StdCtrls,
  ComCtrls, ToolWin, ExtCtrls, DBCtrls, ImgList, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomPopupMenu, cxGridPopupMenu, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, jpeg, ShellApi, DBGridEhGrouping, PrnDbgeh,
  ToolCtrlsEh, ER2Excel;

type
  TfrmLookup_Padrao = class(TForm)
    Image1: TImage;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    DBGridResult: TDBGridEh;
    qryTabelaConsulta: TADOQuery;
    qryResultado: TADOQuery;
    qryTabelaConsultanCdTabelaConsulta: TIntegerField;
    qryTabelaConsultacNmTabelaConsulta: TStringField;
    qryTabelaConsultaComandoSQL: TMemoField;
    dsResultado: TDataSource;
    qryFiltroConsulta: TADOQuery;
    qryFiltroConsultanCdTabelaConsulta: TIntegerField;
    qryFiltroConsultacNmCampoTabela: TStringField;
    qryFiltroConsultacTipoDado: TStringField;
    qryFiltroConsultacNmCampoTela: TStringField;
    ADOTable1: TADOTable;
    ImageList1: TImageList;
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cmbCampo1: TComboBox;
    cmbTipo1: TComboBox;
    cmbCampo2: TComboBox;
    cmbTipo2: TComboBox;
    edtFiltro1: TMaskEdit;
    edtFiltro2: TMaskEdit;
    DBEdit1: TDBEdit;
    Image2: TImage;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    printDBGridEh: TPrintDBGridEh;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    btPlanilha: TToolButton;
    ER2Excel1: TER2Excel;
    function ExecutaConsulta(nCdTabelaConsulta : Integer) : Integer;
    procedure DBGridResultDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtFiltro1KeyPress(Sender: TObject; var Key: Char);
    procedure edtFiltro2KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ProcessarFiltros;
    procedure DBGridResultKeyPress(Sender: TObject; var Key: Char);
    procedure cmbCampo1Select(Sender: TObject);
    procedure cmbCampo2Select(Sender: TObject);
    function ExecutaConsulta2(nCdTabelaConsulta: Integer; cClausula : String) : Integer;
    procedure FormataGrid();
    procedure cmbCampo1Change(Sender: TObject);
    procedure cmbCampo2Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton4Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure emFoco(Sender: TObject) ;
    procedure semFoco(Sender: TObject) ;
    procedure ImprimeDBGrid(DBGridEh1 : TDBGridEh; cTitulo : string);
    procedure edtFiltro1Change(Sender: TObject);
    procedure edtFiltro2Change(Sender: TObject);
    procedure btPlanilhaClick(Sender: TObject);
  private
    { Private declarations }
      oCampoRetorno:TField ;
      oRetornaValor:Boolean ;
      dsConsulta : TDataSource ;
      nCdPK : Integer ;
      Campos : TFieldList ;
      NovoCampo :TFieldList ;
      arrTipoDado : Array of String;
      arrNomeCampo : Array of String;
      cQueryOriginal : String;

  public
    cWhereAdicional : String ;
    cNmUsuarioLogado   : string;
    cNomeComputador    : string;
    nCdEmpresaAtiva    : integer;
    nCdUsuarioLogado   : integer;
    nCdLojaAtiva       : integer;
    nCdTerceiroUsuario : integer;
    { Public declarations }
  end;

var
  frmLookup_Padrao: TfrmLookup_Padrao;

implementation

function TfrmLookup_Padrao.ExecutaConsulta2(nCdTabelaConsulta: Integer; cClausula : String) : Integer;
begin
    cWhereAdicional := cClausula ;
    Result := ExecutaConsulta(nCdTabelaConsulta) ;
end ;

function TfrmLookup_Padrao.ExecutaConsulta(nCdTabelaConsulta : Integer) : Integer;
var
    i          :Integer;
    nCdRetorno : integer ;
begin

    qryResultado.Close ;

    DBGridResult.Columns.Clear;

    DBGridResult.DataSource := dsResultado ;

    if (DBGridResult.Columns.Count > 0) then
        DBGridResult.Columns.Clear;

    // obt�m os dados da consulta no dicion�rio de dados
    qryTabelaConsulta.Close;
    qryTabelaConsulta.Parameters.ParamByName('nCdTabelaConsulta').Value := nCdTabelaConsulta ;
    qryTabelaConsulta.Open ;

    if (qryTabelaConsulta.Eof) then
    begin
        ShowMessage('Tabela de Consulta ' + IntToStr(nCdTabelaConsulta) + ' n�o encontrada.') ;
        Result := 0 ;
        exit ;
    end ;

    frmLookup_Padrao.Caption := qryTabelaConsultanCdTabelaConsulta.asString + ' - ' + qryTabelaConsultacNmTabelaConsulta.Value ;

    cQueryOriginal := qryTabelaConsultaComandoSQL.Value;

    // obt�m os filtros da consulta
    qryFiltroConsulta.Close ;
    qryFiltroConsulta.Parameters.ParamByName('nCdTabelaConsulta').Value := nCdTabelaConsulta ;
    qryFiltroConsulta.Open  ;

    cmbCampo1.Items.Clear;
    cmbCampo2.Items.Clear;

    cmbCampo1.Enabled := False ;
    cmbCampo2.Enabled := False ;

    cmbTipo1.Enabled  := False ;
    cmbTipo2.Enabled  := False ;

    edtFiltro1.Enabled := False ;
    edtFiltro2.Enabled := False ;

    edtFiltro1.Text := '' ;
    edtFiltro2.Text := '' ;

    if not qryFiltroConsulta.Eof then
    begin

        cmbCampo1.Enabled := True ;
        cmbCampo2.Enabled := True ;

        cmbTipo1.Enabled  := True ;
        cmbTipo2.Enabled  := True ;

        edtFiltro1.Enabled := True ;
        edtFiltro2.Enabled := True ;

        qryFiltroConsulta.First;

        SetLength(arrTipoDado,100) ;
        SetLength(arrNomeCampo,100) ;

        i := 0;

        while not qryFiltroConsulta.Eof do
        begin

            cmbCampo1.Items.Add(qryFiltroConsultacNmCampoTela.Value) ;
            cmbCampo2.Items.Add(qryFiltroConsultacNmCampoTela.Value) ;

            arrTipoDado[i]  := qryFiltroConsultacTipoDado.Value ;
            arrNomeCampo[i] := qryFiltroConsultacNmCampoTabela.Value;

            qryFiltroConsulta.Next;

            i := i+1 ;

            //ADOTable1.FieldList.Insert(0,qryFIltroConsultacNmCampoTabela.Value);
            //ADOTable1.FieldList.Fields[0].SetFieldType(StringField);

            //NovoCampo := NovoCampo.Create(Campos);

            //Campos.Insert(NovoCampo);
            //arrCampos.Fields[0].Name := qryFiltroConsultacNmCampoTabela.Value;
            //arrCampos.Fields.Create();

        end ;

        qryFiltroConsulta.Close ;

        cmbTipo1.ItemIndex := 0 ;
        cmbTipo2.ItemIndex := 0 ;

        cmbCampo1.ItemIndex := 0 ;
        cmbCampo2.ItemIndex := 0 ;

        cmbCampo1.OnSelect(nil);
        cmbCampo2.OnSelect(nil);

    end ;

    // atribui o comando SQL ao dataset que tratar� o resultado

    qryResultado.Close ;
    qryResultado.Parameters.Clear;
    qryResultado.SQL.Clear;
    qryResultado.SQL.Add(qryTabelaConsultaComandoSQL.AsString) ;

    If (cWhereAdicional <> '') then
    begin

      If (Pos('WHERE',qryResultado.SQL.Text) > 0) then
        qryResultado.SQL.Add(' AND ' + cWhereAdicional)
      else
        qryResultado.SQL.Add(' WHERE ' + cWhereAdicional) ;

    end;

    {-- estudar maneira de pegar essas variaveis --}
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Empresa',IntToStr(nCdEmpresaAtiva),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Usuario',IntToStr(nCdUsuarioLogado),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Loja',IntToStr(nCdLojaAtiva),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@TerceiroLogin',IntToStr(nCdTerceiroUsuario),[rfReplaceAll,rfIgnoreCase]) ;

    GroupBox1.Visible := True ;

    If (cmbCampo1.Items.Count = 0) then
    begin
        qryResultado.Open;
        FormataGrid() ;
        GroupBox1.Visible := False ;
    end ;

    qryResultado.FieldList.Update;

    DBGridResult.Columns.AddAllColumns(True);

    frmLookup_Padrao.ShowModal ;

    if (qryResultado.FieldCount > 0) and (qryResultado.RecordCount > 0) then
        nCdRetorno := qryResultado.FieldList[0].Value
    else
        nCdRetorno := 0 ;

    {DBGridResult.Col := 1 ;}
    DBGridResult.Columns.RebuildColumns;
    
    Result := nCdRetorno ;

end;

{$R *.dfm}

procedure TfrmLookup_Padrao.FormataGrid();
var
   i:integer ;
   i2:integer ;
begin

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBGridEh) then
    begin
      (Components [I] as TDBGridEh).Flat := True ;
      (Components [I] as TDBGridEh).FixedColor := clMoneyGreen ;
      (Components [I] as TDBGridEh).Font.Name := 'Tahoma' ;
      (Components [I] as TDBGridEh).Font.Size := 10 ;
      (Components [I] as TDBGridEh).Font.Color := clBlue ;
      (Components [I] as TDBGridEh).OddRowColor := clAqua ;
      (Components [I] as TDBGridEh).UseMultiTitle := True ;
      (Components [I] as TDBGridEh).OptionsEh := [dghFixed3D,dghHighlightFocus,dghClearSelection,dghDialogFind] ;

      (Components [I] as TDBGridEh).TitleFont.Name  := 'Tahoma' ;
      (Components [I] as TDBGridEh).TitleFont.Size  := 10 ;
      (Components [I] as TDBGridEh).TitleFont.Style := [fsBold] ;

      if ((Components [I] as TDBGridEh).Tag = 1) then
      begin
          (Components [I] as TDBGridEh).ParentFont := False ;
          (Components [I] as TDBGridEh).ReadOnly   := True ;
          (Components [I] as TDBGridEh).Color      := clSilver ;
          (Components [I] as TDBGridEh).Font.Color := clYellow ;
          Update;
      end ;

      // padroniza as colunas

      with (Components[I] as TDBGridEH) do
      begin
          OptionsEH := OptionsEH + [dghAutoSortMarking] ;

          with Columns do
          begin

            for i2 := 0 to Count - 1 do
            begin

                OnSortMarkingChanged := nil;

            end ;


          end ;

          if Columns.Count = 0 then
            exit ;

          //FooterRowCount := 1;
          //Columns.Items [0].Footer.ValueType := fvtCount;

          for i2 := 1 to Columns.Count - 1 do
          begin
              with Columns.Items[i2] do
              begin

                  // Ajusta a largura da coluna
                  If (Columns.Items[i2].Width < (Length(Columns.Items[i2].Title.Caption) * 3)) then
                      COlumns.Items[i2].Width := (Length(Columns.Items[i2].Title.Caption)*3) ;

                  try

                    if (Field <> nil) then
                    begin
                        if Field.DataType in [ftFloat, ftCurrency, ftBCD] then
                        begin
                            Columns.Items [i2].DisplayFormat := '#,##0.00';
                            Footer.DisplayFormat := '#,##0.00';
                        end ;

                    if Field.DataType in [ftDateTime] then
                    begin
                        Columns.Items [i2].EditMask := '!99/99/9999;1;_';
                        ButtonStyle := cbsNone ;
                    end ;

                    end ;

                  except ;
                  end ;

              end ;
          end ;


      end ;

    end;

  end ;

end ;

procedure TfrmLookup_Padrao.DBGridResultDblClick(Sender: TObject);
begin
    cWhereAdicional := '' ;
    frmLookup_Padrao.Close ;
end;

procedure TfrmLookup_Padrao.FormCreate(Sender: TObject);
var
    i : Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBEdit) then
    begin
      (Components [I] as TDBEdit).OnEnter     := emFoco ;
      (Components [I] as TDBEdit).OnExit      := semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      //(Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := True    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TDBEdit).Font.Size   := 10 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ParentFont := False ;
          (Components [I] as TDBEdit).Enabled := False ;
          (Components [I] as TDBEdit).Color   := clSilver ;
          (Components [I] as TDBEdit).Font.Color := clWhite ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := emFoco ;
      (Components [I] as TEdit).OnExit      := semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      //(Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TEdit).Font.Size   := 10 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      (Components [I] as TMaskEdit).OnEnter     := emFoco ;
      (Components [I] as TMaskEdit).OnExit      := semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      //(Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TMaskEdit).Font.Size   := 10 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnEnter     := emFoco ;
      (Components [I] as TDBLookupComboBox).OnExit      := semFoco ;
      (Components [I] as TDBLookupComboBox).Color       := clWhite ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace   ;
      //(Components [I] as TDBLookupComboBox).BevelKind   := bkNone    ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := True      ;
      (Components [I] as TDBLookupComboBox).Font.Name   := 'Consolas' ;
      (Components [I] as TDBLookupComboBox).Font.Size   := 10 ;
      (Components [I] as TDBLookupComboBox).Font.Style  := [] ;
      (Components [I] as TDBLookupComboBox).DropDownRows := 30 ;

    end;

    if (Components [I] is TComboBox) then
    begin
      (Components [I] as TComboBox).OnEnter     := emFoco ;
      (Components [I] as TComboBox).OnExit      := semFoco ;
      (Components [I] as TComboBox).Color       := clWhite ;
      (Components [I] as TComboBox).BevelInner  := bvSpace   ;
      //(Components [I] as TComboBox).BevelKind   := bkNone    ;
      (Components [I] as TComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TComboBox).Ctl3D       := True      ;
      (Components [I] as TComboBox).Font.Name   := 'Consolas' ;
      (Components [I] as TComboBox).Font.Size   := 10 ;
      (Components [I] as TComboBox).Font.Style  := [] ;
    end;


    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Consolas' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clBlack ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;
  end ;


end;

procedure TfrmLookup_Padrao.ProcessarFiltros;
var
    bFiltro : Boolean ;
begin

    qryResultado.Close ;
    qryResultado.SQL.Clear;
    qryResultado.SQL.Add(cQueryOriginal);

    //Indice do tipo de filtro
    // 0 -Maior que
    // 1 -Menor que
    // 2 -Igual a
    // 3 -Diferente de

    bFiltro := False ;

    If (Trim(edtFiltro1.Text) <> '') and (Trim(edtFiltro1.Text) <> '/  /') and (Trim(edtFiltro1.Text) <> '____________________') then
    begin

        bFiltro := True ;

        If (Pos('WHERE',qryResultado.SQL.Text) > 0) then
          qryResultado.SQL.Add('AND ' + arrNomeCampo[cmbCampo1.ItemIndex])
        else
          qryResultado.SQL.Add('WHERE ' + arrNomeCampo[cmbCampo1.ItemIndex]) ;


        If (cmbTipo1.ItemIndex = 0) and (arrTipoDado[cmbCampo1.ItemIndex] <> 'C') then
            qryResultado.SQL.Add(' > ') ;

        If (cmbTipo1.ItemIndex = 0) and (arrTipoDado[cmbCampo1.ItemIndex] = 'C') then
            qryResultado.SQL.Add(' LIKE ') ;

        If (cmbTipo1.ItemIndex = 1) then
            qryResultado.SQL.Add(' < ') ;

        If ((cmbTipo1.ItemIndex = 2) and (arrTipoDado[cmbCampo1.ItemIndex] <> 'D')) then
            qryResultado.SQL.Add(' = ') ;

        If (cmbTipo1.ItemIndex = 3) then
            qryResultado.SQL.Add(' <> ') ;

        If (cmbTipo1.ItemIndex = 0) and (arrTipoDado[cmbCampo1.ItemIndex] = 'C') then
            qryResultado.SQL.Add(Chr(39) + StringReplace(edtFiltro1.Text,'''','''''',[rfReplaceAll]) + '%' + Chr(39))
        else if (arrTipoDado[cmbCampo1.ItemIndex] <> 'D') then
            qryResultado.SQL.Add(Chr(39) + StringReplace(edtFiltro1.Text,'''','''''',[rfReplaceAll]) + Chr(39)) ;

        // se for igualdade de data, inserir a hora
        if ((cmbTipo1.ItemIndex = 2) and (arrTipoDado[cmbCampo1.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add(' >= Convert(DATETIME,' + Chr(39) + edtFiltro1.Text + ' 00:00:00' + Chr(39) + ',103)') ;
            qryResultado.SQL.Add(' AND ' + arrNomeCampo[cmbCampo1.ItemIndex] + ' <= Convert(DATETIME,' + Chr(39) + edtFiltro1.Text + ' 23:59:59' + Chr(39) + ',103)') ;
        end ;

        // se for maior que de data, inserir a hora
        if ((cmbTipo1.ItemIndex = 0) and (arrTipoDado[cmbCampo1.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add('   Convert(DATETIME,' + Chr(39) + edtFiltro1.Text + ' 23:59:59' + Chr(39) + ',103)') ;
        end ;

        // se for menor que de data, inserir a hora
        if ((cmbTipo1.ItemIndex = 1) and (arrTipoDado[cmbCampo1.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add('   Convert(DATETIME,' + Chr(39) + edtFiltro1.Text + ' 00:00:00' + Chr(39) + ',103)') ;
        end ;

    end ;

    If (Trim(edtFiltro2.Text) <> '') and (Trim(edtFiltro2.Text) <> '/  /') and (Trim(edtFiltro2.Text) <> '____________________') then
    begin

        If (Pos('WHERE',qryResultado.SQL.Text) > 0) then
            qryResultado.SQL.Add(' AND ' + arrNomeCampo[cmbCampo2.ItemIndex])
        else
            qryResultado.SQL.Add('WHERE ' + arrNomeCampo[cmbCampo2.ItemIndex])  ;

        If (cmbTipo2.ItemIndex = 0) then
            qryResultado.SQL.Add(' > ') ;

        If (cmbTipo2.ItemIndex = 1) then
            qryResultado.SQL.Add(' < ') ;

        If ((cmbTipo2.ItemIndex = 2) and (arrTipoDado[cmbCampo2.ItemIndex] <> 'D')) then
            qryResultado.SQL.Add(' = ') ;

        If (cmbTipo2.ItemIndex = 3) then
            qryResultado.SQL.Add(' <> ') ;

        if (arrTipoDado[cmbCampo2.ItemIndex] <> 'D') then
            qryResultado.SQL.Add(Chr(39) + edtFiltro2.Text + Chr(39)) ;

        // se for igualdade de data, inserir a hora
        if ((cmbTipo2.ItemIndex = 2) and (arrTipoDado[cmbCampo2.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add(' >= Convert(DATETIME,' + Chr(39) + edtFiltro2.Text + ' 00:00:00' + Chr(39) + ',103)') ;
            qryResultado.SQL.Add(' AND ' + arrNomeCampo[cmbCampo2.ItemIndex] + ' <= Convert(DATETIME,' + Chr(39) + edtFiltro2.Text + ' 23:59:59' + Chr(39) + ',103)') ;
        end ;

        // se for maior que de data, inserir a hora
        if ((cmbTipo2.ItemIndex = 0) and (arrTipoDado[cmbCampo2.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add('   Convert(DATETIME,' + Chr(39) + edtFiltro2.Text + ' 23:59:59' + Chr(39) + ',103)') ;
        end ;

        // se for menor que de data, inserir a hora
        if ((cmbTipo2.ItemIndex = 1) and (arrTipoDado[cmbCampo2.ItemIndex] = 'D')) then
        begin
            qryResultado.SQL.Add('   Convert(DATETIME,' + Chr(39) + edtFiltro2.Text + ' 00:00:00' + Chr(39) + ',103)') ;
        end ;

    end ;

    If (cWhereAdicional <> '') then
    begin

      If (Pos('WHERE',qryResultado.SQL.Text) > 0) then
        qryResultado.SQL.Add(' AND ' + cWhereAdicional)
      else
        qryResultado.SQL.Add(' WHERE ' + cWhereAdicional) ;

    end;

    {-- estudar troca de parametros --}
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Empresa',IntToStr(nCdEmpresaAtiva),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Usuario',IntToStr(nCdUsuarioLogado),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@Loja',IntToStr(nCdLojaAtiva),[rfReplaceAll,rfIgnoreCase]) ;
    qryResultado.SQL.Text := StringReplace(qryResultado.SQL.Text,'@@TerceiroLogin',IntToStr(nCdTerceiroUsuario),[rfReplaceAll,rfIgnoreCase]) ;


    qryResultado.SQL.Add('ORDER BY ' + arrNomeCampo[cmbCampo1.ItemIndex] ) ;

     if (cmbCampo2.ItemIndex <> cmbCampo1.ItemIndex) then
        qryResultado.SQL.Add(',' + arrNomeCampo[cmbCampo2.ItemIndex]) ;
        
    qryResultado.Open ;
    FormataGrid();
    DBGridResult.SetFocus ;

    if (qryResultado.eof) then
        edtFiltro1.SetFocus;

end ;

procedure TfrmLookup_Padrao.FormKeyPress(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin
    key := #0;
    perform (WM_NextDlgCtl, 0, 0);
  end;

end;

procedure TfrmLookup_Padrao.edtFiltro1KeyPress(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin
    ProcessarFiltros();
  end;

  if (Key = #27) then
  begin
      cWhereAdicional := '' ;
      DBGridResult.Columns.RebuildColumns;
      qryResultado.Close ;
      frmLookup_Padrao.Close ;
  end ;


end;

procedure TfrmLookup_Padrao.edtFiltro2KeyPress(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin
      ProcessarFiltros();
  end;

  if (Key = #27) then
  begin
      cWhereAdicional := '' ;
      DBGridResult.Columns.RebuildColumns;
      qryResultado.Close ;
      frmLookup_Padrao.Close ;
  end ;


end;

procedure TfrmLookup_Padrao.FormShow(Sender: TObject);
begin

    ToolButton1.Enabled := False ;

    if (edtFiltro1.Enabled) then
    begin
        edtFiltro1.SetFocus ;
        ToolButton1.Enabled := True ;
    end
    else
        DBGridResult.SetFocus ;

end;

procedure TfrmLookup_Padrao.ToolButton2Click(Sender: TObject);
begin
    cWhereAdicional := '' ;
    DBGridResult.Columns.RebuildColumns;
    qryResultado.Close ;
    frmLookup_Padrao.Close ;
end;


procedure TfrmLookup_Padrao.ToolButton1Click(Sender: TObject);
begin
    ProcessarFiltros();
end;

procedure TfrmLookup_Padrao.DBGridResultKeyPress(Sender: TObject;
  var Key: Char);
begin

    if key = #13 then
    begin

      cWhereAdicional := '' ;
      frmLookup_Padrao.Close ;

    end;

    if (Key = #27) then
    begin
        cWhereAdicional := '' ;
        DBGridResult.Columns.RebuildColumns;
        qryResultado.Close;
        frmLookup_Padrao.Close;
    end

end;

procedure TfrmLookup_Padrao.cmbCampo1Select(Sender: TObject);
begin

    edtFiltro1.Text := '' ;

    cmbTipo1.ItemIndex  := 0 ;

    if (arrTipoDado[cmbCampo1.ItemIndex] = 'C') then
        edtFiltro1.EditMask := '' ;

    if (arrTipoDado[cmbCampo1.ItemIndex] = 'I') then
    begin
        edtFiltro1.EditMask := '####################;1;_' ;
        cmbTipo1.ItemIndex  := 2 ;
    end ;

    if (arrTipoDado[cmbCampo1.ItemIndex] = 'D') then
    begin
        edtFiltro1.EditMask := '!99/99/9999;1;_' ;
        cmbTipo1.ItemIndex  := 2 ;
    end ;

end;

procedure TfrmLookup_Padrao.cmbCampo2Select(Sender: TObject);
begin
    edtFiltro2.Text := '' ;

    cmbTipo2.ItemIndex  := 0 ;

    if (arrTipoDado[cmbCampo2.ItemIndex] = 'C') then
        edtFiltro2.EditMask := '' ;

    if (arrTipoDado[cmbCampo2.ItemIndex] = 'I') then
    begin
        edtFiltro2.EditMask := '####################;1;_' ;
        cmbTipo2.ItemIndex  := 2 ;
    end ;

    if (arrTipoDado[cmbCampo2.ItemIndex] = 'D') then
    begin
        edtFiltro2.EditMask := '!99/99/9999;1;_' ;
        cmbTipo2.ItemIndex  := 2 ;
    end ;

end;

procedure TfrmLookup_Padrao.cmbCampo1Change(Sender: TObject);
begin
    edtFiltro1.SetFocus ;

end;

procedure TfrmLookup_Padrao.cmbCampo2Change(Sender: TObject);
begin
    edtFiltro2.SetFocus ;

end;

procedure TfrmLookup_Padrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

    cWhereAdicional := '' ;

end;

procedure TfrmLookup_Padrao.ToolButton4Click(Sender: TObject);
begin

    ImprimeDBGrid(DBGridResult,frmLookup_Padrao.Caption);

end;

procedure TfrmLookup_Padrao.Image2Click(Sender: TObject);
begin
    ShellExecute(Handle, 'open', 'http://www.er2soft.com.br','', '', 1);
end;

procedure TfrmLookup_Padrao.emFoco(Sender: TObject);
begin

   if (Sender is TEdit) then
   begin
       (sender as TEdit).Color     := $00AAFFFF;
       (sender as TEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBMemo) then
   begin
       (sender as TDBMemo).Color     := $00AAFFFF;
       (sender as TDBMemo).Font.Color:= clBlack;
   end ;

   if (Sender is TMaskEdit) then
   begin
       (sender as TMaskEdit).Color     := $00AAFFFF;
       (sender as TMaskEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBEdit) then
   begin
       (sender as TDBEdit).Color     := $00AAFFFF;
       (sender as TDBEdit).Font.Color:= clBlack;
   end;

   if (Sender is TDBLookupComboBox) then
   begin
       (sender as TDBLookupComboBox).Color     := $00AAFFFF;
       (sender as TDBLookupComboBox).Font.Color:= clBlack;
   end ;

   if (Sender is TComboBox) then
   begin
       (sender as TComboBox).Color     := $00AAFFFF;
       (sender as TComboBox).Font.Color:= clBlack;
   end ;

end;

procedure TfrmLookup_Padrao.semFoco(Sender: TObject);
begin

   if (Sender is TEdit) then
   begin
       (sender as TEdit).Color     := clWhite;
       (sender as TEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBMemo) then
   begin
       (sender as TDBMemo).Color     := clWhite;
       (sender as TDBMemo).Font.Color:= clBlack;
   end ;

   if (Sender is TMaskEdit) then
   begin
       (sender as TMaskEdit).Color     := clWhite;
       (sender as TMaskEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBEdit) then
   begin
       (sender as TDBEdit).Color     := clWhite ; //clWhite;
       (sender as TDBEdit).Font.Color:= clBlack;
   end;

   if (Sender is TDBLookupComboBox) then
   begin
       (sender as TDBLookupComboBox).Color     := clWhite;
       (sender as TDBLookupComboBox).Font.Color:= clBlack;
   end ;

   if (Sender is TComboBox) then
   begin
       (sender as TComboBox).Color     := clWhite;
       (sender as TComboBox).Font.Color:= clBlack;
   end ;

end;

procedure TfrmLookup_Padrao.ImprimeDBGrid(DBGridEh1: TDBGridEh;
  cTitulo: string);
begin

    PrintDBGridEh.PageFooter.LeftText.Text  := 'Emitido por ' + cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + ' ' + cNomeComputador;
    PrintDBGridEh.PageFooter.RightText.Text := 'www.er2soft.com.br' ;
    PrintDBGridEh.PageFooter.Font.Name      := 'Consolas' ;
    PrintDBGridEh.DBGridEh                  := DBGridEh1 ;
    PrintDBGridEh.Title.Text                := cTitulo ;
    PrintDBGridEh.PrintFontName             := 'Consolas' ;
    PrintDBGridEh.Options                   := [pghFitGridToPageWidth];
    PrintDBGridEh.Preview;
    
end;

procedure TfrmLookup_Padrao.edtFiltro1Change(Sender: TObject);
begin

    DBGridResult.Columns.RebuildColumns;
    qryResultado.Close;

end;

procedure TfrmLookup_Padrao.edtFiltro2Change(Sender: TObject);
begin

    DBGridResult.Columns.RebuildColumns;
    qryResultado.Close;

end;

procedure TfrmLookup_Padrao.btPlanilhaClick(Sender: TObject);
var
  i          : Integer;
  cValorAux  : String;
  arrColunas : array[0..25] of string;
  iLinha     : Integer;

  {-- Fun��o para tratar o t�tulo --}
  function fnTrataTitulo(cValor : String) : String;
  begin
      while (Pos('|', cValor) > 0) do
      begin
          Delete(cValor, 1, Pos('|', cValor));
          Result := cValor;
      end;
  end;

  {-- Fun��o para gerar as colunas no excel --}
  procedure prColunasExcel(var arr : Array of String);
  begin
      {-- Array com letras do alfabero --}
      arr[0]  := 'A';
      arr[1]  := 'B';
      arr[2]  := 'C';
      arr[3]  := 'D';
      arr[4]  := 'E';
      arr[5]  := 'F';
      arr[6]  := 'G';
      arr[7]  := 'H';
      arr[8]  := 'I';
      arr[9]  := 'J';
      arr[10] := 'K';
      arr[11] := 'L';
      arr[12] := 'M';
      arr[13] := 'N';
      arr[14] := 'O';
      arr[15] := 'P';
      arr[16] := 'Q';
      arr[17] := 'R';
      arr[18] := 'S';
      arr[19] := 'T';
      arr[20] := 'U';
      arr[21] := 'V';
      arr[22] := 'W';
      arr[23] := 'X';
      arr[24] := 'Y';
      arr[25] := 'Z';
  end;

begin
  if (qryResultado.IsEmpty) then
      Exit;

  {-- Monta o Excel --}
  ER2Excel1.Titulo := frmLookup_Padrao.Caption;

  ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
  ER2Excel1.Celula['A1'].Negrito;
  ER2Excel1.Celula['A1'].Range('W4');
  ER2Excel1.Celula['A1'].Congelar('A5');

  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := frmLookup_Padrao.Caption;

  {-- Gera��o das colunas --}
  prColunasExcel(arrColunas);

  { -- formata t�tulo -- }
  for i:= 0 to DBGridResult.Columns.Count - 1 do
  begin
      {-- Gera as colunas --}
      if (Pos('|', DBGridResult.Columns[i].Title.Caption) > 0) then
          ER2Excel1.Celula[arrColunas[i] + '4'].Text := fnTrataTitulo(DBGridResult.Columns[i].Title.Caption)
      else
          ER2Excel1.Celula[arrColunas[i] + '4'].Text := DBGridResult.Columns[i].Title.Caption;
  end;

  { -- Percorre o Result para exibir -- }
  qryResultado.First;
  qryResultado.DisableControls;

  iLinha := 5;
  while (not qryResultado.Eof) do
  begin
      for i:= 0 to qryResultado.FieldCount -1 do
      begin
          ER2Excel1.Celula[arrColunas[i] + IntToStr(iLinha)].Text := qryResultado.Fields[i].Text;
      end;

      Inc(iLinha);

      qryResultado.Next;
  end;

  qryResultado.First;
  qryResultado.EnableControls;

  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

end.
