unit fCaixa_LanctoMan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, DB, StdCtrls, Mask, DBCtrls, cxButtons,
  jpeg, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, ADODB, RDprint, cxDBEdit;

type
  TfrmCaixa_LanctoMan = class(TForm)
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    qryTipoLanctocSinalOper: TStringField;
    qryTipoLanctocFlgEmiteECF: TIntegerField;
    qryTipoLanctocFlgPermManual: TIntegerField;
    RDprint1: TRDprint;
    qryLanctoFin: TADOQuery;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFinnCdContaBancaria: TIntegerField;
    qryLanctoFinnCdConta: TStringField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdTipoLancto: TIntegerField;
    qryLanctoFincNmTipoLancto: TStringField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFincNmUsuario: TStringField;
    qryLanctoFincHistorico: TStringField;
    qryContaCofre: TADOQuery;
    qryContaCofrenCdContaBancaria: TIntegerField;
    qryContaCofrenCdConta: TStringField;
    SP_LANCAMENTO_CAIXA: TADOStoredProc;
    edtTipoLancto: TcxCurrencyEdit;
    Label5: TLabel;
    Image1: TImage;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    DataSource1: TDataSource;
    edtValLancto: TcxCurrencyEdit;
    Label3: TLabel;
    DBEdit1: TcxDBTextEdit;
    Label1: TLabel;
    edtHistorico: TcxTextEdit;
    qryLanctoEstornado: TADOQuery;
    qryLanctoEstornadonCdLanctoFin: TIntegerField;
    qryLanctoEstornadodDtEstorno: TDateTimeField;
    qryLanctoEstornadodDtLancto: TDateTimeField;
    qryLanctoEstornadonCdLoja: TStringField;
    qryLanctoEstornadonCdContaBancaria: TIntegerField;
    qryLanctoEstornadonCdConta: TStringField;
    qryLanctoEstornadocNmUsuarioOperador: TStringField;
    qryLanctoEstornadocNmTipoLancto: TStringField;
    qryLanctoEstornadonValLancto: TBCDField;
    qryLanctoEstornadocHistorico: TStringField;
    qryLanctoEstornadocMotivoEstorno: TStringField;
    qryLanctoEstornadocNmUsuario: TStringField;
    SP_CONTABILIZA_LANCTOFIN_MANUAL: TADOStoredProc;
    procedure edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtTipoLanctoExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtTipoLanctoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValLanctoKeyPress(Sender: TObject; var Key: Char);
    function  ImprimirComprovante(nCdLanctoFin : integer) : boolean ;
    procedure edtHistoricoKeyPress(Sender: TObject; var Key: Char);
    procedure imprimirComprovanteEstorno(nCdLanctoFin : integer);
    procedure prProcessaLanctoManual(bAutomatico : Boolean; nCdResumoCx, nCdTipoLancto : Integer; nValLancto : Double; cHistorico : String);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdResumoCaixa : Integer;
    cFlgUsaECF     : Boolean;

  end;

var
  frmCaixa_LanctoMan: TfrmCaixa_LanctoMan;
  nCdLanctoFin : integer;

implementation

uses fLookup_Padrao, fCaixa_Recebimento, fMenu, ACBrECFNaoFiscal;

{$R *.dfm}

procedure  TfrmCaixa_LanctoMan.prProcessaLanctoManual(bAutomatico : Boolean; nCdResumoCx, nCdTipoLancto : Integer; nValLancto : Double; cHistorico : String);
begin
  try
      SP_LANCAMENTO_CAIXA.Close;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCx;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdTipoLancto').Value  := nCdTipoLancto;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nValLancto').Value     := nValLancto;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdContaCofre').Value  := 0;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@cHistorico').Value     := cHistorico;
      SP_LANCAMENTO_CAIXA.ExecProc;

      nCdLanctoFin := SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdLanctoFin_Out').Value;

      {-- contabiliza o lan�amento manual --}
      SP_CONTABILIZA_LANCTOFIN_MANUAL.Close;
      SP_CONTABILIZA_LANCTOFIN_MANUAL.Parameters.ParamByName('@nCdLanctoFin').Value := nCdLanctoFin;
      SP_CONTABILIZA_LANCTOFIN_MANUAL.ExecProc;
  except
      Raise;
  end;
end;

procedure TfrmCaixa_LanctoMan.edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(125);

        If (nPK > 0) then
        begin
            edtTipoLancto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCaixa_LanctoMan.btnConfirmarClick(Sender: TObject);
var
    objComprovante : TACBrECFNaoFiscalComprovanteNaoFiscal ;
    objCaixa       : TfrmCaixa_Recebimento ;
label
    emiteComprovante;
begin

  if (DBEdit1.Text = '') then
  begin
      frmMenu.MensagemAlerta('Informe o tipo de lan�amento.') ;
      edtTipoLancto.SetFocus ;
      exit ;
  end ;

  if (trim(edtHistorico.Text) = '') then
  begin
      frmMenu.MensagemAlerta('Informe o hist�rico do lan�amento.') ;
      edtHistorico.SetFocus ;
      exit ;
  end ;

  if (edtValLancto.Value <= 0) then
  begin
      frmMenu.MensagemAlerta('Informe o valor do lan�amento.') ;
      edtValLancto.SetFocus ;
      exit ;
  end ;

  if (qryTipoLanctocFlgPermManual.Value = 0) then
  begin
      frmMenu.MensagemAlerta('Tipo de lan�amento n�o permitido.') ;
      abort ;
  end ;

  case frmMenu.MessageDlg('Confirma este lan�amento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

      if not objCaixa.AutorizacaoGerente(21) then
      begin
          frmMenu.MensagemAlerta('Esta transa��o requer uma autoriza��o n�vel gerente para que seja conclu�da.') ;
          exit ;
      end ;
  finally

      freeAndNil( objCaixa ) ;

  end ;

  frmMenu.Connection.BeginTrans;

  try
      prProcessaLanctoManual(False, nCdResumoCaixa, StrToInt(Trim(edtTipoLancto.Text)), edtValLancto.Value, edtHistorico.Text);
  except
      frmMenu.Connection.RollbackTrans;
      frmMenu.MensagemErro('Erro no processamento.');
      Raise;
  end;

  frmMenu.Connection.CommitTrans;

  emiteComprovante:
  if (frmMenu.MessageDlg('Deseja emitir o comprovante ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
  begin

      if (qryTipoLanctocFlgEmiteECF.Value = 1) and (cFlgUsaECF) then
      begin
          if not (frmMenu.ACBrECF1.Ativo) then
              frmMenu.ACBrECF1.Ativar;

          if (qryTipoLanctocSinalOper.Value = '-') then
              frmMenu.ACBrECF1.NaoFiscalCompleto(frmMenu.ACBrECF1.AchaCNFDescricao('SANGRIA').Indice, edtValLancto.Value,'01', edtHistorico.Text)
          else frmMenu.ACBrECF1.NaoFiscalCompleto(frmMenu.ACBrECF1.AchaCNFDescricao('SUPRIMENTO').Indice, edtValLancto.Value,'01', edtHistorico.Text)

      end ;

      if (qryTipoLanctocFlgEmiteECF.Value = 0) or not (cFlgUsaECF) then
          ImprimirComprovante(nCdLanctoFin) ;

      if (frmMenu.MessageDlg('O comprovante foi impresso corretamente ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          Goto emiteComprovante;

  end ;

  qryTipoLancto.Close ;

  Close ;
end;

procedure TfrmCaixa_LanctoMan.FormShow(Sender: TObject);
begin

    qryTipoLancto.Close ;
    edtValLancto.Value := 0 ;
    edtTipoLancto.Text := '' ;
    edtHistorico.Text  := '' ;
    edtTipoLancto.SetFocus;

end;

procedure TfrmCaixa_LanctoMan.edtTipoLanctoExit(Sender: TObject);
begin

  qryTipoLancto.Close ;
  qryTipoLancto.Parameters.ParamByName('nPK').Value := frmMenu.convInteiro(edtTipoLancto.Text) ;
  qryTipoLancto.Open ;

end;

procedure TfrmCaixa_LanctoMan.btnCancelarClick(Sender: TObject);
begin

    Close;

end;

procedure TfrmCaixa_LanctoMan.edtTipoLanctoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtHistorico.SetFocus;

end;

procedure TfrmCaixa_LanctoMan.edtValLanctoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        btnConfirmar.SetFocus;

end;

function TfrmCaixa_LanctoMan.ImprimirComprovante(nCdLanctoFin : integer) : boolean ;
begin

  frmMenu.ShowMessage('Prepare a impressora e clique em OK para impress�o do comprovante.') ;

  Result := false ;

  qryLanctoFin.Close ;
  qryLanctoFin.Parameters.paramByName('nPK').Value := nCdLanctoFin ;
  qryLanctoFin.Open ;

  rdPrint1.Abrir ;

  rdprint1.TamanhoQteLPP      := seis ;
  rdprint1.FonteTamanhoPadrao := s10cpp;

  rdprint1.ImpF(01,01,' *** COMPROVANTE CAIXA ***',[Negrito]);
  rdprint1.ImpF(02,01,'- - - - - - - - - - - - - -',[Negrito]);

  rdPrint1.Imp(03,01,'') ;
  rdPrint1.ImpF(04,01,'Lan�amento : ' + qryLanctoFinnCdLanctoFin.AsString,[Normal]) ;
  rdPrint1.Imp(05,01,'') ;
  rdPrint1.ImpF(06,01,'Data Lancto: ' + qryLanctoFindDtLancto.AsString,[Normal]) ;
  rdPrint1.ImpF(07,01,'Tipo       : ' + qryLanctoFincNmTipoLancto.Value,[Normal]) ;
  rdPrint1.ImpF(08,01,'Operador   : ' + qryLanctoFincNmUsuario.Value,[Negrito]) ;
  rdPrint1.ImpF(10,01,'Hist�rico  : ' + qryLanctoFincHistorico.Value,[Normal]) ;
  rdPrint1.Imp(11,01,'') ;
  rdPrint1.ImpF(12,01,'Valor      : ',[Normal]) ;
  rdprint1.IMPD(12,29,formatFloat('###,##0.00',qryLanctoFinnValLancto.Value),[Expandido]);

  rdPrint1.ImpF(14,01,'Hora Atual : ' + DateTimeToStr(Now()),[Normal]) ;
  rdPrint1.ImpF(17,01,'Operador',[Normal]) ;
  rdPrint1.ImpF(19,01,'______________________________',[Normal]) ;
  rdPrint1.ImpF(22,01,'Gerente',[Normal]) ;
  rdPrint1.ImpF(24,01,'______________________________',[Normal]) ;

  if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
  begin
      rdprint1.OpcoesPreview.Preview      := true ;
      rdprint1.OpcoesPreview.Remalina     := false ;
      rdprint1.OpcoesPreview.PaginaZebrada:= false ;
  end ;

  rdPrint1.Fechar ;

  Result := True ;

end;

procedure TfrmCaixa_LanctoMan.edtHistoricoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        edtValLancto.SetFocus;

end;

procedure TfrmCaixa_LanctoMan.imprimirComprovanteEstorno(
  nCdLanctoFin: integer);
begin

  frmMenu.ShowMessage('Prepare a impressora e clique em OK para impress�o do comprovante.') ;

  qryLanctoEstornado.Close ;
  qryLanctoEstornado.Parameters.paramByName('nPK').Value := nCdLanctoFin ;
  qryLanctoEstornado.Open ;

  rdPrint1.Abrir ;

  rdprint1.TamanhoQteLPP      := seis ;
  rdprint1.FonteTamanhoPadrao := s10cpp;

  rdprint1.ImpF(01,01,' *** COMPROVANTE DE ESTORNO ***',[Negrito]);
  rdprint1.ImpF(02,01,'- - - - - - - - - - - - - - - - -',[Negrito]);

  rdPrint1.Imp(03,01,'') ;
  rdPrint1.ImpF(04,01,'Lan�amento    : ' + qryLanctoEstornadonCdLanctoFin.AsString,[Normal]) ;
  rdPrint1.ImpF(05,01,'Loja          : ' + qryLanctoEstornadonCdLoja.AsString,[Normal]) ;
  rdPrint1.ImpF(06,01,'Data Estorno  : ' + qryLanctoEstornadodDtEstorno.AsString,[Normal]) ;
  rdPrint1.ImpF(07,01,'Autorizado por: ' + qryLanctoEstornadocNmUsuario.AsString,[Normal]) ;
  rdPrint1.ImpF(08,01,'Motivo Estorno: ' + Copy(qryLanctoEstornadocMotivoEstorno.Value,1,19),[Normal]) ;
  rdPrint1.ImpF(09,01,Copy(qryLanctoEstornadocMotivoEstorno.Value,20,35),[Normal]) ;
  rdPrint1.ImpF(10,01,Copy(qryLanctoEstornadocMotivoEstorno.Value,55,35),[Normal]) ;
  rdPrint1.ImpF(11,01,Copy(qryLanctoEstornadocMotivoEstorno.Value,90,35),[Normal]) ;
  rdPrint1.ImpF(12,01,Copy(qryLanctoEstornadocMotivoEstorno.Value,125,35),[Normal]) ;

  rdPrint1.Imp(13,01,'') ;
  rdPrint1.ImpF(14,01,'Data Lancto   : ' + qryLanctoEstornadodDtLancto.AsString,[Normal]) ;
  rdPrint1.ImpF(15,01,'Tipo          : ' + qryLanctoEstornadocNmTipoLancto.Value,[Normal]) ;
  rdPrint1.ImpF(16,01,'Operador      : ' + qryLanctoEstornadocNmUsuarioOperador.Value,[Normal]) ;
  rdPrint1.ImpF(17,01,'Hist�rico     : ' + qryLanctoEstornadocHistorico.Value,[Normal]) ;
  rdPrint1.ImpF(18,01,'Valor         : ',[Normal]) ;
  rdprint1.IMPD(18,29,formatFloat('###,##0.00',qryLanctoEstornadonValLancto.Value),[Normal]);

  rdPrint1.ImpF(20,01,'Operador',[Negrito]) ;
  rdPrint1.ImpF(21,01,'______________________________',[Normal]) ;
  rdPrint1.ImpF(23,01,'Gerente',[Negrito]) ;
  rdPrint1.ImpF(24,01,'______________________________',[Normal]) ;

  rdPrint1.ImpF(26,01,'Novo Lan�amento',[Negrito]) ;
  rdPrint1.ImpF(27,01,'______________________________',[Normal]) ;

  rdPrint1.ImpF(29,01,'Informa��o Complementar:',[Negrito]) ;

  if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
  begin
      rdprint1.OpcoesPreview.Preview      := true  ;
      rdprint1.OpcoesPreview.Remalina     := false ;
      rdprint1.OpcoesPreview.PaginaZebrada:= false ;
  end ;

  rdPrint1.Fechar ;

end;

end.
