unit fReabilitacaoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DB, Mask, DBCtrls, ADODB, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmReabilitacaoCliente = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryLojaCobradora: TADOQuery;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    ToolButton4: TToolButton;
    btReabilitar: TToolButton;
    DBGridEh1: TDBGridEh;
    qryReabilitacao: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryReabilitacaocCNPJCPF: TStringField;
    qryReabilitacaocRG: TStringField;
    qryReabilitacaocNmTerceiro: TStringField;
    qryReabilitacaodDtNegativacao: TDateTimeField;
    qryReabilitacaocNrTit: TStringField;
    qryReabilitacaodDtEmissao: TDateTimeField;
    qryReabilitacaodDtVenc: TDateTimeField;
    qryReabilitacaonValTit: TBCDField;
    qryReabilitacaonCdLojaTit: TStringField;
    qryReabilitacaonCdLojaNeg: TStringField;
    qryReabilitacaodDtReabNeg: TDateTimeField;
    dsReabilitacao: TDataSource;
    qryReabilitacaonCdTerceiro: TIntegerField;
    qryReabilitacaonCdTitulo: TIntegerField;
    qryReabilitacaodDtNegativacao_1: TDateTimeField;
    qryReabilitacaoiParcela: TIntegerField;
    qryReabilitar: TADOQuery;
    ToolButton5: TToolButton;
    SP_VERIFICA_REABILITACAO_CLIENTE: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure btReabilitarClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReabilitacaoCliente: TfrmReabilitacaoCliente;

implementation

uses fMenu, rTituloReabilitado_view;

{$R *.dfm}

procedure TfrmReabilitacaoCliente.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;

  btReabilitar.Enabled := False ;

  DBGridEh1.Columns[5].Visible := (frmMenu.nCdLojaAtiva = 0) ;
  
  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      qryLojaCobradora.Close ;
      PosicionaQuery(qryLojaCobradora, intToStr(frmMenu.nCdLojaAtiva)) ;
  end
  else GroupBox1.Visible := False ;
  
end;

procedure TfrmReabilitacaoCliente.ToolButton1Click(Sender: TObject);
var
    cModoReab : string ;
begin
  inherited;

  btReabilitar.Enabled := False ;

  SP_VERIFICA_REABILITACAO_CLIENTE.Close;
  SP_VERIFICA_REABILITACAO_CLIENTE.ExecProc;
  
  qryPreparaTemp.ExecSQL;

  qryReabilitacao.Close;
  qryReabilitacao.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryReabilitacao.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryReabilitacao.Open;

  if (qryReabilitacao.Eof) then
      MensagemAlerta('Nenhum cliente disponível para reabilitação.')
  else btReabilitar.Enabled := True ;

end;

procedure TfrmReabilitacaoCliente.btReabilitarClick(Sender: TObject);
var
  objRel : TrptTituloReabilitado_view;
begin
  inherited;

  if (MessageDlg('Confirma a reabilitação destes clientes ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryReabilitar.Close;
      qryReabilitar.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Clientes reabilitados com sucesso.') ;

  objRel := TrptTituloReabilitado_view.Create(nil);

  try
      try
          objRel.qryTitulos.Close;
          objRel.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          objRel.qryTitulos.Parameters.ParamByName('dDtInicial').Value := DateToStr(Date);
          objRel.qryTitulos.Parameters.ParamByName('dDtFinal').Value   := DateToStr(Date);
          objRel.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value := frmMenu.nCdLojaAtiva;
          objRel.qryTitulos.Parameters.ParamByName('nCdLoja').Value    := 0;
          objRel.qryTitulos.Parameters.ParamByName('nCdTerceiro').Value:= 0;
          objRel.qryTitulos.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Reabilitados em : ' + DateToStr(Date) ;

          if (frmMenu.nCdLojaAtiva > 0) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Loja Cobradora: ' + frmMenu.ZeroEsquerda(qryLojaCobradoranCdLoja.AsString,3) + '-' + qryLojaCobradoracNmLoja.Value ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na criação da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      freeAndNil(objRel) ;
  end;

  qryReabilitacao.Close;
  btReabilitar.Enabled := False ;

end;

procedure TfrmReabilitacaoCliente.ToolButton5Click(Sender: TObject);
var
  objRel : TrptTituloReabilitado_view;
begin
  inherited;

  objRel := TrptTituloReabilitado_view.Create(nil);

  try
      try
          objRel.qryTitulos.Close;
          objRel.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          objRel.qryTitulos.Parameters.ParamByName('dDtInicial').Value := DateToStr(Date);
          objRel.qryTitulos.Parameters.ParamByName('dDtFinal').Value   := DateToStr(Date);
          objRel.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value := frmMenu.nCdLojaAtiva;
          objRel.qryTitulos.Parameters.ParamByName('nCdLoja').Value    := 0;
          objRel.qryTitulos.Parameters.ParamByName('nCdTerceiro').Value:= 0;
          objRel.qryTitulos.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Reabilitados em : ' + DateToStr(Date) ;

          if (frmMenu.nCdLojaAtiva > 0) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Loja Cobradora: ' + frmMenu.ZeroEsquerda(qryLojaCobradoranCdLoja.AsString,3) + '-' + qryLojaCobradoracNmLoja.Value ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na criação da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      freeAndNil(objRel) ;
  end;

end;

initialization
    RegisterClass(TfrmReabilitacaoCliente) ;

end.
