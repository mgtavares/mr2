unit rFluxoCaixaNovo_Tela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, StdCtrls, DB, Mask, DBCtrls, ADODB,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TrptFluxoCaixaNovo_Tela = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox4: TGroupBox;
    DBGridEh2: TDBGridEh;
    qryMovAnalitico: TADOQuery;
    qryMovAnaliticodDtInicial: TDateTimeField;
    qryMovAnaliticodDtFinal: TDateTimeField;
    qryMovAnaliticoiPeriodo: TIntegerField;
    qryMovAnaliticodDtVenc: TDateTimeField;
    qryMovAnaliticonCdTitulo: TIntegerField;
    qryMovAnaliticocNrTit: TStringField;
    qryMovAnaliticocNmEspTit: TStringField;
    qryMovAnaliticocNmTerceiro: TStringField;
    qryMovAnaliticocNmCategFinanc: TStringField;
    qryMovAnaliticonSaldoTit: TBCDField;
    qryMovAnaliticonValTotalPagar: TBCDField;
    qryMovAnaliticonValTotalReceber: TBCDField;
    qryMovAnaliticocSenso: TStringField;
    qryExpectativaCaixa: TADOQuery;
    qryExpectativaCaixanValTotalAtrasReceber: TBCDField;
    qryExpectativaCaixanValTotalAtrasPagar: TBCDField;
    qryExpectativaCaixanValDispTotal: TBCDField;
    qryExpectativaCaixanValDispImediata: TBCDField;
    qryExpectativaCaixanValTotalReceber: TBCDField;
    qryExpectativaCaixanValTotalPagar: TBCDField;
    qryExpectativaCaixanValSaldoSemLimCred: TBCDField;
    qryExpectativaCaixanValLimiteCred: TBCDField;
    qryExpectativaCaixanSaldoTotal: TBCDField;
    qryDemonstraMovFutura: TADOQuery;
    qryDemonstraMovFuturadDtInicial: TDateTimeField;
    qryDemonstraMovFuturadDtFinal: TDateTimeField;
    qryDemonstraMovFuturanValTotalReceber: TBCDField;
    qryDemonstraMovFuturanValTotalPagar: TBCDField;
    qryDemonstraMovFuturanValSaldoAcumulado: TBCDField;
    qryDemonstraMovFuturanValTotalReceberPeriodo: TBCDField;
    qryDemonstraMovFuturanValTotalPagarPeriodo: TBCDField;
    qryPosCaixaAtual: TADOQuery;
    qryPosCaixaAtualcEmpresaLoja: TStringField;
    qryPosCaixaAtualcBanco: TStringField;
    qryPosCaixaAtualcAgencia: TStringField;
    qryPosCaixaAtualnCdConta: TStringField;
    qryPosCaixaAtualnSaldoConta: TBCDField;
    qryPosCaixaAtualdDtSaldo: TDateTimeField;
    qryPosCaixaAtualnValLimite: TBCDField;
    qryPosCaixaAtualnSaldoTotal: TBCDField;
    qryPosCaixaAtualnValLimiteTotal: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsExpectativaCaixa: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    dsPosCaixaAtual: TDataSource;
    dsDemonstraMovFutura: TDataSource;
    dsMovAnalitico: TDataSource;
    GroupBox3: TGroupBox;
    DBGridEh1: TDBGridEh;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1dDtInicial: TcxGridDBColumn;
    cxGrid1DBTableView1dDtFinal: TcxGridDBColumn;
    cxGrid1DBTableView1iPeriodo: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotalPagar: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotalReceber: TcxGridDBColumn;
    cxGrid1DBTableView1cSenso: TcxGridDBColumn;
    qryDemonstraMovFuturacPeriodo: TStringField;
    procedure FormShow(Sender: TObject);
    procedure qryDemonstraMovFuturaAfterScroll(DataSet: TDataSet);
    procedure qryDemonstraMovFuturaCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFluxoCaixaNovo_Tela: TrptFluxoCaixaNovo_Tela;

implementation

{$R *.dfm}

procedure TrptFluxoCaixaNovo_Tela.FormShow(Sender: TObject);
begin
  inherited;

  qryPosCaixaAtual.Close;
  qryPosCaixaAtual.Open;

  qryMovAnalitico.Close;
  qryMovAnalitico.Open;

  qryDemonstraMovFutura.Close;
  qryDemonstraMovFutura.Open;

  qryExpectativaCaixa.Close;
  qryExpectativaCaixa.Open;

end;

procedure TrptFluxoCaixaNovo_Tela.qryDemonstraMovFuturaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryMovAnalitico.Filter := 'dDtInicial = ' + qryDemonstraMovFuturadDtInicial.AsString;
end;

procedure TrptFluxoCaixaNovo_Tela.qryDemonstraMovFuturaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryDemonstraMovFuturacPeriodo.Value := qryDemonstraMovFuturadDtInicial.AsString + ' a ' + qryDemonstraMovFuturadDtFinal.AsString;

end;

end.
