unit fRecebimento_Coferencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, cxPC;

type
  TfrmRecebimento_Conferencia = class(TfrmProcesso_Padrao)
    qryProdutoGrade: TADOQuery;
    dsProdutoGrade: TDataSource;
    qryProdutoGradenCdItemRecebimento: TIntegerField;
    qryProdutoGradecNmProdutoPai: TStringField;
    qryProdutoGradecNmProduto: TStringField;
    qryProdutoGradenQtde: TBCDField;
    qryProdutoGradenQtdeConf: TBCDField;
    qryProdutoGradenSaldo: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryProdutoSemGrade: TADOQuery;
    dsProdutoSemGrade: TDataSource;
    qryProdutoSemGradecNmProduto: TStringField;
    qryProdutoSemGradenCdProduto: TIntegerField;
    qryProdutoSemGradenQtde: TBCDField;
    qryProdutoSemGradenQtdeConf: TBCDField;
    qryProdutoSemGradenSaldo: TBCDField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    qryProdutoGradecFlgDivergencia: TStringField;
    qryProdutoSemGradecFlgDivergencia: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRecebimento_Conferencia: TfrmRecebimento_Conferencia;

implementation

{$R *.dfm}

end.
