unit fCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons,
  GridsEh, DBGridEh, cxPC, cxControls, DBGridEhGrouping;

type
  TfrmCheque = class(TfrmCadastro_Padrao)
    qryMastercConta: TStringField;
    qryMasteriNrCheque: TIntegerField;
    qryMastercCNPJCPF: TStringField;
    qryMasternValCheque: TBCDField;
    qryMasterdDtDeposito: TDateTimeField;
    qryMasternCdTerceiroResp: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMastercNmTerceiroPort: TStringField;
    qryMasterdDtRemessaPort: TDateTimeField;
    qryMasterdDtDevol: TDateTimeField;
    qryMasternCdCheque: TAutoIncField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    btChequeDevolv: TcxButton;
    qryMasternCdTabTipoCheque: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TStringField;
    qryMastercConta_1: TStringField;
    qryMastercDigito: TStringField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmLoja: TStringField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdResumoCaixa: TAutoIncField;
    qryMasterdDtAbertura: TDateTimeField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasternCdUsuario: TIntegerField;
    qryMastercNmUsuario: TStringField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryMasterdDtSegDevol: TDateTimeField;
    Label7: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label12: TLabel;
    DBEdit16: TDBEdit;
    Label14: TLabel;
    DBEdit17: TDBEdit;
    Label15: TLabel;
    DBEdit18: TDBEdit;
    Label16: TLabel;
    DBEdit19: TDBEdit;
    GroupBox1: TGroupBox;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label17: TLabel;
    DBEdit20: TDBEdit;
    Label18: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    Label20: TLabel;
    DBEdit23: TDBEdit;
    Label21: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    qryMasternCdTabStatusCheque: TIntegerField;
    qryMastercNmTabStatusCheque: TStringField;
    Label19: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    qryHistStatusCheque: TADOQuery;
    qryHistStatusChequenCdHistStatusCheque: TIntegerField;
    qryHistStatusChequenCdTabStatusCheque: TIntegerField;
    qryHistStatusChequecNmTabStatusCheque: TStringField;
    qryHistStatusChequedDtStatus: TDateTimeField;
    qryHistStatusChequenCdUsuario: TIntegerField;
    qryHistStatusChequecNmUsuario: TStringField;
    qryHistStatusChequecOBS: TMemoField;
    dsHistStatusCheque: TDataSource;
    GroupBox3: TGroupBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    qryInseriHistorico: TADOQuery;
    qryInseriHistoriconCdHistStatusCheque: TAutoIncField;
    qryInseriHistoriconCdCheque: TIntegerField;
    qryInseriHistoriconCdTabStatusCheque: TIntegerField;
    qryInseriHistoriconCdUsuario: TIntegerField;
    qryInseriHistoricodDtStatus: TDateTimeField;
    qryInseriHistoricocOBS: TMemoField;
    qryMasterdDtStatus: TDateTimeField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBMemo1: TDBMemo;
    Label22: TLabel;
    cxButton9: TcxButton;
    DBGridEh5: TDBGridEh;
    DBMemo2: TDBMemo;
    DataSource13: TDataSource;
    qryFollowUp: TADOQuery;
    qryFollowUpdDtFollowUp: TDateTimeField;
    qryFollowUpcNmUsuario: TStringField;
    qryFollowUpcOcorrenciaResum: TStringField;
    qryFollowUpdDtProxAcao: TDateTimeField;
    qryFollowUpcOcorrencia: TMemoField;
    qryMastercOBSCheque: TStringField;
    Label23: TLabel;
    DBEdit28: TDBEdit;
    cxButton10: TcxButton;
    SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL: TADOStoredProc;
    procedure btChequeDevolvClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure GravaHistorico(nCdTabStatusCheque: integer);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure DBMemo1KeyPress(Sender: TObject; var Key: Char);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCheque: TfrmCheque;

implementation

uses fChequeDevolvido, fMenu, fCheque_TrocarProtador;

{$R *.dfm}

procedure TfrmCheque.btChequeDevolvClick(Sender: TObject);
begin


    if not qryMaster.Active then
    begin
        ShowMessage('Nenhum cheque ativo.') ;
        exit ;
    end ;

    if (qryMasternCdTabTipoCheque.Value <> 2) then
    begin
        ShowMessage('Este tipo de cheque n�o pode receber o movimento de devolvido.') ;
        exit ;
    end ;

    if (qryMasterdDtDevol.AsString <> '') then
    begin
        ShowMessage('Cheque j� foi devolvido.') ;
        exit ;
    end ;

    frmChequeDevolvido.CheckBox1.Checked := True ;
    frmChequeDevolvido.MaskEdit1.Text    := '' ;
    frmChequeDevolvido.MaskEdit2.Text    := '' ;
    frmChequeDevolvido.MaskEdit3.Text    := '' ;

    frmChequeDevolvido.nCdCheque := qryMasternCdCheque.Value ;

    frmChequeDevolvido.ShowModal;

    PosicionaQuery(qryMaster, qryMasternCdCheque.asString) ;

end;

procedure TfrmCheque.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CHEQUE' ;
  nCdTabelaSistema  := 32 ;
  nCdConsultaPadrao := 67 ;
end;

procedure TfrmCheque.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemAlerta('Este cheque n�o pertence a essa empresa.') ;
      btCancelar.Click;
      abort ;
  end ;

  qryHistStatusCheque.Close ;
  PosicionaQuery(qryHistStatusCheque, qryMasternCdCheque.asString) ;

  qryFollowUp.Close ;
  Posicionaquery(qryFollowUp, qryMasternCdCheque.asString) ;

  GroupBox3.Enabled := True ;
  
end;

procedure TfrmCheque.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryHistStatusCheque.Close ;
  qryFollowUp.Close ;
  cxButton9.Enabled := False ;

end;

procedure TfrmCheque.GravaHistorico(nCdTabStatusCheque: integer);
begin

  frmMenu.Connection.BeginTrans ;

  try
      qryInseriHistorico.Close ;
      qryInseriHistorico.Open  ;

      qryInseriHistorico.Insert ;
      qryInseriHistoriconCdHistStatusCheque.Value := frmMenu.fnProximoCodigo('HISTSTATUSCHEQUE') ;
      qryInseriHistoriconCdCheque.Value           := qryMasternCdCheque.Value ;
      qryInseriHistoriconCdTabStatusCheque.Value  := nCdTabStatusCheque ;
      qryInseriHistoriconCdUsuario.Value          := frmMenu.nCdUsuarioLogado ;
      qryInseriHistoricodDtStatus.Value           := Now() ;
      qryInseriHistorico.Post ;

      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.Close;
      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.Parameters.ParamByName('@nCdTerceiro').Value := qryMasternCdTerceiroResp.Value;
      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end ;

procedure TfrmCheque.cxButton1Click(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtSegDevol.AsString <> '') then
  begin

      case MessageDlg('Este cheque j� voltou duas vezes. Confirma o envio para dep�sito novamente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

  end ;

  if (qryMasternCdTabStatusCheque.Value = 1) then
  begin
      MensagemAlerta('O cheque j� est� com status de dispon�vel para dep�sito.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(1) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton2Click(Sender: TObject);
begin
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 4) then
  begin
      MensagemAlerta('O cheque j� est� com status de negociado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(4) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton9Click(Sender: TObject);
begin
  inherited;

  if (qryHistStatusCheque.State = dsEdit) then
  begin
      qryHistStatusCheque.Post ;
      DBGridEh1.SetFocus;
      cxButton9.Enabled := False ;
  end ;

end;

procedure TfrmCheque.DBMemo1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (qryHistStatusCheque.Active) then
      cxButton9.Enabled := True ;

end;

procedure TfrmCheque.cxButton3Click(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 5) then
  begin
      MensagemAlerta('O cheque j� est� com status de primeira devolu��o.') ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryMaster.Edit ;
  qryMasterdDtDevol.Value := StrToDate(DateToStr(Now())) ;
  qryMaster.Post ;

  GravaHistorico(5) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton4Click(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 6) then
  begin
      MensagemAlerta('O cheque j� est� com status de segunda devolu��o.') ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryMaster.Edit ;
  qryMasterdDtSegDevol.Value := StrToDate(DateToStr(Now())) ;
  qryMaster.Post ;

  GravaHistorico(6) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton5Click(Sender: TObject);
begin
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 7) then
  begin
      MensagemAlerta('O cheque j� est� com status de devolvido para o respons�vel.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(7) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton6Click(Sender: TObject);
begin
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 8) then
  begin
      MensagemAlerta('O cheque j� est� com status de enviado para loja.') ;
      exit ;
  end ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      MensagemAlerta('Fun��o dispon�vel apenas para varejistas.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(8) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton7Click(Sender: TObject);
begin
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 10) then
  begin
      MensagemAlerta('O cheque j� est� com status de protesto.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(10) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

procedure TfrmCheque.cxButton8Click(Sender: TObject);
var
  objForm : TfrmCheque_TrocarProtador ;
begin

  if (not qryMaster.Active) then
  begin
    MensagemAlerta('Nenhum cheque ativo.') ;
    exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      objForm := TfrmCheque_TrocarProtador.Create(Self) ;

      objForm.nCdCheque := qryMasternCdCheque.Value ;
      showForm( objForm , TRUE ) ;

      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.Close;
      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.Parameters.ParamByName('@nCdTerceiro').Value := qryMasternCdTerceiroResp.Value;
      SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;
  
  PosicionaQuery(qryMaster, qryMasternCdCheque.asString) ;
end;

procedure TfrmCheque.cxButton10Click(Sender: TObject);
begin
  inherited;
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum cheque ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusCheque.Value = 3) then
  begin
      MensagemAlerta('O cheque j� est� com status de compensado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o do status do cheque ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  GravaHistorico(3) ;
  PosicionaQuery(qryMaster, qryMasternCdCheque.AsString) ;

end;

initialization
    RegisterClass(TfrmCheque) ;

end.
