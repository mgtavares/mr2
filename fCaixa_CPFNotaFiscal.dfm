object frmCaixa_CPFNotaFiscal: TfrmCaixa_CPFNotaFiscal
  Left = 402
  Top = 375
  Width = 272
  Height = 102
  BorderIcons = []
  Caption = 'Nota Fiscal Paulista'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object frmCaixa_CPFNotaFiscal: TLabel
    Left = 8
    Top = 8
    Width = 208
    Height = 21
    Caption = 'CPF/CNPJ Nota Fiscal Paulista'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object edtCPF: TEdit
    Left = 8
    Top = 32
    Width = 241
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = 'edtCPF'
    OnKeyPress = edtCPFKeyPress
  end
end
