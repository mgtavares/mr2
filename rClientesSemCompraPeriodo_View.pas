unit rClientesSemCompraPeriodo_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, DB, StdCtrls, Mask, DBCtrls, ADODB;

type
  TrptClientesSemCompraPeriodo_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro2: TQRLabel;
    lblFiltro4: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRGroup1: TQRGroup;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRBand4: TQRBand;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatoriocNmFantasia: TStringField;
    uspRelatoriocNmRegiao: TStringField;
    uspRelatoriocTelefone1: TStringField;
    uspRelatoriocTelefone2: TStringField;
    uspRelatoriocTelefoneMovel: TStringField;
    uspRelatoriocNmContato: TStringField;
    uspRelatoriocNmRamoAtividade: TStringField;
    DBEdit1: TDBEdit;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel8: TQRLabel;
    lblFiltro5: TQRLabel;
    lblFiltro6: TQRLabel;
    uspRelatorioDtUltimaCompra: TDateTimeField;
    QRLabel1: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape3: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel14: TQRLabel;
    uspRelatorionValPedido: TBCDField;
    QRDBText13: TQRDBText;
    lblFiltro7: TQRLabel;
    lblFiltro8: TQRLabel;
    QRSysData4: TQRSysData;
    PageFooterBand1: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel11: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptClientesSemCompraPeriodo_View: TrptClientesSemCompraPeriodo_View;

implementation

{$R *.dfm}

end.
