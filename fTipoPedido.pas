unit fTipoPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  RDprint, DBGridEhGrouping, ER2Lookup, ToolCtrlsEh;

type
  TfrmTipoPedido = class(TfrmCadastro_Padrao)
    qryMasternCdTipoPedido: TIntegerField;
    qryMastercNmTipoPedido: TStringField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMastercFlgCalcImposto: TIntegerField;
    qryMastercFlgComporRanking: TIntegerField;
    qryMastercFlgVenda: TIntegerField;
    qryMastercGerarFinanc: TIntegerField;
    qryMastercExigeAutor: TIntegerField;
    qryMasternCdEspTitPrev: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryTabTipoPedido: TADOQuery;
    qryTabTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTabTipoPedidocNmTabTipoPedido: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitPrev: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitPrevnCdEspTit: TIntegerField;
    qryEspTitPrevcNmEspTit: TStringField;
    qryEspTitPrevcTipoMov: TStringField;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryMastercNmTabTipoPedido: TStringField;
    qryMastercNmEspTitPrev: TStringField;
    qryMastercNmEspTit: TStringField;
    qryMastercNmCategFinanc: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    qryCategFinanccFlgRecDes: TStringField;
    qryUsuarioTipoPedido: TADOQuery;
    dsUsuarioTipoPedido: TDataSource;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioTipoPedidonCdUsuario: TIntegerField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuarioTipoPedidocNmUsuario: TStringField;
    qryUsuarioTipoPedidoAutor: TADOQuery;
    dsUsuarioTipoPedidoAutor: TDataSource;
    qryUsuarioTipoPedidoAutornCdTipoPedido: TIntegerField;
    qryUsuarioTipoPedidoAutornCdUsuario: TIntegerField;
    qryUsuarioTipoPedidoAutorcNmUsuario: TStringField;
    qryGrupoProdutoTipoPedido: TADOQuery;
    qryGrupoProduto: TADOQuery;
    dsGrupoProdutoTipoPedido: TDataSource;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    qryGrupoProdutoTipoPedidonCdTipoPedido: TIntegerField;
    qryGrupoProdutoTipoPedidonCdGrupoProduto: TIntegerField;
    qryGrupoProdutoTipoPedidocNmGrupoProduto: TStringField;
    qryMastercFlgAtuPreco: TIntegerField;
    qryMastercFlgItemEstoque: TIntegerField;
    qryMastercFlgItemAD: TIntegerField;
    qryMastercFlgExibeAcomp: TIntegerField;
    qryMastercFlgCompra: TIntegerField;
    qryMastercFlgItemFormula: TIntegerField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMastercCFOPEstadual: TStringField;
    qryMastercCFOPInter: TStringField;
    qryMastercTextoCFOPEstadual: TStringField;
    qryMastercTextoCFOPInter: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryMastercNmUnidadeNegocio: TStringField;
    qryMasternCdGrupoPedido: TIntegerField;
    qryGrupoPedido: TADOQuery;
    qryGrupoPedidonCdGrupoPedido: TIntegerField;
    qryGrupoPedidocNmGrupoPedido: TStringField;
    qryGrupoPedidocFlgFaturar: TIntegerField;
    qryMastercNmGrupoPedido: TStringField;
    Label8: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    qryMastercFlgAguardarMontagem: TIntegerField;
    qryTipoPedidoTipoTerceiro: TADOQuery;
    dsTipoPedidoTipoTerceiro: TDataSource;
    qryTipoPedidoTipoTerceironCdTipoPedido: TIntegerField;
    qryTipoPedidoTipoTerceironCdTipoTerceiro: TIntegerField;
    qryTipoTerceiro: TADOQuery;
    qryTipoTerceironCdTipoTerceiro: TIntegerField;
    qryTipoTerceirocNmTipoTerceiro: TStringField;
    qryTipoPedidoTipoTerceirocNmTipoTerceiro: TStringField;
    qryMasternCdOperacaoEstoque: TIntegerField;
    qryOperacaoEstoque: TADOQuery;
    qryOperacaoEstoquenCdOperacaoEstoque: TIntegerField;
    qryOperacaoEstoquecNmOperacaoEstoque: TStringField;
    dsOperacaoEstoque: TDataSource;
    qryMastercNmOperacaoEstoque: TStringField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    StaticText3: TStaticText;
    DBGridEh1: TDBGridEh;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    DBGridEh2: TDBGridEh;
    StaticText4: TStaticText;
    DBGridEh4: TDBGridEh;
    Image2: TImage;
    cxTabSheet2: TcxTabSheet;
    Image3: TImage;
    qryCCTipoPedido: TADOQuery;
    dsCCTipoPedido: TDataSource;
    qryCCTipoPedidonCdTipoPedido: TIntegerField;
    qryCCTipoPedidonCdCC: TIntegerField;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCCTipoPedidocNmCC: TStringField;
    DBGridEh5: TDBGridEh;
    qryMastercLimiteCredito: TIntegerField;
    qryMastercMsgNF: TStringField;
    qryMasternCdNaturezaOperacao: TIntegerField;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMastercFlgOrcamento: TIntegerField;
    qryMastercFlgDevolucVenda: TIntegerField;
    qryMastercFlgDevolucCompra: TIntegerField;
    qryMastercFlgCalcComissao: TIntegerField;
    qryMastercFlgContabilizar: TIntegerField;
    qryMastercFlgExibirPDV: TIntegerField;
    cxTabSheet3: TcxTabSheet;
    Image4: TImage;
    qryMastercRevisaoPedido: TStringField;
    qryMastercCodNBR: TStringField;
    qryMastercCriterioAceite: TMemoField;
    Label10: TLabel;
    DBEdit17: TDBEdit;
    Label11: TLabel;
    DBEdit18: TDBEdit;
    Label12: TLabel;
    DBEdit19: TDBEdit;
    Label13: TLabel;
    DBMemo1: TDBMemo;
    qryMastercObjetivoPedido: TStringField;
    qryMastercFlgBaixaManual: TIntegerField;
    RDprint1: TRDprint;
    qryMastercFlgPermAbatDevVenda: TIntegerField;
    qryUsuarioTipoPedidonCdUsuarioTipoPedido: TIntegerField;
    qryUsuarioTipoPedidoAutornCdUsuarioTipoPedidoAutor: TIntegerField;
    qryGrupoProdutoTipoPedidonCdGrupoProdutoTipoPedido: TIntegerField;
    qryTipoPedidoTipoTerceironCdTipoPedidoTipoTerceiro: TIntegerField;
    qryMastercFlgRetemPisCofins: TIntegerField;
    qryMastercFlgGeraOP: TIntegerField;
    qryMasternCdTipoOP: TIntegerField;
    cxTabSheet4: TcxTabSheet;
    Image5: TImage;
    DBCheckBox22: TDBCheckBox;
    Label14: TLabel;
    ER2LookupDBEdit1: TER2LookupDBEdit;
    qryTipoOP: TADOQuery;
    qryTipoOPcNmTipoOP: TStringField;
    DBEdit20: TDBEdit;
    DataSource1: TDataSource;
    qryMasternCdCCPedido: TIntegerField;
    qryCCusto: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    qryMastercNmCCusto: TStringField;
    cxTabSheet5: TcxTabSheet;
    Image6: TImage;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    DBCheckBox12: TDBCheckBox;
    DBCheckBox13: TDBCheckBox;
    DBCheckBox14: TDBCheckBox;
    DBCheckBox15: TDBCheckBox;
    DBCheckBox16: TDBCheckBox;
    DBCheckBox17: TDBCheckBox;
    DBCheckBox18: TDBCheckBox;
    DBCheckBox19: TDBCheckBox;
    DBCheckBox20: TDBCheckBox;
    DBCheckBox21: TDBCheckBox;
    DBCheckBoxPisCofins: TDBCheckBox;
    cxTabSheet6: TcxTabSheet;
    Image7: TImage;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label15: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    cxTabSheet7: TcxTabSheet;
    Image8: TImage;
    cxPageControl2: TcxPageControl;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    qryRegraPISCOFINSIPI: TADOQuery;
    qryRegraPISCOFINSIPInCdRegraPISCOFINSIPI: TIntegerField;
    qryRegraPISCOFINSIPInCdTipoPedido: TIntegerField;
    qryRegraPISCOFINSIPInCdEmpresa: TIntegerField;
    qryRegraPISCOFINSIPInCdLoja: TIntegerField;
    qryRegraPISCOFINSIPInCdGrupoImposto: TIntegerField;
    qryRegraPISCOFINSIPIcCSTIPI: TStringField;
    qryRegraPISCOFINSIPIcCdEnquadramento: TStringField;
    qryRegraPISCOFINSIPIcCSTPIS: TStringField;
    qryRegraPISCOFINSIPInAliqPIS: TBCDField;
    qryRegraPISCOFINSIPIcCSTCOFINS: TStringField;
    qryRegraPISCOFINSIPInAliqCOFINS: TBCDField;
    DBGridEh6: TDBGridEh;
    dsRegraPISCOFINSIPI: TDataSource;
    qryRegraPISCOFINSIPIcNmGrupoImposto: TStringField;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostocNmGrupoImposto: TStringField;
    qryGrupoImpostocNCM: TStringField;
    qryRegraPISCOFINSIPIcNCM: TStringField;
    qryTipoTributacaoIPI: TADOQuery;
    qryTipoTributacaoIPInCdTipoTributacaoIPI: TIntegerField;
    qryTipoTributacaoIPIcCdStIPI: TStringField;
    qryTipoTributacaoIPIcNmTipoTributacaoIPI: TStringField;
    qryTipoTributacaoPISCOFINS: TADOQuery;
    qryTipoTributacaoPISCOFINSnCdTipoTributacaoPISCOFINS: TIntegerField;
    qryTipoTributacaoPISCOFINScCdStPISCOFINS: TStringField;
    qryTipoTributacaoPISCOFINScNmTipoTributacaoPISCOFINS: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryGrupoImpostonAliqIPI: TBCDField;
    qryMastercFlgExigeRepres: TIntegerField;
    DBCheckBox23: TDBCheckBox;
    qryMastercFlgRateiaCategFinanc: TIntegerField;
    qryMastercFlgApurarCMV: TIntegerField;
    qryMastercFlgGeraValeCompraWeb: TIntegerField;
    qryMastercFlgAbatSaldoTit: TIntegerField;
    DBCheckBox24: TDBCheckBox;
    DBCheckBox25: TDBCheckBox;
    qryMastercFlgTipoPedidoWeb: TIntegerField;
    DBCheckBox26: TDBCheckBox;
    qryRegraPISCOFINSIPIcNmEmpresa: TStringField;
    qryRegraPISCOFINSIPIcNmLoja: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryLojacNmLoja: TStringField;
    qryOperacaoEstoquecSinalOper: TStringField;
    qryMastercFlgTipoPedidoCD: TIntegerField;
    DBCheckBox27: TDBCheckBox;
    DBCheckBox28: TDBCheckBox;
    qryMastercFlgEmitirETQFat: TIntegerField;
    DBCheckBox29: TDBCheckBox;
    qryMastercFlgBloqEstNeg: TIntegerField;
    DBCheckBox30: TDBCheckBox;
    qryMastercFlgTransf: TIntegerField;
    qryMastercFlgGeraSolicitCD: TIntegerField;
    DBCheckBox31: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryUsuarioTipoPedidoCalcFields(DataSet: TDataSet);
    procedure qryUsuarioTipoPedidoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioTipoPedidoAutorBeforePost(DataSet: TDataSet);
    procedure qryUsuarioTipoPedidoAutorCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryGrupoProdutoTipoPedidoBeforePost(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterInsert(DataSet: TDataSet);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTipoPedidoTipoTerceiroBeforePost(DataSet: TDataSet);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCCTipoPedidoBeforePost(DataSet: TDataSet);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCCTipoPedidoCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryRegraPISCOFINSIPIBeforePost(DataSet: TDataSet);
    procedure qryRegraPISCOFINSIPInCdGrupoImpostoChange(Sender: TField);
    procedure qryRegraPISCOFINSIPICalcFields(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBGridEh3Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure DBGridEh5Enter(Sender: TObject);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoPedido: TfrmTipoPedido;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoPedido.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOPEDIDO' ;
  nCdTabelaSistema  := 29 ;
  nCdConsultaPadrao := 54 ;
  bLimpaAposSalvar  := False;
end;

procedure TfrmTipoPedido.btIncluirClick(Sender: TObject);
begin
  inherited;
  qryMastercFlgCalcImposto.Value      := 0 ;
  qryMastercFlgComporRanking.Value    := 0 ;
  qryMastercFlgVenda.Value            := 0 ;
  qryMastercGerarFinanc.Value         := 0 ;
  qryMastercExigeAutor.Value          := 0 ;
  qryMastercFlgAtuPreco.Value         := 0 ;
  qryMastercFlgItemEstoque.Value      := 0 ;
  qryMastercFlgItemAD.Value           := 0 ;
  qryMastercFlgExibeAcomp.Value       := 0 ;
  qryMastercFlgCompra.Value           := 0 ;
  qryMastercFlgItemFormula.Value      := 0 ;
  qryMastercFlgAguardarMontagem.Value := 0 ;
  qryMastercFlgOrcamento.Value        := 0 ;
  qryMastercFlgDevolucVenda.Value     := 0 ;
  qryMastercFlgDevolucCompra.Value    := 0 ;
  qryMastercFlgCalcComissao.Value     := 0 ;
  qryMastercFlgContabilizar.Value     := 0 ;
  qryMastercFlgExibirPDV.Value        := 0 ;
  qryMastercFlgBaixaManual.Value      := 0 ;
  qryMastercLimiteCredito.Value       := 0 ;
  qryMastercFlgPermAbatDevVenda.Value := 0 ;

  DBEdit2.SetFocus;
end;

procedure TfrmTipoPedido.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(55);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(57);

            If (nPK > 0) then
            begin
                qryMasternCdEspTitPrev.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(56);

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoPedido.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(13);

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryMasterBeforePost(DataSet: TDataSet);
begin

  if not qryEspTit.Active then
      qryEspTit.Open ;

  if not qryCategFinanc.Active then
      qryCategFinanc.Open ;

  if (qryMastercGerarFinanc.Value = 1) and (qryMasternCdEspTit.Value = 0) then
  begin
      ShowMessage('Informe uma esp�cie de t�tulo.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  if (qryMasternCdCCPedido.Value > 0) and (Trim(qryMastercNmCCusto.Value) = '') then
  begin
      ShowMessage('Informe um Centro Custo V�lido.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit21.SetFocus ;
      Abort ;
  end ;

  if (qryMasternCdEspTit.Value <> 0) and (qryMasternCdCategFinanc.Value = 0) then
  begin
      ShowMessage('Informe a Categoria Financeira.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit6.SetFocus;
      Abort ;
  end ;

  if (qryMasternCdEspTit.Value = 0) and (qryMasternCdCategFinanc.Value <> 0) then
  begin
      ShowMessage('Informe a Esp�cie de T�tulo.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if (qryMasternCdEspTit.Value <> 0) and (qryMasternCdCategFinanc.Value <> 0) then
  begin
      qryEspTit.Locate('nCdEspTit',qryMasternCdEspTit.Value,[]) ;
      qryCategFinanc.Locate('nCdCategFinanc',qryMasternCdCategFinanc.Value,[]) ;

      if    ((qryEspTitcTipoMov.Value = 'R') and (qryCategFinanccFlgRecDes.Value <> 'R'))
         or ((qryEspTitcTipoMov.Value = 'P') and (qryCategFinanccFlgRecDes.Value <> 'D')) then
      begin

          ShowMessage('Esp�cie de T�tulo e Categoria Financeira com Contexto Diferente.') ;
          ShowMessage('Utilize uma Esp�cie de T�tulo de Pagamento com uma Categoria Financeira de Despesas') ;
          Abort ;

      end ;

  end ;

  if (qryMastercFlgContabilizar.Value = 1) and (DBEdit8.Text = '') then
  begin
      MensagemAlerta('Para pedidos que ser�o contabilizados � obrigat�ria a selec�o de uma categoria financeira.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgVenda.Value = 1) and (qryMastercFlgCompra.Value = 1) then
  begin
      ShowMessage('Selecione Compra ou Venda. Essas op��es s�o excludentes.') ;
      cxPageControl1.ActivePageIndex := 0;
      abort ;
  end ;

  if (qryMasternCdUnidadeNegocio.Value = 0) then
  begin
      ShowMessage('Informe a unidade de neg�cio.') ;
      cxPageControl1.ActivePageIndex := 1;
      DBEdit11.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdGrupoPedido.Value = 0) then
  begin
      ShowMessage('Informe o Grupo de Pedido.') ;
      DBEdit13.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgAtuPreco.Value = 1) and (qryMastercFlgCompra.Value = 0) then
  begin
      ShowMessage('A op��o de atualizar pre�o de venda s� pode ser utilizada em pedido de compra.' +#13#13+'Selecione a op��o Considerar Compra.') ;
      abort ;
  end ;

  if (qryMastercFlgPermAbatDevVenda.Value = 1) and (qryMastercFlgVenda.Value = 0) then
  begin
      MensagemAlerta('S� � permitido abater valor de devolu��o de venda em pedidos de venda.') ;
      abort ;
  end ;

  if (qryMastercFlgPermAbatDevVenda.Value = 1) and (qryMastercGerarFinanc.Value = 0) then
  begin
      MensagemAlerta('S� � permitido abater valor de devolu��o de venda em pedidos que geram t�tulos financeiros.') ;
      abort ;
  end ;

  if (qryMastercFlgRetemPisCofins.Value = 1) and (qryMastercFlgVenda.Value = 0) and (qryMastercFlgDevolucVenda.Value = 0) then
  begin
      MensagemAlerta('S� � permitido Pis/Cofins para Venda ou Devolu��o de venda.') ;
      abort ;
  end ;

  if (qryMastercFlgGeraOP.Value = 1) and (DBEdit20.Text = '') then
  begin
      MensagemAlerta('Selecione o tipo de ordem de produ��o.') ;
      abort ;
  end ;

  inherited;
end;

procedure TfrmTipoPedido.qryUsuarioTipoPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuario, qryUsuarioTipoPedidonCdUsuario.asString) ;
  qryUsuarioTipoPedidocNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmTipoPedido.qryUsuarioTipoPedidoBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryUsuarioTipoPedidonCdTipoPedido.Value := qryMasternCdTipoPedido.Value ;

  if (qryUsuarioTipoPedido.State = dsInsert) then
      qryUsuarioTipoPedidonCdUsuarioTipoPedido.Value := frmMenu.fnProximoCodigo('USUARIOTIPOPEDIDO') ;

  inherited;

end;

procedure TfrmTipoPedido.qryMasterAfterScroll(DataSet: TDataSet);
begin
 // inherited;

  PosicionaQuery(qryUsuarioTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryUsuarioTipoPedidoAutor,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryGrupoProdutoTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTipoPedidoTipoTerceiro,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryCCTipoPedido, qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTipoOP , qryMasternCdTipoOP.asString) ;
  PosicionaQuery(qryRegraPISCOFINSIPI , qryMasternCdTipoPedido.AsString) ;

  qryCCusto.open;

end;

procedure TfrmTipoPedido.qryMasterAfterClose(DataSet: TDataSet);
begin
  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedidoAutor.Close ;
  qryGrupoProdutoTipoPedido.Close ;
  qryCCTipoPedido.Close ;
  qryTipoPedidoTipoTerceiro.CLose ;
  qryTipoOP.Close ;
  qryRegraPISCOFINSIPI.Close ;
  inherited;

end;

procedure TfrmTipoPedido.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTipoPedido.State = dsBrowse) then
             qryUsuarioTipoPedido.Edit ;


        if (qryUsuarioTipoPedido.State = dsInsert) or (qryUsuarioTipoPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTipoPedidonCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryUsuarioTipoPedidoAutorBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryUsuarioTipoPedidoAutornCdTipoPedido.Value := qryMasternCdTipoPedido.Value ;

  if (qryUsuarioTipoPedidoAutor.State = dsInsert) then
      qryUsuarioTipoPedidoAutornCdUsuarioTipoPedidoAutor.Value := frmMenu.fnProximoCodigo('USUARIOTIPOPEDIDOAUTOR') ;

  inherited;

end;

procedure TfrmTipoPedido.qryUsuarioTipoPedidoAutorCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuario, qryUsuarioTipoPedidoAutornCdUsuario.asString) ;
  qryUsuarioTipoPedidoAutorcNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmTipoPedido.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTipoPedidoAutor.State = dsBrowse) then
             qryUsuarioTipoPedidoAutor.Edit ;


        if (qryUsuarioTipoPedidoAutor.State = dsInsert) or (qryUsuarioTipoPedidoAutor.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTipoPedidoAutornCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoPedido.qryGrupoProdutoTipoPedidoBeforePost(
  DataSet: TDataSet);
begin

  if (qryGrupoProdutoTipoPedidocNmGrupoProduto.Value = '') then
  begin
      ShowMessage('Selecione um grupo de produto.') ;
      abort ;
  end ;

  inherited;

  qryGrupoProdutoTipoPedidonCdTipoPedido.Value := qryMasternCdTipoPedido.Value ;

  if (qryGrupoProdutoTipoPedido.State = dsInsert) then
      qryGrupoProdutoTipoPedidonCdGrupoProdutoTipoPedido.Value := frmMenu.fnProximoCodigo('GRUPOPRODUTOTIPOPEDIDO') ;

end;

procedure TfrmTipoPedido.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryGrupoProdutoTipoPedido.State = dsBrowse) then
             qryGrupoProdutoTipoPedido.Edit ;


        if (qryGrupoProdutoTipoPedido.State = dsInsert) or (qryGrupoProdutoTipoPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryGrupoProdutoTipoPedidonCdGrupoProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryMasterAfterInsert(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryUsuarioTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryUsuarioTipoPedidoAutor,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryGrupoProdutoTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTipoPedidoTipoTerceiro,qryMasternCdTipoPedido.asString) ;

end;

procedure TfrmTipoPedido.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(35);

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuarioTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryUsuarioTipoPedidoAutor,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryGrupoProdutoTipoPedido,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTipoPedidoTipoTerceiro,qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryCCTipoPedido, qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTipoOP , qryMasternCdTipoOP.asString) ;
  PosicionaQuery(qryRegraPISCOFINSIPI , qryMasternCdTipoPedido.AsString) ;

  qryCCusto.open;


end;

procedure TfrmTipoPedido.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(79);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryTipoPedidoTipoTerceiroBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryTipoPedidoTipoTerceironCdTipoPedido.Value := qryMasternCdTipoPedido.Value ;

  if (qryTipoPedidoTipoTerceiro.State = dsInsert) then
      qryTipoPedidoTipoTerceironCdTipoPedidoTipoTerceiro.Value := frmMenu.fnProximoCodigo('TIPOPEDIDOTIPOTERCEIRO') ;

  inherited;

end;

procedure TfrmTipoPedido.DBGridEh4KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTipoPedidoTipoTerceiro.State = dsBrowse) then
             qryTipoPedidoTipoTerceiro.Edit ;


        if (qryTipoPedidoTipoTerceiro.State = dsInsert) or (qryTipoPedidoTipoTerceiro.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(18);

            If (nPK > 0) then
            begin
                qryTipoPedidoTipoTerceironCdTipoTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(84);

            If (nPK > 0) then
            begin
                qryMasternCdOperacaoEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryCCTipoPedidoBeforePost(DataSet: TDataSet);
begin

  if (qryCCTipoPedidonCdCC.Value > 0) and (qryCCTipoPedidocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo.') ;
      abort ;
  end ;

  qryCCTipoPedidonCdTipoPedido.Value := qryMasternCdTipoPedido.Value ;

  inherited;

end;

procedure TfrmTipoPedido.DBGridEh5KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCCTipoPedido.State = dsBrowse) then
             qryCCTipoPedido.Edit ;

        if (qryCCTipoPedido.State = dsInsert) or (qryCCTipoPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryCCTipoPedidonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoPedido.qryCCTipoPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryCentroCusto, qryCCTipoPedidonCdCC.asString) ;

  if not qryCentroCusto.eof then
      qryCCTipoPedidocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmTipoPedido.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  DBGridEh6.Columns[DBGridEh6.FieldColumns['nCdLoja'].Index].Visible := ( frmMenu.LeParametro('VAREJO') = 'S' ) ;

end;

procedure TfrmTipoPedido.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMasternCdCCPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoPedido.qryRegraPISCOFINSIPIBeforePost(DataSet: TDataSet);
begin

  qryEmpresa.Close;
  PosicionaQuery( qryEmpresa , qryRegraPISCOFINSIPInCdEmpresa.AsString ) ;

  if ( qryEmpresa.isEmpty ) then
  begin
      MensagemAlerta('Empresa n�o cadastrada.') ;
      abort ;
  end ;

  if (DBGridEh6.Columns[DBGridEh6.FieldColumns['nCdLoja'].Index].Visible) then
  begin
      qryLoja.Close;
      PosicionaQuery( qryLoja , qryRegraPISCOFINSIPInCdLoja.AsString ) ;

      if ( qryLoja.IsEmpty ) then
      begin
          MensagemAlerta('Loja n�o cadastrada.') ;
          abort ;
      end ;

  end ;

  if (qryRegraPISCOFINSIPIcNmGrupoImposto.Value = '') then
  begin
      MensagemAlerta('Informe um grupo de imposto.') ;
      abort ;
  end ;

  if (qryRegraPISCOFINSIPInAliqPIS.Value < 0) then
  begin
      MensagemAlerta('Al�quota do PIS inv�lida.') ;
      abort ;
  end ;

  if (qryRegraPISCOFINSIPInAliqCOFINS.Value < 0) then
  begin
      MensagemAlerta('Al�quota do COFINS inv�lida.') ;
      abort ;
  end ;

  if     ( trim( qryRegraPISCOFINSIPIcCSTIPI.Value ) = '' )
     and (qryGrupoImpostonAliqIPI.Value > 0) then
  begin
      MensagemAlerta('Este grupo de Imposto tem al�quota de IPI definida. Informe o CST do IPI.') ;
      abort ;
  end ;

  if ( trim( qryRegraPISCOFINSIPIcCSTIPI.Value ) <> '' ) then
  begin

      {-- valida o CST do IPI --}
      qryTipoTributacaoIPI.Close;
      PosicionaQuery( qryTipoTributacaoIPI , qryRegraPISCOFINSIPIcCSTIPI.Value ) ;

      if ( qryTipoTributacaoIPI.IsEmpty ) then
      begin
          MensagemAlerta('CST do IPI n�o cadastrado.') ;
          abort;
      end ;

      {-- pedido de sa�da tem que ter CST do IPI de sa�da --}
      if ( qryMasternCdTabTipoPedido.Value = 1 ) and ( qryTipoTributacaoIPIcCdStIPI.AsInteger < 50 ) then
      begin
          MensagemAlerta('O CST do IPI informado � um CST de Entrada e o Tipo de Pedido � uma Sa�da.' +#13#13 + 'Informe um CST de sa�da para o IPI.') ;
          abort ;
      end ;

      {-- pedido de entrada tem que ter CST do IPI de entrada --}
      if ( qryMasternCdTabTipoPedido.Value = 2 ) and ( qryTipoTributacaoIPIcCdStIPI.AsInteger > 49 ) then
      begin
          MensagemAlerta('O CST do IPI informado � um CST de Sa�da e o Tipo de Pedido � uma Entrada.' +#13#13 + 'Informe um CST de Entrada para o IPI.') ;
          abort ;
      end ;

      if ( trim( qryRegraPISCOFINSIPIcCdEnquadramento.Value ) = '' ) then
      begin
          MensagemAlerta('Informe o c�digo do enquadramento legal do IPI.') ;
          abort ;
      end ;

  end ;

  {-- valida os dados do PIS --}
  if ( trim( qryRegraPISCOFINSIPIcCSTPIS.Value ) <> '' ) then
  begin

      qryTipoTributacaoPISCOFINS.Close;
      PosicionaQuery( qryTipoTributacaoPISCOFINS , qryRegraPISCOFINSIPIcCSTPIS.Value ) ;

      if ( qryTipoTributacaoPISCOFINS.IsEmpty ) then
      begin
          MensagemAlerta('CST do PIS n�o cadastrado.') ;
          abort ;
      end ;

      {-- pedido de sa�da tem que ter CST do PIS de sa�da --}
      if     ( qryMasternCdTabTipoPedido.Value = 1 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger  > 49 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger <> 99 ) then
      begin
          MensagemAlerta('O CST do PIS informado � um CST de Entrada e o Tipo de Pedido � uma Sa�da.' +#13#13 + 'Informe um CST de sa�da para o PIS.') ;
          abort ;
      end ;

      {-- pedido de entrada tem que ter CST do PIS de entrada --}
      if     ( qryMasternCdTabTipoPedido.Value = 2 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger  < 50 ) 
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger <> 99 ) then
      begin
          MensagemAlerta('O CST do PIS informado � um CST de Sa�da e o Tipo de Pedido � uma Entrada.' +#13#13 + 'Informe um CST de entrada para o PIS.') ;
          abort ;
      end ;

  end ;

  if ( qryRegraPISCOFINSIPInAliqPIS.Value > 0) then
  begin

      if ( trim( qryRegraPISCOFINSIPIcCSTPIS.Value ) = '' ) then
      begin
          MensagemAlerta('Informe o CST do PIS.') ;
          abort ;
      end ;

  end ;

  {-- valida os dados do COFINS --}
  if ( trim( qryRegraPISCOFINSIPIcCSTCOFINS.Value ) <> '' ) then
  begin

      qryTipoTributacaoPISCOFINS.Close;
      PosicionaQuery( qryTipoTributacaoPISCOFINS , qryRegraPISCOFINSIPIcCSTCOFINS.Value ) ;

      if ( qryTipoTributacaoPISCOFINS.IsEmpty ) then
      begin
          MensagemAlerta('CST do COFINS n�o cadastrado.') ;
          abort ;
      end ;

      {-- pedido de sa�da tem que ter CST do COFINS de sa�da --}
      if     ( qryMasternCdTabTipoPedido.Value = 1 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger  > 49 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger <> 99 ) then
      begin
          MensagemAlerta('O CST do COFINS informado � um CST de Entrada e o Tipo de Pedido � uma Sa�da.' +#13#13 + 'Informe um CST de sa�da para o COFINS.') ;
          abort ;
      end ;

      {-- pedido de entrada tem que ter CST do COFINS de entrada --}
      if     ( qryMasternCdTabTipoPedido.Value = 2 )
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger  < 50 ) 
         and ( qryTipoTributacaoPISCOFINScCdStPISCOFINS.AsInteger <> 99 ) then
      begin
          MensagemAlerta('O CST do COFINS informado � um CST de Sa�da e o Tipo de Pedido � uma Entrada.' +#13#13 + 'Informe um CST de entrada para o COFINS.') ;
          abort ;
      end ;

  end ;

  if ( qryRegraPISCOFINSIPInAliqCOFINS.Value > 0) then
  begin

      if ( trim( qryRegraPISCOFINSIPIcCSTCOFINS.Value ) = '' ) then
      begin
          MensagemAlerta('Informe o CST do COFINS.') ;
          abort ;
      end ;

  end ;

  inherited;

  if (qryRegraPISCOFINSIPI.State = dsInsert) then
  begin
      qryRegraPISCOFINSIPInCdRegraPISCOFINSIPI.Value := frmMenu.fnProximoCodigo('REGRAPISCOFINSIPI') ;
      qryRegraPISCOFINSIPInCdTipoPedido.Value        := qryMasternCdTipoPedido.Value ;
  end ;

end;

procedure TfrmTipoPedido.qryRegraPISCOFINSIPInCdGrupoImpostoChange(
  Sender: TField);
begin
  inherited;

  qryGrupoImposto.Close;
  PosicionaQuery( qryGrupoImposto , qryRegraPISCOFINSIPInCdGrupoImposto.AsString ) ;

  qryRegraPISCOFINSIPIcNCM.Value            := '' ;
  qryRegraPISCOFINSIPIcNmGrupoImposto.Value := '' ;

  if not qryGrupoImposto.IsEmpty then
  begin
      qryRegraPISCOFINSIPIcNCM.Value            := qryGrupoImpostocNCM.Value ;
      qryRegraPISCOFINSIPIcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value ;
  end ;

end;

procedure TfrmTipoPedido.qryRegraPISCOFINSIPICalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryRegraPISCOFINSIPI.Active) then
  begin
      { -- empresa -- }
      if (qryRegraPISCOFINSIPInCdEmpresa.Value > 0) then
      begin
          qryEmpresa.Close;
          PosicionaQuery(qryEmpresa, qryRegraPISCOFINSIPInCdEmpresa.AsString);

          if (not qryEmpresa.IsEmpty) then
          begin
              qryRegraPISCOFINSIPIcNmEmpresa.Value := qryEmpresacNmEmpresa.Value;
          end;
      end;

      { -- loja -- }
      if (qryRegraPISCOFINSIPInCdLoja.Value > 0) then
      begin
          qryLoja.Close;
          PosicionaQuery(qryLoja, qryRegraPISCOFINSIPInCdLoja.AsString);

          if (not qryLoja.IsEmpty) then
          begin
              qryRegraPISCOFINSIPIcNmLoja.Value := qryLojacNmLoja.Value;
          end;
      end;

      { -- grupo de imposto -- }
      if (qryRegraPISCOFINSIPInCdGrupoImposto.Value > 0) then
      begin
          qryGrupoImposto.Close;
          PosicionaQuery(qryGrupoImposto, qryRegraPISCOFINSIPInCdGrupoImposto.AsString);

          if (not qryGrupoImposto.IsEmpty) then
          begin
              qryRegraPISCOFINSIPIcNCM.Value            := qryGrupoImpostocNCM.Value;
              qryRegraPISCOFINSIPIcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value;
          end;
      end;
  end;
end;

procedure TfrmTipoPedido.btCancelarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTipoPedido.btExcluirClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmTipoPedido.btConsultarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmTipoPedido.DBGridEh6Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh2Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh3Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh5Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

end;

procedure TfrmTipoPedido.DBGridEh6KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  case (Key) of
      VK_F4 : begin
                  if (qryRegraPISCOFINSIPI.State = dsBrowse) then
                      qryRegraPISCOFINSIPI.Edit;

                  if (qryRegraPISCOFINSIPI.State in [dsInsert,dsEdit]) then
                  begin

                  if (DBGridEh6.Columns[DBGridEh6.Col-1].FieldName = 'nCdEmpresa') then
                  begin
                      nPK := 0;
                      nPK := frmLookup_Padrao.ExecutaConsulta(8);

                      if (nPK > 0) then
                      begin
                          PosicionaQuery(qryEmpresa, IntToStr(nPK));
                          qryRegraPISCOFINSIPInCdEmpresa.Value := nPK;
                          //qryRegraPISCOFINSIPIcNmEmpresa.Value := qryEmpresacNmEmpresa.Value;
                      end;
                  end;

                  if (DBGridEh6.Columns[DBGridEh6.Col-1].FieldName = 'nCdLoja') then
                  begin
                      if (qryEmpresa.IsEmpty) then
                      begin
                          MensagemAlerta('Informe a empresa.');
                          DBGridEh6.SelectedField := qryRegraPISCOFINSIPInCdEmpresa;
                          DBGridEh6.SetFocus;
                          Abort;
                      end;

                      nPK := 0;
                      nPK := frmLookup_Padrao.ExecutaConsulta2(59,'Empresa.nCdEmpresa = ' + qryEmpresanCdEmpresa.AsString);

                      if (nPK > 0) then
                      begin
                          PosicionaQuery(qryLoja, IntToStr(nPK));
                          qryRegraPISCOFINSIPInCdLoja.Value := nPK;
                          //qryRegraPISCOFINSIPIcNmLoja.Value := qryLojacNmLoja.Value;
                      end;
                  end;

                  if (DBGridEh6.Columns[DBGridEh6.Col-1].FieldName = 'nCdGrupoImposto') then
                  begin
                      nPK := 0;
                      nPK := frmLookup_Padrao.ExecutaConsulta(73);

                      if (nPK > 0) then
                      begin
                          PosicionaQuery(qryGrupoImposto, IntToStr(nPK));
                          qryRegraPISCOFINSIPInCdGrupoImposto.Value := nPK;
                          //qryRegraPISCOFINSIPIcNCM.Value            := qryGrupoImpostocNCM.Value;
                          //qryRegraPISCOFINSIPIcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value;
                      end;
                  end;
                  end;
              end;
  end;
end;

initialization
    RegisterClass(TfrmTipoPedido);

end.
