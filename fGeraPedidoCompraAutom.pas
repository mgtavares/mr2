unit fGeraPedidoCompraAutom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmGeraPedidoCompraAutom = class(TfrmCadastro_Padrao)
    qryMasternCdMapaCompra: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmMapaCompra: TStringField;
    qryMasterdDtGeracao: TDateTimeField;
    qryMasternCdUsuarioGerador: TIntegerField;
    qryMasternCdTerceiroVenc: TIntegerField;
    qryMasternCdTipoRazaoEscolha: TIntegerField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtAutor: TDateTimeField;
    qryMastercOBS: TMemoField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryUsuarioGer: TADOQuery;
    qryUsuarioGernCdUsuario: TIntegerField;
    qryUsuarioGercNmUsuario: TStringField;
    qryUsuarioAutor: TADOQuery;
    qryUsuarioAutornCdUsuario: TIntegerField;
    qryUsuarioAutorcNmUsuario: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryUsuarioCancel: TADOQuery;
    qryUsuarioCancelnCdUsuario: TIntegerField;
    qryUsuarioCancelcNmUsuario: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    qryFornMapaCompra: TADOQuery;
    dsFornMapaCompra: TDataSource;
    qryPrepara_Temp: TADOQuery;
    SP_ATUALIZA_DADOS_MAPA: TADOStoredProc;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    Label7: TLabel;
    DBEdit13: TDBEdit;
    Label8: TLabel;
    DBEdit14: TDBEdit;
    Label10: TLabel;
    DBEdit15: TDBEdit;
    qryFornMapaCompracNmFornecedor: TStringField;
    qryFornMapaCompranCdFornMapaCompra: TIntegerField;
    qryFornMapaCompracNrPedTerceiro: TStringField;
    qryFornMapaCompradDtPrevEntIni: TDateTimeField;
    qryFornMapaCompradDtPrevEntFim: TDateTimeField;
    qryFornMapaCompranCdTerceiroColab: TIntegerField;
    qryFornMapaCompranCdTerceiro: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit8: TDBEdit;
    DataSource7: TDataSource;
    qryComprador: TADOQuery;
    DBEdit10: TDBEdit;
    DataSource8: TDataSource;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    qryPrepara_Temp_Dias: TADOQuery;
    qryDias: TADOQuery;
    qryDiasiDias: TIntegerField;
    qryDiasnValParcela: TBCDField;
    qryDiasnCdFornMapaCompra: TIntegerField;
    dsDias: TDataSource;
    qryFornMapaCompranValTotal: TBCDField;
    qryAux: TADOQuery;
    SP_GERA_PEDIDO_MAPA: TADOStoredProc;
    qryMastercFlgGeradoPedido: TIntegerField;
    qryFornMapaCompranCdTipoPedido: TIntegerField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    qryTipoPedidonCdUnidadeNegocio: TIntegerField;
    qryTipoPedidocCFOPEstadual: TStringField;
    qryTipoPedidocCFOPInter: TStringField;
    qryTipoPedidocTextoCFOPEstadual: TStringField;
    qryTipoPedidocTextoCFOPInter: TStringField;
    qryTipoPedidocMsgNF: TStringField;
    qryTipoPedidonCdNaturezaOperacao: TIntegerField;
    qryTipoPedidonCdTipoDoctoFiscal: TIntegerField;
    qryTipoPedidonCdGrupoPedido: TIntegerField;
    qryTipoPedidocFlgAguardarMontagem: TIntegerField;
    qryTipoPedidonCdOperacaoEstoque: TIntegerField;
    qryTipoPedidocLimiteCredito: TIntegerField;
    Label11: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DataSource9: TDataSource;
    cmdPreparaTemps: TADOCommand;
    qryFornMapaCompranCdLocalEstoqueEntrega: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit5: TDBEdit;
    DataSource10: TDataSource;
    qryCompradornCdTerceiro: TIntegerField;
    qryCompradorcNmTerceiro: TStringField;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label9: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    GroupBox3: TGroupBox;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure qryFornMapaCompraAfterScroll(DataSet: TDataSet);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryDiasBeforePost(DataSet: TDataSet);
    procedure ToolButton13Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit16Exit(Sender: TObject);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraPedidoCompraAutom: TfrmGeraPedidoCompraAutom;

implementation

uses fMenu, rMapaCompra, fLookup_Padrao;

{$R *.dfm}

procedure TfrmGeraPedidoCompraAutom.FormCreate(Sender: TObject);
begin
  cNmTabelaMaster   := 'MAPACOMPRA' ;
  nCdTabelaSistema  := 60 ;
  nCdConsultaPadrao := 134 ;
  inherited;

end;

procedure TfrmGeraPedidoCompraAutom.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa de compra cancelado.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryMasterdDtAutor.AsString = '') then
  begin
      MensagemAlerta('Mapa de compra n�o autorizado.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryMastercFlgGeradoPedido.Value = 1) then
  begin
      MensagemAlerta('Este mapa de compra j� foi encerrado.' +#13#13 + 'O(s) pedido(s) de compra j� foi(oram) gerado(s).') ;
      btCancelar.Click;
      abort ;
  end ;

  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryLoja.Close ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
  PosicionaQuery(qryUsuarioGer, qryMasternCdUsuarioGerador.AsString) ;
  PosicionaQuery(qryUsuarioAutor, qryMasternCdUsuarioAutor.AsString) ;
  PosicionaQuery(qryUsuarioCancel, qryMasternCdUsuarioCancel.AsString) ;

  btSalvar.Enabled   := True ;

  cmdPreparaTemps.Execute;
  
  //qryPrepara_Temp.Close ;
  //qryPrepara_Temp.Parameters.ParamByName('nPK').Value := qryMasternCdMapaCompra.Value ;
  //qryPrepara_Temp.ExecSQL;

  qryPrepara_Temp_Dias.Close ;
  qryPrepara_Temp_Dias.Parameters.ParamByName('nPK').Value := qryMasternCdMapaCompra.Value ;
  qryPrepara_Temp_Dias.ExecSQL;

  qryFornMapaCompra.Close ;
  qryFornMapaCompra.Parameters.ParamByName('nPK').Value := qryMasternCdMapaCompra.Value ;
  qryFornMapaCompra.Open ;

  if (qryFornMapaCompra.Active) and (qryFornMapaCompranCdTerceiroColab.Value = 0) then
  begin
      qryFornMapaCompra.Edit ;

      PosicionaQuery(qryComprador, IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryFornMapaCompranCdTerceiroColab.Value := frmMenu.nCdUsuarioLogado ;

      qryFornMapaCompra.Post ;
  end ;

end;

procedure TfrmGeraPedidoCompraAutom.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryLoja.Close ;
  qryTerceiro.Close ;
  qryComprador.Close ;
  qryLocalEstoque.Close ;

  qryFornMapaCompra.Close ;
  qryDias.Close ;

end;

procedure TfrmGeraPedidoCompraAutom.btSalvarClick(Sender: TObject);
begin

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa somennte para consulta.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString = '') and (qryMasterdDtCancel.AsString = '') then
  begin

      frmMenu.Connection.Begintrans ;

      try
          SP_ATUALIZA_DADOS_MAPA.Close ;
          SP_ATUALIZA_DADOS_MAPA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  inherited;

  qryFornMapaCompra.Close ;

end;

procedure TfrmGeraPedidoCompraAutom.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryLoja.Close ;

  qryFornMapaCompra.Close ;
  qryTerceiro.Close ;
  qryComprador.Close ;
  qryTipoPedido.Close ;
  qryDias.Close ;
  qryLocalEstoque.Close ;
 

end;

procedure TfrmGeraPedidoCompraAutom.ToolButton10Click(Sender: TObject);
var
  objRel : TrptMapaCompra;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione um mapa.') ;
      exit ;
  end ;

  objRel := TrptMapaCompra.Create(nil);
  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
          objRel.usp_Relatorio.Open ;

          objRel.SPREL_MAPA_HEADER.Close ;
          objRel.SPREL_MAPA_HEADER.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
          objRel.SPREL_MAPA_HEADER.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.QuickRep1.Preview;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, Dbedit6.Text) ;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  qryComprador.Close ;
  PosicionaQuery(qryComprador, Dbedit15.Text) ;

end;

procedure TfrmGeraPedidoCompraAutom.qryFornMapaCompraAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryTerceiro.Close ;
  qryComprador.Close ;
  qryDias.Close ;
  qryTipoPedido.Close ;
  qryLocalEstoque.Close;

  PosicionaQuery(qryTerceiro, qryFornMapaCompranCdTerceiro.AsString) ;
  PosicionaQuery(qryComprador, qryFornMapaCompranCdTerceiroColab.AsString) ;
  PosicionaQuery(qryTipoPedido, qryFornMapaCompranCdTipoPedido.AsString) ;
  PosicionaQuery(qryDias, qryFornMapaCompranCdFornMapaCompra.asString) ;
  PosicionaQuery(qryLocalestoque, qryFornMapaCompranCdLocalEstoqueEntrega.asString) ;

  if (qryFornMapaCompra.Active) and (qryFornMapaCompranCdTerceiroColab.Value = 0) then
  begin
      qryFornMapaCompra.Edit ;

      PosicionaQuery(qryComprador, IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryFornMapaCompranCdTerceiroColab.Value := frmMenu.nCdUsuarioLogado ;

      qryFornMapaCompra.Post ;
  end ;
  
  DBEdit6.SetFocus;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if not qryFornMapaCompra.Active then
            exit ;

        if (qryFornMapaCompra.State = dsBrowse) then
             qryFornMapaCompra.Edit ;

        if (qryFornMapaCompra.State = dsInsert) or (qryFornMapaCompra.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(90);

            If (nPK > 0) then
            begin
                qryFornMapaCompranCdTerceiro.Value := nPK ;
                PosicionaQuery(qryTerceiro, qryFornMapaCompranCdTerceiro.AsString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit15KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if not qryFornMapaCompra.Active then
            exit ;

        if (qryFornMapaCompra.State = dsBrowse) then
             qryFornMapaCompra.Edit ;

        if (qryFornMapaCompra.State = dsInsert) or (qryFornMapaCompra.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(152);

            If (nPK > 0) then
            begin
                qryFornMapaCompranCdTerceiroColab.Value := nPK ;
                PosicionaQuery(qryComprador, qryFornMapaCompranCdTerceiroColab.AsString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGeraPedidoCompraAutom.qryDiasBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryDiasiDias.Value < 0) then
  begin
      MensagemAlerta('Informe o n�mero de dias.') ;
      abort ;
  end ;

  if (qryDiasnValParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qryDiasnCdFornMapaCompra.Value := qryFornMapaCompranCdFornMapaCompra.Value ;
  
end;

procedure TfrmGeraPedidoCompraAutom.ToolButton13Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione um mapa.') ;
      exit ;
  end ;

  if (qryDias.State <> dsBrowse) then
      qryDias.Post ;

  if (qryFornMapaCompra.State <> dsBrowse) then
      qryFornMapaCompra.Post ;

  // confere os dados

  qryFornMapaCompra.First ;

  while not qryFornMapaCompra.Eof do begin

      PosicionaQuery(qryTerceiro, qryFornMapaCompranCdTerceiro.AsString) ;
      PosicionaQuery(qryComprador, qryFornMapaCompranCdTerceiroColab.AsString) ;
      PosicionaQuery(qryTipoPedido, qryFornMapaCompranCdTipoPedido.AsString) ;


      if (qryFornMapaCompranCdTerceiro.Value = 0) or (qryTerceiro.Eof) then
      begin
          MensagemAlerta('Selecione o fornecedor do cadastro para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          DBEdit6.SetFocus;
          exit ;
      end ;

      if (qryFornMapaCompradDtPrevEntIni.AsString = '') or (qryFornMapaCompradDtPrevEntFim.AsString = '') then
      begin
          MensagemAlerta('Informe a previs�o de entrega para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          Dbedit13.SetFocus ;
          exit ;
      end ;

      if (qryFornMapaCompradDtPrevEntIni.Value > qryFornMapaCompradDtPrevEntFim.Value) then
      begin
          MensagemAlerta('Previs�o de entrega inv�lida para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          Dbedit13.SetFocus ;
          exit ;
      end ;

      if (qryFornMapaCompranCdTerceiroColab.Value = 0) or (qryComprador.Eof) then
      begin
          MensagemAlerta('Selecione o comprador para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          DbEdit15.SetFocus ;
          exit ;
      end ;

      if (qryFornMapaCompranCdTipoPedido.Value = 0) or (qryTipoPedido.Eof) then
      begin
          MensagemAlerta('Selecione o tipo de pedido para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          DbEdit15.SetFocus ;
          exit ;
      end ;

      if (qryFornMapaCompranCdLocalEstoqueEntrega.Value = 0) then
      begin
          MensagemAlerta('Selecione um local de entrega para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          DbEdit14.SetFocus ;
          exit ;
      end ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValParcela) FROM #Temp_Dias WHERE nCdFornMapaCompra = ' + qryFornMapaCompranCdFornMapaCompra.AsString) ;
      qryAux.Open ;

      if qryAux.Eof or (qryAux.FieldList[0].Value <> qryFornMapaCompranValTotal.Value) then
      begin
          MensagemAlerta('A soma das parcelas n�o confere com o total definido para o fornecedor ' + qryFornMapaCompracNmFornecedor.Value) ;
          DbGridEh2.SetFocus ;
          exit ;
      end ;

      qryAux.Close ;

      qryFornMapaCompra.Next ;
  end ;

  qryFornMapaCompra.First ;

  case MessageDlg('Confirma a gera��o do(s) pedido(s) de compra ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_PEDIDO_MAPA.Close ;
      SP_GERA_PEDIDO_MAPA.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
      SP_GERA_PEDIDO_MAPA.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      SP_GERA_PEDIDO_MAPA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedidos gerados com sucesso!') ;

  btCancelar.Click;

end;

procedure TfrmGeraPedidoCompraAutom.FormShow(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM Usuario WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND cFlgComprador = 1') ;
  qryAux.Open ;

  if (qryAux.Eof) then
  begin
      MensagemAlerta('Somente usu�rios compradores podem operar nesta tela.') ;
      Close ;
  end ;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit16Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, DbEdit16.Text) ;
  
end;

procedure TfrmGeraPedidoCompraAutom.DBEdit16KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryFornMapaCompra.State = dsBrowse) then
             qryFornMapaCompra.Edit ;

        if (qryFornMapaCompra.State = dsInsert) or (qryFornMapaCompra.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(98,'cFlgCompra = 1 AND EXISTS(SELECT 1 FROM UsuarioTipoPedido WHERE UsuarioTipoPedido.nCdTipoPedido = TipoPedido.nCdTipoPedido AND UsuarioTipoPedido.nCdUsuario    = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ')') ;


            If (nPK > 0) then
            begin
                qryFornMapaCompranCdTipoPedido.Value := nPK ;
                PosicionaQuery(qryTipoPedido, qryFornMapaCompranCdTipoPedido.AsString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGeraPedidoCompraAutom.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryLocalEstoque, DBEdit4.Text) ;
  
end;

procedure TfrmGeraPedidoCompraAutom.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryFornMapaCompranCdLocalEstoqueEntrega.Value := nPK ;
            end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmGeraPedidoCompraAutom) ;

end.
