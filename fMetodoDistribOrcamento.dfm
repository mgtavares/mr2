inherited frmMetodoDistribOrcamento: TfrmMetodoDistribOrcamento
  Left = 80
  Top = 69
  Caption = 'M'#233'todo Distribui'#231#227'o Or'#231'amento'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 19
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdMetodoDistribOrcamento'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmMetodoDistribOrcamento'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [6]
    Left = 64
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Linear'
    DataField = 'cFlgLinear'
    DataSource = dsMaster
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [7]
    Left = 168
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Padr'#227'o'
    DataField = 'cFlgPadrao'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [8]
    Left = 64
    Top = 104
    Width = 649
    Height = 337
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 645
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Fator de Distribui'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 641
        Height = 309
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsFatorMetodoDistribOrcamento
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoMes'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Title.Caption = 'C'#243'd. M'#234's'
          end
          item
            EditButtons = <>
            FieldName = 'cNmMes'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Descri'#231#227'o'
          end
          item
            EditButtons = <>
            FieldName = 'nPercent'
            Footers = <
              item
                DisplayFormat = '#,##0.00'
                FieldName = 'nPercent'
                ValueType = fvtSum
              end>
            Title.Caption = '% Distribui'#231#227'o'
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM MetodoDistribOrcamento'
      ' WHERE nCdMetodoDistribOrcamento = :nPK')
    object qryMasternCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryMastercNmMetodoDistribOrcamento: TStringField
      FieldName = 'cNmMetodoDistribOrcamento'
      Size = 50
    end
    object qryMastercFlgLinear: TIntegerField
      FieldName = 'cFlgLinear'
    end
    object qryMastercFlgPadrao: TIntegerField
      FieldName = 'cFlgPadrao'
    end
  end
  object qryFatorDistribOrcam: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryFatorDistribOrcamCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM FatorMetodoDistribOrcamento'
      ' WHERE nCdMetodoDistribOrcamento = :nPK')
    Left = 216
    Top = 488
    object qryFatorDistribOrcamnCdFatorMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdFatorMetodoDistribOrcamento'
    end
    object qryFatorDistribOrcamnCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryFatorDistribOrcamnCdTabTipoMes: TIntegerField
      FieldName = 'nCdTabTipoMes'
    end
    object qryFatorDistribOrcamcNmMes: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmMes'
      Calculated = True
    end
    object qryFatorDistribOrcamnPercent: TBCDField
      FieldName = 'nPercent'
      Precision = 14
    end
  end
  object qryMes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTabTipoMes'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TabTipoMes'
      ' WHERE nCdTabTipoMes = :nCdTabTipoMes')
    Left = 264
    Top = 520
    object qryMesnCdTabTipoMes: TIntegerField
      FieldName = 'nCdTabTipoMes'
    end
    object qryMescNmTabTipoMes: TStringField
      FieldName = 'cNmTabTipoMes'
      Size = 50
    end
  end
  object dsFatorMetodoDistribOrcamento: TDataSource
    DataSet = qryFatorDistribOrcam
    Left = 232
    Top = 520
  end
  object qryDefinePadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetodoDistribOrcamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE MetodoDistribOrcamento'
      '   SET cFlgPadrao = 0 '
      ' WHERE nCdMetodoDistribOrcamento != :nCdMetodoDistribOrcamento')
    Left = 184
    Top = 488
  end
  object qryInsereMes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetodoDistribOrcamento'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdFatorMetodoDistribOrcamento int'
      '       ,@nCdMetodoDistribOrcamento      int'
      '       ,@nCdTabTipoMes                  int'
      ''
      'SET @nCdTabTipoMes             = 1'
      'SET @nCdMetodoDistribOrcamento = :nCdMetodoDistribOrcamento '
      ''
      'WHILE (@nCdTabTipoMes <= 12 )'
      'BEGIN'
      ''
      '    EXEC usp_ProximoID '#39'FATORMETODODISTRIBORCAMENTO'#39
      '                       ,@nCdFatorMetodoDistribOrcamento out'
      ''
      
        '    INSERT into FatorMetodoDistribOrcamento (nCdFatorMetodoDistr' +
        'ibOrcamento'
      
        '                                            ,nCdMetodoDistribOrc' +
        'amento'
      '                                            ,nCdTabTipoMes)'
      
        '                                     VALUES (@nCdFatorMetodoDist' +
        'ribOrcamento'
      
        '                                            ,@nCdMetodoDistribOr' +
        'camento'
      '                                            ,@nCdTabTipoMes)'
      ''
      '    SET @nCdTabTipoMes = @nCdTabTipoMes + 1'
      ''
      'END')
    Left = 328
    Top = 488
  end
end
