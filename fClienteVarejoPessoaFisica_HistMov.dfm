inherited frmClienteVarejoPessoaFisica_HistMov: TfrmClienteVarejoPessoaFisica_HistMov
  Left = 12
  Top = 74
  Width = 1359
  Height = 619
  Caption = 'Hist'#243'rico de Movimenta'#231#227'o'
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1343
    Height = 552
  end
  inherited ToolBar1: TToolBar
    Width = 1343
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1343
    Height = 552
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 548
    ClientRectLeft = 4
    ClientRectRight = 1339
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Compras'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1335
        Height = 524
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsResultado
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGrid1DBTableView1nValTotal
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
            Caption = 'Dt. Compra'
            DataBinding.FieldName = 'dDtPedido'
            Width = 95
          end
          object cxGrid1DBTableView1nCdLoja: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'nCdLoja'
          end
          object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
            Caption = 'Pedido'
            DataBinding.FieldName = 'nCdPedido'
            HeaderAlignmentHorz = taRightJustify
            Width = 70
          end
          object cxGrid1DBTableView1Item: TcxGridDBColumn
            DataBinding.FieldName = 'Item'
            Visible = False
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTerceiro'
            Visible = False
          end
          object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTipoPedido'
            Visible = False
          end
          object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
            Width = 178
          end
          object cxGrid1DBTableView1cReferencia: TcxGridDBColumn
            Caption = 'Refer'#234'ncia'
            DataBinding.FieldName = 'cReferencia'
          end
          object cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn
            Caption = 'Qtde.'
            DataBinding.FieldName = 'nQtdeExpRec'
            HeaderAlignmentHorz = taRightJustify
            Width = 55
          end
          object cxGrid1DBTableView1nValTotal: TcxGridDBColumn
            Caption = 'Valor Final'
            DataBinding.FieldName = 'nValTotal'
            HeaderAlignmentHorz = taRightJustify
            Width = 95
          end
          object cxGrid1DBTableView1nValUnitario: TcxGridDBColumn
            Caption = 'Val. Unit.'
            DataBinding.FieldName = 'nValUnitario'
            HeaderAlignmentHorz = taRightJustify
            Width = 83
          end
          object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
            Caption = 'Val. Desconto'
            DataBinding.FieldName = 'nValDesconto'
            HeaderAlignmentHorz = taRightJustify
            Width = 105
          end
          object cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn
            Caption = 'Val. Acresc.'
            DataBinding.FieldName = 'nValAcrescimo'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn
            Caption = 'Lancto Fin.'
            DataBinding.FieldName = 'nCdLanctoFin'
          end
          object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'cNmLoja'
            Visible = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Parcelas'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 57
        Width = 1335
        Height = 467
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridDBTableView1DblClick
          DataController.DataSource = dsParcelas
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView1nSaldoTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView1nValLiq
            end
            item
              Format = '#'
              Kind = skCount
              Column = cxGridDBTableView1iParcela
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          Styles.OnGetContentStyle = cxGridDBTableView1StylesGetContentStyle
          object cxGridDBTableView1cSituacao: TcxGridDBColumn
            Caption = 'Situa'#231#227'o'
            DataBinding.FieldName = 'cSituacao'
            Options.Moving = False
            Width = 86
          end
          object cxGridDBTableView1cSituacao2: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'cSituacao2'
            Options.Moving = False
          end
          object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
            Caption = 'Dt. Compra'
            DataBinding.FieldName = 'dDtEmissao'
            Options.Moving = False
            Width = 83
          end
          object cxGridDBTableView1nCdLojaTit: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'nCdLojaTit'
            Options.Moving = False
            Width = 53
          end
          object cxGridDBTableView1DBColumn4: TcxGridDBColumn
            Caption = 'Caixa Emiss'#227'o'
            DataBinding.FieldName = 'nCdContaEmissao'
          end
          object cxGridDBTableView1cNrTit: TcxGridDBColumn
            Caption = 'Carnet'
            DataBinding.FieldName = 'cNrTit'
            Options.Moving = False
            Width = 76
          end
          object cxGridDBTableView1iParcela: TcxGridDBColumn
            Caption = 'Parc.'
            DataBinding.FieldName = 'iParcela'
            HeaderAlignmentHorz = taRightJustify
            Options.Moving = False
            Width = 54
          end
          object cxGridDBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencto'
            DataBinding.FieldName = 'dDtVenc'
            Options.Moving = False
            Width = 82
          end
          object cxGridDBTableView1dDtLiq: TcxGridDBColumn
            Caption = 'Dt. Pagto'
            DataBinding.FieldName = 'dDtLiq'
            Options.Moving = False
            Width = 87
          end
          object cxGridDBTableView1DBColumn2: TcxGridDBColumn
            Caption = 'Loja Pagto'
            DataBinding.FieldName = 'nCdLoja'
            Options.Moving = False
            Width = 73
          end
          object cxGridDBTableView1DBColumn3: TcxGridDBColumn
            Caption = 'Caixa Pagto'
            DataBinding.FieldName = 'nCdConta'
            Options.Moving = False
          end
          object cxGridDBTableView1iDiasAtraso: TcxGridDBColumn
            Caption = 'Atraso'
            DataBinding.FieldName = 'iDiasAtraso'
            HeaderAlignmentHorz = taRightJustify
            Options.Moving = False
            Width = 59
          end
          object cxGridDBTableView1nValTit: TcxGridDBColumn
            Caption = 'Vl. Parcela'
            DataBinding.FieldName = 'nValTit'
            HeaderAlignmentHorz = taRightJustify
            Options.Moving = False
            Width = 90
          end
          object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldoTit'
            HeaderAlignmentHorz = taRightJustify
            Options.Moving = False
            Width = 75
          end
          object cxGridDBTableView1nValLiq: TcxGridDBColumn
            Caption = 'Valor Pago'
            DataBinding.FieldName = 'nValLiq'
            HeaderAlignmentHorz = taRightJustify
            Options.Moving = False
          end
          object cxGridDBTableView1cFlgNegativado: TcxGridDBColumn
            Caption = 'SPC'
            DataBinding.FieldName = 'cFlgNegativado'
            Options.Moving = False
            Width = 60
          end
          object cxGridDBTableView1DBColumn1: TcxGridDBColumn
            Caption = 'Reneg.'
            DataBinding.FieldName = 'cFlgRenegociacao'
            Options.Moving = False
            Width = 71
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1335
        Height = 57
        Align = alTop
        Caption = 'Legenda'
        TabOrder = 1
        object Label37: TLabel
          Tag = 2
          Left = 40
          Top = 28
          Width = 49
          Height = 13
          Caption = 'Em aberto'
        end
        object Label1: TLabel
          Tag = 2
          Left = 144
          Top = 28
          Width = 48
          Height = 13
          Caption = 'Em atraso'
        end
        object Label2: TLabel
          Tag = 2
          Left = 240
          Top = 28
          Width = 62
          Height = 13
          Caption = 'Renegociado'
        end
        object Label3: TLabel
          Tag = 2
          Left = 352
          Top = 28
          Width = 19
          Height = 13
          Caption = 'SPC'
        end
        object cxTextEdit2: TcxTextEdit
          Left = 8
          Top = 20
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clYellow
          TabOrder = 0
        end
        object cxTextEdit1: TcxTextEdit
          Left = 112
          Top = 20
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clRed
          TabOrder = 1
        end
        object cxTextEdit3: TcxTextEdit
          Left = 208
          Top = 20
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clBlue
          TabOrder = 2
        end
        object cxTextEdit4: TcxTextEdit
          Left = 320
          Top = 20
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clBlack
          StyleHot.Color = clBlack
          TabOrder = 3
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Cheques'
      ImageIndex = 2
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1335
        Height = 524
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          DataController.DataSource = dsCheque
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView2nValCheque
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          object cxGridDBTableView2dDtCompra: TcxGridDBColumn
            Caption = 'Dt. Compra'
            DataBinding.FieldName = 'dDtCompra'
            Width = 77
          end
          object cxGridDBTableView2nCdLoja: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'nCdLoja'
            Width = 49
          end
          object cxGridDBTableView2nCdBanco: TcxGridDBColumn
            Caption = 'Bco'
            DataBinding.FieldName = 'nCdBanco'
            HeaderAlignmentHorz = taRightJustify
            Width = 44
          end
          object cxGridDBTableView2iNrCheque: TcxGridDBColumn
            Caption = 'Nr. Cheque'
            DataBinding.FieldName = 'iNrCheque'
            HeaderAlignmentHorz = taRightJustify
            Width = 82
          end
          object cxGridDBTableView2dDtVencto: TcxGridDBColumn
            Caption = 'Dt. Vencto'
            DataBinding.FieldName = 'dDtVencto'
            Width = 83
          end
          object cxGridDBTableView2nValCheque: TcxGridDBColumn
            Caption = 'Val. Cheque'
            DataBinding.FieldName = 'nValCheque'
            HeaderAlignmentHorz = taRightJustify
            Width = 89
          end
          object cxGridDBTableView2iDiasAtraso: TcxGridDBColumn
            Caption = 'Atraso'
            DataBinding.FieldName = 'iDiasAtraso'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGridDBTableView2cNmTabStatusCheque: TcxGridDBColumn
            Caption = 'Situa'#231#227'o'
            DataBinding.FieldName = 'cNmTabStatusCheque'
            Width = 99
          end
          object cxGridDBTableView2cMsgResgatado: TcxGridDBColumn
            Caption = 'Resgatado'
            DataBinding.FieldName = 'cMsgResgatado'
            Width = 80
          end
          object cxGridDBTableView2dDtResgate: TcxGridDBColumn
            Caption = 'Dt. Resgate'
            DataBinding.FieldName = 'dDtResgate'
            Width = 91
          end
          object cxGridDBTableView2nCdLojaResgate: TcxGridDBColumn
            Caption = 'Loja Resgate'
            DataBinding.FieldName = 'nCdLojaResgate'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Vales'
      ImageIndex = 3
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 1335
        Height = 524
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView3: TcxGridDBTableView
          OnDblClick = cxGridDBTableView3DblClick
          DataController.DataSource = dsVale
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView3nSaldoVale
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          object cxGridDBTableView3dDtVale: TcxGridDBColumn
            Caption = 'Dt. Vale'
            DataBinding.FieldName = 'dDtVale'
            Width = 77
          end
          object cxGridDBTableView3nCdLoja: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'nCdLoja'
            Width = 47
          end
          object cxGridDBTableView3cCodigo: TcxGridDBColumn
            Caption = 'N'#250'm. Vale'
            DataBinding.FieldName = 'cCodigo'
          end
          object cxGridDBTableView3cNmTipoVale: TcxGridDBColumn
            Caption = 'Tipo de Vale'
            DataBinding.FieldName = 'cNmTipoVale'
            Width = 132
          end
          object cxGridDBTableView3nValVale: TcxGridDBColumn
            Caption = 'Val. Vale'
            DataBinding.FieldName = 'nValVale'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGridDBTableView3nSaldoVale: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldoVale'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGridDBTableView3dDtBaixa: TcxGridDBColumn
            Caption = 'Dt. Baixa'
            DataBinding.FieldName = 'dDtBaixa'
          end
          object cxGridDBTableView3cMsgRecompra: TcxGridDBColumn
            Caption = 'Recomprado'
            DataBinding.FieldName = 'cMsgRecompra'
            Width = 88
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
    end
    object tabConsignacao: TcxTabSheet
      Caption = 'Consigna'#231#245'es'
      ImageIndex = 4
      object cxGrid6: TcxGrid
        Left = 0
        Top = 0
        Width = 1335
        Height = 524
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView5: TcxGridDBTableView
          OnDblClick = cxGridDBTableView5DblClick
          DataController.DataSource = dsConsignacao
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          object cxGridDBTableView5DBColumn1: TcxGridDBColumn
            Caption = 'C'#243'd.'
            DataBinding.FieldName = 'nCdConsignacao'
          end
          object cxGridDBTableView5DBColumn2: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'cNmLoja'
            Width = 136
          end
          object cxGridDBTableView5DBColumn3: TcxGridDBColumn
            Caption = 'Vendedor'
            DataBinding.FieldName = 'cNmLogin'
            Width = 121
          end
          object cxGridDBTableView5DBColumn4: TcxGridDBColumn
            Caption = 'C'#243'd. Pedido'
            DataBinding.FieldName = 'nCdPedido'
            Width = 91
          end
          object cxGridDBTableView5DBColumn5: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'cTipoStatusConsig'
            Width = 216
          end
          object cxGridDBTableView5DBColumn6: TcxGridDBColumn
            Caption = 'Dt. Consigna'#231#227'o'
            DataBinding.FieldName = 'cPedido'
            Width = 170
          end
          object cxGridDBTableView5DBColumn7: TcxGridDBColumn
            Caption = 'Dt. Devolu'#231#227'o'
            DataBinding.FieldName = 'cDevolucao'
            Width = 164
          end
          object cxGridDBTableView5DBColumn8: TcxGridDBColumn
            Caption = 'Dt. Finaliza'#231#227'o'
            DataBinding.FieldName = 'cFinalizacao'
            Width = 153
          end
          object cxGridDBTableView5DBColumn9: TcxGridDBColumn
            Caption = 'Dt. Cancelamento'
            DataBinding.FieldName = 'cCancelamento'
            Width = 150
          end
        end
        object cxGridLevel5: TcxGridLevel
          GridView = cxGridDBTableView5
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = 'Compras Web'
      ImageIndex = 5
      object cxGrid5: TcxGrid
        Left = 0
        Top = 0
        Width = 1335
        Height = 524
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView4: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsCompraWeb
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBColumn_nValPedido
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.ConteudoCaixa
          object cxGridDBColumn_dDtPedido: TcxGridDBColumn
            Caption = 'Dt. Compra'
            DataBinding.FieldName = 'dDtPedido'
            Width = 95
          end
          object cxGridDBColumn_nCdLoja: TcxGridDBColumn
            Caption = 'Loja'
            DataBinding.FieldName = 'nCdLoja'
            Width = 65
          end
          object cxGridDBColumn_nCdPedido: TcxGridDBColumn
            Caption = 'Pedido'
            DataBinding.FieldName = 'nCdPedido'
            Width = 65
          end
          object cxGridDBColumn_nQtdePed: TcxGridDBColumn
            Caption = 'Qtde.'
            DataBinding.FieldName = 'nQtdePed'
            Width = 55
          end
          object cxGridDBColumn_nValProdutos: TcxGridDBColumn
            Caption = 'Val. Produtos'
            DataBinding.FieldName = 'nValProdutos'
            Width = 110
          end
          object cxGridDBColumn_nValFrete: TcxGridDBColumn
            Caption = 'Val. Frete'
            DataBinding.FieldName = 'nValFrete'
            Width = 110
          end
          object cxGridDBColumn_nValAcrescimo: TcxGridDBColumn
            Caption = 'Val. Acr'#233'scimo'
            DataBinding.FieldName = 'nValAcrescimo'
            Width = 110
          end
          object cxGridDBColumn_nValDesconto: TcxGridDBColumn
            Caption = 'Val. Desconto'
            DataBinding.FieldName = 'nValDesconto'
            Width = 110
          end
          object cxGridDBColumn_nValPedido: TcxGridDBColumn
            Caption = 'Val. Pedido'
            DataBinding.FieldName = 'nValPedido'
            Width = 110
          end
          object cxGridDBColumn_cNmTabStatusPed: TcxGridDBColumn
            Caption = 'Status Pedido'
            DataBinding.FieldName = 'cNmTabStatusPed'
            Width = 234
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 144
  end
  object SP_CONSULTA_VENDA_PRODUTO: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_CONSULTA_VENDA_PRODUTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 2
      end
      item
        Name = '@cCNPJCPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = ' '
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iNrDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@nCdCrediario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iNrCartao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = '1234123412341234'
      end>
    Left = 466
    Top = 342
    object SP_CONSULTA_VENDA_PRODUTOnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object SP_CONSULTA_VENDA_PRODUTOItem: TAutoIncField
      FieldName = 'Item'
      ReadOnly = True
    end
    object SP_CONSULTA_VENDA_PRODUTOdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOnQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object SP_CONSULTA_VENDA_PRODUTOnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object SP_CONSULTA_VENDA_PRODUTOnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object SP_CONSULTA_VENDA_PRODUTOnValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object SP_CONSULTA_VENDA_PRODUTOnValTotal: TBCDField
      FieldName = 'nValTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 29
      Size = 10
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object SP_CONSULTA_VENDA_PRODUTOcReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object SP_CONSULTA_VENDA_PRODUTOnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsResultado: TDataSource
    DataSet = SP_CONSULTA_VENDA_PRODUTO
    Left = 500
    Top = 341
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT Titulo.nCdTitulo'
      '       ,CASE WHEN Titulo.dDtLiq IS NULL THEN '#39'ABERTO'#39
      
        '             WHEN Titulo.dDtLiq IS NOT NULL AND IsNull(cFlgReneg' +
        'ociado,0) = 1 THEN '#39'RENEGOCIADO'#39
      '             ELSE '#39'PAGO'#39
      '        END as cSituacao'
      
        '       ,CASE WHEN Titulo.dDtLiq IS NULL THEN CASE WHEN Titulo.dD' +
        'tVenc < dbo.fn_OnlyDate(GetDate()) THEN '#39'ATRASO'#39
      '                                                  ELSE '#39'NORMAL'#39
      '                                             END'
      
        '             ELSE CASE WHEN IsNull(cFlgRenegociado,0) = 1 THEN '#39 +
        'RENEGOCIADO'#39
      
        '                       ELSE CASE WHEN dbo.fn_OnlyDate(Titulo.dDt' +
        'Liq) <= dbo.fn_OnlyDate(Titulo.dDtVenc) THEN '#39'PAGO EM DIA'#39
      '                                 ELSE '#39'PAGO EM ATRASO'#39
      '                            END'
      '                  END'
      '        END as cSituacao2'
      '       ,Titulo.cNrTit'
      '       ,Titulo.iParcela'
      '       ,Titulo.dDtEmissao'
      '       ,Titulo.dDtVenc'
      '       ,Titulo.dDtLiq'
      '       ,Titulo.nValTit'
      
        '       ,dbo.fn_arredondaVarejo(dbo.fn_SimulaJurosTitulo(Titulo.n' +
        'CdTitulo,GetDate())+dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo))+' +
        'Titulo.nSaldoTit as nSaldoTit'
      '       ,Titulo.nValLiq      '
      
        '       ,CASE WHEN dbo.fn_OnlyDate(Titulo.dDtLiq) IS NULL THEN CA' +
        'SE WHEN Titulo.dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN Convert' +
        '(int,(dbo.fn_OnlyDate(GetDate()) - Titulo.dDtVenc))'
      '                                                  ELSE 0'
      '                                             END '
      
        '             ELSE CASE WHEN Titulo.dDtVenc < dbo.fn_OnlyDate(Tit' +
        'ulo.dDtLiq) THEN Convert(int,(dbo.fn_OnlyDate(Titulo.dDtLiq) - T' +
        'itulo.dDtVenc))'
      '                       ELSE 0'
      '                  END '
      '        END as iDiasAtraso'
      '       ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) nCdLojaTit'
      
        '       ,CASE WHEN ISNULL(TituloNegativado.dDtNegativacao, Titulo' +
        '.dDtNegativacao) IS NOT NULL AND ISNULL(TituloNegativado.dDtReab' +
        'Neg, Titulo.dDtReabNeg) IS NULL THEN '#39'SIM'#39
      '             ELSE '#39'NAO'#39
      '        END cFlgNegativado'
      '       ,CASE WHEN Crediario.cFlgRenegociacao = 1 THEN '#39'Sim'#39
      '             ELSE NULL'
      '        END cFlgRenegociacao'
      '       ,(SELECT TOP 1 dbo.fn_ZeroEsquerda(CB.nCdLoja,3)'
      '           FROM MTitulo (NOLOCK)'
      
        '                INNER JOIN Operacao         ON Operacao.nCdOpera' +
        'cao   = MTitulo.nCdOperacao'
      
        '                LEFT  JOIN LanctoFin        ON LanctoFin.nCdLanc' +
        'toFin = MTitulo.nCdLanctoFin'
      
        '                LEFT  JOIN ContaBancaria CB ON CB.nCdContaBancar' +
        'ia    = LanctoFin.nCdContaBancaria'
      '          WHERE MTitulo.nCdTitulo   = Titulo.nCdTitulo'
      '            AND MTitulo.dDtCancel  IS NULL'
      '            AND Operacao.cTipoOper  = '#39'P'#39
      '          ORDER BY MTitulo.dDtMov DESC) nCdLoja'
      '       ,(SELECT TOP 1 CB.nCdConta'
      '           FROM MTitulo (NOLOCK)'
      
        '                INNER JOIN Operacao         ON Operacao.nCdOpera' +
        'cao   = MTitulo.nCdOperacao'
      
        '                LEFT  JOIN LanctoFin        ON LanctoFin.nCdLanc' +
        'toFin = MTitulo.nCdLanctoFin'
      
        '                LEFT  JOIN ContaBancaria CB ON CB.nCdContaBancar' +
        'ia    = LanctoFin.nCdContaBancaria'
      '          WHERE MTitulo.nCdTitulo   = Titulo.nCdTitulo'
      '            AND MTitulo.dDtCancel  IS NULL'
      '            AND Operacao.cTipoOper  = '#39'P'#39
      '          ORDER BY MTitulo.dDtMov DESC) nCdConta'
      '       ,(SELECT nCdConta'
      '           FROM LanctoFin(NOLOCK)'
      
        '                INNER JOIN ContaBancaria ON ContaBancaria.nCdCon' +
        'taBancaria = LanctoFin.nCdContaBancaria'
      
        '          WHERE LanctoFin.nCdLanctoFin = Crediario.nCdLanctoFin)' +
        ' as nCdContaEmissao'
      '   FROM Titulo(NOLOCK)'
      
        '        INNER JOIN Crediario ON Crediario.nCdCrediario = Titulo.' +
        'nCdCrediario'
      
        '        LEFT  JOIN TituloNegativado ON Titulo.nCdTitulo = Titulo' +
        'Negativado.nCdTitulo'
      '  WHERE Titulo.cSenso        = '#39'C'#39
      '    AND Titulo.nCdCrediario IS NOT NULL'
      '    AND Titulo.dDtCancel    IS NULL'
      '    AND Titulo.dDtBloqTit   IS NULL'
      '    AND Titulo.nCdEmpresa    = :nCdEmpresa'
      '    AND Titulo.nCdTerceiro   = :nCdTerceiro'
      '  ORDER BY 6 DESC')
    Left = 468
    Top = 181
    object qryParcelascSituacao: TStringField
      FieldName = 'cSituacao'
      ReadOnly = True
      Size = 16
    end
    object qryParcelascNrTit: TStringField
      FieldName = 'cNrTit'
      ReadOnly = True
      Size = 30
    end
    object qryParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelasdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryParcelasdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryParcelasdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryParcelasnValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryParcelascSituacao2: TStringField
      FieldName = 'cSituacao2'
      ReadOnly = True
      Size = 14
    end
    object qryParcelascFlgNegativado: TStringField
      FieldName = 'cFlgNegativado'
      ReadOnly = True
      Size = 3
    end
    object qryParcelascFlgRenegociacao: TStringField
      FieldName = 'cFlgRenegociacao'
      ReadOnly = True
      Size = 3
    end
    object qryParcelasnCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryParcelasnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryParcelasnCdLojaTit: TStringField
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
    object qryParcelasnCdContaEmissao: TStringField
      FieldName = 'nCdContaEmissao'
      ReadOnly = True
      FixedChar = True
      Size = 15
    end
    object qryParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
  end
  object dsParcelas: TDataSource
    DataSet = qryParcelas
    Left = 500
    Top = 181
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT dbo.fn_OnlyDate(LanctoFin.dDtLancto)       as dDtCompra'
      '      ,dbo.fn_ZeroEsquerda(ResumoCaixa.nCdLoja,3) as nCdLoja'
      '      ,(dbo.fn_ZeroEsquerda(Cheque.nCdBanco ,3))  as nCdBanco'
      '      ,(dbo.fn_ZeroEsquerda(Cheque.iNrCheque,6))  as iNrCheque'
      '      ,Cheque.dDtDeposito                         as dDtVencto'
      '      ,Cheque.nValCheque                          as nValCheque'
      
        '      ,CASE WHEN Cheque.nCdTabStatusCheque IN (5,6,10) THEN Conv' +
        'ert(int,(GetDate() - dDtDeposito)) '
      '            ELSE 0'
      '       END                 as iDiasAtraso'
      '      ,TabStatusCheque.cNmTabStatusCheque'
      '      ,CASE WHEN nCdLanctoFinResgate IS NOT NULL THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cMsgResgatado'
      
        '      ,dbo.fn_OnlyDate(LanctoFinResgate.dDtLancto)       as dDtR' +
        'esgate'
      
        '      ,dbo.fn_ZeroEsquerda(ResumoCaixaResgate.nCdLoja,3) as nCdL' +
        'ojaResgate'
      '  FROM Cheque (NOLOCK)'
      
        '       LEFT JOIN LanctoFin                      ON LanctoFin.nCd' +
        'LanctoFin             = Cheque.nCdLanctoFin'
      
        '       LEFT JOIN ResumoCaixa                    ON ResumoCaixa.n' +
        'CdResumoCaixa         = LanctoFin.nCdResumoCaixa'
      
        '       LEFT JOIN LanctoFin LanctoFinResgate     ON LanctoFinResg' +
        'ate.nCdLanctoFin      = Cheque.nCdLanctoFinResgate'
      
        '       LEFT JOIN ResumoCaixa ResumoCaixaResgate ON ResumoCaixaRe' +
        'sgate.nCdResumoCaixa  = LanctoFinResgate.nCdResumoCaixa'
      
        '       LEFT JOIN TabStatusCheque                ON TabStatusCheq' +
        'ue.nCdTabStatusCheque = Cheque.nCdTabStatusCheque'
      ' WHERE Cheque.nCdTabTipoCheque = 2 --Cheque de terceiro'
      '   AND Cheque.nCdTerceiroResp  = :nCdTerceiro'
      '   AND Cheque.nCdEmpresa       = :nCdEmpresa'
      ' ORDER BY 1 DESC'
      ''
      ''
      '')
    Left = 468
    Top = 261
    object qryChequedDtCompra: TDateTimeField
      FieldName = 'dDtCompra'
      ReadOnly = True
    end
    object qryChequenCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryChequenCdBanco: TStringField
      FieldName = 'nCdBanco'
      ReadOnly = True
      Size = 15
    end
    object qryChequeiNrCheque: TStringField
      FieldName = 'iNrCheque'
      ReadOnly = True
      Size = 15
    end
    object qryChequedDtVencto: TDateTimeField
      FieldName = 'dDtVencto'
    end
    object qryChequenValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequeiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryChequecNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
    object qryChequecMsgResgatado: TStringField
      FieldName = 'cMsgResgatado'
      ReadOnly = True
      Size = 3
    end
    object qryChequedDtResgate: TDateTimeField
      FieldName = 'dDtResgate'
      ReadOnly = True
    end
    object qryChequenCdLojaResgate: TStringField
      FieldName = 'nCdLojaResgate'
      ReadOnly = True
      Size = 15
    end
  end
  object dsCheque: TDataSource
    DataSet = qryCheque
    Left = 500
    Top = 261
  end
  object qryVale: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT nCdVale'
      '      ,dDtVale'
      '      ,dbo.fn_ZeroEsquerda(nCdLoja,3) as nCdLoja'
      '      ,cCodigo'
      '      ,CASE WHEN nCdTabTipoVale = 1 THEN '#39'VALE MERCADORIA'#39
      '            WHEN nCdTabTipoVale = 2 THEN '#39'VALE PRESENTE'#39
      '            WHEN nCdTabTipoVale = 3 THEN '#39'VALE DESCONTO'#39
      '       END as cNmTipoVale'
      '      ,nValVale'
      '      ,nSaldoVale'
      '      ,dDtBaixa'
      '      ,CASE WHEN nCdLanctoFinRecompra IS NOT NULL THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END as cMsgRecompra'
      '  FROM Vale (NOLOCK)'
      ' WHERE nCdTerceiro = :nCdTerceiro'
      ' ORDER BY 1 DESC')
    Left = 468
    Top = 301
    object qryValenCdVale: TIntegerField
      FieldName = 'nCdVale'
    end
    object qryValedDtVale: TDateTimeField
      FieldName = 'dDtVale'
    end
    object qryValenCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryValecCodigo: TStringField
      FieldName = 'cCodigo'
      Size = 30
    end
    object qryValecNmTipoVale: TStringField
      FieldName = 'cNmTipoVale'
      ReadOnly = True
    end
    object qryValenValVale: TBCDField
      FieldName = 'nValVale'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryValenSaldoVale: TBCDField
      FieldName = 'nSaldoVale'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryValedDtBaixa: TDateTimeField
      FieldName = 'dDtBaixa'
    end
    object qryValecMsgRecompra: TStringField
      FieldName = 'cMsgRecompra'
      ReadOnly = True
      Size = 3
    end
  end
  object dsVale: TDataSource
    DataSet = qryVale
    Left = 500
    Top = 301
  end
  object qryConsignacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Cons.nCdConsignacao'
      '      ,Lj.cNmLoja'
      '      ,Vend.cNmLogin'
      '      ,Cons.nCdPedido'
      '      ,TSC.cTipoStatusConsig'
      
        '      ,convert(varchar(19), Cons.dDtPedido, 103) + '#39' - '#39' + Usu1.' +
        'cNmLogin       as cPedido'
      
        '      ,convert(varchar(19), Cons.dDtDevolucao, 103) + '#39' - '#39' + Us' +
        'u2.cNmLogin    as cDevolucao'
      
        '      ,convert(varchar(19), Cons.dDtFinalizacao, 103) + '#39' - '#39' + ' +
        'Usu3.cNmLogin  as cFinalizacao'
      
        '      ,convert(varchar(19), Cons.dDtCancelamento, 103) + '#39' - '#39' +' +
        ' Usu4.cNmLogin as cCancelamento'
      '  FROM Consignacao Cons'
      
        '       INNER JOIN Loja Lj              ON Lj.nCdLoja            ' +
        '  = Cons.nCdLoja'
      
        '       INNER JOIN Usuario Vend         ON Vend.nCdUsuario       ' +
        '  = Cons.nCdUsuario'
      
        '       LEFT JOIN Usuario Usu1          ON Usu1.nCdUsuario       ' +
        '  = Cons.nCdUsuarioPed'
      
        '       LEFT JOIN Usuario Usu2          ON Usu2.nCdUsuario       ' +
        '  = Cons.nCdUsuarioDev'
      
        '       LEFT JOIN Usuario Usu3          ON Usu3.nCdUsuario       ' +
        '  = Cons.nCdUsuarioFinaliza'
      
        '       LEFT JOIN Usuario Usu4          ON Usu4.nCdUsuario       ' +
        '  = Cons.nCdUsuarioCancel'
      
        '       INNER JOIN TipoStatusConsig TSC ON TSC.nCdTipoStatusConsi' +
        'g = Cons.nCdStatusConsig'
      ' WHERE Cons.nCdTerceiro = :nPK')
    Left = 468
    Top = 221
    object qryConsignacaonCdConsignacao: TIntegerField
      FieldName = 'nCdConsignacao'
    end
    object qryConsignacaocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryConsignacaocNmLogin: TStringField
      FieldName = 'cNmLogin'
    end
    object qryConsignacaonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryConsignacaocTipoStatusConsig: TStringField
      FieldName = 'cTipoStatusConsig'
      Size = 50
    end
    object qryConsignacaocPedido: TStringField
      FieldName = 'cPedido'
      ReadOnly = True
      Size = 42
    end
    object qryConsignacaocDevolucao: TStringField
      FieldName = 'cDevolucao'
      ReadOnly = True
      Size = 42
    end
    object qryConsignacaocFinalizacao: TStringField
      FieldName = 'cFinalizacao'
      ReadOnly = True
      Size = 42
    end
    object qryConsignacaocCancelamento: TStringField
      FieldName = 'cCancelamento'
      ReadOnly = True
      Size = 42
    end
  end
  object dsConsignacao: TDataSource
    DataSet = qryConsignacao
    Left = 500
    Top = 221
  end
  object qryCompraWeb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdCliente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.nCdLoja'
      '      ,CONVERT(varchar(10),Pedido.dDtPedido,103) as dDtPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,(SELECT SUM(ItemPedido.nQtdePed) '
      '          FROM ItemPedido'
      
        '         WHERE ItemPedido.nCdPedido = Pedido.nCdPedido) as nQtde' +
        'Ped '
      '      ,Pedido.nValProdutos'
      '      ,Pedido.nValFrete'
      '      ,Pedido.nValAcrescimo'
      '      ,Pedido.nValDesconto'
      '      ,nValPedido'
      '      ,TabStatusPed.cNmTabStatusPed'
      ' FROM Pedido (NOLOCK)'
      
        '      INNER JOIN TipoPedido   ON TipoPedido.nCdTipoPedido       ' +
        '= Pedido.nCdTipoPedido'
      
        '      INNER JOIN Terceiro     ON Terceiro.nCdTerceiro           ' +
        '= Pedido.nCdTerceiro'
      
        '      INNER JOIN TabStatusPed ON TabStatusPed.nCdTabStatusPed   ' +
        '= Pedido.nCdTabStatusPed'
      'WHERE Pedido.nCdEmpresa       = :nCdEmpresa'
      '  AND Pedido.nCdTerceiro      = :nCdCliente'
      '  AND Pedido.cFlgPedidoWeb    = 1'
      '  AND TipoPedido.cFlgVenda    = 1')
    Left = 468
    Top = 373
    object qryCompraWebnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryCompraWebdDtPedido: TStringField
      FieldName = 'dDtPedido'
      ReadOnly = True
      Size = 10
    end
    object qryCompraWebcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryCompraWebcNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryCompraWebnQtdePed: TBCDField
      FieldName = 'nQtdePed'
      ReadOnly = True
      Precision = 32
    end
    object qryCompraWebnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCompraWebnValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCompraWebnValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCompraWebcNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
    object qryCompraWebnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryCompraWebnValProdutos: TBCDField
      FieldName = 'nValProdutos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCompraWebnValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsCompraWeb: TDataSource
    DataSet = qryCompraWeb
    Left = 500
    Top = 374
  end
end
