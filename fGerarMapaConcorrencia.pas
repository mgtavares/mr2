unit fGerarMapaConcorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, DB, ADODB, Menus, cxControls, cxContainer, cxEdit,
  cxTextEdit, DBCtrls, Mask, cxLookAndFeelPainters, cxButtons, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, DBGridEhGrouping;

type
  TfrmGerarMapaConcorrencia = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    SP_SUGERIR_ITEM_MAPA: TADOStoredProc;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    qryTempcFlgAux: TIntegerField;
    qryTempnCdProduto: TIntegerField;
    qryTempcCdProduto: TStringField;
    qryTempcNmProduto: TStringField;
    qryTempcUnidadeMedida: TStringField;
    qryTempnQtdeComprar: TBCDField;
    qryTempnQtdeSugerida: TBCDField;
    qryTempnQtdeRequis: TBCDField;
    qryTempnQtdeEstoque: TBCDField;
    qryTempnQtdeEstoqueEmpenhado: TBCDField;
    qryTempnEstoqueDisponivel: TBCDField;
    qryTempiDiasEstoqueSeg: TBCDField;
    qryTempiDiasEstoqueRessup: TBCDField;
    qryTempiDiasEstoqueMax: TBCDField;
    qryTempnQtdePedido: TBCDField;
    qryTempnQtdeCotacao: TBCDField;
    qryTempnConsumoMedDia: TBCDField;
    qryTempcFlgManual: TIntegerField;
    qryTempnCdRequisicao: TIntegerField;
    qryTempnCdItemRequisicao: TIntegerField;
    qryTempnCdTipoRessuprimento: TIntegerField;
    qryTempnTempoEstoque: TIntegerField;
    qryTempiDiaEntrega: TIntegerField;
    qryTempcTipoEntrega: TStringField;
    qryTempdDtPrevEntrega: TDateTimeField;
    PopupMenu1: TPopupMenu;
    ExibirPedidos1: TMenuItem;
    ltimosPedidos1: TMenuItem;
    FornecedoresHomologados1: TMenuItem;
    FrmuladoProduto1: TMenuItem;
    N2: TMenuItem;
    DetalharEstoqueAtual1: TMenuItem;
    qryTempcFlgFornecHom: TIntegerField;
    CadastrodoProduto1: TMenuItem;
    qryAux: TADOQuery;
    qryTempnCdLoja: TIntegerField;
    qryTempcFlgTipoPonto: TStringField;
    cmdTempFornecedor: TADOCommand;
    cmdTempItens: TADOCommand;
    qryMapasGerados: TADOQuery;
    dsMapasGerados: TDataSource;
    qryMapasGeradosnCdMapaCompra: TIntegerField;
    qryMapasGeradoscNmMapaCompra: TStringField;
    ImageList2: TImageList;
    qryTempcFlgUrgente: TIntegerField;
    qryTempcNotaComprador: TStringField;
    qryTempcJustificativa: TStringField;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    qryTempcDescricaoTecnica: TMemoField;
    Label8: TLabel;
    DBMemo1: TDBMemo;
    cxButton1: TcxButton;
    N3: TMenuItem;
    ExibirRequisio1: TMenuItem;
    PopupMenu3: TPopupMenu;
    ImprimirMapa1: TMenuItem;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    qryTempcNmGrupoProduto: TStringField;
    cxStyleRepository1: TcxStyleRepository;
    LinhaAmarela: TcxStyle;
    LinhaVermelha: TcxStyle;
    LinhaAzul: TcxStyle;
    LinhaAmarelaForte: TcxStyle;
    LinhaTeal: TcxStyle;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ComboBox1: TComboBox;
    cxTextEdit5: TcxTextEdit;
    GroupBox3: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1cFlgAux: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeComprar: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeSugerida: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeRequis: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1nConsumoMedDia: TcxGridDBColumn;
    cxGrid1DBTableView1nTempoEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgTipoPonto: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasEstoqueSeg: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasEstoqueRessup: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasEstoqueMax: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdePedido: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeCotacao: TcxGridDBColumn;
    cxGrid1DBTableView1nCdRequisicao: TcxGridDBColumn;
    cxGrid1DBTableView1iDiaEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1cTipoEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgFornecHom: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    GroupBox4: TGroupBox;
    DBGridEh3: TDBGridEh;
    cxGrid1DBTableView1cFlgUrgente: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    CancelarMapa1: TMenuItem;
    qryCancelaMapaCompra: TADOQuery;
    procedure ToolButton6Click(Sender: TObject);
    procedure qryTempBeforeDelete(DataSet: TDataSet);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryTempAfterScroll(DataSet: TDataSet);
    procedure ToolButton7Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ExibirPedidos1Click(Sender: TObject);
    procedure FornecedoresHomologados1Click(Sender: TObject);
    procedure FrmuladoProduto1Click(Sender: TObject);
    procedure DetalharEstoqueAtual1Click(Sender: TObject);
    procedure CadastrodoProduto1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ltimosPedidos1Click(Sender: TObject);
    procedure ComboBox1CloseUp(Sender: TObject);
    procedure GroupBox1Exit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ExibirRequisio1Click(Sender: TObject);
    procedure ImprimirMapa1Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure CancelarMapa1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerarMapaConcorrencia: TfrmGerarMapaConcorrencia;

implementation

uses fMenu, fMapaConcorrencia_Fornecedor, fPedidoAbertoProduto,
  fMRP_FornecHomol, fMRP_FormulaProd, fMRP_GeraPedido,
  fMRP_DetalheEstoqueAtual, fProdutoERP, rPedCom_Contrato,
  fPedCompraContrato, fMRP_UltimosPedidos, fRequisicao, rMapaCompra;

{$R *.dfm}

procedure TfrmGerarMapaConcorrencia.ToolButton6Click(Sender: TObject);
begin
  inherited;

  Screen.cursor := crSQLWait;

  try
      SP_SUGERIR_ITEM_MAPA.Close ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@nCdEmpresa').Value           := frmMenu.nCdEmpresaAtiva ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@nCdTipoRessuprimento').Value := 1 ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@cFlgPedidoConfirmado').Value := 0 ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@cFlgPontoRessup').Value      := 0 ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@cFlgEstoqueEmpenhado').Value := 0 ;
      SP_SUGERIR_ITEM_MAPA.ExecProc;
  except
      Screen.cursor := crdefault;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  qryTemp.Close ;
  qryTemp.Parameters.ParamByName('nCdRequisicao').Value := 0 ;
  qryTemp.Open ;

  Screen.cursor := crdefault;

  if (qryTemp.eof) then
  begin
      MensagemAlerta('Nenhum item para compra.') ;
      exit ;
  end ;

  ComboBox1.Items.Clear;
  ComboBox1.Items.Add('-- Todas --') ;
  ComboBox1.ItemIndex := 0 ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT DISTINCT nCdRequisicao FROM #Temp_Itens WHERE nCdRequisicao > 0 ORDER BY nCdRequisicao') ;
  qryAux.Open ;

  if not qryAux.Eof then
  begin
      ComboBox1.Enabled   := True ;

      qryAux.First ;

      while not qryAux.Eof do
      begin
          Combobox1.Items.Add(qryAux.FieldList[0].AsString) ;
          qryAux.Next ;
      end ;

      qryAux.Close;

  end ;

end;

procedure TfrmGerarMapaConcorrencia.qryTempBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  if (qryTempcFlgManual.Value = 0) then
  begin
      MensagemAlerta('N�o � poss�vel excluir item autom�tico.') ;
      abort ;
  end ;

end;

procedure TfrmGerarMapaConcorrencia.qryTempBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryTempnQtdeComprar.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  if (qryTempnQtdeComprar.Value > 0) and (qryTempnQtdePedido.Value + qryTempnQtdeCotacao.Value > 0) and (qryTempnCdTipoRessuprimento.Value = 1) then
  begin
      MensagemAlerta('Aten��o!!!') ;
      
      case MessageDlg('O Item j� est� em processo de compra! Confirma a compra de mais ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;
  end ;

end;

procedure TfrmGerarMapaConcorrencia.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if not qryTemp.Active then
      qryTemp.Open ;

  qryTemp.Last ;
  qryTemp.Insert ;

end;

procedure TfrmGerarMapaConcorrencia.ToolButton1Click(Sender: TObject);
var
    iItens : integer ;
    objForm : TfrmMapaConcorrencia_Fornecedor;
begin
  inherited;

  objForm := TfrmMapaConcorrencia_Fornecedor.Create(nil);

  if not qryTemp.Active then
      MensagemAlerta('Nenhum item dispon�vel para compra.') ;

  if (qryTemp.State <> dsBrowse) then
      qryTemp.Post ;

  qryTemp.First ;


  iItens := 0 ;

  While not qryTemp.eof do
  begin

      if (qryTempcFlgAux.Value = 1) and (qryTempnQtdeComprar.Value > 0) then
          iItens := iItens + 1 ;

      qryTemp.Next ;

  end ;

  qryTemp.First ;

  if (iItens = 0) then
  begin
      MensagemAlerta('Nenhum item selecionado ou nenhuma quantidade informada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o de um novo mapa de concorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  objForm.LimpaDados;
  showForm(objForm,true);

  qryMapasGerados.Close ;
  qryMapasGerados.Open ;

  ToolButton6.Click;

end;

procedure TfrmGerarMapaConcorrencia.qryTempAfterScroll(DataSet: TDataSet);
begin
  inherited;

  ToolButton7.Enabled := False ;
  ToolButton9.Enabled := False ;

  if (qryTempnQtdePedido.Value > 0) then
      ToolButton7.Enabled := True ;

  if (qryTempnCdItemRequisicao.Value > 0) then
      ToolButton9.Enabled := True ;

end;

procedure TfrmGerarMapaConcorrencia.ToolButton7Click(Sender: TObject);
var
  objForm : TfrmPedidoAbertoProduto;
begin
  inherited;

  objForm := TfrmPedidoAbertoProduto.Create(nil);

  objForm.qryMaster.Close ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  objForm.qryMaster.Parameters.ParamByName('nCdProduto').Value := qryTempnCdProduto.Value ;
  objForm.qryMaster.Open ;

  if not frmPedidoAbertoProduto.qryMaster.Eof then
      showForm(objForm,true);
      
end;

procedure TfrmGerarMapaConcorrencia.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  ExibirPedidos1.Enabled           := False ;
  ltimosPedidos1.Enabled           := True  ;
  FornecedoresHomologados1.Enabled := True  ;
  FrmuladoProduto1.Enabled         := True  ;
  DetalharEstoqueAtual1.Enabled    := True  ;
  CadastrodoProduto1.Enabled       := True  ;

  if not qryTemp.Active then
      exit ;

  if (qryTemp.Eof) then
      exit ;

  if (qryTempcCdProduto.Value = 'AD') then
  begin
    ltimosPedidos1.Enabled           := False ;
    FornecedoresHomologados1.Enabled := False ;
    FrmuladoProduto1.Enabled         := False ;
    DetalharEstoqueAtual1.Enabled    := False ;
    CadastrodoProduto1.Enabled       := False ;
  end ;

  if (qryTempnQtdePedido.Value > 0) then
      ExibirPedidos1.Enabled := True ;

end;

procedure TfrmGerarMapaConcorrencia.ExibirPedidos1Click(Sender: TObject);
var
  objForm : TfrmPedidoAbertoProduto;
begin
  inherited;

  objForm := TfrmPedidoAbertoProduto.Create(nil);

  objForm.qryMaster.Close ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  objForm.qryMaster.Parameters.ParamByName('nCdProduto').Value := qryTempnCdProduto.Value ;
  objForm.qryMaster.Parameters.ParamByName('nCdLoja').Value    := qryTempnCdLoja.Value    ;
  objForm.qryMaster.Open ;

  if not frmPedidoAbertoProduto.qryMaster.Eof then
      showForm(objForm,true);
  
end;

procedure TfrmGerarMapaConcorrencia.FornecedoresHomologados1Click(
  Sender: TObject);
var
  objForm : TfrmMRP_FornecHomol;
begin
  inherited;

  objForm := TfrmMRP_FornecHomol.Create(nil);

  PosicionaQuery(objForm.qryFornecedores, qryTempnCdProduto.AsString) ;
  showForm(objForm,true) ;
end;

procedure TfrmGerarMapaConcorrencia.FrmuladoProduto1Click(Sender: TObject);
var
  objForm : TfrmMRP_FormulaProd;
begin
  inherited;

  objForm := TfrmMRP_FormulaProd.Create(nil);

  PosicionaQuery(objForm.qryFormula, qryTempnCdProduto.AsString);
  showForm(objForm,true) ;
end;

procedure TfrmGerarMapaConcorrencia.DetalharEstoqueAtual1Click(
  Sender: TObject);
var
  objForm : TfrmMRP_DetalheEstoqueAtual;
begin
  inherited;

  objForm := TfrmMRP_DetalheEstoqueAtual.Create(nil);

  PosicionaQuery(objForm.qryPosicaoEstoque, qryTempnCdProduto.AsString) ;
  showForm(objForm,true) ;

end;

procedure TfrmGerarMapaConcorrencia.CadastrodoProduto1Click(
  Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create(nil);

  qryAux.Close ;
  qryAux.SQL.Text := ('SELECT IsNull(nCdProdutoPai, nCdProduto) FROM Produto WHERE nCdProduto = ' + qryTempnCdProduto.AsString) ;
  qryAux.Open ;

  if not qryAux.Eof and (qryAux.FieldList[0].Value > 0) then
  begin

      PosicionaQuery(objForm.qryMaster, qryAux.FieldList[0].Value) ;

      objForm.btIncluir.Enabled    := False ;
      objForm.btSalvar.Enabled     := False ;
      objForm.btCancelar.Enabled   := False ;
      objForm.btConsultar.Enabled  := False ;
      objForm.ToolButton1.Enabled  := False ;
      objForm.btGerarGrade.Enabled := False ;
      objForm.cxButton1.Enabled    := False ;
      objForm.btExcluir.Enabled    := False ;

      showForm(objForm,true) ;

  end ;

end;

procedure TfrmGerarMapaConcorrencia.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      cxGrid1DBTableView1nCdLoja.Visible := False ;
      
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM Usuario WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND cFlgComprador = 1') ;
  qryAux.Open ;

  if (qryAux.Eof) then
  begin
      MensagemAlerta('Somente usu�rios compradores podem operar nesta tela.') ;
      ToolButton6.Enabled := False ;
  end ;

  cmdTempItens.Execute;

  qryMapasGerados.Close ;
  qryMapasGerados.Open ;

end;

procedure TfrmGerarMapaConcorrencia.ltimosPedidos1Click(Sender: TObject);
var
  objForm : TfrmMRP_UltimosPedidos;
begin
  inherited;

  objForm := TfrmMRP_UltimosPedidos.Create(nil);

  if (qryTempnCdProduto.Value > 0) then
  begin
      objForm.qryResultado.Close ;
      objForm.qryResultado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      objForm.qryResultado.Parameters.ParamByName('nCdProduto').Value := qryTempnCdProduto.Value ;
      objForm.qryResultado.Open ;

      showForm(objForm,true);
  end ;

end;

procedure TfrmGerarMapaConcorrencia.ComboBox1CloseUp(Sender: TObject);
begin
  inherited;

  qryTemp.Close ;

  if (ComboBox1.Text <> '-- Todas --') then
      qryTemp.Parameters.ParamByName('nCdRequisicao').Value := StrToInt(ComboBox1.Text)
  else qryTemp.Parameters.ParamByName('nCdRequisicao').Value := 0 ;

  qryTemp.Open ;


end;

procedure TfrmGerarMapaConcorrencia.GroupBox1Exit(Sender: TObject);
begin
  inherited;

  GroupBox1.Visible := False ;
end;

procedure TfrmGerarMapaConcorrencia.cxButton1Click(Sender: TObject);
begin
  inherited;

  GroupBox1.Visible := False ;

end;

procedure TfrmGerarMapaConcorrencia.ExibirRequisio1Click(Sender: TObject);
var
  objForm : TfrmRequisicao;
begin
  inherited;

  objForm := TfrmRequisicao.Create(nil);

  if not qryTemp.Eof and (qryTempnCdRequisicao.Value > 0) then
  begin
      objForm.btIncluir.Visible := False ;
      objForm.btSalvar.Visible := False ;
      objForm.btExcluir.Visible := False ;

      objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      objForm.PosicionaPK(qryTempnCdRequisicao.Value);

      objForm.DBGridEh1.ReadOnly := True ;
      objForm.DBGridEh2.ReadOnly := True ;
      objForm.DBGridEh3.ReadOnly := True ;

      showForm(objForm,true);
  end ;

end;

procedure TfrmGerarMapaConcorrencia.ImprimirMapa1Click(Sender: TObject);
var
  objRel : TrptMapaCompra;
begin
  inherited;

  if not qryMapasGerados.Active then
  begin
      MensagemAlerta('Nenhum mapa selecionado.') ;
      exit ;
  end ;

  objRel := TrptMapaCompra.Create(nil);
  try
      try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMapaCompra').Value := qryMapasGeradosnCdMapaCompra.Value ;
          objRel.usp_Relatorio.Open ;

          objRel.SPREL_MAPA_HEADER.Close ;
          objRel.SPREL_MAPA_HEADER.Parameters.ParamByName('@nCdMapaCompra').Value := qryMapasGeradosnCdMapaCompra.Value ;
          objRel.SPREL_MAPA_HEADER.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.QuickRep1.Preview;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmGerarMapaConcorrencia.ToolButton9Click(Sender: TObject);
begin
  inherited;

  if not qryTemp.eof and (qryTempnCdItemRequisicao.Value > 0) then
  begin

      case MessageDlg('Confirma o cancelamento deste item?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE ItemRequisicao Set nQtdeCanc = (nQtdeReq - nQtdeAtend) WHERE nCdItemRequisicao = ' + qryTempnCdItemRequisicao.AsString) ;
      qryAux.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryTemp.Delete;

  end ;

end;

procedure TfrmGerarMapaConcorrencia.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGrid1DBTableView1cNmProduto) then
      if (ARecord.Values[25] = 1) then
          AStyle := LinhaAmarelaForte ;

end;

procedure TfrmGerarMapaConcorrencia.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if not qryTemp.Eof and (qryTempnCdRequisicao.Value > 0) then
  begin
      GroupBox1.Visible := True ;
      cxButton1.SetFocus;
  end ;

end;

procedure TfrmGerarMapaConcorrencia.CancelarMapa1Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma o cancelamento deste mapa de compra ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryCancelaMapaCompra.Close ;
      qryCancelaMapaCompra.Parameters.ParamByName('nPK').Value        := qryMapasGeradosnCdMapaCompra.Value ;
      qryCancelaMapaCompra.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryCancelaMapaCompra.ExecSQL;

      qryMapasGerados.Delete;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Mapa Cancelado com Sucesso. As requisi��es foram reabertas.') ;

end;

initialization
    RegisterClass(TfrmGerarMapaConcorrencia) ;

end.
