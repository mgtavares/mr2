unit fMetaVendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DBCtrls, Mask, DB, ADODB,
  GridsEh, DBGridEh, cxPC, cxControls, DBGridEhGrouping;

type
  TfrmMetaVendedor = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    edtMesAno: TMaskEdit;
    DBEdit2: TDBEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    edtEmpresa: TMaskEdit;
    Label2: TLabel;
    edtLoja: TMaskEdit;
    DBEdit1: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryMetaSemana: TADOQuery;
    dsMetaSemana: TDataSource;
    qryMetaSemananCdMetaSemana: TIntegerField;
    qryMetaSemananCdMetaMes: TIntegerField;
    qryMetaSemanaiSemana: TIntegerField;
    qryMetaSemananValMetaMinima: TBCDField;
    qryMetaSemananValMeta: TBCDField;
    qryMetaSemananValVendaMediaPrev: TBCDField;
    qryMetaSemananItensPorVendaPrev: TBCDField;
    qryMetaSemananValItemMedioPrev: TBCDField;
    qryMetaSemananValVendaHoraPrev: TBCDField;
    qryMetaSemananTxConversaoPrev: TBCDField;
    qryMetaSemananPercDistrib: TBCDField;
    qryMetaSemanaiTotalHorasReal: TBCDField;
    qryMetaSemananPercentReal: TBCDField;
    qryMetaSemanaiTotalHorasPrev: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure edtEmpresaChange(Sender: TObject);
    procedure edtLojaChange(Sender: TObject);
    procedure edtMesAnoChange(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMetaVendedor: TfrmMetaVendedor;

implementation

uses fMenu, fLookup_Padrao, fMetaVendedor_CotaVendedor;

{$R *.dfm}

procedure TfrmMetaVendedor.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtLoja.Text    := IntToStr(frmMenu.nCdLojaAtiva);

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmMetaVendedor.cxButton1Click(Sender: TObject);
var
    iMes, iAno : integer;
begin
  inherited;

  {-- verifica se a data digitada � valida --}
  if (trim(edtMesAno.Text) = '/') then
  begin
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
  end ;

  edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end ;

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  {-- se os campos de empresa ou loja estiverem em branco usa a loja e/ou empresa ativa --}

  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  if (Trim(edtLoja.Text) = '') then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva);

      qryLoja.Close;
      PosicionaQuery(qryLoja, edtLoja.Text) ;
  end;

  {-- exibe os valores da busca --}

  qryMetaSemana.Close;
  qryMetaSemana.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text);
  qryMetaSemana.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(edtLoja.Text);
  qryMetaSemana.Parameters.ParamByName('iMes').Value       := iMes;
  qryMetaSemana.Parameters.ParamByName('iAno').Value       := iAno;
  qryMetaSemana.Open;

  DBGridEh1.SetFocus;

end;

procedure TfrmMetaVendedor.edtEmpresaChange(Sender: TObject);
begin
  inherited;
  
  qryMetaSemana.Close;
end;

procedure TfrmMetaVendedor.edtLojaChange(Sender: TObject);
begin
  inherited;

  qryMetaSemana.Close;
end;

procedure TfrmMetaVendedor.edtMesAnoChange(Sender: TObject);
begin
  inherited;

  qryMetaSemana.Close;
end;

procedure TfrmMetaVendedor.cxButton2Click(Sender: TObject);
begin
  inherited;

  qryMetaSemana.Close;
  qryLoja.Close;
  qryEmpresa.Close;
  
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtLoja.Text    := IntToStr(frmMenu.nCdLojaAtiva);
  edtMesAno.Text  := '';

  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

  edtMesAno.SetFocus;
end;

procedure TfrmMetaVendedor.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

procedure TfrmMetaVendedor.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            qryEmpresa.Close;

            edtEmpresa.Text := IntToStr(nPK);
            PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMetaVendedor.edtLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            qryLoja.Close;

            edtLoja.Text := IntToStr(nPK);
            PosicionaQuery(qryLoja, edtLoja.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMetaVendedor.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmMetaVendedor.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmMetaVendedor_CotaVendedor;
begin
  inherited;

  if (qryMetaSemana.Active) then
  begin
      if(qryMetaSemana.RecordCount > 0) then
      begin

          {if (qryMetaSemanaiTotalHorasPrev.Value <= 0) then
          begin
              MensagemAlerta('Esta semana n�o tem horas de venda prevista, imposs�vel atribuir hora de venda aos vendedores. Verifique.') ;
              abort ;
          end ;}

          {-- lista os vendedores cadastrados para a loja, se eles n�o existirem na
           -- tabela MetaVendedor, os mesmos s�o inseridos --}

           objForm := TfrmMetaVendedor_CotaVendedor.Create(nil);

           //objForm.iTotalHorasPrev := qryMetaSemanaiTotalHorasPrev.Value;
           objForm.nValMetaMinima  := qryMetaSemananValMetaMinima.Value;
           objForm.nValMeta        := qryMetaSemananValMeta.Value;
           objForm.nCdLoja         := frmMenu.ConvInteiro(edtLoja.Text);
           objForm.nCdMetaSemana   := qryMetaSemananCdMetaSemana.Value;
           
           frmMenu.Connection.BeginTrans;

           try
               objForm.qryCotaVendedores.Close;
               objForm.qryCotaVendedores.Parameters.ParamByName('nCdMetaSemana').Value := qryMetaSemananCdMetaSemana.Value;
               objForm.qryCotaVendedores.Parameters.ParamByName('nCdLoja').Value       := frmMenu.ConvInteiro(edtLoja.Text);
               objForm.qryCotaVendedores.Open;
           except
               frmMenu.Connection.RollbackTrans;
               MensagemErro('Erro no Processamento');
               raise;
           end;

           frmMenu.Connection.CommitTrans;

           showForm(objForm,true);

           qryMetaSemana.Close;
           qryMetaSemana.Open;
      end;
  end;
end;

procedure TfrmMetaVendedor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

initialization
    RegisterClass(TfrmMetaVendedor);

end.
