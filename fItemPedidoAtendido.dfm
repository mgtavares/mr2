inherited frmItemPedidoAtendido: TfrmItemPedidoAtendido
  Left = 423
  Top = 209
  Width = 533
  Height = 384
  BorderIcons = [biSystemMenu]
  Caption = 'Atendimento de Pedido'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 517
    Height = 317
  end
  inherited ToolBar1: TToolBar
    Width = 517
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 517
    Height = 317
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'iNrDocto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cSerie'
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'dDtAutor'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtAtendimento'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iDias'
        Footers = <>
        Title.Caption = 'Dias para Atendimento'
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeAtendida'
        Footers = <>
        Title.Caption = 'Quant. Atendida'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryItem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT iNrDocto'
      '      ,cSerie'
      '      ,nCdRecebimento'
      '      ,dDtAutor'
      '      ,dDtAtendimento'
      '      ,ItemPedidoAtendido.nQtdeAtendida'
      
        '      ,Convert(int,(dbo.fn_OnlyDate(dDtAtendimento)-dbo.fn_OnlyD' +
        'ate(dDtAutor))) as iDias'
      '  FROM ItemPedidoAtendido'
      
        '       LEFT JOIN DoctoFiscal ON DoctoFiscal.nCdDoctoFiscal = Ite' +
        'mPedidoAtendido.nCdDoctoFiscal'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdItemPedido = ItemP' +
        'edidoAtendido.nCdItemPedido'
      
        '       INNER JOIN Pedido     ON Pedido.nCdPedido         = ItemP' +
        'edido.nCdPedido'
      ' WHERE ItemPedidoAtendido.nCdItemPedido = :nPK')
    Left = 376
    Top = 160
    object qryItemiNrDocto: TIntegerField
      DisplayLabel = 'Nota Fiscal|N'#250'mero'
      FieldName = 'iNrDocto'
    end
    object qryItemcSerie: TStringField
      DisplayLabel = 'Nota Fiscal|S'#233'rie'
      FieldName = 'cSerie'
      FixedChar = True
      Size = 2
    end
    object qryItemnCdRecebimento: TIntegerField
      DisplayLabel = 'Recebimento|Compra'
      FieldName = 'nCdRecebimento'
    end
    object qryItemdDtAutor: TDateTimeField
      DisplayLabel = 'Datas|Pedido'
      FieldName = 'dDtAutor'
    end
    object qryItemdDtAtendimento: TDateTimeField
      DisplayLabel = 'Datas|Atendimento'
      FieldName = 'dDtAtendimento'
    end
    object qryItemnQtdeAtendida: TBCDField
      DisplayLabel = 'Quant.|Atendida'
      FieldName = 'nQtdeAtendida'
      Precision = 12
      Size = 2
    end
    object qryItemiDias: TIntegerField
      DisplayLabel = 'Dias p/|Atendimento'
      FieldName = 'iDias'
      ReadOnly = True
    end
  end
  object dsItem: TDataSource
    DataSet = qryItem
    Left = 288
    Top = 168
  end
end
