inherited frmFollowUP_Requisicao_Filtro: TfrmFollowUP_Requisicao_Filtro
  Left = 345
  Top = 288
  Width = 709
  Height = 288
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Filtro FollowUp'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 693
    Height = 221
  end
  object Label1: TLabel [1]
    Left = 65
    Top = 40
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 32
    Top = 88
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Requisi'#231#227'o'
  end
  object Label3: TLabel [3]
    Left = 29
    Top = 208
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Atend.'
  end
  object Label6: TLabel [4]
    Left = 196
    Top = 208
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 86
    Top = 64
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label7: TLabel [6]
    Left = 77
    Top = 160
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label9: TLabel [7]
    Left = 80
    Top = 136
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Setor'
  end
  object Label11: TLabel [8]
    Left = 68
    Top = 184
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label10: TLabel [9]
    Left = 41
    Top = 112
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo'
  end
  object Label4: TLabel [10]
    Left = 16
    Top = 232
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Requisi'#231#227'o'
  end
  object Label8: TLabel [11]
    Left = 196
    Top = 232
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 693
    TabOrder = 15
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    object ToolButton5: TToolButton
      Left = 166
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 174
      Top = 0
      Hint = 'Limpar Filtros'
      Caption = 'Limpar'
      ImageIndex = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton4Click
    end
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 180
    Top = 32
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [14]
    Tag = 1
    Left = 243
    Top = 32
    Width = 446
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object mskDtAtendInicial: TMaskEdit [15]
    Left = 112
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 11
    Text = '  /  /    '
  end
  object mskDtAtendFinal: TMaskEdit [16]
    Left = 216
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 12
    Text = '  /  /    '
  end
  object DBEdit5: TDBEdit [17]
    Tag = 1
    Left = 180
    Top = 152
    Width = 509
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 8
  end
  object DBEdit7: TDBEdit [18]
    Tag = 1
    Left = 180
    Top = 176
    Width = 509
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 10
  end
  object er2LkpLoja: TER2LookupMaskEdit [19]
    Left = 112
    Top = 56
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object er2LkpTipoRequisicao: TER2LookupMaskEdit [20]
    Left = 112
    Top = 80
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 121
    QueryLookup = qryTipoRequisicao
  end
  object er2LkpCentroCusto: TER2LookupMaskEdit [21]
    Left = 112
    Top = 104
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    OnBeforeLookup = er2LkpCentroCustoBeforeLookup
    OnBeforePosicionaQry = er2LkpCentroCustoBeforePosicionaQry
    CodigoLookup = 157
    QueryLookup = qryCC
  end
  object er2LkpSetor: TER2LookupMaskEdit [22]
    Left = 112
    Top = 128
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 6
    Text = '         '
    OnBeforeLookup = er2LkpSetorBeforeLookup
    OnBeforePosicionaQry = er2LkpSetorBeforePosicionaQry
    CodigoLookup = 122
    QueryLookup = qrySetor
  end
  object er2LkpMarca: TER2LookupMaskEdit [23]
    Left = 112
    Top = 152
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 7
    Text = '         '
    CodigoLookup = 49
    QueryLookup = qryMarca
  end
  object er2LkpProduto: TER2LookupMaskEdit [24]
    Left = 112
    Top = 176
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 9
    Text = '         '
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object edtEmpresa: TDBEdit [25]
    Tag = 1
    Left = 112
    Top = 32
    Width = 65
    Height = 21
    DataField = 'nCdEmpresa'
    DataSource = dsEmpresa
    TabOrder = 0
  end
  object DBEdit1: TDBEdit [26]
    Tag = 1
    Left = 180
    Top = 56
    Width = 509
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 16
  end
  object DBEdit4: TDBEdit [27]
    Tag = 1
    Left = 180
    Top = 80
    Width = 509
    Height = 21
    DataField = 'cNmTipoRequisicao'
    DataSource = dsTipoRequisicao
    TabOrder = 17
  end
  object DBEdit6: TDBEdit [28]
    Tag = 1
    Left = 180
    Top = 104
    Width = 509
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCC
    TabOrder = 18
  end
  object DBEdit9: TDBEdit [29]
    Tag = 1
    Left = 180
    Top = 128
    Width = 509
    Height = 21
    DataField = 'cNmSetor'
    DataSource = dsSetor
    TabOrder = 19
  end
  object mskDtRequisicaoInicial: TMaskEdit [30]
    Left = 112
    Top = 224
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object mskDtRequisicaoFinal: TMaskEdit [31]
    Left = 216
    Top = 224
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 14
    Text = '  /  /    '
  end
  inherited ImageList1: TImageList
    Left = 600
    Top = 152
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C
      6F009C8C6F009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED9000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F009C8C
      6F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000009C8C6F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFC030000
      FE00C003C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C007000000018001C00F000000038001C01F0000
      0077C003C03F0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      '   AND nCdEmpresa = :nPK')
    Left = 376
    Top = 152
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 376
    Top = 184
  end
  object dsTipoRequisicao: TDataSource
    DataSet = qryTipoRequisicao
    Left = 440
    Top = 184
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM Loja'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioLoja'
      '               WHERE UsuarioLoja.nCdLoja    = Loja.nCdLoja'
      '                 AND UsuarioLoja.nCdUsuario = :nPK)')
    Left = 408
    Top = 152
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 408
    Top = 184
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca'
      '      ,cNmMarca'
      '  FROM Marca'
      ' WHERE nCdMarca = :nPK')
    Left = 536
    Top = 152
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 536
    Top = 184
  end
  object qryTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TipoRequisicao'
      ' WHERE nCdTipoRequisicao = :nPK')
    Left = 440
    Top = 152
    object qryTipoRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryTipoRequisicaocNmTipoRequisicao: TStringField
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
    object qryTipoRequisicaonCdTabTipoRequis: TIntegerField
      FieldName = 'nCdTabTipoRequis'
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTipoRequisicao'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CentroCusto.nCdCC'
      '      ,CentroCusto.cNmCC'
      '  FROM CentroCusto'
      
        '       INNER JOIN CentroCustoTipoRequisicao CCT ON CCT.nCdCC = C' +
        'entroCusto.nCdCC'
      ' WHERE CentroCusto.nCdCC     = :nPK'
      '   AND CCT.nCdTipoRequisicao = :nCdTipoRequisicao'
      '')
    Left = 472
    Top = 152
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCC: TDataSource
    DataSet = qryCC
    Left = 472
    Top = 184
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 568
    Top = 152
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 568
    Top = 184
  end
  object qrySetor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Setor'
      ' WHERE nCdSetor = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioSetor'
      '               WHERE UsuarioSetor.nCdSetor   = Setor.nCdSetor'
      '                 AND UsuarioSetor.nCdUsuario = :nCdUsuario)'
      
        '   AND (  ((dbo.fn_LeParametro('#39'BLOQSETORREQUIS'#39') = '#39'S'#39') AND (nC' +
        'dCC IS NOT NULL))'
      '        OR (dbo.fn_LeParametro('#39'BLOQSETORREQUIS'#39') = '#39'N'#39'))')
    Left = 504
    Top = 152
    object qrySetornCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qrySetorcNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
    object qrySetornCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
  end
  object dsSetor: TDataSource
    DataSet = qrySetor
    Left = 504
    Top = 184
  end
end
