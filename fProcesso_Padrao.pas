unit fProcesso_Padrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, StdCtrls,
  ImgList, ComCtrls, ToolWin, ExtCtrls ,
  Dialogs, DBGridEH,ADODB,DB,DBCtrls,Mask, cxGridDBTableView, cxGrid, cxGraphics,
  cxLookAndFeels, cxCurrencyEdit, cxPC, ER2Lookup;

type
  TfrmProcesso_Padrao = class(TForm)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure NavegaEdit(Sender: TObject; var Key: Char);
    procedure FormDeactivate(Sender: TObject);
    procedure TrataData(Sender: TField; const Text: string) ;
    function InputQuery(const ACaption, APrompt: string; var Value: string): Boolean;
    procedure showForm(objForm: TForm; bDestroirObjeto : boolean);
    procedure desativaDBEdit (obj : TDBEdit) ;
    procedure ativaDBEdit (obj : TDBEdit) ;
    procedure desativaMaskEdit (obj : TMaskEdit) ;
    procedure ativaMaskEdit (obj : TMaskEdit) ;
    procedure desativaER2LkpMaskEdit (obj : TER2LookupMaskEdit) ;
    procedure ativaER2LkpMaskEdit (obj : TER2LookupMaskEdit) ;
    
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProcesso_Padrao: TfrmProcesso_Padrao;

implementation

uses fMenu, fCadastro_Template, fMensagem;

{$R *.dfm}

function TfrmProcesso_Padrao.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmProcesso_Padrao.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
    {frmMenu.nShowMessage(cTexto);}
end;

procedure TfrmProcesso_Padrao.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
    {frmMenu.nMensagemErro(cTexto);}
end;

procedure TfrmProcesso_Padrao.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
    {frmMenu.nMensagemAlerta(cTexto);}
end;


procedure TfrmProcesso_Padrao.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmProcesso_Padrao.FormCreate(Sender: TObject);
var
    i : Integer;
    i2 : integer ;
begin

  Application.OnException := frmMenu.TrataErros ;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TcxPageControl) then
    begin

        (Components [I] as TcxPageControl).Font.Name               := 'Calibri' ;
        (Components [I] as TcxPageControl).Font.Size               := 9 ;
        (Components [I] as TcxPageControl).Font.Style              := [] ;
        (Components [I] as TcxPageControl).LookAndFeel.NativeStyle := True ;

    end ;

    if (Components [I] is TGroupBox) then
    begin
       (Components [I] as TGroupBox).Font.Name    := 'Calibri' ;
       (Components [I] as TGroupBox).Font.Size    := 8 ;
       (Components [I] as TGroupBox).Font.Style   := [] ;
    end ;

    if (Components [I] is TCheckBox) then
    begin
       (Components [I] as TCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TCheckBox).Font.Size    := 8 ;
       (Components [I] as TCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDBCheckBox) then
    begin
       (Components [I] as TDBCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TDBCheckBox).Font.Size    := 8 ;
       (Components [I] as TDBCheckBox).Font.Style   := [fsBold] ;
    end ;
  
    if (Components [I] is TField) then
    begin
        (Components [I] as TField).OnSetText := TrataData ;
    end ;

    if (Components [I] is TDBGridEh) then
    begin
      (Components [I] as TDBGridEh).Flat := True ;
      (Components [I] as TDBGridEh).FixedColor := clMoneyGreen ;
      //(Components [I] as TDBGridEh).OddRowColor := clAqua ;
      (Components [I] as TDBGridEh).UseMultiTitle := True ;

      (Components [I] as TDBGridEh).OptionsEh := [dghColumnResize,dghFixed3D,dghHighlightFocus,dghClearSelection,dghEnterAsTab,dghDialogFind] ;

      if ((Components [I] as TDBGridEh).Tag = 1) then
      begin
          (Components [I] as TDBGridEh).ParentFont := False ;
          (Components [I] as TDBGridEh).ReadOnly   := True ;
          (Components [I] as TDBGridEh).Color      := clSilver ;
          (Components [I] as TDBGridEh).Font.Color := clYellow ;
          Update;
      end ;

      (Components [I] as TDBGridEh).Font.Name := 'Calibri' ;
      (Components [I] as TDBGridEh).Font.Size := 9 ;

      (Components [I] as TDBGridEh).TitleFont.Name   := 'Calibri' ;
      (Components [I] as TDBGridEh).TitleFont.Size   := 9 ;
      
      (Components [I] as TDBGridEh).FooterFont.Name  := 'Calibri' ;

      if ((Components [I] as TDBGridEh).FooterFont.Color <> clWhite) then
          (Components [I] as TDBGridEh).FooterFont.Color := clBlack ;

      (Components [I] as TDBGridEh).FooterFont.Style := [fsBold] ;
      (Components [I] as TDBGridEh).FooterFont.Size  := 9 ;

      // padroniza as colunas

      with (Components[I] as TDBGridEH) do
      begin
          OptionsEH := OptionsEH + [dghAutoSortMarking] ;

          with Columns do
          begin

            for i2 := 0 to Count - 1 do
            begin

                OnSortMarkingChanged := nil;

                If Items[0].ReadOnly then
                begin
                    Items[0].Title.Font.Color := clRed;
                    {Items[0].Font.Color      := clTeal; //clGray ;}
                end ;

            end ;


          end ;

          if Columns.Count = 0 then
            exit ;

          //FooterRowCount := 1;
          //Columns.Items [0].Footer.ValueType := fvtCount;

          for i2 := 1 to Columns.Count - 1 do
          begin
              with Columns.Items[i2] do
              begin

                  Footer.Font.Name := 'Calibri' ;
                  Footer.Font.Size := 9 ;

                  if Field.DataType in [ftSmallint, ftInteger, ftLargeint, ftWord, ftFloat,ftCurrency, ftBCD] then
                  begin
                      Footer.ValueType := fvtSum;
                  end ;

                  if (Field.DataType in [ftFloat, ftCurrency, ftBCD]) and (Tag = 0) then
                  begin
                      Columns.Items [i2].DisplayFormat := '#,##0.00';
                      Footer.DisplayFormat := '#,##0.00';
                  end ;

                  if (Field.DataType in [ftDateTime]) and (Tag <> 2) then
                  begin
                      Columns.Items [i2].EditMask := '!99/99/9999;1;_';
                      ButtonStyle := cbsNone ;
                      Columns.Items [i2].Width    := 77 ;
                  end ;

                  If ReadOnly then
                  begin
                      Title.Font.Color := clRed ;
                      {Font.Color := clSilver} ; //clGray ;}
                      Columns.Items [i2].Color := clSilver ;

                  end ;

                  //Font.Style := [fsBold] ;
                  {Font.Name  := 'Tahoma' ;
                  Font.Size  := 7 ;}

              end ;
          end ;


      end ;

    end;

    if (Components [I] is TDBEdit) then
    begin
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := true    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TDBEdit).Font.Size   := 9 ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;
      {(Components [I] as TDBEdit).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;}

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ReadOnly := True ;
          (Components [I] as TDBEdit).Color      := $00E9E4E4 ;
          (Components [I] as TDBEdit).Font.Color := clBlack ;
          (Components [I] as TDBEdit).TabStop  := False ;
          Update;
      end ;

    end;

    if (Components [I] is TDBMemo) then
    begin
      (Components [I] as TDBMemo).Color       := clWhite ;
      (Components [I] as TDBMemo).BevelInner  := bvSpace ;
      (Components [I] as TDBMemo).BevelKind   := bkNone ;
      (Components [I] as TDBMemo).BevelOuter  := bvLowered ;
      (Components [I] as TDBMemo).Ctl3D       := True    ;
      (Components [I] as TDBMemo).BorderStyle := bsSingle ;
      (Components [I] as TDBMemo).Font.Name   := 'Calibri' ;
      (Components [I] as TDBMemo).Font.Size   := 9 ;
      (Components [I] as TDBMemo).Font.Style  := [] ;
      (Components [I] as TDBMemo).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if ((Components [I] as TDBMemo).Tag = 1) then
      begin
          (Components [I] as TDBMemo).ReadOnly   := True ;
          (Components [I] as TDBMemo).Color      := $00E9E4E4 ;
          (Components [I] as TDBMemo).Font.Color := clBlack ;
          (Components [I] as TDBMemo).TabStop    := False ;
          Update;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 11 ;
      (Components [I] as TEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 9 ;
      (Components [I] as TEdit).Font.Style  := [] ;
      {(Components [I] as TEdit).OnKeyDown   := frmCadastro_Padrao.OnKeyUp;}

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if ((Components [I] as TEdit).Tag = 1) then
      begin
          (Components [I] as TEdit).ReadOnly   := True ;
          (Components [I] as TEdit).Color      := $00E9E4E4 ;
          (Components [I] as TEdit).Font.Color := clBlack ;
          (Components [I] as TEdit).TabStop    := False ;
          Update;
      end

    end;

    if (Components [I] is TMaskEdit) then
    begin
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Calibri' ; //'Tahoma' ;
      (Components [I] as TMaskEdit).Font.Size   := 9 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;
      (Components [I] as TMaskEdit).OnKeyUP     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TMaskEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if Trim((Components[I] as TMaskEdit).EditMask) = '######;1;' then
          (Components[I] as TMaskEdit).EditMask := '##########;1; ' ;

      if ((Components [I] as TMaskEdit).Tag = 1) then
      begin
          (Components [I] as TMaskEdit).ReadOnly := True ;
          (Components [I] as TMaskEdit).Color      := $00E9E4E4 ;
          (Components [I] as TMaskEdit).Font.Color := clBlack ;
          (Components [I] as TMaskEdit).TabStop  := False ;
          Update;
      end
      else
      begin
          if (Components[I] as TMaskEdit).EditMask = '!99/99/9999;1;_' then
              (Components[I] as TMaskEdit).Width := 76 ;
      end ;

    end;

    if (Components [I] is TLabel) then
    begin
       if ((Components [I] as TLabel).Tag < 2) then
       begin
           (Components [I] as TLabel).Font.Name    := 'Calibri' ;
           (Components [I] as TLabel).Font.Size    := 8 ;
           (Components [I] as TLabel).Font.Style   := [] ;

           if ((Components [I] as TLabel).Tag = 0) then
           begin
               (Components [I] as TLabel).Font.Color   := clWhite ;
           end ;
           (Components [I] as TLabel).Transparent  := True ;
           (Components [I] as TLabel).BringToFront;
       end ;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Calibri' ;
       (Components [I] as TButton).Font.Style := [fsBold] ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Brush.Color:= clYellow ;

       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    If (Components [I] is TDBCheckBox) then
    begin
        (Components [I] as TDBCheckBox).OnKeyPress  := frmMenu.NavegaEnter ;
    end ;

    If (Components [I] is TcxGridDBTableView) then
    begin
        (Components [I] as TcxGridDBTableView).OptionsView.GridLineColor := clAqua ;
        (Components [I] as TcxGridDBTableView).OptionsView.GridLines     := glVertical ;
        (Components [I] as TcxGridDBTableView).Styles.Header             := frmMenu.Header ;
    end ;

    If (Components [I] is TcxGrid) then
    begin
        (Components [I] as TcxGrid).LookAndFeel.Kind        := lfFlat ;
        (Components [I] as TcxGrid).LookAndFeel.NativeStyle := True ;
        (Components [I] as TcxGrid).Font.Name               := 'Calibri' ;
    end ;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;

      (Components [I] as TDBLookupComboBox).Height      := 12 ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;
    end;

    if (Components [I] is TcxCurrencyEdit) then
    begin
        (Components [I] as TcxCurrencyEdit).Style.Font.Name := 'Calibri' ;
        (Components [I] as TcxCurrencyEdit).Style.Font.Size := 9 ;
    end ;

  end ;

end;

procedure TfrmProcesso_Padrao.ToolButton2Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmProcesso_Padrao.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  {if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;}

end;

procedure TfrmProcesso_Padrao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmProcesso_Padrao.FormActivate(Sender: TObject);
begin
    ToolBar1.Enabled := True ;
    frmMenu.StatusBar1.Panels[5].Text := Self.ClassName ;
end;

procedure TfrmProcesso_Padrao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    frmMenu.StatusBar1.Panels[5].Text := '' ;    
end;

procedure TfrmProcesso_Padrao.NavegaEdit(Sender: TObject; var Key: Char);
begin

  If Key = #13 then
  Begin
       Key:=#0;
         Perform(Wm_NextDlgCtl,0,0);

  end ;


end;

procedure TfrmProcesso_Padrao.FormDeactivate(Sender: TObject);
begin
    ToolBar1.Enabled := False ;
end;

procedure TfrmProcesso_Padrao.TrataData(Sender: TField;
  const Text: string);
begin

  if (Sender.DataType = ftInteger) then
  begin
      if (Trim(Text) = '') then
          Sender.AsString := ''
      else Sender.Value := Text ;
      exit ;
  end ;

  if (Sender.DataType = ftDate) or (Sender.DataType = ftDateTime) then
  begin
    if (Text = '  /  /    ') then
      Sender.asString := ''
    else
    begin
      try
          StrToDate(Text) ;
          Sender.Value := Text
      except
          MensagemErro('Data inv�lida. Utilize o formado dia/mes/ano.' +#13#13 + 'Exemplo: ' + DateToStr(Date)) ;
          abort ;
      end ;
    end;
  end
  else if (Sender.DataType = ftFloat) then
  begin
    if (Trim(Text) = '') then
      Sender.AsFloat := 0
    else
    begin
      try
          StrToFloat(Text) ;
          Sender.Value := Text
      except
          MensagemErro('Valor Inv�lido.' +#13#13+'N�o utilize pontos na digita��o de valores, apenas virgula para separar os centavos.') ;
          abort ;
      end ;
    end;
  end 
  else
  begin
    Sender.Value := Text;
  end;

end;

function TfrmProcesso_Padrao.InputQuery(const ACaption, APrompt: string;
  var Value: string): Boolean;
begin
    Result := frmMenu.InputQuery(ACaption, APrompt, Value) ;
end;

procedure TfrmProcesso_Padrao.showForm(objForm: TForm; bDestroirObjeto : boolean);
begin

    frmMenu.showForm(objForm , bDestroirObjeto);

end;

procedure TfrmProcesso_Padrao.ativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmProcesso_Padrao.desativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TfrmProcesso_Padrao.ativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmProcesso_Padrao.desativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TfrmProcesso_Padrao.ativaER2LkpMaskEdit(obj : TER2LookupMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TfrmProcesso_Padrao.desativaER2LkpMaskEdit(obj : TER2LookupMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

end.
