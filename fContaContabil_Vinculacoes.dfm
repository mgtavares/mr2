inherited frmContaContabil_Vinculacoes: TfrmContaContabil_Vinculacoes
  Left = 260
  Top = 267
  Width = 665
  Height = 364
  Caption = 'Vincula'#231#245'es'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 649
    Height = 299
  end
  inherited ToolBar1: TToolBar
    Width = 649
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 649
    Height = 299
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsVinculacoes
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdAgrupamentoContabil'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmAgrupamentoContabil'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'cMascara'
        Footers = <>
        Width = 134
      end
      item
        EditButtons = <>
        FieldName = 'iNivel'
        Footers = <>
        Width = 38
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryVinculacoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT AgrupamentoContabil.nCdAgrupamentoContabil'
      '      ,AgrupamentoContabil.cNmAgrupamentoContabil'
      '      ,AgrupamentoContabil.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,ContaAgrupamentoContabil.cMascara'
      '      ,ContaAgrupamentoContabil.iNivel'
      '  FROM ContaAgrupamentoContabil'
      
        '       INNER JOIN AgrupamentoContabil ON AgrupamentoContabil.nCd' +
        'AgrupamentoContabil = ContaAgrupamentoContabil.nCdAgrupamentoCon' +
        'tabil'
      
        '       INNER JOIN Empresa             ON Empresa.nCdEmpresa     ' +
        '                    = AgrupamentoContabil.nCdEmpresa'
      ' WHERE ContaAgrupamentoContabil.nCdPlanoConta = :nPK'
      ' ORDER BY 2')
    Left = 264
    Top = 112
    object qryVinculacoesnCdAgrupamentoContabil: TIntegerField
      DisplayLabel = 'Agrupamento Cont'#225'bil|C'#243'd.'
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryVinculacoescNmAgrupamentoContabil: TStringField
      DisplayLabel = 'Agrupamento Cont'#225'bil|Descri'#231#227'o'
      FieldName = 'cNmAgrupamentoContabil'
      Size = 50
    end
    object qryVinculacoesnCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa Cont'#225'bil|C'#243'd.'
      FieldName = 'nCdEmpresa'
    end
    object qryVinculacoescNmEmpresa: TStringField
      DisplayLabel = 'Empresa Cont'#225'bil|Nome'
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryVinculacoescMascara: TStringField
      DisplayLabel = 'Estrutura|M'#225'scara'
      FieldName = 'cMascara'
      Size = 30
    end
    object qryVinculacoesiNivel: TIntegerField
      DisplayLabel = 'Estrutura|N'#237'vel'
      FieldName = 'iNivel'
    end
  end
  object dsVinculacoes: TDataSource
    DataSet = qryVinculacoes
    Left = 296
    Top = 112
  end
end
