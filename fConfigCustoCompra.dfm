inherited frmConfigCustoCompra: TfrmConfigCustoCompra
  Left = 205
  Top = 169
  Width = 1096
  Caption = 'Configura'#231#227'o de Centro de Custo para Compra'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1080
  end
  inherited ToolBar1: TToolBar
    Width = 1080
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1080
    Height = 435
    Align = alClient
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyDown = DBGridEh1KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdClasseCustoCompra'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrupoProduto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmGrupoProduto'
        Footers = <>
        ReadOnly = True
        Width = 252
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nCdCC'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmCC'
        Footers = <>
        ReadOnly = True
      end>
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMasterBeforePost
    OnCalcFields = qryMasterCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM ClasseCustoCompra WHERE nCdEmpresa = :nPK')
    Left = 208
    Top = 168
    object qryMasternCdClasseCustoCompra: TIntegerField
      FieldName = 'nCdClasseCustoCompra'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdGrupoProduto: TIntegerField
      DisplayLabel = 'Grupo de Produto|C'#243'd'
      FieldName = 'nCdGrupoProduto'
    end
    object qryMastercNmGrupoProduto: TStringField
      DisplayLabel = 'Grupo de Produto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmGrupoProduto'
      Size = 50
      Calculated = True
    end
    object qryMasternCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryMastercNmLoja: TStringField
      DisplayLabel = 'Loja|Nome Loja'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
    object qryMasternCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryMastercNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 248
    Top = 168
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK'
      'AND cFlgLanc = 1')
    Left = 320
    Top = 224
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 384
    Top = 224
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 432
    Top = 216
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
end
