inherited frmManutencaoCrediario: TfrmManutencaoCrediario
  Left = -8
  Top = -8
  Height = 788
  Caption = 'Manuten'#231#227'o de Credi'#225'rio'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1066
    Height = 708
  end
  object Label1: TLabel [1]
    Left = 23
    Top = 38
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Carnet'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 180
    Top = 62
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Venda'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 62
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lancto Venda'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 398
    Top = 62
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caixa Venda'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 620
    Top = 62
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Venda'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 49
    Top = 86
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 496
    Top = 86
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor'
    FocusControl = DBEdit7
  end
  object Label9: TLabel [8]
    Left = 397
    Top = 86
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtd Parcelas'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [9]
    Left = 816
    Top = 86
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Entrada'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [10]
    Left = 608
    Top = 86
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ultimo Vencto'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [11]
    Left = 614
    Top = 110
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Estorno'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [12]
    Left = 10
    Top = 110
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lancto Estorno'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [13]
    Left = 774
    Top = 110
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario Estorno'
    FocusControl = DBEdit14
  end
  object Label15: TLabel [14]
    Left = 158
    Top = 110
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo Estorno'
    FocusControl = DBEdit15
  end
  object Label16: TLabel [15]
    Left = 283
    Top = 38
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit16
  end
  object Label18: TLabel [16]
    Left = 165
    Top = 38
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Renegocia'#231#227'o'
    FocusControl = DBEdit1
  end
  object Label8: TLabel [17]
    Left = 399
    Top = 38
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'd. Antigo'
    FocusControl = DBEdit17
  end
  inherited ToolBar2: TToolBar
    Width = 1066
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 89
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCrediario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [20]
    Tag = 1
    Left = 239
    Top = 56
    Width = 154
    Height = 19
    DataField = 'cNmLojaVenda'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [21]
    Tag = 1
    Left = 89
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdLanctoFin'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [22]
    Tag = 1
    Left = 462
    Top = 56
    Width = 142
    Height = 19
    DataField = 'nCdConta'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [23]
    Tag = 1
    Left = 683
    Top = 56
    Width = 87
    Height = 19
    DataField = 'dDtVenda'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [24]
    Tag = 1
    Left = 159
    Top = 80
    Width = 234
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [25]
    Tag = 1
    Left = 526
    Top = 80
    Width = 78
    Height = 19
    DataField = 'nValCrediario'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [26]
    Tag = 1
    Left = 463
    Top = 80
    Width = 30
    Height = 19
    DataField = 'iParcelas'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit10: TDBEdit [27]
    Tag = 1
    Left = 860
    Top = 80
    Width = 84
    Height = 19
    DataField = 'nValEntrada'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit11: TDBEdit [28]
    Tag = 1
    Left = 684
    Top = 80
    Width = 87
    Height = 19
    DataField = 'dDtUltVencto'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit12: TDBEdit [29]
    Tag = 1
    Left = 684
    Top = 104
    Width = 87
    Height = 19
    DataField = 'dDtEstorno'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit13: TDBEdit [30]
    Tag = 1
    Left = 89
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdLanctoFinEstorno'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit14: TDBEdit [31]
    Tag = 1
    Left = 860
    Top = 104
    Width = 85
    Height = 19
    DataField = 'cNmUsuarioEstorno'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit15: TDBEdit [32]
    Tag = 1
    Left = 239
    Top = 104
    Width = 365
    Height = 19
    DataField = 'cMotivoEstorno'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit16: TDBEdit [33]
    Tag = 1
    Left = 320
    Top = 32
    Width = 73
    Height = 19
    DataField = 'cNmStatusDocto'
    DataSource = dsMaster
    TabOrder = 15
  end
  object cxPageControl1: TcxPageControl [34]
    Left = 8
    Top = 144
    Width = 1058
    Height = 409
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 16
    ClientRectBottom = 405
    ClientRectLeft = 4
    ClientRectRight = 1054
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Informa'#231#245'es Parcelas'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1050
        Height = 381
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsInfParcelas
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footers = <>
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nValLiq'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'dDtLiq'
            Footers = <>
            Width = 92
          end
          item
            EditButtons = <>
            FieldName = 'nValJuro'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nValAbatimento'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nCdConta'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Width = 54
          end
          item
            EditButtons = <>
            FieldName = 'nCdLanctoFin'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Restri'#231#245'es Liberadas'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1050
        Height = 292
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRestricoesLiberadas
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoRestricaoVenda'
            Footers = <>
            Title.Caption = 'Tipo de Restri'#231#227'o'
            Width = 201
          end
          item
            EditButtons = <>
            FieldName = 'cJustificativa'
            Footers = <>
            Title.Caption = 'Justificativa'
            Width = 613
          end
          item
            EditButtons = <>
            FieldName = 'cNmLogin'
            Footers = <>
            Title.Caption = 'Usuario Autoriza'#231#227'o'
            Width = 163
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 292
        Width = 1050
        Height = 89
        Align = alBottom
        Caption = 'Justificativa'
        TabOrder = 1
        object Memo1: TMemo
          Left = 1
          Top = 14
          Width = 1048
          Height = 74
          Align = alClient
          Enabled = False
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
  end
  object DBEdit18: TDBEdit [35]
    Tag = 1
    Left = 239
    Top = 32
    Width = 37
    Height = 19
    DataField = 'Renegociacao'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit8: TDBEdit [36]
    Tag = 1
    Left = 89
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 18
  end
  object DBEdit17: TDBEdit [37]
    Tag = 1
    Left = 462
    Top = 32
    Width = 142
    Height = 19
    DataField = 'cIdExterno'
    DataSource = dsMaster
    TabOrder = 19
  end
  object cxButton1: TcxButton [38]
    Left = 8
    Top = 560
    Width = 177
    Height = 41
    Caption = 'Detalhes do Movimento'
    TabOrder = 20
    OnClick = cxButton1Click
    Glyph.Data = {
      06030000424D060300000000000036000000280000000F0000000F0000000100
      180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
      00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
      FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
      80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
      0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
      000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
      00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
      E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
      E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
      99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
      E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
      E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
      99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
      E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
      999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
      99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
      E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
      999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
      9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Crediario.nCdCrediario'
      
        '      ,dbo.fn_ZeroEsquerda(Loja.nCdLoja,3) + '#39' - '#39' + Loja.cNmLoj' +
        'a cNmLojaVenda'
      '      ,Crediario.dDtVenda'
      '      ,Crediario.nCdLanctoFin'
      '      ,ContaBancaria.nCdConta'
      '      ,Crediario.dDtUltVencto'
      '      ,StatusDocto.cNmStatusDocto'
      '      ,dbo.fn_OnlyDate(Crediario.dDtEstorno) dDtEstorno'
      '      ,Usuario.cNmLogin cNmUsuarioEstorno'
      '      ,LanctoFin.cMotivoEstorno'
      '      ,Crediario.nCdLanctoFinEstorno'
      '      ,Crediario.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Crediario.nValCrediario'
      '      ,Crediario.nValEntrada'
      '      ,Crediario.iParcelas'
      '      ,Crediario.cFlgRenegociacao'
      '      ,Crediario.cIdExterno'
      ' FROM Crediario'
      
        '      INNER JOIN Terceiro      ON Terceiro.nCdTerceiro          ' +
        ' = Crediario.nCdTerceiro'
      
        '      INNER JOIN Loja          ON Loja.nCdLoja                  ' +
        ' = Crediario.nCdLoja'
      
        '      LEFT  JOIN LanctoFin     ON LanctoFin.nCdLanctoFin        ' +
        ' = Crediario.nCdLanctoFin'
      
        '      LEFT  JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria' +
        ' = LanctoFin.nCdContaBancaria'
      
        '      LEFT  JOIN Usuario       ON Usuario.nCdUsuario            ' +
        ' = Crediario.nCdUsuarioEstorno'
      
        '      INNER JOIN StatusDocto   ON StatusDocto.nCdStatusDocto    ' +
        ' = Crediario.nCdStatusDocto'
      'WHERE nCdCrediario =:nPK')
    Left = 520
    Top = 128
    object qryMasternCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryMastercNmLojaVenda: TStringField
      FieldName = 'cNmLojaVenda'
      ReadOnly = True
      Size = 68
    end
    object qryMasterdDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMasterdDtUltVencto: TDateTimeField
      FieldName = 'dDtUltVencto'
    end
    object qryMastercNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
    object qryMasterdDtEstorno: TDateTimeField
      FieldName = 'dDtEstorno'
      ReadOnly = True
    end
    object qryMastercNmUsuarioEstorno: TStringField
      FieldName = 'cNmUsuarioEstorno'
    end
    object qryMastercMotivoEstorno: TStringField
      FieldName = 'cMotivoEstorno'
      Size = 30
    end
    object qryMasternCdLanctoFinEstorno: TIntegerField
      FieldName = 'nCdLanctoFinEstorno'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternValCrediario: TBCDField
      FieldName = 'nValCrediario'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValEntrada: TBCDField
      FieldName = 'nValEntrada'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryMastercFlgRenegociacao: TIntegerField
      FieldName = 'cFlgRenegociacao'
    end
    object qryMasterRenegociacao: TStringField
      FieldKind = fkCalculated
      FieldName = 'Renegociacao'
      Size = 3
      Calculated = True
    end
    object qryMastercIdExterno: TStringField
      FieldName = 'cIdExterno'
    end
  end
  inherited dsMaster: TDataSource
    Left = 536
    Top = 160
  end
  inherited qryID: TADOQuery
    Left = 560
    Top = 128
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 624
    Top = 128
  end
  inherited qryStat: TADOQuery
    Left = 592
    Top = 128
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 656
    Top = 128
  end
  inherited ImageList1: TImageList
    Left = 752
    Top = 280
  end
  object qryInfParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCrediario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT Titulo.nCdTitulo'
      '               ,cNrTit'
      '               ,MTitulo.nCdLanctoFin'
      '               ,Titulo.iParcela'
      '               ,Titulo.dDtVenc'
      '               ,dbo.fn_OnlyDate(Titulo.dDtLiq) dDtLiq'
      '               ,Titulo.nValTit'
      '               ,Titulo.nValLiq'
      '               ,Titulo.nValJuro'
      '               ,Titulo.nValDesconto'
      '               ,Titulo.nValAbatimento'
      '               ,Titulo.nSaldoTit'
      '               ,CB.nCdConta'
      '               ,dbo.fn_ZeroEsquerda(CB.nCdLoja,3) nCdLoja'
      '          FROM Titulo'
      
        '               LEFT JOIN MTitulo          ON MTitulo.nCdTitulo  ' +
        '    = Titulo.nCdTitulo'
      
        '               LEFT JOIN LanctoFin        ON LanctoFin.nCdLancto' +
        'Fin = MTitulo.nCdLanctoFin'
      
        '               LEFT JOIN ContaBancaria CB ON CB.nCdContaBancaria' +
        '    = LanctoFin.nCdContaBancaria'
      
        '               LEFT JOIN Operacao         ON Operacao.nCdOperaca' +
        'o   = MTitulo.nCdOperacao'
      '         WHERE ncdCrediario         =:nCdCrediario'
      '           AND (dDtEstorno is null OR Titulo.dDtCancel is null)'
      '           AND (cTipoOper  is null OR cTipoOper != '#39'E'#39')'
      '         ORDER BY iParcela ASC')
    Left = 488
    Top = 128
    object qryInfParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryInfParcelascNrTit: TStringField
      DisplayLabel = 'Parcelas do Credi'#225'rio|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryInfParcelasnCdLanctoFin: TIntegerField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Lancto Fin.'
      FieldName = 'nCdLanctoFin'
    end
    object qryInfParcelasiParcela: TIntegerField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Parc.'
      FieldName = 'iParcela'
    end
    object qryInfParcelasdDtVenc: TDateTimeField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryInfParcelasdDtLiq: TDateTimeField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Dt. Liquida'#231#227'o'
      FieldName = 'dDtLiq'
      ReadOnly = True
    end
    object qryInfParcelasnValTit: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Vl. Parcela'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnValLiq: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Vl. Liquidado'
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnValJuro: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Vl. Juros'
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnValDesconto: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Vl. Desconto'
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnValAbatimento: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Vl. Abatimento'
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnSaldoTit: TBCDField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Saldo em Aberto'
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryInfParcelasnCdConta: TStringField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Caixa Recebimento'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryInfParcelasnCdLoja: TStringField
      DisplayLabel = 'Parcelas do Credi'#225'rio|Loja Receb.'
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
  end
  object dsInfParcelas: TDataSource
    DataSet = qryInfParcelas
    Left = 504
    Top = 160
  end
  object qryRestricoesLiberadas: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryRestricoesLiberadasAfterScroll
    Parameters = <
      item
        Name = 'nCdCrediario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TRV.cNmTabTipoRestricaoVenda'
      
        '      ,CONVERT(Varchar(100),RestricaoVenda.cJustificativa) cJust' +
        'ificativa'
      '      ,Usuario.cNmLogin'
      '  FROM RestricaoVenda'
      
        '       INNER JOIN TabTipoRestricaoVenda TRV ON TRV.nCdTabTipoRes' +
        'tricaoVenda = RestricaoVenda.nCdTabTipoRestricaoVenda '
      
        '       INNER JOIN Usuario                   ON Usuario.nCdUsuari' +
        'o           = RestricaoVenda.nCdUsuarioAutor'
      ' WHERE ncdCrediario =:nCdCrediario')
    Left = 456
    Top = 128
    object qryRestricoesLiberadascNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
    object qryRestricoesLiberadascJustificativa: TStringField
      FieldName = 'cJustificativa'
      ReadOnly = True
      Size = 100
    end
    object qryRestricoesLiberadascNmLogin: TStringField
      FieldName = 'cNmLogin'
    end
  end
  object dsRestricoesLiberadas: TDataSource
    DataSet = qryRestricoesLiberadas
    Left = 472
    Top = 160
  end
end
