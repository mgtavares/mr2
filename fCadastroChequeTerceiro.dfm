inherited frmCadastroChequeTerceiro: TfrmCadastroChequeTerceiro
  Left = 72
  Top = 152
  Caption = 'frmCadastroChequeTerceiro'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 79
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 74
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 85
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 196
    Top = 86
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ag'#234'ncia'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 302
    Top = 86
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 478
    Top = 86
    Width = 15
    Height = 13
    Alignment = taRightJustify
    Caption = 'DV'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 599
    Top = 86
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Cheque'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 27
    Top = 110
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'CNPJ/CPF Emissor'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 47
    Top = 134
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Cheque'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 43
    Top = 158
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Dep'#243'sito'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 10
    Top = 182
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Respons'#225'vel'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 32
    Top = 206
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Portadora'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 57
    Top = 230
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBEdit13
  end
  object DBEdit1: TDBEdit [15]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCheque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [16]
    Tag = 1
    Left = 120
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [17]
    Left = 120
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [18]
    Left = 240
    Top = 80
    Width = 52
    Height = 19
    DataField = 'cAgencia'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [19]
    Left = 336
    Top = 80
    Width = 137
    Height = 19
    DataField = 'cConta'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [20]
    Left = 496
    Top = 80
    Width = 17
    Height = 19
    DataField = 'cDigito'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [21]
    Left = 672
    Top = 80
    Width = 65
    Height = 19
    DataField = 'iNrCheque'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [22]
    Left = 120
    Top = 104
    Width = 137
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 8
    OnExit = DBEdit8Exit
  end
  object DBEdit9: TDBEdit [23]
    Left = 120
    Top = 128
    Width = 89
    Height = 19
    DataField = 'nValCheque'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [24]
    Left = 120
    Top = 152
    Width = 89
    Height = 19
    DataField = 'dDtDeposito'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [25]
    Left = 120
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroResp'
    DataSource = dsMaster
    TabOrder = 11
    OnExit = DBEdit11Exit
    OnKeyDown = DBEdit11KeyDown
  end
  object DBEdit12: TDBEdit [26]
    Left = 120
    Top = 200
    Width = 65
    Height = 19
    DataField = 'nCdContaBancariaDep'
    DataSource = dsMaster
    TabOrder = 12
    OnExit = DBEdit12Exit
    OnKeyDown = DBEdit12KeyDown
  end
  object DBEdit13: TDBEdit [27]
    Left = 120
    Top = 224
    Width = 617
    Height = 19
    DataField = 'cOBSCheque'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit14: TDBEdit [28]
    Tag = 1
    Left = 192
    Top = 56
    Width = 545
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 14
  end
  object DBEdit15: TDBEdit [29]
    Tag = 1
    Left = 192
    Top = 176
    Width = 545
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 15
  end
  object DBEdit16: TDBEdit [30]
    Tag = 1
    Left = 192
    Top = 200
    Width = 195
    Height = 19
    DataField = 'nCdConta'
    DataSource = DataSource3
    TabOrder = 16
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Cheque'
      ' WHERE nCdEmpresa        = :nCdEmpresa'
      '   AND nCdTabTipoCheque  = 2'
      '   AND cFlgChequeManual  = 1'
      '   AND nCdCheque         = :nPK')
    object qryMasternCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryMastercConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryMastercDigito: TStringField
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMasteriNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMasternValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdTerceiroResp: TIntegerField
      FieldName = 'nCdTerceiroResp'
    end
    object qryMasternCdTerceiroPort: TIntegerField
      FieldName = 'nCdTerceiroPort'
    end
    object qryMasterdDtRemessaPort: TDateTimeField
      FieldName = 'dDtRemessaPort'
    end
    object qryMasternCdTabTipoCheque: TIntegerField
      FieldName = 'nCdTabTipoCheque'
    end
    object qryMastercChave: TStringField
      FieldName = 'cChave'
      FixedChar = True
      Size = 35
    end
    object qryMasternCdContaBancariaDep: TIntegerField
      FieldName = 'nCdContaBancariaDep'
    end
    object qryMasterdDtDevol: TDateTimeField
      FieldName = 'dDtDevol'
    end
    object qryMastercFlgCompensado: TIntegerField
      FieldName = 'cFlgCompensado'
    end
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMasterdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryMastercNmFavorecido: TStringField
      FieldName = 'cNmFavorecido'
      Size = 50
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasteriAutorizacao: TIntegerField
      FieldName = 'iAutorizacao'
    end
    object qryMasternCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryMasternCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryMasternCdTabStatusCheque: TIntegerField
      FieldName = 'nCdTabStatusCheque'
    end
    object qryMasterdDtSegDevol: TDateTimeField
      FieldName = 'dDtSegDevol'
    end
    object qryMasterdDtStatus: TDateTimeField
      FieldName = 'dDtStatus'
    end
    object qryMasternCdTituloCheque: TIntegerField
      FieldName = 'nCdTituloCheque'
    end
    object qryMastercOBSCheque: TStringField
      FieldName = 'cOBSCheque'
      Size = 100
    end
    object qryMastercFlgChequeManual: TIntegerField
      FieldName = 'cFlgChequeManual'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 320
    Top = 272
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 592
    Top = 272
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 392
    Top = 352
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 600
    Top = 280
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      ',nCdConta'
      'FROM ContaBancaria'
      'WHERE nCdContaBancaria = :nPK'
      'AND ((cFlgCaixa = 1) OR (cFlgCofre = 1))'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 456
    Top = 368
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource3: TDataSource
    DataSet = qryContaBancaria
    Left = 608
    Top = 288
  end
end
