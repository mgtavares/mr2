unit fConfigSubGrupoBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls, DB, ADODB, DBCtrls,
  DBGridEhGrouping;

type
  TfrmConfigSubGrupoBemPatrim = class(TfrmProcesso_Padrao)
    qryConfigSubGrupoBemPatrim: TADOQuery;
    dsConfigSubGrupoBemPatrim: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qrySubGrupoBemPatrimAux: TADOQuery;
    qrySubGrupoBemPatrimAuxnCdSubGrupoBemPatrim: TAutoIncField;
    qrySubGrupoBemPatrimAuxnCdGrupoBemPatrim: TIntegerField;
    qrySubGrupoBemPatrimAuxcNmSubGrupoBemPatrim: TStringField;
    dsSubGrupoBemPatrimAux: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    qryConfigSubGrupoBemPatrimnCdConfigSubGrupoBemPatrim: TAutoIncField;
    qryConfigSubGrupoBemPatrimnCdSubGrupoBemPatrim: TIntegerField;
    qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim: TStringField;
    procedure qryConfigSubGrupoBemPatrimBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfigSubGrupoBemPatrim: TfrmConfigSubGrupoBemPatrim;

implementation

uses fGrupoBemPatrim;

{$R *.dfm}

procedure TfrmConfigSubGrupoBemPatrim.qryConfigSubGrupoBemPatrimBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim.Value := UpperCase(qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim.Value);
  qryConfigSubGrupoBemPatrimnCdSubGrupoBemPatrim.Value       := qrySubGrupoBemPatrimAuxnCdSubGrupoBemPatrim.Value;
end;

procedure TfrmConfigSubGrupoBemPatrim.FormShow(Sender: TObject);
begin
  inherited;
  DbGridEh1.col := 2;
  DbGridEh1.SetFocus;
end;

end.
