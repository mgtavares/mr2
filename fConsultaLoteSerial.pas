unit fConsultaLoteSerial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, StdCtrls, GridsEh, DBGridEh, Mask, DBCtrls, DB, ADODB;

type
  TfrmConsultaLoteSerial = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdTabTipoModoGestaoProduto: TIntegerField;
    qryProdutocFlgControlaValidadeLote: TIntegerField;
    qryProdutoiDiasDisponibilidadeLote: TIntegerField;
    qryTabTipoOrigemMovLote: TADOQuery;
    qryTabTipoOrigemMovLotenCdTabTipoOrigemMovLote: TIntegerField;
    qryTabTipoOrigemMovLotecNmTabTipoOrigemMovLote: TStringField;
    qryTabTipoOrigemMovLotecFlgEntradaSaida: TStringField;
    DataSource2: TDataSource;
    DataSource1: TDataSource;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    DBGridEh1: TDBGridEh;
    Label4: TLabel;
    Edit1: TEdit;
    qryLocalEstoque: TADOQuery;
    DataSource3: TDataSource;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryLoteDisponivel: TADOQuery;
    qryLoteSelecionado: TADOQuery;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DBGridEh2: TDBGridEh;
    qryPopulaTemp: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryLoteDisponivelnCdLoteProduto: TAutoIncField;
    qryLoteDisponivelcNrLote: TStringField;
    qryLoteDisponiveldDtFabricacao: TDateTimeField;
    qryLoteDisponiveldDtDisponibilidade: TDateTimeField;
    qryLoteDisponiveldDtValidade: TDateTimeField;
    qryLoteDisponivelnSaldoLote: TBCDField;
    qryLoteDisponivelnQtdeSelecionado: TBCDField;
    qryLoteDisponivelcFlgNumSerie: TIntegerField;
    qryLoteDisponivelcFlgSelecionado: TIntegerField;
    qryLoteSelecionadonCdLoteProduto: TAutoIncField;
    qryLoteSelecionadocNrLote: TStringField;
    qryLoteSelecionadodDtFabricacao: TDateTimeField;
    qryLoteSelecionadodDtDisponibilidade: TDateTimeField;
    qryLoteSelecionadodDtValidade: TDateTimeField;
    qryLoteSelecionadonSaldoLote: TBCDField;
    qryLoteSelecionadonQtdeSelecionado: TBCDField;
    qryLoteSelecionadocFlgNumSerie: TIntegerField;
    qryLoteSelecionadocFlgSelecionado: TIntegerField;
    procedure Edit1Change(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryLoteDisponivelAfterScroll(DataSet: TDataSet);
    procedure SelecionaLote;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh2Columns5UpdateData(Sender: TObject;
      var Text: String; var Value: Variant; var UseText, Handled: Boolean);
    procedure qryLoteDisponivelAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    bQuantidadeDigitada : Boolean;
  public
    { Public declarations }
    bPermiteSelecionar, bGravarSelecao   : boolean;
    nQtdeMov : Double;
  end;

var
  frmConsultaLoteSerial: TfrmConsultaLoteSerial;

implementation

{$R *.dfm}

uses fMenu;

procedure TfrmConsultaLoteSerial.Edit1Change(Sender: TObject);
begin
  inherited;

  if (Length(Trim(Edit1.Text)) > 0 ) then
      qryLoteDisponivel.Filter := 'cNrLote LIKE ' + #39 + '%' + Edit1.Text + '%' + #39
  else qryLoteDisponivel.Filter := '';

end;

procedure TfrmConsultaLoteSerial.DBGridEh2DblClick(Sender: TObject);
begin
    SelecionaLote;
end;

procedure TfrmConsultaLoteSerial.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if (qryLoteSelecionadonCdLoteProduto.Value <= 0) then
      abort;

  qryLoteSelecionado.Edit;
  qryLoteSelecionadocFlgSelecionado.Value  := 0;
  qryLoteSelecionadonQtdeSelecionado.Value := 0;
  qryLoteSelecionado.Post;

  qryLoteDisponivel.Close;
  qryLoteDisponivel.Open;

  qryLoteSelecionado.Close;
  qryLoteSelecionado.Open;
end;

procedure TfrmConsultaLoteSerial.qryLoteDisponivelAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  if (qryLoteDisponivelcFlgNumSerie.Value = 1) then
  begin
      DBGridEh2.Columns[2].Visible          := False;
      DBGridEh2.Columns[3].Visible          := False;
      DBGridEh2.Columns[5].ReadOnly         := True;
      DBGridEh2.Columns[5].Color            := clSilver;
      DBGridEh2.Columns[5].Title.Font.Color := clRed;

      DBGridEh1.Columns[2].Visible  := False;
      DBGridEh1.Columns[3].Visible  := False;
      DBGridEh1.Columns[4].ReadOnly := True;
      DBGridEh1.Columns[4].Color    := clSilver;
  end;

end;

procedure TfrmConsultaLoteSerial.SelecionaLote;
var
  cNrLote,dDtFabricacao,dDtDisponibilidade,dDtValidade : String;
  nSaldoLote, nQtdeSelecionado, nTotalSelecionado : Double;
begin
  inherited;

  bQuantidadeDigitada := False;

    {-- armazena os dados nas variveis para manipula��o dos registros --}
  cNrLote            := qryLoteDisponivelcNrLote.Value;
  dDtFabricacao      := qryLoteDisponiveldDtFabricacao.AsString;
  dDtDisponibilidade := qryLoteDisponiveldDtDisponibilidade.AsString;
  dDtValidade        := qryLoteDisponiveldDtValidade.AsString;
  nSaldoLote         := qryLoteDisponivelnSaldoLote.Value;
  nQtdeSelecionado   := qryLoteDisponivelnQtdeSelecionado.Value;

  if not (bPermiteSelecionar) then
  begin
      MensagemAlerta('Esta transa��o permite apenas a consulta de lote/serial.');
      abort;
  end;

  nTotalSelecionado := DBGridEh1.SumList.SumCollection.Items[0].SumValue;
  {--verifica se algum lote/serial foi selecionado--}
  if (qryLoteDisponivelnCdLoteProduto.Value <= 0) then
      abort;

  {-- verifica se a soma dos lotes/seriais � igual a quantidade do movimento --}
  if (nTotalSelecionado = nQtdeMov) then
  begin
      MensagemAlerta('A Soma da quantidade Selecionada � Maior que o Total Movimentado.' + #13#13 + 'Total Movimentado: ' + FloatToStr(nQtdeMov));
      abort;
  end;

  if (nQtdeSelecionado < 0) then
  begin
      MensagemAlerta('A Quantidade Selecionada n�o pode ser inferior a zero.');
      abort;
  end;

  qryLoteDisponivel.Edit;

  if (nSaldoLote < nQtdeSelecionado) then
  begin
      qryLoteDisponivelnSaldoLote.Value := nSaldoLote;
      MensagemAlerta('Saldo do lote insuficiente para atender esta transa��o. Saldo do Lote: ' + FloatToStr(nSaldoLote));
      abort;
  end
  else if ((nTotalSelecionado + nQtdeSelecionado) > nQtdeMov) then
  begin
      qryLoteDisponivelnSaldoLote.Value := nSaldoLote;
      MensagemAlerta('A Soma da quantidade Selecionada � Maior que o Total Movimentado.' + #13#13 + 'Total Movimentado: ' + FloatToStr(nQtdeMov));
      abort;
  end;

  {-- se a quantidade selecionada for = 0 seleciona todo o lote se n�o usa a quantidade selecionada--}
  if (qryLoteDisponivelnQtdeSelecionado.Value = 0) then
  begin
      if (nSaldoLote > (nQtdeMov - nTotalSelecionado)) then
          qryLoteDisponivelnQtdeSelecionado.Value := (nQtdeMov - nTotalSelecionado)
      else qryLoteDisponivelnQtdeSelecionado.Value := nSaldoLote;
  end
  else qryLoteDisponivelnQtdeSelecionado.Value := nQtdeSelecionado;

  qryLoteDisponivelcFlgSelecionado.Value  := 1;

  qryLoteDisponivel.Post;

  qryLoteDisponivel.Close;
  qryLoteDisponivel.Open;

  qryLoteSelecionado.Close;
  qryLoteSelecionado.Open;

  Edit1.Text          := '';
end;

procedure TfrmConsultaLoteSerial.ToolButton1Click(Sender: TObject);
begin
  inherited;

  bGravarSelecao := True;
  Close;
end;

procedure TfrmConsultaLoteSerial.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[1].Width := 105;
  DBGridEh1.Columns[2].Width := 105;
  DBGridEh1.Columns[3].Width := 105;

  DBGridEh2.Columns[1].Width := 105;
  DBGridEh2.Columns[2].Width := 105;
  DBGridEh2.Columns[3].Width := 105;

  DBGridEh1.Columns[0].ReadOnly         := True;
  DBGridEh1.Columns[0].Color            := clSilver;
  DBGridEh1.Columns[0].Title.Font.Color := clRed;

  DBGridEh2.Columns[0].ReadOnly         := True;
  DBGridEh2.Columns[0].Color            := clSilver;
  DBGridEh2.Columns[0].Title.Font.Color := clRed;
end;

procedure TfrmConsultaLoteSerial.DBGridEh2Columns5UpdateData(
  Sender: TObject; var Text: String; var Value: Variant; var UseText,
  Handled: Boolean);
begin
  inherited;

  if (Value > 0) then
      bQuantidadeDigitada := True;

end;

procedure TfrmConsultaLoteSerial.qryLoteDisponivelAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  if (bQuantidadeDigitada) then
      SelecionaLote;
      
end;

procedure TfrmConsultaLoteSerial.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  if ((qryLoteSelecionado.RecordCount > 0) and not(bGravarSelecao)) then
      if(MessageDlg('Ao sair as quantidades selecionadas n�o ser�o gravadas. Confirma ? ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;
end;

procedure TfrmConsultaLoteSerial.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmConsultaLoteSerial.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

end.
