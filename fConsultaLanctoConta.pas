unit fConsultaLanctoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBCtrls, StdCtrls, Mask, DBGridEhGrouping;

type
  TfrmConsultaLanctoConta = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    MaskEdit4: TMaskEdit;
    Label4: TLabel;
    MaskEdit5: TMaskEdit;
    Label5: TLabel;
    Label6: TLabel;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryResultadonCdLanctoFin: TAutoIncField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadonCdTipoLancto: TIntegerField;
    qryResultadocNmTipoLancto: TStringField;
    qryResultadonValCredito: TBCDField;
    qryResultadonValDebito: TBCDField;
    qryResultadonCdUsuario: TIntegerField;
    qryResultadocNmUsuario: TStringField;
    qryResultadocFlgManual: TStringField;
    qryResultadodDtEstorno: TDateTimeField;
    qryResultadocMotivoEstorno: TStringField;
    qryResultadocHistorico: TStringField;
    qryResultadocDocumento: TStringField;
    qryResultadonCdProvisaoTit: TIntegerField;
    qryContaBancariaDebcFlgCaixa: TIntegerField;
    qryContaBancariaDebcFlgCofre: TIntegerField;
    DBEdit1: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryResultadoiNrCheque: TIntegerField;
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaLanctoConta: TfrmConsultaLanctoConta;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsultaLanctoConta.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, MaskEdit4.Text) ;
  
end;

procedure TfrmConsultaLanctoConta.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryContaBancariaDeb.Close ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;

end;

procedure TfrmConsultaLanctoConta.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione a conta.') ;
      MaskEdit3.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit1.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit2.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  if (qryContaBancariaDebcFlgCaixa.Value = 1) or (qryContaBancariaDebcFlgCofre.Value = 1) then
  begin
      RadioGroup1.ItemIndex := 1 ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdContaBancaria').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  qryResultado.Parameters.ParamByName('cDataInicial').Value     := frmMenu.ConvData(MaskEdit1.Text)    ;
  qryResultado.Parameters.ParamByName('cDataFinal').Value       := frmMenu.ConvData(MaskEdit2.Text)    ;
  qryResultado.Parameters.ParamByName('nCdTipoLancto').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  qryResultado.Parameters.ParamByName('iNrCheque').Value        := frmMenu.ConvInteiro(MaskEdit5.Text) ;

  if (RadioGroup1.ItemIndex = 1) then
      qryResultado.Parameters.ParamByName('cFlgConciliado').Value   := 'S'
  else qryResultado.Parameters.ParamByName('cFlgConciliado').Value   := 'N' ;

  if (RadioGroup2.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cFlgManual').Value       := 'S'
  else qryResultado.Parameters.ParamByName('cFlgManual').Value       := 'N' ;

  qryResultado.Open ;

  if (qryResultado.eof) then
  begin
      MensagemAlerta('Nenhum lançamento encontrado para o critério selecionado.') ;
  end ;

end;

procedure TfrmConsultaLanctoConta.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[4].Font.Color := clBlue ;
  DBGridEh1.Columns[5].Font.Color := clRed ;

end;

procedure TfrmConsultaLanctoConta.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            {nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((ContaBancaria.nCdLoja IS NULL) OR EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = @@Usuario AND UL.nCdLoja = ContaBancaria.nCdLoja))');}
            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'EXISTS(SELECT 1 FROM UsuarioContaBancaria UCB WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria AND UCB.nCdUsuario = '+ intToStr(frmMenu.nCdUsuarioLogado)+') AND ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND (cFlgCofre = 1 OR cFlgCaixa = 0)') ;

            If (nPK > 0) then
            begin
                MaskEdit3.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaLanctoConta.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(130);

            If (nPK > 0) then
            begin
                MaskEdit4.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaLanctoConta.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Consulta de Lançamentos');
end;

initialization
    RegisterClass(TfrmConsultaLanctoConta) ;

end.
