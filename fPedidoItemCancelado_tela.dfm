inherited frmPedidoItemCancelado_tela: TfrmPedidoItemCancelado_tela
  Left = 46
  Top = 51
  Width = 1064
  Height = 682
  Caption = 'Pedido/ Item Cancelado'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1048
    Height = 615
  end
  inherited ToolBar1: TToolBar
    Width = 1048
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1048
    Height = 615
    Align = alClient
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = data1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid1DBTableView1nValTotalCanc
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = cxGrid1DBTableView1nValTotalCanc
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'N'#250'm. Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Dt. Pedido'
        DataBinding.FieldName = 'dDtPedido'
      end
      object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
        Caption = 'C'#243'd. Terceiro'
        DataBinding.FieldName = 'nCdTerceiro'
        Visible = False
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 225
      end
      object cxGrid1DBTableView1nCdTipoPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTipoPedido'
        Visible = False
      end
      object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
        Caption = 'Tipo Pedido'
        DataBinding.FieldName = 'cNmTipoPedido'
        Width = 189
      end
      object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
        Caption = 'C'#243'd. Produto'
        DataBinding.FieldName = 'nCdProduto'
        Width = 92
      end
      object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
        Caption = 'Produto'
        DataBinding.FieldName = 'cNmProduto'
        Width = 332
      end
      object cxGrid1DBTableView1nQtdeCanc: TcxGridDBColumn
        Caption = 'Qtd. Cancelada'
        DataBinding.FieldName = 'nQtdeCanc'
        Width = 101
      end
      object cxGrid1DBTableView1nValTotalCanc: TcxGridDBColumn
        Caption = 'Valor Total Cancelado'
        DataBinding.FieldName = 'nValTotalCanc'
        Width = 137
      end
      object cxGrid1DBTableView1dDtCancelSaldoItemPed: TcxGridDBColumn
        Caption = 'Dt. Cancelamento'
        DataBinding.FieldName = 'dDtCancelSaldoItemPed'
        Visible = False
        GroupIndex = 0
        SortIndex = 0
        SortOrder = soAscending
      end
      object cxGrid1DBTableView1nCdUsuarioCancelSaldoItemPed: TcxGridDBColumn
        DataBinding.FieldName = 'nCdUsuarioCancelSaldoItemPed'
        Visible = False
      end
      object cxGrid1DBTableView1nCdMotivoCancSaldoPed: TcxGridDBColumn
        DataBinding.FieldName = 'nCdMotivoCancSaldoPed'
        Visible = False
      end
      object cxGrid1DBTableView1cNmMotivoCancSaldoPed: TcxGridDBColumn
        Caption = 'Motivo Cancelamento'
        DataBinding.FieldName = 'cNmMotivoCancSaldoPed'
        Width = 224
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio Cancelamento'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 181
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object data1: TDataSource
    DataSet = SPREL_PEDIDO_ITEM_CANCELADO
    Left = 672
    Top = 512
  end
  object SPREL_PEDIDO_ITEM_CANCELADO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_PEDIDO_ITEM_CANCELADO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdGrupoProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdDepartamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdSubCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTipoPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdMotivoCancSaldoPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtCancelSaldoItemPedInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtCancelSaldoItemPedFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@cFlgTipoPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 656
    Top = 480
    object SPREL_PEDIDO_ITEM_CANCELADOnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOcNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnQtdeCanc: TBCDField
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnValTotalCanc: TBCDField
      FieldName = 'nValTotalCanc'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed: TDateTimeField
      FieldName = 'dDtCancelSaldoItemPed'
      ReadOnly = True
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnCdUsuarioCancelSaldoItemPed: TIntegerField
      FieldName = 'nCdUsuarioCancelSaldoItemPed'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object SPREL_PEDIDO_ITEM_CANCELADOnCdMotivoCancSaldoPed: TIntegerField
      FieldName = 'nCdMotivoCancSaldoPed'
    end
    object SPREL_PEDIDO_ITEM_CANCELADOcNmMotivoCancSaldoPed: TStringField
      FieldName = 'cNmMotivoCancSaldoPed'
      Size = 50
    end
  end
end
