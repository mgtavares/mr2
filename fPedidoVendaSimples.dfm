inherited frmPedidoVendaSimples: TfrmPedidoVendaSimples
  Left = 116
  Top = 26
  Width = 1168
  Height = 666
  Caption = 'Pedido Venda - Simples'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 24
    Width = 1152
    Height = 604
  end
  object Label1: TLabel [1]
    Left = 55
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 50
    Top = 70
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 32
    Top = 118
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [4]
    Left = 53
    Top = 142
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [5]
    Left = 644
    Top = 46
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Pedido'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [6]
    Left = 675
    Top = 118
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label10: TLabel [7]
    Left = 611
    Top = 70
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedido do Terceiro'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [8]
    Left = 841
    Top = 70
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Contato'
    FocusControl = DBEdit11
  end
  object Label26: TLabel [9]
    Left = 892
    Top = 569
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total do Pedido'
    FocusControl = DBEdit37
  end
  object Label15: TLabel [10]
    Left = 798
    Top = 46
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Entrega'
    FocusControl = DBEdit21
  end
  object Label18: TLabel [11]
    Left = 25
    Top = 569
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor dos Produtos'
    FocusControl = DBEdit27
  end
  object Label19: TLabel [12]
    Left = 213
    Top = 569
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    FocusControl = DBEdit28
  end
  object Label20: TLabel [13]
    Left = 551
    Top = 569
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Acr'#233'scimo'
    FocusControl = DBEdit29
  end
  object Label12: TLabel [14]
    Left = 597
    Top = 94
    Width = 110
    Height = 13
    Alignment = taRightJustify
    Caption = 'Condi'#231#227'o Pagamento'
    FocusControl = DBEdit38
  end
  object Label21: TLabel [15]
    Left = 968
    Top = 46
    Width = 6
    Height = 13
    Caption = 'a'
    FocusControl = DBEdit21
  end
  object Label25: TLabel [16]
    Left = 722
    Top = 569
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Abat. Devolu'#231#227'o'
    FocusControl = DBEdit31
  end
  object Label27: TLabel [17]
    Left = 27
    Top = 166
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'End. Entrega'
    FocusControl = DBEdit5
  end
  object Label13: TLabel [18]
    Left = 7
    Top = 190
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Pagador'
    FocusControl = DBEdit41
  end
  object Label22: TLabel [19]
    Left = 72
    Top = 94
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label39: TLabel [20]
    Left = 407
    Top = 569
    Width = 56
    Height = 13
    Caption = 'Valor Frete'
    FocusControl = DBEdit44
  end
  inherited ToolBar2: TToolBar
    Width = 1152
    Height = 24
    TabOrder = 15
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      ImageIndex = 2
    end
    object ToolButton3: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton12: TToolButton
      Left = 864
      Top = 0
      Caption = 'Imprimir'
      ImageIndex = 9
      OnClick = ToolButton12Click
    end
    object ToolButton14: TToolButton
      Left = 952
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 13
      Style = tbsSeparator
    end
    object bFinalizar: TToolButton
      Left = 960
      Top = 0
      Caption = 'Finalizar'
      ImageIndex = 7
      OnClick = bFinalizarClick
    end
    object ToolButton10: TToolButton
      Left = 1048
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 13
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 1056
      Top = 0
      Caption = 'Op'#231#245'es'
      DropdownMenu = PopupMenu2
      ImageIndex = 12
    end
  end
  object DBEdit1: TDBEdit [22]
    Tag = 1
    Left = 96
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdPedido'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [23]
    Tag = 1
    Left = 96
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 1
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit4: TDBEdit [24]
    Left = 96
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTipoPedido'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [25]
    Left = 96
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [26]
    Tag = 1
    Left = 710
    Top = 40
    Width = 83
    Height = 19
    DataField = 'dDtPedido'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit10: TDBEdit [27]
    Left = 710
    Top = 64
    Width = 83
    Height = 19
    DataField = 'cNrPedTerceiro'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [28]
    Left = 886
    Top = 64
    Width = 171
    Height = 19
    DataField = 'cNmContato'
    DataSource = dsMaster
    TabOrder = 11
    OnEnter = DBEdit11Enter
  end
  object DBEdit12: TDBEdit [29]
    Tag = 1
    Left = 164
    Top = 64
    Width = 353
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 16
  end
  object DBEdit14: TDBEdit [30]
    Tag = 1
    Left = 164
    Top = 112
    Width = 353
    Height = 19
    DataField = 'cNmTipoPedido'
    DataSource = DataSource3
    TabOrder = 17
  end
  object DBEdit15: TDBEdit [31]
    Tag = 1
    Left = 264
    Top = 136
    Width = 321
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 18
  end
  object DBEdit16: TDBEdit [32]
    Tag = 1
    Left = 164
    Top = 136
    Width = 97
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 19
  end
  object DBEdit17: TDBEdit [33]
    Tag = 1
    Left = 710
    Top = 112
    Width = 137
    Height = 19
    DataField = 'cNmTabStatusPed'
    DataSource = DataSource5
    TabOrder = 13
  end
  object cxPageControl1: TcxPageControl [34]
    Left = 8
    Top = 212
    Width = 1049
    Height = 345
    ActivePage = TabItemEstoque
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = True
    TabOrder = 14
    ClientRectBottom = 341
    ClientRectLeft = 4
    ClientRectRight = 1045
    ClientRectTop = 24
    object TabItemEstoque: TcxTabSheet
      Caption = 'Itens de Estoque - Grade'
      Enabled = False
      ImageIndex = 0
      object Label23: TLabel
        Tag = 2
        Left = 672
        Top = 302
        Width = 68
        Height = 13
        Caption = 'Entrega Total'
      end
      object Label24: TLabel
        Tag = 2
        Left = 808
        Top = 302
        Width = 76
        Height = 13
        Caption = 'Entrega Parcial'
      end
      object Label29: TLabel
        Tag = 2
        Left = 952
        Top = 302
        Width = 86
        Height = 13
        Caption = 'Saldo Cancelado'
      end
      object lblHelp: TLabel
        Tag = 2
        Left = 0
        Top = 8
        Width = 252
        Height = 13
        Caption = 'F3 - Inclus'#227'o M'#250'ltipla  /  F4 - Consultar Produtos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 24
        Width = 1038
        Height = 257
        Ctl3D = False
        DataGrouping.GroupLevels = <>
        DataSource = dsItemEstoque
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorTitle.TitleButton = True
        IndicatorOptions = [gioShowRowIndicatorEh]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentCtl3D = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColEnter = DBGridEh1ColEnter
        OnColExit = DBGridEh1ColExit
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            Title.Caption = 'Produto|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 340
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 73
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
            ReadOnly = True
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
            Width = 55
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            Tag = 1
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValAcrescimo'
            Footers = <>
            Tag = 1
            Visible = False
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nValFrete'
            Footers = <>
            Title.Caption = 'Valores|Frete Unit.'
          end
          item
            EditButtons = <>
            FieldName = 'nValDescAbatUnit'
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 76
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footers = <>
            ReadOnly = True
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'nPercIPI'
            Footers = <>
            Visible = False
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'nValIPI'
            Footers = <>
            Visible = False
            Width = 79
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxButton1: TcxButton
        Left = 162
        Top = 284
        Width = 161
        Height = 33
        Caption = 'Visualizar / Alterar Grade'
        TabOrder = 1
        Visible = False
        OnClick = cxButton1Click
        Glyph.Data = {
          06030000424D060300000000000036000000280000000F0000000F0000000100
          180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
          FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
          80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
          0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
          000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
          00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
          E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
          E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
          99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
          E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
          E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
          99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
          E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
          999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
          99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
          E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
          999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
          9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
          FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000}
        LookAndFeel.Kind = lfOffice11
      end
      object cxTextEdit1: TcxTextEdit
        Left = 640
        Top = 296
        Width = 25
        Height = 21
        Style.Color = clBlue
        StyleDisabled.Color = clBlue
        TabOrder = 2
      end
      object cxTextEdit2: TcxTextEdit
        Left = 776
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clYellow
        TabOrder = 3
      end
      object cxTextEdit3: TcxTextEdit
        Left = 920
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clRed
        TabOrder = 4
      end
      object btSituacaoEstoque: TcxButton
        Left = -1
        Top = 284
        Width = 161
        Height = 33
        Caption = 'F5 - Situa'#231#227'o Estoque'
        TabOrder = 5
        OnClick = btSituacaoEstoqueClick
        Glyph.Data = {
          06030000424D060300000000000036000000280000000F0000000F0000000100
          180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
          FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
          80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
          0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
          000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
          00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
          E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
          E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
          99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
          E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
          E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
          99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
          E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
          999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
          99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
          E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
          999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
          9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
          FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000}
        LookAndFeel.Kind = lfOffice11
      end
    end
    object TabAD: TcxTabSheet
      Caption = 'Itens AD / Servi'#231'os'
      Enabled = False
      ImageIndex = 1
      object Label30: TLabel
        Tag = 2
        Left = 40
        Top = 302
        Width = 68
        Height = 13
        Caption = 'Entrega Total'
      end
      object Label31: TLabel
        Tag = 2
        Left = 184
        Top = 302
        Width = 76
        Height = 13
        Caption = 'Entrega Parcial'
      end
      object Label32: TLabel
        Tag = 2
        Left = 344
        Top = 302
        Width = 86
        Height = 13
        Caption = 'Saldo Cancelado'
      end
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 1038
        Height = 297
        DataGrouping.GroupLevels = <>
        DataSource = dsItemAD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh4DrawColumnCell
        OnEnter = DBGridEh4Enter
        OnKeyUp = DBGridEh4KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 376
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footers = <>
            Width = 37
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
            ReadOnly = True
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footers = <>
            Width = 71
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoItemPed'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoImposto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmGrupoImposto'
            Footers = <>
            ReadOnly = True
            Width = 168
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxTextEdit4: TcxTextEdit
        Left = 8
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clBlue
        TabOrder = 1
      end
      object cxTextEdit5: TcxTextEdit
        Left = 152
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clYellow
        TabOrder = 2
      end
      object cxTextEdit6: TcxTextEdit
        Left = 312
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clRed
        TabOrder = 3
      end
    end
    object TabFormula: TcxTabSheet
      Caption = 'Item Formulado'
      Enabled = False
      ImageIndex = 4
      object Label33: TLabel
        Tag = 2
        Left = 608
        Top = 302
        Width = 86
        Height = 13
        Caption = 'Saldo Cancelado'
      end
      object Label34: TLabel
        Tag = 2
        Left = 448
        Top = 302
        Width = 76
        Height = 13
        Caption = 'Entrega Parcial'
      end
      object Label35: TLabel
        Tag = 2
        Left = 304
        Top = 302
        Width = 68
        Height = 13
        Caption = 'Entrega Total'
      end
      object DBGridEh6: TDBGridEh
        Left = 0
        Top = 0
        Width = 1038
        Height = 281
        DataGrouping.GroupLevels = <>
        DataSource = dsItemFormula
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh6ColExit
        OnDrawColumnCell = DBGridEh6DrawColumnCell
        OnEnter = DBGridEh6Enter
        OnKeyUp = DBGridEh6KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 312
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
            ReadOnly = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nValAcrescimo'
            Footers = <>
            Width = 71
          end
          item
            EditButtons = <>
            FieldName = 'nValDescAbatUnit'
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footers = <>
            ReadOnly = True
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoItemPed'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footers = <>
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxTextEdit7: TcxTextEdit
        Left = 576
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clRed
        TabOrder = 1
      end
      object cxTextEdit8: TcxTextEdit
        Left = 416
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clYellow
        TabOrder = 2
      end
      object cxTextEdit9: TcxTextEdit
        Left = 272
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clBlue
        TabOrder = 3
      end
      object cxButton2: TcxButton
        Left = 0
        Top = 288
        Width = 225
        Height = 25
        Caption = 'Visualizar/Alterar Composi'#231#227'o da Caixa'
        TabOrder = 4
        OnClick = cxButton2Click
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = True
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Faturamento'
      ImageIndex = 6
      object Image4: TImage
        Left = 0
        Top = 0
        Width = 488
        Height = 317
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label8: TLabel
        Left = 39
        Top = 158
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transportador'
        FocusControl = DBEdit8
      end
      object Label9: TLabel
        Left = 62
        Top = 110
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incoterms'
        FocusControl = DBEdit19
      end
      object Label3: TLabel
        Left = 21
        Top = 176
        Width = 91
        Height = 26
        Alignment = taRightJustify
        Caption = 'Mensagem Documento Fiscal'
        FocusControl = DBEdit3
        WordWrap = True
      end
      object Label17: TLabel
        Left = 37
        Top = 14
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Representante'
        FocusControl = DBEdit25
      end
      object Label51: TLabel
        Left = 38
        Top = 38
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = #193'rea de Venda'
        FocusControl = DBEdit63
      end
      object Label52: TLabel
        Left = 24
        Top = 62
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Divis'#227'o de Venda'
        FocusControl = DBEdit64
      end
      object Label53: TLabel
        Left = 3
        Top = 86
        Width = 110
        Height = 13
        Alignment = taRightJustify
        Caption = 'Canal de Distribui'#231#227'o'
        FocusControl = DBEdit65
      end
      object Label28: TLabel
        Left = 51
        Top = 133
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modo Frete'
        FocusControl = DBEdit43
      end
      object DBEdit7: TDBEdit
        Tag = 1
        Left = 185
        Top = 152
        Width = 284
        Height = 19
        DataField = 'cNmTerceiro'
        DataSource = DataSource6
        TabOrder = 14
      end
      object DBEdit8: TDBEdit
        Left = 117
        Top = 152
        Width = 65
        Height = 19
        DataField = 'nCdTerceiroTransp'
        DataSource = dsMaster
        TabOrder = 13
        OnExit = DBEdit8Exit
        OnKeyDown = DBEdit8KeyDown
      end
      object DBEdit9: TDBEdit
        Tag = 1
        Left = 227
        Top = 104
        Width = 242
        Height = 19
        DataField = 'cNmIncoterms'
        DataSource = DataSource10
        TabOrder = 10
      end
      object DBEdit18: TDBEdit
        Tag = 1
        Left = 185
        Top = 104
        Width = 39
        Height = 19
        DataField = 'cSigla'
        DataSource = DataSource10
        TabOrder = 9
      end
      object DBEdit19: TDBEdit
        Left = 117
        Top = 104
        Width = 65
        Height = 19
        DataField = 'nCdIncoterms'
        DataSource = dsMaster
        TabOrder = 8
        OnExit = DBEdit19Exit
        OnKeyDown = DBEdit19KeyDown
      end
      object DBEdit3: TDBEdit
        Left = 117
        Top = 176
        Width = 352
        Height = 19
        DataField = 'cMsgNF1'
        DataSource = dsMaster
        TabOrder = 15
      end
      object DBEdit13: TDBEdit
        Left = 117
        Top = 200
        Width = 352
        Height = 19
        DataField = 'cMsgNF2'
        DataSource = dsMaster
        TabOrder = 16
      end
      object DBEdit25: TDBEdit
        Left = 117
        Top = 8
        Width = 65
        Height = 19
        DataField = 'nCdTerceiroRepres'
        DataSource = dsMaster
        TabOrder = 0
        OnExit = DBEdit25Exit
        OnKeyDown = DBEdit25KeyDown
      end
      object DBEdit26: TDBEdit
        Tag = 1
        Left = 185
        Top = 8
        Width = 284
        Height = 19
        DataField = 'cNmTerceiro'
        DataSource = DataSource15
        TabOrder = 1
      end
      object edtAreaVenda: TER2LookupDBEdit
        Left = 117
        Top = 32
        Width = 65
        Height = 19
        DataField = 'nCdAreaVenda'
        DataSource = dsMaster
        TabOrder = 2
        CodigoLookup = 753
        QueryLookup = qryAreaVenda
      end
      object DBEdit63: TDBEdit
        Tag = 1
        Left = 185
        Top = 32
        Width = 284
        Height = 19
        DataField = 'cNmAreaVenda'
        DataSource = dsAreaVenda
        TabOrder = 3
      end
      object edtDivisaoVenda: TER2LookupDBEdit
        Left = 117
        Top = 56
        Width = 65
        Height = 19
        DataField = 'nCdDivisaoVenda'
        DataSource = dsMaster
        TabOrder = 4
        OnBeforeLookup = edtDivisaoVendaBeforeLookup
        OnBeforePosicionaQry = edtDivisaoVendaBeforePosicionaQry
        CodigoLookup = 754
        QueryLookup = qryDivisaoVenda
      end
      object DBEdit64: TDBEdit
        Tag = 1
        Left = 185
        Top = 56
        Width = 284
        Height = 19
        DataField = 'cNmDivisaoVenda'
        DataSource = dsDivisaoVenda
        TabOrder = 5
      end
      object edtCanalDistribuicao: TER2LookupDBEdit
        Left = 117
        Top = 80
        Width = 65
        Height = 19
        DataField = 'nCdCanalDistribuicao'
        DataSource = dsMaster
        TabOrder = 6
        CodigoLookup = 755
        QueryLookup = qryCanalDistribuicao
      end
      object DBEdit65: TDBEdit
        Tag = 1
        Left = 185
        Top = 80
        Width = 284
        Height = 19
        DataField = 'cNmCanalDistribuicao'
        DataSource = dsCanalDistribuicao
        TabOrder = 7
      end
      object cxPageControl2: TcxPageControl
        Left = 488
        Top = 0
        Width = 553
        Height = 317
        ActivePage = cxTabSheet5
        Align = alRight
        LookAndFeel.NativeStyle = True
        TabOrder = 17
        ClientRectBottom = 313
        ClientRectLeft = 4
        ClientRectRight = 549
        ClientRectTop = 24
        object cxTabSheet5: TcxTabSheet
          Caption = 'Promotores'
          ImageIndex = 0
          object DBGridEh9: TDBGridEh
            Left = 0
            Top = 0
            Width = 545
            Height = 289
            Align = alClient
            Ctl3D = False
            DataGrouping.GroupLevels = <>
            DataSource = dsPedidoPromotor
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Segoe UI'
            FooterFont.Style = []
            IndicatorTitle.TitleButton = True
            IndicatorOptions = [gioShowRowIndicatorEh]
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
            ParentCtl3D = False
            PopupMenu = PopupMenu1
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Segoe UI'
            TitleFont.Style = []
            UseMultiTitle = True
            OnKeyDown = DBGridEh9KeyDown
            Columns = <
              item
                EditButtons = <>
                FieldName = 'nCdTerceiroPromotor'
                Footers = <>
                Width = 65
              end
              item
                EditButtons = <>
                FieldName = 'cNmTerceiro'
                Footers = <>
                ReadOnly = True
                Width = 419
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
      object DBEdit42: TDBEdit
        Tag = 1
        Left = 185
        Top = 128
        Width = 284
        Height = 19
        DataField = 'cNmTabTipoModFrete'
        DataSource = dsTabTipoModFrete
        TabOrder = 12
      end
      object DBEdit43: TDBEdit
        Left = 117
        Top = 128
        Width = 65
        Height = 19
        DataField = 'nCdTabTipoModFrete'
        DataSource = dsMaster
        TabOrder = 11
        OnExit = DBEdit43Exit
        OnKeyDown = DBEdit43KeyDown
      end
    end
    object TabParcela: TcxTabSheet
      Caption = 'Parcelas'
      ImageIndex = 3
      object Image3: TImage
        Left = 0
        Top = 0
        Width = 1041
        Height = 317
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label16: TLabel
        Left = 288
        Top = 16
        Width = 103
        Height = 13
        Caption = 'Valor Adiantamento'
        FocusControl = DBEdit24
      end
      object DBGridEh3: TDBGridEh
        Left = 8
        Top = 40
        Width = 465
        Height = 241
        DataGrouping.GroupLevels = <>
        DataSource = dsPrazoPedido
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPrazoPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iDias'
            Footers = <>
            Width = 71
          end
          item
            EditButtons = <>
            FieldName = 'dVencto'
            Footers = <>
            Width = 128
          end
          item
            EditButtons = <>
            FieldName = 'nValPagto'
            Footers = <>
            Width = 122
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btSugParcela: TcxButton
        Left = 8
        Top = 4
        Width = 121
        Height = 33
        Caption = 'Sugerir Parcelas'
        TabOrder = 1
        OnClick = btSugParcelaClick
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF91919189888887868687878788888789
          8988898888888888888887878686878685979797FFFFFFFFFFFFFFFFFFFFFFFF
          888888C2C2C1BCBCBCBCBCBCBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBC2C2
          C18B8B8AFFFFFFFFFFFFFFFFFFFFFFFF8C8C8BFFFFFFEBEBEBEBEBEBE9E9E9E8
          E8E8E7E7E7E7E7E7E6E6E6E6E6E6FFFFFF8E8D8CFFFFFFFFFFFFFFFFFFFFFFFF
          929191FFFFFFB4B4B4949494E7E7E7B2B2B2939393E3E3E3B0B0B0919191FBFB
          FB929291FFFFFFFFFFFFFFFFFFFFFFFF959595FFFFFFE8E8E8E7E7E7E5E5E5E3
          E3E3E2E2E2E0E0E0DFDFDFDCDCDCFFFFFF959595FFFFFFFFFFFFFFFFFFFFFFFF
          9A9A99FFFFFFB1B1B1919191E2E2E2ADADAD8F8F8FDCDCDCA9A9A98D8D8DFBFB
          FB9A9999FFFFFFFFFFFFFFFFFFFFFFFF9E9D9DFFFFFFE3E3E3E1E1E1DCDCDCDB
          DBDBD7D7D7D3D3D3D3D3D3D1D1D1FFFFFF9E9D9CFFFFFFFFFFFFFFFFFFFFFFFF
          A0A0A0FFFFFFADADAD8E8E8ED8D8D8A5A5A58A8A8ACECECE7374E85258DBFBFB
          FB9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFCFCDADADAD7D7D7D2D2D2CE
          CECEC9C9C9C5C5C5C2C2C2BFBFBFFFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
          A3A3A3FFFFFFB07B56C38D67C58F68C69069C8926BCA946CCA956EB07B56FFFF
          FFA1A09FFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4FFFFFFB07B56C18B64C38D66C5
          8F67C69069C8926BCA946CB07B56FFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
          A3A3A3FFFFFFA7724DA7724DA7724DA7724DA7724DA7724DA7724DA7724DFFFF
          FF9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FFFFFFFFFFFFFFFFFFFFFFFF
          B3B3B3A9A9A9A8A8A8ABABABACACACADADADACACACABABABA8A8A8A4A4A4A3A3
          A3B5B5B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        LookAndFeel.Kind = lfOffice11
      end
      object DBEdit24: TDBEdit
        Left = 395
        Top = 10
        Width = 81
        Height = 19
        DataField = 'nValAdiantamento'
        DataSource = dsMaster
        TabOrder = 2
      end
    end
    object TabFollowUp: TcxTabSheet
      Caption = 'Follow Up'
      ImageIndex = 3
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 4
        Width = 609
        Height = 309
        DataGrouping.GroupLevels = <>
        DataSource = DataSource13
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFollowUp'
            Footers = <>
            Width = 113
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 91
          end
          item
            EditButtons = <>
            FieldName = 'cOcorrenciaResum'
            Footers = <>
            Width = 238
          end
          item
            EditButtons = <>
            FieldName = 'dDtProxAcao'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo1: TDBMemo
        Left = 616
        Top = 4
        Width = 409
        Height = 309
        DataField = 'cOcorrencia'
        DataSource = DataSource13
        Enabled = False
        TabOrder = 1
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'Situa'#231#227'o'
      ImageIndex = 5
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 1041
        Height = 317
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label36: TLabel
        Left = 56
        Top = 14
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status'
      end
      object Label37: TLabel
        Left = 24
        Top = 72
        Width = 90
        Height = 13
        Caption = 'Observa'#231#227'o Geral'
        FocusControl = DBMemo2
      end
      object Label38: TLabel
        Left = 464
        Top = 72
        Width = 117
        Height = 13
        Caption = 'Observa'#231#227'o Financeiro'
        FocusControl = DBMemo3
      end
      object Label14: TLabel
        Left = 8
        Top = 38
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'In'#237'cio Produ'#231#227'o'
        FocusControl = DBEdit20
      end
      object DBEdit35: TDBEdit
        Tag = 1
        Left = 96
        Top = 8
        Width = 137
        Height = 19
        DataField = 'cNmTabStatusPed'
        DataSource = DataSource5
        TabOrder = 0
      end
      object DBEdit40: TDBEdit
        Tag = 1
        Left = 247
        Top = 8
        Width = 250
        Height = 19
        DataField = 'cDadoAutoriz'
        DataSource = DataSource12
        TabOrder = 1
      end
      object DBMemo2: TDBMemo
        Left = 24
        Top = 88
        Width = 433
        Height = 217
        DataField = 'cOBS'
        DataSource = dsMaster
        TabOrder = 2
      end
      object DBMemo3: TDBMemo
        Tag = 1
        Left = 464
        Top = 88
        Width = 425
        Height = 217
        DataField = 'cOBSFinanc'
        DataSource = dsMaster
        TabOrder = 3
      end
      object DBEdit20: TDBEdit
        Tag = 1
        Left = 96
        Top = 32
        Width = 137
        Height = 19
        DataField = 'dDtIniProducao'
        DataSource = dsMaster
        TabOrder = 4
      end
    end
    object TabDesTecnica: TcxTabSheet
      Caption = 'Descri'#231#227'o T'#233'cnica'
      ImageIndex = 7
      OnEnter = TabDesTecnicaEnter
      object DBGridEh7: TDBGridEh
        Left = 0
        Top = 0
        Width = 481
        Height = 313
        DataGrouping.GroupLevels = <>
        DataSource = DataSource14
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 279
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 67
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo4: TDBMemo
        Left = 488
        Top = 0
        Width = 329
        Height = 313
        DataField = 'cDescricaoTecnica'
        DataSource = DataSource14
        TabOrder = 1
        OnExit = DBMemo4Exit
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Anota'#231#245'es'
      ImageIndex = 8
      object DBMemo5: TDBMemo
        Left = 0
        Top = 0
        Width = 1041
        Height = 317
        Align = alClient
        DataField = 'cAnotacao'
        DataSource = dsMaster
        TabOrder = 0
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Restri'#231#245'es Cr'#233'dito'
      ImageIndex = 9
      object DBGridEh8: TDBGridEh
        Left = 0
        Top = 0
        Width = 1041
        Height = 317
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsRestricoes
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoRestricaoVenda'
            Footers = <>
            Title.Caption = 'Tipo de Divergencia'
          end
          item
            EditButtons = <>
            FieldName = 'cPendenteLiberacao'
            Footers = <>
            Title.Caption = 'Pendente de Libera'#231#227'o'
            Width = 135
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit37: TDBEdit [35]
    Tag = 1
    Left = 976
    Top = 563
    Width = 81
    Height = 19
    DataField = 'nValPedido'
    DataSource = dsMaster
    TabOrder = 20
  end
  object DBGridEh2: TDBGridEh [36]
    Left = 152
    Top = 360
    Width = 633
    Height = 41
    DataGrouping.GroupLevels = <>
    DataSource = dsGrade
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 21
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Visible = False
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBEdit34: TDBEdit [37]
    Tag = 1
    Left = 850
    Top = 112
    Width = 207
    Height = 19
    DataField = 'cDadoAutoriz'
    DataSource = DataSource12
    TabOrder = 22
  end
  object DBEdit21: TDBEdit [38]
    Left = 886
    Top = 40
    Width = 73
    Height = 19
    DataField = 'dDtPrevEntIni'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit23: TDBEdit [39]
    Left = 980
    Top = 40
    Width = 73
    Height = 19
    DataField = 'dDtPrevEntFim'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit27: TDBEdit [40]
    Tag = 1
    Left = 129
    Top = 563
    Width = 81
    Height = 19
    DataField = 'nValProdutos'
    DataSource = dsMaster
    TabOrder = 23
  end
  object DBEdit28: TDBEdit [41]
    Tag = 1
    Left = 295
    Top = 563
    Width = 81
    Height = 19
    DataField = 'nValDesconto'
    DataSource = dsMaster
    TabOrder = 24
  end
  object DBEdit29: TDBEdit [42]
    Tag = 1
    Left = 635
    Top = 563
    Width = 81
    Height = 19
    DataField = 'nValAcrescimo'
    DataSource = dsMaster
    TabOrder = 25
  end
  object DBEdit38: TDBEdit [43]
    Left = 710
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdCondPagto'
    DataSource = dsMaster
    TabOrder = 12
    OnExit = DBEdit38Exit
    OnKeyDown = DBEdit38KeyDown
  end
  object DBEdit22: TDBEdit [44]
    Tag = 1
    Left = 778
    Top = 88
    Width = 279
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = DataSource9
    TabOrder = 26
  end
  object btContatoPadrao: TcxButton [45]
    Left = 1058
    Top = 62
    Width = 25
    Height = 25
    Hint = 'Sugerir Contato Padr'#227'o'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 27
    TabStop = False
    OnClick = btContatoPadraoClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4DFE64F575C45
      4646404242464D50C9D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF838E95BCBCBBEBEAEACDCCCCA3A19F5A656DFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF79868CA6A5A2A8
      A2A29D9998948F8B525960FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFDBE5EC6683977A95A33A8A98357F8C606E762E4458DFE8EFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F1FB70AFE1469DE64BBEF747
      E6FD41E5FD51C3FB167CDE3D88D3D5E8F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E6F3FB69B3E9A6D3F365AEF074E1F673E1F672E0F671E0F64CA3EC9CC3EF2C81
      D7C9E0F5FFFFFFFFFFFFFFFFFFFDFEFF7FC0ECA5D4F3DCFAFE38A1EB74E1F66A
      E4F65DE2F572E0F61691E8C0F5FDACCEF12D83D7EAF3FBFFFFFFFFFFFFB7DDF5
      8BC8EFECFCFE77E1F72F99EA75E1F674E1F668DEF573E1F60986E646D5F3DCFE
      FE6FAAE579B3E6FFFFFFFFFFFF7FC5EEC9E9F9D4F9FD7CE3F786E5F860B1EF68
      B5EF63B4EF4CA6EC82E4F759DCF58AEBFACBE2F7398FDAFFFFFFFFFFFF7EC6EE
      DFF6FDC8F5FCCDF6FCD6F7FDD3F4FCCFF2FCCAF1FBC4F0FCBAF2FB96EAF872E5
      F7E2F4FD3289D8FFFFFFFFFFFF95D2F2D2EFFBDBF9FEDFF9FDECFBFEEEFCFEEF
      FCFEEFFCFEEBFBFEE0F9FEB8F1FBA8F1FBCBE5F83E95DDFFFFFFFFFFFFC8E9F9
      B4E3F8E5FAFEDBF8FDE4FAFEF0FCFEF9FEFFF9FEFFEFFCFED2F6FDB4F1FBEDFD
      FF6BB3EA88C2ECFFFFFFFFFFFFFCFEFFB5E2F6C3EBFAE2F9FDE0F9FDD5F7FDCF
      F6FDC9F4FCC7F4FCD6F9FDEBFAFE90CAF250A9E6F3F9FDFFFFFFFFFFFFFFFFFF
      F7FCFEC4E7F9B7E4F8C7ECFBD7F3FCE1F7FDE2F8FED8F0FCB6DFF86BBBED6CB9
      EBE8F4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2F5FBC1E6F8A8DDF694
      D2F185CCF080C8F087CBF0BBE1F6FFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit31: TDBEdit [46]
    Tag = 1
    Left = 808
    Top = 563
    Width = 81
    Height = 19
    DataField = 'nValDevoluc'
    DataSource = dsMaster
    TabOrder = 28
  end
  object DBEdit32: TDBEdit [47]
    Left = 96
    Top = 160
    Width = 65
    Height = 19
    DataField = 'nCdEnderecoEntrega'
    DataSource = dsMaster
    TabOrder = 5
    OnEnter = DBEdit32Enter
    OnExit = DBEdit32Exit
    OnKeyDown = DBEdit32KeyDown
  end
  object DBEdit33: TDBEdit [48]
    Tag = 1
    Left = 164
    Top = 160
    Width = 421
    Height = 19
    DataField = 'cEnderecoCompleto'
    DataSource = DataSource16
    TabOrder = 29
  end
  object btDescontoTotalPedido: TcxButton [49]
    Left = 379
    Top = 561
    Width = 25
    Height = 25
    Hint = 'Calcular Desconto em Todo Pedido'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 30
    TabStop = False
    OnClick = btDescontoTotalPedidoClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF91919189888887868687878788888789
      8988898888888888888887878686878685979797FFFFFFFFFFFFFFFFFFFFFFFF
      888888C2C2C1BCBCBCBCBCBCBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBC2C2
      C18B8B8AFFFFFFFFFFFFFFFFFFFFFFFF8C8C8BFFFFFFEBEBEBEBEBEBE9E9E9E8
      E8E8E7E7E7E7E7E7E6E6E6E6E6E6FFFFFF8E8D8CFFFFFFFFFFFFFFFFFFFFFFFF
      929191FFFFFFB4B4B4949494E7E7E7B2B2B2939393E3E3E3B0B0B0919191FBFB
      FB929291FFFFFFFFFFFFFFFFFFFFFFFF959595FFFFFFE8E8E8E7E7E7E5E5E5E3
      E3E3E2E2E2E0E0E0DFDFDFDCDCDCFFFFFF959595FFFFFFFFFFFFFFFFFFFFFFFF
      9A9A99FFFFFFB1B1B1919191E2E2E2ADADAD8F8F8FDCDCDCA9A9A98D8D8DFBFB
      FB9A9999FFFFFFFFFFFFFFFFFFFFFFFF9E9D9DFFFFFFE3E3E3E1E1E1DCDCDCDB
      DBDBD7D7D7D3D3D3D3D3D3D1D1D1FFFFFF9E9D9CFFFFFFFFFFFFFFFFFFFFFFFF
      A0A0A0FFFFFFADADAD8E8E8ED8D8D8A5A5A58A8A8ACECECE7374E85258DBFBFB
      FB9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFCFCDADADAD7D7D7D2D2D2CE
      CECEC9C9C9C5C5C5C2C2C2BFBFBFFFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
      A3A3A3FFFFFFB07B56C38D67C58F68C69069C8926BCA946CCA956EB07B56FFFF
      FFA1A09FFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4FFFFFFB07B56C18B64C38D66C5
      8F67C69069C8926BCA946CB07B56FFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
      A3A3A3FFFFFFA7724DA7724DA7724DA7724DA7724DA7724DA7724DA7724DFFFF
      FF9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FFFFFFFFFFFFFFFFFFFFFFFF
      B3B3B3A9A9A9A8A8A8ABABABACACACADADADACACACABABABA8A8A8A4A4A4A3A3
      A3B5B5B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object btAcompanharProducao: TcxButton [50]
    Left = 8
    Top = 589
    Width = 161
    Height = 34
    Caption = 'Acompanhar Produ'#231#227'o'
    TabOrder = 31
    Visible = False
    OnClick = btAcompanharProducaoClick
    Glyph.Data = {
      06030000424D06030000000000003600000028000000100000000F0000000100
      180000000000D002000000000000000000000000000000000000C6DEC6C6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C0000000000008C9C9C8C9C9CC6D6
      DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C00000000
      0000D6A58CD6A58C0000008C9C9CC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
      C6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFFEFDE0000008C9C9C8C9C
      9CC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFF
      EFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DE000000
      EFFFFFFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDE0000008C9C
      9C8C9C9CC6D6DEC6D6DEC6D6DE000000EFFFFFBDBDBDBDBDBDBDBDBDFFEFDEFF
      EFDEFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DE8C9C9C
      000000000000000000000000BDBDBDBDBDBDFFEFDEFFEFDEFFEFDEFFEFDE0000
      008C9C9C8C9C9CC6D6DEC6D6DE8C9C9C000000EFFFFFEFFFFFEFFFFF0000008C
      9C9CBDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DE000000
      EFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEFDEFFEF
      DE0000008C9C9C8C9C9C000000DEBDD6EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
      FFFF000000BDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9C000000DEBDD6
      EFFFFFEFFFFF000000000000EFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEF
      DED6A58C000000C6D6DE000000DEBDD6DEBDD6EFFFFF000000EFFFFFEFFFFFEF
      FFFF000000BDBDBDFFEFDEEFFFFF0000000000008C9C9CC6D6DEC6D6DE000000
      DEBDD6DEBDD6000000EFFFFFEFFFFF000000D6A58CEFFFFF0000000000008C9C
      9CC6D6DEC6D6DEC6D6DEC6DEC68C9C9C000000DEBDD6DEBDD6DEBDD60000008C
      9C9C0000000000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
      C6DEC60000000000000000000000000000008C9C9CC6D6DEC6D6DEC6DEC6C6D6
      DEC6D6DEC6D6DEC6DEC6}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit41: TDBEdit [51]
    Tag = 1
    Left = 96
    Top = 184
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroPagador'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit39: TDBEdit [52]
    Tag = 1
    Left = 164
    Top = 184
    Width = 421
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource8
    TabOrder = 32
  end
  object DBEdit30: TDBEdit [53]
    Left = 96
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit30Exit
    OnKeyDown = DBEdit30KeyDown
  end
  object DBEdit36: TDBEdit [54]
    Tag = 1
    Left = 164
    Top = 88
    Width = 353
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource2
    TabOrder = 33
  end
  object DBEdit44: TDBEdit [55]
    Tag = 1
    Left = 466
    Top = 564
    Width = 81
    Height = 19
    DataField = 'nValFrete'
    DataSource = dsMaster
    TabOrder = 34
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Pedido'
      ' WHERE nCdPedido           = :nPK'
      '   AND cFlgPedidoWeb       = 0'
      '   AND cFlgPedidoTransfEst = 0'
      '')
    Left = 72
    Top = 352
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryMasternCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdTerceiroColab: TIntegerField
      FieldName = 'nCdTerceiroColab'
    end
    object qryMasternCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryMasternCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryMasterdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMasternCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryMasternCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryMasternPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryMasternPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryMasternValProdutos: TBCDField
      FieldName = 'nValProdutos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValServicos: TBCDField
      FieldName = 'nValServicos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValImposto: TBCDField
      FieldName = 'nValImposto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValOutros: TBCDField
      FieldName = 'nValOutros'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercNrPedTerceiro: TStringField
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryMasterdDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryMasternCdUsuarioAutor: TIntegerField
      FieldName = 'nCdUsuarioAutor'
    end
    object qryMasterdDtRejeicao: TDateTimeField
      FieldName = 'dDtRejeicao'
    end
    object qryMasternCdUsuarioRejeicao: TIntegerField
      FieldName = 'nCdUsuarioRejeicao'
    end
    object qryMastercMotivoRejeicao: TMemoField
      FieldName = 'cMotivoRejeicao'
      BlobType = ftMemo
    end
    object qryMastercNmContato: TStringField
      FieldName = 'cNmContato'
      Size = 35
    end
    object qryMasternCdEstoqueMov: TIntegerField
      FieldName = 'nCdEstoqueMov'
    end
    object qryMastercFlgCritico: TIntegerField
      FieldName = 'cFlgCritico'
    end
    object qryMasternSaldoFat: TBCDField
      FieldName = 'nSaldoFat'
      Precision = 12
      Size = 2
    end
    object qryMastercOBSFinanc: TMemoField
      FieldName = 'cOBSFinanc'
      BlobType = ftMemo
    end
    object qryMastercMsgNF1: TStringField
      FieldName = 'cMsgNF1'
      Size = 150
    end
    object qryMastercMsgNF2: TStringField
      FieldName = 'cMsgNF2'
      Size = 150
    end
    object qryMasternPercDescontoVencto: TBCDField
      FieldName = 'nPercDescontoVencto'
      Precision = 12
      Size = 2
    end
    object qryMasternPercAcrescimoVendor: TBCDField
      FieldName = 'nPercAcrescimoVendor'
      Precision = 12
      Size = 2
    end
    object qryMasternCdMotBloqPed: TIntegerField
      FieldName = 'nCdMotBloqPed'
    end
    object qryMasternCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
    object qryMastercAtuCredito: TIntegerField
      FieldName = 'cAtuCredito'
    end
    object qryMastercCreditoLibMan: TIntegerField
      FieldName = 'cCreditoLibMan'
    end
    object qryMasterdDtIniProducao: TDateTimeField
      FieldName = 'dDtIniProducao'
    end
    object qryMastercAnotacao: TMemoField
      FieldName = 'cAnotacao'
      BlobType = ftMemo
    end
    object qryMastercFlgOrcamento: TIntegerField
      FieldName = 'cFlgOrcamento'
    end
    object qryMasternValAdiantamento: TBCDField
      FieldName = 'nValAdiantamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgAdiantConfirm: TIntegerField
      FieldName = 'cFlgAdiantConfirm'
    end
    object qryMasternCdUsuarioConfirmAdiant: TIntegerField
      FieldName = 'nCdUsuarioConfirmAdiant'
    end
    object qryMasternValDevoluc: TBCDField
      FieldName = 'nValDevoluc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdEnderecoEntrega: TIntegerField
      FieldName = 'nCdEnderecoEntrega'
    end
    object qryMasternCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryMasternCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryMasternCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
    object qryMasternCdTabTipoModFrete: TIntegerField
      FieldName = 'nCdTabTipoModFrete'
    end
  end
  inherited dsMaster: TDataSource
    Left = 72
    Top = 384
  end
  inherited qryID: TADOQuery
    Left = 944
    Top = 208
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 608
    Top = 136
  end
  inherited qryStat: TADOQuery
    Left = 912
    Top = 176
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 576
    Top = 168
  end
  inherited ImageList1: TImageList
    Left = 504
    Top = 376
    Bitmap = {
      494C01010E001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000000000000000000000000
      0000000000008080800000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF000000000000000000000000000000
      00008080800000000000DEBED400000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF000000000000000000000000000000
      000000000000DEBED400DEBED40000000000DEBED40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF000000000000000000999999009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF00000000000000000000000000E6E6
      E6000000000000000000DEBED40000000000DEBED400DEBED400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF00000000000000000000000000E6E6
      E60080808000000000000000000000000000DEBED40000000000808080004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF000000000000000000000000000080
      0000E6E6E600E6E6E6000000000099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF000000000000000000000000000080
      000000800000E6E6E600E6E6E60099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E6009999990000000000000000004E7EA6000000
      0000000000004E7EA60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009797
      97006461600063605F00625F5E00615E5E00615E5D00615E5D00615E5D00615E
      5D00615E5D00BAB1A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009999
      990000000000FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE000000
      0000615E5D00BAB1A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F4F400F9F7F700FEF9FA00FFFE
      FF00E7E1E2000D070800050000000A0405000A04050005090E0000000E000017
      2B0000000F0000051300000C170000000600000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000D5CABC00B9B0A400B9B0A4009B9B
      9B0000000000A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A5000000
      0000615E5D00B8B0A400B8B0A400D5CABB000000000000000000000000000000
      000000000000C2C2C200717272006D6C6B0071727200777B7B00A4A8A800B6B6
      B60000000000000000000000000000000000FEFCFC0000000000FEF9FA00F5F0
      F100FFFEFF00130D0E00B2ACAD00B2ACAD00C3BBBC00A4A8B3000F2B4A003D64
      8B00416387005D7A99004C647C00000114000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A89000000000000000000ABA59C0066666600646464009D9D
      9D0000000000FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD000000
      0000615E5D0048484800454545009D968E00000000000000000000000000CCCC
      CC009E9E9E003D424200191D23000101010001010100040506003D4242003D42
      420051575700B2B2B2000000000000000000FEFCFC00F0EEEE00FFFBFC00F4EF
      F000F9F3F400150F1000E3DBDC00A29A9B00ACA4A500A6ABBA00334F7E00234B
      86001A3E74001634630010295100000322000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF0009236900515757000000000000000000696969009B9B9B00B8B8B8008F8F
      8F00E4E4E400949494009494940094949400949494009494940094949400E4E4
      E4005B585700A5A5A5008E918E00434343000000000000000000C6C6C6009E9E
      9E0031333200092369003D424200CCCCCC00C6C6C600B9BABA00B9BABA00C2C2
      C200313332008C908F00B6B6B60000000000E4DFE000ABA7A600817C7B009691
      90008C84840007000000FFFEFE00FDF4F100F1E8E500D8D8E80040558C00A0BF
      FF00BCD8FF00C0D9FF00556998000000210000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000006A6A6A00B8B8B800B6B6B6007A7A
      7A00C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1
      C10053504F00A3A3A30035FE3500444444000000000000000000CCC5C0003133
      3200107082001070820031333200D3C1B500EEEEEE00FCFEFE00EEEEEE00CCC5
      C0003133320093929200AEAEAE000000000007000000120B0800090000000900
      0000251B14000C000000EFE1DB00B2A59D00B4A79F00A39CA900383E73003543
      8B00354385002D3973001420500000001D0000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF00092369005157570000000000000000006C6C6C00B8B8B800474241000101
      0100020202000202020001010100010101000101010001010100010101000101
      01000101010046424100A5A5A5004646460000000000000000007B8585001070
      82000599CF000599CF0010708200191D23009B9C8F00FCFEFE00AAA39F000405
      06000923690065646400B2B2B20000000000201818009F969300CABFBB00C2B8
      B100ACA199001C0F0700FFFDF400FAEBE200FFFBEF00EADFE70056548200D4D9
      FF00CED2FF00D1D8FF00596189000003210000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF00092369005157570000000000000000006D6D6D00B8B8B800B8B8B8005252
      52006969690065656500606060005B5B5B0056565600525252004D4D4D004848
      480033333300A5A5A500A5A5A5004747470000000000000000003D4242001070
      82000599CF0007F0F90010708200191D23009E9E9E00FCFEFE00A4A8A800191D
      2300191D23003D424200C6C6C60000000000160E0E00F7EEEB00AEA39F00ADA3
      9C00B6ABA3000C010000F1E5DB00ACA09600A8999000AA9FA200555272004043
      6F003A3F6600595E7F005358710000000E0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF00092369005157570000000000000000006E6E6E00B8B8B800B8B8B8005454
      54006B6B6B0066666600626262005D5D5D0058585800535353004F4F4F004A4A
      4A0034343400A5A5A500A5A5A5004848480000000000000000003D4242000599
      CF0007F0F90007F0F9003D424200CBB9B100FCFEFE00FCFEFE00FCFEFE00DBC8
      C300040506003D424200CCCCCC000000000007000000F9F2EF00EEE5E100F7EC
      E800F2E6E00013080000FFFFF700FCF0E600FFF5EB00F9EFEF009F9EAE00E8EA
      FF00EEF2FF00D0D5E400A7ABB60012161B0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000070707000E7E7E70000000000B7B7
      B700C5C5C500C0C0C000B8B8B800B2B2B200ABABAB00A3A3A3009B9B9B009393
      93006F6F6F0000000000CCCCCC004A4A4A0000000000000000003D4242000599
      CF0007F0F90007F0F90007F0F90010708200DAD2C900FCFEFE00DADDD7005157
      5700107082003D424200CCCCCC000000000009040300F8F1EE00B1A8A400A198
      9400B4AAA3001A110800FFFFF700E5DAD200E2D7CF00F1E9E200B7B5B500D0D2
      D300F3F5F500EDF0EE00B2B3AF000405000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000ACA69D006F6F6F006C6C6C006969
      69006E6E6E006C6C6C006A6A6A00686868006666660066666600666666006666
      660053535300505050004E4E4E009E988F000000000000000000939292001070
      820007F0F90007F0F900B0F4FA007EE1DC0010708200CBB9B100515757000599
      CF00107082009392920000000000000000000D080700F1ECEB00EAE3E000F4EB
      E700DDD4D000291F1800ECE2DB00FFFFF800FFFFF800EEE7DE00E0DED400F8F7
      ED00F8F8EC00F8F8EC00FFFFF5000404000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF00092369005157570000000000000000000000000000000000000000009A9A
      9A00F3F3F300EFEFEF00EAEAEA00E6E6E600E1E1E100DFDFDF00DFDFDF00DFDF
      DF00615E5D000000000000000000000000000000000000000000CCC5C0003D42
      42000599CF0007F0F900C4FDFD008BF6F90007F0F900107082000599CF000599
      CF003D424200CCC5C000000000000000000004000000EFEAE9009F9A9700BDB7
      B200B4AEA9000B0200001A110D00090100000D030000140D0400070300001C1B
      0D00050500000B0B0000060600001C1C0C0000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF00107082005157570000000000000000000000000000000000000000009B9B
      9B00F7F7F700F3F3F300EEEEEE00EAEAEA00E5E5E500E1E1E100DFDFDF00DFDF
      DF00615E5D000000000000000000000000000000000000000000C6C6C600B8B1
      AA00484E4E001070820007F0F90007F0F90007F0F90007F0F900107082003D42
      4200B8B1AA00C6C6C600000000000000000005010000FFFFFE00EAE5E200EBE6
      E300E6E1DE00ACA6A100F2ECE700F6F0EB00F3EDE800C0BAB300120D04007371
      6600FFFFF700FEFDEF00F9F9EB00FAFAEC000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000009D9D
      9D00FBFBFB00F7F7F700F2F2F200EEEEEE00EAEAEA00E5E5E500E1E1E100DFDF
      DF00615E5D00000000000000000000000000000000000000000000000000C6CA
      C600AEAEAE007A7363003D42420009236900092369003D4242007A736300AEAE
      AE00C6CAC60000000000000000000000000002000000FFFFFE00F4F0EF00E6E2
      E100ECE8E700B5B0AD00E8E3E000E1DCD900F0EBE800BCB7B40007040000928F
      8B00FFFFFC00F9F8F400F5F5EF00FFFFFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009F9F
      9F00000000000000000000000000000000000000000000000000000000000000
      0000615E5D000000000000000000000000000000000000000000000000000000
      000000000000CCCCCC00AAA39F00AAA39F00AAA39F00AAA39F00CCCCCC000000
      0000000000000000000000000000000000001C1A1900F4F2F100FFFEFD00FEFA
      F900FFFFFE00DEDAD900FFFDFC00FEFBF700FFFFFE00EBE6E700020003007F7E
      8200EDECF000FDFFFF00FDFFFF00FDFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A1A1
      A1009F9F9F009C9C9C009A9A9A009696960094949400919191008F8F8F008C8C
      8C008C8C8C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000606060000000000000000000503
      03000E0C0C00020000000705050005030200110F0F00050204001B1A2400EDED
      F900F7F7FF00E0E1EB00FBFDFF00F7FAFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C0000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C00000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D00000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D100000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF00000000000000000000000000000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D40000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00000000000000000000000000000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD50000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D60000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D80000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB00969696000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A400000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C300000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB0000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C0000000000000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E9000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF00000000FFFFFD7F00000000
      8000F01F000000000000F19F000000000000C04F000000000000845F00000000
      0000A40F000000000000A307000000000000A001000000000000A04300000000
      0000A001000000000000A043000000008000A067000000000000BF7F00000000
      0100C0FF000000000000FFFF00000000FFFFE003FFFFFFFFFFFFE813FFFF0000
      E0030810F80F4000C0030810E0030000C0030000C0010000C0030000C0010000
      C0030000C0010000C0030000C0010000C0030000C0010000C0032004C0010000
      C0030000C0030000C003E007C0030000C003E007C0030000C003E007E0070000
      FFFFEFF7F81F0000FFFFE007FFFF0000FFFFFFFF81C08001C000F00301808001
      DFE0E001000080015000C00100008001D002800100008001CF06800100008001
      B9CE000100008001A0020001000080013F620001000080012002000100008001
      200E00010000C3C3200280030181C3C3800280038181C0038FC2C0078181C003
      C03EE00F8181E007C000F83F8383F00F8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Empresa.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      'FROM UsuarioEmpresa UE'
      'INNER JOIN Empresa ON Empresa.nCdEmpresa = UE.nCdEmpresa'
      'WHERE UE.nCdUsuario = :nCdUsuario'
      'AND UE.nCdEmpresa = :nPK')
    Left = 744
    Top = 344
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '  FROM UsuarioLoja UL'
      '       INNER JOIN Loja ON Loja.nCdLoja = UL.nCdLoja'
      ' WHERE UL.nCdUsuario   = :nCdUsuario'
      '   AND UL.nCdLoja      = :nPK'
      '   AND Loja.nCdEmpresa = :nCdEmpresa')
    Left = 776
    Top = 344
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    AfterClose = qryTipoPedidoAfterClose
    AfterScroll = qryTipoPedidoAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoPedido'
      
        'INNER JOIN UsuarioTipoPedido UTP ON UTP.nCdTipoPedido = TipoPedi' +
        'do.nCdTipoPedido'
      'WHERE TipoPedido.nCdTipoPedido = :nPK'
      'AND nCdUsuario = :nCdUsuario'
      'AND (nCdTabTipoPedido = 1 OR cFlgDevolucVenda = 1)')
    Left = 568
    Top = 440
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryTipoPedidonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryTipoPedidocFlgCalcImposto: TIntegerField
      FieldName = 'cFlgCalcImposto'
    end
    object qryTipoPedidocFlgComporRanking: TIntegerField
      FieldName = 'cFlgComporRanking'
    end
    object qryTipoPedidocFlgVenda: TIntegerField
      FieldName = 'cFlgVenda'
    end
    object qryTipoPedidocGerarFinanc: TIntegerField
      FieldName = 'cGerarFinanc'
    end
    object qryTipoPedidocExigeAutor: TIntegerField
      FieldName = 'cExigeAutor'
    end
    object qryTipoPedidonCdEspTitPrev: TIntegerField
      FieldName = 'nCdEspTitPrev'
    end
    object qryTipoPedidonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTipoPedidonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTipoPedidocFlgAtuPreco: TIntegerField
      FieldName = 'cFlgAtuPreco'
    end
    object qryTipoPedidocFlgItemEstoque: TIntegerField
      FieldName = 'cFlgItemEstoque'
    end
    object qryTipoPedidocFlgItemAD: TIntegerField
      FieldName = 'cFlgItemAD'
    end
    object qryTipoPedidonCdTipoPedido_1: TIntegerField
      FieldName = 'nCdTipoPedido_1'
    end
    object qryTipoPedidonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryTipoPedidocFlgExibeAcomp: TIntegerField
      FieldName = 'cFlgExibeAcomp'
    end
    object qryTipoPedidocFlgCompra: TIntegerField
      FieldName = 'cFlgCompra'
    end
    object qryTipoPedidocFlgItemFormula: TIntegerField
      FieldName = 'cFlgItemFormula'
    end
    object qryTipoPedidocLimiteCredito: TIntegerField
      FieldName = 'cLimiteCredito'
    end
    object qryTipoPedidocFlgPermAbatDevVenda: TIntegerField
      FieldName = 'cFlgPermAbatDevVenda'
    end
    object qryTipoPedidocFlgGeraOP: TIntegerField
      FieldName = 'cFlgGeraOP'
    end
    object qryTipoPedidocFlgCalcComissao: TIntegerField
      FieldName = 'cFlgCalcComissao'
    end
    object qryTipoPedidocFlgExigeRepres: TIntegerField
      FieldName = 'cFlgExigeRepres'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTerceiroAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cCNPJCPF'
      '      ,cNmTerceiro'
      '      ,nCdTerceiroPagador'
      '      ,nCdTerceiroTransp'
      '      ,nCdIncoterms'
      '      ,nCdCondPagto'
      '      ,nPercDesconto'
      '      ,nPercAcrescimo'
      '      ,cFlgTipoFat'
      '      ,nCdAreaVenda'
      '      ,nCdDivisaoVenda'
      '      ,nCdCanalDistribuicao'
      '      ,nCdTipoTabPreco'
      '      ,nCdGrupoCredito'
      '      ,cOBS'
      '  FROM Terceiro'
      ' WHERE nCdStatus = 1'
      '   AND nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      
        '                     INNER JOIN TipoPedidoTipoTerceiro TTTP  ON ' +
        'TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro'
      
        '                                                            AND ' +
        'TTTp.nCdTipoPedido   = :nCdTipoPedido'
      '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro)')
    Left = 768
    Top = 440
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryTerceironCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryTerceironCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryTerceironCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryTerceironPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      Precision = 5
      Size = 2
    end
    object qryTerceironPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 5
      Size = 2
    end
    object qryTerceirocFlgTipoFat: TIntegerField
      FieldName = 'cFlgTipoFat'
    end
    object qryTerceironCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryTerceironCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryTerceironCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
    object qryTerceironCdTipoTabPreco: TIntegerField
      FieldName = 'nCdTipoTabPreco'
    end
    object qryTerceironCdGrupoCredito: TIntegerField
      FieldName = 'nCdGrupoCredito'
    end
    object qryTerceirocOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
  end
  object qryTerceiroColab: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      
        'INNER JOIN TerceiroTipoTerceiro ON TerceiroTipoTerceiro.nCdTerce' +
        'iro = Terceiro.nCdTerceiro'
      'AND nCdTipoTerceiro = 5'
      'WHERE nCdStatus = 1'
      'AND Terceiro.nCdTerceiro = :nPK'
      '')
    Left = 632
    Top = 440
    object qryTerceiroColabnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroColabcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroColabcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryTerceiroTransp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      
        '     INNER JOIN TerceiroTipoTerceiro ON TerceiroTipoTerceiro.nCd' +
        'Terceiro = Terceiro.nCdTerceiro'
      '                                    AND nCdTipoTerceiro = 3'
      'WHERE nCdStatus = 1'
      'AND Terceiro.nCdTerceiro = :nPK'
      '')
    Left = 840
    Top = 344
    object qryTerceiroTranspnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroTranspcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroTranspcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryIncoterms: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM INCOTERMS'
      'WHERE nCdIncoterms = :nPK')
    Left = 672
    Top = 344
    object qryIncotermsnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryIncotermscSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 3
    end
    object qryIncotermscNmIncoterms: TStringField
      FieldName = 'cNmIncoterms'
      Size = 50
    end
    object qryIncotermscFlgPagador: TStringField
      FieldName = 'cFlgPagador'
      FixedChar = True
      Size = 1
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CondPagto'
      'WHERE nCdCondPagto = :nPK'
      'AND cFlgPdv = 0')
    Left = 936
    Top = 344
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Moeda'
      'WHERE nCdMoeda = :nPK ')
    Left = 912
    Top = 208
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object qryEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM Estoque'
      'WHERE nCdEmpresa = :nCdEmpresa'
      'AND (nCdLoja = :nCdLoja OR :nCdLoja = 0)'
      'AND nCdEstoque = :nPK')
    Left = 872
    Top = 344
    object qryEstoquenCdEstoque: TAutoIncField
      FieldName = 'nCdEstoque'
      ReadOnly = True
    end
    object qryEstoquenCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEstoquenCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryEstoquenCdTipoEstoque: TIntegerField
      FieldName = 'nCdTipoEstoque'
    end
    object qryEstoquecNmEstoque: TStringField
      FieldName = 'cNmEstoque'
      Size = 50
    end
    object qryEstoquenCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryStatusPed: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabStatusPed'
      'WHERE nCdTabStatusPed = :nPK')
    Left = 536
    Top = 440
    object qryStatusPednCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryStatusPedcNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
  end
  object qryTerceiroPagador: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdGrupoCredito'
      'FROM Terceiro'
      'WHERE nCdStatus = 1'
      'AND nCdTerceiro = :nPK')
    Left = 904
    Top = 344
    object qryTerceiroPagadornCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroPagadorcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroPagadorcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceiroPagadornCdGrupoCredito: TIntegerField
      FieldName = 'nCdGrupoCredito'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 744
    Top = 376
  end
  object DataSource2: TDataSource
    DataSet = qryLoja
    Left = 776
    Top = 376
  end
  object DataSource3: TDataSource
    DataSet = qryTipoPedido
    Left = 568
    Top = 472
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 768
    Top = 472
  end
  object DataSource5: TDataSource
    DataSet = qryStatusPed
    Left = 536
    Top = 472
  end
  object DataSource6: TDataSource
    DataSet = qryTerceiroTransp
    Left = 840
    Top = 376
  end
  object DataSource7: TDataSource
    DataSet = qryEstoque
    Left = 872
    Top = 376
  end
  object DataSource8: TDataSource
    DataSet = qryTerceiroPagador
    Left = 904
    Top = 376
  end
  object DataSource9: TDataSource
    DataSet = qryCondPagto
    Left = 936
    Top = 376
  end
  object DataSource10: TDataSource
    DataSet = qryIncoterms
    Left = 672
    Top = 376
  end
  object qryUsuarioTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      'FROM UsuarioTipoPedido'
      'WHERE nCdUsuario = :nCdUsuario')
    Left = 976
    Top = 240
    object qryUsuarioTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
  end
  object qryUsuarioLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      'FROM UsuarioLoja'
      'WHERE nCdUsuario = :nCdUsuario')
    Left = 944
    Top = 176
    object qryUsuarioLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object DataSource11: TDataSource
    DataSet = qryTerceiroColab
    Left = 632
    Top = 472
  end
  object qryItemEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemEstoqueBeforePost
    AfterPost = qryItemEstoqueAfterPost
    BeforeDelete = qryItemEstoqueBeforeDelete
    AfterDelete = qryItemEstoqueAfterDelete
    OnCalcFields = qryItemEstoqueCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValUnitario'
      '      ,nValCMV'
      '      ,nValDesconto'
      '      ,nValAcrescimo'
      '      ,nValSugVenda'
      '      ,nValTotalItem'
      '      ,nPercIPI'
      '      ,nValIPI'
      '      ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      '      ,nValUnitarioEsp'
      '      ,nValDescAbatUnit'
      '     ,cCdProduto'
      '     ,nValFrete'
      '     ,nCdItemTransfEst'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK'
      '   AND (   (nCdTipoItemPed = 1)'
      '        OR (nCdTipoItemPed = 2))'
      '')
    Left = 892
    Top = 416
    object qryItemEstoquenCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemEstoquecNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      DisplayWidth = 150
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemEstoquenQtdePed: TBCDField
      DisplayLabel = 'Quantidades|Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemEstoquenQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidades|Atendida'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemEstoquenQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemEstoquenValUnitario: TBCDField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValUnitario'
      OnValidate = qryItemEstoquenValUnitarioValidate
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemEstoquenValAcrescimo: TBCDField
      Tag = 1
      DisplayLabel = 'Valores|Acr'#233'scimo'
      FieldName = 'nValAcrescimo'
      OnValidate = qryItemEstoquenValAcrescimoValidate
      DisplayFormat = '#,##0.0000'
      Precision = 12
    end
    object qryItemEstoquenValSugVenda: TBCDField
      DisplayLabel = 'Valores|Sug.Venda'
      FieldName = 'nValSugVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total Item'
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenPercIPI: TBCDField
      DisplayLabel = 'Impostos|% IPI'
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryItemEstoquenValIPI: TBCDField
      DisplayLabel = 'Impostos|Valor IPI'
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemEstoquenCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemEstoquenCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemEstoquecCalc: TStringField
      FieldKind = fkCalculated
      FieldName = 'cCalc'
      Size = 50
      Calculated = True
    end
    object qryItemEstoquenCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemEstoquenValCustoUnit: TBCDField
      DisplayLabel = 'Valores|Custo Unit.'
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 12
    end
    object qryItemEstoquenValDescAbatUnit: TBCDField
      DisplayLabel = 'Valores|Abatimento'
      FieldName = 'nValDescAbatUnit'
      DisplayFormat = '#,##0.0000'
      Precision = 12
    end
    object qryItemEstoquecCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemEstoquenValDesconto: TBCDField
      DisplayLabel = 'Valores|Desconto'
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryItemEstoquenValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryItemEstoquenCdItemTransfEst: TIntegerField
      FieldName = 'nCdItemTransfEst'
    end
    object qryItemEstoquenValCMV: TBCDField
      FieldName = 'nValCMV'
      Precision = 12
      Size = 2
    end
  end
  object dsItemEstoque: TDataSource
    DataSet = qryItemEstoque
    Left = 892
    Top = 448
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,nCdGrade'
      '      ,nValCusto'
      '      ,nValVenda'
      '  FROM Produto'
      ' WHERE nCdProduto    = :nPK'
      '   AND cFlgProdVenda = 1'
      '   /*AND (   (nCdTabTipoProduto = 2 AND nCdGrade > 0)'
      '        OR (nCdGrade IS NULL))*/'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM Produto Produto2'
      
        '                   WHERE Produto2.nCdProdutoPai = Produto.nCdPro' +
        'duto)'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoTipoPedido GPTP'
      '               WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto'
      '                 AND GPTP.nCdTipoPedido   = :nCdTipoPedido)')
    Left = 944
    Top = 240
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 100
    end
    object qryProdutonCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryProdutonValVenda: TBCDField
      FieldName = 'nValVenda'
      Precision = 14
      Size = 6
    end
    object qryProdutonValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
  end
  object usp_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PREPARA_GRADE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cSQLRetorno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1000
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 704
    Top = 168
  end
  object dsGrade: TDataSource
    DataSet = qryTemp
    Left = 928
    Top = 448
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    AfterPost = qryTempAfterPost
    AfterCancel = qryTempAfterCancel
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM ##Temp_Grade_Qtde')
    Left = 928
    Top = 416
  end
  object usp_Gera_SubItem: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_SUBITEM_GRADE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 640
    Top = 168
  end
  object cmdExcluiSubItem: TADOCommand
    CommandText = 
      'DELETE FROM ItemPedido'#13#10'WHERE nCdItemPedidoPai = :nPK'#13#10'AND nCdTi' +
      'poItemPed IN (4,7,8)'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 544
    Top = 376
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 976
    Top = 208
  end
  object qryPrazoPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPrazoPedidoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM PrazoPedido'
      'WHERE nCdPedido = :nPK')
    Left = 708
    Top = 344
    object qryPrazoPedidonCdPrazoPedido: TAutoIncField
      DisplayLabel = 'Parcela|C'#243'd'
      FieldName = 'nCdPrazoPedido'
      ReadOnly = True
    end
    object qryPrazoPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPrazoPedidoiDias: TIntegerField
      DisplayLabel = 'Parcela|Dias Vencimento'
      FieldName = 'iDias'
    end
    object qryPrazoPedidodVencto: TDateTimeField
      DisplayLabel = 'Parcela|Data Vencimento'
      FieldName = 'dVencto'
    end
    object qryPrazoPedidonValPagto: TBCDField
      DisplayLabel = 'Parcela|Valor Parcela'
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
  end
  object dsPrazoPedido: TDataSource
    DataSet = qryPrazoPedido
    Left = 708
    Top = 376
  end
  object usp_Sugere_Parcela: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SUGERE_PARCELA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 704
    Top = 136
  end
  object qryItemAD: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemADBeforePost
    AfterPost = qryItemADAfterPost
    BeforeDelete = qryItemADBeforeDelete
    AfterDelete = qryItemADAfterDelete
    OnCalcFields = qryItemADCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValTotalItem'
      '      ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      ',cSiglaUnidadeMedida'
      ',nCdGrupoImposto'
      ',nValUnitarioEsp'
      '  FROM ItemPedido'
      ' WHERE nCdPedido      = :nPK'
      '   AND nCdTipoItemPed = 5')
    Left = 732
    Top = 440
    object qryItemADcCdProduto: TStringField
      DisplayLabel = 'Produto|C'#243'd. Autom'#225'tico'
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemADcNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemADcSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Produto|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
    object qryItemADnQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedida'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemADnQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Atendida'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemADnQtdeCanc: TBCDField
      DisplayLabel = 'Quantidade|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemADnValCustoUnit: TBCDField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemADnValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total'
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryItemADnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemADnCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemADnCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemADnCdGrupoImposto: TIntegerField
      DisplayLabel = 'Grupo Imposto|C'#243'd'
      FieldName = 'nCdGrupoImposto'
    end
    object qryItemADcNmGrupoImposto: TStringField
      DisplayLabel = 'Grupo Imposto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmGrupoImposto'
      Size = 50
      Calculated = True
    end
    object qryItemADnValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 14
      Size = 6
    end
  end
  object dsItemAD: TDataSource
    DataSet = qryItemAD
    Left = 732
    Top = 472
  end
  object usp_Finaliza: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_FINALIZA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 736
    Top = 136
  end
  object qryDadoAutorz: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT '#39'em : '#39' + Convert(VARCHAR(20),dDtAutor,103) + '#39' por : '#39' +' +
        ' cNmUsuario  as cDadoAutoriz'
      '  FROM Pedido'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = Pedido.nCdUsua' +
        'rioAutor'
      ' WHERE nCdPedido = :nPK')
    Left = 696
    Top = 440
    object qryDadoAutorzcDadoAutoriz: TStringField
      FieldName = 'cDadoAutoriz'
      ReadOnly = True
      Size = 82
    end
  end
  object DataSource12: TDataSource
    DataSet = qryDadoAutorz
    Left = 696
    Top = 472
  end
  object qryFollow: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtFollowUp'
      '      ,cNmUsuario'
      '      ,cOcorrenciaResum'
      '      ,dDtProxAcao'
      ',cOcorrencia'
      '  FROM FollowUp'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = FollowUp.nCdUs' +
        'uario'
      ' WHERE nCdPedido = :nPK'
      'ORDER BY dDtFollowUp DESC')
    Left = 312
    Top = 443
    object qryFollowdDtFollowUp: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data'
      FieldName = 'dDtFollowUp'
      ReadOnly = True
    end
    object qryFollowcNmUsuario: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryFollowcOcorrenciaResum: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Descri'#231#227'o Resumida'
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowdDtProxAcao: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data Pr'#243'x. A'#231#227'o'
      FieldName = 'dDtProxAcao'
    end
    object qryFollowcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
  end
  object DataSource13: TDataSource
    DataSet = qryFollow
    Left = 312
    Top = 475
  end
  object qryItemFormula: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemFormulaBeforePost
    AfterPost = qryItemFormulaAfterPost
    BeforeDelete = qryItemFormulaBeforeDelete
    AfterDelete = qryItemFormulaAfterDelete
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValTotalItem'
      '      ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      '      ,cSiglaUnidadeMedida'
      '      ,nValUnitario'
      '      ,nValDesconto'
      '      ,nValAcrescimo'
      '      ,nValUnitarioEsp'
      '      ,nValDescAbatUnit'
      '  FROM ItemPedido'
      ' WHERE nCdPedido      = :nPK'
      '   AND nCdTipoItemPed = 6'
      '')
    Left = 504
    Top = 440
    object qryItemFormulacNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemFormulanQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedida'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemFormulanQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Atendida'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemFormulanQtdeCanc: TBCDField
      DisplayLabel = 'Quantidade|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemFormulanValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total'
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryItemFormulanValCustoUnit: TBCDField
      DisplayLabel = 'Valores|Custo Unit.'
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemFormulanCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemFormulanCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemFormulanCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemFormulacSiglaUnidadeMedida: TStringField
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
    object qryItemFormulanCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemFormulanValUnitario: TBCDField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemFormulanValDesconto: TBCDField
      DisplayLabel = 'Valores|Desconto'
      FieldName = 'nValDesconto'
      Precision = 12
    end
    object qryItemFormulanValAcrescimo: TBCDField
      DisplayLabel = 'Valores|Acr'#233'scimo'
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      EditFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemFormulanValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 12
    end
    object qryItemFormulanValDescAbatUnit: TBCDField
      DisplayLabel = 'Valores|Abatimento'
      FieldName = 'nValDescAbatUnit'
      DisplayFormat = '#,##0.0000'
      Precision = 12
    end
  end
  object dsItemFormula: TDataSource
    DataSet = qryItemFormula
    Left = 504
    Top = 472
  end
  object qryProdutoFormulado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto,cNmProduto'
      'FROM Produto'
      'WHERE nCdTabTipoProduto = 4'
      'AND nCdProduto = :nPK')
    Left = 816
    Top = 208
    object qryProdutoFormuladonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoFormuladocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object usp_gera_item_embalagem: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_SUBITEM_GRADE_EMBALAGEM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 672
    Top = 136
  end
  object usp_valida_credito: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_VALIDA_CREDITO_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 672
    Top = 168
  end
  object sp_tab_preco: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SUGERE_PRECO_TABELA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTabPrecoAux'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValor'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValDesconto'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValAcrescimo'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 768
    Top = 136
  end
  object qryGrupoImposto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoImposto, cNmGrupoImposto'
      'FROM GrupoImposto'
      'WHERE nCdGrupoImposto = :nPK')
    Left = 1007
    Top = 241
    object qryGrupoImpostonCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryGrupoImpostocNmGrupoImposto: TStringField
      FieldName = 'cNmGrupoImposto'
      Size = 35
    end
  end
  object qryDescrTecnica: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdItemPedido, nCdProduto, cNmItem, nQtdePed, cDescricaoT' +
        'ecnica'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK'
      'AND nCdTipoItemPed IN (1,2,3,5,6,9,10,11)')
    Left = 664
    Top = 443
    object qryDescrTecnicanCdItemPedido: TAutoIncField
      DisplayLabel = 'Itens|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryDescrTecnicanCdProduto: TIntegerField
      DisplayLabel = 'Itens|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryDescrTecnicacNmItem: TStringField
      DisplayLabel = 'Itens|Descri'#231#227'o '
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryDescrTecnicanQtdePed: TBCDField
      DisplayLabel = 'Itens|Qtde'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryDescrTecnicacDescricaoTecnica: TMemoField
      DisplayLabel = 'Itens|'
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
  end
  object DataSource14: TDataSource
    DataSet = qryDescrTecnica
    Left = 664
    Top = 472
  end
  object SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_EMPENHA_ESTOQUE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 736
    Top = 168
  end
  object PopupMenu1: TPopupMenu
    Left = 612
    Top = 376
    object ExibirAtendimentosdoItem1: TMenuItem
      Caption = 'Exibir Atendimentos do Item'
      OnClick = ExibirAtendimentosdoItem1Click
    end
  end
  object qryTerceiroRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 4)')
    Left = 596
    Top = 440
    object qryTerceiroRepresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroReprescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource15: TDataSource
    DataSet = qryTerceiroRepres
    Left = 600
    Top = 472
  end
  object qryPrecoAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nValorInicial'
        DataType = ftFloat
        Size = 5
        Value = 1000.000000000000000000
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa     int'
      '       ,@nCdTerceiro    int'
      '       ,@nCdProduto     int'
      '       ,@nCdTabPrecoAux int'
      '       ,@nValor         decimal(12,2)'
      '       ,@nPercent       decimal(12,2)'
      '       ,@nValorInicial  decimal(12,2)'
      ''
      'Set @nCdEmpresa    = :nCdEmpresa'
      'Set @nCdTerceiro   = :nCdTerceiro'
      'Set @nCdProduto    = :nCdProduto'
      'Set @nValorInicial = :nValorInicial'
      'Set @nValor        = 0'
      'Set @nPercent      = 0'
      ''
      'Set @nCdTabPrecoAux = NULL'
      ''
      'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      '      ,@nValor         = nValor'
      '      ,@nPercent       = nPercent'
      '  FROM TabPrecoAux'
      ' WHERE nCdEmpresa  = @nCdEmpresa'
      '   AND nCdProduto  = @nCdProduto'
      '   AND nCdTerceiro = @nCdTerceiro'
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      #9'   AND nCdProduto  = @nCdProduto'
      #9'   AND nCdTerceiro IS NULL'
      ''
      'END'
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      ''
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro IS NULL'
      ''
      'END'
      ''
      'IF (@nPercent > 0)'
      'BEGIN'
      '    Set @nValor = ROUND(@nValorInicial * (@nPercent /100),2)'
      'END'
      ''
      '/*IF (@nValor > @nValorInicial) Set @nValor = @nValorInicial*/ '
      ''
      'SELECT @nValor nPrecoAux')
    Left = 816
    Top = 176
    object qryPrecoAuxnPrecoAux: TBCDField
      FieldName = 'nPrecoAux'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object qryContatoPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      ''
      'IF EXISTS(SELECT 1 '
      '             FROM ContatoTerceiro'
      '            WHERE nCdTerceiro = @nCdTerceiro'
      '              AND cFlgContatoPadrao = 1)'
      'BEGIN'
      
        #9'SELECT SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1 cNmConta' +
        'to'
      #9'  FROM ContatoTerceiro'
      '     WHERE nCdTerceiro       = @nCdTerceiro'
      '       AND cFlgContatoPadrao = 1'
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    IF EXISTS(SELECT cNmContato'
      '                FROM Terceiro'
      '               WHERE nCdTerceiro = @nCdTerceiro'
      '                 AND cNmContato IS NOT NULL)'
      '    BEGIN'
      
        #9#9'SELECT SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1 cNmCont' +
        'ato'
      #9#9'  FROM Terceiro'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      '    END'
      '    ELSE'
      '    BEGIN'
      
        #9#9'SELECT TOP 1 SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1  ' +
        'cNmContato'
      #9#9'  FROM ContatoTerceiro'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      '     ORDER BY cNmContato'
      '    END'
      ''
      'END')
    Left = 1044
    Top = 176
    object qryContatoPadraocNmContato: TStringField
      FieldName = 'cNmContato'
      ReadOnly = True
      Size = 38
    end
  end
  object qryEnderecoEntrega: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEndereco'
      '      ,CASE WHEN ncdStatus = 1 THEN '#39'Ativo'#39' '
      '            ELSE '#39'Inativo'#39' '
      '       END as cNmstatus'
      '      ,cNmTipoEnd'
      
        '      ,RTRIM(Endereco.cEndereco) + '#39','#39' + Convert(VARCHAR(10),End' +
        'ereco.iNumero) + '#39'  -  '#39' + RTRIM(Endereco.cBairro) + '#39'  -  '#39' + R' +
        'TRIM(Endereco.cCidade) + '#39'/'#39' + Endereco.cUF + '#39'  -  '#39' + Endereco' +
        '.cCep as cEnderecoCompleto'
      '  FROM Endereco'
      
        '       INNER JOIN TipoEnd ON TipoEnd.nCdTipoEnd = Endereco.nCdTi' +
        'poEnd'
      ' WHERE nCdTerceiro = :nCdTerceiro'
      '   AND nCdEndereco = :nPK'
      ' ORDER BY nCdStatus'
      '         ,cNmTipoEnd')
    Left = 472
    Top = 440
    object qryEnderecoEntreganCdEndereco: TAutoIncField
      FieldName = 'nCdEndereco'
      ReadOnly = True
    end
    object qryEnderecoEntregacNmstatus: TStringField
      FieldName = 'cNmstatus'
      ReadOnly = True
      Size = 7
    end
    object qryEnderecoEntregacNmTipoEnd: TStringField
      FieldName = 'cNmTipoEnd'
      Size = 50
    end
    object qryEnderecoEntregacEnderecoCompleto: TStringField
      FieldName = 'cEnderecoCompleto'
      ReadOnly = True
      Size = 157
    end
  end
  object DataSource16: TDataSource
    DataSet = qryEnderecoEntrega
    Left = 472
    Top = 472
  end
  object qrySugereEnderecoEntrega: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@nCdEndereco int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdEndereco = NULL'
      ''
      'IF EXISTS(SELECT 1 '
      '            FROM Endereco'
      '           WHERE nCdTerceiro = @nCdTerceiro'
      '             AND nCdTipoEnd  = 3'
      '             AND nCdStatus   = 1)'
      'BEGIN'
      ''
      '    SELECT TOP 1 @nCdEndereco = nCdEndereco'
      '      FROM Endereco'
      '     WHERE nCdTerceiro = @nCdTerceiro'
      '       AND nCdTipoEnd  = 3'
      '       AND nCdStatus   = 1'
      '     ORDER BY nCdEndereco'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    SELECT TOP 1 @nCdEndereco = nCdEndereco'
      '      FROM Endereco'
      '     WHERE nCdTerceiro = @nCdTerceiro'
      '       AND nCdTipoEnd  = 1'
      '       AND nCdStatus   = 1'
      '     ORDER BY nCdEndereco'
      ''
      'END'
      ''
      'SELECT @nCdEndereco as nCdEndereco')
    Left = 1012
    Top = 208
    object qrySugereEnderecoEntreganCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
      ReadOnly = True
    end
  end
  object uspBuscaProduto: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_BUSCAPRODUTO_ITEMPEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = -6
      end
      item
        Name = '@cEAN'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = 'null'
      end>
    Left = 608
    Top = 167
    object uspBuscaProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object uspBuscaProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT nCdTerceiroResponsavel'
      '              ,cFlgRepresentante'
      '    FROM Usuario'
      ' WHERE nCdUsuario = :nPk')
    Left = 848
    Top = 240
    object qryUsuarionCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
    object qryUsuariocFlgRepresentante: TIntegerField
      FieldName = 'cFlgRepresentante'
    end
  end
  object qryAreaVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdAreaVenda'
      '      ,cNmAreaVenda'
      '  FROM AreaVenda'
      ' WHERE nCdAreaVenda = :nPK'
      '   AND nCdStatus    = 1 ')
    Left = 344
    Top = 440
    object qryAreaVendanCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryAreaVendacNmAreaVenda: TStringField
      FieldName = 'cNmAreaVenda'
      Size = 50
    end
  end
  object qryDivisaoVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdAreaVenda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDivisaoVenda'
      '      ,cNmDivisaoVenda'
      '  FROM DivisaoVenda'
      ' WHERE nCdDivisaoVenda = :nPK'
      '   AND nCdAreaVenda    = :nCdAreaVenda'
      '   AND nCdStatus       = 1 ')
    Left = 376
    Top = 440
    object qryDivisaoVendanCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryDivisaoVendacNmDivisaoVenda: TStringField
      FieldName = 'cNmDivisaoVenda'
      Size = 50
    end
  end
  object qryCanalDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCanalDistribuicao'
      '      ,cNmCanalDistribuicao'
      '  FROM CanalDistribuicao'
      ' WHERE nCdCanalDistribuicao = :nPK'
      '   AND nCdStatus            = 1')
    Left = 408
    Top = 440
    object qryCanalDistribuicaonCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
    object qryCanalDistribuicaocNmCanalDistribuicao: TStringField
      FieldName = 'cNmCanalDistribuicao'
      Size = 50
    end
  end
  object dsCanalDistribuicao: TDataSource
    DataSet = qryCanalDistribuicao
    Left = 408
    Top = 472
  end
  object dsDivisaoVenda: TDataSource
    DataSet = qryDivisaoVenda
    Left = 376
    Top = 472
  end
  object dsAreaVenda: TDataSource
    DataSet = qryAreaVenda
    Left = 344
    Top = 472
  end
  object qryTerceiroAreaVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdAreaVenda'
      '      ,nCdDivisaoVenda'
      '      ,nCdCanalDistribuicao'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK')
    Left = 880
    Top = 240
    object qryTerceiroAreaVendanCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryTerceiroAreaVendanCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryTerceiroAreaVendanCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
  end
  object qryItensPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdItemPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT TOP 1 1 as iAux'
      '  FROM ItemPedido'
      ' WHERE nCdProduto     = :nCdProduto'
      '   AND nCdPedido      = :nCdPedido'
      '   AND nCdItemPedido <> :nCdItemPedido')
    Left = 816
    Top = 240
    object qryItensPedidoiAux: TIntegerField
      FieldName = 'iAux'
      ReadOnly = True
    end
  end
  object qryRestricoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRestricaoVendaTerceiro'
      '      ,TTRV.cNmTabTipoRestricaoVenda'
      '      ,nCdPedido'
      '      ,(CASE WHEN cFlgDesbloqueado = 0 '
      '             THEN '#39'SIM'#39
      '             ELSE NULL'
      '        END) cPendenteLiberacao'
      'FROM RestricaoVendaTerceiro'
      
        '     INNER JOIN TabTipoRestricaoVenda TTRV ON TTRV.nCdTabTipoRes' +
        'tricaoVenda = RestricaoVendaTerceiro.nCdTabTipoRestricaoVenda'
      'WHERE nCdPedido = :nPK')
    Left = 960
    Top = 416
    object qryRestricoesnCdRestricaoVendaTerceiro: TIntegerField
      FieldName = 'nCdRestricaoVendaTerceiro'
    end
    object qryRestricoescNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
    object qryRestricoesnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryRestricoescPendenteLiberacao: TStringField
      FieldName = 'cPendenteLiberacao'
      ReadOnly = True
      Size = 3
    end
  end
  object dsRestricoes: TDataSource
    DataSet = qryRestricoes
    Left = 960
    Top = 448
  end
  object qryGrupoCredito: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoCredito'
      '      ,cNmGrupoCredito'
      '      ,cFlgPedidosAnaliseFin'
      '  FROM GrupoCredito'
      ' WHERE nCdGrupoCredito = :nPK ')
    Left = 440
    Top = 440
    object qryGrupoCreditonCdGrupoCredito: TIntegerField
      FieldName = 'nCdGrupoCredito'
    end
    object qryGrupoCreditocNmGrupoCredito: TStringField
      FieldName = 'cNmGrupoCredito'
      Size = 50
    end
    object qryGrupoCreditocFlgPedidosAnaliseFin: TIntegerField
      FieldName = 'cFlgPedidosAnaliseFin'
    end
  end
  object DataSource17: TDataSource
    DataSet = qryGrupoCredito
    Left = 440
    Top = 472
  end
  object qryTituloAtraso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM Titulo '
      
        '       INNER JOIN Terceiro     ON Terceiro.nCdTerceiro         =' +
        ' Titulo.nCdTerceiro'
      
        '       LEFT  JOIN GrupoCredito ON GrupoCredito.nCdGrupoCredito =' +
        ' Terceiro.nCdGrupoCredito'
      
        ' WHERE (dbo.fn_OnlyDate(GetDate()) - dDtVenc) > GrupoCredito.iDi' +
        'asToleraAtrasoTit                    '
      '   AND Titulo.nCdTerceiro = :nCdTerceiro'
      '   AND Titulo.dDtCancel IS Null'
      '   AND Titulo.nSaldoTit > 0'
      '   AND GrupoCredito.cFlgAlertaTitAtrasado = 1')
    Left = 880
    Top = 208
  end
  object qryChequeRisco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nValChequeRisco decimal(12,2)'
      '       ,@nCdTerceiro     int'
      ''
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      'SELECT @nValChequeRisco = isNull(Sum(nValCheque) ,0)'
      '  FROM Cheque (NOLOCK)'
      ' WHERE Cheque.nCdTerceiroResp     = @nCdTerceiro'
      
        '   AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerceiroP' +
        'ort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(GetDa' +
        'te())+14)))'
      '   AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)'
      '   AND EXISTS(SELECT 1'
      #9#9#9#9'FROM TerceiroCPFRisco TCR'
      #9#9#9'   WHERE TCR.nCdTerceiro = Cheque.nCdTerceiroResp'
      #9#9#9'     AND TCR.cCNPJCPF    = Cheque.cCNPJCPF)'
      ''
      ''
      'SELECT TOP 1 1'
      '  FROM GrupoCredito'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdGrupoCredito = GrupoCr' +
        'edito.nCdGrupoCredito'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      '   AND (@nValChequeRisco - nValLimiteChequeRisco) > 0'
      '   AND cFlgAlertaChequeRisco = 1')
    Left = 848
    Top = 176
  end
  object qrySaldoLimite: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      ''
      
        'SELECT (Dados.nValLimiteCred - Dados.nValPendenciaFinanceiraRece' +
        'ber - Dados.nValPedidoVendaAberto - Dados.nValChequePendente) as' +
        ' nSaldoLimite'
      
        '  FROM (SELECT (nValLimiteCred + Convert(decimal(12,2),(nValLimi' +
        'teCred * GrupoCredito.nPercLimiteExcesso)/100)) as nValLimiteCre' +
        'd'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      '                          AND Titulo.cSenso       = '#39'C'#39
      
        '                          AND Titulo.nCdEspTit   <> Convert(int,' +
        'dbo.fn_LeParametro('#39'ET-CDR'#39'))'
      
        #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro),0) as nV' +
        'alPendenciaFinanceiraReceber'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      '                          AND Titulo.cSenso       = '#39'D'#39
      
        '                          AND Titulo.nCdEspTit   <> Convert(int,' +
        'dbo.fn_LeParametro('#39'ET-CDP'#39'))'
      
        #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro),0) as nV' +
        'alPendenciaFinanceiraPagar'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro'
      '              AND Titulo.cSenso       = '#39'C'#39
      
        #9#9#9#9#9#9'  AND Titulo.dDtVenc      < dbo.fn_OnlyDate(GetDate())),0)' +
        ' as nValPendenciaFinanceiraAtrasado'
      
        #9#9#9'  ,IsNull((SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9#9#9#9#9#9' FROM Pedido (NOLOCK)'
      
        #9#9#9#9#9#9#9'  INNER JOIN TipoPedido (NOLOCK) ON TipoPedido.nCdTipoPed' +
        'ido = Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9'  INNER JOIN ItemPedido (NOLOCK) ON ItemPedido.nCdPedido ' +
        '    = Pedido.nCdPedido'
      #9#9#9#9#9#9'WHERE Pedido.nCdTerceiro         = Terceiro.nCdTerceiro'
      #9#9#9#9#9#9'  AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      #9#9#9#9#9#9'  AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)'
      #9#9#9#9#9#9'  AND TipoPedido.cGerarFinanc    = 1'
      '                          AND TipoPedido.cFlgVenda       = 1'
      
        '                          AND (ItemPedido.nQtdePed - ItemPedido.' +
        'nQtdeExpRec - ItemPedido.nQtdeCanc) > 0),0) as nValPedidoVendaAb' +
        'erto '
      
        #9#9#9'  ,IsNull((SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9#9#9#9#9#9' FROM Pedido (NOLOCK)'
      
        #9#9#9#9#9#9#9'  INNER JOIN TipoPedido (NOLOCK) ON TipoPedido.nCdTipoPed' +
        'ido = Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9'  INNER JOIN ItemPedido (NOLOCK) ON ItemPedido.nCdPedido ' +
        '    = Pedido.nCdPedido'
      #9#9#9#9#9#9'WHERE Pedido.nCdTerceiro         = Terceiro.nCdTerceiro'
      #9#9#9#9#9#9'  AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      #9#9#9#9#9#9'  AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)'
      #9#9#9#9#9#9'  AND TipoPedido.cGerarFinanc    = 1'
      '                          AND TipoPedido.cFlgCompra      = 1'
      
        '                          AND (ItemPedido.nQtdePed - ItemPedido.' +
        'nQtdeExpRec - ItemPedido.nQtdeCanc) > 0),0) as nValPedidoCompraA' +
        'berto '
      #9#9#9'  ,IsNull((SELECT Sum(nValCheque) '
      #9#9#9#9#9#9' FROM Cheque (NOLOCK)'
      #9#9#9#9#9#9'WHERE Cheque.nCdTerceiroResp     = Terceiro.nCdTerceiro '
      
        #9#9#9#9#9#9'  AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerc' +
        'eiroPort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(' +
        'GetDate())+14)))'
      
        #9#9#9#9#9#9'  AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)),0) ' +
        'as nValChequePendente '
      #9#9#9'  ,IsNull((SELECT Sum(nValCheque) '
      #9#9#9#9#9#9' FROM Cheque (NOLOCK)'
      #9#9#9#9#9#9'WHERE Cheque.nCdTerceiroResp     = Terceiro.nCdTerceiro '
      
        #9#9#9#9#9#9'  AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerc' +
        'eiroPort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(' +
        'GetDate())+14)))'
      #9#9#9#9#9#9'  AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)'
      #9#9#9#9#9#9'  AND EXISTS(SELECT 1'
      #9#9#9#9#9#9#9#9#9'   FROM TerceiroCPFRisco TCR'
      #9#9#9#9#9#9#9#9#9'  WHERE TCR.nCdTerceiro = Terceiro.nCdTerceiro'
      
        #9#9#9#9#9#9#9#9#9#9'AND TCR.cCNPJCPF    = Cheque.cCNPJCPF)),0) as nValCheq' +
        'ueRisco        '
      #9#9'  FROM Terceiro (NOLOCK)'
      
        '               LEFT JOIN GrupoCredito (NOLOCK) ON GrupoCredito.n' +
        'CdGrupoCredito = Terceiro.nCdGrupoCredito'
      '         WHERE Terceiro.nCdTerceiro = @nCdTerceiro'
      '           AND GrupoCredito.cFlgAlertaLimiteCredito = 1) Dados')
    Left = 848
    Top = 208
    object qrySaldoLimitenSaldoLimite: TBCDField
      FieldName = 'nSaldoLimite'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object SP_GERA_DIVERG_PED: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_DIVERG_PED;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuarioAutor'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 640
    Top = 136
  end
  object qryChequeDevolv: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      'SELECT TOP 1 1'
      '      FROM Titulo '
      
        '           INNER JOIN Terceiro     ON Terceiro.nCdTerceiro      ' +
        '   = Titulo.nCdTerceiro'
      
        '           INNER JOIN GrupoCredito ON GrupoCredito.nCdGrupoCredi' +
        'to = Terceiro.nCdGrupoCredito'
      '     WHERE Titulo.nCdTerceiro = @nCdTerceiro'
      '       AND nCdEspTit = Convert(int,dbo.fn_LeParametro('#39'ET-CDR'#39'))'
      '       AND nSaldoTit > 0 '
      '       AND dDtCancel IS NULL'
      '       AND GrupoCredito.cFlgAlertaChequeDevolv = 1'
      '       AND EXISTS (SELECT TOP 1 1'
      '                     FROM Cheque '
      
        '                    WHERE Cheque.iNrCheque           = Titulo.cN' +
        'rTit'
      
        '                      AND Cheque.nCdTerceiroResp     = Titulo.nC' +
        'dTerceiro'
      '                      AND Cheque.nCdTabStatusCheque IN (5,6))')
    Left = 879
    Top = 176
  end
  object PopupMenu2: TPopupMenu
    OnPopup = PopupMenu2Popup
    Left = 576
    Top = 376
    object DuplicarPedido1: TMenuItem
      Caption = 'Duplicar Pedido'
      ImageIndex = 11
      OnClick = DuplicarPedido1Click
    end
    object CancelarPedido1: TMenuItem
      Caption = 'Cancelar Pedido'
      ImageIndex = 10
      OnClick = CancelarPedido1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object btTerceiro: TMenuItem
      Caption = 'Terceiro'
      ImageIndex = 0
      OnClick = btTerceiroClick
    end
    object btTipodePedido: TMenuItem
      Caption = 'Tipo de Pedido'
      ImageIndex = 0
      OnClick = btTipodePedidoClick
    end
    object btCondPagto: TMenuItem
      Caption = 'Condi'#231#227'o de Pagamento'
      ImageIndex = 0
      OnClick = btCondPagtoClick
    end
    object btTabelaPreco: TMenuItem
      Caption = 'Tabela de Pre'#231'o'
      ImageIndex = 0
      OnClick = btTabelaPrecoClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object btProdGrade: TMenuItem
      Caption = 'Produto Grade'
      OnClick = btProdGradeClick
    end
    object btProdERP: TMenuItem
      Caption = 'Produto ERP'
      ImageIndex = 0
      OnClick = btProdERPClick
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object btEmitirPicking: TMenuItem
      Caption = 'Gerar Picking List'
      ImageIndex = 9
      OnClick = btEmitirPickingClick
    end
    object btSeparaoEmb: TMenuItem
      Caption = 'Separa'#231#227'o / Embalagem'
      ImageIndex = 13
      OnClick = btSeparaoEmbClick
    end
  end
  object SP_DUPLICA_PEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_DUPLICA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedidoDuplicar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdOutput
        Precision = 10
        Value = Null
      end>
    Left = 576
    Top = 136
  end
  object qryPedidoPromotor: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPedidoPromotorBeforePost
    OnCalcFields = qryPedidoPromotorCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM PedidoPromotor'
      'WHERE nCdPedido = :nPK')
    Left = 280
    Top = 444
    object qryPedidoPromotornCdPedidoPromotor: TIntegerField
      FieldName = 'nCdPedidoPromotor'
    end
    object qryPedidoPromotornCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPedidoPromotornCdTerceiroPromotor: TIntegerField
      DisplayLabel = 'Promotores do Pedido|C'#243'd.'
      FieldName = 'nCdTerceiroPromotor'
      OnChange = qryPedidoPromotornCdTerceiroPromotorChange
    end
    object qryPedidoPromotorcNmTerceiro: TStringField
      DisplayLabel = 'Promotores do Pedido|Nome Terceiro'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
  end
  object dsPedidoPromotor: TDataSource
    DataSet = qryPedidoPromotor
    Left = 276
    Top = 476
  end
  object qryTerceiroPromotor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 7)')
    Left = 856
    Top = 140
    object qryTerceiroPromotorcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryExcluirItens: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdPedido'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido int'
      ''
      'Set @nCdPedido = :nCdPedido'
      ''
      'DELETE'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      'UPDATE Pedido'
      '   Set nValProdutos  = 0'
      '      ,nValServicos  = 0'
      '      ,nValImposto   = 0'
      '      ,nValDesconto  = 0'
      '      ,nValAcrescimo = 0'
      '      ,nValFrete     = 0'
      '      ,nValPedido    = 0'
      '      ,nSaldoFat     = 0'
      ' WHERE nCdPedido     = @nCdPedido'
      '')
    Left = 912
    Top = 240
  end
  object qrySumTotalPed: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT isNull(SUM(nQtdePed * nValDesconto),0) AS nValTotalDescPe' +
        'd'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK')
    Left = 1008
    Top = 176
    object qrySumTotalPednValTotalDescPed: TBCDField
      FieldName = 'nValTotalDescPed'
      ReadOnly = True
      Precision = 32
      Size = 10
    end
  end
  object qryAplicaDesconto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nValLiquido'
        DataType = ftString
        Size = 3
        Value = '0'
      end
      item
        Name = 'nCdPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nPercDesc     decimal(14,6)'
      '       ,@nValLiquido   decimal(12,2)'
      '       ,@nValDiferenca decimal(14,12)'
      '       ,@nValDesconto  decimal(12,2)'
      '       ,@nCdItemPedido int'
      '       ,@nCdPedido     int'
      ''
      'SET @nValLiquido  = Replace( :nValLiquido,'#39','#39','#39'.'#39')'
      'SET @nCdPedido    = :nCdPedido'
      ''
      
        'SELECT @nPercDesc = (((@nValLiquido * 100)/ sum(nQtdePed * nValU' +
        'nitario))/100)'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      'UPDATE ItemPedido'
      '   SET nValCustoUnit = nValUnitario * @nPercDesc'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      'UPDATE ItemPedido'
      '   SET nValTotalItem = nQtdePed * nValCustoUnit'
      '      ,nValDesconto  = nValUnitario - nValCustoUnit'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      
        'SELECT @nValDiferenca = dbo.fn_ArredondaMatematico((SUM(nValTota' +
        'lItem) - @nValLiquido),2)/MIN(nQtdePed)'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      'IF( @nValDiferenca <> 0)'
      'BEGIN'
      ''
      '    SELECT @nCdItemPedido = nCdItemPedido'
      '      FROM ItemPedido'
      '     WHERE nCdPedido = @nCdPedido'
      '     GROUP BY nCdItemPedido'
      ''
      '             ,nQtdePed'
      '    HAVING nQtdePed = MIN(nQtdePed)'
      ''
      '    UPDATE ItemPedido'
      '       SET nValCustoUnit = nValCustoUnit - @nValDiferenca'
      '     WHERE nCdPedido = @nCdPedido'
      '       AND nCdItemPedido = @nCdItemPedido'
      ''
      '    UPDATE ItemPedido'
      
        '       SET nValTotalItem = nValTotalItem - (nQtdePed * @nValDife' +
        'renca)'
      '     WHERE nCdPedido = @nCdPedido'
      '       AND nCdItemPedido = @nCdItemPedido'
      ''
      'END'
      ''
      'UPDATE Pedido'
      '   SET nValPedido = (SELECT sum(nValTotalItem)'
      '                         FROM ItemPedido'
      '                        WHERE nCdPedido = @nCdPedido)'
      
        '      ,nValDesconto = (SELECT dbo.fn_ArredondaMatematico(sum(nQt' +
        'dePed * nValDesconto),2)'
      '                         FROM ItemPedido'
      '                        WHERE nCdPedido = @nCdPedido)'
      
        '      ,nPercDesconto = dbo.fn_ArredondaMatematico(@nPercDesc,2) ' +
        '* 100'
      ' WHERE nCdPedido    = @nCdPedido')
    Left = 976
    Top = 176
  end
  object qryTerceiroRepresentante: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nCdTerceiroRepres'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiroRepres'
      '  FROM TerceiroRepresentante '
      ' WHERE nCdTerceiroRepres = :nCdTerceiroRepres'
      '   AND nCdTerceiro       = :nCdTerceiro')
    Left = 820
    Top = 140
    object qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
  end
  object qryVerificaQtdRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT COUNT(1) as iQtdRepres'
      '  FROM TerceiroRepresentante'
      ' WHERE nCdTerceiro = :nPK')
    Left = 892
    Top = 140
    object qryVerificaQtdRepresiQtdRepres: TIntegerField
      FieldName = 'iQtdRepres'
      ReadOnly = True
    end
  end
  object qryTerceiroRepresUnico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = (SELECT nCdTerceiroRepres'
      '                        FROM TerceiroRepresentante TR'
      
        '                       WHERE TR.nCdTerceiroRepres = Terceiro.nCd' +
        'Terceiro'
      '                         AND TR.nCdTerceiro       = :nPK)')
    Left = 924
    Top = 140
    object qryTerceiroRepresUniconCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroRepresUnicocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object qryInseriItemGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iQtde'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      ''
      'DECLARE @nCdItemPedido      int'
      '       ,@nCdProdutoDetalhe  int'
      '       ,@iQtde              int'
      '       ,@cNmProduto         varchar(150)'
      '       ,@nCdPedido          int'
      '       ,@iID                int'
      ''
      'Set @nCdItemPedido      = :nCdItemPedido'
      'Set @nCdProdutoDetalhe  = :nCdProduto'
      'Set @iQtde              = :iQtde'
      ''
      'SELECT @nCdPedido = nCdPedido'
      '  FROM ItemPedido'
      ' WHERE nCdItemPedido = @nCdItemPedido'
      ' '
      'IF EXISTS(SELECT 1'
      #9#9#9#9#9#9'FROM ItemPedido'
      #9#9#9#9'   WHERE nCdItemPedidoPai = @nCdItemPedido'
      #9#9#9#9#9#9' AND nCdTipoItemPed   = 4'
      #9#9#9#9#9#9' AND nCdProduto       = @nCdProdutoDetalhe)'
      'BEGIN'
      ''
      '    UPDATE ItemPedido'
      #9#9'   SET nQtdePed         = @iQtde'
      #9#9' WHERE nCdItemPedidoPai = @nCdItemPedido'
      #9#9'   AND nCdTipoItemPed   = 4'
      #9#9'   AND nCdProduto       = @nCdProdutoDetalhe'
      ''
      'END'
      'ELSE BEGIN'
      ''
      '    SELECT @cNmProduto = cNmProduto'
      '      FROM Produto'
      '     WHERE nCdProduto = @nCdProdutoDetalhe'
      ''
      '    Set @iID = NULL'
      ''
      '    EXEC usp_ProximoID  '#39'ITEMPEDIDO'#39
      '                       ,@iID OUTPUT'
      ''
      '    INSERT INTO ItemPedido (nCdItemPedido'
      '                           ,nCdPedido'
      #9#9'      '#9#9#9#9#9#9#9'   ,nCdItemPedidoPai'
      #9#9#9#9#9'      '#9#9#9#9'   ,nCdProduto'
      '                           ,cNmItem'
      #9#9#9#9#9#9#9#9'           ,nCdTipoItemPed'
      '       '#9#9#9#9#9#9#9#9#9'   ,nQtdePed)'
      #9#9#9'    '#9#9#9#9#9' VALUES(@iID'
      '                           ,@nCdPedido'
      #9#9#9#9#9'      '#9#9#9#9'   ,@nCdItemPedido'
      #9#9#9#9#9#9#9#9'       '#9'   ,@nCdProdutoDetalhe'
      '                           ,@cNmProduto'
      #9#9#9#9#9#9#9#9#9'         ,4   --SubItem'
      #9#9#9#9#9#9#9#9#9'         ,@iQtde)'
      ''
      'END'
      ''
      '')
    Left = 960
    Top = 141
  end
  object dsTabTipoModFrete: TDataSource
    DataSet = qryTabTipoModFrete
    Left = 968
    Top = 376
  end
  object qryTabTipoModFrete: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TabTipoModFrete'
      ' WHERE nCdTabTipoModFrete = :nPK'
      '')
    Left = 967
    Top = 344
    object qryTabTipoModFretenCdTabTipoModFrete: TIntegerField
      FieldName = 'nCdTabTipoModFrete'
    end
    object qryTabTipoModFretecNmTabTipoModFrete: TStringField
      FieldName = 'cNmTabTipoModFrete'
      Size = 50
    end
  end
  object qryVerificaTransfEst: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK'
      '   AND nCdItemTransfEst IS NOT NULL')
    Left = 996
    Top = 140
  end
end
