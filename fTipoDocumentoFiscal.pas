unit fTipoDocumentoFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoDocumentoFiscal = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMastercNmTipoDoctoFiscal: TStringField;
    qryMastercFlgAceitaComplemento: TIntegerField;
    DBCheckBox13: TDBCheckBox;
    qryMastercSiglaLivroFiscal: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoDocumentoFiscal: TfrmTipoDocumentoFiscal;

implementation

{$R *.dfm}

procedure TfrmTipoDocumentoFiscal.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus ;
end;

procedure TfrmTipoDocumentoFiscal.FormCreate(Sender: TObject);
begin
  cNmTabelaMaster   := 'TIPODOCTOFISCAL' ;
  nCdTabelaSistema  := 311 ;
  nCdConsultaPadrao := 77 ;
  bCodigoAutomatico := False;
  inherited;

end;
procedure TfrmTipoDocumentoFiscal.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdTipoDoctoFiscal.Value = 0) then
  begin
      MensagemAlerta('Informe o c�digo do documento fiscal.') ;
      DBEdit1.SetFocus;
      abort ;
  end ;

  if (trim(qryMastercNmTipoDoctoFiscal.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do documento fiscal.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (trim(qryMastercSiglaLivroFiscal.Value) = '') then
  begin
      MensagemAlerta('Informe a sigla do documento fiscal.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(tFrmTipoDocumentoFiscal) ;

end.
