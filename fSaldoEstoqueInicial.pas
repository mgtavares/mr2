unit fSaldoEstoqueInicial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, GridsEh, DBGridEh, cxLookAndFeelPainters,
  cxButtons;

type
  TfrmSaldoEstoqueInicial = class(TfrmProcesso_Padrao)
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    DataSource2: TDataSource;
    DBGridEh1: TDBGridEh;
    qryProdutos: TADOQuery;
    qryProdutosnCdProduto: TIntegerField;
    qryProdutosnQtde: TBCDField;
    qryProdutosnValCusto: TBCDField;
    dsProdutos: TDataSource;
    qryProdutoscNmProduto: TStringField;
    qryAux: TADOQuery;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    SP_SALDO_INICIAL: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit1: TMaskEdit;
    DBEdit2: TDBEdit;
    cxButton1: TcxButton;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure qryProdutosCalcFields(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryProdutosBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSaldoEstoqueInicial: TfrmSaldoEstoqueInicial;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmSaldoEstoqueInicial.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  
  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  PosicionaQuery(qryLocalEstoque, MaskEdit3.Text) ;

end;

procedure TfrmSaldoEstoqueInicial.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

        If (nPK > 0) then
        begin
            Maskedit3.Text := IntToStr(nPK) ;
            qryLocalEstoque.Close ;
            qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

            PosicionaQuery(qryLocalEstoque, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmSaldoEstoqueInicial.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione um local de estoque.') ;
      MaskEdit3.SetFocus;
      exit ;
  end ;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, MaskEdit1.Text) ;

end;

procedure TfrmSaldoEstoqueInicial.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit1.Text = '') then
        begin
            MensagemAlerta('Selecione um local de estoque.') ;
            MaskEdit3.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta(129) ;

        If (nPK > 0) then
        begin
            Maskedit1.Text := IntToStr(nPK) ;
            qryDepartamento.Close ;
            PosicionaQuery(qryDepartamento, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmSaldoEstoqueInicial.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (DbEdit1.Text = '') then
  begin
     MensagemAlerta('Selecione um local de estoque.') ;
     MaskEdit3.SetFocus;
     exit ;
  end ;

  if (DbEdit2.Text = '') then
  begin
     MensagemAlerta('Selecione um departamento de produtos.') ;
     MaskEdit1.SetFocus;
     exit ;
  end ;

  cmdPreparaTemp.Execute;
  
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('TRUNCATE TABLE #Temp_Produtos') ;

  try
      qryAux.ExecSQL;
  except
  end ;

  qryProdutos.Close ;
  qryProdutos.Open ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('INSERT INTO #Temp_Produtos (nCdProduto) SELECT nCdProduto FROM Produto WHERE nCdStatus = 1 AND EXISTS (SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdGrupoProduto = nCdGrupo_Produto AND GPLE.nCdLocalEstoque = ' + MaskEdit3.Text + ' ) AND nCdDepartamento = ' + MaskEdit1.Text + ' AND NOT EXISTS(SELECT 1 FROM PosicaoEstoque WHERE PosicaoEstoque.nCdProduto = Produto.nCdProduto AND nCdLocalEstoque = ' + MaskEdit3.Text + ') AND NOT EXISTS(SELECT 1 FROM Produto Produto2 WHERE Produto2.nCdProdutoPai = Produto.nCdProduto) ORDER by cNmProduto') ;
  qryAux.ExecSQL ;

  qryProdutos.Close ;
  qryProdutos.Open ;

  DBGridEh1.SetFocus;

end;

procedure TfrmSaldoEstoqueInicial.qryProdutosCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, qryProdutosnCdProduto.AsString) ;

  if not qryProduto.Eof then
      qryProdutoscNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmSaldoEstoqueInicial.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if not qryProdutos.Active or (qryProdutos.RecordCount < 1) then
  begin
      MensagemAlerta('Nenhum produto selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o do saldo inicial dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_SALDO_INICIAL.Close ;
      SP_SALDO_INICIAL.Parameters.ParamByName('@nCdLocalEstoque').Value := frmMenu.ConvInteiro(Maskedit3.Text) ;
      SP_SALDO_INICIAL.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Estoques atualizados com sucesso.') ;

  qryProdutos.Close ;
  qryDepartamento.Close ;
  qryLocalEstoque.Close ;

  MaskEdit3.Text := '' ;
  MaskEdit1.Text := '' ;

end;

procedure TfrmSaldoEstoqueInicial.qryProdutosBeforePost(DataSet: TDataSet);
begin

  if (qryProdutosnQtde.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  if (qryProdutosnValCusto.Value < 0) then
  begin
      MensagemAlerta('Valor custo unit�rio inv�lido.') ;
      abort ;
  end ;

  if (qryProdutosnValCusto.Value = 0) and (qryProdutosnQtde.Value > 0) then
  begin
      MensagemAlerta('Informe o valor do custo unit�rio.') ;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(TfrmSaldoEstoqueInicial) ;
    
end.
