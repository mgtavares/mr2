unit fLibDivRec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxContainer, cxEdit, cxTextEdit, Menus, cxLookAndFeelPainters, cxButtons,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmLibDivRec = class(TfrmCadastro_Padrao)
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmLoja: TStringField;
    qryMasternCdTipoReceb: TIntegerField;
    qryMastercNmTipoReceb: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasterdDtReceb: TDateTimeField;
    qryItens: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    usp_processo: TADOStoredProc;
    qryItensnCdItemRecebimento: TAutoIncField;
    qryItenscNmTabTipoDiverg: TStringField;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtde: TBCDField;
    qryItensnValTotal: TBCDField;
    qryItensnPercIPI: TBCDField;
    qryItensnPercICMSSub: TBCDField;
    qryItensnCdPedido: TIntegerField;
    qryItensnQtdeSaldoPed: TBCDField;
    qryItensnPercIPIPed: TBCDField;
    qryItensnPercICMSSubPed: TBCDField;
    qryItensdDtPrevEntIni: TDateTimeField;
    qryItensdDtPrevEntFim: TDateTimeField;
    qryItensnCdTabTipoDiverg: TIntegerField;
    qryItensnCdHistAutorDiverg: TAutoIncField;
    qryItenscFlgAutorizar: TIntegerField;
    usp_Grade: TADOStoredProc;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    PopupMenu1: TPopupMenu;
    DBGridEh2: TDBGridEh;
    qryAux: TADOQuery;
    qryPreparaTemp: TADOQuery;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit12: TDBEdit;
    Label9: TLabel;
    Label11: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label16: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit13: TDBEdit;
    Image2: TImage;
    cxButton1: TcxButton;
    ExibirDivergncias1: TMenuItem;
    qryItensnValSugVenda: TBCDField;
    Label7: TLabel;
    DBEdit24: TDBEdit;
    qryItensnPercDescontoItem: TBCDField;
    qryItensnPercDesconto: TBCDField;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit27: TDBEdit;
    qryItemRecebimento: TADOQuery;
    qryItemRecebimentocFlgDescAbatUnitario: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    dsItemRecebimento: TDataSource;
    qryItensnValUnitario: TFloatField;
    qryItensnValCustoFinalUnitReceb: TFloatField;
    qryItensnValUnitarioEsp: TFloatField;
    qryItensnValDescontoPed: TFloatField;
    qryItensnValDesconto: TFloatField;
    qryItensnValCustoFinalUnitPed: TFloatField;
    qryItensnValAcrescimo: TFloatField;
    qryItensnValDescAbatUnit: TFloatField;
    qryItemRecebimentonCdItemRecebimento: TIntegerField;
    qryItemRecebimentonCdRecebimento: TIntegerField;
    qryItemRecebimentonCdItemRecebimentoPai: TIntegerField;
    qryItemRecebimentonCdTipoItemPed: TIntegerField;
    qryItemRecebimentonCdProduto: TIntegerField;
    qryItemRecebimentocNmItem: TStringField;
    qryItemRecebimentonCdItemPedido: TIntegerField;
    qryItemRecebimentonQtde: TBCDField;
    qryItemRecebimentonValUnitario: TBCDField;
    qryItemRecebimentonValTotal: TBCDField;
    qryItemRecebimentonValUnitarioEsp: TBCDField;
    qryItemRecebimentonValUnitarioPed: TBCDField;
    qryItemRecebimentocEANFornec: TStringField;
    qryItemRecebimentonCdTabStatusItemPed: TIntegerField;
    qryItemRecebimentocFlgDiverg: TIntegerField;
    qryItemRecebimentonCdTabTipoDiverg: TIntegerField;
    qryItemRecebimentonCdUsuarioAutorDiverg: TIntegerField;
    qryItemRecebimentodDtAutorDiverg: TDateTimeField;
    qryItemRecebimentonPercIPI: TBCDField;
    qryItemRecebimentonValIPI: TBCDField;
    qryItemRecebimentonPercICMSSub: TBCDField;
    qryItemRecebimentonValICMSSub: TBCDField;
    qryItemRecebimentonPercDesconto: TBCDField;
    qryItemRecebimentonValDesconto: TBCDField;
    qryItemRecebimentonValCustoFinal: TBCDField;
    qryItemRecebimentonPercIPIPed: TBCDField;
    qryItemRecebimentonPercICMSSubPed: TBCDField;
    qryItemRecebimentonValDescAbatUnit: TBCDField;
    qryItemRecebimentonCdServidorOrigem: TIntegerField;
    qryItemRecebimentodDtReplicacao: TDateTimeField;
    VincularPedido1: TMenuItem;
    SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO: TADOStoredProc;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    Label19: TLabel;
    Label15: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    qryItensnValImpostos: TFloatField;
    qryItensnValDespesas: TFloatField;
    qryItensnValDespesasPed: TIntegerField;
    qryItensnValImpostosPed: TFloatField;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    GroupBox3: TGroupBox;
    cxTextEdit2: TcxTextEdit;
    Label37: TLabel;
    btExibeRecebimento: TcxButton;
    qryMastercNrDocto: TStringField;
    Label20: TLabel;
    DBEdit30: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh2Exit(Sender: TObject);
    procedure GroupBox1Exit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ExibirDivergncias1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure vinculaPedidoitemRecebimento(nCdItemRecebimento:integer;nCdProduto:integer);
    procedure VincularPedido1Click(Sender: TObject);
    procedure btExibeRecebimentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLibDivRec: TfrmLibDivRec;

implementation

uses fMenu, fLibDivRec_PrecoVenda, fLibDivRec_Parcelas,
  fConsultaItemPedidoAberto, fRecebimentoCompleto;

{$R *.dfm}

procedure TfrmLibDivRec.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'RECEBIMENTO' ;
  nCdTabelaSistema  := 41 ;
  nCdConsultaPadrao := 93 ;
end;

procedure TfrmLibDivRec.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItens.Close ;
end;

procedure TfrmLibDivRec.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryItensnValUnitarioEsp.DisplayFormat         := frmMenu.cMascaraCompras;
  qryItensnValDescontoPed.DisplayFormat         := frmMenu.cMascaraCompras;
  qryItensnValDesconto.DisplayFormat            := frmMenu.cMascaraCompras;
  qryItensnValCustoFinalUnitPed.DisplayFormat   := frmMenu.cMascaraCompras;
  qryItensnValAcrescimo.DisplayFormat           := frmMenu.cMascaraCompras;
  qryItensnValDescAbatUnit.DisplayFormat        := frmMenu.cMascaraCompras;
  qryItensnValCustoFinalUnitReceb.DisplayFormat := frmMenu.cMascaraCompras;
  qryItensnValUnitario.DisplayFormat            := frmMenu.cMascaraCompras;

  btExibeRecebimento.Enabled := frmMenu.fnValidaUsuarioAPL('FRMRECEBIMENTOCOMPLETO') ;

end;

procedure TfrmLibDivRec.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryItens.Close;
  qryItens.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;
  qryItens.Parameters.ParamByName('nPK').Value         := qryMasternCdRecebimento.Value;
  qryItens.Open ;

  if (qryItens.Eof) then
  begin
      MensagemAlerta('Nenhuma diverg�ncia para este recebimento.') ;
      exit ;
  end ;

end;

procedure TfrmLibDivRec.DBGridEh1DblClick(Sender: TObject);
var
   cOBS : string ;
   objLibDivRec_PrecoVenda : TfrmLibDivRec_PrecoVenda ;
begin
  inherited;

  if not qryitens.Active then
      exit ;
      
  if (qryItenscFlgAutorizar.Value = 0) then
  begin
      MensagemAlerta('Voc� n�o pode autorizar esta diverg�ncia.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a libera��o deste item?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  objLibDivRec_PrecoVenda := TfrmLibDivRec_PrecoVenda.Create( Self ) ;

  objLibDivRec_PrecoVenda.edtPrecoVenda.Value := 0 ;

  if (qryItensnCdTabTipoDiverg.Value = 2) then
  begin

      if (qryItensnCdPedido.Value > 0) then
      begin
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT cFlgAtuPreco                                                              ');
          qryAux.SQL.Add('  FROM TipoPedido                                                                ');
          qryAux.SQL.Add(' WHERE nCdTipoPedido = (SELECT nCdTipoPedido                                     ');
          qryAux.SQL.Add('                          FROM Pedido                                            ');
          qryAux.SQL.Add('                         WHERE nCdPedido = ' + qryItensnCdPedido.asString + ')') ;
          qryAux.Open ;

          if (qryAux.FieldList[0].Value = 1) then
          begin
              case MessageDlg('Houve altera��o no pre�o de custo previso do item.' +#13#13 + 'Deseja alterar o pre�o de venda do produto ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrYes: showForm( objLibDivRec_PrecoVenda , FALSE );
              end ;
          end ;
      end ;

  end ;

  InputQuery('ER2Soft','Mensagem de Observa��o',cOBS) ;

  frmMenu.Connection.BeginTrans ;

  try

      usp_processo.Close ;
      usp_processo.Parameters.ParamByName('@nCdHistAutorDiverg').Value := qryItensnCdHistAutorDiverg.Value ;
      usp_processo.Parameters.ParamByName('@nCdUsuario').Value         := frmMenu.nCdUsuarioLogado;
      usp_processo.Parameters.ParamByName('@cOBS').Value               := cOBS;
      usp_processo.Parameters.ParamByName('@nPrecoVenda').Value        := objLibDivRec_PrecoVenda.edtPrecoVenda.Value ;
      usp_processo.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  freeAndNil( objLibDivRec_PrecoVenda ) ;

  ShowMessage('Diverg�ncia liberada com sucesso.') ;

  qryItens.Close ;
  qryItens.Open ;

end;

procedure TfrmLibDivRec.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryItenscFlgAutorizar.Value = 0) then
  begin

    if (Column = DBGridEh1.Columns[0]) then
    begin
      DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clBlue ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

    if (Column = DBGridEh1.Columns[1]) then
    begin
      DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clBlue ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

    if (Column = DBGridEh1.Columns[2]) then
    begin
      DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clBlue ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

  end ;

end;

procedure TfrmLibDivRec.DBGridEh2Exit(Sender: TObject);
begin
  inherited;

  If (DBGridEh2.Visible) then
      DBGridEh2.Visible := False ;
  
end;

procedure TfrmLibDivRec.GroupBox1Exit(Sender: TObject);
begin
  inherited;

  GroupBox1.Visible := False ;
  
end;

procedure TfrmLibDivRec.cxButton1Click(Sender: TObject);
begin

  if (qryItemRecebimento.Active) and (qryItemRecebimento.State <> dsBrowse) then
  begin
      try
          qryItemRecebimento.Post ;
      except
      end ;
  end ;

  qryItemRecebimento.Close ;

  GroupBox1.Visible := False ;
  
end;

procedure TfrmLibDivRec.ExibirDivergncias1Click(Sender: TObject);
var
  objLibDivRec_Parcelas : TfrmLibDivRec_Parcelas ;
begin
  inherited;

  // custo
  if    (qryItensnCdTabTipoDiverg.Value = 2)
     or (qryItensnCdTabTipoDiverg.Value = 8)
     or (qryItensnCdTabTipoDiverg.Value = 9)
     or (qryItensnCdTabTipoDiverg.Value = 4) then
  begin

      if (qryItensnValDescAbatUnit.Value > 0) then
      begin
          DBCheckBox1.Visible := True ;
          PosicionaQuery(qryItemRecebimento, qryItensnCdItemRecebimento.AsString) ;
      end
      else DBCheckBox1.Visible := False ;

      GroupBox1.Visible := True ;
      cxButton1.SetFocus ;
  end ;

  // prazo
  if (qryItensnCdTabTipoDiverg.Value = 10) then
  begin
  
      objLibDivRec_Parcelas := TfrmLibDivRec_Parcelas.Create( Self ) ;

      objLibDivRec_Parcelas.ExibeParcelas(qryMasternCdRecebimento.Value) ;

      freeAndNil( objLibDivRec_Parcelas ) ;

  end ;


  // grade
  if (qryItensnCdTabTipoDiverg.Value = 5) then
  begin

      DBGridEh2.AutoFitColWidths := False ;

      qryPreparaTemp.Close ;
      qryPreparaTemp.ExecSQL ;

      usp_Grade.Close ;
      usp_Grade.Parameters.ParamByName('@nCdItemRecebimento').Value  := qryItensnCdItemRecebimento.Value ;
      usp_Grade.Open ;

      {qryTemp.Close ;
      qryTemp.SQL.Clear ;
      qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
      qryTemp.Open ;}

      {qryTemp.First ;}

      if (usp_Grade.State = dsBrowse) then
          usp_Grade.Edit
      else usp_Grade.Insert ;

      usp_Grade.FieldList[0].Value := usp_Grade.FieldList[0].Value ;

      DBGridEh2.Columns.Items[0].Visible := False ;
      DBGridEh2.AutoFitColWidths := True ;
      DBGridEh2.Columns.Items[0].Width := 50 ;
      DBGridEh2.Realign;

      DBGridEh2.Visible := True ;
      DBGridEh2.SetFocus ;

  end ;

end;

procedure TfrmLibDivRec.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  VincularPedido1.Enabled := (qryItens.Active and (qryItensnCdPedido.asString = '')) ;

end;

procedure TfrmLibDivRec.vinculaPedidoitemRecebimento(nCdItemRecebimento,
  nCdProduto: integer);
var
  objConsultaItemPedidoAberto : TfrmConsultaItemPedidoAberto ;
begin

    objConsultaItemPedidoAberto := TfrmConsultaItemPedidoAberto.Create( Self ) ;

    objConsultaItemPedidoAberto.nCdTerceiro    := qryMasternCdTerceiro.Value ;
    objConsultaItemPedidoAberto.nCdTipoReceb   := qryMasternCdTipoReceb.Value ;
    objConsultaItemPedidoAberto.nCdLoja        := qryMasternCdLoja.Value ;
    objConsultaItemPedidoAberto.nCdRecebimento := qryMasternCdRecebimento.Value ;
    objConsultaItemPedidoAberto.nCdPedido      := 0 ;
    objConsultaItemPedidoAberto.nCdProduto     := nCdProduto ;

    showForm( objConsultaItemPedidoAberto , FALSE ) ;

    {-- se foi selecionado algum item do pedido , vincula ao item do recebimento --}
    if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
    begin

        if (MessageDlg('Confirma a vincula��o deste item de recebimento com o pedido ' + objConsultaItemPedidoAberto.qryPedidonCdPedido.asString + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
            exit ;

        frmMenu.Connection.BeginTrans;

        try
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Close;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemRecebimento').Value := nCdItemRecebimento;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.') ;
            raise ;
        end ;

        frmMenu.Connection.CommitTrans ;

        ShowMessage('Vincula��o processada com sucesso.') ;

        qryItens.Requery();

    end ;

    freeAndNil( objConsultaItemPedidoAberto ) ;

end;

procedure TfrmLibDivRec.VincularPedido1Click(Sender: TObject);
begin

    vinculaPedidoitemRecebimento(qryItensnCdItemRecebimento.Value, qryItensnCdProduto.Value) ;

end;

procedure TfrmLibDivRec.btExibeRecebimentoClick(Sender: TObject);
var
  objForm : TfrmRecebimentoCompleto;
begin
  inherited;

  if (qryMaster.isEmpty) then
  begin
      MensagemAlerta('Selecione um recebimento.') ;
      abort ;
  end ;

  objForm := TfrmRecebimentoCompleto.Create( Self ) ;

  try

      objForm.PosicionaQuery( objForm.qryMaster , qryMasternCdRecebimento.asString );
      showForm( objForm , FALSE ) ;

  finally

      freeAndNil( objForm ) ;

  end ;


end;

initialization
    RegisterClass(TfrmLibDivRec) ;
    
end.
