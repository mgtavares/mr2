unit fConsultaChequeResponsavel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, StdCtrls, ExtCtrls,
  ImgList, ComCtrls, ToolWin, Mask, DB, DBCtrls, ADODB, DBGridEhGrouping,
  cxPC, cxControls;

type
  TfrmConsultaChequeResponsavel = class(TfrmProcesso_Padrao)
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    dsResultado: TDataSource;
    qryResultado: TADOQuery;
    qryResultadocConta: TStringField;
    qryResultadoiNrCheque: TIntegerField;
    qryResultadocCNPJCPF: TStringField;
    qryResultadonValCheque: TBCDField;
    qryResultadodDtDeposito: TDateTimeField;
    qryResultadocNmTerceiroPort: TStringField;
    qryResultadodDtRemessaPort: TDateTimeField;
    qryResultadodDtDevol: TDateTimeField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    RadioGroup1: TRadioGroup;
    edtTerceiro: TMaskEdit;
    edtCNPJEmissor: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    edtNumCheque: TMaskEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure edtTerceiroExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaChequeResponsavel: TfrmConsultaChequeResponsavel;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsultaChequeResponsavel.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

end;

procedure TfrmConsultaChequeResponsavel.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      ShowMessage('Selecione o terceiro responsável') ;
      exit ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdTerceiroResp').Value := frmMenu.ConvInteiro(edtTerceiro.Text) ;
  qryResultado.Parameters.ParamByName('cCNPJCPF').Value        := Trim(edtCNPJEmissor.Text) ;
  qryResultado.Parameters.ParamByName('iNrCheque').Value       := frmMenu.ConvInteiro(edtNumCheque.Text) ;
  qryResultado.Parameters.ParamByName('iTipoSel').Value        := RadioGroup1.ItemIndex ;
  qryResultado.Open ;

  if (qryResultado.Eof) then
      MensagemAlerta('Nenhum registro encontrado.') ;

end;

procedure TfrmConsultaChequeResponsavel.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmConsultaChequeResponsavel) ;

end.
