unit fOrcamentoEnergy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmOrcamentoEnergy = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DBEdit14: TDBEdit;
    DataSource3: TDataSource;
    DBEdit15: TDBEdit;
    DataSource4: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DataSource5: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    DataSource8: TDataSource;
    DataSource9: TDataSource;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    TabAD: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    DataSource11: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    btSugParcela: TcxButton;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    DBGridEh4: TDBGridEh;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnQtdeExpRec: TBCDField;
    qryItemADnQtdeCanc: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnCdPedido: TIntegerField;
    qryItemADnCdTipoItemPed: TIntegerField;
    qryItemADnCdItemPedido: TAutoIncField;
    dsItemAD: TDataSource;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    ToolButton12: TToolButton;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    DataSource12: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    DataSource13: TDataSource;
    TabFormula: TcxTabSheet;
    qryItemFormula: TADOQuery;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanQtdeExpRec: TBCDField;
    qryItemFormulanQtdeCanc: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanCdPedido: TIntegerField;
    qryItemFormulanCdTipoItemPed: TIntegerField;
    qryItemFormulanCdItemPedido: TAutoIncField;
    qryItemFormulacSiglaUnidadeMedida: TStringField;
    dsItemFormula: TDataSource;
    DBGridEh6: TDBGridEh;
    Label33: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label34: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label35: TLabel;
    cxTextEdit9: TcxTextEdit;
    qryItemFormulanCdProduto: TIntegerField;
    qryProdutoFormulado: TADOQuery;
    qryProdutoFormuladonCdProduto: TIntegerField;
    qryProdutoFormuladocNmProduto: TStringField;
    usp_gera_item_embalagem: TADOStoredProc;
    cxButton2: TcxButton;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    cxTabSheet1: TcxTabSheet;
    qryMastercOBSFinanc: TMemoField;
    cxTabSheet2: TcxTabSheet;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Label9: TLabel;
    DBEdit22: TDBEdit;
    DBEdit38: TDBEdit;
    Label12: TLabel;
    Image4: TImage;
    ToolButton13: TToolButton;
    usp_valida_credito: TADOStoredProc;
    sp_tab_preco: TADOStoredProc;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostonCdGrupoImposto: TIntegerField;
    qryGrupoImpostocNmGrupoImposto: TStringField;
    qryItemADnCdGrupoImposto: TIntegerField;
    qryItemADcNmGrupoImposto: TStringField;
    Label15: TLabel;
    DBEdit21: TDBEdit;
    DBEdit23: TDBEdit;
    TabDesTecnica: TcxTabSheet;
    DBGridEh7: TDBGridEh;
    qryDescrTecnica: TADOQuery;
    qryDescrTecnicanCdItemPedido: TAutoIncField;
    qryDescrTecnicanCdProduto: TIntegerField;
    qryDescrTecnicacNmItem: TStringField;
    qryDescrTecnicanQtdePed: TBCDField;
    qryDescrTecnicacDescricaoTecnica: TMemoField;
    DBMemo4: TDBMemo;
    DataSource14: TDataSource;
    qryMasternValDevoluc: TBCDField;
    qryMasternValValePres: TBCDField;
    qryMasternValValeMerc: TBCDField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdMapaCompra: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercAnotacao: TMemoField;
    qryMastercFlgOrcamento: TIntegerField;
    qryMastercOrcamentoFin: TIntegerField;
    qryMasternCdPedidoOrcamento: TIntegerField;
    DBMemo1: TDBMemo;
    qryItemEstoquenDimensao: TBCDField;
    qryItemEstoquenAltura: TBCDField;
    qryItemEstoquenLargura: TBCDField;
    qryProdutocReferencia: TStringField;
    qryItemEstoquecReferencia: TStringField;
    qryProdutonAltura: TBCDField;
    qryProdutonLargura: TBCDField;
    qryProdutonDimensao: TBCDField;
    qryProdutonValVenda: TBCDField;
    cxTabSheet3: TcxTabSheet;
    DBMemo2: TDBMemo;
    qryProdutocDescricaoTecnica: TMemoField;
    qryItemEstoquecDescricaoTecnica: TMemoField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADBeforePost(DataSet: TDataSet);
    procedure qryItemADAfterPost(DataSet: TDataSet);
    procedure qryItemADBeforeDelete(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure DBGridEh4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaBeforePost(DataSet: TDataSet);
    procedure qryItemFormulaAfterPost(DataSet: TDataSet);
    procedure qryItemFormulaBeforeDelete(DataSet: TDataSet);
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure DBGridEh6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh6ColExit(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit41KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit19Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton13Click(Sender: TObject);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemADCalcFields(DataSet: TDataSet);
    procedure DBMemo4Exit(Sender: TObject);
    procedure TabDesTecnicaEnter(Sender: TObject);
    procedure qryItemEstoqueAfterInsert(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
    nCdProdutoAux : integer ;
    cReferenciaAux, cNmItemAux   : string ;
    nValM2Aux, nAlturaAux, nLarguraAux, nDimensaoAux, nQtdePedAux, nValUnitAux, nValTotalAux : double ;
  public
    { Public declarations }
  end;

var
  frmOrcamentoEnergy: TfrmOrcamentoEnergy;

implementation

uses Math,fMenu, fLookup_Padrao, fEmbFormula,
  fPrecoEspPedCom, rPedidoVenda, rOrcamentoEnergy;

{$R *.dfm}


procedure TfrmOrcamentoEnergy.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmOrcamentoEnergy.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 137 ;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmOrcamentoEnergy.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      MensagemAlerta('Nenhum tipo de or�amento vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
      DBEdit4.OnExit(nil);
  end ;

  qryUsuarioTipoPedido.Close ;


  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          MensagemAlerta('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;

  end ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else DBEdit5.SetFocus ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  btSugParcela.Enabled := True ;
  btSalvar.Enabled     := True ;
  ToolButton10.Enabled := True ;

end;

procedure TfrmOrcamentoEnergy.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmOrcamentoEnergy.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(136);

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                MensagemAlerta('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
    if (qryTerceironCdTerceiroPagador.Value <> 0) and (qryMasternCdTerceiroPagador.Value = 0) then
    begin
      PosicionaQuery(qryTerceiroPagador,qryTerceironCdTerceiroPagador.AsString) ;
      qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
    end ;
  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if not qryTipoPedido.Active then
  begin
      MensagemAlerta('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end ;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

      qryMasternPercDesconto.Value := qryTerceironPercDesconto.Value ;
      qryMasternPercAcrescimo.Value := qryTerceironPercAcrescimo.Value ;
  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.Enabled := False ;
  TabAD.Enabled          := False ;
  TabFormula.Enabled     := False ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
        TabItemEstoque.Enabled := True ;

    if (qryTipoPedidocFlgItemAD.Value = 1) then
        TabAD.Enabled := True ;

    if (qryTipoPedidocFlgItemFormula.Value = 1) then
        TabFormula.Enabled := True ;
  end ;

  if not TabItemEstoque.enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmOrcamentoEnergy.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmOrcamentoEnergy.DBEdit38Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCondPagto, DBEdit38.Text) ;
end;

procedure TfrmOrcamentoEnergy.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemErro('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      MensagemErro('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryTerceiroPagador,qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryFollow,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;


  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  DBGridEh4.ReadOnly   := False ;
  DBGridEh6.ReadOnly   := False ;

  btSugParcela.Enabled := True ;
  btSalvar.Enabled     := True ;
  ToolButton10.Enabled := True ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      DBGridEh1.ReadOnly   := True ;
      DBGridEh3.ReadOnly   := True ;
      DBGridEh4.ReadOnly   := True ;
      DBGridEh6.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
      btSalvar.Enabled     := False ;
      ToolButton10.Enabled := False ;
  end ;

  TabItemEstoque.Enabled := False ;
  TabAD.Enabled          := False ;
  TabFormula.Enabled     := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
      TabItemEstoque.Enabled := True ;

  if (qryTipoPedidocFlgItemAD.Value = 1) then
      TabAD.Enabled := True ;

  if (qryTipoPedidocFlgItemFormula.Value = 1) then
      TabFormula.Enabled := True ;

  if not TabItemEstoque.enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

  nCdProdutoAux  := 0 ;
  cReferenciaAux := '' ;
  cNmItemAux     := '' ;
  nValM2Aux      := 0 ;
  nAlturaAux     := 0 ;
  nLarguraAux    := 0 ;
  nDimensaoAux   := 0 ;
  nQtdePedAux    := 0 ;
  nValUnitAux    := 0 ;
  nValTotalAux   := 0 ;

end;

procedure TfrmOrcamentoEnergy.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiroColab.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryIncoterms.Close ;
  qryTerceiroTransp.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryItemAD.Close ;
  qryDadoAutorz.Close ;
  qryFollow.Close ;
  qryItemFormula.Close ;
  qryDescrTecnica.Close ;

  nCdProdutoAux  := 0 ;
  cReferenciaAux := '' ;
  cNmItemAux     := '' ;
  nValM2Aux      := 0 ;
  nAlturaAux     := 0 ;
  nLarguraAux    := 0 ;
  nDimensaoAux   := 0 ;
  nQtdePedAux    := 0 ;
  nValUnitAux    := 0 ;
  nValTotalAux   := 0 ;
  

end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      abort ;
  end ;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do item.') ;
      abort ;
  end ;

  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

  if (qryProdutonCdGrade.Value = 0) then
      qryItemEstoquenCdTipoItemPed.Value := 2
  else  qryItemEstoquenCdTipoItemPed.Value := 1 ;

  inherited;

  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value) ; //+ 0.005  ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemEstoquenValTotalItem.OldValue) ;
  end ;
  
  qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

  if (qryItemEstoque.State = dsInsert) then
  begin
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;
      qryItemEstoquecDescricaoTecnica.Value := qryProdutocDescricaoTecnica.Value ;
  end ;

  nCdProdutoAux  := qryItemEstoquenCdProduto.Value ;
  cReferenciaAux := qryItemEstoquecReferencia.Value ;
  cNmItemAux     := qryItemEstoquecNmItem.Value ;
  nValM2Aux      := qryItemEstoquenValSugVenda.Value ;
  nAlturaAux     := qryItemEstoquenAltura.Value ;
  nLarguraAux    := qryItemEstoquenLargura.Value ;
  nDimensaoAux   := qryItemEstoquenDimensao.Value ;
  nQtdePedAux    := qryItemEstoquenQtdePed.Value ;
  nValUnitAux    := qryItemEstoquenValUnitario.Value ;
  nValTotalAux   := qryItemEstoquenValTotalItem.Value ;
  

end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if not qryProduto.eof then
  begin
  
      if (qryItemEstoquecNmItem.Value = '') then
          qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;

      if (qryItemEstoquecReferencia.AsString = '') then
          qryItemEstoquecReferencia.Value := qryProdutocReferencia.Value ;

      if (qryItemEstoquenAltura.AsString = '') then
      begin
          qryItemEstoquenAltura.Value   := qryProdutonAltura.Value ;
          qryItemEstoquenLargura.Value  := qryProdutonLargura.Value ;
          qryItemEstoquenDimensao.Value := qryProdutonDimensao.Value ;
      end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;
  
  If (DBGridEh1.Col = 1) then
  begin

      DBGridEh1.Columns[2].ReadOnly   := True ;

      if (qryItemEstoque.State <> dsBrowse) then
      begin

        qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

        PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
        begin
            qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;

            DBGridEh2.AutoFitColWidths := True ;

            usp_Grade.Close ;
            usp_Grade.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
            usp_Grade.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
            usp_Grade.ExecProc ;

            qryTemp.Close ;
            qryTemp.SQL.Clear ;
            qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
            qryTemp.Open ;

            qryTemp.Edit ;

            DBGridEh2.Columns.Items[0].Visible := False ;

            DBGridEh2.Visible := True ;

            DBGridEh2.SetFocus ;

            DBGridEh1.Col := 3 ;
        end ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
        begin

            DBGridEh1.Columns[2].ReadOnly := False ;
            DBGridEh1.Col                 := 2 ;

        end ;

      end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.LeParametro('USADESCTECPED') <> 'S') then
  begin
      TabDesTecnica.Enabled := False ;
  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;

  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueAfterPost(DataSet: TDataSet);
begin
  usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;

  inherited;
  
  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemEstoquenValTotalItem.Value ;
  qryMaster.Post ;

  qryItemEstoque.Last ;

end;

procedure TfrmOrcamentoEnergy.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDesconto.Value < 0) then
  begin
      MensagemErro('Percentual de desconto inv�lido.') ;
      DbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercAcrescimo.Value < 0) then
  begin
      MensagemErro('Percentual de acr�scimo inv�lido.') ;
      DbEdit9.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntIni.AsString = '') then
  begin
      qryMasterdDtPrevEntIni.Value := qryMasterdDtPedido.Value ;
  end ;

  if (qryMasterdDtPrevEntFim.AsString = '') then
  begin
      qryMasterdDtPrevEntFim.Value := qryMasterdDtPedido.Value ;
  end ;

  if (qryMasterdDtPrevEntFim.Value < qryMasterdDtPrevEntIni.Value) then
  begin
      MensagemAlerta('Previs�o de entrega inv�lida.') ;
      DBEdit21.SetFocus;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusPed.Value := 1;
      qryMastercOrcamentoFin.Value   := 0;
  end ;

  qryMasternValPedido.Value    := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value ;
  qryMastercFlgOrcamento.Value := 1 ;

end;

procedure TfrmOrcamentoEnergy.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemEstoquenValTotalItem.Value ;


end;

procedure TfrmOrcamentoEnergy.cxButton1Click(Sender: TObject);
begin
  if not qryItemEstoque.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      ShowMessage('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;


  DBGridEh2.AutoFitColWidths := True ;

  usp_Grade.Close ;
  usp_Grade.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
  usp_Grade.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
  usp_Grade.ExecProc ;

  qryTemp.Close ;
  qryTemp.SQL.Clear ;
  qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
  qryTemp.Open ;

  qryTemp.First ;

  if (qryTemp.State = dsBrowse) then
      qryTemp.Edit
  else qryTemp.Insert ;

  qryTemp.FieldList[0].Value := qryTemp.FieldList[0].Value ; 

  DBGridEh2.Columns.Items[0].Visible := False ;

  DBGridEh2.Visible := True ;
  DBGridEh2.SetFocus ;

end;

procedure TfrmOrcamentoEnergy.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmOrcamentoEnergy.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value ;

  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;

end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmOrcamentoEnergy.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmOrcamentoEnergy.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      ShowMessage('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmOrcamentoEnergy.ToolButton10Click(Sender: TObject);
var
  nValProdutos, nValDesconto, nValAcrescimo : double ;
begin

  nValProdutos := 0 ;
  nValDesconto  := 0 ;
  nValAcrescimo := 0 ;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  if qryItemAD.Active then
      qryItemAD.First ;

  if qryItemFormula.Active then
      qryItemformula.First ;

  // calcula o total do pedido
  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos := nValProdutos + qryItemEstoquenValTotalItem.Value ;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  if qryItemAD.Active and not qryItemAD.eof then
  begin

      qryItemAD.First ;

      while not qryItemAD.Eof do
      begin
          nValProdutos := nValProdutos + qryItemADnValTotalItem.Value ;
          qryItemAD.Next ;
      end ;

      qryItemAD.First ;

  end ;

  if qryItemFormula.Active and not qryItemFormula.eof then
  begin

      qryItemFormula.First ;

      while not qryItemFormula.Eof do
      begin
          nValProdutos := nValProdutos + qryItemFormulanValTotalItem.Value ;
          qryItemFormula.Next ;
      end ;

      qryItemFormula.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value := nValProdutos ;
  qryMaster.Post ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (dbEdit38.Text = '') then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
  qryAux.Open ;

  If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da soma das parcelas � diferente do valor total dos pedidos') ;
      exit ;
  end ;

  qryAux.Close ;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 2 ; // Aguardando aprova��o
      qryMasternSaldoFat.Value       := qryMasternValPedido.Value ;
      qryMaster.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Or�amento finalizado com sucesso.') ;

  case MessageDlg('Deseja imprimir este or�amento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: ToolButton12.Click ;
  end ;

  btCancelar.Click;

end;

procedure TfrmOrcamentoEnergy.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmOrcamentoEnergy.qryItemADBeforePost(DataSet: TDataSet);
begin

  if (qryItemADcNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemADcSiglaUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItemADnQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemADnValCustoUnit.Value < 0) then
  begin
      MensagemAlerta('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  if (qryItemADnCdGrupoImposto.Value = 0) or (qryItemADcNmGrupoImposto.Value = '') then
  begin
      MensagemAlerta('Informe o grupo de imposto') ;
      abort ;
  end ;

  inherited;

  if (qryItemAD.State = dsInsert) then
  begin
      qryItemADnCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemADnCdTipoItemPed.Value := 5 ;
      qryItemADcCdProduto.Value     := 'AD' + qryItemADnCdItemPedido.AsString;
  end ;

  qryItemADcNmItem.Value             := UpperCase(qryItemADcNmItem.Value) ;
  qryItemADcSiglaUnidadeMedida.Value := UpperCase(qryItemADcSiglaUnidadeMedida.Value) ;

  qryItemADnValTotalItem.Value := qryItemADnQtdePed.Value * qryItemADnValCustoUnit.Value ;

  if (qryItemAD.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemADnValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmOrcamentoEnergy.qryItemADAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryItemAD.Edit ;
  qryItemADcCdProduto.Value   := 'AD' + qryItemADnCdItemPedido.AsString;
  //qryItemAD.Post ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemADnValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmOrcamentoEnergy.qryItemADBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemADnValTotalItem.Value ;

end;

procedure TfrmOrcamentoEnergy.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmOrcamentoEnergy.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(68,'EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = Departamento.nCdGrupoProduto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.asString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.State <> dsBrowse) and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Columns[8].ReadOnly := True ;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
      DBGridEh1.Columns[8].ReadOnly := false ;


end;

procedure TfrmOrcamentoEnergy.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmOrcamentoEnergy.ToolButton12Click(Sender: TObject);
begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  if (qryMasternCdTabStatusPed.Value = 1) then
  begin
      MensagemAlerta('Finalize o Or�amento antes de Imprimir.') ;
      exit ;
  end ;

  PosicionaQuery(rptOrcamentoEnergy.qryPedido,qryMasternCdPedido.asString) ;

  rptOrcamentoEnergy.QuickRep1.PreviewModal;

end;

procedure TfrmOrcamentoEnergy.DBGridEh4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) < qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clYellow;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) >= qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clBlue;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemADnQtdeCanc.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clRed;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.qryItemFormulaBeforePost(DataSet: TDataSet);
begin

  if (qryItemFormulacNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemFormulanQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemFormulanValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemFormula.State = dsInsert) then
  begin
      qryItemFormulanCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemFormulanCdTipoItemPed.Value := 6 ;
  end ;

  qryItemFormulanValTotalItem.Value := qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value ;

  if (qryItemFormula.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemFormulanValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmOrcamentoEnergy.qryItemFormulaAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemFormulanValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmOrcamentoEnergy.qryItemFormulaBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemFormulanValTotalItem.Value ;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemFormulanCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

end;

procedure TfrmOrcamentoEnergy.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmOrcamentoEnergy.DBGridEh6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) < qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clYellow;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) >= qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clBlue;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemFormulanQtdeCanc.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clRed;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBGridEh6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemFormula.State = dsBrowse) then
             qryItemFormula.Edit ;


        if (qryItemFormula.State = dsInsert) or (qryItemFormula.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(70,'EXISTS(SELECT 1 FROM FormulaTipoPedido PTP WHERE PTP.nCdProdutoPai = Produto.nCdProduto AND nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemFormulanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmOrcamentoEnergy.DBGridEh6ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh6.Col = 1) then
  begin

      if (qryItemFormula.State <> dsBrowse) then
      begin

        PosicionaQuery(qryProdutoFormulado, qryItemFormulanCdProduto.AsString) ;

        if not qryProdutoFormulado.eof then
        begin
            qryItemFormulacNmItem.Value := qryProdutoFormuladocNmProduto.Value ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
            qryAux.Open ;

            if not qryAux.eof then
            begin

                frmEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
                frmEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
                frmEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
                frmEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
                frmEmbFormula.RenderizaGrade() ;

                // calcula o pre�o
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
                qryAux.SQL.Add('                ELSE 0       ') ;
                qryAux.SQL.Add('           END)              ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
                qryAux.SQL.Add('                ELSE 0       ') ;
                qryAux.SQL.Add('           END)              ') ;
                qryAux.SQL.Add('  FROM ##Temp_Grade_Embalagem') ;
                qryAux.Open ;

                qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value + qryAux.FieldList[1].Value ;

            end ;

            qryAux.Close ;

        end ;

      end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.cxButton2Click(Sender: TObject);
begin

  if not qryItemFormula.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
  qryAux.Open ;

  if not qryAux.eof then
  begin

      frmEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
      frmEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
      frmEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
      frmEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
      frmEmbFormula.RenderizaGrade() ;

      // calcula o pre�o
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('  FROM ##Temp_Grade_Embalagem') ;
      qryAux.Open ;

      if (qryMasternCdTabStatusPed.Value <= 1) then
      begin
      
          if (qryItemFormula.State = dsBrowse) then
              qryItemFormula.Edit ;

          qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value + qryAux.FieldList[1].Value ;

          DbGridEh6.SetFocus ;
          DBGridEh6.Col := 3 ;

      end ;

    end
    else begin
        ShowMessage('Produto n�o tem configura��o de embalagem.') ;
    end ;

    qryAux.Close ;


end;

procedure TfrmOrcamentoEnergy.DBGridEh6Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmOrcamentoEnergy.cxButton3Click(Sender: TObject);
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    frmPrecoEspPedCom.nCdPedido := qryMasternCdPedido.Value ;
    frmPrecoEspPedCom.ShowModal ;

end;

procedure TfrmOrcamentoEnergy.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasternCdTabStatusPed.Value > 1) then
      begin
          MensagemAlerta('Pedido n�o pode ser alterado.') ;
          abort ;
      end ;
  end ;

  if (qryDescrTecnica.Active) then
      if (qryDescrTecnica.State = dsEdit) then
      begin
          qryDescrTecnica.Post ;
      end ;
  
  inherited;

end;

procedure TfrmOrcamentoEnergy.DBEdit41KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryIncoterms,DbEdit19.Text) ;

end;

procedure TfrmOrcamentoEnergy.DBEdit8Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroTransp, DbEdit8.Text) ;

end;

procedure TfrmOrcamentoEnergy.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.ToolButton13Click(Sender: TObject);
begin
  inherited;
  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  
  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmOrcamentoEnergy.DBGridEh1ColEnter(Sender: TObject);
var
    vFrac : integer ;
    nAlturaCalc, nLarguraCalc : double ;
begin
  inherited;

  if (dbgridEh1.Col = 4) then
  begin

      if (qryItemEstoque.State = dsInsert) then
      begin
          PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.AsString) ;

          if not qryProduto.eof then
              qryItemEstoquenValSugVenda.Value := qryProdutonValVenda.Value ;
      end ;

  end ;

  if (dbgridEh1.Col = 7) then
  begin

      if (qryItemEstoque.State <> dsBrowse) then
      begin
          nAlturaCalc  := frmMenu.TBRound(qryItemEstoquenAltura.Value,3) ;
          nLarguraCalc := frmMenu.TBRound(qryItemEstoquenLargura.Value,3) ;

          // se for menor que quatro, o valor j� foi arredondado.
          if (length(FloatToStr(Frac(nAlturaCalc))) > 3) then
          begin
              vFrac := StrToInt(Copy(FloatToStr(Frac(nAlturaCalc)),4,1));

              if (vFrac > 0) and (vFrac <> 5) then
                  nAlturaCalc := nAlturaCalc + ((10 - vFrac)/100) ;
          end ;

          // se for menor que tres, o valor j� foi arredondado.
          if (length(FloatToStr(Frac(nLarguraCalc))) > 3) then
          begin
              vFrac := StrToInt(Copy(FloatToStr(Frac(nLarguraCalc)),4,1));

              if (vFrac > 0) and (vFrac <> 5) then
                  nLarguraCalc := nLarguraCalc + ((10 - vFrac)/100) ;
          end ;

          qryItemEstoquenDimensao.Value := nAlturaCalc * nLarguraCalc ;

      end ;

      if (nAlturaCalc + nLarguraCalc) <> (qryItemEstoquenAltura.Value + qryItemEstoquenLargura.Value) then
          ShowMessage('Medidas do C�lculo : alt ' + FloatToStr(nAlturaCalc) + ' x larg ' + FloatToStr(nLarguraCalc)) ;

  end ;

  if (dbGridEh1.Col = 8) then
  begin

      if (qryItemEstoque.State <> dsBrowse) then
          qryItemEstoquenValUnitario.Value := qryItemEstoquenValSugVenda.Value * qryItemEstoquenDimensao.Value ;

  end ;

end;

procedure TfrmOrcamentoEnergy.DBGridEh4KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemAD.State = dsBrowse) then
             qryItemAD.Edit ;
        
        if (qryItemAD.State = dsInsert) or (qryItemAD.State = dsEdit) then
        begin

            if (dbGridEh4.Col = 12) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(73);

                If (nPK > 0) then
                begin
                    qryItemADnCdGrupoImposto.Value := nPK ;
                    PosicionaQuery(qryGrupoImposto, qryItemADnCdGrupoImposto.AsString) ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrcamentoEnergy.qryItemADCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryGrupoImposto, qryItemADnCdGrupoImposto.AsString) ;

  if not qryGrupoImposto.eof then
      qryItemADcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value ;
      
end;

procedure TfrmOrcamentoEnergy.DBMemo4Exit(Sender: TObject);
begin
  inherited;

  if (qryDescrTecnica.Active) then
      if (qryDescrTecnica.State = dsEdit) then
      begin
          qryDescrTecnica.Post ;
      end ;
      
end;

procedure TfrmOrcamentoEnergy.TabDesTecnicaEnter(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;
  
end;

procedure TfrmOrcamentoEnergy.qryItemEstoqueAfterInsert(DataSet: TDataSet);
begin
  inherited;

  qryItemEstoquenCdProduto.Value    := nCdProdutoAux  ;
  qryItemEstoquecReferencia.Value   := cReferenciaAux ;
  qryItemEstoquecNmItem.Value       := cNmItemAux     ;
  qryItemEstoquenValSugVenda.Value  := nValM2Aux      ;
  qryItemEstoquenAltura.Value       := nAlturaAux     ;
  qryItemEstoquenLargura.Value      := nLarguraAux    ;
  qryItemEstoquenDimensao.Value     := nDimensaoAux   ;
  qryItemEstoquenQtdePed.Value      := nQtdePedAux    ;
  qryItemEstoquenValUnitario.Value  := nValUnitAux    ;
  qryItemEstoquenValTotalItem.Value := nValTotalAux   ;

end;

procedure TfrmOrcamentoEnergy.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  nCdProdutoAux  := 0 ;
  cReferenciaAux := '' ;
  cNmItemAux     := '' ;
  nValM2Aux      := 0 ;
  nAlturaAux     := 0 ;
  nLarguraAux    := 0 ;
  nDimensaoAux   := 0 ;
  nQtdePedAux    := 0 ;
  nValUnitAux    := 0 ;
  nValTotalAux   := 0 ;
  
end;

initialization
    RegisterClass(TfrmOrcamentoEnergy) ;

end.
