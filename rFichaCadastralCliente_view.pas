unit rFichaCadastralCliente_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFichaCadastralCliente_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryTerceiro: TADOQuery;
    qrySPCPessoaFisica: TADOQuery;
    qrySPCPessoaFisicanCdTabTipoSPC: TIntegerField;
    qrySPCPessoaFisicacNmTabTipoSPC: TStringField;
    qrySPCPessoaFisicacFlgRestricao: TIntegerField;
    qrySPCPessoaFisicacDadosAdicionais: TStringField;
    qrySPCPessoaFisicadDtConsulta: TDateTimeField;
    qrySPCPessoaFisicanCdUsuarioConsulta: TIntegerField;
    qrySPCPessoaFisicanCdLojaConsulta: TIntegerField;
    qrySPCPessoaFisicadDtCad: TDateTimeField;
    QRLabel1: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel22: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText16: TQRDBText;
    QRShape3: TQRShape;
    QRDBText6: TQRDBText;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel32: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText21: TQRDBText;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirodDtNasc: TStringField;
    qryTerceirocRG: TStringField;
    qryTerceirodDtEmissaoRG: TStringField;
    qryTerceirocNmTabTipoSexo: TStringField;
    qryTerceirocNmTabTipoEstadoCivil: TStringField;
    qryTerceirocNmMae: TStringField;
    qryTerceirocNmPai: TStringField;
    qryTerceirocTelefone: TStringField;
    qryTerceirocTelefoneMovel: TStringField;
    qryTerceirocEmail: TStringField;
    qryTerceirocCEP: TStringField;
    qryTerceirocEndereco: TStringField;
    qryTerceirocNmEmpTrab: TStringField;
    qryTerceirocCNPJEmpTrab: TStringField;
    qryTerceirocTelefoneEmpTrab: TStringField;
    qryTerceirocRamalEmpTrab: TStringField;
    qryTerceirocCEPEmpTrab: TStringField;
    qryTerceirocEnderecoEmpTrab: TStringField;
    qryTerceirocNmTabTipoProfissao: TStringField;
    qryTerceironValRendaBruta: TBCDField;
    qryTerceironValLimiteCred: TBCDField;
    qryTerceironPercEntrada: TBCDField;
    QRLabel34: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText23: TQRDBText;
    QRShape6: TQRShape;
    QRLabel37: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel38: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel4: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaCadastralCliente_view: TrptFichaCadastralCliente_view;

implementation

uses fMenu;

{$R *.dfm}

end.
