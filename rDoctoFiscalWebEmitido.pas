unit rDoctoFiscalWebEmitido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBCtrls, ACBrBase, ACBrValidador;

type
  TrptDoctoFiscalWebEmitido = class(TfrmRelatorio_Padrao)
    edtEmpresa: TMaskEdit;
    edtLoja: TMaskEdit;
    edtTerceiro: TMaskEdit;
    edtDataInicial: TMaskEdit;
    edtDataFinal: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryTerceiro: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    dsLoja: TDataSource;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    dsTerceiro: TDataSource;
    rdgSituacao: TRadioGroup;
    Label6: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    edtTransp: TMaskEdit;
    DBEdit4: TDBEdit;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    dsTerceiroTransp: TDataSource;
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtTranspExit(Sender: TObject);
    procedure edtTranspKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDoctoFiscalWebEmitido: TrptDoctoFiscalWebEmitido;
  cFiltro              : string;

implementation

uses fMenu, rDoctoFiscalWebEmitido_View, fLookup_Padrao;

{$R *.dfm}

procedure TrptDoctoFiscalWebEmitido.edtEmpresaExit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text);
end;

procedure TrptDoctoFiscalWebEmitido.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja,edtLoja.Text);
end;

procedure TrptDoctoFiscalWebEmitido.edtTerceiroExit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text);
end;

procedure TrptDoctoFiscalWebEmitido.ToolButton1Click(Sender: TObject);
var
  objRel : TrptDoctoFiscalWebEmitido_View;
begin
  inherited;

  if (edtDataInicial.Text = '  /  /    ') then
      edtDataInicial.Text := DateToStr(Now());

  if (edtDataFinal.Text = '  /  /    ') then
      edtDataFinal.Text := DateToStr(Now());

  objRel :=  TrptDoctoFiscalWebEmitido_View.Create(nil);

  Try
      Try
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Close;
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(edtEmpresa.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(edtTerceiro.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@nCdTerceiroTransp').Value := frmMenu.ConvInteiro(edtTransp.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(edtDataInicial.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(edtDataFinal.Text);
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Parameters.ParamByName('@nCdStatus').Value         := rdgSituacao.ItemIndex;
          objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.Open;

          if (objRel.SPREL_DOCTO_FISCALWEB_EMITIDO.RecordCount = 0) then
          begin
              ShowMessage('Nenhum resultado encontrado.');
              Exit;
          end;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          {-- prepara filtros para serem exibidos --}
          objRel.lblFiltro1.Caption := '';
          cFiltro := '';

          if (Trim(edtEmpresa.Text) <> '') then
              cFiltro := cFiltro + 'Empresa: ' + DBEdit1.Text ;

          if (Trim(edtLoja.Text) <> '') then
              cFiltro := cFiltro + '/ Loja: ' + DBEdit2.Text;

          if (Trim(edtTerceiro.Text) <> '') then
              cFiltro := cFiltro + '/ Terceiro: ' + DBEdit3.Text;

          if (rdgSituacao.ItemIndex = 0) then
              cFiltro := cFiltro + '/ Situa��o: Todos'
          else if (rdgSituacao.ItemIndex = 1) then
              cFiltro := cFiltro + '/ Situa��o: Ativo'
          else cFiltro := cFiltro + '/ Situa��o: Cancelado';

          cFiltro := cFiltro  + '/ Per�odo: ' + edtDataInicial.Text + ' � ' + edtDataFinal.Text;

          objRel.lblFiltro1.Caption := cFiltro;

          {-- exibe relat�rio --}
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

procedure TrptDoctoFiscalWebEmitido.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, edtLoja.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalWebEmitido.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(25);

            If (nPK > 0) then
            begin
                edtEmpresa.Text := IntToStr(nPK);
                PosicionaQuery(qryEmpresa, edtEmpresa.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalWebEmitido.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                edtTerceiro.Text := IntToStr(nPK);
                PosicionaQuery(qryTerceiro, edtTerceiro.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalWebEmitido.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      edtLoja.ReadOnly   := True ;
      edtLoja.Color      := $00E9E4E4 ;
      edtLoja.Font.Color := clBlack ;
      edtLoja.TabStop    := False ;
  end;

end;

procedure TrptDoctoFiscalWebEmitido.edtTranspExit(Sender: TObject);
begin
  inherited;
  qryTerceiroTransp.Close;
  PosicionaQuery(qryTerceiroTransp, edtTransp.Text);
end;

procedure TrptDoctoFiscalWebEmitido.edtTranspKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
      vk_F4 : begin
          nPK := frmLookup_Padrao.ExecutaConsulta(19);

          if (nPK > 0) then
          begin
              edtTransp.Text := IntToStr(nPK);
              PosicionaQuery(qryTerceiroTransp, edtTransp.Text);
          end;
      end;
  end;
end;

initialization
    RegisterClass(TrptDoctoFiscalWebEmitido);

end.
