unit fCalendario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, Mask, ER2Lookup, StdCtrls, DBCtrls, DB,
  ADODB, ImgList, ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping,
  ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmCalendario = class(TfrmCadastro_Padrao)
    qryMasternCdCalendario: TIntegerField;
    qryMastercNmCalendario: TStringField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasteriAno: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit3: TDBEdit;
    qryLojacNmLoja: TStringField;
    DBEdit4: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItemCalendario: TADOQuery;
    dsItemCalendario: TDataSource;
    qryItemCalendarionCdItemCalendario: TIntegerField;
    qryItemCalendarionCdCalendario: TIntegerField;
    qryItemCalendariodDtFeriado: TDateTimeField;
    ER2LkEmpresa: TER2LookupDBEdit;
    ER2LkLoja: TER2LookupDBEdit;
    qryItemCalendariocNmItemCalendario: TStringField;
    qryVerificaCalendario: TADOQuery;
    qryVerificaCalendarionCdCalendario: TIntegerField;
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryItemCalendarioBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCalendario: TfrmCalendario;

implementation

{$R *.dfm}
uses
  fMenu;

procedure TfrmCalendario.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster           := 'CALENDARIO';
  nCdTabelaSistema          := 522;
  nCdConsultaPadrao         := 771;

  if frmMenu.LeParametro('VAREJO') =  'N' then
  begin
      ER2LkLoja.ReadOnly := True;
      ER2LkLoja.Color    := $00E9E4E4;
  end;
end;

procedure TfrmCalendario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryItemCalendario.Close;

  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);
  PosicionaQuery(qryLoja,qryMasternCdLoja.AsString);
  PosicionaQuery(qryItemCalendario,qryMasternCdCalendario.AsString);
end;

procedure TfrmCalendario.qryItemCalendarioBeforePost(
  DataSet: TDataSet);
begin
  { -- verifica se ano corresponde ao ano informado no calend�rio -- }
  if (qryMasteriAno.Value <> StrToInt(FormatDateTime('yyyy',qryItemCalendariodDtFeriado.Value))) then
  begin
      MensagemAlerta('Ano n�o corresponde ao ano do calend�rio.');
      Abort;
  end;

  inherited;

  qryItemCalendariocNmItemCalendario.Value := UpperCase(qryItemCalendariocNmItemCalendario.Value);
  qryItemCalendarionCdItemCalendario.Value := frmMenu.fnProximoCodigo('ITEMCALENDARIO');
  qryItemCalendarionCdCalendario.Value     := qryMasternCdCalendario.Value;
end;

procedure TfrmCalendario.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryItemCalendario.Close;
end;

procedure TfrmCalendario.qryMasterBeforePost(DataSet: TDataSet);
begin
  { -- efetua consist�ncias -- }
  if (Trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Informe a empresa.');
      ER2LkEmpresa.SetFocus;
      Abort;
  end;

  if ((Trim(DBEdit4.Text) = '') and (not ER2LkLoja.ReadOnly)) then
  begin
      MensagemAlerta('Informe a loja.');
      ER2LkLoja.SetFocus;
      Abort;
  end;

  if (Trim(qryMastercNmCalendario.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do calend�rio.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (frmMenu.ConvInteiro(qryMasteriAno.AsString) = 0) then
  begin
      MensagemAlerta('Informe a ano.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (Length(qryMasteriAno.AsString) <> 4) then
  begin
      MensagemAlerta('Ano inv�lido. Utilize o formato AAAA.' + #13#13 + 'Exemplo: ' + FormatDateTime('yyyy',Now) + '.');
      DBEdit5.SetFocus;
      Abort;
  end;

  { --  verifica se n�o existe um calend�rio j� cadastrado para mesma empresa, com a mesma loja, no mesmo ano -- }
  qryVerificaCalendario.Close;
  qryVerificaCalendario.Parameters.ParamByName('nPK').Value        := qryMasternCdCalendario.Value;
  qryVerificaCalendario.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(ER2LkEmpresa.Text);
  qryVerificaCalendario.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(ER2LkLoja.text);
  qryVerificaCalendario.Parameters.ParamByName('iAno').Value       := StrToInt(DBEdit5.text);
  qryVerificaCalendario.Open;

  if (not qryVerificaCalendario.Eof) then
  begin
      if (MessageDLG('Calend�rio j� cadastrado no sistema, deseja visualizar? ',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
          PosicionaQuery(qryMaster,qryVerificaCalendarionCdCalendario.AsString)
      else
          DBEdit5.SetFocus;

      Abort;
  end;

  { -- verifica se ano corresponde ao ano informado no calend�rio -- }
  qryItemCalendario.First;

  while (not qryItemCalendario.Eof) do
      begin
      if (qryMasteriAno.Value <> StrToInt(FormatDateTime('yyyy',qryItemCalendariodDtFeriado.Value))) then
      begin
          MensagemAlerta('Ano n�o corresponde ao ano do calend�rio.');
          Abort;
      end;

      qryItemCalendario.Next;
  end;

  inherited;
end;

procedure TfrmCalendario.btIncluirClick(Sender: TObject);
begin
  inherited;

  ER2LkEmpresa.SetFocus;
end;

procedure TfrmCalendario.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (not qryMaster.Active) then
      Exit;

  if (qryMaster.State = dsInsert) then
  begin
      qryMaster.Post;

      PosicionaQuery(qryItemCalendario,qryMasternCdCalendario.AsString);
  end;
end;

initialization
    RegisterClass(TfrmCalendario);
    
end.
