unit fAnaliseLanctoCtbConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, DBCtrls, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmAnaliseLanctoCtbConta = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    edtConta: TMaskEdit;
    Label3: TLabel;
    edtDtInicial: TMaskEdit;
    Label6: TLabel;
    edtDtFinal: TMaskEdit;
    edtGrupo: TMaskEdit;
    Label1: TLabel;
    qryGrupoPlanoConta: TADOQuery;
    qryGrupoPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryGrupoPlanoContacNmGrupoPlanoConta: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryConsulta: TADOQuery;
    qryConsultacNmUnidadeNegocio: TStringField;
    qryConsultacNmGrupoPlanoConta: TStringField;
    qryConsultacNmPlanoConta: TStringField;
    qryConsultanValor: TBCDField;
    dsConsulta: TDataSource;
    cxGridDBTableView1cNmUnidadeNegocio: TcxGridDBColumn;
    cxGridDBTableView1cNmGrupoPlanoConta: TcxGridDBColumn;
    cxGridDBTableView1cNmPlanoConta: TcxGridDBColumn;
    cxGridDBTableView1nValor: TcxGridDBColumn;
    edtUnidadeNegocio: TMaskEdit;
    Label2: TLabel;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qryConsultanCdPlanoConta: TIntegerField;
    qryConsultanCdUnidadeNegocio: TIntegerField;
    procedure edtGrupoExit(Sender: TObject);
    procedure edtContaExit(Sender: TObject);
    procedure edtGrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtUnidadeNegocioExit(Sender: TObject);
    procedure edtUnidadeNegocioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtGrupoChange(Sender: TObject);
    procedure edtContaChange(Sender: TObject);
    procedure edtUnidadeNegocioChange(Sender: TObject);
    procedure edtDtInicialChange(Sender: TObject);
    procedure edtDtFinalChange(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAnaliseLanctoCtbConta: TfrmAnaliseLanctoCtbConta;

implementation

uses fMenu, fLookup_Padrao, fAnaliseLanctoCtbConta_Detalhe;

{$R *.dfm}

procedure TfrmAnaliseLanctoCtbConta.edtGrupoExit(Sender: TObject);
begin
  inherited;

  qryGrupoPlanoConta.Close ;
  PosicionaQuery(qryGrupoPlanoConta, edtGrupo.Text) ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtContaExit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  qryPlanoConta.Parameters.ParamByName('nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro(edtGrupo.Text) ;

  PosicionaQuery(qryPlanoConta, edtConta.Text) ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtGrupoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(40);

            If (nPK > 0) then
            begin
                edtGrupo.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtContaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit1.text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(41,'PlanoConta.nCdGrupoPlanoConta = ' + qryGrupoPlanoContanCdGrupoPlanoConta.AsString)
        else nPK := frmLookup_Padrao.ExecutaConsulta(41) ;

        If (nPK > 0) then
            edtConta.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtUnidadeNegocioExit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, edtUnidadeNegocio.Text) ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtUnidadeNegocioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(110) ;

        If (nPK > 0) then
            edtUnidadeNegocio.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmAnaliseLanctoCtbConta.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') or (trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Informe o per�odo de lan�amentos.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo de lan�amentos inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  qryConsulta.Close ;
  qryConsulta.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva ;
  qryConsulta.Parameters.ParamByName('nCdUnidadeNegocio').Value  := frmMenu.ConvInteiro(edtUnidadeNegocio.Text) ;
  qryConsulta.Parameters.ParamByName('nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro(edtGrupo.Text) ;
  qryConsulta.Parameters.ParamByName('nCdPlanoConta').Value      := frmMenu.ConvInteiro(edtConta.Text) ;
  qryConsulta.Parameters.ParamByName('dDtInicial').Value         := frmMenu.ConvData(edtDtInicial.Text) ;
  qryConsulta.Parameters.ParamByName('dDtFinal').Value           := frmMenu.ConvData(edtDtFinal.Text) ;
  qryConsulta.Open ;

  if (qryConsulta.eof) then
  begin
      ShowMessage('Nenhum lan�amento encontrado para o crit�rio utilizado.') ;
      exit ;
  end ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtGrupoChange(Sender: TObject);
begin
  inherited;

  qryConsulta.Close ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtContaChange(Sender: TObject);
begin
  inherited;
  qryConsulta.Close ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtUnidadeNegocioChange(
  Sender: TObject);
begin
  inherited;
  qryConsulta.Close ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtDtInicialChange(Sender: TObject);
begin
  inherited;
  qryConsulta.Close ;

end;

procedure TfrmAnaliseLanctoCtbConta.edtDtFinalChange(Sender: TObject);
begin
  inherited;
  qryConsulta.Close ;

end;

procedure TfrmAnaliseLanctoCtbConta.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmAnaliseLanctoCtbConta_Detalhe ;
begin
  inherited;

  {-- Exibe a consulta analitica --}
  if qryConsulta.Active then
  begin

      if (qryConsultacNmPlanoConta.Value <> '') then
      begin

          objForm := TfrmAnaliseLanctoCtbConta_Detalhe.Create(nil) ;

          objForm.qryConsultaAnalitica.close ;
          objForm.qryConsultaAnalitica.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva;
          objForm.qryConsultaAnalitica.Parameters.ParamByName('nCdUnidadeNegocio').Value := qryConsultanCdUnidadeNegocio.Value;
          objForm.qryConsultaAnalitica.Parameters.ParamByName('nCdPlanoConta').Value     := qryConsultanCdPlanoConta.Value;
          objForm.qryConsultaAnalitica.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(edtDtInicial.Text) ;
          objForm.qryConsultaAnalitica.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(edtDtFinal.Text);
          objForm.qryConsultaAnalitica.open ;

          showForm( objForm , TRUE ) ;

      end ;
      
  end ;

end;

initialization
    RegisterClass(TfrmAnaliseLanctoCtbConta) ;

end.
