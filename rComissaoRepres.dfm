inherited rptComissaoRepres: TrptComissaoRepres
  Left = 12
  Top = 200
  Caption = 'Relat'#243'rio Comiss'#227'o de Representante'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel [1]
    Left = 198
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [2]
    Left = 9
    Top = 96
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Faturamento'
  end
  object Label5: TLabel [3]
    Left = 81
    Top = 72
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label1: TLabel [4]
    Left = 38
    Top = 48
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit2: TMaskEdit [6]
    Left = 226
    Top = 88
    Width = 77
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit1: TMaskEdit [7]
    Left = 114
    Top = 88
    Width = 77
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit6: TMaskEdit [8]
    Left = 114
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object MaskEdit3: TMaskEdit [9]
    Left = 114
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 186
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 186
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource2
    TabOrder = 6
  end
  object RadioGroup1: TRadioGroup [12]
    Left = 113
    Top = 115
    Width = 208
    Height = 46
    Caption = 'Modo de Exibi'#231#227'o'
    TabOrder = 7
  end
  object RadioButton1: TRadioButton [13]
    Left = 128
    Top = 133
    Width = 81
    Height = 17
    Caption = 'Anal'#237'tico'
    TabOrder = 8
  end
  object RadioButton2: TRadioButton [14]
    Left = 231
    Top = 133
    Width = 81
    Height = 17
    Caption = 'Sint'#233'tico'
    TabOrder = 9
    OnClick = RadioButton2Click
  end
  inherited ImageList1: TImageList
    Left = 584
    Top = 168
  end
  object qryRepresentante: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      
        '   AND EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT WHERE TTT.n' +
        'CdTerceiro = Terceiro.nCdTerceiro AND TTT.nCdTipoTerceiro = 4)'
      '')
    Left = 432
    Top = 200
    object qryRepresentantenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryRepresentantecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryRepresentante
    Left = 624
    Top = 272
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 344
    Top = 208
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryMarca
    Left = 632
    Top = 280
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT nCdTerceiroResponsavel'
      '              ,cFlgRepresentante'
      '    FROM Usuario'
      ' WHERE nCdUsuario = :nPk')
    Left = 400
    Top = 232
    object qryUsuarionCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
    object qryUsuariocFlgRepresentante: TIntegerField
      FieldName = 'cFlgRepresentante'
    end
  end
end
