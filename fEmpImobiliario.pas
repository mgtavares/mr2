unit fEmpImobiliario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  DBGridEhGrouping;

type
  TfrmEmpImobiliario = class(TfrmCadastro_Padrao)
    qryMasternCdEmpImobiliario: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMastercNmEmpImobiliario: TStringField;
    qryMasterdDtLancamento: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit6: TDBEdit;
    DataSource1: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    DBEdit7: TDBEdit;
    DataSource2: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryBlocos: TADOQuery;
    dsBlocos: TDataSource;
    qryBlocosnCdBlocoEmpImobiliario: TAutoIncField;
    qryBlocosnCdEmpImobiliario: TIntegerField;
    qryBlocoscNmBlocoEmpImobiliario: TStringField;
    qryBlocosnCdCC: TIntegerField;
    qryBlocosdDtHabitese: TDateTimeField;
    qryBlocosdDtChaves: TDateTimeField;
    qryBlocoscNmCC: TStringField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryMastercEndereco: TStringField;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    qryBlocosiQtdeUnidade: TIntegerField;
    procedure qryBlocosBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryBlocosCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmpImobiliario: TfrmEmpImobiliario;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmEmpImobiliario.qryBlocosBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryBlocoscNmBlocoEmpImobiliario.Value = '') then
  begin
      MensagemAlerta('Informe o nome do bloco.') ;
      abort ;
  end ;

  if (qryBlocoscNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo.') ;
      abort ;
  end ;

  qryBlocosnCdEmpImobiliario.Value := qryMasternCdEmpImobiliario.Value ;
  
end;

procedure TfrmEmpImobiliario.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  
  DBEdit3.SetFocus ;
end;

procedure TfrmEmpImobiliario.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, DBEdit3.Text) ;
  
end;

procedure TfrmEmpImobiliario.DBGridEh1Enter(Sender: TObject);
begin

  if (qryMaster.Active) then
      if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
          qryMaster.Post ;

  inherited;

end;

procedure TfrmEmpImobiliario.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryBlocos.State = dsBrowse) then
             qryBlocos.Edit ;

        if (qryBlocos.State = dsInsert) or (qryBlocos.State = dsEdit) AND (DBGridEh1.Col = 4) then
        begin

            if (DBEdit7.Text='') then
            begin
                MensagemAlerta('Selecione o tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(29,'EXISTS(SELECT 1 FROM CentroCustoTipoPedido CCTP WHERE CCTP.nCdTipopedido = ' + qryMasternCdTipoPedido.asString + ' AND CCTP.nCdCC = CentroCusto.nCdCC)');

            If (nPK > 0) then
            begin
                qryBlocosnCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEmpImobiliario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryBlocos.Close ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.AsString) ;
  PosicionaQuery(qryBlocos, qryMasternCdEmpImobiliario.AsString) ;

end;

procedure TfrmEmpImobiliario.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(54,'cFlgVenda = 1');

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEmpImobiliario.qryBlocosCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryBlocoscNmCC.Value = '') then
  begin
      qryCC.Close ;
      qryCC.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.AsString ;
      PosicionaQuery(qryCC, qryBlocosnCdCC.asString) ;

      if not qryCC.Eof then
          qryBlocoscNmCC.Value := qryCCcNmCC.Value ;

  end ;

end;

procedure TfrmEmpImobiliario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'EMPIMOBILIARIO' ;
  nCdTabelaSistema  := 80 ;
  nCdConsultaPadrao := 185 ;
end;

procedure TfrmEmpImobiliario.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryBlocos.Close ;
  
end;

procedure TfrmEmpImobiliario.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryBlocos.close ;
  PosicionaQuery(qryBlocos, qryMasternCdEmpImobiliario.AsString) ;


end;

procedure TfrmEmpImobiliario.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

initialization
    RegisterClass(TfrmEmpImobiliario) ;
    
end.
