inherited frmProdutoPosicaoEstoque: TfrmProdutoPosicaoEstoque
  Left = 216
  Top = 148
  Width = 1012
  Caption = 'Produto - Posi'#231#227'o de Estoque'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 81
    Width = 996
    Height = 381
  end
  inherited ToolBar1: TToolBar
    Width = 996
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 996
    Height = 52
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 24
      Width = 38
      Height = 13
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object DBEdit2: TDBEdit
      Left = 124
      Top = 16
      Width = 517
      Height = 21
      DataField = 'cNmProduto'
      DataSource = dsPosicaoEstoque
      Enabled = False
      TabOrder = 0
    end
    object DBEdit1: TDBEdit
      Left = 56
      Top = 16
      Width = 65
      Height = 21
      DataField = 'nCdProduto'
      DataSource = dsPosicaoEstoque
      Enabled = False
      TabOrder = 1
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 81
    Width = 996
    Height = 381
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      DataController.DataSource = dsPosicaoEstoque
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = ',0'
          Kind = skSum
          Column = cxGridDBTableView1nQtdeDisponivelReal
        end
        item
          Format = ',0'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtdeTransito
        end
        item
          Format = ',0'
          Kind = skSum
          Column = cxGridDBTableView1nQtdeEmpenhado
        end
        item
          Format = ',0'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtdeDisponivel
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.ConteudoCaixa
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
        DataBinding.FieldName = 'nCdEmpresa'
        Visible = False
      end
      object cxGrid1DBTableView1cNmEmpresa: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'cNmEmpresa'
        Visible = False
        Width = 118
      end
      object cxGrid1DBTableView1nCdLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'nCdLoja'
        Visible = False
      end
      object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'cNmLoja'
        Visible = False
        Width = 156
      end
      object cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLocalEstoque'
        Visible = False
      end
      object cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn
        Caption = 'Local de Estoque'
        DataBinding.FieldName = 'cNmLocalEstoque'
        Width = 142
      end
      object cxGridDBTableView1nQtdeDisponivelReal: TcxGridDBColumn
        Caption = 'Estoque F'#237'sico'
        DataBinding.FieldName = 'nQtdeDisponivelReal'
        HeaderAlignmentHorz = taRightJustify
        Width = 100
      end
      object cxGrid1DBTableView1nQtdeTransito: TcxGridDBColumn
        Caption = 'Tr'#226'nsito'
        DataBinding.FieldName = 'nQtdeTransito'
        HeaderAlignmentHorz = taRightJustify
        Width = 100
      end
      object cxGridDBTableView1nQtdeEmpenhado: TcxGridDBColumn
        Caption = 'Empenhado'
        DataBinding.FieldName = 'nQtdeEmpenhado'
        HeaderAlignmentHorz = taRightJustify
        Width = 100
      end
      object cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn
        Caption = 'Dispon'#237'vel'
        DataBinding.FieldName = 'nQtdeDisponivel'
        HeaderAlignmentHorz = taRightJustify
        Width = 100
      end
      object cxGrid1DBTableView1dDtUltRecebEstoque: TcxGridDBColumn
        Caption = 'Dt. '#218'lt. Receb.'
        DataBinding.FieldName = 'dDtUltRecebEstoque'
        Width = 115
      end
      object cxGridDBTableView1DBColumn1: TcxGridDBColumn
        Caption = 'Dt. '#218'lt. Receb. CD'
        DataBinding.FieldName = 'dDtUltRecebEstoqueCD'
        Width = 115
      end
      object cxGrid1DBTableView1dDtUltVendaEstoque: TcxGridDBColumn
        Caption = 'Dt. '#218'lt. Venda'
        DataBinding.FieldName = 'dDtUltVendaEstoque'
        Width = 115
      end
      object cxGrid1DBTableView1dDtUltInventario: TcxGridDBColumn
        Caption = #218'lt. Invent'#225'rio'
        DataBinding.FieldName = 'dDtUltInventario'
        Width = 108
      end
      object cxGridDBColumn2: TcxGridDBColumn
        DataBinding.FieldName = 'nCdProduto'
        Visible = False
      end
      object cxGridDBColumn3: TcxGridDBColumn
        DataBinding.FieldName = 'cNmProduto'
        Visible = False
      end
      object cxGrid1DBTableView1DBColumn2: TcxGridDBColumn
        Caption = 'Est. Terceiro'
        DataBinding.FieldName = 'cTerceiro'
        Width = 88
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 272
    Top = 208
  end
  object qryPosicaoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Empresa.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,LocalEstoque.nCdLocalEstoque'
      '      ,LocalEstoque.cNmLocalEstoque'
      '      ,nQtdeTransito'
      '      ,CASE WHEN dDtUltInventario = '#39'1901/01/01'#39' THEN NULL'
      '                 ELSE dDtUltInventario'
      '       END dDtUltInventario'
      '      ,Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,CASE WHEN cFlgEstoqueDisp = 1 THEN '#39'Sim'#39
      '            ELSE '#39'N'#227'o'#39
      '       END cDisponivel'
      '      ,CASE WHEN LocalEstoque.nCdTerceiro IS NOT NULL THEN '#39'Sim'#39
      '            ELSE '#39'N'#227'o'#39
      '       END cTerceiro'
      '      ,PosicaoEstoque.dDtUltRecebEstoque'
      '      ,PosicaoEstoque.dDtUltRecebEstoqueCD'
      '      ,PosicaoEstoque.dDtUltVendaEstoque'
      '      ,nQtdeDisponivel as nQtdeDisponivelReal'
      
        '      ,dbo.fn_CalculaSaldoEmpenhoProd(PosicaoEstoque.nCdProduto,' +
        ' 0, PosicaoEstoque.nCdLocalEstoque) as nQtdeEmpenhado'
      
        '      ,(nQtdeDisponivel - dbo.fn_CalculaSaldoEmpenhoProd(Posicao' +
        'Estoque.nCdProduto, 0, PosicaoEstoque.nCdLocalEstoque)) as nQtde' +
        'Disponivel'
      '      /*,IsNull((SELECT Sum(nQtdeEstoqueEmpenhado)'
      '                 FROM EstoqueEmpenhado'
      
        '                WHERE EstoqueEmpenhado.nCdProduto = PosicaoEstoq' +
        'ue.nCdProduto'
      
        '                  AND EstoqueEmpenhado.nCdLocalEstoque = Posicao' +
        'Estoque.nCdLocalEstoque),0) as nQtdeEmpenhado'
      '      '
      
        '      ,(nQtdeDisponivel - IsNull((SELECT Sum(nQtdeEstoqueEmpenha' +
        'do)'
      '                                    FROM EstoqueEmpenhado'
      
        '                                   WHERE EstoqueEmpenhado.nCdPro' +
        'duto = PosicaoEstoque.nCdProduto'
      
        '                                     AND EstoqueEmpenhado.nCdLoc' +
        'alEstoque = PosicaoEstoque.nCdLocalEstoque),0)) as nQtdeDisponiv' +
        'el*/'
      '  FROM PosicaoEstoque'
      
        '       INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque =' +
        ' PosicaoEstoque.nCdLocalEstoque'
      
        '       INNER JOIN Empresa      ON Empresa.nCdEmpresa           =' +
        ' LocalEstoque.nCdEmpresa'
      
        '       LEFT  JOIN Loja         ON Loja.nCdLoja                 =' +
        ' LocalEstoque.nCdLoja'
      
        '       INNER JOIN Produto      ON Produto.nCdProduto           =' +
        ' PosicaoEstoque.nCdProduto'
      'WHERE PosicaoEstoque.nCdProduto = :nPK'
      '')
    Left = 240
    Top = 208
    object qryPosicaoEstoquenCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa|C'#243'd'
      FieldName = 'nCdEmpresa'
    end
    object qryPosicaoEstoquecNmEmpresa: TStringField
      DisplayLabel = 'Empresa|Nome'
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryPosicaoEstoquenCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryPosicaoEstoquecNmLoja: TStringField
      DisplayLabel = 'Loja|Nome'
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryPosicaoEstoquenCdLocalEstoque: TIntegerField
      DisplayLabel = 'Local de Estoque|C'#243'd'
      FieldName = 'nCdLocalEstoque'
    end
    object qryPosicaoEstoquecNmLocalEstoque: TStringField
      DisplayLabel = 'Local de Estoque|Nome'
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryPosicaoEstoquenQtdeDisponivel: TBCDField
      DisplayLabel = 'Saldo|Dispon'#237'vel'
      FieldName = 'nQtdeDisponivel'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryPosicaoEstoquenQtdeTransito: TBCDField
      DisplayLabel = 'Saldo|em Tr'#226'nsito'
      FieldName = 'nQtdeTransito'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryPosicaoEstoquedDtUltInventario: TDateTimeField
      DisplayLabel = #218'ltimo|Invent'#225'rio'
      FieldName = 'dDtUltInventario'
    end
    object qryPosicaoEstoquenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryPosicaoEstoquecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryPosicaoEstoquecDisponivel: TStringField
      FieldName = 'cDisponivel'
      ReadOnly = True
      Size = 3
    end
    object qryPosicaoEstoquecTerceiro: TStringField
      FieldName = 'cTerceiro'
      ReadOnly = True
      Size = 3
    end
    object qryPosicaoEstoquedDtUltRecebEstoque: TDateTimeField
      FieldName = 'dDtUltRecebEstoque'
    end
    object qryPosicaoEstoquedDtUltVendaEstoque: TDateTimeField
      FieldName = 'dDtUltVendaEstoque'
    end
    object qryPosicaoEstoquenQtdeDisponivelReal: TBCDField
      FieldName = 'nQtdeDisponivelReal'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryPosicaoEstoquenQtdeEmpenhado: TBCDField
      FieldName = 'nQtdeEmpenhado'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 32
      Size = 2
    end
    object qryPosicaoEstoquedDtUltRecebEstoqueCD: TDateTimeField
      FieldName = 'dDtUltRecebEstoqueCD'
    end
  end
  object dsPosicaoEstoque: TDataSource
    DataSet = qryPosicaoEstoque
    Left = 240
    Top = 240
  end
end
