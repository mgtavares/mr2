unit fCaixa_ParcelaVencida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB;

type
  TfrmCaixa_ParcelaVencida = class(TfrmProcesso_Padrao)
    qryParcelasVencidas: TADOQuery;
    qryParcelasVencidasnCdTitulo: TIntegerField;
    qryParcelasVencidasnCdLoja: TStringField;
    qryParcelasVencidascNrTit: TStringField;
    qryParcelasVencidasiParcela: TIntegerField;
    qryParcelasVencidasdDtEmissao: TDateTimeField;
    qryParcelasVencidasdDtVenc: TDateTimeField;
    qryParcelasVencidasnValTit: TBCDField;
    qryParcelasVencidasnValJuro: TBCDField;
    qryParcelasVencidasnValDesconto: TBCDField;
    qryParcelasVencidasnSaldoTit: TBCDField;
    qryParcelasVencidasiDiasAtraso: TIntegerField;
    qryParcelasVencidascNmTerceiro: TStringField;
    qryParcelasVencidasnCdLanctoFin: TIntegerField;
    dsParcelasVencidas: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    cxGridDBTableView1cNrTit: TcxGridDBColumn;
    cxGridDBTableView1iParcela: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1nValTit: TcxGridDBColumn;
    cxGridDBTableView1nValJuro: TcxGridDBColumn;
    cxGridDBTableView1nValDesconto: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1iDiasAtraso: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn;
    function verificaParcelasAtraso():integer;
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure exibeMovimento(nCdLanctoFin: integer);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_ParcelaVencida: TfrmCaixa_ParcelaVencida;

implementation

uses fConsultaVendaProduto_Dados, fMenu;

{$R *.dfm}

{ TfrmCaixa_ParcelaVencida }

function TfrmCaixa_ParcelaVencida.verificaParcelasAtraso: integer;
begin

    qryParcelasVencidas.Close;
    qryParcelasVencidas.Open;

    result := qryParcelasVencidas.RecordCount;

end;

procedure TfrmCaixa_ParcelaVencida.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  ToolButton1.Click ;

end;

procedure TfrmCaixa_ParcelaVencida.exibeMovimento(nCdLanctoFin: integer);
var
    objForm : TfrmConsultaVendaProduto_Dados ;
begin

    try
        objForm := TfrmConsultaVendaProduto_Dados.Create( Self ) ;

        PosicionaQuery(objForm.qryItemPedido, intToStr(nCdLanctoFin)) ;
        PosicionaQuery(objForm.qryCondicao  , intToStr(nCdLanctoFin)) ;
        PosicionaQuery(objForm.qryPrestacoes, intToStr(nCdLanctoFin)) ;
        PosicionaQuery(objForm.qryCheques   , intToStr(nCdLanctoFin)) ;

        showForm( objForm , FALSE ) ;
    finally
        freeAndNil( objForm ) ;
    end ;

end;

procedure TfrmCaixa_ParcelaVencida.ToolButton1Click(Sender: TObject);
begin
  inherited;

  exibeMovimento(qryParcelasVencidasnCdLanctoFin.Value) ;

end;

end.
