unit rTituloReabilitado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, ER2Lookup, DBCtrls, DB, ADODB;

type
  TrptTituloReabilitado = class(TfrmRelatorio_Padrao)
    ER2LkpEmp: TER2LookupMaskEdit;
    Label1: TLabel;
    ER2LkpLoja: TER2LookupMaskEdit;
    ER2LkpLojaNeg: TER2LookupMaskEdit;
    ER2LpkTerceiro: TER2LookupMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryLojaCobradora: TADOQuery;
    dsLojaCobradora: TDataSource;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    MaskEdit1: TMaskEdit;
    Label15: TLabel;
    Label5: TLabel;
    MaskEdit2: TMaskEdit;
    qryTerceirocNmTerceiro: TStringField;
    qryLojaCobradoracNmLoja: TStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    qryEmpresacSigla: TStringField;
    DBEdit5: TDBEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptTituloReabilitado: TrptTituloReabilitado;

implementation

{$R *.dfm}

uses
 fMenu, rTituloReabilitado_view;

procedure TrptTituloReabilitado.ToolButton1Click(Sender: TObject);
var
  objRel : TrptTituloReabilitado_view;
begin
  try
      try
          objRel := TrptTituloReabilitado_view.Create(nil);

          objRel.qryTitulos.Close;
          objRel.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.ConvInteiro(ER2LkpEmp.Text);
          objRel.qryTitulos.Parameters.ParamByName('nCdLoja').Value     := frmMenu.ConvInteiro(ER2LkpLoja.Text);
          objRel.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value  := frmMenu.ConvInteiro(ER2LkpLojaNeg.Text);
          objRel.qryTitulos.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(ER2LpkTerceiro.Text);
          objRel.qryTitulos.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text);
          objRel.qryTitulos.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text);
          objRel.qryTitulos.Open;

          if (Trim(DBedit1.Text) <> '') then
              objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(ER2LkpEmp.Text) + '-' + DBEdit1.Text;

          if (Trim(DBEdit2.Text) <> '') then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja: ' + Trim(ER2LkpLoja.Text) + '-' + DBEdit2.Text;

          if (Trim(DBEdit3.Text) <> '') then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja Cobradora: ' + Trim(ER2LkpLojaNeg.Text) + '-' + DBEdit3.Text;

          if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Reabilita��o: ' + Trim(MaskEdit1.Text) + ' � ' + Trim(MaskEdit2.Text) ;

          if (Trim(DBEdit4.Text) <> '') then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Cliente: ' + Trim(ER2LpkTerceiro.Text) + '-' + DBEdit4.Text;

          objRel.lblEmpresa.Caption := DBEdit1.Text;
          
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptTituloReabilitado.FormActivate(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));

  ER2LkpEmp.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  
  ER2LkpLoja.SetFocus;
end;

initialization
  RegisterClass(TrptTituloReabilitado) ;
end.
