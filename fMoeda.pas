unit fMoeda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, DBGridEhGrouping, cxPC,
  cxControls, ImgList;

type
  TfrmMoeda = class(TfrmCadastro_Padrao)
    qryMasternCdMoeda: TIntegerField;
    qryMastercSigla: TStringField;
    qryMastercNmMoeda: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryCotacaoMoeda: TADOQuery;
    dsCotacaoMoeda: TDataSource;
    qryCotacaoMoedanCdMoeda: TIntegerField;
    qryCotacaoMoedadDtCotacao: TDateTimeField;
    qryCotacaoMoedanValCotacao: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryCotacaoMoedaBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMoeda: TfrmMoeda;

implementation

{$R *.dfm}

procedure TfrmMoeda.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmMoeda.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryCotacaoMoeda.Close ;
  qryCotacaoMoeda.Parameters.ParamByName('nCdMoeda').Value := qryMasternCdMoeda.Value ;
  qryCotacaoMoeda.Open ;

end;

procedure TfrmMoeda.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCotacaoMoeda.Close ;

end;

procedure TfrmMoeda.qryCotacaoMoedaBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryCotacaoMoedanCdMoeda.Value := qryMaster.FieldList[0].Value ;
  inherited;
end;

procedure TfrmMoeda.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'MOEDA' ;
  nCdTabelaSistema  := 12 ;
  nCdConsultaPadrao := 16 ;
  inherited;

end;

initialization
    RegisterClass(tFrmMoeda) ;

end.
