unit rOrcamentoPrevReal_View_Trimestral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptOrcamentoPrevReal_View_Trimestral = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTRE: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRLabel11: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel59: TQRLabel;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREcNmOrcamento: TStringField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREcNmCC: TStringField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnCdGrupoPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREcNmGrupoPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnCdPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREcNmPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValPrev1Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValReal1Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnPerc1Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValPrev2Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValReal2Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnPerc2Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValPrev3Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValReal3Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnPerc3Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValPrev4Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValReal4Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnPerc4Trim: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValPrevTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnValRealTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_TRIMESTREnPercTotal: TFloatField;
    QRDBText3: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptOrcamentoPrevReal_View_Trimestral: TrptOrcamentoPrevReal_View_Trimestral;

implementation

{$R *.dfm}

procedure TrptOrcamentoPrevReal_View_Trimestral.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
