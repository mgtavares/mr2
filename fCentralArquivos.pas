unit fCentralArquivos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Menus;

type
  TfrmCentralArquivos = class(TfrmProcesso_Padrao)
    qryArquivoDigital: TADOQuery;
    qryArquivoDigitalnCdArquivoDigital: TAutoIncField;
    qryArquivoDigitalcFonte: TStringField;
    qryArquivoDigitaliID: TIntegerField;
    qryArquivoDigitalcNmArquivo: TStringField;
    qryArquivoDigitaldDtArquivo: TDateTimeField;
    qryArquivoDigitalcDescricao: TStringField;
    qryArquivoDigitalcNmUsuario: TStringField;
    dsArquivoDigital: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdArquivoDigital: TcxGridDBColumn;
    cxGrid1DBTableView1cFonte: TcxGridDBColumn;
    cxGrid1DBTableView1iID: TcxGridDBColumn;
    cxGrid1DBTableView1cNmArquivo: TcxGridDBColumn;
    cxGrid1DBTableView1dDtArquivo: TcxGridDBColumn;
    cxGrid1DBTableView1cDescricao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    RestaurarArquivo1: TMenuItem;
    SaveDialog1: TSaveDialog;
    qryConteudoArquivo: TADOQuery;
    qryConteudoArquivocArquivo: TBlobField;
    PostarNovoArquivo1: TMenuItem;
    OpenDialog1: TOpenDialog;
    qryNovoArquivo: TADOQuery;
    qryNovoArquivonCdArquivoDigital: TAutoIncField;
    qryNovoArquivocFonte: TStringField;
    qryNovoArquivoiID: TIntegerField;
    qryNovoArquivocNmArquivo: TStringField;
    qryNovoArquivodDtArquivo: TDateTimeField;
    qryNovoArquivocDescricao: TStringField;
    qryNovoArquivocArquivo: TBlobField;
    qryNovoArquivonCdUsuario: TIntegerField;
    procedure GerenciaArquivos(cFonte:String;iID:integer);
    procedure RestaurarArquivo1Click(Sender: TObject);
    function LoadFromBlob(const AField: TField;
  const Stream: TStream): boolean;
    procedure ToolButton2Click(Sender: TObject);
    function RightStr
    (Const Str: String; Size: Word): String;
    procedure PostarNovoArquivo1Click(Sender: TObject);
    function SaveToBlob(const Stream: TStream; const AField: TField): boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCentralArquivos: TfrmCentralArquivos;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmCentralArquivos }

procedure TfrmCentralArquivos.GerenciaArquivos(cFonte: String;
  iID: integer);
begin

    qryArquivoDigital.Close ;
    qryArquivoDigital.Parameters.ParamByName('cFonte').Value := cFonte ;
    qryArquivoDigital.Parameters.ParamByName('iID').Value    := iID ;
    qryArquivoDigital.Open ;

    Self.ShowModal ;

end;

function TfrmCentralArquivos.LoadFromBlob(const AField: TField;
  const Stream: TStream): boolean;
var
  ResultStr: string;
  PResultStr: PChar;
begin
  Result := false;
  if (Assigned(AField)) and (Assigned(Stream)) then begin
    try
      ResultStr := AField.Value;
      PResultStr := PChar(ResultStr);
      Stream.Write(PResultStr^, Length(ResultStr));
      Stream.Seek(0,0);
      Result := true;
    except
    end;
  end;

end;

procedure TfrmCentralArquivos.RestaurarArquivo1Click(Sender: TObject);
var
  FS: TFileStream;
begin
  inherited;

  if (qryArquivoDigitalnCdArquivoDigital.Value > 0) then
  begin
      SaveDialog1.FileName := qryArquivoDigitalcNmArquivo.Value ;
      SaveDialog1.DefaultExt := RightStr(qryArquivoDigitalcNmarquivo.Value,4) ;
      if (SaveDialog1.Execute) then
      begin

          PosicionaQuery(qryConteudoArquivo, qryArquivoDigitalnCdArquivoDigital.AsString) ;

          if not qryConteudoArquivo.Eof then
          begin
              FS := TFileStream.Create(SaveDialog1.FileName, fmCreate);
              LoadFromBlob(qryConteudoArquivo.FieldByName('cArquivo'), FS);
              FS.Free;

              ShowMessage('Arquivo gerado com sucesso!') ;
          end ;

          qryConteudoArquivo.Close ;

      end ;
  end ;

end;

function TfrmCentralArquivos.RightStr(const Str: String;
  Size: Word): String;
begin
  if Size > Length(Str) then Size := Length(Str) ;
  RightStr := Copy(Str, Length(Str)-Size+1, Size);
end;

procedure TfrmCentralArquivos.ToolButton2Click(Sender: TObject);
begin
  qryConteudoArquivo.Close ;
  qryArquivoDigital.Close ;

  inherited;

end;

procedure TfrmCentralArquivos.PostarNovoArquivo1Click(Sender: TObject);
var
  sStream: TFileStream  ;
  Blob: TStream;
  cDescricao : String ;
begin

    if (OpenDialog1.Execute) then
    begin

        qryNovoArquivo.Close ;
        qryNovoArquivo.Open ;

        qryNovoArquivo.Insert ;

        sStream := TFileStream.Create(OpenDialog1.FileName, fmOpenRead);

        InputQuery('Descri��o do Arquivo','Descri��o Resumida',cDescricao);

        SaveToBlob(sStream,qryNovoArquivo.FieldByName('cArquivo')) ;

        qryNovoArquivocNmArquivo.Value        := ExtractFileName(OpenDialog1.FileName) ;
        qryNovoArquivocFonte.Value            := qryArquivoDigital.Parameters.ParamByName('cFonte').Value ;
        qryNovoArquivoiID.Value               := qryArquivoDigital.Parameters.ParamByName('iID').Value ;
        qryNovoArquivodDtArquivo.Value        := Now() ;
        qryNovoArquivonCdUsuario.Value        := frmMenu.nCdUsuarioLogado ;
        qryNovoArquivonCdArquivoDigital.Value := frmMenu.fnProximoCodigo('ARQUIVODIGITAL') ;


        if (MessageDlg('Confirma a grava��o deste arquivo ?',mtConfirmation,[mbYes,mbNo],0)=MRYes) then
        begin
            qryNovoArquivocDescricao.Value := Uppercase(cDescricao) ;
            qryNovoArquivo.Post ;

            qryArquivoDigital.Close ;
            qryArquivoDigital.Open ;
        end ;

    end ;

end;

function TfrmCentralArquivos.SaveToBlob(const Stream: TStream;
  const AField: TField): boolean;
var
  FieldStr: string;
  PFieldStr: PChar;
begin
  Result := false;
  if (Assigned(AField)) and (Assigned(Stream)) then begin
    try
      Stream.Seek(0,0);
      SetLength(FieldStr, Stream.Size);
      PFieldStr := PChar(FieldStr);
      Stream.Read(PFieldStr^, Stream.Size);
      AField.Value := FieldStr;
      Result := true;
    except
    end;
  end;

end;

end.
