inherited frmMotBaixaBemPatrim: TfrmMotBaixaBemPatrim
  Left = 136
  Top = 138
  Caption = 'Motivo de Baixa Bem Patrimonial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 52
    Top = 39
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 6
    Top = 64
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Classe'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 33
    Width = 65
    Height = 19
    DataField = 'nCdMotBaixaBemPatrim'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 58
    Width = 650
    Height = 19
    DataField = 'cNmMotBaixaBemPatrim'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MotBaixaBemPatrim'
      'WHERE nCdMotBaixaBemPatrim = :nPK'
      ''
      '')
    object qryMasternCdMotBaixaBemPatrim: TIntegerField
      FieldName = 'nCdMotBaixaBemPatrim'
    end
    object qryMastercNmMotBaixaBemPatrim: TStringField
      FieldName = 'cNmMotBaixaBemPatrim'
      Size = 50
    end
  end
end
