object rptBorderoCaixaNovo: TrptBorderoCaixaNovo
  Left = 192
  Top = 122
  Width = 979
  Height = 563
  Caption = 'Bordero de Caixa Novo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object qryResumoCaixa: TADOQuery
    Parameters = <>
    Left = 144
    Top = 144
  end
  object qryResumoFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 192
    Top = 144
  end
  object qryLanctoManual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 248
    Top = 144
  end
  object SPREL_BORDEROINDIV_CABECALHO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_BORDEROINDIV_CABECALHO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdResumoCaixa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 168
    Top = 224
    object SPREL_BORDEROINDIV_CABECALHOcNmCaixa: TStringField
      FieldName = 'cNmCaixa'
      ReadOnly = True
      Size = 31
    end
    object SPREL_BORDEROINDIV_CABECALHOcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object SPREL_BORDEROINDIV_CABECALHOcIntervalo: TStringField
      FieldName = 'cIntervalo'
      ReadOnly = True
      Size = 25
    end
    object SPREL_BORDEROINDIV_CABECALHOnSaldoAbertura: TBCDField
      FieldName = 'nSaldoAbertura'
      Precision = 12
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValMovimentacao: TBCDField
      FieldName = 'nValMovimentacao'
      ReadOnly = True
      Precision = 13
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnSaldoFechamento: TBCDField
      FieldName = 'nSaldoFechamento'
      Precision = 12
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValSangria: TBCDField
      FieldName = 'nValSangria'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValSuprimento: TBCDField
      FieldName = 'nValSuprimento'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValSaidaDeposito: TBCDField
      FieldName = 'nValSaidaDeposito'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValOutrosCreditos: TBCDField
      FieldName = 'nValOutrosCreditos'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValOutrosDebitos: TBCDField
      FieldName = 'nValOutrosDebitos'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValEstornoVenda: TBCDField
      FieldName = 'nValEstornoVenda'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoVenda: TIntegerField
      FieldName = 'iQtdeEstornoVenda'
      ReadOnly = True
    end
    object SPREL_BORDEROINDIV_CABECALHOnValEstornoRecParc: TBCDField
      FieldName = 'nValEstornoRecParc'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoRecParc: TIntegerField
      FieldName = 'iQtdeEstornoRecParc'
      ReadOnly = True
    end
    object SPREL_BORDEROINDIV_CABECALHOnValEstornoOutros: TBCDField
      FieldName = 'nValEstornoOutros'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoOutros: TIntegerField
      FieldName = 'iQtdeEstornoOutros'
      ReadOnly = True
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValeMercEmitido: TBCDField
      FieldName = 'nValValeMercEmitido'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValeMercRecebido: TBCDField
      FieldName = 'nValValeMercRecebido'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValeMercRecomprado: TBCDField
      FieldName = 'nValValeMercRecomprado'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValePresenteEmitido: TBCDField
      FieldName = 'nValValePresenteEmitido'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValePresenteRecebido: TBCDField
      FieldName = 'nValValePresenteRecebido'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValeDescontoEmitido: TBCDField
      FieldName = 'nValValeDescontoEmitido'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object SPREL_BORDEROINDIV_CABECALHOnValValeDescontoRecebido: TBCDField
      FieldName = 'nValValeDescontoRecebido'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
  end
  object SPREL_BORDEROINDIV_RESUMOFORMA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_BORDEROINDIV_RESUMOFORMA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdResumoCaixa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 208
    Top = 224
    object SPREL_BORDEROINDIV_RESUMOFORMAnCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
    object SPREL_BORDEROINDIV_RESUMOFORMAcNmTabTipoFormaPagto: TStringField
      FieldName = 'cNmTabTipoFormaPagto'
      Size = 50
    end
    object SPREL_BORDEROINDIV_RESUMOFORMAnValCalculado: TBCDField
      FieldName = 'nValCalculado'
      Precision = 12
      Size = 2
    end
    object SPREL_BORDEROINDIV_RESUMOFORMAnValInformado: TBCDField
      FieldName = 'nValInformado'
      Precision = 12
      Size = 2
    end
    object SPREL_BORDEROINDIV_RESUMOFORMAnValDiferenca: TBCDField
      FieldName = 'nValDiferenca'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object SPREL_BORDEROINDIV_LANCTOMANUAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_BORDEROINDIV_LANCTOMANUAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdResumoCaixa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 256
    Top = 224
    object SPREL_BORDEROINDIV_LANCTOMANUALnCdLanctoFin: TStringField
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
      Size = 15
    end
    object SPREL_BORDEROINDIV_LANCTOMANUALdDtLancto: TStringField
      FieldName = 'dDtLancto'
      ReadOnly = True
      Size = 11
    end
    object SPREL_BORDEROINDIV_LANCTOMANUALnValLancto: TBCDField
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
    object SPREL_BORDEROINDIV_LANCTOMANUALcNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      ReadOnly = True
      Size = 103
    end
  end
  object RDprint1: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Border'#244' de Caixa'
    OpcoesPreview.PreviewZoom = -1
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = True
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Invisivel
    OpcoesPreview.BotaoLer = Invisivel
    OpcoesPreview.BotaoProcurar = Invisivel
    Margens.Left = 2
    Margens.Right = 2
    Margens.Top = 2
    Margens.Bottom = 2
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'EDGAR DE SOUZA'
    RegistroUsuario.SerieProduto = 'SINGLE-0708/01624'
    RegistroUsuario.AutorizacaoKey = 'BWVI-4846-SAHU-8864-PAPQ'
    About = 'RDprint 4.0e - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = True
    CorForm = clWhite
    CorFonte = clBlack
    Impressora = HP
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    OnNewPage = RDprint1NewPage
    Left = 424
    Top = 224
  end
end
