unit rFluxoCaixaRealizadoAnalitico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCheckBox, ER2Excel;

type
  TrptFluxoCaixaRealizadoAnalitico = class(TfrmRelatorio_Padrao)
    qryTempResumoContas: TADOQuery;
    qryTempResumoContascFlgOK: TIntegerField;
    qryTempResumoContascTipo: TStringField;
    qryTempResumoContascNmGrupoPlanoConta: TStringField;
    qryTempResumoContasnCdPlanoConta: TIntegerField;
    qryTempResumoContascNmPlanoConta: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsTempResumoContas: TDataSource;
    cxGrid1DBTableView1cFlgOK: TcxGridDBColumn;
    cxGrid1DBTableView1cTipo: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryAux: TADOQuery;
    qryResultadoPlanilha: TADOQuery;
    qryResultadoPlanilhanCdPlanoConta: TIntegerField;
    qryResultadoPlanilhacNmPlanoConta: TStringField;
    qryResultadoPlanilhadDtLancto: TDateTimeField;
    qryResultadoPlanilhanCdLanctoFin: TIntegerField;
    qryResultadoPlanilhacNrDocto: TStringField;
    qryResultadoPlanilhacNrNF: TStringField;
    qryResultadoPlanilhanCdSP: TIntegerField;
    qryResultadoPlanilhacNmTerceiro: TStringField;
    qryResultadoPlanilhanValLancto: TBCDField;
    qryResultadoPlanilhacFlgManual: TStringField;
    qryResultadoPlanilhacHistorico: TStringField;
    qryResultadoPlanilhacNmUsuarioLiquidacao: TStringField;
    qryResultadoPlanilhaiNrCheque: TIntegerField;
    qryResultadoPlanilhanCdContaLiquidacao: TStringField;
    qryResultadoPlanilhanCdLojaPG: TStringField;
    qryResultadoPlanilhanCdLojaEmi: TStringField;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ER2Excel1: TER2Excel;
    qryResultadoPlanilhacNmCC: TStringField;
    qryResultadoPlanilhacMascaraConta: TStringField;
    qryResultadoPlanilhanCdTitulo: TIntegerField;
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cFiltro : String ;
  end;

var
  rptFluxoCaixaRealizadoAnalitico: TrptFluxoCaixaRealizadoAnalitico;

implementation

uses rFluxoCaixaRealizadoAnalitico_view , fMenu;

{$R *.dfm}

procedure TrptFluxoCaixaRealizadoAnalitico.ToolButton4Click(
  Sender: TObject);
begin
  inherited;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempResumoContasFCR Set cFlgOK = 1') ;
  qryAux.ExecSQL;

  qryTempResumoContas.Close;
  qryTempResumoContas.Open;

  cxGrid1DBTableView1.ViewData.Expand( TRUE );

end;

procedure TrptFluxoCaixaRealizadoAnalitico.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #TempResumoContasFCR Set cFlgOK = 0') ;
  qryAux.ExecSQL;

  qryTempResumoContas.Close;
  qryTempResumoContas.Open;

  cxGrid1DBTableView1.ViewData.Expand( TRUE );
  
end;

procedure TrptFluxoCaixaRealizadoAnalitico.ToolButton1Click(
  Sender: TObject);
var
  objRel : TrptFluxoCaixaRealizadoAnalitico_view ;
begin
  inherited;

  if (qryTempResumoContas.State = dsEdit) then
      qryTempResumoContas.Post ;

  objRel := TrptFluxoCaixaRealizadoAnalitico_view.Create( Self ) ;

  try

      objRel.qryResultado.Close;
      objRel.qryResultado.Open;
      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRel.lblFiltro.Caption  := cFiltro;


      objRel.QuickRep1.PreviewModal;

  finally

      freeAndNil( objRel ) ;

  end ;

end;

procedure TrptFluxoCaixaRealizadoAnalitico.ToolButton7Click(
  Sender: TObject);
var
  iLinhaAux, iLinha: integer;
  nPeriodo         : integer;
  iPeriodo         : integer;
  nValTotalPagar   : Double;
  nValTotalReceber : Double;
begin

  inherited;

  if (qryTempResumoContas.State = dsEdit) then
      qryTempResumoContas.Post ;

  qryResultadoPlanilha.Close;
  qryResultadoPlanilha.Open;

  {inicia a gera��o da planilha}

  ER2Excel1.Celula['A1'].Width := 2;
  ER2Excel1.Celula['B2'].Mesclar('D2');
  ER2Excel1.Celula['B2'].HorizontalAlign := haLeft;
  ER2Excel1.Celula['B2'].Text := 'FLUXO DE CAIXA REALIZADO';

  iLinha := 3;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text            := 'Conta Cont�bil';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width           := 35;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['C' + IntToStr(iLinha)].Text            := 'Data Lancto';
  ER2Excel1.Celula['C' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['D' + IntToStr(iLinha)].Text            := 'Hist�rico';
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := 'Valor';
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := 'Lancto. Fin.';
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['G' + IntToStr(iLinha)].Text            := 'Nr. Docto.';
  ER2Excel1.Celula['G' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := 'Terceiro';
  ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 20;
  ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['I' + IntToStr(iLinha)].Text            := 'Nr. NF';
  ER2Excel1.Celula['I' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['J' + IntToStr(iLinha)].Text            := 'Nr. SP';
  ER2Excel1.Celula['J' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['J' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['K' + IntToStr(iLinha)].Text            := 'ID T�tulo';
  ER2Excel1.Celula['K' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['K' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['L' + IntToStr(iLinha)].Text            := 'Nr. Cheque';
  ER2Excel1.Celula['L' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['L' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['M' + IntToStr(iLinha)].Text            := 'Loja Emi.';
  ER2Excel1.Celula['M' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['M' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['N' + IntToStr(iLinha)].Text            := 'Loja Pagto';
  ER2Excel1.Celula['N' + IntToStr(iLinha)].Width           := 10;
  ER2Excel1.Celula['N' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['O' + IntToStr(iLinha)].Text            := 'Conta Banc�ria';
  ER2Excel1.Celula['O' + IntToStr(iLinha)].Width           := 20;
  ER2Excel1.Celula['O' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['P' + IntToStr(iLinha)].Text            := 'Usu�rio';
  ER2Excel1.Celula['P' + IntToStr(iLinha)].Width           := 20;
  ER2Excel1.Celula['P' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text            := 'Centro Custo';
  ER2Excel1.Celula['Q' + IntToStr(iLinha)].Width           := 20;
  ER2Excel1.Celula['Q' + IntToStr(iLinha)].HorizontalAlign := haLeft;

  iLinha := iLinha + 1;

  while not qryResultadoPlanilha.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacMascaraConta.Value ;
      ER2Excel1.Celula['A' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['A' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNmPlanoConta.Value;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Width           := 35;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text            := qryResultadoPlanilhadDtLancto.asString;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacHistorico.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanValLancto.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdLanctoFin.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNrDocto.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNmTerceiro.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 20;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['I' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNrNF.Value;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['J' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdSP.Value;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['K' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdTitulo.Value;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['L' + IntToStr(iLinha)].Text            := qryResultadoPlanilhaiNrCheque.Value;
      ER2Excel1.Celula['L' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['L' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['M' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdLojaEmi.Value;
      ER2Excel1.Celula['M' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['M' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['N' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdLojaPG.Value;
      ER2Excel1.Celula['N' + IntToStr(iLinha)].Width           := 10;
      ER2Excel1.Celula['N' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['O' + IntToStr(iLinha)].Text            := qryResultadoPlanilhanCdContaLiquidacao.Value;
      ER2Excel1.Celula['O' + IntToStr(iLinha)].Width           := 20;
      ER2Excel1.Celula['O' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['P' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNmUsuarioLiquidacao.Value;
      ER2Excel1.Celula['P' + IntToStr(iLinha)].Width           := 20;
      ER2Excel1.Celula['P' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text            := qryResultadoPlanilhacNmCC.Value;
      ER2Excel1.Celula['Q' + IntToStr(iLinha)].Width           := 20;
      ER2Excel1.Celula['Q' + IntToStr(iLinha)].HorizontalAlign := haLeft;

      iLinha := iLinha + 1;

      qryResultadoPlanilha.Next;
  end;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

end.
