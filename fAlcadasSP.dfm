inherited frmAlcadasSP: TfrmAlcadasSP
  Left = 205
  Top = 174
  Caption = 'Al'#231'adas SP'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    DataSource = dsAlcadaSP
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Title.Caption = 'Empresa|C'#243'digo'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Empresa|Nome'
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Title.Caption = 'Loja|C'#243'digo'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Loja|Nome'
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuario'
        Footers = <>
        Title.Caption = 'Usu'#225'rio|C'#243'digo'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Usu'#225'rio|Nome'
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoSP'
        Footers = <>
        Title.Caption = 'Tipo SP|C'#243'digo'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoSP'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Tipo SP|Descri'#231#227'o'
      end
      item
        EditButtons = <>
        FieldName = 'nValAlcada'
        Footers = <>
      end>
  end
  inherited ImageList1: TImageList
    Left = 744
  end
  object qryAlcadasSP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryAlcadasSPBeforePost
    OnCalcFields = qryAlcadasSPCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM AlcadaAutorizSP ')
    Left = 552
    Top = 100
    object qryAlcadasSPnCdEmpresa: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdEmpresa'
    end
    object qryAlcadasSPnCdLoja: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdLoja'
    end
    object qryAlcadasSPnCdUsuario: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdUsuario'
    end
    object qryAlcadasSPnCdTipoSP: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdTipoSP'
    end
    object qryAlcadasSPnValAlcada: TBCDField
      DisplayLabel = 'Valor Al'#231'ada'
      FieldName = 'nValAlcada'
      Precision = 12
      Size = 2
    end
    object qryAlcadasSPcNmTipoSP: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTipoSP'
      Size = 30
      Calculated = True
    end
    object qryAlcadasSPcNmUsuario: TStringField
      DisplayLabel = 'Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 30
      Calculated = True
    end
    object qryAlcadasSPcNmEmpresa: TStringField
      DisplayLabel = 'Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 30
      Calculated = True
    end
    object qryAlcadasSPcNmLoja: TStringField
      DisplayLabel = 'Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 30
      Calculated = True
    end
    object qryAlcadasSPnCdAlcadaAutorizSP: TIntegerField
      FieldName = 'nCdAlcadaAutorizSP'
    end
  end
  object dsAlcadaSP: TDataSource
    DataSet = qryAlcadasSP
    Left = 592
    Top = 100
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPk')
    Left = 552
    Top = 140
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPk')
    Left = 552
    Top = 180
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'Select nCdUsuario'
      '         , cNmUsuario '
      'FROM Usuario'
      'WHERE nCdUsuario = :nPk')
    Left = 552
    Top = 220
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryTipoSP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoSP'
      '             , cNmTipoSP'
      'FROM TipoSP'
      'WHERE nCdTipoSP = :nPk')
    Left = 552
    Top = 260
    object qryTipoSPnCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryTipoSPcNmTipoSP: TStringField
      FieldName = 'cNmTipoSP'
      Size = 50
    end
  end
end
