unit rHistoricoEmpenho;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, ER2Lookup, DB, ADODB, DBCtrls, ER2Excel;

type
  TrRelHistoricoEmpenho = class(TfrmRelatorio_Padrao)
    er2LkpEstoqueOrig: TER2LookupMaskEdit;
    er2LkpEstoqueDest: TER2LookupMaskEdit;
    er2LkpStatus: TER2LookupMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RadioGroup1: TRadioGroup;
    qryLocalOrigem: TADOQuery;
    qryLocalDestino: TADOQuery;
    qryLocalDestinocNmLocalEstoque: TStringField;
    qryLocalOrigemcNmLocalEstoque: TStringField;
    DBEdit1: TDBEdit;
    dsLocalOrigem: TDataSource;
    DBEdit2: TDBEdit;
    dsLocalDestino: TDataSource;
    qryStatus: TADOQuery;
    DBEdit3: TDBEdit;
    DataSource1: TDataSource;
    qryStatuscNmStatus: TStringField;
    edtDtFinal: TMaskEdit;
    Label6: TLabel;
    edtDtInicial: TMaskEdit;
    Label4: TLabel;
    rgModeloImp: TRadioGroup;
    ER2Excel1: TER2Excel;
    procedure er2LkpEstoqueOrigExit(Sender: TObject);
    procedure er2LkpEstoqueDestExit(Sender: TObject);
    procedure er2LkpStatusExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rRelHistoricoEmpenho: TrRelHistoricoEmpenho;

implementation
uses
  rHistoricoEmpenho_view, fMenu, DateUtils;
{$R *.dfm}

procedure TrRelHistoricoEmpenho.er2LkpEstoqueOrigExit(Sender: TObject);
begin
  inherited;
  qryLocalOrigem.Close;
  PosicionaQuery(qryLocalOrigem, er2LkpEstoqueOrig.Text);
end;

procedure TrRelHistoricoEmpenho.er2LkpEstoqueDestExit(Sender: TObject);
begin
  inherited;
  qryLocalDestino.Close;
  PosicionaQuery(qryLocalDestino, er2LkpEstoqueDest.Text);
end;

procedure TrRelHistoricoEmpenho.er2LkpStatusExit(Sender: TObject);
begin
  inherited;
  qryStatus.Close;
  PosicionaQuery(qryStatus, er2LkpStatus.Text);
end;

procedure TrRelHistoricoEmpenho.ToolButton1Click(Sender: TObject);
var
  objForm : TrptHistoricoEmpenho_view;
  iLinha  : Integer;
  cMsg    : String;
begin
  inherited;
  
  {if (trim(DBEdit1.Text) = '') then
  begin
      ShowMessage('Informe o local de estoque origem.');
      er2LkpEstoqueOrig.SetFocus;
      Abort;
  end;

  if (trim(DBEdit2.Text) = '') then
  begin
      ShowMessage('Informe o local de estoque destino.');
      er2LkpEstoqueDest.SetFocus;
      Abort;
  end;}

  if edtDtInicial.Text = '  /  /    ' then
  begin
      MensagemAlerta('Infome o per�odo inicial do empenho.');
      edtDtInicial.SetFocus;
      Abort;
  end;

  if edtDtFinal.Text = '  /  /    ' then
  begin
      MensagemAlerta('Infome o per�odo final do empenho.');
      edtDtFinal.SetFocus;
      Abort;
  end;

  if edtDtInicial.Text > edtDtFinal.Text then
  begin
      MensagemAlerta('Data inicial informada maior do que a data final.');
      edtDtInicial.SetFocus;
      Abort;
  end;

  if (DaysBetween(StrToDate(edtDtInicial.Text),StrToDate(edtDtFinal.Text))) > 60 then
  begin
      MensagemAlerta('Per�odo informado superior a 60 dias.');
      edtDtInicial.SetFocus;
      Abort;
  end;

  try
      objForm := TrptHistoricoEmpenho_view.Create(Nil);

      {-- Imprime Filtros --}
      objForm.lblFiltro1.Caption := DBEdit1.Text      + ' / '
                                  + DBEdit2.Text      + ' / '
                                  + DBEdit3.Text      + ' / '
                                  + edtDtInicial.Text + ' / '
                                  + edtDtFinal.Text   + ' / '
                                  + UpperCase(RadioGroup1.Items.ValueFromIndex[RadioGroup1.ItemIndex]);


      objForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      objForm.qryEstoqueEmpenhado.Close;
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('nCdLocalEstoqueOrigem').Value   := frmMenu.ConvInteiro(er2LkpEstoqueOrig.Text);
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('nCdLocalEstoqueDestino').Value  := frmMenu.ConvInteiro(er2LkpEstoqueDest.Text);
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('nCdStatus').Value               := frmMenu.ConvInteiro(er2LkpStatus.Text);
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('nCdTabTipoOrigemEmpenho').Value := RadioGroup1.ItemIndex;
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('dDtIni').Value                  := frmMenu.ConvData(edtDtInicial.Text);
      objForm.qryEstoqueEmpenhado.Parameters.ParamByName('dDtFim').Value                  := frmMenu.ConvData(edtDtFinal.Text);
      objForm.qryEstoqueEmpenhado.Open;

      if objForm.qryEstoqueEmpenhado.IsEmpty then
      begin
          MensagemAlerta('Nenhum registro encontrado.');
          Abort;
      end;

      case (rgModeloImp.ItemIndex)of
          0 : objForm.QuickRep1.PreviewModal;
          1 : begin

                  frmMenu.mensagemUsuario('Exportando Planilha...');

                  {-- Formata��o --}
                  ER2Excel1.Titulo := 'Rel. Hist�rico de Empenho';

                  ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
                  ER2Excel1.Celula['A1'].Negrito;
                  ER2Excel1.Celula['A1'].Range('W6');
                  ER2Excel1.Celula['A1'].Congelar('A7');

                  ER2Excel1.Celula['B6'].Mesclar('D6');
                  ER2Excel1.Celula['E6'].Mesclar('G6');
                  ER2Excel1.Celula['H6'].Mesclar('J6');
                  ER2Excel1.Celula['K6'].Mesclar('L6');
                  ER2Excel1.Celula['S6'].Mesclar('W6');

                  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                  ER2Excel1.Celula['A2'].Text := 'Rel. Hist�rico de Empenho';
                  ER2Excel1.Celula['A4'].Text := 'Estoque Empenhado: ' + qryLocalOrigemcNmLocalEstoque.Value;
                  ER2Excel1.Celula['E4'].Text := 'Estoque Requisitante: ' + qryLocalDestinocNmLocalEstoque.Value;
                  ER2Excel1.Celula['I4'].Text := 'Dt. Inicial: ' + edtDtInicial.Text + ' Dt. Final: ' + edtDtFinal.Text;
                  ER2Excel1.Celula['M4'].Text := 'Origem Empenho: ' + UpperCase(RadioGroup1.Items.ValueFromIndex[RadioGroup1.ItemIndex]);
                  ER2Excel1.Celula['O4'].Text := 'Status: ' + qryStatuscNmStatus.Value;

                  ER2Excel1.Celula['A6'].Text := 'C�d. Emp.';
                  ER2Excel1.Celula['B6'].Text := 'Produto';
                  ER2Excel1.Celula['E6'].Text := 'Estoque Empenhado';
                  ER2Excel1.Celula['H6'].Text := 'Estoque Requisitante';
                  ER2Excel1.Celula['K6'].Text := 'Origem';
                  ER2Excel1.Celula['M6'].Text := 'Dt. Empenho';
                  ER2Excel1.Celula['N6'].Text := 'Real';
                  ER2Excel1.Celula['O6'].Text := 'Empenhado';
                  ER2Excel1.Celula['P6'].Text := 'Conferido';
                  ER2Excel1.Celula['Q6'].Text := 'Dt. Cancel';
                  ER2Excel1.Celula['R6'].Text := 'Usu�rio';
                  ER2Excel1.Celula['S6'].Text := 'Motivo Cancel';

                  iLinha := 7;

                  objForm.qryEstoqueEmpenhado.First;

                  while (not objForm.qryEstoqueEmpenhado.Eof) do
                  begin
                      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadonCdEstoqueEmpenhado.AsString;
                      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadonCdProduto.Value;
                      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocNmProduto.Value;
                      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocNmLocalEstoque.Value;
                      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocNmLocalEstoqueOrigem.Value;
                      ER2Excel1.Celula['K' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocNmTabTipoOrigemEmpenho.Value;
                      ER2Excel1.Celula['M' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadodDtEmpenho.Value;
                      ER2Excel1.Celula['N' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadonQtdeReal.Value;
                      ER2Excel1.Celula['O' + IntToStr(iLinha)].Text := Trunc(objForm.qryEstoqueEmpenhadonQtdeEstoqueEmpenhado.Value);
                      ER2Excel1.Celula['P' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadonQtdeConf.Value;
                      ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadodDtCancel.Value;
                      ER2Excel1.Celula['R' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocNmUsuario.Value;
                      ER2Excel1.Celula['S' + IntToStr(iLinha)].Text := objForm.qryEstoqueEmpenhadocMotivoCancel.Value;

                      objForm.qryEstoqueEmpenhado.Next;

                      Inc(iLinha);
                  end;

                  ER2Excel1.ExportXLS;
                  ER2Excel1.CleanupInstance;

                  frmMenu.mensagemUsuario('');
          end;
      end;
  finally
      FreeAndNil(objForm);
  end;
end;

procedure TrRelHistoricoEmpenho.FormShow(Sender: TObject);
begin
  inherited;

  er2LkpStatus.Text := '1';

  PosicionaQuery(qryStatus, '1');
end;

Initialization
    RegisterClass(TrRelHistoricoEmpenho);

end.
