unit rTituloNegativado_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptTituloNegativado_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    qryTitulos: TADOQuery;
    QRLabel1: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel20: TQRLabel;
    qryTitulosnCdTerceiro: TStringField;
    qryTituloscCNPJCPF: TStringField;
    qryTituloscRG: TStringField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosdDtNegativacao: TDateTimeField;
    qryTitulosnCdTitulo: TStringField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TStringField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnCdLojaTit: TStringField;
    qryTitulosnCdLojaNeg: TStringField;
    QRDBText1: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel22: TQRLabel;
    qryTitulosdDtNasc: TDateTimeField;
    qryTitulosdDtReabNe: TDateTimeField;
    QRSubDetail1: TQRSubDetail;
    QRDBText17: TQRDBText;
    qryTituloscEndereco: TStringField;
    qryTitulosiNumero: TStringField;
    qryTituloscBairro: TStringField;
    qryTituloscCidade: TStringField;
    qryTituloscUF: TStringField;
    qryTituloscCep: TStringField;
    qryTituloscComplemento: TStringField;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    qryTitulosnValCrediario: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptTituloNegativado_view: TrptTituloNegativado_view;

implementation

{$R *.dfm}
uses
  fMenu,rTituloNegativado;
end.
