unit fRegistrosRecebidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxPC, cxControls, GridsEh, DBGridEh, Menus, DBGridEhGrouping;

type
  TfrmRegistrosRecebidos = class(TfrmProcesso_Padrao)
    qryRecebidosHoje: TADOQuery;
    qryRecebidosHojecNmServidor: TStringField;
    qryRecebidosHojecTabela: TStringField;
    qryRecebidosHojeiIDRegistro: TIntegerField;
    qryRecebidosHojecFlgAcao: TStringField;
    qryRecebidosHojedDtInclusao: TDateTimeField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsRecebidosHoje: TDataSource;
    qryRecebidosQuinze: TADOQuery;
    dsRecebidosQuinze: TDataSource;
    qryRecebidosQuinzecNmServidor: TStringField;
    qryRecebidosQuinzecTabela: TStringField;
    qryRecebidosQuinzeiIDRegistro: TIntegerField;
    qryRecebidosQuinzecFlgAcao: TStringField;
    qryRecebidosQuinzedDtInclusao: TDateTimeField;
    DBGridEh2: TDBGridEh;
    PopupMenu1: TPopupMenu;
    Atualizar1: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    procedure Atualizar1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistrosRecebidos: TfrmRegistrosRecebidos;

implementation

{$R *.dfm}

procedure TfrmRegistrosRecebidos.Atualizar1Click(Sender: TObject);
begin
  inherited;

  qryRecebidosHoje.Close;
  qryRecebidosHoje.Open;
  
end;

procedure TfrmRegistrosRecebidos.MenuItem1Click(Sender: TObject);
begin
  inherited;

  qryRecebidosQuinze.Close;
  qryRecebidosQuinze.Open;
  
end;

procedure TfrmRegistrosRecebidos.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0;
end;

end.
