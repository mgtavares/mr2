unit rOscilacaoEstoque_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TrptOscilacaoEstoque_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBand5: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    Cancel: TQRShape;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdGrupoProduto: TIntegerField;
    uspRelatoriocNmGrupoProduto: TStringField;
    uspRelatorionCdProduto: TIntegerField;
    uspRelatoriocNmProduto: TStringField;
    uspRelatoriocUnidadeMedida: TStringField;
    uspRelatorionSaldoAnt: TBCDField;
    uspRelatorionQtdeEnt: TBCDField;
    uspRelatorionQtdeSai: TBCDField;
    uspRelatorionSaldoPos: TBCDField;
    QRDBText2: TQRDBText;
    QRBand3: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText4: TQRDBText;
    uspRelatorionQtdeOsc: TBCDField;
    uspRelatorionValorAnt: TBCDField;
    uspRelatorionValorEnt: TBCDField;
    uspRelatorionValorSai: TBCDField;
    uspRelatorionValorPos: TBCDField;
    uspRelatorionValorOsc: TBCDField;
    uspRelatorionValCusto: TBCDField;
    uspRelatorionValMedio: TBCDField;
    uspRelatoriocNmDepartamento: TStringField;
    uspRelatoriocNmCategoria: TStringField;
    uspRelatoriocNmSubCategoria: TStringField;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRDBText6: TQRDBText;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRShape4: TQRShape;
    uspRelatorionCdLocalEstoque: TIntegerField;
    uspRelatoriocNmLocalEstoque: TStringField;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel26: TQRLabel;
    uspRelatorionValPrecoUltRec: TBCDField;
    uspRelatoriodDtUltReceb: TDateTimeField;
    QRDBText5: TQRDBText;
    QRLabel27: TQRLabel;
    uspRelatoriocNmMarcaProduto: TStringField;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOscilacaoEstoque_View: TrptOscilacaoEstoque_View;

implementation

{$R *.dfm}

end.
