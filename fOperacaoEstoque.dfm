inherited frmOperacaoEstoque: TfrmOperacaoEstoque
  Caption = 'Opera'#231#227'o de Estoque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 72
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Opera'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 120
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdOperacaoEstoque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 120
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmOperacaoEstoque'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBRadioGroup1: TDBRadioGroup [6]
    Left = 120
    Top = 88
    Width = 185
    Height = 41
    Caption = 'Efeito no Estoque'
    Columns = 2
    DataField = 'cSinalOper'
    DataSource = dsMaster
    Items.Strings = (
      'Cr'#233'dito'
      'D'#233'bito')
    TabOrder = 3
    Values.Strings = (
      '+'
      '-')
  end
  object DBCheckBox1: TDBCheckBox [7]
    Left = 120
    Top = 136
    Width = 185
    Height = 17
    Caption = 'Permitir Uso Manual'
    DataField = 'cFlgPermManual'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [8]
    Left = 120
    Top = 160
    Width = 257
    Height = 17
    Caption = 'Influenciar no c'#225'lculo do consumo m'#233'dio'
    DataField = 'cFlgConsumoMedio'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM OperacaoEstoque'
      'WHERE nCdOperacaoEstoque = :nPK')
    object qryMasternCdOperacaoEstoque: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object qryMastercNmOperacaoEstoque: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
    object qryMastercSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
    object qryMastercFlgPermManual: TIntegerField
      FieldName = 'cFlgPermManual'
    end
    object qryMastercFlgConsumoMedio: TIntegerField
      FieldName = 'cFlgConsumoMedio'
    end
  end
end
