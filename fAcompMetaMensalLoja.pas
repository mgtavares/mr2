unit fAcompMetaMensalLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DBGridEhGrouping, GridsEh, DBGridEh, cxPC,
  cxControls, DB, ADODB, StdCtrls, cxButtons, DBCtrls, Mask, TeEngine,
  Series, TeeProcs, Chart, DbChart, ToolCtrlsEh;

type
  TfrmAcompMetaMensalLoja = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    edtMesAno: TMaskEdit;
    DBEdit2: TDBEdit;
    cxButton1: TcxButton;
    edtEmpresa: TMaskEdit;
    edtLoja: TMaskEdit;
    DBEdit1: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxPageControl3: TcxPageControl;
    cxTabSheet4: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    cmdPreparaTemp: TADOCommand;
    qryResumoSemana: TADOQuery;
    SP_ACOMPANHAMENTO_META_MENSAL: TADOStoredProc;
    dsResumoSemana: TDataSource;
    qryResumoSemanaiSemana: TIntegerField;
    qryResumoSemanaiTotalHorasPrev: TBCDField;
    qryResumoSemanaiTotalHorasReal: TBCDField;
    qryResumoSemananValMetaMinima: TBCDField;
    qryResumoSemananValMetaMinimaReal: TBCDField;
    qryResumoSemananValMeta: TBCDField;
    qryResumoSemananValMetaReal: TBCDField;
    qryResumoSemananPercMetaReal: TBCDField;
    qryResumoSemananValTotalVenda: TBCDField;
    qryResumoSemananPercMetaMinimaAting: TBCDField;
    qryResumoSemananPercMetaAting: TBCDField;
    qryResumoSemananCdMetaSemana: TIntegerField;
    dsResumoVendedor: TDataSource;
    qryResumoVendedor: TADOQuery;
    qryResumoVendedornCdVendedor: TIntegerField;
    qryResumoVendedoriTotalHorasPrev: TBCDField;
    qryResumoVendedoriTotalHorasReal: TBCDField;
    qryResumoVendedornValMetaMinima: TBCDField;
    qryResumoVendedornValMetaMinimaReal: TBCDField;
    qryResumoVendedornValMeta: TBCDField;
    qryResumoVendedornValMetaReal: TBCDField;
    qryResumoVendedornValTotalVenda: TBCDField;
    qryResumoVendedornPercMetaMinimaAting: TBCDField;
    qryResumoVendedornPercMetaAting: TBCDField;
    qryVendedor: TADOQuery;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    qryVendedornCdTerceiroResponsavel: TIntegerField;
    dsVendedor: TDataSource;
    cxPageControl4: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    qryResumoVendedorMes: TADOQuery;
    dsResumoVendedorMes: TDataSource;
    qryResumoVendedorMesnCdVendedor: TIntegerField;
    qryResumoVendedorMesiTotalHorasPrev: TBCDField;
    qryResumoVendedorMesiTotalHorasReal: TBCDField;
    qryResumoVendedorMesnValMetaMinima: TBCDField;
    qryResumoVendedorMesnValMetaMinimaReal: TBCDField;
    qryResumoVendedorMesnValMeta: TBCDField;
    qryResumoVendedorMesnValMetaReal: TBCDField;
    qryResumoVendedorMesnPercMetaReal: TBCDField;
    qryResumoVendedorMesnValTotalVenda: TBCDField;
    qryResumoVendedorMesnPercMetaMinimaAting: TBCDField;
    qryResumoVendedorMesnPercMetaAting: TBCDField;
    qryResumoVendedorMescNmVendedor: TStringField;
    ImageList2: TImageList;
    qryResumoVendedornCdStatusMeta: TIntegerField;
    qryResumoVendedornCdMetaSemana: TIntegerField;
    qryResumoVendedorcNmVendedor: TStringField;
    qryResumoVendedoriSemana: TIntegerField;
    qryResumoVendedorMesnCdStatusMeta: TIntegerField;
    qryResumoSemananCdStatusMeta: TIntegerField;
    GroupBox2: TGroupBox;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    qryRelVendaAnalitico: TADOQuery;
    qryRelVendaAnaliticonCdMetaSemana: TIntegerField;
    qryRelVendaAnaliticonCdMetaMes: TIntegerField;
    qryRelVendaAnaliticodDtInicialSemana: TDateTimeField;
    qryRelVendaAnaliticodDtFinalSemana: TDateTimeField;
    qryRelVendaAnaliticodDtInicialMes: TDateTimeField;
    qryRelVendaAnaliticodDtFinalMes: TDateTimeField;
    cxTabSheet9: TcxTabSheet;
    GroupBox3: TGroupBox;
    DBChart2: TDBChart;
    cxButton2: TcxButton;
    GroupBox4: TGroupBox;
    qryBuscaSemana: TADOQuery;
    qryBuscaSemananCdMetaSemana: TIntegerField;
    qryBuscaSemanadDtInicialSemana: TDateTimeField;
    qryBuscaSemanadDtFinalSemana: TDateTimeField;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    edtNumPage: TEdit;
    btnAnterior: TcxButton;
    btnProximo: TcxButton;
    qryResumoSemananValAcumulado: TBCDField;
    qryResumoSemananValDiferenca: TFloatField;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryResumoVendedorCalcFields(DataSet: TDataSet);
    procedure edtEmpresaChange(Sender: TObject);
    procedure edtLojaChange(Sender: TObject);
    procedure edtMesAnoChange(Sender: TObject);
    procedure qryResumoSemanaAfterScroll(DataSet: TDataSet);
    procedure qryResumoVendedorMesCalcFields(DataSet: TDataSet);
    procedure qryResumoSemanaCalcFields(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure DBGridEh3DblClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
  private
    { Private declarations }
    cFiltro : string;
  public
    { Public declarations }
  end;

var
  frmAcompMetaMensalLoja: TfrmAcompMetaMensalLoja;

implementation

uses fMenu, fLookup_Padrao, rVendaXVendedorAnalitico_view;

{$R *.dfm}

procedure TfrmAcompMetaMensalLoja.cxButton1Click(Sender: TObject);
var
    iMes, iAno, iSemana : integer;
begin
  inherited;

  {-- verifica se a data digitada � valida --}
  if (trim(edtMesAno.Text) = '/') then
  begin
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
  end ;

  edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end ;

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  {-- se os campos de empresa ou loja estiverem em branco usa a loja e/ou empresa ativa --}

  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  if (Trim(edtLoja.Text) = '') then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva);

      qryLoja.Close;
      PosicionaQuery(qryLoja, edtLoja.Text) ;
  end;

  {-- exibe os valores da busca --}

  cmdPreparaTemp.Execute;

  cxPageControl1.ActivePageIndex := 0;
  cxPageControl2.ActivePageIndex := 0;

  SP_ACOMPANHAMENTO_META_MENSAL.Close;
  SP_ACOMPANHAMENTO_META_MENSAL.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text);
  SP_ACOMPANHAMENTO_META_MENSAL.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.ConvInteiro(edtLoja.Text);
  SP_ACOMPANHAMENTO_META_MENSAL.Parameters.ParamByName('@iMes').Value       := iMes;
  SP_ACOMPANHAMENTO_META_MENSAL.Parameters.ParamByName('@iAno').Value       := iAno;
  SP_ACOMPANHAMENTO_META_MENSAL.ExecProc;

  qryResumoSemana.Close;
  qryResumoSemana.Open;

  qryResumoVendedorMes.Close;
  qryResumoVendedorMes.Open;

  qryBuscaSemana.Close;
  qryBuscaSemana.Parameters.ParamByName('nCdMetaSemana').Value := qryResumoSemananCdMetaSemana.Value;
  qryBuscaSemana.Open;

  RadioGroup2.Items.Clear;
  iSemana := 1;
  
  while not (qryBuscaSemana.Eof) do
  begin
      RadioGroup2.Items.Add(IntToStr(iSemana) + ' -' + qryBuscaSemanadDtInicialSemana.AsString + ' � ' + qryBuscaSemanadDtFinalSemana.AsString);
      iSemana := iSemana + 1;
      
      qryBuscaSemana.Next;
  end;

  cxTabSheet9.Enabled := true;

  DBGridEh1.SetFocus;

end;

procedure TfrmAcompMetaMensalLoja.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtLoja.Text    := IntToStr(frmMenu.nCdLojaAtiva);

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmAcompMetaMensalLoja.edtEmpresaExit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

procedure TfrmAcompMetaMensalLoja.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            qryEmpresa.Close;

            edtEmpresa.Text := IntToStr(nPK);
            PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmAcompMetaMensalLoja.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmAcompMetaMensalLoja.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            qryLoja.Close;

            edtLoja.Text := IntToStr(nPK);
            PosicionaQuery(qryLoja, edtLoja.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmAcompMetaMensalLoja.qryResumoVendedorCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryResumoVendedoriSemana.Value := qryResumoSemanaiSemana.Value;

  if ((qryResumoVendedornValTotalVenda.Value < qryResumoVendedornValMetaMinimaReal.Value)) then
  begin
      qryResumoVendedornCdStatusMeta.Value := 0;
  end;

  if ((qryResumoVendedornValTotalVenda.Value > qryResumoVendedornValMetaMinimaReal.Value)
      and (qryResumoVendedornValTotalVenda.Value < qryResumoVendedornValMetaReal.Value)) then
  begin
      qryResumoVendedornCdStatusMeta.Value := 1;
  end;

  if ((qryResumoVendedornValTotalVenda.Value >= qryResumoVendedornValMetaReal.Value)) then
  begin
      qryResumoVendedornCdStatusMeta.Value := 2;
  end;

end;

procedure TfrmAcompMetaMensalLoja.edtEmpresaChange(Sender: TObject);
begin
  inherited;

  qryResumoSemana.Close;
  qryResumoVendedor.Close;
  qryResumoVendedorMes.Close;
end;

procedure TfrmAcompMetaMensalLoja.edtLojaChange(Sender: TObject);
begin
  inherited;

  qryResumoSemana.Close;
  qryResumoVendedor.Close;
  qryResumoVendedorMes.Close;
end;

procedure TfrmAcompMetaMensalLoja.edtMesAnoChange(Sender: TObject);
begin
  inherited;

  qryResumoSemana.Close;
  qryResumoVendedor.Close;
  qryResumoVendedorMes.Close;
end;

procedure TfrmAcompMetaMensalLoja.qryResumoSemanaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  {-- exibe o resumo semanal do vendedor --}

  qryResumoVendedor.Close;
  qryResumoVendedor.Parameters.ParamByName('nCdMetaSemana').Value := qryResumoSemananCdMetaSemana.Value;
  qryResumoVendedor.Open;
end;

procedure TfrmAcompMetaMensalLoja.qryResumoVendedorMesCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if ((qryResumoVendedorMesnValTotalVenda.Value < qryResumoVendedorMesnValMetaMinimaReal.Value)) then
  begin
      qryResumoVendedorMesnCdStatusMeta.Value := 0;
  end;

  if ((qryResumoVendedorMesnValTotalVenda.Value > qryResumoVendedorMesnValMetaMinimaReal.Value)
      and (qryResumoVendedorMesnValTotalVenda.Value < qryResumoVendedorMesnValMetaReal.Value)) then
  begin
      qryResumoVendedorMesnCdStatusMeta.Value := 1;
  end;

  if ((qryResumoVendedorMesnValTotalVenda.Value >= qryResumoVendedorMesnValMetaReal.Value)) then
  begin
      qryResumoVendedorMesnCdStatusMeta.Value := 2;
  end;

end;

procedure TfrmAcompMetaMensalLoja.qryResumoSemanaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryResumoSemananValDiferenca.Value = 0) then
      qryResumoSemananValDiferenca.Value := (qryResumoSemananValTotalVenda.Value - qryResumoSemananValMetaReal.Value) ;

  if ((qryResumoSemananValTotalVenda.Value < qryResumoSemananValMetaMinimaReal.Value)) then
  begin
      qryResumoSemananCdStatusMeta.Value := 0;
  end;

  if ((qryResumoSemananValTotalVenda.Value > qryResumoSemananValMetaMinimaReal.Value)
      and (qryResumoSemananValTotalVenda.Value < qryResumoSemananValMetaReal.Value)) then
  begin
      qryResumoSemananCdStatusMeta.Value := 1;
  end;

  if ((qryResumoSemananValTotalVenda.Value >= qryResumoSemananValMetaReal.Value)) then
  begin
      qryResumoSemananCdStatusMeta.Value := 2;
  end;
end;

procedure TfrmAcompMetaMensalLoja.DBGridEh1DblClick(Sender: TObject);
var
  objRel : TrptVendaXVendedorAnalitico_view;
begin
  inherited;

  if not (qryResumoVendedor.Active) then
      abort;

  qryRelVendaAnalitico.Close;
  qryRelVendaAnalitico.Parameters.ParamByName('nCdMetaSemana').Value := qryResumoSemananCdMetaSemana.Value;
  qryRelVendaAnalitico.Open;

  objRel := TrptVendaXVendedorAnalitico_view.Create(nil);

  try
      try
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Close;

          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdVendedor').Value := 0;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtInicial').Value  := qryRelVendaAnaliticodDtInicialSemana.Value;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtFinal').Value    := qryRelVendaAnaliticodDtFinalSemana.Value;

          objRel.cmd_PreparaTemp.Execute;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.ExecProc;

          objRel.qryDescricaoPedido.Close;
          objRel.qryDescricaoPedido.Open;

          {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Loja: ' + DBEdit2.Text + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + qryRelVendaAnaliticodDtInicialSemana.AsString + ' � ' + qryRelVendaAnaliticodDtFinalSemana.AsString;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmAcompMetaMensalLoja.DBGridEh2DblClick(Sender: TObject);
var
  objRel : TrptVendaXVendedorAnalitico_view;
begin
  inherited;

  if not (qryResumoVendedor.Active) then
      abort;

  qryRelVendaAnalitico.Close;
  qryRelVendaAnalitico.Parameters.ParamByName('nCdMetaSemana').Value := qryResumoSemananCdMetaSemana.Value;
  qryRelVendaAnalitico.Open;

  objRel := TrptVendaXVendedorAnalitico_view.Create(nil);

  try
      try
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Close;

          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdVendedor').Value := qryResumoVendedornCdVendedor.Value;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtInicial').Value  := qryRelVendaAnaliticodDtInicialSemana.Value;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtFinal').Value    := qryRelVendaAnaliticodDtFinalSemana.Value;

          objRel.cmd_PreparaTemp.Execute;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.ExecProc;

          objRel.qryDescricaoPedido.Close;
          objRel.qryDescricaoPedido.Open;

          {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Loja: ' + DBEdit2.Text + '/ ';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Vendedor: ' + qryResumoVendedorcNmVendedor.Value + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + qryRelVendaAnaliticodDtInicialSemana.AsString + ' � ' + qryRelVendaAnaliticodDtFinalSemana.AsString;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmAcompMetaMensalLoja.DBGridEh3DblClick(Sender: TObject);
var
  objRel : TrptVendaXVendedorAnalitico_view;
begin
  inherited;

  if not (qryResumoVendedorMes.Active) then
      abort;

  qryRelVendaAnalitico.Close;
  qryRelVendaAnalitico.Parameters.ParamByName('nCdMetaSemana').Value := qryResumoSemananCdMetaSemana.Value;
  qryRelVendaAnalitico.Open;

  objRel := TrptVendaXVendedorAnalitico_view.Create(nil);
  try
      try
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Close;

          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdVendedor').Value := qryResumoVendedorMesnCdVendedor.Value;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtInicial').Value  := qryRelVendaAnaliticodDtInicialMes.Value;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtFinal').Value    := qryRelVendaAnaliticodDtFinalMes.Value;

          objRel.cmd_PreparaTemp.Execute;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.ExecProc;

          objRel.qryDescricaoPedido.Close;
          objRel.qryDescricaoPedido.Open;

          {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Loja: ' + DBEdit2.Text + '/ ';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Vendedor: ' + qryResumoVendedorcNmVendedor.Value + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + qryRelVendaAnaliticodDtInicialMes.AsString + ' � ' + qryRelVendaAnaliticodDtFinalMes.AsString;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmAcompMetaMensalLoja.cxButton2Click(Sender: TObject);
var
  i  : integer;
  tpSeries : TChartSeries;
  Venda,Realizado,Prevista : TBarSeries;

begin
  inherited;
  {-- este � o processo para exibi��o do gr�fico de acordo com o filtro --
   -- selecionado --}

  btnProximo.Enabled  := false; // desabilita os bot�es de acesso as p�ginas
  btnAnterior.Enabled := false; // do gr�fico

  {-- cria as series do gr�fico --}
  if (RadioGroup1.ItemIndex = 0) then
  begin
      DBChart2.SeriesList.Clear;

      Realizado             := TBarSeries.Create(Self);
      Realizado.Title       := 'Meta';
      Realizado.Active      := true;
      Realizado.SeriesColor := RGB(0,64,128);
      DBChart2.AddSeries(Realizado);

      Venda             := TBarSeries.Create(Self);
      Venda.Title       := 'Total de Venda';
      Venda.Active      := true;
      Venda.SeriesColor := RGB(255,255,0);
      DBChart2.AddSeries(Venda);

  end
  else
  begin
      DBChart2.SeriesList.Clear;

      Prevista             := TBarSeries.Create(Self);
      Prevista.Title       := 'Meta Prevista';
      Prevista.Active      := true;
      Prevista.SeriesColor := RGB(255,128,0);
      DBChart2.AddSeries(Prevista);

      Realizado             := TBarSeries.Create(Self);
      Realizado.Title       := 'Meta Realizada';
      Realizado.Active      := true;
      Realizado.SeriesColor := RGB(0,64,128);
      DBChart2.AddSeries(Realizado);

      Venda             := TBarSeries.Create(Self);
      Venda.Title       := 'Total de Venda';
      Venda.Active      := true;
      Venda.SeriesColor := RGB(255,255,0);
      DBChart2.AddSeries(Venda);

  end;

  {-- remove as refer�ncias ao datasource --}

  for i := 0 to (DBChart2.SeriesList.Count - 1) do
  begin
      DBChart2.Series[i].XLabelsSource       := '';
      DBChart2.Series[i].XValues.ValueSource := '';
      DBChart2.Series[i].YValues.ValueSource := '';
  end;

  {-- quando for o gr�fico semanal, usa gr�fico de colunas --
   -- listando os valores por semana --}

  if (RadioGroup1.ItemIndex = 0) then
  begin
      for i := 0 to (DBChart2.SeriesList.Count - 1) do
      begin
          tpSeries := DBChart2.Series[i];
          ChangeSeriesType(tpSeries, TBarSeries);
      end;

      DBChart2.Title.Text.Text := 'Resumo Semanal';

      {-- para cada s�rie de valores do gr�fico aplico o datasource --
       -- e as respectivas colunas respons�veis pela s�rie --}

      with DBChart2.SeriesList.Series[0] do begin
          DataSource          := qryResumoSemana;
          XLabelsSource       := 'iSemana';
          XValues.ValueSource := 'iSemana';
          YValues.ValueSource := 'nValMetaReal';
          Marks.Style         := smsValue;
          CheckDataSource;
      end;

      with DBChart2.SeriesList.Series[1] do begin
          DataSource          := qryResumoSemana;
          XLabelsSource       := 'iSemana';
          XValues.ValueSource := 'iSemana';
          YValues.ValueSource := 'nValTotalVenda';
          Marks.Style         := smsValue;
          CheckDataSource;
      end;
  end

    {-- quando for o gr�fico semanal ou mensal X Vendedor, usa gr�fico
    -- de colunas  barras listando os valores do mes ou da semana selecionada --}

  else if (RadioGroup1.ItemIndex = 1) then
  begin
      for i := 0 to (DBChart2.SeriesList.Count - 1) do
      begin
          tpSeries := DBChart2.Series[i];
          ChangeSeriesType(tpSeries, THorizBarSeries);
      end;

      DBChart2.Title.Text.Text := 'Resumo Vendedor - Semanal';

      qryResumoSemana.Locate('iSemana',Copy(RadioGroup2.Items[RadioGroup2.ItemIndex],1,1),[loPartialKey,loCaseInsensitive]);

      with DBChart2.SeriesList.Series[0] do begin
          DataSource          := qryResumoVendedor;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValMeta';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;

      with DBChart2.SeriesList.Series[1] do begin
          DataSource          := qryResumoVendedor;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValMetaReal';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;

      with DBChart2.SeriesList.Series[2] do begin
          DataSource          := qryResumoVendedor;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValTotalVenda';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;
  end
  else if (RadioGroup1.ItemIndex = 2) then
  begin
      for i := 0 to (DBChart2.SeriesList.Count - 1) do
      begin
          tpSeries := DBChart2.Series[i];
          ChangeSeriesType(tpSeries, THorizBarSeries);
      end;

      DBChart2.Title.Text.Text := 'Resumo Vendedor - Mensal';

      with DBChart2.SeriesList.Series[0] do begin
          DataSource          := qryResumoVendedorMes;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValMeta';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;

      with DBChart2.SeriesList.Series[1] do begin
          DataSource          := qryResumoVendedorMes;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValMetaReal';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;

      with DBChart2.SeriesList.Series[2] do begin
          DataSource          := qryResumoVendedorMes;
          XLabelsSource       := 'cNmVendedor';
          XValues.ValueSource := 'nValTotalVenda';
          YValues.ValueSource := '';
          Marks.Style         := smsXValue;
          CheckDataSource;
      end;
  end;

  {-- atualiza aqui o numero de paginas habilitando os bot�es quando necess�rio --}
  DBChart2.Page := 1;

  edtNumPage.Text := IntToStr(DbChart2.Page);

  if (DBChart2.NumPages > 1) then
     btnProximo.Enabled := true;

end;

procedure TfrmAcompMetaMensalLoja.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  if (RadioGroup1.ItemIndex = 1) then
  begin
      RadioGroup2.Enabled   := true;
      RadioGroup2.ItemIndex := 0;
  end
  else RadioGroup2.Enabled  := false;
end;

procedure TfrmAcompMetaMensalLoja.btnProximoClick(Sender: TObject);
begin
  inherited;

  DBChart2.NextPage;

  edtNumPage.Text := IntToStr(DbChart2.Page);

  btnAnterior.Enabled := true;

  if (DBChart2.Page = DBChart2.NumPages) then
      btnProximo.Enabled := false;

end;

procedure TfrmAcompMetaMensalLoja.btnAnteriorClick(Sender: TObject);
begin
  inherited;

  DBChart2.PreviousPage;

  edtNumPage.Text := IntToStr(DbChart2.Page);

  if (DBChart2.Page < DBChart2.NumPages) then
      btnProximo.Enabled := true;

  if (DBChart2.Page = 1) then
      btnAnterior.Enabled := false;

end;

initialization
    RegisterClass(TfrmAcompMetaMensalLoja);

end.
