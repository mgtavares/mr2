program ER2Loja;

uses
  Forms,
  fMenuLoja in 'fMenuLoja.pas' {frmMenuLoja},
  fPdv in 'fPdv.pas' {frmPDV},
  fMenu in 'fMenu.pas' {frmMenu},
  fSelecServer in 'fSelecServer.pas' {frmSelecServer},
  fSelecEmpresa in 'fSelecEmpresa.pas' {frmSelecEmpresa},
  fSelecLoja in 'fSelecLoja.pas' {frmSelecLoja},
  fLogin in 'fLogin.pas' {frmLogin},
  fCadastro_Template in 'fCadastro_Template.pas' {frmCadastro_Padrao},
  fErroPadrao in 'fErroPadrao.pas' {frmErroPadrao},
  fProcesso_Padrao in 'fProcesso_Padrao.pas' {frmProcesso_Padrao},
  fConsultaVendaProduto in 'fConsultaVendaProduto.pas' {frmConsultaVendaProduto},
  fConsultaVendaProduto_Dados in 'fConsultaVendaProduto_Dados.pas' {frmConsultaVendaProduto_Dados},
  fCaixa_Recebimento in 'fCaixa_Recebimento.pas' {frmCaixa_Recebimento},
  fCaixa_AlteraCartao in 'fCaixa_AlteraCartao.pas' {frmCaixa_AlteraCartao},
  fCaixa_Cheques in 'fCaixa_Cheques.pas' {frmCaixa_Cheques},
  fCaixa_ConfUsuarioCaixa in 'fCaixa_ConfUsuarioCaixa.pas' {frmCaixa_ConfUsuarioCaixa},
  fCaixa_ConsOperadora in 'fCaixa_ConsOperadora.pas' {frmCaixa_ConsOperadora},
  fCaixa_ConsPropReneg in 'fCaixa_ConsPropReneg.pas' {frmCaixa_ConsPropReneg},
  fCaixa_DadosCartao in 'fCaixa_DadosCartao.pas' {frmCaixa_DadosCartao},
  fCaixa_DadosCheque in 'fCaixa_DadosCheque.pas' {frmCaixa_DadosCheque},
  fCaixa_DescontoJuros in 'fCaixa_DescontoJuros.pas' {frmCaixa_DescontoJuros},
  fCaixa_Dinheiro in 'fCaixa_Dinheiro.pas' {frmCaixa_Dinheiro},
  fCaixa_EfetivaPropReneg in 'fCaixa_EfetivaPropReneg.pas' {frmCaixa_EfetivaPropReneg},
  fCaixa_EntradaDin in 'fCaixa_EntradaDin.pas' {frmCaixa_EntradaDin},
  fCaixa_EstornoDia in 'fCaixa_EstornoDia.pas' {frmCaixa_EstornoDia},
  fCaixa_Fechamento in 'fCaixa_Fechamento.pas' {frmCaixa_Fechamento},
  fCaixa_Funcoes in 'fCaixa_Funcoes.pas' {frmCaixa_Funcoes},
  fCaixa_Justificativa in 'fCaixa_Justificativa.pas' {frmCaixa_Justificativa},
  fCaixa_LanctoMan in 'fCaixa_LanctoMan.pas' {frmCaixa_LanctoMan},
  fCaixa_LiberaRestricao in 'fCaixa_LiberaRestricao.pas' {frmCaixa_LiberaRestricao},
  fCaixa_ListaParcelas in 'fCaixa_ListaParcelas.pas' {frmCaixa_ListaParcelas},
  fCaixa_ListaPedidos in 'fCaixa_ListaPedidos.pas' {frmCaixa_ListaPedidos},
  fCaixa_MotivoEstorno in 'fCaixa_MotivoEstorno.pas' {frmCaixa_MotivoEstorno},
  fCaixa_NumValePresente in 'fCaixa_NumValePresente.pas' {frmCaixa_NumValePresente},
  fCaixa_PagtoCheque in 'fCaixa_PagtoCheque.pas' {frmCaixa_PagtoCheque},
  fCaixa_ParcelaAberto in 'fCaixa_ParcelaAberto.pas' {frmCaixa_ParcelaAberto},
  fCaixa_ParcelaCred in 'fCaixa_ParcelaCred.pas' {frmCaixa_ParcelaCred},
  fCaixa_Pendencias in 'fCaixa_Pendencias.pas' {frmCaixa_Pendencias},
  fCaixa_ReembolsoVale in 'fCaixa_ReembolsoVale.pas' {frmCaixa_ReembolsoVale},
  fCaixa_Reimpressao in 'fCaixa_Reimpressao.pas' {frmCaixa_Reimpressao},
  fCaixa_ResgateCheque in 'fCaixa_ResgateCheque.pas' {frmCaixa_ResgateCheque},
  fCaixa_Sangria in 'fCaixa_Sangria.pas' {frmCaixa_Sangria},
  fCaixa_SelCliente in 'fCaixa_SelCliente.pas' {frmCaixa_SelCliente},
  fCaixa_StatusDocto in 'fCaixa_StatusDocto.pas' {frmCaixa_StatusDocto},
  fCaixa_Suprimento in 'fCaixa_Suprimento.pas' {frmCaixa_Suprimento},
  fRemessa_Numerario in 'fRemessa_Numerario.pas' {frmRemessa_Numerarios},
  fClienteVarejoPessoaFisica in 'fClienteVarejoPessoaFisica.pas' {frmClienteVarejoPessoaFisica},
  fAprovacaoClienteVarejo in 'fAprovacaoClienteVarejo.pas' {frmAprovacaoClienteVarejo},
  fManutBloqueioCliente in 'fManutBloqueioCliente.pas' {frmManutBloqueioCliente},
  fManutLimiteCliente in 'fManutLimiteCliente.pas' {frmManutLimiteCliente},
  fManutLimiteClienteManual in 'fManutLimiteClienteManual.pas' {frmManutLimiteClienteManual},
  fRelatorio_Padrao in 'fRelatorio_Padrao.pas' {frmRelatorio_Padrao},
  rPreVendaPendente in 'rPreVendaPendente.pas' {rptPreVendaPendente},
  rPreVendaPendente_view in 'rPreVendaPendente_view.pas' {rptPreVendaPendente_view},
  rVendaCrediario in 'rVendaCrediario.pas' {rptVendaCrediario},
  rVendaCrediario_view in 'rVendaCrediario_view.pas' {rptVendaCrediario_view},
  rVendaEstruturaMarcaLoja in 'rVendaEstruturaMarcaLoja.pas' {rptVendaEstruturaMarcaLoja},
  rVendaEstruturaMarcaLoja_view in 'rVendaEstruturaMarcaLoja_view.pas' {rptVendaEstruturaMarcaLoja_view},
  rPosicaoVale in 'rPosicaoVale.pas' {rptPosicaoVale},
  rPosicaoVale_view in 'rPosicaoVale_view.pas' {rptPosicaoVale_view},
  rAlteracaoPrecoProduto in 'rAlteracaoPrecoProduto.pas' {rptAlteracaoPrecoProduto},
  rAlteracaoPrecoProduto_view in 'rAlteracaoPrecoProduto_view.pas' {rptAlteracaoPrecoProduto_view},
  fLookup_Padrao in 'fLookup_Padrao.pas' {frmLookup_Padrao},
  fTipoBloqueioCliente in 'fTipoBloqueioCliente.pas' {frmTipoBloqueioCliente},
  fConsultaMovtoCaixa in 'fConsultaMovtoCaixa.pas' {frmConsultaMovtoCaixa},
  rExtratoContaBancaria in 'rExtratoContaBancaria.pas' {rptExtratoContaBancaria},
  fLancManConta in 'fLancManConta.pas' {frmLancManConta},
  fExploraCaixaCofre in 'fExploraCaixaCofre.pas' {frmExploraCaixaCofre},
  fContaCaixaCofre in 'fContaCaixaCofre.pas' {frmContaCaixaCofre},
  fMensagem in 'fMensagem.pas' {frmMensagem},
  fMensagemPadrao in 'fMensagemPadrao.pas',
  fConfirma_Numerarios in 'fConfirma_Numerarios.pas' {frmConfirma_Numerarios},
  dcVendaLoja in 'dcVendaLoja.pas' {dcmVendaLoja},
  dcVendaLoja_view in 'dcVendaLoja_view.pas' {dcmVendaLoja_view},
  fCondPagtoPDV in 'fCondPagtoPDV.pas' {frmCondPagtoPDV},
  fManutBloqueioCliente_Inserir in 'fManutBloqueioCliente_Inserir.pas' {frmManutBloqueioCliente_Inserir},
  fPdvCondPagto in 'fPdvCondPagto.pas' {frmPdvCondPagto},
  fPdvConsProduto in 'fPdvConsProduto.pas' {frmPdvConsProduto},
  fPDVConsVended in 'fPDVConsVended.pas' {frmPDVConsVended},
  fPdvDesconto in 'fPdvDesconto.pas' {frmPdvDesconto},
  fPdvFormaPagto in 'fPdvFormaPagto.pas' {frmPdvFormaPagto},
  fPDVItemGrade in 'fPDVItemGrade.pas' {frmPDVItemGrade},
  fPdvReabrePedido in 'fPdvReabrePedido.pas' {frmPdvReabrePedido},
  fPdvSupervisor in 'fPdvSupervisor.pas' {frmPdvSupervisor},
  fPdvValeMerc in 'fPdvValeMerc.pas' {frmPDVValeMerc},
  fPDVValePresente in 'fPDVValePresente.pas' {frmPdvValePresente},
  fImpDFPadrao in 'fImpDFPadrao.pas' {frmImpDFPadrao},
  rBorderoCaixaNovo in 'rBorderoCaixaNovo.pas' {rptBorderoCaixaNovo},
  fFuncoesECF in 'fFuncoesECF.pas' {frmFuncoesECF},
  fTrilhaClienteVarejo in 'fTrilhaClienteVarejo.pas' {frmTrilhaClienteVarejo},
  fImagemDigital in 'fImagemDigital.pas' {frmImagemDigital},
  fImagemDigital_Visualizar in 'fImagemDigital_Visualizar.pas' {frmImagemDigital_Visualizar},
  rRemessaNum_view in 'rRemessaNum_view.pas' {rptRemessaNum_view: TQuickRep},
  fSplash in 'fSplash.pas' {SplashScreen},
  fConfigAmbiente in 'fConfigAmbiente.pas' {frmConfigAmbiente},
  fClienteVarejoPessoaFisica_HistAltEnd in 'fClienteVarejoPessoaFisica_HistAltEnd.pas' {frmClienteVarejoPessoaFisica_HistAltEnd},
  fAprovacaoClienteVarejo_LimiteInicial in 'fAprovacaoClienteVarejo_LimiteInicial.pas' {frmAprovacaoClienteVarejo_LimiteInicial},
  fAltSenha in 'fAltSenha.pas' {frmAltSenha},
  fBuscaPK in 'fBuscaPK.pas' {frmBuscaPK},
  rExtratoContaBancaria_view in 'rExtratoContaBancaria_view.pas' {rptExtratoContaBancaria_view: TQuickRep},
  fHistoricoLimiteCredito in 'fHistoricoLimiteCredito.pas' {frmHistoricoLimiteCredito},
  fConsultaEndereco in 'fConsultaEndereco.pas' {frmConsultaEndereco},
  fProdutoPosicaoEstoque in 'fProdutoPosicaoEstoque.pas' {frmProdutoPosicaoEstoque},
  fClienteVarejoPessoaFisica_TitulosNeg in 'fClienteVarejoPessoaFisica_TitulosNeg.pas' {frmClienteVarejoPessoaFisica_TitulosNeg},
  fClienteVarejoPessoaFisica_HistMov in 'fClienteVarejoPessoaFisica_HistMov.pas' {frmClienteVarejoPessoaFisica_HistMov},
  fEncerrarMovtoPOS in 'fEncerrarMovtoPOS.pas' {frmEncerrarMovtoPOS},
  fEncerrarMovtoPOS_GeraLote in 'fEncerrarMovtoPOS_GeraLote.pas' {frmEncerrarMovtoPOS_GeraLote},
  fAuditoria in 'fAuditoria.pas' {frmAuditoria},
  fPDVConsProdutoDetalhe in 'fPDVConsProdutoDetalhe.pas' {frmPDVConsProdutoDetalhe},
  rCaixa_Recebimento_Comprovante in 'rCaixa_Recebimento_Comprovante.pas' {rptCaixa_Recebimento_Comprovante},
  fConsultaLanctoConta in 'fConsultaLanctoConta.pas' {frmConsultaLanctoConta},
  fConsultaMovtoCaixa_Parcelas in 'fConsultaMovtoCaixa_Parcelas.pas' {frmConsultaMovtoCaixa_Parcelas},
  rResumoMovimentoLoja in 'rResumoMovimentoLoja.pas' {rptResumoMovimentoLoja},
  fAddServer in 'fAddServer.pas' {frmAddServer},
  fClienteVarejoPessoaFisica_HistContato in 'fClienteVarejoPessoaFisica_HistContato.pas' {frmClienteVarejoPessoaFisica_HistContato},
  fClienteVarejoPessoaFisica_HistoricoNeg in 'fClienteVarejoPessoaFisica_HistoricoNeg.pas' {frmClienteVarejoPessoaFisica_HistoricoNeg},
  fCaixa_CPFNotaFiscal in 'fCaixa_CPFNotaFiscal.pas' {frmCaixa_CPFNotaFiscal},
  fCobranca_VisaoContato_Contato in 'fCobranca_VisaoContato_Contato.pas' {frmCobranca_VisaoContato_Contato},
  fCaixa_ParcelaVencida in 'fCaixa_ParcelaVencida.pas' {frmCaixa_ParcelaVencida},
  fConsignacao in 'fConsignacao.pas' {frmConsignacao},
  fConsignacao_Leitor in 'fConsignacao_Leitor.pas' {frmConsignacao_Leitor},
  rFichaCadastralCliente_view in 'rFichaCadastralCliente_view.pas' {rptFichaCadastralCliente_view},
  fBuscaBorderoData in 'fBuscaBorderoData.pas' {frmBuscaBorderoData},
  fClienteVarejoPessoaFisica_Simples in 'fClienteVarejoPessoaFisica_Simples.pas' {frmClienteVarejoPessoaFisica_Simples},
  fTransacaoTEF in 'fTransacaoTEF.pas' {frmTransacaoTEF},
  fInputTEF in 'fInputTEF.pas' {frmInputTEF},
  fCaixa_CredErro in 'fCaixa_CredErro.pas' {frmCaixa_CredErro},
  fSobre in 'fSobre.pas' {frmSobre},
  fHistoricoVersoes in 'fHistoricoVersoes.pas' {frmHistoricoVersoes},
  fFeedback in 'fFeedback.pas' {frmFeedback},
  fFuncoesSAT in 'fFuncoesSAT.pas' {frmFuncoesSAT},
  fAcompanhaMetaDescVendedor in 'fAcompanhaMetaDescVendedor.pas' {frmAcompanhaMetaDescVendedor},
  fCapptAPI in 'fCapptAPI.pas' {frmCapptAPI},
  fValidaCNPJ in 'fValidaCNPJ.pas' {frmValidaCNPJ};

{$R *.res}

begin
  SplashScreen := TSplashScreen.Create(Application) ;
  SplashScreen.Show;
  Application.Initialize; //this line exists!
  SplashScreen.Update;

  Application.Title := 'ER2Soft - M�dulo Loja';
  Application.CreateForm(TfrmMenuLoja, frmMenuLoja);
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmCadastro_Padrao, frmCadastro_Padrao);
  Application.CreateForm(TfrmProcesso_Padrao, frmProcesso_Padrao);
  Application.CreateForm(TfrmRelatorio_Padrao, frmRelatorio_Padrao);
  Application.CreateForm(TfrmLookup_Padrao, frmLookup_Padrao);
  Application.CreateForm(TfrmBuscaPK, frmBuscaPK);
  Application.CreateForm(TfrmAuditoria, frmAuditoria);
  Application.CreateForm(TfrmSelecServer, frmSelecServer);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmSelecLoja, frmSelecLoja);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TfrmErroPadrao, frmErroPadrao);
  Application.CreateForm(TfrmMensagem, frmMensagem);
  Application.CreateForm(TfrmAltSenha, frmAltSenha);
  Application.CreateForm(TfrmAddServer, frmAddServer);
  Application.CreateForm(TfrmImpDFPadrao, frmImpDFPadrao);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TrptFichaCadastralCliente_view, rptFichaCadastralCliente_view);
  Application.CreateForm(TfrmBuscaBorderoData, frmBuscaBorderoData);
  {Application.CreateForm(TSplashScreen, SplashScreen);}

  SplashScreen.Hide;
  SplashScreen.Free;

  Application.Run;
end.
