unit dcCompraRepresPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TdcmCompraRepresPeriodo = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    DBEdit7: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescCNPJCPF: TStringField;
    qryTerceiroReprescNmTerceiro: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    dsTerceiroRepres: TDataSource;
    DataSource5: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmCompraRepresPeriodo: TdcmCompraRepresPeriodo;

implementation

uses fMenu, fLookup_Padrao, dcCompraRepresPeriodo_view;

{$R *.dfm}

procedure TdcmCompraRepresPeriodo.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit10.SetFocus ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiroRepres.Close ;

  If (Trim(MaskEdit10.Text) <> '') then
  begin

    qryTerceiroRepres.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit10.Text ;
    qryTerceiroRepres.Open ;
    
  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(103);

        If (nPK > 0) then
        begin
            Maskedit10.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;


end;

procedure TdcmCompraRepresPeriodo.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

procedure TdcmCompraRepresPeriodo.ToolButton1Click(Sender: TObject);
var
  objForm : TdcmCompraRepresPeriodo_view;
begin
  inherited;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  objForm := TdcmCompraRepresPeriodo_view.Create(nil);

  objForm.ADODataSet1.Close ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.ConvInteiro(MaskEdit10.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := frmMenu.ConvInteiro(MaskEdit9.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  objForm.PivotMap1.Measures[1].ColumnPercent := True;
  objForm.PivotMap1.Measures[1].Value := False;

  objForm.PivotMap1.Measures[2].Rank := True ;
  objForm.PivotMap1.Measures[2].Value := False;

  objForm.PivotMap1.SortColumn(0,2,False) ;

  objForm.PivotMap1.Title := 'ER2Soft - An�lise de Compras por Fornecedor' ;
  objForm.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm,true) ;

end;

initialization
    RegisterClass(TdcmCompraRepresPeriodo) ;
    
end.

