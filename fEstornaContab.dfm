inherited frmEstornaContab: TfrmEstornaContab
  Left = -8
  Top = -8
  Width = 1152
  Height = 786
  Caption = 'Estornar Contabiliza'#231#227'o Lan'#231'amentos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 129
    Width = 1136
    Height = 621
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    TabOrder = 2
    Transparent = True
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 100
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Tag = 1
      Left = 40
      Top = 24
      Width = 99
      Height = 13
      Caption = #218'ltima contabiliza'#231#227'o'
    end
    object Label3: TLabel
      Tag = 1
      Left = 46
      Top = 48
      Width = 93
      Height = 13
      Caption = 'Data Limite Estorno'
    end
    object Label1: TLabel
      Tag = 1
      Left = 13
      Top = 72
      Width = 126
      Height = 13
      Caption = 'Estornar Lan'#231'amentos at'#233
    end
    object MaskEdit1: TMaskEdit
      Tag = 1
      Left = 144
      Top = 16
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Tag = 1
      Left = 144
      Top = 40
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit2: TMaskEdit
      Left = 144
      Top = 64
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 129
    Width = 1136
    Height = 621
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 617
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Log de Processamentos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 593
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLogProcesso
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtLog'
            Footers = <>
            Width = 138
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cObserv'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM PARAMETRO'
      '  WHERE cParametro = :cParametro')
    Left = 424
    Top = 64
    object qryParametrocParametro: TStringField
      FieldName = 'cParametro'
      Size = 15
    end
    object qryParametrocValor: TStringField
      FieldName = 'cValor'
      Size = 15
    end
    object qryParametrocDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 50
    end
  end
  object usp_Contabiliza: TADOStoredProc
    AutoCalcFields = False
    Connection = frmMenu.Connection
    CursorType = ctStatic
    EnableBCD = False
    ProcedureName = 'SP_ESTORNA_CONTABILIZA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtFechamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 64
  end
  object qryLogProcesso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 30 dDtLog'
      '      ,LogProcesso.nCdUsuario'
      '      ,cNmUsuario'
      '      ,cObserv '
      '  FROM LogProcesso'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = LogProcesso.nC' +
        'dUsuario'
      ' WHERE nCdProcesso = 1'
      ' ORDER BY dDtLog DESC')
    Left = 504
    Top = 64
    object qryLogProcessodDtLog: TDateTimeField
      DisplayLabel = 'Data Processo'
      FieldName = 'dDtLog'
    end
    object qryLogProcessonCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio|C'#243'digo'
      FieldName = 'nCdUsuario'
    end
    object qryLogProcessocNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio|Nome'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLogProcessocObserv: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cObserv'
      Size = 50
    end
  end
  object dsLogProcesso: TDataSource
    DataSet = qryLogProcesso
    Left = 536
    Top = 64
  end
end
