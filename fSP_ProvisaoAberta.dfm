inherited frmSP_ProvisaoAberta: TfrmSP_ProvisaoAberta
  Left = 294
  Top = 272
  Width = 856
  Height = 408
  Caption = 'Provis'#245'es em Aberto - Pressione ESC para fechar'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 840
    Height = 341
  end
  inherited ToolBar1: TToolBar
    Width = 840
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 840
    Height = 341
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 337
    ClientRectLeft = 4
    ClientRectRight = 836
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Provis'#245'es'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 832
        Height = 240
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource1
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdEmpresa'
            Footers = <>
            Width = 45
          end
          item
            EditButtons = <>
            FieldName = 'nCdLojaTit'
            Footers = <>
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
            Width = 81
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footers = <>
            Width = 135
          end
          item
            EditButtons = <>
            FieldName = 'cNmCategFinanc'
            Footers = <>
            Width = 172
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
            Width = 97
          end
          item
            EditButtons = <>
            FieldName = 'cOBSTit'
            Footers = <>
            ShowImageAndText = True
            TextEditing = False
            Width = 130
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 240
        Width = 832
        Height = 73
        Align = alBottom
        Caption = 'Observa'#231#227'o'
        TabOrder = 1
        object DBMemo1: TDBMemo
          Left = 2
          Top = 15
          Width = 828
          Height = 56
          Align = alClient
          DataField = 'cOBSTit'
          DataSource = DataSource1
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
  end
  object qryProvisoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLojaTit'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@nCdEmpresa  int'
      '       ,@nCdLojaTit  int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdEmpresa  = :nCdEmpresa'
      'Set @nCdLojaTit  = :nCdLojaTit'
      ''
      'SELECT Titulo.nCdEmpresa'
      '      ,Titulo.nCdLojaTit'
      '      ,Titulo.cNrTit'
      '      ,EspTit.cNmEspTit'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.cOBSTit'
      '      ,Titulo.nCdTitulo'
      '  FROM Titulo'
      
        '       INNER JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Ti' +
        'tulo.nCdCategFinanc'
      
        '       INNER JOIN EspTit      ON EspTit.nCdEspTit           = Ti' +
        'tulo.nCdEspTit'
      ' WHERE EspTit.cFlgPrev    = 1'
      '   AND Titulo.nCdEmpresa  = @nCdEmpresa'
      '   AND Titulo.nCdTerceiro = @nCdTerceiro'
      '   AND (   ( Titulo.nCdLojaTit = @nCdLojaTit )'
      '        OR ( @nCdLojaTit = 0 ) )'
      '   AND Titulo.nCdSP IS NULL'
      ' ORDER BY Titulo.dDtVenc')
    Left = 360
    Top = 224
    object qryProvisoesnCdEmpresa: TIntegerField
      DisplayLabel = 'Emp.'
      FieldName = 'nCdEmpresa'
    end
    object qryProvisoesnCdLojaTit: TIntegerField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLojaTit'
    end
    object qryProvisoescNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryProvisoescNmEspTit: TStringField
      DisplayLabel = 'Esp'#233'cie T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryProvisoescNmCategFinanc: TStringField
      DisplayLabel = 'Categoria Financeira'
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryProvisoesdDtVenc: TDateTimeField
      DisplayLabel = 'Data Venc.'
      FieldName = 'dDtVenc'
    end
    object qryProvisoesnValTit: TBCDField
      DisplayLabel = 'Valor Provis'#227'o'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryProvisoescOBSTit: TMemoField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cOBSTit'
      BlobType = ftMemo
    end
    object qryProvisoesnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProvisoes
    Left = 412
    Top = 229
  end
end
