unit rDRE_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptDRE_View = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro2: TQRLabel;
    lblFiltro3: TQRLabel;
    QRDBText13: TQRDBText;
    QRExpr3: TQRExpr;
    QRCSVFilter1: TQRCSVFilter;
    QRTextFilter1: TQRTextFilter;
    QRCompositeReport1: TQRCompositeReport;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    usp_RelatorionCdPlanoConta: TIntegerField;
    usp_RelatoriocNmPlanoConta: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatorionValAcumMes1: TFloatField;
    usp_RelatorionVariacao1: TFloatField;
    usp_RelatorionValAcumMes2: TFloatField;
    usp_RelatorionVariacao2: TFloatField;
    usp_RelatorionValAcumMes3: TFloatField;
    usp_RelatorionVariacao3: TFloatField;
    usp_RelatorionValAcumMes4: TFloatField;
    usp_RelatorionVariacao4: TFloatField;
    usp_RelatorionValAcumMes5: TFloatField;
    usp_RelatorionVariacao5: TFloatField;
    usp_RelatorionValAcumMes6: TFloatField;
    usp_RelatorionVariacao6: TFloatField;
    usp_RelatorionValAcumMes7: TFloatField;
    usp_RelatorionVariacao7: TFloatField;
    usp_RelatorionValAcumMes8: TFloatField;
    usp_RelatorionVariacao8: TFloatField;
    usp_RelatorionValAcumMes9: TFloatField;
    usp_RelatorionVariacao9: TFloatField;
    usp_RelatorionValAcumMes10: TFloatField;
    usp_RelatorionVariacao10: TFloatField;
    usp_RelatorionValAcumMes11: TFloatField;
    usp_RelatorionVariacao11: TFloatField;
    usp_RelatorionValAcumMes12: TFloatField;
    usp_RelatorionVariacao12: TFloatField;
    usp_RelatorionValTotal: TFloatField;
    usp_RelatorionVariacaoTotal: TFloatField;
    QRDBText5: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel1: TQRLabel;
    lblmes1: TQRLabel;
    lblmes3: TQRLabel;
    lblmes4: TQRLabel;
    lblmes5: TQRLabel;
    lblmes6: TQRLabel;
    lblmes7: TQRLabel;
    lblmes8: TQRLabel;
    lblmes9: TQRLabel;
    lblmes10: TQRLabel;
    lblmes11: TQRLabel;
    lblmes12: TQRLabel;
    QRLabel18: TQRLabel;
    lblmes2: TQRLabel;
    usp_RelatorioiNivel: TIntegerField;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    lblFiltro4: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape5: TQRShape;
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBText15Print(sender: TObject; var Value: String);
    procedure QRDBText17Print(sender: TObject; var Value: String);
    procedure QRDBText19Print(sender: TObject; var Value: String);
    procedure QRDBText21Print(sender: TObject; var Value: String);
    procedure QRDBText23Print(sender: TObject; var Value: String);
    procedure QRDBText25Print(sender: TObject; var Value: String);
    procedure QRDBText27Print(sender: TObject; var Value: String);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure formataExibicao(qrDBText:TQRDBText; iNivel:integer);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRDBText10Print(sender: TObject; var Value: String);
    procedure QRDBText12Print(sender: TObject; var Value: String);
    procedure QRDBText16Print(sender: TObject; var Value: String);
    procedure QRDBText18Print(sender: TObject; var Value: String);
    procedure QRDBText20Print(sender: TObject; var Value: String);
    procedure QRDBText22Print(sender: TObject; var Value: String);
    procedure QRDBText24Print(sender: TObject; var Value: String);
    procedure QRDBText26Print(sender: TObject; var Value: String);
    procedure QRDBText28Print(sender: TObject; var Value: String);
    procedure QRDBText29Print(sender: TObject; var Value: String);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDRE_View: TrptDRE_View;

implementation

{$R *.dfm}

procedure TrptDRE_View.QRDBText2Print(sender: TObject; var Value: String);
begin

    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

    {QRDBText2.Alignment := taLeftJustify ;}

    {if (usp_RelatorioiNivel.Value = 1) then
        QRDBText2.Font.Style := [fsUnderline] ;}

end;

procedure TrptDRE_View.QRDBText6Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText8Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText11Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText15Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText17Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText19Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText21Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText23Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText25Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText27Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText5Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.formataExibicao(qrDBText: TQRDBText;
  iNivel: integer);
begin

    if (iNivel = 1) or (iNivel = 3) then
    begin
        qrDBText.Font.Style := [] ;

        if (iNivel = 3) then
            qrDBText.Font.Style := [fsItalic, fsUnderline] ;

        if (usp_Relatorio.Parameters.ParamByName('@cTipo').Value = 'A') then
            qrDBText.Font.Style := [fsItalic, fsUnderline] ;
    end ;

    if (iNivel = 2) then
        qrDBText.Font.Style := [] ;

end;

procedure TrptDRE_View.QRDBText13Print(sender: TObject; var Value: String);
begin

    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText9Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText3Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText4Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;
end;

procedure TrptDRE_View.QRDBText7Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText10Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText12Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText16Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText18Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText20Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText22Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText24Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText26Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText28Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRDBText29Print(sender: TObject; var Value: String);
begin
    formataExibicao(sender as TQRDBText, usp_RelatorioiNivel.Value) ;

end;

procedure TrptDRE_View.QRBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

    Sender.Color := clWhite ;

    {if (usp_RelatorioiNivel.Value = 1) then
    begin
        if (usp_Relatorio.Parameters.ParamByName('@cTipo').Value = 'A') then
            Sender.Color := clSilver ;
    end ;

    if (usp_RelatorioiNivel.Value = 3) then
        Sender.Color := clTeal ;}

end;

end.
