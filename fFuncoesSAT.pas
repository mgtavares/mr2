unit fFuncoesSAT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Menus, DB, ADODB, ACBrSATClass, ACBrBase, pcnConversao, ACBrUtil, cxPC,
  cxControls, ScktComp, xmldom, XMLIntf, msxmldom, XMLDoc, ACBrSATExtratoFortesFr;

type
  TER2AmbienteSAT = (ambHom, ambProd);
  TER2TipoReqSAT = (tpStatusSAT, tpInformacaoSAT, tpVendaCFe, tpCancCFe);
  TER2StatusReqSAT = (stReqPend, stReqEmProc, stReqAtendOk, stReqAtendErro);

  TER2CFe = record
      iNrDocto  : Integer;
      cChaveCFe : String;
      cXMLCFe   : WideString;
      cNumSerie : String;
  end;

  TER2ReqSAT = record
      cFlgOk         : Integer;
      iCodRetornoSAT : Integer;
      cMsgRetornoSAT : String;
      cXMLCFe        : String;
      cPathXml       : String;
      tpAmbSAT       : TER2AmbienteSAT;
  end;

type
  TfrmFuncoesSAT = class(TfrmProcesso_Padrao)
    qryAmbiente: TADOQuery;
    qryAmbientecFlgCFeAtivo: TIntegerField;
    qryAmbientecModoEnvioCFe: TStringField;
    qryAmbientecCdAtivacaoSAT: TStringField;
    qryAmbientecAssinaturaACSAT: TStringField;
    qryAmbientenCdUFEmissaoCFe: TIntegerField;
    qryAmbientecCNPJACSAT: TStringField;
    qryAmbientecFlgTipoAmbienteCFe: TIntegerField;
    qryAmbientecModeloSAT: TStringField;
    qryTerceiroEmitente: TADOQuery;
    qryTerceiroEmitentenCdTerceiro: TIntegerField;
    qryTerceiroEmitentecCNPJCPF: TStringField;
    qryTerceiroEmitentenCdTabTipoEnquadTributario: TIntegerField;
    qryTerceiroEmitentecIE: TStringField;
    qryTerceiroEmitentecIM: TStringField;
    cxPageControl1: TcxPageControl;
    tabLog: TcxTabSheet;
    mResp: TMemo;
    btConsulta: TToolButton;
    btConfig: TToolButton;
    menuAtivacao: TPopupMenu;
    menuConfig: TPopupMenu;
    menuConsulta: TPopupMenu;
    btLigarSAT: TMenuItem;
    btAtivarSAT: TMenuItem;
    btAssociarAssinatura: TMenuItem;
    N4: TMenuItem;
    btBloquearSAT: TMenuItem;
    btDesbloquearSAT: TMenuItem;
    N5: TMenuItem;
    btTrocarCodAtivacao: TMenuItem;
    btConsultarInfoSAT: TMenuItem;
    btConsultarStatusSAT: TMenuItem;
    btConsultarNumSessao: TMenuItem;
    btAtualizarSAT: TMenuItem;
    btVisualizarLog: TMenuItem;
    N6: TMenuItem;
    btnAtivacao: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    N1: TMenuItem;
    qryAmbientecFlgCodSeguranca: TIntegerField;
    qryAmbientecFlgSATBloqueado: TIntegerField;
    qryAux: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    qryDoctoFiscal: TADOQuery;
    qryItemDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscalnCdEmpresa: TIntegerField;
    qryDoctoFiscalnCdLoja: TIntegerField;
    qryDoctoFiscalnCdSerieFiscal: TIntegerField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscalcModelo: TStringField;
    qryDoctoFiscalcSerie: TStringField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscaldDtSaida: TDateTimeField;
    qryDoctoFiscaldDtImpressao: TDateTimeField;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalcCNPJCPF: TStringField;
    qryDoctoFiscalcIE: TStringField;
    qryDoctoFiscalcTelefone: TStringField;
    qryDoctoFiscalnCdEndereco: TIntegerField;
    qryDoctoFiscalcEndereco: TStringField;
    qryDoctoFiscaliNumero: TIntegerField;
    qryDoctoFiscalcBairro: TStringField;
    qryDoctoFiscalcCidade: TStringField;
    qryDoctoFiscalcCep: TStringField;
    qryDoctoFiscalcUF: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalnValProduto: TBCDField;
    qryDoctoFiscalnValBaseICMS: TBCDField;
    qryDoctoFiscalnValICMS: TBCDField;
    qryDoctoFiscalnValBaseICMSSub: TBCDField;
    qryDoctoFiscalnValICMSSub: TBCDField;
    qryDoctoFiscalnValIsenta: TBCDField;
    qryDoctoFiscalnValOutras: TBCDField;
    qryDoctoFiscalnValDespesa: TBCDField;
    qryDoctoFiscalnValFrete: TBCDField;
    qryDoctoFiscaldDtCad: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCad: TIntegerField;
    qryDoctoFiscalnCdStatus: TIntegerField;
    qryDoctoFiscalcFlgFaturado: TIntegerField;
    qryDoctoFiscalnCdTerceiroTransp: TIntegerField;
    qryDoctoFiscalcNmTransp: TStringField;
    qryDoctoFiscalcCNPJTransp: TStringField;
    qryDoctoFiscalcIETransp: TStringField;
    qryDoctoFiscalcEnderecoTransp: TStringField;
    qryDoctoFiscalcBairroTransp: TStringField;
    qryDoctoFiscalcCidadeTransp: TStringField;
    qryDoctoFiscalcUFTransp: TStringField;
    qryDoctoFiscaliQtdeVolume: TIntegerField;
    qryDoctoFiscalnPesoBruto: TBCDField;
    qryDoctoFiscalnPesoLiq: TBCDField;
    qryDoctoFiscalcPlaca: TStringField;
    qryDoctoFiscalcUFPlaca: TStringField;
    qryDoctoFiscalnValIPI: TBCDField;
    qryDoctoFiscalnValFatura: TBCDField;
    qryDoctoFiscalnCdCondPagto: TIntegerField;
    qryDoctoFiscalnCdIncoterms: TIntegerField;
    qryDoctoFiscalnCdUsuarioImpr: TIntegerField;
    qryDoctoFiscalnCdTerceiroPagador: TIntegerField;
    qryDoctoFiscaldDtCancel: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCancel: TIntegerField;
    qryDoctoFiscalnCdTipoPedido: TIntegerField;
    qryDoctoFiscalcFlgIntegrado: TIntegerField;
    qryDoctoFiscalnValCredAdiant: TBCDField;
    qryDoctoFiscalnValIcentivoFiscal: TBCDField;
    qryDoctoFiscalnCdLanctoFinDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtContab: TDateTimeField;
    qryDoctoFiscaldDtIntegracao: TDateTimeField;
    qryDoctoFiscalnCdServidorOrigem: TIntegerField;
    qryDoctoFiscaldDtReplicacao: TDateTimeField;
    qryDoctoFiscalcFlgBoletoImpresso: TIntegerField;
    qryDoctoFiscalcXMLNFe: TMemoField;
    qryDoctoFiscalcNrProtocoloNFe: TStringField;
    qryDoctoFiscalcNrReciboNFe: TStringField;
    qryDoctoFiscalcComplEnderDestino: TStringField;
    qryDoctoFiscalnCdMunicipioDestinoIBGE: TIntegerField;
    qryDoctoFiscalnCdPaisDestino: TIntegerField;
    qryDoctoFiscalcNmPaisDestino: TStringField;
    qryDoctoFiscalcCNPJCPFEmitente: TStringField;
    qryDoctoFiscalcIEEmitente: TStringField;
    qryDoctoFiscalcNmRazaoSocialEmitente: TStringField;
    qryDoctoFiscalcNmFantasiaEmitente: TStringField;
    qryDoctoFiscalcTelefoneEmitente: TStringField;
    qryDoctoFiscalnCdEnderecoEmitente: TIntegerField;
    qryDoctoFiscalcEnderecoEmitente: TStringField;
    qryDoctoFiscaliNumeroEmitente: TIntegerField;
    qryDoctoFiscalcComplEnderEmitente: TStringField;
    qryDoctoFiscalcBairroEmitente: TStringField;
    qryDoctoFiscalnCdMunicipioEmitenteIBGE: TIntegerField;
    qryDoctoFiscalcCidadeEmitente: TStringField;
    qryDoctoFiscalcUFEmitente: TStringField;
    qryDoctoFiscalcCepEmitente: TStringField;
    qryDoctoFiscalnCdPaisEmitente: TIntegerField;
    qryDoctoFiscalcNmPaisEmitente: TStringField;
    qryDoctoFiscalcSuframa: TStringField;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryDoctoFiscalcNrProtocoloCancNFe: TStringField;
    qryDoctoFiscalcCaminhoXML: TStringField;
    qryDoctoFiscalcArquivoXML: TStringField;
    qryDoctoFiscalcFlgComplementar: TIntegerField;
    qryDoctoFiscalnCdDoctoFiscalOrigemCompl: TIntegerField;
    qryDoctoFiscalcNmMotivoComplemento: TStringField;
    qryDoctoFiscalnValSeguro: TBCDField;
    qryDoctoFiscalcFlgFormularioProprio: TIntegerField;
    qryDoctoFiscalnCdPedidoGerador: TIntegerField;
    qryDoctoFiscalcNmMotivoComplemento2: TStringField;
    qryDoctoFiscalcNrProtocoloDPEC: TStringField;
    qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal: TIntegerField;
    qryDoctoFiscalcFlgDPECTransmitida: TIntegerField;
    qryDoctoFiscalnValBaseIPI: TBCDField;
    qryDoctoFiscalnValPIS: TBCDField;
    qryDoctoFiscalnValCOFINS: TBCDField;
    qryDoctoFiscalcCdNumSerieECF: TStringField;
    qryDoctoFiscalcJustCancelamento: TStringField;
    qryDoctoFiscalcFlgCancForaPrazo: TIntegerField;
    qryDoctoFiscaliStatusRetorno: TIntegerField;
    qryDoctoFiscalcNmStatusRetorno: TStringField;
    qryDoctoFiscalnCdTabTipoModFrete: TIntegerField;
    qryDoctoFiscalnValTotalImpostoAproxFed: TBCDField;
    qryDoctoFiscalnValTotalImpostoAproxEst: TBCDField;
    qryDoctoFiscalnValTotalImpostoAproxMun: TBCDField;
    qryDoctoFiscalcFlgDevolucao: TIntegerField;
    qryDoctoFiscalcFlgCFe: TIntegerField;
    qryTabIBPT: TADOQuery;
    qryTabIBPTcChave: TStringField;
    qryTabIBPTiDiasValidade: TIntegerField;
    qryItemDoctoFiscalnCdProduto: TStringField;
    qryItemDoctoFiscalcNmItem: TStringField;
    qryItemDoctoFiscalcDescricaoAdicional: TStringField;
    qryItemDoctoFiscalcCFOP: TStringField;
    qryItemDoctoFiscalcNCM: TStringField;
    qryItemDoctoFiscalcCdST: TStringField;
    qryItemDoctoFiscalcCdSTIPI: TStringField;
    qryItemDoctoFiscalcUnidadeMedida: TStringField;
    qryItemDoctoFiscalcUnidadeMedidaTribuvel: TStringField;
    qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField;
    qryItemDoctoFiscalnValUnitario: TBCDField;
    qryItemDoctoFiscalnAliqICMS: TBCDField;
    qryItemDoctoFiscalnAliqIPI: TBCDField;
    qryItemDoctoFiscalnPercIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMS: TBCDField;
    qryItemDoctoFiscalnPercBCIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMSST: TBCDField;
    qryItemDoctoFiscalcCSTPIS: TStringField;
    qryItemDoctoFiscalnAliqPIS: TBCDField;
    qryItemDoctoFiscalcCSTCOFINS: TStringField;
    qryItemDoctoFiscalnAliqCOFINS: TBCDField;
    qryItemDoctoFiscalcEnqLegalIPI: TStringField;
    qryItemDoctoFiscalcEXTIPI: TStringField;
    qryItemDoctoFiscalnAliqICMSInternaOrigem: TBCDField;
    qryItemDoctoFiscalnQtde: TBCDField;
    qryItemDoctoFiscalnValTotal: TBCDField;
    qryItemDoctoFiscalnValBaseICMS: TBCDField;
    qryItemDoctoFiscalnValICMS: TBCDField;
    qryItemDoctoFiscalnValIPI: TBCDField;
    qryItemDoctoFiscalnValBaseIPI: TBCDField;
    qryItemDoctoFiscalnValBASEICMSSub: TBCDField;
    qryItemDoctoFiscalnValICMSSub: TBCDField;
    qryItemDoctoFiscalnValIsenta: TBCDField;
    qryItemDoctoFiscalnValOutras: TBCDField;
    qryItemDoctoFiscalnValDespesa: TBCDField;
    qryItemDoctoFiscalnValFrete: TBCDField;
    qryItemDoctoFiscalnValBasePIS: TBCDField;
    qryItemDoctoFiscalnValPIS: TBCDField;
    qryItemDoctoFiscalnValBaseCOFINS: TBCDField;
    qryItemDoctoFiscalnValCOFINS: TBCDField;
    qryItemDoctoFiscalnValIcentivoFiscal: TBCDField;
    qryItemDoctoFiscalnValDesconto: TBCDField;
    qryItemDoctoFiscalnCdTabTipoCodProdFat: TIntegerField;
    qryItemDoctoFiscalcCdFabricante: TStringField;
    qryItemDoctoFiscalcEAN: TStringField;
    qryItemDoctoFiscalnValImpostoAproxFed: TBCDField;
    qryItemDoctoFiscalnValImpostoAproxEst: TBCDField;
    qryItemDoctoFiscalnValImpostoAproxMun: TBCDField;
    qryDescontoTotal: TADOQuery;
    qryDescontoTotalnValDescontoItens: TBCDField;
    qryPrazoDoctoFiscal: TADOQuery;
    qryPrazoDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryPrazoDoctoFiscaliParcela: TIntegerField;
    qryPrazoDoctoFiscaldDtVenc: TDateTimeField;
    qryPrazoDoctoFiscalnValPagto: TBCDField;
    qryPrazoDoctoFiscalnCdTabTipoFormaPagto: TIntegerField;
    qryAmbientenCdCodPDVSAT: TIntegerField;
    N2: TMenuItem;
    btTesteFim: TMenuItem;
    btComunicarCert: TMenuItem;
    OpenDialog1: TOpenDialog;
    qryAmbientecFlgTipoImpSAT: TIntegerField;
    qryAmbienteiLarguraImpSAT: TIntegerField;
    qryAmbienteiTopoImpSAT: TIntegerField;
    qryAmbienteiFundoImpSAT: TIntegerField;
    qryAmbienteiEsquerdoImpSAT: TIntegerField;
    qryAmbienteiDireitoImpSAT: TIntegerField;
    qryAmbientecFlgPreviewImpSAT: TIntegerField;
    qryAmbientecPortaImpSAT: TStringField;
    qryAmbientecNmImpSATDefault: TStringField;
    qryAmbientecFlgSATCompartilhado: TIntegerField;
    qryAmbientecArquivoXMLRede: TMemoField;
    btConfigRede: TMenuItem;
    qryDoctoFiscalcChaveCanc: TStringField;
    qryDoctoFiscalcArquivoXMLCanc: TStringField;
    qryDoctoFiscalcXMLCanc: TMemoField;
    qryDoctoFiscalcCaminhoXMLCanc: TStringField;
    qryDoctoFiscalcNumSerieSAT: TStringField;
    qryEnviaReqServidorSAT: TADOQuery;
    qryRespReqServidorSAT: TADOQuery;
    qryEnviaReqServidorSATnCdTempRegistroSAT: TAutoIncField;
    ToolButton5: TToolButton;
    btnImpressao: TToolButton;
    menuImpressao: TPopupMenu;
    btImpExtratoVenda: TMenuItem;
    btImpExtratoVendaResumido: TMenuItem;
    N3: TMenuItem;
    btImpExtratoCancelamento: TMenuItem;
    qryBuscaCFe: TADOQuery;
    qryBuscaCFenCdDoctoFiscal: TIntegerField;
    qryBuscaCFecXML: TMemoField;
    qryBuscaCFenCdStatus: TIntegerField;
    qryBuscaCFecChaveNFe: TStringField;
    qryBuscaCFecChaveCanc: TStringField;
    qryAmbientenCdServidorSAT: TIntegerField;
    qryAmbientecNumSerieSAT: TStringField;
    qryAmbientecPathLogoCFe: TStringField;
    qryAmbientecModeloImpSAT: TStringField;
    btDesligarSAT: TMenuItem;
    qryAmbientecArquivoDllSAT: TStringField;
    qryRespReqServidorSATnCdTempRegistroSATAtend: TIntegerField;
    qryRespReqServidorSATcFlgOk: TIntegerField;
    qryRespReqServidorSATiCodRetornoSAT: TIntegerField;
    qryRespReqServidorSATcMsgRetornoSAT: TMemoField;
    qryRespReqServidorSATcXMLSAT: TMemoField;
    qryRespReqServidorSATcFlgAmbienteHom: TIntegerField;
    qryRespReqServidorSATdDtAtendimento: TDateTimeField;
    qryDoctoFiscalcFlgAmbienteHom: TIntegerField;
    qryAmbientecPageCodImpSAT: TStringField;
    qryAmbienteiLinhaImpSAT: TIntegerField;
    qryAmbienteiColunaImpSAT: TIntegerField;
    qryAmbienteiEspacoImpSAT: TIntegerField;
    qryAmbientecFlgImpUmaLinha: TIntegerField;
    qryDoctoFiscaliNrDoctoCanc: TIntegerField;
    qryRespReqServidorSATcCaminhoArquivo: TStringField;
    qryPrazoDoctoFiscalnCdTabTipoCredencCartao: TIntegerField;
    qryItemDoctoFiscalcCEST: TStringField;
    {$IFDEF DELPHI9_UP}
    procedure ACBrSAT1GetcodigoDeAtivacao(var Chave: AnsiString);
    procedure ACBrSAT1GetsignAC(var Chave: AnsiString);
    {$ELSE}
    procedure ACBrSAT1GetcodigoDeAtivacao(var Chave: String);
    procedure ACBrSAT1GetsignAC(var Chave: String);
  	{$ENDIF}
    procedure ACBrSAT1GravarLog(const ALogLine: String; var Tratado: Boolean);
    procedure btAssociarAssinaturaClick(Sender: TObject);
    procedure btAtivarSATClick(Sender: TObject);
    procedure btBloquearSATClick(Sender: TObject);
    procedure btTrocarCodAtivacaoClick(Sender: TObject);
    procedure btDesbloquearSATClick(Sender: TObject);
    procedure btConsultarInfoSATClick(Sender: TObject);
    procedure btConsultarNumSessaoClick(Sender: TObject);
    procedure btVisualizarLogClick(Sender: TObject);
    procedure btConsultarStatusSATClick(Sender: TObject);
    procedure btLigarSATClick(Sender: TObject);
    procedure btTesteFimClick(Sender: TObject);
    procedure btAtualizarSATClick(Sender: TObject);
    procedure btComunicarCertClick(Sender: TObject);
    procedure prAjustaACBrSAT;
    procedure prPreparaImpressao;
    procedure prImpExtratoVenda(nCdDoctoFiscal : Integer);
    procedure prImpExtratoVendaResumido(nCdDoctoFiscal : Integer);
    procedure prImpExtratoCancelamento(nCdDoctoFiscal : Integer);
    procedure prGeraVendaSAT(nCdDoctoFiscal : Integer);
    procedure prGeraCancSAT(nCdDoctoFiscal : Integer);
    function fnStatusSAT : Boolean;
    procedure btConfigRedeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btImpExtratoVendaClick(Sender: TObject);
    procedure btImpExtratoVendaResumidoClick(Sender: TObject);
    procedure btImpExtratoCancelamentoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btDesligarSATClick(Sender: TObject);
    function TipoReqSATToStr(const tpReqSAT : TER2TipoReqSAT) : String;
    function IntToAmbSAT(iAmbienteSAT : Integer) : TER2AmbienteSAT;
    function AmbSATToInt(const ambSAT : TER2AmbienteSAT) : Integer;
    function fnPathCFe(cPath : String) : String;
  private
    { Private declarations }
    cCNPJACHom   : String;
    cCNPJEmitHom : String;
    function fnGeraReqServidorSAT(tpReqSAT : TER2TipoReqSAT; nCdDoctoFiscal : Integer = 0) : TER2ReqSAT;
    function fnLeXMLCFe(cXMLCFe : WideString) : TER2CFe;

  public
    { Public declarations }
    ambSAT            : TER2AmbienteSAT;
    nCdCodPDVSAT      : Integer;
    cFlgSATBloqueado  : Integer;
    iQtdeListaInicial : Integer;
    iQtdeListaFinal   : Integer;
    cNumSerieSAT      : String;
    cVersaoLayoutCFe  : String;
    cNmEstadoSAT      : String;

  end;

var
  frmFuncoesSAT: TfrmFuncoesSAT;

implementation

{$R *.dfm}

uses
  fMenu, ACBrSAT, ACBrSATExtratoESCPOS, pcnCFe, StrUtils, ACBrPosPrinter;

{$IFDEF DELPHI9_UP}
procedure TfrmFuncoesSAT.ACBrSAT1GetcodigoDeAtivacao(var Chave: AnsiString);
begin
  Chave := qryAmbientecCdAtivacaoSAT.Value;
end;

procedure TfrmFuncoesSAT.ACBrSAT1GetsignAC(var Chave: AnsiString);
begin
  Chave := qryAmbientecAssinaturaACSAT.Value;
end;
{$ELSE}
procedure TfrmFuncoesSAT.ACBrSAT1GetcodigoDeAtivacao(var Chave: String);
begin
  Chave := qryAmbientecCdAtivacaoSAT.Value;
end;

procedure TfrmFuncoesSAT.ACBrSAT1GetsignAC(var Chave: String);
begin
  Chave := qryAmbientecAssinaturaACSAT.Value;
end;
{$ENDIF}

function TfrmFuncoesSAT.TipoReqSATToStr(const tpReqSAT : TER2TipoReqSAT) : String;
begin
  Result := '';

  case (tpReqSAT) of
      tpStatusSAT     : Result := 'S'; //consulta de status sat
      tpInformacaoSAT : Result := 'I'; //consulta de de inf. sat
      tpVendaCFe      : Result := 'V'; //venda cf-e
      tpCancCFe       : Result := 'C'; //cancelamento cf-e
  end;
end;

function TfrmFuncoesSAT.IntToAmbSAT(iAmbienteSAT : Integer) : TER2AmbienteSAT;
begin
  case (iAmbienteSAT) of
      0 : Result := ambProd;
      1 : Result := ambHom;
  end;
end;

function TfrmFuncoesSAT.AmbSATToInt(const ambSAT : TER2AmbienteSAT) : Integer;
begin
  case (ambSAT) of
      ambProd : Result := 0;
      ambHom  : Result := 1;
  end;
end;

function TfrmFuncoesSAT.fnPathCFe(cPath : String) : String;
begin
  Result := StringReplace(cPath, frmMenu.cPathSistema, '', [rfIgnoreCase]);
end;

procedure TfrmFuncoesSAT.FormCreate(Sender: TObject);
begin
  inherited;

  prAjustaACBrSAT;
end;

procedure TfrmFuncoesSAT.ACBrSAT1GravarLog(const ALogLine: String;
  var Tratado: Boolean);
begin
  mResp.Lines.Add(ALogLine);

  Tratado := False; //gerar arquivo autom�tico pelo componente
end;

procedure TfrmFuncoesSAT.prAjustaACBrSAT;
var
  satModelo  : TACBrSATModelo;
  pcnRegTrib : TpcnRegTrib;
begin
  frmMenu.mensagemUsuario('Configurando SAT...');

  PosicionaQuery(qryAmbiente, frmMenu.cNomeComputador);

  if (qryAmbientecFlgTipoAmbienteCFe.Value = 1) then
  begin
      ambSAT := ambHom;

      { -- dados de emiss�o em ambiente de homologa��o para teste   -- }
      
      { -- #### DIMEP ####                                          -- }
      { -- CNPJAC   : 16716114000172                                -- }
      { -- CNPJEmit : 61099008000141                                -- }
      { -- Ass. AC  : SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT -- }

      { -- #### Emulador Offline SEFAZ ####                          -- }
      { -- CNPJAC   : 22222222222222                                 -- }
      { -- CNPJEmit : 11111111111111                                 -- }
      { -- Ass. AC  : C�digo r�ndomico para teste com 344 caracteres -- }

      cCNPJACHom   := '11864296000105';    { -- CNPJ ER2 }
      cCNPJEmitHom := '11111111111111';    { -- CNPJ HOMOLOGA SAT SEFAZ/SP TESTE}
  end
  else
      ambSAT := ambProd;

  nCdCodPDVSAT := qryAmbientenCdCodPDVSAT.Value;
  cNumSerieSAT := qryAmbientecNumSerieSAT.Value;
  satModelo    := satNenhum;
  pcnRegTrib   := RTRegimeNormal;

  if (qryAmbientecModeloSAT.Value = 'satDinamico_cdecl') then
      satModelo := satDinamico_cdecl;

  if (qryAmbientecModeloSAT.Value = 'satDinamico_stdcall') then
      satModelo := satDinamico_stdcall;

  qryTerceiroEmitente.Close;
  qryTerceiroEmitente.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryTerceiroEmitente.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryTerceiroEmitente.Open;

  { -- se terceiro � optante do simples nacional informa regime trib. -- }
  if (qryTerceiroEmitentenCdTabTipoEnquadTributario.Value = 1) then
      pcnRegTrib := RTSimplesNacional;

  with (frmMenu.ACBrSAT1) do
  begin
      { -- configura eventos do componente SAT -- }
      OnGetcodigoDeAtivacao := ACBrSAT1GetcodigoDeAtivacao;
      OnGetsignAC           := ACBrSAT1GetsignAC;
      OnGravarLog           := ACBrSAT1GravarLog;

      if (qryAmbientecArquivoXMLRede.Value <> '') then
          Rede.SetXMLString(qryAmbientecArquivoXMLRede.Value);

      { -- configura propriedades do componente SAT -- }
      Modelo                       := satModelo;
      Config.ide_numeroCaixa       := nCdCodPDVSAT;
      Config.emit_IM               := qryTerceiroEmitentecIM.Value;
      Config.emit_cRegTrib         := pcnRegTrib;
      Config.emit_cRegTribISSQN    := RTISSNenhum;
      Config.emit_indRatISSQN      := irNao;
      Config.PaginaDeCodigo        := 65001;
      Config.EhUTF8                := True;
      Config.infCFe_versaoDadosEnt := 0.07; //StrToFloatDef(cVersaoLayoutCFe,0.07);
      ConfigArquivos.SalvarEnvio   := True;
      ConfigArquivos.SalvarCFe     := True;
      ConfigArquivos.SalvarCFeCanc := True;
      NomeDLL                      := frmMenu.cPathSistema + qryAmbientecArquivoDllSAT.Value;
      //ConfigArquivos.SepararPorMes := True;
      //ConfigArquivos.SepararPorCNPJ := True;

      case (ambSAT) of
          ambHom : begin
              Config.ide_tpAmb                    := taHomologacao;
              Config.ide_CNPJ                     := cCNPJACHom;
              Config.emit_CNPJ                    := cCNPJEmitHom;
              Config.emit_IE                      := '111111111111';
              ArqLOG                              := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\CFe\ER2SATHom.log';
              ConfigArquivos.PastaEnvio           := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\CFe\Envio e Resposta\';
              ConfigArquivos.PastaCFeVenda        := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\CFe\CFeVenda\';
              ConfigArquivos.PastaCFeCancelamento := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\CFe\CFeCanc\';
          end;
          ambProd : begin
              Config.ide_tpAmb                    := taProducao;
              Config.ide_CNPJ                     := qryAmbientecCNPJACSAT.Value;
              Config.emit_CNPJ                    := qryTerceiroEmitentecCNPJCPF.Value;
              Config.emit_IE                      := qryTerceiroEmitentecIE.Value;
              ArqLOG                              := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\CFe\ER2SATProd.log';
              ConfigArquivos.PastaEnvio           := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\CFe\Envio e Resposta\';
              ConfigArquivos.PastaCFeVenda        := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\CFe\CFeVenda\';
              ConfigArquivos.PastaCFeCancelamento := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\CFe\CFeCanc\';
          end;
      end;
  end;
end;

procedure TfrmFuncoesSAT.prPreparaImpressao;
var
  ppMarcaImpSAT : TACBrPosPrinterModelo;
  pcPagCodigo   : TACBrPosPaginaCodigo;
  imgLogo       : TPicture;
begin
  with (frmMenu) do
  begin
      if (qryAmbientecModeloImpSAT.Value = 'Epson') then
          ppMarcaImpSAT :=  ppEscPosEpson;

      if (qryAmbientecModeloImpSAT.Value = 'Bematech') then
          ppMarcaImpSAT := ppEscBematech;

      if (qryAmbientecModeloImpSAT.Value = 'Daruma') then
          ppMarcaImpSAT := ppEscDaruma;

      if (qryAmbientecModeloImpSAT.Value = 'Elgin') then
          ppMarcaImpSAT := ppEscVox;    //ppEscElgin;   Alterado 02/08/18 nova versoa ACBR por Marcelo

      if (qryAmbientecModeloImpSAT.Value = 'Diebold') then
          ppMarcaImpSAT := ppEscDiebold;

      if (qryAmbientecPageCodImpSAT.Value = 'pcNone') then
          pcPagCodigo := pcNone;

      if (qryAmbientecPageCodImpSAT.Value = 'pc437') then
          pcPagCodigo := pc437;

      if (qryAmbientecPageCodImpSAT.Value = 'pc850') then
          pcPagCodigo := pc850;

      if (qryAmbientecPageCodImpSAT.Value = 'pc852') then
          pcPagCodigo := pc852;

      if (qryAmbientecPageCodImpSAT.Value = 'pc860') then
          pcPagCodigo := pc860;

      if (qryAmbientecPageCodImpSAT.Value = 'pcUTF8') then
          pcPagCodigo := pcUTF8;

      if (qryAmbientecPageCodImpSAT.Value = 'pc1252') then
          pcPagCodigo := pc1252;

      { -- EscPOS -- }
      if (qryAmbientecFlgTipoImpSAT.Value = 1) then
      begin
          ACBrSAT1.Extrato                 := ACBrSATExtratoESCPOS1;
          ACBrSATExtratoESCPOS1.PosPrinter := ACBrPosPrinter1;

          ACBrSATExtratoESCPOS1.SoftwareHouse     := 'ER2Soft Solucoes Intel. para o seu Negocio';
          ACBrSATExtratoESCPOS1.Site              := 'www.er2soft.com.br';
          ACBrSATExtratoESCPOS1.ImprimeQRCode     := True;
          ACBrSATExtratoESCPOS1.MostrarPreview    := True;
          ACBrSATExtratoESCPOS1.ImprimeEmUmaLinha := (qryAmbientecFlgImpUmaLinha.Value = 1);

          ACBrPosPrinter1.Desativar;
          ACBrPosPrinter1.Modelo             := ppMarcaImpSAT;
          ACBrPosPrinter1.Porta              := qryAmbientecPortaImpSAT.Value;
          ACBrPosPrinter1.CortaPapel         := True;
          ACBrPosPrinter1.PaginaDeCodigo     := pcPagCodigo;
          ACBrPosPrinter1.LinhasEntreCupons  := qryAmbienteiLinhaImpSAT.Value;
          ACBrPosPrinter1.ColunasFonteNormal := qryAmbienteiColunaImpSAT.Value;
          ACBrPosPrinter1.EspacoEntreLinhas  := qryAmbienteiEspacoImpSAT.Value;

          if (Trim(qryAmbientecPathLogoCFe.Value) <> '') then
              ACBrSATExtratoESCPOS1.PictureLogo.LoadFromFile(qryAmbientecPathLogoCFe.Value);

          ACBrPosPrinter1.Ativar;
           sleep(500);
      end
      { -- Fortes Report -- }
      else
      begin

//         ACBrSATExtratoFortes1.Free;
//         ACBrSATExtratoFortes1 := TACBrSATExtratoFortes.Create(Application);

          ACBrSAT1.Extrato := ACBrSATExtratoFortes1;

          ACBrSATExtratoFortes1.SoftwareHouse    := 'ER2Soft Solucoes Intel. para o seu Negocio';
          ACBrSATExtratoFortes1.Site             := 'www.er2soft.com.br';
          ACBrSATExtratoFortes1.ImprimeQRCode    := True;
          ACBrSATExtratoFortes1.LarguraBobina    := qryAmbienteiLarguraImpSAT.Value;
          ACBrSATExtratoFortes1.Margens.Topo     := qryAmbienteiTopoImpSAT.Value;
          ACBrSATExtratoFortes1.Margens.Fundo    := qryAmbienteiFundoImpSAT.Value;
          ACBrSATExtratoFortes1.Margens.Esquerda := qryAmbienteiEsquerdoImpSAT.Value;
          ACBrSATExtratoFortes1.Margens.Direita  := qryAmbienteiDireitoImpSAT.Value;
          ACBrSATExtratoFortes1.MostrarPreview   := (qryAmbientecFlgPreviewImpSAT.Value = 1);

          if (Trim(qryAmbientecPathLogoCFe.Value) <> '') then
              ACBrSATExtratoFortes1.PictureLogo.LoadFromFile(qryAmbientecPathLogoCFe.Value);

          try
              if (Trim(qryAmbientecNmImpSATDefault.Value) <> '') then
                  ACBrSATExtratoFortes1.PrinterName := qryAmbientecNmImpSATDefault.Value;
          except
          end;
      end;
  end;
end;




procedure TfrmFuncoesSAT.btAtivarSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  //frmMenu.ACBrSAT1.ConsultarSAT;

  { -- se sat estiver em opera��o, finaliza processo -- }
  //if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno = 8000) then
  //    Exit;

  try

      case frmMenu.MessageDlg('Confirma a ativa��o do SAT ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo : begin
              Raise Exception.Create('Processo de ativa��o do SAT cancelado pelo usu�rio.');
              Exit;
          end;
      end;

      case (ambSAT) of
          ambHom  : frmMenu.ACBrSAT1.AtivarSAT(1,cCNPJEmitHom,qryAmbientenCdUFEmissaoCFe.Value);
          ambProd : frmMenu.ACBrSAT1.AtivarSAT(1,qryTerceiroEmitentecCNPJCPF.Value,qryAmbientenCdUFEmissaoCFe.Value);
      end;

      { -- 4000 : ativado corretamente -- }
      { -- 4003 : sat j� ativado       -- }
      if (   (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 4000)
          and(frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 4003)) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;

      frmMenu.ShowMessage('Equipamento SAT ativado com sucesso.');
  except
     frmMenu.MensagemErro('Erro ao ativar o equipamento SAT.');
     Raise;
  end;

//   Adicionado por Marcelo, removido por suspeita de excesso consumo cpu ao entrar no caixa com SAT
//   try
//       if not frmMenu.ACBrSAT1.Inicializado then
//          frmMenu.ACBrSAT1.Inicializar;
//          sleep(500);
//          frmMenu.ACBrSAT1.ConsultarSAT;
//          { -- se sat estiver em opera��o, finaliza processo -- }
//          if  not (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno = 8000) then
//          begin
//             frmMenu.MensagemErro('Erro Consultar o equipamento SAT Erro:'+ IntToSTR(frmMenu.ACBrSAT1.Resposta.codigoDeRetorno ));
//          end;
//   except
//     frmMenu.MensagemErro('Erro ao iniciar o equipamento SAT.');
//     Raise;
//   end;
//   Fim da Adicao
end;

procedure TfrmFuncoesSAT.btComunicarCertClick(Sender: TObject);
var
  slCertificado : TStringList;
begin
  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  { -- verifica modo de ativa��o do sat                 -- }
  { -- 1� utiliza assinatura gerado pela software house -- }
  { -- 2� utiliza certificado digital do cliente *.cert -- }

  if (Trim(qryAmbientecAssinaturaACSAT.Value) <> '') then
      Exit;

  ShowMessage('Equipamento SAT necessita vincular o certificado digital (*.cer).');

  OpenDialog1.Title      := 'Selecione o Arquivo';
  OpenDialog1.DefaultExt := '*.cer';
  OpenDialog1.Filter     := 'Certificados (*.cer)|*.cer|Arquivo Texto (*.txt)|*.txt';

  if (not OpenDialog1.Execute) then
  begin
      MensagemAlerta('Certificado digital n�o informado.');
      Exit;
  end;

  try
      try
          slCertificado := TStringList.Create;

          slCertificado.LoadFromFile(OpenDialog1.FileName);
          frmMenu.ACBrSAT1.ComunicarCertificadoICPBRASIL(slCertificado.Text);

          if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 5000) then
          begin
              Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
              Exit;
          end;
      except
          MensagemErro('Erro ao comunicar o certificado digital.');
          Raise;
      end;
  finally
      slCertificado.Free;
  end;
end;

procedure TfrmFuncoesSAT.btAssociarAssinaturaClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  frmMenu.ACBrSAT1.ConsultarSAT;

  { -- se sat estiver em opera��o, finaliza processo -- }
  if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno = 8000) then
      Exit;

  { -- verificar se existem documentos pendentes na fila -- }
  if ((qryAmbientecFlgCFeAtivo.OldValue = 1) and ((iQtdeListaInicial > 0) or (iQtdeListaFinal > 0))) then
  begin
      MensagemAlerta('Existem documentos pendentes na fila de transmiss�o do equipamento SAT.'
                    +#13
                    +'Envie os arquivos para associar nova assinatura.');
      Abort;
  end;

  {case frmMenu.MessageDlg('Confirma a associa��o da assinatura do SAT ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;}

  case (ambSAT) of
      ambHom  : frmMenu.ACBrSAT1.AssociarAssinatura(cCNPJACHom + cCNPJEmitHom, qryAmbientecAssinaturaACSAT.Value);
      ambProd : frmMenu.ACBrSAT1.AssociarAssinatura(qryAmbientecCNPJACSAT.Value + qryTerceiroEmitentecCNPJCPF.Value, qryAmbientecAssinaturaACSAT.Value);
  end;

  if (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 13000) then
  begin
      MensagemErro('Erro ao associar assinatura ao equipamento SAT.');
      Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
      Exit;
  end;
end;

procedure TfrmFuncoesSAT.btTrocarCodAtivacaoClick(Sender: TObject);
var
  cCdAtivacaoSATNovo     : String;
  cCdAtivacaoSATNovoConf : String;
  iTipoCodAtual          : Integer;
  cTipoCod               : String;
  cNmTipoCod             : String;
label
  solicitaCodAtivacaoSAT;
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  { -- cTipoCod      -- }
  { -- 1 : normal    -- }
  { -- 2 : seguran�a -- }
  cTipoCod               := '';
  cCdAtivacaoSATNovo     := '';
  cCdAtivacaoSATNovoConf := '';

  if (qryAmbientecFlgCodSeguranca.Value = 0) then
      iTipoCodAtual := 1  //c�digo de ativa��o
  else
      iTipoCodAtual := 2; //c�digo de seguran�a

  solicitaCodAtivacaoSAT:

  frmMenu.InputQuery('Trocar C�digo de Ativa��o','Informe o novo c�digo de ativa��o/emerg�ncia:', cCdAtivacaoSATNovo);

  if (Trim(cCdAtivacaoSATNovo) = '') then
      ShowMessage('C�digo de ativa��o/emerg�ncia n�o informado.')
  else
  begin
      frmMenu.InputQuery('Confirma��o do C�digo de Ativa��o','Confirme o novo c�digo de ativa��o/emerg�ncia:', cCdAtivacaoSATNovoConf);

      if (Trim(cCdAtivacaoSATNovo) <> Trim(cCdAtivacaoSATNovoConf)) then
      begin
          MensagemAlerta('Confirma��o do c�digo de ativa��o/emerg�ncia inv�lido.');

          cCdAtivacaoSATNovo     := '';
          cCdAtivacaoSATNovoConf := '';

          goto solicitaCodAtivacaoSAT;
      end
      else
      begin
          InputQuery('Trocar C�digo de Ativa��o','Tipo do c�digo informado (1 : ativa��o / 2 : emerg�ncia):',cTipoCod);

          cTipoCod := Trim(IntToStr(StrToIntDef(cTipoCod,0)));

          if ((cTipoCod <> '1') and (cTipoCod <> '2')) then
          begin
              frmMenu.MensagemAlerta('Tipo do c�digo de ativa��o inv�lido.');
              Exit;
          end;

          if (cTipoCod = '1') then
              cNmTipoCod := 'C�digo de ativa��o.'
          else
              cNmTipoCod := 'C�digo de emerg�ncia.';

          case frmMenu.MessageDlg('Confirma a altera��o do c�digo de ativa��o do SAT ?'
                         +#13#13
                         +'C�digo atual: ' + qryAmbientecCdAtivacaoSAT.Value + #13
                         +'Novo c�digo: '  + cCdAtivacaoSATNovo + #13
                         +'Tipo: '         + cNmTipoCod,mtConfirmation,[mbYes,mbNo],0) of
              mrNo : Exit;
          end;

          try
              frmMenu.ACBrSAT1.TrocarCodigoDeAtivacao(PChar(qryAmbientecCdAtivacaoSAT.Value), iTipoCodAtual, PChar(cCdAtivacaoSATNovo));

              if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 18000) then
              begin
                  Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
                  Exit;
              end;

              if (cTipoCod = '1') then
                  cTipoCod := '0'  //c�digo de ativa��o
              else
                  cTipoCod := '1'; //c�digo de seguran�a

              qryAmbiente.Edit;
              qryAmbientecCdAtivacaoSAT.Value   := cCdAtivacaoSATNovo;
              qryAmbientecFlgCodSeguranca.Value := StrToInt(cTipoCod);
              qryAmbiente.Post;

              PosicionaQuery(qryAmbiente, frmMenu.cNomeComputador);

              frmMenu.ShowMessage('C�digo de Ativa��o alterado com sucesso.');
          except
              MensagemErro('Erro ao efetivar altera��o do c�digo de ativa��o do SAT.');
              Raise;
          end;
      end;
  end;
end;

procedure TfrmFuncoesSAT.btBloquearSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  { -- verificar se existem documentos pendentes na fila -- }
  btConsultarInfoSAT.Click;
  
  if ((iQtdeListaInicial > 0) or (iQtdeListaFinal > 0)) then
  begin
      MensagemAlerta('Existem documentos pendentes na fila de transmiss�o do equipamento SAT.'
                    +#13
                    +'Envie os arquivos para associar nova assinatura.');
      Abort;
  end;

  {if (cNmEstadoSAT <> 'DESBLOQUEADO') then
  begin
      frmMenu.MensagemAlerta('Equipamento SAT j� esta bloqueado.');
      Exit;
  end;}

  case frmMenu.MessageDlg('Confirma o bloqueio do equipamento SAT ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.ACBrSAT1.BloquearSAT;

      if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 16000) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;
  except
      frmMenu.MensagemErro('Erro ao efetuar o bloqueio do equipamento SAT.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btDesbloquearSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  btConsultarInfoSAT.Click;

  {if (cNmEstadoSAT = 'DESBLOQUEADO') then
  begin
      frmMenu.MensagemAlerta('Equipamento SAT j� esta desbloqueado.');
      Exit;
  end;}

  case frmMenu.MessageDlg('Confirma o desbloqueio do equipamento SAT ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.ACBrSAT1.DesbloquearSAT;

      if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 17000) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;
  except
      frmMenu.MensagemErro('Erro ao efetuar o desbloqueio do equipamento SAT.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btConsultarInfoSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      cNumSerieSAT := fnGeraReqServidorSAT(tpInformacaoSAT).cXMLCFe;
      Exit;
  end;

  if (not fnStatusSAT) then
      Exit;

  { -- status de opera��o            -- }
  { -- 0 : DESBLOQUEADO              -- }
  { -- 1 : BLOQUEIO SEFAZ            -- }
  { -- 2 : BLOQUEIO CONTRIBUINTE     -- }
  { -- 3 : BLOQUEIO AUT�NOMO         -- }
  { -- 4 : BLOQUEIO PARA DESATIVA��O -- }

  frmMenu.ACBrSAT1.ConsultarStatusOperacional;

  if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 10000) then
  begin
      MensagemErro('Erro ao consultar informa��es do SAT.');
      Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
      Exit;
  end;

  with (frmMenu.ACBrSAT1.Status) do
  begin
      mResp.Lines.Add('NSERIE.........: ' + NSERIE);
      mResp.Lines.Add('LAN_MAC........: ' + LAN_MAC);
      mResp.Lines.Add('STATUS_LAN.....: ' + StatusLanToStr(STATUS_LAN));
      mResp.Lines.Add('NIVEL_BATERIA..: ' + NivelBateriaToStr(NIVEL_BATERIA));
      mResp.Lines.Add('MT_TOTAL.......: ' + MT_TOTAL);
      mResp.Lines.Add('MT_USADA.......: ' + MT_USADA);
      mResp.Lines.Add('DH_ATUAL.......: ' + DateTimeToStr(DH_ATUAL));
      mResp.Lines.Add('VER_SB.........: ' + VER_SB);
      mResp.Lines.Add('VER_LAYOUT.....: ' + VER_LAYOUT);
      mResp.Lines.Add('ULTIMO_CFe.....: ' + ULTIMO_CFe);
      mResp.Lines.Add('LISTA_INICIAL..: ' + LISTA_INICIAL);
      mResp.Lines.Add('LISTA_FINAL....: ' + LISTA_FINAL);
      mResp.Lines.Add('DH_CFe.........: ' + DateTimeToStr(DH_CFe));
      mResp.Lines.Add('DH_ULTIMA......: ' + DateTimeToStr(DH_CFe));
      mResp.Lines.Add('CERT_EMISSAO...: ' + DateToStr(CERT_EMISSAO));
      mResp.Lines.Add('CERT_VENCIMENTO: ' + DateToStr(CERT_VENCIMENTO));
      mResp.Lines.Add('ESTADO_OPERACAO: ' + EstadoOperacaoToStr(ESTADO_OPERACAO));

      cNumSerieSAT      := NSERIE;
      cVersaoLayoutCFe  := VER_LAYOUT;
      iQtdeListaInicial := StrToIntDef(LISTA_INICIAL,0);
      iQtdeListaFinal   := StrToIntDef(LISTA_FINAL,0);
      cNmEstadoSAT      := EstadoOperacaoToStr(ESTADO_OPERACAO);
  end;
end;

procedure TfrmFuncoesSAT.btConsultarStatusSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  frmMenu.ACBrSAT1.ConsultarSAT;

  if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 8000) then
  begin
      MensagemErro('Erro ao consultar status do SAT.');
      Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
      Exit;
  end;
end;

procedure TfrmFuncoesSAT.btConsultarNumSessaoClick(Sender: TObject);
var
  cNumSessao : String;
  iNumSessao : Integer;
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  cNumSessao := '';
  iNumSessao := 0;

  if (not InputQuery('Consultar N�mero de Sess�o','Informe o n�mero de sess�o:', cNumSessao)) then
      Exit;

  iNumSessao := StrToIntDef(Trim(cNumSessao), 0);

  if (iNumSessao <= 0) then
  begin
      frmMenu.MensagemErro('N�mero de sess�o informado � inv�lido.');
      Abort;
  end;

  frmMenu.ACBrSAT1.ConsultarNumeroSessao(iNumSessao);
end;
procedure TfrmFuncoesSAT.btVisualizarLogClick(Sender: TObject);
var
  cPathCFe : String;
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  case (ambSAT) of
      ambHom  : cPathCFe := 'Homologacao\CFe\ER2SATHom.log';
      ambProd : cPathCFe := 'Producao\CFe\ER2SATProd.log';
  end;

  OpenURL(frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + cPathCFe);
end;

procedure TfrmFuncoesSAT.btLigarSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  prAjustaACBrSAT;

  try
      frmMenu.ACBrSAT1.Inicializar;
  except
      frmMenu.MensagemErro('Erro ao inicializar o SAT, verifica as configura��es do equipamento.');
      Raise;
  end;

  btAtivarSAT.Click;
  btComunicarCert.Click;
  btAssociarAssinatura.Click;
end;

procedure TfrmFuncoesSAT.btDesligarSATClick(Sender: TObject);
begin
  inherited;

  frmMenu.ACBrSAT1.DesInicializar;
end;

procedure TfrmFuncoesSAT.prGeraVendaSAT(nCdDoctoFiscal : Integer);
var
  cArquivoXML    : WideString;
  iItem          : Integer;
  iDiasIBPT      : Integer;
  iQtdeTentativa : Integer;
  cCdST          : String;
  ok             : Boolean;
  er2CFe         : TER2CFe;
  er2ReqSAT      : TER2ReqSAT;
label
  enviaVendaSAT;
begin
  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      er2ReqSAT := fnGeraReqServidorSAT(tpVendaCFe, nCdDoctoFiscal);
      er2CFe    := fnLeXMLCFe(er2ReqSAT.cXMLCFe);

      { -- atualiza informa��es do cupom fiscal -- }
      PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

      qryDoctoFiscal.Edit;
      qryDoctoFiscaliNrDocto.Value        := er2CFe.iNrDocto;
      qryDoctoFiscalcChaveNFe.Value       := er2CFe.cChaveCFe;
      qryDoctoFiscalcArquivoXML.Value     := 'AD' + er2CFe.cChaveCFe + '.xml';
      qryDoctoFiscalcXMLNFe.Value         := er2CFe.cXMLCFe;
      qryDoctoFiscalcNumSerieSAT.Value    := er2CFe.cNumSerie;
      qryDoctoFiscalcCaminhoXML.Value     := er2ReqSAT.cPathXml;
      qryDoctoFiscalcFlgAmbienteHom.Value := AmbSATToInt(er2ReqSAT.tpAmbSAT);
      qryDoctoFiscal.Post;

      Exit;
  end;

  { -- verifica status do sat -- }
  if (not fnStatusSAT) then
      Exit;

  frmMenu.mensagemUsuario('Gerando CF-e...');

  PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));
  PosicionaQuery(qryItemDoctoFiscal, IntToStr(nCdDoctoFiscal));
  PosicionaQuery(qryPrazoDoctoFiscal, IntToStr(nCdDoctoFiscal));

  frmMenu.ACBrSAT1.CFe.IdentarXML       := True;
  frmMenu.ACBrSAT1.CFe.TamanhoIdentacao := 3;

  btConsultarInfoSAT.Click;

  frmMenu.ACBrSAT1.InicializaCFe;

  with (frmMenu.ACBrSAT1.CFe) do
  begin
      { -- identifica��o do CFe -- }
      Ide.numeroCaixa :=  nCdCodPDVSAT;

      Dest.CNPJCPF := qryDoctoFiscalcCNPJCPF.Value;
      Dest.xNome   := qryDoctoFiscalcNmTerceiro.Value;

      { -- endere�o de entrega caso haja entrega a domic�lio -- }
      { -- obs: removido c�digo abaixo devido n�o haver entrega domic�lio -- }
      {Entrega.xLgr    := qryDoctoFiscalcEndereco.Value;
      Entrega.nro     := qryDoctoFiscaliNumero.AsString;
      Entrega.xBairro := qryDoctoFiscalcBairro.Value;
      Entrega.xCpl    := qryDoctoFiscalcComplEnderDestino.Value;
      Entrega.xMun    := qryDoctoFiscalcCidade.Value;
      Entrega.UF      := qryDoctoFiscalcUF.Value;}

      { -- dados dos itens -- }
      qryItemDoctoFiscal.First;
      Inc(iItem);

      { -- verifica limite de itens -- }
      if (qryItemDoctoFiscal.RecordCount > 500) then
      begin
          Raise Exception.Create('Quantidade de intens maior do que o limite permitido.'
                                +#13#13
                                +'Qtde. m�xima: 500.'
                                +#13
                                +'Qtde. informada: ' + IntToStr(qryItemDoctoFiscal.RecordCount) + '.');
      end;

      while (not qryItemDoctoFiscal.Eof) do
      begin
          with (Det.Add) do
          begin
              { -- 1 : c�digo pr�prio    -- }
              { -- 2 : c�digo fabricante -- }
              { -- 3 : c�digo barra      -- }
              { -- 4 : n�o informar      -- }

              { -- c�digo pr�prio ou n�o informar c�digo -- }
              if ((qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <= 1) or (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 4)) then
                  Prod.cProd := qryItemDoctoFiscalnCdProduto.Value;

              { -- c�digo do fabricante -- }
              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 2) then
              begin
                  if (Length(Trim(qryItemDoctoFiscalcCdFabricante.Value)) <= 0) then
                  begin
                      Raise Exception.Create('C�digo do fabricante n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                  end;

                  Prod.cProd := qryItemDoctoFiscalcCdFabricante.Value;
              end;

              { -- c�digo EAN -- }
              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 3) then
              begin
                  if (Length(Trim(qryItemDoctoFiscalcEAN.Value)) <= 0) then
                  begin
                      Raise Exception.Create('C�digo EAN/UPC n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                  end;

                  Prod.cProd := qryItemDoctoFiscalcEAN.Value;
              end;

              { -- se produto possuir EAN e deve informar c�digo -- }
              if ((Trim(qryItemDoctoFiscalcEAN.Value ) <> '') and (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <> 4)) then
              begin
                  Prod.cEAN := qryItemDoctoFiscalcEAN.Value;
              end;

              nItem         := iItem;
              Prod.xProd    := qryItemDoctoFiscalcNmItem.Value;
              Prod.NCM      := qryItemDoctoFiscalcNCM.Value;
              Prod.CFOP     := qryItemDoctoFiscalcCFOP.Value;
              Prod.uCom     := qryItemDoctoFiscalcUnidadeMedida.Value;
              Prod.indRegra := irArredondamento;
              Prod.qCom     := qryItemDoctoFiscalnQtde.Value;
              Prod.vUnCom   := qryItemDoctoFiscalnValUnitario.Value;
              Prod.vOutro   := qryItemDoctoFiscalnValOutras.Value;
              Prod.vDesc    := qryItemDoctoFiscalnValDesconto.Value;
              Prod.vItem    := qryItemDoctoFiscalnValTotal.Value;
              Prod.vRatDesc := 0; //se houve acr�scimo/desconto do subtotal do pedido
              Prod.vRatAcr  := 0; //efetuar rateio sobre os itens com valor l�quido
                                  //(campo obrigat�rio a partir de 01/01/2016 vide nota t�cnica SAT)
              Prod.CEST     := qryItemDoctoFiscalcCEST.Value;

              {with (Prod.obsFiscoDet.Add) do
              begin
                  xCampoDet := 'campo';
                  xTextoDet := 'texto';
              end;}

              with (Imposto) do
              begin
                  { -- inf. valor do imposto aproximado no item (federal + estadual + municipal) -- }
                  Imposto.vItem12741 := qryItemDoctoFiscalnValImpostoAproxFed.Value + qryItemDoctoFiscalnValImpostoAproxEst.Value + qryItemDoctoFiscalnValImpostoAproxMun.Value;

                  { -- icms -- }
                  with (ICMS) do
                  begin
                      cCdST := Trim(qryItemDoctoFiscalcCdST.Value);

                      case (StrToInt(cCdST)) of
                          0   : CST   := cst00;
                          10  : CST   := cst10;
                          20  : CST   := cst20;
                          30  : CST   := cst30;
                          40  : CST   := cst40;
                          41  : CST   := cst41;
                          45  : CST   := cst45;
                          50  : CST   := cst50;
                          51  : CST   := cst51;
                          60  : CST   := cst60;
                          70  : CST   := cst70;
                          80  : CST   := cst80;
                          81  : CST   := cst81;
                          90  : CST   := cst90;
                          101 : CSOSN := csosn101;
                          102 : CSOSN := csosn102;
                          103 : CSOSN := csosn103;
                          201 : CSOSN := csosn201;
                          202 : CSOSN := csosn202;
                          203 : CSOSN := csosn203;
                          300 : CSOSN := csosn300;
                          400 : CSOSN := csosn400;
                          500 : CSOSN := csosn500;
                          900 : CSOSN := csosn900;
                      end;

                      if (StrToInt(cCdST) > 90) then
                          ICMS.CSOSN := CSOSN
                      else
                          ICMS.CST   := CST;

                      ICMS.pICMS := qryItemDoctoFiscalnAliqICMS.Value;
                      ICMS.vICMS := qryItemDoctoFiscalnValICMS.Value;
                      ICMS.orig  := TpcnOrigemMercadoria(qryItemDoctoFiscalnCdTabTipoOrigemMercadoria.Value);
                  end;

                  { -- pis -- }
                  PIS.CST  := StrToCSTPIS(ok, qryItemDoctoFiscalcCSTPIS.Value);
                  PIS.vBC  := qryItemDoctoFiscalnValBasePIS.Value;
                  PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value/100;
                  PIS.vPIS := qryItemDoctoFiscalnValPIS.Value;

                  if (   (qryItemDoctoFiscalcCSTPIS.Value = '03')
                      or (qryItemDoctoFiscalcCSTPIS.Value = '99')) then
                  begin
                      PIS.qBCProd   := qryItemDoctoFiscalnQtde.Value;
                      PIS.vAliqProd := qryItemDoctoFiscalnAliqPIS.Value;
                  end
                  else
                      PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value/100;

                  { -- cofins -- }
                  COFINS.CST     := StrToCSTCOFINS(ok, qryItemDoctoFiscalcCSTCOFINS.Value);
                  COFINS.vBC     := qryItemDoctoFiscalnValBaseCOFINS.Value;
                  COFINS.vCOFINS := qryItemDoctoFiscalnValCOFINS.Value;

                  if (   (qryItemDoctoFiscalcCSTCOFINS.Value = '03')
                      or (qryItemDoctoFiscalcCSTCOFINS.Value = '99')) then
                  begin
                      COFINS.qBCProd   := qryItemDoctoFiscalnQtde.Value;
                      COFINS.vAliqProd := qryItemDoctoFiscalnAliqCOFINS.Value;
                  end
                  else
                      COFINS.pCOFINS := qryItemDoctoFiscalnAliqCOFINS.Value/100;
              end;
          end;

          qryItemDoctoFiscal.Next;
          Inc(iItem);
      end;

      qryItemDoctoFiscal.Close;

      { -- dados de totaliza��o -- }
      qryDescontoTotal.Close;
      qryDescontoTotal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
      qryDescontoTotal.Open;

      Total.ICMSTot.vICMS   := qryDoctoFiscalnValICMS.Value;
      Total.ICMSTot.vPIS    := qryDoctoFiscalnValPIS.Value ;
      Total.ICMSTot.vCOFINS := qryDoctoFiscalnValCOFINS.Value ;
      Total.ICMSTot.vOutro  := qryDoctoFiscalnValOutras.Value;
      Total.ICMSTot.vProd   := qryDoctoFiscalnValProduto.Value + qryDescontoTotalnValDescontoItens.Value;
      Total.ICMSTot.vDesc   := qryDescontoTotalnValDescontoItens.Value;

      Total.DescAcrEntr.vAcresSubtot := 0; //se houve acr�scimo/desconto do subtotal do pedido
      Total.DescAcrEntr.vDescSubtot  := 0; //efetuar rateio sobre os itens com valor l�quido
      Total.vCFeLei12741             := qryDoctoFiscalnValTotalImpostoAproxFed.Value + qryDoctoFiscalnValTotalImpostoAproxEst.Value + qryDoctoFiscalnValTotalImpostoAproxMun.Value;

      { -- dados de pagamento -- }
      qryPrazoDoctoFiscal.First;

      while (not qryPrazoDoctoFiscal.Eof) do
      begin
          with (Pagto.Add) do
          begin
              case (qryPrazoDoctoFiscalnCdTabTipoFormaPagto.Value) of
                //0 : cMP := mpCreditoLoja;     //cMP : 05 - devolu��es
                  1 : cMP := mpDinheiro;        //cMP : 01 - dinheiro
                  2 : cMP := mpCheque;          //cMP : 02 - chegue
                  3 : cMP := mpCartaodeDebito;  //cMP : 04 - cart�o d�bito
                  4 : cMP := mpCartaodeCredito; //cMP : 03 - cart�o cr�dito
                  5 : cMP := mpOutros;          //cMP : 99 - credi�rio
                  6 : cMP := mpValePresente;    //cMP : 12 - vale presente/mercadoria
              end;

              vMP := qryPrazoDoctoFiscalnValPagto.Value;

              { -- se foi pago em cart�o, informa credenciadora respons. -- }
              if (qryPrazoDoctoFiscalnCdTabTipoFormaPagto.Value in[3,4]) then
                  cAdmC := qryPrazoDoctoFiscalnCdTabTipoCredencCartao.Value;
          end;

          qryPrazoDoctoFiscal.Next;
      end;

      qryPrazoDoctoFiscal.Close;

      { -- inclui informa��es sobre devolu��o (troca/defeito) -- }
      //InfAdic.infCpl := 'Total de trocas/defeitos: R$ 0,00;';

      qryTabIBPT.Close;
      qryTabIBPT.Open;

      iDiasIBPT := StrToIntDef(frmMenu.LeParametro('DIASVENCTABIBPT'),0);

      if (not qryTabIBPT.IsEmpty) then
      begin
          if ((qryTabIBPTiDiasValidade.Value <= iDiasIBPT) and (iDiasIBPT > 0)) then
              MensagemAlerta('Faltam ' + qryTabIBPTiDiasValidade.AsString + ' dias para o vencimento das al�quotas para atendimento da lei 12.741 da Transpar�ncia Fiscal.'
                            +#13#13
                            +'Caso n�o seja atualizado dentro do prazo, o c�lculo deixar� de aparecer no cupom fiscal.');

          { -- removido pois trunk2 trata descri��o dos tributos automaticamente -- }
          {if (InfAdic.infCpl <> '') then
              infAdic.infCpl := infAdic.infCpl + ';';

          InfAdic.infCpl := InfAdic.infCpl + 'Valor Aprox. Tributos: ';

          if (qryDoctoFiscalnValTotalImpostoAproxFed.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + 'Federal R$ ' + qryDoctoFiscalnValTotalImpostoAproxFed.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxEst.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Estadual R$ ' + qryDoctoFiscalnValTotalImpostoAproxEst.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxMun.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Municipal R$ ' + qryDoctoFiscalnValTotalImpostoAproxMun.AsString;

          InfAdic.infCpl := InfAdic.infCpl + ' Fonte: IBPT/FECOMERCIO (' + qryTabIBPTcChave.Value + ').';}
      end;
  end;

  frmMenu.mensagemUsuario('Gerando XML...');

  try
      try
          iQtdeTentativa := 0;

          enviaVendaSAT:

          Inc(iQtdeTentativa);

          cArquivoXML := frmMenu.ACBrSAT1.CFe.GerarXML(True);

          frmMenu.mensagemUsuario('Transmitindo CF-e...');
          frmMenu.ACBrSAT1.EnviarDadosVenda(cArquivoXML);

          if (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 6000) then
          begin
              { -- se houver problemas de envio, efetua nova tentativa -- }
              { -- projetado no m�ximo 3 tentativas                    -- }
              if (iQtdeTentativa <= 3) then
              begin
                  frmMenu.mensagemUsuario('Transmitindo CF-e ' + IntToStr(iQtdeTentativa) + '� tent...');
                  goto enviaVendaSAT;
              end
              else
              begin
                  Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
                  Exit;
              end;
          end;

          frmMenu.mensagemUsuario('Atualizando Inf. CF-e...');

          { -- atualiza informa��es do cupom fiscal -- }
          PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

          qryDoctoFiscal.Edit;
          qryDoctoFiscaliNrDocto.Value        := frmMenu.ACBrSAT1.CFe.ide.nCFe;
          qryDoctoFiscalcChaveNFe.Value       := frmMenu.ACBrSAT1.CFe.infCFe.ID;
          qryDoctoFiscalcArquivoXML.Value     := 'AD' + frmMenu.ACBrSAT1.CFe.infCFe.ID + '.xml';
          qryDoctoFiscalcXMLNFe.Value         := frmMenu.ACBrSAT1.CFe.AsXMLString;
          qryDoctoFiscalcCaminhoXML.Value     := fnPathCFe(frmMenu.ACBrSAT1.ConfigArquivos.PastaCFeVenda + '\');
          qryDoctoFiscalcNumSerieSAT.Value    := cNumSerieSAT;
          qryDoctoFiscalcFlgAmbienteHom.Value := AmbSATToInt(ambSAT);
          qryDoctoFiscal.Post;
      except
          frmMenu.MensagemErro('Erro ao enviar os dados de venda ao SAT, tente novamente.');
          Raise;
      end;
  finally
      frmMenu.mensagemUsuario('');
  end;
end;

procedure TfrmFuncoesSAT.prGeraCancSAT(nCdDoctoFiscal : Integer);
var
  cArquivoXMLCanc : WideString;
  cChaveCanc      : String;
  er2CFe          : TER2CFe;
  er2ReqSAT       : TER2ReqSAT;
begin
  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      er2ReqSAT := fnGeraReqServidorSAT(tpCancCFe, nCdDoctoFiscal);
      er2CFe    := fnLeXMLCFe(er2ReqSAT.cXMLCFe);

      { -- atualiza informa��es do cupom fiscal -- }
      PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

      qryDoctoFiscal.Edit;
      qryDoctoFiscaliNrDoctoCanc.Value     := er2CFe.iNrDocto;
      qryDoctoFiscalnCdStatus.Value        := 2;
      qryDoctoFiscaldDtCancel.Value        := Now();
      qryDoctoFiscalnCdUsuarioCancel.Value := frmMenu.nCdUsuarioLogado;
      qryDoctoFiscalcChaveCanc.Value       := er2CFe.cChaveCFe;
      qryDoctoFiscalcArquivoXMLCanc.Value  := 'ADC' + er2CFe.cChaveCFe + '.xml';
      qryDoctoFiscalcXMLCanc.Value         := er2CFe.cXMLCFe;
      qryDoctoFiscalcCaminhoXMLCanc.Value  := er2ReqSAT.cPathXml;
      qryDoctoFiscalcFlgAmbienteHom.Value  := AmbSATToInt(er2ReqSAT.tpAmbSAT);
      qryDoctoFiscal.Post;

      prImpExtratoCancelamento(nCdDoctoFiscal);

      Exit;
  end;

  { -- verifica status do sat -- }
  if (not fnStatusSAT) then
      Exit;

  btConsultarInfoSAT.Click;

  PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

  if ((cNumSerieSAT = '') or (cNumSerieSAT <> qryDoctoFiscalcNumSerieSAT.Value)) then
  begin
      Raise Exception.Create('N�mero de s�rie do equipamento SAT difere do n�mero de s�rie vinculado ao cupom fiscal eletr�nico. '
                            +'Envie a informa��o de cancelamento para o equipamento SAT de origem.'
                            +' (N�m. SAT: ' + cNumSerieSAT
                            +' / N�m. CF-e: ' + qryDoctoFiscalcNumSerieSAT.Value + ')');
      Exit;
  end;

  try
      frmMenu.mensagemUsuario('Gerando Canc. CF-e...');

      frmMenu.ACBrSAT1.CFe.IdentarXML       := True;
      frmMenu.ACBrSAT1.CFe.TamanhoIdentacao := 3;
      frmMenu.ACBrSAT1.CFe.SetXMLString(qryDoctoFiscalcXMLNFe.Value);
      frmMenu.ACBrSAT1.CFe2CFeCanc;

      cChaveCanc      := frmMenu.ACBrSAT1.CFeCanc.infCFe.chCanc;
      cArquivoXMLCanc := frmMenu.ACBrSAT1.CFeCanc.GerarXML(True);

      frmMenu.mensagemUsuario('Transmitindo Canc. CF-e...');
      frmMenu.ACBrSAT1.CancelarUltimaVenda(cChaveCanc, cArquivoXMLCanc);

      if (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 7000) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;

      frmMenu.mensagemUsuario('Atualizando Inf. CF-e...');

      { -- atualiza informa��es do cupom fiscal -- }
      qryDoctoFiscal.Edit;
      qryDoctoFiscaliNrDoctoCanc.Value     := frmMenu.ACBrSAT1.CFeCanc.ide.nCFe;
      qryDoctoFiscalnCdStatus.Value        := 2;
      qryDoctoFiscaldDtCancel.Value        := Now();
      qryDoctoFiscalnCdUsuarioCancel.Value := frmMenu.nCdUsuarioLogado;
      qryDoctoFiscalcChaveCanc.Value       := frmMenu.ACBrSAT1.CFeCanc.infCFe.ID;
      qryDoctoFiscalcArquivoXMLCanc.Value  := 'ADC' + frmMenu.ACBrSAT1.CFeCanc.infCFe.ID + '.xml';
      qryDoctoFiscalcXMLCanc.Value         := frmMenu.ACBrSAT1.CFeCanc.AsXMLString;
      qryDoctoFiscalcCaminhoXMLCanc.Value  := fnPathCFe(frmMenu.ACBrSAT1.ConfigArquivos.PastaCFeCancelamento + '\');
      qryDoctoFiscalcFlgAmbienteHom.Value  := AmbSATToInt(ambSAT);
      qryDoctoFiscal.Post;

      frmMenu.mensagemUsuario('');
  except
      frmMenu.mensagemUsuario('');
      frmMenu.MensagemErro('Erro ao enviar os dados de cancelamento ao SAT, tente novamente.');
      Raise;
  end;

  try
      frmMenu.mensagemUsuario('Imprimindo extrato de cancel...');

      prPreparaImpressao;
      frmMenu.ACBrSAT1.ImprimirExtratoCancelamento;
      frmMenu.ACBRPosPrinter1FechaImpTermica;

  except on E : Exception do
      frmMenu.MensagemErro('Erro ao efetuar a impress�o extrato de cancelamento, entre em "Fun��es do SAT" e reimprima o comprovante.'
                          +#13#13
                          +'Mensagem SAT: ' + E.Message);
  end;
end;

procedure TfrmFuncoesSAT.prImpExtratoVenda(nCdDoctoFiscal : Integer);
begin
  prPreparaImpressao;

  PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

  frmMenu.ACBrSAT1.CFe.Clear;
  frmMenu.ACBrSAT1.CFe.SetXMLString(qryDoctoFiscalcXMLNFe.Value);
  frmMenu.ACBrSAT1.ImprimirExtrato;
  frmMenu.ACBRPosPrinter1FechaImpTermica;


end;

procedure TfrmFuncoesSAT.prImpExtratoVendaResumido(nCdDoctoFiscal : Integer);
begin
  prPreparaImpressao;

  PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

  frmMenu.ACBrSAT1.CFe.Clear;
  frmMenu.ACBrSAT1.CFe.SetXMLString(qryDoctoFiscalcXMLNFe.Value);

   frmMenu.ACBrSAT1.ImprimirExtratoResumido;
   frmMenu.ACBRPosPrinter1FechaImpTermica;

end;

procedure TfrmFuncoesSAT.prImpExtratoCancelamento(nCdDoctoFiscal : Integer);
begin
  prPreparaImpressao;

  PosicionaQuery(qryDoctoFiscal, IntToStr(nCdDoctoFiscal));

  frmMenu.ACBrSAT1.CFe.Clear;
  frmMenu.ACBrSAT1.CFeCanc.Clear;
  frmMenu.ACBrSAT1.CFe.SetXMLString(qryDoctoFiscalcXMLNFe.Value);
  frmMenu.ACBrSAT1.CFeCanc.SetXMLString(qryDoctoFiscalcXMLCanc.Value);


   frmMenu.ACBrSAT1.ImprimirExtratoCancelamento;
   frmMenu.ACBRPosPrinter1FechaImpTermica;
end;

procedure TfrmFuncoesSAT.btTesteFimClick(Sender: TObject);
var
 cCodigoCF : String;
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  frmMenu.InputQuery('Teste Fim a Fim','Informe o c�digo do cupom fiscal eletr�nico :',cCodigoCF);

  cCodigoCF := IntToStr(StrToIntDef(Trim(cCodigoCF),0));

  if (cCodigoCF = '0') then
  begin
      MensagemErro('Cupom fiscal eletr�nico No ' + cCodigoCF + ' n�o encontrado.');
      Exit;
  end;

  try
      PosicionaQuery(qryDoctoFiscal, cCodigoCF);

      if (qryDoctoFiscal.IsEmpty) then
      begin
          frmMenu.MensagemErro('Cupom fiscal eletr�nico No ' + cCodigoCF + ' n�o encontrado.');
          Exit;
      end;

      frmMenu.ACBrSAT1.TesteFimAFim(qryDoctoFiscalcXMLNFe.Value);
      
      if (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 9000) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;

      frmMenu.ShowMessage('Teste fim a fim efetuado com sucesso.');
  except;
      frmMenu.MensagemErro('Erro ao efetuar o teste fim a fim.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btAtualizarSATClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  case frmMenu.MessageDlg('Confirma a atualiza��o do equipamento SAT ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.ACBrSAT1.AtualizarSoftwareSAT;

      if  (frmMenu.ACBrSAT1.Resposta.codigoDeRetorno <> 14000) then
      begin
          Raise Exception.Create('Mensagem SAT: ' + StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]));
          Exit;
      end;

      frmMenu.ShowMessage('Equipamento SAT atualizado com sucesso.');
  except
      frmMenu.MensagemErro('Erro ao atualizar o equipamento SAT.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btConfigRedeClick(Sender: TObject);
begin
  inherited;

  if (qryAmbientecFlgSATCompartilhado.Value = 1) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar esta fun��o remota.');
      Abort;
  end;

  frmMenu.ACBrSAT1.ConfigurarInterfaceDeRede();
end;

function TfrmFuncoesSAT.fnStatusSAT() : Boolean;
var
  bStatus          : Boolean;
  iCodRetorno      : Integer;
  cRespostaComando : String;
  iQtdeTentativa   : Integer;
label
  verificaStatusSAT;
begin
  iQtdeTentativa := 0;
  bStatus        := False;

  verificaStatusSAT:

  Inc(iQtdeTentativa);

  prAjustaACBrSAT;
  frmMenu.ACBrSAT1.ConsultarSAT;

  iCodRetorno      := frmMenu.ACBrSAT1.Resposta.codigoDeRetorno;
  cRespostaComando := StringReplace(frmMenu.ACBrSAT1.RespostaComando,'|',' - ',[rfReplaceAll]);

  if (   (iCodRetorno <> 8000)
      and(iCodRetorno <> 8098)
      and(iCodRetorno <> 8099)) then
  begin
      Raise Exception.Create('SAT Erro ' + IntToStr(iCodRetorno) + ' - ' + cRespostaComando);
      Exit;
  end;

  { -- se sat estiver em processamento, efetua nova tentativa -- }
  { -- projetado no m�ximo 3 tentativas                       -- }
  if ((iCodRetorno = 8098) and (iQtdeTentativa <= 3)) then
  begin
      Sleep(1000);
      goto verificaStatusSAT;
  end;

  case (iCodRetorno) of
      8000 : bStatus := True;
      8098 : Raise Exception.Create('SAT Erro 8098 - status em processamento, tente novamente.');
      8099 : Raise Exception.Create('SAT Erro 8099 - erro desconhecido, tente novamente.');
  end;

  Result := bStatus;
end;

procedure TfrmFuncoesSAT.btImpExtratoVendaClick(Sender: TObject);
var
  cChaveCFe : String;
begin
  inherited;

  cChaveCFe := '';

  if (not InputQuery('Reimpress�o Extrato de Venda','Chave CF-e Venda:', cChaveCFe)) then
      Exit;

  if (Trim(cChaveCFe) = '') then
  begin
      frmMenu.MensagemErro('N�mero de chave n�o informado.');
      Abort;
  end;

  qryBuscaCFe.Close;
  PosicionaQuery(qryBuscaCFe, cChaveCFe);

  if (qryBuscaCFe.IsEmpty) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' n�o encontrado.');
      Abort;
  end;

  if (qryBuscaCFenCdStatus.Value <> 1) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' est� cancelada, efetue a reimpress�o do extrato de cancelamento.');
      Abort;
  end;

  try
      prImpExtratoVenda(qryBuscaCFenCdDoctoFiscal.Value);
  except
      frmMenu.MensagemErro('Erro ao efetuar a reimpress�o do extrato de venda.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btImpExtratoVendaResumidoClick(
  Sender: TObject);
var
  cChaveCFe : String;
begin
  inherited;

  cChaveCFe := '';

  if (not InputQuery('Reimpress�o Extrato de Venda Resumido','Chave CF-e Venda:', cChaveCFe)) then
      Exit;

  if (Trim(cChaveCFe) = '') then
  begin
      frmMenu.MensagemErro('N�mero de chave n�o informado.');
      Abort;
  end;

  qryBuscaCFe.Close;
  PosicionaQuery(qryBuscaCFe, cChaveCFe);

  if (qryBuscaCFe.IsEmpty) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' n�o encontrado.');
      Abort;
  end;

  if (qryBuscaCFenCdStatus.Value <> 1) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' est� cancelada, efetue a reimpress�o do extrato de cancelamento.');
      Abort;
  end;

  try
      prImpExtratoVendaResumido(qryBuscaCFenCdDoctoFiscal.Value);
  except
      frmMenu.MensagemErro('Erro ao efetuar a reimpress�o do extrato de venda resumido.');
      Raise;
  end;
end;

procedure TfrmFuncoesSAT.btImpExtratoCancelamentoClick(
  Sender: TObject);
var
  cChaveCFe : String;
begin
  inherited;

  cChaveCFe := '';

  if (not InputQuery('Reimpress�o Extrato de Cancel.','Chave CF-e Canc.:', cChaveCFe)) then
      Exit;

  if (Trim(cChaveCFe) = '') then
  begin
      frmMenu.MensagemErro('N�mero de chave n�o informado.');
      Abort;
  end;

  qryBuscaCFe.Close;
  PosicionaQuery(qryBuscaCFe, cChaveCFe);

  if (qryBuscaCFe.IsEmpty) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' n�o encontrado.');
      Abort;
  end;

  if (qryBuscaCFenCdStatus.Value <> 2) then
  begin
      frmMenu.MensagemAlerta('CF-e No ' + cChaveCFe + ' est� ativo, efetue a reimpress�o do extrato de venda.');
      Abort;
  end;

  try
      prImpExtratoCancelamento(qryBuscaCFenCdDoctoFiscal.Value);
  except
      frmMenu.MensagemErro('Erro ao efetuar a reimpress�o do extrato de cancelamento.');
      Raise;
  end;
end;

function TfrmFuncoesSAT.fnLeXMLCFe(cXMLCFe : WideString) : TER2CFe;
begin
  frmMenu.ACBrSAT1.CFe.Clear;
  frmMenu.ACBrSAT1.CFe.SetXMLString(cXMLCFe);

  Result.iNrDocto  := frmMenu.ACBrSAT1.CFe.ide.nCFe;
  Result.cChaveCFe := frmMenu.ACBrSAT1.CFe.infCFe.ID;
  Result.cXMLCFe   := frmMenu.ACBrSAT1.CFe.AsXMLString;
  Result.cNumSerie := '000' + IntToStr(frmMenu.ACBrSAT1.CFe.ide.nserieSAT);
end;

function TfrmFuncoesSAT.fnGeraReqServidorSAT(tpReqSAT : TER2TipoReqSAT; nCdDoctoFiscal : Integer = 0) : TER2ReqSAT;
var
  iQtdeTentativa : Integer;
  iCodigoReqSAT  : Integer;
  stReqSAT       : TER2StatusReqSAT;
begin
  PosicionaQuery(qryAmbiente, frmMenu.cNomeComputador);

  try
      frmMenu.mensagemUsuario('Enviando requisi��o SAT...');

      qryEnviaReqServidorSAT.Close;
      qryEnviaReqServidorSAT.Parameters.ParamByName('cNmComputador').Value  := frmMenu.cNomeComputador;
      qryEnviaReqServidorSAT.Parameters.ParamByName('cFlgAcao').Value       := TipoReqSATToStr(tpReqSAT);
      qryEnviaReqServidorSAT.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
      qryEnviaReqServidorSAT.Parameters.ParamByName('nCdServidorSAT').Value := qryAmbientenCdServidorSAT.Value;
      qryEnviaReqServidorSAT.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
      qryEnviaReqServidorSAT.Parameters.ParamByName('nCdLoja').Value        := frmMenu.nCdLojaAtiva;
      qryEnviaReqServidorSAT.Open;

      if (qryEnviaReqServidorSAT.IsEmpty) then
          Raise Exception.Create('Erro ao enviar requisi��o para o servidor SAT, tente novamente.')
      else
          iCodigoReqSAT := qryEnviaReqServidorSATnCdTempRegistroSAT.Value;
          
      iQtdeTentativa := 1;
      stReqSAT       := stReqPend;

      while (iQtdeTentativa <= 50) do
      begin
          PosicionaQuery(qryRespReqServidorSAT, IntToStr(iCodigoReqSAT));

          { -- se houve atendimento na requisi��o do caixa, verifica resultado e finaliza verifica��o -- }
          if (not qryRespReqServidorSAT.IsEmpty) then
          begin
              case (qryRespReqServidorSATcFlgOk.Value) of
                  0 : stReqSAT := stReqAtendErro;
                  1 : stReqSAT := stReqAtendOk;
              end;

              Break;
          end;

          frmMenu.mensagemUsuario('Aguard. resposta SAT ' + IntToStr(iQtdeTentativa) + '� tent...');
          Application.ProcessMessages;

          Sleep(400);
          Inc(iQtdeTentativa);
      end;

      case (stReqSAT) of
          //stReqAtendOk   : ShowMessage('Requisi��o ' + IntToStr(iCodigoReqSAT) + ' atendida.');
          stReqAtendErro : Raise Exception.Create('Mensagem Servidor SAT: ' + qryRespReqServidorSATiCodRetornoSAT.AsString + ' - ' + qryRespReqServidorSATcMsgRetornoSAT.Value);
          stReqPend      : Raise Exception.Create('Erro ao enviar requisi��o para o servidor SAT, tente novamente.');
      end;
  finally
      Result.cFlgOk         := qryRespReqServidorSATcFlgOk.Value;
      Result.iCodRetornoSAT := qryRespReqServidorSATiCodRetornoSAT.Value;
      Result.cMsgRetornoSAT := qryRespReqServidorSATcMsgRetornoSAT.Value;
      Result.cXMLCFe        := qryRespReqServidorSATcXMLSAT.Value;
      Result.cPathXml       := fnPathCFe(qryRespReqServidorSATcCaminhoArquivo.Value);
      Result.tpAmbSAT       := IntToAmbSAT(qryRespReqServidorSATcFlgAmbienteHom.Value);
  end;
end;

procedure TfrmFuncoesSAT.FormShow(Sender: TObject);
begin
  inherited;

  { -- desabilita visibilidade dos bot�es devido os mesmos serem chamados de forma autom�tica pela aplica��o -- }
  btLigarSAT.Visible           := False;
  btDesligarSAT.Visible        := False;
  btAtivarSAT.Visible          := False;
  btComunicarCert.Visible      := False;
  btAssociarAssinatura.Visible := False;
  N1.Visible                   := False;
  N5.Visible                   := False;
end;

initialization
    RegisterClass(TfrmFuncoesSAT);

end.
