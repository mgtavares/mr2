unit fFollowUP_Requisicao_NovoFollowUp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB;

type
  TfrmFollowUP_Requisicao_NovoFollowUp = class(TfrmProcesso_Padrao)
    Label2: TLabel;
    Label1: TLabel;
    edtDescResumida: TEdit;
    mskDtProximaAcao: TMaskEdit;
    mskDtAtendInicial: TMaskEdit;
    Label4: TLabel;
    Label3: TLabel;
    memoOcorrencia: TMemo;
    mskDtAtendFinal: TMaskEdit;
    Label5: TLabel;
    qryInsereFollow: TADOQuery;
    qryAlteraPrevisaoRequis: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdRequisicao : Integer;
  end;

var
  frmFollowUP_Requisicao_NovoFollowUp: TfrmFollowUP_Requisicao_NovoFollowUp;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmFollowUP_Requisicao_NovoFollowUp.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtDescResumida.Text) = '') then
  begin
      ShowMessage('Informe a descri��o resumida.');
      edtDescResumida.SetFocus;
      Abort;
  end;

  if (Trim(mskDtProximaAcao.Text) <> '/  /') then
  begin
      if (frmMenu.ConvData(mskDtProximaAcao.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtProximaAcao.SetFocus;
          Abort;
      end;
  end;

  if ((Trim(mskDtAtendInicial.Text) <> '/  /') and (Trim(mskDtAtendFinal.Text) = '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de atendimento, informe a data final.');
      mskDtAtendFinal.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtAtendInicial.Text) = '/  /') and (Trim(mskDtAtendFinal.Text) <> '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de atendimento, informe a data inicial.') ;
      mskDtAtendInicial.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtAtendInicial.Text) <> '/  /') and (Trim(mskDtAtendFinal.Text) <> '/  /'))then
  begin
      { -- valida datas -- }
      if (frmMenu.ConvData(mskDtAtendInicial.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtAtendInicial.SetFocus;
          Abort;
      end;

      if (frmMenu.ConvData(mskDtAtendFinal.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtAtendFinal.SetFocus;
          Abort;
      end;

      if (not frmMenu.fnValidaPeriodo(frmMenu.ConvData(mskDtAtendInicial.Text),frmMenu.ConvData(mskDtAtendFinal.Text))) then
      begin
          mskDtAtendInicial.SetFocus;
          Abort;
      end;

      { -- atualiza previs�o de atendimento -- }
      try
          frmMenu.Connection.BeginTrans;

          qryAlteraPrevisaoRequis.Close;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('nCdRequisicao').Value   := nCdRequisicao;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('dDtPrevAtendIni').Value := mskDtAtendInicial.Text;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('dDtPrevAtendFim').Value := mskDtAtendFinal.Text;
          qryAlteraPrevisaoRequis.ExecSQL;

          frmMenu.Connection.CommitTrans;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento');
          Raise;
          Exit;
      end;
  end;

  if (MessageDlg('Confirma a gera��o desta ocorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      Exit;

  try
      frmMenu.Connection.BeginTrans;

      { -- gera registro follow up -- }
      qryInsereFollow.Close;
      qryInsereFollow.Parameters.ParamByName('nCdRequisicao').Value    := nCdRequisicao;
      qryInsereFollow.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      qryInsereFollow.Parameters.ParamByName('cOcorrenciaResum').Value := edtDescResumida.Text;
      qryInsereFollow.Parameters.ParamByName('cOcorrencia').Value      := AnsiUpperCase(memoOcorrencia.Text);

      if (Trim(mskDtProximaAcao.Text) = '/  /') then
          qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value := Null
      else
          qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value := mskDtProximaAcao.Text;

      qryInsereFollow.ExecSQL;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Ocorr�ncia registrada com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento');
      Raise;
  end;

  Close;
end;

end.
