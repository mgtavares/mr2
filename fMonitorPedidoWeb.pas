unit fMonitorPedidoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, ADODB, Mask, ER2Lookup, DBCtrls, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, Menus, ER2Excel;

type
  TfrmMonitorPedidoWeb = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryPedidoWeb: TADOQuery;
    Label3: TLabel;
    mskDtPedidoInicial: TMaskEdit;
    Label2: TLabel;
    mskDtPedidoFinal: TMaskEdit;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    Label1: TLabel;
    Label4: TLabel;
    rgbStatusPed: TRadioGroup;
    ER2LookupMaskEdit3: TER2LookupMaskEdit;
    Label5: TLabel;
    qryLoja: TADOQuery;
    qryTerceiro: TADOQuery;
    qryStatusPedido: TADOQuery;
    dsLoja: TDataSource;
    dsStatusPedido: TDataSource;
    dsTerceiro: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsPedidoWeb: TDataSource;
    qryStatusPedidonCdTabStatusPed: TIntegerField;
    qryStatusPedidocNmTabStatusPed: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    cxGrid: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1cCdPedidoWeb: TcxGridDBColumn;
    cxGridDBTableView1dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1nValPedidoWeb: TcxGridDBColumn;
    cxGridDBTableView1cCdCliente: TcxGridDBColumn;
    cxGridDBTableView1cNmCliente: TcxGridDBColumn;
    cxGridDBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView1cNmTabStatusPed: TcxGridDBColumn;
    cxGridDBTableView1cCdTabStatusPed: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1cStatusIntegra: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    btImpPedido: TToolButton;
    ToolButton5: TToolButton;
    qryPedidoWebcCdPedidoWeb: TStringField;
    qryPedidoWebdDtPedido: TDateTimeField;
    qryPedidoWebnValPedidoWeb: TBCDField;
    qryPedidoWebcNmFormaPagto: TStringField;
    qryPedidoWebcCdCliente: TStringField;
    qryPedidoWebcNmCliente: TStringField;
    qryPedidoWebcCNPJCPF: TStringField;
    qryPedidoWebcNmTabStatusPed: TStringField;
    qryPedidoWebcStatusIntegra: TStringField;
    qryPedidoWebcCdTabStatusPed: TStringField;
    qryPedidoWebnCdPedido: TIntegerField;
    Label6: TLabel;
    PopupMenu: TPopupMenu;
    btVisualizarPedidoWeb: TMenuItem;
    ToolButton4: TToolButton;
    btExportaExcel: TToolButton;
    ER2Excel: TER2Excel;
    procedure btImpPedidoClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryLojaBeforeOpen(DataSet: TDataSet);
    procedure btVisualizarPedidoWebClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure btExportaExcelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorPedidoWeb: TfrmMonitorPedidoWeb;

implementation

uses
    fMenu, rPedidoVenda, fPedidoVendaWeb, Math;

{$R *.dfm}

procedure TfrmMonitorPedidoWeb.btImpPedidoClick(Sender: TObject);
var
  objRel : TrptPedidoVenda;
begin
  if (qryPedidoWeb.IsEmpty) then
      Exit;

  if (qryPedidoWebnCdPedido.IsNull) then
  begin
      MensagemAlerta('Pedido n�o pode ser impresso devido ainda n�o estar importado no sistema.');
      Abort;
  end;

  try
      try
          objRel := TrptPedidoVenda.Create(nil);

          PosicionaQuery(objRel.qryPedido,qryPedidoWebnCdPedido.AsString);
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryPedidoWebnCdPedido.AsString);

          objRel.QRSubDetail1.Enabled := True;
          objRel.QRSubDetail2.Enabled := False;
          objRel.QRSubDetail3.Enabled := False;

         if (objRel.qryItemEstoque_Grade.eof) then
             objRel.QRSubDetail1.Enabled := False;

         objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

         objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmMonitorPedidoWeb.ToolButton1Click(Sender: TObject);
var
  nCdLoja, nCdTabStatusPed : integer;
  cCNPJCPF                 : string;
begin
  inherited;

  if not (qryLoja.IsEmpty) then
      nCdLoja := qryLojanCdLoja.Value
  else
      nCdLoja := 0;

  if not (qryTerceiro.IsEmpty) then
      cCNPJCPF := qryTerceirocCNPJCPF.Value
  else
      cCNPJCPF := '';

  if not (qryStatusPedido.IsEmpty) then
      nCdTabStatusPed := qryStatusPedidonCdTabStatusPed.Value
  else
      nCdTabStatusPed := 0;

  { -- valida per�odo -- }
  if ((Trim(mskDtPedidoInicial.Text) <> '/  /') and (Trim(mskDtPedidoFinal.Text) = '/  /')) then
  begin
      MensagemAlerta('Informe o per�odo final.');
      mskDtPedidoFinal.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtPedidoInicial.Text) = '/  /') and (Trim(mskDtPedidoFinal.Text) <> '/  /')) then
  begin
      MensagemAlerta('Informe o per�odo inicial.');
      mskDtPedidoInicial.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtPedidoInicial.Text) = '/  /') and (Trim(mskDtPedidoFinal.Text) = '/  /')) then
  begin
      mskDtPedidoInicial.Text := DateToStr(Date-30);
      mskDtPedidoFinal.Text   := DateToStr(Date);
  end;

  if (not frmMenu.fnValidaPeriodo(frmMenu.ConvData(mskDtPedidoInicial.Text), frmMenu.ConvData(mskDtPedidoFinal.Text))) then
  begin
      mskDtPedidoInicial.SetFocus;
      Abort;
  end;

  if (frmMenu.ConvData(mskDtPedidoFinal.Text) > Date) then
  begin
      MensagemAlerta('A data final n�o pode ser maior do que a data atual.');
      mskDtPedidoFinal.SetFocus;
      Abort;
  end;

  qryPedidoWeb.Close;
  qryPedidoWeb.Parameters.ParamByName('nCdLoja').Value           := nCdLoja;
  qryPedidoWeb.Parameters.ParamByName('cCNPJCPF').Value          := cCNPJCPF;
  qryPedidoWeb.Parameters.ParamByName('nCdTabStatusPed').Value   := nCdTabStatusPed;
  qryPedidoWeb.Parameters.ParamByName('cDtInicial').Value        := frmMenu.ConvData(mskDtPedidoInicial.Text);
  qryPedidoWeb.Parameters.ParamByName('cDtFinal').Value          := frmMenu.ConvData(mskDtPedidoFinal.Text);
  qryPedidoWeb.Parameters.ParamByName('iStatusIntegracao').Value := rgbStatusPed.ItemIndex;
  qryPedidoWeb.Open;

  if (qryPedidoWeb.IsEmpty) then
      ShowMessage('Nenhuma informa��o encontrada para o crit�rio selecionado.');
end;

procedure TfrmMonitorPedidoWeb.qryLojaBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmMonitorPedidoWeb.btVisualizarPedidoWebClick(Sender: TObject);
var
  objFormPedido : TfrmPedidoVendaWeb;
begin
  if (qryPedidoWeb.IsEmpty) then
      Exit;

  if (qryPedidoWebnCdPedido.IsNull) then
  begin
      MensagemAlerta('Pedido n�o pode ser visualizado devido ainda n�o estar importado no sistema.');
      Abort;
  end;

  if (not frmMenu.fnValidaUsuarioAPL('FRMPEDIDOVENDAWEB')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Exit;
  end;

  objFormPedido := TfrmPedidoVendaWeb.Create(nil);

  objFormPedido.PosicionaQuery(objFormPedido.qryMaster, qryPedidoWebnCdPedido.AsString);
  showForm(objFormPedido, True);
end;

procedure TfrmMonitorPedidoWeb.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;

  btVisualizarPedidoWeb.Click;
end;

procedure TfrmMonitorPedidoWeb.btExportaExcelClick(Sender: TObject);
var
  iLinha  : integer;
  cFiltro : String;
begin
  inherited;

  if (qryPedidoWeb.IsEmpty) then
      Exit;

  cFiltro := '';

  if not (qryLoja.IsEmpty) then
      cFiltro := 'Loja: ' + qryLojacNmLoja.Value + ' / ';

  if not (qryTerceiro.IsEmpty) then
      cFiltro := cFiltro + 'Cliente : ' + qryTerceirocNmTerceiro.Value + ' / ';

  if not (qryStatusPedido.IsEmpty) then
      cFiltro := cFiltro + 'Status Ped.: ' + qryStatusPedidocNmTabStatusPed.Value + ' / ';

  cFiltro := cFiltro + 'Status Integra��o: ' + rgbStatusPed.Items[rgbStatusPed.ItemIndex] + ' / ';

  cFiltro := cFiltro + 'Per�odo: ' + qryPedidoWeb.Parameters.ParamByName('cDtInicial').Value + ' at� ' + qryPedidoWeb.Parameters.ParamByName('cDtFinal').Value;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  ER2Excel.Celula['A1'].Background := RGB(221,221,221);
  ER2Excel.Celula['A1'].Range('J6');
  ER2Excel.Celula['A1'].Congelar('K7');

  ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel.Celula['A2'].Text := 'Monitor de Pedidos Web';
  ER2Excel.Celula['A4'].Text := cFiltro;

  ER2Excel.Celula['A6'].Text := 'C�d. Pedido WEB';
  ER2Excel.Celula['B6'].Text := 'C�d. Pedido';
  ER2Excel.Celula['C6'].Text := 'Dt. Pedido';
  ER2Excel.Celula['D6'].Text := 'Valor';
  ER2Excel.Celula['E6'].Text := 'Forma de Pagamento';
  ER2Excel.Celula['F6'].Text := 'C�d. Cliente';
  ER2Excel.Celula['G6'].Text := 'Nome Cliente';
  ER2Excel.Celula['H6'].Text := 'CNPJ/CPF';
  ER2Excel.Celula['I6'].Text := 'Status Pedido';
  ER2Excel.Celula['J6'].Text := 'Status Integra��o';

  ER2Excel.Celula['A1'].Negrito;
  ER2Excel.Celula['A2'].Negrito;
  ER2Excel.Celula['A4'].Negrito;
  ER2Excel.Celula['A6'].Negrito;
  ER2Excel.Celula['B6'].Negrito;
  ER2Excel.Celula['C6'].Negrito;
  ER2Excel.Celula['D6'].Negrito;
  ER2Excel.Celula['E6'].Negrito;
  ER2Excel.Celula['F6'].Negrito;
  ER2Excel.Celula['G6'].Negrito;
  ER2Excel.Celula['H6'].Negrito;
  ER2Excel.Celula['I6'].Negrito;
  ER2Excel.Celula['J6'].Negrito;

  iLinha := 7;

  qryPedidoWeb.First;

  while not (qryPedidoWeb.Eof) do
  begin
      ER2Excel.Celula['A' + IntToStr(iLinha)].Text := qryPedidoWebcCdPedidoWeb.Value;
      ER2Excel.Celula['B' + IntToStr(iLinha)].Text := qryPedidoWebnCdPedido.AsString;
      ER2Excel.Celula['C' + IntToStr(iLinha)].Text := qryPedidoWebdDtPedido.Value;

      ER2Excel.Celula['D' + IntToStr(iLinha)].Mascara := '0,00';
      ER2Excel.Celula['D' + IntToStr(iLinha)].Text    := qryPedidoWebnValPedidoWeb.Value;

      ER2Excel.Celula['E' + IntToStr(iLinha)].Text := qryPedidoWebcNmFormaPagto.Value;
      ER2Excel.Celula['F' + IntToStr(iLinha)].Text := qryPedidoWebcCdCliente.Value;
      ER2Excel.Celula['G' + IntToStr(iLinha)].Text := qryPedidoWebcNmCliente.Value;
      ER2Excel.Celula['H' + IntToStr(iLinha)].Text := qryPedidoWebcCNPJCPF.Value;
      ER2Excel.Celula['I' + IntToStr(iLinha)].Text := qryPedidoWebcCdTabStatusPed.Value + ' - ' + qryPedidoWebcNmTabStatusPed.Value;
      ER2Excel.Celula['J' + IntToStr(iLinha)].Text := qryPedidoWebcStatusIntegra.Value;

      ER2Excel.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haRight;

      Inc(iLinha);

      qryPedidoWeb.Next;
  end;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel.ExportXLS;
  ER2Excel.CleanupInstance;

  frmMenu.mensagemUsuario('');
end;

initialization
    RegisterClass(TfrmMonitorPedidoWeb);

end.
