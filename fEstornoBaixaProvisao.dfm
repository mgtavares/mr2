inherited frmEstornoBaixaProvisao: TfrmEstornoBaixaProvisao
  Left = -8
  Top = -8
  Width = 1152
  Height = 786
  Caption = 'Estorno de Baixa de Provis'#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1280
    Height = 721
  end
  inherited ToolBar1: TToolBar
    Width = 1280
    ButtonWidth = 91
    inherited ToolButton1: TToolButton
      Caption = 'Estornar Baixa'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 91
    end
    inherited ToolButton2: TToolButton
      Left = 99
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1280
    Height = 721
    DataGrouping.GroupLevels = <>
    DataSource = dsProvisaoTit
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProvisaoTit'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'dDtBaixa'
        Footers = <>
        Width = 133
      end
      item
        EditButtons = <>
        FieldName = 'dDtPagto'
        Footers = <>
        Width = 105
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuarioBaixa'
        Footers = <>
        Width = 188
      end
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footers = <>
        Width = 169
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValProvisao'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 29
      end
      item
        EditButtons = <>
        FieldName = 'cAgencia'
        Footers = <>
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'cNmTitular'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryProvisaoTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdUsuario int'
      '       ,@nCdEmpresa int'
      ''
      'Set @nCdUsuario = :nCdUsuario'
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'SELECT nCdProvisaoTit'
      '      ,dDtBaixa'
      '      ,cNmUsuario cNmUsuarioBaixa'
      '      ,ProvisaoTit.nCdFormaPagto'
      '      ,cNmFormaPagto'
      
        '      ,(SELECT iNrCheque FROM Cheque WHERE Cheque.nCdProvisaoTit' +
        ' = ProvisaoTit.nCdProvisaoTit) as iNrCheque'
      '      ,nValProvisao'
      '      ,ProvisaoTit.nCdContaBancaria'
      '      ,ContaBancaria.nCdBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.cNmTitular'
      '      ,dDtPagto'
      '  FROM ProvisaoTit'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = ProvisaoTit.nC' +
        'dUsuarioBaixa'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = ProvisaoTit.nCdContaBancaria'
      
        '       INNER JOIN FormaPagto    ON FormaPagto.nCdFormaPagto     ' +
        '  = ProvisaoTit.nCdFormaPagto'
      ' WHERE cFlgBaixado = 1'
      '   AND ProvisaoTit.nCdEmpresa  = @nCdEmpresa'
      '   AND ProvisaoTit.dDtBaixa   >= dbo.fn_OnlyDate(GetDate())-90'
      '   AND (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))'
      ' ORDER BY dDtBaixa DESC')
    Left = 184
    Top = 184
    object qryProvisaoTitnCdProvisaoTit: TAutoIncField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|C'#243'd'
      FieldName = 'nCdProvisaoTit'
      ReadOnly = True
    end
    object qryProvisaoTitdDtBaixa: TDateTimeField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Data Baixa'
      FieldName = 'dDtBaixa'
    end
    object qryProvisaoTitcNmUsuarioBaixa: TStringField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Usu'#225'rio Respons'#225'vel'
      FieldName = 'cNmUsuarioBaixa'
      Size = 50
    end
    object qryProvisaoTitnCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryProvisaoTitcNmFormaPagto: TStringField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Forma de Pagamento'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryProvisaoTitiNrCheque: TIntegerField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Nr. Cheque'
      FieldName = 'iNrCheque'
      ReadOnly = True
    end
    object qryProvisaoTitnValProvisao: TBCDField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Valor Total'
      FieldName = 'nValProvisao'
      Precision = 12
      Size = 2
    end
    object qryProvisaoTitnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryProvisaoTitnCdBanco: TIntegerField
      DisplayLabel = 'Conta D'#233'bito|Bco'
      FieldName = 'nCdBanco'
    end
    object qryProvisaoTitcAgencia: TIntegerField
      DisplayLabel = 'Conta D'#233'bito|Ag.'
      FieldName = 'cAgencia'
    end
    object qryProvisaoTitnCdConta: TStringField
      DisplayLabel = 'Conta D'#233'bito|Nr. Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryProvisaoTitcNmTitular: TStringField
      DisplayLabel = 'Conta D'#233'bito|Titular'
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryProvisaoTitdDtPagto: TDateTimeField
      DisplayLabel = 'Provis'#245'es Baixadas '#218'ltimos 90 dias|Data de Pagamento'
      FieldName = 'dDtPagto'
    end
  end
  object dsProvisaoTit: TDataSource
    DataSet = qryProvisaoTit
    Left = 224
    Top = 184
  end
  object SP_ESTORNA_BAIXA_PROVISAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ESTORNA_BAIXA_PROVISAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProvisaoTit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 248
    Top = 248
  end
end
