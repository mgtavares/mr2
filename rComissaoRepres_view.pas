unit rComissaoRepres_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptComissaoRepres_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    qryResultado: TADOQuery;
    qryResultadocNmTerceiro: TStringField;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadonCdLanctoComissao: TAutoIncField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadoiAnoMes: TIntegerField;
    qryResultadonCdPedido: TIntegerField;
    qryResultadocDoctoFiscal: TStringField;
    qryResultadonCdRegraGrupoComissao: TIntegerField;
    qryResultadonValAcumulado: TBCDField;
    qryResultadonValVenda: TBCDField;
    qryResultadonValBaseCalculo: TBCDField;
    qryResultadonValComissao: TBCDField;
    qryResultadonCdSP: TIntegerField;
    qryResultadocComissaoPaga: TStringField;
    qryResultadocFlgComissaoPaga: TIntegerField;
    qryResultadonCdGrupoProduto: TIntegerField;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmProduto: TStringField;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRShape6: TQRShape;
    QRLabel22: TQRLabel;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRLabel23: TQRLabel;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRLabel24: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRLabel25: TQRLabel;
    qryResultadocNmTerceiro_1: TStringField;
    QRLabel26: TQRLabel;
    QRDBText16: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptComissaoRepres_view: TrptComissaoRepres_view;

implementation

{$R *.dfm}

end.
