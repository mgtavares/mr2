USE ER2SOFTDEV;


UPDATE VersaoSistema
   SET dDtFimVersao = GETDATE()
 WHERE dDtFimVersao IS NULL
    
INSERT INTO VersaoSistema(cNmVersaoSistema
                         ,dDtInicioVersao)
                   VALUES('v3.41'
                         ,GETDATE());

ALTER TABLE Cobradora ADD cFlgPermiteLiqLoja INTEGER;

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_CalcRetornoCobradora02](@nCdCobradora     int
                                                 ,@nCdLojaCobradora int
                                                 ,@nCdTitulo        int)
RETURNS DECIMAL(12,2)
AS
BEGIN
    DECLARE @nSaldoTit DECIMAL(12,2)

    SELECT @nSaldoTit = nSaldoTit
      FROM Titulo
     WHERE nCdTitulo  = @nCdTitulo

    Set @nSaldoTit = IsNull(@nSaldoTit,0)

    RETURN (@nSaldoTit * 0.00)

END



-- AND ( Titulo.cFlgCobradora = 0 OR EXISTS( SELECT TOP 1 nCdCobradora FROM Cobradora WHERE nCdCobradora = Titulo.nCdCobradora AND cFlgPermiteLiqLoja = 1 ) )



