USE [ER2SOFTDEV]
GO

/****** Object:  StoredProcedure [dbo].[SP_FINALIZA_RECEB_CAIXA]    Script Date: 26/06/2019 Implementacao TEF  Cappta ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SP_FINALIZA_RECEB_CAIXA] (@nCdUsuario          int
                                         ,@nCdResumoCaixa      int
                                         ,@nSaldo              decimal(12,2)
                                         ,@nCdDoctoFiscal      int
                                         ,@nCdLanctoFin_Out    int           OUTPUT
                                         ,@cFlgValePresente    char(1)       OUTPUT
                                         ,@cFlgValeMercadoria  char(1)       OUTPUT
                                         ,@dDtValidadeValeMerc int           OUTPUT
                                         ,@cFlgValeDesconto    char(1)       OUTPUT
                                         ,@nCdVale_Out         int           OUTPUT
                                         ,@nCdValeDesconto_Out int           OUTPUT)
                                         
AS

    DECLARE @nCdPedido                     int
           ,@nCdItemPedido                 int
           ,@nCdProduto                    int
           ,@nCdTipoItemPed                int
           ,@nQtdePed                      decimal(12,4)
           ,@nValTotalItem                 decimal(12,2)
           ,@nValTotalCheque               decimal(12,2)
           ,@nValCrediario                 decimal(12,2)
           ,@nValEntrada                   decimal(12,2)
           ,@cFlgEntrada                   int
           ,@nCdTerceiro                   int
           ,@nCdTerceiroTit                int
           ,@nCdLojaOperadoraCartao        int
           ,@cFlgConsignacao               int
           ,@nCdConsignacao                int
           ,@cFlgMovEstoqueConsignado      int

    DECLARE @nCdOperacaoEstoqueDev         int
           ,@nCdEmpresa                    int
           ,@nCdOperacaoEstoque            int
           ,@nCdLocalEstoque               int
           ,@nCdLoja                       int
           ,@nCdGrupoProduto               int
           ,@nCdTipoPedido                 int
           ,@nCdContaBancaria              int
           ,@nCdFormaPagtoDin              int
           ,@nCdLanctoFin                  int
           ,@nCdFormaPagto                 int
           ,@cNmGrupoProduto               varchar(50)
           ,@nCdEspTitCartao               int

    DECLARE @nCdBanco                      int
		   ,@nCdAgencia                    CHAR(4)
		   ,@nCdConta                      CHAR(15)
		   ,@cDigito                       CHAR(1)
		   ,@iNrCheque                     int
		   ,@nValCheque                    DECIMAL(12,2)
		   ,@cCNPJCPF                      CHAR(14)
		   ,@dDtDeposito                   DATETIME
		   ,@dDtLimite                     DATETIME
		   ,@iAutorizacao                  int
           ,@cChave                        VARCHAR(30)
    
    DECLARE @dDtVencto                     DATETIME
           ,@nValParcela                   DECIMAL(12,2)

    DECLARE @nCdTitulo                     int
           ,@iParcela                      int
           ,@nCdEspTit                     int
           ,@cNrTit                        varchar(10)
           ,@nCdCategFinanc                int
           ,@nCdUnidadeNegocio             int
           ,@nCdEspTitCred                 int
           ,@nCdEspTitCredReneg            int
           ,@nCdCrediario                  int
           ,@nCdCrediarioReneg             int
           ,@iFloating                     int
           ,@cFlgDiaUtil                   int
           ,@dDtCredito                    datetime
           ,@nCdTransacaoCartao            int
           ,@nPercent                      decimal(12,2)
           ,@nValAux                       decimal(12,2)
           ,@nValRecCrediario              decimal(12,2)
           ,@nValJuro                      decimal(12,2)
           ,@nValDesconto                  decimal(12,2)
           ,@nValTotalPagto                decimal(12,2)
           ,@nValDinheiro                  decimal(12,2)

    DECLARE @nCdEspTitDin                  int
           ,@nCdCategFinancDin             int
           ,@nCdCategFinancAcr             int
           ,@nCdOperJuroCred               int
           ,@nCdOperDescCred               int
           ,@nCdOperDescJuroCred           int
           ,@nCdValePed                    int
           ,@nCdTipoLancto                 int
           ,@nCdCrediarioNovo              int
           ,@cAux                          varchar(150)
           ,@nCdTemp_Pagto                 int
           ,@nCdTabTipoFormaPagto          int
           ,@nCdCondPagto                  int
           ,@nValPagto                     decimal(12,2)
           ,@cFlgTipo                      int
           ,@iNrCartao                     bigint
           ,@iNrDocto                      int
           ,@iNrAutorizacao                int
		   ,@cNumeroControle			   varchar(50)
		   ,@cNrDoctoNSU                   varchar(50)
		   ,@cNrAutorizacao                varchar(12)
           ,@nCdOperadoraCartao            int
           ,@nValAcrescimo                 decimal(12,2)
           ,@nCdOperJuroFinCred            int
           ,@nDif                          decimal(12,2)
           ,@cFlgTemPedido                 int
           ,@cOBSEstoque                   varchar(15)
           ,@nTaxaOperacaoCartao           decimal(12,4)
           ,@iTotalParcelas                int
           ,@nCdOperTaxaCartao             int
           ,@nValTaxaOperacaoCartao        decimal(12,2)
           ,@nValTaxaOperacaoAcumulado     decimal(12,2)
           ,@nValTaxaOperacaoCartaoParcela decimal(12,2)
           ,@nCdTitParcela                 int
           ,@cNmOperadoraCartao            varchar(50)
           ,@nCdGrupoOperadoraCartao       int
           ,@cFlgVale                      int
           ,@cFlgValeLiqDebito             int
           ,@cFlgTeveDefeito               int
           ,@cFlgDefeito                   int
           ,@nSaldoVale                    decimal(12,2)
           ,@cNmEspTit                     varchar(50)
           ,@nCdCheque                     int
           ,@nSaldoTitLiq                  decimal(12,2)
           ,@nCdTerceiroPagto              int
           ,@nCdCondPagtoMaior             int
           ,@nValDescontoTotal             decimal(12,2)
           ,@nValAcrescimoTotal            decimal(12,2)
           ,@nValPedidoTotal               decimal(12,2)
           ,@nSaldoValeAbater              decimal(12,2)
           ,@nValorAbatimento              decimal(12,2)
           ,@nCdOperacaoAbatimento         int
           ,@cUsaCartaoPresente            varchar(1)
           ,@nCdTerceiroPDV                int
           ,@nCdTabTipoVale                int
           ,@nCdCCReceita                  int

    DECLARE @nCdTipoLancVDCx               int
           ,@nCdTipoLancRPCx               int
           ,@nCdTipoLancRCCx               int
           ,@iQtdeRecCrediario             int
           ,@iQtdeRecChequeResgate         int
           ,@iQtdeRecPedido                int
           ,@nCdCampanhaPromoc             int
           ,@nValCMV                       decimal(12,2)
           ,@nValUnitario                  decimal(12,2)
           ,@nCdItemPedidoTroca            int
           ,@nValJurosDescontado           decimal(12,2)
           ,@iDiasCarencia                 int
           ,@nPercMulta                    decimal(12,4)
           ,@nPercJuroDia                  decimal(12,4)
           ,@nCdPropostaReneg              int
           ,@iQtdeParcReneg                int            
           ,@nValParcReneg                 decimal(12,2)  
           ,@nValEntradaReneg              decimal(12,2)
           ,@iParcCorrente                 int
           ,@nCdFormaPagtoRen              int
           ,@nValRenegTotal                decimal(12,2)
           ,@iParcelas                     int
           ,@nCdOperAbatReneg              int
           ,@nSaldoDescJuros               decimal(12,2)
           ,@nSaldoDescPrincipal           decimal(12,2)
           ,@nValJurosTotal                decimal(12,2)
           ,@nValOriginalTotal             decimal(12,2)
           ,@dDtPropostaReneg              datetime
           ,@nPercDescPrincipal            decimal(12,2)
           ,@nPercDescJuros                decimal(12,2)
           ,@iQtdeTitulos                  int
           ,@iQtdeTitulosProc              int
           ,@nValDescontoJuroTit           decimal(12,2)
           ,@nValDescontoPrincTit          decimal(12,2)
           ,@nCdTipoLancReneg              int
           ,@cFlgPrimeiraCompra            int
           ,@nCdOperacaoEstoqueDefeito     int
           ,@nValTrocaTotal                decimal(12,2)
           ,@nValValeMercTotal             decimal(12,2)
           ,@nCdTabStatusPed               int
           ,@nCdLanctoFinPed               int
           ,@cDadosBaixa                   VARCHAR(100)
           ,@nValTitReneg                  int

    DECLARE @nValDescontoCondPagto         decimal(12,2)
           ,@nCdFormaPagtoLanctoFin        int
           ,@nValJurosCondPagto            decimal(12,2)
           ,@nValDescontoManual            decimal(12,2)
           ,@nPercAcrescimo                decimal(12,4)

    DECLARE @nCdItemPedidoAtendido    int
           ,@nCdCategFinancTitulo     int
           ,@nCdCategFinancTituloAcr  int
           ,@nCdMTitulo               int
           ,@nCdTabTipoRestricaoVenda int
           ,@nCdUsuarioAutor          int
           ,@nCdRestricaoVenda        int
           ,@dDtAutor                 datetime
           ,@cJustificativa           varchar(1000)
           ,@nCdVale                  int
           ,@nCdHistStatusCheque      int
           ,@cCodigoValePresente      varchar(30)
           ,@nCdTerceiroVale          int
           ,@nValValePresente         decimal(12,2)
           ,@nValPedidoAnterior       decimal(12,2)
           ,@nValPedidoAtual          decimal(12,2)
           ,@nValValeDesconto         decimal(12,2)
           ,@nCdValeDesconto          int
           
    DECLARE @cFlgUtilizaSaldoVP varchar(1)
           ,@cCodigoVP          varchar(30)
           ,@nValVP             decimal(12,2)
           ,@iTipoVale          int
           
    DECLARE @cModoEmpenhoCD char(1)
    
    DECLARE @cFlgUsaMetaDesc         varchar(1)
           ,@nCdMetaDescontoUtil     int
           ,@nCdMetaVendedorDesconto int
           ,@nValDescontoAnt         decimal(12,2)
           ,@nCdPedidoMetaDesc       int
           ,@nCdLojaMetaDesc         int     
           ,@nCdTerceiroMetaDesc     int
           
BEGIN

    Set @cFlgValePresente    = 'N'
    Set @cFlgValeMercadoria  = 'N'
    Set @cFlgValeDesconto    = 'N'
    Set @dDtValidadeValeMerc = NULL
    Set @cFlgValeLiqDebito   = 0
    Set @nSaldoValeAbater    = 0
    Set @nCdVale_Out         = 0
    Set @nCdValeDesconto_Out = 0

    IF (@nCdDoctoFiscal = 0) Set @nCdDoctoFiscal = NULL

    --
    -- Valida os parametros
    --
    Set @nCdTipoLancReneg = NULL
    
    SELECT @nCdTipoLancReneg = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'TIPOLANCRENEG'

    IF (@nCdTipoLancReneg IS NULL)
    BEGIN
        RAISERROR('Parametro TIPOLANCRENEG n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperAbatReneg = NULL
    
    SELECT @nCdOperAbatReneg = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CDOPERABTTITREN'

    IF (@nCdOperAbatReneg IS NULL)
    BEGIN
        RAISERROR('Parametro CDOPERABTTITREN n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdTipoLancVDCx = NULL
    
    SELECT @nCdTipoLancVDCx = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'TIPOLANCVDCX'

    IF (@nCdTipoLancVDCx IS NULL)
    BEGIN
        RAISERROR('Parametro TIPOLANCVDCX n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdFormaPagtoRen = NULL
    
    SELECT @nCdFormaPagtoRen = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'FORMAPAGTOREN'

    IF (@nCdFormaPagtoRen IS NULL)
    BEGIN
        RAISERROR('Parametro FORMAPAGTOREN n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdTipoLancRPCx = NULL
    
    SELECT @nCdTipoLancRPCx = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'TIPOLANCRPCX'

    IF (@nCdTipoLancRPCx IS NULL)
    BEGIN
        RAISERROR('Parametro TIPOLANCRPCX n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdTipoLancRCCx = NULL
    
    SELECT @nCdTipoLancRCCx = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'TIPOLANCRCCX'

    IF (@nCdTipoLancRCCx IS NULL)
    BEGIN
        RAISERROR('Parametro TIPOLANCRCCX n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdTerceiroPDV = NULL
    
    SELECT @nCdTerceiroPDV = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'TERCPDV'

    IF (@nCdTerceiroPDV IS NULL)
    BEGIN
        RAISERROR('Parametro TERCPDV n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @cUsaCartaoPresente = NULL
    
    SELECT @cUsaCartaoPresente = cValor
      FROM Parametro
      WHERE cParametro = 'PEDIRNUMVPCX'

    IF (@cUsaCartaoPresente IS NULL)
    BEGIN
        RAISERROR('Parametro PEDIRNUMVPCX n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperacaoAbatimento = NULL
    
    SELECT @nCdOperacaoAbatimento = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPFINABATCRED'

    IF (@nCdOperacaoAbatimento IS NULL)
    BEGIN
        RAISERROR('Parametro OPFINABATCRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdEspTitDin = NULL
    
    SELECT @nCdEspTitDin = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'ET-PDV'

    IF (@nCdEspTitDin IS NULL)
    BEGIN
        RAISERROR('Parametro ET-PDV n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperTaxaCartao = NULL
    
    SELECT @nCdOperTaxaCartao = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERTAXAOPER'

    IF (@nCdOperTaxaCartao IS NULL)
    BEGIN
        RAISERROR('Parametro OPERTAXAOPER n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdCategFinancDin = NULL
    
    SELECT @nCdCategFinancDin = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CATEGPAD-PDV'

    IF (@nCdCategFinancDin IS NULL)
    BEGIN
        RAISERROR('Parametro CATEGPAD-PDV n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END
    
    SELECT @nCdCategFinancAcr = CAST(ISNULL(cValor,0) as int)
      FROM Parametro
     WHERE cParametro = 'CATEGPAD-ACR'
    
    Set @nCdOperacaoEstoqueDev = NULL

    SELECT @nCdOperacaoEstoqueDev = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERESTDEVVDA'

    IF (@nCdOperacaoEstoqueDev IS NULL)
    BEGIN
        RAISERROR('Parametro OPERESTDEVVDA n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperacaoEstoqueDefeito = NULL

    SELECT @nCdOperacaoEstoqueDefeito = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERESTDEVDEF'

    IF (@nCdOperacaoEstoqueDefeito IS NULL)
    BEGIN
        RAISERROR('Parametro OPERESTDEVDEF n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdEspTitCred = NULL

    SELECT @nCdEspTitCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CDESPTIT-CRED'

    IF (@nCdEspTitCred IS NULL)
    BEGIN
        RAISERROR('Parametro CDESPTIT-CRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdEspTitCredReneg = NULL

    SELECT @nCdEspTitCredReneg = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CDESPTIT-CREDRE'

    IF (@nCdEspTitCredReneg IS NULL)
    BEGIN
        RAISERROR('Parametro CDESPTIT-CREDRE n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdEspTitCartao = NULL

    SELECT @nCdEspTitCartao = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CDESPTIT-CARTAO'

    IF (@nCdEspTitCartao IS NULL)
    BEGIN
        RAISERROR('Parametro CDESPTIT-CARTAO n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdFormaPagtoDin = NULL

    SELECT @nCdFormaPagtoDin = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'FORMAPAGTODIN'

    IF (@nCdFormaPagtoDin IS NULL)
    BEGIN
        RAISERROR('Parametro FORMAPAGTODIN n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperJuroCred = NULL

    SELECT @nCdOperJuroCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERJUROCRED'

    IF (@nCdOperJuroCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERJUROCRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperDescJuroCred = NULL

    SELECT @nCdOperDescJuroCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERDESCJURCRED'

    IF (@nCdOperDescJuroCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERDESCJURCRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperJuroFinCred = NULL

    SELECT @nCdOperJuroFinCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERJUROFINCRED'

    IF (@nCdOperJuroFinCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERJUROFINCRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperDescCred = NULL

    SELECT @nCdOperDescCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERDESCCRED'

    IF (@nCdOperDescCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERDESCCRED n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END
    
    Set @cFlgUtilizaSaldoVP = NULL

    SELECT @cFlgUtilizaSaldoVP = cValor
      FROM Parametro
     WHERE cParametro = 'UTILIZASALDOVP'
    
    IF (@cFlgUtilizaSaldoVP IS NULL)
    BEGIN
        RAISERROR('Parametro UTILIZASALDOVP n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END 
    
    SELECT @cFlgUsaMetaDesc = dbo.fn_LeParametro('USARMETADESC')
    IF (@cFlgUsaMetaDesc NOT IN('N','S'))
    BEGIN
        RAISERROR('Parametro USARMETADESC n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END
    
    --
    -- P : gera solicita��o de empenho por pedido
    -- D : gera solicita��o de empenho di�rio
    --
    SET @cModoEmpenhoCD = dbo.fn_LeParametro('MODEMPENHOVENDA')
    
    IF (@cModoEmpenhoCD NOT IN('P','D'))
    BEGIN
        RAISERROR('Par�metro MODEMPENHOVENDA n�o configurado ou inv�lido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END 

    IF (OBJECT_ID('tempdb..#Temp_Pedido') IS NULL) 
    BEGIN

        CREATE TABLE  #Temp_Pedido (nCdPedido int
                                   ,nValPedido decimal(12,2))

    END

	IF (OBJECT_ID('tempdb..#Temp_Cheque_Pagamento') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Cheque_Pagamento (nCdCheque int identity(1,1)
								   ,nCdBanco      int
								   ,nCdAgencia    char(4)
								   ,nCdConta      char(15)
								   ,cDigito       CHAR(1)
								   ,iNrCheque     int
								   ,nValCheque    DECIMAL(12,2) default 0 not null
								   ,cCNPJCPF      CHAR(14)
								   ,dDtDeposito   DATETIME
								   ,dDtLimite     DATETIME
								   ,iAutorizacao  int
								   ,nCdTemp_Pagto int
								   ,PRIMARY KEY (nCdCheque))

	END

    IF (OBJECT_ID('tempdb..#Temp_Crediario') IS NULL)
    BEGIN

        CREATE TABLE  #Temp_Crediario (dDtVencto     DATETIME
                                      ,dDtSugerida   DATETIME
                                      ,dDtLimite     DATETIME
                                      ,nValParcela   DECIMAL(12,2)                                
                                      ,cFlgEntrada   int           default 0 not null
                                      ,nCdTemp_Pagto int)

    END

	IF (OBJECT_ID('tempdb..#Temp_Titulos') IS NULL)
	BEGIN

        CREATE TABLE #Temp_Titulos (nCdTitulo           int
			                       ,cNrTit              char(17)
			                       ,cNmEspTit           varchar(50)
                                   ,dDtEmissao          datetime
			                       ,dDtVenc             datetime
			                       ,nValTit             decimal(12,2) default 0 not null
			                       ,nValJuro            decimal(12,2) default 0 not null
			                       ,nValDesconto        decimal(12,2) default 0 not null
			                       ,nSaldoTit           decimal(12,2) default 0 not null
			                       ,nValTotal           decimal(12,2) default 0 not null
			                       ,cNmLoja             varchar(50)
			                       ,iDiasAtraso         int           default 0 NOT NULL
                                   ,cFlgEntrada         int           default 0 NOT NULL
                                   ,nCdCrediario        int           
                                   ,iParcela            int           default 0 NOT NULL
                                   ,cFlgMarcado         int           default 0 NOT NULL
                                   ,nCdTerceiro         int
                                   ,cFlgDescontoJuros   int           default 0 not null
                                   ,nValJurosDescontado decimal(12,2) default 0 not null)

	END

	IF (OBJECT_ID('tempdb..#Temp_Pagtos') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Pagtos (nCdTemp_Pagto      int identity(1,1)
								  ,nCdFormaPagto      int
								  ,nCdCondPagto       int
								  ,nCdTerceiro        int
								  ,nValPagto          decimal(12,2)  default 0 not null
								  ,nValEntrada        decimal(12,2)  default 0 not null
								  ,nValDesconto       decimal(12,2)  default 0 not null
								  ,nValAcrescimo      decimal(12,2)  default 0 not null
                                  ,nValJurosCondPagto decimal(12,2)  default 0 not null
								  ,cFlgTipo           int
								  ,iNrCartao          bigint
								  ,iNrDocto           int
								  ,iNrAutorizacao     int
								  ,cNumeroControle	  varchar(50)
								  ,cNrDoctoNSU        varchar(50)
								  ,cNrAutorizacao     varchar(12)
								  ,nCdOperadoraCartao int
								  ,nCdCrediario       int
                                  ,cFlgVale           int            default 0 not null
                                  ,cFlgTEF            int            default 0 not null
                                  ,nValDescontoManual decimal(12,2)  default 0 not null
                                  ,nCdPropostaReneg   int
                                  ,iQtdeParcReneg     int            default 0 not null
                                  ,nValParcReneg      decimal(12,2)  default 0 not null
                                  ,nValValeDesconto   decimal(12,2)  default 0 not null
                                  ,PRIMARY KEY (nCdTemp_Pagto))
	END

	IF (OBJECT_ID('tempdb..#Temp_ChequeResgate') IS NULL)
	BEGIN

		CREATE TABLE #Temp_ChequeResgate (nCdCheque          int
										 ,nCdBanco           int
										 ,iNrCheque          int
										 ,nValCheque         decimal(12,2) default 0 not null
										 ,dDtDeposito        datetime
										 ,cNmTabStatusCheque varchar(50)
										 ,cNmTerceiro        varchar(50)
										 ,nValJuros          decimal(12,2) default 0 not null
										 ,nValTotal          decimal(12,2) default 0 not null
										 ,PRIMARY KEY (nCdCheque))

	END

	IF (OBJECT_ID('tempdb..#Temp_RestricaoVenda') IS NULL)
	BEGIN

		CREATE TABLE #Temp_RestricaoVenda (nCdTempRestricaoVenda    int      identity(1,1)
										  ,nCdTemp_Pagto            int
										  ,nCdTabTipoRestricaoVenda int      not null
										  ,nCdUsuarioAutor          int      
										  ,dDtAutor                 datetime
										  ,cJustificativa           text
										  ,cFlgLiberado             int      default 0 not null
										  ,cTemporario              varchar(15))

	END

	IF OBJECT_ID('tempdb..#Temp_ValePresente') IS NULL
	BEGIN
		CREATE TABLE #Temp_ValePresente (cCodigo varchar(30)
										,nValor  decimal(12,2) default 0 not null
										,PRIMARY KEY (cCodigo))
	END

	IF OBJECT_ID('tempdb..#Temp_AtualizaCarteira') IS NULL
	BEGIN
		CREATE TABLE #Temp_AtualizaCarteira (nCdTerceiro int)
	END

    SELECT @nCdContaBancaria = nCdContaBancaria
          ,@nCdLoja          = Loja.nCdLoja
          ,@nCdEmpresa       = Loja.nCdEmpresa
      FROM ResumoCaixa
           INNER JOIN Loja ON Loja.nCdLoja = ResumoCaixa.nCdLoja
     WHERE nCdResumoCaixa = @nCdResumoCaixa

    SELECT @nCdCCReceita = nCdCCReceita
      FROM Loja
     WHERE nCdLoja = @nCdLoja

    Set @nValTotalCheque = 0

    SELECT @nValTotalCheque = Sum(IsNull(nValCheque,0))
      FROM #Temp_Cheque_Pagamento

    Set @nValCrediario = 0

    SELECT @nValCrediario   = Sum(IsNull(nValParcela,0))
      FROM #Temp_Crediario

    Set @nValRecCrediario = 0

    SELECT @nValRecCrediario = Sum(IsNull(nValTotal,0)) -- Valor Total Pago
      FROM #Temp_Titulos

    --
    -- Verifica os valores do movimento (parcelas pagas, cheques resgatados e pedidos recebidos)
    --
    SELECT @iQtdeRecCrediario     = Count(1)
      FROM #Temp_Titulos

    SELECT @iQtdeRecChequeResgate = Count(1)
      FROM #Temp_ChequeResgate

    SELECT @iQtdeRecPedido        = Count(1)
      FROM #Temp_Pedido

    Set @nValRecCrediario = IsNull(@nValRecCrediario,0)

    --
    -- Descobre se � recebimento de uma renegocia��o
    --
    SELECT TOP 1 @nCdPropostaReneg = Temp.nCdPropostaReneg
                ,@iQtdeParcReneg   = Temp.iQtdeParcReneg
                ,@nValParcReneg    = Temp.nValParcReneg
                ,@nValEntradaReneg = Temp.nValEntrada
                ,@nValRenegTotal   = PropostaReneg.nSaldoNegociado
      FROM #Temp_Pagtos Temp
           INNER JOIN PropostaReneg ON PropostaReneg.nCdPropostaReneg = Temp.nCdPropostaReneg
     WHERE Temp.nCdPropostaReneg IS NOT NULL

    --
    -- Soma o valor em dinheiro na conta do caixa
    --
    Set @nValDinheiro = 0

    SELECT @nValDinheiro       = Sum(nValPagto)
      FROM #Temp_Pagtos
     WHERE nCdFormaPagto = 1

    IF (IsNull(@nValDinheiro,0) > 0) 
    BEGIN

		UPDATE ContaBancaria
		   SET nSaldoConta      = nSaldoConta + @nValDinheiro
		 WHERE nCdContaBancaria = @nCdContaBancaria

    END

    --
    -- Verifica se algum tipo de lan�amento foi feito
    --
    IF (    (IsNull(@iQtdeRecPedido,0)        <= 0)
        AND (IsNull(@iQtdeRecCrediario,0)     <= 0)
        AND (IsNull(@iQtdeRecChequeResgate,0) <= 0)
        AND @nCdPropostaReneg IS NULL) 
    BEGIN
        RAISERROR('Nenhum tipo de opera��o encontrado para encerrar o movimento. Por favor reinicie o processo.',16,1)
        ROLLBACK TRAN
        RETURN
    END

    --
    -- Gera o lancamento financeiro do recebimento
    --
    Set @nValTotalPagto = 0 

    SELECT @nValTotalPagto     = Sum(nValPagto)
      FROM #Temp_Pagtos 

    Set @nValTotalPagto     = IsNull(@nValTotalPagto,0)

	Set @nCdLanctoFin = NULL

	EXEC usp_ProximoID  'LANCTOFIN'
					   ,@nCdLanctoFin OUTPUT

    INSERT INTO LanctoFin (nCdLanctoFin
                          ,nCdResumoCaixa 
                          ,nCdContaBancaria 
                          ,dDtLancto               
                          ,nCdTipoLancto 
                          ,nValLancto                              
                          ,nCdUsuario)
                    VALUES(@nCdLanctoFin
                          ,@nCdResumoCaixa
                          ,@nCdContaBancaria
                          ,GetDate()
                          ,CASE WHEN IsNull(@iQtdeRecPedido,0)        > 0 THEN @nCdTipoLancVDCx
                                WHEN IsNull(@iQtdeRecCrediario,0)     > 0 THEN @nCdTipoLancRPCx
                                WHEN IsNull(@iQtdeRecChequeResgate,0) > 0 THEN @nCdTipolancRCCx
                                WHEN @nCdPropostaReneg IS NOT NULL        THEN @nCdTipoLancReneg 
                           END
                          ,@nValTotalPagto
                          ,@nCdUsuario)

    --
    -- Atualiza o lan�amento financeiro no documento fiscal ECF
    --
    IF (@nCdDoctoFiscal IS NOT NULL)
    BEGIN

        UPDATE DoctoFiscal
           SET nCdLanctoFinDoctoFiscal = @nCdLanctoFin
         WHERE nCdDoctoFiscal          = @nCdDoctoFiscal
           AND nCdLanctoFinDoctoFiscal IS NULL

    END

    -- 
    -- Gera os detalhes do pagamento
    --
    DECLARE curAux CURSOR
       FAST_FORWARD
        FOR SELECT nCdFormaPagto
                  ,nCdCondPagto
                  ,CASE WHEN cFlgTipo = 1 THEN 0  --cFlgTipo = 1 --Pagamento Normal
                        WHEN cFlgTipo = 2 THEN 1  --cFlgTipo = 2 --Pagamento Entrada
                   END
                  ,nValPagto
                  ,nValDesconto-nValDescontoManual --O desconto vem com o desconto da condi��o + o desconto manual
                  ,nValAcrescimo
                  ,nValDescontoManual
              FROM #Temp_pagtos

    OPEN curAux

    FETCH NEXT
     FROM curAux
     INTO @nCdFormaPagto
         ,@nCdCondPagto
         ,@cFlgEntrada
         ,@nValPagto
         ,@nValDescontoCondPagto
         ,@nValJurosCondPagto
         ,@nValDescontoManual

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

		Set @nCdFormaPagtoLanctoFin = NULL

		EXEC usp_ProximoID  'FORMAPAGTOLANCTOFIN'
						   ,@nCdFormaPagtoLanctoFin OUTPUT

		INSERT INTO FormaPagtoLanctoFin (nCdFormaPagtoLanctoFin
                                        ,nCdLanctoFin
										,nCdFormaPagto 
										,nCdCondPagto 
										,cFlgEntrada
										,nValPagto
										,nValDescontoCondPagto
										,nValJurosCondPagto
										,nValDescontoManual)
								  VALUES(@nCdFormaPagtoLanctoFin
                                        ,@nCdLanctoFin
										,@nCdFormaPagto
										,@nCdCondPagto
										,@cFlgEntrada
										,@nValPagto
										,@nValDescontoCondPagto
										,@nValJurosCondPagto
										,@nValDescontoManual)
										
        --
        -- se utiliza meta de desconto, contabiliza descontos utilizados
        --
		IF (@cFlgUsaMetaDesc = 'S')
		BEGIN
		
			SELECT TOP 1
				   @nCdPedidoMetaDesc   = Pedido.nCdPedido
				  ,@nCdLojaMetaDesc     = Pedido.nCdLoja
				  ,@nCdTerceiroMetaDesc = Pedido.nCdTerceiroColab
			  FROM #Temp_Pedido
				   INNER JOIN Pedido ON Pedido.nCdPedido = #Temp_Pedido.nCdPedido
			
			SELECT TOP 1 
			       @nCdMetaVendedorDesconto = MetaVendedorDesconto.nCdMetaVendedorDesconto
				  ,@nValDescontoAnt         = ISNULL(MetaDescontoUtil.nValDescontoPost, MetaVendedorDesconto.nValCotaDesconto)
			  FROM MetaVendedorDesconto
				   LEFT JOIN MetaDescontoMes  ON MetaDescontoMes.nCdMetaDescontoMes = MetaVendedorDesconto.nCdMetaDescontoMes
				   LEFT JOIN MetaDescontoUtil ON MetaDescontoUtil.nCdMetaVendedorDesconto = MetaVendedorDesconto.nCdMetaVendedorDesconto
			 WHERE MetaVendedorDesconto.nCdTerceiroColab = @nCdTerceiroMetaDesc
			   AND MetaDescontoMes.nCdLoja               = @nCdLojaMetaDesc
			   AND MetaDescontoMes.iAno                  = YEAR(GETDATE())
			   AND MetaDescontoMes.iMes                  = MONTH(GETDATE())
			 ORDER BY MetaDescontoUtil.dDtPedido DESC
			 
			IF ((@nCdMetaVendedorDesconto IS NULL) AND (@nCdTerceiroMetaDesc IS NOT NULL))
			BEGIN
			    CLOSE curAux
                DEALLOCATE curAux
			    RAISERROR('Vendedor %i n�o possu� distribui��o de meta para desconto para o m�s vigente.',16,1,@nCdTerceiroMetaDesc)
                RETURN
			END
			  
			EXEC usp_ProximoID 'METADESCONTOUTIL'
							  ,@nCdMetaDescontoUtil OUTPUT
			              						
			INSERT INTO MetaDescontoUtil (nCdMetaDescontoUtil
										 ,nCdPedido
										 ,nCdTerceiroColab
										 ,nCdMetaVendedorDesconto
										 ,nValDescontoAnt
										 ,nValDescontoUtil
										 ,nValDescontoPost
										 ,dDtPedido)
								  VALUES (@nCdMetaDescontoUtil
										 ,@nCdPedidoMetaDesc
										 ,@nCdTerceiroMetaDesc
										 ,@nCdMetaVendedorDesconto
										 ,@nValDescontoAnt
										 ,@nValDescontoManual
										 ,@nValDescontoAnt - @nValDescontoManual
										 ,GETDATE())
										 
        END

		FETCH NEXT
		 FROM curAux
		 INTO @nCdFormaPagto
			 ,@nCdCondPagto
			 ,@cFlgEntrada
			 ,@nValPagto
			 ,@nValDescontoCondPagto
			 ,@nValJurosCondPagto
			 ,@nValDescontoManual

    END

    CLOSE curAux
    DEALLOCATE curAux

    UPDATE ResumoCaixa 
       SET nValCredito    = nValCredito + @nValTotalPagto
     WHERE nCdResumoCaixa = @nCdResumoCaixa

    --
    -- Baixa estoque dos pedidos
    --

    Set @nCdTerceiroPagto  = NULL
    Set @nCdCondPagtoMaior = NULL

    --
    -- Descobre o maior cliente do pagamento
    --
    SELECT TOP 1 @nCdTerceiroPagto = nCdTerceiro
      FROM #Temp_Pagtos
     WHERE nCdTerceiro IS NOT NULL
       AND cFlgTipo     = 1 -- Tipo 1 - pagamento (exceto pagamento de entrada)
     ORDER BY nValPagto DESC

    --
    -- Descobre a maior condi��o de pagamento
    --
    SELECT TOP 1 @nCdCondPagtoMaior = nCdCondPagto
      FROM #Temp_Pagtos
     WHERE nCdCondPagto IS NOT NULL
       AND cFlgTipo      = 1 -- Tipo 1 - pagamento (exceto pagamento de entrada)
     ORDER BY nValPagto DESC

    --
    -- Descobre o percentual de juros e o percentual de desconto para aplicar nos produtos
    --
    SELECT @nValDescontoTotal = Sum(nValDesconto)
      FROM #Temp_Pagtos

    SELECT @nValAcrescimoTotal = Sum(nValAcrescimo)
      FROM #Temp_Pagtos

    SELECT @nValPedidoTotal = Sum(#Temp_Pedido.nValPedido)
          ,@nValTrocaTotal  = Sum(Pedido.nValDevoluc)
      FROM #Temp_Pedido
           INNER JOIN Pedido ON Pedido.nCdPedido = #Temp_Pedido.nCdPedido

    SELECT @nValValeMercTotal = Sum(ABS(ItemPedido.nValTotalItem))
      FROM #Temp_Pedido
           INNER JOIN ItemPedido ON ItemPedido.nCdPedido = #Temp_Pedido.nCdPedido
     WHERE ItemPedido.nCdTipoItemPed = 11

    Set @cFlgTemPedido      = 0
    Set @cFlgDefeito        = 0
    Set @cFlgTeveDefeito    = 0
    Set @nValDescontoTotal  = IsNull(@nValDescontoTotal,0)
    Set @nValAcrescimoTotal = IsNull(@nValAcrescimoTotal,0)
    Set @nValPedidoTotal    = IsNull(@nValPedidoTotal,0)
    Set @nValTrocaTotal     = IsNull(@nValTrocaTotal,0)
    Set @nValValeMercTotal  = IsNull(@nValValeMercTotal,0)

    DECLARE curPedidos CURSOR FOR
      SELECT nCdPedido
            ,nValPedido
        FROM #Temp_Pedido

    OPEN curPedidos

    FETCH NEXT
     FROM curPedidos
     INTO @nCdPedido
         ,@nValPedidoAnterior

    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        Set @cFlgTemPedido   = 1
        Set @nCdTabStatusPed = NULL
        Set @nCdLanctoFinPed = NULL

        --
        -- Verifica se o pedido n�o passou em nenhum caixa
        -- Faz esse teste para garantir que duas caixas n�o passem o mesmo pedido ao mesmo tempo.
        --
        SELECT @nCdTabStatusPed = nCdTabStatusPed 
              ,@nCdLanctoFinPed = nCdLanctoFin
              ,@nValPedidoAtual = nValPedido
          FROM Pedido
         WHERE nCdPedido = @nCdPedido

        IF (@nCdTabStatusPed = 5) --Pedido ja processado em outro caixa
        BEGIN

            SELECT @cDadosBaixa = 'Pedido ' + Convert(VARCHAR(10),@nCdPedido) + ' j� processado no caixa ' + LTRIM(RTRIM(Convert(VARCHAR(50),ContaBancaria.nCdConta))) + ' em ' + Convert(VARCHAR(10),LanctoFin.dDtLancto,103) + ' ' + Convert(VARCHAR(10),LanctoFin.dDtLancto,108)
              FROM LanctoFin
                   INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria = LanctoFin.nCdContaBancaria
             WHERE LanctoFin.nCdLanctoFin = @nCdLanctoFinPed

            CLOSE curPedidos
            DEALLOCATE curPedidos

		    RAISERROR('%s',16,1,@cDadosBaixa)
		    ROLLBACK
		    RETURN

        END

        IF (@nCdTabStatusPed = 10) --Pedido cancelado
        BEGIN

            Set @cDadosBaixa = 'Pedido ' + Convert(VARCHAR(10),@nCdPedido) + ' est� cancelado.'

            CLOSE curPedidos
            DEALLOCATE curPedidos

		    RAISERROR(@cDadosBaixa,16,1)
		    ROLLBACK
		    RETURN

        END

        IF (@nValPedidoAnterior <> @nValPedidoAtual)
        BEGIN
         
            Set @cDadosBaixa = 'O Valor do Pedido ' + Convert(VARCHAR(10),@nCdPedido) + ' foi alterado, por favor reinicie o processo de recebimento.'

            CLOSE curPedidos
            DEALLOCATE curPedidos

		    RAISERROR(@cDadosBaixa,16,1)
		    ROLLBACK
		    RETURN

        END

        UPDATE Pedido
           SET nCdLanctoFin    = @nCdLanctoFin
              ,nCdTabStatusPed = 5
              ,nCdTerceiro     = CASE WHEN IsNull(@nCdTerceiroPagto,0) > 0 THEN @nCdTerceiroPagto
                                      ELSE nCdTerceiro
                                 END   
              ,nCdCondPagto    = CASE WHEN IsNull(@nCdCondPagtoMaior,0) > 0 THEN @nCdCondPagtoMaior
                                      ELSE nCdCondPagto
                                 END
         WHERE nCdPedido = @nCdPedido

        UPDATE Pedido
           SET nCdTerceiroPagador = nCdTerceiro
         WHERE nCdPedido = @nCdPedido
        
        --
        -- Envia o pedido para o central
        --
        EXEC SP_ENVIA_PEDIDO_TEMPREGISTRO @nCdPedido

 
        DECLARE curItens CURSOR FOR
           SELECT nCdItemPedido
                 ,nCdProduto
                 ,nCdTipoItemPed
                 ,nQtdePed
                 ,nValTotalItem
                 ,nCdVale
                 ,cFlgDefeito
                 ,nCdCampanhaPromoc
                 ,nValCMV
                 ,nValUnitario
                 ,nCdItemPedidoTroca
                 ,cFlgMovEstoqueConsignado
             FROM ItemPedido
            WHERE nCdPedido = @nCdPedido

        OPEN curItens

        FETCH NEXT
         FROM curItens
         INTO @nCdItemPedido
             ,@nCdProduto
             ,@nCdTipoItemPed
             ,@nQtdePed
             ,@nValTotalItem
             ,@nCdValePed
             ,@cFlgDefeito
             ,@nCdCampanhaPromoc
             ,@nValCMV
             ,@nValUnitario
             ,@nCdItemPedidoTroca
             ,@cFlgMovEstoqueConsignado

        WHILE (@@FETCH_STATUS = 0)
        BEGIN

            IF (@cFlgDefeito = 1) Set @cFlgTeveDefeito = 1

            --
            -- Recupera os dados b�sicos da transa��o
            --
 			SELECT @nCdEmpresa         = Pedido.nCdEmpresa
				  ,@nCdLoja            = Pedido.nCdLoja
				  ,@nCdPedido          = Pedido.nCdPedido
				  ,@nCdTipoPedido      = Pedido.nCdTipoPedido
				  ,@nCdOperacaoEstoque = nCdOperacaoEstoque
				  ,@nCdGrupoProduto    = GrupoProduto.nCdGrupoProduto
				  ,@cNmGrupoProduto    = GrupoProduto.cNmGrupoProduto
				  ,@nCdProduto         = ItemPedido.nCdProduto
				  ,@nCdTipoItemPed     = ItemPedido.nCdTipoItemPed
                  ,@nCdCategFinanc     = TipoPedido.nCdCategFinanc
                  ,@nCdUnidadeNegocio  = TipoPedido.nCdUnidadeNegocio
                  ,@nCdEspTit          = TipoPedido.nCdEspTit
			  FROM ItemPedido
				   LEFT  JOIN Produto      ON Produto.nCdProduto           = ItemPedido.nCdProduto
				   LEFT  JOIN Departamento ON Departamento.nCdDepartamento = Produto.nCdDepartamento
				   LEFT  JOIN GrupoProduto ON GrupoProduto.nCdGrupoProduto = Departamento.nCdGrupoProduto
				   INNER JOIN Pedido       ON Pedido.nCdPedido             = ItemPedido.nCdPedido
				   INNER JOIN TipoPedido   ON TipoPedido.nCdTipoPedido     = Pedido.nCdTipoPedido
			 WHERE nCdItemPedido = @nCdItemPedido

            IF (@nCdTipoItemPed IN (1,2,4,9))
            BEGIN

                IF (@nValTotalItem < 0)
                BEGIN
                    Set @nCdOperacaoEstoque = @nCdOperacaoEstoqueDev

                    IF (@cFlgDefeito = 1) Set @nCdOperacaoEstoque = @nCdOperacaoEstoqueDefeito
                END

                --
                -- Descobre o Estoque que ir� movimentar
                --
				Set @nCdLocalEstoque = NULL

				IF (@nCdLoja IS NULL)
				BEGIN

					SELECT @nCdLocalEstoque = nCdLocalEstoque
					  FROM OperacaoLocalEstoque
					 WHERE nCdEmpresa         = @nCdEmpresa
					   AND nCdLoja           IS NULL
					   AND nCdGrupoProduto    = @nCdGrupoProduto
					   AND nCdOperacaoEstoque = @nCdOperacaoEstoque

					IF (@nCdLocalEstoque IS NULL)
					BEGIN

						SELECT @nCdLocalEstoque = nCdLocalEstoque
						  FROM OperacaoLocalEstoque
						 WHERE nCdEmpresa          = @nCdEmpresa
						   AND nCdLoja            IS NULL
						   AND nCdOperacaoEstoque  = @nCdOperacaoEstoque

					END

				END
				ELSE
				BEGIN

					SELECT @nCdLocalEstoque = nCdLocalEstoque
					  FROM OperacaoLocalEstoque
					 WHERE nCdEmpresa         = @nCdEmpresa
					   AND nCdLoja            = @nCdLoja
					   AND nCdGrupoProduto    = @nCdGrupoProduto
					   AND nCdOperacaoEstoque = @nCdOperacaoEstoque

					IF (@nCdLocalEstoque IS NULL)
					BEGIN

						SELECT @nCdLocalEstoque = nCdLocalEstoque
						  FROM OperacaoLocalEstoque
						 WHERE nCdEmpresa         = @nCdEmpresa
						   AND nCdLoja            = @nCdLoja
						   AND nCdOperacaoEstoque = @nCdOperacaoEstoque

					END

				END

				IF (@nCdLocalEstoque IS NULL)
				BEGIN
                    --
                    -- Tenta localizar um estoque vinculado a loja
                    --
                    SELECT TOP 1 
                           @nCdLocalEstoque = nCdLocalEstoque
                      FROM LocalEstoque
                     WHERE nCdLoja          = @nCdLoja
                       AND EXISTS(SELECT 1
                                    FROM GrupoProdutoLocalEstoque GPLE
                                   WHERE GPLE.nCdGrupoProduto = @nCdGrupoProduto
                                     AND GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque)

                    IF (@nCdLocalEstoque IS NULL)
                    BEGIN
                        CLOSE curItens
                        DEALLOCATE curItens

                        CLOSE curPedidos
                        DEALLOCATE curPedidos

					    RAISERROR('Local de estoque n�o encontrado para o grupo de produtos %i-%s   Empresa: %i    Loja: %i   Opera��o de Estoque: %i',16,1,@nCdGrupoProduto,@cNmGrupoProduto,@nCdEmpresa, @nCdLoja, @nCdOperacaoEstoque)
					    ROLLBACK
					    RETURN
                    END

				END
				
				SELECT @cFlgConsignacao = cFlgConsignacao
                  FROM Pedido
                 WHERE nCdPedido = @nCdPedido
                
                IF(@cFlgConsignacao = 0)
                BEGIN
                    SET @cOBSEstoque = 'PDV ' + Convert(VARCHAR,@nCdPedido)               
                END
                ELSE
                BEGIN
                    SELECT @nCdConsignacao = nCdConsignacao
                      FROM Consignacao
                     WHERE nCdPedido = @nCdPedido
                    
                    SET @cOBSEstoque = 'CONSIG-' + Convert(VARCHAR,@nCdConsignacao)
                END
                
                --
                -- verifica se item n�o foi movimentado pela consigna��o
                --
                IF (@cFlgMovEstoqueConsignado = 0) 
                BEGIN
                
				    EXEC SP_MOVIMENTA_ESTOQUE @nCdProduto
					    					 ,@nCdLocalEstoque    
						    				 ,@nCdOperacaoEstoque 
							    			 ,@nQtdePed             
                                             ,@cOBSEstoque
                END

				--
				-- Atualiza a data de venda no local de estoque
				--
				UPDATE PosicaoEstoque
				   SET dDtUltVendaEstoque = GetDate()
				 WHERE nCdProduto         = @nCdProduto
				   AND nCdLocalEstoque    = @nCdLocalEstoque  


            END

            --
            -- Gerar Vale presente
            -- Se foi vendido vale presente e a empresa n�o usa cart�o presente, gera os vales aqui
            --
            IF (@nCdTipoItemPed = 10) AND (@cUsaCartaoPresente = 'N')
            BEGIN
            
                Set @cFlgValePresente = 'S' 

				Set @nCdVale_Out = NULL

				EXEC usp_ProximoID  'VALE'
								   ,@nCdVale_Out OUTPUT

                INSERT INTO Vale (nCdVale
                                 ,dDtVale                 
                                 ,nCdTerceiro 
                                 ,nCdTabTipoVale 
                                 ,nValVale                                
                                 ,nSaldoVale                              
                                 ,nCdEmpresa  
                                 ,nCdLoja     
                                 ,nCdLanctoFin)
                           VALUES(@nCdVale_Out
                                 ,GetDate()
                                 ,CASE WHEN IsNull(@nCdTerceiroPagto,0) > 0 THEN @nCdTerceiroPagto
                                       ELSE @nCdTerceiroPDV
                                  END
                                 ,2
                                 ,@nValTotalItem
                                 ,@nValTotalItem
                                 ,@nCdEmpresa
                                 ,@nCdLoja
                                 ,@nCdLanctoFin)

                UPDATE Vale
                   SET cCodigo = nCdVale
                 WHERE nCdVale = @nCdVale_Out

            END

            IF (@nCdTipoItemPed = 11)
            BEGIN

                --
                -- Antes de baixar, verifica se o vale ainda tem saldo disponivel
                --
                Set @nSaldoVale = 0

                SELECT @nSaldoVale = nSaldoVale
                  FROM Vale
                 WHERE nCdVale = @nCdValePed

                IF (@nSaldoVale <= 0)
                BEGIN

					CLOSE curItens
					DEALLOCATE curItens

					CLOSE curPedidos
					DEALLOCATE curPedidos

                    RAISERROR('O saldo do vale %i n�o est� mais dispon�vel.',16,1,@nCdValePed)
					ROLLBACK
					RETURN
                 
                END

                UPDATE Vale
                   SET nSaldoVale        = 0
                      ,nCdLanctoFinBaixa = @nCdLanctoFin
                      ,dDtBaixa          = GetDate()
                 WHERE nCdVale           = @nCdValePed

            END

            --
            -- Atualiza o custo final do item e a quantidade atendida do pedido
            --
            UPDATE ItemPedido
               SET nQtdeExpRec   = nQtdePed
                  ,nValCustoUnit = CASE WHEN nValUnitario <> 0 THEN nValUnitario
                                        ELSE 0
                                   END
             WHERE nCdItemPedido = @nCdItemPedido

            --
            -- Envia para central
            --
            EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedido

            IF (@nCdItemPedidoTroca IS NOT NULL)
            BEGIN

                UPDATE ItemPedido
                   SET cFlgTrocado   = 1
                 WHERE nCdItemPedido = @nCdItemPedidoTroca

                --
                -- Envia para central
                --
                EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedidoTroca

            END

            --
            -- Atualiza o saldo da campanha promocional
            -- Esta atualiza��o � feita antes do rateio do juros/desconto do caixa, para n�o influenciar na margem dos produtos
            -- em campanha promocional.
            --
            IF (@nCdCampanhaPromoc IS NOT NULL)
            BEGIN

                UPDATE CampanhaPromoc
                   SET nValVenda = nValVenda + (@nValUnitario * @nQtdePed)
                      ,nValCMV   = nValCMV   + @nValCMV
                 WHERE nCdCampanhaPromoc = @nCdCampanhaPromoc

                UPDATE CampanhaPromoc
                   SET nMargemLucro = CASE WHEN nValVenda = 0 AND nValCMV = 0 THEN 0
                                           WHEN nValVenda > 0 AND nValCMV = 0 THEN 100
                                           ELSE (((nValVenda / nValCMV)*100)-100)
                                      END
                 WHERE nCdCampanhaPromoc = @nCdCampanhaPromoc

            END

            --
            -- Se n�o for vale mercadoria e n�o for item de troca, atualiza os totais
            --
           
            IF (@nCdTipoItemPed != 11) AND (@nValTotalItem > 0)
            BEGIN
				--
				-- Atualiza o desconto no item
				--
				IF (@nValDescontoTotal > 0)
				BEGIN

					UPDATE ItemPedido
					   SET nValDesconto  = ROUND((nValUnitario  * (@nValDescontoTotal/(@nValPedidoTotal+@nValTrocaTotal+@nValDescontoTotal+@nValValeMercTotal))),2) * nQtdePed
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Quando tem troca no pedido, o percentual de desconto no total n�o � o mesmo se aplicado no item
                    --
                    UPDATE ItemPedido
                       SET nValDesconto  = @nValDescontoTotal
					 WHERE nCdItemPedido = @nCdItemPedido
                       AND nValDesconto  > @nValDescontoTotal
                
					UPDATE ItemPedido
					   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                          ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Envia para central
                    --
                    EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedido


				END

				--
				-- Atualiza o acrescimo no item
				--
				IF (@nValAcrescimoTotal > 0)
				BEGIN
	 	 
					UPDATE ItemPedido
					   SET nValAcrescimo = ROUND((nValUnitario * (@nValAcrescimoTotal/(@nValPedidoTotal+@nValTrocaTotal-@nValAcrescimoTotal+@nValValeMercTotal))),2) * nQtdePed
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Quando tem troca no pedido, o percentual de acrescimo no total n�o � o mesmo se aplicado no item
                    --
                    UPDATE ItemPedido
                       SET nValAcrescimo  = @nValAcrescimoTotal
					 WHERE nCdItemPedido  = @nCdItemPedido
                       AND nValAcrescimo  > @nValAcrescimoTotal

					UPDATE ItemPedido
					   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
						  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Envia para central
                    --
                    EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedido
				END

            END

			Set @nCdItemPedidoAtendido = NULL

			EXEC usp_ProximoID  'ITEMPEDIDOATENDIDO'
							   ,@nCdItemPedidoAtendido OUTPUT

			INSERT INTO ItemPedidoAtendido (nCdItemPedidoAtendido
                                           ,nCdItemPedido
										   ,nCdProduto
										   ,nQtdeAtendida
										   ,dDtAtendimento
                                           ,nCdDoctoFiscal)
                                     VALUES(@nCdItemPedidoAtendido
                                           ,@nCdItemPedido
                                           ,@nCdProduto
                                           ,@nQtdePed
                                           ,GetDate()
                                           ,@nCdDoctoFiscal)

			FETCH NEXT
			 FROM curItens
			 INTO @nCdItemPedido
				 ,@nCdProduto
				 ,@nCdTipoItemPed
				 ,@nQtdePed
				 ,@nValTotalItem
                 ,@nCdValePed
                 ,@cFlgDefeito
				 ,@nCdCampanhaPromoc
				 ,@nValCMV
                 ,@nValUnitario
                 ,@nCdItemPedidoTroca
                 ,@cFlgMovEstoqueConsignado

        END

        CLOSE curItens
        DEALLOCATE curItens
        
        --
        -- se modo de empenho for por pedido, efetua solicita��o do empenho
        --
        IF (@cModoEmpenhoCD = 'P')
        BEGIN
        
            EXEC SP_GERA_SOLICITACAO_CD @nCdPedido
            
        END

        FETCH NEXT
         FROM curPedidos
         INTO @nCdPedido
             ,@nValPedidoAnterior

    END

    CLOSE curPedidos

    --
    -- Se foi dado um desconto/acrescimo para zerar a venda, essa transa��o n�o tem pagamentos
    -- e o desconto/acrescimo deve ser rateado entre os itens
    --
    IF (     NOT EXISTS(SELECT 1
                        FROM #Temp_Pagtos) 
        AND  EXISTS(SELECT 1
                      FROM #Temp_Pedido
                           INNER JOIN Pedido ON Pedido.nCdPedido = #Temp_Pedido.nCdPedido
                     WHERE Pedido.nValDesconto  > 0
                        OR Pedido.nValAcrescimo > 0))
    BEGIN
   
        --
        -- Descobre o valor total do desconto/acrescimo
        --
        Set @nValDescontoTotal  = 0
        Set @nValAcrescimoTotal = 0

        SELECT @nValDescontoTotal  = Sum(Pedido.nValDesconto)
              ,@nValAcrescimoTotal = Sum(Pedido.nValAcrescimo)
          FROM #Temp_Pedido
               INNER JOIN Pedido ON Pedido.nCdPedido = #Temp_Pedido.nCdPedido

        IF (@nValDescontoTotal > 0) OR (@nValAcrescimoTotal > 0)
        BEGIN

            --
            -- Rateia nos itens
            --
            DECLARE curItensDesconto CURSOR
                FOR SELECT nCdItemPedido
                      FROM ItemPedido
                           INNER JOIN #Temp_Pedido ON #Temp_Pedido.nCdPedido = ItemPedido.nCdPedido
                     WHERE ItemPedido.nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9,10)
                       AND ItemPedido.nValTotalItem   > 0
             
            OPEN curItensDesconto

            FETCH NEXT 
             FROM curItensDesconto
             INTO @nCdItempedido
                 
            WHILE (@@FETCH_STATUS = 0)
            BEGIN

				--
				-- Atualiza o desconto no item
				--
				IF (@nValDescontoTotal > 0)
				BEGIN

					UPDATE ItemPedido
					   SET nValDesconto  = ROUND((nValUnitario  * (@nValDescontoTotal/(@nValPedidoTotal+@nValTrocaTotal+@nValDescontoTotal+@nValValeMercTotal))),2) * nQtdePed
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Quando tem troca no pedido, o percentual de desconto no total n�o � o mesmo se aplicado no item
                    --
                    UPDATE ItemPedido
                       SET nValDesconto  = @nValDescontoTotal
					 WHERE nCdItemPedido = @nCdItemPedido
                       AND nValDesconto  > @nValDescontoTotal
                
					UPDATE ItemPedido
					   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                          ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Envia para central
                    --
                    EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedido


				END

				--
				-- Atualiza o acrescimo no item
				--
				IF (@nValAcrescimoTotal > 0)
				BEGIN
	 	 
					UPDATE ItemPedido
					   SET nValAcrescimo = ROUND((nValUnitario * (@nValAcrescimoTotal/(@nValPedidoTotal+@nValTrocaTotal-@nValAcrescimoTotal+@nValValeMercTotal))),2) * nQtdePed
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Quando tem troca no pedido, o percentual de acrescimo no total n�o � o mesmo se aplicado no item
                    --
                    UPDATE ItemPedido
                       SET nValAcrescimo  = @nValAcrescimoTotal
					 WHERE nCdItemPedido  = @nCdItemPedido
                       AND nValAcrescimo  > @nValAcrescimoTotal

					UPDATE ItemPedido
					   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
						  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
					 WHERE nCdItemPedido = @nCdItemPedido

                    --
                    -- Envia para central
                    --
                    EXEC SP_ENVIA_SOMENTE_ITEMPEDIDO_TEMPREGISTRO @nCdItemPedido
				END

				FETCH NEXT 
				 FROM curItensDesconto
				 INTO @nCdItempedido

            END

            CLOSE curItensDesconto
            DEALLOCATE curItensDesconto

        END

    END

    --
    -- Confere se o total de desconto foi aplicado exatamente
    --
    IF (@nValDescontoTotal <> 0)
    BEGIN
        Set @nValAux = 0

        SELECT @nValAux       = Sum(ItemPedido.nValDesconto)
          FROM #Temp_Pedido Temp
               INNER JOIN ItemPedido ON ItemPedido.nCdPedido = Temp.nCdPedido
         WHERE ItemPedido.nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9,10)
           AND ItemPedido.nValTotalItem > 0

        --
        -- Escolhe o item com maior desconto para fazer o ajuste
        --
        SELECT TOP 1 @nCdItemPedido = nCdItemPedido
          FROM #Temp_Pedido Temp
               INNER JOIN ItemPedido ON ItemPedido.nCdPedido = Temp.nCdPedido
         WHERE ItemPedido.nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9,10)
           AND ItemPedido.nValTotalItem > 0
         ORDER BY ItemPedido.nValDesconto DESC

        --
        -- Se o desconto calculado foi maior que o desconto aplicado na venda
        --
        IF (@nValAux > @nValDescontoTotal)
        BEGIN

            UPDATE ItemPedido
               SET nValDesconto  = nValDesconto - (@nValAux - @nValDescontoTotal)
			 WHERE nCdItemPedido = @nCdItemPedido

			UPDATE ItemPedido
			   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
			 WHERE nCdItemPedido = @nCdItemPedido

        END

        --
        -- Se o desconto calculado foi menor que o desconto aplicado na venda
        --
        IF (@nValAux < @nValDescontoTotal)
        BEGIN

            UPDATE ItemPedido
               SET nValDesconto  = nValDesconto + (@nValDescontoTotal - @nValAux)
			 WHERE nCdItemPedido = @nCdItemPedido

			UPDATE ItemPedido
			   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
			 WHERE nCdItemPedido = @nCdItemPedido

        END

    END

    --
    -- Confere se o total de acrescimo foi aplicado exatamente
    --
    IF (@nValAcrescimoTotal <> 0)
    BEGIN
        Set @nValAux = 0

        SELECT @nValAux       = Sum(ItemPedido.nValAcrescimo)
          FROM #Temp_Pedido Temp
               INNER JOIN ItemPedido ON ItemPedido.nCdPedido = Temp.nCdPedido
         WHERE nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9)
           --AND ItemPedido.nValTotalItem > 0

        SELECT TOP 1 @nCdItemPedido = ItemPedido.nCdItemPedido
          FROM #Temp_Pedido Temp
               INNER JOIN ItemPedido ON ItemPedido.nCdPedido = Temp.nCdPedido
         WHERE nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9)
         ORDER BY ItemPedido.nValAcrescimo

        --
        -- Se o acresimo calculado foi maior que o acrescimo aplicado na venda
        --
        IF (@nValAux > @nValAcrescimoTotal)
        BEGIN
          
            UPDATE ItemPedido
               SET nValAcrescimo  = nValAcrescimo - (@nValAux - @nValAcrescimoTotal)
			 WHERE nCdItemPedido = @nCdItemPedido

			UPDATE ItemPedido
			   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
			 WHERE nCdItemPedido = @nCdItemPedido

        END

        --
        -- Se o acresimo calculado foi menor que o acresimo aplicado na venda
        --
        IF (@nValAux < @nValAcrescimoTotal)
        BEGIN

            UPDATE ItemPedido
               SET nValAcrescimo  = nValAcrescimo + (@nValAcrescimoTotal - @nValAux)
			 WHERE nCdItemPedido = @nCdItemPedido

			UPDATE ItemPedido
			   SET nValTotalItem = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) 
                  ,nValCustoUnit = ((nValUnitario * nQtdePed) - nValDesconto + nValAcrescimo) / nQtdePed   --nValUnitario - nValDesconto + nValAcrescimo
			 WHERE nCdItemPedido = @nCdItemPedido

        END

    END

    --
    -- Atualiza o total do desconto/acrescimo no pedido de venda
    --
    OPEN curPedidos

    FETCH NEXT
     FROM curPedidos
     INTO @nCdPedido
         ,@nValPedidoAnterior

    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        --
        -- atualiza o total do pedido
        --
        UPDATE Pedido
           SET nValPedido = nValPedido + nValDesconto - nValAcrescimo 
         WHERE nCdPedido = @nCdPedido

        --
        -- Soma todos os descontos e acresimos no campo do pedido (sem desconsiderar os desconto/acrescimo da condi��o)
        --
        UPDATE Pedido
           SET nValDesconto  = IsNull((SELECT Sum(nValDesconto)
                                         FROM ItemPedido
                                        WHERE ItemPedido.nCdPedido = Pedido.nCdPedido),0)
              ,nValAcrescimo = IsNull((SELECT Sum(nValAcrescimo)
                                         FROM ItemPedido
                                        WHERE ItemPedido.nCdPedido = Pedido.nCdPedido),0)
         WHERE nCdPedido = @nCdPedido

        --
        -- desconsidera os desconto/acrescimo da condi��o
        --
        UPDATE Pedido
           SET nValDesconto  = CASE WHEN nValDesconto  - nValDescontoCondPagto > 0 THEN nValDesconto  - nValDescontoCondPagto
                                    ELSE 0
                               END
              ,nValAcrescimo = CASE WHEN nValAcrescimo - nValJurosCondPagto > 0 THEN nValAcrescimo - nValJurosCondPagto
                                    ELSE 0
                               END
         WHERE nCdPedido = @nCdPedido

        --
        -- atualiza o total do pedido
        --
        UPDATE Pedido
           SET nValPedido = nValPedido - nValDesconto + nValAcrescimo 
         WHERE nCdPedido = @nCdPedido


		FETCH NEXT
		 FROM curPedidos
		 INTO @nCdPedido
             ,@nValPedidoAnterior

    END

    CLOSE curPedidos
    DEALLOCATE curPedidos

    --
    -- Cursor com os pagamentos
    --
    DECLARE curPagamentos CURSOR
       FAST_FORWARD
        FOR SELECT nCdTemp_Pagto
                  ,#Temp_pagtos.nCdFormaPagto
                  ,nCdCondPagto
                  ,nCdTerceiro
                  ,nValPagto
                  ,cFlgTipo
                  ,iNrCartao
                  ,iNrDocto
                  ,iNrAutorizacao
				  ,cNumeroControle
				  ,cNrDoctoNSU
				  ,cNrAutorizacao
                  ,nCdOperadoraCartao
                  ,nCdTabTipoFormaPagto
                  ,nValDesconto
                  ,nValAcrescimo
                  ,nValEntrada
                  ,cFlgVale
                  ,nValJurosCondPagto
              FROM #Temp_pagtos
                   LEFT JOIN FormaPagto ON FormaPagto.nCdFormaPagto = #Temp_Pagtos.nCdFormaPagto
             ORDER BY cFlgVale DESC --Ordena os vales primeiro para acumular o total geral de vales e abater nos titulos

    OPEN curPagamentos

    FETCH NEXT
     FROM curPagamentos
     INTO @nCdTemp_Pagto
         ,@nCdFormaPagto
         ,@nCdCondPagto
         ,@nCdTerceiro
         ,@nValPagto
         ,@cFlgTipo
         ,@iNrCartao
         ,@iNrDocto
         ,@iNrAutorizacao
		 ,@cNumeroControle
		 ,@cNrDoctoNSU
		 ,@cNrAutorizacao
         ,@nCdOperadoraCartao
         ,@nCdTabTipoFormaPagto
         ,@nValDesconto
         ,@nValAcrescimo
         ,@nValEntrada
         ,@cFlgVale
         ,@nValJurosCondPagto

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        Set @nCdCrediario = NULL

		IF (IsNull(@nCdTerceiro,0) > 0)
		BEGIN

            UPDATE Terceiro
               SET nCdStatus     = 1
                  ,dDtUltNegocio = GetDate()
             WHERE nCdTerceiro   = @nCdTerceiro

		END

        --
        -- Se foi pago em vale (vale que permite liquida��o de d�bitos)
        --
        IF (@cFlgVale = 1) 
        BEGIN

            SELECT @nSaldoVale     = nSaldoVale
                  ,@nCdTabTipoVale = nCdTabTipoVale
              FROM Vale
             WHERE nCdVale = @iNrDocto

            IF (@nSaldoVale <= 0)
            BEGIN

                CLOSE curPagamentos
                DEALLOCATE curPagamentos

                IF (@nCdTabTipoVale = 1) RAISERROR('O saldo do vale %i n�o est� mais dispon�vel.',16,1,@iNrDocto)
                ELSE RAISERROR('O saldo do cart�o presente %i n�o est� mais dispon�vel.',16,1,@iNrDocto) 
                ROLLBACK TRAN
                RETURN

            END
            
            Set @nSaldoValeAbater  = @nSaldoValeAbater + @nSaldoVale
            Set @cFlgValeLiqDebito = 1

            --
            -- Baixa o saldo do vale
            --
            UPDATE Vale
               SET nSaldoVale        = 0
                  ,nCdLanctoFinBaixa = @nCdLanctoFin
                  ,dDtBaixa          = GetDate()
             WHERE nCdVale    = @iNrDocto

        END

		--
		-- Gera os registros de cheques
		--
        IF (@nCdTabTipoFormaPagto = 2)
        BEGIN

			DECLARE curCheques CURSOR FOR
			  SELECT nCdBanco    
					,nCdAgencia  
					,nCdConta    
					,cDigito     
					,iNrCheque   
					,nValCheque  
					,cCNPJCPF    
					,dDtDeposito 
					,dDtLimite   
					,iAutorizacao
				FROM #Temp_Cheque_Pagamento
               WHERE nCdTemp_Pagto = @nCdTemp_Pagto

			OPEN curCheques

			FETCH NEXT
			 FROM curCheques
			 INTO @nCdBanco    
				 ,@nCdAgencia  
				 ,@nCdConta    
				 ,@cDigito     
				 ,@iNrCheque   
				 ,@nValCheque  
				 ,@cCNPJCPF    
				 ,@dDtDeposito 
				 ,@dDtLimite   
				 ,@iAutorizacao

			WHILE (@@FETCH_STATUS = 0)
			BEGIN
				IF (@nCdTerceiro IS NULL)
				BEGIN
					CLOSE curCheques
					DEALLOCATE curCheques
					RAISERROR('Cliente n�o selecionado.',16,1)
					IF (@@TRANCOUNT > 0) ROLLBACK TRAN
					RETURN
				END

				SELECT @cChave = dbo.fn_ChaveCheque(@nCdBanco, @nCdAgencia, @nCdConta, @cDigito, @iNrCheque)

				IF NOT EXISTS(SELECT 1
								FROM Cheque
							   WHERE cChave = @cChave)
				BEGIN

					Set @nCdCheque = NULL

					EXEC usp_ProximoID  'CHEQUE'
									   ,@nCdCheque OUTPUT

					INSERT INTO Cheque (nCdCheque
                                       ,nCdEmpresa  
									   ,nCdBanco    
									   ,cAgencia 
									   ,cConta          
									   ,cDigito 
									   ,cCNPJCPF       
									   ,iNrCheque   
									   ,nValCheque                              
									   ,dDtDeposito 
                                       ,dDtEmissao            
									   ,nCdTabTipoCheque 
									   ,cChave                              
									   ,cFlgCompensado 
									   ,iAutorizacao
									   ,nCdLanctoFin
									   ,nCdContaBancariaDep
                                       ,nCdTerceiroResp)
								 VALUES(@nCdCheque
                                       ,@nCdEmpresa
									   ,@nCdBanco
									   ,@nCdAgencia
									   ,@nCdConta
									   ,@cDigito
									   ,@cCNPJCPF
									   ,@iNrCheque
									   ,@nValCheque
									   ,dbo.fn_OnlyDate(@dDtDeposito)
                                       ,GetDate()
									   ,2
									   ,@cChave
									   ,0
									   ,@iAutorizacao
									   ,@nCdLanctoFin
									   ,@nCdContaBancaria
                                       ,@nCdTerceiro)

				END
				ELSE
				BEGIN
					CLOSE curCheques
					DEALLOCATE curCheques

                    CLOSE curPagamentos
                    DEALLOCATE curPagamentos

					RAISERROR('O Cheque Banco: %i Agencia: %s Conta: %s Cheque: %i j� est� registrado na base de dados de cheques.',16,1,@nCdBanco,@nCdAgencia,@nCdConta,@iNrCheque)
					IF (@@TRANCOUNT > 0) ROLLBACK TRAN
					RETURN

				END

				FETCH NEXT
				 FROM curCheques
				 INTO @nCdBanco    
					 ,@nCdAgencia  
					 ,@nCdConta    
					 ,@cDigito     
					 ,@iNrCheque   
					 ,@nValCheque  
					 ,@cCNPJCPF    
					 ,@dDtDeposito 
					 ,@dDtLimite   
					 ,@iAutorizacao
			END

			CLOSE curCheques
			DEALLOCATE curCheques
	    
        END

		--
		-- Gera os registros das parcelas do crediario
		--
        IF (@nCdTabTipoFormaPagto = 5) AND (@nCdPropostaReneg IS NULL)
        BEGIN

            SELECT @iParcelas = Count(1)
    		  FROM #Temp_Crediario
			 WHERE nCdTemp_Pagto = @nCdTemp_Pagto
               AND nValParcela   > 0

			DECLARE curCrediario CURSOR FOR
				SELECT dDtVencto   
					  ,nValParcela 
					  ,cFlgEntrada
				  FROM #Temp_Crediario
				 WHERE nCdTemp_Pagto = @nCdTemp_Pagto
                   AND nValParcela   > 0
				 ORDER BY dDtVencto

			OPEN curCrediario

			FETCH NEXT
			 FROM curCrediario
			 INTO @dDtVencto
				 ,@nValParcela
				 ,@cFlgEntrada

			Set @iParcela     = 1
			Set @nCdCrediario = NULL

			WHILE (@@FETCH_STATUS = 0)
			BEGIN

				IF (@nCdTerceiro IS NULL)
				BEGIN
					CLOSE curCrediario
					DEALLOCATE curCrediario
					RAISERROR('Cliente n�o selecionado.',16,1)
					IF (@@TRANCOUNT > 0) ROLLBACK TRAN
					RETURN
				END

				IF (@nCdCategFinanc IS NULL)
				BEGIN
					CLOSE curCrediario
					DEALLOCATE curCrediario
                    CLOSE curPagamentos
                    DEALLOCATE curPagamentos
					RAISERROR('Categoria Financeira n�o encontrada para o tipo de pedido',16,1)
					IF (@@TRANCOUNT > 0) ROLLBACK TRAN
					RETURN
				END


				IF (@nCdCrediario IS NULL)
				BEGIN

                    Set @cFlgPrimeiraCompra = 0

                    SELECT @cFlgPrimeiraCompra = 1
                     WHERE NOT EXISTS(SELECT 1
                                        FROM Crediario
                                       WHERE nCdTerceiro = @nCdTerceiro
                                         AND dDtEstorno  IS NULL)
                           
					Set @nCdCrediario = NULL

					EXEC usp_ProximoID  'CREDIARIO'
									   ,@nCdCrediario OUTPUT

					INSERT INTO Crediario (nCdCrediario
                                          ,nCdEmpresa  
										  ,nCdLoja     
										  ,dDtVenda  
										  ,dDtUltVencto              
										  ,nCdTerceiro 
										  ,nValCrediario   
										  ,nSaldo                        
										  ,nCdLanctoFin
										  ,nCdContaBancaria
                                          ,nCdCondPagto
                                          ,nValJuro
                                          ,nValDesconto
                                          ,nValEntrada
                                          ,iParcelas
                                          ,cFlgPrimeiraCompra)
									VALUES(@nCdCrediario
                                          ,@nCdEmpresa
										  ,@nCdLoja
										  ,dbo.fn_OnlyDate(GetDate())
										  ,dbo.fn_OnlyDate(GetDate())
										  ,@nCdTerceiro
										  ,(@nValPagto+@nValEntrada)
										  ,(@nValPagto+@nValEntrada)
										  ,@nCdLanctoFin
										  ,@nCdContaBancaria
                                          ,@nCdCondPagto
                                          ,@nValAcrescimo
                                          ,@nValDesconto
                                          ,@nValEntrada
                                          ,@iParcelas
                                          ,@cFlgPrimeiraCompra)

                    UPDATE #Temp_Pagtos
                       SET nCdCrediario  = @nCdCrediario
                     WHERE nCdTemp_Pagto = @nCdTemp_Pagto

					Set @nCdCrediarioNovo = @nCdCrediario

				END

                --
                -- Consulta a taxa de juros do credi�rio
                --
                SELECT @iDiasCarencia = iDiasCarencia
                      ,@nPercMulta    = nPercMulta
                      ,@nPercJuroDia  = nPercJuroDia
                  FROM TabelaJuros
                 WHERE nCdEmpresa   = @nCdEmpresa
                   AND nCdEspTit    = @nCdEspTitCred
                   AND dDtValidade >= dbo.fn_OnlyDate(GetDate())

                Set @iDiasCarencia = IsNull(@iDiasCarencia,0)
                Set @nPercMulta    = IsNull(@nPercMulta,0)
                Set @nPercJuroDia  = IsNull(@nPercJuroDia,0)

				--
				-- Gera o t�tulo
				--
				Set @nCdTitulo = NULL

				EXEC usp_ProximoID  'TITULO'
								   ,@nCdTitulo OUTPUT

				INSERT INTO Titulo (nCdTitulo   
								   ,nCdEmpresa  
								   ,nCdEspTit   
								   ,cNrTit            
								   ,iParcela    
								   ,nCdTerceiro 
                                   ,nCdCC
								   ,nCdCategFinanc 
								   ,nCdMoeda    
								   ,cSenso 
								   ,dDtEmissao              
								   ,dDtReceb                
								   ,dDtVenc                 
								   ,dDtLiq                  
								   ,dDtCad                  
								   ,dDtCancel               
								   ,nValTit                                 
								   ,nValLiq                                 
								   ,nSaldoTit                               
								   ,nValJuro                                
								   ,nValDesconto                            
								   ,cObsTit                                                                                                                                                                                                                                                          
								   ,nCdBancoPortador 
								   ,dDtRemPortador          
								   ,dDtBloqTit              
								   ,cObsBloqTit                                        
								   ,nCdUnidadeNegocio
								   ,nCdCrediario
								   ,nCdLanctoFin
                                   ,nCdLojaTit
                                   ,nPercTaxaJurosDia                       
                                   ,nPercTaxaMulta                          
                                   ,iDiaCarenciaJuros)
							 VALUES(@nCdTitulo
								   ,@nCdEmpresa
								   ,@nCdEspTitCred
								   ,Convert(VARCHAR(17),@nCdCrediario)
								   ,@iParcela
								   ,@nCdTerceiro
                                   ,@nCdCCReceita
								   ,@nCdCategFinanc
								   ,1 
								   ,'C'
								   ,dbo.fn_OnlyDate(GetDate())
								   ,NULL
								   ,@dDtVencto
								   ,NULL
								   ,GetDate()
								   ,NULL
								   ,@nValParcela
								   ,0
								   ,@nValParcela
								   ,0
								   ,0
								   ,'COND. PAGTO: ' + Convert(VARCHAR(10),@nCdCondPagto) + ' LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + CASE WHEN @cFlgEntrada = 1 THEN ' - PAGAMENTO ENTRADA ' ELSE ' ' END
								   ,NULL
								   ,NULL
								   ,NULL
								   ,NULL
								   ,@nCdUnidadeNegocio
								   ,@nCdCrediario
								   ,@nCdLanctoFin
                                   ,@nCdLoja
                                   ,@nPercJuroDia
                                   ,@nPercMulta
                                   ,@iDiasCarencia)

				Set @nCdCategFinancTitulo = NULL

				EXEC usp_ProximoID  'CATEGFINANCTITULO'
								   ,@nCdCategFinancTitulo OUTPUT

				INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                              ,nCdTitulo
											  ,nCdCategFinanc
											  ,nPercent
											  ,nValor)
										VALUES(@nCdCategFinancTitulo
                                              ,@nCdTitulo
											  ,@nCdCategFinanc
											  ,100
											  ,@nValParcela)
									
			    --
			    -- se par�metro estiver configurado, rateia percentual de participa��o do acr�scimo
			    --	  
                IF ((@nCdCategFinancAcr <> 0) AND (@nValAcrescimo > 0))
                BEGIN
                    
                    SET @nPercAcrescimo = (@nValAcrescimo / (@nValPagto - @nValAcrescimo)) * 100
                     
                    --
                    -- atualiza percentual de participa��o da categoria financeira do t�tulo acima
                    -- e inseri nova categoria financeira sobre o acr�scimo utilizado no pedido
                    --
                    UPDATE CategFinancTitulo
                       SET nPercent = nPercent - @nPercAcrescimo
                          ,nValor   = nValor   - @nValAcrescimo
                     WHERE nCdCategFinancTitulo = @nCdCategFinancTitulo
                    
                    SET @nCdCategFinancTituloAcr = NULL

				    EXEC usp_ProximoID 'CATEGFINANCTITULO'
                                       ,@nCdCategFinancTituloAcr OUTPUT

				    INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                                  ,nCdTitulo
						    					  ,nCdCategFinanc
											      ,nPercent
											      ,nValor)
										    VALUES(@nCdCategFinancTituloAcr
                                                  ,@nCdTitulo
											      ,@nCdCategFinancAcr
											      ,@nPercAcrescimo
                                                  ,@nValAcrescimo)
                    
                END

                Set @nValAcrescimo = 0
                Set @nValDesconto  = 0

				--
				-- Se for parcela de entrada, inseri na temporaria de titulos para ser liquidada
				-- pelo c�digo abaixo como pagamento de parcelas.
				--
				
				IF (@cFlgEntrada = 1)
				BEGIN

					INSERT INTO  #Temp_Titulos (nCdTitulo
											   ,iDiasAtraso
											   ,nValJuro
											   ,nValDesconto
											   ,nValTit
											   ,nSaldoTit
											   ,nValTotal
											   ,cNmEspTit
											   ,dDtVenc
                                               ,cFlgEntrada
                                               ,nCdCrediario
                                               ,iParcela)
										 VALUES(@nCdTitulo
											   ,0
											   ,0
											   ,0
											   ,@nValParcela
											   ,@nValParcela
											   ,@nValParcela
											   ,'ENTRADA'
											   ,GetDate()
                                               ,1
                                               ,@nCdCrediario
                                               ,1)

				END
				

				UPDATE Crediario
				   SET dDtUltVencto = @dDtVencto
				 WHERE nCdCrediario = @nCdCrediario
				   AND dDtUltVencto < @dDtVencto

				FETCH NEXT
				 FROM curCrediario
				 INTO @dDtVencto
					 ,@nValParcela
					 ,@cFlgEntrada

				Set @iParcela = @iParcela + 1

			END

			CLOSE curCrediario
			DEALLOCATE curCrediario

        END

		--
		-- Gera os registros do cartao
		--
        IF (@nCdTabTipoFormaPagto IN (3,4))
        BEGIN

			IF (IsNull(@nCdTerceiro,0) = 0)
			BEGIN

				SELECT @nCdTerceiro = Convert(int,cValor)
				  FROM Parametro
				 WHERE cParametro = 'TERCPDV'

			END

			SELECT @iFloating               = iFloating
				  ,@cFlgDiaUtil             = cFlgDiaUtil
                  ,@cNmOperadoraCartao      = cNmOperadoraCartao
                  ,@nCdGrupoOperadoraCartao = nCdGrupoOperadoraCartao
			  FROM OperadoraCartao
			 WHERE nCdOperadoraCartao = @nCdOperadoraCartao

			Set @dDtCredito = dbo.fn_OnlyDate(GetDate()) + @iFloating

			IF (@cFlgDiaUtil = 1) Set @dDtCredito = dbo.fn_DiaUtil(@dDtCredito,'N')

			Set @nCdTransacaoCartao = NULL

			EXEC usp_ProximoID  'TRANSACAOCARTAO'
							   ,@nCdTransacaoCartao OUTPUT

			INSERT INTO TransacaoCartao (nCdTransacaoCartao
                                        ,dDtTransacao            
										,dDtCredito              
										,iNrCartao            
										,iNrDocto    
										,iNrAutorizacao
										,cNumeroControle 
										,cNrDoctoNSU
										,cNrAutorizacao
										,nCdOperadoraCartao 
										,nCdLanctoFin 
										,nValTransacao                           
										,nCdContaBancaria
                                        ,nCdGrupoOperadoraCartao
                                        ,nCdLojaCartao
                                        ,nCdCondPagtoCartao
                                        ,cFlgGeradoWeb
                                        ,nCdTerceiro)
								  VALUES(@nCdTransacaoCartao
                                        ,dbo.fn_OnlyDate(GetDate())
										,@dDtCredito
										,@iNrCartao
										,@iNrDocto
										,@iNrAutorizacao
										,@cNumeroControle
										,@cNrDoctoNSU
										,@cNrAutorizacao
										,@nCdOperadoraCartao
										,@nCdLanctoFin
										,@nValPagto
										,@nCdContaBancaria
                                        ,@nCdGrupoOperadoraCartao
                                        ,@nCdLoja
                                        ,@nCdCondPagto
                                        ,0
                                        ,@nCdTerceiro)

			SELECT @nCdTransacaoCartao = MAX(nCdTransacaoCartao)
			  FROM TransacaoCartao
			 WHERE nCdLanctoFin = @nCdLanctoFin

            SELECT @iTotalParcelas = iQtdeParcelas
    		  FROM CondPagto
			 WHERE nCdCondPagto = @nCdCondPagto

            SELECT @nCdLojaOperadoraCartao = nCdLojaOperadoraCartao
                  ,@nTaxaOperacaoCartao    = CASE WHEN @iTotalParcelas <= 1 THEN nTaxaOperacao
                                                  ELSE nTaxaOperacaoPrazo
                                             END
              FROM LojaOperadoraCartao
             WHERE nCdOperadoraCartao = @nCdOperadoraCartao
               AND nCdLoja            = @nCdLoja

            --
            -- Verifica se existe uma taxa de juros espec�fica para essa quantidade de parcelas
            --
            IF EXISTS(SELECT 1
                        FROM LojaOperadoraCartaoTaxaJuros
                       WHERE nCdLojaOperadoraCartao = @nCdLojaOperadoraCartao
                         AND iParcelas              = @iTotalParcelas
                         AND nTaxaJuroOperadora     > 0)
            BEGIN

                SELECT @nTaxaOperacaoCartao   = nTaxaJuroOperadora
                  FROM LojaOperadoraCartaoTaxaJuros
                 WHERE nCdLojaOperadoraCartao = @nCdLojaOperadoraCartao
                   AND iParcelas              = @iTotalParcelas

            END

            --
            -- Calcula a Taxa de Operacao
            --
            Set @nValTaxaOperacaoCartao        = 0
            Set @nValTaxaOperacaoAcumulado     = 0
            Set @nValTaxaOperacaoCartaoParcela = 0

            IF (@nTaxaOperacaoCartao > 0)
            BEGIN

                Set @nValTaxaOperacaoCartao        = (@nValPagto * (@nTaxaOperacaoCartao/100))                        -- Taxa para Opera��o
                Set @nValTaxaOperacaoCartaoParcela = @nValTaxaOperacaoCartao / Convert(decimal(12,2),@iTotalParcelas) -- Valor de Opera��o por Parcela

            END

			Set @iParcela = 1

    		Set @nCdTitulo = NULL
	    	Set @nValAux   = 0

			IF (@nCdCategFinanc IS NULL)    Set @nCdCategFinanc = @nCdCategFinancDin
			IF (@nCdUnidadeNegocio IS NULL) Set @nCdUnidadeNegocio = 1

            WHILE (@iParcela <= @iTotalParcelas)
            BEGIN

				--
				-- O Primeiro Vencimento ja foi calculado no bloco de codigo acima
				--
				IF (@nCdTitulo IS NOT NULL)
				BEGIN

					Set @dDtCredito = dbo.fn_OnlyDate(@dDtCredito) + @iFloating

					IF (@cFlgDiaUtil = 1) Set @dDtCredito = dbo.fn_DiaUtil(@dDtCredito,'N')

				END

				Set @nValParcela = ROUND((@nValPagto / Convert(DECIMAL(12,2),@iTotalParcelas)),2)
				Set @nValAux     = @nValAux + @nValParcela

				--
				-- Gera o t�tulo
				--
				Set @nCdTitulo = NULL

				EXEC usp_ProximoID  'TITULO'
								   ,@nCdTitulo OUTPUT

				INSERT INTO Titulo (nCdTitulo   
								   ,nCdEmpresa  
								   ,nCdEspTit   
								   ,cNrTit            
								   ,iParcela    
								   ,nCdTerceiro 
                                   ,nCdCC
								   ,nCdCategFinanc 
								   ,nCdMoeda    
								   ,cSenso 
								   ,dDtEmissao              
								   ,dDtReceb                
								   ,dDtVenc                 
								   ,dDtLiq                  
								   ,dDtCad                  
								   ,dDtCancel               
								   ,nValTit                                 
								   ,nValLiq                                 
								   ,nSaldoTit                               
								   ,nValJuro                                
								   ,nValDesconto                            
								   ,cObsTit                                                                                                                                                                                                                                                          
								   ,nCdBancoPortador 
								   ,dDtRemPortador          
								   ,dDtBloqTit              
								   ,cObsBloqTit                                        
								   ,nCdUnidadeNegocio
								   ,nCdTransacaoCartao
								   ,nCdLanctoFin
                                   ,nCdLojaTit
                                   ,nCdOperadoraCartao
                                   ,nCdGrupoOperadoraCartao)
							 VALUES(@nCdTitulo
								   ,@nCdEmpresa
								   ,@nCdEspTitCartao
								   ,Convert(VARCHAR(10),@iNrDocto)
								   ,@iParcela
								   ,@nCdTerceiro
                                   ,@nCdCCReceita
								   ,@nCdCategFinanc
								   ,1 
								   ,'C'
								   ,dbo.fn_OnlyDate(GetDate())
								   ,NULL
								   ,@dDtCredito
								   ,NULL
								   ,GetDate()
								   ,NULL
								   ,@nValParcela
								   ,0
								   ,@nValParcela
								   ,@nValAcrescimo
								   ,@nValDesconto
								   ,'OPERACAO CARTAO Doc: ' + @cNrDoctoNSU + ' Autoriz.: ' + @cNrAutorizacao + ' Operadora: ' + @cNmOperadoraCartao
								   ,NULL
								   ,NULL
								   ,NULL
								   ,NULL
								   ,@nCdUnidadeNegocio
								   ,@nCdTransacaoCartao
								   ,@nCdLanctoFin
                                   ,@nCdLoja
                                   ,@nCdOperadoraCartao
                                   ,@nCdGrupoOperadoraCartao)

				Set @nCdCategFinancTitulo = NULL

				EXEC usp_ProximoID  'CATEGFINANCTITULO'
								   ,@nCdCategFinancTitulo OUTPUT

				INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                              ,nCdTitulo
											  ,nCdCategFinanc
											  ,nPercent
											  ,nValor)
										VALUES(@nCdCategFinancTitulo
                                              ,@nCdTitulo
											  ,@nCdCategFinanc
											  ,100
											  ,@nValParcela)
									                      
                --
			    -- se par�metro estiver configurado, rateia percentual de participa��o do acr�scimo
			    --	  
                IF ((@nCdCategFinancAcr <> 0) AND (@nValAcrescimo > 0))
                BEGIN
                    
                    SET @nPercAcrescimo = (@nValAcrescimo / (@nValPagto - @nValAcrescimo)) * 100
                     
                    --
                    -- atualiza percentual de participa��o da categoria financeira do t�tulo acima
                    -- e inseri nova categoria financeira sobre o acr�scimo utilizado no pedido
                    --
                    UPDATE CategFinancTitulo
                       SET nPercent = nPercent - @nPercAcrescimo
                          ,nValor   = nValor   - @nValAcrescimo
                     WHERE nCdCategFinancTitulo = @nCdCategFinancTitulo
                    
                    SET @nCdCategFinancTituloAcr = NULL

				    EXEC usp_ProximoID 'CATEGFINANCTITULO'
                                       ,@nCdCategFinancTituloAcr OUTPUT

				    INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                                  ,nCdTitulo
						    					  ,nCdCategFinanc
											      ,nPercent
											      ,nValor)
										    VALUES(@nCdCategFinancTituloAcr
                                                  ,@nCdTitulo
											      ,@nCdCategFinancAcr
											      ,@nPercAcrescimo
                                                  ,@nValAcrescimo)
                    
                END

				--
				-- Gera a movimentacao de desconto
				--
				IF (@nValDesconto > 0)
				BEGIN
					Set @nCdMTitulo = NULL

					EXEC usp_ProximoID  'MTITULO'
									   ,@nCdMTitulo OUTPUT

					INSERT INTO MTitulo (nCdMTitulo
                                        ,nCdTitulo
										,nCdOperacao 
										,nCdFormaPagto 
										,nValMov
										,dDtMov                  
										,dDtCad
										,cObsMov
										,cFlgMovAutomatico
										,nCdLanctoFin
										,nCdContaBancaria)
								  VALUES(@nCdMTitulo
                                        ,@nCdTitulo
										,@nCdOperDescCred
										,NULL
										,@nValDesconto
										,dbo.fn_OnlyDate(GetDate())
										,GetDate()
										,'MOVIMENTO AUTOMATICO'
										,1
										,@nCdLanctoFin
										,@nCdContaBancaria)

				END

				--
				-- Gera a movimentacao de acrescimo
				--
				IF (@nValAcrescimo > 0)
				BEGIN
					Set @nCdMTitulo = NULL

					EXEC usp_ProximoID  'MTITULO'
									   ,@nCdMTitulo OUTPUT

					INSERT INTO MTitulo (nCdMTitulo
                                        ,nCdTitulo
										,nCdOperacao 
										,nCdFormaPagto 
										,nValMov
										,dDtMov                  
										,dDtCad
										,cObsMov
										,cFlgMovAutomatico
										,nCdLanctoFin
										,nCdContaBancaria)
								  VALUES(@nCdMTitulo
                                        ,@nCdTitulo
										,@nCdOperJuroFinCred
										,NULL
										,@nValAcrescimo
										,dbo.fn_OnlyDate(GetDate())
										,GetDate()
										,'MOVIMENTO AUTOMATICO'
										,1
										,@nCdLanctoFin
										,@nCdContaBancaria)

				END

				--
				-- Gera a movimentacao da taxa de opera��o
				--
				IF (@nValTaxaOperacaoCartaoParcela > 0)
				BEGIN
                    Set @nValTaxaOperacaoAcumulado = @nValTaxaOperacaoAcumulado + @nValTaxaOperacaoCartaoParcela

                    UPDATE Titulo
                       SET nValTaxaOperadora = @nValTaxaOperacaoCartaoParcela
                          ,nSaldoTit         = nSaldoTit - @nValTaxaOperacaoCartaoParcela
                     WHERE nCdTitulo         = @nCdTitulo

                    UPDATE CategFinancTitulo
                       SET nValor = nValor - @nValTaxaOperacaoCartaoParcela
                     WHERE nCdTitulo             = @nCdTitulo
                       AND nCdCategFinancTitulo != ISNULL(@nCdCategFinancTituloAcr,0)

					Set @nCdMTitulo = NULL

					EXEC usp_ProximoID  'MTITULO'
									   ,@nCdMTitulo OUTPUT

					INSERT INTO MTitulo (nCdMTitulo
                                        ,nCdTitulo
										,nCdOperacao 
										,nCdFormaPagto 
										,nValMov
										,dDtMov                  
										,dDtCad
										,cObsMov
										,cFlgMovAutomatico
										,nCdLanctoFin
										,nCdContaBancaria)
								  VALUES(@nCdMTitulo
                                        ,@nCdTitulo
										,@nCdOperTaxaCartao
										,NULL
										,@nValTaxaOperacaoCartaoParcela
										,dbo.fn_OnlyDate(GetDate())
										,GetDate()
										,'MOVIMENTO AUTOMATICO'
										,1
										,@nCdLanctoFin
										,@nCdContaBancaria)

				END

                SET @nValAcrescimo = 0
                SET @nValDesconto  = 0 
				SET @iParcela      = @iParcela + 1

			END

            UPDATE TransacaoCartao
               SET iParcelas          = IsNull(@iTotalParcelas,1)
                  ,nValTaxaOperadora  = IsNull(@nValTaxaOperacaoCartao,0)
             WHERE nCdTransacaoCartao = @nCdTransacaoCartao

            --
            -- Ajusta o valor da taxa de operacao, quando necessario
            --
            IF (IsNull(@nValTaxaOperacaoAcumulado,0) <> IsNull(@nValTaxaOperacaoCartao,0))
            BEGIN

                IF (@nValTaxaOperacaoCartao > @nValTaxaOperacaoAcumulado)
                BEGIN

                    SELECT @nCdTitParcela     = nCdTitulo
                      FROM Titulo
                     WHERE nCdTransacaoCartao = @nCdTransacaoCartao
                       AND iParcela           = 1

                    /*UPDATE Titulo
                       SET nValTit           = nValTit           - Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
                          ,nValTaxaOperadora = nValTaxaOperadora - Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
                          ,nSaldoTit         = nSaldoTit         + Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
                     WHERE nCdTitulo         = @nCdTitParcela*/
                     
                    UPDATE Titulo
                       SET nValTit   = nValTit - (@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado)
                     WHERE nCdTitulo = @nCdTitParcela
                     
                    UPDATE Titulo
                       SET nValTaxaOperadora = nValTaxaOperadora + (@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado)
                          ,nSaldoTit         = nValTit           - ((@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado) + @nValTaxaOperacaoCartaoParcela)
                     WHERE nCdTitulo         = @nCdTitParcela

                    UPDATE MTitulo
                       SET nValMov     = nValMov + (@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado)
                     WHERE nCdTitulo   = @nCdTitParcela
                       AND nCdOperacao = @nCdOperTaxaCartao

                    UPDATE CategFinancTitulo
                       SET nValor = nValor + (@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado)
                     WHERE nCdTitulo             = @nCdTitulo
                       AND nCdCategFinancTitulo != ISNULL(@nCdCategFinancTituloAcr,0)

                END

                IF (@nValTaxaOperacaoCartao < @nValTaxaOperacaoAcumulado)
                BEGIN

                    SELECT @nCdTitParcela     = nCdTitulo
                      FROM Titulo
                     WHERE nCdTransacaoCartao = @nCdTransacaoCartao
                       AND iParcela           = 1

                    /*UPDATE Titulo
                       SET nValTit           = nValTit           + Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
                          ,nValTaxaOperadora = nValTaxaOperadora + Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
                          ,nSaldoTit         = nSaldoTit         - (@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao)
                     WHERE nCdTitulo = @nCdTitParcela*/

                    UPDATE Titulo
                       SET nValTit   = nValTit + (@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao)
                     WHERE nCdTitulo = @nCdTitParcela     
                          
                    UPDATE Titulo
                       SET nValTaxaOperadora = nValTaxaOperadora - (@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao)
                          ,nSaldoTit         = nSaldoTit         + ((@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao) + @nValTaxaOperacaoCartaoParcela)
                     WHERE nCdTitulo = @nCdTitParcela

                    UPDATE MTitulo
                       SET nValMov     = nValMov   - (@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao)
                     WHERE nCdTitulo   = @nCdTitParcela
                       AND nCdOperacao = @nCdOperTaxaCartao

                    UPDATE CategFinancTitulo
                       SET nValor = nValor - (@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao)
                     WHERE nCdTitulo = @nCdTitulo
                       AND nCdCategFinancTitulo != ISNULL(@nCdCategFinancTituloAcr,0)

                END

            END

		END

        --
        -- Se teve restricao liberada, gera o registro
        --
        DECLARE curAux CURSOR
            FOR SELECT nCdTabTipoRestricaoVenda
                      ,nCdUsuarioAutor
                      ,dDtAutor
                      ,cJustificativa
                  FROM #Temp_RestricaoVenda
                 WHERE nCdTemp_Pagto = @nCdTemp_Pagto

        OPEN curAux

        FETCH NEXT
         FROM curAux
         INTO @nCdTabTipoRestricaoVenda
             ,@nCdUsuarioAutor
             ,@dDtAutor
             ,@cJustificativa

        WHILE (@@FETCH_STATUS = 0)
        BEGIN

			Set @nCdRestricaoVenda = NULL

			EXEC usp_ProximoID  'RESTRICAOVENDA'
							   ,@nCdRestricaoVenda OUTPUT

			INSERT INTO RestricaoVenda (nCdRestricaoVenda
									   ,nCdLoja     
									   ,nCdTabTipoRestricaoVenda 
									   ,nCdTerceiro 
									   ,nCdLanctoFin 
									   ,nCdCrediario 
									   ,nCdUsuarioAutor 
									   ,dDtAutor                
									   ,cJustificativa)
								 VALUES(@nCdRestricaoVenda
                                       ,@nCdLoja
									   ,@nCdTabTipoRestricaoVenda
									   ,@nCdTerceiro
									   ,@nCdLanctoFin
									   ,@nCdCrediario
									   ,@nCdUsuarioAutor
									   ,@dDtAutor
									   ,@cJustificativa)
 
			FETCH NEXT
			 FROM curAux
			 INTO @nCdTabTipoRestricaoVenda
				 ,@nCdUsuarioAutor
				 ,@dDtAutor
				 ,@cJustificativa

        END

        CLOSE curAux
        DEALLOCATE curAux

        
		--
		-- Processa a renegocia��o, se n�o foi pago a vista gera as parcelas do novo carnet.
		--
        IF (@nCdPropostaReneg IS NOT NULL) AND (@iQtdeParcReneg > 0) AND (@nCdFormaPagto = @nCdFormaPagtoRen)
        BEGIN
            
            --
            -- Gera as parcelas do crediario
            --
            TRUNCATE TABLE #Temp_Crediario

            Set @nValPagto = 0

            --
            -- Se teve entrada, gera a parcela da entrada
            --
            IF (@nValEntradaReneg > 0)
            BEGIN

                INSERT INTO #Temp_Crediario (dDtVencto
                                            ,nValParcela
                                            ,cFlgEntrada
                                            ,nCdTemp_Pagto)
                                      VALUES(dbo.fn_OnlyDate(GetDate())
                                            ,@nValEntradaReneg
                                            ,1
                                            ,@nCdTemp_Pagto)

                Set @nValPagto      = @nValEntradaReneg

            END

            Set @iParcCorrente = 1
            Set @dDtVencto     = dbo.fn_OnlyDate(GetDate())

            WHILE (@iParcCorrente <= @iQtdeParcReneg)
            BEGIN

                Set @dDtVencto     = DateAdd(m,1,@dDtVencto) --Adiciona um m�s na data do vencimento

                INSERT INTO #Temp_Crediario (dDtVencto
                                            ,nValParcela
                                            ,cFlgEntrada
                                            ,nCdTemp_Pagto)
                                      VALUES(@dDtVencto
                                            ,@nValParcReneg
                                            ,0
                                            ,@nCdTemp_Pagto)

                Set @iParcCorrente = @iParcCorrente + 1
                Set @nValPagto     = @nValPagto + @nValParcReneg

            END

            --
            -- Confere se o total das parcelas � igual ao total renegociado
            --
            IF (@nValPagto <> @nValRenegTotal)
            BEGIN

                --
                -- Ajusta na ultima parcela
                --
                IF (@nValPagto < @nValRenegTotal)
                BEGIN
                    UPDATE #Temp_Crediario
                       SET nValParcela   = nValParcela + (@nValRenegTotal - @nValPagto)
                     WHERE nCdTemp_Pagto = @nCdTemp_Pagto
                       AND dDtVencto     = @dDtVencto
                END
                ELSE
                BEGIN
                    UPDATE #Temp_Crediario
                       SET nValParcela   = nValParcela - (@nValPagto - @nValRenegTotal)
                     WHERE nCdTemp_Pagto = @nCdTemp_Pagto
                       AND dDtVencto     = @dDtVencto
                END

            END

            Set @nValPagto = @nValRenegTotal

            SELECT @nCdTerceiro = nCdTerceiro
              FROM PropostaReneg
             WHERE nCdPropostaReneg = @nCdPropostaReneg

			DECLARE curCrediario CURSOR FOR
				SELECT dDtVencto   
					  ,nValParcela 
					  ,cFlgEntrada
				  FROM #Temp_Crediario
				 WHERE nCdTemp_Pagto = @nCdTemp_Pagto
                   AND nValParcela   > 0
				 ORDER BY dDtVencto

			OPEN curCrediario

			FETCH NEXT
			 FROM curCrediario
			 INTO @dDtVencto
				 ,@nValParcela
				 ,@cFlgEntrada

			Set @iParcela     = 1
			Set @nCdCrediario = NULL

			WHILE (@@FETCH_STATUS = 0)
			BEGIN

				IF (@nCdCrediario IS NULL)
				BEGIN

					Set @nCdCrediario = NULL

					EXEC usp_ProximoID  'CREDIARIO'
									   ,@nCdCrediario OUTPUT

					INSERT INTO Crediario (nCdCrediario
                                          ,nCdEmpresa  
										  ,nCdLoja     
										  ,dDtVenda  
										  ,dDtUltVencto              
										  ,nCdTerceiro 
										  ,nValCrediario   
										  ,nSaldo                        
										  ,nCdLanctoFin
										  ,nCdContaBancaria
                                          ,nCdCondPagto
                                          ,nValJuro
                                          ,nValDesconto
                                          ,nValEntrada             
                                          ,cFlgRenegociacao 
                                          ,iParcelas
                                          ,nCdPropostaReneg)
									VALUES(@nCdCrediario
                                          ,@nCdEmpresa
										  ,@nCdLoja
										  ,dbo.fn_OnlyDate(GetDate())
										  ,dbo.fn_OnlyDate(GetDate())
										  ,@nCdTerceiro
										  ,@nValPagto
										  ,@nValPagto
										  ,@nCdLanctoFin
										  ,@nCdContaBancaria
                                          ,@nCdCondPagto
                                          ,@nValAcrescimo
                                          ,@nValDesconto
                                          ,@nValEntrada
                                          ,1
                                          ,(@iQtdeParcReneg+1) --Soma mais uma parcela, a parcela da entrada.
                                          ,@nCdPropostaReneg)

                    UPDATE #Temp_Pagtos
                       SET nCdCrediario  = @nCdCrediario
                     WHERE nCdTemp_Pagto = @nCdTemp_Pagto

					Set @nCdCrediarioNovo = @nCdCrediario

				END

                --
                -- Consulta a taxa de juros do credi�rio de renegociacao
                --
                SELECT @iDiasCarencia = iDiasCarencia
                      ,@nPercMulta    = nPercMulta
                      ,@nPercJuroDia  = nPercJuroDia
                  FROM TabelaJuros
                 WHERE nCdEmpresa   = @nCdEmpresa
                   AND nCdEspTit    = @nCdEspTitCredReneg
                   AND dDtValidade >= dbo.fn_OnlyDate(GetDate())

                Set @iDiasCarencia = IsNull(@iDiasCarencia,0)
                Set @nPercMulta    = IsNull(@nPercMulta,0)
                Set @nPercJuroDia  = IsNull(@nPercJuroDia,0)

                --
                -- Descobre os dados dos titulos renegociados
                --
                Set @nCdCategFinanc    = NULL
                Set @nCdUnidadeNegocio = NULL

                SELECT TOP 1 @nCdCategFinanc    = Titulo.nCdCategFinanc 
                            ,@nCdUnidadeNegocio = Titulo.nCdUnidadeNegocio
                  FROM TituloPropostaReneg
                       INNER JOIN Titulo ON Titulo.nCdTitulo = TituloPropostaReneg.nCdTitulo
                 WHERE TituloPropostaReneg.nCdPropostaReneg = @nCdPropostaReneg
                   
                IF (@nCdUnidadeNegocio IS NULL)
                BEGIN
					CLOSE curCrediario
					DEALLOCATE curCrediario
                    CLOSE curPagamentos
                    DEALLOCATE curPagamentos
					RAISERROR('Nenhum t�tulo vinculado nesta proposta de renegocia��o. Proposta: %i',16,1,@nCdPropostaReneg)
					IF (@@TRANCOUNT > 0) ROLLBACK TRAN
					RETURN
                END

				--
				-- Gera o t�tulo
				--
				Set @nCdTitulo = NULL

				EXEC usp_ProximoID  'TITULO'
								   ,@nCdTitulo OUTPUT

				INSERT INTO Titulo (nCdTitulo   
								   ,nCdEmpresa  
								   ,nCdEspTit   
								   ,cNrTit            
								   ,iParcela    
								   ,nCdTerceiro 
                                   ,nCdCC
								   ,nCdCategFinanc 
								   ,nCdMoeda    
								   ,cSenso 
								   ,dDtEmissao              
								   ,dDtReceb                
								   ,dDtVenc                 
								   ,dDtLiq                  
								   ,dDtCad                  
								   ,dDtCancel               
								   ,nValTit                                 
								   ,nValLiq                                 
								   ,nSaldoTit                               
								   ,nValJuro                                
								   ,nValDesconto                            
								   ,cObsTit                                                                                                                                                                                                                                                          
								   ,nCdBancoPortador 
								   ,dDtRemPortador          
								   ,dDtBloqTit              
								   ,cObsBloqTit                                        
								   ,nCdUnidadeNegocio
								   ,nCdCrediario
								   ,nCdLanctoFin
                                   ,nCdLojaTit
                                   ,nPercTaxaJurosDia                       
                                   ,nPercTaxaMulta                          
                                   ,iDiaCarenciaJuros)
							 VALUES(@nCdTitulo
								   ,@nCdEmpresa
								   ,@nCdEspTitCredReneg
								   ,Convert(VARCHAR(17),@nCdCrediario)
								   ,@iParcela
								   ,@nCdTerceiro
                                   ,@nCdCCReceita
								   ,@nCdCategFinanc
								   ,1 
								   ,'C'
								   ,dbo.fn_OnlyDate(GetDate())
								   ,NULL
								   ,@dDtVencto
								   ,NULL
								   ,GetDate()
								   ,NULL
								   ,@nValParcela
								   ,0
								   ,@nValParcela
								   ,0
								   ,0
								   ,'RENEGOCIA��O' + CASE WHEN @nValEntradaReneg > 0 THEN ' - ENTRADA + ' + Convert(VARCHAR(10),@iQtdeParcReneg) + 'x' 
                                                            ELSE ' ' 
                                                       END + '- LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + CASE WHEN @cFlgEntrada = 1 THEN ' - PAGAMENTO ENTRADA ' ELSE ' ' END
								   ,NULL
								   ,NULL
								   ,NULL
								   ,NULL
								   ,@nCdUnidadeNegocio
								   ,@nCdCrediario
								   ,@nCdLanctoFin
                                   ,@nCdLoja
                                   ,@nPercJuroDia
                                   ,@nPercMulta
                                   ,@iDiasCarencia)

				Set @nCdCategFinancTitulo = NULL

				EXEC usp_ProximoID  'CATEGFINANCTITULO'
								   ,@nCdCategFinancTitulo OUTPUT

				INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                              ,nCdTitulo
											  ,nCdCategFinanc
											  ,nPercent
											  ,nValor)
										VALUES(@nCdCategFinancTitulo
                                              ,@nCdTitulo
											  ,@nCdCategFinanc
											  ,100
											  ,@nValParcela)

                --
			    -- se par�metro estiver configurado, rateia percentual de participa��o do acr�scimo
			    --	  
                IF ((@nCdCategFinancAcr <> 0) AND (@nValAcrescimo > 0))
                BEGIN
                    
                    SET @nPercAcrescimo = (@nValAcrescimo / (@nValPagto - @nValAcrescimo)) * 100
                     
                    --
                    -- atualiza percentual de participa��o da categoria financeira do t�tulo acima
                    -- e inseri nova categoria financeira sobre o acr�scimo utilizado no pedido
                    --
                    UPDATE CategFinancTitulo
                       SET nPercent = nPercent - @nPercAcrescimo
                          ,nValor   = nValor   - @nValAcrescimo
                     WHERE nCdCategFinancTitulo = @nCdCategFinancTitulo
                    
                    SET @nCdCategFinancTituloAcr = NULL

				    EXEC usp_ProximoID 'CATEGFINANCTITULO'
                                       ,@nCdCategFinancTituloAcr OUTPUT

				    INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                                  ,nCdTitulo
						    					  ,nCdCategFinanc
											      ,nPercent
											      ,nValor)
										    VALUES(@nCdCategFinancTituloAcr
                                                  ,@nCdTitulo
											      ,@nCdCategFinancAcr
											      ,@nPercAcrescimo
                                                  ,@nValAcrescimo)
                    
                END

                SET @nValAcrescimo = 0
                SET @nValDesconto  = 0 

				--
				-- Se for parcela de entrada, inseri na temporaria de titulos para ser liquidada
				-- pelo c�digo abaixo como pagamento de parcelas.
				--
				IF (@cFlgEntrada = 1)
				BEGIN

					INSERT INTO  #Temp_Titulos (nCdTitulo
											   ,iDiasAtraso
											   ,nValJuro
											   ,nValDesconto
											   ,nValTit
											   ,nSaldoTit
											   ,nValTotal
											   ,cNmEspTit
											   ,dDtVenc
                                               ,cFlgEntrada
                                               ,nCdCrediario
                                               ,iParcela)
										 VALUES(@nCdTitulo
											   ,0
											   ,0
											   ,0
											   ,@nValParcela
											   ,@nValParcela
											   ,@nValParcela
											   ,'ENTRADA'
											   ,GetDate()
                                               ,1
                                               ,@nCdCrediario
                                               ,1)

				END

				UPDATE Crediario
				   SET dDtUltVencto = @dDtVencto
				 WHERE nCdCrediario = @nCdCrediario
				   AND dDtUltVencto < @dDtVencto

				FETCH NEXT
				 FROM curCrediario
				 INTO @dDtVencto
					 ,@nValParcela
					 ,@cFlgEntrada

				Set @iParcela = @iParcela + 1

			END

			CLOSE curCrediario
			DEALLOCATE curCrediario

        END

		FETCH NEXT
		 FROM curPagamentos
		 INTO @nCdTemp_Pagto
			 ,@nCdFormaPagto
			 ,@nCdCondPagto
			 ,@nCdTerceiro
			 ,@nValPagto
			 ,@cFlgTipo
			 ,@iNrCartao
			 ,@iNrDocto
			 ,@iNrAutorizacao
			 ,@cNumeroControle
			 ,@cNrDoctoNSU
			 ,@cNrAutorizacao
			 ,@nCdOperadoraCartao
			 ,@nCdTabTipoFormaPagto
			 ,@nValDesconto
			 ,@nValAcrescimo
             ,@nValEntrada
             ,@cFlgVale
             ,@nValJurosCondPagto

    END

    CLOSE curPagamentos
    DEALLOCATE curPagamentos

	--
	-- Processa a renegocia��o, abate o t�tulos que participam da renegocia��o e baixa a proposta
	--
    TRUNCATE TABLE #Temp_AtualizaCarteira

    IF (@nCdPropostaReneg IS NOT NULL)
    BEGIN

        --
        -- Efetiva a negocia��o
        --
        UPDATE PropostaReneg
           SET nCdTabStatusPropostaReneg = 3 --Efetivada
              ,nCdLojaEfetiv             = @nCdLoja
              ,nCdUsuarioEfetiv          = @nCdUsuario
              ,dDtEfetiv                 = GetDate()
              ,nCdLanctoFinEfetiv        = @nCdLanctoFin
              ,iParcelasEfetiv           = @iQtdeParcReneg
         WHERE nCdPropostaReneg          = @nCdPropostaReneg  
    
        --
        -- Descobre os valores da proposta
        --
        SELECT @nSaldoDescJuros     = nValDescJuros
              ,@nSaldoDescPrincipal = nValDescOriginal
              ,@nValJurosTotal      = nValJuros
              ,@nValOriginalTotal   = nValOriginal
              ,@dDtPropostaReneg    = dDtProposta
          FROM PropostaReneg
         WHERE nCdPropostaReneg  = @nCdPropostaReneg

        --
        -- Descobre o percentual de desconto que foi dado na proposta de renegocia��o
        --
        Set @nPercDescJuros     = 0
        Set @nPercDescPrincipal = 0
         
        IF (@nValJurosTotal    > 0) Set @nPercDescJuros     = (@nSaldoDescJuros     / @nValJurosTotal)
        IF (@nValOriginalTotal > 0) Set @nPercDescPrincipal = (@nSaldoDescPrincipal / @nValOriginalTotal)

        --
        -- Descobre a quantidade de t�tulos envolvidos na negocia��o
        --
        SELECT @iQtdeTitulos = Count(1)
          FROM TituloPropostaReneg
         WHERE TituloPropostaReneg.nCdPropostaReneg = @nCdPropostaReneg

        DECLARE curTitulos CURSOR
            FOR SELECT TituloPropostaReneg.nCdTitulo
                      ,nCdCrediario

                  FROM TituloPropostaReneg
                       INNER JOIN Titulo ON Titulo.nCdTitulo = TituloPropostaReneg.nCdTitulo
                 WHERE TituloPropostaReneg.nCdPropostaReneg = @nCdPropostaReneg
                 ORDER BY Titulo.dDtVenc

        OPEN curTitulos

        FETCH NEXT
         FROM curTitulos
         INTO @nCdTitulo
             ,@nCdCrediarioReneg


        Set @iQtdeTitulosProc = 0

        WHILE (@@FETCH_STATUS = 0)
        BEGIN
            Set @iQtdeTitulosProc = @iQtdeTitulosProc + 1

            --
            -- Calcula o juros do t�tulo (at� a data de formaliza��o da proposta)
            --
            SELECT @nValJuro       = dbo.fn_SimulaJurosTitulo(Titulo.nCdTitulo
                                                             ,@dDtPropostaReneg)+dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo)
                  ,@nSaldoTitLiq   = nSaldoTit

                  ,@nCdTerceiroTit = nCdTerceiro
              FROM Titulo
             WHERE nCdTitulo = @nCdTitulo

            INSERT INTO #Temp_AtualizaCarteira (nCdTerceiro)
                                         VALUES(@nCdTerceiroTit)

            --
            -- Estima o desconto do t�tulo, de acordo com o 
            --
            Set @nValDescontoJuroTit  = @nValJuro  * @nPercDescJuros
            Set @nValDescontoPrincTit = @nSaldoTitLiq * @nPercDescPrincipal

            --
            -- Se for o �ltimo t�tulo, atualiza o valor do desconto com o saldo de desconto ainda restante da negocia��o
            --
            IF (@iQtdeTitulosProc = @iQtdeTitulos)
            BEGIN
                Set @nValDescontoJuroTit  = @nSaldoDescJuros
                Set @nValDescontoPrincTit = @nSaldoDescPrincipal
            END
            
            IF (@nValJuro > 0)
            BEGIN

                UPDATE Titulo 
                   SET nValJuro  = nValJuro  + @nValJuro 
                      ,nSaldoTit = nSaldoTit + @nValJuro
                 WHERE nCdTitulo = @nCdTitulo


				Set @nCdMTitulo = NULL

				EXEC usp_ProximoID  'MTITULO'
								   ,@nCdMTitulo OUTPUT

				INSERT INTO MTitulo (nCdMTitulo
                                    ,nCdTitulo
									,nCdOperacao 
									,nCdFormaPagto 
									,nValMov
									,dDtMov                  
									,dDtCad
									,cObsMov
									,cFlgMovAutomatico
									,nCdLanctoFin
									,nCdContaBancaria)
							  VALUES(@nCdMTitulo
                                    ,@nCdTitulo
									,@nCdOperJuroCred
									,NULL
									,@nValJuro
									,dbo.fn_OnlyDate(GetDate())
									,GetDate()
									,'JUROS. TITULO RENEGOCIADO. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
									,1
									,@nCdLanctoFin
									,NULL)        

			END

			IF (@nValDescontoPrincTit > 0)
			BEGIN

				UPDATE Titulo 
				   SET nValDesconto = nValDesconto + @nValDescontoPrincTit
                      ,nSaldoTit    = nSaldoTit    - @nValDescontoPrincTit
				 WHERE nCdTitulo    = @nCdTitulo

				Set @nCdMTitulo = NULL

				EXEC usp_ProximoID  'MTITULO'
								   ,@nCdMTitulo OUTPUT

				INSERT INTO MTitulo (nCdMTitulo
                                    ,nCdTitulo
									,nCdOperacao 
									,nCdFormaPagto 
									,nValMov
									,dDtMov                  
									,dDtCad
									,cObsMov
									,cFlgMovAutomatico
									,nCdLanctoFin
									,nCdContaBancaria)
							  VALUES(@nCdMTitulo
                                    ,@nCdTitulo
									,@nCdOperDescCred
									,NULL
									,@nValDescontoPrincTit
									,dbo.fn_OnlyDate(GetDate())
									,GetDate()
									,'DESCONTO. TITULO RENEGOCIADO. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
									,1
									,@nCdLanctoFin
									,NULL)        

			END

            IF (@nValDescontoJuroTit > 0)
            BEGIN
            
				UPDATE Titulo 
				   SET nValDesconto = nValDesconto + @nValDescontoJuroTit
                      ,nSaldoTit    = nSaldoTit    - @nValDescontoJuroTit
				 WHERE nCdTitulo    = @nCdTitulo

				Set @nCdMTitulo = NULL

				EXEC usp_ProximoID  'MTITULO'
								   ,@nCdMTitulo OUTPUT

				INSERT INTO MTitulo (nCdMTitulo
                                    ,nCdTitulo
									,nCdOperacao 
									,nCdFormaPagto 
									,nValMov
									,dDtMov                  
									,dDtCad
									,cObsMov
									,cFlgMovAutomatico
									,nCdLanctoFin
									,nCdContaBancaria)
							  VALUES(@nCdMTitulo
                                    ,@nCdTitulo
									,@nCdOperDescJuroCred
									,NULL
									,CASE WHEN @nCdOperJuroCred = @nCdOperDescJuroCred THEN (@nValDescontoJuroTit * -1)
										  ELSE @nValDescontoJuroTit
									 END --Quando a conta contabil dos juros for a mesma do desconto dos juros, gera com valor negativo para abater na conta do DRE.
									,dbo.fn_OnlyDate(GetDate())
									,GetDate()
									,'DESCONTO SOB JUROS. TITULO RENEGOCIADO. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
									,1
									,@nCdLanctoFin
									,NULL)        

			END

            --
            -- Gera movimento de abatimento do saldo novo do t�tulo
            -- Se foi renegocia��o a vista, n�o gera abatimento, e sim registro de pagamento.
            --
            SELECT @nValTitReneg = nValTit
              FROM Titulo                    
             WHERE nCdTitulo = @nCdTitulo    

            -- Saldo Total � Pagar
            IF (@nSaldoTitLiq > 0)
            BEGIN

                IF (@iQtdeParcReneg > 0)
                BEGIN

					UPDATE Titulo 
					   SET nValAbatimento  = nValAbatimento + @nSaldoTitLiq
						  ,nSaldoTit       = 0
						  ,dDtLiq          = GetDate()
						  ,cFlgRenegociado = 1
					 WHERE nCdTitulo       = @nCdTitulo

					Set @nCdMTitulo = NULL

					EXEC usp_ProximoID  'MTITULO'
									   ,@nCdMTitulo OUTPUT

					INSERT INTO MTitulo (nCdMTitulo
                                        ,nCdTitulo
										,nCdOperacao 
										,nCdFormaPagto 
										,nValMov
										,dDtMov                  
										,dDtCad
										,cObsMov
										,cFlgMovAutomatico
										,nCdLanctoFin
										,nCdContaBancaria)
								  VALUES(@nCdMTitulo
                                        ,@nCdTitulo
										,@nCdOperAbatReneg
										,NULL
										,@nValTitReneg
										,dbo.fn_OnlyDate(GetDate())
										,GetDate()
										,'ABATIMENTO. TITULO RENEGOCIADO. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
										,1
										,@nCdLanctoFin
										,NULL)        

                END
                ELSE
                BEGIN

					UPDATE Titulo 
					   SET nValLiq         = nValLiq + @nSaldoTitLiq + @nValJuro - (ABS(@nValDescontoPrincTit) + ABS(@nValDescontoJuroTit))
						  ,nSaldoTit       = 0
						  ,dDtLiq          = GetDate()
						  ,cFlgRenegociado = 1
					 WHERE nCdTitulo       = @nCdTitulo

					Set @nCdMTitulo = NULL

					EXEC usp_ProximoID  'MTITULO'
									   ,@nCdMTitulo OUTPUT

					INSERT INTO MTitulo (nCdMTitulo
                                        ,nCdTitulo
										,nCdOperacao 
										,nCdFormaPagto 
										,nValMov
										,dDtMov                  
										,dDtCad
										,cObsMov
										,cFlgMovAutomatico
										,nCdLanctoFin
										,nCdContaBancaria)
								  VALUES(@nCdMTitulo
                                        ,@nCdTitulo
										,1
										,NULL
										,@nValTitReneg
										,dbo.fn_OnlyDate(GetDate())
										,GetDate()
										,'TITULO RENEGOCIADO A VISTA. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
										,1
										,@nCdLanctoFin
										,NULL)        

                END

				--
				-- atualiza informa��es do credi�rio renegociado
				--
				UPDATE Crediario
				   SET nValDesconto = nValDesconto + ABS(@nValDescontoPrincTit) + ABS(@nValDescontoJuroTit)
				      ,nValJuro     = nValJuro     + @nValJuro
				      ,nSaldo       = nSaldo       - @nSaldoTitLiq
				 WHERE nCdCrediario = @nCdCrediarioReneg    
				 
				UPDATE Crediario
				   SET dDtLiq       = GETDATE()
				 WHERE nCdCrediario = @nCdCrediarioReneg
				   AND NOT EXISTS (SELECT TOP 1 1
				                     FROM Titulo
			                        WHERE nCdCrediario = @nCdCrediarioReneg
			                          AND dDtLiq IS NULL)			                       
					  
            END

            --
            -- Atualiza o saldo das variaveis
            --
            Set @nSaldoDescJuros     = @nSaldoDescJuros     - @nValDescontoJuroTit
            Set @nSaldoDescPrincipal = @nSaldoDescPrincipal - @nValDescontoPrincTit

			FETCH NEXT
			 FROM curTitulos
			 INTO @nCdTitulo
			     ,@nCdCrediarioReneg

        END

        CLOSE curTitulos
        DEALLOCATE curTitulos
        
        --
        -- Caso algum dos titulos envolvidos na proposta de renegocia��o n�o seja baixado 
        -- n�o permite prosseguir
        --
        
        IF EXISTS(SELECT TOP 1 1 
                    FROM TituloPropostaReneg 
                         INNER JOIN Titulo ON Titulo.nCdTitulo = TituloPropostaReneg.nCdTitulo
                   WHERE nCdPropostaReneg = @nCdPropostaReneg 
                     AND dDtLiq IS NULL )
        BEGIN
		    RAISERROR('Um ou mais titulos n�o foram processados na Proposta %i. Imposs�vel prosseguir.',16,1,@nCdPropostaReneg)
			IF (@@TRANCOUNT > 0) ROLLBACK TRAN
			RETURN
        END

    END

    --
    -- Baixa as parcelas do credi�rio
    --
    DECLARE curTitulos CURSOR FOR
        SELECT nCdTitulo
              ,nValJuro
              ,0
              ,nValTotal -- Valor Total Pago 
              ,cNmEspTit
              ,nValJurosDescontado
          FROM #Temp_Titulos

    OPEN curTitulos

    FETCH NEXT
     FROM curTitulos
     INTO @nCdTitulo
         ,@nValJuro
         ,@nValDesconto
         ,@nSaldoTitLiq

         ,@cNmEspTit
         ,@nValJurosDescontado

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        Set @nCdCrediario = NULL

        SELECT @nCdCrediario   = nCdCrediario
              ,@nCdTerceiroTit = nCdTerceiro
          FROM Titulo
         WHERE nCdTitulo = @nCdTitulo

        INSERT INTO #Temp_AtualizaCarteira (nCdTerceiro)
                                     VALUES(@nCdTerceiroTit)

        --
        -- Gera a movimentacao do titulo de liquidacao
        --
        IF (@nValJuro > 0)
        BEGIN

            UPDATE Titulo 
               SET nValJuro  = nValJuro + @nValJuro 
             WHERE nCdTitulo = @nCdTitulo

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
                                ,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperJuroCred
								,NULL
								,@nValJuro
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
								,'JUROS. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
								,1
								,@nCdLanctoFin
                                ,@nCdContaBancaria)        

        END

        IF (@nValDesconto > 0)
        BEGIN

            UPDATE Titulo 
               SET nValDesconto = nValDesconto + @nValDesconto
             WHERE nCdTitulo    = @nCdTitulo

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
                                ,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperDescCred
								,NULL
								,@nValDesconto
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
								,'DESCONTO. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
								,1
								,@nCdLanctoFin
                                ,NULL)        

        END

        IF (@nValJurosDescontado > 0)
        BEGIN

            UPDATE Titulo 
               SET nValDesconto = nValDesconto + @nValJurosDescontado
             WHERE nCdTitulo    = @nCdTitulo

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
                                ,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperDescJuroCred
								,NULL
								,CASE WHEN @nCdOperJuroCred = @nCdOperDescJuroCred THEN (@nValJurosDescontado * -1)
                                      ELSE @nValJurosDescontado
                                 END --Quando a conta contabil dos juros for a mesma do desconto dos juros, gera com valor negativo para abater na conta do DRE.
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
								,'DESCONTO SOB JUROS. RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
								,1
								,@nCdLanctoFin
                                ,@nCdContaBancaria)        

        END


        --
        -- Se tem vale mercadoria no recebimento e a parcela n�o for parcela de entrada
        -- gera a movimenta��o de abatimento no titulo
        --
        Set @nValorAbatimento = 0

        IF (@nSaldoValeAbater > 0) AND (@cNmEspTit <> 'ENTRADA')
        BEGIN

            IF (@nSaldoValeAbater >= ((@nSaldoTitLiq - @nValJuro - @nValDesconto))) Set @nValorAbatimento = (@nSaldoTitLiq - @nValJuro - @nValDesconto)
            ELSE Set @nValorAbatimento = @nSaldoValeAbater

            Set @nSaldoValeAbater = @nSaldoValeAbater - @nValorAbatimento

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperacaoAbatimento
								,NULL
								,@nValorAbatimento
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
								,CASE WHEN @nCdTabTipoVale = 1 THEN 'ABATIMENTO ATRAVES DE VALE. LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
                                      ELSE 'ABATIMENTO ATRAVES DE CART�O PRESENTE No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
                                 END
								,1
								,@nCdLanctoFin
								,NULL)        

			UPDATE Titulo 
			   SET nValAbatimento = nValAbatimento + @nValorAbatimento
				  ,nSaldoTit      = nSaldoTit      - @nValorAbatimento
			 WHERE nCdTitulo      = @nCdTitulo

			UPDATE Titulo
			   SET dDtLiq     = GetDate()
			 WHERE nCdTitulo  = @nCdTitulo
			   AND nSaldoTit <= 0

        END

        --
        -- Se o saldo do titulo for maior que o saldo do abatimento (se nao teve vale no pagamento, o valor do abatimento � zero)
        -- gera um segundo movimento de liquida��o, para considerar como pagamento.
        --
        IF ((@nSaldoTitLiq + @nValJuro - @nValDesconto) > @nValorAbatimento)
        BEGIN

			Set @nCdMTitulo = NULL
            

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,1
								,NULL
								,((@nSaldoTitLiq - @nValJuro - @nValDesconto + @nValJurosDescontado) - @nValorAbatimento)
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
								,'RECEBIMENTO CAIXA LANCAMENTO No ' + Convert(varchar,@nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.'
								,1
								,@nCdLanctoFin
								,@nCdContaBancaria)        

			UPDATE Titulo 
			   SET nValLiq   = nValLiq   + (@nSaldoTitLiq - @nValorAbatimento)
				  ,nSaldoTit = CASE WHEN (nSaldoTit > @nSaldoTitLiq) -- Em caso de Liquida��o Parcial, atualiza o saldo do t�tulo para o restante a ser pago
				                    THEN nSaldoTit - @nSaldoTitLiq
				                    ELSE nSaldoTit - (@nSaldoTitLiq - @nValorAbatimento - @nValJuro + @nValDesconto + @nValJurosDescontado)
				               END
			 WHERE nCdTitulo = @nCdTitulo

			UPDATE Titulo
			   SET dDtLiq    = GetDate()
			 WHERE nCdTitulo = @nCdTitulo
			   AND nSaldoTit = 0

        END

        --
        -- Se n�o for entrada de crediario, marca o lancamento financeiro como liquidacao de crediario
        --
        IF (@cNmEspTit <> 'ENTRADA')
        BEGIN
			UPDATE LanctoFin
			   SET cFlgLiqCrediario = 1
			 WHERE nCdLanctoFin     = @nCdLanctoFin
        END

        --
        -- Baixa a capa do carnet
        --
        IF (@nCdCrediario IS NOT NULL)
        BEGIN

			UPDATE Crediario
			   SET nValJuro     = nValJuro     + @nValJuro
				  ,nValDesconto = nValDesconto + @nValDesconto + @nValJurosDescontado
				  ,nSaldo       = nSaldo       - (@nSaldoTitLiq - (@nValJuro - @nValJurosDescontado) - @nValDesconto)

			 WHERE nCdCrediario = @nCdCrediario
               AND cIDExterno   IS NULL -- Se n�o foi importado de outro sistema

            UPDATE Crediario 
               SET dDtLiq       = dbo.fn_OnlyDate(GetDate())
             WHERE nCdCrediario = @nCdCrediario
               AND nSaldo      <= 0
               AND cIDExterno  IS NULL -- Se n�o foi importado de outro sistema

        END

        FETCH NEXT
         FROM curTitulos
         INTO @nCdTitulo
			 ,@nValJuro
			 ,@nValDesconto
			 ,@nSaldoTitLiq

             ,@cNmEspTit
             ,@nValJurosDescontado

    END

    CLOSE curTitulos
    DEALLOCATE curTitulos

    --
    -- Gera Vale Mercadoria
    --
        
    IF (@nSaldo < 0)
    BEGIN
    
        --
        -- Quando o saldo � negativo n�o existe terceiro pagador, por isso
        -- pega o c�digo do terceiro que foi selecionado no pedido.
        --
        
        IF (@nCdTerceiroPagto IS NULL) 
            SELECT TOP 1 @nCdTerceiroPagto = nCdTerceiro
              FROM #Temp_Pedido
		     WHERE nCdPedido = @nCdPedido
    
        Set @cFlgValeMercadoria = 'S' 
        
        SELECT @dDtValidadeValeMerc = (CASE WHEN '0' <> dbo.fn_LeParametro('VALIDDEVALEMERC')
                                            THEN CONVERT(int,dbo.fn_OnlyDate(GetDate()), 103) + CONVERT(int,dbo.fn_LeParametro('VALIDDEVALEMERC'))
                                            ELSE NULL
                                       END)

        SELECT @cCodigoVP = cCodigo
          FROM Vale
         WHERE nCdLanctoFinBaixa = @nCdLanctoFin
         
        --
        -- descobre tipo do vale
        --
        SET @iTipoVale = (CASE WHEN @cFlgUtilizaSaldoVP = 'S'                  -- gera vale presente caso o par�metro UTILIZASALDOVP
                                AND EXISTS(SELECT 1                            -- com o valor do pedido negativo
                                             FROM Pedido                       -- e se o valor do vale utilizado no pedido for maior
                                            WHERE nCdLanctoFin = @nCdLanctoFin -- que o valor da devolu��o se existir
                                              AND nValValeMerc > nValDevoluc   -- se n�o, considerar como vale mercadoria
                                              AND nValPedido   < 0)          
                               THEN 2 -- Vale Presente                       
                               ELSE 1 -- Vale Mercadoria
                          END)

        IF (@cFlgTeveDefeito = 1) 
            Set @cFlgValeLiqDebito = 1

		Set @nCdVale = NULL

		EXEC usp_ProximoID  'VALE'
		     			   ,@nCdVale OUTPUT

        INSERT INTO Vale (nCdVale
                         ,dDtVale                 
                         ,nCdTerceiro 
                         ,nCdTabTipoVale 
                         ,nValVale                                
                         ,nSaldoVale  
                         ,dDtValidadeVale                            
                         ,nCdEmpresa  
                         ,nCdLoja     
                         ,nCdLanctoFin
                         ,cFlgLiqVenda
                         ,cCodigo)
                   VALUES(@nCdVale
                         ,GetDate()
                         ,CASE WHEN IsNull(@nCdTerceiroPagto,0) > 0 THEN @nCdTerceiroPagto
                               ELSE @nCdTerceiroPDV
                          END
                         ,@iTipoVale
                         ,ABS(@nSaldo)
                         ,ABS(@nSaldo)
                         ,@dDtValidadeValeMerc
                         ,@nCdEmpresa
                         ,@nCdLoja
                         ,@nCdLanctoFin
                         ,@cFlgValeLiqDebito
                         ,@cCodigoVP)
        
        --
        -- Verifica se n�o utiliza saldo do vale e baixa o mesmo
        --
        IF (@cFlgUtilizaSaldoVP = 'N') OR (@cCodigoVP IS NULL)
        BEGIN                 
            UPDATE Vale
               SET cCodigo      = nCdVale
             WHERE nCdLanctoFin = @nCdLanctoFin
        END            

        SELECT @nCdVale_Out = nCdVale 
          FROM Vale
         WHERE nCdLanctoFin   = @nCdLanctoFin
           AND ((nCdTabTipoVale = 1) OR (nCdTabTipoVale = 2)) -- Mercadoria / Presente
    
    END   

    --
    -- Baixa os cheques resgatados
    --
    DECLARE curChequeResg CURSOR
        FOR SELECT Temp.nCdCheque 
                  ,Temp.nValCheque
                  ,Temp.nValJuros 
                  ,Temp.nValTotal 
                  ,Cheque.nCdTituloCheque
              FROM #Temp_ChequeResgate Temp
                   LEFT JOIN Cheque ON Cheque.nCdCheque = Temp.nCdCheque

    OPEN curChequeResg

    FETCH NEXT
     FROM curChequeResg
     INTO @nCdCheque
         ,@nValCheque
         ,@nValJuro
         ,@nSaldoTitLiq

         ,@nCdTitulo

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        UPDATE LanctoFin
		   SET cFlgLiqCrediario = 1
		 WHERE nCdLanctoFin     = @nCdLanctoFin

        --
        -- Atualiza o status como devolvido para o emissor
        --
        UPDATE Cheque
           SET nCdContaBancariaAntesResg = nCdContaBancariaDep --Armazena a conta bancaria antes do resgate, para poder fazer o estorno do lan�amento
         WHERE nCdCheque                 = @nCdCheque

        UPDATE Cheque
           SET nCdTabStatusCheque  = 7
              ,dDtStatus           = GetDate()
              ,nCdLanctoFinResgate = @nCdLanctoFin
              ,nCdContaBancariaDep = NULL  --Desvincula o cheque do caixa ou do cofre
              ,cFlgCompensado      = 1     --Marca como compensado para debitar a carteira do cliente
         WHERE nCdCheque           = @nCdCheque

		Set @nCdHistStatusCheque = NULL

		EXEC usp_ProximoID  'HISTSTATUSCHEQUE'
						   ,@nCdHistStatusCheque OUTPUT

        INSERT INTO HistStatusCheque (nCdHistStatusCheque
                                     ,nCdCheque
                                     ,nCdTabStatusCheque
                                     ,dDtStatus
                                     ,cOBS)
                               VALUES(@nCdHistStatusCheque
                                     ,@nCdCheque
                                     ,7
                                     ,GetDate()
                                     ,'CHEQUE RESGATADO NO CAIXA. LANCAMENTO FINANCEIRO: ' + Convert(VARCHAR(10), @nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja)  + '  MOVIMENTO AUTOMATICO.')

        --
        -- Se era um cheque devolvido, gera a baixa do titulo
        --
        IF (@nCdTitulo IS NOT NULL)
        BEGIN

            --
            -- Se teve juros no titulo, gera a movimenta��o separada
            --
            IF (@nValJuro > 0)
            BEGIN

				Set @nCdMTitulo = NULL

				EXEC usp_ProximoID  'MTITULO'
								   ,@nCdMTitulo OUTPUT

				INSERT INTO MTitulo (nCdMTitulo
                                    ,nCdTitulo
									,nCdOperacao 
									,nCdFormaPagto 
									,nValMov
									,dDtMov                  
									,dDtCad
									,cObsMov
									,cFlgMovAutomatico
									,nCdLanctoFin
									,nCdContaBancaria)
							  VALUES(@nCdMTitulo
                                    ,@nCdTitulo
									,@nCdOperJuroCred
									,NULL
									,@nValJuro
									,dbo.fn_OnlyDate(GetDate())
									,GetDate()
									,'JUROS. LANCAMENTO FINANCEIRO: ' + Convert(VARCHAR(10), @nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR(10),@nCdLoja) + '  MOVIMENTO AUTOMATICO.'
									,1
									,@nCdLanctoFin
									,@nCdContaBancaria)        

            END

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,1
								,NULL
								,(@nSaldoTitLiq-@nValJuro)
								,dbo.fn_OnlyDate(GetDate())
								,GetDate()
                                ,'RECEBIMENTO CAIXA. LANCAMENTO FINANCEIRO: ' + Convert(VARCHAR(10), @nCdLanctoFin) + ' Loja: ' + Convert(VARCHAR,@nCdLoja) + '  MOVIMENTO AUTOMATICO.'
								,1
								,@nCdLanctoFin
								,@nCdContaBancaria)        

			UPDATE Titulo 
			   SET nValLiq   = nValLiq   + @nSaldoTitLiq
                  ,nValJuro  = nValJuro  + @nValJuro
				  ,nSaldoTit = nSaldoTit - (@nSaldoTitLiq - @nValJuro)
			 WHERE nCdTitulo = @nCdTitulo

			UPDATE Titulo
			   SET dDtLiq    = GetDate()
			 WHERE nCdTitulo = @nCdTitulo
			   AND nSaldoTit = 0

        END

		FETCH NEXT
		 FROM curChequeResg
		 INTO @nCdCheque
			 ,@nValCheque
			 ,@nValJuro
			 ,@nSaldoTitLiq

             ,@nCdTitulo

    END

    CLOSE curChequeResg
    DEALLOCATE curChequeResg

    --
    -- Gera os cart�es presente
    --
    IF (@cUsaCartaoPresente = 'S')
    BEGIN

        DECLARE curAux CURSOR
            FOR SELECT cCodigo
					  ,CASE WHEN IsNull(@nCdTerceiroPagto,0) > 0 THEN @nCdTerceiroPagto
                            ELSE @nCdTerceiroPDV
                       END
					  ,nValor
                  FROM #Temp_ValePresente
                 ORDER BY cCodigo

        OPEN curAux

        FETCH NEXT
         FROM curAux
         INTO @cCodigoValePresente
             ,@nCdTerceiroVale
             ,@nValValePresente

        WHILE (@@FETCH_STATUS = 0)
        BEGIN

			Set @nCdVale = NULL

			EXEC usp_ProximoID  'VALE'
							   ,@nCdVale OUTPUT

			INSERT INTO Vale (nCdVale
							 ,cCodigo
							 ,dDtVale                 
							 ,nCdTerceiro 
							 ,nCdTabTipoVale 
							 ,nValVale                                
							 ,nSaldoVale                              
							 ,nCdEmpresa  
							 ,nCdLoja     
							 ,nCdLanctoFin)
					   VALUES(@nCdVale
                             ,@cCodigoValePresente
							 ,GetDate()
							 ,@nCdTerceiroVale
							 ,2
							 ,@nValValePresente
							 ,@nValValePresente
							 ,@nCdEmpresa
							 ,@nCdLoja
							 ,@nCdLanctoFin)

			FETCH NEXT
			 FROM curAux
			 INTO @cCodigoValePresente
				 ,@nCdTerceiroVale
				 ,@nValValePresente

        END

        CLOSE curAux
        DEALLOCATE curAux

    END

    --
    -- Gera o vale-desconto
    --
    Set @nValValeDesconto = 0

    SELECT @nValValeDesconto = Sum(nValValeDesconto)
      FROM #Temp_Pagtos

    IF (IsNull(@nValValeDesconto,0) > 0)
    BEGIN
        Set @cFlgValeDesconto = 'S' 
		Set @nCdValeDesconto  = NULL

		EXEC usp_ProximoID  'VALE'
						   ,@nCdValeDesconto OUTPUT

		INSERT INTO Vale (nCdVale
						 ,cCodigo
						 ,dDtVale                 
						 ,nCdTerceiro 
						 ,nCdTabTipoVale 
						 ,nValVale                                
						 ,nSaldoVale                              
						 ,nCdEmpresa  
						 ,nCdLoja     
						 ,nCdLanctoFin)
				   VALUES(@nCdValeDesconto
                         ,@nCdValeDesconto
						 ,GetDate()
						 ,@nCdTerceiroVale
						 ,3 --Vale Desconto
						 ,@nValValeDesconto
						 ,@nValValeDesconto
						 ,@nCdEmpresa
						 ,@nCdLoja
						 ,@nCdLanctoFin)

    END

    --
    -- Atualiza a carteira financeira dos clientes movimentados
    --
    DECLARE curCliente CURSOR
        FOR SELECT DISTINCT nCdTerceiro
              FROM #Temp_AtualizaCarteira

    OPEN curCliente

    FETCH NEXT
     FROM curCliente
     INTO @nCdTerceiroTit

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

		UPDATE Terceiro 
		   SET nValCreditoUtil  = IsNull((SELECT Sum(nValTit)   FROM vwLimiteUsadoClienteVarejo vw WHERE vw.nCdTerceiro = Terceiro.nCdTerceiro),0)
			  ,nValRecAtraso    = IsNull((SELECT Sum(nValTit)   FROM vwLimiteUsadoClienteVarejo vw WHERE vw.nCdTerceiro = Terceiro.nCdTerceiro AND vw.iDiasAtraso > 0),0)
			  ,dMaiorRecAtraso  = IsNull((SELECT Min(dDtVencto) FROM vwLimiteUsadoClienteVarejo vw WHERE vw.nCdTerceiro = Terceiro.nCdTerceiro AND vw.iDiasAtraso > 0),0)
			  ,cFlgBloqPedVenda = 0
			  ,nValPedidoAberto = IsNull((SELECT Sum(nValTit) FROM vwLimiteUsadoClienteVarejo vw WHERE vw.nCdTerceiro = Terceiro.nCdTerceiro AND vw.nCdCrediario IS NOT NULL),0)
		 WHERE nCdTerceiro      = @nCdTerceiroTit

		FETCH NEXT
		 FROM curCliente
		 INTO @nCdTerceiroTit
    END

    CLOSE curCliente
    DEALLOCATE curCliente

    Set @nCdLanctoFin_Out    = IsNull(@nCdLanctoFin,0)
    Set @nCdValeDesconto_Out = IsNull(@nCdValeDesconto,0)

END
GO


