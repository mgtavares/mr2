USE [ER2SOFTDEV]
GO
/****** Object:  StoredProcedure [dbo].[SP_ALTERA_CONDICAO_TRANSACAO_CARTAO]    Script Date: 07/11/2019 15:43:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[SP_ALTERA_CONDICAO_TRANSACAO_CARTAO] (@nCdTransacaoCartao int
                                                             ,@nCdCondPagto       int
                                                             ,@nCdOperadoraCartao int
                                                             ,@iNrDocto           int
                                                             ,@iNrAutorizacao     int
															 ,@cNrDoctoNSU        varchar(50)
															 ,@cNrAutorizacao     varchar(12) )
AS

    DECLARE @nCdCategFinancDin             int
           ,@nCdEspTitCartao               int
           ,@iFloating                     int
           ,@nTaxaOperacaoCartao           decimal(12,4)
           ,@iTotalParcelas                int
           ,@nCdOperTaxaCartao             int
           ,@nValTaxaOperacaoCartao        decimal(12,2)
           ,@nValTaxaOperacaoAcumulado     decimal(12,2)
           ,@nValTaxaOperacaoCartaoParcela decimal(12,2)
           ,@nCdTitParcela                 int
           ,@cFlgDiaUtil                   int
           ,@cNmOperadoraCartao            varchar(50)
           ,@nCdGrupoOperadoraCartao       int
           ,@dDtCredito                    datetime
           ,@nCdLoja                       int
           ,@nValPagto                     int
           ,@cOBSTit                       varchar(100)
           ,@nCdTitulo                     int
           ,@nCdContaBancaria              int
           ,@nPercent                      decimal(12,2)
           ,@nValAux                       decimal(12,2)
           ,@nValRecCrediario              decimal(12,2)
           ,@nValJuro                      decimal(12,2)
           ,@nValDesconto                  decimal(12,2)
           ,@nSaldoTit                     decimal(12,2)
           ,@nValTotalPagto                decimal(12,2)
           ,@nValDinheiro                  decimal(12,2)
           ,@nCdCategFinanc                int
           ,@nCdUnidadeNegocio             int
           ,@iParcela                      int
           ,@nValParcela                   decimal(12,2)
           ,@nCdEmpresa                    int
           ,@nCdOperDescCred               int
           ,@nCdOperJuroCred               int
           ,@nCdOperJuroFinCred            int
           ,@nCdTerceiro                   int
           ,@nValAcrescimo                 decimal(12,2)
           ,@nCdLanctoFin                  int
           ,@nCdCategFinancTitulo          int
           ,@nCdMTitulo                    int
           ,@dDtTransacao                  datetime

BEGIN

    SELECT @nCdTerceiro = Convert(int,cValor)
      FROM Parametro
     WHERE cParametro = 'TERCPDV'

    Set @nCdOperTaxaCartao = NULL
    
    SELECT @nCdOperTaxaCartao = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERTAXAOPER'

    IF (@nCdOperTaxaCartao IS NULL)
    BEGIN
        RAISERROR('Parametro OPERTAXAOPER não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdCategFinancDin = NULL
    
    SELECT @nCdCategFinancDin = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CATEGPAD-PDV'

    IF (@nCdCategFinancDin IS NULL)
    BEGIN
        RAISERROR('Parametro CATEGPAD-PDV não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdEspTitCartao = NULL

    SELECT @nCdEspTitCartao = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'CDESPTIT-CARTAO'

    IF (@nCdEspTitCartao IS NULL)
    BEGIN
        RAISERROR('Parametro CDESPTIT-CARTAO não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperJuroCred = NULL

    SELECT @nCdOperJuroCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERJUROCRED'

    IF (@nCdOperJuroCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERJUROCRED não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperJuroFinCred = NULL

    SELECT @nCdOperJuroFinCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERJUROFINCRED'

    IF (@nCdOperJuroFinCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERJUROFINCRED não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END

    Set @nCdOperDescCred = NULL

    SELECT @nCdOperDescCred = Convert(int,cValor)
      FROM Parametro
      WHERE cParametro = 'OPERDESCCRED'

    IF (@nCdOperDescCred IS NULL)
    BEGIN
        RAISERROR('Parametro OPERDESCCRED não configurado ou inválido.',16,1)
        IF (@@TRANCOUNT > 0) ROLLBACK TRAN
        RETURN
    END


	SELECT @iTotalParcelas = iQtdeParcelas
	  FROM CondPagto
	 WHERE nCdCondPagto = @nCdCondPagto

	SELECT @iFloating               = iFloating
		  ,@cFlgDiaUtil             = cFlgDiaUtil
          ,@cNmOperadoraCartao      = cNmOperadoraCartao
		  ,@nCdGrupoOperadoraCartao = nCdGrupoOperadoraCartao
	  FROM OperadoraCartao
	 WHERE nCdOperadoraCartao = @nCdOperadoraCartao


    UPDATE TransacaoCartao
       SET nCdCondPagtoCartao      = @nCdCondPagto
          ,iParcelas               = @iTotalParcelas
          ,nCdOperadoraCartao      = @nCdOperadoraCartao
          ,iNrDocto                = @iNrDocto
          ,iNrAutorizacao          = @iNrAutorizacao
          ,cNrDoctoNSU             = @cNrDoctoNSU
          ,cNrAutorizacao          = @cNrAutorizacao
          ,nCdGrupoOperadoraCartao = @nCdGrupoOperadoraCartao
     WHERE nCdTransacaoCartao      = @nCdTransacaoCartao

    SELECT @nValPagto          = nValTransacao
          ,@nCdContaBancaria   = nCdContaBancaria
          ,@nCdOperadoraCartao = nCdOperadoraCartao
          ,@nCdLoja            = nCdLojaCartao
          ,@dDtTransacao       = dDtTransacao
          ,@dDtCredito         = dDtTransacao--dDtCredito
      FROM TransacaoCartao
     WHERE nCdTransacaoCartao = @nCdTransacaoCartao

	SELECT @nTaxaOperacaoCartao = CASE WHEN @iTotalParcelas <= 1 THEN nTaxaOperacao
									   ELSE nTaxaOperacaoPrazo
								  END
	  FROM LojaOperadoraCartao
	 WHERE nCdOperadoraCartao = @nCdOperadoraCartao
	   AND nCdLoja            = @nCdLoja

	--
	-- Calcula a Taxa de Operacao
	--
	Set @nValTaxaOperacaoCartao        = 0
	Set @nValTaxaOperacaoAcumulado     = 0
	Set @nValTaxaOperacaoCartaoParcela = 0
    Set @nValDesconto                  = 0
    Set @nValAcrescimo                 = 0

	IF (@nTaxaOperacaoCartao > 0)
	BEGIN

		Set @nValTaxaOperacaoCartao        = (@nValPagto * (@nTaxaOperacaoCartao/100))                        -- Taxa para Operação
		Set @nValTaxaOperacaoCartaoParcela = @nValTaxaOperacaoCartao / Convert(decimal(12,2),@iTotalParcelas) -- Valor de Operação por Parcela

	END

    --
    -- Apaga os titulos e movimentos gerados anteriormente
    --
    SELECT TOP 1 @cOBSTit = cOBSTit
          ,@nCdEmpresa    = nCdEmpresa
          ,@nCdTerceiro   = nCdTerceiro
      FROM Titulo
     WHERE nCdTransacaoCartao = @nCdTransacaoCartao
      
    DECLARE curTitulos CURSOR FOR
      SELECT nCdTitulo
        FROM Titulo
       WHERE nCdTransacaoCartao = @nCdTransacaoCartao

    OPEN curTitulos
   
    FETCH NEXT
     FROM curTitulos
     INTO @nCdTitulo

    IF (@nCdCategFinanc IS NULL)    Set @nCdCategFinanc = @nCdCategFinancDin
	IF (@nCdUnidadeNegocio IS NULL) Set @nCdUnidadeNegocio = 1

    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        BEGIN TRY
			DELETE 
			  FROM CategFinancTitulo
			 WHERE nCdTitulo  = @nCdTitulo
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            RAISERROR('Erro excluindo as categorias financeiras dos titulos do cartão. Transacao Cartao: %i    Titulo: %i',16,1,@nCdTransacaoCartao,@nCdTitulo)
        END CATCH
        BEGIN TRY
			DELETE 
			  FROM CentroCustoTitulo
			 WHERE nCdTitulo  = @nCdTitulo
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            RAISERROR('Erro excluindo as centros de custos dos titulos do cartão. Transacao Cartao: %i    Titulo: %i',16,1,@nCdTransacaoCartao,@nCdTitulo)
        END CATCH
        BEGIN TRY
			DELETE 
			  FROM MTitulo
			 WHERE nCdTitulo  = @nCdTitulo
			   AND dDtContab IS NULL
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            RAISERROR('Erro excluindo os movimentos dos titulos do cartão. Transacao Cartao: %i    Titulo: %i',16,1,@nCdTransacaoCartao,@nCdTitulo)
        END CATCH

        BEGIN TRY
			DELETE
			  FROM Titulo
			 WHERE nCdTitulo = @nCdTitulo
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            RAISERROR('Erro excluindo os titulos do cartão. Transacao Cartao: %i    Titulo: %i',16,1,@nCdTransacaoCartao,@nCdTitulo)
        END CATCH

		FETCH NEXT
		 FROM curTitulos
		 INTO @nCdTitulo
    END

    CLOSE curTitulos
    DEALLOCATE curTitulos


	Set @iParcela = 1

    WHILE (@iParcela <= @iTotalParcelas)
    BEGIN

		--
		-- O Primeiro Vencimento ja foi calculado no bloco de codigo acima
		--
		IF (@nCdTitulo IS NOT NULL)
		BEGIN

	       	Set @dDtCredito = dbo.fn_OnlyDate(@dDtCredito) + @iFloating

    		IF (@cFlgDiaUtil = 1) Set @dDtCredito = dbo.fn_DiaUtil(@dDtCredito,'N')

		END

		Set @nValParcela = ROUND((@nValPagto / Convert(DECIMAL(12,2),@iTotalParcelas)),2)
		Set @nValAux     = @nValAux + @nValParcela

		Set @nCdTitulo = NULL

		EXEC usp_ProximoID  'TITULO'
						   ,@nCdTitulo OUTPUT

		--
		-- Gera o título
		--
		INSERT INTO Titulo (nCdTitulo   
						   ,nCdEmpresa  
						   ,nCdEspTit   
						   ,cNrTit            
						   ,iParcela    
						   ,nCdTerceiro 
						   ,nCdCategFinanc 
						   ,nCdMoeda    
						   ,cSenso 
						   ,dDtEmissao              
						   ,dDtReceb                
						   ,dDtVenc                 
						   ,dDtLiq                  
						   ,dDtCad                  
						   ,dDtCancel               
						   ,nValTit                                 
						   ,nValLiq                                 
						   ,nSaldoTit                               
						   ,nValJuro                                
						   ,nValDesconto                            
						   ,cObsTit                                                                                                                                                                                                                                                          
						   ,nCdBancoPortador 
						   ,dDtRemPortador          
						   ,dDtBloqTit              
						   ,cObsBloqTit                                        
						   ,nCdUnidadeNegocio
						   ,nCdTransacaoCartao
						   ,nCdLanctoFin
                           ,nCdLojaTit
                           ,nCdOperadoraCartao
                           ,nCdGrupoOperadoraCartao)
					 VALUES(@nCdTitulo
						   ,@nCdEmpresa
						   ,@nCdEspTitCartao
						   ,Convert(VARCHAR(10),@iNrDocto)
						   ,@iParcela
						   ,@nCdTerceiro
						   ,@nCdCategFinanc
						   ,1 
						   ,'C'
						   ,@dDtTransacao
						   ,NULL
						   ,@dDtCredito
						   ,NULL
						   ,@dDtTransacao
						   ,NULL
						   ,(@nValParcela + @nValDesconto - @nValAcrescimo)
						   ,0
						   ,@nValParcela
						   ,@nValAcrescimo
						   ,@nValDesconto
						   ,'OPERACAO CARTAO Doc: ' + @cNrDoctoNSU + ' Autoriz.: ' + @cNrAutorizacao + ' Operadora: ' + @cNmOperadoraCartao
						   ,NULL
						   ,NULL
						   ,NULL
						   ,NULL
						   ,@nCdUnidadeNegocio
						   ,@nCdTransacaoCartao
						   ,@nCdLanctoFin
                           ,@nCdLoja
                           ,@nCdOperadoraCartao
                           ,@nCdGrupoOperadoraCartao)


		Set @nCdCategFinancTitulo = NULL

		EXEC usp_ProximoID  'CATEGFINANCTITULO'
						   ,@nCdCategFinancTitulo OUTPUT

		INSERT INTO CategFinancTitulo (nCdCategFinancTitulo
                                      ,nCdTitulo
									  ,nCdCategFinanc
									  ,nPercent
									  ,nValor)
								VALUES(@nCdCategFinancTitulo
                                      ,@nCdTitulo
									  ,@nCdCategFinanc
									  ,100
									  ,(@nValParcela + @nValDesconto - @nValAcrescimo))

		--
		-- Gera a movimentacao de desconto
		--
		IF (@nValDesconto > 0)
		BEGIN

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperDescCred
								,NULL
								,@nValDesconto
								,@dDtTransacao
								,@dDtTransacao
								,'MOVIMENTO AUTOMATICO'
								,1
								,@nCdLanctoFin
								,@nCdContaBancaria)

		END

		--
		-- Gera a movimentacao de acrescimo
		--
		IF (@nValAcrescimo > 0)
		BEGIN

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperJuroFinCred
								,NULL
								,@nValAcrescimo
								,@dDtTransacao
								,@dDtTransacao
								,'MOVIMENTO AUTOMATICO'
								,1
								,@nCdLanctoFin
								,@nCdContaBancaria)

		END

		--
		-- Gera a movimentacao da taxa de operação
		--
		IF (@nValTaxaOperacaoCartaoParcela > 0)
		BEGIN
            Set @nValTaxaOperacaoAcumulado = @nValTaxaOperacaoAcumulado + @nValTaxaOperacaoCartaoParcela

            UPDATE Titulo
               SET nValTaxaOperadora = @nValTaxaOperacaoCartaoParcela
                  ,nSaldoTit         = nSaldoTit - @nValTaxaOperacaoCartaoParcela
             WHERE nCdTitulo         = @nCdTitulo


            UPDATE CategFinancTitulo
               SET nValor     = nValor - @nValTaxaOperacaoCartaoParcela
             WHERE nCdTitulo  = @nCdTitulo

			Set @nCdMTitulo = NULL

			EXEC usp_ProximoID  'MTITULO'
							   ,@nCdMTitulo OUTPUT

			INSERT INTO MTitulo (nCdMTitulo
                                ,nCdTitulo
								,nCdOperacao 
								,nCdFormaPagto 
								,nValMov
								,dDtMov                  
								,dDtCad
								,cObsMov
								,cFlgMovAutomatico
								,nCdLanctoFin
								,nCdContaBancaria)
						  VALUES(@nCdMTitulo
                                ,@nCdTitulo
								,@nCdOperTaxaCartao
								,NULL
								,@nValTaxaOperacaoCartaoParcela
								,@dDtTransacao
								,@dDtTransacao
								,'MOVIMENTO AUTOMATICO'
								,1
								,@nCdLanctoFin
								,@nCdContaBancaria)

		END

        Set @nValAcrescimo = 0
        Set @nValDesconto  = 0 

		Set @iParcela = @iParcela + 1

	END

    UPDATE TransacaoCartao
       SET iParcelas          = IsNull(@iTotalParcelas,1)
          ,nValTaxaOperadora  = IsNull(@nValTaxaOperacaoCartao,0)
     WHERE nCdTransacaoCartao = @nCdTransacaoCartao

    --
    -- Ajusta o valor da taxa de operacao, quando necessario
    --
    IF (IsNull(@nValTaxaOperacaoAcumulado,0) <> IsNull(@nValTaxaOperacaoCartao,0))
    BEGIN

        IF (@nValTaxaOperacaoCartao > @nValTaxaOperacaoAcumulado)
        BEGIN

            SELECT @nCdTitParcela     = nCdTitulo
              FROM Titulo
             WHERE nCdTransacaoCartao = @nCdTransacaoCartao
               AND iParcela           = 1

            UPDATE Titulo
               SET nValTit           = nValTit           - Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
                  ,nValTaxaOperadora = nValTaxaOperadora - Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
             WHERE nCdTitulo         = @nCdTitParcela

            UPDATE Titulo
               SET nSaldoTit  = nValTit + nValJuro - nValDesconto - nValTaxaOperadora
             WHERE nCdTitulo  = @nCdTitParcela

            UPDATE MTitulo
               SET nValMov     = nValMov   + Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
             WHERE nCdTitulo   = @nCdTitParcela
               AND nCdOperacao = @nCdOperTaxaCartao

            UPDATE CategFinancTitulo
               SET nValor     = nValor + Convert(DECIMAL(12,2),(@nValTaxaOperacaoCartao - @nValTaxaOperacaoAcumulado))
             WHERE nCdTitulo  = @nCdTitulo

        END

        IF (@nValTaxaOperacaoCartao < @nValTaxaOperacaoAcumulado)
        BEGIN

            SELECT @nCdTitParcela     = nCdTitulo
              FROM Titulo
             WHERE nCdTransacaoCartao = @nCdTransacaoCartao
               AND iParcela           = 1

            UPDATE Titulo
               SET nValTit           = nValTit           + Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
                  ,nValTaxaOperadora = nValTaxaOperadora + Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
             WHERE nCdTitulo = @nCdTitParcela

            UPDATE Titulo
               SET nSaldoTit  = nValTit + nValJuro - nValDesconto - nValTaxaOperadora
             WHERE nCdTitulo  = @nCdTitParcela

            UPDATE MTitulo
               SET nValMov     = nValMov   - Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
             WHERE nCdTitulo   = @nCdTitParcela
               AND nCdOperacao = @nCdOperTaxaCartao

            UPDATE CategFinancTitulo
               SET nValor     = nValor - Convert(DECIMAL(12,2),(@nValTaxaOperacaoAcumulado - @nValTaxaOperacaoCartao))
             WHERE nCdTitulo  = @nCdTitulo

        END
    END
END
