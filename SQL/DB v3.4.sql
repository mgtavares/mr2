USE ER2SOFTDEV;


UPDATE VersaoSistema
   SET dDtFimVersao = GETDATE()
 WHERE dDtFimVersao IS NULL
    
INSERT INTO VersaoSistema(cNmVersaoSistema
                         ,dDtInicioVersao)
                   VALUES('v3.4'
                         ,GETDATE());

ALTER TABLE TransacaoCartao ADD cNrAutorizacao VARCHAR(12)
