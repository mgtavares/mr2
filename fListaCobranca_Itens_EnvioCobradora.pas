unit fListaCobranca_Itens_EnvioCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DB, DBCtrls, ADODB, StdCtrls, cxButtons, Mask;

type
  TfrmListaCobranca_Itens_EnvioCobradora = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    edtCobradora: TMaskEdit;
    cxButton1: TcxButton;
    qryCobradora: TADOQuery;
    qryCobradoranCdCobradora: TIntegerField;
    qryCobradoracNmCobradora: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    SP_REGISTRA_TITULO_COBRADORA: TADOStoredProc;
    procedure edtCobradoraExit(Sender: TObject);
    procedure edtCobradoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobranca : integer ;
    bProcessado      : boolean ;
  end;

var
  frmListaCobranca_Itens_EnvioCobradora: TfrmListaCobranca_Itens_EnvioCobradora;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmListaCobranca_Itens_EnvioCobradora.edtCobradoraExit(
  Sender: TObject);
begin
  inherited;

  qryCobradora.Close;
  PosicionaQuery(qryCobradora, edtCobradora.Text) ;

end;

procedure TfrmListaCobranca_Itens_EnvioCobradora.edtCobradoraKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(199,'cFlgAtivo = 1');

        If (nPK > 0) then
            edtCobradora.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmListaCobranca_Itens_EnvioCobradora.cxButton1Click(
  Sender: TObject);
begin

    if (DBEdit1.Text = '') then
    begin
        MensagemErro('Selecione a cobradora.') ;
        edtCobradora.SetFocus;
        abort ;
    end ;

    if (MessageDlg('Confirma o envio dos t�tulos para ' + Trim(DBEdit1.Text) + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
        exit ;

    {--Executa a procedure que marca os t�tulos como enviados para cobradora --}
    frmMenu.Connection.BeginTrans;

    try
        SP_REGISTRA_TITULO_COBRADORA.Close;
        SP_REGISTRA_TITULO_COBRADORA.Parameters.ParamByName('@nCdListaCobranca').Value := nCdListaCobranca;
        SP_REGISTRA_TITULO_COBRADORA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
        SP_REGISTRA_TITULO_COBRADORA.Parameters.ParamByName('@nCdCobradora').Value     := qryCobradoranCdCobradora.Value;
        SP_REGISTRA_TITULO_COBRADORA.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    ShowMessage('Titulos enviados com sucesso.') ;

    bProcessado := True ;
    close ;

end;

procedure TfrmListaCobranca_Itens_EnvioCobradora.FormShow(Sender: TObject);
begin
  inherited;

  if (nCdListaCobranca <= 0) then
      MensagemErro('Variavel nCdListaCobranca n�o valorizada corretamente.') ;

  bProcessado := False ;
  
  qryCobradora.Close ;
  edtCobradora.Text := '' ;
  edtCobradora.SetFocus ;
end;

procedure TfrmListaCobranca_Itens_EnvioCobradora.ToolButton2Click(
  Sender: TObject);
begin
  bProcessado := False ;
  inherited;

end;

end.
