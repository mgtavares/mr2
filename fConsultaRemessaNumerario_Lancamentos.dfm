inherited frmConsultaRemessaNumerario_Lancamentos: TfrmConsultaRemessaNumerario_Lancamentos
  Left = 269
  Top = 201
  Width = 809
  Height = 350
  BorderIcons = []
  Caption = 'Lan'#231'amentos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 801
    Height = 294
  end
  inherited ToolBar1: TToolBar
    Width = 801
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 801
    Height = 294
    ActivePage = cxTabSheet2
    Align = alClient
    Focusable = False
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 294
    ClientRectRight = 801
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Cheques'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 785
        Height = 255
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsChequeRemessaNum
        FixedColor = clMoneyGreen
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nCdBanco'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Dados dos Cheques|Banco'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 65
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cAgencia'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Dados dos Cheques|Ag'#234'ncia'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 200
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            ReadOnly = True
            Title.Caption = 'Dados dos Cheques|N'#176' Cheque'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 120
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Dados dos Cheques|Terceiro'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 260
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nValCheque'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Dados dos Cheques|Valor'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Comprovante Cart'#227'o'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 801
        Height = 271
        Align = alClient
        AllowedOperations = [alopAppendEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCartaoRemessaNum
        FixedColor = clMoneyGreen
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'dDtTransacao'
            Footers = <>
            ReadOnly = True
            Tag = 1
            Title.Caption = 'Comprovantes Dispon'#237'veis| Data Transa'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iNrDocto'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            ReadOnly = True
            Title.Caption = 'Comprovantes Dispon'#237'veis|N'#250'm. Docto'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 110
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iNrAutorizacao'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            ReadOnly = True
            Title.Caption = 'Comprovantes Dispon'#237'veis|Autoriza'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 110
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cNmOperadoraCartao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Comprovantes Dispon'#237'veis|Operadora'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 260
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iParcelas'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            ReadOnly = True
            Title.Caption = 'Comprovantes Dispon'#237'veis|Parcelas'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 65
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nValTransacao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Comprovantes Dispon'#237'veis|Valor'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Carnet Credi'#225'rio'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 785
        Height = 255
        Align = alClient
        AllowedOperations = [alopAppendEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCrediarioRemessaNum
        FixedColor = clMoneyGreen
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'dDtVenda'
            Footers = <>
            Title.Caption = 'Comprovante de Credi'#225'rio Dispon'#237'veis|Data Venda'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nCdCrediario'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            Title.Caption = 'Comprovante de Credi'#225'rio Dispon'#237'veis|Credi'#225'rio'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 65
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            Title.Caption = 'Comprovante de Credi'#225'rio Dispon'#237'veis|Cliente'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 260
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iParcelas'
            Footers = <
              item
                ValueType = fvtStaticText
              end>
            Title.Caption = 'Comprovante de Credi'#225'rio Dispon'#237'veis|Parcelas'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 65
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nValCrediario'
            Footers = <>
            Title.Caption = 'Comprovante de Credi'#225'rio Dispon'#237'veis|Valor'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 128
    Top = 160
  end
  object dsCrediarioRemessaNum: TDataSource
    DataSet = qryCrediarioRemessaNum
    Left = 96
    Top = 192
  end
  object dsCartaoRemessaNum: TDataSource
    DataSet = qryCartaoRemessaNum
    Left = 64
    Top = 192
  end
  object dsChequeRemessaNum: TDataSource
    DataSet = qryChequeRemessaNum
    Left = 32
    Top = 192
  end
  object qryChequeRemessaNum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRemessaNum'
      '      ,Cheque.nCdBanco'
      '      ,Cheque.cAgencia'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      '      ,cNmTerceiro'
      '  FROM ChequeRemessaNum'
      
        '       INNER JOIN Cheque   ON Cheque.nCdCheque     = ChequeRemes' +
        'saNum.nCdCheque'
      
        '       LEFT  JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdT' +
        'erceiroResp'
      ' WHERE nCdRemessaNum = :nPK'
      ' ORDER BY nCdBanco, cAgencia, cConta, cDigito, iNrCheque')
    Left = 32
    Top = 160
    object qryChequeRemessaNumnCdChequeRemessaNum: TIntegerField
      FieldName = 'nCdRemessaNum'
    end
    object qryChequeRemessaNumnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequeRemessaNumcAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequeRemessaNumiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryChequeRemessaNumnValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequeRemessaNumcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object qryCartaoRemessaNum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRemessaNum'
      '      ,dDtTransacao'
      '      ,OperadoraCartao.cNmOperadoraCartao'
      '      ,TransacaoCartao.iParcelas'
      '      ,TransacaoCartao.iNrDocto'
      '      ,TransacaoCartao.iNrAutorizacao'
      '      ,nValTransacao'
      '  FROM CartaoRemessaNum'
      
        '       INNER JOIN TransacaoCartao ON TransacaoCartao.nCdTransaca' +
        'oCartao = CartaoRemessaNum.nCdTransacaoCartao'
      
        '       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = TransacaoCartao.nCdOperadoraCartao'
      ' WHERE nCdRemessaNum = :nPK'
      ' ORDER BY dDtTransacao')
    Left = 64
    Top = 160
    object qryCartaoRemessaNumdDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryCartaoRemessaNumcNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryCartaoRemessaNumiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryCartaoRemessaNumiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryCartaoRemessaNumiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryCartaoRemessaNumnValTransacao: TBCDField
      FieldName = 'nValTransacao'
      Precision = 12
      Size = 2
    end
  end
  object qryCrediarioRemessaNum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRemessaNum'
      '      ,Crediario.nCdCrediario'
      '      ,dDtVenda'
      '      ,cNmTerceiro'
      '      ,iParcelas'
      '      ,nValCrediario'
      '  FROM CrediarioRemessaNum'
      
        '       INNER JOIN Crediario ON Crediario.nCdCrediario = Crediari' +
        'oRemessaNum.nCdCrediario'
      
        '       INNER JOIN Terceiro  ON Terceiro.nCdTerceiro   = Crediari' +
        'o.nCdTerceiro'
      ' WHERE nCdRemessaNum = :nPK'
      ' ORDER BY dDtVenda')
    Left = 96
    Top = 160
    object qryCrediarioRemessaNumnCdRemessaNum: TIntegerField
      FieldName = 'nCdRemessaNum'
    end
    object qryCrediarioRemessaNumnCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryCrediarioRemessaNumdDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object qryCrediarioRemessaNumcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryCrediarioRemessaNumiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryCrediarioRemessaNumnValCrediario: TBCDField
      FieldName = 'nValCrediario'
      Precision = 12
      Size = 2
    end
  end
end
