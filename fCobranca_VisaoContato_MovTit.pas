unit fCobranca_VisaoContato_MovTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmCobranca_VisaoContato_MovTit = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryMovimentos: TADOQuery;
    dsMovimentos: TDataSource;
    qryMovimentoscNmOperacao: TStringField;
    qryMovimentosdDtMov: TDateTimeField;
    qryMovimentosnValMov: TBCDField;
    qryMovimentoscOBSMov: TMemoField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCobranca_VisaoContato_MovTit: TfrmCobranca_VisaoContato_MovTit;

implementation

{$R *.dfm}

end.
