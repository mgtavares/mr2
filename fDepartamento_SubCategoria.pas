unit fDepartamento_SubCategoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmDepartamento_SubCategoria = class(TfrmProcesso_Padrao)
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    dsSubCategoria: TDataSource;
    DBGridEh1: TDBGridEh;
    qryAux: TADOQuery;
    PopupMenu1: TPopupMenu;
    ExibirSegmentos1: TMenuItem;
    ExibirMarcasVinculadas1: TMenuItem;
    qrySubCategorianCdIdExternoWeb: TIntegerField;
    procedure qrySubCategoriaBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure ExibirSegmentos1Click(Sender: TObject);
    procedure ExibirMarcasVinculadas1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdCategoria : integer ;
  end;

var
  frmDepartamento_SubCategoria: TfrmDepartamento_SubCategoria;

implementation

uses fDepartamento_Segmento, fDepartamento_Marca, fMenu;

{$R *.dfm}

procedure TfrmDepartamento_SubCategoria.qrySubCategoriaBeforePost(
  DataSet: TDataSet);
begin

  if (qrySubCategoriacNmSubCategoria.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da sub-categoria.') ;
      abort ;
  end ;

  qrySubCategoriacNmSubCategoria.Value := Uppercase(qrySubCategoriacNmSubCategoria.Value) ;

  if (qrySubCategoria.State = dsInsert) then
  begin
      qrySubCategorianCdCategoria.Value := nCdCategoria ;
      qrySubCategorianCdStatus.Value    := 1 ;

      qryAux.Close ;
      qryAux.Parameters.ParamByName('cNmSubCategoria').Value := qrySubCategoriacNmSubCategoria.Value ;
      qryAux.Parameters.ParamByName('nCdCategoria').Value    := nCdCategoria ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Sub Categoria j� existente, verifique.') ;
          abort ;
      end ;

      qryAux.Close ;
  end ;

  if (qrySubCategoria.State = dsInsert) then
      qrySubCategorianCdSubCategoria.Value := frmMenu.fnProximoCodigo('SUBCATEGORIA') ;
      
  inherited;


end;

procedure TfrmDepartamento_SubCategoria.FormShow(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, IntToStr(nCdCategoria)) ;

end;

procedure TfrmDepartamento_SubCategoria.ExibirSegmentos1Click(
  Sender: TObject);
var
  objForm : TfrmDepartamento_Segmento;
begin
  inherited;
  if (qrySubCategoria.Active) and (qrySubCategorianCdCategoria.Value > 0) then
  begin

      objForm := TfrmDepartamento_Segmento.Create(nil);

      objForm.nCdSubCategoria := qrySubCategorianCdSubCategoria.Value ;
      showForm(objForm,true) ;
  end ;

end;

procedure TfrmDepartamento_SubCategoria.ExibirMarcasVinculadas1Click(
  Sender: TObject);
var
  objForm : TfrmDepartamento_Marca;
begin
  inherited;

  objForm := TfrmDepartamento_Marca.Create(nil);

  objForm.nCdSubCategoria := qrySubCategorianCdSubCategoria.Value ;
  showForm(objForm,true) ;
  
end;

procedure TfrmDepartamento_SubCategoria.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDepartamento_SubCategoria.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

end.
