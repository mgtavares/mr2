unit fGrupoCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoCredito = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoCredito: TIntegerField;
    qryMastercNmGrupoCredito: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMasternPercLimiteExcesso: TBCDField;
    qryMasteriDiasToleraAtrasoTit: TIntegerField;
    qryMastercFlgAlertaTitAtrasado: TIntegerField;
    qryMastercFlgAlertaChequeDevolv: TIntegerField;
    qryMastercFlgAlertaChequeRisco: TIntegerField;
    qryMastercFlgAlertaLimiteCredito: TIntegerField;
    qryMastercFlgBloquearPedTitAtraso: TIntegerField;
    qryMastercFlgBloquearPedCheqDevolv: TIntegerField;
    qryMastercFlgBloquearPedCheqRisco: TIntegerField;
    qryMastercFlgBloquearPedLimitCred: TIntegerField;
    qryMastercFlgPedidosAnaliseFin: TIntegerField;
    qryMastercFlgAutorAutoPedSemRestri: TIntegerField;
    qryMastercFlgAutorAutoPedidos: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    DBEdit7: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    dsStatus: TDataSource;
    GroupBox1: TGroupBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    qryMasternValLimiteChequeRisco: TBCDField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoCredito: TfrmGrupoCredito;

implementation

{$R *.dfm}

procedure TfrmGrupoCredito.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabelaSistema  := 432;
  cNmTabelaMaster   := 'GRUPOCREDITO';
  nCdConsultaPadrao := 756;
end;

procedure TfrmGrupoCredito.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  {qryMasternValLimiteChequeRisco.Value := 0;
  qryMasteriDiasToleraAtrasoTit.Value  := 0;
  qryMasternPercLimiteExcesso.Value    := 0;
   }
end;

procedure TfrmGrupoCredito.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if (qryMasternCdStatus.Value = 0) then
      qryMasternCdStatus.Value := 1;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmGrupoCredito.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
end;

procedure TfrmGrupoCredito.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmGrupoCredito.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmGrupoCredito.Value = '') then
  begin
      MensagemAlerta('Digite a Descri��o do Grupo de Cr�dito.');
      DBEdit2.SetFocus;
      abort;
  end;

  if (qryMasternPercLimiteExcesso.Value < 0) then
  begin
      MensagemAlerta('O percentual de limite � exceder n�o pode ser inferior a zero.');
      DBEdit4.SetFocus;
      abort;
  end;

  if (qryMasteriDiasToleraAtrasoTit.Value < 0) then
  begin
      MensagemAlerta('A toler�ncia de dias para atraso de titulo deve ser maior ou igual a zero.');
      DBEdit5.SetFocus;
      abort;
  end;

  if (qryMasternValLimiteChequeRisco.Value < 0) then
  begin
      MensagemAlerta('O limite para Cheque de Risco Concentrado n�o pode ser menor que zero.');
      DBEdit6.SetFocus;
      abort;
  end;

  {-- S� � possivel selecionar uma a��o no pedido --}

  if (((DBCheckBox5.Checked) AND (DBCheckBox6.Checked))
      OR ((DBCheckBox5.Checked) AND (DBCheckBox7.Checked))
      OR ((DBCheckBox6.Checked) AND (DBCheckBox7.Checked))) then
  begin
      MensagemAlerta('N�o � poss�vel selecionar mais de uma a��o para o Grupo de Cr�dito.');
      abort;
  end;

  {-- Pelo menos uma a��o deve estar selecionada --}

  if ((not DBCheckBox5.Checked) AND (not DBCheckBox6.Checked) AND (not DBCheckBox7.Checked)) then
  begin
      MensagemAlerta('Pelo menos uma a��o deve estar selecionada para o Grupo de Cr�dito.');
      abort;
  end;

  if (qryMasternCdStatus.Value = 0) then
      qryMasternCdStatus.Value := 1;

  inherited;

end;

initialization
  RegisterClass(TfrmGrupoCredito);

end.
