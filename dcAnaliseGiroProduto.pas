unit dcAnaliseGiroProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TdcmAnaliseGiroProduto = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    edtReferencia: TEdit;
    Label10: TLabel;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DataSource11: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmAnaliseGiroProduto: TdcmAnaliseGiroProduto;

implementation

uses fMenu, fLookup_Padrao, dcAnaliseGiroProduto_view;

{$R *.dfm}

procedure TdcmAnaliseGiroProduto.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TdcmAnaliseGiroProduto.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TdcmAnaliseGiroProduto.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TdcmAnaliseGiroProduto.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TdcmAnaliseGiroProduto.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TdcmAnaliseGiroProduto.ToolButton1Click(Sender: TObject);
var
  cFiltro : string ;
  i : integer ;
  objForm : TdcmAnaliseGiroProduto_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') or (Trim(edtDtFinal.Text) = '/  /') then
  begin
      edtDtFinal.Text   := DateToStr(Date) ;
      edtDtInicial.Text := DateToStr(Date) ;
  end ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;
 
  objForm := TdcmAnaliseGiroProduto_view.Create(nil);

  objForm.ADODataSet1.Close;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
  objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('cReferencia').Value      := edtReferencia.Text ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  {dcmVendaLoja_view.PivotMap1.Measures[3].ColumnPercent := True;}
  {dcmVendaLoja_view.PivotMap1.Measures[3].Value := False;}

  objForm.PivotMap1.Measures[6].ColumnPercent := True;
  objForm.PivotMap1.Measures[6].Value := False;

  objForm.PivotMap1.Measures[7].Rank := True ;
  objForm.PivotMap1.Measures[7].Value := False;

  objForm.PivotGrid1.ColWidths[0] := 500 ;

  objForm.PivotMap1.SortColumn(0,7,False) ;

  objForm.PivotMap1.Title := 'ER2Soft - An�lise de Giro de Produtos' ;
  objForm.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm,true);
  Self.Activate ;

end;

procedure TdcmAnaliseGiroProduto.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TdcmAnaliseGiroProduto.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmAnaliseGiroProduto.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(TdcmAnaliseGiroProduto) ;

end.
