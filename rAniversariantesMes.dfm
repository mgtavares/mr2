inherited rptAniversariantesMes: TrptAniversariantesMes
  Left = 317
  Top = 110
  Caption = 'Aniversariantes do M'#234's'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 17
    Top = 48
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Cadastro'
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [3]
    Left = 88
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdLoja'
    DataSource = dsLoja
    TabOrder = 1
    OnExit = DBEdit1Exit
    OnKeyDown = DBEdit1KeyDown
  end
  object DBEdit2: TDBEdit [4]
    Tag = 1
    Left = 160
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 2
  end
  object GroupMes: TRadioGroup [5]
    Left = 88
    Top = 72
    Width = 729
    Height = 49
    Caption = 'M'#234's'
    Columns = 6
    ItemIndex = 0
    Items.Strings = (
      'Janeiro'
      'Fevereiro'
      'Mar'#231'o'
      'Abril'
      'Maio'
      'Junho'
      'Julho'
      'Agosto'
      'Setembro'
      'Outubro'
      'Novembro'
      'Dezembro')
    TabOrder = 3
  end
  object GroupSexo: TRadioGroup [6]
    Left = 88
    Top = 128
    Width = 369
    Height = 33
    Caption = 'Sexo'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Masculino'
      'Feminino')
    TabOrder = 4
  end
  object rgModeloImp: TRadioGroup [7]
    Left = 88
    Top = 168
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 176
    Top = 240
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja =:nPK')
    Left = 176
    Top = 272
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 208
    Top = 272
  end
  object ER2Excel: TER2Excel
    Titulo = 'Aniversariantes do M'#234's'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 208
    Top = 240
  end
end
