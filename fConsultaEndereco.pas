unit fConsultaEndereco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxControls, cxContainer, cxEdit, cxLabel, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB;

type
  TfrmConsultaEndereco = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtEndereco: TEdit;
    Label1: TLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryConsultaEndereco: TADOQuery;
    edtUF: TEdit;
    Label2: TLabel;
    edtCidade: TEdit;
    Label3: TLabel;
    dsConsultaEndereco: TDataSource;
    qryConsultaEnderecocCidade: TStringField;
    qryConsultaEnderecocBairro: TStringField;
    qryConsultaEnderecocEndereco: TStringField;
    qryConsultaEnderecocCEP: TStringField;
    cxGrid1DBTableView1cCidade: TcxGridDBColumn;
    cxGrid1DBTableView1cBairro: TcxGridDBColumn;
    cxGrid1DBTableView1cEndereco: TcxGridDBColumn;
    cxGrid1DBTableView1cCEP: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtEnderecoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaEndereco: TfrmConsultaEndereco;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmConsultaEndereco.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtUF.Text) = '') then
  begin
      MensagemAlerta('Informe o estado') ;
      edtUF.SetFocus;
      abort ;
  end ;

  qryConsultaEndereco.Close ;
  qryConsultaEndereco.Parameters.ParamByName('cUF').Value       := edtUF.Text;
  qryConsultaEndereco.Parameters.ParamByName('cCidade').Value   := edtCidade.Text ;
  qryConsultaEndereco.Parameters.ParamByName('cEndereco').Value := edtEndereco.Text ;
  qryConsultaEndereco.Open ;

  if (qryConsultaEndereco.Eof) then
  begin
      MensagemAlerta('Nenhum endere�o encontrado para os crit�rios utilizados.') ;
      abort ;
  end
  else cxGrid1.SetFocus ;

end;

procedure TfrmConsultaEndereco.edtEnderecoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

  if (Key = vk_return) then
      ToolButton1.Click ;

end;

procedure TfrmConsultaEndereco.ToolButton2Click(Sender: TObject);
begin

  qryConsultaEndereco.Close ;

  inherited;

end;

procedure TfrmConsultaEndereco.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = vk_return) then
  begin

      if not qryConsultaEndereco.Eof and (qryConsultaEnderecocEndereco.Value <> '') then
      begin
          case MessageDlg('Confirma o endere�o selecionado ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          close ;
      end ;

  end ;

end;

procedure TfrmConsultaEndereco.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  i : integer ;
begin
  {inherited;}

  i := 0 ;

end;

procedure TfrmConsultaEndereco.edtUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //inherited;
  if (Key = vk_return) then
      edtCidade.SetFocus ;

end;

procedure TfrmConsultaEndereco.edtCidadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited;

  if (Key = vk_return) then
      edtEndereco.SetFocus ;

end;

procedure TfrmConsultaEndereco.FormShow(Sender: TObject);
begin
  inherited;

  edtUF.Text       := '' ;
  edtCidade.Text   := '' ;
  edtEndereco.Text := '' ;

  qryConsultaEndereco.Close ;

  edtUF.SetFocus ;
  
end;

procedure TfrmConsultaEndereco.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryConsultaEndereco.Eof and (qryConsultaEnderecocEndereco.Value <> '') then
  begin
      case MessageDlg('Confirma o endere�o selecionado ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      close ;
  end ;

end;

end.
