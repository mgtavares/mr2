inherited frmPrecoEspPedCom: TfrmPrecoEspPedCom
  Left = 171
  Top = 153
  Width = 1040
  Height = 473
  Caption = 'Pre'#231'o Esperado'
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1024
    Height = 406
  end
  inherited ToolBar1: TToolBar
    Width = 1024
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1024
    Height = 406
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsItemPedido
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    ParentShowHint = False
    ShowHint = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemPedido'
        Footer.FieldName = 'nCdItemPedido'
        Footer.ValueType = fvtCount
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <
          item
            FieldName = 'nCdProduto'
          end>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        ReadOnly = True
        Width = 606
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePed'
        Footer.FieldName = 'nQtdePed'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nValUnitarioEsp'
        Footer.FieldName = 'nValUnitarioEsp'
        Footer.ValueType = fvtSum
        Footers = <>
        Tag = 1
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'nValTotalEsp'
        Footer.FieldName = 'nValTotalEsp'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Width = 80
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 432
    Top = 176
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemPedidoBeforePost
    OnCalcFields = qryItemPedidoCalcFields
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,ItemPedido.nCdProduto'
      '      ,CASE WHEN cNmItem IS NOT NULL THEN cNmItem'
      '            ELSE cNmProduto'
      '       END cNmItem'
      '      ,nQtdePed'
      '      ,nValUnitarioEsp'
      '      ,cFlgComplemento'
      '      ,nValUnitario'
      '      ,nQtdePed * nValUnitarioEsp as nValTotalEsp'
      '  FROM ItemPedido'
      
        '       LEFT JOIN Produto ON Produto.nCdProduto = ItemPedido.nCdP' +
        'roduto'
      ' WHERE nCdPedido = :nPK'
      '   AND nCdItemPedidoPai IS NULL')
    Left = 400
    Top = 144
    object qryItemPedidonCdItemPedido: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidonCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtdePed: TBCDField
      DisplayLabel = 'Pedido|Quantidade'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidocFlgComplemento: TIntegerField
      FieldName = 'cFlgComplemento'
    end
    object qryItemPedidonValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemPedidonValUnitarioEsp: TFloatField
      DisplayLabel = 'Valor Esperado|Unit'#225'rio Esp.'
      FieldName = 'nValUnitarioEsp'
    end
    object qryItemPedidonValTotalEsp: TFloatField
      DisplayLabel = 'Valor Esperado|Total Esp.'
      FieldKind = fkCalculated
      FieldName = 'nValTotalEsp'
      Calculated = True
    end
  end
  object dsItemPedido: TDataSource
    DataSet = qryItemPedido
    Left = 400
    Top = 176
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 432
    Top = 144
  end
end
