unit fTituloProv_Duplicar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBClient, GridsEh, DBGridEh, ADODB, DBGridEhGrouping;

type
  TfrmTituloProv_Duplicar = class(TfrmProcesso_Padrao)
    cdsVenctos: TClientDataSet;
    cdsVenctosdDtVenc: TDateField;
    cdsVenctosnValTit: TFloatField;
    dsVenctos: TDataSource;
    DBGridEh1: TDBGridEh;
    SP_DUPLICA_TITULO_PROV: TADOStoredProc;
    procedure cdsVenctosBeforePost(DataSet: TDataSet);
    procedure cdsVenctosAfterInsert(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nValTit : double ;
    nCdTitulo : integer ;
  end;

var
  frmTituloProv_Duplicar: TfrmTituloProv_Duplicar;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmTituloProv_Duplicar.cdsVenctosBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (cdsVenctosdDtVenc.AsString = '') then
  begin
      MensagemAlerta('Informe a data de vencimento.') ;
      abort ;
  end ;

  if (cdsVenctosdDtVenc.Value <= StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('A data de vencimento n�o pode ser menor ou igual a hoje.') ;
      abort ;
  end ;

  if (cdsVenctosnValTit.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da previs�o') ;
      abort ;
  end ;

end;

procedure TfrmTituloProv_Duplicar.cdsVenctosAfterInsert(DataSet: TDataSet);
begin
  inherited;

  cdsVenctosnValTit.Value := nValTit ;
  
end;

procedure TfrmTituloProv_Duplicar.FormShow(Sender: TObject);
begin
  inherited;

  cdsVenctos.Close ;
  cdsVenctos.Open ;

  while not cdsVenctos.eof do
  begin
      cdsVenctos.Delete;
      cdsVenctos.Next ;
  end ;

  cdsVenctos.Close ;
  cdsVenctos.Open ;
  
end;

procedure TfrmTituloProv_Duplicar.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (cdsVenctos.RecordCount <= 0) then
  begin
      MensagemAlerta('Nenhum vencimento informado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o das previs�es de pagamentos para os vencimentos informados ?' +#13#13 + 'Este processo n�o poder� ser estornado.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      cdsVenctos.First ;

      while not cdsVenctos.Eof do
      begin
          SP_DUPLICA_TITULO_PROV.Close ;
          SP_DUPLICA_TITULO_PROV.Parameters.ParamByName('@nCdTitulo').Value := nCdTitulo ;
          SP_DUPLICA_TITULO_PROV.Parameters.ParamByName('@dDtVenc').Value   := cdsVenctosdDtVenc.AsString ;
          SP_DUPLICA_TITULO_PROV.Parameters.ParamByName('@nValTit').Value   := cdsVenctosnValTit.Value ;
          SP_DUPLICA_TITULO_PROV.ExecProc;
          cdsVenctos.Next ;
      end ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('T�tulos gerados com sucesso.') ;

  close ;

end;

end.
