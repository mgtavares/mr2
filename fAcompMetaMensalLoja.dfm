inherited frmAcompMetaMensalLoja: TfrmAcompMetaMensalLoja
  Left = 88
  Top = 0
  Width = 1229
  Height = 725
  Caption = 'Acompanhamento Meta Mensal - por Loja'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 118
    Width = 1213
    Height = 539
  end
  inherited ToolBar1: TToolBar
    Width = 1213
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1213
    Height = 89
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 50
      Top = 20
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 68
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Meta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 70
      Top = 44
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object edtMesAno: TMaskEdit
      Left = 96
      Top = 60
      Width = 65
      Height = 21
      EditMask = '99/9999'
      MaxLength = 7
      TabOrder = 2
      Text = '  /    '
      OnChange = edtMesAnoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 168
      Top = 36
      Width = 385
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 4
    end
    object cxButton1: TcxButton
      Left = 168
      Top = 59
      Width = 105
      Height = 25
      Caption = 'Exibir Meta'
      TabOrder = 3
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object edtEmpresa: TMaskEdit
      Left = 96
      Top = 12
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 0
      Text = '        '
      OnChange = edtEmpresaChange
      OnExit = edtEmpresaExit
      OnKeyDown = edtEmpresaKeyDown
    end
    object edtLoja: TMaskEdit
      Left = 96
      Top = 36
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 1
      Text = '        '
      OnChange = edtLojaChange
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 168
      Top = 12
      Width = 385
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 5
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 118
    Width = 1213
    Height = 539
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 535
    ClientRectLeft = 4
    ClientRectRight = 1209
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'An'#225'lise Semanal'
      ImageIndex = 0
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 0
        Width = 1205
        Height = 249
        ActivePage = cxTabSheet3
        Align = alTop
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        ClientRectBottom = 245
        ClientRectLeft = 4
        ClientRectRight = 1201
        ClientRectTop = 24
        object cxTabSheet3: TcxTabSheet
          Caption = 'Resumo por Semana'
          ImageIndex = 0
          object DBGridEh1: TDBGridEh
            Left = 0
            Top = 0
            Width = 1197
            Height = 221
            Align = alClient
            AllowedOperations = []
            DataGrouping.GroupLevels = <>
            DataSource = dsResumoSemana
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Tahoma'
            FooterFont.Style = []
            FooterRowCount = 1
            IndicatorOptions = [gioShowRowIndicatorEh]
            SumList.Active = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = DBGridEh1DblClick
            Columns = <
              item
                EditButtons = <>
                FieldName = 'nCdStatusMeta'
                Footers = <>
                ImageList = ImageList2
                KeyList.Strings = (
                  '0'
                  '1'
                  '2')
                ReadOnly = True
                Title.Caption = ' '
                Width = 27
              end
              item
                EditButtons = <>
                FieldName = 'iSemana'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clWindow
                Footer.Font.Height = -11
                Footer.Font.Name = 'Tahoma'
                Footer.Font.Style = []
                Footers = <>
                ReadOnly = True
                Width = 49
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaMinimaReal'
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaReal'
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                EditButtons = <>
                FieldName = 'nValTotalVenda'
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                EditButtons = <>
                FieldName = 'nValDiferenca'
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                DisplayFormat = '#,##0.00'
                EditButtons = <>
                FieldName = 'nValAcumulado'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clWhite
                Footer.Font.Height = -11
                Footer.Font.Name = 'Tahoma'
                Footer.Font.Style = []
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaMinimaAting'
                Footers = <>
                ReadOnly = True
                Width = 127
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaAting'
                Footers = <>
                ReadOnly = True
                Width = 127
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
      object cxPageControl3: TcxPageControl
        Left = 0
        Top = 249
        Width = 1205
        Height = 262
        ActivePage = cxTabSheet4
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 258
        ClientRectLeft = 4
        ClientRectRight = 1201
        ClientRectTop = 24
        object cxTabSheet4: TcxTabSheet
          Caption = 'Resumo por Vendedor'
          ImageIndex = 0
          object DBGridEh2: TDBGridEh
            Left = 0
            Top = 0
            Width = 1197
            Height = 234
            Align = alClient
            AllowedOperations = []
            DataGrouping.GroupLevels = <>
            DataSource = dsResumoVendedor
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Tahoma'
            FooterFont.Style = []
            FooterRowCount = 1
            IndicatorOptions = [gioShowRowIndicatorEh]
            SumList.Active = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = DBGridEh2DblClick
            Columns = <
              item
                EditButtons = <>
                FieldName = 'nCdStatusMeta'
                Footers = <>
                ImageList = ImageList2
                KeyList.Strings = (
                  '0'
                  '1'
                  '2')
                ReadOnly = True
                Title.Caption = ' '
                Width = 27
              end
              item
                EditButtons = <>
                FieldName = 'iSemana'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clWindow
                Footer.Font.Height = -11
                Footer.Font.Name = 'Tahoma'
                Footer.Font.Style = []
                Footers = <>
                ReadOnly = True
                Width = 48
              end
              item
                EditButtons = <>
                FieldName = 'nCdVendedor'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clWindow
                Footer.Font.Height = -11
                Footer.Font.Name = 'Tahoma'
                Footer.Font.Style = []
                Footers = <>
                ReadOnly = True
                Width = 49
              end
              item
                EditButtons = <>
                FieldName = 'cNmVendedor'
                Footers = <>
                ReadOnly = True
                Width = 166
              end
              item
                EditButtons = <>
                FieldName = 'iTotalHorasPrev'
                Footers = <>
                ReadOnly = True
                Width = 55
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaMinima'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMeta'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'iTotalHorasReal'
                Footers = <>
                ReadOnly = True
                Width = 55
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaMinimaReal'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaReal'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValTotalVenda'
                Footers = <>
                ReadOnly = True
                Width = 87
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaMinimaAting'
                Footers = <>
                ReadOnly = True
                Width = 71
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaAting'
                Footers = <>
                ReadOnly = True
                Width = 73
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'An'#225'lise Mensal'
      ImageIndex = 1
      object cxPageControl4: TcxPageControl
        Left = 0
        Top = 0
        Width = 1205
        Height = 511
        ActivePage = cxTabSheet5
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        ClientRectBottom = 507
        ClientRectLeft = 4
        ClientRectRight = 1201
        ClientRectTop = 24
        object cxTabSheet5: TcxTabSheet
          Caption = 'Resumo por Vendedor'
          ImageIndex = 0
          object DBGridEh3: TDBGridEh
            Left = 0
            Top = 0
            Width = 1197
            Height = 483
            Align = alClient
            AllowedOperations = []
            DataGrouping.GroupLevels = <>
            DataSource = dsResumoVendedorMes
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Tahoma'
            FooterFont.Style = []
            FooterRowCount = 1
            IndicatorOptions = [gioShowRowIndicatorEh]
            SumList.Active = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = DBGridEh3DblClick
            Columns = <
              item
                EditButtons = <>
                FieldName = 'nCdStatusMeta'
                Footer.FieldName = 'nCdStatusMeta'
                Footers = <>
                ImageList = ImageList2
                KeyList.Strings = (
                  '0'
                  '1'
                  '2')
                ReadOnly = True
                Title.Caption = ' '
                Width = 26
              end
              item
                EditButtons = <>
                FieldName = 'nCdVendedor'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clWindow
                Footer.Font.Height = -11
                Footer.Font.Name = 'Tahoma'
                Footer.Font.Style = []
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'cNmVendedor'
                Footers = <>
                ReadOnly = True
                Width = 198
              end
              item
                EditButtons = <>
                FieldName = 'iTotalHorasPrev'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'iTotalHorasReal'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaMinima'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaMinimaReal'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMeta'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nValMetaReal'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaReal'
                Footers = <>
                ReadOnly = True
                Width = 79
              end
              item
                EditButtons = <>
                FieldName = 'nValTotalVenda'
                Footers = <>
                ReadOnly = True
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaMinimaAting'
                Footers = <>
                ReadOnly = True
                Width = 75
              end
              item
                EditButtons = <>
                FieldName = 'nPercMetaAting'
                Footers = <>
                ReadOnly = True
                Width = 76
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
    object cxTabSheet9: TcxTabSheet
      Caption = 'An'#225'lise Gr'#225'fica'
      Enabled = False
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 938
        Top = 0
        Width = 267
        Height = 511
        Align = alRight
        Caption = 'Op'#231#245'es de Gr'#225'fico'
        TabOrder = 0
        object cxButton2: TcxButton
          Left = 80
          Top = 272
          Width = 105
          Height = 25
          Caption = '&Exibir Gr'#225'fico'
          TabOrder = 0
          OnClick = cxButton2Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF874480833F7E7F387D7C337C792E7BC63A13C63712C4
            3112C02E11C02A10BF270F7C4F257A4A2177461E75421B733F19413DF3999BFE
            9698FE9395FE9193FEC94315E0935DDF8F59DF8D55DE8A51C02E11A2C99E9DC6
            9998C39594C091276D2C4A47F59EA0FE5B5EFE5659FE9597FECD4C16E19763DA
            8041D97D3BDF8F58C63512A8CDA477B07170AB6A9AC4962E76335351F6A2A4FE
            6266FE5D61FE999BFECF5518E39C6CDC864ADB8344E0945EC63C13AFD1AB80B7
            7A79B273A0C89C35803C5B5BF8A8AAFE6B6FFE6569FE9EA0FED1601AE5A274DE
            8C53DD894DE29966C94515B5D5B08ABD8383B87DA7CDA33D8B446364FAACAFFE
            7276FE6D71FEA2A4FED6691EE7A77BE0925CDF8F56E49E6ECD4E16BAD9B5B7D6
            B1B3D3AEAED1A945964D6A6CFCB0B3FE797EFE7579FEA8AAFED8742AE8AD83E2
            9865E19560E5A376CF571860BC6C5CB66757B06152A85C4DA1557073FDB4B7FE
            8185FE7C81FEACAFFED97B39EAB28AE49F6EE39C6AE7A87DD1621AFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF7478FEB8BBFEB6B9FEB3B6FEB0B3FEDF8445ECB792E6
            A578E6A273E9AD85D66B1EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7478FE7478FE
            7276FE6E71FD6A6CFCE18D52EDBB99E9AC81E7A87CEAB38DD8742CFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2925CEEC1A0EA
            B189E9AE85EDB794DB7C3AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFE69B67F0C5A6ECB691EBB38CEEBC9BDF8547FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8A070F2C8ACF0
            C6A8F0C5A6EFC2A2E18E53FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFEAA677E8A474E8A06EE69D6BE49965E2955EFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object GroupBox4: TGroupBox
          Left = 2
          Top = 15
          Width = 263
          Height = 250
          Align = alTop
          Caption = 'Tipo de An'#225'lise'
          TabOrder = 1
          object RadioGroup1: TRadioGroup
            Left = 2
            Top = 15
            Width = 259
            Height = 65
            Align = alTop
            ItemIndex = 0
            Items.Strings = (
              'Semanal'
              'Semanal X Vendedor'
              'Mensal X Vendedor')
            TabOrder = 0
            OnClick = RadioGroup1Click
          end
          object RadioGroup2: TRadioGroup
            Left = 2
            Top = 80
            Width = 259
            Height = 168
            Align = alClient
            Caption = 'Semana'
            Enabled = False
            TabOrder = 1
          end
        end
        object edtNumPage: TEdit
          Left = 118
          Top = 306
          Width = 33
          Height = 21
          Enabled = False
          TabOrder = 2
        end
        object btnAnterior: TcxButton
          Left = 8
          Top = 304
          Width = 98
          Height = 25
          Caption = 'P'#225'gina &Anterior'
          Enabled = False
          TabOrder = 3
          OnClick = btnAnteriorClick
          LookAndFeel.NativeStyle = True
        end
        object btnProximo: TcxButton
          Left = 161
          Top = 304
          Width = 98
          Height = 25
          Caption = 'Pr'#243'&xima P'#225'gina'
          Enabled = False
          TabOrder = 4
          OnClick = btnProximoClick
          LookAndFeel.NativeStyle = True
        end
      end
      object DBChart2: TDBChart
        Left = 0
        Top = 0
        Width = 938
        Height = 511
        BackWall.Brush.Color = clWhite
        Title.Text.Strings = (
          '  ')
        MaxPointsPerPage = 6
        View3DOptions.Elevation = 327
        View3DOptions.HorizOffset = -19
        View3DOptions.Perspective = 10
        View3DOptions.Rotation = 358
        View3DOptions.VertOffset = 12
        Align = alClient
        TabOrder = 1
      end
    end
  end
  object GroupBox2: TGroupBox [4]
    Left = 0
    Top = 657
    Width = 1213
    Height = 30
    Align = alBottom
    TabOrder = 3
    object Image2: TImage
      Left = 16
      Top = 9
      Width = 17
      Height = 17
      Center = True
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        000010000000010018000000000000030000C40E0000C40E0000000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFD4DFE64F575C454646404242464D50C9D6DFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF838E95BCBCBBEBEAEACDCCCCA3A19F
        5A656DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF79868CA6A5A2A8A2A29D9998948F8B525960FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFF3EDE366839798A19A69918664887B817F71
        2E4458F4EFE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F4EED8C7
        A6CFBA93D4C19ED5C2A0D4C19DD7C5A4C3A876C9B185F4EFE5FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF9F6F0D7C6A6E8DECBD9C8A8DDCEB3DDCEB2DDCEB2DDCEB2
        D2BE9AE5D9C4C6AC7DF1EADEFFFFFFFFFFFFFFFFFFFEFEFEDED0B4E8DECBF7F3
        ED91AEB3DDCEB3DBCBAED8C7A7DDCEB27FA1A7F0EADEE9DFCDC6AC7EF9F7F2FF
        FFFFFFFFFFECE4D5E1D4BBFBF9F6DFD1B68DA9B0DDCEB3DDCEB3DACAACDDCEB2
        7799A1D2BE9AF7F3EDD9C8A8DBCBADFFFFFFFFFFFFDECFB4F2ECE1F5F0E8DFD1
        B7E2D5BEA7BDC3ACC0C5A9BFC49CB4BBE0D3BAD7C5A5E4D8C1F2ECE1CAB286FF
        FFFFFFFFFFDECFB4F7F3EDF2ECE2F3EEE4F5F1E9F4EFE6F3EEE5F2ECE2F1EBE0
        EFE8DAE5DAC5DDCEB2F8F5EFC7AE81FFFFFFFFFFFFE3D8C1F3EEE5F6F2ECF7F3
        EDFBF9F6FBF9F6FBF9F6FBF9F6FAF8F4F8F5EFEEE7D9EAE1D0F2ECE1CCB48BFF
        FFFFFFFFFFF1EBE0EDE5D6F9F7F2F6F2ECF9F7F2FBFAF7FEFEFDFEFEFDFBF9F6
        F4EFE6EDE5D6FBF9F6D9C8A8E0D1B8FFFFFFFFFFFFFEFEFDECE3D4F0EADEF8F5
        EFF7F3EDF5F1E9F3EEE5F2ECE2F2ECE1F5F1E9FAF8F4E3D7C0D1BD98FCFAF8FF
        FFFFFFFFFFFFFFFFFDFCFBF0E9DDEDE5D6F2ECE1F5F1E9F8F5EFF8F5F0F5F1E9
        EDE5D6D9C8A9DAC9AAF9F6F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F5
        EFEFE8DCEAE0CEE4D8C2E0D2B9DED0B6E1D3BAEDE5D7FFFFFFFFFFFFFFFFFFFF
        FFFF}
    end
    object Image3: TImage
      Left = 168
      Top = 9
      Width = 17
      Height = 17
      Center = True
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        000010000000010018000000000000030000C40E0000C40E0000000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFD4DFE64F575C454646404242464D50C9D6DFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF838E95BCBCBBEBEAEACDCCCCA3A19F
        5A656DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF79868CA6A5A2A8A2A29D9998948F8B525960FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFDBE5EC6683977A95A33A8A98357F8C606E76
        2E4458DFE8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F1FB70AF
        E1469DE64BBEF747E6FD41E5FD51C3FB167CDE3D88D3D5E8F7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFE6F3FB69B3E9A6D3F365AEF074E1F673E1F672E0F671E0F6
        4CA3EC9CC3EF2C81D7C9E0F5FFFFFFFFFFFFFFFFFFFDFEFF7FC0ECA5D4F3DCFA
        FE38A1EB74E1F66AE4F65DE2F572E0F61691E8C0F5FDACCEF12D83D7EAF3FBFF
        FFFFFFFFFFB7DDF58BC8EFECFCFE77E1F72F99EA75E1F674E1F668DEF573E1F6
        0986E646D5F3DCFEFE6FAAE579B3E6FFFFFFFFFFFF7FC5EEC9E9F9D4F9FD7CE3
        F786E5F860B1EF68B5EF63B4EF4CA6EC82E4F759DCF58AEBFACBE2F7398FDAFF
        FFFFFFFFFF7EC6EEDFF6FDC8F5FCCDF6FCD6F7FDD3F4FCCFF2FCCAF1FBC4F0FC
        BAF2FB96EAF872E5F7E2F4FD3289D8FFFFFFFFFFFF95D2F2D2EFFBDBF9FEDFF9
        FDECFBFEEEFCFEEFFCFEEFFCFEEBFBFEE0F9FEB8F1FBA8F1FBCBE5F83E95DDFF
        FFFFFFFFFFC8E9F9B4E3F8E5FAFEDBF8FDE4FAFEF0FCFEF9FEFFF9FEFFEFFCFE
        D2F6FDB4F1FBEDFDFF6BB3EA88C2ECFFFFFFFFFFFFFCFEFFB5E2F6C3EBFAE2F9
        FDE0F9FDD5F7FDCFF6FDC9F4FCC7F4FCD6F9FDEBFAFE90CAF250A9E6F3F9FDFF
        FFFFFFFFFFFFFFFFF7FCFEC4E7F9B7E4F8C7ECFBD7F3FCE1F7FDE2F8FED8F0FC
        B6DFF86BBBED6CB9EBE8F4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2F5
        FBC1E6F8A8DDF694D2F185CCF080C8F087CBF0BBE1F6FFFFFFFFFFFFFFFFFFFF
        FFFF}
    end
    object Image4: TImage
      Left = 328
      Top = 9
      Width = 17
      Height = 17
      Center = True
      Picture.Data = {
        07544269746D617036030000424D360300000000000036000000280000001000
        000010000000010018000000000000030000C40E0000C40E0000000000000000
        0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFF7FAF7F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E
        4754A35C4F9F57337D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE
        897BCC8776CA8176C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF9FCFA59B0636BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C
        36803CF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A7
        5CB2D6B59CC9A05CAD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFF
        FFFFFFFFFFD9EFDC6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD89
        7CCD8756A55F38823EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFF
        FFFFFFFFFFFFFFFFFFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E
        60B06A81CF8D7FCF8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609F
        C4A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9ACA9F63B36D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF}
    end
    object Label4: TLabel
      Tag = 1
      Left = 42
      Top = 11
      Width = 102
      Height = 11
      Caption = 'Abaixo Meta M'#237'nima'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Tag = 1
      Left = 194
      Top = 11
      Width = 63
      Height = 11
      Caption = 'Abaixo Meta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Tag = 1
      Left = 354
      Top = 11
      Width = 70
      Height = 11
      Caption = 'Meta Atingida'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited ImageList1: TImageList
    Left = 512
    Top = 256
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 7
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa  '
      '  FROM Empresa'
      ' WHERE nCdEmpresa =:nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE '
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa '
      '                 AND UE.nCdUsuario = :nCdUsuario)')
    Left = 328
    Top = 257
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE UL.nCdUsuario = :nCdUsuario'
      '                  AND UL.nCdLoja    = Loja.nCdLoja) ')
    Left = 364
    Top = 257
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 328
    Top = 288
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 364
    Top = 288
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      '    IF (OBJECT_ID('#39'tempdb..#TempResumoSemana'#39') IS NULL)'#13#10'    BEG' +
      'IN'#13#10#13#10'        CREATE TABLE #TempResumoSemana(nCdMetaSemana      ' +
      '  int           default 0 not null'#13#10'                            ' +
      '          ,iSemana              int           default 0 not null' +
      #13#10'                                      ,iTotalHorasPrev      de' +
      'cimal(12,2) default 0 not null'#13#10'                                ' +
      '      ,iTotalHorasReal      decimal(12,2) default 0 not null'#13#10'  ' +
      '                                    ,nValMetaMinima       decima' +
      'l(12,2) default 0 not null'#13#10'                                    ' +
      '  ,nValMetaMinimaReal   decimal(12,2) default 0 not null'#13#10'      ' +
      '                                ,nValMeta             decimal(12' +
      ',2) default 0 not null'#13#10'                                      ,n' +
      'ValMetaReal         decimal(12,2) default 0 not null'#13#10'          ' +
      '                            ,nPercMetaReal        decimal(12,2) ' +
      'default 0 not null'#13#10'                                      ,nValT' +
      'otalVenda       decimal(12,2) default 0 not null'#13#10'              ' +
      '                        ,nPercMetaMinimaAting decimal(12,2) defa' +
      'ult 0 not null'#13#10'                                      ,nPercMeta' +
      'Ating       decimal(12,2) default 0 not null'#13#10'                  ' +
      '                    ,nValAcumulado        decimal(12,2) default ' +
      '0 not null)'#13#10#13#10'    END'#13#10#13#10#13#10'    IF (OBJECT_ID('#39'tempdb..#TempResu' +
      'moVendedor'#39') IS NULL)'#13#10'    BEGIN'#13#10#13#10'        CREATE TABLE #TempRe' +
      'sumoVendedor(nCdMetaSemana        int                     not nu' +
      'll'#13#10'                                        ,nCdVendedor        ' +
      '  int                     not null'#13#10'                            ' +
      '            ,cNmVendedor          varchar(50)'#13#10'                 ' +
      '                       ,iTotalHorasPrev      decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                        ,iTotalHo' +
      'rasReal      decimal(12,2) default 0 not null'#13#10'                 ' +
      '                       ,nValMetaMinima       decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                        ,nValMeta' +
      'MinimaReal   decimal(12,2) default 0 not null'#13#10'                 ' +
      '                       ,nValMeta             decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                        ,nValMeta' +
      'Real         decimal(12,2) default 0 not null'#13#10'                 ' +
      '                       ,nPercMetaReal        decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                        ,nValTota' +
      'lVenda       decimal(12,2) default 0 not null'#13#10'                 ' +
      '                       ,nPercMetaMinimaAting decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                        ,nPercMet' +
      'aAting       decimal(12,2) default 0 not null)'#13#10#13#10'    END'#13#10#13#10'   ' +
      ' IF (OBJECT_ID('#39'tempdb..#TempResumoVendedorMes'#39') IS NULL)'#13#10'    B' +
      'EGIN'#13#10#13#10'        CREATE TABLE #TempResumoVendedorMes(nCdVendedor ' +
      '         int                     not null'#13#10'                     ' +
      '                      ,cNmVendedor          varchar(50)'#13#10'       ' +
      '                                    ,iTotalHorasPrev      decima' +
      'l(12,2) default 0 not null'#13#10'                                    ' +
      '       ,iTotalHorasReal      decimal(12,2) default 0 not null'#13#10' ' +
      '                                          ,nValMetaMinima       ' +
      'decimal(12,2) default 0 not null'#13#10'                              ' +
      '             ,nValMetaMinimaReal   decimal(12,2) default 0 not n' +
      'ull'#13#10'                                           ,nValMeta       ' +
      '      decimal(12,2) default 0 not null'#13#10'                        ' +
      '                   ,nValMetaReal         decimal(12,2) default 0' +
      ' not null'#13#10'                                           ,nPercMeta' +
      'Real        decimal(12,2) default 0 not null'#13#10'                  ' +
      '                         ,nValTotalVenda       decimal(12,2) def' +
      'ault 0 not null'#13#10'                                           ,nPe' +
      'rcMetaMinimaAting decimal(12,2) default 0 not null'#13#10'            ' +
      '                               ,nPercMetaAting       decimal(12,' +
      '2) default 0 not null)'#13#10#13#10'    END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 616
    Top = 254
  end
  object qryResumoSemana: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryResumoSemanaAfterScroll
    OnCalcFields = qryResumoSemanaCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempResumoSemana')
    Left = 584
    Top = 254
    object qryResumoSemanaiSemana: TIntegerField
      DisplayLabel = 'Semana'
      FieldName = 'iSemana'
    end
    object qryResumoSemanaiTotalHorasPrev: TBCDField
      DisplayLabel = 'Horas Venda|Previsto'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemanaiTotalHorasReal: TBCDField
      DisplayLabel = 'Horas Venda|Realizado'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValMetaMinima: TBCDField
      DisplayLabel = 'Meta M'#237'nima|Previsto'
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValMetaMinimaReal: TBCDField
      DisplayLabel = 'Meta M'#237'nima'
      FieldName = 'nValMetaMinimaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValMeta: TBCDField
      DisplayLabel = 'Meta|Previsto'
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValMetaReal: TBCDField
      DisplayLabel = 'Meta'
      FieldName = 'nValMetaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananPercMetaReal: TBCDField
      DisplayLabel = 'Meta|% Realizado'
      FieldName = 'nPercMetaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValTotalVenda: TBCDField
      DisplayLabel = 'Total de Vendas'
      FieldName = 'nValTotalVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananPercMetaMinimaAting: TBCDField
      DisplayLabel = '% Atingida|Meta M'#237'nima'
      FieldName = 'nPercMetaMinimaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananPercMetaAting: TBCDField
      DisplayLabel = '% Atingida|Meta'
      FieldName = 'nPercMetaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryResumoSemananCdStatusMeta: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'nCdStatusMeta'
      Calculated = True
    end
    object qryResumoSemananValAcumulado: TBCDField
      DisplayLabel = 'Acumulado'
      FieldName = 'nValAcumulado'
      Precision = 12
      Size = 2
    end
    object qryResumoSemananValDiferenca: TFloatField
      DisplayLabel = 'Diferen'#231'a'
      FieldKind = fkCalculated
      FieldName = 'nValDiferenca'
      Calculated = True
    end
  end
  object SP_ACOMPANHAMENTO_META_MENSAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ACOMPANHAMENTO_META_MENSAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@iMes'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iAno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
    Top = 286
  end
  object dsResumoSemana: TDataSource
    DataSet = qryResumoSemana
    Left = 584
    Top = 286
  end
  object dsResumoVendedor: TDataSource
    DataSet = qryResumoVendedor
    Left = 400
    Top = 287
  end
  object qryResumoVendedor: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryResumoVendedorCalcFields
    Parameters = <
      item
        Name = 'nCdMetaSemana'
        DataType = ftString
        Size = 2
        Value = '11'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempResumoVendedor'
      ' WHERE nCdMetaSemana = :nCdMetaSemana'
      ' ORDER BY nValTotalVenda DESC')
    Left = 400
    Top = 255
    object qryResumoVendedornCdVendedor: TIntegerField
      DisplayLabel = 'Vendedor|C'#243'd.'
      FieldName = 'nCdVendedor'
    end
    object qryResumoVendedoriTotalHorasPrev: TBCDField
      DisplayLabel = 'Previsto|Horas Venda'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornValMetaMinima: TBCDField
      DisplayLabel = 'Previsto|Meta M'#237'nima'
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornValMeta: TBCDField
      DisplayLabel = 'Previsto|Meta'
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornValMetaMinimaReal: TBCDField
      DisplayLabel = 'Realizado|Meta M'#237'nima'
      FieldName = 'nValMetaMinimaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedoriTotalHorasReal: TBCDField
      DisplayLabel = 'Realizado|Horas Venda'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornValMetaReal: TBCDField
      DisplayLabel = 'Realizado|Meta'
      FieldName = 'nValMetaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornValTotalVenda: TBCDField
      DisplayLabel = 'Total de Vendas'
      FieldName = 'nValTotalVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornPercMetaMinimaAting: TBCDField
      DisplayLabel = '% Atingida|Meta M'#237'nima'
      FieldName = 'nPercMetaMinimaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornPercMetaAting: TBCDField
      DisplayLabel = '% Atingida|Meta'
      FieldName = 'nPercMetaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedornCdStatusMeta: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'nCdStatusMeta'
      Calculated = True
    end
    object qryResumoVendedornCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryResumoVendedorcNmVendedor: TStringField
      DisplayLabel = 'Vendedor|Nome'
      FieldName = 'cNmVendedor'
      Size = 50
    end
    object qryResumoVendedoriSemana: TIntegerField
      DisplayLabel = 'Semana'
      FieldKind = fkCalculated
      FieldName = 'iSemana'
      Calculated = True
    end
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '8'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '      ,nCdTerceiroResponsavel'
      '  FROM Usuario '
      ' WHERE nCdUsuario = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdUsuario = nCdUsuario'
      '                 AND UL.nCdLoja    = :nCdLoja)')
    Left = 436
    Top = 255
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryVendedornCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 436
    Top = 287
  end
  object qryResumoVendedorMes: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryResumoVendedorMesCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempResumoVendedorMes'
      ' ORDER BY nValTotalVenda DESC')
    Left = 472
    Top = 255
    object qryResumoVendedorMesnCdStatusMeta: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'nCdStatusMeta'
      Calculated = True
    end
    object qryResumoVendedorMesnCdVendedor: TIntegerField
      DisplayLabel = 'Vendedor|C'#243'd.'
      FieldName = 'nCdVendedor'
    end
    object qryResumoVendedorMescNmVendedor: TStringField
      DisplayLabel = 'Vendedor|Nome'
      FieldName = 'cNmVendedor'
      Size = 50
    end
    object qryResumoVendedorMesiTotalHorasPrev: TBCDField
      DisplayLabel = 'Horas Venda|Previsto'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesiTotalHorasReal: TBCDField
      DisplayLabel = 'Horas Venda|Realizado'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnValMetaMinima: TBCDField
      DisplayLabel = 'Meta M'#237'nima|Previsto'
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnValMetaMinimaReal: TBCDField
      DisplayLabel = 'Meta M'#237'nima|Realizado'
      FieldName = 'nValMetaMinimaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnValMeta: TBCDField
      DisplayLabel = 'Meta|Previsto'
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnValMetaReal: TBCDField
      DisplayLabel = 'Meta|Realizado'
      FieldName = 'nValMetaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnPercMetaReal: TBCDField
      DisplayLabel = 'Meta|% Realizado'
      FieldName = 'nPercMetaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnValTotalVenda: TBCDField
      DisplayLabel = 'Total de Vendas'
      FieldName = 'nValTotalVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnPercMetaMinimaAting: TBCDField
      DisplayLabel = '% Atingida|Meta M'#237'nima'
      FieldName = 'nPercMetaMinimaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResumoVendedorMesnPercMetaAting: TBCDField
      DisplayLabel = '% Atingida|Meta'
      FieldName = 'nPercMetaAting'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsResumoVendedorMes: TDataSource
    DataSet = qryResumoVendedorMes
    Left = 472
    Top = 287
  end
  object ImageList2: TImageList
    Left = 512
    Top = 287
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D4DFE6004F575C004546460040424200464D5000C9D6DF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D4DFE6004F575C004546460040424200464D5000C9D6DF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7FAF700F9FBF900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000838E9500BCBCBB00EBEAEA00CDCCCC00A3A19F005A656D000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000838E9500BCBCBB00EBEAEA00CDCCCC00A3A19F005A656D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7FAF80037833D00347D3A00F9FBF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000079868C00A6A5A200A8A2A2009D999800948F8B00525960000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000079868C00A6A5A200A8A2A2009D999800948F8B00525960000000
      000000000000000000000000000000000000000000000000000000000000F8FB
      F800408E470054A35C004F9F5700337D3900F8FBF90000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3EDE3006683970098A19A006991860064887B00817F71002E445800F4EF
      E600000000000000000000000000000000000000000000000000000000000000
      0000DBE5EC00668397007A95A3003A8A9800357F8C00606E76002E445800DFE8
      EF00000000000000000000000000000000000000000000000000F8FBF800499A
      51005BAC640077CA820074C87E0051A05900347E3A00F8FBF900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F8F4
      EE00D8C7A600CFBA9300D4C19E00D5C2A000D4C19D00D7C5A400C3A87600C9B1
      8500F4EFE500000000000000000000000000000000000000000000000000E3F1
      FB0070AFE100469DE6004BBEF70047E6FD0041E5FD0051C3FB00167CDE003D88
      D300D5E8F70000000000000000000000000000000000F8FCF90051A65A0063B5
      6D007ECE89007BCC870076CA810076C9810052A25A00357F3B00F9FBF9000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F9F6F000D7C6
      A600E8DECB00D9C8A800DDCEB300DDCEB200DDCEB200DDCEB200D2BE9A00E5D9
      C400C6AC7D00F1EADE0000000000000000000000000000000000E6F3FB0069B3
      E900A6D3F30065AEF00074E1F60073E1F60072E0F60071E0F6004CA3EC009CC3
      EF002C81D700C9E0F5000000000000000000F9FCFA0059B063006BBD760084D2
      90007AC9850060B26A0063B46D0078C9830078CB820053A35C0036803C00F9FB
      F900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00DED0B400E8DE
      CB00F7F3ED0091AEB300DDCEB300DBCBAE00D8C7A700DDCEB2007FA1A700F0EA
      DE00E9DFCD00C6AC7E00F9F7F2000000000000000000FDFEFF007FC0EC00A5D4
      F300DCFAFE0038A1EB0074E1F6006AE4F6005DE2F50072E0F6001691E800C0F5
      FD00ACCEF1002D83D700EAF3FB0000000000D3ECD6006CBD760079C9860080CE
      8D0053A75C00B2D6B5009CC9A0005CAD67007CCC860079CB850054A45D003781
      3D00F9FBF9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ECE4D500E1D4BB00FBF9
      F600DFD1B6008DA9B000DDCEB300DDCEB300DACAAC00DDCEB2007799A100D2BE
      9A00F7F3ED00D9C8A800DBCBAD000000000000000000B7DDF5008BC8EF00ECFC
      FE0077E1F7002F99EA0075E1F60074E1F60068DEF50073E1F6000986E60046D5
      F300DCFEFE006FAAE50079B3E6000000000000000000D9EFDC006CBD75006DC0
      7900B5DBB900000000000000000098C79D005EAE68007DCD89007CCD870056A5
      5F0038823E00F9FBF90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DECFB400F2ECE100F5F0
      E800DFD1B700E2D5BE00A7BDC300ACC0C500A9BFC4009CB4BB00E0D3BA00D7C5
      A500E4D8C100F2ECE100CAB2860000000000000000007FC5EE00C9E9F900D4F9
      FD007CE3F70086E5F80060B1EF0068B5EF0063B4EF004CA6EC0082E4F70059DC
      F5008AEBFA00CBE2F700398FDA00000000000000000000000000D5EDD800BEE2
      C3000000000000000000000000000000000099C89D005FAF69007FCE8A007ECE
      890057A6600039833F00F9FBF900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DECFB400F7F3ED00F2EC
      E200F3EEE400F5F1E900F4EFE600F3EEE500F2ECE200F1EBE000EFE8DA00E5DA
      C500DDCEB200F8F5EF00C7AE810000000000000000007EC6EE00DFF6FD00C8F5
      FC00CDF6FC00D6F7FD00D3F4FC00CFF2FC00CAF1FB00C4F0FC00BAF2FB0096EA
      F80072E5F700E2F4FD003289D800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099C89E0060B06A0081CF
      8D007FCF8B0058A7610039854000F9FBF9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E3D8C100F3EEE500F6F2
      EC00F7F3ED00FBF9F600FBF9F600FBF9F600FBF9F600FAF8F400F8F5EF00EEE7
      D900EAE1D000F2ECE100CCB48B00000000000000000095D2F200D2EFFB00DBF9
      FE00DFF9FD00ECFBFE00EEFCFE00EFFCFE00EFFCFE00EBFBFE00E0F9FE00B8F1
      FB00A8F1FB00CBE5F8003E95DD00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000099C99E0062B2
      6C0082D18F007AC8850057A660009FC4A2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F1EBE000EDE5D600F9F7
      F200F6F2EC00F9F7F200FBFAF700FEFEFD00FEFEFD00FBF9F600F4EFE600EDE5
      D600FBF9F600D9C8A800E0D1B8000000000000000000C8E9F900B4E3F800E5FA
      FE00DBF8FD00E4FAFE00F0FCFE00F9FEFF00F9FEFF00EFFCFE00D2F6FD00B4F1
      FB00EDFDFF006BB3EA0088C2EC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009ACA
      9F0063B36D005FAF6900A5CBA900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFD00ECE3D400F0EA
      DE00F8F5EF00F7F3ED00F5F1E900F3EEE500F2ECE200F2ECE100F5F1E900FAF8
      F400E3D7C000D1BD9800FCFAF8000000000000000000FCFEFF00B5E2F600C3EB
      FA00E2F9FD00E0F9FD00D5F7FD00CFF6FD00C9F4FC00C7F4FC00D6F9FD00EBFA
      FE0090CAF20050A9E600F3F9FD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009ACA9F00A5CEA90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFCFB00F0E9
      DD00EDE5D600F2ECE100F5F1E900F8F5EF00F8F5F000F5F1E900EDE5D600D9C8
      A900DAC9AA00F9F6F10000000000000000000000000000000000F7FCFE00C4E7
      F900B7E4F800C7ECFB00D7F3FC00E1F7FD00E2F8FE00D8F0FC00B6DFF8006BBB
      ED006CB9EB00E8F4FC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F8F5EF00EFE8DC00EAE0CE00E4D8C200E0D2B900DED0B600E1D3BA00EDE5
      D700000000000000000000000000000000000000000000000000000000000000
      0000E2F5FB00C1E6F800A8DDF60094D2F10085CCF00080C8F00087CBF000BBE1
      F600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000F81FF81FF9FF0000
      F81FF81FF0FF0000F81FF81FE07F0000F00FF00FC03F0000E007E007801F0000
      C003C003000F00008001800100070000800180018603000080018001CF010000
      80018001FF80000080018001FFC0000080018001FFE1000080018001FFF30000
      C003C003FFFF0000F00FF00FFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryRelVendaAnalitico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetaSemana'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'SELECT MetaSemana.nCdMetaSemana'
      '      ,MetaSemana.nCdMetaMes'
      '      ,MetaSemana.dDtInicial dDtInicialSemana'
      '      ,MetaSemana.dDtFinal dDtFinalSemana'
      '      ,MetaMes.dDtInicial dDtInicialMes'
      '      ,MetaMes.dDtFinal dDtFinalMes'
      ' FROM MetaSemana'
      
        '      INNER JOIN MetaMes ON MetaMes.nCdMetaMes = MetaSemana.nCdM' +
        'etaMes'
      'WHERE nCdMetaSemana = :nCdMetaSemana')
    Left = 544
    Top = 255
    object qryRelVendaAnaliticonCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryRelVendaAnaliticonCdMetaMes: TIntegerField
      FieldName = 'nCdMetaMes'
    end
    object qryRelVendaAnaliticodDtInicialSemana: TDateTimeField
      FieldName = 'dDtInicialSemana'
    end
    object qryRelVendaAnaliticodDtFinalSemana: TDateTimeField
      FieldName = 'dDtFinalSemana'
    end
    object qryRelVendaAnaliticodDtInicialMes: TDateTimeField
      FieldName = 'dDtInicialMes'
    end
    object qryRelVendaAnaliticodDtFinalMes: TDateTimeField
      FieldName = 'dDtFinalMes'
    end
  end
  object qryBuscaSemana: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetaSemana'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT MetaSemana.nCdMetaSemana'
      '      ,MetaSemana.dDtInicial dDtInicialSemana'
      '      ,MetaSemana.dDtFinal dDtFinalSemana'
      ' FROM MetaSemana'
      
        'WHERE MetaSemana.nCdMetaMes = (SELECT nCdMetaMes FROM MetaSemana' +
        ' WHERE nCdMetaSemana = :nCdMetaSemana)')
    Left = 544
    Top = 286
    object qryBuscaSemananCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryBuscaSemanadDtInicialSemana: TDateTimeField
      FieldName = 'dDtInicialSemana'
    end
    object qryBuscaSemanadDtFinalSemana: TDateTimeField
      FieldName = 'dDtFinalSemana'
    end
  end
end
