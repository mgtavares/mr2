inherited frmConsultaRemessaNumerario: TfrmConsultaRemessaNumerario
  Left = 226
  Top = 95
  Width = 1117
  Height = 584
  Caption = 'Consulta Remessa de Numer'#225'rios'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 129
    Width = 1101
    Height = 417
  end
  inherited ToolBar1: TToolBar
    Width = 1101
    ButtonWidth = 117
    TabOrder = 1
    inherited ToolButton1: TToolButton
      Caption = 'Executar &Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 117
    end
    inherited ToolButton2: TToolButton
      Left = 125
    end
  end
  object DBGridEh4: TDBGridEh [2]
    Left = 0
    Top = 129
    Width = 1101
    Height = 417
    Align = alClient
    AllowedOperations = [alopAppendEh]
    Color = clWhite
    DataGrouping.GroupLevels = <>
    DataSource = dsRemessaNum
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh4DblClick
    Columns = <
      item
        Color = clWindow
        EditButtons = <>
        FieldName = 'nCdRemessaNum'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|C'#243'd.'
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Contas|Origem'
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdConta_1'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Contas|Destino'
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'dDtRemessa'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Datas|Remessa'
        Width = 180
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'dDtConfirma'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Datas|Confirma'#231#227'o'
        Width = 180
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'dDtCancel'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Datas|Cancelamento'
        Width = 180
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Usu'#225'rios|Remessa'
        Width = 200
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'cNmUsuario_1'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Usu'#225'rios|Confirma'#231#227'o'
        Width = 200
      end
      item
        DisplayFormat = '#,##0.00'
        EditButtons = <>
        FieldName = 'nValDinheiro'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Valores|Dinheiro'
      end
      item
        DisplayFormat = '#,##0.00'
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Valores|Cheque'
        Width = 82
      end
      item
        DisplayFormat = '#,##0.00'
        EditButtons = <>
        FieldName = 'nValTransacao'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Valores|Cart'#227'o'
        Width = 82
      end
      item
        DisplayFormat = '#,##0.00'
        EditButtons = <>
        FieldName = 'nValCrediario'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Valores|Credi'#225'rio'
        Width = 82
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'cOBS'
        Footers = <>
        Tag = 1
        Title.Caption = 'Remessa de Numer'#225'rios|Observa'#231#245'es'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1101
    Height = 100
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 2
    object Label5: TLabel
      Tag = 1
      Left = 217
      Top = 72
      Width = 16
      Height = 13
      Alignment = taCenter
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 8
      Top = 72
      Width = 108
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Movimenta'#231#227'o'
    end
    object Label3: TLabel
      Tag = 1
      Left = 26
      Top = 48
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Caixa/Cofre'
    end
    object Label1: TLabel
      Tag = 1
      Left = 96
      Top = 24
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object er2LkpLoja: TER2LookupMaskEdit
      Left = 124
      Top = 16
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 147
      QueryLookup = qryLoja
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 192
      Top = 16
      Width = 319
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 5
    end
    object er2LkpContaBancaria: TER2LookupMaskEdit
      Left = 124
      Top = 40
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnBeforeLookup = er2LkpContaBancariaBeforeLookup
      CodigoLookup = 126
      WhereAdicional.Strings = (
        '')
      QueryLookup = qryContaBancaria
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 192
      Top = 40
      Width = 319
      Height = 21
      DataField = 'nCdConta'
      DataSource = dsContaBancaria
      TabOrder = 6
    end
    object rdgSituacao: TRadioGroup
      Left = 516
      Top = 14
      Width = 325
      Height = 45
      Caption = ' Situa'#231#227'o Remessa '
      Color = clWhite
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Em aberto'
        'Confirmado'
        'Cancelado')
      ParentColor = False
      TabOrder = 4
    end
    object mskdDtInicial: TMaskEdit
      Left = 124
      Top = 64
      Width = 70
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object mskdDtFinal: TMaskEdit
      Left = 251
      Top = 64
      Width = 70
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
  end
  inherited ImageList1: TImageList
    Left = 544
    Top = 216
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      9900999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFF30000
      FE00C003FFE9000000008001F051000000008001E023000000008001C0270000
      00008001860F0000000080019987000000008001008700000000800100470000
      0000800100070000000080010007000000018001800F000000038001801F0000
      0077C003C03F0000007FFFFFF0FF000000000000000000000000000000000000
      000000000000}
  end
  object qryRemessaNum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cFlgEmAberto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgConfirmado'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgCancelado'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @cFlgEmAberto     int'
      '       ,@cFlgConfirmado   int'
      '       ,@cFlgCancelado    int'
      '       ,@nCdContaBancaria int'
      ''
      'SET @cFlgEmAberto     = :cFlgEmAberto'
      'SET @cFlgConfirmado   = :cFlgConfirmado'
      'SET @cFlgCancelado    = :cFlgCancelado'
      'SET @nCdContaBancaria = :nCdContaBancaria'
      ''
      'SELECT nCdRemessaNum'
      '      ,dDtRemessa'
      '      ,Conta1.nCdConta'
      '      ,Conta2.nCdConta'
      '      ,dDtConfirma'
      '      ,dDtCancel'
      '      ,Usu1.cNmUsuario'
      '      ,Usu2.cNmUsuario'
      '      ,ISNULL(nValDinheiro,0) as nValDinheiro'
      '      ,ISNULL((SELECT SUM(nValCheque) '
      '                 FROM ChequeRemessaNum'
      
        '                      INNER JOIN Cheque ON Cheque.nCdCheque = Ch' +
        'equeRemessaNum.nCdCheque'
      
        '                WHERE nCdRemessaNum = RemessaNum.nCdRemessaNum),' +
        '0) as nValCheque'
      '      ,ISNULL((SELECT SUM(nValTransacao) '
      '                 FROM CartaoRemessaNum'
      
        '                      INNER JOIN TransacaoCartao ON TransacaoCar' +
        'tao.nCdTransacaoCartao = CartaoRemessaNum.nCdTransacaoCartao'
      
        '                WHERE nCdRemessaNum = RemessaNum.nCdRemessaNum),' +
        '0) as nValTransacao'
      '      ,ISNULL((SELECT SUM(nValCrediario)'
      '                 FROM CrediarioRemessaNum'
      
        '                      INNER JOIN Crediario ON Crediario.nCdCredi' +
        'ario = CrediarioRemessaNum.nCdCrediario'
      
        '                WHERE nCdRemessaNum = RemessaNum.nCdRemessaNum),' +
        '0) as nValCrediario'
      '      ,cOBS'
      '  FROM RemessaNum'
      
        '       LEFT  JOIN ContaBancaria Conta1 ON Conta1.nCdContaBancari' +
        'a = RemessaNum.nCdContaOrigem'
      
        '       LEFT  JOIN ContaBancaria Conta2 ON Conta2.nCdContaBancari' +
        'a = RemessaNum.nCdContaDestino         '
      
        '       INNER JOIN Usuario Usu1         ON Usu1.nCdUsuario       ' +
        '  = RemessaNum.nCdUsuarioCad'
      
        '       LEFT  JOIN Usuario Usu2         ON Usu2.nCdUsuario       ' +
        '  = RemessaNum.nCdUsuarioConfirma'
      ' WHERE RemessaNum.nCdEmpresa     = :nCdEmpresa'
      '   AND RemessaNum.nCdLoja        = :nCdLoja'
      
        '   AND ((Conta1.nCdContaBancaria = @nCdContaBancaria) OR (@nCdCo' +
        'ntaBancaria = 0))'
      '   AND dDtRemessa >= CONVERT(datetime, :dDtInicial, 103)'
      '   AND dDtRemessa <  CONVERT(datetime, :dDtFinal, 103) + 1'
      
        '   AND (((dDtFech     IS NOT NULL) AND (@cFlgEmAberto   = 1)  AN' +
        'D (dDtCancel IS NULL) AND (dDtConfirma IS NULL)) OR @cFlgEmAbert' +
        'o = 0)'
      
        '   AND (((dDtConfirma IS NOT NULL) AND (@cFlgConfirmado = 1)) OR' +
        ' @cFlgConfirmado = 0)'
      
        '   AND (((dDtCancel   IS NOT NULL) AND (@cFlgCancelado  = 1)) OR' +
        ' @cFlgCancelado  = 0)'
      ' ORDER BY dDtRemessa'
      '')
    Left = 584
    Top = 216
    object qryRemessaNumnCdRemessaNum: TIntegerField
      FieldName = 'nCdRemessaNum'
    end
    object qryRemessaNumdDtRemessa: TDateTimeField
      FieldName = 'dDtRemessa'
    end
    object qryRemessaNumnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryRemessaNumnCdConta_1: TStringField
      FieldName = 'nCdConta_1'
      FixedChar = True
      Size = 15
    end
    object qryRemessaNumdDtConfirma: TDateTimeField
      FieldName = 'dDtConfirma'
    end
    object qryRemessaNumdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryRemessaNumcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryRemessaNumcNmUsuario_1: TStringField
      FieldName = 'cNmUsuario_1'
      Size = 50
    end
    object qryRemessaNumnValDinheiro: TBCDField
      FieldName = 'nValDinheiro'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object qryRemessaNumnValCheque: TBCDField
      FieldName = 'nValCheque'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryRemessaNumnValTransacao: TBCDField
      FieldName = 'nValTransacao'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryRemessaNumnValCrediario: TBCDField
      FieldName = 'nValCrediario'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryRemessaNumcOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryContaBancariaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '      ,cFlgCaixa'
      '      ,cFlgCofre'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND nCdLoja          = :nCdLoja'
      '   AND nCdEmpresa       = :nCdEmpresa')
    Left = 648
    Top = 216
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 648
    Top = 248
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '       ,cNmLoja'
      '   FROM Loja'
      '  WHERE nCdLoja = :nPK')
    Left = 616
    Top = 216
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 616
    Top = 248
  end
  object dsRemessaNum: TDataSource
    DataSet = qryRemessaNum
    Left = 584
    Top = 248
  end
end
