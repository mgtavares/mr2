unit rPlanilhaCobranca_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPlanilhaCobranca_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand5: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    qryRelatorio: TADOQuery;
    qryRelatorionCdTerceiro: TIntegerField;
    qryRelatoriocNmTerceiro: TStringField;
    qryRelatoriocUF: TStringField;
    qryRelatorionTotalVencer: TBCDField;
    qryRelatorionTotalVencido: TBCDField;
    qryRelatorionTotalCD: TBCDField;
    qryRelatorionTotal: TBCDField;
    qryRelatorionPerc: TBCDField;
    qryRelatorionFatMes1: TBCDField;
    qryRelatorionFatMes2: TBCDField;
    qryRelatorionFatMes3: TBCDField;
    qryRelatorionFatMes4: TBCDField;
    qryRelatorionFatMes5: TBCDField;
    qryRelatorionFatMes6: TBCDField;
    qryRelatoriodDtProxAcao: TDateTimeField;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel11: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPlanilhaCobranca_view: TrptPlanilhaCobranca_view;

implementation

{$R *.dfm}

end.
