inherited frmTabFichaTecnica: TfrmTabFichaTecnica
  Left = 380
  Top = 251
  Width = 759
  Height = 283
  Caption = 'Tabela Ficha Tecnica'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 743
    Height = 220
  end
  object Label1: TLabel [1]
    Left = 18
    Top = 44
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 68
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
  end
  object Label4: TLabel [3]
    Left = 151
    Top = 44
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Externo Web'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 743
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton50: TToolButton
      Left = 0
      Wrap = True
    end
    inherited btCancelar: TToolButton
      Left = 0
      Top = 30
    end
    inherited ToolButton11: TToolButton
      Left = 88
      Top = 30
    end
    inherited btConsultar: TToolButton
      Left = 96
      Top = 30
    end
    inherited ToolButton8: TToolButton
      Left = 184
      Top = 30
    end
    inherited ToolButton5: TToolButton
      Left = 192
      Top = 30
    end
    inherited ToolButton4: TToolButton
      Left = 280
      Top = 30
    end
    inherited btnAuditoria: TToolButton
      Left = 288
      Top = 30
    end
  end
  object DBEdit1: TDBEdit [5]
    Left = 64
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmTabFichaTecnica'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 264
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdIdExternoWeb'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTabFichaTecnica'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBRadioGroup1: TDBRadioGroup [8]
    Left = 64
    Top = 96
    Width = 289
    Height = 57
    Caption = ' Tipo Dado '
    Columns = 3
    DataField = 'cTipoDado'
    DataSource = dsMaster
    Items.Strings = (
      'Valor'
      'Caracter'
      'Data')
    TabOrder = 4
    Values.Strings = (
      'V'
      'C'
      'D')
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TabFichaTecnica ')
    Left = 480
    Top = 96
    object qryMasternCdTabFichaTecnica: TIntegerField
      FieldName = 'nCdTabFichaTecnica'
    end
    object qryMastercNmTabFichaTecnica: TStringField
      FieldName = 'cNmTabFichaTecnica'
      Size = 50
    end
    object qryMasternCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
    object qryMastercTipoDado: TStringField
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
  end
  inherited dsMaster: TDataSource
    Left = 480
    Top = 128
  end
  inherited qryID: TADOQuery
    Left = 512
    Top = 128
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 544
    Top = 128
  end
  inherited qryStat: TADOQuery
    Left = 512
    Top = 96
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 544
    Top = 96
  end
  inherited ImageList1: TImageList
    Left = 576
    Top = 96
  end
end
