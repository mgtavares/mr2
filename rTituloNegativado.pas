unit rTituloNegativado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptTituloNegativado = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit4: TMaskEdit;
    DBEdit4: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    Image2: TImage;
    dsLoja: TDataSource;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptTituloNegativado: TrptTituloNegativado;

implementation

uses fMenu, rImpSP, fLookup_Padrao,rTituloNegativado_view;

{$R *.dfm}

procedure TrptTituloNegativado.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;

end;


procedure TrptTituloNegativado.ToolButton1Click(Sender: TObject);
var
  objRel : TrptTituloNegativado_view;
begin

  objRel := TrptTituloNegativado_view.Create(nil);

  try
      try
          objRel.qryTitulos.Close;

          {--posiciona qryEmpresa para verificar se a empresa digitada � valida
           --e tem acesso permitido para o usu�rio logado--}

          qryEmpresa.Close ;
          qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
          qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
          qryEmpresa.Open;
          if qryEmpresa.Eof then MaskEdit3.Text := '';

          {--posiciona qryLoja para verificar se a loja digitada � valida
           --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

          qryLoja.Close ;
          if (Trim(MaskEdit4.Text) <> '') then
          begin
              qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
              qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit4.Text ;
              qryLoja.Open ;
              if qryLoja.Eof then MaskEdit4.Text := '';
          end;

          {--aqui passa os valores para os parametros de entrada da procedure--}
          objRel.qryTitulos.Parameters.ParamByName('nCdListaCobranca').Value := 0 ;
          objRel.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.ConvInteiro(MaskEdit3.Text);
          objRel.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value       := frmMenu.ConvInteiro(MaskEdit4.Text);
          objRel.qryTitulos.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.qryTitulos.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.qryTitulos.Parameters.ParamByName('nCdLoja').Value          := 0 ;

          {-- abre a qryTitulos para exibi��o dos dados no relat�rio --}
          objRel.qryTitulos.Open;

          {-- aqui preenche os filtros do relat�rio--}
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.lblFiltro1.Caption := '';

          if (Trim(DBedit3.Text) <> '') then
              objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

          if (Trim(MaskEdit4.Text) <> '')then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja Cobradora : ' + trim(MaskEdit4.Text) + '-' + DbEdit4.Text ;

          if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Negativa��o: ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

          { -- exibe dados complementares do cliente no relat�rio -- }
          if (RadioGroup1.ItemIndex = 0) then
              objRel.QRSubDetail1.PrintIfEmpty := False;

          objRel.QuickRep1.PreviewModal ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptTituloNegativado.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(59,'Loja.cFlgLojaCobradora = 1');

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptTituloNegativado.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TrptTituloNegativado.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptTituloNegativado.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

initialization
     RegisterClass(TrptTituloNegativado) ;

end.
