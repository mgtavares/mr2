inherited frmLanctoCaixaEstornado: TfrmLanctoCaixaEstornado
  Left = 1
  Top = 93
  Width = 1151
  Height = 677
  Caption = 'Consulta Lan'#231'amentos de Caixa Estornado'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 153
    Width = 1135
    Height = 488
  end
  inherited ToolBar1: TToolBar
    Width = 1135
    ButtonWidth = 122
    inherited ToolButton1: TToolButton
      Caption = '&Executar Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 122
    end
    inherited ToolButton2: TToolButton
      Left = 130
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1135
    Height = 124
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 87
      Top = 24
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label2: TLabel
      Tag = 1
      Left = 80
      Top = 49
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Caixa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 71
      Top = 101
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo'
    end
    object Label5: TLabel
      Tag = 1
      Left = 11
      Top = 75
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Lan'#231'amento'
    end
    object Label4: TLabel
      Tag = 1
      Left = 192
      Top = 101
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object MaskEdit2: TMaskEdit
      Left = 112
      Top = 93
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Left = 216
      Top = 93
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object edtLoja: TMaskEdit
      Left = 112
      Top = 16
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object edtCaixa: TMaskEdit
      Left = 112
      Top = 41
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtCaixaExit
      OnKeyDown = edtCaixaKeyDown
    end
    object edtTipoLancto: TMaskEdit
      Left = 112
      Top = 67
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnExit = edtTipoLanctoExit
      OnKeyDown = edtTipoLanctoKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 184
      Top = 16
      Width = 313
      Height = 21
      DataField = 'cNmLoja'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 184
      Top = 40
      Width = 313
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource2
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 184
      Top = 67
      Width = 313
      Height = 21
      DataField = 'cNmTipoLancto'
      DataSource = DataSource3
      TabOrder = 7
    end
    object btConsLancto: TcxButton
      Left = 506
      Top = 14
      Width = 159
      Height = 28
      Caption = 'Consultar Lan'#231'amento'
      TabOrder = 8
      TabStop = False
      OnClick = btConsLanctoClick
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 153
    Width = 1135
    Height = 488
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsResultado
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = ',0'
          Kind = skCount
          Column = cxGridDBTableView1nCdLanctoFin
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLanctoFin'
        Width = 113
      end
      object cxGridDBTableView1nCdLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'nCdLoja'
        Width = 56
      end
      object cxGridDBTableView1nCdConta: TcxGridDBColumn
        DataBinding.FieldName = 'nCdConta'
        Width = 105
      end
      object cxGridDBTableView1dDtLancto: TcxGridDBColumn
        DataBinding.FieldName = 'dDtLancto'
        Width = 112
      end
      object cxGridDBTableView1dDtEstorno: TcxGridDBColumn
        DataBinding.FieldName = 'dDtEstorno'
        Width = 105
      end
      object cxGridDBTableView1cNmTipoLancto: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTipoLancto'
        Width = 189
      end
      object cxGridDBTableView1cHistorico: TcxGridDBColumn
        Caption = 'Hist'#243'rico'
        DataBinding.FieldName = 'cHistorico'
        Width = 189
      end
      object cxGridDBTableView1cMotivoEstorno: TcxGridDBColumn
        DataBinding.FieldName = 'cMotivoEstorno'
        Width = 189
      end
      object cxGridDBTableView1nValLancto: TcxGridDBColumn
        DataBinding.FieldName = 'nValLancto'
        HeaderAlignmentHorz = taRightJustify
        Width = 135
      end
      object cxGridDBTableView1cNmUsuario: TcxGridDBColumn
        DataBinding.FieldName = 'cNmUsuario'
        Width = 145
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 744
    Top = 88
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdTipoLancto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja          int'
      '       ,@nCdContaBancaria int'
      '       ,@dDtInicial       varchar(10)'
      '       ,@dDtFinal         varchar(10)'
      '       ,@nCdTipoLancto    int'
      ''
      'SET @nCdLoja          = :nCdLoja'
      'SET @nCdContaBancaria = :nCdContaBancaria'
      'SET @dDtInicial       = :dDtInicial'
      'SET @dDtFinal         = :dDtFinal'
      'SET @nCdTipoLancto    = :nCdTipoLancto'
      ''
      'SELECT LanctoFin.nCdLanctoFin'
      '      ,LanctoFin.dDtLancto'
      '      ,LanctoFin.dDtEstorno'
      '      ,dbo.fn_ZeroEsquerda(ContaBancaria.nCdLoja,3) as nCdLoja'
      '      ,TipoLancto.nCdTipoLancto'
      '      ,TipoLancto.cNmTipoLancto'
      '      ,LanctoFin.cMotivoEstorno'
      '      ,LanctoFin.nValLancto'
      '      ,Usuario.cNmUsuario'
      '      ,ContaBancaria.nCdConta'
      '      ,LanctoFin.cHistorico'
      '  FROM LanctoFin '
      
        '       INNER JOIN TipoLancto    ON TipoLancto.nCdTipoLancto     ' +
        '  = LanctoFin.nCdTipoLancto'
      
        '       LEFT  JOIN Usuario       ON Usuario.nCdUsuario           ' +
        '  = LanctoFin.nCdUsuarioEstorno'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = LanctoFin.nCdContaBancaria'
      
        ' WHERE ((@nCdLoja               = 0)                            ' +
        '       OR (ContaBancaria.nCdLoja      = @nCdLoja))'
      
        '   AND ((@nCdTipoLancto         = 0)                            ' +
        '       OR (LanctoFin.nCdTipoLancto    = @nCdTipoLancto))'
      
        '   AND ((@nCdContaBancaria      = 0)                            ' +
        '       OR (LanctoFin.nCdContaBancaria = @nCdContaBancaria))'
      
        '   AND ((LanctoFin.dDtEstorno  >=  Convert(DATETIME,@dDtInicial,' +
        '103))  OR (@dDtInicial                = '#39'01/01/1900'#39'))'
      
        '   AND ((LanctoFin.dDtEstorno   < (Convert(DATETIME,@dDtFinal,10' +
        '3)+1)) OR (@dDtInicial                = '#39'01/01/1900'#39'))'
      '   AND dDtEstorno               is not null'
      
        '   AND (ContaBancaria.cFlgCaixa = 1 AND ContaBancaria.cFlgCofre ' +
        '= 0)')
    Left = 400
    Top = 232
    object qryResultadonCdLanctoFin: TAutoIncField
      DisplayLabel = 'C'#243'd. Lan'#231'amento'
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryResultadodDtLancto: TDateTimeField
      DisplayLabel = 'Data Lan'#231'amento'
      FieldName = 'dDtLancto'
    end
    object qryResultadodDtEstorno: TDateTimeField
      DisplayLabel = 'Data Estorno'
      FieldName = 'dDtEstorno'
    end
    object qryResultadocNmTipoLancto: TStringField
      DisplayLabel = 'Tipo de Lan'#231'amento'
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryResultadonValLancto: TBCDField
      DisplayLabel = 'Valor Lan'#231'amento'
      FieldName = 'nValLancto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadocNmUsuario: TStringField
      DisplayLabel = 'Usuario Autorizador'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryResultadonCdConta: TStringField
      DisplayLabel = 'Caixa'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryResultadocMotivoEstorno: TStringField
      DisplayLabel = 'Motivo Estorno'
      FieldName = 'cMotivoEstorno'
      Size = 150
    end
    object qryResultadonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryResultadocHistorico: TStringField
      FieldName = 'cHistorico'
      Size = 50
    end
    object qryResultadonCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 416
    Top = 264
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 440
    Top = 232
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria        = :nPK'
      '   AND ContaBancaria.nCdLoja   = :nCdLoja'
      '   AND ContaBancaria.cFlgCaixa = 1 '
      '   AND ContaBancaria.cFlgCofre = 0')
    Left = 480
    Top = 232
    object qryCaixanCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryCaixanCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object qryTipoLancto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTipoLancto'
      '      ,cNmTipoLancto '
      '  FROM TipoLancto'
      ' WHERE nCdTipoLancto  = :nPK'
      '   AND cFlgContaCaixa = 1')
    Left = 520
    Top = 232
    object qryTipoLanctonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryTipoLanctocNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 456
    Top = 264
  end
  object DataSource2: TDataSource
    DataSet = qryCaixa
    Left = 496
    Top = 264
  end
  object DataSource3: TDataSource
    DataSet = qryTipoLancto
    Left = 536
    Top = 264
  end
  object qryVerificaTipoLancto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoLancto'
      '  FROM LanctoFin'
      ' WHERE nCdLanctoFin =:nCdLanctoFin')
    Left = 672
    Top = 37
    object qryVerificaTipoLanctonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
  end
end
