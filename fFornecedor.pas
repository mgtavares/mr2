unit fFornecedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls, ImgList,
  DBGridEhGrouping, ToolCtrlsEh, ACBrBase, ACBrSocket, ACBrCEP,
  ACBrValidador;

type
  TfrmFornecedor = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercNmFantasia: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMastercFlgMicroEmpresa: TIntegerField;
    qryMastercFlgContICMS: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmStatus: TStringField;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit14: TDBEdit;
    dsTerceiroTipoTerceiro: TDataSource;
    qryValidaCNPJ: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Image2: TImage;
    DBGridEh2: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    Image3: TImage;
    qryMastercFlgIsentoCNPJCPF: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    qryTipoEnd: TADOQuery;
    qryTipoEndnCdTipoEnd: TIntegerField;
    qryTipoEndcNmTipoEnd: TStringField;
    qryEndereco: TADOQuery;
    qryPais: TADOQuery;
    qryPaisnCdPais: TIntegerField;
    qryPaiscNmPais: TStringField;
    qryPaiscSigla: TStringField;
    qryRegiao: TADOQuery;
    qryRegiaonCdRegiao: TIntegerField;
    qryRegiaocNmRegiao: TStringField;
    dsEndereco: TDataSource;
    qryEndereconCdEndereco: TAutoIncField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecoiNumero: TIntegerField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEndereconCdTipoEnd: TIntegerField;
    qryEndereconCdRegiao: TIntegerField;
    qryEnderecocFlgZFM: TIntegerField;
    qryEnderecocSuframa: TStringField;
    qryEnderecocIE: TStringField;
    qryEnderecocIM: TStringField;
    qryEnderecocNmContato: TStringField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEnderecocEmail: TStringField;
    qryEndereconCdPais: TIntegerField;
    qryEndereconCdStatus: TIntegerField;
    qryEnderecocSiglaPais: TStringField;
    qryEnderecocNmRegiao: TStringField;
    qryEnderecocSiglaTipoEnd: TStringField;
    qryTipoEndcSigla: TStringField;
    qryEnderecocCep: TStringField;
    qryValidaEndereco: TADOQuery;
    qryEnderecocComplemento: TStringField;
    cxTabSheet5: TcxTabSheet;
    Image6: TImage;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TStringField;
    qryMastercNumConta: TStringField;
    qryMastercDigitoConta: TStringField;
    qryMasternCdFormaPagto: TIntegerField;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryMastercNmFormaPagto: TStringField;
    Label11: TLabel;
    DBEdit15: TDBEdit;
    Label12: TLabel;
    DBEdit16: TDBEdit;
    Label13: TLabel;
    DBEdit17: TDBEdit;
    Label14: TLabel;
    DBEdit18: TDBEdit;
    Label15: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    qryIncoterms: TADOQuery;
    qryMoeda: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSiglaIncoterms: TStringField;
    qryMastercNmIncoterms: TStringField;
    Label16: TLabel;
    DBEdit22: TDBEdit;
    Label17: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMasternValCredFinanc: TBCDField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    Label18: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    qryMasteriQtdeChequeDev: TIntegerField;
    qryMasterdDtUltChequeDev: TDateTimeField;
    qryMasternValMaiorChequeDev: TBCDField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    qryMastercFlgCobrarJuro: TIntegerField;
    qryMasternValMultaAtraso: TBCDField;
    qryMasternPercJuroDia: TBCDField;
    qryMasternCdGrupoEconomico: TIntegerField;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasterdMaiorRecAtraso: TDateTimeField;
    qryMastercFlgBloqPedVenda: TIntegerField;
    DBEdit13: TDBEdit;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    cxTabSheet6: TcxTabSheet;
    qryContatoTerceiro: TADOQuery;
    qryContatoTerceironCdContatoTerceiro: TAutoIncField;
    qryContatoTerceironCdTerceiro: TIntegerField;
    qryContatoTerceirocNmContato: TStringField;
    qryContatoTerceirocDepartamento: TStringField;
    qryContatoTerceirocTelefone1: TStringField;
    qryContatoTerceirocTelefone2: TStringField;
    qryContatoTerceirocFax: TStringField;
    qryContatoTerceirocEmail: TStringField;
    qryContatoTerceirocOBS: TStringField;
    qryContatoTerceirocFlgContatoPadrao: TIntegerField;
    dsContatoTerceiro: TDataSource;
    DBGridEh3: TDBGridEh;
    qryAux: TADOQuery;
    qryMastercRG: TStringField;
    qryMasternCdBanco2: TIntegerField;
    qryMastercAgencia2: TStringField;
    qryMastercNumConta2: TStringField;
    qryMastercDigitoConta2: TStringField;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label23: TLabel;
    DBEdit34: TDBEdit;
    Label24: TLabel;
    DBEdit35: TDBEdit;
    cxTabSheet4: TcxTabSheet;
    Image5: TImage;
    qryMastercNmContato: TStringField;
    qryMastercTelefone1: TStringField;
    qryMastercTelefone2: TStringField;
    qryMastercTelefoneMovel: TStringField;
    qryMastercFax: TStringField;
    qryMastercEmail: TStringField;
    qryMastercWebSite: TStringField;
    qryMastercOBS: TMemoField;
    Label28: TLabel;
    DBEdit21: TDBEdit;
    Label29: TLabel;
    DBEdit40: TDBEdit;
    Label30: TLabel;
    DBEdit41: TDBEdit;
    Label31: TLabel;
    DBEdit42: TDBEdit;
    Label32: TLabel;
    DBEdit43: TDBEdit;
    Label33: TLabel;
    DBEdit44: TDBEdit;
    Label34: TLabel;
    DBEdit45: TDBEdit;
    Label35: TLabel;
    DBMemo1: TDBMemo;
    DBEdit39: TDBEdit;
    DBEdit38: TDBEdit;
    Label27: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    qryTerceiroTipoTerceiro: TADOQuery;
    qryTerceiroTipoTerceironCdTerceiro: TIntegerField;
    qryTerceiroTipoTerceironCdTipoTerceiro: TIntegerField;
    DataSource5: TDataSource;
    qryMastercNmTitularConta1: TStringField;
    qryMastercNmTitularConta2: TStringField;
    DBEdit50: TDBEdit;
    Label39: TLabel;
    Label38: TLabel;
    DBEdit49: TDBEdit;
    qryMastercFlgHomologado: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    ToolButton3: TToolButton;
    qryTerceiroTipoTerceironCdTerceiroTipoTerceiro: TIntegerField;
    qryTerceiroRepresentante: TADOQuery;
    qryTerceiroRepresentantenCdTerceiroRepresentante: TIntegerField;
    qryTerceiroRepresentantenCdTerceiro: TIntegerField;
    qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField;
    dsTerceiroRepresentante: TDataSource;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTerceiroRepresentantecNmRepresentante: TStringField;
    qryMasternCdIdExternoWeb: TIntegerField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    GroupBox1: TGroupBox;
    DBCheckBox3: TDBCheckBox;
    qryMastercFlgTerceiroWeb: TIntegerField;
    ToolButton10: TToolButton;
    qryTipoLogradouro: TADOQuery;
    qryTipoLogradouronCdTabTipoLogradouro: TIntegerField;
    qryTipoLogradourocDescricao: TStringField;
    dsTipoLogradouro: TDataSource;
    qryEndereconCdTabTipoLogradouro: TIntegerField;
    qryEnderecocDescricaoLogradouro: TStringField;
    qryBuscaEnderecoCEP: TADOQuery;
    qryBuscaEnderecoCEPcEndereco: TStringField;
    qryBuscaEnderecoCEPcBairro: TStringField;
    qryBuscaEnderecoCEPnCdPais: TIntegerField;
    qryBuscaEnderecoCEPcNmPais: TStringField;
    qryBuscaEnderecoCEPnCdEstado: TIntegerField;
    qryBuscaEnderecoCEPcUF: TStringField;
    qryBuscaEnderecoCEPnCdMunicipio: TIntegerField;
    qryBuscaEnderecoCEPcNmMunicipio: TStringField;
    ACBrCEP1: TACBrCEP;
    qryEndereconCdEstado: TIntegerField;
    qryEndereconCdMunicipio: TIntegerField;
    qryEnderecocNmPais: TStringField;
    qryMunicipio: TADOQuery;
    qryMunicipionCdMunicipio: TIntegerField;
    qryMunicipiocNmMunicipio: TStringField;
    qryEstado: TADOQuery;
    qryEstadonCdEstado: TIntegerField;
    qryEstadocUF: TStringField;
    qryEnderecocxxNmPais: TStringField;
    qryEnderecocxxNmEstado: TStringField;
    qryEnderecocxxNmMunicipio: TStringField;
    ACBrValidador1: TACBrValidador;
    qryMastercFlgOptSimples: TIntegerField;
    qryMasternCdRamoAtividade: TIntegerField;
    qryMasternCdGrupoComissao: TIntegerField;
    qryMasternCdLojaTerceiro: TIntegerField;
    qryMastercFlgTipoFat: TIntegerField;
    qryMasterdDtUltNegocio: TDateTimeField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternCdTipoTabPreco: TIntegerField;
    qryMasterdDtNegativacao: TDateTimeField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgEnviaNFeEmail: TIntegerField;
    qryMastercEmailNFe: TStringField;
    qryMastercEmailCopiaNFe: TStringField;
    qryMastercFlgTipoDescontoDanfe: TIntegerField;
    qryMasternCdAreaVenda: TIntegerField;
    qryMasternCdDivisaoVenda: TIntegerField;
    qryMasternCdCanalDistribuicao: TIntegerField;
    qryMasternCdGrupoCredito: TIntegerField;
    qryMasternCdPlanoContaContabil: TIntegerField;
    qryMasternCdGrupoTerceiro: TIntegerField;
    qryMasternCdProjeto: TIntegerField;
    qryMastercInstrucaoBoleto1: TStringField;
    qryMastercInstrucaoBoleto2: TStringField;
    qryMasteriDiasProtesto: TIntegerField;
    qryMasternPercDesctoBolAteVencto: TBCDField;
    qryMasteriDiasCarencia: TIntegerField;
    qryMasternPercMultaBoleto: TBCDField;
    qryMasternPercJurosMesBoleto: TBCDField;
    qryMastercFlgPermitirEmissaoBoleto: TIntegerField;
    qryMastercFlgNaoEnviarProtesto: TIntegerField;
    qryMasternCdSegmentoCobranca: TIntegerField;
    qryMasternCdPerfilCobranca: TIntegerField;
    qryMasternCdTabTipoEnquadTributario: TIntegerField;
    qryMasternCdTipoPedidoProvador: TIntegerField;
    qryMasternCdLocalEstoquePadrao: TIntegerField;
    qryMastercFlgDestacarICMSSTRetNFe: TIntegerField;
    qryMasternValPedidoAbertoWeb: TBCDField;
    qryMastercIDTransportadora: TStringField;
    qryMasternCdTabTipoPorteEmp: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryTerceiroTipoTerceiroBeforePost(DataSet: TDataSet);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEnderecoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit22KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryContatoTerceiroBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit10Enter(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit22Exit(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure qryTerceiroRepresentanteBeforePost(DataSet: TDataSet);
    procedure qryTerceiroRepresentanteCalcFields(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEnderecoCalcFields(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFornecedor: TfrmFornecedor;

implementation

uses fMenu, fLookup_Padrao, fCentralArquivos;

{$R *.dfm}

procedure TfrmFornecedor.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TERCEIRO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 90 ;

  bLimpaAposSalvar  := False ;
  
  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmFornecedor.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  DBEdit3.SetFocus ;
end;

procedure TfrmFornecedor.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit3.Text = '') then
  begin
      ShowMessage('Informe o Nome/Raz�o Social');
      DBEdit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercNmFantasia.Value = '') then
      qryMastercNmFantasia.Value := qryMastercNmTerceiro.Value;

  if ((DBEdit14.Text = '') or (qryMasternCdStatus.Value = 0)) then
  begin
      qryMasternCdStatus.Value := 1 ;
  end ;

  if ((DBEdit25.Text = '') or (qryMasternCdMoeda.Value = 0)) then
  begin
      qryMasternCdmoeda.Value := 1 ;
  end ;

  If (qryMaster.State = dsInsert) then
      qryMasterdDtCadastro.Value := Now() ;

  if ((qryMastercCNPJCPF.Value <> '') And (frmMenu.TestaCpfCgc(qryMastercCNPJCPF.Value) = '')) then
  begin
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if ((qryMastercCNPJCPF.Value = '') and (qryMastercFlgIsentoCNPJCPF.Value = 0)) then
  begin
      ShowMessage('Informe o CNPJ/CPF ou marque como isento da inscri��o.') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  // Valida se o CPF / CNPJ n�o existe
  If (qryMastercCNPJCPF.Value <> '') then
  begin
      qryValidaCNPJ.Close ;
      qryValidaCNPJ.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qryValidaCNPJ.Parameters.ParamByName('cCNPJ').Value       := qryMastercCNPJCPF.Value ;
      qryValidaCNPJ.Open ;

      if not qryValidaCNPJ.Eof then
      begin
          ShowMessage('CNPJ / CPF j� cadastrado para outro terceiro. Utilize a consulta para localizar.') ;
          DbEdit5.SetFocus;
          Abort ;
      end ;

  end ;

  qryMasternValMultaAtraso.Value := abs(qryMasternValMultaAtraso.Value) ;
  qryMasternPercJuroDia.Value := abs(qryMasternPercJuroDia.Value) ;

  inherited;
end;

procedure TfrmFornecedor.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryEndereco.Open ;

  PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryTerceiroRepresentante, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.AsString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondpagto.AsString) ;
  Posicionaquery(qryGrupoEconomico, qryMasternCdGrupoEconomico.AsString) ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.AsString) ;

end;

procedure TfrmFornecedor.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryEndereco.Close ;
  qryContatoTerceiro.Close ;

  qryTerceiroRepres.Close ;
  qryTerceiroRepresentante.Close;
  qryTerceiroTransp.Close ;
  qryCondPagto.Close ;
  qryGrupoEconomico.Close ;
  qryMoeda.Close ;

end;

procedure TfrmFornecedor.qryTerceiroTipoTerceiroBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
  begin
      qryMaster.Post ;
      qryMaster.Edit ;
  end ;

  if (qryTerceiroTipoTerceiro.State = dsInsert) then
      qryTerceiroTipoTerceironCdTerceiroTipoTerceiro.Value := frmMenu.fnProximoCodigo('TERCEIROTIPOTERCEIRO');

  inherited;

end;

procedure TfrmFornecedor.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.qryEnderecoBeforePost(DataSet: TDataSet);
var strEstados : TstringList;
begin

  if (trim(qryEnderecocCEP.Value) = '') then
  begin
      MensagemAlerta('Informe o CEP.') ;
      abort ;
  end ;

  if (length(trim(qryEnderecocCep.Value)) <> 8) then
  begin
      MensagemAlerta('O CEP deve conter 8 d�gitos. Verifique.') ;
      abort ;
  end ;

  try
      strToint(qryEnderecocCEP.Value) ;
  except
      MensagemAlerta('CEP inv�lido. Informe somente os n�meros do cep.') ;
      abort ;
  end ;

  if (qryEnderecocDescricaoLogradouro.Value = '') then
  begin
      MensagemAlerta('Informe o Tipo do Logradouro.');
      Abort;
  end;

  if (qryEnderecocEndereco.Value = '') then
  begin
      MensagemAlerta('Informe o Endere�o.') ;
      Abort ;
  end ;

  if (qryEnderecocBairro.Value = '') then
  begin
      MensagemAlerta('Informe o Bairro.') ;
      Abort ;
  end ;

  if (qryEnderecocxxNmPais.Value = '') then
  begin
      MensagemAlerta('Selecione o pa�s.') ;
      abort ;
  end ;

  if (qryEnderecocxxNmEstado.Value = '') then
  begin
      MensagemAlerta('Selecione o Estado.') ;
      Abort ;
  end ;

  if (qryEnderecocxxNmMunicipio.Value = '') then
  begin
      MensagemAlerta('Selecione o Munic�pio.') ;
      Abort ;
  end ;

  if (qryEnderecocSiglaTipoend.Value = '') then
  begin
      MensagemAlerta('Informe o tipo de endere�o.') ;
      Abort ;
  end ;

  if (qryEndereconCdRegiao.Value = 0) or (qryEnderecocNmRegiao.Value = '') then
  begin
      MensagemAlerta('Informe a regi�o.') ;
      Abort ;
  end ;


  if (qryEnderecocFlgZFM.value = 1) and (qryEnderecocSuframa.Value = '') then
  begin
      MensagemAlerta('Informe a inscri��o do Suframa') ;
      Abort ;
  end ;

  if (qryEndereconCdStatus.Value = 0) then
      qryEndereconCdStatus.Value := 1 ;

  if (qryEnderecocIE.Value = '') then
  begin
      if (qryMastercFlgIsentoCNPJCPF.Value = 0) then
      begin
          if (Length(qryMastercCNPJCPF.Value) > 11) then
          begin
              MensagemAlerta('Informe a inscri��o estadual ou escreva ISENTO.') ;
              Abort;
          end;
      end;
  end
  else
  begin
      {-- v�lida a inscri��o estadual --}
      ACBrValidador1.Documento   := qryEnderecocIE.Value;
      ACBrValidador1.Complemento := qryEnderecocUF.Value;

      ACBrValidador1.Formatar;

      if not (ACBrValidador1.Validar) then
      begin
          MensagemAlerta('Inscri��o estadual digitada � inv�lida');
          Abort;
      end;
  end;

  strEstados := TStringList.Create;

  strEstados.Add('AC') ;
  strEstados.Add('AL') ;
  strEstados.Add('AM') ;
  strEstados.Add('AP') ;
  strEstados.Add('BA') ;
  strEstados.Add('CE') ;
  strEstados.Add('DF') ;
  strEstados.Add('ES') ;
  strEstados.Add('EX') ;
  strEstados.Add('GO') ;
  strEstados.Add('MA') ;
  strEstados.Add('MG') ;
  strEstados.Add('MS') ;
  strEstados.Add('MT') ;
  strEstados.Add('PA') ;
  strEstados.Add('PB') ;
  strEstados.Add('PE') ;
  strEstados.Add('PI') ;
  strEstados.Add('PR') ;
  strEstados.Add('RJ') ;
  strEstados.Add('RN') ;
  strEstados.Add('RO') ;
  strEstados.Add('RR') ;
  strEstados.Add('RS') ;
  strEstados.Add('SC') ;
  strEstados.Add('SE') ;
  strEstados.Add('SP') ;
  strEstados.Add('TO') ;

  if strEstados.IndexOf(qryEnderecocUF.Value) = -1 then
  begin
      ShowMessage('Unidade federativa inv�lida.') ;
      Abort ;
  end;

  if (qryMaster.FieldList[0].Value > 0) then
  begin
      qryValidaEndereco.Close ;
      qryValidaEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMaster.FieldList[0].Value ;
      qryValidaEndereco.Parameters.ParamByName('nCdTipOEnd').Value  := qryEndereconCdTipoEnd.Value  ;
      qryValidaEndereco.Parameters.ParamByName('nCdEndereco').Value := qryEndereconCdEndereco.Value ;
      qryValidaEndereco.Open ;

      if not qryValidaEndereco.eof then
      begin
          ShowMessage('J� existe um endere�o para este tipo. Troque o tipo ou altere o existente.') ;
          abort ;
      end ;

  end;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryEndereconCdTerceiro.Value := qryMasternCdTerceiro.Value ;
  qryEnderecocEndereco.Value   := UpperCase(qryEnderecocEndereco.Value) ;
  qryEnderecocBairro.Value     := UpperCase(qryEnderecocBairro.Value) ;
  qryEnderecocCidade.Value     := UpperCase(qryEnderecocCidade.Value) ;
  qryEnderecocUF.Value         := UpperCase(qryEnderecocUF.Value) ;
  qryEnderecocIE.Value         := UpperCase(qryEnderecocIE.Value) ;
  qryEnderecocIM.Value         := UpperCase(qryEnderecocIM.Value) ;
  qryEnderecocNmContato.Value  := UpperCase(qryEnderecocNmContato.Value) ;

  if (qryEndereco.State = dsEdit) and (qryEndereconCdStatus.Value <> 1) then
  begin
      case MessageDlg('Confirma o cancelamento deste endere�o ? Ap�s o cancelamento este endere�o n�o ser� mais visualizado.',mtConfirmation,mbYesNoCancel,0) of
          mrNo : begin
                     Abort;
                 end;
          mrCancel : begin
                         Abort;
                     end;
      end;
  end;

  inherited;

  if (qryEndereco.State = dsInsert) then
      qryEndereconCdEndereco.Value := frmMenu.fnProximoCodigo('ENDERECO') ;
end;

procedure TfrmFornecedor.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryEndereco.State = dsBrowse) then
             qryEndereco.Edit ;


        if (qryEndereco.State = dsInsert) or (qryEndereco.State = dsEdit) then
        begin

            if (DBGridEH2.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(772);

                If (nPK > 0) then
                    qryEndereconCdTabTipoLogradouro.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 9) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(20);

                If (nPK > 0) then
                begin
                    qryEndereconCdPais.Value := nPK ;
                end ;

            end ;

            if (DBGridEH2.Col = 11) then
            begin

                if (qryEnderecocxxNmPais.Value = '') then
                begin
                    MensagemAlerta('Selecione o pa�s.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(204,'Estado.nCdPais = ' + qryEndereconCdPais.asString);

                If (nPK > 0) then
                    qryEndereconCdEstado.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 13) then
            begin
                if (qryEnderecocxxNmEstado.Value = '') then
                begin
                    MensagemAlerta('Selecione o estado.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(205,'Municipio.nCdEstado = ' + qryEndereconCdEstado.asString);

                If (nPK > 0) then
                    qryEndereconCdMunicipio.Value := nPK ;

            end ;


            if (DBGridEH2.Col = 15) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(22);

                If (nPK > 0) then
                begin
                    qryEndereconCdTipoEnd.Value := nPK ;
                end ;

            end ;

            if (DBGridEH2.Col = 17) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(21);

                If (nPK > 0) then
                begin
                    qryEndereconCdRegiao.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.btSalvarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmFornecedor.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmFornecedor.ToolButton1Click(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmFornecedor.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(15);

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBanco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit22KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit23KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit28KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.DBEdit38KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(94);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEconomico.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFornecedor.qryContatoTerceiroBeforePost(DataSet: TDataSet);
begin

  if (qryContatoTerceirocNmContato.Value = '') then
  begin
      MensagemAlerta('Informe o nome do contato.') ;
      abort ;
  end ;

  qryContatoTerceirocNmContato.Value    := Uppercase(qryContatoTerceirocNmContato.Value) ;
  qryContatoTerceirocDepartamento.Value := Uppercase (qryContatoTerceirocDepartamento.Value) ;
  qryContatoTerceironCdTerceiro.Value   := qryMasternCdTerceiro.Value ;

  if (qryContatoTerceiro.State = dsInsert) then
      qryContatoTerceironCdContatoTerceiro.Value := frmMenu.fnProximocodigo('CONTATOTERCEIRO') ;

  inherited;


end;

procedure TfrmFornecedor.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryaux.SQL.Add('SELECT 1 FROM TerceiroTipoTerceiro WHERE nCdTipoTerceiro = 1 AND nCdTerceiro = ' + qryMasternCdTerceiro.asString) ;
  qryAux.Open ;

  if qryAux.eof then
  begin

      qryTerceiroTipoTerceiro.Open ;
      qryTerceiroTipoTerceiro.Insert ;
      qryTerceiroTipoTerceironCdTerceiro.Value     := qryMasternCdTerceiro.Value ;
      qryTerceiroTipoTerceironCdTipoTerceiro.Value := 1 ;
      qryTerceiroTipoTerceiro.Post ;
      qryTerceiroTipoTerceiro.Close ;

  end ;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryEndereco.Open ;

  PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.asString) ;

end;

procedure TfrmFornecedor.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  btSalvar.Click ;
end;

procedure TfrmFornecedor.DBEdit10Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdStatus.Value := 1 ;
end;

procedure TfrmFornecedor.DBEdit38Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  PosicionaQuery(qryGrupoEconomico, DBEdit38.Text) ;

end;

procedure TfrmFornecedor.DBEdit28Exit(Sender: TObject);
begin
  inherited;

  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit28.Text) ;
  
end;

procedure TfrmFornecedor.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  qryTerceiroTransp.Close ;
  PosicionaQuery(qryTerceiroTransp, DBEdit9.Text) ;
  
end;

procedure TfrmFornecedor.DBEdit22Exit(Sender: TObject);
begin
  inherited;

  qryMoeda.Close ;
  PosicionaQuery(qryMoeda, DBEdit22.Text) ;
  
end;

procedure TfrmFornecedor.ToolButton3Click(Sender: TObject);
var
    objForm : TfrmCentralArquivos;
begin
  inherited;
  if not qryMaster.Active then
      exit ;

  if (qryMasternCdTerceiro.Value > 0) then
  begin
      objForm := TfrmCentralArquivos.Create(Self);
      objForm.GerenciaArquivos('TERCEIRO',qryMasternCdTerceiro.Value);
  end;

end;

procedure TfrmFornecedor.qryTerceiroRepresentanteBeforePost(
  DataSet: TDataSet);
begin
  if ( qryTerceiroRepresentantecNmRepresentante.Value = '' ) then
  begin
      MensagemAlerta('Selecione um terceiro representante.') ;
      Abort;
  end ;

  inherited;

  if (qryTerceiroRepresentante.State = dsInsert) then
      qryTerceiroRepresentantenCdTerceiroRepresentante.Value := frmMenu.fnProximoCodigo('TERCEIROREPRESENTANTE');

  qryTerceiroRepresentantenCdTerceiro.Value := qryMasternCdTerceiro.Value;
end;

procedure TfrmFornecedor.qryTerceiroRepresentanteCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryTerceiroRepres.Close;
  PosicionaQuery(qryTerceiroRepres, qryTerceiroRepresentantenCdTerceiroRepres.AsString);

  qryTerceiroRepresentantecNmRepresentante.Value := qryTerceiroReprescNmTerceiro.Value;
end;

procedure TfrmFornecedor.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit] ) then
  begin
      qryMaster.Post;

      PosicionaQuery(qryTerceiroRepresentante, qryMasternCdTerceiro.AsString);
      
      qryTerceiroRepresentante.Active := True;
  end;


end;

procedure TfrmFornecedor.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (qryMaster.IsEmpty) then
            Exit;

        if (qryTerceiroRepresentante.State = dsBrowse) then
            qryTerceiroRepresentante.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
                qryTerceiroRepresentantenCdTerceiroRepres.Value := nPK ;

    end ;

  end ;

end;

procedure TfrmFornecedor.qryEnderecoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryEndereco.Active) then
  begin

      if (qryEndereconCdTabTipoLogradouro.Value > 0) then
      begin
          PosicionaQuery(qryTipoLogradouro,qryEndereconCdTabTipoLogradouro.AsString);
          qryEnderecocDescricaoLogradouro.Value := qryTipoLogradourocDescricao.Value;
      end;

      if (qryEndereconCdPais.Value > 0) then
      begin

          qryPais.Close;
          PosicionaQuery(qryPais, qryEndereconCdPais.AsString) ;

          if not qryPais.Eof then
              qryEnderecocxxNmPais.Value := qryPaiscNmPais.Value ;

      end ;

      if (qryEndereconCdEstado.Value > 0) then
      begin

          qryEstado.Close;
          qryEstado.Parameters.ParamByName('nCdPais').Value := qryEndereconCdPais.Value ;
          PosicionaQuery(qryEstado, qryEndereconCdEstado.AsString) ;

          if not qryEstado.Eof then
              qryEnderecocxxNmEstado.Value := qryEstadocUF.Value ;

      end ;

      if (qryEndereconCdMunicipio.Value > 0) then
      begin

          qryMunicipio.Close;
          qryMunicipio.Parameters.ParamByName('nCdEstado').Value := qryEndereconCdEstado.Value ;
          PosicionaQuery(qryMunicipio, qryEndereconCdMunicipio.AsString) ;

          if not qryMunicipio.Eof then
              qryEnderecocxxNmMunicipio.Value := qryMunicipiocNmMunicipio.Value ;

      end ;

      if qryEndereconCdTabTipoLogradouro.Value > 0 then
      begin
          PosicionaQuery(qryTipoLogradouro,qryEndereconCdTabTipoLogradouro.AsString);
          qryEnderecocDescricaoLogradouro.Value := qryTipoLogradourocDescricao.Value;
      end;
  end ;

end;

procedure TfrmFornecedor.DBGridEh2ColExit(Sender: TObject);
var
  I : integer;
begin
  inherited;

  if (qryEndereco.State in [dsInsert,dsEdit]) and (DBGridEH2.Col = 2) then
  begin
      qryBuscaEnderecoCEP.Close;
      PosicionaQuery(qryBuscaEnderecoCEP, qryEnderecocCEP.Value) ;

      if (not qryBuscaEnderecoCEP.Eof) then
      begin
          qryEnderecocEndereco.Value    := qryBuscaEnderecoCEPcEndereco.Value ;
          qryEnderecocBairro.Value      := qryBuscaEnderecoCEPcBairro.Value ;
          qryEndereconCdPais.Value      := qryBuscaEnderecoCEPnCdPais.Value ;
          qryEnderecocNmPais.Value      := qryBuscaEnderecoCEPcNmPais.Value ;
          qryEndereconCdEstado.Value    := qryBuscaEnderecoCEPnCdEstado.Value ;
          qryEnderecocUF.Value          := qryBuscaEnderecoCEPcUF.Value ;
          qryEndereconCdMunicipio.Value := qryBuscaEnderecoCEPnCdMunicipio.Value ;
          qryEnderecocCidade.Value      := qryBuscaEnderecoCEPcNmMunicipio.Value ;

          qryBuscaEnderecoCEP.Close ;
      end
      else
      begin
          if (Trim(qryEnderecocCEP.Value) = '') then
              Exit;

          { -- Efetua consulta atrav�s do componente ACBrCEP caso endere�o n�o exista na base de dados -- }
          { -- WebService: wsByJG / login: admsoft / senha: ed101580 / site: http://www.byjg.com.br -- }
          try
              ACBrCEP1.BuscarPorCEP(qryEnderecocCEP.Value);

              if (ACBrCEP1.Enderecos.Count < 1) then
                  MensagemAlerta('CEP n�o localizado em nossa base de endere�os.')
              else
              begin
                  for i := 0 to ACBrCEP1.Enderecos.Count-1 do
                  begin
                      with ACBrCEP1.Enderecos[i] do
                      begin
                          qryEnderecocEndereco.Value    := Logradouro ;
                          qryEnderecocComplemento.Value := Complemento ;
                          qryEnderecocBairro.Value      := Bairro ;
                          qryEnderecocCidade.Value      := Municipio ;
                          qryEnderecocUF.Value          := UF ;
                      end ;
                  end ;
              end ;
          except
              MensagemErro('Erro no processamento.');
              Raise;
          end;
      end;
  end ;
end;

initialization
    RegisterClass(tfrmFornecedor) ;

end.
