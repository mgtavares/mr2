inherited frmConvTitSP: TfrmConvTitSP
  Left = 443
  Top = 264
  Width = 336
  Height = 263
  BorderIcons = [biSystemMenu]
  Caption = 'Converter Provis'#227'o em SP'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 320
    Height = 196
  end
  object Label1: TLabel [1]
    Left = 34
    Top = 88
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
  end
  object Label2: TLabel [2]
    Left = 10
    Top = 112
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Recebimento'
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 136
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Vencimento'
  end
  object Label4: TLabel [4]
    Left = 21
    Top = 160
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Real T'#237'tulo'
  end
  object Label5: TLabel [5]
    Left = 45
    Top = 40
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
  end
  object Label6: TLabel [6]
    Left = 74
    Top = 64
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie'
  end
  object Label7: TLabel [7]
    Left = 32
    Top = 184
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero T'#237'tulo'
  end
  inherited ToolBar1: TToolBar
    Width = 320
    TabOrder = 2
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [9]
    Left = 104
    Top = 80
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 104
    Top = 104
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [11]
    Left = 104
    Top = 128
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object MaskEdit5: TMaskEdit [12]
    Left = 104
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'MaskEdit5'
    OnExit = MaskEdit5Exit
  end
  object Edit1: TEdit [13]
    Left = 104
    Top = 32
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 0
  end
  object Edit2: TEdit [14]
    Left = 104
    Top = 56
    Width = 25
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 1
  end
  object Edit3: TEdit [15]
    Left = 104
    Top = 176
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 7
  end
  object CheckBox1: TCheckBox [16]
    Left = 104
    Top = 200
    Width = 193
    Height = 17
    Caption = 'Documento de Cobran'#231'a Entregue'
    TabOrder = 8
  end
  object SP_CONVERTE_PROV_SP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONVERTE_PROV_SP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Size = 17
        Value = Null
      end
      item
        Name = '@cSerieTit'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@dDtEmissao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtReceb'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtVencto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nValTit'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@cNrNF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@cFlgDocCobranca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 184
    Top = 88
  end
end
