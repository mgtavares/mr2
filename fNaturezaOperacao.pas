unit fNaturezaOperacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, ER2Lookup;

type
  TfrmNaturezaOperacao = class(TfrmCadastro_Padrao)
    qryMasternCdNaturezaOperacao: TIntegerField;
    qryMastercNmNaturezaOperacao: TStringField;
    qryMastercCFOPInterno: TStringField;
    qryMastercCFOPExterno: TStringField;
    qryMastercFlgEntSai: TStringField;
    qryMastercFlgOpTerceiro: TIntegerField;
    qryMasternCdTipoOpTerceiro: TIntegerField;
    qryMastercFlgCalcICMS: TIntegerField;
    qryMastercFlgCalcIPI: TIntegerField;
    qryMastercMsgNF: TStringField;
    qryTipoOpTerceiro: TADOQuery;
    qryTipoOpTerceironCdTipoOpTerceiro: TIntegerField;
    qryTipoOpTerceirocNmTipoOpTerceiro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    rgOperacao: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    qryMastercNmTipoOpTerceiro: TStringField;
    qryMasternCdNaturezaOperacaoEntSai: TIntegerField;
    Label7: TLabel;
    qryVinculoNatOperacao: TADOQuery;
    qryVinculoNatOperacaonCdNaturezaOperacao: TIntegerField;
    qryVinculoNatOperacaocNmNaturezaOperacao: TStringField;
    qryVinculoNatOperacaocTipoNatOperacao: TStringField;
    DBEdit9: TDBEdit;
    dsVinculoNatOperacao: TDataSource;
    DBEdit10: TDBEdit;
    er2LkpNaturezaOperacaoEntSai: TER2LookupDBEdit;
    rgOpcao: TGroupBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    qryVinculoNatOperacaocFlgEntSai: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure er2LkpNaturezaOperacaoEntSaiExit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure rgOperacaoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNaturezaOperacao: TfrmNaturezaOperacao;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmNaturezaOperacao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'NATUREZAOPERACAO' ;
  nCdTabelaSistema  := 35 ;
  nCdConsultaPadrao := 74 ;
end;

procedure TfrmNaturezaOperacao.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) and (DBEdit5.ReadOnly = False)) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(75);

            If (nPK > 0) then
            begin
                qryMasternCdTipoOpTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmNaturezaOperacao.DBCheckBox1Click(Sender: TObject);
begin
  inherited;

  if (DBCheckBox1.Checked) then
  begin
      ativaDBEdit(DBEdit5);
      DBEdit5.SetFocus;
  end
  else desativaDBEdit(DBEdit5);

end;

procedure TfrmNaturezaOperacao.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  desativaDBEdit(DBEdit5);
  qryMastercFlgCalcICMS.Value := 1 ;
  qryMastercFlgCalcIPI.Value  := 1 ;
end;

procedure TfrmNaturezaOperacao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmNaturezaOperacao.Value = '') then
  begin
      ShowMessage('Informe a descri��o natureza da opera��o.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (qryMastercCFOPInterno.Value = '') then
  begin
      ShowMessage('Informe O CFOP interno.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryMastercCFOPExterno.Value = '') then
  begin
      ShowMessage('Informe o CFOP externo.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (qryMastercFlgEntSai.Value = '') then
  begin
      ShowMessage('Informe a Opera��o. (Entrada ou Sa�da)') ;
      rgOperacao.SetFocus;
      abort ;
  end ;

  if ((qryMastercFlgOpTerceiro.Value = 0) and (qryMasternCdTipoOpTerceiro.Value > 0)) then
  begin
      MensagemAlerta('Natureza de opera��o n�o permite opera��o de terceiros.' + #13 + 'Selecione a op��o de permiss�o ou remova o tipo de opera��o.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if ((qryMastercFlgOpTerceiro.Value = 1) and (qryMasternCdTipoOpTerceiro.Value = 0)) then
  begin
      MensagemAlerta('Natureza de opera��o permite opera��o de terceiros.' + #13 + 'Informe o tipo de opera��o ou desmarque a op��o de permiss�o.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (qryMastercFlgEntSai.Value = qryVinculoNatOperacaocFlgEntSai.Value) then
  begin
      case (rgOperacao.ItemIndex) of
          0 : MensagemAlerta('V�nculo da natureza de opera��o deve possuir a opera��o de SA�DA.');
          1 : MensagemAlerta('V�nculo da natureza de opera��o deve possuir a opera��o de ENTRADA.');
      end;

      er2LkpNaturezaOperacaoEntSai.SetFocus;

      Abort;
  end;

  inherited;

end;

procedure TfrmNaturezaOperacao.er2LkpNaturezaOperacaoEntSaiExit(
  Sender: TObject);
begin
  inherited;

  qryVinculoNatOperacao.Close;
  PosicionaQuery(qryVinculoNatOperacao, qryMasternCdNaturezaOperacaoEntSai.AsString);
end;

procedure TfrmNaturezaOperacao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryVinculoNatOperacao, qryMasternCdNaturezaOperacaoEntSai.AsString);

  if (qryMastercFlgOpTerceiro.Value > 0) then
       ativaDBEdit(DBEdit5)
  else desativaDBEdit(DBEdit5);
end;

procedure TfrmNaturezaOperacao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryVinculoNatOperacao.Close;
end;

procedure TfrmNaturezaOperacao.rgOperacaoChange(Sender: TObject);
begin
  inherited;

  er2LkpNaturezaOperacaoEntSai.WhereAdicional.Text := 'cFlgEntSai <> ' + #39 + rgOperacao.Value + #39;
  qryVinculoNatOperacao.Parameters.ParamByName('cFlgEntSai').Value := #39 + rgOperacao.Value + #39;
end;

initialization
    RegisterClass(TfrmNaturezaOperacao) ;

end.
