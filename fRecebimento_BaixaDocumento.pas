unit fRecebimento_BaixaDocumento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DBCtrls,
  StdCtrls, Mask, ER2Lookup;

type
  TfrmRecebimento_BaixaDocumento = class(TfrmProcesso_Padrao)
    qryRecebimento: TADOQuery;
    dsRecebimento: TDataSource;
    qryRecebimentonCdRecebimento: TIntegerField;
    qryRecebimentonCdLoja: TIntegerField;
    qryRecebimentonCdTerceiro: TIntegerField;
    qryRecebimentocNmTerceiro: TStringField;
    qryRecebimentocNrDocto: TStringField;
    qryRecebimentonValTotalNF: TBCDField;
    DBGridEh1: TDBGridEh;
    qryRecebimentocFlgDocEntregue: TIntegerField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    dsTerceiro: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label10: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    er2LkpTerceiro: TER2LookupMaskEdit;
    mskdDtInicial: TMaskEdit;
    mskdDtFinal: TMaskEdit;
    Label12: TLabel;
    er2LkpLoja: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    qryRecebimentodDtReceb: TDateTimeField;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaTabela();
  public
    { Public declarations }
  end;

var
  frmRecebimento_BaixaDocumento: TfrmRecebimento_BaixaDocumento;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TfrmRecebimento_BaixaDocumento.AtualizaTabela();
begin
  qryRecebimento.Close;
  qryRecebimento.Parameters.ParamByName('nCdTerceiro').Value     := frmMenu.ConvInteiro(er2LkpTerceiro.Text);
  qryRecebimento.Parameters.ParamByName('nCdLoja').Value         := frmMenu.ConvInteiro(er2LkpLoja.Text);
  qryRecebimento.Parameters.ParamByName('dDtInicial').Value      := frmMenu.ConvData(mskdDtInicial.Text);
  qryRecebimento.Parameters.ParamByName('dDtFinal').Value        := frmMenu.ConvData(mskdDtFinal.Text);
  qryRecebimento.Parameters.ParamByName('nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva;
  qryRecebimento.Open;
end;

procedure TfrmRecebimento_BaixaDocumento.ToolButton1Click(Sender: TObject);
begin
  //inherited;

  AtualizaTabela();
end;

procedure TfrmRecebimento_BaixaDocumento.DBGridEh1DblClick(Sender: TObject);
begin
  if (qryRecebimento.Eof) then
  begin
      ShowMessage('N�o existem Notas Fiscais selecionadas para realizar a opera��o, por favor verifique.');
      abort;
  end;

  inherited;

  if (MessageDlg('Confirma baixa de documento pendente deste recebimento ?', mtConfirmation, [mbYes,mbNo], 0) = mrNo) then
      Exit;

  qryRecebimento.Edit;
  qryRecebimentocFlgDocEntregue.Value := 1;
  qryRecebimento.Post;

  ShowMessage('Confirma��o de entrega de documento processada com sucesso.');

  AtualizaTabela();
end;

procedure TfrmRecebimento_BaixaDocumento.FormCreate(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      er2LkpLoja.ReadOnly := True;
      er2LkpLoja.Color    := $00E9E4E4;
  end;
end;

initialization
  RegisterClass(TfrmRecebimento_BaixaDocumento);

end.
