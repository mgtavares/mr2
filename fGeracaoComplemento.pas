unit fGeracaoComplemento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxLookAndFeelPainters, StdCtrls, cxButtons,
  GridsEh, DBGridEh, cxPC, cxControls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DB, ADODB, Mask, DBCtrls, DBGridEhGrouping;

type
  TfrmGeracaoComplemento = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTempItemNotaFiscal: TADOQuery;
    qryTempItemNotaFiscalnCdProduto: TIntegerField;
    qryTempItemNotaFiscalcNmItem: TStringField;
    qryTempItemNotaFiscalcCdST: TStringField;
    qryTempItemNotaFiscalcCFOP: TStringField;
    qryTempItemNotaFiscalcUnidadeMedida: TStringField;
    qryTempItemNotaFiscalnValUnitario: TBCDField;
    qryTempItemNotaFiscalnValTotal: TBCDField;
    qryTempItemNotaFiscalnValBaseICMS: TBCDField;
    qryTempItemNotaFiscalnAliqICMS: TBCDField;
    qryTempItemNotaFiscalnValICMS: TBCDField;
    qryTempItemNotaFiscalnValBaseIPI: TBCDField;
    qryTempItemNotaFiscalnAliqIPI: TBCDField;
    qryTempItemNotaFiscalcCdSTIPI: TStringField;
    qryTempItemNotaFiscalnValBaseICMSSub: TBCDField;
    qryTempItemNotaFiscalnValICMSSub: TBCDField;
    qryTempItemNotaFiscalnValIsenta: TBCDField;
    qryTempItemNotaFiscalnValOutras: TBCDField;
    qryTempItemNotaFiscalnValDespesa: TBCDField;
    qryTempItemNotaFiscalnValFrete: TBCDField;
    DataSource1: TDataSource;
    qryImportaItens: TADOQuery;
    qryAux: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryCalculoImposto: TADOQuery;
    qryCalculoImpostonCdCalculoImposto: TAutoIncField;
    qryCalculoImpostonValBaseICMS: TBCDField;
    qryCalculoImpostonValICMS: TBCDField;
    qryCalculoImpostonValBaseICMSSub: TBCDField;
    qryCalculoImpostonValICMSSub: TBCDField;
    qryCalculoImpostonValProduto: TBCDField;
    qryCalculoImpostonValFrete: TBCDField;
    qryCalculoImpostonValSeguro: TBCDField;
    qryCalculoImpostonValDesconto: TBCDField;
    qryCalculoImpostonValAcessorias: TBCDField;
    qryCalculoImpostonValIPI: TBCDField;
    qryCalculoImpostonValTotal: TBCDField;
    qryCalculoImpostocMotivo: TStringField;
    DataSource2: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    qryTempItemNotaFiscalnValIPI: TBCDField;
    qryTempItemNotaFiscalcClasseFiscal: TStringField;
    qryTempItemNotaFiscalnCdGrupoImposto: TIntegerField;
    SP_GERA_DOCTO_FISCAL_COMPL: TADOStoredProc;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonCdGrupoImposto: TIntegerField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryProdutocCdST: TStringField;
    qryProdutocCFOP: TStringField;
    qryProdutocNCM: TStringField;
    qryCalculoImpostocMotivo2: TStringField;
    DBEdit12: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure qryTempItemNotaFiscalBeforePost(DataSet: TDataSet);
    procedure qryCalculoImpostoBeforePost(DataSet: TDataSet);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdDoctoFiscal : integer;
  end;

var
  frmGeracaoComplemento: TfrmGeracaoComplemento;

implementation

uses fGeraDoctoFiscalCompl, fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmGeracaoComplemento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('TRUNCATE TABLE #TempItensNotaFiscal');
  qryAux.SQL.Add('TRUNCATE TABLE #TempCalculoImposto');
  qryAux.ExecSQL;

  qryTempItemNotaFiscal.Close;
  qryCalculoImposto.Close;
end;

procedure TfrmGeracaoComplemento.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTempItemNotaFiscal.State = dsBrowse) then
             qryTempItemNotaFiscal.Edit ;

        if (qryTempItemNotaFiscal.State = dsInsert) or (qryTempItemNotaFiscal.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(141,'EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto)');

            If (nPK > 0) then
            begin

                qryTempItemNotaFiscalnCdProduto.Value      := nPK ;
                qryProduto.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
                PosicionaQuery(qryProduto, qryTempItemNotaFiscalnCdProduto.asString) ;

                if not qryProduto.eof then
                begin
                    qryTempItemNotaFiscalcNmItem.Value         := qryProdutocNmProduto.Value ;
                    qryTempItemNotaFiscalnCdGrupoImposto.Value := qryProdutonCdGrupoImposto.Value;
                    qryTempItemNotaFiscalcClasseFiscal.Value   := qryProdutocNCM.Value;
                    qryTempItemNotaFiscalcUnidadeMedida.Value  := qryProdutocUnidadeMedida.Value;
                    qryTempItemNotaFiscalcCdST.Value           := qryProdutocCdST.Value;
                    qryTempItemNotaFiscalcCFOP.Value           := qryProdutocCFOP.Value;
                end;

            end ;

        end ;
     end;
  end ;

end;

procedure TfrmGeracaoComplemento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryTempItemNotaFiscal.Close;
  qryTempItemNotaFiscal.Open;

  if (qryTempItemNotaFiscal.RecordCount <= 0) then
  begin
      MensagemAlerta('N�o � poss�vel Gerar o Documento Fiscal Complementar, pois nenhum item foi adicionado.');
      ToolButton5.Click;
      abort;
  end;

  case MessageDlg('Deseja gerar o Documento Fiscal Complementar ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if (qryCalculoImposto.State = dsEdit) then
      qryCalculoImposto.Post;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_DOCTO_FISCAL_COMPL.Close;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nCdDoctoFiscal').Value  := nCdDoctoFiscal;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValBaseICMS').Value    := qryCalculoImpostonValBaseICMS.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValICMS').Value        := qryCalculoImpostonValICMS.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValBaseICMSSub').Value := qryCalculoImpostonValBaseICMSSub.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValICMSSub').Value     := qryCalculoImpostonValICMSSub.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValProduto').Value     := qryCalculoImpostonValProduto.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValFrete').Value       := qryCalculoImpostonValFrete.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValSeguro').Value      := qryCalculoImpostonValSeguro.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValDespesa').Value     := qryCalculoImpostonValAcessorias.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValIPI').Value         := qryCalculoImpostonValIPI.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@nValTotal').Value       := qryCalculoImpostonValTotal.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@cMotivoCompl').Value    := qryCalculoImpostocMotivo.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.Parameters.ParamByName('@cMotivoCompl2').Value   := qryCalculoImpostocMotivo2.Value;
      SP_GERA_DOCTO_FISCAL_COMPL.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Documento Complementar gerado com Sucesso.');

  Close;
end;

procedure TfrmGeracaoComplemento.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValBaseICMS.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit2.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValICMS.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit3.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValBaseICMSSub.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit4.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit1Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValICMSSub.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit1.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValProduto.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit5.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValFrete.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit6.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit7Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValSeguro.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit7.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit9Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValAcessorias.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit9.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit10Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValIPI.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit10.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.DBEdit11Exit(Sender: TObject);
begin
  inherited;
  if (qryCalculoImpostonValTotal.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit11.SetFocus;
      exit;
  end
end;

procedure TfrmGeracaoComplemento.qryTempItemNotaFiscalBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if (qryTempItemNotaFiscalcNmItem.Value = '') then
  begin
      MensagemAlerta('Preencha a descri��o do item.');
      abort;
  end
end;

procedure TfrmGeracaoComplemento.qryCalculoImpostoBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if (qryCalculoImpostonValBaseICMS.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit2.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValICMS.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit3.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValBaseICMSSub.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit4.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValICMSSub.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit1.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValProduto.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit5.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValFrete.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit6.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValSeguro.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit7.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValAcessorias.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit9.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValIPI.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit10.SetFocus;
      abort;
  end;

  if (qryCalculoImpostonValTotal.Value < 0) then
  begin
      MensagemAlerta('O valor deve ser maior ou igual a zero.');
      DBEdit11.SetFocus;
      abort;
  end;
end;

procedure TfrmGeracaoComplemento.ToolButton5Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Deseja realizar a importa��o dos itens do Documento Fiscal Gerador ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort ;
  end ;
  
  try
      qryImportaItens.Close;
      qryImportaItens.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
      qryImportaItens.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  if (qryImportaItens.RowsAffected > 0) then
      ShowMessage('Itens Importados com sucesso!')
  else MensagemAlerta('N�o existem itens para serem importados.');

  qryTempItemNotaFiscal.Close;
  qryTempItemNotaFiscal.Open;
end;

procedure TfrmGeracaoComplemento.ToolButton7Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Deseja excluir todos itens ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('DELETE FROM #TempItensNotaFiscal');
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  if (qryAux.RowsAffected > 0) then
      ShowMessage('Itens exlu�dos com sucesso!')
  else MensagemAlerta('N�o existem itens para serem excluidos.');

  qryTempItemNotaFiscal.Close;
  qryTempItemNotaFiscal.Open;
end;

procedure TfrmGeracaoComplemento.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmGeracaoComplemento.FormShow(Sender: TObject);
begin
  inherited;
  DBGridEh1.SetFocus;
end;

procedure TfrmGeracaoComplemento.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit3.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit4.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit1.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit5.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit6.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit7.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit9.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit10.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit11.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit12.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBEdit12KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit8.SetFocus;
  end;
end;

procedure TfrmGeracaoComplemento.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;
  if ((DBGridEh1.Col = 2) and (DBGridEh1.Fields[0].AsString <> '')) then
  begin
      if (qryTempItemNotaFiscal.State <> dsBrowse) then
      begin
          qryTempItemNotaFiscalnCdProduto.Value                     := DBGridEh1.Fields[0].Value;
          qryProduto.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
          PosicionaQuery(qryProduto, qryTempItemNotaFiscalnCdProduto.asString) ;

          if not qryProduto.eof then
          begin
              qryTempItemNotaFiscalcNmItem.Value         := qryProdutocNmProduto.Value ;
              qryTempItemNotaFiscalnCdGrupoImposto.Value := qryProdutonCdGrupoImposto.Value;
              qryTempItemNotaFiscalcClasseFiscal.Value   := qryProdutocNCM.Value;
              qryTempItemNotaFiscalcUnidadeMedida.Value  := qryProdutocUnidadeMedida.Value;
              qryTempItemNotaFiscalcCdST.Value           := qryProdutocCdST.Value;
              qryTempItemNotaFiscalcCFOP.Value           := qryProdutocCFOP.Value;
          end;
      end;
  end ;
end;

procedure TfrmGeracaoComplemento.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      VK_RETURN:
      DBEdit2.SetFocus;
  end;
end;

end.
