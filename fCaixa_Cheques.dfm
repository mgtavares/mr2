inherited frmCaixa_Cheques: TfrmCaixa_Cheques
  Left = 204
  Top = 178
  Width = 712
  Height = 394
  BorderIcons = []
  Caption = 'Cheques'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 696
    Height = 329
  end
  inherited ToolBar1: TToolBar
    Width = 696
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'F4-Processar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 101
      Visible = False
    end
  end
  object DBGridEh3: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 696
    Height = 329
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsTemp_Cheque
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clBlue
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = [fsBold]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghFooter3D, dghData3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBlack
    TitleFont.Height = -11
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh3Enter
    OnKeyUp = DBGridEh3KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'nCdAgencia'
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'cDigito'
        Footers = <>
        Width = 22
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 91
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 81
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'iAutorizacao'
        Footers = <>
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'dDtDeposito'
        Footers = <>
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'dDtLimite'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Footers = <>
        ReadOnly = True
        Width = 75
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 496
    Top = 272
  end
  object qryPrepara_Temp_Cheque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPrepara_Temp_ChequeBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Cheque_Pagamento'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Cheque_Pagamento (nCdBanco      int'
      '                               ,nCdAgencia    int'
      '                               ,nCdConta      int'
      '                               ,cDigito       CHAR(1)'
      '                               ,iNrCheque     int'
      
        '                               ,nValCheque    DECIMAL(12,2) defa' +
        'ult 0'
      '                               ,cCNPJCPF      CHAR(14)'
      '                               ,dDtDeposito   DATETIME'
      '                               ,dDtLimite     DATETIME'
      '                               ,iAutorizacao  int'
      '                               ,nCdTemp_Pagto int'
      
        '                               ,CONSTRAINT PK_Temp_Cheque_Pagame' +
        'nto PRIMARY KEY (nCdBanco, nCdAgencia, nCdConta, iNrCheque))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Cheque_Pagamento'
      ' WHERE nCdTemp_Pagto = :nPK'
      ' ORDER BY dDtDeposito')
    Left = 544
    Top = 272
    object qryPrepara_Temp_ChequenCdBanco: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Banco'
      FieldName = 'nCdBanco'
    end
    object qryPrepara_Temp_ChequenCdAgencia: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Ag'#234'ncia'
      FieldName = 'nCdAgencia'
    end
    object qryPrepara_Temp_ChequenCdConta: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Conta'
      FieldName = 'nCdConta'
    end
    object qryPrepara_Temp_ChequecDigito: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|DV'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryPrepara_Temp_ChequeiNrCheque: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryPrepara_Temp_ChequecCNPJCPF: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|CPF'
      FieldName = 'cCNPJCPF'
      FixedChar = True
      Size = 14
    end
    object qryPrepara_Temp_ChequenValCheque: TBCDField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Valor Cheque'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrepara_Temp_ChequedDtDeposito: TDateTimeField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
      EditMask = '!99/99/9999;1;_'
    end
    object qryPrepara_Temp_ChequedDtLimite: TDateTimeField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Data Limite'
      FieldName = 'dDtLimite'
    end
    object qryPrepara_Temp_ChequeiAutorizacao: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|C'#243'd. Autor.'
      FieldName = 'iAutorizacao'
    end
    object qryPrepara_Temp_ChequenCdTemp_Pagto: TIntegerField
      FieldName = 'nCdTemp_Pagto'
    end
  end
  object dsTemp_Cheque: TDataSource
    DataSet = qryPrepara_Temp_Cheque
    Left = 600
    Top = 272
  end
end
