inherited frmConsultaPedComAberTerceiro: TfrmConsultaPedComAberTerceiro
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Consulta de Pedido de Compra em Aberto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 137
    Width = 1152
    Height = 677
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    ButtonWidth = 71
    inherited ToolButton1: TToolButton
      Caption = 'Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 71
    end
    inherited ToolButton2: TToolButton
      Left = 79
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 108
    Align = alTop
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 52
      Top = 16
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label2: TLabel
      Tag = 1
      Left = 9
      Top = 40
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo Economico'
    end
    object Label3: TLabel
      Tag = 1
      Left = 62
      Top = 64
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = 'Marca'
    end
    object Label4: TLabel
      Tag = 1
      Left = 19
      Top = 88
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero Pedido'
    end
    object edtTerceiro: TMaskEdit
      Left = 96
      Top = 8
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnChange = edtTerceiroChange
      OnExit = edtTerceiroExit
      OnKeyDown = edtTerceiroKeyDown
    end
    object edtGrupoEconomico: TMaskEdit
      Left = 96
      Top = 32
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnChange = edtGrupoEconomicoChange
      OnExit = edtGrupoEconomicoExit
      OnKeyDown = edtGrupoEconomicoKeyDown
    end
    object edtMarca: TMaskEdit
      Left = 96
      Top = 56
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnChange = edtMarcaChange
      OnExit = edtMarcaExit
      OnKeyDown = edtMarcaKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 160
      Top = 8
      Width = 454
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 160
      Top = 32
      Width = 454
      Height = 21
      DataField = 'cNmGrupoEconomico'
      DataSource = DataSource2
      TabOrder = 4
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 160
      Top = 56
      Width = 454
      Height = 21
      DataField = 'cNmMarca'
      DataSource = DataSource3
      TabOrder = 5
    end
    object edtPedido: TMaskEdit
      Left = 96
      Top = 80
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 6
      Text = '      '
      OnChange = edtPedidoChange
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 137
    Width = 1152
    Height = 677
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsResultado
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 289
      end
      item
        EditButtons = <>
        FieldName = 'cNmGrupoEconomico'
        Footers = <>
        Width = 211
      end
      item
        EditButtons = <>
        FieldName = 'cNrPedTerceiro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmContato'
        Footers = <>
        Width = 207
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro, nCdGrupoEconomico'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 448
    Top = 216
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmGrupoEconomico'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 504
    Top = 216
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 552
    Top = 216
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 632
    Top = 376
  end
  object DataSource2: TDataSource
    DataSet = qryGrupoEconomico
    Left = 640
    Top = 384
  end
  object DataSource3: TDataSource
    DataSet = qryMarca
    Left = 648
    Top = 392
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdGrupoEconomico'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdMarca'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa        int'
      '       ,@nCdLoja           int'
      '       ,@nCdTerceiro       int'
      '       ,@nCdGrupoEconomico int'
      '       ,@nCdMarca          int'
      '       ,@nCdPedido         int'
      ''
      'Set @nCdEmpresa        = :nCdEmpresa'
      'Set @nCdLoja           = :nCdLoja'
      'Set @nCdTerceiro       = :nCdTerceiro'
      'Set @nCdGrupoEconomico = :nCdGrupoEconomico'
      'Set @nCdMarca          = :nCdMarca'
      'Set @nCdPedido         = :nCdPedido'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,Pedido.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Pedido.cNrPedTerceiro'
      '      ,Pedido.cNmContato'
      '      ,GrupoEconomico.cNmGrupoEconomico'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro       ON Terceiro.nCdTerceiro        ' +
        '     = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN GrupoEconomico ON GrupoEconomico.nCdGrupoEcono' +
        'mico = Terceiro.nCdGrupoEconomico'
      
        '       INNER JOIN TipoPedido     ON TipoPedido.nCdTipoPedido    ' +
        '     = Pedido.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed IN (3,4)'
      '   AND Pedido.nCdEmpresa = @nCdEmpresa'
      
        '   AND (   ((Pedido.nCdTerceiro         = @nCdTerceiro)       AN' +
        'D (@nCdTerceiro        > 0))'
      
        '        OR ((Terceiro.nCdGrupoEconomico = @nCdGrupoEconomico) AN' +
        'D (@nCdGrupoEconomico  > 0)))'
      '   AND (   (Pedido.nCdLoja  = @nCdLoja)'
      '        OR (Pedido.nCdLoja IS NULL))'
      '   AND EXISTS(SELECT 1'
      '                FROM ItemPedido'
      
        '                     LEFT JOIN Produto ON Produto.nCdProduto = I' +
        'temPedido.nCdProduto'
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND (nQtdePed-nQtdeExpRec-nQtdeCanc)>0'
      
        '                 AND ((@nCdMarca = 0) OR (Produto.nCdMarca = @nC' +
        'dMarca)))'
      '   AND ((@nCdPedido = 0) OR (Pedido.nCdPedido = @nCdPedido))'
      '   AND TipoPedido.cFlgCompra = 1'
      ' ORDER BY nCdPedido')
    Left = 448
    Top = 344
    object qryResultadonCdPedido: TIntegerField
      DisplayLabel = 'Pedido|N'#250'mero'
      FieldName = 'nCdPedido'
    end
    object qryResultadodDtPedido: TDateTimeField
      DisplayLabel = 'Pedido|Data Pedido'
      FieldName = 'dDtPedido'
    end
    object qryResultadonCdTerceiro: TIntegerField
      DisplayLabel = 'Terceiro/Fornecedor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryResultadocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro/Fornecedor|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryResultadocNrPedTerceiro: TStringField
      DisplayLabel = 'Pedido do|Fornecedor'
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryResultadocNmContato: TStringField
      DisplayLabel = 'Contato'
      FieldName = 'cNmContato'
      Size = 35
    end
    object qryResultadocNmGrupoEconomico: TStringField
      DisplayLabel = 'Terceiro/Fornecedor|Grupo Econ'#244'mico'
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 488
    Top = 344
  end
end
