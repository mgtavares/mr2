inherited frmSyncInfra: TfrmSyncInfra
  Left = 135
  Top = 146
  Caption = 'Sincroniza'#231#227'o Infra Estrutura'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 8
    Top = 48
    Width = 127
    Height = 13
    Caption = 'Endere'#231'o Servidor Destino'
  end
  object Label2: TLabel [2]
    Left = 88
    Top = 72
    Width = 46
    Height = 13
    Caption = 'Database'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object Edit1: TEdit [4]
    Left = 136
    Top = 40
    Width = 377
    Height = 21
    TabOrder = 1
  end
  object Edit2: TEdit [5]
    Left = 136
    Top = 64
    Width = 377
    Height = 21
    TabOrder = 2
  end
  object ckLookup: TCheckBox [6]
    Left = 8
    Top = 112
    Width = 97
    Height = 17
    Caption = 'Lookup'
    TabOrder = 3
  end
  object ckTabelaSistema: TCheckBox [7]
    Left = 8
    Top = 136
    Width = 97
    Height = 17
    Caption = 'Tabela Sistema'
    TabOrder = 4
  end
  object ckUltimoCodigo: TCheckBox [8]
    Left = 8
    Top = 160
    Width = 97
    Height = 17
    Caption = #218'ltimo C'#243'digo'
    TabOrder = 5
  end
  object ckAplicacao: TCheckBox [9]
    Left = 8
    Top = 184
    Width = 97
    Height = 17
    Caption = 'Aplica'#231#245'es'
    TabOrder = 6
  end
  object ConnDestino: TADOConnection
    Left = 536
    Top = 56
  end
  object qryLookup: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabelaConsulta')
    Left = 160
    Top = 112
    object qryLookupnCdTabelaConsulta: TIntegerField
      FieldName = 'nCdTabelaConsulta'
    end
    object qryLookupcNmTabelaConsulta: TStringField
      FieldName = 'cNmTabelaConsulta'
      Size = 50
    end
    object qryLookupComandoSQL: TMemoField
      FieldName = 'ComandoSQL'
      BlobType = ftMemo
    end
  end
  object qryFiltroTabelaConsulta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM FiltroTabelaConsulta'
      'WHERE nCdTabelaConsulta = :nPK')
    Left = 128
    Top = 112
    object qryFiltroTabelaConsultanCdTabelaConsulta: TIntegerField
      FieldName = 'nCdTabelaConsulta'
    end
    object qryFiltroTabelaConsultacNmCampoTabela: TStringField
      FieldName = 'cNmCampoTabela'
      Size = 50
    end
    object qryFiltroTabelaConsultacTipoDado: TStringField
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryFiltroTabelaConsultacNmCampoTela: TStringField
      FieldName = 'cNmCampoTela'
      Size = 50
    end
  end
  object qryAux: TADOQuery
    Parameters = <>
    Left = 8
    Top = 80
  end
  object qryLookupDestino: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabelaConsulta')
    Left = 264
    Top = 104
    object qryLookupDestinonCdTabelaConsulta: TIntegerField
      FieldName = 'nCdTabelaConsulta'
    end
    object qryLookupDestinocNmTabelaConsulta: TStringField
      FieldName = 'cNmTabelaConsulta'
      Size = 50
    end
    object qryLookupDestinoComandoSQL: TMemoField
      FieldName = 'ComandoSQL'
      BlobType = ftMemo
    end
  end
  object qryFiltroTabelaConsultaDestino: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM FiltroTabelaConsulta')
    Left = 296
    Top = 104
    object qryFiltroTabelaConsultaDestinonCdTabelaConsulta: TIntegerField
      FieldName = 'nCdTabelaConsulta'
    end
    object qryFiltroTabelaConsultaDestinocNmCampoTabela: TStringField
      FieldName = 'cNmCampoTabela'
      Size = 50
    end
    object qryFiltroTabelaConsultaDestinocTipoDado: TStringField
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryFiltroTabelaConsultaDestinocNmCampoTela: TStringField
      FieldName = 'cNmCampoTela'
      Size = 50
    end
  end
  object qryTabelaSistema: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabelaSistema')
    Left = 128
    Top = 152
    object qryTabelaSistemanCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryTabelaSistemacTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
    object qryTabelaSistemacNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
  end
  object qryTabelaSistemaDestino: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM TabelaSistema')
    Left = 264
    Top = 152
    object qryTabelaSistemaDestinonCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryTabelaSistemaDestinocTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
    object qryTabelaSistemaDestinocNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
  end
  object qryUltimoCodigo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM UltimoCodigo')
    Left = 128
    Top = 192
    object qryUltimoCodigocNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
    object qryUltimoCodigoiUltimoCodigo: TIntegerField
      FieldName = 'iUltimoCodigo'
    end
  end
  object qryUltimoCodigoDestino: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM UltimoCodigo')
    Left = 264
    Top = 192
    object qryUltimoCodigoDestinocNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
    object qryUltimoCodigoDestinoiUltimoCodigo: TIntegerField
      FieldName = 'iUltimoCodigo'
    end
  end
  object qryAplicacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM Aplicacao')
    Left = 128
    Top = 232
    object qryAplicacaonCdAplicacao: TIntegerField
      FieldName = 'nCdAplicacao'
    end
    object qryAplicacaocNmAplicacao: TStringField
      FieldName = 'cNmAplicacao'
      Size = 50
    end
    object qryAplicacaonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAplicacaonCdModulo: TIntegerField
      FieldName = 'nCdModulo'
    end
    object qryAplicacaocNmObjeto: TStringField
      FieldName = 'cNmObjeto'
      Size = 50
    end
  end
  object qryAplicacaoDestino: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM Aplicacao')
    Left = 264
    Top = 232
    object qryAplicacaoDestinonCdAplicacao: TIntegerField
      FieldName = 'nCdAplicacao'
    end
    object qryAplicacaoDestinocNmAplicacao: TStringField
      FieldName = 'cNmAplicacao'
      Size = 50
    end
    object qryAplicacaoDestinonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAplicacaoDestinonCdModulo: TIntegerField
      FieldName = 'nCdModulo'
    end
    object qryAplicacaoDestinocNmObjeto: TStringField
      FieldName = 'cNmObjeto'
      Size = 50
    end
  end
end
