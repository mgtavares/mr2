unit fListaCobranca_Itens_SelCliCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmListaCobranca_Itens_SelCliCobradora = class(TfrmProcesso_Padrao)
    qryPreparaTemp: TADOQuery;
    qryTempTerceiroNegativar: TADOQuery;
    qryTempTerceiroNegativarnCdTerceiro: TIntegerField;
    qryTempTerceiroNegativarcCNPJCPF: TStringField;
    qryTempTerceiroNegativarcNmTerceiro: TStringField;
    qryTempTerceiroNegativarcFlgAux: TIntegerField;
    DBGridEh1: TDBGridEh;
    dsTempTerceiroNegativar: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobranca : integer ;
  end;

var
  frmListaCobranca_Itens_SelCliCobradora: TfrmListaCobranca_Itens_SelCliCobradora;

implementation

uses fMenu, rTituloNegativado_view, fListaCobranca_Itens_EnvioCobradora,
  rFichaCobradora_view;

{$R *.dfm}

procedure TfrmListaCobranca_Itens_SelCliCobradora.ToolButton1Click(
  Sender: TObject);
var
    i : integer ;
    objForm : TfrmListaCobranca_Itens_EnvioCobradora;
begin
  inherited;

  if (qryTempTerceiroNegativar.State <> dsBrowse) then
      qryTempTerceiroNegativar.Post ;

  qryTempTerceiroNegativar.First ;

  i := 0 ;
  
  while not qryTempTerceiroNegativar.Eof do
  begin
      if (qryTempTerceiroNegativarcFlgAux.Value = 1) then
          i := i + 1 ;

      qryTempTerceiroNegativar.Next ;
  end ;

  qryTempTerceiroNegativar.First ;

  if (i=0) then
  begin
      MensagemErro('Nenhum cliente selecionado para envio.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma o envio dos t�tulos destes clientes para cobradora ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryTempTerceiroNegativar.First ;

  {--Abre a tela para selecionar a cobradora--}
  objForm := TfrmListaCobranca_Itens_EnvioCobradora.Create(nil);

  objForm.nCdListaCobranca := nCdListaCobranca;
  showForm(objForm,false);

  if (objForm.bProcessado) then
  begin
      ToolButton5.Click;
      Close ;
  end ;

end;

procedure TfrmListaCobranca_Itens_SelCliCobradora.ToolButton5Click(
  Sender: TObject);
var
  objRel : TrptFichaCobradora_view;
begin
  inherited;

  objRel := TrptFichaCobradora_view.Create(nil);

  try
      try
          PosicionaQuery(objRel.qryFicha,intToStr(nCdListaCobranca)) ;
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      freeAndNil(objRel) ;
  end;

end;

end.
