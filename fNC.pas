unit fNC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask;

type
  TfrmNC = class(TfrmProcesso_Padrao)
    MaskEdit4: TMaskEdit;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nPerTaxa1  : double ;
    nPerTaxa2  : double ;
    end;

var
  frmNC: TfrmNC;

implementation

{$R *.dfm}

procedure TfrmNC.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  If (trim(MaskEdit3.Text) <> '') then
      MaskEdit3.text := formatcurr('##,##0.00',StrToFloat(StringReplace(MaskEdit3.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;


end;

procedure TfrmNC.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  If (trim(MaskEdit4.Text) <> '') then
      MaskEdit4.text := formatcurr('##,##0.00',StrToFloat(StringReplace(MaskEdit4.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmNC.FormShow(Sender: TObject);
begin
  inherited;
//  MaskEdit3.Text := '100' ;
//  MaskEdit4.Text := '0' ;
  MaskEdit3.SetFocus;

end;

procedure TfrmNC.ToolButton1Click(Sender: TObject);
begin
  inherited;

  If (Trim(MaskEdit3.Text) = '') then
      MaskEdit3.Text := '0' ;

  If (Trim(MaskEdit4.Text) = '') then
      MaskEdit4.Text := '0' ;

If ( nPerTaxa1<>StrToFloat(Trim(MaskEdit3.Text))) or
       ( nPerTaxa2<>StrToFloat(Trim(MaskEdit4.Text))) then
begin
  if  ( StrToFloat(Trim(MaskEdit3.Text)) + StrToFloat(Trim(MaskEdit4.Text)) ) <> 100   then
  begin
    ShowMessage('A soma dos valores deve ser igual a 100.') ;
    abort ;
  end ;
end;
  Close ;

end;

procedure TfrmNC.MaskEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  case key of
    13 : Begin
        MaskEdit4.SetFocus ;
    end ;

  end ;

end;

procedure TfrmNC.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  case key of
    13 : Begin
        ToolButton1.Click ;
    end ;

  end ;

end;

end.
