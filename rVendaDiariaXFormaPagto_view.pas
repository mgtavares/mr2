unit rVendaDiariaXFormaPagto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaDiariaXFormaPagto_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand5: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText8: TQRDBText;
    QRShape3: TQRShape;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape1: TQRShape;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    lblEmpresa: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel7: TQRLabel;
    QRSysData3: TQRSysData;
    QRSysData4: TQRSysData;
    lblFiltro: TQRLabel;
    usp_Relatorio: TADOStoredProc;
    QRGroup1: TQRGroup;
    QRDBText2: TQRDBText;
    QRBand4: TQRBand;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRLabel6: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape2: TQRShape;
    QRBand2: TQRBand;
    QRLabel9: TQRLabel;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRShape4: TQRShape;
    QRLabel10: TQRLabel;
    QRDBText11: TQRDBText;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    QRShape5: TQRShape;
    usp_RelatoriocNmLoja: TStringField;
    usp_RelatoriodDtVenda: TDateTimeField;
    usp_RelatorionValOper: TBCDField;
    usp_RelatorionValDinheiro: TBCDField;
    usp_RelatorionValCheque: TBCDField;
    usp_RelatorionValCartao: TBCDField;
    usp_RelatorionValTEF: TBCDField;
    usp_RelatorionValCrediario: TBCDField;
    usp_RelatorionValOutros: TBCDField;
    usp_RelatorionValRecPrest: TBCDField;
    usp_RelatorionValDescontoCondPagto: TBCDField;
    usp_RelatorionValJurosCondPagto: TBCDField;
    QRDBText12: TQRDBText;
    QRLabel11: TQRLabel;
    QRExpr19: TQRExpr;
    QRExpr20: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaDiariaXFormaPagto_view: TrptVendaDiariaXFormaPagto_view;

implementation

{$R *.dfm}

uses
  fMenu;

end.
