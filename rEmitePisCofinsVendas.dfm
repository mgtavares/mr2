inherited frmEmitePisCofinsVendas: TfrmEmitePisCofinsVendas
  Left = 100
  Top = 127
  Caption = 'frmEmitePisCofinsVendas'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 63
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 15
    Top = 96
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Vendas'
  end
  object Label6: TLabel [3]
    Left = 191
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label9: TLabel [4]
    Tag = 1
    Left = 84
    Top = 70
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit3: TMaskEdit [6]
    Left = 107
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 179
    Top = 40
    Width = 67
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 5
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 251
    Top = 40
    Width = 652
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 6
  end
  object MaskEdit1: TMaskEdit [9]
    Left = 107
    Top = 88
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 219
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object RadioGroup1: TRadioGroup [11]
    Left = 107
    Top = 124
    Width = 200
    Height = 46
    Caption = 'Modo de Exibi'#231#227'o'
    TabOrder = 7
  end
  object RadioButton1: TRadioButton [12]
    Left = 114
    Top = 144
    Width = 73
    Height = 17
    Caption = 'Anal'#237'tico'
    TabOrder = 8
  end
  object RadioButton2: TRadioButton [13]
    Left = 198
    Top = 144
    Width = 85
    Height = 17
    Caption = 'Sint'#233'tico'
    TabOrder = 9
  end
  object MaskEdit7: TMaskEdit [14]
    Left = 107
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit1: TDBEdit [15]
    Tag = 1
    Left = 179
    Top = 64
    Width = 649
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 10
  end
  inherited ImageList1: TImageList
    Left = 792
    Top = 72
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 528
    Top = 112
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 488
    Top = 144
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 528
    Top = 141
  end
end
