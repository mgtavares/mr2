inherited frmArvorePlanoConta: TfrmArvorePlanoConta
  Left = 180
  Top = 156
  BorderIcons = [biSystemMenu]
  Caption = 'Estrutura Plano de Contas'
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      object cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn
        DataBinding.FieldName = 'cNmGrupoPlanoConta'
        Visible = False
        GroupIndex = 0
      end
      object cxGrid1DBTableView1nCdPlanoConta: TcxGridDBColumn
        DataBinding.FieldName = 'nCdPlanoConta'
      end
      object cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn
        DataBinding.FieldName = 'cNmPlanoConta'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 504
    Top = 56
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      
        'SELECT (dbo.fn_ZeroEsquerda(GrupoPlanoConta.iSeqDRE,3) + '#39' - '#39' +' +
        ' GrupoPlanoConta.cNmGrupoPlanoConta) as cNmGrupoPlanoConta'
      '      ,PlanoConta.nCdPlanoConta'
      '      ,PlanoConta.cNmPlanoConta'
      '  FROM PlanoConta'
      
        '       INNER JOIN GrupoPlanoConta ON GrupoPlanoConta.nCdGrupoPla' +
        'noConta = PlanoConta.nCdGrupoPlanoConta'
      ' ORDER BY GrupoPlanoConta.iSeqDRE'
      '         ,cNmPlanoConta')
    Left = 336
    Top = 200
    object qryPlanoContacNmGrupoPlanoConta: TStringField
      DisplayLabel = 'Grupo Conta'
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryPlanoContanCdPlanoConta: TIntegerField
      DisplayLabel = 'C'#243'd'
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      DisplayLabel = 'Conta'
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryPlanoConta
    Left = 384
    Top = 240
  end
end
