unit fDadoComplDoctoFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Mask,
  StdCtrls, DB, ADODB;

type
  TfrmDadoComplDoctoFiscal = class(TfrmProcesso_Padrao)
    edtnCdTransp: TEdit;
    edtcNmTransp: TEdit;
    edtCNPJ: TEdit;
    edtIE: TEdit;
    edtEndereco: TEdit;
    edtBairro: TEdit;
    edtUF: TEdit;
    edtPlacaVeic: TEdit;
    edtUFVeic: TEdit;
    edtVolume: TMaskEdit;
    edtPesoBruto: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtCidade: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    edtPesoLiquido: TMaskEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    StaticText2: TStaticText;
    qryTransp: TADOQuery;
    qryTranspnCdEndereco: TAutoIncField;
    qryTranspcNmTerceiro: TStringField;
    qryTranspcEndereco: TStringField;
    qryTranspcBairro: TStringField;
    qryTranspcCidade: TStringField;
    qryTranspcUF: TStringField;
    qryTranspcCNPJCPF: TStringField;
    qryTranspcIE: TStringField;
    qryTranspnCdTerceiro: TIntegerField;
    qryAux: TADOQuery;
    SP_ATUALIZA_DADO_COMPL: TADOStoredProc;
    edtDtSaida: TMaskEdit;
    Label14: TLabel;
    Label15: TLabel;
    edtEspecieTransp: TEdit;
    procedure edtVolumeExit(Sender: TObject);
    procedure edtPesoBrutoExit(Sender: TObject);
    procedure edtPesoLiquidoExit(Sender: TObject);
    procedure edtcNmTranspKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTranspAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro    : integer ;
    nCdDoctoFiscal : integer ;
    bResultado     : boolean ;
    bProcessado    : boolean;
  end;

var
  frmDadoComplDoctoFiscal: TfrmDadoComplDoctoFiscal;

implementation

uses fLookup_Padrao, fImpDoctoFiscal, fMenu;

{$R *.dfm}

procedure TfrmDadoComplDoctoFiscal.edtVolumeExit(Sender: TObject);
begin
  inherited;
  If (trim(EdtVolume.Text) <> '') then
      EdtVolume.text := formatcurr('0.00',StrToFloat(StringReplace(EdtVolume.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmDadoComplDoctoFiscal.edtPesoBrutoExit(Sender: TObject);
begin
  inherited;
  If (trim(EdtpesoBruto.Text) <> '') then
      EdtpesoBruto.text := formatcurr('0.00',StrToFloat(StringReplace(EdtpesoBruto.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmDadoComplDoctoFiscal.edtPesoLiquidoExit(Sender: TObject);
begin
  inherited;
  If (trim(EdtpesoLiquido.Text) <> '') then
      EdtpesoLiquido.text := formatcurr('0.00',StrToFloat(StringReplace(EdtpesoLiquido.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmDadoComplDoctoFiscal.edtcNmTranspKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(83);

        If (nPK > 0) then
        begin
            PosicionaQuery(qryTransp, IntToStr(nPK)) ;
        end ;

    end ;

  end ;

end;

procedure TfrmDadoComplDoctoFiscal.qryTranspAfterScroll(DataSet: TDataSet);
begin
  inherited;

  edtnCdTransp.Text := qryTranspnCdTerceiro.AsString;
  edtcNmTransp.Text := qryTranspcNmTerceiro.AsString;
  edtCNPJ.Text      := qryTranspcCNPJCPF.AsString;
  edtIE.Text        := qryTranspcIE.AsString;
  edtEndereco.Text  := qryTranspcEndereco.AsString;
  edtBairro.Text    := qryTranspcBairro.AsString;
  edtCidade.Text    := qryTranspcCidade.AsString;
  edtUF.Text        := qryTranspcUF.AsString ;

end;

procedure TfrmDadoComplDoctoFiscal.FormShow(Sender: TObject);
begin
  inherited;

  If (nCdTerceiro > 0) then
  begin
      qryAux.Close ;
      qryAux.SQL.Add('SELECT TOP 1 nCdEndereco FROM Endereco WHERE nCdStatus = 1 AND nCdTerceiro = ' + intToStr(nCdTerceiro)) ;
      qryAux.Open ;

      if not qryaux.Eof then
      begin
          PosicionaQuery(qryTransp,qryAux.FieldList[0].AsString) ;
      end ;

      qryAux.Close ;

  end ;

  edtVolume.SetFocus;
end;

procedure TfrmDadoComplDoctoFiscal.ToolButton1Click(Sender: TObject);
begin

  bResultado  := False ;

  if (Trim(edtVolume.Text) = '') then
      edtVolume.Text := '0' ;

  if (Trim(edtPesoBruto.Text) = '') then
      edtPesoBruto.Text := '0' ;

  if (Trim(edtPesoLiquido.Text) = '') then
      edtPesoLiquido.Text := '0' ;

  if ((Trim(edtDtSaida.Text)) = '/  /') then
  begin
      MensagemAlerta('Informe a Data a Sa�da.');
      edtDtSaida.SetFocus;
      exit;
  end;

  if ((length((Trim(edtDtSaida.Text))) < 10)) then
  begin
      MensagemAlerta('Informe a Data a Sa�da.');
       edtDtSaida.SetFocus;
      exit;
  end;

  if ((strToDate(edtDtSaida.Text)) < Date()) then
  begin
      MensagemAlerta('Data de Sa�da menor que data do dia.');
      edtDtSaida.SetFocus;
      exit;
  end;

  if (strToFloat(edtPesoLiquido.Text) > strToFloat(edtPesoBruto.Text)) then
  begin
      ShowMessage('Peso L�quido superior ao peso bruto.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a impress�o deste documento ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_ATUALIZA_DADO_COMPL.Close ;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@nCdDoctoFiscal').Value    := nCdDoctoFiscal ;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@nCdTerceiroTransp').Value := edtnCdTransp.Text ;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cNmTransp').Value         := edtcNmTransp.Text ;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cCNPJ').Value             := edtCNPJ.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cIE').Value               := edtIE.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cEndereco').Value         := edtEndereco.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cBairro').Value           := edtBairro.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cCidade').Value           := edtCidade.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cUF').Value               := edtUF.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cPlaca').Value            := edtPlacaVeic.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cUFPlaca').Value          := edtUFVeic.Text;
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@iVolume').Value           := StrToFloat(edtVolume.Text);
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@nPesoBruto').Value        := StrToFloat(edtPesoBruto.Text);
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@nPesoLiquido').Value      := StrToFloat(edtPesoLiquido.Text);
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@dDtSaida').Value          := Trim(edtDtSaida.Text);
      SP_ATUALIZA_DADO_COMPL.Parameters.ParamByName('@cEspecieTransp').Value    := edtEspecieTransp.Text;
      SP_ATUALIZA_DADO_COMPL.ExecProc;
  except
      frmMenu.Connection.RollBackTrans;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  bResultado  := True;
  bProcessado := True;

  Close ;

end;

procedure TfrmDadoComplDoctoFiscal.FormCreate(Sender: TObject);
begin
  inherited;

  bProcessado := False;
end;

end.
