unit fEmbFormula;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmEmbFormula = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    usp_Grade: TADOStoredProc;
    usp_GradenCdProdutoFormulado: TIntegerField;
    usp_GradeiColuna: TIntegerField;
    usp_GradenCdDepartamento: TIntegerField;
    usp_GradecNmDepartamento: TStringField;
    usp_GradenQtdeProduto: TBCDField;
    usp_GradenQtdeAmostra: TBCDField;
    usp_GradecApelidoProduto: TStringField;
    usp_GradecApelidoAmostra: TStringField;
    usp_GradenCdProduto: TIntegerField;
    usp_GradenCdAmostra: TIntegerField;
    usp_GradenCdFormulaColuna: TIntegerField;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    qryTempnCdProdutoFormulado: TIntegerField;
    qryTempiColuna: TIntegerField;
    qryTempnCdDepartamento: TIntegerField;
    qryTempcNmDepartamento: TStringField;
    qryTempnQtdeProduto: TBCDField;
    qryTempnQtdeAmostra: TBCDField;
    qryTempcApelidoProduto: TStringField;
    qryTempcApelidoAmostra: TStringField;
    qryTempnCdProduto: TIntegerField;
    qryTempnCdAmostra: TIntegerField;
    qryTempnCdFormulaColuna: TIntegerField;
    qryTempnValUnitProduto: TBCDField;
    qryTempnValUnitAmostra: TBCDField;
    qryFormulaColunaProduto: TADOQuery;
    qryFormulaColunaProdutonCdProduto: TIntegerField;
    qryZeraCodigo: TADOQuery;
    sp_tab_preco: TADOStoredProc;
    qryPosicaoEstoque: TADOQuery;
    qryPosicaoEstoquenQtdeDisponivel: TBCDField;
    qryPosicaoEstoquenQtdeEstoqueEmpenhado: TBCDField;
    qryTempnEstoqueProduto: TBCDField;
    qryTempnEstoqueAmostra: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryTempnValDescontoProd: TBCDField;
    qryTempnValDescontoAmos: TBCDField;
    qryTempnValAcrescimoProd: TBCDField;
    qryTempnValAcrescimoAmos: TBCDField;
    qryTempnValUnitarioProdEsp: TBCDField;
    qryPrecoAux: TADOQuery;
    qryPrecoAuxnPrecoAux: TBCDField;
    procedure ToolButton1Click(Sender: TObject);
    procedure RenderizaGrade();
    procedure FormShow(Sender: TObject);
    procedure qryTempAfterScroll(DataSet: TDataSet);
    function ValidaEmbalagem(): boolean;
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido       : integer ;
    nCdItemPedido   : integer ;
    nCdProduto      : integer ;
    nCdTabStatusPed : integer ;
    nCdTerceiro     : integer ;
    cFlgTipoFat     : integer ;
    bEdit           : boolean ;
    bWERP           : boolean ;
  end;

var
  frmEmbFormula: TfrmEmbFormula;

implementation

uses fMenu;

{$R *.dfm}


procedure TfrmEmbFormula.RenderizaGrade();
begin
    qryTemp.Close ;
    qryTemp.Open  ;

    usp_Grade.Close ;
    usp_Grade.Parameters.ParamByName('@nCdPedido').Value           := nCdPedido     ;
    usp_Grade.Parameters.ParamByName('@nCdItemPedido').Value       := nCdItemPedido ;
    usp_Grade.Parameters.ParamByName('@nCdProdutoFormulado').Value := nCdProduto    ;
    usp_Grade.ExecProc ;

    qryTemp.Close ;
    qryTemp.Open  ;

    bEdit := True ;

    Self.ShowModal;

end;

function TfrmEmbFormula.ValidaEmbalagem(): boolean;
var
    bSemEstoque : boolean ;
begin

    bEdit       := False ;
    bSemEstoque := false ;

    qryTemp.First ;

    while not qryTemp.Eof do
    begin

        qryTemp.Edit ;

        qryTempnCdProduto.Value := 0 ;
        qryTempnCdAmostra.Value := 0 ;

        if (Trim(qryTempcApelidoProduto.Value) <> '') then
        begin

          qryFormulaColunaProduto.Close ;
          qryFormulaColunaProduto.Parameters.ParamByName('nCdFormulaColuna').Value := qryTempnCdFormulaColuna.Value ;
          qryFormulaColunaProduto.Parameters.ParamByName('cFlgProdAmostra').Value := 'P'                            ;
          qryFormulaColunaProduto.Parameters.ParamByName('cApelido').Value         := qryTempcApelidoProduto.Value  ;
          qryFormulaColunaProduto.Open ;

          if qryFormulaColunaProduto.Eof then
          begin
            MensagemAlerta('Apelido de produto da coluna ' + qryTempiColuna.AsString + ' n�o encontrado na lista de produtos permitidos.') ;
            Result := false;
            bEdit  := True ;
            exit ;
          end ;

          qryTempnCdProduto.Value := qryFormulaColunaProdutonCdProduto.Value ;

          if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') or (qryTempnValUnitProduto.Value = 0) then
          begin

              sp_tab_preco.Close;
              sp_tab_preco.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido ;
              sp_tab_preco.Parameters.ParamByName('@nCdProduto').Value := qryTempnCdProduto.Value ;
              sp_tab_preco.ExecProc;

              if (sp_tab_preco.Parameters.ParamByName('@nValor').Value = -1.00 ) then
              begin

                  if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') then
                  begin
                      MensagemAlerta('Tabela de pre�o n�o encontrada. Coluna ' + qryTempiColuna.AsString) ;
                      Result := false;
                      bEdit  := True ;
                      exit ;
                  end ;

              end ;

              if (sp_tab_preco.Parameters.ParamByName('@nValor').Value > 0) then
              begin

                  qryTempnValUnitProduto.Value   := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
                  qryTempnValDescontoProd.Value  := sp_tab_preco.Parameters.ParamByName('@nValDesconto').Value  ;
                  qryTempnValAcrescimoProd.Value := sp_tab_preco.Parameters.ParamByName('@nValAcrescimo').Value ;

              end ;

          end ;

          if (qryTempnCdProduto.Value > 0) and (qryTempnValUnitProduto.Value <= 0) then
          begin

            MensagemAlerta('Informe o valor do produto da coluna ' + qryTempiColuna.AsString) ;
            Result := false;
            bEdit  := True ;
            exit ;

          end ;

          qryPosicaoEstoque.Close ;
          qryPosicaoEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          qryPosicaoEstoque.Parameters.ParamByName('nCdProduto').Value := qryTempnCdProduto.Value;
          qryPosicaoEstoque.Open ;

          if not qryPosicaoEstoque.Eof then
          begin
              qryTempnEstoqueProduto.Value := qryPosicaoEstoquenQtdeDisponivel.Value ;

              if (qryPosicaoEstoquenQtdeDisponivel.Value <= 0) then
                  bSemEstoque := True ;

          end ;

          if (cFlgTipoFat = 1) then
          begin
          
              qryPrecoAux.Close ;
              qryPrecoAux.Parameters.ParamByName('nCdEmpresa').Value    := frmMenu.nCdEmpresaAtiva;
              qryPrecoAux.Parameters.ParamByName('nCdProduto').Value    := qryTempnCdProduto.Value ;
              qryPrecoAux.Parameters.ParamByName('nCdTerceiro').Value   := nCdTerceiro ;
              qryPrecoAux.Parameters.ParamByName('nValorInicial').Value := (qryTempnValUnitProduto.Value-qryTempnValDescontoProd.Value+qryTempnValAcrescimoProd.Value) ;
              qryPrecoAux.Open ;

              if not qryPrecoAux.Eof then
                  qryTempnValUnitarioProdEsp.Value := qryPrecoAuxnPrecoAux.Value ;

          end ;


        end ;

        qryTemp.Post ;

        if (Trim(qryTempcApelidoAmostra.Value) <> '') then
        begin

          qryFormulaColunaProduto.Close ;
          qryFormulaColunaProduto.Parameters.ParamByName('nCdFormulaColuna').Value := qryTempnCdFormulaColuna.Value ;
          qryFormulaColunaProduto.Parameters.ParamByName('cFlgProdAmostra').Value := 'A'                            ;
          qryFormulaColunaProduto.Parameters.ParamByName('cApelido').Value         := qryTempcApelidoAmostra.Value  ;
          qryFormulaColunaProduto.Open ;

          if qryFormulaColunaProduto.Eof then
          begin
              MensagemAlerta('Apelido da amostra da coluna ' + qryTempiColuna.AsString + ' n�o encontrado na lista de amostras permitidas.') ;
              Result := false;
              bEdit  := True ;
              exit ;
          end ;

          qryTemp.Edit ;
          qryTempnCdAmostra.Value        := qryFormulaColunaProdutonCdProduto.Value ;
          qryTempnValUnitAmostra.Value   := 0 ;
          qryTempnValDescontoAmos.Value  := 0 ;
          qryTempnValAcrescimoAmos.Value := 0 ;

          qryPosicaoEstoque.Close ;
          qryPosicaoEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          qryPosicaoEstoque.Parameters.ParamByName('nCdProduto').Value := qryFormulaColunaProdutonCdProduto.Value;
          qryPosicaoEstoque.Open ;

          if not qryPosicaoEstoque.Eof then
          begin
              qryTempnEstoqueAmostra.Value := qryPosicaoEstoquenQtdeDisponivel.Value ;

              if (qryPosicaoEstoquenQtdeDisponivel.Value <= 0) then
                  bSemEstoque := True ;
          end ;

          qryTemp.Post ;

        end ;

        qryTemp.Next ;

    end ;

    qryZeraCodigo.Close ;
    qryZeraCodigo.ExecSQL ;

    if (bSemEstoque) then
    begin

        case MessageDlg('Alguns itens n�o tem estoque dispon�vel para atendimento. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo: begin
                qryTemp.Close ;
                qryTemp.Open ;
                result := false ;
                bEdit  := True ;
                exit ;
            end ;
        end ;

    end ;

    bEdit  := True ;
    Result := True ;

end ;

procedure TfrmEmbFormula.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if ValidaEmbalagem() then
      Close ;
      
end;

procedure TfrmEmbFormula.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Enabled := False ;
  DBGridEh1.ReadOnly  := True  ;

  if (nCdTabStatusPed <= 1) then
  begin
      DBGridEh1.ReadOnly  := False ;
      ToolButton1.Enabled := True ;
  end ;

  DBGridEh1.Col := 4 ;

  if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') or (bWERP) then
  begin
      DBGridEh1.Columns[5].ReadOnly := True ;
      DBGridEh1.Columns[6].ReadOnly := True ;
      DBGridEh1.Columns[5].Title.Font.Color := clRed ;
      DBGridEh1.Columns[6].Title.Font.Color := clRed ;
  end ;

  DBGridEh1.SetFocus;
end;

procedure TfrmEmbFormula.qryTempAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.Columns[3].ReadOnly := False ;
  DBGridEh1.Columns[4].ReadOnly := False ;
  DBGridEh1.Columns[5].ReadOnly := False ;
  DBGridEh1.Columns[6].ReadOnly := False ;

  if (qryTempnQtdeProduto.Value = 0) then
  begin
      DBGridEh1.Columns[3].ReadOnly := True ;
      DBGridEh1.Columns[5].ReadOnly := True ;
  end ;

  if (qryTempnQtdeAmostra.Value = 0) then
  begin
      DBGridEh1.Columns[4].ReadOnly := True ;
      DBGridEh1.Columns[6].ReadOnly := True ;
  end ;

end;

procedure TfrmEmbFormula.qryTempBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryTempcApelidoProduto.Value := Uppercase(qryTempcApelidoProduto.Value) ;
  qryTempcApelidoAmostra.Value := Uppercase(qryTempcApelidoAmostra.Value) ;

end;

procedure TfrmEmbFormula.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmEmbFormula.ToolButton5Click(Sender: TObject);
var
    cApelidoProduto, cApelidoAmostra : string ;
begin
  inherited;

  qryTemp.First ;

  cApelidoProduto := qryTempcApelidoProduto.Value ;
  cApelidoAmostra := qryTempcApelidoAmostra.Value ;

  qryTemp.Next ;

  While not qryTemp.Eof do
  begin

      qryTemp.Edit ;
      qryTempcApelidoProduto.Value := cApelidoProduto ;
      qryTempcApelidoAmostra.Value := cApelidoAmostra ;
      qryTemp.Post ;

      qryTemp.Next ;
  end ;

  qryTemp.First ;

  ToolButton1.Click ;

end;

procedure TfrmEmbFormula.qryTempAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bEdit) then
  begin

      if (qryTemp.Recno = qryTemp.RecordCount) then
      begin
          ToolButton1.Click ;
          exit ;
      end ;

  end ;

end;

procedure TfrmEmbFormula.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 5) and (qryTemp.State = dsEdit) and (trim(qryTempcApelidoProduto.Value) <> '') and (qryTempnValUnitProduto.Value = 0) then
  begin
  
      if (qryTempnQtdeAmostra.Value > 0) then
          qryTempcApelidoAmostra.Value := 'P' + Trim(qryTempcApelidoProduto.Value) ;

  end ;

end;

procedure TfrmEmbFormula.ToolButton2Click(Sender: TObject);
begin
  qryTemp.Close ;
  inherited;

end;

end.
