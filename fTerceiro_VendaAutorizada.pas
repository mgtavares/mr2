unit fTerceiro_VendaAutorizada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, cxPC, ADODB, Menus;

type
  TfrmTerceiro_VendaAutorizada = class(TfrmProcesso_Padrao)
    qryVendaAutorizada: TADOQuery;
    DataSource1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nValPedidoVendaAberto: TcxGridDBColumn;
    qryVendaAutorizadanCdPedido: TIntegerField;
    qryVendaAutorizadadDtPedido: TDateTimeField;
    qryVendaAutorizadacNmTipoPedido: TStringField;
    qryVendaAutorizadanValPedidoVendaAberto: TBCDField;
    qryVendaAutorizadaTerceiro: TStringField;
    cxGrid1DBTableView1Terceiro: TcxGridDBColumn;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure exibeValores(nCdTerceiro: Integer);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTerceiro_VendaAutorizada: TfrmTerceiro_VendaAutorizada;

implementation

uses fConsultaPedComAberTerceiro_Itens;

{$R *.dfm}

procedure TfrmTerceiro_VendaAutorizada.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaPedComAberTerceiro_Itens ;
begin
  inherited;

  objForm := TfrmConsultaPedComAberTerceiro_Itens.Create(nil) ;

  PosicionaQuery(objForm.qryItens, qryVendaAutorizadanCdPedido.asString) ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmTerceiro_VendaAutorizada.exibeValores(nCdTerceiro: Integer);
begin

    qryVendaAutorizada.Close;
    qryVendaAutorizada.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryVendaAutorizada.Open;

    if (not Self.Showing) then
      Self.ShowModal ;

end;

end.
