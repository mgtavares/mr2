inherited frmGrupoTotalCC: TfrmGrupoTotalCC
  Caption = 'Grupo Totalizador Centro Custo'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 35
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 25
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 76
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoTotalCC'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 76
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoTotalCC'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 24
    Top = 96
    Width = 705
    Height = 329
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 325
    ClientRectLeft = 4
    ClientRectRight = 701
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Totalizadores'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 27
        Width = 697
        Height = 274
        Align = alBottom
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = DataSource1
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnDblClick = DBGridEh1DblClick
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cNmColunaGrupoTotalCC'
            Footers = <>
            Title.Caption = 'Descri'#231#227'o'
            Width = 585
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btInsereTotal: TcxButton
        Left = 0
        Top = 0
        Width = 75
        Height = 25
        Caption = 'Inserir'
        TabOrder = 1
        OnClick = btInsereTotalClick
        Glyph.Data = {
          96030000424D9603000000000000360000002800000010000000120000000100
          1800000000006003000000000000000000000000000000000000C6D6DEC6D6DE
          C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
          DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6
          D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          008C9C9CC6D6DEC6D6DE000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD
          BDBDBDBDBD0000006BD6BD6BD6BD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948C0000006BD6BD6BD6BD0000
          008C9C9C8C9C9C8C9C9C000000EFFFFFC6948CE7E7E7FFEFDEFFEFDE00000000
          00000000000000006BD6BD6BD6BD000000000000000000000000000000EFFFFF
          C6948CC6948CC6948CC6948C0000006BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6
          BD6BD6BD6BD6BD000000000000EFFFFFFFEFDEFFEFDEFFEFDEFFEFDE0000006B
          D6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD000000000000EFFFFF
          007B5A007B5A007B5A00C6940000000000000000000000006BD6BD6BD6BD0000
          00000000000000000000000000EFFFFF007B5A007B5A00C694007B5A00C69400
          7B5A00C6940000006BD6BD6BD6BD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          007B5A007B5A007B5A00C694007B5A00C69400C6940000006BD6BD6BD6BD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFF
          EFDEFFEFDE0000000000000000000000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CBDBDBD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFC6948CFFEFDEE7E7E7FFEFDEFFEFDEFF
          EFDEFFEFDEFFEFDEC6948CBDBDBD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CBDBDBD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
          FFFFEFFFFFEFFFFFEFFFFFEFFFFF0000008C9C9CC6D6DEC6D6DE000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00C6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6
          D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE}
        LookAndFeel.NativeStyle = True
      end
      object btExcluiTotal: TcxButton
        Left = 81
        Top = 0
        Width = 75
        Height = 25
        Caption = 'Excluir'
        TabOrder = 2
        OnClick = btExcluiTotalClick
        Glyph.Data = {
          06030000424D060300000000000036000000280000000F0000000F0000000100
          180000000000D002000000000000000000000000000000000000BDBDBDBDBDBD
          BDBDBDBDBDBDC6948C000000000000000000000000000000C6948CC6948C0000
          00000000BDBDBD000000BDBDBDBDBDBDBDBDBD000000000000000000EFFFFF00
          0000000084000084000000000000EFFFFF000000C6948C000000BDBDBDBDBDBD
          000000000084000084000000EFFFFFEFFFFF000000000084000000EFFFFFEFFF
          FF000000C6948C000000BDBDBD0000000000842100C62100C6000000000000EF
          FFFFEFFFFF000000EFFFFFEFFFFF000000000000C6948C000000BDBDBD000000
          2100C60000842100C62100C62100C6000000EFFFFFEFFFFFEFFFFF0000000000
          00000000C6948C0000000000000000842100C62100C62100C60000FF2100C600
          0000EFFFFFEFFFFFEFFFFF0000000000840000840000000000000000002100C6
          0000FF2100C60000FF0000FF000000EFFFFFEFFFFF000000EFFFFFEFFFFF0000
          000000000000000000000000000000FF2100C60000FF0000FF000000EFFFFFEF
          FFFF2100C60000FF000000EFFFFFEFFFFF0000000000000000000000002100C6
          0000FF0000FF0000FF000000EFFFFF0000000000FF2100C60000FF000000EFFF
          FF0000000000000000000000002100C60000FF0000FF0000FF00000000000000
          00FF0000FF0000FF2100C60000FF000000000000000000000000BDBDBD000000
          0000FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF0000FF2100C62100
          C6000000BDBDBD000000BDBDBD0000000000FF0000FF0000FFEFFFFFEFFFFF00
          00FF0000FF0000FF2100C60000FF2100C6000000BDBDBD000000BDBDBDBDBDBD
          0000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF2100C60000
          00BDBDBDBDBDBD000000BDBDBDBDBDBDBDBDBD0000000000000000FF0000FF00
          00FF0000FF0000FF000000000000BDBDBDBDBDBDBDBDBD000000BDBDBDBDBDBD
          BDBDBDBDBDBDBDBDBD000000000000000000000000000000BDBDBDBDBDBDBDBD
          BDBDBDBDBDBDBD000000}
        LookAndFeel.NativeStyle = True
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoTotalCC'
      ' WHERE nCdGrupoTotalCC = :nPK')
    object qryMasternCdGrupoTotalCC: TIntegerField
      FieldName = 'nCdGrupoTotalCC'
    end
    object qryMastercNmGrupoTotalCC: TStringField
      FieldName = 'cNmGrupoTotalCC'
      Size = 50
    end
  end
  object qryColunaGrupoTotalCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColunaGrupoTotalCC'
      '      ,nCdGrupoTotalCC'
      '      ,cNmColunaGrupoTotalCC'
      '  FROM ColunaGrupoTotalCC'
      ' WHERE nCdGrupoTotalCC = :nPK')
    Left = 312
    Top = 440
    object qryColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField
      FieldName = 'nCdColunaGrupoTotalCC'
    end
    object qryColunaGrupoTotalCCnCdGrupoTotalCC: TIntegerField
      FieldName = 'nCdGrupoTotalCC'
    end
    object qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField
      FieldName = 'cNmColunaGrupoTotalCC'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryColunaGrupoTotalCC
    Left = 320
    Top = 472
  end
end
