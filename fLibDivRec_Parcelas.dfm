inherited frmLibDivRec_Parcelas: TfrmLibDivRec_Parcelas
  Left = 172
  Top = 232
  Width = 826
  Height = 343
  BorderIcons = [biSystemMenu]
  Caption = 'Parcelas'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Left = 481
    Top = 70
    Width = 0
    Height = 237
  end
  inherited ToolBar1: TToolBar
    Width = 810
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 70
    Width = 233
    Height = 237
    Align = alLeft
    DataGrouping.GroupLevels = <>
    DataSource = dsParcelasPrevistas
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iDias'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 29
    Width = 810
    Height = 41
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object DBGridEh2: TDBGridEh [4]
    Left = 233
    Top = 70
    Width = 248
    Height = 237
    Align = alLeft
    DataGrouping.GroupLevels = <>
    DataSource = dsParcelasRealizadas
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 85
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeDias'
        Footers = <>
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'nValParcela'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBGridEh3: TDBGridEh [5]
    Left = 481
    Top = 70
    Width = 329
    Height = 237
    Align = alRight
    DataGrouping.GroupLevels = <>
    DataSource = dsPedidos
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'cNmCondPagto'
        Footers = <>
        Width = 230
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryParcelasPrevistas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iQtdePedidos'
        DataType = ftInteger
        Direction = pdInputOutput
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdRecebimento int'
      ''
      'Set @nCdRecebimento = :nCdRecebimento'
      ''
      'IF (OBJECT_ID('#39'tempdb..#PrazoRecebimento'#39') IS NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #PrazoRecebimento (iParcela int'
      #9#9#9#9#9#9#9#9'   ,dDtVenc  datetime'
      #9#9#9#9#9#9#9#9'   ,iDias    int)'
      ''
      'END'
      ''
      'EXEC SP_GERA_PARCELA_RECEBIMENTO_TEMP @nCdRecebimento'
      '                                     ,:iQtdePedidos '
      ''
      'SELECT *'
      '  FROM #PrazoRecebimento'
      ' ORDER BY dDtVenc')
    Left = 128
    Top = 200
    object qryParcelasPrevistasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelasPrevistasdDtVenc: TDateTimeField
      DisplayLabel = 'Vencimentos Previstos|Data'
      FieldName = 'dDtVenc'
    end
    object qryParcelasPrevistasiDias: TIntegerField
      DisplayLabel = 'Vencimentos Previstos|Dias'
      FieldName = 'iDias'
    end
  end
  object dsParcelasPrevistas: TDataSource
    DataSet = qryParcelasPrevistas
    Left = 168
    Top = 200
  end
  object qryParcelasRealizadas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT dDtVenc'
      '      ,iQtdeDias'
      '      ,nValParcela'
      '  FROM PrazoRecebimento'
      ' WHERE nCdRecebimento = :nCdRecebimento'
      ' ORDER BY dDtVenc')
    Left = 352
    Top = 176
    object qryParcelasRealizadasdDtVenc: TDateTimeField
      DisplayLabel = 'Vencimentos Reais|Data'
      FieldName = 'dDtVenc'
    end
    object qryParcelasRealizadasiQtdeDias: TIntegerField
      DisplayLabel = 'Vencimentos Reais|Dias'
      FieldName = 'iQtdeDias'
    end
    object qryParcelasRealizadasnValParcela: TBCDField
      DisplayLabel = 'Vencimentos Reais|Valor'
      FieldName = 'nValParcela'
      Precision = 12
      Size = 2
    end
  end
  object dsParcelasRealizadas: TDataSource
    DataSet = qryParcelasRealizadas
    Left = 392
    Top = 176
  end
  object qryPedidos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT DISTINCT ItemPedido.nCdPedido'
      '               ,CondPagto.cNmCondPagto'
      '  FROM ItemRecebimento'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdItemPedido = ItemR' +
        'ecebimento.nCdItemPedido'
      
        '       INNER JOIN Pedido     ON Pedido.nCdPedido         = ItemP' +
        'edido.nCdPedido'
      
        '       LEFT  JOIN CondPagto  ON CondPagto.nCdCondPagto   = Pedid' +
        'o.nCdCondPagto'
      ' WHERE ItemRecebimento.nCdRecebimento = :nCdRecebimento'
      ' ORDER BY ItemPedido.nCdPedido')
    Left = 488
    Top = 192
    object qryPedidosnCdPedido: TIntegerField
      DisplayLabel = 'Pedidos|N'#250'mero'
      FieldName = 'nCdPedido'
    end
    object qryPedidoscNmCondPagto: TStringField
      DisplayLabel = 'Pedidos|Condi'#231#227'o Pagamento'
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  object dsPedidos: TDataSource
    DataSet = qryPedidos
    Left = 528
    Top = 192
  end
end
