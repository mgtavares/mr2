unit rRazaoAuxiliarFornecedores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, ER2Lookup, ER2Excel, DB, ADODB;

type
  TrptRazaoAuxiliarFornecedores = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    edtEmpresa: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    edtGrupoEconomico: TER2LookupMaskEdit;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    edtTerceiro: TER2LookupMaskEdit;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    MaskEditDataInicial: TMaskEdit;
    Label11: TLabel;
    MaskEditDataFinal: TMaskEdit;
    RadioGroupTipoSaida: TRadioGroup;
    RadioGroupSaida: TRadioGroup;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsTerceiro: TDataSource;
    ER2Excel1: TER2Excel;
    procedure edtEmpresaBeforePosicionaQry(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRazaoAuxiliarFornecedores: TrptRazaoAuxiliarFornecedores;

implementation

uses fMenu,rRazaoAuxiliarFornecedores_View,fRazaoAuxiliarFornecedores_View;

{$R *.dfm}

procedure TrptRazaoAuxiliarFornecedores.edtEmpresaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

end;

procedure TrptRazaoAuxiliarFornecedores.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := intToStr(frmMenu.nCdEmpresaAtiva);
  edtEmpresa.PosicionaQuery;
  edtGrupoEconomico.SetFocus;

end;

procedure TrptRazaoAuxiliarFornecedores.ToolButton1Click(Sender: TObject);
var
  objRel  : TrptRazaoAuxiliarFornecedores_View;
  objForm : TfrmRazaoAuxiliarFornecedores_View;
  linha   : integer;
begin
  inherited;
  if (DBEDit3.Text = '') then edtGrupoEconomico.Text := '';
  if (DBEDit4.Text = '') then edtTerceiro.Text := '';

  objRel := TrptRazaoAuxiliarFornecedores_View.Create(nil) ;

  try
      try

          //Relatorio / Planilha
          if (RadioGroupSaida.ItemIndex <> 2) then
          begin

              objRel.uspRelatorio.Close ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value           := frmMenu.ConvInteiro(edtEmpresa.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value    := frmMenu.ConvInteiro(edtGrupoEconomico.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value          := frmMenu.ConvInteiro(edtTerceiro.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value           := frmMenu.ConvData(MaskEditDataInicial.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value             := frmMenu.ConvData(MaskEditDataFinal.Text) ;

              objRel.uspRelatorio.Open  ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              objRel.lblFiltro1.Caption := '';

              if (Trim(edtEmpresa.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + 'Empresa : ' + Trim(EdtEmpresa.Text) + '-' + DbEdit1.Text ;

              if (Trim(EdtGrupoEconomico.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Grupo Econ�mico : ' + Trim(EdtGrupoEconomico.Text) + '-' + DbEdit3.Text ;

              if (Trim(edtTerceiro.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Terceiro: ' + Trim(edtTerceiro.Text) + '-' + DbEdit4.Text ;

              if ((Trim(MaskEditDataInicial.Text) <> '/  /') or (Trim(MaskEditDataFinal.Text) <> '/  /')) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo : ' + Trim(MaskEditDataInicial.Text) + '-' + String(MaskEditDataFinal.Text) ;

              //if (RadioGroupExibirMovCancelados.ItemIndex = 0) then
              //    objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Exibir Mov. Cancelados : Sim'
              //else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Exibir Mov. Cancelados : N�o' ;

              if (RadioGroupTipoSaida.ItemIndex = 0) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo de Sa�da: Anal�tico'
              else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo de Sa�da: Sint�tico' ;

              //Oculta itens - modelo saida sintetico
              objRel.QRBand1.Enabled := True;
              if RadioGroupTipoSaida.ItemIndex = 1 then
                  objRel.QRBand3.Enabled := false;

              {--visualiza o relat�rio--}
              // saida em relat�rio
              if (RadioGroupSaida.ItemIndex = 0) then
                   objRel.QuickRep1.PreviewModal;


              // sa�da em planilha
              if (RadioGroupSaida.ItemIndex = 1) then
              begin

                  for linha := 0 to objRel.uspRelatorio.RecordCount - 1 do
                  begin

                      //cabe�alho
                      if linha = 0 then
                      begin

                          ER2Excel1.Celula['A1'].Text := 'Fornecedor';
                          ER2Excel1.Celula['B1'].Text := 'Nome Fornecedor';
                          ER2Excel1.Celula['C1'].Text := 'Data Movimento';
                          ER2Excel1.Celula['D1'].Text := 'ID';
                          ER2Excel1.Celula['E1'].Text := 'N�m. NF';
                          ER2Excel1.Celula['F1'].Text := 'N�m. T�tulo';
                          ER2Excel1.Celula['G1'].Text := 'Parcela';
                          ER2Excel1.Celula['H1'].Text := 'Opera��o';
                          ER2Excel1.Celula['I1'].Text := 'Hist�rico';
                          ER2Excel1.Celula['J1'].Text := 'Vr. Cr�dito';
                          ER2Excel1.Celula['L1'].Text := 'Vr. Debito';
                          ER2Excel1.Celula['M1'].Text := 'Saldo';
                          ER2Excel1.Celula['N1'].Text := 'Documento';
                          ER2Excel1.Celula['O1'].Text := 'Usu�rio';
                          ER2Excel1.Celula['P1'].Text := 'Conta Banc�ria';
                      end;

                      ER2Excel1.Celula['A' + intToStr(linha +2)].Text := objRel.uspRelatorionCdTerceiro.Value;
                      ER2Excel1.Celula['B' + intToStr(linha +2)].Text := objRel.uspRelatoriocNmTerceiro.Value;
                      ER2Excel1.Celula['C' + intToStr(linha +2)].Text := objRel.uspRelatoriodDtMovto.Value;
                      ER2Excel1.Celula['D' + intToStr(linha +2)].Text := objRel.uspRelatorionCdTitulo.Value;
                      ER2Excel1.Celula['E' + intToStr(linha +2)].Text := objRel.uspRelatoriocNrNf.Value;
                      ER2Excel1.Celula['F' + intToStr(linha +2)].Text := objRel.uspRelatoriocNrTit.Value;
                      ER2Excel1.Celula['G' + intToStr(linha +2)].Text := objRel.uspRelatorionCdParc.Value;
                      ER2Excel1.Celula['H' + intToStr(linha +2)].Text := objRel.uspRelatoriocNmOperacao.Value;
                      ER2Excel1.Celula['I' + intToStr(linha +2)].Text := objRel.uspRelatoriocHistorico.Value;
                      ER2Excel1.Celula['J' + intToStr(linha +2)].Text := objRel.uspRelatorionValCredito.Value;
                      ER2Excel1.Celula['L' + intToStr(linha +2)].Text := objRel.uspRelatorionValDebito.Value;
                      ER2Excel1.Celula['M' + intToStr(linha +2)].Text := objRel.uspRelatorionSaldo.Value;
                      ER2Excel1.Celula['N' + intToStr(linha +2)].Text := objRel.uspRelatoriocDocumento.Value;
                      ER2Excel1.Celula['O' + intToStr(linha +2)].Text := objRel.uspRelatoriocNmUsuario.Value;
                      ER2Excel1.Celula['P' + intToStr(linha +2)].Text := objRel.uspRelatoriocContaBancaria.Value;

                      objRel.uspRelatorio.Next;
                  End;

                  { -- exporta planilha e limpa result do componente -- }
                  ER2Excel1.ExportXLS;
                  ER2Excel1.CleanupInstance;
              end;
          end;

          //saida em tela
          if (RadioGroupSaida.ItemIndex = 2) then
          begin

              objForm := TfrmRazaoAuxiliarFornecedores_View.Create(nil) ;

              objForm.uspRelatorio.Close ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value           := frmMenu.ConvInteiro(edtEmpresa.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value    := frmMenu.ConvInteiro(edtGrupoEconomico.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value          := frmMenu.ConvInteiro(edtTerceiro.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value           := frmMenu.ConvData(MaskEditDataInicial.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value             := frmMenu.ConvData(MaskEditDataFinal.Text) ;

              objForm.uspRelatorio.Open  ;

              objForm.cxGrid1.Align := alClient ;
              showForm(objForm,true) ;
          end;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;


end;

initialization
     RegisterClass(TrptRazaoAuxiliarFornecedores) ;

end.
