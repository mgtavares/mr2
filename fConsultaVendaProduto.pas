unit fConsultaVendaProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, ADODB, DBCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmConsultaVendaProduto = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtEANProduto: TEdit;
    Label1: TLabel;
    edtLoja: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtNumPedido: TMaskEdit;
    edtNumCupom: TMaskEdit;
    Label4: TLabel;
    edtDtFinal: TMaskEdit;
    Label6: TLabel;
    edtDtInicial: TMaskEdit;
    Label5: TLabel;
    edtCliente: TMaskEdit;
    Label7: TLabel;
    edtCPF: TEdit;
    Label8: TLabel;
    edtNumCarnet: TMaskEdit;
    Label9: TLabel;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryCliente: TADOQuery;
    qryClientenCdTerceiro: TIntegerField;
    qryClientecNmTerceiro: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SP_CONSULTA_VENDA_PRODUTO: TADOStoredProc;
    SP_CONSULTA_VENDA_PRODUTOnCdPedido: TIntegerField;
    SP_CONSULTA_VENDA_PRODUTOdDtPedido: TDateTimeField;
    SP_CONSULTA_VENDA_PRODUTOcNmTerceiro: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcNmLoja: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcNmTipoPedido: TStringField;
    SP_CONSULTA_VENDA_PRODUTOnQtdeExpRec: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValUnitario: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValDesconto: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValAcrescimo: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValTotal: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOcNmProduto: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcReferencia: TStringField;
    SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin: TIntegerField;
    dsResultado: TDataSource;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cReferencia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn;
    SP_CONSULTA_VENDA_PRODUTOnCdUsuario: TIntegerField;
    SP_CONSULTA_VENDA_PRODUTOcNmUsuario: TStringField;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    procedure edtEANProdutoExit(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNumCartaoExit(Sender: TObject);
    procedure edtEANProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaVendaProduto: TfrmConsultaVendaProduto;

implementation

uses fMenu, fPdvConsProduto, fLookup_Padrao, fCaixa_SelCliente,
  fConsultaVendaProduto_Dados;

{$R *.dfm}

procedure TfrmConsultaVendaProduto.edtEANProdutoExit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto,trim(edtEANProduto.Text));

  if (qryProduto.eof) and (trim(edtEANProduto.Text) <> '') then
  begin
      MensagemAlerta('Produto n�o cadastrado.') ;
      edtEANProduto.Text := '' ;
      edtEANProduto.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmConsultaVendaProduto.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TfrmConsultaVendaProduto.edtClienteExit(Sender: TObject);
begin
  inherited;

  qryCliente.Close ;
  PosicionaQuery(qryCliente, edtCliente.Text) ;
  
end;

procedure TfrmConsultaVendaProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;


  if ((StrToDate(edtDtFinal.Text) - StrToDate(edtDtInicial.Text)) > 7) then
  begin

      if (DBEdit1.Text = '') and (DBEdit2.Text = '') and (trim(edtNumPedido.Text) = '') then
      begin

          if (trim(edtNumCarnet.Text) = '') and (trim(edtNumCupom.Text) = '') and (trim(edtCPF.Text) = '') then
          begin

              case MessageDlg('Poucos filtros foram utilizados e esta consulta pode demorar alguns minutos. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo: begin
                      edtEANProduto.SetFocus ;
                      exit ;
                  end ;
              end ;

          end ;

      end ;

  end ;

  SP_CONSULTA_VENDA_PRODUTO.Close ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;

  if (DBEdit1.Text <> '') then
      SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdProduto').Value  := qryProdutonCdProduto.Value 
  else SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdProduto').Value := 0 ;

  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdLoja').Value        := frmMenu.ConvInteiro(edtLoja.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdCliente').Value     := frmMenu.ConvInteiro(edtCliente.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value       := edtCPF.Text ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdPedido').Value      := frmMenu.ConvInteiro(edtNumPedido.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@iNrDoctoFiscal').Value := frmMenu.ConvInteiro(edtNumCupom.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@dDtInicial').Value     := frmMenu.ConvData(edtDtInicial.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@dDtFinal').Value       := frmMenu.ConvData(edtDtFinal.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdCrediario').Value   := frmMenu.ConvInteiro(edtNumCarnet.Text) ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@iNrCartao').Value      := '';
  SP_CONSULTA_VENDA_PRODUTO.Open ;

  if (SP_CONSULTA_VENDA_PRODUTO.eof) then
  begin
      MensagemAlerta('Nenhum registro de venda encontrado.') ;
      edtEANProduto.SetFocus ;
      exit ;
  end ;

end;

procedure TfrmConsultaVendaProduto.FormShow(Sender: TObject);
begin
  inherited;

  edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TfrmConsultaVendaProduto.edtNumCartaoExit(Sender: TObject);
begin
  inherited;
  ToolButton1.Click ;
end;

procedure TfrmConsultaVendaProduto.edtEANProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
    objForm : TfrmPDVConsProduto;
begin
  inherited;

  case key of
    vk_F4 : begin

        objForm := TfrmPDVConsProduto.Create(nil);

        nPK := objForm.ConsultaProduto;

        If (nPK > 0) then
        begin
            edtEANProduto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaVendaProduto.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(86);

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaVendaProduto.edtClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
    objForm : TfrmCaixa_SelCliente;
begin
  inherited;

  case key of
    vk_F4 : begin

        objForm := TfrmCaixa_SelCliente.Create(nil);

        nPK := objForm.SelecionaCliente;

        If (nPK > 0) then
        begin
            edtCliente.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaVendaProduto.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados;
begin
  inherited;

  if not SP_CONSULTA_VENDA_PRODUTO.Eof then
  begin

      objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

      PosicionaQuery(objForm.qryItemPedido, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCondicao, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryPrestacoes, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCheques, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      showForm(objForm,true);

  end ;

end;

initialization
    RegisterClass(TfrmConsultaVendaProduto) ;
    
end.
