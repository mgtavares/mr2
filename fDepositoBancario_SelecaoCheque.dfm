inherited frmDepositoBancario_SelecaoCheque: TfrmDepositoBancario_SelecaoCheque
  Left = 144
  Top = 161
  Width = 903
  Height = 594
  BorderIcons = [biSystemMenu]
  Caption = 'Sele'#231#227'o de Cheques'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 121
    Width = 887
    Height = 199
  end
  inherited ToolBar1: TToolBar
    Width = 887
    ButtonWidth = 119
    inherited ToolButton1: TToolButton
      Caption = 'Adicionar Cheques'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 119
    end
    inherited ToolButton2: TToolButton
      Left = 127
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 887
    Height = 92
    Align = alTop
    Caption = 'Filtro'
    Ctl3D = True
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 1
    object Label6: TLabel
      Tag = 1
      Left = 204
      Top = 44
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label3: TLabel
      Tag = 1
      Left = 11
      Top = 44
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data de Dep'#243'sito At'#233
    end
    object Label5: TLabel
      Tag = 1
      Left = 37
      Top = 68
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero Cheque'
    end
    object Label1: TLabel
      Tag = 1
      Left = 34
      Top = 20
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Portadora'
    end
    object MaskEdit2: TMaskEdit
      Left = 224
      Top = 36
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object MaskEdit1: TMaskEdit
      Left = 120
      Top = 36
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit6: TMaskEdit
      Left = 120
      Top = 60
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 3
      Text = '      '
    end
    object cxButton1: TcxButton
      Left = 192
      Top = 56
      Width = 105
      Height = 25
      Caption = 'Exibir Cheques'
      TabOrder = 4
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object MaskEdit3: TMaskEdit
      Left = 120
      Top = 12
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 192
      Top = 12
      Width = 199
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource1
      TabOrder = 5
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 121
    Width = 887
    Height = 199
    Align = alClient
    Caption = 'Cheques Dispon'#237'veis'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 883
      Height = 182
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dsChequesPend
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Header = frmMenu.Header
        object cxGrid1DBTableView1nCdBanco: TcxGridDBColumn
          DataBinding.FieldName = 'nCdBanco'
        end
        object cxGrid1DBTableView1iNrCheque: TcxGridDBColumn
          DataBinding.FieldName = 'iNrCheque'
          Width = 92
        end
        object cxGrid1DBTableView1nValCheque: TcxGridDBColumn
          DataBinding.FieldName = 'nValCheque'
        end
        object cxGrid1DBTableView1dDtDeposito: TcxGridDBColumn
          DataBinding.FieldName = 'dDtDeposito'
        end
        object cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn
          DataBinding.FieldName = 'cCNPJCPF'
          Width = 111
        end
        object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 407
        end
        object cxGrid1DBTableView1cFlgAux: TcxGridDBColumn
          DataBinding.FieldName = 'cFlgAux'
          Visible = False
        end
        object cxGrid1DBTableView1nCdCheque: TcxGridDBColumn
          DataBinding.FieldName = 'nCdCheque'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 320
    Width = 887
    Height = 238
    Align = alBottom
    Caption = 'Cheques Selecionados'
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 2
      Top = 15
      Width = 883
      Height = 221
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        OnDblClick = cxGridDBTableView1DblClick
        DataController.DataSource = dsChequesSEL
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            Column = cxGridDBTableView1nValCheque
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Header = frmMenu.Header
        object cxGridDBTableView1nCdBanco: TcxGridDBColumn
          DataBinding.FieldName = 'nCdBanco'
        end
        object cxGridDBTableView1iNrCheque: TcxGridDBColumn
          DataBinding.FieldName = 'iNrCheque'
          Width = 90
        end
        object cxGridDBTableView1nValCheque: TcxGridDBColumn
          DataBinding.FieldName = 'nValCheque'
        end
        object cxGridDBTableView1dDtDeposito: TcxGridDBColumn
          DataBinding.FieldName = 'dDtDeposito'
        end
        object cxGridDBTableView1cCNPJCPF: TcxGridDBColumn
          DataBinding.FieldName = 'cCNPJCPF'
          Width = 115
        end
        object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 404
        end
        object cxGridDBTableView1cFlgAux: TcxGridDBColumn
          DataBinding.FieldName = 'cFlgAux'
          Visible = False
        end
        object cxGridDBTableView1nCdCheque: TcxGridDBColumn
          DataBinding.FieldName = 'nCdCheque'
          Visible = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 592
    Top = 48
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000666666003D3D3D003D3D
      3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D
      3D003D3D3D00A7A7A700F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006666660055DFD4003D3D3D0055DF
      FF0055DFFF0055DFFF0055DFFF0055DFFF0055DFD40055DFFF0055DFD40055DF
      FF0055C0D4003D3D3D00F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006666660000F2FF0055DFD4003D3D
      3D00A9FFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DF
      D40055DFFF0055DFD4003D3D3D00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000007373730054FFFF0000F2FF0055DF
      D4003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D
      3D003D3D3D003D3D3D003D3D3D003D3D3D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000007373730054FFFF0054FFFF0000F2
      FF0055DFD40000F2FF0055DFD40099F8FF0099F8FF0099F8FF0099F8FF0099F8
      FF0099F8FF0099F8FF00B4B4B400F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000081818100A9FFFF0054FFFF0054FF
      FF0000F2FF00EFAD00007F5B0000EFAD0000AAFFFF0099F8FF00AAFFFF0099F8
      FF00AAFFFF0099F8FF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000008181810054FFFF00A9FFFF0054FF
      FF0054FFFF007F5B0000D9A77D007F5B0000FFFFFF00AAFFFF0099F8FF00AAFF
      FF0099F8FF00AAFFFF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000009A9A9A00A9FFFF0054FFFF00EFAD
      0000A2760000A2760000D9A77D00A37700007F5B0000EFAD0000AAFFFF0099F8
      FF00AAFFFF0099F8FF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000009A9A9A0054FFFF00A9FFFF00AA7F
      0000FFFFCC00D9A77D00D9A77D00D9A77D00D9A77D007F5B0000AAFFFF00AAFF
      FF0099F8FF00AAFFFF0081818100F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000A7A7A700A7A7A70055C0D400F7D0
      6C00E5B72600E2B62900F6CF6D00AA7F0000AA7F0000F7CF6C00AAFFFF0099F8
      FF00AAFFFF0099F8FF008E8E8E00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000F0F0F000A7A7A70000F2FF0000F2
      FF0000F1FF00F1BF2B00FFFFCC00AA7F0000AAFFFF00AAFFFF00AAFFFF0055DF
      FF0055DFFF0055C0D4009A9A9A00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000F0F0F0009B9B9B0054FFFF0067F4
      FF0067F4FF00F8D06D00FDC83100F7CF6C00AAFFFF00AAFFFF00B4B4B400A0A0
      A0008D8D8D0081818100A7A7A700F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000F0F0F000F0F0F0009B9B9B008D8D
      8D008D8D8D00B4B4B40090909000FFFFFF00AAFFFF00FFFFFF00A1A1A100E6E6
      E600DADADA00DADADA00B4B4B400F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F00091919100FFFFFF00FFFFFF00AAFFFF00A7A7A700FFFF
      FF00E7E7E700B4B4B400F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F00091919100FFFFFF00FFFFFF00FFFFFF008D8D8D00FFFF
      FF00B4B4B400F0F0F000F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F000B4B4B4009A9A9A009A9A9A008E8E8E0081818100C1C1
      C100F0F0F000F0F0F000F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF80000000FE00FFFF00000000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000018001000000000003800100000000
      0077C00300000000007FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Cheques'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'    CR' +
      'EATE TABLE #Temp_Cheques (nCdCheque   int'#13#10'                     ' +
      '          ,dDtDeposito datetime'#13#10'                               ' +
      ',iNrCheque   int'#13#10'                               ,nValCheque  de' +
      'cimal(12,2)'#13#10'                               ,cCNPJCPF    varchar' +
      '(15)'#13#10'                               ,nCdBanco    int'#13#10'         ' +
      '                      ,cNmTerceiro varchar(50)'#13#10'                ' +
      '               ,cFlgAux     int)'#13#10#13#10#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 320
    Top = 160
  end
  object qryChequesPend: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #Temp_Cheques'
      ' WHERE cFlgAux = 0'
      ' ORDER BY nCdBanco, iNrCheque')
    Left = 392
    Top = 160
    object qryChequesPenddDtDeposito: TDateTimeField
      DisplayLabel = 'Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object qryChequesPendiNrCheque: TIntegerField
      DisplayLabel = 'N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequesPendnValCheque: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequesPendcCNPJCPF: TStringField
      DisplayLabel = 'CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequesPendnCdBanco: TIntegerField
      DisplayLabel = 'Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequesPendcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryChequesPendcFlgAux: TIntegerField
      DisplayLabel = 'Cheques Dispon'#237'veis para Dep'#243'sito|OK'
      FieldName = 'cFlgAux'
    end
    object qryChequesPendnCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
  end
  object dsChequesPend: TDataSource
    DataSet = qryChequesPend
    Left = 424
    Top = 160
  end
  object qryContaBancariaDeb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND ((@nCdLoja = 0) OR (@nCdLoja = ContaBancaria.nCdLoja))'
      '   AND ((cFlgCaixa = 1 OR cFlgCofre = 1))'
      '   AND nCdEmpresa       = :nCdEmpresa')
    Left = 324
    Top = 200
    object qryContaBancariaDebnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDebnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancariaDeb
    Left = 424
    Top = 240
  end
  object qryItemDepositoBancario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM ItemDepositoBancario')
    Left = 272
    Top = 272
    object qryItemDepositoBancarionCdItemDepositoBancario: TAutoIncField
      FieldName = 'nCdItemDepositoBancario'
    end
    object qryItemDepositoBancarionCdDepositoBancario: TIntegerField
      FieldName = 'nCdDepositoBancario'
    end
    object qryItemDepositoBancarioiTipo: TIntegerField
      FieldName = 'iTipo'
    end
    object qryItemDepositoBancarionCdContaDebito: TIntegerField
      FieldName = 'nCdContaDebito'
    end
    object qryItemDepositoBancarioiQtdeCheques: TIntegerField
      FieldName = 'iQtdeCheques'
    end
    object qryItemDepositoBancarionValor: TBCDField
      FieldName = 'nValor'
      Precision = 12
      Size = 2
    end
  end
  object qryChequeItemDepositoBancario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM ChequeItemDepositoBancario')
    Left = 360
    Top = 288
    object qryChequeItemDepositoBancarionCdChequeItemDepositoBancario: TAutoIncField
      FieldName = 'nCdChequeItemDepositoBancario'
    end
    object qryChequeItemDepositoBancarionCdItemDepositoBancario: TIntegerField
      FieldName = 'nCdItemDepositoBancario'
    end
    object qryChequeItemDepositoBancarionCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 536
    Top = 256
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iTipo'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtDepositoIni'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtDepositoFim'
        Size = -1
        Value = Null
      end
      item
        Name = 'iNrCheque'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdContaBancaria'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdDepositoBancario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @iTipo               int'
      '       ,@dDtDepositoIni      varchar(10)'
      '       ,@dDtDepositoFim      varchar(10)'
      '       ,@iNrCheque           int'
      '       ,@nCdContaBancaria    int'
      '       ,@nCdEmpresa          int'
      '       ,@nCdDepositoBancario int'
      ''
      'Set @iTipo               = :iTipo'
      'Set @dDtDepositoIni      = :dDtDepositoIni'
      'Set @dDtDepositoFim      = :dDtDepositoFim'
      'Set @iNrCheque           = :iNrCheque'
      'Set @nCdEmpresa          = :nCdEmpresa'
      'Set @nCdContaBancaria    = :nCdContaBancaria'
      'Set @nCdDepositoBancario = :nCdDepositoBancario'
      ''
      'IF (@dDtDepositoIni = '#39#39')'
      'BEGIN'
      '    Set @dDtDepositoIni = '#39'01/01/1900'#39
      'END'
      ''
      'IF (@dDtDepositoFim = '#39#39')'
      'BEGIN'
      '    Set @dDtDepositoFim = '#39'01/01/1900'#39
      'END'
      ''
      
        'INSERT INTO #Temp_Cheques (dDtDeposito, iNrCheque, nValCheque, c' +
        'CNPJCPF, nCdBanco, cNmTerceiro, cFlgAux, nCdCheque)'
      'SELECT Cheque.dDtDeposito'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      '      ,Cheque.cCNPJCPF'
      '      ,Cheque.nCdBanco'
      '      ,cNmTerceiro'
      '      ,0'
      '      ,Cheque.nCdCheque'
      '  FROM Cheque'
      
        '       LEFT JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdTe' +
        'rceiroResp'
      ' WHERE Cheque.nCdTabTipoCheque    = 2 --Cheque Terceiro'
      '   AND Cheque.nCdContaBancariaDep = @nCdContaBancaria'
      '   AND Cheque.nCdTabStatusCheque  = 1 --Disponivel para Deposito'
      '   AND Cheque.nCdEmpresa          = @nCdEmpresa'
      '   AND ((@iNrCheque = 0) OR (Cheque.iNrCheque = @iNrCheque))'
      
        '   AND (@iTipo = 2 AND (Cheque.dDtDeposito <= Convert(DATETIME,@' +
        'dDtDepositoIni,103)) OR (@iTipo = 3))'
      
        '   AND (@iTipo = 3 AND (Cheque.dDtDeposito BETWEEN Convert(DATET' +
        'IME,@dDtDepositoIni,103) AND Convert(DATETIME,@dDtDepositoFim,10' +
        '3)) OR (@iTipo = 2))'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM ItemDepositoBancario'
      
        '                         INNER JOIN ChequeItemDepositoBancario C' +
        'IDB ON CIDB.nCdItemDepositoBancario = ItemDepositoBancario.nCdIt' +
        'emDepositoBancario'
      
        '                   WHERE CIDB.nCdCheque                         ' +
        '  = Cheque.nCdCheque'
      
        '                     AND ItemDepositoBancario.nCdDepositoBancari' +
        'o = @nCdDepositoBancario)'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM #Temp_Cheques'
      
        '                   WHERE #Temp_Cheques.nCdCheque = Cheque.nCdChe' +
        'que)')
    Left = 416
    Top = 360
  end
  object qryChequesSEL: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #Temp_Cheques'
      ' WHERE cFlgAux = 1'
      ' ORDER BY nCdBanco, iNrCheque')
    Left = 520
    Top = 200
    object DateTimeField1: TDateTimeField
      DisplayLabel = 'Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequesSELnValCheque: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField1: TStringField
      DisplayLabel = 'CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'Banco'
      FieldName = 'nCdBanco'
    end
    object StringField2: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryChequesSELcFlgAux: TIntegerField
      DisplayLabel = 'Cheques Dispon'#237'veis para Dep'#243'sito|OK'
      FieldName = 'cFlgAux'
    end
    object qryChequesSELnCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
  end
  object dsChequesSEL: TDataSource
    DataSet = qryChequesSEL
    Left = 552
    Top = 200
  end
end
