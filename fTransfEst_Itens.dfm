inherited frmTransfEst_Itens: TfrmTransfEst_Itens
  Left = 509
  Top = 131
  Width = 559
  Height = 465
  BorderIcons = [biSystemMenu]
  Caption = 'Itens da Transfer'#234'ncia'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 543
    Height = 398
  end
  inherited ToolBar1: TToolBar
    Width = 543
    ButtonWidth = 82
    inherited ToolButton1: TToolButton
      Caption = 'Gravar Itens'
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 82
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 90
    end
  end
  object RadioGroup1: TRadioGroup [2]
    Left = 0
    Top = 29
    Width = 217
    Height = 60
    Caption = 'Modo'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Digita'#231#227'o/Leitura'
      'Confer'#234'ncia')
    TabOrder = 1
  end
  object GroupBox4: TGroupBox [3]
    Left = 216
    Top = 29
    Width = 326
    Height = 60
    Caption = 'Produto'
    TabOrder = 2
    object Label5: TLabel
      Left = 8
      Top = 22
      Width = 24
      Height = 13
      Caption = 'Qtde'
      FocusControl = DBEdit10
    end
    object Label6: TLabel
      Left = 96
      Top = 22
      Width = 57
      Height = 13
      Caption = 'C'#243'digo/EAN'
    end
    object DBEdit10: TDBEdit
      Left = 40
      Top = 16
      Width = 49
      Height = 21
      DataField = 'nQtde'
      DataSource = dsClientProduto
      TabOrder = 0
    end
    object edtEAN: TEdit
      Left = 160
      Top = 16
      Width = 153
      Height = 21
      TabOrder = 1
      OnExit = edtEANExit
      OnKeyDown = edtEANKeyDown
    end
  end
  object DBGridEh1: TDBGridEh [4]
    Left = 0
    Top = 91
    Width = 542
    Height = 334
    DataGrouping.GroupLevels = <>
    DataSource = dsClientProduto
    Enabled = False
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cCodigo'
        Footers = <>
        Width = 176
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footers = <>
        Width = 163
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object cdsProduto: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 352
    Top = 104
    Data = {
      550000009619E0BD0100000018000000030000000000030000005500056E5174
      646508000400000000000763436F6469676F0100490000000100055749445448
      0200020014000869506F736963616F04000100000000000000}
    object cdsProdutonQtde: TFloatField
      DisplayLabel = 'Itens|Quantidade'
      FieldName = 'nQtde'
    end
    object cdsProdutocCodigo: TStringField
      DisplayLabel = 'Itens|C'#243'digo/EAN'
      FieldName = 'cCodigo'
    end
    object cdsProdutoiPosicao: TIntegerField
      FieldName = 'iPosicao'
    end
  end
  object dsClientProduto: TDataSource
    DataSet = cdsProduto
    Left = 392
    Top = 104
  end
end
