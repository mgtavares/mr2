inherited frmParametro: TfrmParametro
  Left = 260
  Top = 191
  Width = 1005
  Height = 377
  Caption = 'Parametros'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 989
    Height = 310
  end
  inherited ToolBar1: TToolBar
    Width = 989
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 989
    Height = 310
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 306
    ClientRectLeft = 4
    ClientRectRight = 985
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Par'#226'metro Global'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 65
        Width = 981
        Height = 217
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsParametro
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OddRowColor = clWhite
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cParametro'
            Footers = <>
            ReadOnly = True
            Width = 173
          end
          item
            EditButtons = <>
            FieldName = 'cValor'
            Footers = <>
            Width = 245
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cDescricao'
            Footers = <>
            ReadOnly = True
            Width = 525
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 981
        Height = 65
        Align = alTop
        Caption = ' Filtro '
        TabOrder = 1
        object edtFiltro: TEdit
          Left = 112
          Top = 24
          Width = 161
          Height = 21
          TabOrder = 0
        end
        object btBuscar: TcxButton
          Left = 280
          Top = 23
          Width = 25
          Height = 25
          TabOrder = 1
          OnClick = btBuscarClick
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
            393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
            00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
            F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
            83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
            68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
            FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
            9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
            8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
            D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
            B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
            5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
            A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
            9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
            488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
            FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
            FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
            FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
        object cmbFiltro: TComboBox
          Left = 16
          Top = 24
          Width = 89
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 2
          Text = 'Todos'
          OnChange = cmbFiltroChange
          Items.Strings = (
            'Todos'
            'Par'#226'metro'
            'Descri'#231#227'o')
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Par'#226'metro Empresa'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 981
        Height = 282
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsEmpresa
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OddRowColor = clWhite
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdEmpresa'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cParametro'
            Footers = <>
            ReadOnly = True
            Width = 173
          end
          item
            EditButtons = <>
            FieldName = 'cValor'
            Footers = <>
            Width = 119
          end
          item
            EditButtons = <>
            FieldName = 'cDescricao'
            Footers = <>
            ReadOnly = True
            Width = 525
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Par'#226'metro Web'
      ImageIndex = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 981
        Height = 65
        Align = alTop
        Caption = ' Filtro '
        TabOrder = 0
        object edtFiltroWeb: TEdit
          Left = 112
          Top = 24
          Width = 161
          Height = 21
          TabOrder = 0
        end
        object btBuscarWeb: TcxButton
          Left = 280
          Top = 23
          Width = 89
          Height = 25
          Caption = 'Buscar'
          TabOrder = 1
          OnClick = btBuscarWebClick
          LookAndFeel.Kind = lfOffice11
        end
        object cmbFiltroWeb: TComboBox
          Left = 16
          Top = 24
          Width = 89
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 2
          Text = 'Todos'
          OnChange = cmbFiltroWebChange
          Items.Strings = (
            'Todos'
            'Par'#226'metro'
            'URL Web Service')
        end
      end
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 65
        Width = 981
        Height = 217
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsParametroWeb
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OddRowColor = clWhite
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh3KeyDown
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cParametro'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Par'#226'metro Web|Par'#226'metro'
          end
          item
            EditButtons = <>
            FieldName = 'cURLWebService'
            Footers = <>
            Title.Caption = 'Par'#226'metro Web|URL Web Service'
            Width = 430
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabelaSistema'
            Footers = <>
            Title.Caption = 'Tabela Sistema|C'#243'd.'
            Width = 65
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cNmTabela'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tabela Sistema|Descri'#231#227'o'
            Width = 306
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 819
    Top = 48
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Parametro'
      '')
    Left = 754
    Top = 48
    object qryParametrocParametro: TStringField
      DisplayLabel = 'Par'#226'metro Global|Par'#226'metro'
      FieldName = 'cParametro'
      Size = 15
    end
    object qryParametrocValor: TStringField
      DisplayLabel = 'Par'#226'metro Global|Valor'
      FieldName = 'cValor'
      Size = 35
    end
    object qryParametrocDescricao: TStringField
      DisplayLabel = 'Par'#226'metro Global|Descri'#231#227'o'
      FieldName = 'cDescricao'
      Size = 100
    end
  end
  object dsParametro: TDataSource
    DataSet = qryParametro
    Left = 786
    Top = 48
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ParametroEmpresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 754
    Top = 80
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacParametro: TStringField
      DisplayLabel = 'Par'#226'metro Empresa|Par'#226'metro'
      FieldName = 'cParametro'
      Size = 15
    end
    object qryEmpresacValor: TStringField
      DisplayLabel = 'Par'#226'metro Empresa|Valor'
      FieldName = 'cValor'
      Size = 15
    end
    object qryEmpresacDescricao: TStringField
      DisplayLabel = 'Par'#226'metro Empresa|Descri'#231#227'o'
      FieldName = 'cDescricao'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 786
    Top = 80
  end
  object dsTabelaSistema: TDataSource
    DataSet = qryTabelaSistema
    Left = 688
    Top = 80
  end
  object qryTabelaSistema: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabelaSistema'
      '      ,cTabela'
      '      ,SUBSTRING(cNmTabela,1,100) as cNmTabela'
      '  FROM TabelaSistema'
      ' WHERE nCdTabelaSistema = :nPK')
    Left = 721
    Top = 80
    object qryTabelaSistemacNmTabela: TStringField
      DisplayWidth = 100
      FieldName = 'cNmTabela'
      Size = 100
    end
    object qryTabelaSistemanCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryTabelaSistemacTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
  end
  object qryParametroWeb: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryParametroWebCalcFields
    Parameters = <>
    SQL.Strings = (
      'select * from ParametroWeb')
    Left = 720
    Top = 48
    object qryParametroWebcParametro: TStringField
      FieldName = 'cParametro'
    end
    object qryParametroWebcURLWebService: TStringField
      FieldName = 'cURLWebService'
      Size = 400
    end
    object qryParametroWebnCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryParametroWebcNmTabela: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTabela'
      KeyFields = 'cNmTabela'
      Calculated = True
    end
  end
  object dsParametroWeb: TDataSource
    DataSet = qryParametroWeb
    Left = 688
    Top = 48
  end
end
