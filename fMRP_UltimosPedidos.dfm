inherited frmMRP_UltimosPedidos: TfrmMRP_UltimosPedidos
  Left = 38
  Top = 201
  Width = 985
  Height = 418
  BorderIcons = [biSystemMenu]
  Caption = #218'ltimos 30 Pedidos do Produto'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 969
    Height = 353
  end
  inherited ToolBar1: TToolBar
    Width = 969
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 969
    Height = 353
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsResultado
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtdePed
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtdeRec
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Fornecedor'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 233
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Dt Pedido'
        DataBinding.FieldName = 'dDtPedido'
      end
      object cxGrid1DBTableView1dDtEntregaIni: TcxGridDBColumn
        Caption = 'Prev. Entrega Inicial'
        DataBinding.FieldName = 'dDtEntregaIni'
      end
      object cxGrid1DBTableView1dDtEntregaFim: TcxGridDBColumn
        Caption = 'Prev. Entrega Final'
        DataBinding.FieldName = 'dDtEntregaFim'
      end
      object cxGrid1DBTableView1nPercentCompra: TcxGridDBColumn
        Caption = '% Balanc.'
        DataBinding.FieldName = 'nPercentCompra'
      end
      object cxGrid1DBTableView1nQtdePed: TcxGridDBColumn
        Caption = 'Qt. Pedida'
        DataBinding.FieldName = 'nQtdePed'
        Width = 89
      end
      object cxGrid1DBTableView1nQtdeRec: TcxGridDBColumn
        Caption = 'Qt. Recebida'
        DataBinding.FieldName = 'nQtdeRec'
        Width = 98
      end
      object cxGrid1DBTableView1cSiglaUnidadeMedida: TcxGridDBColumn
        Caption = 'UM'
        DataBinding.FieldName = 'cSiglaUnidadeMedida'
        Width = 39
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 30 Pedido.nCdPedido'
      '      ,cNmTerceiro'
      '      ,dDtPedido'
      '      ,CASE WHEN dDtEntregaIni IS NULL THEN dDtPrevEntIni'
      '            ELSE dDtEntregaIni'
      '       END dDtEntregaIni'
      '      ,CASE WHEN dDtEntregaFim IS NULL THEN dDtPrevEntFim'
      '            ELSE dDtEntregaFim'
      '       END dDtEntregaFim'
      '      ,nPercentCompra'
      '      ,cSiglaUnidadeMedida'
      '      ,nQtdePed-nQtdeCanc as nQtdePed'
      '      ,nQtdeExpRec        as nQtdeRec'
      '  FROM Pedido'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdPedido     = Pedid' +
        'o.nCdPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE nCdProduto = :nCdProduto'
      '   AND nCdEmpresa = :nCdEmpresa'
      '   AND nQtdeExpRec > 0'
      '   AND cFlgCompra = 1'
      ' ORDER BY dDtPedido DESC')
    Left = 240
    Top = 104
    object qryResultadonCdPedido: TIntegerField
      DisplayLabel = 'Pedido|N'#250'mero'
      FieldName = 'nCdPedido'
    end
    object qryResultadocNmTerceiro: TStringField
      DisplayLabel = 'Pedido|Fornecedor'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryResultadodDtPedido: TDateTimeField
      DisplayLabel = 'Pedido|Data Ped.'
      FieldName = 'dDtPedido'
    end
    object qryResultadodDtEntregaIni: TDateTimeField
      DisplayLabel = 'Entrega Prevista|Inicial'
      FieldName = 'dDtEntregaIni'
    end
    object qryResultadodDtEntregaFim: TDateTimeField
      DisplayLabel = 'Entrega Prevista|Final'
      FieldName = 'dDtEntregaFim'
    end
    object qryResultadonPercentCompra: TBCDField
      DisplayLabel = '%|Balanc.'
      FieldName = 'nPercentCompra'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonQtdePed: TBCDField
      DisplayLabel = 'Quantidades|Pedida'
      FieldName = 'nQtdePed'
      ReadOnly = True
      Precision = 32
    end
    object qryResultadonQtdeRec: TBCDField
      DisplayLabel = 'Quantidades|Recebida'
      FieldName = 'nQtdeRec'
      ReadOnly = True
      Precision = 32
    end
    object qryResultadocSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Quantidades|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 280
    Top = 104
  end
end
