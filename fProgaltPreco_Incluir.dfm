inherited frmProgaltPreco_Incluir: TfrmProgaltPreco_Incluir
  Left = 171
  Top = 179
  Width = 823
  Height = 293
  BorderIcons = [biSystemMenu]
  Caption = 'Inclus'#227'o de Altera'#231#227'o de Pre'#231'o'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 807
    Height = 228
  end
  object Label5: TLabel [1]
    Left = 54
    Top = 104
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 218
    Width = 79
    Height = 26
    Alignment = taRightJustify
    Caption = 'Dt. Programada Altera'#231#227'o'
    WordWrap = True
  end
  object Label2: TLabel [3]
    Left = 7
    Top = 137
    Width = 85
    Height = 13
    Caption = 'Valor Venda Atual'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [4]
    Left = 7
    Top = 167
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Novo Valor Venda'
  end
  object Label1: TLabel [5]
    Left = 9
    Top = 199
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Novo Valor Custo'
  end
  object Label6: TLabel [6]
    Left = 214
    Top = 167
    Width = 49
    Height = 13
    Caption = '(Opcional)'
  end
  object Label7: TLabel [7]
    Left = 214
    Top = 199
    Width = 49
    Height = 13
    Caption = '(Opcional)'
  end
  inherited ToolBar1: TToolBar
    Width = 807
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [9]
    Left = 8
    Top = 40
    Width = 281
    Height = 49
    Caption = 'N'#237'vel de Produto'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Referencia'
      'Cor'
      'Tamanho')
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object edtnCdProduto: TMaskEdit [10]
    Left = 96
    Top = 96
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtnCdProdutoExit
    OnKeyDown = edtnCdProdutoKeyDown
  end
  object edtDtProgramada: TMaskEdit [11]
    Left = 96
    Top = 224
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
    OnEnter = edtDtProgramadaEnter
    OnExit = edtDtProgramadaExit
  end
  object DBEdit1: TDBEdit [12]
    Tag = 1
    Left = 168
    Top = 96
    Width = 626
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 96
    Top = 129
    Width = 113
    Height = 21
    DataField = 'nValVenda'
    DataSource = DataSource1
    TabOrder = 7
  end
  object edtNovoPreco: TMaskEdit [14]
    Left = 96
    Top = 160
    Width = 113
    Height = 21
    TabOrder = 3
    OnExit = edtNovoPrecoExit
  end
  object edtNovoCusto: TMaskEdit [15]
    Left = 96
    Top = 192
    Width = 113
    Height = 21
    TabOrder = 4
    OnExit = edtNovoPrecoExit
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTabTipoProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,nValVenda'
      '     ,nCdTabTipoProduto'
      '  FROM Produto'
      ' WHERE nCdProduto        = :nPK'
      '   AND nCdTabTipoProduto = :nCdTabTipoProduto')
    Left = 304
    Top = 64
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutonValVenda: TBCDField
      FieldName = 'nValVenda'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryProdutonCdTabTipoProduto: TIntegerField
      FieldName = 'nCdTabTipoProduto'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 336
    Top = 64
  end
  object SP_INCLUI_PROG_ALT_PRECO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INCLUI_PROG_ALT_PRECO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@dDtProgramada'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@nValVendaAnterior'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end
      item
        Name = '@nValNovoPreco'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end
      item
        Name = '@nValNovoCusto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end>
    Left = 304
    Top = 152
  end
end
