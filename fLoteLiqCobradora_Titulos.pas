unit fLoteLiqCobradora_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmLoteLiqCobradora_Titulos = class(TfrmProcesso_Padrao)
    qryTitulos: TADOQuery;
    DBGridEh1: TDBGridEh;
    dsTitulos: TDataSource;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosnSaldoAnterior: TBCDField;
    qryTitulosnValLiq: TBCDField;
    qryTitulosnSaldoFinal: TBCDField;
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoteLiqCobradora_Titulos: TfrmLoteLiqCobradora_Titulos;

implementation

{$R *.dfm}

procedure TfrmLoteLiqCobradora_Titulos.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryTitulosnSaldoFinal.Value > 0) and (DataCol < 5) then
  begin
  
     DBGridEh1.Canvas.Brush.Color := clRed;
     DBGridEh1.Canvas.Font.Color  := clWhite ;

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;
end;

end.
