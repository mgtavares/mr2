unit fContaContabil_Vinculacoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmContaContabil_Vinculacoes = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryVinculacoes: TADOQuery;
    qryVinculacoesnCdAgrupamentoContabil: TIntegerField;
    qryVinculacoescNmAgrupamentoContabil: TStringField;
    qryVinculacoesnCdEmpresa: TIntegerField;
    qryVinculacoescNmEmpresa: TStringField;
    qryVinculacoescMascara: TStringField;
    qryVinculacoesiNivel: TIntegerField;
    dsVinculacoes: TDataSource;
    procedure exibeVinculacoes( nCdPlanoConta : integer );
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContaContabil_Vinculacoes: TfrmContaContabil_Vinculacoes;

implementation

{$R *.dfm}

{ TfrmContaContabil_Vinculacoes }

procedure TfrmContaContabil_Vinculacoes.exibeVinculacoes(
  nCdPlanoConta: integer);
begin

    qryVinculacoes.Close;
    PosicionaQuery( qryVinculacoes , intToStr( nCdPlanoConta ) ) ;

    Self.ShowModal;

end;

end.
