unit fAjusteEmpenho_Produtos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh, MemTableDataEh,
  MemTableEh, DataDriverEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmAjusteEmpenho_Produtos = class(TfrmProcesso_Padrao)
    qryProdutosEmpenhados: TADOQuery;
    dsProdutosEmpenhados: TDataSource;
    qryProdutosEmpenhadosnCdProduto: TIntegerField;
    qryProdutosEmpenhadoscNmProduto: TStringField;
    qryProdutosEmpenhadosnCdEmpresa: TIntegerField;
    qryProdutosEmpenhadosnQtdeEstoqueEmpenhado: TBCDField;
    qryProdutosEmpenhadosnCdLocalEstoque: TIntegerField;
    qryProdutosEmpenhadoscNmLocalEstoque: TStringField;
    qryProdutosEmpenhadosdDtEmpenho: TDateTimeField;
    qryProdutosEmpenhadoscNumeroOP: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    qryProdutosEmpenhadosnCdEstoqueEmpenhado: TAutoIncField;
    qryExcluiEmpenho: TADOQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdEstoqueEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmpenho: TcxGridDBColumn;
    cxGrid1DBTableView1nCdItemPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNumeroOP: TcxGridDBColumn;
    qryProdutosEmpenhadoscUsuarioAltManual: TStringField;
    qryProdutosEmpenhadosdDtAltManual: TDateTimeField;
    cxGrid1DBTableView1cUsuarioAltManual: TcxGridDBColumn;
    cxGrid1DBTableView1dDtAltManual: TcxGridDBColumn;
    qryProdutosEmpenhadosnCdPedido: TIntegerField;
    procedure ToolButton9Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAjusteEmpenho_Produtos: TfrmAjusteEmpenho_Produtos;

implementation

uses fAjusteEmpenho_IncluirEditar, fMenu;

{$R *.dfm}

procedure TfrmAjusteEmpenho_Produtos.ToolButton9Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Ap�s excluir o empenho essa a��o n�o poder� ser desfeita. Confirma a Exclus�o ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      abort;

  frmMenu.Connection.BeginTrans ;

  try
     qryExcluiEmpenho.Close;
     qryExcluiEmpenho.Parameters.ParamByName('nCdEstoqueEmpenhado').Value := qryProdutosEmpenhadosnCdEstoqueEmpenhado.Value;
     qryExcluiEmpenho.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans ;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Empenho exclu�do com sucesso');

  qryProdutosEmpenhados.Close;
  qryProdutosEmpenhados.Open;

end;

procedure TfrmAjusteEmpenho_Produtos.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmAjusteEmpenho_IncluirEditar;
begin
  inherited;

  objForm := TfrmAjusteEmpenho_IncluirEditar.Create(nil);

  objForm.qryEstoqueEmpenhado.Close;
  objForm.PosicionaQuery(objForm.qryEstoqueEmpenhado,qryProdutosEmpenhadosnCdEstoqueEmpenhado.AsString);

  objForm.Caption := 'Edi��o de Empenho';

  objForm.edtOP.ReadOnly           := True;
  objForm.edtOP.Color              := $00E9E4E4 ;
  objForm.edtOP.TabStop            := False;

  objForm.edtPedido.ReadOnly       := True;
  objForm.edtPedido.Color          := $00E9E4E4 ;
  objForm.edtPedido.TabStop        := False;

  objForm.edtProduto.ReadOnly      := True;
  objForm.edtProduto.Color         := $00E9E4E4 ;
  objForm.edtProduto.TabStop       := False;

  objForm.edtLocalEstoque.ReadOnly := True;
  objForm.edtLocalEstoque.Color    := $00E9E4E4 ;
  objForm.edtLocalEstoque.TabStop  := False;

  showForm(objForm,True);

  qryProdutosEmpenhados.Close;
  qryProdutosEmpenhados.Open;

end;

procedure TfrmAjusteEmpenho_Produtos.ToolButton7Click(Sender: TObject);
var
  objForm : TfrmAjusteEmpenho_IncluirEditar;
begin
  inherited;

  objForm := TfrmAjusteEmpenho_IncluirEditar.Create(nil);

  objForm.qryEstoqueEmpenhado.Close;
  objForm.qryEstoqueEmpenhado.Open;
  objForm.qryEstoqueEmpenhado.Insert;

  objForm.Caption := 'Inclus�o de Empenho';
  showForm(objForm,True);

  qryProdutosEmpenhados.Close;
  qryProdutosEmpenhados.Open;

end;

end.
