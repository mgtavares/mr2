unit fInventario_Digitacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, StdCtrls, Mask, DBCtrls, ADODB, cxLookAndFeelPainters,
  cxButtons, cxPC, cxControls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmInventario_Digitacao = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    qryInventario: TADOQuery;
    qryInventarionCdInventario: TIntegerField;
    qryInventarionCdEmpresa: TIntegerField;
    qryInventariocNmEmpresa: TStringField;
    qryInventariodDtAbertura: TDateTimeField;
    qryInventariodDtFech: TDateTimeField;
    qryInventarionCdGrupoEstoque: TIntegerField;
    qryInventariocNmGrupoEstoque: TStringField;
    qryInventarionCdLocalEstoque: TIntegerField;
    qryInventariocNmLocalEstoque: TStringField;
    qryInventariocResponsavel: TStringField;
    qryInventariocOBS: TStringField;
    dsInventario: TDataSource;
    qryItemInventario: TADOQuery;
    qryItemInventarionCdInventario: TIntegerField;
    qryItemInventarioiItem: TIntegerField;
    qryItemInventarionCdProduto: TIntegerField;
    qryItemInventarionCdLocalEstoque: TIntegerField;
    qryItemInventarionQtdeFisica: TBCDField;
    qryItemInventarionQtdeLogica: TBCDField;
    qryItemInventarionDiferenca: TBCDField;
    qryItemInventarionPercDif: TBCDField;
    dsItemInventario: TDataSource;
    qryItemInventariocNmProduto: TStringField;
    qryItemInventariocNmLocalEstoque: TStringField;
    qryLocalEstoque: TADOQuery;
    qryProduto: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    SP_APURA_CONTAGEM_INVENTARIO: TADOStoredProc;
    ToolButton7: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    cxButton1: TcxButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    cxButton2: TcxButton;
    btnCancelarInv: TcxButton;
    qryInventarionCdUsuarioAbertura: TIntegerField;
    qryInventarionCdUsuarioFech: TIntegerField;
    qryInventarionCdUsuarioCancel: TIntegerField;
    qryInventariodDtCancel: TDateTimeField;
    DBEdit6: TDBEdit;
    Label3: TLabel;
    qryUsuarioAbertura: TADOQuery;
    qryUsuarioFechamento: TADOQuery;
    qryUsuarioCancel: TADOQuery;
    dsUsuarioAbertura: TDataSource;
    dsUsuarioCancel: TDataSource;
    dsUsuarioFechamento: TDataSource;
    qryUsuarioCancelnCdUsuario: TIntegerField;
    qryUsuarioCancelcNmUsuario: TStringField;
    qryUsuarioFechamentonCdUsuario: TIntegerField;
    qryUsuarioFechamentocNmUsuario: TStringField;
    qryUsuarioAberturanCdUsuario: TIntegerField;
    qryUsuarioAberturacNmUsuario: TStringField;
    DBEdit7: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    qryInventariodDtContagemEncerrada: TDateTimeField;
    qryItemInventarionCdItemInventario: TLargeintField;
    procedure qryInventarioAfterScroll(DataSet: TDataSet);
    procedure qryInventarioAfterClose(DataSet: TDataSet);
    procedure qryItemInventarioCalcFields(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure btnCancelarInvClick(Sender: TObject);
    procedure qryItemInventarioAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventario_Digitacao: TfrmInventario_Digitacao;

implementation

uses fMenu, rInventario, fLookup_Padrao;

{$R *.dfm}

procedure TfrmInventario_Digitacao.qryInventarioAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryItemInventario, qryInventarionCdInventario.AsString) ;
  PosicionaQuery(qryUsuarioAbertura, qryInventarionCdUsuarioAbertura.AsString) ;
  PosicionaQuery(qryUsuarioFechamento, qryInventarionCdUsuarioFech.AsString) ;
  PosicionaQuery(qryUsuarioCancel, qryInventarionCdUsuarioCancel.AsString) ;
  
end;

procedure TfrmInventario_Digitacao.qryInventarioAfterClose(
  DataSet: TDataSet);
begin
  inherited;

  qryItemInventario.Close ;
  qryUsuarioAbertura.Close ;
  qryUsuarioFechamento.Close ;
  qryUsuarioCancel.Close ;
  
end;

procedure TfrmInventario_Digitacao.qryItemInventarioCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryLocalEstoque.Close;
  qryProduto.Close ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryInventarionCdEmpresa.Value ;

  PosicionaQuery(qryProduto, qryItemInventarionCdProduto.AsString) ;
  PosicionaQuery(qryLocalEstoque, qryItemInventarionCdLocalEstoque.AsString) ;

  if not qryProduto.eof then
      qryItemInventariocNmProduto.Value := qryProdutocNmProduto.Value ;

  if not qryLocalEstoque.Eof then
      qryItemInventariocNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
      
end;

procedure TfrmInventario_Digitacao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if not qryInventario.Active or (qryInventarionCdInventario.Value = 0) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      exit ;
  end ;

  if (qryInventariodDtFech.AsString <> '') then
  begin
      MensagemAlerta('Invent�rio j� foi encerrado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_APURA_CONTAGEM_INVENTARIO.Close ;
      SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value  := qryInventarionCdInventario.Value ;
      SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@cFlgAtuEstoque').Value := 'N' ;
      SP_APURA_CONTAGEM_INVENTARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryItemInventario, qryInventarionCdInventario.AsString) ;

end;

procedure TfrmInventario_Digitacao.ToolButton7Click(Sender: TObject);
begin
  inherited;

  if not qryInventario.Active or (qryInventarionCdInventario.Value = 0) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      exit ;
  end ;

  if (qryInventariodDtFech.AsString <> '') then
  begin
      MensagemAlerta('Invent�rio j� foi encerrado.') ;
      exit ;
  end ;

  case MessageDlg('Esta atualiza��o ir� sobrepor a posi��o do estoque da data de abertura pela posi��o de estoque atual.' + #13#13 +'Esta opera��o n�o poder� ser estornada.' + #13#13 + 'Voc� confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;


  frmMenu.Connection.BeginTrans;

  try
      SP_APURA_CONTAGEM_INVENTARIO.Close ;
      SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value  := qryInventarionCdInventario.Value ;
      SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@cFlgAtuEstoque').Value := 'S' ;
      SP_APURA_CONTAGEM_INVENTARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryItemInventario, qryInventarionCdInventario.AsString) ;

end;

procedure TfrmInventario_Digitacao.cxButton1Click(Sender: TObject);
var
  i:Integer ;
begin

    i := frmMenu.BuscaPK() ;

    If (i > 0) then
    begin
        PosicionaQuery(qryInventario, IntToStr(i)) ;

        DbGridEh1.ReadOnly := False ;

        if not qryInventario.eof then
        begin
            if (qryInventariodDtFech.asString <> '') then
            begin
                MensagemAlerta('Invent�rio j� foi encerrado, somente consulta � permitida.') ;
                DbGridEh1.ReadOnly  := True ;
                ToolButton1.Enabled := False ;
                exit ;
            end ;
        end ;

        ToolButton1.Enabled := True ;
        DBGridEh1.SetFocus;

    end ;

end;

procedure TfrmInventario_Digitacao.ToolButton6Click(Sender: TObject);
var
  objRel : TrptInventario_view;
begin
  inherited;

  if (qryInventario.isEmpty) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      abort;
  end ;

  objRel := TrptInventario_view.Create(nil);

  try
      try
          objRel.SPREL_INVENTARIO.Close ;
          objRel.SPREL_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := qryInventarionCdInventario.Value ;
          objRel.SPREL_INVENTARIO.Open ;

          objRel.Prepare;
          objRel.QRTotalPagina.Caption := '/' + IntToStr(objRel.QRPrinter.PageCount) ;
          objRel.QRPrinter.Free;
          objRel.QRPrinter := nil ;

          objRel.lblEmpresa.Caption      := frmMenu.cNmEmpresaAtiva;
          objRel.lblEmpresa2.Caption     := frmMenu.cNmEmpresaAtiva;
          objRel.lblInventario.Caption   := qryInventarionCdInventario.asString;
          objRel.Preview;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmInventario_Digitacao.cxButton2Click(Sender: TObject);
var
  nCdPK       : Integer ;
begin

  nCdPK := frmLookup_Padrao.ExecutaConsulta(216) ;

  if (nCdPK > 0) then
  begin
      DbGridEh1.ReadOnly := False ;

      PosicionaQuery(qryInventario, IntToStr(nCdPK)) ;
  end ;

end;

procedure TfrmInventario_Digitacao.btnCancelarInvClick(Sender: TObject);
begin
  inherited;

  if (qryInventario.IsEmpty) then
      Exit;

  qryInventario.Edit;

  if not (qryInventariodDtCancel.IsNull) then
  begin
      MensagemAlerta('O invent�rio j� est� cancelado.');

      Exit;
  end;

  if not (qryInventariodDtContagemEncerrada.IsNull) then
  begin
      MensagemAlerta('A contagem do invent�rio j� foi encerrada.');

      Exit;
  end;

  if ((MessageDLG('Confirma o cancelamento do invent�rio?', mtConfirmation, [mbYes, mbNo], 0)) = mrNo) then
      Exit;

  qryInventariodDtCancel.Value        := Now;
  qryInventarionCdUsuarioCancel.Value := frmMenu.nCdUsuarioLogado;

  qryInventario.Post;

  ShowMessage('Invent�rio cancelado com sucesso.');

  qryInventario.Close;
  qryInventario.Open;

end;

procedure TfrmInventario_Digitacao.qryItemInventarioAfterOpen(
  DataSet: TDataSet);
begin
    inherited;

    if not (qryInventario.IsEmpty) then
    begin
        if not (qryInventariodDtFech.IsNull) then
        begin
            MensagemAlerta('Invent�rio encerrado, somente consulta � permitida.') ;
            DbGridEh1.ReadOnly  := True ;
            ToolButton1.Enabled := False ;
            exit ;
        end ;

        if not (qryInventariodDtCancel.IsNull) then
        begin
            MensagemAlerta('Invent�rio cancelado, somente consulta � permitida.') ;
            DbGridEh1.ReadOnly  := True ;
            ToolButton1.Enabled := False ;
            exit ;
        end ;

        ToolButton1.Enabled := True ;
        DBGridEh1.SetFocus;

    end ;

end;

initialization
    RegisterClass(TfrmInventario_Digitacao) ;

end.
