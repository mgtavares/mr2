unit fCaixa_AtualizaCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxButtons, jpeg, ExtCtrls, DB, ADODB, DBCtrls, Mask;

type
  TfrmCaixa_AtualizaCliente = class(TForm)
    Image2: TImage;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    Label12: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    ComboBoxDom: TComboBox;
    MaskEditEmail: TMaskEdit;
    MaskEditCel: TMaskEdit;
    DBEditNome: TDBEdit;
    DBEditCPF: TDBEdit;
    qryAtualizaCel: TADOQuery;
    qryAtualizaEmail: TADOQuery;
    qryValidaCel: TADOQuery;
    qryValidaEmail: TADOQuery;
    Label2: TLabel;
    Label4: TLabel;
    qryAtualizaData: TADOQuery;
    DBEditPK: TDBEdit;
    Label5: TLabel;
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_AtualizaCliente: TfrmCaixa_AtualizaCliente;

implementation

{$R *.dfm}

uses
  fMenu, fCaixa_Recebimento, fCadastro_Template, fMensagem;

function RemoveAcento(aText : string) : string;
const
  ComAcento = '��������������������������������';
  SemAcento = 'aaeouaoaeioucunyAAEOUAOAEIOUCUNY';
var
  x: Cardinal;
begin;
  for x := 1 to Length(aText) do
  try
    if (Pos(aText[x], ComAcento) <> 0) then
      aText[x] := SemAcento[ Pos(aText[x], ComAcento) ];
  except on E: Exception do
    raise Exception.Create('Erro no processo.');
  end;
 
  Result := aText;
end;

function ValidarEMail(aStr: string): Boolean;
begin 
 aStr := Trim(UpperCase(aStr));
 if Pos('@', aStr) > 1 then 
 begin 
   Delete(aStr, 1, pos('@', aStr)); 
   Result := (Length(aStr) > 0) and (Pos('.', aStr) > 2); 
 end 
 else 
   Result := False; 
end;

function TfrmCaixa_AtualizaCliente.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmCaixa_AtualizaCliente.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
    {frmMenu.nShowMessage(cTexto);}
end;

procedure TfrmCaixa_AtualizaCliente.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
    {frmMenu.nMensagemErro(cTexto);}
end;

procedure TfrmCaixa_AtualizaCliente.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
    {frmMenu.nMensagemAlerta(cTexto);}
end;


procedure TfrmCaixa_AtualizaCliente.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmCaixa_AtualizaCliente.btnCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmCaixa_AtualizaCliente.btnConfirmarClick(Sender: TObject);
var
  NovoCel, NovoEmail, loja, usuario, qry, nCdTerceiro  : String;
  NewForm : TForm;
begin
  try
    nCdTerceiro := DBEditPK.Text;
    if ((MaskEditEmail.Text = '') and (MaskEditCel.Text = '(  )     -    ')) then
      begin
        ShowMessage('Preencha os campos!');
        NewForm := TfrmCaixa_AtualizaCliente.Create(Self);
        NewForm.ShowModal;
        Close;
      end
    else
      begin
        if ((MaskEditEmail.Text <> '') and (ComboBoxDom.Text <> '')) then
          begin
            NovoEmail := RemoveAcento(MaskEditEmail.Text + ComboBoxDom.Text);
            if ((ValidarEMail(NovoEmail)) and (pos('@',MaskEditEmail.Text) = 0)) then
              begin
                qryValidaEmail.Close;
                qryValidaEmail.SQL.Clear;
                qryValidaEmail.SQL.Add('SELECT TOP 1 cEmail FROM Terceiro WHERE nCdTerceiro = ' + nCdTerceiro);
                qryValidaEmail.Open;
                if NovoEmail <> qryValidaEmail.FieldByName('cEmail').Text then
                  begin
                    qryAtualizaEmail.Close;
                    qryAtualizaEmail.SQL.Clear;
                    qryAtualizaEmail.SQL.Add('UPDATE Terceiro SET cEmail = ' + chr(39) + NovoEmail + chr(39) + 'WHERE nCdTerceiro = ' + nCdTerceiro);
                    qryAtualizaEmail.ExecSQL;
                  end;
              end
            else
              begin
                ShowMessage('Email inv�lido!');
                MaskEditEmail.Text := '';
                ComboBoxDom.Text := '@hotmail.com';
                NewForm := TfrmCaixa_AtualizaCliente.Create(Self);
                NewForm.ShowModal;
                Close;
              end;
          end;
        if (MaskEditCel.Text <> '(  )     -    ') then
          begin
            NovoCel := StringReplace(StringReplace(StringReplace(StringReplace(MaskEditCel.Text,'(','',[rfReplaceAll, rfIgnoreCase]),')','',[rfReplaceAll, rfIgnoreCase]),'-','',[rfReplaceAll, rfIgnoreCase]),' ','',[rfReplaceAll, rfIgnoreCase]);
            if Length(NovoCel) = 11 then
              begin
                qryValidaCel.Close;
                qryValidaCel.SQL.Clear;
                qryValidaCel.SQL.Add('SELECT TOP 1 cTelefoneMovel FROM Terceiro WHERE nCdTerceiro = ' + nCdTerceiro);
                qryValidaCel.Open;
                if NovoCel <> qryValidaCel.FieldByName('cTelefoneMovel').Text then
                  begin
                    qryAtualizaCel.Close;
                    qryAtualizaCel.SQL.Clear;
                    qryAtualizaCel.SQL.Add('UPDATE Terceiro SET cTelefoneMovel = ' + chr(39) +  NovoCel + chr(39) + 'WHERE nCdTerceiro = ' + nCdTerceiro);
                    qryAtualizaCel.ExecSQL;
                  end;
              end
            else
              begin
                ShowMessage('N�mero de celular inv�lido! digite o ddd (2 dig.) + n�m. (9 dig.).');
                MaskEditCel.Text := '(  )     -    ';
                NewForm := TfrmCaixa_AtualizaCliente.Create(Self);
                NewForm.ShowModal;
                Close;
              end;
          end;
        if (StrToInt(nCdTerceiro) > 0 ) and ((((MaskEditEmail.Text <> '') and (ComboBoxDom.Text <> '')) or (MaskEditCel.Text <> '(  )     -    '))) then
          begin
            usuario := IntToStr(frmMenu.nCdUsuarioLogado);
            loja := IntToStr(frmMenu.nCdLojaAtiva);
            qry := 'UPDATE PessoaFisica SET dDtUltAlt = GETDATE() ' +
                   ',nCdLojaUltAlt = ' + loja +
                   ',nCdUsuarioUltAlt = ' + usuario +
                   ' WHERE nCdPessoaFisica = (' +
                   'SELECT nCdPessoaFisica FROM PessoaFisica AS P INNER JOIN Terceiro AS T ON T.nCdTerceiro = P.nCdTerceiro ' +
                   'WHERE T.nCdTerceiro = ' + nCdTerceiro + '); ' +
                   'INSERT INTO ClienteAtualizado (nCdTerceiro,cCNPJCPF,dDtUltAlt,nCdLojaUltAlt,nCdUsuarioUltAlt) VALUES('
                                                            + nCdTerceiro
                                                            + ',' + chr(39) + DBEditCPF.Text + chr(39)
                                                            + ',GETDATE()'
                                                            + ',' + loja
                                                            + ',' + usuario
                                                            + ');';
            qryAtualizaData.Close;
            qryAtualizaData.SQL.Clear;
            qryAtualizaData.SQL.Add(qry);
            qryAtualizaData.ExecSQL;
            ShowMessage('Dados atualizados com sucesso!');
          end;
    end;
  except
    begin
      ShowMessage('Algo deu errado, tente novamente!');
      NewForm := TfrmCaixa_AtualizaCliente.Create(Self);
      NewForm.ShowModal;
      Close;
    end;
  end;
  Close;    
end;

initialization
   RegisterClass(TfrmCaixa_AtualizaCliente);

end.
