inherited frmGrupoComissao: TfrmGrupoComissao
  Left = 46
  Top = 177
  Caption = 'Grupo de Comiss'#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 78
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 31
    Top = 62
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Comiss'#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 38
    Top = 158
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dia Pagamento'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 10
    Top = 86
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria Financeira'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 48
    Top = 110
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 26
    Top = 134
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Neg'#243'cio'
    FocusControl = DBEdit6
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoComissao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [9]
    Left = 120
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoComissao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [10]
    Left = 120
    Top = 152
    Width = 65
    Height = 19
    DataField = 'iDiaPagamento'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit4: TDBEdit [11]
    Left = 120
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdCategFinanc'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [12]
    Left = 120
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [13]
    Left = 120
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object cxPageControl1: TcxPageControl [14]
    Left = 16
    Top = 200
    Width = 1081
    Height = 369
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 7
    ClientRectBottom = 365
    ClientRectLeft = 4
    ClientRectRight = 1077
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Regras de Comiss'#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1073
        Height = 341
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRegraGrupoComissao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRegraGrupoComissao'
            Footers = <>
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoComissao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoProduto'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmGrupoProduto'
            Footers = <>
            ReadOnly = True
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nCdTerceiro'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            ReadOnly = True
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaIni'
            Footers = <>
            Width = 103
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaFim'
            Footers = <>
            Width = 104
          end
          item
            EditButtons = <>
            FieldName = 'nPercBaseCalc'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nPercComissao'
            Footers = <>
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'cFlgAtivo'
            Footers = <>
            Width = 41
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit7: TDBEdit [15]
    Tag = 1
    Left = 192
    Top = 80
    Width = 577
    Height = 19
    DataField = 'cNmCategFinanc'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit8: TDBEdit [16]
    Tag = 1
    Left = 192
    Top = 104
    Width = 577
    Height = 19
    DataField = 'cNmCC'
    DataSource = DataSource2
    TabOrder = 9
  end
  object DBEdit9: TDBEdit [17]
    Tag = 1
    Left = 192
    Top = 128
    Width = 577
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource3
    TabOrder = 10
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoComissao'
      'WHERE nCdGrupoComissao = :nPK')
    object qryMasternCdGrupoComissao: TIntegerField
      FieldName = 'nCdGrupoComissao'
    end
    object qryMastercNmGrupoComissao: TStringField
      FieldName = 'cNmGrupoComissao'
      Size = 50
    end
    object qryMasteriDiaPagamento: TIntegerField
      FieldName = 'iDiaPagamento'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
  end
  object qryRegraGrupoComissao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryRegraGrupoComissaoBeforePost
    OnCalcFields = qryRegraGrupoComissaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RegraGrupoComissao'
      'WHERE nCdGrupoComissao = :nPK')
    Left = 472
    Top = 336
    object qryRegraGrupoComissaonCdRegraGrupoComissao: TAutoIncField
      DisplayLabel = 'C'#243'd Regra'
      FieldName = 'nCdRegraGrupoComissao'
      ReadOnly = True
    end
    object qryRegraGrupoComissaonCdGrupoComissao: TIntegerField
      DisplayLabel = 'Regras para C'#225'lculo da Comiss'#227'o'
      FieldName = 'nCdGrupoComissao'
    end
    object qryRegraGrupoComissaonCdGrupoProduto: TIntegerField
      DisplayLabel = 'Grupo de Produtos|C'#243'd'
      FieldName = 'nCdGrupoProduto'
    end
    object qryRegraGrupoComissaonValVendaIni: TBCDField
      DisplayLabel = 'Intervalo de Faturamento (R$)|De'
      FieldName = 'nValVendaIni'
      Precision = 12
      Size = 2
    end
    object qryRegraGrupoComissaonValVendaFim: TBCDField
      DisplayLabel = 'Intervalo de Faturamento (R$)|At'#233
      FieldName = 'nValVendaFim'
      Precision = 12
      Size = 2
    end
    object qryRegraGrupoComissaonPercBaseCalc: TBCDField
      DisplayLabel = 'Instru'#231#227'o para c'#225'lculo|% Base C'#225'lculo'
      FieldName = 'nPercBaseCalc'
      Precision = 12
      Size = 2
    end
    object qryRegraGrupoComissaonPercComissao: TBCDField
      DisplayLabel = 'Instru'#231#227'o para c'#225'lculo|% Comiss'#227'o'
      FieldName = 'nPercComissao'
      Precision = 12
      Size = 2
    end
    object qryRegraGrupoComissaocFlgAtivo: TIntegerField
      DisplayLabel = 'Regra|Ativa'
      FieldName = 'cFlgAtivo'
    end
    object qryRegraGrupoComissaocNmGrupoProduto: TStringField
      DisplayLabel = 'Grupo de Produtos|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmGrupoProduto'
      Size = 50
      Calculated = True
    end
    object qryRegraGrupoComissaonCdTerceiro: TIntegerField
      DisplayLabel = 'Terceiro|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryRegraGrupoComissaocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
  end
  object dsRegraGrupoComissao: TDataSource
    DataSet = qryRegraGrupoComissao
    Left = 504
    Top = 336
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 376
    Top = 360
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE EXISTS(SELECT 1 '
      '                FROM TerceiroTipoTerceiro TTT '
      '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 2)'
      '   AND Terceiro.nCdTerceiro = :nPK'
      '')
    Left = 500
    Top = 384
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategFinanc'
      'FROM CategFinanc'
      'WHERE cFlgRecDes = '#39'D'#39
      'AND nCdCategFinanc = :nPK')
    Left = 656
    Top = 96
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryCategFinanc
    Left = 496
    Top = 272
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCC'
      '  FROM CentroCusto'
      ' WHERE nCdStatus = 1'
      '   AND cFlgLanc  = 1'
      '   AND nCdCC     = :nPK')
    Left = 696
    Top = 120
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCC
    Left = 504
    Top = 280
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 696
    Top = 184
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 512
    Top = 288
  end
end
