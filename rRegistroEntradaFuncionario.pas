unit rRegistroEntradaFuncionario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, ER2Lookup, cxControls, cxContainer,
  cxEdit, cxGroupBox;

type
  TrptRegistroEntradaFuncionario = class(TfrmRelatorio_Padrao)
    edtDtFinal: TMaskEdit;
    Label14: TLabel;
    edtDtInicial: TMaskEdit;
    Label18: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qryLoja: TADOQuery;
    qryEmpresa: TADOQuery;
    qryFuncionario: TADOQuery;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    dsFuncionario: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    edtEmpresa: TER2LookupMaskEdit;
    edtLoja: TER2LookupMaskEdit;
    edtFuncionario: TER2LookupMaskEdit;
    qryFuncionarionCdUsuario: TIntegerField;
    qryFuncionariocNmTerceiro: TStringField;
    qryLojanCdEmpresa: TIntegerField;
    procedure edtFuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtFuncionarioExit(Sender: TObject);
    procedure qryFuncionarioAfterOpen(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEmpresaAfterOpen(DataSet: TDataSet);
    procedure qryLojaAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRegistroEntradaFuncionario: TrptRegistroEntradaFuncionario;

implementation

uses
    fMenu, fLookup_Padrao, rRegistroEntradaFuncionario_view;

{$R *.dfm}

procedure TrptRegistroEntradaFuncionario.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
  qryLoja.Parameters.ParamByName('nCdLoja').Value    := IntToStr(frmMenu.nCdLojaAtiva);
  qryLoja.Open;

  edtEmpresa.SetFocus;
end;

procedure TrptRegistroEntradaFuncionario.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text);

  if (qryLojanCdEmpresa.Value <> qryEmpresanCdEmpresa.Value) then
  begin
      qryLoja.Close;
      qryFuncionario.Close;

      edtLoja.Clear;
      edtFuncionario.Clear;
  end;
end;

procedure TrptRegistroEntradaFuncionario.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : integer;
begin
  inherited;

  if (Key = VK_F4) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta(25);

      if (nPK <> 0) then
      begin
          qryEmpresa.Close;
          PosicionaQuery(qryEmpresa, IntToStr(nPK));

          qryLoja.Close;
          qryFuncionario.Close;

          edtLoja.Clear;
          edtFuncionario.Clear;
      end;
  end;
end;

procedure TrptRegistroEntradaFuncionario.edtLojaExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtLoja.Text) <> '') then
      if (Trim(edtEmpresa.Text) <> '') then
      begin
          if (Trim(edtLoja.Text) <> qryLojanCdLoja.AsString) then
          begin
              qryLoja.Close;
              qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
              qryLoja.Parameters.ParamByName('nCdLoja').Value    := edtLoja.Text;
              qryLoja.Open;

              qryFuncionario.Close;

              edtFuncionario.Clear;
          end;
      end

      else
      begin
          MensagemAlerta('Selecione a empresa!');

          qryLoja.Close;
          qryFuncionario.Close;

          edtLoja.Clear;
          edtFuncionario.Clear;

          edtEmpresa.SetFocus;
      end;
end;

procedure TrptRegistroEntradaFuncionario.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : integer;
begin
  inherited;

  if (Trim(edtEmpresa.Text) = '') then
  begin
      MensagemAlerta('Selecione uma empresa!');

      qryLoja.Close;
      qryFuncionario.Close;

      edtLoja.Clear;
      edtFuncionario.Clear;

      edtEmpresa.SetFocus;
  end

  else if (Key = VK_F4) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta2(147, 'nCdEmpresa = ' + edtEmpresa.Text);

      if (nPK <> 0) then
      begin
          qryLoja.Close;
          qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
          qryLoja.Parameters.ParamByName('nCdLoja').Value    := nPK;
          qryLoja.Open;

          qryFuncionario.Close;

          edtFuncionario.Clear;
      end;
  end;
end;

procedure TrptRegistroEntradaFuncionario.edtFuncionarioExit(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtFuncionario.Text) <> '') then
      if (Trim(edtLoja.Text) <> '') then
      begin
          qryFuncionario.Close;
          qryFuncionario.Parameters.ParamByName('nCdLoja').Value      := qryLojanCdLoja.Value;
          qryFuncionario.Parameters.ParamByName('nCdUsuario').Value   := edtFuncionario.Text;
          qryFuncionario.Parameters.ParamByName('nCdLojaAtiva').Value := frmMenu.nCdLojaAtiva;
          qryFuncionario.Open;
      end

      else
      begin
          MensagemAlerta('Selecione uma loja!');

          qryFuncionario.Close;

          edtFuncionario.Clear;

          edtLoja.SetFocus;
      end;
end;

procedure TrptRegistroEntradaFuncionario.edtFuncionarioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : integer;
begin
  inherited;

  if (Trim(edtLoja.Text) = '') then
  begin
      MensagemAlerta('Selecione uma loja!');

      qryFuncionario.Close;

      edtFuncionario.Clear;

      edtLoja.SetFocus;
  end

  else if (Key = VK_F4) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta2(5, 'EXISTS (SELECT TOP 1 1 ' +
                                                            'FROM UsuarioLoja UL ' +
                                                           'WHERE UL.nCdUsuario = Usuario.nCdUsuario ' +
                                                             'AND UL.nCdLoja    = ' + qryLojanCdLoja.AsString + ')');

      if (nPK <> 0) then
      begin
          qryFuncionario.Close;
          qryFuncionario.Parameters.ParamByName('nCdLoja').Value      := IntToStr(qryLojanCdLoja.Value);
          qryFuncionario.Parameters.ParamByName('nCdUsuario').Value   := nPK;
          qryFuncionario.Parameters.ParamByName('nCdLojaAtiva').Value := frmMenu.nCdLojaAtiva;
          qryFuncionario.Open;

          edtFuncionario.Text := qryFuncionarionCdUsuario.AsString;
      end;
  end;
end;

procedure TrptRegistroEntradaFuncionario.qryEmpresaAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  edtEmpresa.Text := qryEmpresanCdEmpresa.AsString;
end;

procedure TrptRegistroEntradaFuncionario.qryLojaAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  edtLoja.Text := qryLojanCdLoja.AsString;
end;

procedure TrptRegistroEntradaFuncionario.qryFuncionarioAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  edtFuncionario.Text := qryFuncionarionCdUsuario.AsString;
end;

procedure TrptRegistroEntradaFuncionario.ToolButton1Click(Sender: TObject);
var
  objRel : TrptRegistroEntradaFuncionario_view;
  dDtInicial, dDtFinal : string;
  auxDia, auxMes, auxAno : Word;
begin
  inherited;

  if (qryEmpresa.IsEmpty) then
  begin
      MensagemErro('Selecione uma empresa!');

      Abort;
  end;

  if (qryLoja.IsEmpty) then
  begin
      MensagemErro('Selecione uma loja!');

      Abort;
  end;
  
  if (Trim(edtDtInicial.Text) <> '/  /') then
  begin
      if (Trim(edtDtFinal.Text) = '/  /') then
          dDtFinal := DateToStr(Date)

      else if ((StrToDate(edtDtInicial.Text)) > (StrToDate(edtDtFinal.Text))) then
      begin
          MensagemErro('Per�odo Inv�lido!');

          Abort;
      end

      else
          dDtFinal   := edtDtFinal.Text;

      dDtInicial := edtDtInicial.Text;
  end

  else
  begin
      DecodeDate(Date, auxAno, auxMes, auxDia);

      dDtInicial := DateToStr(EncodeDate(auxAno, auxMes, 1));

      if (Trim(edtDtFinal.Text) = '/  /') then
          dDtFinal := DateToStr(Date)
      else
          dDtFinal := edtDtFinal.Text;
  end;

  objRel := TrptRegistroEntradaFuncionario_view.Create(nil);

  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Close;
  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@nCdLoja').Value    := qryLojanCdLoja.Value;

  if (Trim(edtFuncionario.Text) = '') then
      objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@nCdFuncionario').Value := 0
  else
      objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@nCdFuncionario').Value := qryFuncionarionCdUsuario.Value;

  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@dDtInicial').Value := dDtInicial;
  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Parameters.ParamByName('@dDtFinal').Value   := dDtFinal;
  objRel.SPREL_REGISTRO_ENTRADA_FUNCIONARIO.Open;

  objRel.lblPeriodo.Caption := dDtInicial + ' � ' + dDtFinal;
  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.QuickRep1.Preview;

  FreeAndNil(objRel);
end;

initialization

    RegisterClass(TrptRegistroEntradaFuncionario);

end.
