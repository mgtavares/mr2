unit fAvaliacaoCQM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping;

type
  TfrmAvaliacaoCQM = class(TfrmCadastro_Padrao)
    qryMasternCdAvaliacaoCQM: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNrDocto: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTipoReceb: TIntegerField;
    qryMasterdDtEntrega: TDateTimeField;
    qryMasternCdTipoResultadoCQM: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuario: TIntegerField;
    qryMasterdDtFech: TDateTimeField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit9: TDBEdit;
    DataSource1: TDataSource;
    qryTipoReceb: TADOQuery;
    qryTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoRecebcNmTipoReceb: TStringField;
    qryTipoRecebnCdQuestionarioCQM: TIntegerField;
    DBEdit10: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    qryRespostas: TADOQuery;
    qryRespostasnCdRespAvaliacaoCQM: TAutoIncField;
    qryRespostasnCdAvaliacaoCQM: TIntegerField;
    qryRespostasiOrdem: TIntegerField;
    qryRespostascPergunta: TStringField;
    qryRespostasnCdTabTipoResposta: TIntegerField;
    qryRespostascResposta: TStringField;
    qryGeraPerguntas: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsRespostas: TDataSource;
    qryTabTipoResposta: TADOQuery;
    qryTabTipoRespostacNmTabTipoResposta: TStringField;
    qryRespostascNmTabTipoResposta: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    qryTipoResultadoCQM: TADOQuery;
    qryTipoResultadoCQMnCdTipoResultadoCQM: TIntegerField;
    qryTipoResultadoCQMcNmTipoResultadoCQM: TStringField;
    qryTipoResultadoCQMcFlgNC: TIntegerField;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    btFinalizar: TToolButton;
    qryMastercEmitente: TStringField;
    qryMastercAvaliador: TStringField;
    qryMastercNmLocalEntrega: TStringField;
    Label11: TLabel;
    DBEdit14: TDBEdit;
    Label12: TLabel;
    DBEdit15: TDBEdit;
    Label13: TLabel;
    DBEdit16: TDBEdit;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryQuestionarioCQM: TADOQuery;
    qryQuestionarioCQMcEmitente: TStringField;
    qryQuestionarioCQMcAvaliador: TStringField;
    qryMasternCdQuestionarioCQM: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    ToolButton12: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryRespostasCalcFields(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure btFinalizarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure DBEdit14KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAvaliacaoCQM: TfrmAvaliacaoCQM;

implementation

uses fLookup_Padrao, fCentralArquivos, fMenu, rAvaliacaoCQM_View;

{$R *.dfm}

procedure TfrmAvaliacaoCQM.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'AVALIACAOCQM' ;
  nCdTabelaSistema  := 78 ;
  nCdConsultaPadrao := 183 ;
end;

procedure TfrmAvaliacaoCQM.btIncluirClick(Sender: TObject);
begin
  inherited;

  btFinalizar.Enabled := True ;
  DBGridEh1.ReadOnly  := false ;

  DBEdit2.SetFocus;
  
end;

procedure TfrmAvaliacaoCQM.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit3.Text) ;
  
end;

procedure TfrmAvaliacaoCQM.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoReceb.Close ;
  PosicionaQuery(qryTipoReceb, DBEdit4.Text) ;

  if (not qryTipoReceb.eof) and (qryTipoRecebnCdQuestionarioCQM.Value <= 0) then
  begin
      MensagemAlerta('Este tipo de recebimento n�o tem nenhum question�rio de CQM vinculado.') ;
      qryTipoReceb.Close ;
      DBEdit4.SetFocus;
      exit ;
  end ;

  qryQuestionarioCQM.Close ;
  PosicionaQuery(qryQuestionarioCQM, qryTipoRecebnCdQuestionarioCQM.AsString) ;

  if not (qryQuestionarioCQM.Eof) then
  begin
  
      qryMastercEmitente.Value          := qryQuestionarioCQMcEmitente.Value ;
      qryMastercAvaliador.Value         := qryQuestionarioCQMcAvaliador.Value ;
      qryMasternCdQuestionarioCQM.Value := qryTipoRecebnCdQuestionarioCQM.Value ;

  end ;
  
end;

procedure TfrmAvaliacaoCQM.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(90);

            If (nPK > 0) then
                qryMasternCdTerceiro.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmAvaliacaoCQM.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(88);

            If (nPK > 0) then
                qryMasternCdTipoReceb.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmAvaliacaoCQM.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bInclusao) then
  begin
  
      qryGeraPerguntas.Parameters.ParamByName('nCdQuestionarioCQM').Value := qryTipoRecebnCdQuestionarioCQM.Value ;
      qryGeraPerguntas.Parameters.ParamByName('nCdAvaliacaoCQM').Value    := qryMasternCdAvaliacaoCQM.Value ;
      qryGeraPerguntas.ExecSQL;

      PosicionaQuery(qryRespostas, qryMasternCdAvaliacaoCQM.AsString) ;

  end ;

end;

procedure TfrmAvaliacaoCQM.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryTipoReceb, qryMasternCdTipoReceb.AsString) ;
  PosicionaQuery(qryTipoResultadoCQM, qryMasternCdTipoResultadoCQM.AsString) ;
  PosicionaQuery(qryRespostas, qryMasternCdAvaliacaoCQM.AsString) ;

  btFinalizar.Enabled := True ;
  DBGridEh1.ReadOnly  := false ;

  if (qryMasterdDtFech.AsString <> '') then
  begin
      btFinalizar.Enabled := False ;
      DBGridEh1.ReadOnly  := True ;
  end ;

end;

procedure TfrmAvaliacaoCQM.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmCentralArquivos;
begin
  inherited;
  objForm := TfrmCentralArquivos.Create(nil);
  if not qryMaster.Active then
      exit ;

  if (qryMasternCdAvaliacaoCQM.Value > 0) then
      objForm.GerenciaArquivos('AVALIACAOCQM',qryMasternCdAvaliacaoCQM.Value) ;
  FreeAndNil(objForm);
  
end;

procedure TfrmAvaliacaoCQM.qryRespostasCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryRespostascNmTabTipoResposta.Value = '') and (qryRespostasnCdTabTipoResposta.Value > 0) then
  begin
      qryTabTipoResposta.Close ;
      PosicionaQuery(qryTabTipoResposta, qryRespostasnCdTabTipoResposta.AsString) ;

      if not qryTabTipoResposta.Eof then
          qryRespostascNmTabTipoResposta.Value := qryTabTipoRespostacNmTabTipoResposta.Value ;
  end ;
  
end;

procedure TfrmAvaliacaoCQM.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe o n�mero do documento.') ;
      DBedit2.SetFocus;
      abort ;
  end ;

  if (DBEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro fornecedor.') ;
      DBedit3.SetFocus;
      abort ;
  end ;

  if (DBEdit10.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de recebimento.') ;
      DBedit4.SetFocus;
      abort ;
  end ;

  if (trim(DBEdit5.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data da entrega.') ;
      DBedit5.SetFocus;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
  
      qryMasterdDtCad.Value     := Now();
      qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado ;
      qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;

  end ;

end;

procedure TfrmAvaliacaoCQM.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmAvaliacaoCQM.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  
  if (qryMaster.State <> dsInactive) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  DBGridEh1.Col := 6 ;
  
end;

procedure TfrmAvaliacaoCQM.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryTerceiro.Close ;
  qryTipoReceb.Close ;
  qryTipoResultadoCQM.close ;

  qryRespostas.Close ;
  
end;

procedure TfrmAvaliacaoCQM.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  DBGridEh1.SetFocus ;
end;

procedure TfrmAvaliacaoCQM.DBEdit12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(184);

            If (nPK > 0) then
                qryMasternCdTipoResultadoCQM.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmAvaliacaoCQM.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  qryTipoResultadoCQM.Close ;
  PosicionaQuery(qryTipoResultadoCQM, qryMasternCdTipoResultadoCQM.asString) ;
  
end;

procedure TfrmAvaliacaoCQM.btFinalizarClick(Sender: TObject);
begin
  inherited;

  if (not qryMaster.Active) then
  begin
      MensagemErro('Nenhuma avalia��o ativa.') ;
      abort ;
  end ;

  if (frmMenu.nCdUsuarioLogado <> qryMasternCdUsuario.Value) then
  begin
      MensagemAlerta('Somente o usu�rio que digitou pode finalizar a avalia��o.') ;
      abort ;
  end ;

  if (DBEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o nome do respons�vel pela avalia��o.') ;
      DBEdit15.SetFocus;
      abort ;
  end ;

  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Informe a data da avalia��o.') ;
      DBEdit6.SetFocus;
      abort ;
  end ;

  qryRespostas.DisableControls;

  qryRespostas.First ;

  While not qryRespostas.Eof do
  begin
      if (qryRespostascResposta.Value = '') then
      begin
          qryRespostas.EnableControls;
          MensagemAlerta('Para finalizar � necess�rio responder todas as perguntas.' +#13#13 + 'Pergunta sem resposta: ' + qryRespostascPergunta.Value) ;
          cxPageControl1.ActivePageIndex := 0 ;
          DBGridEh1.SetFocus;
          abort ;
      end ;

      qryRespostas.Edit ;
      qryRespostascResposta.Value := Uppercase(qryRespostascResposta.Value) ;
      qryRespostas.Post ;

      {-- valida a resposta --}
      if (qryRespostasnCdTabTipoResposta.Value = 1) then {--Sim/Nao--}
      begin

          if (qryRespostascResposta.Value = 'N�O') THEN
              qryRespostascResposta.Value := 'NAO' ;

          if (qryRespostascResposta.Value <> 'SIM') and (qryRespostascResposta.Value <> 'NAO') then
          begin
              qryRespostas.EnableControls;
              MensagemErro('Resposta inv�lida.' +#13#13 + 'Pergunta : ' + qryRespostascPergunta.Value + ' Respostas Aceitas: SIM ou NAO ') ;
              abort ;
          end ;
      end ;

      {-- valida a resposta --}
      if (qryRespostasnCdTabTipoResposta.Value = 2) then {--nota de 0 a 10--}
      begin
          try
              StrToint(qryRespostascResposta.Value) ;
          except
              qryRespostas.EnableControls;
              MensagemErro('Resposta inv�lida.' +#13#13 + 'Pergunta : ' + qryRespostascPergunta.Value + ' Respostas Aceitas: uma nota de 0 a 10') ;
              abort ;
          end ;

          if not (qryRespostascResposta.AsInteger in [0,1,2,3,4,5,6,7,8,9,10]) then
          begin
              qryRespostas.EnableControls;
              MensagemErro('Resposta inv�lida.' +#13#13 + 'Pergunta : ' + qryRespostascPergunta.Value + ' Respostas Aceitas: uma nota de 0 a 10') ;
              abort ;
          end ;
      end ;

      {-- valida a resposta --}
      if (qryRespostasnCdTabTipoResposta.Value = 3) then {--nota 0 ou 5 ou 10--}
      begin
          try
              StrToint(qryRespostascResposta.Value) ;
          except
              qryRespostas.EnableControls;
              MensagemErro('Resposta inv�lida.' +#13#13 + 'Pergunta : ' + qryRespostascPergunta.Value + ' Respostas Aceitas: nota 0 ou 5 ou 10') ;
              abort ;
          end ;

          if not (qryRespostascResposta.AsInteger in [0,5,10]) then
          begin
              qryRespostas.EnableControls;
              MensagemErro('Resposta inv�lida.' +#13#13 + 'Pergunta : ' + qryRespostascPergunta.Value + ' Respostas Aceitas: nota 0 ou 5 ou 10') ;
              abort ;
          end ;
      end ;

      qryRespostas.Next ;
  end ;

  qryRespostas.First;
  qryRespostas.EnableControls;

  if (DBEdit13.Text = '') then
  begin
      MensagemAlerta('Informe o resultado final da avalia��o.') ;
      cxPageControl1.ActivePage := cxTabSheet2 ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if (MessageDlg('Confirma a finaliza��o desta avalia��o ?'+#13#13+'Ap�s este processo a avalia��o n�o poder� ser modificada.',mtConfirmation,[mbYes,mbNo],0)=MRNO) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasterdDtFech.Value := Now() ;
      qryMaster.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  if (MessageDlg('Deseja imprimir a avalia��o ?',mtConfirmation,[mbYes,mbNo],0)=MRYES) then
      ToolButton12.Click ;

  btCancelar.Click;

end;

procedure TfrmAvaliacaoCQM.btCancelarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmAvaliacaoCQM.btSalvarClick(Sender: TObject);
begin
  if (qryMasterdDtFech.AsString <> '') then
  begin
      MensagemErro('Avalia��o j� finalizada n�o pode ser alterada.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmAvaliacaoCQM.btExcluirClick(Sender: TObject);
begin
  if (qryMasterdDtFech.AsString <> '') then
  begin
      MensagemErro('Avalia��o j� finalizada n�o pode ser exclu�da.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmAvaliacaoCQM.DBEdit14KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(87);

            If (nPK > 0) then
            begin
                qryLocalEstoque.Close ;
                PosicionaQuery(qryLocalEstoque, IntToStr(nPK)) ;

                if not qryLocalEstoque.eof then
                    qryMastercNmLocalEntrega.Value := qryLocalEstoquecNmLocalEstoque.Value ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAvaliacaoCQM.ToolButton12Click(Sender: TObject);
var
    objRel : TrptAvaliacaoCQM_View;
begin
  if (not qryMaster.Active) then
  begin
      MensagemErro('Nenhuma avalia��o ativa.') ;
      abort ;
  end ;

  objRel := TrptAvaliacaoCQM_View.Create(nil);

  Try
      Try
          PosicionaQuery(objRel.qryAvaliacao, qryMasternCdAvaliacaoCQM.AsString) ;
          objRel.lblEmpresa.Caption := frmMenu.cNmFantasiaEmpresa ;
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TfrmAvaliacaoCQM) ;

end.
