unit fMonitorEmpenho;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, ER2Lookup, StdCtrls, Mask, DBCtrls, cxButtons, DB,
  ADODB, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, Menus, ER2Excel;

type
  TfrmMonitorEmpenho = class(TfrmProcesso_Padrao)
    qryCentroDistribuicao: TADOQuery;
    dsCentroDistribuicao: TDataSource;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    DBGridEh1: TDBGridEh;
    edtDtFinal: TMaskEdit;
    edtDtInicial: TMaskEdit;
    Label1: TLabel;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    qryResultadocNmLoja: TStringField;
    qryResultadonCdTipoFrequenciaDistribuicao: TIntegerField;
    qryResultadocNmTipoFrequenciaDistribuicao: TStringField;
    qryResultadodDtUltEntrega: TStringField;
    qryResultadodDtProxEntrega: TStringField;
    DtpcHoraIni: TDateTimePicker;
    DtpcHoraFim: TDateTimePicker;
    Panel1: TPanel;
    DBGridEh2: TDBGridEh;
    qryOrdemSeparacao: TADOQuery;
    dsOrdemSeparacao: TDataSource;
    qryOrdemSeparacaonCdOrdemSeparacao: TIntegerField;
    qryOrdemSeparacaonCdEmpresa: TIntegerField;
    qryOrdemSeparacaonCdLoja: TIntegerField;
    qryOrdemSeparacaonCdCentroDistribuicao: TIntegerField;
    qryOrdemSeparacaodDtGeracao: TDateTimeField;
    qryOrdemSeparacaonCdUsuarioGeracao: TIntegerField;
    qryOrdemSeparacaodDtBaixa: TDateTimeField;
    qryOrdemSeparacaonCdUsuarioBaixa: TIntegerField;
    qryOrdemSeparacaocFlgConferido: TIntegerField;
    qryOrdemSeparacaocNmUsuarioGeracao: TStringField;
    qryResultadonCdEmpresa: TIntegerField;
    qryResultadonCdLoja: TIntegerField;
    qryResultadocLoja: TStringField;
    Panel2: TPanel;
    imgConferido: TImage;
    Label23: TLabel;
    Panel3: TPanel;
    btImpOrdem: TcxButton;
    btVisualizarOrdem: TcxButton;
    btExcluirOrdem: TcxButton;
    btValidarOrdem: TcxButton;
    btProcessarOrdem: TcxButton;
    cmdTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryResultadonQtdeEmSeparacao: TBCDField;
    qryExcluiLista: TADOQuery;
    qryVerificaItemDivergencia: TADOQuery;
    qryProdutosBipados: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryAux: TADOQuery;
    cmdTempOrdemSeparacao: TADOCommand;
    imgPendente: TImage;
    Label3: TLabel;
    SP_GERA_TRANSFEST_DISTRIBUICAO: TADOStoredProc;
    imgDivergencia: TImage;
    Label6: TLabel;
    qryOrdemSeparacaodDtEntrega: TDateTimeField;
    qryOrdemSeparacaocNmLoja: TStringField;
    btExportarOrdem: TcxButton;
    ER2Excel1: TER2Excel;
    qryPosicaoEstoque: TADOQuery;
    qryCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryCentroDistribuicaocNmCentroDistribuicao: TStringField;
    qryCentroDistribuicaonCdLocalEstEmpenhoPadrao: TIntegerField;
    qryPosicaoEstoquenQtdeFisico: TBCDField;
    qryPosicaoEstoquenQtdeEmpenho: TBCDField;
    qryResultadonQtdeTotalItensEmpenho: TBCDField;
    popEmpenho: TPopupMenu;
    btVisualizarEmpenhoPendCanc: TMenuItem;
    qryProdutosBipadoscCdProduto: TStringField;
    qryProdutosBipadosnQtdeBipada: TIntegerField;
    qryPopulaConfPadrao: TADOQuery;
    qryPopulaConfPadraonCdProduto: TIntegerField;
    qryPopulaConfPadraocNmProduto: TStringField;
    qryPopulaConfPadraocEAN: TStringField;
    qryPopulaConfPadraocEANFornec: TStringField;
    qryGravaConfPadrao: TADOQuery;
    procedure ER2LookupMaskEdit1Change(Sender: TObject);
    procedure ER2LookupMaskEdit2Change(Sender: TObject);
    procedure edtDtInicialChange(Sender: TObject);
    procedure edtDtFinalChange(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1CellClick(Column: TColumnEh);
    procedure btImpOrdemClick(Sender: TObject);
    procedure btExcluirOrdemClick(Sender: TObject);
    procedure btVisualizarOrdemClick(Sender: TObject);
    procedure btValidarOrdemClick(Sender: TObject);
    procedure btProcessarOrdemClick(Sender: TObject);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btExportarOrdemClick(Sender: TObject);
    procedure btVisualizarEmpenhoPendCancClick(Sender: TObject);
    procedure popEmpenhoPopup(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CarregaMonitor();
    procedure VerificaItemSelecionado();
    procedure ExibeItens(nCdOrdemSeparacao : integer ; cModo : string);
  end;

var
  frmMonitorEmpenho: TfrmMonitorEmpenho;

implementation

uses fMenu,fMonitorEmpenho_Itens,rItensSeparacaoDistribuicao,fConferenciaProdutoPadrao;

{$R *.dfm}

procedure TfrmMonitorEmpenho.VerificaItemSelecionado();
begin

  if (DBGridEh2.SelectedRows.Count = 0) then
  begin
      MensagemAlerta('Nenhuma ordem de separa��o selecionada.');
      Abort;
  end;

end;

procedure TfrmMonitorEmpenho.CarregaMonitor();
begin
  if DBEdit1.Text = '' then
  begin
      MensagemAlerta('Informe o centro de distribui��o.');
      ER2LookupMaskEdit1.SetFocus;
      Abort;
  end;

  qryOrdemSeparacao.Close;

  cmdTemp.Execute;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value            := frmMenu.nCdEmpresaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdLoja').Value               := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);

  if edtDtInicial.Text = '  /  /    ' then
  begin
      qryPopulaTemp.Parameters.ParamByName('cDtIni').Value   := '01/01/1900';
      qryPopulaTemp.Parameters.ParamByName('cHoraIni').Value := '00:00:00';
  end
  else
  begin
      qryPopulaTemp.Parameters.ParamByName('cDtIni').Value   := edtDtInicial.Text;
      qryPopulaTemp.Parameters.ParamByName('cHoraIni').Value := TimeToStr(DtpcHoraIni.Time);
  end;

  if (edtDtFinal.Text = '  /  /    ') then
  begin
      qryPopulaTemp.Parameters.ParamByName('cDtFim').Value   := '01/01/1900';
      qryPopulaTemp.Parameters.ParamByName('cHoraFim').Value := '00:00:00';
  end
  else
  begin
      qryPopulaTemp.Parameters.ParamByName('cDtFim').Value   := edtDtFinal.Text;
      qryPopulaTemp.Parameters.ParamByName('cHoraFim').Value := TimeToStr(DtpcHoraFim.Time);
  end;

  qryPopulaTemp.ExecSQL;

  qryResultado.Close;
  qryResultado.Open;

  if qryResultado.IsEmpty then
  begin
     MensagemAlerta('Nenhum empenho encontrado.');
     Abort;
  end;
end;

procedure TfrmMonitorEmpenho.ER2LookupMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ER2LookupMaskEdit1.Text <> '' then
     qryResultado.Close;
end;

procedure TfrmMonitorEmpenho.ER2LookupMaskEdit2Change(Sender: TObject);
begin
  inherited;
  if ER2LookupMaskEdit2.Text <> '' then
     qryResultado.Close;
end;

procedure TfrmMonitorEmpenho.edtDtInicialChange(Sender: TObject);
begin
  inherited;
  if edtDtInicial.Text <> '  /  /    ' then
     qryResultado.Close;
end;

procedure TfrmMonitorEmpenho.edtDtFinalChange(Sender: TObject);
begin
  inherited;
  if edtDtFinal.Text <> '  /  /    ' then
     qryResultado.Close;
end;

procedure TfrmMonitorEmpenho.ExibeItens(nCdOrdemSeparacao : integer ; cModo : string);
var
  ObjForm : TfrmMonitorEmpenho_Itens;
begin

  try
      ObjForm := TfrmMonitorEmpenho_Itens.Create(nil);

      {-- Prepara Temp --}
      ObjForm.cmdPreparaTemp.Execute;
      
      {-- Popula Temp --}
      ObjForm.qryPopulaTemp.Close;
      ObjForm.qryPopulaTemp.Parameters.ParamByName('nCdOrdemSeparacao').Value     := nCdOrdemSeparacao;
      ObjForm.qryPopulaTemp.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('nCdLoja').Value               := frmMenu.ConvInteiro(qryResultadonCdLoja.AsString);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('cDtIni').Value                := frmMenu.ConvData(edtDtInicial.Text);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('cDtFim').Value                := frmMenu.ConvData(edtDtFinal.Text);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('cHoraIni').Value              := TimeToStr(DtpcHoraIni.Time);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('cHoraFim').Value              := TimeToStr(DtpcHoraFim.Time);
      ObjForm.qryPopulaTemp.Parameters.ParamByName('cFlgPendCanc').Value          := 0;

      if (cModo = 'C') then
          ObjForm.qryPopulaTemp.Parameters.ParamByName('cFlgPendCanc').Value := 1;

      ObjForm.qryPopulaTemp.ExecSQL;

      {-- Abre a query dos itens empenhados--}
      ObjForm.qryItensEmpenho.Close;
      ObjForm.qryItensEmpenho.Open;

      {-- Verifica se existem itens pendentes --}
      if (cModo = 'C') then
      begin
          if (ObjForm.qryItensEmpenho.RecordCount = 0) then
          begin
              FreeAndNil(ObjForm);
              ShowMessage('N�o existem empenhos pendentes para efetiva��o de cancelamento.');
              Exit;
          end;
      end;

      {-- Paramentros do cabe�alho --}
      ObjForm.ER2LookupMaskEdit2.Text := qryResultadonCdLoja.AsString;
      ObjForm.PosicionaQuery(ObjForm.qryLoja,ObjForm.ER2LookupMaskEdit2.Text);
      ObjForm.edtDtUlt.Text           := qryResultadodDtUltEntrega.Value;

      if nCdOrdemSeparacao > 0 then
      begin
         ObjForm.edtDtProx.Text          := DateToStr(qryOrdemSeparacaodDtEntrega.Value);
         ObjForm.DBEdit2.Text            := IntToStr(nCdOrdemSeparacao);
      end
      else
         ObjForm.edtDtProx.Text          := qryResultadodDtProxEntrega.Value;

      ObjForm.nCdCentroDistribuicao   := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);

      ObjForm.ModoExibicao(cModo);
      
      showForm(ObjForm,true);

      CarregaMonitor;

      qryOrdemSeparacao.Close;
      qryOrdemSeparacao.Parameters.ParamByName('nCdEmpresa').Value            := qryResultadonCdEmpresa.Value;
      qryOrdemSeparacao.Parameters.ParamByName('nCdLoja').Value               := qryResultadonCdLoja.Value;
      qryOrdemSeparacao.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
      qryOrdemSeparacao.Open;
  Except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end;
end;

procedure TfrmMonitorEmpenho.DBGridEh1DblClick(Sender: TObject);
begin
  if qryResultado.IsEmpty then
      exit;

  if not (qryResultadonQtdeTotalItensEmpenho.Value > 0) then
     Abort;

  ExibeItens(0,'I');
end;

procedure TfrmMonitorEmpenho.ToolButton1Click(Sender: TObject);
begin
  inherited;

  CarregaMonitor;
end;

procedure TfrmMonitorEmpenho.DBGridEh1CellClick(Column: TColumnEh);
begin
  inherited;

  if (qryResultado.IsEmpty) then
      Exit;

  qryOrdemSeparacao.Close;
  qryOrdemSeparacao.Parameters.ParamByName('nCdEmpresa').Value            := qryResultadonCdEmpresa.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdLoja').Value               := qryResultadonCdLoja.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
  qryOrdemSeparacao.Open;
end;

procedure TfrmMonitorEmpenho.btImpOrdemClick(Sender: TObject);
var
  ObjRel : TrelItensSeparacaoDistribuicao_view;
begin

  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  try

      ObjRel := TrelItensSeparacaoDistribuicao_view.Create(nil);

      ObjRel.qryResultado.Close;
      ObjRel.qryResultado.Parameters.ParamByName('nCdOrdemSeparacao').Value := qryOrdemSeparacaonCdOrdemSeparacao.Value;
      ObjRel.qryResultado.Open;

      ObjRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      ObjRel.QuickRep1.PreviewModal;

      FreeAndNil(ObjRel);

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end;
end;

procedure TfrmMonitorEmpenho.btExcluirOrdemClick(Sender: TObject);
begin

  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  case MessageDlg('Confirma exclus�o da ordem de separa��o No ' + qryOrdemSeparacaonCdOrdemSeparacao.AsString + ' ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      qryExcluiLista.Close;
      qryExcluiLista.Parameters.ParamByName('nPK').Value := qryOrdemSeparacaonCdOrdemSeparacao.Value;
      qryExcluiLista.ExecSQL;

      if qryExcluiLista.RowsAffected > 0 then
      begin
          ShowMessage('Ordem de separa��o exclu�da com sucesso.');
      end;

      frmMenu.Connection.CommitTrans;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;

  CarregaMonitor;

  if (qryResultado.IsEmpty) then
      Exit;

  qryOrdemSeparacao.Close;
  qryOrdemSeparacao.Parameters.ParamByName('nCdEmpresa').Value            := qryResultadonCdEmpresa.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdLoja').Value               := qryResultadonCdLoja.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
  qryOrdemSeparacao.Open;
end;

procedure TfrmMonitorEmpenho.btVisualizarOrdemClick(Sender: TObject);
begin
  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  ExibeItens(qryOrdemSeparacaonCdOrdemSeparacao.Value,'V');
end;

procedure TfrmMonitorEmpenho.btValidarOrdemClick(Sender: TObject);
var
  objConfProdPadrao : TfrmConferenciaProdutoPadrao;
begin
  inherited;

  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  PosicionaQuery(qryVerificaItemDivergencia,qryOrdemSeparacaonCdOrdemSeparacao.AsString);

  if (qryVerificaItemDivergencia.IsEmpty) then
  begin
      case MessageDlg('Os itens j� foram conferidos. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo: exit;
      end ;
  end;

  try
      try
          objConfProdPadrao := TfrmConferenciaProdutoPadrao.Create(nil);
          frmMenu.mensagemUsuario('Buscando produtos para conf...');

          { -- popula dataset dos produtos -- }
          PosicionaQuery(qryPopulaConfPadrao, qryOrdemSeparacaonCdOrdemSeparacao.AsString);

          if (qryPopulaConfPadrao.IsEmpty) then
          begin
              MensagemAlerta('N�o existe produtos para confer�ncia.');
              Exit;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.DisableControls;

          while (not qryPopulaConfPadrao.Eof) do
          begin
              objConfProdPadrao.cdsProdutosConfere.Insert;
              objConfProdPadrao.cdsProdutosConferecCdProduto.Value := qryPopulaConfPadraonCdProduto.AsString;
              objConfProdPadrao.cdsProdutosConferecNmProduto.Value := qryPopulaConfPadraocNmProduto.Value;
              objConfProdPadrao.cdsProdutosConferecEAN.Value       := Trim(qryPopulaConfPadraocEAN.Value);
              objConfProdPadrao.cdsProdutosConferecEANFornec.Value := Trim(qryPopulaConfPadraocEANFornec.Value);
              objConfProdPadrao.cdsProdutosConfere.Post;

              qryPopulaConfPadrao.Next;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.EnableControls;

          if (objConfProdPadrao.cdsProdutosConfere.IsEmpty) then
          begin
              MensagemErro('Falha ao popular produtos para confer�ncia.');
              Exit;
          end;

          { -- configura tela de confer�ncia padr�o -- }
          objConfProdPadrao.Caption               := 'Confer�ncia de Produtos';
          objConfProdPadrao.rbConferencia.Checked := True;

          showForm(objConfProdPadrao,False);

          if (objConfProdPadrao.bProcessa) then
              case MessageDlg('Confirma a grava��o da confer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:Exit;
              end
          else
              Exit;
          try
              frmMenu.Connection.BeginTrans;

              { -- processa confer�ncia dos itens -- }
              qryGravaConfPadrao.Close;
              qryGravaConfPadrao.Parameters.ParamByName('nCdOrdemSeparacao').Value := qryOrdemSeparacaonCdOrdemSeparacao.Value;
              qryGravaConfPadrao.Parameters.ParamByName('nCdUsuarioConf').Value    := frmMenu.nCdUsuarioLogado;
              qryGravaConfPadrao.Parameters.ParamByName('dDtConfIni').Value        := DateTimeToStr(objConfProdPadrao.dDtInicioConf);
              qryGravaConfPadrao.Parameters.ParamByName('dDtConfFim').Value        := DateTimeToStr(objConfProdPadrao.dDtFimConf);
              qryGravaConfPadrao.ExecSQL;

              frmMenu.Connection.CommitTrans;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.');
              Raise;
          end;
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;

      PosicionaQuery(qryVerificaItemDivergencia, qryOrdemSeparacaonCdOrdemSeparacao.AsString);

      if (not qryVerificaItemDivergencia.IsEmpty) then
      begin
          MensagemAlerta('Existem itens com saldo divergente.');
          ExibeItens(qryOrdemSeparacaonCdOrdemSeparacao.Value,'V');
          Exit;
      end
      else
          ShowMessage('Ordem de separa��o conferida com sucesso.');
  finally
      FreeAndNil(objConfProdPadrao);
      qryOrdemSeparacao.Requery();
  end;
end;

procedure TfrmMonitorEmpenho.btProcessarOrdemClick(Sender: TObject);
var
  i            : integer;
  nCdTransfEst : integer;
begin

  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  cmdTempOrdemSeparacao.Execute;

  for i := 0 to DBGridEh2.SelectedRows.Count - 1 do
  begin
      qryOrdemSeparacao.GotoBookmark(Pointer(DBGridEh2.SelectedRows.Items[i]));

      if qryOrdemSeparacaocFlgConferido.Value = 1 then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('INSERT INTO #TempOrdemSeparacao VALUES (' + IntToStr(qryOrdemSeparacaonCdOrdemSeparacao.Value)  + ')');
          qryAux.ExecSQL;
      end
      else
      begin
          MensagemAlerta('Ordem de Separa��o No ' + qryOrdemSeparacaonCdOrdemSeparacao.AsString + ' n�o conferida.');
          Abort;
      end;
  end;

  case MessageDlg('Confirma a gera��o da transfer�ncia?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: exit;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      {-- Gera a transfer�ncia --}
      SP_GERA_TRANSFEST_DISTRIBUICAO.Close;
      SP_GERA_TRANSFEST_DISTRIBUICAO.Parameters.ParamByName('@nCdCentroDistribuicao').Value := qryOrdemSeparacaonCdCentroDistribuicao.Value;
      SP_GERA_TRANSFEST_DISTRIBUICAO.Parameters.ParamByName('@nCdLoja').Value               := qryOrdemSeparacaonCdLoja.Value;
      SP_GERA_TRANSFEST_DISTRIBUICAO.Parameters.ParamByName('@nCdUsuarioCad').Value         := frmMenu.nCdUsuarioLogado;
      SP_GERA_TRANSFEST_DISTRIBUICAO.Parameters.ParamByName('@nCdTransfEst').Value          := 0;
      SP_GERA_TRANSFEST_DISTRIBUICAO.ExecProc;

      nCdTransfEst := SP_GERA_TRANSFEST_DISTRIBUICAO.Parameters.ParamByName('@nCdTransfEst').Value;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Transfer�ncia gerada com sucesso.' + #13#13 + 'C�digo: ' + IntToStr(nCdTransfEst) + '.');
  except
      MensagemErro('Erro no processamento.') ;
      frmMenu.Connection.RollbackTrans;
      raise ;
  end;

  CarregaMonitor;

  if (qryResultado.IsEmpty) then
      Exit;

  qryOrdemSeparacao.Close;
  qryOrdemSeparacao.Parameters.ParamByName('nCdEmpresa').Value            := qryResultadonCdEmpresa.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdLoja').Value               := qryResultadonCdLoja.Value;
  qryOrdemSeparacao.Parameters.ParamByName('nCdCentroDistribuicao').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
  qryOrdemSeparacao.Open;
end;

procedure TfrmMonitorEmpenho.DBGridEh2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin

   if qryOrdemSeparacao.IsEmpty then
       Exit;

   if DataCol = 0 then
   begin
        DBGridEh2.Canvas.FillRect(Rect);
        if qryOrdemSeparacaocFlgConferido.Value = 1 then
            DBGridEh2.Canvas.Draw(Rect.Left, Rect.Top ,imgConferido.Picture.Bitmap)
        else
        begin
           if qryOrdemSeparacaocFlgConferido.Value = 0 then
             DBGridEh2.Canvas.Draw(Rect.Left, Rect.Top ,imgPendente.Picture.Bitmap)
           else
             DBGridEh2.Canvas.Draw(Rect.Left, Rect.Top ,imgDivergencia.Picture.Bitmap);
        end;

   end;

end;

procedure TfrmMonitorEmpenho.btExportarOrdemClick(Sender: TObject);
var
  iLinha : Integer;
  ObjRel : TrelItensSeparacaoDistribuicao_view;
begin
  inherited;

  if qryOrdemSeparacao.IsEmpty then
      Abort;

  VerificaItemSelecionado;

  try
      try
          ObjRel := TrelItensSeparacaoDistribuicao_view.Create(nil);
      
          ObjRel.qryResultado.Close;
          ObjRel.qryResultado.Parameters.ParamByName('nCdOrdemSeparacao').Value := qryOrdemSeparacaonCdOrdemSeparacao.Value;
          ObjRel.qryResultado.Open;

          {-- Formata��o --}
          ER2Excel1.Titulo := 'Ordem de Separa��o';

          ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
          ER2Excel1.Celula['A1'].Negrito;
          ER2Excel1.Celula['A1'].Range('S6');
          ER2Excel1.Celula['A1'].Congelar('A7');
          ER2Excel1.Celula['P5'].Mesclar('S5');

          ObjRel.qryResultado.First;

          ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
          ER2Excel1.Celula['A2'].Text := 'Ordem Separa��o - Distribui��o';
          ER2Excel1.Celula['A4'].Text := 'Loja Destino: ' + IntToStr(ObjRel.qryResultadonCdLoja.Value) + ' - ' + ObjRel.qryResultadocNmLoja.Value;
          ER2Excel1.Celula['D4'].Text := 'Ordem Separa��o: ' + IntToStr(ObjRel.qryResultadonCdOrdemSeparacao.Value);
          ER2Excel1.Celula['G4'].Text := 'Dt: ' + DateTimeToStr(qryOrdemSeparacaodDtGeracao.Value);
          ER2Excel1.Celula['J4'].Text := 'Usu�rio: ' + qryOrdemSeparacaocNmUsuarioGeracao.Value;
          ER2Excel1.Celula['P5'].Text := '****** Posi��o Estoque ******';
          ER2Excel1.Celula['P5'].HorizontalAlign := haCenter;

          ER2Excel1.Celula['A6'].Text := 'C�d. Emp.';
          ER2Excel1.Celula['C6'].Text := 'C�digo';
          ER2Excel1.Celula['D6'].Text := 'Categoria';
          ER2Excel1.Celula['E6'].Text := 'Descri��o';
          ER2Excel1.Celula['M6'].Text := 'Qtde';
          ER2Excel1.Celula['N6'].Text := 'Conf.';
          ER2Excel1.Celula['O6'].Text := 'Diverg.';
          ER2Excel1.Celula['P6'].Text := 'F�sico.';
          ER2Excel1.Celula['Q6'].Text := 'Empenh.';
          ER2Excel1.Celula['R6'].Text := 'Dispon.';
          ER2Excel1.Celula['S6'].Text := 'Dt. Empenho';

          iLinha := 7;
      
          while (not ObjRel.qryResultado.Eof) do
          begin
              { -- busca posi��o de estoque -- }
              qryPosicaoEstoque.Close;
              qryPosicaoEstoque.Parameters.ParamByName('nCdProduto').Value      := ObjRel.qryResultadonCdProduto.Value;
              qryPosicaoEstoque.Parameters.ParamByName('nCdLocalEstoque').Value := qryCentroDistribuicaonCdLocalEstEmpenhoPadrao.Value;
              qryPosicaoEstoque.Parameters.ParamByName('nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva;
              qryPosicaoEstoque.Open;

              ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := ObjRel.qryResultadonCdEstoqueEmpenhado.Value;
              ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := ObjRel.qryResultadonCdProduto.Value;
              ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := ObjRel.qryResultadocNmCategoria.Value;
              ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := ObjRel.qryResultadocNmProduto.Value;

              ER2Excel1.Celula['M' + IntToStr(iLinha)].Text := ObjRel.qryResultadonQtdeEstoqueEmpenhado.AsInteger;
              ER2Excel1.Celula['N' + IntToStr(iLinha)].Text := StrToIntDef(ObjRel.qryResultadocQtdeConf.Value,0);
              ER2Excel1.Celula['O' + IntToStr(iLinha)].Text := StrToIntDef(ObjRel.qryResultadocQtdeDiv.Value,0);

              if (not qryPosicaoEstoque.IsEmpty) then
              begin
                  ER2Excel1.Celula['P' + IntToStr(iLinha)].Text := qryPosicaoEstoquenQtdeFisico.AsInteger;
                  ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text := qryPosicaoEstoquenQtdeEmpenho.AsInteger;
                  ER2Excel1.Celula['R' + IntToStr(iLinha)].Text := qryPosicaoEstoquenQtdeFisico.AsInteger - qryPosicaoEstoquenQtdeEmpenho.AsInteger;
              end;

              ER2Excel1.Celula['S' + IntToStr(iLinha)].Text := ObjRel.qryResultadodDtEmpenho.Value;

              ObjRel.qryResultado.Next;

              Inc(iLinha);
          end;
          { -- exporta planilha e limpa result do componente -- }
          ER2Excel1.ExportXLS;
          ER2Excel1.CleanupInstance;
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(ObjRel);
  end;
end;

procedure TfrmMonitorEmpenho.btVisualizarEmpenhoPendCancClick(
  Sender: TObject);
begin
  inherited;

  if (qryResultado.IsEmpty) then
      Exit;

  if not (qryResultadonQtdeTotaLItensEmpenho.Value > 0) then
      Exit;

  ExibeItens(0,'C');
end;

procedure TfrmMonitorEmpenho.popEmpenhoPopup(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('CONFCANCEMP') <> 'S') then
      Abort;
end;

procedure TfrmMonitorEmpenho.DBGridEh2DblClick(Sender: TObject);
begin
  inherited;

  btVisualizarOrdem.Click;    
end;

initialization
    RegisterClass(TfrmMonitorEmpenho) ;
end.
