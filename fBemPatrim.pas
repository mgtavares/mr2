unit fBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, cxPC, cxControls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  DBGridEhGrouping;

type
  TfrmBemPatrim = class(TfrmCadastro_Padrao)
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    qryGrupoBemPatrim: TADOQuery;
    dsGrupoBemPatrim: TDataSource;
    qryGrupoBemPatrimnCdGrupoBemPatrim: TIntegerField;
    qryGrupoBemPatrimcNmGrupoBemPatrim: TStringField;
    qrySubGrupoBemPatrim: TADOQuery;
    dsSubGrupoBemPatrim: TDataSource;
    qrySubGrupoBemPatrimnCdSubGrupoBemPatrim: TAutoIncField;
    qrySubGrupoBemPatrimnCdGrupoBemPatrim: TIntegerField;
    qrySubGrupoBemPatrimcNmSubGrupoBemPatrim: TStringField;
    qryClasseBemPatrim: TADOQuery;
    dsClasseBemPatrim: TDataSource;
    qryClasseBemPatrimnCdClasseBemPatrim: TIntegerField;
    qryClasseBemPatrimcNmClasseBemPatrim: TStringField;
    DataSource1: TDataSource;
    qrySetorBemPatrim: TADOQuery;
    qrySetorBemPatrimnCdSetorBemPatrim: TIntegerField;
    qrySetorBemPatrimcNmSetorBemPatrim: TStringField;
    DataSource2: TDataSource;
    dsSetorBemPatrim: TDataSource;
    qryLocalBemPatrim: TADOQuery;
    dsLocalBemPatrim: TDataSource;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryCentroCustonCdServidorOrigem: TIntegerField;
    qryCentroCustodDtReplicacao: TDateTimeField;
    dsCentroCusto: TDataSource;
    qryLocalBemPatrimnCdLocalBemPatrim: TAutoIncField;
    qryLocalBemPatrimcNmLocalBemPatrim: TStringField;
    qryLocalBemPatrimnCdSetorBemPatrim: TIntegerField;
    qryProduto: TADOQuery;
    dsProduto: TDataSource;
    qryProdutonCdProduto: TIntegerField;
    qryProdutonCdDepartamento: TIntegerField;
    qryProdutonCdCategoria: TIntegerField;
    qryProdutonCdSubCategoria: TIntegerField;
    qryProdutonCdSegmento: TIntegerField;
    qryProdutonCdMarca: TIntegerField;
    qryProdutonCdLinha: TIntegerField;
    qryProdutocReferencia: TStringField;
    qryProdutonCdTabTipoProduto: TIntegerField;
    qryProdutonCdProdutoPai: TIntegerField;
    qryProdutonCdGrade: TIntegerField;
    qryProdutonCdColecao: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdClasseProduto: TIntegerField;
    qryProdutonCdStatus: TIntegerField;
    qryProdutonCdCor: TIntegerField;
    qryProdutonCdMaterial: TIntegerField;
    qryProdutoiTamanho: TIntegerField;
    qryProdutonQtdeEstoque: TBCDField;
    qryProdutonValCusto: TBCDField;
    qryProdutonValVenda: TBCDField;
    qryProdutocEAN: TStringField;
    qryProdutocEANFornec: TStringField;
    qryProdutodDtUltReceb: TDateTimeField;
    qryProdutodDtUltVenda: TDateTimeField;
    qryProdutonCdUsuarioCad: TIntegerField;
    qryProdutodDtCad: TDateTimeField;
    qryProdutonCdUsuarioAtuPreco: TIntegerField;
    qryProdutodDtAtuPreco: TDateTimeField;
    qryProdutonQtdeEstoqueSeg: TBCDField;
    qryProdutonQtdeEstoqueMin: TBCDField;
    qryProdutocFlgExigeInspecao: TIntegerField;
    qryProdutocFlgControlaLote: TIntegerField;
    qryProdutonQtdeEstoqueMax: TBCDField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonCdCC: TIntegerField;
    qryProdutonQtdeEstoqueEmpenhado: TBCDField;
    qryProdutonConsumoMedDia: TBCDField;
    qryProdutodDtUltMRP: TDateTimeField;
    qryProdutoiDiasEstoqueSeg: TIntegerField;
    qryProdutoiDiasEstoqueRessup: TIntegerField;
    qryProdutoiDiasEstoqueMax: TIntegerField;
    qryProdutonCdTipoRessuprimento: TIntegerField;
    qryProdutonCdPlanoConta: TIntegerField;
    qryProdutoiDiaEntrega: TIntegerField;
    qryProdutoiTipoEntrega: TIntegerField;
    qryProdutonCdLocalEstoqueEntrega: TIntegerField;
    qryProdutonCdAlaProducao: TIntegerField;
    qryProdutocFlgProdVenda: TIntegerField;
    qryProdutonPesoBruto: TBCDField;
    qryProdutonPesoLiquido: TBCDField;
    qryProdutonAltura: TBCDField;
    qryProdutonLargura: TBCDField;
    qryProdutonDimensao: TBCDField;
    qryProdutonVolume: TBCDField;
    qryProdutocUMVolume: TStringField;
    qryProdutocDescricaoTecnica: TMemoField;
    qryProdutonEspessura: TBCDField;
    qryProdutonFatorPeso: TBCDField;
    qryProdutonCdGrupo_Produto: TIntegerField;
    qryProdutonCdGrupoInventario: TIntegerField;
    qryProdutocUnidadeMedidaCompra: TStringField;
    qryProdutonFatorCompra: TBCDField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    qryProdutonCdTipoPlanejamento: TIntegerField;
    qryProdutonCdTipoObtencao: TIntegerField;
    qryProdutonLoteMultiplo: TBCDField;
    qryProdutoiLLC: TIntegerField;
    qryProdutocFlgFantasma: TIntegerField;
    qryProdutonCdGrupoMRP: TIntegerField;
    qryProdutoiLeadTimeInsumos: TIntegerField;
    qryProdutonCdServidorOrigem: TIntegerField;
    qryProdutodDtReplicacao: TDateTimeField;
    qryProdutonCdTabTipoModoGestaoProduto: TIntegerField;
    qryProdutocFlgProdEstoque: TIntegerField;
    qryProdutocFlgProdCompra: TIntegerField;
    qryProdutocFlgAtivoFixo: TIntegerField;
    qryProdutonCdTabTipoMetodoUso: TIntegerField;
    DBEdit23: TDBEdit;
    GroupBox1: TGroupBox;
    TipoBemNormal: TRadioButton;
    TipoBemAcoplado: TRadioButton;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocIDExterno: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocNmFantasia: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirodDtCadastro: TDateTimeField;
    qryTerceirocFlgMicroEmpresa: TIntegerField;
    qryTerceirocFlgOptSimples: TIntegerField;
    qryTerceirocFlgContICMS: TIntegerField;
    qryTerceironCdTerceiroRepres: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdStatus: TIntegerField;
    qryTerceirocFlgIsentoCNPJCPF: TIntegerField;
    qryTerceironCdBanco: TIntegerField;
    qryTerceirocAgencia: TStringField;
    qryTerceirocNumConta: TStringField;
    qryTerceirocDigitoConta: TStringField;
    qryTerceironCdFormaPagto: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdMoeda: TIntegerField;
    qryTerceironValCredFinanc: TBCDField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    qryTerceiroiQtdeChequeDev: TIntegerField;
    qryTerceirodDtUltChequeDev: TDateTimeField;
    qryTerceironValMaiorChequeDev: TBCDField;
    qryTerceirocFlgCobrarJuro: TIntegerField;
    qryTerceironValMultaAtraso: TBCDField;
    qryTerceironPercJuroDia: TBCDField;
    qryTerceironCdGrupoEconomico: TIntegerField;
    qryTerceironValLimiteCred: TBCDField;
    qryTerceironValCreditoUtil: TBCDField;
    qryTerceironValPedidoAberto: TBCDField;
    qryTerceironValRecAtraso: TBCDField;
    qryTerceirodMaiorRecAtraso: TDateTimeField;
    qryTerceirocFlgBloqPedVenda: TIntegerField;
    qryTerceirocRG: TStringField;
    qryTerceironCdBanco2: TIntegerField;
    qryTerceirocAgencia2: TStringField;
    qryTerceirocNumConta2: TStringField;
    qryTerceirocDigitoConta2: TStringField;
    qryTerceirocNmContato: TStringField;
    qryTerceirocTelefone1: TStringField;
    qryTerceirocTelefone2: TStringField;
    qryTerceirocTelefoneMovel: TStringField;
    qryTerceirocFax: TStringField;
    qryTerceirocEmail: TStringField;
    qryTerceirocWebSite: TStringField;
    qryTerceirocOBS: TMemoField;
    qryTerceironCdRamoAtividade: TIntegerField;
    qryTerceironCdGrupoComissao: TIntegerField;
    qryTerceirocNmTitularConta1: TStringField;
    qryTerceirocNmTitularConta2: TStringField;
    qryTerceironCdLojaTerceiro: TIntegerField;
    qryTerceirocFlgTipoFat: TIntegerField;
    qryTerceirodDtUltNegocio: TDateTimeField;
    qryTerceironValChequePendComp: TBCDField;
    qryTerceirocFlgHomologado: TIntegerField;
    qryTerceironCdTipoTabPreco: TIntegerField;
    qryTerceirodDtNegativacao: TDateTimeField;
    qryTerceironCdServidorOrigem: TIntegerField;
    qryTerceirodDtReplicacao: TDateTimeField;
    DBEdit24: TDBEdit;
    GroupBox2: TGroupBox;
    TipoAquisicaoNovo: TRadioButton;
    TipoAquisicaoUsado: TRadioButton;
    cxTabSheet4: TcxTabSheet;
    Label17: TLabel;
    DBEdit25: TDBEdit;
    Label18: TLabel;
    DBEdit26: TDBEdit;
    Label19: TLabel;
    DBEdit27: TDBEdit;
    Label20: TLabel;
    DBEdit28: TDBEdit;
    Label21: TLabel;
    DBEdit29: TDBEdit;
    Label22: TLabel;
    DBEdit30: TDBEdit;
    Label23: TLabel;
    DBEdit31: TDBEdit;
    Image5: TImage;
    Image2: TImage;
    Image3: TImage;
    Label24: TLabel;
    qryMasternCdBemPatrim: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdGrupoBemPatrim: TIntegerField;
    qryMasternCdSubGrupoBemPatrim: TIntegerField;
    qryMasternCdClasseBemPatrim: TIntegerField;
    qryMasternCdSetorBemPatrim: TIntegerField;
    qryMasternCdLocalBemPatrim: TIntegerField;
    qryMasternCdMotBaixaBemPatrim: TIntegerField;
    qryMasternCdCentroCustoBemPatrim: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdProduto: TIntegerField;
    qryMasterdDtAquisicao: TDateTimeField;
    qryMasterdDtBaixa: TDateTimeField;
    qryMasternValBemPatrim: TBCDField;
    qryMasternvalBaixa: TBCDField;
    qryMastercNrDocBaixa: TStringField;
    qryMastercCCF: TStringField;
    qryMasternCdUsuario: TIntegerField;
    qryMasternCdUsuarioBaixa: TIntegerField;
    qryMasteriMesVidaUtil: TIntegerField;
    qryMasternTaxaDeprecNormal: TBCDField;
    qryMasternTaxaDeprecAcelerada: TBCDField;
    qryMasternTaxaDeprecEspecial: TBCDField;
    qryMastercNroBemPatrim: TStringField;
    qryMastercNrDocAquisicao: TStringField;
    qryMasternQtde: TBCDField;
    qryMasternCdTabTipoBemPatrim: TIntegerField;
    qryMasternCdTabSituacaoBemPatrim: TIntegerField;
    qryMastercNmBemPatrim: TStringField;
    Label25: TLabel;
    DBEdit32: TDBEdit;
    qryConfigBemPatrim: TADOQuery;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Label1: TLabel;
    qryConfigBemPatrimnCdConfigBemPatrim: TAutoIncField;
    qryConfigBemPatrimnCdConfigSubGrupoBemPatrim: TIntegerField;
    qryConfigBemPatrimnCdBemPatrim: TIntegerField;
    qryConfigBemPatrimcNmConfigSubGrupoBemPatrim: TStringField;
    qryConfigBemPatrimcConteudoConfigBemPatrim: TStringField;
    qryConfigSubGrupoBemPatrim: TADOQuery;
    qryConfigSubGrupoBemPatrimnCdConfigSubGrupoBemPatrim: TAutoIncField;
    qryConfigSubGrupoBemPatrimnCdSubGrupoBemPatrim: TIntegerField;
    qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim: TStringField;
    dsConfigBemPatrim: TDataSource;
    qryMotBaixaBemPatrim: TADOQuery;
    qryMotBaixaBemPatrimnCdMotBaixaBemPatrim: TIntegerField;
    qryMotBaixaBemPatrimcNmMotBaixaBemPatrim: TStringField;
    DBEdit17: TDBEdit;
    dsMotBaixaBemPatrim: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DataSource3: TDataSource;
    DBEdit19: TDBEdit;
    Label27: TLabel;
    DBEdit20: TDBEdit;
    cxTabSheet5: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryConfigBemPatrimAdicional: TADOQuery;
    dsConfigBemPatrimAdicional: TDataSource;
    qryConfigBemPatrimAdicionalnCdConfigBemPatrim: TAutoIncField;
    qryConfigBemPatrimAdicionalnCdConfigSubGrupoBemPatrim: TIntegerField;
    qryConfigBemPatrimAdicionalnCdBemPatrim: TIntegerField;
    qryConfigBemPatrimAdicionalcNmConfigSubGrupoBemPatrim: TStringField;
    qryConfigBemPatrimAdicionalcConteudoConfigBemPatrim: TStringField;
    qryConfigBemPatrimAdicionalcFlgConfigAutomatica: TIntegerField;
    qryConfigBemPatrimcFlgConfigAutomatica: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8Exit(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure cxPageControl1Enter(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20Exit(Sender: TObject);
    procedure qryConfigBemPatrimAdicionalBeforePost(DataSet: TDataSet);
    procedure qryConfigBemPatrimBeforeDelete(DataSet: TDataSet);
    procedure btExcluirClick(Sender: TObject);
    procedure qryConfigBemPatrimBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBemPatrim: TfrmBemPatrim;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}
procedure TfrmBemPatrim.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'BEMPATRIM' ;
  nCdTabelaSistema  := 504 ;
  nCdConsultaPadrao := 1008 ;
  bLimpaAposSalvar  := False;
end;

procedure TfrmBemPatrim.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit32.SetFocus ;
  TipoBemNormal.Checked := True;
  cxPageControl1.ActivePageIndex := 0 ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nPK').Value        := frmMenu.nCdEmpresaAtiva;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryEmpresa.Open;

end;

procedure TfrmBemPatrim.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1009);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryGrupoBemPatrim , DbEdit2.Text) ;

end;

procedure TfrmBemPatrim.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmBemPatrim.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DbEdit2.Text = '') then
        begin
            MensagemAlerta('Informe o Grupo.') ;
            DBEdit2.SetFocus ;
            exit ;
        end;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(1003,'SubGrupoBemPatrim.nCdGrupoBemPatrim = ' + DbEdit2.Text );

            If (nPK > 0) then
            begin
                qryMasternCdSubGrupoBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubGrupoBemPatrim , DBEdit3.Text) ;

end;

procedure TfrmBemPatrim.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1005);

            If (nPK > 0) then
            begin
                qryMasternCdClasseBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryClasseBemPatrim , DbEdit6.Text) ;
end;

procedure TfrmBemPatrim.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1004);

            If (nPK > 0) then
            begin
                qryMasternCdSetorBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySetorBemPatrim , DbEdit4.Text) ;

end;

procedure TfrmBemPatrim.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DbEdit4.Text = '') then
        begin
            MensagemAlerta('Informe o Setor.') ;
            DBEdit4.SetFocus ;
            exit ;
        end;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(1007,'LocalBemPatrim.nCdSetorBemPatrim = ' + DbEdit4.Text );

            If (nPK > 0) then
            begin
                qryMasternCdLocalBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryLocalBemPatrim , DbEdit5.Text) ;

end;

procedure TfrmBemPatrim.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(28);

            If (nPK > 0) then
            begin
                qryMasternCdCentroCustoBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit7Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCentroCusto, DbEdit7.Text) ;

end;

procedure TfrmBemPatrim.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(42);

            If (nPK > 0) then
            begin
                qryMasternCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit8Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryProduto, DbEdit8.Text) ;
end;

procedure TfrmBemPatrim.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  if (bInclusao = true) then
  begin
     PosicionaQuery(qryConfigSubGrupoBemPatrim, qryMasternCdSubGrupoBemPatrim.AsString) ;
     PosicionaQuery(qryConfigBemPatrim, qryMasternCdBemPatrim.AsString) ;
     if (qryConfigBemPatrim.Eof) then
     begin
        while not qryConfigSubGrupoBemPatrim.Eof do
        begin
           qryConfigBemPatrim.Open;
           qryConfigBemPatrim.Insert;
           qryConfigBemPatrimnCdConfigSubGrupoBemPatrim.Value := qryConfigSubGrupoBemPatrimnCdConfigSubGrupoBemPatrim.Value;
           qryConfigBemPatrimnCdBemPatrim.Value               := qryMasternCdBemPatrim.Value;
           qryConfigBemPatrimcNmConfigSubGrupoBemPatrim.Value := qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim.Value;
           qryConfigBemPatrimcFlgConfigAutomatica.Value       := 1;
           qryConfigBemPatrim.Post;
           qryConfigSubGrupoBemPatrim.Next;
        end;
        PosicionaQuery(qryConfigBemPatrim, qryMasternCdBemPatrim.AsString) ;
        PosicionaQuery(qryConfigBemPatrimAdicional, qryMasternCdBemPatrim.AsString) ;
        PosicionaQuery(qryMaster,DBEdit1.Text);
     end;
     DBGridEh1.SetFocus;
     DBGridEh1.Col := 2;
  end;
end;

procedure TfrmBemPatrim.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  if DbEdit19.Text = '' then
  begin
      MensagemAlerta('Informe a Empresa.') ;
      DbEdit20.SetFocus;
      abort;
  end;
  if DbEdit32.Text = '' then
  begin
      MensagemAlerta('Informe a Descri��o.') ;
      DbEdit32.SetFocus;
      abort;
  end;
  if ((DbEdit2.Text = '') or (DbEdit23.Text = '')) then
  begin
      MensagemAlerta('Informe o Grupo.') ;
      DbEdit2.SetFocus;
      abort;
  end;
  if ((DbEdit3.Text = '') or (DbEdit33.Text = '')) then
  begin
      MensagemAlerta('Informe o SubGrupo.') ;
      DbEdit3.SetFocus;
      abort;
  end;
  if ((DbEdit6.Text = '') or (DbEdit34.Text = '')) then
  begin
      MensagemAlerta('Informe a Classe.') ;
      DbEdit6.SetFocus;
      abort;
  end;
  if ((DbEdit4.Text = '') or (DbEdit35.Text = '')) then
  begin
      MensagemAlerta('Informe o Setor.') ;
      DbEdit4.SetFocus;
      abort;
  end;
  if ((DbEdit5.Text = '') or (DbEdit36.Text = '')) then
  begin
      MensagemAlerta('Informe o Local.') ;
      DbEdit5.SetFocus;
      abort;
  end;
  if (DbEdit30.Text = '') then
  begin
      MensagemAlerta('Informe a Quantidade.') ;
      DbEdit30.SetFocus;
      abort;
  end;

  if ((TipoBemNormal.Checked = false) and (TipoBemAcoplado.Checked = false)) then
  begin
      MensagemAlerta('Informe o Tipo de Bem.') ;
      TipoBemNormal.SetFocus;
      abort;
  end;

  if ((qryMasterdDtAquisicao.AsString > '')  and (qryMasterdDtBaixa.Value < qryMasterdDtAquisicao.Value) and (qryMasterdDtBaixa.AsString <> '')) then
  begin
      MensagemAlerta('Data de Baixa menor que Data de Aquisi��o.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DbEdit13.SetFocus;
      abort;
  end;

  if ((qryMasternCdMotBaixaBemPatrim.Value > 0) and (qryMasterdDtBaixa.AsString = '')) then
  begin
      MensagemAlerta('Informe a Data de Baixa.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DbEdit13.SetFocus;
      abort;
  end;

  if ((qryMasternCdMotBaixaBemPatrim.Value = 0) and (qryMasterdDtBaixa.AsString <> '')) then
  begin
      MensagemAlerta('Informe o Motivo de Baixa.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DbEdit15.SetFocus;
      abort;
  end;

  if (qryMasternCdMotBaixaBemPatrim.Value > 0) then qryMasternCdUsuarioBaixa.Value := frmMenu.nCdUsuarioLogado;
  if (((trim(DBEdit15.Text)) = '') and (qryMasternCdUsuarioBaixa.Value > 0)) then qryMasternCdUsuarioBaixa.AsString := '';

  qryMasternCdUsuario.Value              := frmMenu.nCdUsuarioLogado;
  qryMasternCdTabTipoBemPatrim.Value     := 1;
  qryMasternCdTabSituacaoBemPatrim.Value := 1;

  if (TipoBemAcoplado.Checked)    then qryMasternCdTabTipoBemPatrim.Value := 2;
  if (TipoAquisicaoUsado.Checked) then qryMasternCdTabSituacaoBemPatrim.Value := 2;

end;

procedure TfrmBemPatrim.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nPK').Value        := qryMasternCdEmpresa.Value;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryEmpresa.Open;

  if qryEmpresa.Eof then
  begin
      MensagemAlerta('Voce nao tem permissao para ver este Bem.') ;
      btCancelar.Click;
      exit;
  end;

  PosicionaQuery(qryConfigBemPatrim,qryMasternCdBemPatrim.AsString);

  PosicionaQuery(qryConfigBemPatrimAdicional,qryMasternCdBemPatrim.AsString);
  PosicionaQuery(qryGrupoBemPatrim,qryMasternCdGrupoBemPatrim.AsString);
  PosicionaQuery(qrySubGrupoBemPatrim,qryMasternCdSubGrupoBemPatrim.AsString);
  PosicionaQuery(qryClasseBemPatrim,qryMasternCdClasseBemPatrim.AsString);
  PosicionaQuery(qrySetorBemPatrim,qryMasternCdSetorBemPatrim.AsString);

  PosicionaQuery(qryLocalBemPatrim,qryMasternCdLocalBemPatrim.AsString);
  PosicionaQuery(qryCentroCusto,qryMasternCdCentroCustoBemPatrim.AsString);
  PosicionaQuery(qryProduto,qryMasternCdProduto.AsString);

  qryTerceiro.Close;
  qryMotBaixaBemPatrim.Close;

  PosicionaQuery(qryTerceiro,qryMasternCdTerceiro.AsString);
  PosicionaQuery(qryMotBaixaBemPatrim,qryMasternCdMotBaixaBemPatrim.AsString);

  TipoBemNormal.Checked      := (qryMasternCdTabTipoBemPatrim.Value = 1);
  TipoBemAcoplado.Checked    := (qryMasternCdTabTipoBemPatrim.Value = 2);
  TipoAquisicaoNovo.Checked  := (qryMasternCdTabSituacaoBemPatrim.Value = 1);
  TipoAquisicaoUsado.Checked := (qryMasternCdTabSituacaoBemPatrim.Value = 2);

  PosicionaQuery(qryConfigSubGrupoBemPatrim, qryMasternCdSubGrupoBemPatrim.AsString) ;
  
  PosicionaQuery(qryEmpresa, DBEdit20.Text) ;

end;

procedure TfrmBemPatrim.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryConfigBemPatrim.Close;
  qryConfigBemPatrim.Close;
  qryConfigBemPatrimAdicional.Close;
  qryGrupoBemPatrim.Close;
  qrySubGrupoBemPatrim.Close;
  qryClasseBemPatrim.Close;
  qrySetorBemPatrim.Close;
  qryLocalBemPatrim.Close;
  qryCentroCusto.Close;
  qryProduto.Close;
  qryTerceiro.Close;
  qryEmpresa.Close;
  qryMotBaixaBemPatrim.Close;
  TipoBemNormal.Checked := false;
  TipoBemAcoplado.Checked := false;
end;

procedure TfrmBemPatrim.cxPageControl1Enter(Sender: TObject);
begin
  inherited;
  if ((qryMaster.Active) and (qryMaster.State = dsInsert)) then
  qryMaster.Post ;
end;

procedure TfrmBemPatrim.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit9Exit(Sender: TObject);
begin
  inherited;
   PosicionaQuery(qryTerceiro,qryMasternCdTerceiro.AsString);
end;

procedure TfrmBemPatrim.DBGridEh1KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (qryConfigBemPatrimnCdConfigBemPatrim.Value = 0) then DBGridEh1.Columns.Items[1].ReadOnly := True
  else DBGridEh1.Columns.Items[1].ReadOnly := False ;
end;

procedure TfrmBemPatrim.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1006);

            If (nPK > 0) then
            begin
                qryMasternCdMotBaixaBemPatrim.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBemPatrim.DBEdit15Exit(Sender: TObject);
begin
  inherited;
  qryMotBaixaBemPatrim.Close;
  PosicionaQuery(qryMotBaixaBemPatrim,qryMasternCdMotBaixaBemPatrim.AsString);
end;

procedure TfrmBemPatrim.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmBemPatrim.DBEdit20Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, DBEdit20.Text) ;

end;

procedure TfrmBemPatrim.qryConfigBemPatrimAdicionalBeforePost(
  DataSet: TDataSet);
begin

  if (Trim(qryConfigBemPatrimAdicionalcNmConfigSubGrupoBemPatrim.Value) = '') and (Trim(qryConfigBemPatrimAdicionalcConteudoConfigBemPatrim.Value) <> '') then
  begin
      MensagemAlerta('Descri��o da configura��o n�o informado.') ;
      abort ;
  end;

  inherited;
  qryConfigBemPatrimAdicionalcNmConfigSubGrupoBemPatrim.Value := UpperCase(qryConfigBemPatrimAdicionalcNmConfigSubGrupoBemPatrim.Value);
  qryConfigBemPatrimAdicionalcConteudoConfigBemPatrim.Value   := UpperCase(qryConfigBemPatrimAdicionalcConteudoConfigBemPatrim.Value);
  if (qryConfigBemPatrimAdicional.State = dsInsert) then
  begin
    qryConfigBemPatrimAdicionalnCdBemPatrim.Value               := qryMasternCdBemPatrim.Value;
  end;

end;

procedure TfrmBemPatrim.qryConfigBemPatrimBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  MensagemAlerta('N�o � poss�vel excluir Configura��o Padr�o.') ;
  abort ;
end;

procedure TfrmBemPatrim.btExcluirClick(Sender: TObject);
begin
  inherited;
  qryConfigBemPatrim.Close;
  qryConfigBemPatrim.Close;
  qryGrupoBemPatrim.Close;
  qrySubGrupoBemPatrim.Close;
  qryClasseBemPatrim.Close;
  qrySetorBemPatrim.Close;
  qryLocalBemPatrim.Close;
  qryCentroCusto.Close;
  qryProduto.Close;
  qryTerceiro.Close;
  qryEmpresa.Close;
  qryMotBaixaBemPatrim.Close;
  TipoBemNormal.Checked := false;
  TipoBemAcoplado.Checked := false;

end;

procedure TfrmBemPatrim.qryConfigBemPatrimBeforePost(DataSet: TDataSet);
begin
  inherited;
  qryConfigBemPatrimcConteudoConfigBemPatrim.Value := UpperCase(qryConfigBemPatrimcConteudoConfigBemPatrim.Value);
end;

initialization
    RegisterClass(TfrmBemPatrim) ;

end.
