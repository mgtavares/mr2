inherited frmLimiteCredCliente: TfrmLimiteCredCliente
  Caption = 'Limite Cr'#233'dito Cliente'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 82
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 56
    Top = 62
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Raz'#227'o Social'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 44
    Top = 86
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Fantasia'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 72
    Top = 110
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'CNPJ/CPF'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 14
    Top = 134
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'A Receber em aberto'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 24
    Top = 158
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedidos em aberto'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 16
    Top = 182
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'A Receber em atraso'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 0
    Top = 206
    Width = 120
    Height = 13
    Alignment = taRightJustify
    Caption = 'Maior atraso em aberto'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 33
    Top = 230
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Limite de Cr'#233'dito'
    FocusControl = DBEdit9
  end
  inherited ToolBar2: TToolBar
    TabOrder = 10
    inherited btIncluir: TToolButton
      Enabled = False
    end
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 128
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 128
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit3: TDBEdit [13]
    Tag = 1
    Left = 128
    Top = 80
    Width = 650
    Height = 19
    DataField = 'cNmFantasia'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit4: TDBEdit [14]
    Tag = 1
    Left = 128
    Top = 104
    Width = 182
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit5: TDBEdit [15]
    Tag = 1
    Left = 128
    Top = 128
    Width = 169
    Height = 19
    DataField = 'nValCreditoUtil'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [16]
    Tag = 1
    Left = 128
    Top = 152
    Width = 169
    Height = 19
    DataField = 'nValPedidoAberto'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [17]
    Tag = 1
    Left = 128
    Top = 176
    Width = 169
    Height = 19
    DataField = 'nValRecAtraso'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit8: TDBEdit [18]
    Tag = 1
    Left = 128
    Top = 200
    Width = 234
    Height = 19
    DataField = 'dMaiorRecAtraso'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [19]
    Left = 128
    Top = 224
    Width = 169
    Height = 19
    DataField = 'nValLimiteCred'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBCheckBox1: TDBCheckBox [20]
    Left = 128
    Top = 248
    Width = 241
    Height = 17
    Caption = 'Bloqueado para Pedidos e Faturamento'
    DataField = 'cFlgBloqPedVenda'
    DataSource = dsMaster
    TabOrder = 9
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,cNmFantasia'
      '      ,cCNPJCPF'
      '      ,nValCreditoUtil                         '
      '      ,nValPedidoAberto                        '
      '      ,nValRecAtraso                           '
      '      ,dMaiorRecAtraso'
      '      ,nValLimiteCred'
      '      ,cFlgBloqPedVenda'
      '  FROM Terceiro'
      ' WHERE EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 2)'
      '   AND nCdTerceiro = :nPK')
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMasternValCreditoUtil: TBCDField
      FieldName = 'nValCreditoUtil'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValPedidoAberto: TBCDField
      FieldName = 'nValPedidoAberto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValRecAtraso: TBCDField
      FieldName = 'nValRecAtraso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdMaiorRecAtraso: TDateTimeField
      FieldName = 'dMaiorRecAtraso'
    end
    object qryMasternValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgBloqPedVenda: TIntegerField
      FieldName = 'cFlgBloqPedVenda'
    end
  end
end
