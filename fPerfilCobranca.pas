unit fPerfilCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmPerfilCobranca = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasternCdPerfilCobranca: TIntegerField;
    qryMastercNmPerfilCobranca: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPerfilCobranca: TfrmPerfilCobranca;

implementation

{$R *.dfm}

procedure TfrmPerfilCobranca.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'PERFILCOBRANCA' ;
  nCdTabelaSistema  := 238 ;
  nCdConsultaPadrao := 236 ;
end;

procedure TfrmPerfilCobranca.qryMasterBeforePost(DataSet: TDataSet);
begin
  
  if ( trim( DBEdit2.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus;
      abort;
  end ;
  
  inherited;

end;

procedure TfrmPerfilCobranca.btIncluirClick(Sender: TObject);
begin
  inherited;
  
  DBEdit2.SetFocus;
end;

initialization
  RegisterClass( TfrmPerfilCobranca ) ;
end.
