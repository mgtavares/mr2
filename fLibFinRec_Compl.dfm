inherited frmLibFinRec_Compl: TfrmLibFinRec_Compl
  Left = 215
  Top = 111
  Width = 996
  Height = 579
  BorderIcons = []
  Caption = 'Dados Complementares'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 352
    Width = 980
    Height = 189
  end
  inherited ToolBar1: TToolBar
    Width = 980
    ButtonWidth = 115
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
    object ToolButton4: TToolButton
      Left = 238
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
      Visible = False
    end
    object ToolButton5: TToolButton
      Left = 246
      Top = 0
      Caption = 'Recalcular Parcelas'
      ImageIndex = 2
      Visible = False
      OnClick = ToolButton5Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 980
    Height = 44
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 4
      Top = 22
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Nota Fiscal'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 213
      Top = 22
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Caption = '% Desconto Vencimento'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Tag = 1
      Left = 415
      Top = 22
      Width = 107
      Height = 13
      Alignment = taRightJustify
      Caption = '% Acr'#233'scimo (Vendor)'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Tag = 1
      Left = 698
      Top = 22
      Width = 120
      Height = 13
      Alignment = taRightJustify
      Caption = 'Adiantamento em Aberto'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 88
      Top = 14
      Width = 119
      Height = 21
      DataField = 'nValTotalNF'
      DataSource = dsRecebimento
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 336
      Top = 14
      Width = 65
      Height = 21
      DataField = 'nPercDescontoVencto'
      DataSource = dsRecebimento
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 528
      Top = 14
      Width = 65
      Height = 21
      DataField = 'nPercAcrescimoVendor'
      DataSource = dsRecebimento
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 824
      Top = 14
      Width = 119
      Height = 21
      DataField = 'nValAdiantamento'
      DataSource = dsTitulosAdiantamento
      TabOrder = 3
    end
    object cxButton6: TcxButton
      Left = 944
      Top = 12
      Width = 25
      Height = 25
      TabOrder = 4
      TabStop = False
      OnClick = cxButton6Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = True
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 73
    Width = 980
    Height = 279
    ActivePage = cxTabSheet1
    Align = alTop
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 275
    ClientRectLeft = 4
    ClientRectRight = 976
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Parcelas'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 972
        Height = 251
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsParcela
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRecebimento'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'nValParcela'
            Footers = <>
            ReadOnly = True
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'nValOperacaoFin'
            Footers = <>
            ReadOnly = True
            Width = 106
          end
          item
            EditButtons = <>
            FieldName = 'nValPagto'
            Footers = <>
            ReadOnly = True
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
            Width = 160
          end
          item
            EditButtons = <>
            FieldName = 'cFlgDocCobranca'
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            PickList.Strings = (
              'Sim'
              'N'#227'o')
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'cCodBarra'
            Footers = <>
            Width = 341
          end
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
            ReadOnly = True
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl [4]
    Left = 0
    Top = 352
    Width = 980
    Height = 189
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 185
    ClientRectLeft = 4
    ClientRectRight = 976
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 972
        Height = 161
        Align = alClient
        Lines.Strings = (
          'Memo1')
        TabOrder = 0
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 288
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000919191008988
      8800878686008787870088888700898988008988880088888800888887008786
      8600878685009797970000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000088888800C2C2
      C100BCBCBC00BCBCBC00BCBCBB00BCBCBB00BCBCBB00BCBCBB00BCBCBB00BCBC
      BB00C2C2C1008B8B8A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000000000008C8C8B000000
      0000EBEBEB00EBEBEB00E9E9E900E8E8E800E7E7E700E7E7E700E6E6E600E6E6
      E600000000008E8D8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000929191000000
      0000B4B4B40094949400E7E7E700B2B2B20093939300E3E3E300B0B0B0009191
      9100FBFBFB009292910000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000959595000000
      0000E8E8E800E7E7E700E5E5E500E3E3E300E2E2E200E0E0E000DFDFDF00DCDC
      DC00000000009595950000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000000000009A9A99000000
      0000B1B1B10091919100E2E2E200ADADAD008F8F8F00DCDCDC00A9A9A9008D8D
      8D00FBFBFB009A99990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000009E9D9D000000
      0000E3E3E300E1E1E100DCDCDC00DBDBDB00D7D7D700D3D3D300D3D3D300D1D1
      D100000000009E9D9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000A0A0A0000000
      0000ADADAD008E8E8E00D8D8D800A5A5A5008A8A8A00CECECE007374E8005258
      DB00FBFBFB009E9E9E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000A3A3A300FCFC
      FC00DADADA00D7D7D700D2D2D200CECECE00C9C9C900C5C5C500C2C2C200BFBF
      BF0000000000A0A0A00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000A3A3A3000000
      0000B07B5600C38D6700C58F6800C6906900C8926B00CA946C00CA956E00B07B
      560000000000A1A09F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000A4A4A4000000
      0000B07B5600C18B6400C38D6600C58F6700C6906900C8926B00CA946C00B07B
      560000000000A0A0A00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000A3A3A3000000
      0000A7724D00A7724D00A7724D00A7724D00A7724D00A7724D00A7724D00A772
      4D00000000009E9E9E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000AAAAAA000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A3A3A30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B3B3B300A9A9
      A900A8A8A800ABABAB00ACACAC00ADADAD00ACACAC00ABABAB00A8A8A800A4A4
      A400A3A3A300B5B5B50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFC0030000
      FE00C003C003000000008001D00B000000008001D003000000008001D00B0000
      00008001D003000000008001D00B000000008001D003000000008001C00B0000
      00008001D00B000000008001D00B000000018001D00B000000038001DFFB0000
      0077C003C0030000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryRecebimento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM Recebimento'
      ' WHERE nCdRecebimento = :nPK')
    Left = 264
    Top = 288
    object qryRecebimentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryRecebimentonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryRecebimentonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryRecebimentonCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryRecebimentodDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryRecebimentonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryRecebimentocNrDocto: TStringField
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryRecebimentocSerieDocto: TStringField
      FieldName = 'cSerieDocto'
      FixedChar = True
      Size = 2
    end
    object qryRecebimentodDtDocto: TDateTimeField
      FieldName = 'dDtDocto'
    end
    object qryRecebimentonValDocto: TBCDField
      FieldName = 'nValDocto'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonCdTabStatusReceb: TIntegerField
      FieldName = 'nCdTabStatusReceb'
    end
    object qryRecebimentonCdUsuarioFech: TIntegerField
      FieldName = 'nCdUsuarioFech'
    end
    object qryRecebimentodDtFech: TDateTimeField
      FieldName = 'dDtFech'
    end
    object qryRecebimentocOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryRecebimentonValTotalNF: TBCDField
      FieldName = 'nValTotalNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonValTitulo: TBCDField
      FieldName = 'nValTitulo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonPercDescontoVencto: TBCDField
      FieldName = 'nPercDescontoVencto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonPercAcrescimoVendor: TBCDField
      FieldName = 'nPercAcrescimoVendor'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonValTotalItensPag: TBCDField
      FieldName = 'nValTotalItensPag'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentonValDescontoNF: TBCDField
      FieldName = 'nValDescontoNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryParcelasBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM PrazoRecebimento'
      ' WHERE nCdRecebimento = :nPK')
    Left = 232
    Top = 288
    object qryParcelasnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryParcelasdDtVenc: TDateTimeField
      DisplayLabel = 'Parcelas Pagamento|Data Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryParcelasnValPagto: TBCDField
      DisplayLabel = 'Parcelas Pagamento|Valor a pagar'
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
    object qryParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryParcelascNrTit: TStringField
      DisplayLabel = 'Parcelas Pagamento|N'#250'mero T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelascFlgDocCobranca: TIntegerField
      DisplayLabel = 'Parcelas Pagamento|Enviado Documento de Cobran'#231'a'
      FieldName = 'cFlgDocCobranca'
    end
    object qryParcelasnValParcela: TBCDField
      DisplayLabel = 'Parcelas Pagamento|Valor Parcela'
      FieldName = 'nValParcela'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValOperacaoFin: TBCDField
      DisplayLabel = 'Parcelas Pagamento|Opera'#231#227'o Financeira'
      FieldName = 'nValOperacaoFin'
      Precision = 12
      Size = 2
    end
    object qryParcelascCodBarra: TStringField
      DisplayLabel = 'Parcelas Pagamento|C'#243'd. Barras'
      FieldName = 'cCodBarra'
      Size = 560
    end
  end
  object qryPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nPercDescontoVencto'
      '      ,nPercAcrescimoVendor'
      '  FROM Pedido'
      ' WHERE nCdPedido = (SELECT nCdPedido'
      '                      FROM ItemPedido'
      
        '                     WHERE nCdItemPedido = (SELECT TOP 1 nCdItem' +
        'Pedido'
      
        '                                              FROM ItemRecebimen' +
        'to'
      
        '                                             WHERE nCdItemPedido' +
        ' IS NOT NULL'
      
        '                                               AND nCdRecebiment' +
        'o = :nPK))')
    Left = 328
    Top = 288
    object qryPedidonPercDescontoVencto: TBCDField
      FieldName = 'nPercDescontoVencto'
      Precision = 12
      Size = 2
    end
    object qryPedidonPercAcrescimoVendor: TBCDField
      FieldName = 'nPercAcrescimoVendor'
      Precision = 12
      Size = 2
    end
  end
  object dsRecebimento: TDataSource
    DataSet = qryRecebimento
    Left = 264
    Top = 320
  end
  object dsParcela: TDataSource
    DataSet = qryParcelas
    Left = 232
    Top = 320
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 328
    Top = 320
  end
  object usp_processo: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_AUTORIZA_PAGAMENTO_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cOBSTitulo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1000
        Value = Null
      end>
    Left = 360
    Top = 288
  end
  object SP_GERA_PARCELA_RECEBIMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_PARCELA_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 320
  end
  object qryTitulosAdiantamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Sum(nSaldoTit) as nValAdiantamento'
      '  FROM Titulo'
      ' WHERE cFlgAdiantamento = 1'
      '   AND nSaldoTit        > 0'
      '   AND nCdTerceiro      = :nPK'
      ''
      ''
      ''
      '')
    Left = 296
    Top = 288
    object qryTitulosAdiantamentonValAdiantamento: TBCDField
      FieldName = 'nValAdiantamento'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object dsTitulosAdiantamento: TDataSource
    DataSet = qryTitulosAdiantamento
    Left = 296
    Top = 320
  end
end
