unit rPreVendaPendente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, DBCtrls, ADODB;

type
  TrptPreVendaPendente = class(TfrmRelatorio_Padrao)
    edtDtFinal: TMaskEdit;
    Label6: TLabel;
    edtDtInicial: TMaskEdit;
    Label3: TLabel;
    Label5: TLabel;
    edtCdLoja: TMaskEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    procedure edtCdLojaExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtCdLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPreVendaPendente: TrptPreVendaPendente;

implementation

uses fMenu, rPreVendaPendente_view, fLookup_Padrao;

{$R *.dfm}

procedure TrptPreVendaPendente.edtCdLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryLoja, edtCdLoja.Text) ;
  
end;

procedure TrptPreVendaPendente.ToolButton1Click(Sender: TObject);
var
    cFiltro :string ;
    objRel : TrptPreVendaPendente_view;
begin
  inherited;

  if (Trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  if (Trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date-30) ;


  objRel := TrptPreVendaPendente_view.Create(nil);

  try
      try

          objRel.qryResultado.Close;

          objRel.qryResultado.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(edtCdLoja.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          objRel.qryResultado.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          cFiltro := '' ;
          cFiltro := 'Per�odo : ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text ;

          if (DBEdit1.Text <> '') then
              cFiltro := cFiltro + ' / Loja : ' + trim(edtCdLoja.Text) + ' - ' + DBEdit1.Text ;

          objRel.lblFiltro1.Caption := cFiltro ;

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;

end;

procedure TrptPreVendaPendente.edtCdLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            edtCdLoja.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(TrptPreVendaPendente) ;

end.
