unit fMapaCompra_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, StdCtrls, DBCtrls, Mask, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB;

type
  TfrmMapaCompra_Itens = class(TfrmProcesso_Padrao)
    qryRequisicoes: TADOQuery;
    DataSource1: TDataSource;
    qryRequisicoesnCdRequisicao: TIntegerField;
    qryRequisicoescNmSetor: TStringField;
    qryRequisicoescNmSolicitante: TStringField;
    qryRequisicoescNmUsuario: TStringField;
    qryRequisicoesnQtde: TBCDField;
    qryRequisicoescFlgUrgente: TStringField;
    qryRequisicoescJustificativa: TStringField;
    qryRequisicoescNotaComprador: TStringField;
    qryRequisicoescDescricaoTecnica: TMemoField;
    qryRequisicoescNmCC: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdRequisicao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmSetor: TcxGridDBColumn;
    cxGrid1DBTableView1cNmSolicitante: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCC: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBMemo1: TDBMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMapaCompra_Itens: TfrmMapaCompra_Itens;

implementation

{$R *.dfm}

end.
