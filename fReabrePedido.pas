unit fReabrePedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB;

type
  TfrmReabrePedido = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    edtPedidoIni: TMaskEdit;
    Label3: TLabel;
    dDtInicial: TMaskEdit;
    Label6: TLabel;
    dDtFinal: TMaskEdit;
    Label1: TLabel;
    edtPedidoFim: TMaskEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryPedido: TADOQuery;
    dsPedido: TDataSource;
    qryPedidonCdPedido: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonValPedido: TBCDField;
    qryPedidocNmTabStatusPed: TStringField;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabStatusPed: TcxGridDBColumn;
    qryPedidonQtdeFat: TBCDField;
    SP_REABRE_PEDIDO: TADOStoredProc;
    qryPedidonValDevoluc: TBCDField;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReabrePedido: TfrmReabrePedido;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmReabrePedido.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (frmMenu.ConvInteiro(edtPedidoIni.Text) = 0) and (frmMenu.ConvInteiro(edtPedidoFim.Text) = 0) then
  begin
      if (trim(dDtInicial.Text) = '/  /') and (trim(dDtFinal.Text) = '/  /') then
      begin
          MensagemAlerta('Selecione um filtro para a consulta.') ;
          exit ;
      end ;
  end ;

  qryPedido.Close ;
  qryPedido.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
  qryPedido.Parameters.ParamByName('nCdPedidoIni').Value := frmMenu.ConvInteiro(edtPedidoIni.Text);
  qryPedido.Parameters.ParamByName('nCdPedidoFim').Value := frmMenu.ConvInteiro(edtPedidoFim.Text);
  qryPedido.Parameters.ParamByName('cDtInicial').Value   := frmMenu.ConvData(dDtInicial.Text);
  qryPedido.Parameters.ParamByName('cDtFinal').Value     := frmMenu.ConvData(dDtFinal.Text);
  qryPedido.Parameters.ParamByName('nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
  qryPedido.Open ;

  if qryPedido.eof then
  begin
      MensagemAlerta('Nenhum pedido encontrado para o crit�rio utilizado.') ;
  end ;
  
end;

procedure TfrmReabrePedido.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;

  if not (qryPedido.eof) then
  begin

      if (qryPedidonQtdeFat.Value > 0) then
      begin
          MensagemAlerta('Este pedido j� movimentou itens no estoque e n�o poder� ser reaberto antes do cancelamento destes movimentos.') ;
          exit ;
      end ;

      if (qryPedidonValDevoluc.Value > 0) then
      begin
          MensagemAlerta('Este pedido tem abatimento por devolu��o de venda e n�o pode ser reaberto.') ;
          exit ;
      end ;

      case MessageDlg('Confirma a reabertura deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          SP_REABRE_PEDIDO.Close ;
          SP_REABRE_PEDIDO.Parameters.ParamByName('@nCdPedido').Value  := qryPedidonCdPedido.Value ;
          SP_REABRE_PEDIDO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          SP_REABRE_PEDIDO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Pedido reaberto com sucesso!') ;

      qryPedido.Close ;

  end ;

end;

initialization
    RegisterClass(TfrmReabrePedido) ;

end.
