inherited frmTipoLancto: TfrmTipoLancto
  Caption = 'Tipo de Lan'#231'amento de Conta'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 48
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 37
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 94
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sinal Opera'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 112
    Top = 94
    Width = 46
    Height = 13
    Caption = '(  +  /  -  )'
    FocusControl = DBEdit3
  end
  object Label5: TLabel [5]
    Left = 8
    Top = 214
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cont'#225'bil'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [6]
    Left = 88
    Top = 230
    Width = 563
    Height = 13
    Caption = 
      '(Informe a conta somente se este tipo de lan'#231'amento for contabil' +
      'izado no DRE e no Fluxo de Caixa Realizado)'
    FocusControl = DBEdit4
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 88
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoLancto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [9]
    Left = 88
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmTipoLancto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [10]
    Left = 88
    Top = 88
    Width = 17
    Height = 19
    DataField = 'cSinalOper'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBCheckBox1: TDBCheckBox [11]
    Left = 88
    Top = 112
    Width = 177
    Height = 17
    Caption = 'Permite Lan'#231'amento Manual '
    DataField = 'cFlgPermManual'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [12]
    Left = 360
    Top = 112
    Width = 265
    Height = 17
    Caption = 'Emitir Comprovante Impressora Fiscal'
    DataField = 'cFlgEmiteECF'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox3: TDBCheckBox [13]
    Left = 88
    Top = 136
    Width = 265
    Height = 17
    Caption = 'Permitir Lan'#231'amento na Conta Banc'#225'ria'
    DataField = 'cFlgContaBancaria'
    DataSource = dsMaster
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox4: TDBCheckBox [14]
    Left = 360
    Top = 136
    Width = 265
    Height = 17
    Caption = 'Permitir Lan'#231'amento na Conta Caixa/Cofre'
    DataField = 'cFlgContaCaixa'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit4: TDBEdit [15]
    Left = 88
    Top = 208
    Width = 65
    Height = 19
    DataField = 'nCdPlanoConta'
    DataSource = dsMaster
    TabOrder = 11
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [16]
    Tag = 1
    Left = 160
    Top = 208
    Width = 345
    Height = 19
    DataField = 'cNmPlanoConta'
    DataSource = DataSource1
    TabOrder = 12
  end
  object DBCheckBox5: TDBCheckBox [17]
    Left = 88
    Top = 160
    Width = 265
    Height = 17
    Caption = 'Permitir Uso para D'#233'bito Autom'#225'tico'
    DataField = 'cFlgPermDA'
    DataSource = dsMaster
    TabOrder = 8
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox6: TDBCheckBox [18]
    Left = 360
    Top = 160
    Width = 265
    Height = 17
    Caption = 'Apurar Lan'#231'amento no DRE'
    DataField = 'cFlgApurarDRE'
    DataSource = dsMaster
    TabOrder = 9
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox7: TDBCheckBox [19]
    Left = 88
    Top = 184
    Width = 265
    Height = 17
    Caption = 'Considerar Cr'#233'dito n'#227'o Identificado'
    DataField = 'cFlgCredNaoIdent'
    DataSource = dsMaster
    TabOrder = 10
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TIPOLancto'
      'WHERE nCdTipoLancto = :nPK')
    Left = 368
    Top = 264
    object qryMasternCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryMastercNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryMastercSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
    object qryMastercFlgPermManual: TIntegerField
      FieldName = 'cFlgPermManual'
    end
    object qryMastercFlgEmiteECF: TIntegerField
      FieldName = 'cFlgEmiteECF'
    end
    object qryMasternCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryMastercFlgContaBancaria: TIntegerField
      FieldName = 'cFlgContaBancaria'
    end
    object qryMastercFlgContaCaixa: TIntegerField
      FieldName = 'cFlgContaCaixa'
    end
    object qryMastercFlgPermDA: TIntegerField
      FieldName = 'cFlgPermDA'
    end
    object qryMastercFlgApurarDRE: TIntegerField
      FieldName = 'cFlgApurarDRE'
    end
    object qryMastercFlgCredNaoIdent: TIntegerField
      FieldName = 'cFlgCredNaoIdent'
    end
  end
  inherited dsMaster: TDataSource
    Left = 400
    Top = 264
  end
  inherited qryID: TADOQuery
    Left = 432
    Top = 264
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdPlanoConta, cNmPlanoConta FROM PlanoConta WHERE nCdPla' +
        'noConta = :nPK')
    Left = 456
    Top = 320
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryPlanoConta
    Left = 592
    Top = 328
  end
end
