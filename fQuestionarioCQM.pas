unit fQuestionarioCQM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmQuestionarioCQM = class(TfrmCadastro_Padrao)
    qryMasternCdQuestionarioCQM: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryItemQuestionario: TADOQuery;
    qryItemQuestionarionCdItemQuestionarioCQM: TAutoIncField;
    qryItemQuestionarionCdQuestionarioCQM: TIntegerField;
    qryItemQuestionarioiOrdem: TIntegerField;
    qryItemQuestionariocPergunta: TStringField;
    qryItemQuestionarionCdTabTipoResposta: TIntegerField;
    dsItemQuestionario: TDataSource;
    qryItemQuestionarioiPeso: TIntegerField;
    qryMastercRevisaoQuestionario: TStringField;
    qryMastercCodFormulario: TStringField;
    qryMastercInformacaoAdicional: TMemoField;
    qryMastercEmitente: TStringField;
    qryMastercAvaliador: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBMemo1: TDBMemo;
    qryMastercNmQuestionarioCQM: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryItemQuestionarioBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmQuestionarioCQM: TfrmQuestionarioCQM;

implementation

{$R *.dfm}

procedure TfrmQuestionarioCQM.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'QUESTIONARIOCQM' ;
  nCdTabelaSistema  := 77 ;
  nCdConsultaPadrao := 181 ;

end;

procedure TfrmQuestionarioCQM.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  DBedit2.SetFocus;
end;

procedure TfrmQuestionarioCQM.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do question�rio.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

end;

procedure TfrmQuestionarioCQM.qryItemQuestionarioBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;
  inherited;

  if (qryItemQuestionarioiOrdem.Value <= 0) then
  begin
      MensagemAlerta('Informe a ordem que a pergunta ser� exibida na avalia��o da qualidade.') ;
      abort ;
  end ;

  if (qryItemQuestionariocPergunta.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o da pergunta.') ;
      abort ;
  end ;

  if (qryItemQuestionarionCdTabTipoResposta.Value <= 0) then
  begin
      MensagemAlerta('Informe o tipo de resposta esperado para a pergunta.') ;
      abort ;
  end ;


  qryItemQuestionarionCdQuestionarioCQM.Value := qryMasternCdQuestionarioCQM.Value ;
end;

procedure TfrmQuestionarioCQM.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryItemQuestionario, qryMasternCdQuestionarioCQM.AsString) ;
  
end;

procedure TfrmQuestionarioCQM.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryItemQuestionario.Close ;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmQuestionarioCQM.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItemQuestionario, qryMasternCdQuestionarioCQM.AsString) ;

end;

procedure TfrmQuestionarioCQM.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInactive) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;
end;

procedure TfrmQuestionarioCQM.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmQuestionarioCQM.btExcluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

initialization
    RegisterClass(TfrmQuestionarioCQM) ;

end.
