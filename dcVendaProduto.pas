unit dcVendaProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  TeeProcs, TeEngine, Chart, MXGRAPH, DB, MXPIVSRC, Grids, MXGRID, ADODB,
  Mxstore, MXDB, DBTables, MXTABLES, PivotMap_SRC, PivotToolBar_SRC,
  PivotGrid_SRC, PivotCube_SRC, StdCtrls, Mask, Series, PivotChart_SRC,
  cxPC, cxControls;

type
  TdcmVendaProduto = class(TfrmProcesso_Padrao)
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1cNmDepartamento: TStringField;
    ADODataSet1cNmCategoria: TStringField;
    ADODataSet1cNmSubCategoria: TStringField;
    ADODataSet1cNmSegmento: TStringField;
    ADODataSet1nValor: TBCDField;
    ADODataSet1nQtde: TBCDField;
    ADODataSet1cNmMarca: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    d: TPivotGrid;
    ADODataSet1cNmProduto: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaProduto: TdcmVendaProduto;

implementation

uses fMenu;

{$R *.dfm}

procedure TdcmVendaProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  ADODataSet1.Close ;
  ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  ADODataSet1.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(MaskEdit1.Text) ;
  ADODataSet1.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(MaskEdit2.Text) ;
  ADODataSet1.Open ;

  if (ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informação encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if PivotCube1.Active then
  begin
      PivotCube1.Active := False;
  end;

  PivotCube1.ExtendedMode := True;
  PVMeasureToolBar1.HideButtons := False;
  PivotCube1.Active := True;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

end;

procedure TdcmVendaProduto.FormShow(Sender: TObject);
begin
  inherited;
  MaskEdit1.SetFocus;
end;

initialization
    RegisterClass(tdcmVendaProduto) ;

end.
