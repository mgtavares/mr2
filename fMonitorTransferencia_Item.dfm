inherited frmMonitorTransferencia_Item: TfrmMonitorTransferencia_Item
  Left = 298
  Top = 166
  Width = 832
  Height = 488
  BorderIcons = []
  Caption = 'Monitor de Transfer'#234'ncias - Itens'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 816
    Height = 421
  end
  inherited ToolBar1: TToolBar
    Width = 816
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 816
    Height = 421
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsItemTransfEst
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemTransfEst'
        Footers = <>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|C'#243'd.'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|Produto|C'#243'd.'
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|Produto|Descri'#231#227'o'
        Width = 403
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footer.FieldName = 'nQtde'
        Footer.Value = '0,00'
        Footers = <
          item
            DisplayFormat = '#,##0.00'
            FieldName = 'nQtde'
            Value = '0,00'
            ValueType = fvtSum
          end>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|Qtd.'
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeConf'
        Footer.FieldName = 'nQtdeConf'
        Footers = <
          item
            DisplayFormat = '#,##0.00'
            FieldName = 'nQtdeConf'
            Value = '0,00'
            ValueType = fvtSum
          end>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|Qtd. Conf.'
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footer.FieldName = 'nValTotal'
        Footers = <
          item
            DisplayFormat = '#,##0.00'
            FieldName = 'nValTotal'
            Value = '#,##0.00'
            ValueType = fvtSum
          end>
        Title.Caption = 'Rela'#231#227'o de Itens Transferidos|Total Item Transf.'
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
      object Label1: TLabel
        Left = 248
        Top = 56
        Width = 53
        Height = 13
        Caption = 'nQtdeConf'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 248
        Top = 72
        Width = 173
        Height = 21
        DataField = 'nQtdeConf'
        DataSource = dsItemTransfEst
        TabOrder = 0
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 280
    Top = 168
  end
  object qryItemTransfEst: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemTransfEst'
      '      ,Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,nQtde'
      '      ,nQtdeConf'
      '      ,nValTotal'
      '  FROM ItemTransfEst'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = ItemTransfEst.' +
        'nCdProduto'
      ' WHERE nCdTransfEst = :nPK')
    Left = 248
    Top = 168
    object qryItemTransfEstnCdItemTransfEst: TIntegerField
      FieldName = 'nCdItemTransfEst'
    end
    object qryItemTransfEstnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItemTransfEstcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItemTransfEstnQtde: TBCDField
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemTransfEstnQtdeConf: TBCDField
      FieldName = 'nQtdeConf'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemTransfEstnValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsItemTransfEst: TDataSource
    DataSet = qryItemTransfEst
    Left = 248
    Top = 200
  end
end
