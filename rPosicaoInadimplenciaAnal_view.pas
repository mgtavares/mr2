unit rPosicaoInadimplenciaAnal_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, QuickRpt, ADODB, jpeg, QRCtrls, ExtCtrls;

type
  TrelPosicaoInadimplenciaAnal_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRBand3: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    SPREL_POSICAO_INADIMPLENCIA: TADOStoredProc;
    QRGroup1: TQRGroup;
    QRBand2: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText13: TQRDBText;
    SPREL_POSICAO_INADIMPLENCIAcNmTerceiro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAcCNPJCPF: TStringField;
    SPREL_POSICAO_INADIMPLENCIAdDtCadastro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAcMesAno: TStringField;
    SPREL_POSICAO_INADIMPLENCIAiAno: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAiMes: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotal: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloTotal: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimp: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimp: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimpAtual: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimpAtual: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnMediaCompetencia: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAnMediaAtual: TFloatField;
    QRExpr5: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr7: TQRExpr;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRBand4: TQRBand;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  relPosicaoInadimplenciaAnal_view: TrelPosicaoInadimplenciaAnal_view;

implementation

{$R *.dfm}

end.
