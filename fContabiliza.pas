unit fContabiliza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  Mask, DB, ADODB, cxControls, cxContainer, cxEdit, cxProgressBar, GridsEh,
  DBGridEh, DBGridEhGrouping, cxPC;

type
  TfrmContabiliza = class(TfrmProcesso_Padrao)
    qryParametro: TADOQuery;
    qryParametrocParametro: TStringField;
    qryParametrocValor: TStringField;
    qryParametrocDescricao: TStringField;
    usp_Contabiliza: TADOStoredProc;
    dsLogProcesso: TDataSource;
    qryLogProcesso: TADOQuery;
    qryLogProcessodDtLog: TDateTimeField;
    qryLogProcessonCdUsuario: TIntegerField;
    qryLogProcessocNmUsuario: TStringField;
    qryLogProcessocObserv: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContabiliza: TfrmContabiliza;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmContabiliza.FormShow(Sender: TObject);
begin
  inherited;

  usp_Contabiliza.ProcedureName := Trim(frmMenu.LeParametroEmpresa('proc:SP_CONTABILIZA')) + ';1' ;
  usp_Contabiliza.Parameters.Refresh;

  MaskEdit3.Text := DateTimeToStr(Now()-1) ;
  Maskedit1.Text := frmMenu.LeParametro('DTCONTAB') ;
  MaskEdit2.SetFocus;

  qryLogProcesso.Close ;
  qryLogProcesso.Open ;

end;

procedure TfrmContabiliza.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(maskedit2.Text) = '/  /') then
  begin
      ShowMessage('Informe a data para contabilização') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  If (StrToDateTime(MaskEdit2.Text) > StrToDateTime(MaskEdit3.Text)) then
  begin
      ShowMessage('A data para contabilização é superior a data limite') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;


  if ((Trim(maskedit1.Text) <> '/  /') and (StrToDateTime(MaskEdit2.Text) <= StrToDateTime(MaskEdit1.Text))) then
  begin
      ShowMessage('A data para contabilização deve ser superior a data da última contabilização.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  case MessageDlg('Confirma o Processamento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      usp_Contabiliza.Close ;
      usp_Contabiliza.Parameters.ParamByName('@dDtFechamento').Value := MaskEdit2.Text ;
      usp_Contabiliza.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      usp_Contabiliza.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processo de contabilização') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Processo concluído com sucesso.') ;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open  ;

  MaskEdit2.Text := '' ;

  

end;

initialization
    RegisterClass(tFrmContabiliza) ;
end.
