unit fDigitaSaldoDiarioContaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, StdCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  DB, ADODB;

type
  TfrmDigitaSaldoDiarioContaBancaria = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacContaBancaria: TStringField;
    qryContaBancarianSaldoConta: TBCDField;
    DataSource1: TDataSource;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    SP_EFETIVA_SALDO_DIARIO: TADOStoredProc;
    GroupBox1: TGroupBox;
    edtDataSaldo: TMaskEdit;
    Label1: TLabel;
    procedure edtDataSaldoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    dDtSaldo : String;
  public
    { Public declarations }
  end;

var
  frmDigitaSaldoDiarioContaBancaria: TfrmDigitaSaldoDiarioContaBancaria;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmDigitaSaldoDiarioContaBancaria.edtDataSaldoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
  begin

      try
          StrToDate(edtDataSaldo.Text);
      except
          MensagemAlerta('A data digitada � inv�lida.');
          edtDataSaldo.SetFocus;
          abort;
      end;

      if (frmMenu.ConvData(edtDataSaldo.Text) > Date()) then
      begin
          MensagemAlerta('N�o � poss�vel informar o saldo para uma data posterior � hoje.');
          edtDataSaldo.SetFocus;
          abort;
      end;

      cmdPreparaTemp.Execute;

      qryPopulaTemp.Close;
      qryPopulaTemp.Parameters.ParamByName('dDtSaldo').Value   := edtDataSaldo.Text;
      qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryPopulaTemp.ExecSQL;

      qryContaBancaria.Close;
      qryContaBancaria.Open;

      dDtSaldo := edtDataSaldo.Text;

      DBGridEh1.SetFocus;

  end;
  
end;

procedure TfrmDigitaSaldoDiarioContaBancaria.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryContaBancaria.Active) then
  begin

      if (MessageDLG('Confirma os valores digitados ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          exit;

      if (StrToDate(edtDataSaldo.Text) <> StrToDate(dDtSaldo)) then
      begin
          MensagemAlerta('A data de consulta do saldo foi alterada, imposs�vel continuar.');
          abort;
      end;

      frmMenu.Connection.BeginTrans;

      try
          SP_EFETIVA_SALDO_DIARIO.Close;
          SP_EFETIVA_SALDO_DIARIO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          SP_EFETIVA_SALDO_DIARIO.Parameters.ParamByName('@dDtSaldo').Value   := StrToDate(dDtSaldo);
          SP_EFETIVA_SALDO_DIARIO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no Processamento.');
          raise;
      end;

      frmMenu.Connection.CommitTrans;

      qryContaBancaria.Close;
      edtDataSaldo.Text := '';
      dDtSaldo          := '';

      ShowMessage('Saldo di�rio atualizado com sucesso.');

      edtDataSaldo.SetFocus;
  end;
  
end;

procedure TfrmDigitaSaldoDiarioContaBancaria.FormShow(Sender: TObject);
begin
  inherited;

  edtDataSaldo.SetFocus;
  
end;

initialization
    RegisterClass(TfrmDigitaSaldoDiarioContaBancaria);

end.
