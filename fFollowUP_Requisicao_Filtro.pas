unit fFollowUP_Requisicao_Filtro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, Mask, StdCtrls, DBCtrls, ER2Lookup;

type
  TfrmFollowUP_Requisicao_Filtro = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    mskDtAtendInicial: TMaskEdit;
    mskDtAtendFinal: TMaskEdit;
    DBEdit5: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    dsTipoRequisicao: TDataSource;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    dsMarca: TDataSource;
    Label9: TLabel;
    qryTipoRequisicao: TADOQuery;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryCC: TADOQuery;
    dsCC: TDataSource;
    Label11: TLabel;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit7: TDBEdit;
    dsProduto: TDataSource;
    Label10: TLabel;
    er2LkpLoja: TER2LookupMaskEdit;
    er2LkpTipoRequisicao: TER2LookupMaskEdit;
    er2LkpCentroCusto: TER2LookupMaskEdit;
    er2LkpSetor: TER2LookupMaskEdit;
    er2LkpMarca: TER2LookupMaskEdit;
    er2LkpProduto: TER2LookupMaskEdit;
    qrySetor: TADOQuery;
    dsSetor: TDataSource;
    edtEmpresa: TDBEdit;
    qryLojanCdLoja: TIntegerField;
    qryLojanCdEmpresa: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryTipoRequisicaonCdTipoRequisicao: TIntegerField;
    qryTipoRequisicaocNmTipoRequisicao: TStringField;
    qryTipoRequisicaonCdTabTipoRequis: TIntegerField;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qrySetornCdSetor: TIntegerField;
    qrySetorcNmSetor: TStringField;
    qrySetornCdCC: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit9: TDBEdit;
    Label4: TLabel;
    mskDtRequisicaoInicial: TMaskEdit;
    Label8: TLabel;
    mskDtRequisicaoFinal: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure er2LkpCentroCustoBeforePosicionaQry(Sender: TObject);
    procedure er2LkpSetorBeforePosicionaQry(Sender: TObject);
    procedure er2LkpSetorBeforeLookup(Sender: TObject);
    procedure er2LkpCentroCustoBeforeLookup(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFollowUP_Requisicao_Filtro: TfrmFollowUP_Requisicao_Filtro;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmFollowUP_Requisicao_Filtro.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryEmpresa.Parameters.ParamByName('nPK').Value        := frmMenu.nCdEmpresaAtiva;
  qryEmpresa.Open;
end;

procedure TfrmFollowUP_Requisicao_Filtro.er2LkpCentroCustoBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione o tipo de requisição.');
      er2LkpTipoRequisicao.SetFocus;
      Abort;
  end;

  er2LkpCentroCusto.WhereAdicional.Text := 'EXISTS(SELECT 1 FROM CentroCustoTipoRequisicao CCTR WHERE CCTR.nCdCC = CentroCusto.nCdCC AND CCTR.nCdTipoRequisicao = '+ qryTipoRequisicaonCdTipoRequisicao.AsString +')';
end;

procedure TfrmFollowUP_Requisicao_Filtro.er2LkpCentroCustoBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione o tipo de requisição.');
      er2LkpTipoRequisicao.SetFocus;
      Abort;
  end;

  qryCC.Parameters.ParamByName('nCdTipoRequisicao').Value := qryTipoRequisicaonCdTipoRequisicao.AsString;
end;

procedure TfrmFollowUP_Requisicao_Filtro.er2LkpSetorBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('BLOQSETORREQUIS') = 'N' ) then
  begin
      er2LkpSetor.WhereAdicional.Text := 'EXISTS (SELECT 1 FROM UsuarioSetor US WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND US.nCdSetor = Setor.nCdSetor)';
  end
  else
  begin
      if (DBEdit6.Text = '') then
      begin
          MensagemAlerta('Selecione o centro de custo.');
          er2LkpCentroCusto.SetFocus;
          Abort;
      end;

      er2LkpSetor.WhereAdicional.Text := 'nCdCC = ' + qryCCnCdCC.AsString;
  end;
end;

procedure TfrmFollowUP_Requisicao_Filtro.er2LkpSetorBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  qrySetor.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmFollowUP_Requisicao_Filtro.ToolButton4Click(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryTipoRequisicao.Close;
  qryCC.Close;
  qrySetor.Close;
  qryMarca.Close;
  qryProduto.Close;

  er2LkpLoja.Clear;
  er2LkpTipoRequisicao.Clear;
  er2LkpCentroCusto.Clear;
  er2LkpSetor.Clear;
  er2LkpMarca.Clear;
  er2LkpProduto.Clear;
  mskDtAtendInicial.Clear;
  mskDtAtendFinal.Clear;
  mskDtRequisicaoInicial.Clear;
  mskDtRequisicaoFinal.Clear;

  er2LkpLoja.SetFocus;
end;

end.
