inherited frmEnvioClienteCobradora_Titulos: TfrmEnvioClienteCobradora_Titulos
  Left = 280
  Top = 143
  Width = 855
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos Pendentes do Cliente n'#227'o Enviado para Cobradora'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 839
  end
  inherited ToolBar1: TToolBar
    Width = 839
    ButtonWidth = 100
    inherited ToolButton1: TToolButton
      Caption = 'Confirmar Envio'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 100
    end
    inherited ToolButton2: TToolButton
      Left = 108
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 839
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulosPendentes
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -13
    FooterFont.Name = 'Consolas'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    ParentFont = False
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtraso'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValLiq'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 416
    Top = 152
  end
  object qryTitulosPendentes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTitulo'
      '      ,dbo.fn_ZeroEsquerda(nCdLojaTit,3) as nCdLojaTit'
      '      ,cNrTit'
      #9'    ,iParcela'
      #9'    ,cNmEspTit'
      #9'    ,dDtEmissao'
      #9'    ,dDtVenc'
      
        #9'    ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN Conver' +
        't(int,dbo.fn_OnlyDate(GetDate())-dDtVenc)'
      #9#9#9'      ELSE 0'
      #9'     END  iDiasAtraso'
      #9'    ,nValTit'
      #9'    ,nValLiq'
      
        #9'    ,dbo.fn_SimulaJurosTitulo(nCdTitulo,dbo.fn_OnlyDate(GetDate' +
        '()))+dbo.fn_SimulaMultaTitulo(nCdTitulo) nValCorrigido'
      #9'    ,nValDesconto'
      #9'    ,nSaldoTit'
      #9'    ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN 1'
      #9#9#9'      ELSE 0'
      #9'     END  cFlgAtraso'
      '  FROM Titulo'
      #9'     INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE EspTit.cFlgCobranca  = 1'
      '   AND Titulo.nCdEmpresa    = :nCdEmpresa'
      '   AND Titulo.nCdTerceiro   = :nPK'
      '   AND Titulo.nSaldoTit     > 0'
      '   AND Titulo.cSenso        = '#39'C'#39
      '   AND Titulo.dDtCancel    IS NULL'
      '   AND Titulo.cFlgCobradora = 0'
      ' ORDER BY dDtVenc')
    Left = 336
    Top = 141
    object qryTitulosPendentesnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos em Aberto|ID'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosPendentesnCdLojaTit: TStringField
      DisplayLabel = 'T'#237'tulos em Aberto|Loja'
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
    object qryTitulosPendentescNrTit: TStringField
      DisplayLabel = 'T'#237'tulos em Aberto|Nr. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosPendentesiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos em Aberto|Parc.'
      FieldName = 'iParcela'
    end
    object qryTitulosPendentescNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos em Aberto|Esp'#233'cie T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosPendentesdDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos em Aberto|Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosPendentesdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos em Aberto|Dt. Vencto.'
      FieldName = 'dDtVenc'
    end
    object qryTitulosPendentesiDiasAtraso: TIntegerField
      DisplayLabel = 'T'#237'tulos em Aberto|Atraso'
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryTitulosPendentesnValTit: TBCDField
      DisplayLabel = 'T'#237'tulos em Aberto|Valor Original'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosPendentesnValLiq: TBCDField
      DisplayLabel = 'T'#237'tulos em Aberto|Valor Pago'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulosPendentesnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulosPendentesnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos em Aberto|Saldo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosPendentescFlgAtraso: TIntegerField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
    end
  end
  object dsTitulosPendentes: TDataSource
    DataSet = qryTitulosPendentes
    Left = 376
    Top = 141
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 296
    Top = 144
  end
  object cmd: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTerceiroEnvioCobradora'#39') IS NULL)'#13#10'B' +
      'EGIN'#13#10#13#10'    CREATE TABLE #TempTerceiroEnvioCobradora (nCdTerceir' +
      'o int'#13#10#9#9'                                         ,nCdTitulo   i' +
      'nt)'#13#10#13#10'END'#13#10#13#10'TRUNCATE TABLE #TempTerceiroEnvioCobradora '
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 376
    Top = 176
  end
  object SP_REGISTRA_TITULO_COBRADORA_MANUAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRA_TITULO_COBRADORA_MANUAL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 172
  end
end
