unit fTipoSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxControls, cxContainer,
  cxEdit, cxCheckBox, cxDBEdit, DBCtrlsEh, ImgList, cxPC, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmTipoSP = class(TfrmCadastro_Padrao)
    qryMasternCdTipoSP: TIntegerField;
    qryMastercNmTipoSP: TStringField;
    qryMastercFlgTipoSP: TStringField;
    qryMastercFlgExigeNF: TIntegerField;
    qryMastercFlgImposto: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryMastercNmEspTit: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label5: TLabel;
    qryUsuarioTipoSP: TADOQuery;
    dsUsuarioTipoSP: TDataSource;
    qryUsuarioTipoSPnCdTipoSP: TIntegerField;
    qryUsuarioTipoSPnCdUsuario: TIntegerField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmLogin: TStringField;
    qryUsuariocSenha: TStringField;
    qryUsuariocFlgAtivo: TSmallintField;
    qryUsuariodDtUltAcesso: TDateTimeField;
    qryUsuariodDtUltAltSenha: TDateTimeField;
    qryUsuarionCdEmpPadrao: TIntegerField;
    qryUsuarionCdStatus: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuariocCPF: TStringField;
    qryUsuarionCdTerceiroResponsavel: TIntegerField;
    qryUsuariocFlagMaster: TSmallintField;
    qryUsuariocAcessoWERP: TSmallintField;
    qryUsuariocTrocaSenhaProxLogin: TSmallintField;
    qryUsuarioTipoSPcNmUsuario: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoSPnCdUsuarioTipoSP: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    qryMasternCdTipoAdiantamento: TIntegerField;
    qryTipoAdiantamento: TADOQuery;
    qryTipoAdiantamentonCdTipoAdiantamento: TIntegerField;
    qryTipoAdiantamentocNmTipoAdiantamento: TStringField;
    qryTipoAdiantamentonCdEspTitAdiant: TIntegerField;
    qryTipoAdiantamentonCdPlanoContaSaida: TIntegerField;
    qryTipoAdiantamentonCdPlanoContaEntrada: TIntegerField;
    dsTipoAdiantamento: TDataSource;
    qryMastercHistoricoPadrao: TStringField;
    Label7: TLabel;
    DBEdit8: TDBEdit;
    dsEspTit: TDataSource;
    DBEdit4: TDBEdit;
    qryAux: TADOQuery;
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioTipoSPBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoSP: TfrmTipoSP;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoSP.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmTipoSP.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (qryMastercNmTipoSP.Value = '') Then
  begin
      MensagemAlerta('Informe a descri��o do tipo da SP.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  If (qryMastercFlgTipoSP.Value <> 'A') and (qryMastercFlgTipoSP.Value <> 'P') Then
  begin
      MensagemAlerta('Tipo de SP inv�lido.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  If (qryMasternCdEspTit.Value = 0) or (DBEdit4.Text = '') Then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (qryEspTitcFlgPrev.Value = 1) then
  begin
      MensagemAlerta('Esp�cie de t�tulo provisionado n�o permitido para o tipo de SP.');
      Abort;
  end;

  If (qryMastercFlgTipoSP.Value = 'A') and  (DBEdit6.Text = '') Then
  begin
      MensagemAlerta('Informe o tipo de adiantamento.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  If (qryMastercFlgTipoSP.Value <> 'A') and  (DBEdit6.Text <> '') Then
  begin
      MensagemAlerta('Tipo de adiantamento n�o permitido para o tipo de SP.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  If (qryMastercFlgTipoSP.Value = 'A') and  (DBCheckBox2.Checked) Then
  begin
      MensagemAlerta('SP de adiantamento n�o permitido para pagamento de impostos.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  inherited;
end;

procedure TfrmTipoSP.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cTipoMov = ' + Chr(39) + 'P' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
                DBEdit6.SetFocus;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoSP.qryUsuarioTipoSPBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT 1                                                  ');
  qryAux.SQL.Add('  FROM UsuarioTipoSP                                      ');
  qryAux.SQL.Add(' WHERE nCdUsuario = ' + qryUsuarioTipoSPnCdUsuario.AsString);
  qryAux.SQL.Add('   AND nCdTipoSP  = ' + qryMasternCdTipoSP.AsString        );
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuarioTipoSPcNmUsuario.Value + ' j� vinculado a este Tipo de SP.');
      Abort;
  end;

  qryUsuarioTipoSPnCdTipoSP.Value := qryMaster.FieldList[0].Value ;

  if (qryUsuarioTipoSP.State = dsInsert) then
      qryUsuarioTipoSPnCdUsuarioTipoSP.Value := frmMenu.fnProximoCodigo('USUARIOTIPOSP') ;
      
  inherited;

end;

procedure TfrmTipoSP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioTipoSP.Close ;
  qryUsuarioTipoSP.Parameters.ParamByName('nCdTipoSP').Value := qryMasternCdTipoSP.Value ;
  qryUsuarioTipoSP.Open ;

  qryTipoAdiantamento.Close;
  PosicionaQuery(qryTipoAdiantamento,qryMasternCdTipoAdiantamento.AsString);
  qryEspTit.Close;
  PosicionaQuery(qryEspTit,qryMasternCdEspTit.AsString);


end;

procedure TfrmTipoSP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioTipoSP.Close ;
  qryTipoAdiantamento.Close;
  qryEspTit.Close;
end;

procedure TfrmTipoSP.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTipoSP.State = dsBrowse) then
        begin
            if (qryUsuarioTipoSPnCdUsuario.Value <> null) then
                qryUsuarioTipoSP.Edit
            else
                qryUsuarioTipoSP.Insert ;
                
        end ;

        if (qryUsuarioTipoSP.State = dsInsert) or (qryUsuarioTipoSP.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTipoSPnCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoSP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOSP' ;
  nCdTabelaSistema  := 8 ;
  nCdConsultaPadrao := 11 ;

end;

procedure TfrmTipoSP.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if qryMastercFlgTipoSP.Value = 'A' then
        begin
            if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(1002);

                If (nPK > 0) then
                begin
                    qryMasternCdTipoAdiantamento.Value := nPK ;
                end ;

            end ;
        end;

    end ;

  end ;

end;

procedure TfrmTipoSP.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTipoAdiantamento.Close;
  if qryMastercFlgTipoSP.Value = 'A' then
  begin
      PosicionaQuery(qryTipoAdiantamento,DBEdit6.Text);
  end;
end;

procedure TfrmTipoSP.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsEdit) or (qryMaster.State = dsInsert) then
  begin
      qryEspTit.Close;
      PosicionaQuery(qryEspTit,qryMasternCdEspTit.AsString);
  end;
end;

initialization
    RegisterClass(TfrmTipoSP) ;

end.
