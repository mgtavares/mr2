unit rBorderoCaixa_view;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, jpeg;

type
  TrptBorderoCaixa_view = class(TQuickRep)
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    lblEmpresa: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel3: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    lblCaixa: TQRLabel;
    lblLoja: TQRLabel;
    lblInicial: TQRLabel;
    lblFinal: TQRLabel;
    SPREL_BORDERO_CAIXA: TADOStoredProc;
    qryTemp_Resumo_Vendas: TADOQuery;
    qryTemp_Resumo_VendascNmTabTipoFormaPagto: TStringField;
    qryTemp_Resumo_VendasnValPagto: TBCDField;
    QRSubDetail1: TQRSubDetail;
    QRGroup1: TQRGroup;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRBand2: TQRBand;
    QRLabel8: TQRLabel;
    qryTemp_Resumo_Recebimento: TADOQuery;
    qryTemp_Resumo_RecebimentocNmTabTipoFormaPagto: TStringField;
    qryTemp_Resumo_RecebimentonValPagto: TBCDField;
    QRSubDetail2: TQRSubDetail;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand3: TQRBand;
    QRLabel10: TQRLabel;
    lblTotReceb: TQRLabel;
    lblTotVenda: TQRLabel;
    QRGroup2: TQRGroup;
    qryTemp_Resumo_Lanctos: TADOQuery;
    qryTemp_Resumo_LanctosdDtLancto: TDateTimeField;
    qryTemp_Resumo_LanctoscNmTipoLancto: TStringField;
    qryTemp_Resumo_LanctoscNmTabTipoFormaPagto: TStringField;
    qryTemp_Resumo_LanctosnValLancto: TBCDField;
    qryTemp_Resumo_LanctosnCdConta: TStringField;
    qryTemp_Resumo_LanctoscNmLoja: TStringField;
    QRSubDetail3: TQRSubDetail;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRGroup3: TQRGroup;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRBand4: TQRBand;
    lblTotLancto: TQRLabel;
    QRLabel18: TQRLabel;
    qryTemp_Resumo_Caixa: TADOQuery;
    QRSubDetail4: TQRSubDetail;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    qryTemp_Resumo_CaixacNmTabTipoFormaPagto: TStringField;
    qryTemp_Resumo_CaixanValTotal: TBCDField;
    qryTemp_Resumo_CaixanValInformado: TBCDField;
    qryTemp_Resumo_CaixanValDiferenca: TBCDField;
    QRGroup4: TQRGroup;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRBand5: TQRBand;
    QRLabel24: TQRLabel;
    lblTotalMov: TQRLabel;
    QRLabel25: TQRLabel;
    lblUsuario: TQRLabel;
    QRBand6: TQRBand;
    qryTemp_Resumo_LanctoscMotivoEstorno: TStringField;
    QRDBText15: TQRDBText;
    QRLabel27: TQRLabel;
    QRLabel9: TQRLabel;
    qryTemp_Resumo_LanctosnCdLanctoFin: TIntegerField;
    qryTemp_Resumo_LanctosnCdLanctoFinPai: TIntegerField;
    QRLabel17: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText16: TQRDBText;
    QRShape3: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel6: TQRLabel;
    QRImage1: TQRImage;
    QRShape5: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    qryPreparaTemp: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRBand3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText11Print(sender: TObject; var Value: String);
  private

      nValTotalVenda, nValTotalReceb, nValTotalLancto, nValTotalResumo : double ;

  public

  end;

var
  rptBorderoCaixa_view: TrptBorderoCaixa_view;

implementation

{$R *.DFM}

procedure TrptBorderoCaixa_view.QRDBText3Print(sender: TObject;
  var Value: String);
begin

    nValTotalReceb := nValTotalReceb + qryTemp_Resumo_RecebimentonValPagto.Value ;

    lblTotReceb.Caption := FormatCurr('#,##0.00',nValTotalReceb) ;

end;

procedure TrptBorderoCaixa_view.QRDBText2Print(sender: TObject;
  var Value: String);
begin
    nValTotalVenda := nValTotalVenda + qryTemp_Resumo_VendasnValPagto.Value ;

    lblTotVenda.Caption := FormatCurr('#,##0.00',nValTotalVenda) ;

end;

procedure TrptBorderoCaixa_view.QRBand3AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
    nValTotalReceb := 0 ;
    nValTotalVenda := 0 ;
    nValTotalLancto:= 0 ;
    nValTotalResumo:= 0 ;
end;

procedure TrptBorderoCaixa_view.QRDBText9Print(sender: TObject;
  var Value: String);
begin

    nValTotalLancto := nValTotalLancto + qryTemp_Resumo_LanctosnValLancto.Value ;

    lblTotLancto.Caption := formatCurr('#,##0.00',nValTotalLancto) ;

end;

procedure TrptBorderoCaixa_view.QRDBText11Print(sender: TObject;
  var Value: String);
begin

    nValTotalResumo := nValTotalResumo + qryTemp_Resumo_CaixanValTotal.Value ;

    lblTotalMov.Caption := formatCurr('#,##0.00',nValTotalResumo) ;


end;

end.


