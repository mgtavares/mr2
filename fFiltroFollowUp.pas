unit fFiltroFollowUp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, Mask, StdCtrls, DBCtrls, ER2Lookup;

type
  TfrmFiltroFollowUp = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit5: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsEmpresa: TDataSource;
    dsTipoPedido: TDataSource;
    dsTerceiro: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    dsMarca: TDataSource;
    MaskEdit9: TMaskEdit;
    Label4: TLabel;
    MaskEdit8: TMaskEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label9: TLabel;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    DBEdit6: TDBEdit;
    dsCC: TDataSource;
    Label11: TLabel;
    edtCdProduto: TER2LookupMaskEdit;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit7: TDBEdit;
    dsProduto: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton4Click(Sender: TObject);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFiltroFollowUp: TfrmFiltroFollowUp;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmFiltroFollowUp.FormShow(Sender: TObject);
begin
  inherited;

  if not qryEmpresa.Active then
  begin
      MaskEdit1.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      PosicionaQuery(qryEmpresa, MaskEdit1.text) ;
  end ;

  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  MaskEdit2.SetFocus;

end;

procedure TfrmFiltroFollowUp.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close;

  PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, MaskEdit5.Text) ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmFiltroFollowUp.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmFiltroFollowUp.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(102);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmFiltroFollowUp.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            MaskEdit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmFiltroFollowUp.ToolButton4Click(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  qryTerceiro.Close ;
  qryTipoPedido.Close ;
  qryMarca.Close ;
  qryCC.Close ;
  qryProduto.Close;

  MaskEdit2.Text    := '' ;
  MaskEdit3.Text    := '' ;
  MaskEdit4.Text    := '' ;
  MaskEdit5.Text    := '' ;
  MaskEdit6.Text    := '' ;
  MaskEdit7.Text    := '' ;
  MaskEdit8.Text    := '' ;
  MaskEdit9.Text    := '' ;
  MaskEdit10.Text   := '' ;
  edtCdProduto.Text := '' ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, MaskEdit10.Text) ;
  
end;

procedure TfrmFiltroFollowUp.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(29);

        If (nPK > 0) then
        begin
            MaskEdit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCC, MaskEdit10.Text) ;
        end ;

    end ;

  end ;

end;

end.
