unit fOperManEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmOperManEstoque = class(TfrmCadastro_Padrao)
    qryMasternCdOperManEstoque: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdLocalEstoque: TIntegerField;
    qryMasternCdOperacaoEstoque: TIntegerField;
    qryMasternQtde: TBCDField;
    qryMasternCdUsuario: TIntegerField;
    qryMasterdDtOperacao: TDateTimeField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBMemo1: TDBMemo;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryOperacaoEstoque: TADOQuery;
    qryOperacaoEstoquenCdOperacaoEstoque: TIntegerField;
    qryOperacaoEstoquecNmOperacaoEstoque: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit8: TDBEdit;
    DataSource1: TDataSource;
    DBEdit9: TDBEdit;
    DataSource2: TDataSource;
    DBEdit10: TDBEdit;
    DataSource3: TDataSource;
    DBEdit11: TDBEdit;
    DataSource4: TDataSource;
    SP_PROCESSA_AJUSTE_ESTOQUE: TADOStoredProc;
    btRegistroLoteSerial: TcxButton;
    qryOperacaoEstoquecSinalOper: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMasterdDtProcesso: TDateTimeField;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProdutoExterno : integer ;
  end;

var
  frmOperManEstoque: TfrmOperManEstoque;

implementation

uses fMenu, fLookup_Padrao, fRegistroMovLote;

{$R *.dfm}

procedure TfrmOperManEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  
  cNmTabelaMaster   := 'OPERMANESTOQUE' ;
  nCdTabelaSistema  := 61 ;
  nCdConsultaPadrao := 135 ;
  bLimpaAposSalvar  := False;

end;

procedure TfrmOperManEstoque.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmOperManEstoque.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, Dbedit2.Text) ;
  
end;

procedure TfrmOperManEstoque.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  PosicionaQuery(qryLocalEstoque, DbEdit3.Text) ;
  
end;

procedure TfrmOperManEstoque.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryOperacaoEstoque.Close ;
  PosicionaQuery(qryOperacaoEstoque, DBEdit4.Text) ;
  
end;

procedure TfrmOperManEstoque.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto)' );

            If (nPK > 0) then
            begin
                qryMasternCdProduto.Value := nPK ;
                PosicionaQuery(qryProduto, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperManEstoque.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoque.Value := nPK ;
                qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
                PosicionaQuery(qryLocalEstoque, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperManEstoque.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(84,'cFlgPermManual = 1');

            If (nPK > 0) then
            begin
                qryMasternCdOperacaoEstoque.Value := nPK ;
                PosicionaQuery(qryOperacaoEstoque, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperManEstoque.btSalvarClick(Sender: TObject);
begin
  inherited;

  if (dsMaster.State <> dsInsert) then
  begin
      MensagemAlerta('Este movimento j� foi processado, imposs�vel alterar.') ;
      abort ;
  end ;

end;

procedure TfrmOperManEstoque.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close ;
  qryLocalEstoque.Close ;
  qryOperacaoEstoque.Close ;
  qryUsuario.Close ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  PosicionaQuery(qryProduto, qryMasternCdProduto.AsString) ;
  PosicionaQuery(qryLocalEstoque, qryMasternCdLocalEstoque.AsString) ;
  PosicionaQuery(qryOperacaoEstoque, qryMasternCdOperacaoEstoque.AsString) ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;

  btSalvar.Enabled := False ;
end;

procedure TfrmOperManEstoque.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (DbEdit2.Text = '') or (DbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (DbEdit3.Text = '') or (Dbedit9.Text = '') then
  begin
      MensagemAlerta('Informe o local de estoque.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DbEdit4.Text = '') or (Dbedit10.Text = '') then
  begin
      MensagemAlerta('Informe o opera��o de estoque.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (DbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a quantidade.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  inherited;
  
  qryMasternCdUsuario.Value  := frmMenu.nCdUsuarioLogado;
  qryMasterdDtOperacao.Value := Now() ;
  qryMasternCdEmpresa.Value  := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmOperManEstoque.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if ( nCdProdutoExterno > 0 ) then
  begin

      btIncluir.Click;
      qryMasternCdProduto.Value := nCdProdutoExterno ;
      PosicionaQuery( qryProduto , intToStr( nCdProdutoExterno ) ) ;

  end ;

end;

procedure TfrmOperManEstoque.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close ;
  qryLocalEstoque.Close ;
  qryOperacaoEstoque.Close ;
  qryUsuario.Close ;
  
end;

procedure TfrmOperManEstoque.btRegistroLoteSerialClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  if (qryMaster.State = dsInsert) and (qryMasternCdOperManEstoque.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum Registro Ativo.') ;
      abort ;
  end ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  {-- opera��o de entrada --}
  if (qryOperacaoEstoquecSinalOper.Value = '+') then
  begin

      objForm.registraMovLote( qryMasternCdProduto.Value
                             , 0
                             , 9 {-- Ajuste de Entrada --}
                             , qryMasternCdOperManEstoque.Value
                             , qryMasternCdLocalEstoque.Value
                             , qryMasternQtde.Value
                             , 'AJUSTE MANUAL ESTOQUE No. ' + Trim( qryMasternCdOperManEstoque.asString )
                             , TRUE
                             , (qryMasterdDtProcesso.asString <> '')
                             , FALSE) ;

  end
  else {-- opera��o de sa�da --}
  begin

      objForm.registraMovLote( qryMasternCdProduto.Value
                             , 0
                             , 10 {-- Ajuste de Sa�da --}
                             , qryMasternCdOperManEstoque.Value
                             , qryMasternCdLocalEstoque.Value
                             , qryMasternQtde.Value
                             , 'AJUSTE MANUAL ESTOQUE No. ' + Trim( qryMasternCdOperManEstoque.asString )
                             , FALSE
                             , (qryMasterdDtProcesso.asString <> '')
                             , FALSE) ;

  end ;

  freeAndNil( objForm ) ;

end;

procedure TfrmOperManEstoque.ToolButton10Click(Sender: TObject);
var
  objForm   : TfrmRegistroMovLote ;
  nQtdeLote : double ;
begin
  inherited;

  if (qryMaster.State = dsInsert) and (qryMasternCdOperManEstoque.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum Registro Ativo.') ;
      abort ;
  end ;

  if (qryMasterdDtProcesso.asString <> '') then
  begin
      MensagemAlerta('Esta transa��o j� foi processada.') ;
      abort ;
  end ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  try

      {-- opera��o de entrada --}
      if (qryOperacaoEstoquecSinalOper.Value = '+') then
      begin

          nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryMasternCdProduto.Value
                                                                    , 9
                                                                    , qryMasternCdOperManEstoque.Value ) ;

      end
      else
      begin

          nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryMasternCdProduto.Value
                                                                    , 10
                                                                    , qryMasternCdOperManEstoque.Value ) ;

      end ;

      {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
      if (nQtdeLote <> -1) then
      begin

          if ( nQtdeLote <> qryMasternQtde.Value ) then
          begin
              MensagemAlerta('A quantidade especificada no Registro de Lote/Serial n�o confere com o total deste movimento.') ;
              btRegistroLoteSerial.Click;
              abort ;
          end ;

      end ;

  finally
      freeAndNil( objForm ) ;
  end ;

  case MessageDlg('Est� opera��o ir� gerar uma movimenta��o no estoque em seu nome. ' +#13#13 + 'Este processo n�o poder� ser desfeito. Confirma mesmo assim ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_AJUSTE_ESTOQUE.Close ;
      SP_PROCESSA_AJUSTE_ESTOQUE.Parameters.ParamByName('@nCdOperManEstoque').Value := qryMasternCdOperManEstoque.Value;
      SP_PROCESSA_AJUSTE_ESTOQUE.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_PROCESSA_AJUSTE_ESTOQUE.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Transa��o processada com sucesso!') ;

  btCancelar.Click;

end;

initialization
    RegisterClass(TfrmOperManEstoque);

end.
