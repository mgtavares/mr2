unit fMovTitulo_Abatimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit,
  StdCtrls, Mask, DBCtrls;

type
  TfrmMovTitulo_Abatimento = class(TfrmProcesso_Padrao)
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    edtValor: TcxCurrencyEdit;
    Label1: TLabel;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTitulonValTit: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulocNrNF: TStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    SP_ABATIMENTO_TITULO: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro       : integer ;
    nCdTitulo         : integer ;
    nSaldoTit         : double  ;
    nCdGrupoEconomico : integer ;
  end;

var
  frmMovTitulo_Abatimento: TfrmMovTitulo_Abatimento;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmMovTitulo_Abatimento.FormShow(Sender: TObject);
begin
  inherited;

  qryTitulo.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva;
  qryTitulo.Parameters.ParamByName('nCdTerceiro').Value       := nCdTerceiro            ;
  qryTitulo.Parameters.ParamByName('nCdGrupoEconomico').Value := nCdGrupoEconomico      ;

  MaskEdit6.Text := '' ;
  
  qryTitulo.Close ;
  edtValor.Value := 0 ;
  edtValor.SetFocus;

end;

procedure TfrmMovTitulo_Abatimento.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTitulo.Close ;
  PosicionaQuery(qryTitulo, MaskEdit6.Text) ;
end;

procedure TfrmMovTitulo_Abatimento.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(66,'Titulo.nCdTerceiro = ' + IntToStr(nCdTerceiro) + ' OR Terceiro.nCdGrupoEconomico = ' + IntToStr(nCdGrupoEconomico));

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTitulo, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMovTitulo_Abatimento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (frmMenu.TBRound(edtValor.Value,2) <= 0) then
  begin
      MensagemAlerta('Informe o valor do abatimento.') ;
      edtValor.SetFocus ;
      exit ;
  end ;

  if (frmMenu.TBRound(edtValor.Value,2) > frmMenu.TBRound(nSaldoTit,2)) then
  begin
      MensagemAlerta('O valor do abatimento � maior que o saldo do t�tulo a ser abatido.') ;
      exit ;
  end ;

  if (not qryTitulo.Active) then
  begin
      MensagemAlerta('Selecione um t�tulo para cr�dito do abatimento.' +#13#13 + 'O t�tulo deve pertencer ao mesmo terceiro do t�tulo a pagar ou ao mesmo grupo economico.') ;
      MaskEdit6.SetFocus;
      abort ;
  end ;

  if (frmMenu.TBRound(edtValor.Value,2) > frmMenu.TBRound(qryTitulonSaldoTit.Value,2)) then
  begin
      MensagemAlerta('O Valor do abatimento � maior que o saldo do t�tulo credit�rio.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o abatimento no saldo do t�tulo ?' +#13#13 + 'Este processo n�o poder� ser estornado.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_ABATIMENTO_TITULO.Close ;
      SP_ABATIMENTO_TITULO.Parameters.ParamByName('@nCdTituloAbat').Value       := nCdTitulo ;
      SP_ABATIMENTO_TITULO.Parameters.ParamByName('@nCdTituloCreditorio').Value := qryTitulonCdTitulo.Value ;
      SP_ABATIMENTO_TITULO.Parameters.ParamByName('@nValAbatimento').Value      := edtValor.Value ;
      SP_ABATIMENTO_TITULO.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
      SP_ABATIMENTO_TITULO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  close ;

end;

end.
