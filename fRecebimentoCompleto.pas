unit fRecebimentoCompleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, DBCtrlsEh, DBLookupEh, Menus,
  cxContainer, cxEdit, cxTextEdit, GIFImage, DBGridEhGrouping, ACBrNFe,pcnConversao,
  ACBrNFeDANFEClass, ToolCtrlsEh, ACBrNFeDANFeRLClass,
  ACBrBase, ACBrDFe;

type
  TfrmRecebimentoCompleto = class(TfrmCadastro_Padrao)
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoReceb: TIntegerField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNrDocto: TStringField;
    qryMastercSerieDocto: TStringField;
    qryMasterdDtDocto: TDateTimeField;
    qryMasternValDocto: TBCDField;
    qryMasternCdTabStatusReceb: TIntegerField;
    qryMasternCdUsuarioFech: TIntegerField;
    qryMasterdDtFech: TDateTimeField;
    qryMastercOBS: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    DBEdit15: TDBEdit;
    dsEmpresa: TDataSource;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    DBEdit16: TDBEdit;
    dsLoja: TDataSource;
    qryTipoReceb: TADOQuery;
    qryTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoRecebcNmTipoReceb: TStringField;
    qryTipoRecebcFlgExigeNF: TIntegerField;
    DBEdit17: TDBEdit;
    dsTipoReceb: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit18: TDBEdit;
    dsTerceiro: TDataSource;
    qryTabStatusReceb: TADOQuery;
    qryTabStatusRecebnCdTabStatusReceb: TIntegerField;
    qryTabStatusRecebcNmTabStatusReceb: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit19: TDBEdit;
    dsTabStatusReceb: TDataSource;
    DBEdit11: TDBEdit;
    dsUsuario: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryItemRecebimento: TADOQuery;
    qryItemRecebimentonCdItemRecebimento: TIntegerField;
    qryItemRecebimentonCdRecebimento: TIntegerField;
    qryItemRecebimentonCdItemRecebimentoPai: TIntegerField;
    qryItemRecebimentonCdTipoItemPed: TIntegerField;
    qryItemRecebimentonCdProduto: TIntegerField;
    qryItemRecebimentocNmItem: TStringField;
    qryItemRecebimentonCdItemPedido: TIntegerField;
    qryItemRecebimentonQtde: TBCDField;
    qryItemRecebimentonValTotal: TBCDField;
    qryItemRecebimentocEANFornec: TStringField;
    qryItemRecebimentonCdTabStatusItemPed: TIntegerField;
    qryItemRecebimentocFlgDiverg: TIntegerField;
    qryItemRecebimentonCdTabTipoDiverg: TIntegerField;
    qryItemRecebimentonCdUsuarioAutorDiverg: TIntegerField;
    qryItemRecebimentodDtAutorDiverg: TDateTimeField;
    qryItemRecebimentonCdPedido: TIntegerField;
    dsItemRecebimento: TDataSource;
    DBGridEh1: TDBGridEh;
    qryAux: TADOQuery;
    cxButton1: TcxButton;
    qryConsultaPedido: TADOQuery;
    qryTabTipoDiverg: TADOQuery;
    qryTabTipoDivergnCdTabTipoDiverg: TIntegerField;
    qryTabTipoDivergcNmTabTipoDiverg: TStringField;
    qryTabStatusItemPed: TADOQuery;
    qryTabStatusItemPednCdTabStatusItemPed: TIntegerField;
    qryTabStatusItemPedcNmTabStatusItemPed: TStringField;
    qryTabStatusItemPedcFlgFaturar: TIntegerField;
    qryItemRecebimentocNmTabStatusItemPed: TStringField;
    qryItemRecebimentocNmTabTipoDiverg: TStringField;
    cxTabSheet4: TcxTabSheet;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    dsItemBarra: TDataSource;
    qryPrazoRecebimento: TADOQuery;
    dsPrazoRecebimento: TDataSource;
    qryPrazoRecebimentonCdRecebimento: TIntegerField;
    qryPrazoRecebimentodDtVenc: TDateTimeField;
    qryPrazoRecebimentonValPagto: TBCDField;
    DBGridEh3: TDBGridEh;
    usp_Gera_SubItem: TADOStoredProc;
    usp_Grade: TADOStoredProc;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    cmdExcluiSubItem: TADOCommand;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdGrade: TIntegerField;
    qryProdutonCdTabTipoProduto: TIntegerField;
    qryProdutonCdProdutoPai: TIntegerField;
    SP_FINALIZA_RECEBIMENTO: TADOStoredProc;
    qryMasternValTotalNF: TBCDField;
    qryMasternValTitulo: TBCDField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdUsuarioAutorFinanc: TIntegerField;
    qryMasterdDtAutorFinanc: TDateTimeField;
    qryMastercCFOP: TStringField;
    qryMastercCNPJEmissor: TStringField;
    qryMastercIEEmissor: TStringField;
    qryMastercUFEmissor: TStringField;
    qryMastercModeloNF: TStringField;
    qryMasternValBaseICMS: TBCDField;
    qryMasternValICMS: TBCDField;
    qryMasternValIsenta: TBCDField;
    qryMasternValOutras: TBCDField;
    qryMasternValIPI: TBCDField;
    TabCentroCusto: TcxTabSheet;
    DBGridEh8: TDBGridEh;
    Image3: TImage;
    usp_copia_cc: TADOStoredProc;
    qryItemCC: TADOQuery;
    dsItemCC: TDataSource;
    qryCCItemRecebimento: TADOQuery;
    dsCCItemRecebimento: TDataSource;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryItemCCnCdItemRecebimento: TAutoIncField;
    qryItemCCcNmItem: TStringField;
    qryCCItemRecebimentonCdItemRecebimento: TIntegerField;
    qryCCItemRecebimentonCdCC: TIntegerField;
    qryCCItemRecebimentonPercent: TBCDField;
    qryCCItemRecebimentocNmCC: TStringField;
    qryMasternValTotalItensPag: TBCDField;
    qryMasternValDescontoNF: TBCDField;
    TabLote: TcxTabSheet;
    Image4: TImage;
    qryLoteItemRecebimento: TADOQuery;
    qryLoteItemRecebimentonCdLoteItemRecebimento: TAutoIncField;
    qryLoteItemRecebimentocNrLote: TStringField;
    qryLoteItemRecebimentodDtFabricacao: TDateTimeField;
    qryLoteItemRecebimentodDtValidade: TDateTimeField;
    qryLoteItemRecebimentonQtde: TBCDField;
    dsLoteItemRecebimento: TDataSource;
    qryProdLote: TADOQuery;
    qryProdLotenCdProduto: TIntegerField;
    qryProdLotecNmProduto: TStringField;
    qryProdLotenQtde: TBCDField;
    dsProdLote: TDataSource;
    qryLoteItemRecebimentonCdRecebimento: TIntegerField;
    qryLoteItemRecebimentonCdProduto: TIntegerField;
    qryItemPagavel: TADOQuery;
    SP_GERA_PARCELA_RECEBIMENTO: TADOStoredProc;
    qryMasterdDtContab: TDateTimeField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercFlgParcelaAutom: TIntegerField;
    qryMasternValICMSSub: TBCDField;
    qryMasternValDespesas: TBCDField;
    qryMasternValFrete: TBCDField;
    qryItemRecebimentonPercIPI: TBCDField;
    qryItemRecebimentonValIPI: TBCDField;
    qryItemRecebimentonPercICMSSub: TBCDField;
    qryItemRecebimentonValICMSSub: TBCDField;
    qryItemRecebimentonPercDesconto: TBCDField;
    qryItemRecebimentonValDesconto: TBCDField;
    qryPrazoRecebimentonCdTitulo: TIntegerField;
    qryPrazoRecebimentocNrTit: TStringField;
    qryPrazoRecebimentoiParcela: TIntegerField;
    qryPrazoRecebimentocFlgDocCobranca: TIntegerField;
    Label29: TLabel;
    DBEdit31: TDBEdit;
    Label30: TLabel;
    DBEdit32: TDBEdit;
    Label32: TLabel;
    DBEdit34: TDBEdit;
    Label34: TLabel;
    DBEdit36: TDBEdit;
    qryMasternPercDescProduto: TBCDField;
    qryConsultaPedidonCdItemPedido: TAutoIncField;
    qryConsultaPedidonValCustoUnit: TBCDField;
    qryConsultaPedidonValUnitarioEsp: TBCDField;
    qryConsultaPedidonPercIPI: TBCDField;
    qryConsultaPedidonPercICMSSub: TBCDField;
    SP_INSERI_DIVERGENCIA_RECEB: TADOStoredProc;
    qryItemRecebimentonPercIPIPed: TBCDField;
    qryItemRecebimentonPercICMSSubPed: TBCDField;
    PopupMenu1: TPopupMenu;
    ExibirDivergncias1: TMenuItem;
    ExcluirItem1: TMenuItem;
    cxButton2: TcxButton;
    Label37: TLabel;
    cxTextEdit2: TcxTextEdit;
    qryPrazoRecebimentonValParcela: TBCDField;
    qryPrazoRecebimentonValOperacaoFin: TBCDField;
    cxTabSheet5: TcxTabSheet;
    Image5: TImage;
    qryMastercFlgAnaliseQualidade: TIntegerField;
    qryMastercFlgAnaliseQualidadeOK: TIntegerField;
    qryMasterdDtAnaliseQualidade: TDateTimeField;
    qryMasternCdUsuarioAnaliseQualidade: TIntegerField;
    qryMastercOBSQualidade: TStringField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label38: TLabel;
    DBEdit38: TDBEdit;
    Label39: TLabel;
    DBEdit39: TDBEdit;
    Label40: TLabel;
    DBEdit40: TDBEdit;
    qryUsuarioQualidade: TADOQuery;
    qryUsuarioQualidadenCdUsuario: TIntegerField;
    qryUsuarioQualidadecNmUsuario: TStringField;
    DBEdit41: TDBEdit;
    qryTipoRecebcFlgPrecoMedio: TIntegerField;
    qryTipoRecebcFlgRecebCego: TIntegerField;
    qryTipoRecebcFlgAguardConfirm: TIntegerField;
    qryGradeRecebimento: TADOQuery;
    qryInseriItemGrade: TADOQuery;
    qryItemRecebimentonValTotalLiq: TFloatField;
    cxButton3: TcxButton;
    qryPrazoRecebimentoiQtdeDias: TIntegerField;
    qryPrazoRecebimentocFlgDiverg: TIntegerField;
    SP_INSERI_DIVERGENCIA_PARCELA_RECEB: TADOStoredProc;
    qryTesteDupNF: TADOQuery;
    qryTesteDupNFnCdRecebimento: TIntegerField;
    cxButton5: TcxButton;
    qryProdutocReferencia: TStringField;
    qryConsultaPedidonValUnitario: TBCDField;
    qryItemRecebimentocFlgDescAbatUnitario: TIntegerField;
    qryItemRecebimentonValDescAbatUnit: TBCDField;
    cxTextEdit1: TcxTextEdit;
    Label20: TLabel;
    qryCalculaST: TADOQuery;
    qryCalculaSTnValICMSSub: TBCDField;
    qryCalculaSTnPercICMSSub: TBCDField;
    qryItemRecebimentonValUnitario: TFloatField;
    qryItemRecebimentonValUnitarioEsp: TFloatField;
    qryItemRecebimentonValUnitarioPed: TFloatField;
    qryItemRecebimentonValCustoFinal: TFloatField;
    qryItemPagavelnValTotal: TFloatField;
    qryTipoRecebnCdQuestionarioCQM: TIntegerField;
    qryTipoRecebcFlgExigeCQM: TIntegerField;
    qryTipoRecebcFlgDivValor: TIntegerField;
    qryTipoRecebcFlgDivIPI: TIntegerField;
    qryTipoRecebcFlgDivICMSSub: TIntegerField;
    qryTipoRecebcFlgDivPrazoEntrega: TIntegerField;
    qryTipoRecebcFlgDivPrazoPagto: TIntegerField;
    qryTipoRecebcFlgDivGrade: TIntegerField;
    qryPrazoRecebimentocFlgAutomatica: TIntegerField;
    qryPrazoRecebimentocCodBarra: TStringField;
    qryItemEtiqueta: TADOQuery;
    qryItemEtiquetanCdProduto: TIntegerField;
    qryItemEtiquetanQtde: TBCDField;
    qryPrazoRecebimentonCdPrazoRecebimento: TIntegerField;
    qryCCItemRecebimentonCdCentroCustoItemRecebimento: TAutoIncField;
    DBEdit24: TDBEdit;
    Label25: TLabel;
    VincularPedido1: TMenuItem;
    SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO: TADOStoredProc;
    qryTipoRecebcFlgExigeNFe: TIntegerField;
    qryMastercChaveNFe: TStringField;
    qryItemRecebimentonCdServidorOrigem: TIntegerField;
    qryItemRecebimentodDtReplicacao: TDateTimeField;
    qryItemRecebimentonPercRedBaseCalcICMS: TFloatField;
    qryItemRecebimentonBaseCalcICMS: TFloatField;
    qryItemRecebimentonPercRedBaseCalcSubTrib: TFloatField;
    qryItemRecebimentonPercAliqICMS: TFloatField;
    qryItemRecebimentonValSeguro: TFloatField;
    qryItemRecebimentonValICMS: TFloatField;
    qryItemRecebimentonValDifAliqICMS: TFloatField;
    qryItemRecebimentonValFrete: TFloatField;
    qryItemRecebimentonValAcessorias: TFloatField;
    qryItemRecebimentocCdProduto: TStringField;
    qryItemRecebimentocCdST: TStringField;
    qryItemRecebimentonCdTipoICMS: TIntegerField;
    qryItemRecebimentonCdTipoIPI: TIntegerField;
    qryItemRecebimentocFlgImportacao: TIntegerField;
    qryCfop: TADOQuery;
    qryCfopnCdCFOP: TAutoIncField;
    qryCfopcCFOP: TStringField;
    qryCfopcCFOPPai: TStringField;
    qryCfopcNmCFOP: TStringField;
    qryCfopcFlgGeraLivroFiscal: TIntegerField;
    qryCfopcFlgGeraCreditoICMS: TIntegerField;
    qryCfopcFlgGeraCreditoIPI: TIntegerField;
    qryCfopcFlgGeraCreditoPIS: TIntegerField;
    qryCfopcFlgGeraCreditoCOFINS: TIntegerField;
    qryCfopcFlgImportacao: TIntegerField;
    qryCfopnCdTipoICMS: TIntegerField;
    qryCfopnCdTipoIPI: TIntegerField;
    qryItemRecebimentocFlgGeraLivroFiscal: TIntegerField;
    qryItemRecebimentocCFOP: TStringField;
    qryItemRecebimentonValBaseCalcSubTrib: TFloatField;
    qryTipoRecebcFlgAtuPrecoVenda: TIntegerField;
    qryTipoRecebnCdServidorOrigem: TIntegerField;
    qryTipoRecebdDtReplicacao: TDateTimeField;
    qryTipoRecebcFlgPermiteItemAD: TIntegerField;
    qryTipoRecebcFlgExigeInformacoesFiscais: TIntegerField;
    qryEnderecoEmpresa: TADOQuery;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEnderecoEmpresanCdTerceiro: TIntegerField;
    qryEnderecoEmpresacUF: TStringField;
    qryTipoTributacaoICMS: TADOQuery;
    qryItemRecebimentocFlgGeraCreditoICMS: TIntegerField;
    qryItemRecebimentocFlgGeraCreditoIPI: TIntegerField;
    qryItemRecebimentonValAliquotaII: TFloatField;
    Label33: TLabel;
    DBEdit33: TDBEdit;
    Label35: TLabel;
    DBEdit35: TDBEdit;
    Label36: TLabel;
    DBEdit37: TDBEdit;
    Label41: TLabel;
    DBEdit42: TDBEdit;
    Label42: TLabel;
    DBEdit43: TDBEdit;
    qryMasternValSeguroNF: TBCDField;
    qryMasternPercFreteNF: TBCDField;
    qryMasternPercSeguroNF: TBCDField;
    qryMasternPercDespesaNF: TBCDField;
    qryMasternValBaseCalcICMSSub: TBCDField;
    qryItemRecebimentocNmTipoICMS: TStringField;
    qryItemRecebimentocNmTipoIPI: TStringField;
    Label19: TLabel;
    DBEdit23: TDBEdit;
    Label27: TLabel;
    DBEdit29: TDBEdit;
    qryTipoTributacaoICMSnCdTipoTributacaoICMS: TAutoIncField;
    qryTipoTributacaoICMScCdST: TStringField;
    qryTipoTributacaoICMScNmTipoTributacaoICMS: TStringField;
    qryTipoTributacaoICMS_Aux: TADOQuery;
    qryTipoTributacaoICMS_AuxnCdTipoTributacaoICMS: TAutoIncField;
    qryTipoTributacaoICMS_AuxcCdST: TStringField;
    qryTipoTributacaoICMS_AuxcNmTipoTributacaoICMS: TStringField;
    DBGridEh4: TDBGridEh;
    qryProdLotenCdItemRecebimento: TIntegerField;
    cxButton7: TcxButton;
    OpenDialog1: TOpenDialog;
    qryTerceirocCnpjCpf: TStringField;
    btRegistroLoteSerial: TcxButton;
    qryProdutoFornecedor: TADOQuery;
    qryProdutoFornecedornPercIPI: TBCDField;
    qryProdutoFornecedornPercIVA: TBCDField;
    qryProdutoFornecedornPercBCICMSSub: TBCDField;
    qryProdutoFornecedornPercICMSSub: TBCDField;
    qryTerceirocFlgOptSimples: TIntegerField;
    qryMastercUFOrigemNF: TStringField;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    qryItemRecebimentonAliqICMSInterna: TFloatField;
    qryItemRecebimentonPercIVA: TFloatField;
    qryChecaEnderecoUFTerceiro: TADOQuery;
    qryItemRecebimentonPercBaseCalcIPI: TFloatField;
    qryItemRecebimentonValBaseIPI: TFloatField;
    qryItemRecebimentocNCM: TStringField;
    qryItemRecebimentocCFOPNF: TStringField;
    qryItemRecebimentocCdSTIPI: TStringField;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostocNCM: TStringField;
    qryItemRecebimentocCSTPIS: TStringField;
    qryItemRecebimentonValBasePIS: TFloatField;
    qryItemRecebimentonAliqPIS: TFloatField;
    qryItemRecebimentonValPIS: TFloatField;
    qryItemRecebimentocCSTCOFINS: TStringField;
    qryItemRecebimentonValBaseCOFINS: TFloatField;
    qryItemRecebimentonAliqCOFINS: TFloatField;
    qryItemRecebimentonValCOFINS: TFloatField;
    qryItemRecebimentonValBaseII: TFloatField;
    qryItemRecebimentonValDespAdu: TFloatField;
    qryItemRecebimentonValII: TFloatField;
    qryItemRecebimentonValIOFImp: TFloatField;
    qryItemRecebimentocFlgGeraCreditoPIS: TIntegerField;
    qryItemRecebimentocFlgGeraCreditoCOFINS: TIntegerField;
    qryMasternValPIS: TBCDField;
    qryMasternValCOFINS: TBCDField;
    qryMasternValII: TBCDField;
    Label16: TLabel;
    DBEdit20: TDBEdit;
    Label17: TLabel;
    DBEdit21: TDBEdit;
    Label18: TLabel;
    DBEdit22: TDBEdit;
    Label21: TLabel;
    qryBuscaTerceiroCNPJ: TADOQuery;
    qryBuscaTerceiroCNPJnCdTerceiro: TIntegerField;
    qryItemRecebimentocCdSTIPIEntrada: TStringField;
    qryCST_IPI: TADOQuery;
    qryCST_IPInCdTipoTributacaoIPI: TIntegerField;
    qryCST_IPIcCdStIPI: TStringField;
    qryCST_IPIcNmTipoTributacaoIPI: TStringField;
    qryCST_PISCOFINS: TADOQuery;
    qryCST_PISCOFINSnCdTipoTributacaoPISCOFINS: TIntegerField;
    qryCST_PISCOFINScCdStPISCOFINS: TStringField;
    qryCST_PISCOFINScNmTipoTributacaoPISCOFINS: TStringField;
    qryItemRecebimentocUnidadeMedida: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryUnidadeMedida: TADOQuery;
    qryUnidadeMedidanCdUnidadeMedida: TAutoIncField;
    RemoverVnculoPedido1: TMenuItem;
    N1: TMenuItem;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    PopupMenu2: TPopupMenu;
    Protocolo1: TMenuItem;
    N3: TMenuItem;
    qryMasternCdTabTipoRecebimento: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryItemRecebidoSemPedido: TADOQuery;
    qryItemRecebidoSemPedidonCdItemRecebimento: TIntegerField;
    qryItemRecebidoSemPedidocCdProduto: TStringField;
    qryItemRecebidoSemPedidocNmItem: TStringField;
    qryItemRecebidoSemPedidonQtde: TBCDField;
    qryItemRecebidoSemPedidonCdTipoPedidoRecebido: TIntegerField;
    qryItemRecebidoSemPedidocNmTipoPedido: TStringField;
    dsItemRecebidoSemPedido: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    GroupBox1: TGroupBox;
    Label26: TLabel;
    DBComboItens: TDBLookupComboboxEh;
    cxButton4: TcxButton;
    cxTabSheet3: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    qryResumoDivergencia: TADOQuery;
    qryResumoDivergenciacCdProduto: TStringField;
    qryResumoDivergenciacNmItem: TStringField;
    qryResumoDivergencianCdPedido: TIntegerField;
    qryResumoDivergencianCdItemPedido: TIntegerField;
    qryResumoDivergencianCdTabTipoDiverg: TIntegerField;
    qryResumoDivergenciacNmTabTipoDiverg: TStringField;
    qryResumoDivergenciacNmUsuario: TStringField;
    qryResumoDivergenciadDtAutorizacao: TDateTimeField;
    qryResumoDivergenciacFlgAutomatica: TStringField;
    qryResumoDivergenciacOBS: TStringField;
    dsResumoDivergencia: TDataSource;
    ImportarXMLNFe1: TMenuItem;
    N2: TMenuItem;
    ProdutoERP1: TMenuItem;
    ProdutoGrade1: TMenuItem;
    VisualizarNFe1: TMenuItem;
    EmitirEtiquetaCdigoBarra1: TMenuItem;
    qryMastercXMLNFe: TMemoField;
    qryTipoRecebcFlgEscriturarEntrada: TIntegerField;
    qryTipoRecebcFlgDivSemPedido: TIntegerField;
    qryTipoRecebcFlgExigeImportacaoXMLNFe: TIntegerField;
    dsUsuarioQualidade: TDataSource;
    qryNaturezaOperacao: TADOQuery;
    qryNaturezaOperacaocCFOP: TStringField;
    ACBrNFe1: TACBrNFe;
    ACBrNFe2: TACBrNFe;
    ACBrNFeDANFeRL1: TACBrNFeDANFeRL;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemRecebimentoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure qryPrazoRecebimentoBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemRecebimentoAfterPost(DataSet: TDataSet);
    procedure qryItemRecebimentoBeforeDelete(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryCCItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure qryCCItemRecebimentoCalcFields(DataSet: TDataSet);
    procedure cxButton4Click(Sender: TObject);
    procedure DBGridEh8KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxPageControl1Change(Sender: TObject);
    procedure DBComboItensChange(Sender: TObject);
    procedure qryLoteItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure DBComboProdutosChange(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExibirDivergncias1Click(Sender: TObject);
    procedure ExcluirItem1Click(Sender: TObject);
    procedure qryTipoRecebAfterScroll(DataSet: TDataSet);
    procedure usp_GradeAfterPost(DataSet: TDataSet);
    procedure usp_GradeAfterCancel(DataSet: TDataSet);
    procedure qryGradeRecebimentoAfterCancel(DataSet: TDataSet);
    procedure qryGradeRecebimentoAfterPost(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure DBGridEh3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure cxButton5Click(Sender: TObject);
    function gerarProduto() : integer;
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
    procedure qryItemRecebimentoAfterScroll(DataSet: TDataSet);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure vinculaPedidoitemRecebimento (nCdItemRecebimento:integer; nCdProduto:integer);
    procedure VincularPedido1Click(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure verificaControlesICMS ( cCST : string );
    procedure qryItemRecebimentocCdSTChange(Sender: TField);
    procedure qryItemRecebimentocCFOPChange(Sender: TField);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure atualizaColunasInformacoesFiscais();
    procedure sugerirCalculoST (bCalcularIPI : boolean);
    procedure DBEdit12Exit(Sender: TObject);
    procedure checaValidadeCFOP( cCFOP : string ; bCFOPEntrada : boolean );
    procedure qryItemRecebimentocCFOPNFChange(Sender: TField);
    procedure calculaTotaisItem( bGravar : boolean );
    procedure ativaEdicaoProduto( bPermitirEditar : boolean ) ;
    procedure RemoverVnculoPedido1Click(Sender: TObject);
    procedure VincularMltiplosPedidos1Click(Sender: TObject);
    procedure Protocolo1Click(Sender: TObject);
    procedure cxTabSheet2Show(Sender: TObject);
    procedure qryItemRecebidoSemPedidonCdTipoPedidoRecebidoChange(
      Sender: TField);
    procedure qryItemRecebidoSemPedidoCalcFields(DataSet: TDataSet);
    procedure qryItemRecebidoSemPedidoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabCentroCustoShow(Sender: TObject);
    procedure cxTabSheet3Show(Sender: TObject);
    procedure ProdutoERP1Click(Sender: TObject);
    procedure ProdutoGrade1Click(Sender: TObject);
    procedure EmitirEtiquetaCdigoBarra1Click(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure ImportarXMLNFe1Click(Sender: TObject);
    procedure VisualizarNFe1Click(Sender: TObject);
    procedure gravaXMLNFe( cXML : string ; bGravarDadosCabecalho : boolean) ;
  private
    { Private declarations }
    cTipoEAN          : string;
    iDiasMinVenc      : integer ;
    nCdPedidoConsulta : integer ;
    cSugerirST        : string ;
    bReadOnly         : Boolean;
  public
    { Public declarations }
  end;

var
  frmRecebimentoCompleto: TfrmRecebimentoCompleto;

implementation

uses fMenu, fLookup_Padrao, fConsultaItemPedidoAberto, rRecebimento_view,
  fRecebimento_Divergencias, fMontaGrade, fRecebimento_Etiqueta,
  fModImpETP, fRecebimento_CadProduto,fRegistroMovLote,fRecebimentoLeituraXML, strUtils,
  fProdutoERP, fProduto, fRecebimentoCompletoMultiplosPedidos ;

{$R *.dfm}

var
   objConsultaItemPedidoAberto : TfrmConsultaItemPedidoAberto ;
   objGrade                    : TfrmMontaGrade ;
   objGeraProduto              : TfrmRecebimento_CadProduto ;


procedure TfrmRecebimentoCompleto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'RECEBIMENTO' ;
  nCdTabelaSistema  := 41 ;
  nCdConsultaPadrao := 89 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmRecebimentoCompleto.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa,DbEdit2.Text) ;
  if (qryEmpresa.Active) and (qryEmpresanCdTerceiroEmp.Value <> 0) then
        PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);
end;

procedure TfrmRecebimentoCompleto.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if qryEmpresa.Active then
      qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, DbEdit3.Text) ;
end;

procedure TfrmRecebimentoCompleto.DBEdit4Exit(Sender: TObject);
var
  i : integer;
begin
  inherited;
  PosicionaQuery(qryTipoReceb,DBEdit4.Text) ;

  atualizaColunasInformacoesFiscais() ;

end;

procedure TfrmRecebimentoCompleto.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  if (trim(DBEdit4.Text) = '') then
  begin
      MensagemAlerta('Informe o tipo de recebimento.') ;
      DBEdit4.SetFocus;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;

  PosicionaQuery(qryTerceiro, DBEdit6.Text) ;

  if (DBEdit18.Text <> '') and (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin
      qryTesteDupNF.Close ;
      qryTesteDupNF.Parameters.ParamByName('cNrDocto').Value    := DBEdit7.Text ;
      qryTesteDupNF.Parameters.ParamByName('cSerieDocto').Value := DBEdit8.Text ;
      qryTesteDupNF.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value ;
      qryTesteDupNF.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
      qryTesteDupNF.Open ;

      if not qryTesteDUPnf.Eof then
      begin
          MensagemAlerta('Esta nota fiscal j� foi recebida no recebimento No : ' + qryTesteDupNFnCdRecebimento.AsString) ;
      end ;

      qryTesteDupNf.Close ;

  end ;

end;

procedure TfrmRecebimentoCompleto.qryMasterBeforePost(DataSet: TDataSet);
begin

  if DBEdit3.Enabled and (Trim(DbEdit16.Text) = '') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

  if (trim(DbEdit17.Text) = '') then
  begin
      MensagemAlerta('Informe o Tipo de Recebimento.') ;
      dbEdit4.SetFocus;
      abort ;
  end ;

  if (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      if (trim(DbEdit18.Text) = '') then
      begin
          MensagemAlerta('Informe o Terceiro.') ;
          DBEdit6.SetFocus;
          abort ;
      end ;

      if (qryTipoRecebcFlgExigeNF.Value = 1) then
      begin

          if (trim(DbEdit7.Text) = '') then
          begin
              MensagemAlerta('Informe o N�mero da Nota Fiscal.') ;
              DbEdit7.SetFocus;
              abort ;
          end ;

          if (trim(DbEdit8.Text) = '') then
          begin
              qryMastercSerieDocto.Value := 'UN' ;
          end ;

          if (trim(qryMasterdDtDocto.AsString) = '/  /') then
          begin
              MensagemAlerta('Informe a data da nota fiscal.') ;
              DbEdit9.SetFocus;
              abort ;
          end ;

          if (qryMasterdDtDocto.Value > Now()) then
          begin
              MensagemAlerta('A data de emiss�o n�o pode ser maior que hoje.') ;
              DbEdit9.SetFocus;
              abort ;
          end ;

          if (qryMasternValTotalNF.Value <= 0) then
          begin
              MensagemAlerta('Informe o total da nota fiscal.') ;
              DBEdit36.SetFocus;
              abort ;
          end ;

      end ;

      if (qryMasternValDocto.Value <= 0) then
      begin
          MensagemAlerta('Informe o valor total dos produtos.') ;
          DbEdit10.SetFocus;
          abort ;
      end ;

  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusReceb.Value := 1 ;
      qryMasterdDtReceb.Value          := StrToDate(DateToStr(Now())) ;
  end ;

  if (qryMasternValBaseCalcICMSSub.asString = '') then
      qryMasternValBaseCalcICMSSub.Value := 0 ;

  if (qryMasternValICMSSub.asString = '') then
      qryMasternValICMSSub.Value := 0 ;

  if (qryMasternValICMS.asString = '') then
      qryMasternValICMS.Value := 0 ;

  if (qryMasternValDocto.asString = '') then
      qryMasternValDocto.Value := 0 ;

  if (qryMasternValSeguroNF.asString = '') then
      qryMasternValSeguroNF.Value := 0 ;

  if (qryMasternValFrete.asString = '') then
      qryMasternValFrete.Value := 0 ;

  if (qryMasternValDespesas.asString = '') then
      qryMasternValDespesas.Value := 0 ;

  if (qryMasternValIPI.asString = '') then
      qryMasternValIPI.Value := 0 ;

  if (qryMasternValDescontoNF.asString = '') then
      qryMasternValDescontoNF.Value := 0 ;

  if (qryMasternValTotalNF.asString = '') then
      qryMasternValTotalNF.Value := 0 ;


  if (qryTipoRecebcFlgExigeNF.Value = 1) and (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      if (ABS(qryMasternValTotalNF.Value - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value + qryMasternValIPI.Value + qryMasternValICMSSub.Value + qryMasternValDespesas.Value + qryMasternValSeguroNF.Value + qryMasternValFrete.Value)) > 1) then
      begin
          MensagemAlerta('Total dos valores digitados n�o confere com o total da nota fiscal.') ;
          DBEdit42.SetFocus;
          abort ;
      end ;

      qryMasternPercDescProduto.Value := 0 ;
      qryMasternPercDespesaNF.Value   := 0 ;
      qryMasternPercFreteNF.Value     := 0 ;
      qryMasternPercSeguroNF.Value    := 0 ;

      if (qryMasternValDescontoNF.Value > 0) then
          qryMasternPercDescProduto.Value := (qryMasternValDescontoNF.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValDespesas.Value > 0) then
          qryMasternPercDespesaNF.Value := (qryMasternValDespesas.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValFrete.Value > 0) then
          qryMasternPercFreteNF.Value := (qryMasternValFrete.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValSeguroNF.Value > 0) then
         qryMasternPercSeguroNF.Value := (qryMasternValSeguroNF.Value / qryMasternValDocto.Value) *100;

  end ;

  inherited;

end;

procedure TfrmRecebimentoCompleto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  if (qryMaster.State = dsInsert) then
      exit ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;

  PosicionaQuery(qryTipoReceb,qryMasternCdTipoReceb.AsString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryPrazoRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioFech.asString) ;
  PosicionaQuery(qryUsuarioQualidade, qryMasternCdUsuarioAnaliseQualidade.AsString) ;

  PosicionaQuery(qryTabStatusReceb,qryMasternCdTabStatusReceb.asString) ;

  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;
  DBGridEh3.ReadOnly := False ;

  if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      DBGridEh1.ReadOnly := True ;
      DBGridEh3.ReadOnly := True ;
  end ;

  if (qryMaster.Active) and (qryMasternCdTabStatusReceb.Value >= 5) then
  begin
      DBGridEh2.ReadOnly := True ;
  end ;

  PosicionaQuery(qryItemCC, qryMasternCdRecebimento.AsString) ;

  atualizaColunasInformacoesFiscais() ;

end;

procedure TfrmRecebimentoCompleto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItemRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryPrazoRecebimento, qryMasternCdRecebimento.AsString) ;

end;

procedure TfrmRecebimentoCompleto.btIncluirClick(Sender: TObject);
begin
  inherited;

  nCdPedidoConsulta := 0 ;
  
  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;
  DBGridEh3.ReadOnly := False ;

  qryMasternCdEmpresa.Value                          := frmMenu.nCdEmpresaAtiva;
  qryMasternCdTabTipoRecebimento.Value               := 1 ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  PosicionaQuery(qryEmpresa, DbEdit2.Text) ;

  If not DBEdit3.Enabled then
      DBEdit4.SetFocus
  else
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryaux.Fields.Clear ;
      qryAux.SQL.Add('SELECT nCdLoja FROM UsuarioLoja WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Fields.Clear;
      qryAux.Open ;

      if qryAux.eof then
      begin
          MensagemAlerta('Nenhuma loja vinculada para este usu�rio.') ;
          btcancelar.Click;
          abort ;
      end ;

      DbEdit3.SetFocus ;

      if (qryAux.RecordCount = 1) then
      begin
          qryMasternCdLoja.Value := qryAux.FieldList[0].Value ;
          PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
          DBEdit4.SetFocus;
      end ;

      qryAux.Close ;

  end ;

end;

procedure TfrmRecebimentoCompleto.FormShow(Sender: TObject);
begin
  inherited;

  //ACBrNFeDANFERave1.RavFile   := ExtractFileDir(Application.ExeName)+'\Nfe\Reports\NotaFiscalEletronica.rav';

  objConsultaItemPedidoAberto := TfrmConsultaItemPedidoAberto.Create( Self ) ;
  objGrade                    := TfrmMontaGrade.Create( Self ) ;
  objGeraProduto              := TfrmRecebimento_CadProduto.Create( Self ) ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  cTipoEAN     := frmMenu.LeParametro('EANRECEB') ;
  iDiasMinVenc := StrToint(frmMenu.LeParametro('DIAMINVENC')) ;

  cxPageControl1.ActivePageIndex := 0 ;

  TabLote.Enabled     := True ;
  cxTabSheet5.Enabled := True ;

  if (frmMenu.LeParametroEmpresa('LOTEPROD') <> 'S') then
  begin
      TabLote.Enabled     := False ;
      cxTabSheet5.Enabled := False ;
  end ;

  DBEdit3.Enabled := False ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      DBEdit3.Enabled := True ;
  end ;

  cSugerirST := frmMenu.LeParametro('SUGERIRSTREC') ;

  {-- formata a casa da quantidade sem decimal --}
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].DisplayFormat := ',0' ;
  DBGridEh2.Columns[DBGridEh2.FieldColumns['nQtde'].Index].DisplayFormat := ',0' ;

  qryItemRecebimentonValUnitario.DisplayFormat    := frmMenu.cMascaraCompras;
  qryItemRecebimentonValUnitarioEsp.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemRecebimentonValUnitarioPed.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemRecebimentonValCustoFinal.DisplayFormat  := frmMenu.cMascaraCompras;

  if (qryEmpresa.Active) and (qryEmpresanCdTerceiroEmp.Value <> 0) then
          PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);

end;

procedure TfrmRecebimentoCompleto.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(59,'Loja.nCdEmpresa = ' + intToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(173,'EXISTS(SELECT 1 FROM TipoPedido INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR.nCdTipoPedido = TipoPedido.nCdTipoPedido WHERE TPTR.nCdTipoReceb = ' + qryTipoRecebnCdTipoReceb.AsString + ')');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(88);

            If (nPK > 0) then
            begin
                qryMasternCdTipoReceb.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      abort ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

end;

procedure TfrmRecebimentoCompleto.cxButton1Click(Sender: TObject);
var
    nCdProdutoNovo : integer ;
begin

    if (not qryMaster.Active) or (qryMasternCdRecebimento.Value = 0) then
    begin
        MensagemAlerta('Salve os dados recebimento antes de consultar os pedidos.') ;
        abort ;
    end ;

    if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
    begin
       MensagemAlerta('Recebimento j� finalizado, imposs�vel alterar.') ;
       abort ;
    end ;

    if (qryMaster.State <> dsBrowse) then
    begin
      qryMaster.Post ;
    end ;

    cxPageControl1.ActivePageIndex := 0 ;

    objConsultaItemPedidoAberto.nCdTerceiro    := qryMasternCdTerceiro.Value ;
    objConsultaItemPedidoAberto.nCdTipoReceb   := qryMasternCdTipoReceb.Value ;
    objConsultaItemPedidoAberto.nCdLoja        := qryMasternCdLoja.Value ;
    objConsultaItemPedidoAberto.nCdRecebimento := qryMasternCdRecebimento.Value ;
    objConsultaItemPedidoAberto.nCdPedido      := nCdPedidoConsulta ;
    objConsultaItemPedidoAberto.nCdProduto     := 0 ;

    showForm( objConsultaItemPedidoAberto , FALSE ) ;

    if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
    begin

        qryItemRecebimento.Insert ;
        qryItemRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;

        nCdPedidoConsulta := objConsultaItemPedidoAberto.qryPedidonCdPedido.Value ;

        if (objConsultaItemPedidoAberto.qryItemPedidonCdProduto.Value > 0) then
        begin
            qryItemRecebimentonCdProduto.Value := objConsultaItemPedidoAberto.qryItemPedidonCdProduto.Value ;
            qryItemRecebimentocCdProduto.Value := qryItemRecebimentonCdProduto.AsString;
        end;

        qryItemRecebimentocCdProduto.Value     := objConsultaItemPedidoAberto.qryItemPedidonCdProduto.asString ;

        if (objConsultaItemPedidoAberto.qryItemPedidocCdProduto.Value <> '') then
            qryItemRecebimentocCdProduto.Value     := objConsultaItemPedidoAberto.qryItemPedidocCdProduto.Value ;

        qryItemRecebimentocNmItem.Value        := objConsultaItemPedidoAberto.qryItemPedidocNmItem.Value ;
        qryItemRecebimentocUnidadeMedida.Value := objConsultaItemPedidoAberto.qryItemPedidocSiglaUnidadeMedida.Value ;
        qryItemRecebimentonQtde.Value          := objConsultaItemPedidoAberto.qryItemPedidonQtde.Value ;
        qryItemRecebimentonValUnitario.Value   := 0 ;
        qryItemRecebimentonValTotal.Value      := 0 ;

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.Fields.Clear;
        qryAux.SQL.Add('SELECT ItemPedido.nCdTipoItemPed') ;
        qryAux.SQL.Add('      ,CASE WHEN ItemPedido.nCdTipoItemPed = 4 THEN (ItemPedido.nValUnitario - Pai.nValDesconto + Pai.nValAcrescimo)') ;
        qryAux.SQL.Add('            ELSE (ItemPedido.nValUnitario - ItemPedido.nValDesconto + ItemPedido.nValAcrescimo)') ;
        qryAux.SQL.Add('       END nValUnitario') ;
        qryAux.SQL.Add('      ,ItemPedido.nValUnitarioEsp') ;
        qryAux.SQL.Add('      ,ItemPedido.nCdPedido') ;
        qryAux.SQL.Add('      ,ItemPedido.nPercIPI') ;
        qryAux.SQL.Add('      ,ItemPedido.nPercICMSSub') ;
        qryAux.SQL.Add('  FROM ItemPedido') ;
        qryAux.SQL.Add('       LEFT JOIN ItemPedido Pai ON Pai.nCdItemPedido = ItemPedido.nCdItemPedidoPai') ;
        qryAux.SQL.Add(' WHERE ItemPedido.nCdItemPedido = ' + objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.AsString) ;
        qryAux.Open ;

        if not qryAux.eof then
        begin
            qryItemRecebimentonCdTipoItemPed.Value  := qryAux.FieldList[0].Value ;
            qryItemRecebimentonValUnitarioPed.Value := qryAux.FieldList[1].AsFloat;
            qryItemRecebimentonValUnitarioEsp.Value := qryAux.FieldList[2].AsFloat;
            qryItemRecebimentonCdPedido.Value       := qryAux.FieldList[3].Value ;
            qryItemRecebimentonPercIPIPed.Value     := qryAux.FieldList[4].AsFloat;
            qryItemRecebimentonPercICMSSubPed.Value := qryAux.FieldList[5].AsFloat;
        end
        else begin
            qryItemRecebimentonValUnitarioPed.Value := 0 ;
            qryItemRecebimentonValUnitarioEsp.Value := 0 ;
        end ;

        qryAux.Close ;

        qryItemRecebimentonCdItemPedido.Value  := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value ;

        if (qryItemRecebimentonCdProduto.asString <> '') then
        begin

              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT nCdGrade FROM Produto WHERE nCdTabTipoProduto = 2 AND nCdProduto = ' + qryItemRecebimentonCdProduto.asString) ;
              qryAux.Fields.Clear ;
              qryAux.Open ;

              if (not qryAux.eof) and (qryAux.FieldList[0].AsString <> '') then
              begin

                objGrade.qryValorAnterior.SQL.Text := '' ;
                objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);
                objGrade.Renderiza;

                qryItemRecebimentonQtde.Value := objGrade.nQtdeTotal ;

                DBGridEh1.SetFocus;
                DBGridEh1.Col := 5 ;

            end
            else begin
                DBGridEh1.SetFocus ;
                DBGridEh1.Col := 5 ;
            end ;
        end
        else begin
            //qryItemRecebimento.Post;
            DBGridEh1.SetFocus ;
        end ;


        DBGridEh1.Col := 5 ;
    end ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimento.State = dsCalcFields) then
  begin

      if (qryItemRecebimento.Active) then
          qryItemRecebimentonValTotalLiq.Value := qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ;

      if qryItemRecebimentonCdItemPedido.asString = '' then
          exit ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdPedido FROM ItemPedido WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.AsString) ;
      qryAux.Fields.Clear;
      qryAux.Open ;

      if not qryAux.eof then
          qryItemRecebimentonCdPedido.Value := qryAux.FieldList[0].Value ;

      qryAux.Close ;

  end ;

end;

procedure TfrmRecebimentoCompleto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoReceb.Close ;
  qryTerceiro.Close ;
  qryItemRecebimento.Close ;
  qryUsuario.Close ;
  qryTabStatusReceb.Close ;
  qryPrazoRecebimento.Close ;
  qryItemCC.Close ;
  qryUsuario.Close ;
  qryUsuarioQualidade.Close ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimentocCdProduto.Value <> 'AD') and (qryItemRecebimentonCdProduto.Value = 0) and (qryItemRecebimentonCdTipoItemPed.Value <> 5) then
  begin
      MensagemAlerta('Informe o produto ou pressione ESC.') ;
      DbGridEh1.Col := 1 ;
      abort ;
  end ;

  if (qryItemRecebimentocCdProduto.Value = 'AD') and (qryItemRecebimentonCdTipoItemPed.Value = 5) and (Trim(qryItemRecebimentocNmItem.Value) = '') then
  begin
      MensagemAlerta('Informe a Descri��o do produto AD.') ;
      DbGridEh1.Col := 4 ;
      abort ;
  end ;

  if ( trim( qryItemRecebimentocUnidadeMedida.Value ) = '' ) then
  begin
      MensagemAlerta('Informe a Unidade de Medida.') ;
      abort ;
  end ;

  qryItemRecebimentocUnidadeMedida.Value := Uppercase( qryItemRecebimentocUnidadeMedida.Value ) ;

  qryUnidadeMedida.Close;
  PosicionaQuery( qryUnidadeMedida , qryItemRecebimentocUnidadeMedida.Value ) ;

  if ( qryUnidadeMedida.Eof ) then
  begin
      MensagemAlerta('Unidade de Medida ' + qryItemRecebimentocUnidadeMedida.Value + ' n�o cadastrada.') ;
      abort ;
  end ;

  if (qryItemRecebimentonQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade recebida.') ;
      abort ;
  end ;

  {-- RECALCULA OS TOTAIS DO ITEM E SEUS IMPOSTOS --}
  calculaTotaisItem( TRUE ) ;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

      if Trim(qryItemRecebimentocCdSt.Value) = '' then
      begin
          MensagemAlerta('CST do item n�o informado.');
          abort;
      end;

      if Trim(qryItemRecebimentocCfopNF.Value) = '' then
      begin
          MensagemAlerta('Informe o CFOP da NF.');
          abort;
      end;

      checaValidadeCFOP( qryItemRecebimentocCFOPNF.Value , FALSE ) ;

      if     (length(trim(qryItemRecebimentocNCM.Value)) <> 2)
         and (length(trim(qryItemRecebimentocNCM.Value)) <> 8) then
      begin
          MensagemAlerta('NCM/SH Inv�lido. O NCM deve ter 02 ou 08 d�gitos.') ;
          abort;
      end ;

      { -- valida NCM -- }
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TabIBPT WHERE cNCM = ' + #39 + qryItemRecebimentocNCM.Value + #39);
      qryAux.Open;

      if (qryAux.IsEmpty) then
      begin
          MensagemAlerta('NCM ' + qryItemRecebimentocNCM.Value + ' n�o cadastrado.');
          DBGridEh1.SelectedField := qryItemRecebimentocNCM;
          DBGridEh1.SetFocus;
          Abort;
      end;

      {-- se o item tem IPI, verifica se tem um CST do IPI v�lido e de sa�da --}
      if (qryItemRecebimentonValIPI.Value > 0) then
      begin

          qryCST_IPI.Close;
          PosicionaQuery( qryCST_IPI , qryItemRecebimentocCdSTIPI.Value ) ;

          if ( qryCST_IPI.eof ) then
          begin
              MensagemAlerta('CST do IPI n�o cadastrado.') ;
              abort ;
          end
          else
          begin

              {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
              {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
              {-- num�ricos e mantidos pela ER2.                                                         --}
              if ( strToint( qryCST_IPIcCdStIPI.Value ) < 50 ) then
              begin
                  MensagemAlerta('Informe um CST de Sa�da para o IPI. O CST informado � um CST de Entrada.') ;
                  abort ;
              end ;

          end ;

      end ;

      if (frmMenu.LeParametro('CLASSEENTRRECEB') = 'S') then
      begin

          {-- checa se o CFOP de entrada � um CFOP v�lido. --}
          if trim(qryItemRecebimentocCFOP.Value) = '' then
          begin
              MensagemAlerta('Informe o CFOP de entrada.');
              abort;
          end;

          checaValidadeCFOP( qryItemRecebimentocCFOP.Value , TRUE ) ;

          {-- se o item tem IPI, verifica se tem um CST de entrada v�lido para o IPI --}
          if (qryItemRecebimentonValIPI.Value > 0) then
          begin

              qryCST_IPI.Close;
              PosicionaQuery( qryCST_IPI , qryItemRecebimentocCdSTIPIEntrada.Value ) ;

              if ( qryCST_IPI.eof ) then
              begin
                  MensagemAlerta('CST de Entrada do IPI inv�lido.') ;
                  abort ;
              end
              else
              begin

                  {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
                  {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
                  {-- num�ricos e mantidos pela ER2.                                                         --}
                  if ( strToint( qryCST_IPIcCdStIPI.Value ) >= 50 ) then
                  begin
                      MensagemAlerta('Informe um CST de Entrada para o IPI. O CST informado � um CST de sa�da.') ;
                      abort ;
                  end ;

              end ;

          end ;

      end ;

  end;

  if (qryTerceirocFlgOptSimples.Value = 1) then
  begin
      if (qryItemRecebimentocFlgGeraCreditoICMS.Value = 1) then
      begin
          MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de ICMS.') ;
          abort ;
      end ;

      if (qryItemRecebimentocFlgGeraCreditoIPI.Value = 1) then
      begin
          MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de IPI.') ;
          abort ;
      end ;
  end ;

  if (qryItemRecebimentonValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio.') ;
      abort ;
  end ;

  if (qryItemRecebimentonValICMS.Value < 0) then
  begin
      MensagemAlerta('Valor do ICMS n�o pode ser negativo.') ;
      abort ;
  end ;

  if (qryItemRecebimentonValIPI.Value < 0) then
  begin
      MensagemAlerta('Valor do IPI n�o pode ser negativo.') ;
      abort ;
  end ;

  if (qryItemRecebimentonValICMSSub.Value < 0) then
  begin
      MensagemAlerta('Valor do ICMS de Substitui��o Tribut�ria n�o pode ser negativo.') ;
      abort ;
  end ;

  qryItemRecebimentonCdTabStatusItemPed.Value  := 1 ;
  qryItemRecebimentonCdRecebimento.Value       := qryMasternCdRecebimento.Value ;
  qryItemRecebimentocFlgDiverg.Value           := 0 ;
  qryItemRecebimentonCdTabTipoDiverg.AsVariant := Null ;

  // calculo custo final unitario
  qryItemRecebimentonValCustoFinal.Value := ((qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value   + qryItemRecebimentonValIPI.Value   + qryItemRecebimentonValICMSSub.Value  +
                                              qryItemRecebimentonValAcessorias.Value + qryItemRecebimentonValFrete.Value + qryItemRecebimentonValSeguro.Value) / qryItemRecebimentonQtde.Value) ;

  if (qryItemRecebimentonCdItemPedido.Value > 0) and (qryItemRecebimento.State = dsInsert) then
  begin

    qryAux.Close ;
    qryAux.Fields.Clear ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT 1 FROM ItemRecebimento WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.asString + ' AND nCdRecebimento = ' + qryMasternCdRecebimento.asString) ;
    qryAux.Open ;

    if not qryAux.Eof then
    begin
        MensagemAlerta('Este Item j� est� digitado neste recebimento.') ;
        qryAux.Close ;
        qryItemRecebimento.Cancel ;
        abort ;
    end ;

    qryAux.Close ;

  end ;

  if (qryItemRecebimentonCdItemPedido.Value = 0) then
  begin

      qryConsultaPedido.Close ;
      qryConsultaPedido.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva            ;

      if (qryLoja.Active) then
          qryConsultaPedido.Parameters.ParamByName('nCdLoja').Value    := qryLojanCdLoja.Value               ;

      if (qryTerceiro.Active) and (not qryTerceiro.Eof) then
          qryConsultaPedido.Parameters.ParamByName('nCdTerceiro').Value    := qryTerceironCdTerceiro.Value
      else qryConsultaPedido.Parameters.ParamByName('nCdTerceiro').Value := 0 ;

      qryConsultaPedido.Parameters.ParamByName('nCdTipoReceb').Value   := qryTipoRecebnCdTipoReceb.Value     ;
      qryConsultaPedido.Parameters.ParamByName('nCdProduto').Value     := qryItemRecebimentonCdProduto.Value ;
      qryConsultaPedido.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value      ;
      qryConsultaPedido.Open ;

      if not qryConsultaPedido.eof then
      begin

          if (qryConsultaPedido.FieldList[0].Value <> null) then
          begin
              qryItemRecebimentonCdItemPedido.Value   := qryConsultaPedido.FieldList[0].Value ;
              qryItemRecebimentonValUnitarioPed.Value := qryConsultaPedido.FieldList[5].Value ;
              qryItemRecebimentonValUnitarioEsp.Value := qryConsultaPedido.FieldList[2].Value ;
          end ;

      end ;

      qryConsultaPedido.Close ;
  end ;

  if (qryItemRecebimento.State = dsInsert) then
      qryItemRecebimentonCdItemRecebimento.Value := frmMenu.fnProximoCodigo('ITEMRECEBIMENTO') ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryItemRecebimentocFlgDescAbatUnitario.Value = 0) AND (qryItemRecebimentonValDescAbatUnit.Value > 0) then
  begin

    if (Column = DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index]) then
    begin
      DBGridEh1.Canvas.Brush.Color := clWhite;
      DBGridEh1.Canvas.Font.Color  := clRed ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

  end ;

  if (qryItemRecebimentonCdTabStatusItemPed.Value = 4) then
  begin

    if (Column = DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index]) then
    begin
      DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clBlue ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

  end ;

end;



procedure TfrmRecebimentoCompleto.DBGridEh1ColExit(Sender: TObject);
var
  bGrade: boolean ;
label
  montaGrade;
begin
  inherited;

  bGrade := false ;

  if (DbGridEh1.Col = 1) and (qryItemRecebimento.State <> dsBrowse) then
  begin

      ativaEdicaoProduto( TRUE ) ;

      //Produto AD
      qryItemRecebimentocCdProduto.Value := Uppercase(qryItemRecebimentocCdProduto.Value);
      qryItemRecebimentocCdProduto.Value := Trim(qryItemRecebimentocCdProduto.Value);

      if (qryTipoRecebcFlgPermiteItemAD.Value = 0) and (qryItemRecebimentocCdProduto.Value = 'AD') then
      begin
          MensagemAlerta('Tipo de Recebimento n�o permite item AD.') ;
          DbGridEh1.Col := 1 ;
          abort ;
      end
      else
      begin

          {-- se for um item AD ativa a edi��o, se n�o for o desativa os campos do produto --}
          ativaEdicaoProduto( (trim(qryItemRecebimentocCdProduto.Value) = 'AD') ) ;

          if ( trim( qryItemRecebimentocCdProduto.Value ) = 'AD' ) then
          begin
              DbGridEh1.Col                          := 4 ;
              qryItemRecebimentonCdTipoItemPed.Value := 5;
          end;

      end;

      if ((qryItemRecebimentocCdProduto.Value <> '') and (qryItemRecebimentocCdProduto.Value <> 'AD')) then
      begin

          PosicionaQuery(qryProduto,qryItemRecebimentocCdProduto.Value); //  asString) ;

          if (qryProduto.eof) then
          begin
              MensagemAlerta('C�digo de produto inv�lido.') ;
              abort ;
          end ;

          qryItemRecebimentonCdProduto.Value     := qryProdutonCdProduto.Value;
          qryItemRecebimentocNmItem.Value        := qryProdutocNmProduto.Value;
          qryItemRecebimentocUnidadeMedida.Value := qryProdutocUnidadeMedida.Value;
          qryItemRecebimentonCdTipoItemPed.Value := 2 ;

          qryGrupoImposto.Close;
          PosicionaQuery( qryGrupoImposto , qryProdutonCdProduto.asString ) ;

          if not qryGrupoImposto.eof then
              qryItemRecebimentocNCM.Value := qryGrupoImpostocNCM.Value ;

          qryGrupoImposto.Close;

          if (qryProdutonCdGrade.Value > 0) then
          begin
              bGrade:= true ;

              if (qryProdutonCdTabTipoProduto.Value = 3) then
              begin
                  MensagemAlerta('Produto em grade s� pode receber entrada pelo c�digo intermedi�rio. Utilize o C�digo : ' + qryProdutonCdProdutoPai.asString) ;
                  abort ;
              end ;

              if (qryProdutonCdTabTipoProduto.Value = 1) then
              begin
                  MensagemAlerta('O produto estruturado n�o pode receber entradas. Utilize o produto intermedi�rio.') ;
                  abort ;
              end ;

              qryItemRecebimentonCdTipoItemPed.Value := 1 ;

          end ;

          qryProduto.Close ;

          qryAux.Close ;
          qryAux.Fields.Clear;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT 1                                                                                            ') ;
          qryAux.SQL.Add('  FROM Produto                                                                                      ') ;
          qryAux.SQL.Add(' WHERE nCdProduto = ' + qryItemRecebimentonCdProduto.asString                                        );
          qryAux.SQL.Add('   AND EXISTS(SELECT 1                                                                              ');
          qryAux.SQL.Add('                FROM GrupoProdutoTipoPedido GPTP                                                    ');
          qryAux.SQL.Add('                     INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR.nCdTipoPedido = GPTP.nCdTipoPedido ');
          qryAux.SQL.Add('               WHERE GPTP.nCdGrupoProduto = Produto.nCdGrupo_Produto AND nCdTipoReceb = ' + qryMasternCdTipoReceb.asString + ')                         ') ;
          qryAux.Open ;

          if qryAux.Eof and (qryTipoRecebcFlgRecebCego.Value = 0) then
          begin
              qryAux.Close ;
              MensagemAlerta('Este produto pertence a um grupo de produto que n�o � permitido neste tipo de recebimento.') ;
              qryItemRecebimento.Cancel;
              abort ;
          end ;

          qryAux.Close ;

          if bGrade then
          begin
              { -- verifica se a grade ao menos tem um item informado cadastrado -- }
              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('SELECT 1 FROM ItemGrade WHERE nCdGrade = ' + qryProdutonCdGrade.AsString);
              qryAux.Open;

              if (qryAux.IsEmpty) then
              begin
                  MensagemAlerta('Grade No ' + qryProdutonCdGrade.AsString + ' do produto ' + qryProdutocNmProduto.Value + ' n�o configurado corretamente.');
                  Abort;
              end;

              montaGrade:
              objGrade.qryValorAnterior.SQL.Text := '' ;

              if (qryItemRecebimentonCdItemRecebimento.Value > 0) then
                  objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtde FROM ItemRecebimento WHERE nCdTipoItemPed = 4 AND nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString + ' ORDER BY ncdProduto') ;

              objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);
              objGrade.Renderiza;

              if (objGrade.nQtdeTotal <= 0) then
              begin
                  MensagemErro('Quantidade digitada na grade inv�lida ou n�o informada.') ;
                  goto montaGrade;
              end ;

              qryItemRecebimentonQtde.Value := objGrade.nQtdeTotal ;

              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].Title.Font.Color := clRed ;
              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].ReadOnly         := True ;
              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].Color            := clSilver ;

              DBGridEh1.SetFocus;

              if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
                  DBGridEh1.Col := 7
              else DBGridEh1.Col := 11 ;

          end
          else
          begin
              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].Title.Font.Color := clBlack ;
              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].ReadOnly         := False ;
              DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].Color            := clWhite ;
              DBGridEh1.Col := 6 ;
          end;
      end;
  end ;

  if (DBGridEh1.Col = 4)  and (qryItemRecebimentocCdProduto.Value = 'AD') and (qryItemRecebimento.State <> dsBrowse) then
      qryItemRecebimentocNmItem.Value := Uppercase(qryItemRecebimentocNmItem.Value);

  { -- inclui sugest�o de CFOP de entrada caso a natureza de opera��o esteja configurada (entrada x sa�da) -- }
  if ((DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOPNF') and (qryItemRecebimento.State <> dsBrowse)) then
  begin
      qryNaturezaOperacao.Close;
      qryNaturezaOperacao.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value;
      qryNaturezaOperacao.Parameters.ParamByName('cCFOPPedido').Value    := qryItemRecebimentocCFOPNF.Value;
      qryNaturezaOperacao.Parameters.ParamByName('cFlgEntSai').Value     := 'S';
      qryNaturezaOperacao.Open;

      if (not qryNaturezaOperacao.IsEmpty) then
          qryItemRecebimentocCFOP.Value := qryNaturezaOperacaocCFOP.Value;
  end;

  calculaTotaisItem( FALSE ) ;

end;

procedure TfrmRecebimentoCompleto.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex        := 0 ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh2Enter(Sender: TObject);
begin
  inherited;
  if not qryMaster.Active then
      abort ;

  if (qryMaster.State <> dsBrowse) then
  begin
      qryMaster.Post ;
  end ;

end;

procedure TfrmRecebimentoCompleto.qryPrazoRecebimentoBeforePost(DataSet: TDataSet);
begin
  inherited;

  //if (qryPrazoRecebimentodDtVenc.Value < StrToDate(DateToStr(Now() + iDiasMinVenc))) then
  //begin
  //    ShowMessage('Data de vencimento inferior a pol�cita de pagamento utilizada. Prazo m�nimo para vencimento: ' + IntToStr(iDiasMinVenc) + ' dia(s).') ;
  //    abort ;
  //end ;

  if (qryPrazoRecebimentonValParcela.Value <= 0) then
  begin
      MensagemAlerta('Valor da parcela inv�lido ou n�o informado.') ;
      abort ;
  end ;

  qryPrazoRecebimentonValPagto.Value := qryPrazoRecebimentonValParcela.Value ;

  if (qryPrazoRecebimentocFlgDocCobranca.Value = 1) and (qryPrazoRecebimentocNrTit.Value = '') then
  begin
      MensagemAlerta('Informe o n�mero do t�tulo a pagar.') ;
      abort ;
  end ;

  qryPrazoRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;
  qryPrazoRecebimentoiQtdeDias.Value      := Trunc(qryPrazoRecebimentodDtVenc.Value - qryMasterdDtDocto.Value) ;

  if (qryPrazoRecebimento.State = dsInsert) then
      qryPrazoRecebimentonCdPrazoRecebimento.Value := frmMenu.fnProximoCodigo('PRAZORECEBIMENTO') ;

end;

procedure TfrmRecebimentoCompleto.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentoAfterPost(DataSet: TDataSet);
var
 i : integer ;
 bGrade : boolean ;
begin

  bGrade := False ;

  if (qryItemRecebimentonCdProduto.Value > 0) then
  begin

      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdGrade FROM Produto WHERE nCdTabTipoProduto = 2 AND nCdProduto = ' + qryItemRecebimentonCdProduto.asString) ;
      qryAux.Fields.Clear ;
      qryAux.Open ;


      if (not qryAux.eof) and (qryAux.FieldList[0].AsString <> '') then
          bGrade := true ;

      qryAux.Close ;
  end ;


  if (not objGrade.bMontada) and (bGrade) then
  begin

      objGrade.qryValorAnterior.SQL.Text := '' ;

      if (qryItemRecebimentonCdItemRecebimento.Value > 0) then
          objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtde FROM ItemRecebimento WHERE nCdTipoItemPed = 4 AND nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString + ' ORDER BY ncdProduto') ;

      objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);

  end ;

  // exclui as diverg�ncias
  try
      cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemRecebimentonCdItemRecebimento.Value ;
      cmdExcluiSubItem.Execute;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  if (qryItemRecebimentonCdItemPedido.Value > 0) then
  begin

      if (qryItemRecebimentonPercICMSSub.Value > qryItemRecebimentonPercICMSSubPed.Value) and (qryTipoRecebcFlgDivICMSSub.Value = 1) then
      begin
          SP_INSERI_DIVERGENCIA_RECEB.Close ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 9 ;
          SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

      if (qryItemRecebimentonPercIPI.Value > qryItemRecebimentonPercIPIPed.Value) and (qryTipoRecebcFlgDivIPI.Value = 1) then
      begin
          SP_INSERI_DIVERGENCIA_RECEB.Close ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 8 ;
          SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

      qryAux.Close ;
      qryAux.Fields.Clear ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT (nQtdePed-nQtdeExpRec-nQtdeCanc) FROM ItemPedido WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.asString) ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          if (qryItemRecebimentonQtde.Value > qryAux.FieldList[0].Value) and (qryItemRecebimentonCdTipoItemPed.Value <> 1) then
          begin
              SP_INSERI_DIVERGENCIA_RECEB.Close ;
              SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 4 ;
              SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
          end ;
      end ;

      //inseri os itens da grade
      if (bGrade) then
      begin
          objGrade.cdsQuantidade.First;
          objGrade.cdsCodigo.First ;
      end ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (objGrade.cdsQuantidade.Fields[i].Value <> null) and (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdRecebimento').Value     := qryMasternCdRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value         := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value              := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      if (bGrade) then
          objGrade.LiberaGrade;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 WHERE IsNull((SELECT Sum(nCdProduto*nQtdePed) FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemRecebimentonCdItemPedido.AsString +' AND nCdTipoItemPed = 4),0) <> IsNull((SELECT Sum(nCdProduto*nQtde) FROM ItemRecebimento WHERE nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.asString +' AND nCdTipoItemPed = 4),0)') ;
      qryAux.Open ;

      if not qryAux.Eof and (qryTipoRecebcFlgDivGrade.Value = 1) then
      begin
        SP_INSERI_DIVERGENCIA_RECEB.Close ;
        SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
        SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 5 ;
        SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

  end
  else begin
      // sem pedido
      //inseri os itens da grade
      if (bGrade) then
      begin
          objGrade.cdsQuantidade.First;
          objGrade.cdsCodigo.First ;
      end ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdRecebimento').Value     := qryMasternCdRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value         := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value              := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      if (bGrade) then
          objGrade.LiberaGrade;

      SP_INSERI_DIVERGENCIA_RECEB.Close ;
      SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
      SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 1 ;
      SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
  end ;

  inherited;

  {qryItemRecebimento.Requery();
  qryItemRecebimento.Last;}

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentoBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  frmMenu.Connection.BeginTrans;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemRecebimentonCdItemRecebimento.Value ;
    cmdExcluiSubItem.Execute;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM HistAutorDiverg WHERE nCdItemRecebimento = ' + qryItemRecebimentonCdItemRecebimento.asString);
    qryAux.ExecSQL;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM CentroCustoItemRecebimento WHERE nCdItemRecebimento = ' + qryItemRecebimentonCdItemRecebimento.asString);
    qryAux.ExecSQL;

  except
    frmMenu.Connection.RollbackTrans;
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmRecebimentoCompleto.ToolButton10Click(Sender: TObject);
var
    iLimiteDias : integer ;
    strEstados  : TStringList ;
    objForm     : TfrmRegistroMovLote ;
    nQtdeLote   : double ;
    bClassificaEntrada : boolean ;

label
    PulaConsistencias ;

begin
  inherited;

  bClassificaEntrada := (frmMenu.LeParametro('CLASSEENTRRECEB') = 'S') ;

  if not qryMaster.Active then
      exit ;

  If (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      abort;
  end ;

  if (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Recebimento j� finalizado.') ;
      abort;
  end ;

  if (qryTipoRecebcFlgExigeImportacaoXMLNFe.Value = 1) and (qryMastercXMLNFe.Value = '') then
  begin
      MensagemAlerta('Importe o XML da NFe antes de finalizar o recebimento.') ;
      abort;
  end ;

  if (qryPrazoRecebimento.Active) and (qryPrazoRecebimento.State <> dsBrowse) then
      qryPrazoRecebimento.Post ;

  if (qryMasternCdTabTipoRecebimento.Value = 0) then
  begin
      MensagemAlerta('Pr� - Digita��o de Nota Fiscal Registrada com Sucesso.') ;
      abort;
  end ;

  iLimiteDias := 0 ;

  // se for recebimento CEGO n�o confere NADA!
  if (qryTipoRecebcFlgRecebCego.Value = 1) then
      goto PulaConsistencias ;

  if ((qryTipoRecebcFlgExigeNfe.Value = 1) and (DBEdit29.Text = '')) then
  begin
      MensagemAlerta('� obrigat�ria a informa��o da Chave da NFe.') ;
      DBEdit29.SetFocus;
      abort;
  end;

  // verifica o estado de origem da NF
  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

       if (Trim(DBEdit12.Text) = '') then
       begin
           MensagemAlerta('Informe a unidade federativa de origem da nota fiscal.');
           DBEdit12.SetFocus;
           Abort;
       end;

       qryChecaEnderecoUFTerceiro.Close;
       qryChecaEnderecoUFTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
       qryChecaEnderecoUFTerceiro.Parameters.ParamByName('cUF').Value         := DBEdit12.Text ;
       qryChecaEnderecoUFTerceiro.Open;

       if (qryChecaEnderecoUFTerceiro.eof) then
       begin
           MensagemAlerta('Nenhum endere�o ativo encontrado para este terceiro no estado de origem da nota fiscal. Verifique.') ;
           abort ;
       end ;

       qryChecaEnderecoUFTerceiro.Close;

  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT IsNull(Sum(nValTotal),0)          ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValDesconto),0)       ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValIPI),0)            ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValICMSSub),0)        ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValSeguro),0)         ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValAcessorias),0)     ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValFrete),0)          ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValBaseCalcSubTrib),0)');
  qryAux.SQL.Add('      ,IsNull(Sum(nBaseCalcICMS),0)      ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValICMS),0)           ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValPIS),0)            ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValCOFINS),0)         ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValII),0)             ');
  qryAux.SQL.Add('  FROM ItemRecebimento                   ');
  qryAux.SQL.Add(' WHERE nCdTipoItemPed IN (1,2,3,5,6,9)   ');
  qryAux.SQL.Add('   AND nCdRecebimento = ' + qryMasternCdRecebimento.asString) ;
  qryAux.Open ;

  if (qryTipoRecebcFlgExigeNfe.Value = 1) then
  begin
  if (ABS(qryMasternValTotalNF.Value - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value + qryMasternValIPI.Value + qryMasternValICMSSub.Value + qryMasternValDespesas.Value + qryMasternValSeguroNF.Value + qryMasternValFrete.Value)) > 1) then
      begin
          MensagemAlerta('Total dos valores digitados n�o confere com o total da nota fiscal.') ;
          DBEdit42.SetFocus;
          abort ;
      end;
  end;

  {-- s� checa os dados de ICMS comum quando o tipo de recebimento exige os dados fiscais --}
  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

      // Valor da base de calculo de icms
      if qryAux.Eof or (ABS((qryAux.FieldList[8].Value) -  qryMasternValBaseICMS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor da Base de C�lculo do ICMS da Nota Fiscal n�o confere com o valor da BC dos itens digitados.') ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
          abort ;
      end ;

      // Valor do icms
      if qryAux.Eof or (ABS((qryAux.FieldList[9].Value) -  qryMasternValICMS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor do ICMS da Nota Fiscal n�o confere com o valor do ICMS dos itens digitados.') ;
          abort ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
      end ;

      // Valor do PIS
      if qryAux.Eof or (ABS((qryAux.FieldList[10].Value) -  qryMasternValPIS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor do PIS da Nota Fiscal n�o confere com o valor do PIS dos itens digitados.') ;
          abort ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
      end ;

      // Valor do COFINS
      if qryAux.Eof or (ABS((qryAux.FieldList[11].Value) -  qryMasternValCOFINS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor do COFINS da Nota Fiscal n�o confere com o valor do COFINS dos itens digitados.') ;
          abort ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
      end ;

      // Valor do II (Imposto de Importa��o)
      if qryAux.Eof or (ABS((qryAux.FieldList[12].Value) -  qryMasternValII.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor do Imposto de Importa��o da Nota Fiscal n�o confere com o valor do II dos itens digitados.') ;
          abort ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
      end ;

  end ;

  // Valor da base de calculo substituicao
  if qryAux.Eof or (ABS((qryAux.FieldList[7].Value) -  qryMasternValBaseCalcICMSSub.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da Base de C�lculo Substititui��o ICMS da Nota Fiscal n�o confere com o valor B.Calc. Sub. Total dos itens digitados.') ;
      abort ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
  end ;

  // Valor da icms substituicao
  if qryAux.Eof or (ABS((qryAux.FieldList[3].Value) -  qryMasternValICMSSub.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da ICMS Substitui��o da Nota Fiscal n�o confere com o valor ICMS Subs. total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor do Seguro
  if qryAux.Eof or (ABS((qryAux.FieldList[4].Value) -  qryMasternValSeguroNF.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do Seguro da Nota Fiscal n�o confere com o seguro total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor do Frete
  if qryAux.Eof or (ABS((qryAux.FieldList[6].Value) -  qryMasternValFrete.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do Frete da Nota Fiscal n�o confere com o Frete total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor das Despesas Acessorias
  if qryAux.Eof or (ABS((qryAux.FieldList[5].Value) -  qryMasternValDespesas.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da Despesa Acess�ria da Nota Fiscal n�o confere com o Valor das Despesas Acess�rias Total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;


  if qryAux.Eof or (ABS((qryAux.FieldList[0].Value-qryAux.FieldList[1].Value) - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value)) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor Total dos produtos n�o confere com o total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  {if qryAux.Eof or (ABS(qryAux.FieldList[1].Value - qryMasternValDescontoNF.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do desconto da NF n�o confere com o total de desconto dos itens digitados.') ;
      abort ;
  end ;}

  {-- verifica se o total do IPI dos itens confere com o total do IPI digitado na capa do recebimento --}
  if qryAux.Eof or (ABS(qryAux.FieldList[2].Value - qryMasternValIPI.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do IPI da NF n�o confere com o total de IPI dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  qryAux.Close ;

  if (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin

      try
          iLimiteDias := StrToInt(frmMenu.LeParametro('LIMDIAEMINFREC')) ;
      except
          MensagemAlerta('Parametro LIMDIAEMINFREC configurado incorretamente.') ;
          iLimiteDias := 0 ;
      end;

      if (iLimiteDias > 0) AND ((StrToDate(DateToStr(Now())) - qryMasterdDtDocto.Value) > iLimiteDias) then
      begin
          MensagemAlerta('A data de emiss�o da nota fiscal � superior ao limite de dias tolerado para recebimento.') ;
          exit ;
      end ;

  end ;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin
      if (qryMasternValBaseICMS.Value < 0) then
      begin
          MensagemAlerta('Base de c�lculo de ICMS inv�lida') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value > 0) and (qryMasternValICMS.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS') ;
          DbEdit37.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value <= 0) and (qryMasternValICMS.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value > 0) and (qryMasternValICMSSub.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS Substitui��o') ;
          DbEdit31.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value <= 0) and (qryMasternValICMSSub.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS Substitui��o') ;
          DbEdit43.SetFocus ;
          exit ;
      end ;

  end;

  if (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin

      if (Trim(DBEdit23.Text) = '') then
      begin
          MensagemAlerta('Informe o Modelo da Nota Fiscal.') ;
          DbEdit23.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value < 0) then
      begin
          MensagemAlerta('Base de c�lculo de ICMS inv�lida') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value > 0) and (qryMasternValICMS.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS') ;
          DbEdit37.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value <= 0) and (qryMasternValICMS.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value > 0) and (qryMasternValICMSSub.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS Substitui��o') ;
          DbEdit31.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value <= 0) and (qryMasternValICMSSub.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS Substitui��o') ;
          DbEdit43.SetFocus ;
          exit ;
      end ;

      cxPageControl1.ActivePage := cxTabSheet1 ;

  end ;


  {-- faz uma varredura nos itens recebidos para conferir algumas informa��es                                     --}
  {-- essas mesmas consist�ncias s�o feitas no BeforePost e no bot�o finalizar                                    --}
  {-- Alguns recebimento s�o importados do XML e n�o passam pelo BeforePost, por isso � conferido aqui novamente. --}

  qryItemRecebimento.First;

  while not qryItemRecebimento.eof do
  begin

      if ( trim( qryItemRecebimentocUnidadeMedida.Value ) = '' ) then
      begin
          MensagemAlerta('Informe a Unidade de Medida.') ;
          abort ;
      end ;

      qryUnidadeMedida.Close;
      PosicionaQuery( qryUnidadeMedida , qryItemRecebimentocUnidadeMedida.Value ) ;

      if ( qryUnidadeMedida.Eof ) then
      begin
          MensagemAlerta('Unidade de Medida ' + qryItemRecebimentocUnidadeMedida.Value + ' n�o cadastrada.') ;
          abort ;
      end ;

      // confere se as informa��es de CFOP de entrada e CST do IPI de entrada foram informados para todos os itens
      if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
      begin

          if Trim(qryItemRecebimentocCdSt.Value) = '' then
          begin
              MensagemAlerta('CST do ICMS do item n�o informado.');
              abort;
          end;

          if Trim(qryItemRecebimentocCFOPNF.Value) = '' then
          begin
              MensagemAlerta('Informe o CFOP da NF.');
              abort;
          end;

          checaValidadeCFOP( qryItemRecebimentocCFOPNF.Value , FALSE ) ;

          if     (length(trim(qryItemRecebimentocNCM.Value)) <> 2)
             and (length(trim(qryItemRecebimentocNCM.Value)) <> 8) then
          begin
              MensagemAlerta('NCM/SH Inv�lido. O NCM deve ter 02 ou 08 d�gitos.') ;
              abort;
          end ;

          {-- se o item tem IPI, verifica se tem um CST do IPI v�lido e de sa�da --}
          if (qryItemRecebimentonValIPI.Value > 0) then
          begin

              qryCST_IPI.Close;
              PosicionaQuery( qryCST_IPI , qryItemRecebimentocCdSTIPI.Value ) ;

              if ( qryCST_IPI.eof ) then
              begin
                  MensagemAlerta('CST do IPI n�o cadastrado.') ;
                  abort ;
              end
              else
              begin

                  {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
                  {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
                  {-- num�ricos e mantidos pela ER2.                                                         --}
                  if ( strToint( qryCST_IPIcCdStIPI.Value ) < 50 ) then
                  begin
                      MensagemAlerta('Informe um CST de Sa�da para o IPI. O CST informado � um CST de Entrada.') ;
                      abort ;
                  end ;

              end ;

          end ;

          {-- se o parametro manda classificar os dados de entrada no momento do recebimento --}
          {-- valida o CFOP de entrada e o CST do IPI de entrada --}
          if (bClassificaEntrada) then
          begin

              {-- checa se o CFOP de entrada � um CFOP v�lido. --}
              if trim(qryItemRecebimentocCFOP.Value) = '' then
              begin
                  MensagemAlerta('Informe o CFOP de entrada.');
                  abort;
              end;

              checaValidadeCFOP( qryItemRecebimentocCFOP.Value , TRUE ) ;

              {-- se o item tem IPI, verifica se tem um CST de entrada v�lido para o IPI --}
              if (qryItemRecebimentonValIPI.Value > 0) then
              begin

                  qryCST_IPI.Close;
                  PosicionaQuery( qryCST_IPI , qryItemRecebimentocCdSTIPIEntrada.Value ) ;

                  if ( qryCST_IPI.eof ) then
                  begin
                      MensagemAlerta('CST de Entrada do IPI inv�lido.') ;
                      abort ;
                  end
                  else
                  begin

                      {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
                      {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
                      {-- num�ricos e mantidos pela ER2.                                                         --}
                      if ( strToint( qryCST_IPIcCdStIPI.Value ) >= 50 ) then
                      begin
                          MensagemAlerta('Informe um CST de Entrada para o IPI. O CST informado � um CST de sa�da.') ;
                          abort ;
                      end ;

                  end ;

              end ;

              if (qryTerceirocFlgOptSimples.Value = 1) then
              begin

                  if (qryItemRecebimentocFlgGeraCreditoICMS.Value = 1) then
                  begin
                      MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de ICMS.') ;
                      abort ;
                  end ;

                  if (qryItemRecebimentocFlgGeraCreditoIPI.Value = 1) then
                  begin
                      MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de IPI.') ;
                      abort ;
                  end ;

              end ;

          end ;

      end ;

      qryItemRecebimento.Next ;

  end ;

  qryItemRecebimento.First ;


  // Confere os centros de custo
  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'S') and (qryMasternValDocto.Value > 0) then
  begin

      cxPageControl1.ActivePage := TabCentroCusto ;

      qryItemCC.First;

      while not qryItemCC.Eof do
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT Sum(nPercent) FROM CentroCustoItemRecebimento WHERE nCdItemRecebimento = ' + qryItemCCnCdItemRecebimento.AsString);
          qryAux.Open ;

          if qryAux.Eof or (qryAux.FieldList[0].Value <> 100) then
          begin
              qryAux.Close ;
              MensagemAlerta('O rateio de centro de custo do item � diferente de 100%.' + #13#13 + 'Item: ' + qryItemCCcNmItem.Value) ;
              exit ;
          end ;

          qryItemCC.Next;
      end ;

      qryItemCC.First;

  end ;

  // confere o tipo de pedido nos itens recebidos sem pedido.
  if (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM ItemRecebimento WHERE nCdRecebimento = ' + qryMasternCdRecebimento.asString + ' AND nCdItemRecebimentoPai IS NULL AND nCdItemPedido IS NULL AND nCdTipoPedidoRecebido IS NULL') ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin
          qryAux.Close;
          MensagemAlerta('Informe o tipo de pedido de entrada para os itens recebidos sem pedido.') ;
          cxPageControl1.ActivePageIndex := 1 ;
          abort;
      end ;

  end ;

  // Confere os lotes de fabrica��o
  qryProdLote.Close;
  PosicionaQuery(qryProdLote, qryMasternCdRecebimento.AsString) ;

  if not qryProdLote.eof then
  begin

      cxPageControl1.ActivePage := TabLote ;

      objForm := TfrmRegistroMovLote.Create( Self ) ;

      try

          qryProdLote.First;

          while not qryProdLote.Eof do
          begin

              nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryProdLotenCdProduto.Value
                                                                        , 2
                                                                        , qryProdLotenCdItemRecebimento.Value ) ;

              {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
              if (nQtdeLote <> -1) then
              begin

                  if ( nQtdeLote <> qryProdLotenQtde.Value ) then
                  begin
                      MensagemAlerta('A quantidade especificada no Registro de Lotes/Seriais para o produto: ' + qryProdLotenCdProduto.AsString + ' - ' + qryProdLotecNmProduto.Value + ' n�o confere com a quantidade recebida.') ;
                      btRegistroLoteSerial.Click;
                      abort ;
                  end ;

              end ;

              qryProdLote.Next;
          end ;

      finally
          freeAndNil( objForm ) ;
      end ;

      qryProdLote.First;

  end ;

  // confere as parcelas
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT IsNull(Sum(nValParcela),0) FROM PrazoRecebimento WHERE nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
  qryAux.Open ;

  qryItemPagavel.Close ;
  PosicionaQuery(qryItemPagavel, qryMasternCdRecebimento.AsString) ;

  if (qryItemPagavel.Eof or (qryItemPagavelnValTotal.Value = 0)) and (qryAux.FieldList[0].Value > 0) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('Este recebimento n�o tem nenhum item pag�vel, exclua as parcelas.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if ((not qryItemPagavel.Eof) and (qryItemPagavelnValTotal.Value > 0) and (qryAux.FieldList[0].Value = 0)) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('Informe as parcelas do pagamento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if (qryPrazoRecebimento.Active) and (qryPrazoRecebimento.RecordCount = 0) and (qryItemPagavelnValTotal.Value > 0) then
  begin
      MensagemAlerta('Gere as parcelas antes de finalizar o recebimento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if (abs((qryItemPagavelnValTotal.Value+qryMasternValIPI.Value+qryMasternValDespesas.Value+qryMasternValIcmsSub.Value+qryMasternValFrete.Value+qryMasternValSeguroNF.Value) - (qryAux.FieldList[0].Value )) > 1) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('O valor das parcelas � diferente da soma dos itens pag�veis do recebimento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  PulaConsistencias:

  cxPageControl1.ActivePageIndex := 0 ;

  case MessageDlg('Confirma a finaliza��o deste recebimento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_FINALIZA_RECEBIMENTO.Close ;
      SP_FINALIZA_RECEBIMENTO.Parameters.ParamByName('@nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
      SP_FINALIZA_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_FINALIZA_RECEBIMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryMaster, qryMasternCdRecebimento.asString) ;

  if (qryMasternCdTabStatusReceb.Value = 2) and (frmMenu.LeParametro('FRECDIV') = 'N') then
  begin
      MensagemAlerta('Recebimento com diverg�ncias.                    ' + #13#13 + 'Solicite libera��o.') ;
  end
  else
  begin
      ShowMessage('Recebimento finalizado com sucesso.') ;
  end ;

end;

procedure TfrmRecebimentoCompleto.btSalvarClick(Sender: TObject);
begin

  if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Recebimento j� finalizado, imposs�vel alterar.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmRecebimentoCompleto.qryCCItemRecebimentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryCCItemRecebimentonCdCC.Value > 0) and (qryCCItemRecebimentocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe um centro de custo.') ;
      abort ;
  end ;

  qryCCItemRecebimentonCdItemRecebimento.Value := qryItemCCnCdItemRecebimento.Value ;

  if (qryCCItemRecebimento.State = dsInsert) then
      qryCCItemRecebimentonCdCentroCustoItemRecebimento.Value := frmMenu.fnProximoCodigo('CENTROCUSTOITEMRECEBIMENTO') ; 

  inherited;

end;

procedure TfrmRecebimentoCompleto.qryCCItemRecebimentoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryCentroCusto, qryCCItemRecebimentonCdCC.AsString) ;

  if not qryCentroCusto.Eof then
      qryCCItemRecebimentocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmRecebimentoCompleto.cxButton4Click(Sender: TObject);
begin
  inherited;

  if qryItemCC.Eof or (DBComboItens.Text = '') then
  begin
      MensagemAlerta('Selecione o item a ser copiado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_copia_cc.Close;
      usp_copia_cc.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemCCnCdItemRecebimento.Value ;
      usp_copia_cc.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmRecebimentoCompleto.DBGridEh8KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCCItemRecebimento.State = dsBrowse) then
             qryCCItemRecebimento.Edit ;

        if (qryCCItemRecebimento.State = dsInsert) or (qryCCItemRecebimento.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryCCItemRecebimentonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
  begin

    if (cxPageControl1.ActivePage = TabCentroCusto) then
    begin
      PosicionaQuery(qryItemCC, qryMasternCdRecebimento.AsString) ;

      if not qryItemCC.Eof then
          qryItemCC.First;
    end ;

    if (cxPageControl1.ActivePage = TabLote) then
    begin
      qryProdLote.Close;
      PosicionaQuery(qryProdLote, qryMasternCdRecebimento.AsString) ;

      qryProdLote.First;

      bReadOnly := False ;

      if (qryMasternCdTabStatusReceb.Value > 2) then
      begin
          bReadOnly := True ;
      end ;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.DBComboItensChange(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCCItemRecebimento, qryItemCCnCdItemRecebimento.AsString) ;

end;

procedure TfrmRecebimentoCompleto.qryLoteItemRecebimentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryLoteItemRecebimentocNrLote.Value = '') then
  begin
      MensagemAlerta('Informe o N�mero do Lote.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtFabricacao.Value > qryLoteItemRecebimentodDtValidade.Value) or (qryLoteItemRecebimentodDtFabricacao.asString = '') or (qryLoteItemRecebimentodDtValidade.asString = '') then
  begin
      MensagemAlerta('Data de Validade/Fabrica��o Inv�lida.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtFabricacao.Value > qryMasterdDtReceb.Value) then
  begin
      MensagemAlerta('A data de fabrica��o do lote n�o pode ser maior que a data de recebimento.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtValidade.Value < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('Lote Vencido! Imposs�vel receber.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentonQtde.Value <= 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  inherited;

  qryLoteItemRecebimentocNrLote.Value        := Uppercase(qryLoteItemRecebimentocNrLote.Value) ;
  qryLoteItemRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;
  qryLoteItemRecebimentonCdProduto.Value     := qryProdLotenCdProduto.Value ;

end;

procedure TfrmRecebimentoCompleto.DBComboProdutosChange(Sender: TObject);
begin
  inherited;

  qryLoteItemRecebimento.Close ;
  qryLoteItemRecebimento.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
  PosicionaQuery(qryLoteItemRecebimento, qryProdLotenCdProduto.AsString) ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 1) then
        begin

            if (qryItemRecebimento.State = dsBrowse) then
                 qryItemRecebimento.Edit ;


            if ((qryItemRecebimento.State = dsInsert) or (qryItemRecebimento.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(68);

                If (nPK > 0) then
                begin
                    qryItemRecebimentonCdProduto.Value := nPK ;
                    qryItemRecebimentocCdProduto.Value := qryItemRecebimentonCdProduto.AsString;
                end ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOP') then
        begin
            if (qryItemRecebimento.State = dsBrowse) then
            qryItemRecebimento.Edit ;

            if ((qryItemRecebimento.State = dsInsert) or (qryItemRecebimento.State = dsEdit)) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(1011,'Convert(INTEGER,cCFOP) < 4000');
                If (nPK > 0) then
                begin
                    qryItemRecebimentocCFOP.Value := intToStr(nPk);
                end ;
            end ;

        end;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.ExibirDivergncias1Click(Sender: TObject);
var
  objForm : TfrmRecebimento_Divergencias ;
begin
  inherited;

  if (qryItemRecebimentonCdItemRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  objForm := TfrmRecebimento_Divergencias.Create( Self ) ;

  PosicionaQuery(objForm.qryDivergencias, qryItemRecebimentonCdItemRecebimento.AsString) ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimentoCompleto.ExcluirItem1Click(Sender: TObject);
begin
  inherited;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (MessageDlg('Confirma a exclus�o deste item?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryItemRecebimento.Delete;
  end ;
  
end;

procedure TfrmRecebimentoCompleto.qryTipoRecebAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.Pages[1].Enabled := True ;
  cxPageControl1.Pages[2].Enabled := True ;
  cxPageControl1.Pages[3].Enabled := True ;
  cxPageControl1.Pages[4].Enabled := True ;

  if not qryTipoReceb.Eof and (qryTipoRecebcFlgRecebCego.Value = 1) then
  begin
      cxPageControl1.Pages[1].Enabled := False ;
      cxPageControl1.Pages[2].Enabled := False ;
      cxPageControl1.Pages[3].Enabled := False ;
      cxPageControl1.Pages[4].Enabled := False ;
  end ;

  if (frmMenu.LeParametroEmpresa('LOTEPROD') <> 'S') then
  begin
      TabLote.Enabled     := False ;
      cxTabSheet5.Enabled := False ;
  end ;

end;

procedure TfrmRecebimentoCompleto.usp_GradeAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to usp_Grade.FieldList.Count-1 do
  begin
      iQtde := iQtde + usp_Grade.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.usp_GradeAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to usp_Grade.FieldList.Count-1 do
  begin
      iQtde := iQtde + usp_Grade.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.qryGradeRecebimentoAfterCancel(
  DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryGradeRecebimento.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryGradeRecebimento.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.qryGradeRecebimentoAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryGradeRecebimento.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryGradeRecebimento.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimentoCompleto.cxButton3Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a exclus�o das parcelas?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryPrazoRecebimento.Close ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM PrazoRecebimento WHERE nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.ExecSQL;

      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM HistAutorDiverg WHERE nCdTabTipoDiverg = 10 AND nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryPrazoRecebimento.Open ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryPrazoRecebimentocFlgDiverg.Value = 1) and ((DataCol >= 1) and (DataCol <= 2)) then
  begin

      DBGridEh3.Canvas.Brush.Color := $00A6FFFF;
      DBGridEh3.Canvas.Font.Color  := clBlack ;
      DBGridEh3.Canvas.FillRect(Rect);
      DBGridEh3.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

procedure TfrmRecebimentoCompleto.cxButton5Click(Sender: TObject);
var
  objRecebimento_Etiqueta : TfrmRecebimento_Etiqueta ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin
      MensagemAlerta('Finalize o recebimento antes de imprimir as etiquetas.') ;
      exit ;
  end ;

  objRecebimento_Etiqueta := TfrmRecebimento_Etiqueta.Create( Self ) ;

  objRecebimento_Etiqueta.cdsProduto.Close ;
  objRecebimento_Etiqueta.cdsProduto.Open ;

  while not objRecebimento_Etiqueta.cdsProduto.eof do
  begin
      objRecebimento_Etiqueta.cdsProduto.Delete;
      objRecebimento_Etiqueta.cdsProduto.Next ;
  end ;

  if not qryItemRecebimento.eof then
  begin

      qryItemRecebimento.First ;

      while not qryItemRecebimento.Eof do
      begin

          if (qryItemRecebimentonCdProduto.Value > 0) then
          begin
              PosicionaQuery(qryProduto, qryItemRecebimentonCdProduto.asString) ;

              objRecebimento_Etiqueta.cdsProduto.Insert;
              objRecebimento_Etiqueta.cdsProdutonCdProduto.Value         := qryItemRecebimentonCdProduto.Value ;
              objRecebimento_Etiqueta.cdsProdutocNmProduto.Value         := qryItemRecebimentocNmItem.Value    ;
              objRecebimento_Etiqueta.cdsProdutocReferencia.Value        := qryProdutocReferencia.Value        ;
              objRecebimento_Etiqueta.cdsProdutocFlgOK.Value             := 1 ;
              objRecebimento_Etiqueta.cdsProdutonCdItemRecebimento.Value := qryItemRecebimentonCdItemRecebimento.Value ;
              objRecebimento_Etiqueta.cdsProduto.Post ;
          end ;

          qryItemRecebimento.Next ;

      end ;

      qryItemRecebimento.First ;

      showForm( objRecebimento_Etiqueta , FALSE ) ;

  end ;

  freeAndNil( objRecebimento_Etiqueta ) ;

end;

function TfrmRecebimentoCompleto.gerarProduto: integer;
begin

    Sleep(100) ;
    frmMenu.limpaBufferTeclado;

    showForm( objGeraProduto , FALSE ) ;

    if (objGeraProduto.nCdProdutoGerado > 0) then
    begin

        if (qryItemRecebimento.State = dsBrowse) then
            qryItemRecebimento.Insert ;

        qryItemRecebimentonCdProduto.Value   := objGeraProduto.nCdProdutoGerado ;
        qryItemRecebimentocCdProduto.Value   := intToStr( objGeraProduto.nCdProdutoGerado );
        qryItemRecebimentonValUnitario.Value := objGeraProduto.nValCustoUnitario ;

        DBGridEh1.Col := 1 ;
        DBGridEh1.SetFocus;

        exit ;

    end ;

    Result := objGeraProduto.nCdProdutoGerado ;

end;

procedure TfrmRecebimentoCompleto.DBGridEh1KeyPress(Sender: TObject;
  var Key: Char);
var
    nValPedido : double ;
    State : TKeyboardState ;
    nPk   : integer;
begin
  inherited;

    GetKeyboardState(State);

    if (((State[vk_Control] And 128) <> 0)) and (Key = #10) then
    begin

        if (qryMaster.Active) then
            if (qryMasternCdRecebimento.Value > 0) then
                if not (DBGridEh1.ReadOnly) then
                    if (frmMenu.LeParametro('CADPRODUTORECEB') = 'S') then
                        GerarProduto() ;

    end ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimento.State = dsBrowse) then
  begin

      {-- se for um item AD ativa a edi��o, se n�o for o desativa os campos do produto --}
      ativaEdicaoProduto( (trim(qryItemRecebimentocCdProduto.Value) = 'AD') ) ;

      {-- se o recebimento j� foi processado, n�o permite a edi��o do nome do produto --}
      if (qryMasternCdTabStatusReceb.Value >= 3) then
          ativaEdicaoProduto( FALSE ) ;

      verificaControlesICMS( qryItemRecebimentocCdST.Value ) ;

  end ;

end;

procedure TfrmRecebimentoCompleto.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  ExibirDivergncias1.Enabled   := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0)) ;
  ExcluirItem1.Enabled         := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0) and (qryMasternCdTabStatusReceb.Value <= 2)) ;
  VincularPedido1.Enabled      := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0) and (qryMasternCdTabStatusReceb.Value <= 2)) ;

  RemoverVnculoPedido1.Enabled := (    (qryItemRecebimento.Active)
                                   and (qryItemRecebimentonCdItemRecebimento.Value  > 0)
                                   and (qryItemRecebimentonCdItemPedido.Value       > 0)
                                   and (qryMasternCdTabStatusReceb.Value           <= 2)) ;

end;

procedure TfrmRecebimentoCompleto.vinculaPedidoitemRecebimento(nCdItemRecebimento:integer;nCdProduto:integer);
begin

    cxPageControl1.ActivePageIndex := 0 ;

    objConsultaItemPedidoAberto.nCdTerceiro    := qryMasternCdTerceiro.Value ;
    objConsultaItemPedidoAberto.nCdTipoReceb   := qryMasternCdTipoReceb.Value ;
    objConsultaItemPedidoAberto.nCdLoja        := qryMasternCdLoja.Value ;
    objConsultaItemPedidoAberto.nCdRecebimento := qryMasternCdRecebimento.Value ;
    objConsultaItemPedidoAberto.nCdPedido      := 0 ;
    objConsultaItemPedidoAberto.nCdProduto     := nCdProduto ;

    showForm( objConsultaItemPedidoAberto , FALSE ) ;

    {-- se foi selecionado algum item do pedido , vincula ao item do recebimento --}
    if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
    begin

        if (MessageDlg('Confirma a vincula��o deste item de recebimento com o pedido ' + objConsultaItemPedidoAberto.qryPedidonCdPedido.asString + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
            exit ;

        frmMenu.Connection.BeginTrans;

        try
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Close;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemRecebimento').Value := nCdItemRecebimento;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.') ;
            raise ;
        end ;

        frmMenu.Connection.CommitTrans ;

        ShowMessage('Vincula��o processada com sucesso.') ;

        qryItemRecebimento.Requery();

    end ;

end;

procedure TfrmRecebimentoCompleto.VincularPedido1Click(Sender: TObject);
begin

    vinculaPedidoitemRecebimento(qryItemRecebimentonCdItemRecebimento.Value, qryItemRecebimentonCdProduto.Value) ;

end;

procedure TfrmRecebimentoCompleto.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      {-- se for um inteiro, retira os zeros da esquerda e manter somente os numeros --}
      try
          qryMastercNrDocto.Value := intToStr( StrToInt( Trim( Uppercase( qryMastercNrDocto.Value ) ) ) ) ;
      except
      end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.verificaControlesICMS(cCST: string);
var
    bICMS   , bICMSST    : boolean ;
    bICMSRed, bICMSSTRed : boolean ;
    i                    : integer ;

begin

    if (not qryMaster.Active) then
        exit ;

    if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 0) then
    begin

        if (qryItemRecebimentonPercICMSSub.Value = 0) and (qryItemRecebimento.State in [dsInsert,dsEdit]) then
        begin
             qryItemRecebimentonValBaseCalcSubTrib.Value     := 0;
             qryItemRecebimentonValICMSSub.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
        end ;

        exit ;
    end ;

    bICMS   := true ;
    bICMSST := false ;

    if (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '40') or (trim(RightStr(cCST,2)) = '41') or (trim(RightStr(cCST,2)) = '50') then
        bICMS := false ;

    if (trim(RightStr(cCST,2)) = '10') or (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '70') then
    begin
        bICMSST := true ;
    end ;

    bICMSRed   := (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '70') ;
    bICMSSTRed := (trim(RightStr(cCST,2)) = '70') ;

    // formata as colunas do grid
    with DBGridEh1 do
    begin

        {-- ICMS NORMAL --}
        // % reducao base calc icms
        Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly := (not bICMSRed) ;

        if (bICMSRed)  then
            Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Title.Font.Color := clBlack
        else
            Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Title.Font.Color := clRed ;


        {-- as colunas de base de c�lculo do ICMS de Valor do ICMS SEMPRE ficam somente leitura --}
        Columns[FieldColumns['nBaseCalcICMS'].Index].ReadOnly := TRUE ;
        Columns[FieldColumns['nValICMS'].Index].ReadOnly      := TRUE ;

        Columns[FieldColumns['nBaseCalcICMS'].Index].Title.Font.Color := clRed  ;
        Columns[FieldColumns['nValICMS'].Index].Title.Font.Color      := clRed ;

        {-- *** Substitui��o Tributaria *** --}
        {-- se bICMSST for TRUE, indica que o CST do item tem substitui��o tribut�ria --}
        {-- quando bICMSST for TRUE, ele precisa deixar o ReadOnly como false, por isso � atribuido o valor de (not bICMSST) --}

        Columns[FieldColumns['nPercIVA'].Index].ReadOnly            := (not bICMSST) ;
        Columns[FieldColumns['nAliqICMSInterna'].Index].ReadOnly    := (not bICMSST) ;
        Columns[FieldColumns['nValBaseCalcSubTrib'].Index].ReadOnly := (not bICMSST) ;
        Columns[FieldColumns['nValICMSSub'].Index].ReadOnly         := (not bICMSST) ;

        if (bICMSST)  then
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color            := clBlack ;
            Columns[FieldColumns['nAliqICMSInterna'].Index].Title.Font.Color    := clBlack ;
            Columns[FieldColumns['nValBaseCalcSubTrib'].Index].Title.Font.Color := clBlack ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color         := clBlack ;

        end
        else
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color            := clRed ;
            Columns[FieldColumns['nAliqICMSInterna'].Index].Title.Font.Color    := clRed ;
            Columns[FieldColumns['nValBaseCalcSubTrib'].Index].Title.Font.Color := clRed ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color         := clRed ;

        end ;

    end ;

    if (qryItemRecebimento.State in [dsInsert,dsEdit]) then
    begin
         if not (bICMS) Then
         Begin
             qryItemRecebimentonPercAliqICMS.Value        := 0;
             qryItemRecebimentonBaseCalcICMS.Value        := 0;
             qryItemRecebimentonValICMS.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcICMS.Value := 0;
         end;

         if not (bICMSRed) then
             qryItemRecebimentonPercRedBaseCalcICMS.Value := 0;

         if not (bICMSST) then
         begin
             qryItemRecebimentonPercIVA.Value                := 0;
             qryItemRecebimentonAliqICMSInterna.Value        := 0;
             qryItemRecebimentonValBaseCalcSubTrib.Value     := 0;
             qryItemRecebimentonPercICMSSub.Value            := 0;
             qryItemRecebimentonValICMSSub.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
         end ;

         if not (bICMSSTRed) then
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
    end;

    calculaTotaisItem( FALSE ) ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentocCdSTChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCdST.Value <> '') then
  begin

      if (Length(qryItemRecebimentocCdST.Value) <> 3) then
      begin
          MensagemAlerta('O CST do item deve ter 3 d�gitos. Ex: 010, 020') ;
          abort ;
      end ;
      
      PosicionaQuery(qryTipoTributacaoICMS_Aux,RightStr(qryItemRecebimentocCdSt.AsString,2));

      if qryTipoTributacaoICMS_Aux.eof then
      begin
          MensagemAlerta('C�digo de Situa��o Tribut�ria Inv�lido');
          abort;
      end;

      verificaControlesICMS( qryItemRecebimentocCdST.Value ) ;

      {-- verifica os par�metros do produto x fornecedor para j� sugerir algumas informa��es --}
      if     (qryItemRecebimentonCdTipoItemPed.Value <> 5) {-- se n�o for item AD --}
         and (qryItemRecebimento.State = dsInsert) then    {-- se estiver inserindo o registro --}
      begin

        qryProdutoFornecedor.Close;
        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            {-- traz os valores pre-definidos no cadastro do fornecedor --}
            qryItemRecebimentonPercIPI.Value         := qryProdutoFornecedornPercIPI.Value ;
            qryItemRecebimentonPercIVA.Value         := qryProdutoFornecedornPercIVA.Value ;
            qryItemRecebimentonAliqICMSInterna.Value := qryProdutoFornecedornPercICMSSub.Value ;

            if (qryItemRecebimentonPercIPI.Value > 0) then
            begin
                qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;
            end ;

            {-- calcula a base do ICMS --}
            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly) then
            begin
                MensagemAlerta('O produto est� vinculado ao fornecedor com redu��o na base de c�lculo do ICMS. O CST digitado para o item n�o permite redu��o na base. Verifique.') ;
                DBGridEh1.Col := 6;
                abort ;
            end ;

            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (not DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly) then
            begin
                qryItemRecebimentonPercRedBaseCalcICMS.Value := qryProdutoFornecedornPercBCICMSSub.Value ;
            end ;

            qryItemRecebimentonBaseCalcICMS.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)) ;

        end ;

      end;

  end;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentocCFOPChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCFOP.Value <> '') then
  begin

      checaValidadeCFOP( qryItemRecebimentocCFOP.Value , TRUE ) ;

      PosicionaQuery(qryCfop,qryItemRecebimentocCfop.AsString);

      qryItemRecebimentocFlgGeraLivroFiscal.Value    := qryCFOPcFlgGeraLivroFiscal.Value;
      qryItemRecebimentocFlgGeraCreditoICMS.Value    := qryCFOPcFlgGeraCreditoICMS.Value;
      qryItemRecebimentocFlgGeraCreditoIPI.Value     := qryCFOPcFlgGeraCreditoIPI.Value;
      qryItemRecebimentocFlgImportacao.Value         := qryCFOPcFlgImportacao.Value;

      {-- se o terceiro for optante pelo simples, n�o gera credito de ICMS nem IPI --}
      if (qryTerceirocFlgOptSimples.Value = 1) then
      begin
          qryItemRecebimentocFlgGeraCreditoICMS.Value := 0;
          qryItemRecebimentocFlgGeraCreditoIPI.Value  := 0;
      end ;

      {-- verifica os par�metros do produto x fornecedor para j� sugerir algumas informa��es --}
      if     (qryItemRecebimentonCdTipoItemPed.Value <> 5)   {-- se n�o for item AD --}
         and (qryItemRecebimento.State = dsInsert) then
      begin

        qryProdutoFornecedor.Close;
        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            qryItemRecebimentonPercIPI.Value := qryProdutoFornecedornPercIPI.Value ;

            if (qryItemRecebimentonPercIPI.Value > 0) then
            begin
                qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;
            end ;


            {-- calcula a base do ICMS --}
            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly) then
            begin
                MensagemAlerta('O produto est� vinculado ao fornecedor com redu��o na base de c�lculo do ICMS. O CST digitado para o item n�o permite redu��o na base. Verifique.') ;
                DBGridEh1.Col := 6;
                abort ;
            end ;

            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (not DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly) then
            begin
                qryItemRecebimentonPercRedBaseCalcICMS.Value := qryProdutoFornecedornPercBCICMSSub.Value ;
            end ;

            qryItemRecebimentonBaseCalcICMS.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)) ;

        end ;

      end;

  end;

end;

procedure TfrmRecebimentoCompleto.DBGridEh4DblClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryProdLotenCdProduto.Value
                         , 0
                         , 2 {-- Tipo de movimenta��o que esta sendo realizada --}
                         , qryProdLotenCdItemRecebimento.Value
                         , 0 {-- o local de estoque sera ajustado atraves da procedure --}
                         , qryProdLotenQtde.Value
                         , 'RECEBIMENTO DE MERCADORIAS - ITEM No. ' + Trim( qryProdLotenCdItemRecebimento.AsString )
                         , True
                         , bReadOnly
                         , False) ;

  freeAndNil( objForm ) ;
end;

procedure TfrmRecebimentoCompleto.btRegistroLoteSerialClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  if (qryProdLote.Active) and (qryProdLote.RecordCount > 0) then
  begin

      objForm := TfrmRegistroMovLote.Create( Self ) ;

      objForm.registraMovLote( qryProdLotenCdProduto.Value
                             , 0
                             , 2 {-- Tipo de movimenta��o que esta sendo realizada --}
                             , qryProdLotenCdItemRecebimento.Value
                             , 0 {-- o local de estoque sera ajustado atraves da procedure --}
                             , qryProdLotenQtde.Value
                             , 'RECEBIMENTO DE MERCADORIAS - ITEM No. ' + Trim( qryProdLotenCdItemRecebimento.AsString )
                             , True
                             , bReadOnly
                             , False) ;

      freeAndNil( objForm ) ;

  end ;

end;

procedure TfrmRecebimentoCompleto.cxButton7Click(Sender: TObject);
var
   objLerXML : TfrmRecebimentoLeituraXML ;
   i : integer;
begin
  inherited;

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.');
      abort;
  end;

  if (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Status do Recebimento n�o permite importar XML.');
      abort;
  end;

  if (qryTipoReceb.Eof) then
  begin
      MensagemAlerta('Informe o Tipo de Recebimento.');
      abort;
  end;

  if Trim(DBEdit7.Text) = '' then
  begin
      MensagemAlerta('Informe o N�mero da NFe antes de importar o XML.');
      DBEdit7.setFocus;
      abort;
  end;

  if (not qryItemRecebimento.isEmpty) then
  begin
      MensagemAlerta('Exclua os itens do recebimento antes de processar a leitura do arquivo XML.');
      DBGridEh1.SetFocus;
      Exit;
  end;

  objLerXML := TfrmRecebimentoLeituraXML.Create( Self ) ;

  OpenDialog1.FileName  :=  '';
  OpenDialog1.Title := 'Selecione um arquivo NFE XML';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Arquivos TXT (*.TXT)|*.TXT|Todos os Arquivos (*.*)|*.*';

  if OpenDialog1.Execute then
  begin

      if (OpenDialog1.FileName <> '') then
      begin

          //tenta XML
          ACBrNFe1.NotasFiscais.Clear;
          try
              ACBrNFe1.NotasFiscais.LoadFromFile(OpenDialog1.FileName);
          except
              MensagemAlerta('Arquivo NFe Inv�lido.');
              exit;
          end;

      end
      else
      begin
          exit ;
      end ;

  end
  else
  begin

      exit ;

  end ;

  with ACBrNFe1.NotasFiscais.Items[0].NFe do
  begin

     objLerXML.qryTerceiro.Close;
     objLerXML.qryTerceiro.Parameters.ParamByName('cCnpjCpf').Value := Trim(emit.CNPJCPF);
     objLerXML.qryTerceiro.Open;

     if Trim(DBEdit7.Text) <> Trim(intToStr(ide.nNF)) then
     begin
         MensagemAlerta('O N�mero da NF do XML � diferente do N�mero da NF informado neste recebimento. NF Selecionada: ' + intToStr(ide.nNF));
         DBEdit7.setFocus;
         abort;
     end;

     objLerXML.qryNFeXML.Close;
     objLerXML.qryNFeXML.ExecSQL;
     objLerXML.qryNFeXML.Open;

     with ACBrNFe1.NotasFiscais.Items[0].NFe do
     begin

         qryBuscaTerceiroCNPJ.Close;
         PosicionaQuery( qryBuscaTerceiroCNPJ , emit.CNPJCPF ) ;

         if ( qryBuscaTerceiroCNPJ.eof ) then
         begin
             MensagemAlerta('Nenhum terceiro cadastrado com o CNPJ emissor da NFe. CNPJ : ' + emit.CNPJCPF ) ;
             abort ;
         end ;

         {-- atualiza a capa do recebimento --}
         qryMaster.Edit;
         qryMastercSerieDocto.Value         := intTostr( ide.serie ) ;
         qryMastercModeloNF.Value           := intToStr( ide.modelo ) ;
         qryMasternCdTerceiro.Value         := qryBuscaTerceiroCNPJnCdTerceiro.Value ;
         qryMasterdDtDocto.Value            := ide.dEmi ;
         qryMastercCNPJEmissor.Value        := emit.CNPJCPF;
         qryMastercIEEmissor.Value          := emit.IE;
         qryMastercUFEmissor.Value          := emit.EnderEmit.UF;
         qryMasternValDocto.Value           := total.ICMSTot.vProd ;
         qryMasternValTotalNF.Value         := total.ICMSTot.vNF ;
         qryMasternValBaseICMS.Value        := total.ICMSTot.vBC ;
         qryMasternValICMS.Value            := total.ICMSTot.vICMS ;
         qryMasternValIPI.Value             := total.ICMSTot.vIPI ;
         qryMasternValDescontoNF.Value      := total.ICMSTot.vDesc ;
         qryMasternValICMSSub.Value         := total.ICMSTot.vST ;
         qryMasternValDespesas.Value        := total.ICMSTot.vOutro ;
         qryMasternValFrete.Value           := total.ICMSTot.vFrete ;
         qryMastercChaveNFe.Value           := Copy(infNFe.ID,4,(length(infNFe.ID))) ;
         qryMasternValBaseCalcICMSSub.Value := total.ICMSTot.vBCST ;
         qryMasternValSeguroNF.Value        := total.ICMSTot.vSeg ;
         qryMastercUFOrigemNF.Value         := emit.EnderEmit.UF ;
         qryMasternValPIS.Value             := total.ICMSTot.vPIS ;
         qryMasternValCOFINS.Value          := total.ICMSTot.vCOFINS ;
         qryMasternValII.Value              := total.ICMSTot.vII ;

         qryTerceiro.Close;
         qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;
         PosicionaQuery( qryTerceiro, qryMasternCdTerceiro.asString ) ;

         qryTesteDupNF.Close ;
         qryTesteDupNF.Parameters.ParamByName('cNrDocto').Value       := DBEdit7.Text ;
         qryTesteDupNF.Parameters.ParamByName('cSerieDocto').Value    := qryMastercSerieDocto.Text ;
         qryTesteDupNF.Parameters.ParamByName('nCdTerceiro').Value    := qryTerceironCdTerceiro.Value ;
         qryTesteDupNF.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
         qryTesteDupNF.Open ;

         if not qryTesteDUPnf.Eof then
             MensagemAlerta('Esta nota fiscal j� foi recebida no recebimento No : ' + qryTesteDupNFnCdRecebimento.AsString) ;

         qryTesteDupNf.Close ;

         qryMaster.Post;

         {-- carrega os itens do recebimento --}

         for I := 0 to Det.Count-1 do
         begin

             with Det.Items[I] do
             begin

                 objLerXML.qryNFeXML.Insert;
                 objLerXML.qryNFeXMLnCdRecebimento.Value       := qryMasternCdRecebimento.Value;
                 objLerXML.qryNFeXMLcCdProdutoFornecedor.Value := Prod.cProd;
                 objLerXML.qryNFeXMLcEAN.Value                 := Prod.cEAN;
                 objLerXML.qryNFeXMLcNmProdutoFornecedor.Value := Prod.xProd;
                 objLerXML.qryNFeXMLnQtdeFornecedor.Value      := Prod.qCom;
                 objLerXML.qryNFeXMLcUnMedidaFornecedor.Value  := Prod.uCom;
                 objLerXML.qryNFeXMLnValUnitario.Value         := Prod.vUnCom;
                 objLerXML.qryNFeXMLnValTotal.Value            := Prod.vProd;
                 objLerXML.qryNFeXMLnValDesconto.Value         := Prod.vDesc;
                 objLerXML.qryNFeXMLcNCM.Value                 := Prod.NCM;

                 objLerXML.qryNFeXMLnValSeguro.Value           := Prod.vSeg;
                 objLerXML.qryNFeXMLnValDespAcessorias.Value   := Prod.vOutro;
                 objLerXML.qryNFeXMLnValFrete.Value            := Prod.vFrete;

                 objLerXML.qryNFeXMLcOrigemMercadoria.Value    := OrigToStr( Imposto.ICMS.orig ); 
                 objLerXML.qryNFeXMLcCdST.Value                := CSTICMSToStrTagPos( Imposto.ICMS.CST );
                 objLerXML.qryNFeXMLnValBaseICMS.Value         := Imposto.ICMS.vBC;
                 objLerXML.qryNFeXMLnValICMS.Value             := Imposto.ICMS.vICMS;
                 objLerXML.qryNFeXMLnPercAliqICMS.Value        := Imposto.ICMS.pICMS;

                 if (Imposto.ICMS.pRedBC > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseIcms.Value := (100 - Imposto.ICMS.pRedBC) ;

                 if (Imposto.ICMS.pRedBCST > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseICMSSt.Value := (100 - Imposto.ICMS.pRedBCST) ;

                 objLerXML.qryNFeXMLnValBaseICMSST.Value  := Imposto.ICMS.vBCST;
                 objLerXML.qryNFeXMLnValICMSST.Value      := Imposto.ICMS.vICMSST;
                 objLerXML.qryNFeXMLnpercAliqICMSST.Value := Imposto.ICMS.pICMSST;
                 objLerXML.qryNFeXMLnPercIVA.Value        := Imposto.ICMS.pMVAST;

                 objLerXML.qryNFeXMLnValBaseIPI.Value     := Imposto.IPI.vBC ;
                 objLerXML.qryNFeXMLnPercAliqIPI.Value    := Imposto.IPI.pIPI;
                 objLerXML.qryNFeXMLnValIPI.Value         := Imposto.IPI.vIPI;
                 objLerXML.qryNFeXMLcCSTIPI.Value         := CSTIPIToStr( Imposto.IPI.CST) ;

                 objLerXML.qryNFeXMLcCdSt.Value           := CSTICMSToStr(Imposto.ICMS.CST);
                 objLerXML.qryNFeXMLcCFOP.Value           := Prod.CFOP;

                 objLerXML.qryNFeXMLcCSTPIS.Value         := CSTPISToStr( Imposto.PIS.CST ) ;
                 objLerXML.qryNFeXMLnValBasePIS.Value     := Imposto.PIS.vBC ;
                 objLerXML.qryNFeXMLnAliqPIS.Value        := Imposto.PIS.pPIS ;
                 objLerXML.qryNFeXMLnValPIS.Value         := Imposto.PIS.vPIS ;
                 objLerXML.qryNFeXMLcCSTCOFINS.Value      := CSTCOFINSToStr( Imposto.COFINS.CST ) ;
                 objLerXML.qryNFeXMLnValBaseCOFINS.Value  := Imposto.COFINS.vBC ;
                 objLerXML.qryNFeXMLnAliqCOFINS.Value     := Imposto.COFINS.pCOFINS ;
                 objLerXML.qryNFeXMLnValCOFINS.Value      := Imposto.COFINS.vCOFINS ;
                 objLerXML.qryNFeXMLnValBaseII.Value      := Imposto.II.vBc ;
                 objLerXML.qryNFeXMLnValDespAdu.Value     := Imposto.II.vDespAdu ;
                 objLerXML.qryNFeXMLnValII.Value          := Imposto.II.vII ;
                 objLerXML.qryNFeXMLnValIOFImp.Value      := Imposto.II.vIOF ;

                 {-- primeiro procura pelo c�digo EAN do produto para ver se encontra no cadastro de produtos --}
                 if (objLerXML.qryNFeXMLcEAN.Value <> '') then
                 begin

                     objLerXML.qryProdutoEAN.Close;
                     objLerXML.PosicionaQuery( objLerXML.qryProdutoEAN , objLerXML.qryNFeXMLcEAN.Value);

                     if (not objLerXML.qryProdutoEAN.eof) then
                     begin
                         objLerXML.qryNFeXMLnCdProduto.Value := objLerXML.qryProdutoEANnCdProduto.Value;
                         objLerXML.qryNFeXMLcNmProduto.Value := objLerXML.qryProdutoEANcNmProduto.Value;
                         objLerXML.qryNFeXMLcUnMedida.Value  := objLerXML.qryProdutoEANcUnidadeMedida.Value;
                     end ;

                 end ;

                 if (objLerXML.qryNFeXMLnCdProduto.Value = 0) then
                 begin

                     objLerXML.qryProdutoFornecedor.Close;
                     objLerXML.qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := Prod.cProd;
                     objLerXML.qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
                     objLerXML.qryProdutoFornecedor.Open;

                     if not objLerXML.qryProdutoFornecedor.eof then
                     begin

                         objLerXML.qryProduto.Close;
                         objLerXML.qryProduto.Parameters.ParamByName('nPk').Value := objLerXML.qryProdutoFornecedornCdProduto.Value;
                         objLerXML.qryProduto.Open;

                         if not objLerXML.qryProduto.eof then
                         begin

                             objLerXML.qryNFeXMLnCdProduto.Value           := objLerXML.qryProdutonCdProduto.Value;
                             objLerXML.qryNFeXMLcNmProduto.Value           := objLerXML.qryProdutocNmProduto.Value;
                             objLerXML.qryNFeXMLcUnMedida.Value            := objLerXML.qryProdutocUnidadeMedida.Value;

                         end ;

                     end ;

                 end ;

                 objLerXML.qryNFeXML.Post;

             end;

         end;

     end;

     objLerXML.qryNFeXML.Open;
     objLerXML.nCdRecebimento := qryMasternCdRecebimento.Value ;

     showForm( objLerXML , TRUE ) ;

     PosicionaQuery( qryMaster ,qryMasternCdRecebimento.AsString );

     gravaXMLNFe( OpenDialog1.FileName , FALSE ) ;

 end;

end;

procedure TfrmRecebimentoCompleto.atualizaColunasInformacoesFiscais;
begin

  if qryTipoReceb.Eof then
      exit;

  {-- quando o tipo de recebimento n�o exigir informa��es fiscais, oculta v�rias colunas --}
  with DBGridEh1 do
  begin

        Columns[FieldColumns['nBaseCalcICMS'].Index].Visible        := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercAliqICMS'].Index].Visible        := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nValICMS'].Index].Visible             := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercIVA'].Index].Visible             := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nAliqICMSInterna'].Index].Visible     := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Visible := (qryTipoRecebcFlgExigeNF.Value = 1) ;

        Columns[FieldColumns['cCdST'].Index].Visible                := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cCFOP'].Index].Visible                := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cCFOPNF'].Index].Visible              := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cNCM'].Index].Visible                 := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraLivroFiscal'].Index].Visible  := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraCreditoICMS'].Index].Visible  := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraCreditoIPI'].Index].Visible   := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgImportacao'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValFrete'].Index].Visible            := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValSeguro'].Index].Visible           := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValAliquotaII'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValAcessorias'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;

  end ;

end;

procedure TfrmRecebimentoCompleto.sugerirCalculoST (bCalcularIPI : boolean) ;
begin


    if (qryItemRecebimento.Active) and (qryItemRecebimentonCdProduto.Value > 0) then
    begin

        if (qryItemRecebimento.State = dsBrowse) then
            exit ;

        qryProdutoFornecedor.Close;

        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            if (bCalcularIPI) then
            begin
                qryItemRecebimentonPercIPI.Value := qryProdutoFornecedornPercIPI.Value ;

                if (qryItemRecebimentonPercIPI.Value > 0) then
                begin
                    qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;
                end ;
            end ;

            {-- calcula a base da ST --}

            qryItemRecebimentonValBaseCalcSubTrib.Value := (qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value) * (1 + (qryItemRecebimentonPercIVA.Value/100)) ;
            qryItemRecebimentonValICMSSub.Value         := (qryItemRecebimentonValBaseCalcSubTrib.Value * (qryItemRecebimentonAliqICMSInterna.Value / 100)) - qryItemRecebimentonValICMS.Value ;
            qryItemRecebimentonPercICMSSub.Value        := qryItemRecebimentonPercAliqICMS.Value ;

        end ;

    end ;

end;

procedure TfrmRecebimentoCompleto.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  {-- se o tipo de recebimento exige os dados fiscais, obrigar informar a UF de origem da NF de entrada --}
  if (qryMaster.Active) and (DBEdit17.Text <> '') then
  begin

      if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
      begin

          if (pos('#',DBEdit12.Text) > 0) or (pos( trim(DBEdit12.Text) , frmMenu.estadosBrasileiros ) = 0) then
          begin
              MensagemAlerta('Unidade federativa inv�lida. Ex: SP / MG / RJ. Utilize EX para notas de outros pa�ses.') ;
              DBEdit12.SetFocus;
              abort ;
          end;

      end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.checaValidadeCFOP( cCFOP : string ; bCFOPEntrada : boolean );
begin
    PosicionaQuery( qryCFOP , cCFOP );

    if qryCFOP.Eof then
    begin
        MensagemAlerta('CFOP ' + cCFOP + ' n�o cadastrado.');
        abort;
    end;

    //Os CFOP�s de Entrada Iniciam com 1,2,3 e os CFOP�s de sa�da iniciam com 5,6,7.

    if (Pos(Copy(cCFOP,1,1),'#1#2#3#') = 0) and (bCFOPEntrada) then
    begin
        MensagemAlerta('CFOP inv�lido para a opera��o. Os CFOP�s de Entrada iniciam-se com 1,2 ou 3.');
       abort;
    end;

    if (Pos(Copy(cCFOP,1,1),'#5#6#7#') = 0) and (not bCFOPEntrada) then
    begin
        MensagemAlerta('CFOP inv�lido para a opera��o. Os CFOP�s de Sa�da (NF) iniciam-se com 5,6 ou 7.');
       abort;
    end;

    PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);

    if not qryEnderecoEmpresa.Eof then
    begin

        {-- se for uma opera��o interestadual, verifica se o CFOP come�a com 5 ou 1, caso comece avisa que est� errado --}
        {-- todo CFOP de opera��o fora do estado deve iniciar com 2,3 para entrada e 6,7 para sa�da                    --}
        if (DBEdit12.Text <> qryEnderecoEmpresacUf.Value) and ( ( Copy(cCFOP,1,1) = '1' ) or ( Copy(cCFOP,1,1) = '5' ) ) then
        begin
            MensagemAlerta('O CFOP informado � um CFOP para opera��es dentro do estado. Utilize um CFOP para opera��es de fora do estado.');
            abort;
        end;

        {-- se for uma opera��o interestadual, verifica se o CFOP come�a com 5 ou 1, caso comece avisa que est� errado --}
        {-- todo CFOP de opera��o fora do estado deve iniciar com 2,3 para entrada e 6,7 para sa�da                    --}
        if (DBEdit12.Text = qryEnderecoEmpresacUf.Value) and ( ( Copy(cCFOP,1,1) <> '1' ) and ( Copy(cCFOP,1,1) <> '5' ) ) then
        begin
            MensagemAlerta('O CFOP informado � um CFOP para opera��es fora do estado. Utilize um CFOP para opera��es de dentro do estado.');
            abort;
        end;

    end;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebimentocCFOPNFChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCFOP.Value <> '') then
      checaValidadeCFOP( qryItemRecebimentocCFOPNF.Value , FALSE ) ;

end;

procedure TfrmRecebimentoCompleto.calculaTotaisItem(bGravar: boolean);
begin

  if (qryItemRecebimento.State in ([dsInsert,dsEdit])) then
  begin

      {-- TOTAL DO ITEM --}
      qryItemRecebimentonValTotal.Value := qryItemRecebimentonValUnitario.Value * qryItemRecebimentonQtde.Value ;
      qryItemRecebimentonValTotal.Value := frmMenu.TBRound(qryItemRecebimentonValTotal.Value,2);

      //Calcula o desconto no item conforme percentual do desconto no valor dos produtos da nota
      if (qryItemRecebimentonPercDesconto.Value = 0) and (qryMasternPercDescProduto.Value > 0) then
      begin
          qryItemRecebimentonPercDesconto.Value := qryMasternPercDescProduto.Value ;
          qryItemRecebimentonValDesconto.Value  := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercDesconto.Value/100)) ;
      end ;

      // desconto
      qryItemRecebimentonValDesconto.Value := 0 ;

      if (qryItemRecebimentonPercDesconto.Value > 0) then
          qryItemRecebimentonValDesconto.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercDesconto.Value/100)) ;

      {-- ICMS NORMAL --}
      if (qryItemRecebimentonPercAliqICMS.Value <> 0) then
      begin

          if (qryItemRecebimentonPercRedBaseCalcICMS.Value <> 0) then
              qryItemRecebimentonBaseCalcICMS.Value  := ( qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ) * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)
          else qryItemRecebimentonBaseCalcICMS.Value := ( qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ) ;

          qryItemRecebimentonValICMS.Value := qryItemRecebimentonBaseCalcICMS.Value * (qryItemRecebimentonPercAliqICMS.Value / 100);

      end
      else
      begin

          qryItemRecebimentonValICMS.Value := 0 ;

          if (bGravar) then
          begin
              qryItemRecebimentonBaseCalcICMS.Value        := 0 ;
              qryItemRecebimentonPercRedBaseCalcICMS.Value := 0 ;
          end ;

      end ;


      {-- IPI --}
      if (qryItemRecebimentonPercIPI.Value > 0) then
      begin

          if (qryItemRecebimentonPercBaseCalcIPI.Value > 0) then
              qryItemRecebimentonValBaseIPI.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercBaseCalcIPI.Value / 100))
          else qryItemRecebimentonValBaseIPI.Value := qryItemRecebimentonValTotal.Value ;

          qryItemRecebimentonValIPI.Value  := (qryItemRecebimentonValBaseIPI.Value * (qryItemRecebimentonPercIPI.Value/100)) ;

      end
      else
      begin
          qryItemRecebimentonValIPI.Value := 0 ;

          if (bGravar) then
          begin
              qryItemRecebimentocCdSTIPI.Value         := '' ;
              qryItemRecebimentocCdSTIPIEntrada.Value  := '' ;
              qryItemRecebimentonPercBaseCalcIPI.Value := 0  ;
              qryItemRecebimentonValBaseIPI.Value      := 0  ;
          end ;

      end ;

      {-- ICMS ST --}
      if not DBGridEh1.Columns[DBGridEh1.FieldColumns['nValBaseCalcSubTrib'].Index].ReadOnly then
      begin

          if (qryItemRecebimentonAliqICMSInterna.Value > 0) then
          begin

              qryItemRecebimentonValBaseCalcSubTrib.Value := (qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value) * (1 + (qryItemRecebimentonPercIVA.Value/100)) ;
              qryItemRecebimentonValICMSSub.Value         := (qryItemRecebimentonValBaseCalcSubTrib.Value * (qryItemRecebimentonAliqICMSInterna.Value / 100)) - qryItemRecebimentonValICMS.Value ;
              qryItemRecebimentonPercICMSSub.Value        := qryItemRecebimentonPercAliqICMS.Value ;

              if (qryItemRecebimentonPercIVA.Value = 0) and (bGravar) then
              begin
                  qryItemRecebimentonValBaseCalcSubTrib.Value := 0 ;
                  qryItemRecebimentonValICMSSub.Value         := 0 ;
                  qryItemRecebimentonPercICMSSub.Value        := 0 ;
              end ;

          end ;


      end ;

      {-- PIS --}
      if (qryItemRecebimentonAliqPIS.Value = 0) then
      begin

            if (bGravar) then
            begin
                if (qryItemRecebimentonValBasePIS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do PIS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemRecebimentonValPIS.Value := 0 ;
                qryItemRecebimentocCSTPIS.Value := '' ;

            end ;

        end
        else
        begin

            qryItemRecebimentonValBasePIS.Value := qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value ;
            qryItemRecebimentonValPIS.Value     := (qryItemRecebimentonValBasePIS.Value * (qryItemRecebimentonAliqPIS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemRecebimentocCSTPIS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do PIS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemRecebimentocCSTPIS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do PIS inv�lido.') ;
                    abort ;
                end ;

            end ;

      end ;


        {-- COFINS --}
        if (qryItemRecebimentonAliqCOFINS.Value = 0) then
        begin

            if (bGravar) then
            begin
                if (qryItemRecebimentonValBaseCOFINS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do COFINS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemRecebimentonValCOFINS.Value := 0 ;
                qryItemRecebimentocCSTCOFINS.Value := '' ;

            end ;

        end
        else
        begin

            qryItemRecebimentonValBaseCOFINS.Value := qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value ;
            qryItemRecebimentonValCOFINS.Value     := (qryItemRecebimentonValBaseCOFINS.Value * (qryItemRecebimentonAliqCOFINS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemRecebimentocCSTCOFINS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do COFINS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemRecebimentocCSTCOFINS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do COFINS inv�lido.') ;
                    abort ;
                end ;

            end ;

        end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.ativaEdicaoProduto(
  bPermitirEditar: boolean);
begin

    if (bPermitirEditar) then
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := False ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clWhite ;
    end
    else
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clRed ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := True ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clSilver ;
    end ;

end;

procedure TfrmRecebimentoCompleto.RemoverVnculoPedido1Click(
  Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a exclus�o do v�nculo deste item do recebimento com o pedido ' + qryItemRecebimentonCdPedido.AsString + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNO) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Close;
      SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
      SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := 0; {-- Indica que ir� remover o v�nculo --}
      SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Vincula��o removida com sucesso.') ;

  qryItemRecebimento.Requery();

end;

procedure TfrmRecebimentoCompleto.VincularMltiplosPedidos1Click(
  Sender: TObject);
var
  objForm : TfrmRecebimentoCompletoMultiplosPedidos ;
begin
  inherited;

  if (qryItemRecebimentonCdItemPedido.Value > 0) then
  begin
      MensagemAlerta('Antes de utilizar o v�nculos de m�ltiplos pedidos, remova o v�nculo atual.') ;
      abort ; 
  end ;

  objForm := TfrmRecebimentoCompletoMultiplosPedidos.Create( Self ) ;

  objForm.nCdItemRecebimento := qryItemRecebimentonCdItemRecebimento.Value ;
  objForm.nCdProduto         := qryItemRecebimentonCdProduto.Value ;
  objForm.nCdRecebimento     := qryItemRecebimentonCdRecebimento.Value ;
  objForm.nCdTerceiro        := qryMasternCdTerceiro.Value ;
  objForm.nCdTipoReceb       := qryMasternCdTipoReceb.Value ;
  objForm.nCdLoja            := qryMasternCdLoja.Value ;
  objForm.nQtdeReceb         := qryItemRecebimentonQtde.Value ;
  objForm.bSomenteLeitura    := (qryMasternCdTabStatusReceb.Value >= 3) ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimentoCompleto.Protocolo1Click(Sender: TObject);
var
    objRel : TrptRecebimento_view ;
begin

  if not qryMaster.Active then
      exit ;

  objRel := TrptRecebimento_view.Create( Self ) ;

  objRel.qryMaster.Close ;
  PosicionaQuery(objRel.qryMaster, qryMasternCdRecebimento.asString) ;
  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmRecebimentoCompleto.cxTabSheet2Show(Sender: TObject);
begin
  inherited;

  qryItemRecebidoSemPedido.Close;

  if not qryMaster.IsEmpty then
      PosicionaQuery( qryItemRecebidoSemPedido , qryMasternCdRecebimento.AsString ) ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebidoSemPedidonCdTipoPedidoRecebidoChange(
  Sender: TField);
begin
  inherited;

  qryTipoPedido.Close;

  if (qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value > 0) then
  begin

      PosicionaQuery( qryTipoPedido , qryItemRecebidoSemPedidonCdTipoPedidoRecebido.AsString ) ;

      if not qryTipoPedido.IsEmpty then
          qryItemRecebidoSemPedidocNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value ;

  end ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebidoSemPedidoCalcFields(
  DataSet: TDataSet);
begin
  inherited;


  if (    (qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value > 0)
      and (qryItemRecebidoSemPedidocNmTipoPedido.Value = '') ) then
  begin
      qryTipoPedido.Close;

      PosicionaQuery( qryTipoPedido , qryItemRecebidoSemPedidonCdTipoPedidoRecebido.AsString ) ;

      if not qryTipoPedido.IsEmpty then
          qryItemRecebidoSemPedidocNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value ;

  end ;

end;

procedure TfrmRecebimentoCompleto.qryItemRecebidoSemPedidoBeforePost(
  DataSet: TDataSet);
begin

  if (qryItemRecebidoSemPedidocNmTipoPedido.Value = '') then
  begin

      MensagemAlerta('Informe o tipo de pedido de entrada.') ;
      abort;

  end ;

  inherited;


end;

procedure TfrmRecebimentoCompleto.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh2.Columns[DBGridEh2.Col-1].FieldName = 'nCdTipoPedidoRecebido') then
        begin
            if (qryItemRecebidoSemPedido.State = dsBrowse) then
                qryItemRecebidoSemPedido.Edit ;

            if ((qryItemRecebidoSemPedido.State = dsInsert) or (qryItemRecebidoSemPedido.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(102,'TipoPedido.nCdTabTipoPedido = 2');

                If (nPK > 0) then
                    qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value := nPK;

            end ;

        end;

    end ;

  end ;

end;

procedure TfrmRecebimentoCompleto.TabCentroCustoShow(Sender: TObject);
begin
  inherited;

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(qryItemCC, qryMasternCdRecebimento.AsString) ;

end;

procedure TfrmRecebimentoCompleto.cxTabSheet3Show(Sender: TObject);
begin
  inherited;

  if (not qryMaster.IsEmpty) then
      PosicionaQuery( qryResumoDivergencia , qryMasternCdRecebimento.AsString ) ;
      
end;

procedure TfrmRecebimentoCompleto.ProdutoERP1Click(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimentoCompleto.ProdutoGrade1Click(Sender: TObject);
var
  objForm : TfrmProduto;
begin
  inherited;

  objForm := TfrmProduto.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimentoCompleto.EmitirEtiquetaCdigoBarra1Click(
  Sender: TObject);
var
    arrID : array of integer ;
    iAux,iAuxTotal  : integer ;
    objModImpETP : TfrmModImpETP ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusReceb.Value = 1) then
  begin
      MensagemAlerta('Finalize o recebimento antes de imprimir as etiquetas.') ;
      exit ;
  end ;

  { -- verifica se algum item foi inserido/alterado ap�s a �ltima finaliza��o -- }

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT TOP 1 1                    ');
  qryAux.SQL.Add('  FROM ItemRecebimento            ');
  qryAux.SQL.Add(' WHERE nCdRecebimento      = ' + qryMasternCdRecebimento.AsString) ;
  qryAux.SQL.Add('   AND nCdTabStatusItemPed = 1');
  qryAux.Open;

  if not qryAux.Eof then
  begin
      qryAux.Close;
      MensagemErro('O recebimento foi alterado. Finalize o recebimento novamente.') ;
      abort ;
  end ;

  qryAux.Close;

  { -- s� impede a impress�o das etiquetas caso algum item do recebimento tenha uma restria��o -- }
  { -- de grade/quantidade pedido/fora pedido sem autoriza��o.                                 -- }
  { -- se for s� divergencia de pre�o libera as etiquetas                                      -- }
  { -- se n�o for recebimento cego                                                             -- }
  if (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1                    ');
      qryAux.SQL.Add('  FROM HistAutorDiverg            ');
      qryAux.SQL.Add(' WHERE nCdRecebimento    = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.SQL.Add('   AND nCdTabTipoDiverg IN (1,4,5)');
      qryAux.SQL.Add('   AND dDtAutorizacao   IS NULL   ');
      qryAux.Open;

      if not qryAux.Eof then
      begin
          qryAux.Close;
          if (frmMenu.LeParametro('VAREJO') = 'S') then
              MensagemErro('Produto com diverg�ncia de grade ou fora de pedido. Emiss�o de etiquetas n�o permitida.')
          else MensagemErro('Produto com diverg�ncia de quantidade ou fora de pedido. Emiss�o de etiquetas n�o permitida.') ;
          abort ;
      end ;

      qryAux.Close;

  end ;

  {-- posiciona o dataset com os produtos para emitir as etiquetas --}
  PosicionaQuery(qryItemEtiqueta, qryMasternCdRecebimento.AsString) ;

  if qryItemEtiqueta.eof then
  begin
      MensagemAlerta('Nenhum produto para emitir etiqueta.');
      abort ;
  end ;

  SetLength(arrID,0) ;

  qryItemEtiqueta.First;

  iAuxTotal := 0 ;

  while not qryItemEtiqueta.Eof do
  begin
      for iAux := 0 to (qryItemEtiquetanQtde.AsInteger-1) do
      begin
          SetLength(arrID,Length(arrID)+1) ;
          arrID[iAuxTotal] := qryItemEtiquetanCdProduto.Value ;
          inc(iAuxTotal) ;
      end ;

      qryItemEtiqueta.Next;
  end ;

  qryItemEtiqueta.Close;

  objModImpETP := TfrmModImpETP.Create( Self ) ;

  objModImpETP.emitirEtiquetas(arrID,1) ;

  freeAndNil( objModImpETP ) ;

end;

procedure TfrmRecebimentoCompleto.PopupMenu2Popup(Sender: TObject);
begin
  inherited;

  Protocolo1.Enabled                := (not qryMaster.IsEmpty) ;
  EmitirEtiquetaCdigoBarra1.Enabled := (not qryMaster.IsEmpty) ;
  ImportarXMLNFe1.Enabled           := (not qryMaster.IsEmpty) ;
  VisualizarNFe1.Enabled            := ( (not qryMaster.IsEmpty) and (qryMastercXMLNFe.Value <> '') );

  ProdutoERP1.Enabled   := (frmMenu.fnValidaUsuarioAPL('FRMPRODUTOERP')) ;
  ProdutoGrade1.Enabled := (frmMenu.fnValidaUsuarioAPL('FRMPRODUTO')) ;

end;

procedure TfrmRecebimentoCompleto.ImportarXMLNFe1Click(Sender: TObject);
begin

  OpenDialog1.FileName  :=  '';
  OpenDialog1.Title := 'Selecione um arquivo NFE XML';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Arquivos TXT (*.TXT)|*.TXT|Todos os Arquivos (*.*)|*.*';

  if OpenDialog1.Execute then
  begin

      if (OpenDialog1.FileName <> '') then
          gravaXMLNFe( OpenDialog1.FileName , TRUE ) ;

      ShowMessage('XML importado com sucesso!') ;
  end ;

end;

procedure TfrmRecebimentoCompleto.VisualizarNFe1Click(Sender: TObject);
var
  st : TStringStream;
begin

  if (qryMaster.isEmpty) then
      exit;

  if (qryMastercXMLNFe.Value = '') then
  begin
      MensagemAlerta('O XML da NFe n�o foi carregado. Verifique.') ;
      abort;
  end ;


  try
      st := TStringStream.Create( qryMastercXMLNFe.Value );

      ACBrNFe2.NotasFiscais.Clear;
      ACBrNFe2.NotasFiscais.LoadFromStream( st ) ;

      ACBrNFe2.NotasFiscais.Imprimir;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

end;

procedure TfrmRecebimentoCompleto.gravaXMLNFe(cXML: string ; bGravarDadosCabecalho : boolean);
var
   objLerXML : TfrmRecebimentoLeituraXML ;
begin

  try
      ACBrNFe2.NotasFiscais.Clear;
      ACBrNFe2.NotasFiscais.LoadFromFile( cXML ) ;
  except
      MensagemErro('Erro carregando o XML. Arquivo: ' + cXML) ;
      raise ;
  end ;


  with ACBrNFe2.NotasFiscais.Items[0].NFe do
  begin

     if Trim(DBEdit7.Text) <> Trim(intToStr(ide.nNF)) then
     begin
         MensagemAlerta('O N�mero da NF do XML � diferente do N�mero da NF informado neste recebimento. NF Selecionada: ' + intToStr(ide.nNF));
         DBEdit7.setFocus;
         abort;
     end;

  end ;

  if (qryMaster.State = dsBrowse) then
      qryMaster.Edit ;

  if (bGravarDadosCabecalho) then
  begin

     objLerXML := TfrmRecebimentoLeituraXML.Create( Self ) ;

     with ACBrNFe2.NotasFiscais.Items[0].NFe do
     begin

         objLerXML.qryTerceiro.Close;
         objLerXML.qryTerceiro.Parameters.ParamByName('cCnpjCpf').Value := Trim(emit.CNPJCPF);
         objLerXML.qryTerceiro.Open;

         if Trim(DBEdit7.Text) <> Trim(intToStr(ide.nNF)) then
         begin
             MensagemAlerta('O N�mero da NF do XML � diferente do N�mero da NF informado neste recebimento. NF Selecionada: ' + intToStr(ide.nNF));
             DBEdit7.setFocus;
             abort;
         end;

         objLerXML.qryNFeXML.Close;
         objLerXML.qryNFeXML.ExecSQL;
         objLerXML.qryNFeXML.Open;

         with ACBrNFe2.NotasFiscais.Items[0].NFe do
         begin

             qryBuscaTerceiroCNPJ.Close;
             PosicionaQuery( qryBuscaTerceiroCNPJ , emit.CNPJCPF ) ;

             if ( qryBuscaTerceiroCNPJ.eof ) then
             begin
                 MensagemAlerta('Nenhum terceiro cadastrado com o CNPJ emissor da NFe. CNPJ : ' + emit.CNPJCPF ) ;
                 abort ;
             end ;

             {-- atualiza a capa do recebimento --}
             qryMaster.Edit;
             qryMastercSerieDocto.Value         := intTostr( ide.serie ) ;
             qryMastercModeloNF.Value           := intToStr( ide.modelo ) ;
             qryMasternCdTerceiro.Value         := qryBuscaTerceiroCNPJnCdTerceiro.Value ;
             qryMasterdDtDocto.Value            := ide.dEmi ;
             qryMastercCNPJEmissor.Value        := emit.CNPJCPF;
             qryMastercIEEmissor.Value          := emit.IE;
             qryMastercUFEmissor.Value          := emit.EnderEmit.UF;
             qryMasternValDocto.Value           := total.ICMSTot.vProd ;
             qryMasternValTotalNF.Value         := total.ICMSTot.vNF ;
             qryMasternValBaseICMS.Value        := total.ICMSTot.vBC ;
             qryMasternValICMS.Value            := total.ICMSTot.vICMS ;
             qryMasternValIPI.Value             := total.ICMSTot.vIPI ;
             qryMasternValDescontoNF.Value      := total.ICMSTot.vDesc ;
             qryMasternValICMSSub.Value         := total.ICMSTot.vST ;
             qryMasternValDespesas.Value        := total.ICMSTot.vOutro ;
             qryMasternValFrete.Value           := total.ICMSTot.vFrete ;
             qryMastercChaveNFe.Value           := Copy(infNFe.ID,4,(length(infNFe.ID))) ;
             qryMasternValBaseCalcICMSSub.Value := total.ICMSTot.vBCST ;
             qryMasternValSeguroNF.Value        := total.ICMSTot.vSeg ;
             qryMastercUFOrigemNF.Value         := emit.EnderEmit.UF ;
             qryMasternValPIS.Value             := total.ICMSTot.vPIS ;
             qryMasternValCOFINS.Value          := total.ICMSTot.vCOFINS ;
             qryMasternValII.Value              := total.ICMSTot.vII ;

             qryTerceiro.Close;
             qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;
             PosicionaQuery( qryTerceiro, qryMasternCdTerceiro.asString ) ;

             qryTesteDupNF.Close ;
             qryTesteDupNF.Parameters.ParamByName('cNrDocto').Value       := DBEdit7.Text ;
             qryTesteDupNF.Parameters.ParamByName('cSerieDocto').Value    := qryMastercSerieDocto.Text ;
             qryTesteDupNF.Parameters.ParamByName('nCdTerceiro').Value    := qryTerceironCdTerceiro.Value ;
             qryTesteDupNF.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
             qryTesteDupNF.Open ;

             if not qryTesteDUPnf.Eof then
                 MensagemAlerta('Esta nota fiscal j� foi recebida no recebimento No : ' + qryTesteDupNFnCdRecebimento.AsString) ;

             qryTesteDupNf.Close ;

         end ;

     end ;

  end ;

  try
      qryMastercXMLNFe.Value := UTF8ToAnsi( ACBrNFe2.NotasFiscais.Items[0].XML ) ;
      qryMaster.Post;
  except
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  
end;

initialization
    RegisterClass(TfrmRecebimentoCompleto) ;

end.
