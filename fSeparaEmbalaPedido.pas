unit fSeparaEmbalaPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, DBGridEhGrouping, cxLookAndFeelPainters, StdCtrls, cxButtons,
  GridsEh, DBGridEh, DB, ADODB, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, Menus;

type
  TfrmSeparaEmbalaPedido = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    GroupBox1: TGroupBox;
    btExibeOP: TcxButton;
    qryAguardProd: TADOQuery;
    dsAguardProd: TDataSource;
    qryAguardProdnCdPedido: TIntegerField;
    qryAguardProdcNumeroOP: TStringField;
    qryAguardProdcNmTipoPedido: TStringField;
    qryAguardProddDtEntrega: TDateTimeField;
    qryAguardProdcAtraso: TStringField;
    qryAguardProdcNmTerceiro: TStringField;
    qryAguardProdcNmTerceiroPagador: TStringField;
    qryAguardProdcAutFin: TStringField;
    qryAguardProdnCdProduto: TIntegerField;
    qryAguardProdcNmProduto: TStringField;
    qryAguardProdnQtdePed: TBCDField;
    qryAguardProdnQtdeAberto: TBCDField;
    qryAguardProdnPercCumprimento: TBCDField;
    cxTabSheet11: TcxTabSheet;
    cxPageControl3: TcxPageControl;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxPageControl4: TcxPageControl;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    GroupBox2: TGroupBox;
    btGerarPicking: TcxButton;
    GroupBox3: TGroupBox;
    btGerarNF: TcxButton;
    GroupBox4: TGroupBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    qryAguardProdnCdOrdemProducao: TIntegerField;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGrid4DBTableView1: TcxGridDBTableView;
    cxGrid4Level1: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5: TcxGrid;
    cxGrid6DBTableView1: TcxGridDBTableView;
    cxGrid6Level1: TcxGridLevel;
    cxGrid6: TcxGrid;
    cxGrid7DBTableView1: TcxGridDBTableView;
    cxGrid7Level1: TcxGridLevel;
    cxGrid7: TcxGrid;
    qryAguardSepResum: TADOQuery;
    qryAguardSepResumnCdPedido: TIntegerField;
    qryAguardSepResumcNmTipoPedido: TStringField;
    qryAguardSepResumdDtPedido: TDateTimeField;
    qryAguardSepResumdDtPrevEntFim: TDateTimeField;
    qryAguardSepResumcAtraso: TStringField;
    qryAguardSepResumcNmTerceiro: TStringField;
    qryAguardSepResumcNmTerceiroPagador: TStringField;
    qryAguardSepResumcAutFin: TStringField;
    dsAguardSepResum: TDataSource;
    cxGrid2DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid2DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid2DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid2DBTableView1dDtPrevEntFim: TcxGridDBColumn;
    cxGrid2DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid2DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid2DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid2DBTableView1cAutFin: TcxGridDBColumn;
    qryEmSeparacao: TADOQuery;
    qryEmSeparacaonCdPickList: TIntegerField;
    qryEmSeparacaonCdPedido: TIntegerField;
    qryEmSeparacaodDtGeracao: TDateTimeField;
    qryEmSeparacaocNmUsuarioGeracao: TStringField;
    qryEmSeparacaodDtPrimImp: TDateTimeField;
    qryEmSeparacaocNmUsuarioPrimImp: TStringField;
    qryEmSeparacaodDtEntrega: TDateTimeField;
    qryEmSeparacaocAtraso: TStringField;
    qryEmSeparacaocNmTerceiro: TStringField;
    qryEmSeparacaocNmTerceiroPagador: TStringField;
    qryEmSeparacaocAutFin: TStringField;
    dsEmSeparacao: TDataSource;
    cxGrid4DBTableView1nCdPickList: TcxGridDBColumn;
    cxGrid4DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid4DBTableView1dDtGeracao: TcxGridDBColumn;
    cxGrid4DBTableView1cNmUsuarioGeracao: TcxGridDBColumn;
    cxGrid4DBTableView1dDtPrimImp: TcxGridDBColumn;
    cxGrid4DBTableView1cNmUsuarioPrimImp: TcxGridDBColumn;
    cxGrid4DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid4DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid4DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid4DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid4DBTableView1cAutFin: TcxGridDBColumn;
    cxButton6: TcxButton;
    qryAguardandoFat: TADOQuery;
    dsAguardandoFat: TDataSource;
    qryAguardandoFatnCdPedido: TIntegerField;
    qryAguardandoFatcNmTipoPedido: TStringField;
    qryAguardandoFatdDtPedido: TDateTimeField;
    qryAguardandoFatdDtPrevEntFim: TDateTimeField;
    qryAguardandoFatcAtraso: TStringField;
    qryAguardandoFatcNmTerceiro: TStringField;
    qryAguardandoFatcNmTerceiroPagador: TStringField;
    qryAguardandoFatcAutFin: TStringField;
    cxGrid7DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid7DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid7DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid7DBTableView1dDtPrevEntFim: TcxGridDBColumn;
    cxGrid7DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid7DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid7DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid7DBTableView1cAutFin: TcxGridDBColumn;
    qryAguardandoFatnCdEmpresa: TIntegerField;
    qryAguardSepDetail: TADOQuery;
    qryAguardSepDetailnCdPedido: TIntegerField;
    qryAguardSepDetailcNmTipoPedido: TStringField;
    qryAguardSepDetaildDtEntrega: TDateTimeField;
    qryAguardSepDetailcAtraso: TStringField;
    qryAguardSepDetailcNmTerceiroEntrega: TStringField;
    qryAguardSepDetailcNmTerceiroPagador: TStringField;
    qryAguardSepDetailcAutFin: TStringField;
    qryAguardSepDetailnCdProduto: TIntegerField;
    qryAguardSepDetailcNmItem: TStringField;
    qryAguardSepDetailnQtdePed: TBCDField;
    qryAguardSepDetailnQtdeAberto: TIntegerField;
    qryAguardSepDetailnPercCumprimento: TBCDField;
    dsAguardSepDetail: TDataSource;
    cxGrid3DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid3DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid3DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid3DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid3DBTableView1cNmTerceiroEntrega: TcxGridDBColumn;
    cxGrid3DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid3DBTableView1cAutFin: TcxGridDBColumn;
    cxGrid3DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid3DBTableView1cNmItem: TcxGridDBColumn;
    cxGrid3DBTableView1nQtdePed: TcxGridDBColumn;
    cxGrid3DBTableView1nQtdeAberto: TcxGridDBColumn;
    cxGrid3DBTableView1nPercCumprimento: TcxGridDBColumn;
    qryEmSepDetail: TADOQuery;
    qryEmSepDetailnCdPickList: TIntegerField;
    qryEmSepDetailnCdPedido: TIntegerField;
    qryEmSepDetaildDtGeracao: TDateTimeField;
    qryEmSepDetailcNmUsuarioGeracao: TStringField;
    qryEmSepDetaildDtEntrega: TDateTimeField;
    qryEmSepDetailcAtraso: TStringField;
    qryEmSepDetailcNmTerceiro: TStringField;
    qryEmSepDetailcNmTerceiroPagador: TStringField;
    qryEmSepDetailcAutFin: TStringField;
    qryEmSepDetailnCdProduto: TIntegerField;
    qryEmSepDetailcNmItem: TStringField;
    qryEmSepDetailnQtdePick: TBCDField;
    dsEmSepDetail: TDataSource;
    cxGrid5DBTableView1nCdPickList: TcxGridDBColumn;
    cxGrid5DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid5DBTableView1dDtGeracao: TcxGridDBColumn;
    cxGrid5DBTableView1cNmUsuarioGeracao: TcxGridDBColumn;
    cxGrid5DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid5DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid5DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid5DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid5DBTableView1cAutFin: TcxGridDBColumn;
    cxGrid5DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid5DBTableView1cNmItem: TcxGridDBColumn;
    cxGrid5DBTableView1nQtdePick: TcxGridDBColumn;
    qryAguardFatDetail: TADOQuery;
    qryAguardFatDetailnCdPedido: TIntegerField;
    qryAguardFatDetailcNmTipoPedido: TStringField;
    qryAguardFatDetaildDtEntrega: TDateTimeField;
    qryAguardFatDetailcAtraso: TStringField;
    qryAguardFatDetailcNmTerceiroEntrega: TStringField;
    qryAguardFatDetailcNmTerceiroPagador: TStringField;
    qryAguardFatDetailcAutFin: TStringField;
    qryAguardFatDetailnCdProduto: TIntegerField;
    qryAguardFatDetailcNmProduto: TStringField;
    qryAguardFatDetailnQtdePed: TBCDField;
    qryAguardFatDetailnQtdeLibFat: TBCDField;
    qryAguardFatDetailnPercCumprimento: TBCDField;
    dsAguardFatDetail: TDataSource;
    cxGrid6DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid6DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid6DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid6DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid6DBTableView1cNmTerceiroEntrega: TcxGridDBColumn;
    cxGrid6DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid6DBTableView1cAutFin: TcxGridDBColumn;
    cxGrid6DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid6DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid6DBTableView1nQtdePed: TcxGridDBColumn;
    cxGrid6DBTableView1nQtdeLibFat: TcxGridDBColumn;
    cxGrid6DBTableView1nPercCumprimento: TcxGridDBColumn;
    qryCancelaPicking: TADOQuery;
    btImprimePicking: TcxButton;
    qryAguardSepResumcOBS: TMemoField;
    popAguardProducao: TPopupMenu;
    popAguardSeparacaoResumo: TPopupMenu;
    popEmSeparacao: TPopupMenu;
    popAguardFaturamento: TPopupMenu;
    Atualizar1: TMenuItem;
    Atualizar2: TMenuItem;
    Atualizar3: TMenuItem;
    Atualizar4: TMenuItem;
    cxPageControl5: TcxPageControl;
    cxTabSheet10: TcxTabSheet;
    cxTabSheet12: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nCdOrdemProducao: TcxGridDBColumn;
    cxGrid1DBTableView1cNumeroOP: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1cAtraso: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGrid1DBTableView1cAutFin: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdePed: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeAberto: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    popProduzidoParcial: TPopupMenu;
    GerarPickList1: TMenuItem;
    AutorizarFaturamento1: TMenuItem;
    qryProdParcial: TADOQuery;
    qryProdParcialnCdPedido: TIntegerField;
    qryProdParcialnCdOrdemProducao: TIntegerField;
    qryProdParcialcNumeroOP: TStringField;
    qryProdParcialcNmTipoPedido: TStringField;
    qryProdParcialdDtEntrega: TDateTimeField;
    qryProdParcialcAtraso: TStringField;
    qryProdParcialcNmTerceiro: TStringField;
    qryProdParcialcNmTerceiroPagador: TStringField;
    qryProdParcialcAutFin: TStringField;
    qryProdParcialnCdProduto: TIntegerField;
    qryProdParcialcNmProduto: TStringField;
    qryProdParcialnQtdePed: TBCDField;
    qryProdParcialnQtdeProdParcial: TBCDField;
    cxGrid8: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dsProdParcial: TDataSource;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdOrdemProducao: TcxGridDBColumn;
    cxGridDBTableView1cNumeroOP: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView1dDtEntrega: TcxGridDBColumn;
    cxGridDBTableView1cAtraso: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiroPagador: TcxGridDBColumn;
    cxGridDBTableView1cAutFin: TcxGridDBColumn;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmProduto: TcxGridDBColumn;
    cxGridDBTableView1nQtdePed: TcxGridDBColumn;
    cxGridDBTableView1nQtdeProdParcial: TcxGridDBColumn;
    Atualizar5: TMenuItem;
    N1: TMenuItem;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    popAguardSeparacaoAnalitico: TPopupMenu;
    MenuItem1: TMenuItem;
    qryAguardSepResumcTipoEntrega: TStringField;
    cxGrid2DBTableView1DBColumn1: TcxGridDBColumn;
    procedure btExibeOPClick(Sender: TObject);
    procedure btGerarPickingClick(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btGerarNFClick(Sender: TObject);
    procedure qryAguardSepDetailAfterScroll(DataSet: TDataSet);
    procedure qryEmSepDetailAfterScroll(DataSet: TDataSet);
    procedure qryAguardFatDetailAfterScroll(DataSet: TDataSet);
    procedure cxButton6Click(Sender: TObject);
    procedure btImprimePickingClick(Sender: TObject);
    procedure cxGrid4DBTableView1DblClick(Sender: TObject);
    procedure cxGrid5DBTableView1DblClick(Sender: TObject);
    procedure Atualizar1Click(Sender: TObject);
    procedure Atualizar2Click(Sender: TObject);
    procedure Atualizar3Click(Sender: TObject);
    procedure Atualizar4Click(Sender: TObject);
    procedure Atualizar5Click(Sender: TObject);
    procedure AutorizarFaturamento1Click(Sender: TObject);
    procedure GerarPickList1Click(Sender: TObject);
    procedure qryProdParcialAfterScroll(DataSet: TDataSet);
    procedure cxGrid7DBTableView1DblClick(Sender: TObject);
    procedure cxGrid6DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure cxGrid3DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSeparaEmbalaPedido: TfrmSeparaEmbalaPedido;

implementation

uses fOP, fGerarPickingList, fAutorizarFaturamento, fMenu,fItemFaturar;

{$R *.dfm}

procedure TfrmSeparaEmbalaPedido.btExibeOPClick(Sender: TObject);
var
  objForm : TfrmOP ;
begin
  inherited;

  if (qryAguardProd.isEmpty) then
      exit;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK(qryAguardProdnCdOrdemProducao.Value);

  showForm( objForm , True ) ;

  qryAguardProd.Close;
  qryAguardProd.Open;
end;

procedure TfrmSeparaEmbalaPedido.btGerarPickingClick(Sender: TObject);
var
  objForm : TfrmGerarPickingList ;
begin
  inherited;

  if (cxPageControl2.ActivePageIndex = 0) then
  begin

      if (qryAguardSepResum.IsEmpty) then
          exit;

      objForm := TfrmGerarPickingList.Create( Self ) ;

      objForm.nCdPedido         := qryAguardSepResumnCdPedido.Value;
      objForm.cFlgOrdemProducao := 0; {-- informa que o pedido n�o tem OP Vinculada --}
      objForm.nCdOrdemProducao  := 0;
      objForm.cOBS              := qryAguardSepResumcOBS.Value;

      showForm( objForm , True ) ;

      qryAguardSepResum.Close;
      qryAguardSepResum.Open;

  end
  else
  begin

      if (qryAguardSepDetail.IsEmpty) then
          exit;

      objForm := TfrmGerarPickingList.Create( Self ) ;

      objForm.nCdPedido         := qryAguardSepDetailnCdPedido.Value;
      objForm.cFlgOrdemProducao := 0; {-- informa que o pedido n�o tem OP Vinculada --}
      objForm.nCdOrdemProducao  := 0;

      showForm( objForm , True ) ;

      qryAguardSepDetail.Close;
      qryAguardSepDetail.Open;

  end ;

end;

procedure TfrmSeparaEmbalaPedido.cxButton5Click(Sender: TObject);
var
  objForm : TfrmAutorizarFaturamento ;
begin
  inherited;

  if not ((qryAguardSepResum.Active) and (qryAguardSepResum.RecordCount > 0)) then
      exit;

  objForm := TfrmAutorizarFaturamento.Create( Self ) ;

  objForm.cmdPreparaTemp.Execute;

  objForm.qryPopulaTemp.Close;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value         := qryAguardSepResumnCdPedido.Value;
  objForm.qryPopulaTemp.Parameters.ParamByName('cFlgOrdemProducao').Value := 0; {-- pedido n�o vinculado a uma OP --}
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdOrdemProducao').Value  := 0;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPickList').Value       := Null;
  objForm.qryPopulaTemp.ExecSQL;

  objForm.qryItensFaturar.Close;
  objForm.qryItensFaturar.Open;

  showForm( objForm , True ) ;

  qryAguardSepResum.Close;
  qryAguardSepResum.Open;

  qryAguardSepDetail.Close;
  qryAguardSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.cxButton4Click(Sender: TObject);
var
  objForm : TfrmAutorizarFaturamento ;
begin
  inherited;

  if not ((qryEmSeparacao.Active) and (qryEmSeparacao.RecordCount > 0)) then
      exit;

  objForm := TfrmAutorizarFaturamento.Create( Self ) ;

  objForm.cmdPreparaTemp.Execute;

  objForm.qryPopulaTemp.Close;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value   := Null;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPickList').Value := qryEmSeparacaonCdPickList.Value;
  objForm.qryPopulaTemp.ExecSQL;

  objForm.qryItensFaturar.Close;
  objForm.qryItensFaturar.Open;

  showForm( objForm , True ) ;

  qryEmSeparacao.Close;
  qryEmSeparacao.Open;

  qryEmSepDetail.Close;
  qryEmSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.btGerarNFClick(Sender: TObject);
var
  objItemFaturar : TfrmItemFaturar;
begin
  inherited;

  if not ((qryAguardandoFat.Active) and (qryAguardandoFat.RecordCount > 0)) then
      exit;

  if (qryAguardandoFatcAutFin.Value = 'PARCIAL') then
  begin
      MensagemAlerta('Pedido autorizado parcialmente, impossivel faturar.');
      abort;
  end;

  objItemFaturar := TfrmItemFaturar.Create(nil);

  try
      try
          objItemFaturar.nCdPedido  := qryAguardandoFatnCdPedido.Value;
          objItemFaturar.dDtFaturar := Now();
          PosicionaQuery(objItemFaturar.qryItemPedido,qryAguardandoFatnCdPedido.AsString) ;
          showForm(objItemFaturar,false);
      except
          MensagemErro('Erro ao gerar o Faturamento');
          raise;
      end
  finally
      FreeAndNil(objItemFaturar);
  end;

  qryAguardandoFat.Close;
  qryAguardandoFat.Open;

  qryAguardFatDetail.Close;
  qryAguardFatDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.qryAguardSepDetailAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  {qryAguardSepResum.Locate('nCdPedido',qryAguardSepDetailnCdPedido.Value,[]);}
  
end;

procedure TfrmSeparaEmbalaPedido.qryEmSepDetailAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryEmSeparacao.Locate('nCdPickList',qryEmSepDetailnCdPickList.Value,[]);

end;

procedure TfrmSeparaEmbalaPedido.qryAguardFatDetailAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryAguardandoFat.Locate('nCdPedido',qryAguardFatDetailnCdPedido.Value,[]);

end;

procedure TfrmSeparaEmbalaPedido.cxButton6Click(Sender: TObject);
begin
  inherited;

  if not ((qryEmSeparacao.Active) and (qryEmSeparacao.RecordCount > 0)) then
      exit;

  if (MessageDlg('Ap�s cancelar o Picking essa a��o n�o poder� ser desfeita. Confirma o Cancelamento ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryCancelaPicking.Close;
      qryCancelaPicking.Parameters.ParamByName('nCdPickList').Value := qryEmSeparacaonCdPickList.Value;
      qryCancelaPicking.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      qryCancelaPicking.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Picking Cancelado com Sucesso.');

  qryEmSeparacao.Close;
  qryEmSeparacao.Open;

  qryEmSepDetail.Close;
  qryEmSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.btImprimePickingClick(Sender: TObject);
var
  objForm : TfrmGerarPickingList;
begin
  inherited;

  if not ((qryEmSeparacao.Active) and (qryEmSeparacao.RecordCount > 0)) then
      exit;

  objForm := TfrmGerarPickingList.Create(nil);

  try
      try
          objForm.ImprimePickList(qryEmSeparacaonCdPickList.Value, (qryEmSeparacaodDtPrimImp.AsString = ''));
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objForm.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objForm);
  end;

  qryEmSeparacao.Close;
  qryEmSeparacao.Open;

  qryEmSepDetail.Close;
  qryEmSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.cxGrid4DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btImprimePicking.Click;
end;

procedure TfrmSeparaEmbalaPedido.cxGrid5DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btImprimePicking.Click;
end;

procedure TfrmSeparaEmbalaPedido.Atualizar1Click(Sender: TObject);
begin
  inherited;

  qryAguardProd.Close;
  qryAguardProd.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardProd.Open;

  qryProdParcial.Close;
  qryProdParcial.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProdParcial.Open;

end;

procedure TfrmSeparaEmbalaPedido.Atualizar2Click(Sender: TObject);
begin
  inherited;

  qryAguardSepResum.Close;
  qryAguardSepResum.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardSepResum.Open;

  {qryAguardSepDetail.Close;
  qryAguardSepDetail.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardSepDetail.Open;}

end;

procedure TfrmSeparaEmbalaPedido.Atualizar3Click(Sender: TObject);
begin
  inherited;

  qryEmSeparacao.Close;
  qryEmSeparacao.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryEmSeparacao.Open;

  qryEmSepDetail.Close;
  qryEmSepDetail.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryEmSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.Atualizar4Click(Sender: TObject);
begin
  inherited;

  qryAguardandoFat.Close;
  qryAguardandoFat.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardandoFat.Open;

  qryAguardFatDetail.Close;
  qryAguardFatDetail.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardFatDetail.Open;
end;

procedure TfrmSeparaEmbalaPedido.Atualizar5Click(Sender: TObject);
begin
  inherited;

  qryAguardProd.Close;
  qryAguardProd.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardProd.Open;

  qryProdParcial.Close;
  qryProdParcial.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProdParcial.Open;
end;

procedure TfrmSeparaEmbalaPedido.AutorizarFaturamento1Click(
  Sender: TObject);
var
  objForm : TfrmAutorizarFaturamento ;
begin
  inherited;

  if not ((qryProdParcial.Active) and (qryProdParcial.RecordCount > 0)) then
      exit;

  objForm := TfrmAutorizarFaturamento.Create( Self ) ;

  objForm.cmdPreparaTemp.Execute;

  objForm.qryPopulaTemp.Close;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value         := qryProdParcialnCdPedido.Value;
  objForm.qryPopulaTemp.Parameters.ParamByName('cFlgOrdemProducao').Value := 1;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdOrdemProducao').Value  := qryProdParcialnCdOrdemProducao.Value;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdPickList').Value       := Null;
  objForm.qryPopulaTemp.ExecSQL;

  objForm.qryItensFaturar.Close;
  objForm.qryItensFaturar.Open;

  showForm( objForm , True ) ;

  qryAguardProd.Close;
  qryAguardProd.Open;

  qryProdParcial.Close;
  qryProdParcial.Open;
end;

procedure TfrmSeparaEmbalaPedido.GerarPickList1Click(Sender: TObject);
var
  objForm : TfrmGerarPickingList ;
begin
  inherited;

  if not ((qryProdParcial.Active) and (qryProdParcial.RecordCount > 0)) then
      exit;

  objForm := TfrmGerarPickingList.Create( Self ) ;

  objForm.nCdPedido         := qryProdParcialnCdPedido.Value;
  objForm.cFlgOrdemProducao := 1; {-- informa que o pedido est� vinculado a uma OP --}
  objForm.nCdOrdemProducao  := qryProdParcialnCdOrdemProducao.Value;
  objForm.cOBS              := '';

  showForm( objForm , True ) ;

  qryAguardProd.Close;
  qryAguardProd.Open;

  qryProdParcial.Close;
  qryProdParcial.Open;
end;

procedure TfrmSeparaEmbalaPedido.qryProdParcialAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryAguardProd.Locate('nCdPedido',qryProdParcialnCdPedido.Value,[]);

end;

procedure TfrmSeparaEmbalaPedido.cxGrid7DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btGerarNF.Click;
end;

procedure TfrmSeparaEmbalaPedido.cxGrid6DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  btGerarNF.Click;

end;

procedure TfrmSeparaEmbalaPedido.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btExibeOp.Click;
  
end;

procedure TfrmSeparaEmbalaPedido.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  btExibeOp.Click;
end;

procedure TfrmSeparaEmbalaPedido.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 1 ;
end;

procedure TfrmSeparaEmbalaPedido.MenuItem1Click(Sender: TObject);
begin
  inherited;

  qryAguardSepDetail.Close;
  qryAguardSepDetail.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryAguardSepDetail.Open;

end;

procedure TfrmSeparaEmbalaPedido.cxGrid2DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btGerarPicking.Click;

end;

procedure TfrmSeparaEmbalaPedido.cxGrid3DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  btGerarPicking.Click;

end;

initialization
  RegisterClass(TfrmSeparaEmbalaPedido);

end.
