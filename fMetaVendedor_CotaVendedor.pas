unit fMetaVendedor_CotaVendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, cxPC,
  cxControls, ImgList, ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping;

type
  TfrmMetaVendedor_CotaVendedor = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryCotaVendedores: TADOQuery;
    qryCotaVendedoresnCdMetaVendedor: TIntegerField;
    qryCotaVendedoresnCdMetaSemana: TIntegerField;
    qryCotaVendedoresnCdTerceiroColab: TIntegerField;
    qryCotaVendedoresnPercHoraCalc: TBCDField;
    qryCotaVendedoresnPercHoraLojaReal: TBCDField;
    qryVendedor: TADOQuery;
    dsVendedor: TDataSource;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    qryVendedornCdTerceiroResponsavel: TIntegerField;
    qryCotaVendedoresnValMetaMinima: TFloatField;
    qryCotaVendedoresnValMeta: TFloatField;
    qryCotaVendedorescNmUsuario: TStringField;
    dsCotaVendedores: TDataSource;
    qryCotaVendedoresnCdUsuario: TIntegerField;
    qryCotaVendedoresnPercHoralojaPrev: TBCDField;
    qryCotaVendedoresnPercHoraLojaCalc: TFloatField;
    qryVerificaDepApontamento: TADOQuery;
    qryCotaVendedoresiTotalHorasPrev: TBCDField;
    qryCotaVendedoresiTotalHorasReal: TBCDField;
    qryCotaVendedorescFlgAutomatico: TIntegerField;
    qryAtualizaHorasPrev: TADOQuery;
    procedure qryCotaVendedoresCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCotaVendedoresBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryCotaVendedoresBeforeDelete(DataSet: TDataSet);
    procedure qryCotaVendedoresAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLoja,nCdMetaSemana : integer;
    iTotalHorasPrev,iTotalHoras : Double;
    nValMetaMinima, nValMeta : Double;
  end;

var
  frmMetaVendedor_CotaVendedor: TfrmMetaVendedor_CotaVendedor;

implementation

uses fMenu, fLookup_Padrao,fMetaVendedor, Math;

{$R *.dfm}

procedure TfrmMetaVendedor_CotaVendedor.qryCotaVendedoresCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  iTotalHorasPrev := DBGridEh1.Columns.Items[5].Footers[0].SumValue;

  {--coloca o nome do vendedor --}
  if (qryCotaVendedorescNmUsuario.Value = '') then
  begin
      qryVendedor.Close;
      qryVendedor.Parameters.ParamByName('nCdLoja').Value := nCdLoja;
      PosicionaQuery(qryVendedor,qryCotaVendedoresnCdUsuario.AsString);

      qryCotaVendedorescNmUsuario.Value := qryVendedorcNmUsuario.Value;
  end ;

  {-- calcula a % de horas prevista --}
  if (iTotalHorasPrev > 0) then
      qryCotaVendedoresnPercHoraCalc.Value     := (qryCotaVendedoresiTotalHorasPrev.Value/iTotalHorasPrev) * 100;

  {-- calcula a meta minima e a meta do vendedor --}
  if (nValMetaMinima > 0) then
      qryCotaVendedoresnValMetaMinima.Value := (nValMetaMinima * qryCotaVendedoresnPercHoraCalc.Value) / 100;

  if (nValMeta > 0) then
      qryCotaVendedoresnValMeta.Value       := (nValMeta       * qryCotaVendedoresnPercHoraCalc.Value) / 100;

  {-- calcula % de horas j� realizadas --}

  if (qryCotaVendedoresiTotalHorasPrev.Value > 0) then
      qryCotaVendedoresnPercHoraLojaCalc.Value := (qryCotaVendedoresiTotalHorasReal.Value/qryCotaVendedoresiTotalHorasPrev.Value) * 100;

end;

procedure TfrmMetaVendedor_CotaVendedor.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 4) then
        begin
            if (qryCotaVendedores.State = dsBrowse) then
                qryCotaVendedores.Edit;
                
            nPK := frmLookup_Padrao.ExecutaConsulta(703);

            If (nPK > 0) then
            begin
                qryVendedor.Close;
                PosicionaQuery(qryVendedor, IntToStr(nPK)) ;

                qryCotaVendedoresnCdUsuario.Value       := qryVendedornCdUsuario.Value;
                qryCotaVendedoresnCdTerceiroColab.Value := qryVendedornCdTerceiroResponsavel.Value;
            end ;
        end;
    end ;

  end ;

end;

procedure TfrmMetaVendedor_CotaVendedor.qryCotaVendedoresBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryCotaVendedores.State = dsInsert) then
  begin
      qryCotaVendedoresnCdMetaVendedor.Value := frmMenu.fnProximoCodigo('METAVENDEDOR');
      qryCotaVendedoresnCdMetaSemana.Value   := nCdMetaSemana;
  end;

  if ((qryCotaVendedores.State = dsInsert) or (qryCotaVendedores.State = dsEdit)) then
  begin
      qryCotaVendedoresnPercHoralojaPrev.Value := qryCotaVendedoresnPercHoraCalc.Value;
      qryCotaVendedoresnPercHoraLojaReal.Value := qryCotaVendedoresnPercHoraLojaCalc.Value;
      qryCotaVendedoresnCdTerceiroColab.Value  := qryVendedornCdTerceiroResponsavel.Value;
  end;
      
end;

procedure TfrmMetaVendedor_CotaVendedor.ToolButton1Click(Sender: TObject);
begin
  inherited;
  
  Close;

end;

procedure TfrmMetaVendedor_CotaVendedor.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmMetaVendedor_CotaVendedor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  {inherited;}

  iTotalHoras := DBGridEh1.Columns.Items[5].Footers[0].SumValue;

  if ((iTotalHoras <> iTotalHorasPrev) and (iTotalHoras > 0)) then
  begin
      MensagemAlerta('A soma das horas prevista de venda dos vendedores � diferente das horas previstas da semana.' + #13#10#13#10 + 'Horas Previstas da Semana: ' + FloatToStr(iTotalHorasPrev) + #13#10 + 'Horas de Venda Distribuidas: ' + FloatToStr(iTotalHoras));
      abort;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryAtualizaHorasPrev.Close;
      qryAtualizaHorasPrev.Parameters.ParamByName('nCdMetaSemana').Value   := nCdMetaSemana;
      qryAtualizaHorasPrev.Parameters.ParamByName('iTotalHorasPrev').Value := iTotalHorasPrev;
      qryAtualizaHorasPrev.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro ao Processar.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  qryCotaVendedores.Close;

end;

procedure TfrmMetaVendedor_CotaVendedor.qryCotaVendedoresBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  qryVerificaDepApontamento.Close;
  qryVerificaDepApontamento.Parameters.ParamByName('nCdMetaVendedor').Value := qryCotaVendedoresnCdMetaVendedor.Value;
  qryVerificaDepApontamento.Open;

  if not (qryVerificaDepApontamento.Eof) then
  begin
      MensagemAlerta('O vendedor n�o pode ser exclu�do, pois j� foram feitos um ou mais apontamentos.');
      abort;
  end;

end;

procedure TfrmMetaVendedor_CotaVendedor.qryCotaVendedoresAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  iTotalHorasPrev := DBGridEh1.Columns.Items[5].Footers[0].SumValue;

  qryCotaVendedores.Requery([]);
  
end;

end.
