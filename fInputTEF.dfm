object frmInputTEF: TfrmInputTEF
  Left = 513
  Top = 280
  BorderStyle = bsDialog
  Caption = 'ER2Soft - TEF'
  ClientHeight = 138
  ClientWidth = 338
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object lblBuffer: TLabel
    Left = 8
    Top = 40
    Width = 321
    Height = 17
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object lblTituloBox: TLabel
    Left = 8
    Top = 16
    Width = 321
    Height = 17
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object btnMenuAnt: TcxButton
    Left = 8
    Top = 96
    Width = 105
    Height = 33
    Caption = 'Menu Anterior'
    TabOrder = 0
    Visible = False
    OnClick = btnMenuAntClick
    LookAndFeel.NativeStyle = True
  end
  object btnOK: TcxButton
    Left = 120
    Top = 96
    Width = 105
    Height = 33
    Caption = 'Confirmar'
    TabOrder = 1
    OnClick = btnOKClick
    LookAndFeel.NativeStyle = True
  end
  object edtValor: TMaskEdit
    Left = 8
    Top = 64
    Width = 321
    Height = 24
    TabOrder = 2
    OnKeyDown = edtValorKeyDown
  end
  object btnCancelar: TcxButton
    Left = 232
    Top = 96
    Width = 97
    Height = 33
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelarClick
    LookAndFeel.NativeStyle = True
  end
end
