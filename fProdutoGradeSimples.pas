unit fProdutoGradeSimples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmProdutoGradeSimples = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TAutoIncField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TAutoIncField;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdStatus: TIntegerField;
    Label3: TLabel;
    DBEdit10: TDBEdit;
    Label5: TLabel;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhanCdMarca: TIntegerField;
    qryLinhacNmLinha: TStringField;
    qryLinhanCdStatus: TIntegerField;
    DBEdit14: TDBEdit;
    DataSource5: TDataSource;
    qryGrade: TADOQuery;
    qryColecao: TADOQuery;
    qryGradenCdGrade: TAutoIncField;
    qryGradecNmGrade: TStringField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    Label10: TLabel;
    DBEdit16: TDBEdit;
    Label11: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DataSource6: TDataSource;
    qryColecaonCdColecao: TAutoIncField;
    qryColecaocNmColecao: TStringField;
    DBEdit19: TDBEdit;
    DataSource7: TDataSource;
    Label12: TLabel;
    DBEdit20: TDBEdit;
    Label13: TLabel;
    DBEdit21: TDBEdit;
    qryClasse: TADOQuery;
    qryStatus: TADOQuery;
    qryClassenCdClasseProduto: TAutoIncField;
    qryClassecNmClasseProduto: TStringField;
    DBEdit22: TDBEdit;
    DataSource8: TDataSource;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit23: TDBEdit;
    DataSource9: TDataSource;
    qryProdutoIntermediario: TADOQuery;
    qryProdutoIntermediarionCdProduto: TAutoIncField;
    qryProdutoIntermediariocNmCor: TStringField;
    qryProdutoIntermediariocNmMaterial: TStringField;
    dsProdutoIntermediario: TDataSource;
    qryProdutoIntermediarionQtdeEstoque: TBCDField;
    btGerarGrade: TcxButton;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdDepartamento: TIntegerField;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    qryMasternCdLinha: TIntegerField;
    qryMastercReferencia: TStringField;
    qryMasternCdTabTipoProduto: TIntegerField;
    qryMasternCdProdutoPai: TIntegerField;
    qryMasternCdGrade: TIntegerField;
    qryMasternCdColecao: TIntegerField;
    qryMastercNmProduto: TStringField;
    qryMasternCdClasseProduto: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdCor: TIntegerField;
    qryMasternCdMaterial: TIntegerField;
    qryMasteriTamanho: TIntegerField;
    qryMasternQtdeEstoque: TBCDField;
    qryMasternValCusto: TBCDField;
    qryMasternValVenda: TBCDField;
    qryMastercEAN: TStringField;
    qryMastercEANFornec: TStringField;
    qryMasterdDtUltReceb: TDateTimeField;
    qryMasterdDtUltVenda: TDateTimeField;
    qryProdutoFinal: TADOQuery;
    DBGridEh2: TDBGridEh;
    dsProdutoFinal: TDataSource;
    qryProdutoFinalnCdProduto: TIntegerField;
    qryProdutoFinalcNmTamanho: TStringField;
    qryProdutoFinalnQtdeEstoque: TBCDField;
    qryProdutoFinalnValCusto: TBCDField;
    qryProdutoFinalnValVenda: TBCDField;
    qryProdutoFinalcEAN: TStringField;
    qryProdutoFinalcEANFornec: TStringField;
    StaticText1: TStaticText;
    qryProdutoIntermediariodDtUltReceb: TDateTimeField;
    qryProdutoIntermediariodDtUltVenda: TDateTimeField;
    qryProdutoIntermediarionCdCor: TAutoIncField;
    qryProdutoIntermediarionCdMaterial: TAutoIncField;
    qryConsultaProd: TADOQuery;
    qryConsultaProdnCdProduto: TIntegerField;
    Label14: TLabel;
    DBEdit24: TDBEdit;
    Label15: TLabel;
    DBEdit25: TDBEdit;
    Label16: TLabel;
    DBEdit26: TDBEdit;
    Label17: TLabel;
    DBEdit27: TDBEdit;
    Label18: TLabel;
    DBEdit28: TDBEdit;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryProdutoIntermediariocNmProduto: TStringField;
    qryProdutoFinalcNmProduto: TStringField;
    qryMastercUnidadeMedida: TStringField;
    qryMastercFlgProdVenda: TIntegerField;
    qryMastercUnidadeMedidaCompra: TStringField;
    qryMasternFatorCompra: TBCDField;
    qryMasternQtdeMinimaCompra: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryDepartamentonCdGrupoProduto: TIntegerField;
    qryMasternCdGrupo_Produto: TIntegerField;
    Label19: TLabel;
    DBEdit29: TDBEdit;
    cxButton1: TcxButton;
    Label20: TLabel;
    DBEdit30: TDBEdit;
    Label21: TLabel;
    DBEdit31: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBEdit16Exit(Sender: TObject);
    procedure DBEdit20Exit(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure DBEdit17Enter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryProdutoIntermediarioAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btGerarGradeClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoGradeSimples: TfrmProdutoGradeSimples;

implementation

uses fGerarGrade, fLookup_Padrao, fIncLinha, fProduto_Duplicar, fMenu,
  fGerarGradeSimples, fProdutoPosicaoEstoque;

{$R *.dfm}

procedure TfrmProdutoGradeSimples.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PRODUTO' ;
  nCdTabelaSistema  := 25 ;
  nCdConsultaPadrao := 42 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmProdutoGradeSimples.DBEdit2Exit(Sender: TObject);
begin
  PosicionaQuery(qryDepartamento, DBEdit2.Text) ;

  if (qryDepartamento.Active) then
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TfrmProdutoGradeSimples.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmProdutoGradeSimples.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategoria, DBEdit4.Text) ;

  if (qryCategoria.Active) then
      qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

end;

procedure TfrmProdutoGradeSimples.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubCategoria, DBEdit6.Text) ;

  if (qrySubCategoria.Active) then
  begin
    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
    qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  end;

end;

procedure TfrmProdutoGradeSimples.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qrySegmento, DBEdit7.Text) ;
end;

procedure TfrmProdutoGradeSimples.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMarca, DBEdit10.Text) ;

  if (qryMarca.Active) then
    qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

end;

procedure TfrmProdutoGradeSimples.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLinha, DBEdit11.Text) ;
end;

procedure TfrmProdutoGradeSimples.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryGrade, DBEdit15.Text) ;
end;

procedure TfrmProdutoGradeSimples.DBEdit16Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryColecao, DBEdit16.Text) ;
end;

procedure TfrmProdutoGradeSimples.DBEdit20Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryClasse, DBEdit20.Text) ;

end;

procedure TfrmProdutoGradeSimples.DBEdit21Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryStatus, DBEdit21.Text) ;

end;

procedure TfrmProdutoGradeSimples.DBEdit17Enter(Sender: TObject);
var
  cDescricao : String;
  cParametro : String;

begin
  inherited;

  If (DbEdit17.Text = '') and (qryMaster.state = dsInsert) then
  begin
      cParametro := frmMenu.LeParametro('FORMATNOMEPROD');

      if (cParametro = '') then
          qryMastercNmProduto.Value := Trim(DBEdit13.Text) + ' ' + Trim(DBEdit12.Text) + ' ' + Trim(DBEdit14.Text)

      else
      begin
          { -- atribui departamento -- }
          if (Pos('DE', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit3.Text + ' ';

          { -- atribui categoria -- }
          if (Pos('CA', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit5.Text + ' ';

          { -- atribui subcategoria -- }
          if (Pos('SU', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit8.Text + ' ';

          { -- atribui segmento -- }
          if (Pos('SE', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit9.Text + ' ';

          { -- atribui marca -- }
          if (Pos('MA', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit13.Text + ' ';

          { -- atribui linha -- }
          if (Pos('LI', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit14.Text + ' ';

          { -- atribui grade -- }
          if (Pos('GR', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit18.Text + ' ';

          { -- atribui cole��o -- }
          if (Pos('CO', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit19.Text + ' ';

          { -- atribui refer�ncia -- }
          if (Pos('RE', cParametro) > 0) then
              cDescricao := cDescricao + DBEdit12.Text + ' ';

          qryMastercNmProduto.Value := cDescricao;
      end;
  end;
end;

procedure TfrmProdutoGradeSimples.btSalvarClick(Sender: TObject);
var
    bInclusao : Boolean ;
begin

  bInclusao := false ;

  if (qryMaster.State = dsInsert) then
  begin
      bInclusao := true ;
      qryMasternCdTabTipoProduto.Value    := 1 ;
      qryMasternCdStatus.Value            := 1 ;
      qryMastercFlgProdVenda.Value        := 1 ;
      qryMastercUnidadeMedida.Value       := 'UN' ;
      qryMastercUnidadeMedidaCompra.Value := 'UN' ;
      qryMasternFatorCompra.Value         := 1 ;
      qryMasternQtdeMinimaCompra.Value    := 1 ;
      qryMasterdDtCad.Value               := Now() ;
      qryMasternCdUsuarioCad.Value        := frmMenu.nCdUsuarioLogado;
      qryMasternCdGrupo_Produto.Value     := qryDepartamentonCdGrupoProduto.Value ;
  end ;

  inherited;

  if (bInclusao) and (qryMasternCdProduto.Value > 0) and (qryMasternCdGrade.Value > 0) then
  begin
  
    case MessageDlg('Deseja gerar grade para deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: btGerarGrade.Click;
    end;

  end ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;
  

end;

procedure TfrmProdutoGradeSimples.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;
      
  PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


  PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
  qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryMasternCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, qryMasternCdSubCategoria.asString) ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, qryMasternCdSegmento.asString) ;

  PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;

  qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

  PosicionaQuery(qryLinha, qryMasternCdLinha.asString) ;

  PosicionaQuery(qryGrade, qryMasternCdGrade.asString) ;

  PosicionaQuery(qryColecao, qryMasternCdColecao.asString) ;

  PosicionaQuery(qryClasse, qryMasternCdClasseProduto.asString) ;

  PosicionaQuery(qryStatus, qryMasternCdStatus.asString) ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;
  PosicionaQuery(qryProdutoFinal, qryProdutoIntermediarionCdProduto.asString) ;

end;

procedure TfrmProdutoGradeSimples.qryProdutoIntermediarioAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryProdutoFinal,qryProdutoIntermediarionCdProduto.asString) ;

end;

procedure TfrmProdutoGradeSimples.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryLinha.Close ;
  qryGrade.Close ;
  qryColecao.Close ;
  qryClasse.Close ;
  qryStatus.Close ;
  qryProdutoIntermediario.Close ;
  qryProdutoFinal.Close ;


end;

procedure TfrmProdutoGradeSimples.btGerarGradeClick(Sender: TObject);
begin
  inherited;
  If (qryMasternCdProduto.Value > 0) then
  begin
      frmGerarGradeSimples.nCdProduto     := qryMasternCdProduto.Value ;
      frmGerarGradeSimples.MaskEdit3.Text := qryMasternValCusto.AsString;
      frmGerarGradeSimples.MaskEdit4.Text := qryMasternValVenda.AsString;
      frmGerarGradeSimples.ShowModal ;

      PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;
      PosicionaQuery(qryProdutoFinal, qryProdutoIntermediarionCdProduto.asString) ;

  end ;
end;

procedure TfrmProdutoGradeSimples.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(45);
            If (nPK > 0) then
            begin
                qryMasternCdDepartamento.Value := nPK ;
                PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;
                qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

                // consulta de categoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
                If (nPK > 0) then
                begin
                    qryMasternCdCategoria.Value := nPK ;
                    PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
                    qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                    // consulta de subcategoria
                    nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                    If (nPK > 0) then
                    begin
                        qryMasternCdSubCategoria.Value := nPK ;
                        PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.asString) ;

                        qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
                        qryMarca.Parameters.ParamByName('nCdSubCategoria').Value    := qrySubCategorianCdSubCategoria.Value ;

                        // Consulta de segmento
                        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                        If (nPK > 0) then
                        begin
                            qryMasternCdSegmento.Value := nPK ;
                            PosicionaQuery(qrySegmento,qryMasternCdSegmento.AsString) ;

                            // consulta de marca
                            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
                            If (nPK > 0) then
                            begin
                                qryMasternCdMarca.Value := nPK ;
                                PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;
                                qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

                                DBEdit11.SetFocus ;
                            end ;

                        end ;



                    end ;

                end ;

            end ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
            begin
                MensagemAlerta('Selecione um departamento.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
                qryMasternCdCategoria.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSubCategoria.Value := nPK ;
        end ;
    end ;
  end ;
end;

procedure TfrmProdutoGradeSimples.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qrySubCategoria.active or (qrySubCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSegmento.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryMarca.active or (qryMarcanCdMarca.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Marca.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdStatus = 1 and nCdMarca = ' + qryMarcanCdMarca.asString);
            If (nPK > 0) then
                qryMasternCdLinha.Value := nPK ;
        end ;
    end ;

    73 : begin
        if (qryMaster.State <> dsInsert) then
        begin
            MensagemAlerta('Fun��o s� permitida na inclus�o de produtos.') ;
            exit ;
        end ;

        frmIncLinha.nCdMarca   := qryMarcanCdMarca.Value ;
        frmIncLinha.Edit1.Text := '' ;
        frmIncLinha.ShowModal ;

        if (frmIncLinha.nCdLinha > 0) then
        begin
            qryMasternCdLinha.Value := frmIncLinha.nCdLinha ;
            PosicionaQuery(qryLinha, IntToStr(frmIncLinha.nCdLinha)) ;
            //DBEdit12.SetFocus ;
        end ;

    end ;

  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(51);
            If (nPK > 0) then
                qryMasternCdGrade.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(52);
            If (nPK > 0) then
                qryMasternCdColecao.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(53);
            If (nPK > 0) then
                qryMasternCdClasseProduto.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            If (nPK > 0) then
                qryMasternCdStatus.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoGradeSimples.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) then
      exit ;
      
  qryConsultaProd.Close ;

  if (Trim(dbEdit2.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdDepartamento').Value := StrToInt(Trim(DbEdit2.Text)) ;

  if (Trim(dbEdit4.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdCategoria').Value    := StrToInt(Trim(DbEdit4.Text)) ;

  if (Trim(dbEdit6.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSubCategoria').Value := StrToInt(Trim(DbEdit6.Text)) ;

  if (Trim(dbEdit7.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSegmento').Value     := StrToInt(Trim(DbEdit7.Text)) ;

  if (Trim(dbEdit10.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdMarca').Value        := StrToInt(Trim(DbEdit10.Text)) ;

  if (Trim(dbEdit11.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdLinha').Value        := StrToInt(Trim(DbEdit11.Text)) ;

  if (Trim(dbEdit12.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('cReferencia').Value     := Trim(DbEdit12.Text) ;

  qryConsultaProd.Open ;

  if not qryConsultaProd.eof then
  begin
      PosicionaPK(qryConsultaProdnCdProduto.Value) ;
  end ;

end;

procedure TfrmProdutoGradeSimples.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
end;

procedure TfrmProdutoGradeSimples.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (dbEdit2.Text = '') or (dbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o departamento.') ;
      dbEdit2.SetFocus ;
      Abort ;
  end ;

  If (dbEdit4.Text = '') or (dbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a categoria.') ;
      dbEdit4.SetFocus ;
      Abort ;
  end ;

  If (dbEdit6.Text = '') or (dbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe a Sub Categoria.') ;
      dbEdit6.SetFocus ;
      Abort ;
  end ;

  If (dbEdit7.Text = '') or (dbEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o Segmento.') ;
      dbEdit7.SetFocus ;
      Abort ;
  end ;

  If (dbEdit10.Text = '') or (dbEdit13.Text = '') then
  begin
      MensagemAlerta('Informe a marca.') ;
      dbEdit10.SetFocus ;
      Abort ;
  end ;

  If (dbEdit11.Text = '') or (dbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe a Linha.') ;
      dbEdit11.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') and (dbEdit15.Text <> '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If (dbEdit16.Text = '') or (dbEdit19.Text = '') then
  begin
      MensagemAlerta('Informe a Cole��o.') ;
      dbEdit16.SetFocus ;
      Abort ;
  end ;

  If (dbEdit17.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o.') ;
      dbEdit17.SetFocus ;
      Abort ;
  end ;

  inherited;

end;

procedure TfrmProdutoGradeSimples.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if qryMaster.eof then
  begin
      MensagemAlerta('Nenhum produto ativo para duplicar.') ;
      exit ;
  end ;

  frmProduto_Duplicar.nCdProduto         := qryMasternCdProduto.Value ;
  frmProduto_Duplicar.cDescricaoAnterior := qryMastercNmproduto.Value ;
  frmProduto_Duplicar.ShowModal;

  if (frmProduto_Duplicar.nCdProdutoNovo > 0) then
      PosicionaQuery(qryMaster, IntToStr(frmProduto_Duplicar.nCdProdutoNovo)) ;

end;

procedure TfrmProdutoGradeSimples.DBEdit28Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      btSalvar.Click;

  end ;

end;

procedure TfrmProdutoGradeSimples.cxButton1Click(Sender: TObject);
begin
  if (qryProdutoFinal.Active) then
  begin
      PosicionaQuery(frmProdutoPosicaoEstoque.qryPosicaoEstoque, qryProdutoFinalnCdProduto.AsString) ;

      frmProdutoPosicaoEstoque.ShowModal;
  end ;

end;

initialization
    RegisterClass(TfrmProdutoGradeSimples) ;

end.
