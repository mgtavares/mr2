inherited frmAprovacaoClienteVarejo_LimiteInicial: TfrmAprovacaoClienteVarejo_LimiteInicial
  Left = 465
  Top = 249
  Width = 490
  Height = 267
  Caption = 'Limite de Cr'#233'dito'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 474
    Height = 202
  end
  object Label1: TLabel [1]
    Left = 86
    Top = 48
    Width = 76
    Height = 13
    Caption = 'Renda Trabalho'
  end
  object Label2: TLabel [2]
    Left = 22
    Top = 112
    Width = 140
    Height = 13
    Caption = 'Renda Adicional Comprovada'
  end
  object Label3: TLabel [3]
    Left = 35
    Top = 146
    Width = 127
    Height = 13
    Caption = 'Renda Adicional Declarada'
  end
  object Label4: TLabel [4]
    Left = 43
    Top = 80
    Width = 119
    Height = 13
    Caption = 'Renda Trabalho C'#244'njuge'
  end
  object Label5: TLabel [5]
    Left = 52
    Top = 178
    Width = 110
    Height = 13
    Caption = 'Limite de Cr'#233'dito Inicial'
  end
  object Label6: TLabel [6]
    Left = 16
    Top = 210
    Width = 146
    Height = 13
    Caption = '% Entrada Compra Financiada'
  end
  inherited ToolBar1: TToolBar
    Width = 474
    ButtonWidth = 104
    inherited ToolButton1: TToolButton
      Caption = 'Conceder Limite'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 104
      Caption = '&Processar Aprova'#231#227'o'
    end
    inherited ToolButton2: TToolButton
      Left = 112
    end
  end
  object edtRendaTrabalho: TcxCurrencyEdit [8]
    Left = 168
    Top = 40
    Width = 137
    Height = 23
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 1
  end
  object edtRendaAddComprovada: TcxCurrencyEdit [9]
    Left = 168
    Top = 104
    Width = 137
    Height = 23
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 2
  end
  object edtRendaAddDeclarada: TcxCurrencyEdit [10]
    Left = 168
    Top = 136
    Width = 137
    Height = 23
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 3
  end
  object edtRendaCjg: TcxCurrencyEdit [11]
    Left = 168
    Top = 72
    Width = 137
    Height = 23
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 4
  end
  object edtLimiteCredito: TcxCurrencyEdit [12]
    Left = 168
    Top = 168
    Width = 137
    Height = 23
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 5
    OnKeyDown = edtLimiteCreditoKeyDown
    OnKeyPress = edtLimiteCreditoKeyPress
  end
  object edtPercEntrada: TcxCurrencyEdit [13]
    Left = 168
    Top = 200
    Width = 137
    Height = 23
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taVCenter
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.HideSelection = False
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 6
    OnKeyDown = edtPercEntradaKeyDown
    OnKeyPress = edtPercEntradaKeyPress
  end
  object btCalcular: TcxButton [14]
    Left = 320
    Top = 162
    Width = 145
    Height = 29
    Caption = 'F5 - Calcular Limite'
    TabOrder = 7
    TabStop = False
    OnClick = btCalcularClick
    Glyph.Data = {
      76020000424D760200000000000036000000280000000C000000100000000100
      1800000000004002000000000000000000000000000000000000C4AEA2806860
      806050705840705840705040705040605040605030605030705040B6A3948676
      66D0C0B0D0B8B0D0B0A0C0B0A0C0A8A0C0A8A0C0A090C0A090B0A090B0989077
      5F48957D66F0E0D0D0C0B0D0B8B0D0B0B0C0B0A0C0A8A0C0A8A0C0A090C0A090
      B0A090775F48957D75F0E0D0C08870B07050D0B8B0C08870B07050C0B0A0C088
      70B07050C0A090775F48958575F0E0E0FFC8B0C08870D0C0B0F0C8B0C08870C0
      B0A0F0C8B0C08070C0A890775F48958C84F0E8E0E0D0C0D0C8C0D0C0C0D0C0B0
      D0B8B0D0B8B0C0B0A0C0B0A0C0A8A0776757A48C84FFE8E0C09070B07050E0C8
      C0C09080B07050D0B8B0C09070B07050C0B0A0866757A49484FFF0F0FFC8B0C0
      8870E0D0C0F0C8B0C08870D0C0B0F0C8B0C08870D0B0A0866E57A49B93FFF0F0
      E0E0E0E0D8D0E0D0D0E0D0D0E0C8C0D0C8C0D0C0B0D0C0B0D0B8B0866E57B3A3
      93FFF8F09080D04030A0E0D8D09080D04030A0E0C8C09080D04030A0D0C0B086
      7666B3A3A2FFF8F0C0C0E09080D0E0E0E0C0C0E09080D0E0D0D0C0C0E09080D0
      D0C0C0957666B3AAA2FFF8FFFFE0D0FFD0B0FFD0B0FFC090F0B890D0A080D0A0
      80D0B0A0D0C8C0957D75B3AAA2FFFFFFFF9860FFA060FFA060FFA060FFA060FF
      A060FFA060B06850C0A890958575B3B2A2FFFFFFE07840F0D8D0F0D8D0F0C8B0
      E0B0A0E0A080E08050A05830D0A090958575D4C3BDBBB1A8F0B8A0D09070D088
      60D08050D07040D06830C06030D08060A08880C5B4A5E1D0C8C7C1B1B0A8A0B0
      A8A0B0A090B09890B09890A09890A09080A08880C7B8ACDBCAC0}
    LookAndFeel.NativeStyle = True
  end
  object SP_GERA_LIMITE_CREDITO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_LIMITE_CREDITO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValLimite'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nPercEntrada'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@cFlgAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 360
    Top = 120
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 324
    Top = 197
  end
  object SP_LIMITE_CREDITO_INICIAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_LIMITE_CREDITO_INICIAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValRenda'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValRendaCjg'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValRendaComp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValRendaDecl'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValLimiteInicial'
        Attributes = [paNullable]
        DataType = ftBCD
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 432
    Top = 112
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,nValRendaBruta'
      '      ,CASE WHEN cFlgRendaCjgLimite = 1 THEN nValRendaBrutaCjg'
      '            ELSE 0'
      '       END nValRendaBrutaCjg'
      '      ,(CASE WHEN nCdTabTipoRendaAdd1 = 1 THEN nValRendaAdd1'
      '             ELSE 0'
      '        END +'
      '        CASE WHEN nCdTabTipoRendaAdd2 = 1 THEN nValRendaAdd2'
      '             ELSE 0'
      '        END +  '
      '        CASE WHEN nCdTabTipoRendaAdd3 = 1 THEN nValRendaAdd3'
      '             ELSE 0'
      '        END) nValRendaComprovada'
      '      ,(CASE WHEN nCdTabTipoRendaAdd1 = 2 THEN nValRendaAdd1'
      '             ELSE 0'
      '        END +'
      '        CASE WHEN nCdTabTipoRendaAdd2 = 2 THEN nValRendaAdd2'
      '             ELSE 0'
      '        END +  '
      '        CASE WHEN nCdTabTipoRendaAdd3 = 2 THEN nValRendaAdd3'
      '             ELSE 0'
      '        END) nValRendaDeclarada'
      '   FROM vwClienteVarejo'
      '  WHERE nCdTerceiro = :nPK')
    Left = 336
    Top = 48
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceironValRendaBruta: TBCDField
      FieldName = 'nValRendaBruta'
      Precision = 12
      Size = 2
    end
    object qryTerceironValRendaBrutaCjg: TBCDField
      FieldName = 'nValRendaBrutaCjg'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object qryTerceironValRendaComprovada: TBCDField
      FieldName = 'nValRendaComprovada'
      ReadOnly = True
      Precision = 14
      Size = 2
    end
    object qryTerceironValRendaDeclarada: TBCDField
      FieldName = 'nValRendaDeclarada'
      ReadOnly = True
      Precision = 14
      Size = 2
    end
  end
end
