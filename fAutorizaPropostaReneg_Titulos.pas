unit fAutorizaPropostaReneg_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, Menus, DBGridEhGrouping;

type
  TfrmAutorizaPropostaReneg_Titulos = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosiDiasAtraso: TIntegerField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnValLiq: TBCDField;
    qryTitulosnValJuro: TBCDField;
    qryTitulosnValMulta: TBCDField;
    qryTitulosnValDesconto: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    dsTitulos: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure ExibeTitulos(nCdPropostaReneg:Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    dDtProposta: TDateTime;
  end;

var
  frmAutorizaPropostaReneg_Titulos: TfrmAutorizaPropostaReneg_Titulos;

implementation

{$R *.dfm}

procedure TfrmAutorizaPropostaReneg_Titulos.ExibeTitulos(
  nCdPropostaReneg: Integer);
begin

    qryTitulos.Close;
    qryTitulos.Parameters.ParamByName('dDtBase').Value := DateToStr(dDtProposta) ;
    PosicionaQuery(qryTitulos, intToStr(nCdPropostaReneg)) ;

    showForm(Self,true);    

end;

procedure TfrmAutorizaPropostaReneg_Titulos.FormShow(Sender: TObject);
begin
  inherited;

  qryTitulosnSaldoTit.DisplayLabel := 'T�tulos Vinculados nesta Proposta|Saldo Pagar|em ' + DateTimeToStr(dDtProposta) ;
  DBGridEh1.Align := alClient;
  DBGridEh1.Refresh;

end;

end.
