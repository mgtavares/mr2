inherited frmGrupoPedido: TfrmGrupoPedido
  Caption = 'frmGrupoPedido'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 38
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 62
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoPedido'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoPedido'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [6]
    Left = 96
    Top = 80
    Width = 249
    Height = 17
    Caption = 'Grupo de Pedido que permite Faturamento'
    DataField = 'cFlgFaturar'
    DataSource = dsMaster
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOPEDIDO'
      'WHERE nCdGrupoPedido = :nPK')
    object qryMasternCdGrupoPedido: TIntegerField
      FieldName = 'nCdGrupoPedido'
    end
    object qryMastercNmGrupoPedido: TStringField
      FieldName = 'cNmGrupoPedido'
      Size = 50
    end
    object qryMastercFlgFaturar: TIntegerField
      FieldName = 'cFlgFaturar'
    end
  end
end
