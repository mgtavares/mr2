inherited frmLibDivRec_PrecoVenda: TfrmLibDivRec_PrecoVenda
  Left = 382
  Top = 234
  Width = 253
  Height = 124
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Altera'#231#227'o Pre'#231'o Venda'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 237
    Height = 88
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 48
    Width = 88
    Height = 13
    Caption = 'Novo pre'#231'o venda'
  end
  inherited ToolBar1: TToolBar
    Width = 237
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object edtPrecoVenda: TcxCurrencyEdit [3]
    Left = 104
    Top = 40
    Width = 121
    Height = 21
    Properties.ValidateOnEnter = True
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsFlat
    TabOrder = 1
    OnKeyDown = oVendaKeyDown
  end
  object cxCurrencyEdit1: TcxCurrencyEdit [4]
    Left = 104
    Top = 96
    Width = 121
    Height = 21
    Properties.ValidateOnEnter = True
    Style.BorderColor = clBlack
    Style.BorderStyle = ebsFlat
    TabOrder = 2
    OnKeyDown = oVendaKeyDown
  end
end
