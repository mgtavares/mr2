unit fAcompProducaoPedidoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, Mask, DBCtrls, ADODB, cxPC, cxControls, DBGridEhGrouping,
  GridsEh, DBGridEh, Menus;

type
  TfrmAcompProducaoPedidoVenda = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonCdTerceiroPagador: TIntegerField;
    qryPedidocNmTerceiroPagador: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryPreparaTemp: TADOQuery;
    SP_ACOMPANHA_PRODUCAO_PEDIDO: TADOStoredProc;
    qryTemp: TADOQuery;
    qryTempnCdProduto: TIntegerField;
    qryTempcNmProduto: TStringField;
    qryTempnQtdePed: TBCDField;
    qryTempnUnidadeMedida: TStringField;
    qryTempnSaldoEstoque: TIntegerField;
    qryTempnQtdeOrdem: TIntegerField;
    qryTempnQtdeEmpenhado: TIntegerField;
    qryTempnQtdeDisponivel: TIntegerField;
    qryTempnCdOrdemProducao: TIntegerField;
    qryTempcNumeroOP: TStringField;
    qryTempdDtPrevConclusao: TDateTimeField;
    qryTempcNmTabTipoStatusOP: TStringField;
    qryTempnQtdePlanejada: TIntegerField;
    qryTempnQtdeConcluida: TIntegerField;
    qryTempdDtInicioPrevisto: TDateTimeField;
    qryTempdDtInicioOP: TDateTimeField;
    qryTempdDtRealConclusao: TDateTimeField;
    qryTempcFlgAux: TIntegerField;
    dsTemp: TDataSource;
    SP_GERA_OP_PRODUTO_PEDIDO: TADOStoredProc;
    PopupMenu1: TPopupMenu;
    CancelarOP1: TMenuItem;
    VisualizarOP1: TMenuItem;
    procedure exibeAcompanhamento( nCdPedido : integer );
    procedure FormShow(Sender: TObject);
    procedure qryTempAfterScroll(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure exibeOP( nCdOrdemProducao : integer ) ;
    procedure VisualizarOP1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bGerarOP : boolean
  end;

var
  frmAcompProducaoPedidoVenda: TfrmAcompProducaoPedidoVenda;

implementation

uses fMenu, fOP;

{$R *.dfm}

{ TfrmAcompProducaoPedidoVenda }

procedure TfrmAcompProducaoPedidoVenda.exibeAcompanhamento(
  nCdPedido: integer);
begin


    qryPedido.Close;
    PosicionaQuery( qryPedido , intToStr( nCdPedido ) ) ;

    qryPreparaTemp.ExecSQL ;

    try

        SP_ACOMPANHA_PRODUCAO_PEDIDO.Close ;
        SP_ACOMPANHA_PRODUCAO_PEDIDO.Parameters.ParamByName('@nCdPedido').Value := nCdPedido ;
        SP_ACOMPANHA_PRODUCAO_PEDIDO.ExecProc ;

    except

        MensagemErro('Erro no processamento.') ;
        raise ;

    end ;

    qryTemp.Close;
    qryTemp.Open ;

    if ( qryTemp.eof ) then
        MensagemAlerta('Nenhum produto produzido para venda no pedido selecionado.') ;

    Self.ShowModal ;

end;

procedure TfrmAcompProducaoPedidoVenda.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Enabled      := bGerarOP ;
  DBGridEh1.Font.Size      := 8 ;
  DBGridEh1.TitleFont.Size := 8 ;
  
  DBGridEh1.SetFocus ;

end;

procedure TfrmAcompProducaoPedidoVenda.qryTempAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.Columns[13].ReadOnly := (qryTempnCdOrdemProducao.Value > 0)
  
end;

procedure TfrmAcompProducaoPedidoVenda.ToolButton1Click(Sender: TObject);
var
  iQtdeOP : integer;
begin
  inherited;

  if (qryTemp.State in [dsEdit]) then
      qryTemp.Post ;
      
  iQtdeOP := 0 ;

  qryTemp.First ;

  while not qryTemp.Eof do
  begin

      if (qryTempcFlgAux.Value = 1) then
      begin
          iQtdeOP := iQtdeOP + 1 ;

          if (qryTempnQtdePlanejada.Value <= 0) then
          begin

              MensagemAlerta('Informe a quantidade planejada.'+#13#13+'Produto : ' + qryTempnCdProduto.asString + ' - ' + qryTempcNmProduto.Value) ;
              abort ;

          end ;

      end ;

      qryTemp.Next ;

  end ;

  qryTemp.First ;

  if (iQtdeOP = 0) then
  begin

      MensagemAlerta('Nenhum produto selecionado para gera��o de ordem de produ��o.') ;
      abort ;

  end ;

  if (MessageDlg('Confirma a gera��o das ordens de produ��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryTemp.First ;

  try
      while not qryTemp.Eof do
      begin

          {-- se o produto foi selecionado e n�o tem ordem de produ��o gerada ainda --}
          if (qryTempcFlgAux.Value = 1) and (qryTempnCdOrdemProducao.Value = 0) then
          begin

              frmMenu.Connection.BeginTrans ;

              try

                  SP_GERA_OP_PRODUTO_PEDIDO.Close;
                  SP_GERA_OP_PRODUTO_PEDIDO.Parameters.ParamByName('@nCdPedido').Value      := qryPedidonCdPedido.Value ;
                  SP_GERA_OP_PRODUTO_PEDIDO.Parameters.ParamByName('@nCdProduto').Value     := qryTempnCdProduto.Value ;
                  SP_GERA_OP_PRODUTO_PEDIDO.Parameters.ParamByName('@nQtdePlanejada').Value := qryTempnQtdePlanejada.Value ;
                  SP_GERA_OP_PRODUTO_PEDIDO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado ;
                  SP_GERA_OP_PRODUTO_PEDIDO.ExecProc;

                  {-- retira a marca��o do item --}
                  qryTemp.Edit ;
                  qryTempcFlgAux.Value := 0 ;
                  qryTemp.Post ;

              except

                  frmMenu.Connection.RollbackTrans ;
                  MensagemErro('Erro no Processamento.') ;
                  raise ;

              end ;

              frmMenu.Connection.CommitTrans;

          end ;

          qryTemp.Next ;

      end ;

  finally

      qryTemp.First ;

      {-- atualiza os dados da tela --}
      qryPreparaTemp.ExecSQL ;

      try

          SP_ACOMPANHA_PRODUCAO_PEDIDO.Close ;
          SP_ACOMPANHA_PRODUCAO_PEDIDO.Parameters.ParamByName('@nCdPedido').Value := qryPedidonCdPedido.Value ;
          SP_ACOMPANHA_PRODUCAO_PEDIDO.ExecProc ;

      except

          MensagemErro('Erro no processamento.') ;
          raise ;

      end ;

      qryTemp.Close;
      qryTemp.Open ;

  end ;

  if ( iQtdeOP = 1 ) then
      ShowMessage('Ordem de produ��o gerada com Sucesso!')
  else ShowMessage('Ordens de produ��o geradas com Sucesso!') ;

end;

procedure TfrmAcompProducaoPedidoVenda.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAcompProducaoPedidoVenda.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAcompProducaoPedidoVenda.qryTempBeforePost(
  DataSet: TDataSet);
begin

  if (qryTempcFlgAux.Value = 1) and (qryTempnCdOrdemProducao.Value > 0) then
  begin
      ShowMessage('Este produto j� tem uma ordem de produ��o gerada. Imposs�vel selecionar.') ;
      abort; 
  end ;

  inherited;

end;

procedure TfrmAcompProducaoPedidoVenda.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  VisualizarOP1.Enabled := ( (qryTemp.Active) and (qryTempnCdOrdemProducao.Value > 0) ) ;
  CancelarOP1.Enabled   := ( (qryTemp.Active) and (qryTempnCdOrdemProducao.Value > 0) ) ;

end;

procedure TfrmAcompProducaoPedidoVenda.exibeOP(nCdOrdemProducao: integer) ;
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( nCdOrdemProducao );

  ToolButton1.Enabled := False ;
  
  showForm( objForm , TRUE ) ;

end;

procedure TfrmAcompProducaoPedidoVenda.VisualizarOP1Click(Sender: TObject);
begin
  inherited;

  exibeOP ( qryTempnCdOrdemProducao.Value ) ;

end;

procedure TfrmAcompProducaoPedidoVenda.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if ( ( qryTemp.Active ) and ( qryTempnCdOrdemProducao.Value > 0 ) ) then
      exibeOP ( qryTempnCdOrdemProducao.Value ) ;

end;

end.
