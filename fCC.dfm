inherited frmCC: TfrmCC
  Left = 56
  Top = 75
  Caption = 'Centro de Custo'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 22
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 11
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmCC'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBRadioGroup1: TDBRadioGroup [6]
    Left = 64
    Top = 93
    Width = 153
    Height = 41
    Caption = 'Centro de Custo Ativo ?'
    Columns = 2
    DataField = 'nCdStatus'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 3
    Values.Strings = (
      '1'
      '2')
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK')
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryMasternCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object qryMasternCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object qryMastercCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryMastercCdHie: TStringField
      FieldName = 'cCdHie'
      FixedChar = True
      Size = 4
    end
    object qryMasteriNivel: TSmallintField
      FieldName = 'iNivel'
    end
    object qryMastercFlgLanc: TIntegerField
      FieldName = 'cFlgLanc'
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
end
