unit fFluxoCaixaRealizado_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, cxGridCustomPopupMenu, cxGridPopupMenu,
  ER2Excel, cxCheckBox;

type
  TfrmFluxoCaixaRealizado_View = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    SP_FLUXOCAIXA_REALIZADO: TADOStoredProc;
    dsResultado: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryResultado: TADOQuery;
    qryResultadoiOrdem: TIntegerField;
    qryResultadocDescricao: TStringField;
    qryResultadonValor: TFloatField;
    dsResumoLancto: TDataSource;
    cxGrid1Level2: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2iOrdem: TcxGridDBColumn;
    cxGridDBTableView2cDescricao: TcxGridDBColumn;
    cxGridDBTableView2nValor: TcxGridDBColumn;
    qrySaldoAnterior: TADOQuery;
    qrySaldoAnteriornCdContaBancaria: TIntegerField;
    qrySaldoAnteriorcNmContaBancaria: TStringField;
    qrySaldoAnteriordDtSaldoAnterior: TDateField;
    qrySaldoAnteriornValorAnterior: TFloatField;
    dsSaldoAnterior: TDataSource;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1nCdContaBancaria: TcxGridDBColumn;
    cxGrid3DBTableView1cNmContaBancaria: TcxGridDBColumn;
    cxGrid3DBTableView1dDtSaldoAnterior: TcxGridDBColumn;
    cxGrid3DBTableView1nValorAnterior: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    cxTabSheet3: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qrySaldoFinal: TADOQuery;
    dsSaldoFinal: TDataSource;
    qrySaldoFinalnCdContaBancaria: TIntegerField;
    qrySaldoFinalcNmContaBancaria: TStringField;
    qrySaldoFinaldDtSaldoFinal: TDateField;
    qrySaldoFinalnValorFinal: TFloatField;
    cxGridDBTableView1nCdContaBancaria: TcxGridDBColumn;
    cxGridDBTableView1cNmContaBancaria: TcxGridDBColumn;
    cxGridDBTableView1dDtSaldoFinal: TcxGridDBColumn;
    cxGridDBTableView1nValorFinal: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qrySaldoFinalnValEntradas: TFloatField;
    qrySaldoFinalnValSaidas: TFloatField;
    qrySaldoFinalnValDiferenca: TFloatField;
    cxGridDBTableView1nValEntradas: TcxGridDBColumn;
    cxGridDBTableView1nValSaidas: TcxGridDBColumn;
    cxGridDBTableView1nValDiferenca: TcxGridDBColumn;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ER2Excel1: TER2Excel;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cFiltro : string ;
    imprimeSaldoFinal : boolean ;
  end;

var
  frmFluxoCaixaRealizado_View: TfrmFluxoCaixaRealizado_View;

implementation

uses fFluxoCaixaRealizado_Lanctos, fMenu, rFluxoCaixaRealizado_view, rFluxoCaixaRealizadoAnalitico;

var
  objForm : TfrmFluxoCaixaRealizado_Lanctos ;

{$R *.dfm}

procedure TfrmFluxoCaixaRealizado_View.FormShow(Sender: TObject);
var
   i : integer ;
begin
  inherited;

  qryResultado.Recordset := SP_FLUXOCAIXA_REALIZADO.NextRecordset(i) ;
  qryResultado.Open ;

  qrySaldoAnterior.Recordset := SP_FLUXOCAIXA_REALIZADO.NextRecordset(i) ;
  qrySaldoAnterior.Open ;

  qrySaldoFinal.Recordset := SP_FLUXOCAIXA_REALIZADO.NextRecordset(i) ;
  qrySaldoFinal.Open ;

  objForm := TfrmFluxoCaixaRealizado_Lanctos.Create( Self ) ;

  cxPageControl1.ActivePageIndex := 0 ;

  if (cxTabSheet1.Enabled) then
      cxPageControl1.ActivePageIndex := 0
  else cxPageControl1.ActivePageIndex := 1 ;

end;

procedure TfrmFluxoCaixaRealizado_View.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if not SP_FLUXOCAIXA_REALIZADO.eof then
  begin

      objForm.qryLanctos.Close;
      objForm.qryLanctos.Parameters.ParamByName('nCdPlanoConta').Value := SP_FLUXOCAIXA_REALIZADO.FieldValues['nCdPlanoConta'] ;
      objForm.qryLanctos.Open;

      if (frmMenu.LeParametro('VAREJO') = 'N') then
      begin
          objForm.cxGrid1DBTableView1nCdLojaPG.Visible       := False ;
          objForm.cxGrid1DBTableView1nCdLojaTit.Visible      := False ;
      end ;

      showForm( objForm , FALSE ) ;

  end ;

end;

procedure TfrmFluxoCaixaRealizado_View.ToolButton5Click(Sender: TObject);
var
  objRel : TrptFluxoCaixaRealizado_view ;
begin
  inherited;

  objRel := TrptFluxoCaixaRealizado_view.Create( Self ) ;

  objRel.qryResumoContas.Recordset := SP_FLUXOCAIXA_REALIZADO.Recordset ;
  objRel.qryResumoContas.Open ;

  objRel.qryResumo.Recordset := qryResultado.Recordset ;
  objRel.qryResumo.Open ;

  objRel.qrySaldoFinal.Recordset := qrySaldoFinal.Recordset ;
  objRel.qrySaldoFinal.Open ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  objRel.lblFiltro1.Caption := cFiltro ;

  if (imprimeSaldoFinal) then objRel.imprimeSaldoFinal := True
  else objRel.imprimeSaldoFinal := False ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QRCompositeReport1.Preview ;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;


end;

procedure TfrmFluxoCaixaRealizado_View.ToolButton7Click(Sender: TObject);
var
  n                  : integer;
  linha              : integer;
  iLinhaAux          : integer;
  cTipo              : String;
  cNmGrupoPlanoConta : String;
  iLinhaAuxSoma      : integer;
begin
  inherited;

  ER2Excel1.Celula['A1'].Text := Copy(Self.Caption,0,66);

  linha              := 2;
  iLinhaAux          := 0;
  iLinhaAuxSoma      := 0;
  cTipo              := '';
  cNmGrupoPlanoConta := '';

  SP_FLUXOCAIXA_REALIZADO.First;

  while not SP_FLUXOCAIXA_REALIZADO.Eof do
  begin

      if (cTipo <> SP_FLUXOCAIXA_REALIZADO.FieldValues['cTipo']) then
      begin
          if (cTipo <> '') then
              linha := linha + 2
          else linha := linha + 1;

          cTipo := SP_FLUXOCAIXA_REALIZADO.FieldValues['cTipo'];
          ER2Excel1.Celula['A' + intToStr(linha)].Text     := cTipo;
          ER2Excel1.Celula['A' + intToStr(linha)].FontSize := 16;
          ER2Excel1.Celula['A' + intToStr(linha)].Height   := 21;
          ER2Excel1.Celula['A' + intToStr(linha)].Negrito;

          cNmGrupoPlanoConta := '';
      end;

      if (cNmGrupoPlanoConta <> SP_FLUXOCAIXA_REALIZADO.FieldValues['cNmGrupoPlanoConta']) then
      begin
          if (cNmGrupoPlanoConta <> '') then
              linha := linha + 2
          else linha := linha + 1;

          iLinhaAux := linha + 1;

          for n := 10 to cxGrid1DBTableView1.ColumnCount - 1 do
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].Text            := cxGrid1DBTableView1.Columns[n].Caption;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].VerticalAlign   := vaMiddle;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].Background      := clSilver;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].Negrito;
          end;

          cNmGrupoPlanoConta := SP_FLUXOCAIXA_REALIZADO.FieldValues['cNmGrupoPlanoConta'];
          ER2Excel1.Celula['A' + intToStr(linha)].Text       := cNmGrupoPlanoConta ;
          ER2Excel1.Celula['A' + intToStr(linha)].Background := clSilver;
          ER2Excel1.Celula['A' + intToStr(linha)].Negrito;
      end;

      linha := linha + 1;

      ER2Excel1.Celula['A' + intToStr(linha)].Text := SP_FLUXOCAIXA_REALIZADO.FieldValues['cNmPlanoConta'] ;

      for n := 10 to SP_FLUXOCAIXA_REALIZADO.FieldCount - 1 do
      begin
          ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].Text            := SP_FLUXOCAIXA_REALIZADO.Fields[n].Value;
          ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha)].HorizontalAlign := haRight;
      end;

      SP_FLUXOCAIXA_REALIZADO.Next;

      if ((cNmGrupoPlanoConta <> SP_FLUXOCAIXA_REALIZADO.FieldValues['cNmGrupoPlanoConta']) or (SP_FLUXOCAIXA_REALIZADO.Eof)) then
      begin
          for n := 10 to cxGrid1DBTableView1.ColumnCount - 1 do
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].AutoSoma(ER2Excel1.RetornaEndereco(n - 8) + intToStr(iLinhaAux),ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha));
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].Negrito;
          end;

          linha := linha + 1;

          if (iLinhaAuxSoma = 0) then
              iLinhaAuxSoma := linha;
      end;

      if ((cTipo <> SP_FLUXOCAIXA_REALIZADO.FieldValues['cTipo']) or (SP_FLUXOCAIXA_REALIZADO.Eof)) then
      begin

          for n := 10 to cxGrid1DBTableView1.ColumnCount - 1 do
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].AutoSoma(ER2Excel1.RetornaEndereco(n - 8) + intToStr(iLinhaAuxSoma),ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha));
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].Border          := [EdgeTop];
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(n - 8) + intToStr(linha + 1)].Negrito;
          end;

          ER2Excel1.Celula['A' + intToStr(linha + 1)].Text            := 'TOTAL';
          ER2Excel1.Celula['A' + intToStr(linha + 1)].HorizontalAlign := haRight;
          ER2Excel1.Celula['A' + intToStr(linha + 1)].Negrito;

          linha := linha + 1;
          iLinhaAuxSoma := 0;

      end;

  end;

  linha := Linha + 2;

  ER2Excel1.Celula['A' + intToStr(linha)].Text     := 'RESUMO';
  ER2Excel1.Celula['A' + intToStr(linha)].FontSize := 16;
  ER2Excel1.Celula['A' + intToStr(linha)].Height   := 21;
  ER2Excel1.Celula['A' + intToStr(linha)].Negrito;

  qryResultado.First;

  while not qryResultado.Eof do
  begin
      linha := linha + 1;

      ER2Excel1.Celula['A' + intToStr(linha)].Text            := qryResultado.FieldValues['cDescricao'];
      ER2Excel1.Celula['B' + intToStr(linha)].Text            := qryResultadonValor.AsCurrency;
      ER2Excel1.Celula['B' + intToStr(linha)].HorizontalAlign := haRight;

      qryResultado.Next;
  end;

  linha := linha + 2;

  ER2Excel1.Celula['A' + intToStr(linha)].Text     := 'COMPOSI��O SALDO FINAL';
  ER2Excel1.Celula['A' + intToStr(linha)].FontSize := 12;
  ER2Excel1.Celula['A' + intToStr(linha)].Negrito;

  linha := linha + 1;

  ER2Excel1.Celula['A' + intToStr(linha)].Text       := 'C�d/Conta';
  ER2Excel1.Celula['A' + intToStr(linha)].Background := clSilver;
  ER2Excel1.Celula['A' + intToStr(linha)].Negrito;
  ER2Excel1.Celula['A' + intToStr(linha)].Range('C' + intToStr(linha));

  ER2Excel1.Celula['B' + intToStr(linha)].Text            := 'Saldo Final';
  ER2Excel1.Celula['B' + intToStr(linha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + intToStr(linha)].Negrito;

  ER2Excel1.Celula['C' + intToStr(linha)].Text := 'Data Saldo';
  ER2Excel1.Celula['C' + intToStr(linha)].Negrito;

  qrySaldoFinal.First;

  while not qrySaldoFinal.Eof do
  begin
      linha := linha + 1;

      ER2Excel1.Celula['A' + intToStr(linha)].Text := qrySaldoFinalnCdContaBancaria.AsString + ' (' + qrySaldoFinalcNmContaBancaria.Value + ')';
      
      ER2Excel1.Celula['B' + intToStr(linha)].Text            := qrySaldoFinalnValorFinal.AsCurrency;
      ER2Excel1.Celula['B' + intToStr(linha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['C' + intToStr(linha)].Text    := qrySaldoFinaldDtSaldoFinal.AsString;
      ER2Excel1.Celula['C' + intToStr(linha)].Mascara := 'dd/mm/aaaa';
      
      qrySaldoFinal.Next;
  end;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

procedure TfrmFluxoCaixaRealizado_View.ToolButton9Click(Sender: TObject);
var
  objRel : TrptFluxoCaixaRealizadoAnalitico;
begin
  inherited;

  objRel := TrptFluxoCaixaRealizadoAnalitico.Create( Self ) ;

  try

      objRel.qryTempResumoContas.Close;
      objRel.qryTempResumoContas.Open;

      objRel.cxGrid1DBTableView1.ViewData.Expand( TRUE );

      objRel.cFiltro := cFiltro ;

      showForm( objRel , FALSE ) ;

  finally
      freeAndNil(objRel) ;
  end ;

end;

end.
