unit fBaixaTituloDeposito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, DBCtrls, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmBaixaTituloDeposito = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    edtBanco: TMaskEdit;
    Label3: TLabel;
    edtDtInicial: TMaskEdit;
    Label6: TLabel;
    edtDtFinal: TMaskEdit;
    edtTerceiro: TMaskEdit;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryBanco: TADOQuery;
    qryBanconCdBanco: TIntegerField;
    qryBancocNmBanco: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryLanctos: TADOQuery;
    qryLanctosnCdLanctoFin: TAutoIncField;
    qryLanctosdDtExtrato: TDateTimeField;
    qryLanctosnCdBanco: TIntegerField;
    qryLanctoscAgencia: TIntegerField;
    qryLanctosnCdConta: TStringField;
    qryLanctosdDtConciliacao: TDateTimeField;
    qryLanctoscNmUsuarioConciliacao: TStringField;
    qryLanctosnValLancto: TBCDField;
    qryLanctosnSaldoPendenteIdent: TBCDField;
    qryLanctosnCdTerceiroDep: TIntegerField;
    qryLanctoscNmTerceiro: TStringField;
    dsLanctos: TDataSource;
    cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGrid1DBTableView1dDtExtrato: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cAgencia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdConta: TcxGridDBColumn;
    cxGrid1DBTableView1dDtConciliacao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuarioConciliacao: TcxGridDBColumn;
    cxGrid1DBTableView1nValLancto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoPendenteIdent: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiroDep: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtBancoExit(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtBancoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaTituloDeposito: TfrmBaixaTituloDeposito;

implementation

uses fMenu, fBaixaTituloDeposito_Movtos, fLookup_Padrao;

{$R *.dfm}

procedure TfrmBaixaTituloDeposito.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Now()-60) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Now()) ;

  qryLanctos.Close ;
  qryLanctos.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryLanctos.Parameters.ParamByName('nCdBanco').Value    := frmMenu.ConvInteiro(edtBanco.Text) ;
  qryLanctos.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtTerceiro.Text) ;
  qryLanctos.Parameters.ParamByName('cTipo').Value       := RadioGroup1.ItemIndex;
  qryLanctos.Parameters.ParamByName('dDtInicial').Value  := DateToStr(frmMenu.ConvData(edtDtInicial.Text)) ;
  qryLanctos.Parameters.ParamByName('dDtFinal').Value    := DateToStr(frmMenu.ConvData(edtDtFinal.Text)) ;
  qryLanctos.Open ;

  if qryLanctos.eof then
      MensagemAlerta('Nenhum dep�sito encontrado para o crit�rio utilizado.') ;
      
end;

procedure TfrmBaixaTituloDeposito.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
  
end;

procedure TfrmBaixaTituloDeposito.edtBancoExit(Sender: TObject);
begin
  inherited;

  qryBanco.Close ;
  PosicionaQuery(qryBanco, edtBanco.Text) ;

end;

procedure TfrmBaixaTituloDeposito.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmBaixaTituloDeposito_Movtos ;
begin
  inherited;

  if not qryLanctos.Eof then
  begin

      objForm := TfrmBaixaTituloDeposito_Movtos.Create(Self) ;

      objForm.nCdTerceiro  := qryLanctosnCdTerceiroDep.Value ;
      objForm.nSaldo       := qryLanctosnSaldoPendenteIdent.Value ;
      objForm.dDtDeposito  := qryLanctosdDtExtrato.Value ;
      objForm.nCdLanctoFin := qryLanctosnCdLanctoFin.Value ;
      objForm.bMovimento   := False ;

      PosicionaQuery(objForm.qryMovtos,qryLanctosnCdlanctoFin.asString) ;

      showForm( objForm , FALSE ) ;

      if (objForm.bMovimento) then
      begin
          qryLanctos.Close ;
          qryLanctos.Open ;
      end ;

  end ;

end;

procedure TfrmBaixaTituloDeposito.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                edtTerceiro.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmBaixaTituloDeposito.edtBancoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                edtBanco.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmBaixaTituloDeposito) ;

end.
