unit fCargaInicial_ChequeTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, ADODB,
  DB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmCargaInicial_ChequeTerceiro = class(TfrmProcesso_Padrao)
    cmdTemp: TADOCommand;
    DBGridEh1: TDBGridEh;
    qryCheque: TADOQuery;
    qryChequenCdCheque: TAutoIncField;
    qryChequecLinha: TStringField;
    qryChequenCdBanco: TIntegerField;
    qryChequecAgencia: TStringField;
    qryChequecConta: TStringField;
    qryChequecDigito: TStringField;
    qryChequecCNPJCPF: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryChequedDtDeposito: TDateTimeField;
    qryChequenCdTerceiroResp: TIntegerField;
    dsCheque: TDataSource;
    qryChequecNmTerceiro: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    SP_CARGA_INICIAL_CHEQUE_TERCEIRO: TADOStoredProc;
    qryChequenCdContaBancariaDep: TIntegerField;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryChequecNmContaBancaria: TStringField;
    qryContaBancarianCdConta: TStringField;
    procedure qryChequeCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCargaInicial_ChequeTerceiro: TfrmCargaInicial_ChequeTerceiro;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCargaInicial_ChequeTerceiro.qryChequeCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryChequenCdTerceiroResp.Value > 0) and (qryChequecNmTerceiro.Value = '') then
  begin
      qryTerceiro.Close ;
      PosicionaQuery(qryTerceiro, qryChequenCdTerceiroResp.AsString) ;
      qryChequecNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;
  end ;

  if (qryChequenCdContaBancariaDEp.Value > 0) and (qryChequecNmContaBancaria.Value = '') then
  begin
      qryContaBancaria.Close ;
      qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      PosicionaQuery(qryContaBancaria, qryChequenCdContaBancariaDep.asString) ;
      qryChequecNmContaBancaria.Value := qryContaBancarianCdConta.Value ;
  end ;

end;

procedure TfrmCargaInicial_ChequeTerceiro.FormShow(Sender: TObject);
begin
  inherited;

  cmdTemp.Execute;

  qryCheque.Close ;
  qryCheque.Open ;
  
end;

procedure TfrmCargaInicial_ChequeTerceiro.DBGridEh1ColExit(
  Sender: TObject);
begin
  inherited;

  if (qryChequecLinha.Value <> '') and (qryChequenCdBanco.Value = 0) and (qryCheque.State <> dsBrowse) then
  begin

      try
          qryChequeiNrCheque.Value := StrToInt(Copy(qryChequecLinha.Value,14,6)) ;
          qryChequecAgencia.Value  := Copy(qryChequecLinha.Value,5,4)            ;
          qryChequecConta.Value    := Copy(qryChequecLinha.Value,23,10)          ;
          qryChequecDigito.Value   := Copy(qryChequecLinha.Value,33,1)           ;
          qryChequenCdBanco.Value  := StrToInt(Copy(qryChequecLinha.Value,2,3))  ;
      except
      end ;

  end ;


end;

procedure TfrmCargaInicial_ChequeTerceiro.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (qryCheque.State = dsBrowse) then
             qryCheque.Edit ;

        if ((qryCheque.State = dsInsert) or (qryCheque.State = dsEdit)) and (DBGridEh1.Col = 11) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryChequenCdTerceiroResp.Value := nPK ;
            end ;

        end ;

        if ((qryCheque.State = dsInsert) or (qryCheque.State = dsEdit)) and (DBGridEh1.Col = 13) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((cFlgCaixa = 1 OR cFlgCofre = 1))');

            If (nPK > 0) then
            begin
                qryChequenCdContaBancariaDep.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCargaInicial_ChequeTerceiro.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (not qryCheque.Active) or (qryCheque.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum cheque para grava��o.') ;
      exit ;
  end ;

  qryCheque.First ;

  while not qryCheque.eof do
  begin

      if (qryChequenCdBanco.Value = 0) then
      begin
          MensagemAlerta('Informe o c�digo do banco.') ;
          exit ;
      end ;

      if (qryChequecAgencia.Value = '') then
      begin
          MensagemAlerta('Informe a ag�ncia.') ;
          exit ;
      end ;

      if (qryChequecConta.Value = '') then
      begin
          MensagemAlerta('Informe o n�mero da conta.') ;
          exit ;
      end ;

      if (qryChequecDigito.Value = '') then
      begin
          MensagemAlerta('Informe o d�gito da conta.') ;
          exit ;
      end ;

      if (qryChequeiNrCheque.Value <= 0) then
      begin
          MensagemAlerta('Informe o n�mero do cheque.') ;
          exit ;
      end ;

      if (qryChequenValCheque.Value <= 0) then
      begin
          MensagemAlerta('Informe o valor do cheque.') ;
          exit ;
      end ;

      if (frmMenu.TestaCpfCgc(qryChequecCNPJCPF.Value) = '') then
          exit ;

      if (qryChequedDtDeposito.AsString = '') then
      begin
          MensagemAlerta('Informe a data de dep�sito.') ;
          exit ;
      end ;

      if (qryChequecNmterceiro.Value = '') then
      begin
          MensagemAlerta('Informe o terceiro respons�vel pelo cheque.') ;
          exit ;
      end ;

      if (qryChequecNmContaBancaria.Value = '') then
      begin
          MensagemAlerta('Informe a conta portadora.') ;
          exit ;
      end ;

      qryCheque.Next ;

  end ;

  qryCheque.First ;

  case MessageDlg('Ap�s este passo, as informa��es n�o poder�o mais ser alteradas. Confirma os dados dos cheques ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_CARGA_INICIAL_CHEQUE_TERCEIRO.Close;
      SP_CARGA_INICIAL_CHEQUE_TERCEIRO.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      SP_CARGA_INICIAL_CHEQUE_TERCEIRO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Processamento conclu�do com sucesso.') ;

  qryCheque.Close ;
  qryCheque.Open ;

end;

initialization
    RegisterClass(TfrmCargaInicial_ChequeTerceiro) ;

end.
