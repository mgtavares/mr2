unit fIncMovTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, ADODB, StdCtrls, DBCtrls, Mask, ImgList, ComCtrls,
  ToolWin, fProcesso_Padrao;

type
  TfrmIncMovTit = class(TfrmProcesso_Padrao)
    Image1: TImage;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdEmpresa: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancarianCdCC: TIntegerField;
    qryContaBancariacFlgFluxo: TIntegerField;
    qryContaBancariaiUltimoCheque: TIntegerField;
    qryContaBancariaiUltimoBordero: TIntegerField;
    qryContaBancariaDadosConta: TStringField;
    qryOperacao: TADOQuery;
    qryOperacaonCdOperacao: TIntegerField;
    qryOperacaocNmOperacao: TStringField;
    qryOperacaocTipoOper: TStringField;
    qryOperacaocSinalOper: TStringField;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryMTitulo: TADOQuery;
    qryMTitulonCdMTitulo: TAutoIncField;
    qryMTitulonCdTitulo: TIntegerField;
    qryMTitulonCdContaBancaria: TIntegerField;
    qryMTitulonCdOperacao: TIntegerField;
    qryMTitulonCdFormaPagto: TIntegerField;
    qryMTituloiNrCheque: TIntegerField;
    qryMTitulonValMov: TBCDField;
    qryMTitulodDtMov: TDateTimeField;
    qryMTitulodDtCad: TDateTimeField;
    qryMTitulodDtCancel: TDateTimeField;
    qryMTitulocNrDoc: TStringField;
    qryMTitulodDtContab: TDateTimeField;
    qryMTitulonCdUsuarioCad: TIntegerField;
    qryMTitulonCdUsuarioCancel: TIntegerField;
    qryMTitulocObsMov: TMemoField;
    qryMTitulonCdCC: TIntegerField;
    qryMTitulocDadoConta: TStringField;
    qryMTitulocNmOperacao: TStringField;
    qryMTitulocNmFormaPagto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBMemo1: TDBMemo;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    ToolButton3: TToolButton;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulonCdEmpresa: TIntegerField;
    qryTitulonCdEspTit: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTituloiParcela: TIntegerField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulonCdCategFinanc: TIntegerField;
    qryTitulonCdMoeda: TIntegerField;
    qryTitulocSenso: TStringField;
    qryTitulodDtEmissao: TDateTimeField;
    qryTitulodDtReceb: TDateTimeField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulodDtLiq: TDateTimeField;
    qryTitulodDtCad: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryTitulonValLiq: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonValDesconto: TBCDField;
    qryTitulocObsTit: TMemoField;
    qryTitulonCdSP: TIntegerField;
    qryTitulonCdBancoPortador: TIntegerField;
    qryTitulodDtRemPortador: TDateTimeField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulocObsBloqTit: TStringField;
    qryTitulonCdUnidadeNegocio: TIntegerField;
    qryTitulonCdCC: TIntegerField;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryMTitulocFlgMovAutomatico: TIntegerField;
    qryMTitulodDtBomPara: TDateTimeField;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    qryContaBancarianValLimiteCredito: TBCDField;
    qryContaBancariacFlgDeposito: TIntegerField;
    qryContaBancariacFlgEmiteCheque: TIntegerField;
    qryCheque: TADOQuery;
    qryChequenCdCheque: TAutoIncField;
    qryChequenCdEmpresa: TIntegerField;
    qryChequenCdBanco: TIntegerField;
    qryChequecAgencia: TStringField;
    qryChequecConta: TStringField;
    qryChequecDigito: TStringField;
    qryChequecCNPJCPF: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryChequedDtDeposito: TDateTimeField;
    qryChequenCdTerceiroResp: TIntegerField;
    qryChequenCdTerceiroPort: TIntegerField;
    qryChequedDtRemessaPort: TDateTimeField;
    qryChequenCdTabTipoCheque: TIntegerField;
    qryChequecChave: TStringField;
    qryChequenCdContaBancariaDep: TIntegerField;
    qryChequedDtDevol: TDateTimeField;
    qryChequecFlgCompensado: TIntegerField;
    qryChequenCdContaBancaria: TIntegerField;
    qryAux: TADOQuery;
    qryChequedDtEmissao: TDateTimeField;
    qryChequecNmFavorecido: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryLanctoFin: TADOQuery;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFinnCdResumoCaixa: TIntegerField;
    qryLanctoFinnCdContaBancaria: TIntegerField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdTipoLancto: TIntegerField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFinnCdUsuarioAutor: TIntegerField;
    qryLanctoFincFlgManual: TIntegerField;
    qryLanctoFindDtEstorno: TDateTimeField;
    qryLanctoFinnCdUsuarioEstorno: TDateTimeField;
    qryLanctoFinnCdLanctoFinPai: TIntegerField;
    qryLanctoFincMotivoEstorno: TStringField;
    qryLanctoFincHistorico: TStringField;
    qryLanctoFincFlgConciliado: TIntegerField;
    qryLanctoFinnCdUsuarioConciliacao: TIntegerField;
    qryLanctoFindDtConciliacao: TDateTimeField;
    qryLanctoFincDocumento: TStringField;
    qryLanctoFinnCdCheque: TIntegerField;
    qryMTitulonCdLanctoFin: TIntegerField;
    qryMTitulocFlgIntegrado: TIntegerField;
    qryTitulodDtIntegracao: TDateTimeField;
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure MensagemAlerta(cTexto: string);
    procedure MensagemErro(cTexto: string);

    procedure ShowMessage(cTexto: string);

    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;

  private
    { Private declarations }
  public
    { Public declarations }
    nCdTitulo : Integer ;
    cNmTerceiro : String ;
  end;

var
  frmIncMovTit: TfrmIncMovTit;

implementation

uses fMenu, fCadastro_Template, fLookup_Padrao;

function TfrmIncMovTit.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;


procedure TfrmIncMovTit.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
end;

procedure TfrmIncMovTit.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
end;

procedure TfrmIncMovTit.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
end;

{$R *.dfm}

procedure TfrmIncMovTit.ToolButton3Click(Sender: TObject);
begin
    Close ;
end;

procedure TfrmIncMovTit.ToolButton2Click(Sender: TObject);
begin
  frmIncMovTit.qryMTitulo.Close ;
  frmIncMovTit.qryMTitulo.Open ;
  frmIncMovTit.qryMTitulo.Insert ;
  DBEdit2.SetFocus ;
end;

procedure TfrmIncMovTit.FormShow(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus ;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Open ;

  qryOperacao.Close ;
  qryOperacao.Open  ;

  qryFormaPagto.Close ;
  qryFormaPagto.Open ;

  qryMTitulo.Close ;
  qryMTitulo.Open ;
  qryMTitulo.Insert ;
  DBEdit2.SetFocus ;

end;

procedure TfrmIncMovTit.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
i:integer ;
begin

  {if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;}

end;

procedure TfrmIncMovTit.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(30);

            If (nPK > 0) then
            begin
                qryMTitulonCdOperacao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmIncMovTit.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro = 1');

            If (nPK > 0) then
            begin
                qryMTitulonCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmIncMovTit.DBEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(39);

            If (nPK > 0) then
            begin
                qryMTitulonCdContaBancaria.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmIncMovTit.ToolButton1Click(Sender: TObject);
var
    nCdCheque : integer ;
begin

    nCdCheque := 0 ;

    qryOperacao.Locate('nCdOperacao',qryMTitulonCdOperacao.Value,[]) ;
    qryContaBancaria.Locate('nCdContaBancaria',qryMTitulonCdContaBancaria.Value, []) ;

    If (qryMTitulonCdOperacao.Value = 0) or (DBEdit9.Text = '') then
    begin
        MensagemAlerta('Informe a opera��o do movimento.') ;
        DBEDit2.SetFocus ;
        Exit ;
    end ;

    If ((qryMTitulonCdFormaPagto.Value = 0) or (DBEdit10.Text = ''))
      and (qryOperacaocTipoOper.Value = 'P') then
    begin
        MensagemAlerta('Informe a forma de liquida��o.') ;
        DBEDit3.SetFocus ;
        Exit ;
    end ;

    If ((qryMTitulonCdContaBancaria.Value = 0) or (DBEdit8.Text = ''))
      and (qryOperacaocTipoOper.Value = 'P') then
    begin
        MensagemAlerta('Informe a conta banc�ria.') ;
        DBEDit1.SetFocus ;
        Exit ;
    end ;

    if (qryContaBancariacFlgEmiteCheque.Value = 0) and (qryMTitulonCdFormaPagto.Value = 3) then
    begin
        MensagemAlerta('Esta conta banc�ria n�o movimenta cheque.') ;
        DbEdit1.SetFocus;
        Exit ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = 3) and (qryMTituloiNrCheque.Value = 0) then
    begin
        MensagemAlerta('Informe o n�mero do cheque.') ;
        DbEdit4.SetFocus;
        Exit ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = 3) and (qryMTitulodDtBomPara.AsString = '') then
    begin
        MensagemAlerta('Informe a data de dep�sito do cheque.') ;
        DbEdit11.SetFocus;
        Exit ;
    end ;

    if (qryMTitulonValMov.Value <= 0) then
    begin
        MensagemAlerta('Informe o valor do movimento ou valor inv�lido.') ;
        DbEdit5.SetFocus;
        Exit ;
    end ;

    if (qryMTitulodDtMov.AsString = '') then
    begin
        MensagemAlerta('Informe a data do movimento.') ;
        DbEdit6.SetFocus;
        Exit ;
    end ;

    qryParametro.Close ;
    qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
    qryParametro.Open ;

    If (qryMTitulodDtMov.Value <= StrToDateTime(qryParametrocValor.Value)) then
    begin
        MensagemAlerta('A data do movimento n�o pode ser inferior a data da contabiliza��o financeira.') ;
        Abort ;
    end ;

    If (qryMTitulonCdFormaPagto.Value = 3) and (qryMTitulodDtBomPara.Value <= StrToDateTime(qryParametrocValor.Value)) then
    begin
        MensagemAlerta('A data de dep�sito do cheque n�o pode ser inferior a data da contabiliza��o financeira.') ;
        Abort ;
    end ;

    if (qryMTitulodDtMov.Value > Now()) then
    begin
        MensagemAlerta('Data do Movimento inv�lida.') ;
        Abort ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = 3) then
    begin

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM Cheque WHERE nCdTabTipoCheque = 1 AND iNrCheque = ' + DbEdit4.Text + ' AND nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString) ;
        qryAux.Open ;

        if not qryAux.Eof then
        begin
            qryAux.Close ;
            MensagemAlerta('Cheque j� Emitido. Imposs�vel registrar o movimento.') ;
            exit ;
        end ;

        qryAux.Close ;
    end ;

    case MessageDlg('Confirma a inclus�o deste movimento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    qryTitulo.Close ;
    qryTitulo.Parameters.ParamByName('nCdTitulo').Value := nCdTitulo;
    qryTitulo.Open  ;

    qryTitulo.Edit ;

    frmMenu.Connection.BeginTrans;

    try
        if (qryMTitulo.State = dsBrowse) then
            qryMTitulo.Edit ;
            
        qryMTitulonCdTitulo.Value := nCdTitulo ;
        qryMTitulodDtCad.Value := Now() ;
        qryMTitulonCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado;

        if (qryOperacaocTipoOper.Value = 'P') then
        begin
            qryTitulonValLiq.Value := qryTitulonValLiq.Value + qryMTitulonValMov.Value ;
        end ;

        if (qryOperacaocTipoOper.Value = 'J') then
        begin
            qryTitulonValJuro.Value := qryTitulonValJuro.Value + qryMTitulonValMov.Value ;
        end ;

        if (qryOperacaocTipoOper.Value = 'D') then
        begin
            qryTitulonValDesconto.Value := qryTitulonValDesconto.Value + qryMTitulonValMov.Value ;
        end ;

        if (qryOperacaocSinalOper.Value = '+') then
            qryTitulonSaldoTit.Value := qryTitulonSaldoTit.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocSinalOper.Value = '-') then
            qryTitulonSaldoTit.Value := qryTitulonSaldoTit.Value - qryMTitulonValMov.Value ;

        if (qryTitulonSaldoTit.Value < 0) then
        begin
            MensagemAlerta('Valor do Movimento maior que o saldo � liquidar.') ;
            Abort;
            Exit ;
        end ;

        if (qryTitulonSaldoTit.Value = 0) then
        begin
            qryTitulodDtLiq.Value := qryMTitulodDtMov.Value ;

        end ;

        qryTitulodDtIntegracao.AsString := '' ;
        
        qryTitulo.Post ;

        qryMTitulo.Post ;

        // inseri o cheque
        if (qryMTitulonCdFormaPagto.Value = 3) then
        begin

            qryCheque.Open ;
            qryCheque.Insert ;
            qryChequenCdEmpresa.Value       := frmMenu.nCdEmpresaAtiva;
            qryChequeiNrCheque.Value        := qryMTituloiNrCheque.Value ;
            qryChequenValCheque.Value       := qryMTitulonValMov.Value ;
            qryChequedDtDeposito.Value      := qryMTitulodDtBomPara.Value ;
            qryChequedDtEmissao.Value       := qryMTitulodDtMov.Value ;
            qryChequecNmFavorecido.Value    := cNmTerceiro ;
            qryChequenCdTabTipoCheque.Value := 1 ;
            qryChequecFlgCompensado.Value   := 0 ;
            qryChequenCdContaBancaria.Value := qryMTitulonCdContaBancaria.Value ;
            qryChequecChave.Value           := 'PROPRIO:' + qryMTitulonCdContaBancaria.AsString + ':' + qryMTituloiNrCheque.AsString;
            qryCheque.Post ;

            nCdCheque := qryChequenCdCheque.Value ;

            qryCheque.Close ;

        end ;

        // gera o lan�amento da movimenta��o da conta
        if (qryOperacaocTipoOper.Value = 'P') then
        begin

            qryLanctoFin.Close ;
            qryLanctoFin.Open ;

            qryLanctoFin.Insert ;
            qryLanctoFinnCdContaBancaria.Value := qryMTitulonCdContaBancaria.Value ;
            qryLanctoFindDtLancto.Value        := qryMTitulodDtMov.Value ;
            qryLanctoFinnCdUsuario.Value       := frmMenu.nCdUsuarioLogado;
            qryLanctoFincFlgManual.Value       := 0 ;

            if (qryTitulocSenso.Value = 'C') then
            begin
                qryLanctoFincHistorico.Value       := 'BAIXA RECEBIMENTO MANUAL' ;
                qryLanctoFinnValLancto.Value       := qryMTitulonValMov.Value ;
            end ;

            qryLanctoFincDocumento.Value := qryMTitulocNrDoc.Value ;

            if (qryTitulocSenso.Value = 'D') then
            begin
                qryLanctoFincHistorico.Value       := 'BAIXA PAGAMENTO MANUAL' ;
                qryLanctoFinnValLancto.Value       := (qryMTitulonValMov.Value * -1) ;

                if (qryMTitulonCdFormaPagto.Value = 3) then
                begin
                    qryLanctoFincHistorico.Value := 'BAIXA PAGAMENTO MANUAL - CHEQUE' ;
                    qryLanctoFincDocumento.Value := qryMTituloiNrCheque.asString ;
                end

            end ;

            if (qryMTitulodDtBomPara.asString <> '') then
                qryLanctoFindDtLancto.Value := qryMTitulodDtBomPara.Value ;

            if (nCdCheque > 0) then
                qryLanctoFinnCdCheque.Value := nCdCheque ;

            qryLanctoFin.Post ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('SELECT Max(nCdLanctoFin) FROM LanctoFin WHERE nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString) ;
            qryAUx.Open ;

            if not qryAux.eof then
            begin
                qryMTitulo.Edit ;
                qryMTitulonCdLanctoFin.Value := qryAux.FieldList[0].Value ;
                qryMTitulo.Post ;
            end ;

        end ;

        qryTitulo.Close ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro na inclus�o do movimento.') ;
        Raise ;
        exit ;
    end ;

    frmMenu.Connection.CommitTrans;

    qryMTitulo.Insert ;

    DbEdit4.Enabled  := False ;
    DbEdit11.Enabled := False ;

    DbEdit2.SetFocus ;

end;

procedure TfrmIncMovTit.DBEdit3Exit(Sender: TObject);
begin

    if (Trim(DbEdit3.Text) = '3') then
    begin
        DbEdit4.Enabled  := True ;
        DbEdit11.Enabled := True ;
    end ;

end;

end.
