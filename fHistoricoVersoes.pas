unit fHistoricoVersoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, OleCtrls, SHDocVw;

type
  TfrmHistoricoVersoes = class(TfrmProcesso_Padrao)
    cmbHistoricoVersoes: TComboBox;
    ToolButton4: TToolButton;
    WebBrowser1: TWebBrowser;
    procedure cmbHistoricoVersoesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistoricoVersoes: TfrmHistoricoVersoes;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmHistoricoVersoes.cmbHistoricoVersoesChange(Sender: TObject);
begin
  inherited;

  WebBrowser1.Navigate(frmMenu.cPathSistema + 'Versao\' + cmbHistoricoVersoes.Text + '.pdf');
end;

procedure TfrmHistoricoVersoes.FormShow(Sender: TObject);
var
  i           : Integer;
  Arquivo     : TextFile;
  buscaPdf    : TSearchRec;
  arquivosPdf : TStrings;
  ArquivoNome : String;
begin
  inherited;

  {-- Varre a pasta de vers�es e seleciona os arquivos .pdf --}
  arquivosPdf := TStringList.Create;
  if FindFirst('.\Versao\*.*', faArchive, buscaPdf) = 0 then
  begin
      repeat
          {-- adiciona o arquivo enontrado na pasta, dentro do ArquivosPdf --}
          arquivosPdf.Add(buscaPdf.Name);
      until FindNext(buscaPdf) <> 0;
      FindClose(buscaPdf);
  end;

  {-- Faz a contagem e lista os arquivos --}
  for i := 0 to arquivosPdf.Count - 1 do
  begin
      {-- Tira o .pdf do nome do arquivo --}
      ArquivoNome := StringReplace(arquivosPdf[i], '.pdf', '', [rfReplaceAll, rfIgnoreCase]);
      cmbHistoricoVersoes.Items.Add(ArquivoNome);
  end;

  cmbHistoricoVersoes.ItemIndex := cmbHistoricoVersoes.Items.Count - 1;

  WebBrowser1.Navigate(frmMenu.cPathSistema + 'Versao\' + cmbHistoricoVersoes.Text + '.pdf');
end;

end.
