inherited frmModImpBOL: TfrmModImpBOL
  Caption = 'Modelo Impress'#227'o Boleto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 16
    Top = 94
    Width = 73
    Height = 13
    Caption = 'C'#243'digo Banco'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 16
    Top = 120
    Width = 31
    Height = 13
    Caption = 'Query'
    FocusControl = DBMemo1
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 96
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdModImpBOL'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 96
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmModImpBOL'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [8]
    Left = 96
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBMemo1: TDBMemo [9]
    Left = 16
    Top = 136
    Width = 457
    Height = 393
    DataField = 'cQuery'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBGridEh1: TDBGridEh [10]
    Left = 480
    Top = 136
    Width = 753
    Height = 393
    DataSource = dsCampoC
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCampoModImpBOL'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdModImpBOL'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iSequencia'
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'cNmCampo'
        Footers = <>
        Width = 124
      end
      item
        EditButtons = <>
        FieldName = 'cDescricao'
        Footers = <>
        Width = 261
      end
      item
        EditButtons = <>
        FieldName = 'iLinha'
        Footers = <>
        Width = 30
      end
      item
        EditButtons = <>
        FieldName = 'iColuna'
        Footers = <>
        Width = 29
      end
      item
        EditButtons = <>
        FieldName = 'cTipoDado'
        Footers = <>
        KeyList.Strings = (
          'C'
          'I'
          'V')
        PickList.Strings = (
          'Caracter'
          'Inteiro'
          'Valor')
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'cTamanhoFonte'
        Footers = <>
        PickList.Strings = (
          'NORMAL'
          'EXPANDIDO'
          'COMP12'
          'COMP17'
          'COMP20')
      end
      item
        EditButtons = <>
        FieldName = 'cEstiloFonte'
        Footers = <>
        PickList.Strings = (
          'NORMAL'
          'ITALICO'
          'NEGRITO')
      end
      item
        EditButtons = <>
        FieldName = 'cLPP'
        Footers = <>
        PickList.Strings = (
          'SEIS'
          'OITO')
      end>
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ModImpBOL'
      'WHERE nCdModImpBol = :nPK')
    Left = 568
    Top = 352
    object qryMasternCdModImpBOL: TIntegerField
      FieldName = 'nCdModImpBOL'
    end
    object qryMastercNmModImpBOL: TStringField
      FieldName = 'cNmModImpBOL'
      Size = 50
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercQuery: TMemoField
      FieldName = 'cQuery'
      BlobType = ftMemo
    end
  end
  inherited dsMaster: TDataSource
    Left = 504
    Top = 352
  end
  inherited qryID: TADOQuery
    Left = 536
    Top = 352
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 592
    Top = 208
  end
  inherited qryStat: TADOQuery
    Left = 496
    Top = 224
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 560
    Top = 264
  end
  inherited ImageList1: TImageList
    Left = 696
    Top = 248
  end
  object qryCampoC: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCampoCBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CampoModImpBOL'
      ' WHERE nCdModImpBOL = :nPK')
    Left = 612
    Top = 384
    object qryCampoCnCdCampoModImpBOL: TAutoIncField
      FieldName = 'nCdCampoModImpBOL'
      ReadOnly = True
    end
    object qryCampoCnCdModImpBOL: TIntegerField
      FieldName = 'nCdModImpBOL'
    end
    object qryCampoCiSequencia: TIntegerField
      DisplayLabel = 'Campo|Ordem'
      FieldName = 'iSequencia'
    end
    object qryCampoCcNmCampo: TStringField
      DisplayLabel = 'Campo|Nome Campo'
      FieldName = 'cNmCampo'
      Size = 30
    end
    object qryCampoCcDescricao: TStringField
      DisplayLabel = 'Campo|Descri'#231#227'o'
      FieldName = 'cDescricao'
      Size = 50
    end
    object qryCampoCiLinha: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Lin.'
      FieldName = 'iLinha'
    end
    object qryCampoCiColuna: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Col.'
      FieldName = 'iColuna'
    end
    object qryCampoCcTipoDado: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryCampoCcTamanhoFonte: TStringField
      DisplayLabel = 'Fonte|Tamanho'
      FieldName = 'cTamanhoFonte'
      Size = 10
    end
    object qryCampoCcEstiloFonte: TStringField
      DisplayLabel = 'Fonte|Estilo'
      FieldName = 'cEstiloFonte'
      Size = 10
    end
    object qryCampoCcLPP: TStringField
      DisplayLabel = 'Fonte|lpp'
      FieldName = 'cLPP'
      Size = 10
    end
  end
  object dsCampoC: TDataSource
    DataSet = qryCampoC
    Left = 652
    Top = 384
  end
end
