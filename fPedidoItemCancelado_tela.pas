unit fPedidoItemCancelado_tela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, ADODB;

type
  TfrmPedidoItemCancelado_tela = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    data1: TDataSource;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeCanc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotalCanc: TcxGridDBColumn;
    cxGrid1DBTableView1dDtCancelSaldoItemPed: TcxGridDBColumn;
    cxGrid1DBTableView1nCdUsuarioCancelSaldoItemPed: TcxGridDBColumn;
    cxGrid1DBTableView1nCdMotivoCancSaldoPed: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMotivoCancSaldoPed: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    SPREL_PEDIDO_ITEM_CANCELADO: TADOStoredProc;
    SPREL_PEDIDO_ITEM_CANCELADOnCdPedido: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOdDtPedido: TDateTimeField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdTerceiro: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmTerceiro: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdTipoPedido: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmTipoPedido: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdProduto: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmProduto: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnQtdeCanc: TBCDField;
    SPREL_PEDIDO_ITEM_CANCELADOnValTotalCanc: TBCDField;
    SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed: TDateTimeField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdUsuarioCancelSaldoItemPed: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmUsuario: TStringField;
    SPREL_PEDIDO_ITEM_CANCELADOnCdMotivoCancSaldoPed: TIntegerField;
    SPREL_PEDIDO_ITEM_CANCELADOcNmMotivoCancSaldoPed: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedidoItemCancelado_tela: TfrmPedidoItemCancelado_tela;

implementation

{$R *.dfm}

end.
