unit fProgAltPreco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, GridsEh, DBGridEh, DB, ADODB, Menus, DBGridEhGrouping;

type
  TfrmProgAltPreco = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryPendente: TADOQuery;
    qryPendentenCdProgAltPreco: TIntegerField;
    qryPendentenCdProduto: TIntegerField;
    qryPendentecNmProduto: TStringField;
    qryPendentenValVendaAnterior: TBCDField;
    qryPendentenValVendaNovo: TBCDField;
    qryPendentedDtAlteracaoPreco: TDateTimeField;
    qryPendentedDtProcesso: TDateTimeField;
    qryPendentecNmUsuario: TStringField;
    qryPendentedDtCad: TDateTimeField;
    dsPendente: TDataSource;
    qryPendentenCdTabTipoProduto: TIntegerField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopupMenu1: TPopupMenu;
    ExcluirProgramao1: TMenuItem;
    qryAux: TADOQuery;
    qryPendentenValCustoNovo: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ExcluirProgramao1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProgAltPreco: TfrmProgAltPreco;

implementation

uses fMenu, fProgaltPreco_Incluir;

{$R *.dfm}

procedure TfrmProgAltPreco.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0;

  DBGridEh1.Align      := alClient;

  qryPendente.Close;
  qryPendente.Open;

end;

procedure TfrmProgAltPreco.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Programa��o de Altera��o de Pre�os') ;
  
end;

procedure TfrmProgAltPreco.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmProgaltPreco_Incluir;
begin
  inherited;

  objForm := TfrmProgaltPreco_Incluir.Create(nil);
  showForm(objForm,true);

  qryPendente.Close;
  qryPendente.Open;
  
end;

procedure TfrmProgAltPreco.ExcluirProgramao1Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a exclus�o desta programa��o de altera��o de pre�o ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
  begin

      frmMenu.Connection.BeginTrans;

      try
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ProgAltPreco WHERE nCdProgAltPreco = ' + qryPendentenCdProgAltPreco.AsString) ;
          qryAux.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryPendente.Requery();

  end ;

end;

initialization
    RegisterClass(TfrmProgAltPreco) ;

end.
