unit rInventario;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, jpeg;

type
  TrptInventario_view = class(TQuickRep)
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    lblEmpresa: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel3: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    lblEmpresa2: TQRLabel;
    lblInventario: TQRLabel;
    QRBand2: TQRBand;
    QRGroup1: TQRGroup;
    SPREL_INVENTARIO: TADOStoredProc;
    SPREL_INVENTARIOnCdLocalEstoque: TIntegerField;
    SPREL_INVENTARIOcNmLocalEstoque: TStringField;
    SPREL_INVENTARIOiItem: TIntegerField;
    SPREL_INVENTARIOnCdProduto: TIntegerField;
    SPREL_INVENTARIOcNmProduto: TStringField;
    SPREL_INVENTARIOcLocalizacao: TStringField;
    QRLabel6: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape2: TQRShape;
    DetailBand1: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape3: TQRShape;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    SPREL_INVENTARIOdDtAbertura: TDateTimeField;
    SPREL_INVENTARIOcNmUsuario: TStringField;
    QRLabel13: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel14: TQRLabel;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRTotalPagina: TQRLabel;
  private

  public

  end;

var
  rptInventario_view: TrptInventario_view;

implementation

{$R *.DFM}

end.
