inherited frmMonitorOcorrencias: TfrmMonitorOcorrencias
  Left = 84
  Top = 57
  Width = 1270
  Height = 630
  Caption = 'Monitor de Ocorr'#234'ncias'
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 193
    Width = 1254
    Height = 399
  end
  inherited ToolBar1: TToolBar
    Width = 1254
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl2: TcxPageControl [2]
    Left = 0
    Top = 193
    Width = 1254
    Height = 399
    ActivePage = tabOcorrencia
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 395
    ClientRectLeft = 4
    ClientRectRight = 1250
    ClientRectTop = 24
    object tabOcorrencia: TcxTabSheet
      Caption = 'Ocorr'#234'ncias'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1246
        Height = 371
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsOcorrencia
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        RowSizingAllowed = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdOcorrencia'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|C'#243'd. Ocorr'#234'ncia'
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|Loja|C'#243'd.'
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|Loja|Descri'#231#227'o'
            Width = 115
          end
          item
            EditButtons = <>
            FieldName = 'nCdTerceiro'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|Cliente|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|Cliente|Descri'#231#227'o'
            Width = 230
          end
          item
            EditButtons = <>
            FieldName = 'dDtPrazoAtendIni'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncias|Prazo Atendimento|Data Inicial'
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'dDtPrazoAtendFinal'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncias|Prazo Atendimento|Data Final'
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuarioCad'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncias|Usu'#225'rio Cadastro'
            Width = 184
          end
          item
            EditButtons = <>
            FieldName = 'dDtOcorrencia'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncias|Data Ocorr'#234'ncia'
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoOcorrencia'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Ocorr'#234'ncias|Tipo Ocorr'#234'ncia'
            Width = 106
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusOcorrencia'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncias|Status'
            Width = 134
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1254
    Height = 164
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 2
    object Label2: TLabel
      Tag = 2
      Left = 93
      Top = 23
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Tag = 2
      Left = 24
      Top = 46
      Width = 91
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status Ocorr'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Tag = 2
      Left = 34
      Top = 70
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo Ocorr'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object lblHelp: TLabel
      Tag = 2
      Left = 16
      Top = 93
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = 'Periodo Ocorr'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Tag = 2
      Left = 202
      Top = 93
      Width = 16
      Height = 13
      Caption = 'at'#233
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object ER2LknCdLoja: TER2LookupMaskEdit
      Left = 122
      Top = 18
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 147
      QueryLookup = qryLoja
    end
    object MaskEdit1: TMaskEdit
      Left = 122
      Top = 90
      Width = 76
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 6
      Text = '  /  /    '
    end
    object ER2LkTipoOcorrencia: TER2LookupMaskEdit
      Left = 122
      Top = 66
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 4
      Text = '         '
      CodigoLookup = 777
      WhereAdicional.Strings = (
        'EXISTS(SELECT 1'
        '         FROM UsuarioAtendTipoOcorrencia'
        '        WHERE nCdUsuario       = @@Usuario'
        
          '          AND UsuarioAtendTipoOcorrencia.nCdTipoOcorrencia = Tip' +
          'oOcorrencia.nCdTipoOcorrencia)')
      QueryLookup = qryTipoOcorrencia
    end
    object ER2LktStatusOcorrencia: TER2LookupMaskEdit
      Left = 122
      Top = 42
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      CodigoLookup = 779
      QueryLookup = qryTabStatusOcorrencia
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 189
      Top = 42
      Width = 445
      Height = 21
      DataField = 'cNmTabStatusOcorrencia'
      DataSource = dsTabStatusOcorrencia
      TabOrder = 3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 189
      Top = 18
      Width = 445
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 189
      Top = 66
      Width = 445
      Height = 21
      DataField = 'cNmTipoOcorrencia'
      DataSource = dsTipoOcorrencia
      TabOrder = 5
    end
    object MaskEdit2: TMaskEdit
      Left = 228
      Top = 90
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 7
      Text = '  /  /    '
    end
    object cxButton1: TcxButton
      Left = 313
      Top = 89
      Width = 121
      Height = 22
      Caption = 'Exibir Ocorr'#234'ncias'
      TabOrder = 8
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 80
  end
  object qryOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtIni'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFim'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTabStatusOcorrencia'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoOcorrencia'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtIni                 varchar(10)'
      '       ,@dDtFim                 varchar(10)'
      '       ,@nCdTabStatusOcorrencia int'
      '       ,@nCdLoja                int'
      '       ,@nCdTipoOcorrencia      int'
      '       ,@nCdUsuario             int'
      ''
      'SET @dDtIni                 = :dDtIni'
      'SET @dDtFim                 = :dDtFim'
      'SET @nCdLoja                = :nCdLoja'
      'SET @nCdTabStatusOcorrencia = :nCdTabStatusOcorrencia'
      'SET @nCdTipoOcorrencia      = :nCdTipoOcorrencia'
      'SET @nCdUsuario             = :nCdUsuario'
      ''
      'IF @dDtIni = '#39'  /  /    '#39
      '    SET @dDtIni = '#39'01/01/1900'#39
      ''
      'IF @dDtFim = '#39'  /  /    '#39
      '    SET @dDtFim = '#39'01/01/1900'#39
      ''
      'SELECT nCdOcorrencia'
      '      ,cResumoOcorrencia'
      '      ,Ocorrencia.nCdLoja'
      '      ,cNmLoja'
      '      ,Ocorrencia.nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,cNmTipoOcorrencia'
      
        '      ,CONVERT(VARCHAR(10),Ocorrencia.dDtOcorrencia,103)      dD' +
        'tOcorrencia'
      
        '      ,CONVERT(VARCHAR(10),Ocorrencia.dDtUsuarioAtend,103)    dD' +
        'tUsuarioAtend'
      
        '      ,CONVERT(VARCHAR(10),Ocorrencia.dDtPrazoAtendIni,103)   dD' +
        'tPrazoAtendIni'
      
        '      ,CONVERT(VARCHAR(10),Ocorrencia.dDtPrazoAtendFinal,103) dD' +
        'tPrazoAtendFinal'
      '      ,cDescOcorrencia'
      '      ,UPPER(cNmTabStatusOcorrencia) cNmTabStatusOcorrencia'
      '      ,(SELECT cNmUsuario'
      '          FROM Usuario'
      
        '         WHERE nCdUsuario = Ocorrencia.nCdUsuarioCad) AS cNmUsua' +
        'rioCad'
      '      ,(SELECT cNmUsuario'
      '          FROM Usuario'
      
        '         WHERE nCdUsuario = Ocorrencia.nCdUsuarioResp) AS cNmUsu' +
        'arioResp'
      '  FROM Ocorrencia'
      
        '       INNER JOIN Loja                    ON Loja.nCdLoja       ' +
        '              = Ocorrencia.nCdLoja'
      
        '       INNER JOIN Terceiro                ON Terceiro.nCdTerceir' +
        'o             = Ocorrencia.nCdTerceiro'
      
        '       INNER JOIN TipoOcorrencia          ON TipoOcorrencia.nCdT' +
        'ipoOcorrencia = Ocorrencia.nCdTipoOcorrencia'
      
        '       INNER JOIN TabStatusOcorrencia TSO ON TSO.nCdTabStatusOco' +
        'rrencia       = Ocorrencia.nCdTabStatusOcorrencia'
      
        ' WHERE ((dbo.fn_OnlyDate(Ocorrencia.dDtOcorrencia) >= Convert(DA' +
        'TETIME,@dDtIni,103))   OR (@dDtIni = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(Ocorrencia.dDtOcorrencia) <  Convert(DA' +
        'TETIME,@dDtFim,103)+1) OR (@dDtFim = '#39'01/01/1900'#39'))'
      
        '   AND ((Ocorrencia.nCdTabStatusOcorrencia = @nCdTabStatusOcorre' +
        'ncia) OR (@nCdTabStatusOcorrencia = 0))'
      '   AND ((Ocorrencia.nCdLoja = @nCdLoja) OR (@nCdLoja = 0))'
      
        '   AND ((Ocorrencia.nCdTipoOcorrencia = @nCdTipoOcorrencia) OR (' +
        '@nCdTipoOcorrencia = 0))'
      '   AND  EXISTS(SELECT 1'
      '                 FROM UsuarioAtendTipoOcorrencia'
      
        '                WHERE nCdUsuario                                ' +
        '   = @nCdUsuario'
      
        '                  AND UsuarioAtendTipoOcorrencia.nCdTipoOcorrenc' +
        'ia = Ocorrencia.nCdTipoOcorrencia)'
      ''
      ''
      ''
      '')
    Left = 504
    Top = 77
    object qryOcorrencianCdOcorrencia: TIntegerField
      FieldName = 'nCdOcorrencia'
    end
    object qryOcorrencianCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryOcorrenciacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryOcorrenciacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryOcorrenciacNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
    object qryOcorrenciacDescOcorrencia: TMemoField
      FieldName = 'cDescOcorrencia'
      BlobType = ftMemo
    end
    object qryOcorrenciacNmTabStatusOcorrencia: TStringField
      FieldName = 'cNmTabStatusOcorrencia'
      ReadOnly = True
      Size = 30
    end
    object qryOcorrenciacResumoOcorrencia: TStringField
      FieldName = 'cResumoOcorrencia'
      Size = 150
    end
    object qryOcorrenciacNmUsuarioCad: TStringField
      FieldName = 'cNmUsuarioCad'
      ReadOnly = True
      Size = 50
    end
    object qryOcorrenciacNmUsuarioResp: TStringField
      FieldName = 'cNmUsuarioResp'
      ReadOnly = True
      Size = 50
    end
    object qryOcorrenciadDtOcorrencia: TStringField
      FieldName = 'dDtOcorrencia'
      ReadOnly = True
      Size = 10
    end
    object qryOcorrenciadDtUsuarioAtend: TStringField
      FieldName = 'dDtUsuarioAtend'
      ReadOnly = True
      Size = 10
    end
    object qryOcorrenciadDtPrazoAtendIni: TStringField
      FieldName = 'dDtPrazoAtendIni'
      ReadOnly = True
      Size = 10
    end
    object qryOcorrenciadDtPrazoAtendFinal: TStringField
      FieldName = 'dDtPrazoAtendFinal'
      ReadOnly = True
      Size = 10
    end
    object qryOcorrencianCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
  end
  object dsOcorrencia: TDataSource
    DataSet = qryOcorrencia
    Left = 504
    Top = 108
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK      ')
    Left = 536
    Top = 76
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 536
    Top = 108
  end
  object qryTabStatusOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTabStatusOcorrencia       '
      '  FROM TabStatusOcorrencia'
      ' WHERE nCdTabStatusOcorrencia = :nPK')
    Left = 568
    Top = 76
    object qryTabStatusOcorrenciacNmTabStatusOcorrencia: TStringField
      FieldName = 'cNmTabStatusOcorrencia'
      Size = 30
    end
  end
  object dsTabStatusOcorrencia: TDataSource
    DataSet = qryTabStatusOcorrencia
    Left = 568
    Top = 108
  end
  object qryTipoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryTipoOcorrenciaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTipoOcorrencia'
      '  FROM TipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = :nPK'
      '   AND EXISTS(SELECT 1'
      '         FROM UsuarioAtendTipoOcorrencia'
      '        WHERE nCdUsuario                       = :nCdUsuario'
      
        '          AND TipoOcorrencia.nCdTipoOcorrencia = UsuarioAtendTip' +
        'oOcorrencia.nCdTipoOcorrencia)')
    Left = 600
    Top = 76
    object qryTipoOcorrenciacNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
  end
  object dsTipoOcorrencia: TDataSource
    DataSet = qryTipoOcorrencia
    Left = 600
    Top = 108
  end
end
