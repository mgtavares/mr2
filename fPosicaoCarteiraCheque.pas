unit fPosicaoCarteiraCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBCtrls, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, GridsEh, DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmPosicaoCarteiraCheque = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    DataSource1: TDataSource;
    qryResultado: TADOQuery;
    qryResultadonCdContaBancariaDep: TIntegerField;
    qryResultadocNmConta: TStringField;
    qryResultadonCdLoja: TIntegerField;
    qryResultadocNmLoja: TStringField;
    qryResultadonTotalCheque: TBCDField;
    dsResultado: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    DBGridEh2: TDBGridEh;
    qryPosicaoStatus: TADOQuery;
    dsPosicaoStatus: TDataSource;
    qryPosicaoStatusnCdTabStatusCheque: TIntegerField;
    qryPosicaoStatuscNmTabStatusCheque: TStringField;
    qryPosicaoStatusnTotalCheque: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPosicaoVencto: TADOQuery;
    dsPosicaoVencto: TDataSource;
    qryPosicaoVenctodDtDeposito: TDateTimeField;
    qryPosicaoVenctonTotalCheque: TBCDField;
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryResultadoAfterScroll(DataSet: TDataSet);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure qryPosicaoStatusAfterScroll(DataSet: TDataSet);
    procedure DBGridEh3DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPosicaoCarteiraCheque: TfrmPosicaoCarteiraCheque;

implementation

uses fLookup_Padrao, fMenu, fPosicaoCarteiraCheque_Cheques;

{$R *.dfm}

procedure TfrmPosicaoCarteiraCheque.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa');

            If (nPK > 0) then
            begin
                MaskEdit3.Text := IntToStr(nPK) ;

                qryContaBancariaDeb.Close ;
                qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
                qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := 0 ;
                PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;
            end ;

    end ;

  end ;

end;

procedure TfrmPosicaoCarteiraCheque.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

   qryContaBancariaDeb.Close ;
   qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
   qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := 0 ;
   PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;

end;

procedure TfrmPosicaoCarteiraCheque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('cDataInicial').Value     := frmMenu.ConvData(MaskEdit1.Text);
  qryResultado.Parameters.ParamByName('cDataFinal').Value       := frmMenu.ConvData(MaskEdit2.Text);
  qryResultado.Parameters.ParamByName('nCdContaBancaria').Value := frmMenu.ConvInteiro(MaskEdit3.Text);
  qryResultado.Open ;

  if (qryResultado.eof) then
      MensagemAlerta('Nenhum cheque encontrado para o crit�rio selecionado.') ;

end;

procedure TfrmPosicaoCarteiraCheque.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      DBGridEh1.Columns[2].Visible := False ;
      DBGridEh1.Columns[3].Visible := False ;
  end ;

end;

procedure TfrmPosicaoCarteiraCheque.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if not qryResultado.eof then
  begin
      qryPosicaoStatus.Close ;
      qryPosicaoStatus.Parameters.ParamByName('nCdContaBancaria').Value := qryResultadonCdContaBancariaDep.Value;
      qryPosicaoStatus.Parameters.ParamByName('cDataInicial').Value     := frmMenu.ConvData(MaskEdit1.Text);
      qryPosicaoStatus.Parameters.ParamByName('cDataFinal').Value       := frmMenu.ConvData(MaskEdit2.Text);
      qryPosicaoStatus.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
      qryPosicaoStatus.Open ;
  end ;

end;

procedure TfrmPosicaoCarteiraCheque.qryResultadoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryPosicaoStatus.Close ;
end;

procedure TfrmPosicaoCarteiraCheque.DBGridEh2DblClick(Sender: TObject);
begin
  inherited;

  if not qryPosicaoStatus.eof then
  begin
      qryPosicaoVencto.Close ;
      qryPosicaoVencto.Parameters.ParamByName('nCdContaBancaria').Value   := qryResultadonCdContaBancariaDep.Value;
      qryPosicaoVencto.Parameters.ParamByName('cDataInicial').Value       := frmMenu.ConvData(MaskEdit1.Text);
      qryPosicaoVencto.Parameters.ParamByName('cDataFinal').Value         := frmMenu.ConvData(MaskEdit2.Text);
      qryPosicaoVencto.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva ;
      qryPosicaoVencto.Parameters.ParamByName('nCdTabStatusCheque').Value := qryPosicaoStatusnCdTabStatusCheque.Value ;
      qryPosicaoVencto.Open ;
  end ;

end;

procedure TfrmPosicaoCarteiraCheque.qryPosicaoStatusAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryPosicaoVencto.Close ;
end;

procedure TfrmPosicaoCarteiraCheque.DBGridEh3DblClick(Sender: TObject);
var
  objForm : TfrmPosicaoCarteiraCheque_Cheques ;
begin
  inherited;

  if not qryPosicaoVencto.Eof then
  begin

      objForm := TfrmPosicaoCarteiraCheque_Cheques.Create(nil) ;

      objForm.qryCheques.Close;
      objForm.qryCheques.Parameters.ParamByName('nCdContaBancaria').Value   := qryResultadonCdContaBancariaDep.Value ;
      objForm.qryCheques.Parameters.ParamByName('cDataInicial').Value       := qryPosicaoVenctodDtDeposito.AsString  ;
      objForm.qryCheques.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva ;
      objForm.qryCheques.Parameters.ParamByName('nCdTabStatusCheque').Value := qryPosicaoStatusnCdTabStatusCheque.Value ;
      objForm.qryCheques.Open ;

      showForm ( objForm , TRUE ) ;

  end ;

end;

initialization
    RegisterClass(TfrmPosicaoCarteiraCheque) ;

end.
