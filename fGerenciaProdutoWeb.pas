unit fGerenciaProdutoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, Menus, cxLookAndFeelPainters,
  StdCtrls, cxButtons, Mask, ER2Lookup, DBCtrls;

type
  TER2TipoProdWeb = (tpProduto, tpSku);
  TER2TipoAcao = (tpAtivarWeb, tpInativarWeb, tpAtualizarPreco, tpAtualizarDimensao);

  TfrmGerenciaProdutoWeb = class(TfrmProcesso_Padrao)
    ADOConnection1: TADOConnection;
    cxGridTempProdWeb: TcxGrid;
    cxGridTempProdWebDBTableView1: TcxGridDBTableView;
    cxGridTempProdWebLevel1: TcxGridLevel;
    cxGridTempProdWebDBTableView1DBColumn_cFlgProdWeb: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_nValVendaWebDe: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_nValVendaWebPor: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cNmProdutoWeb: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    meuOpcao: TPopupMenu;
    btProdutoWeb: TMenuItem;
    panel: TPanel;
    N1: TMenuItem;
    MonitordeTransmissoWeb1: TMenuItem;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryProdutoWebSel: TADOQuery;
    qryAux: TADOQuery;
    qryProdutoWebSelnCdRegistro: TIntegerField;
    gbFiltros: TGroupBox;
    Label3: TLabel;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    Label1: TLabel;
    ER2LookupMaskEdit3: TER2LookupMaskEdit;
    Label2: TLabel;
    ER2LookupMaskEdit4: TER2LookupMaskEdit;
    Label4: TLabel;
    ER2LookupMaskEdit5: TER2LookupMaskEdit;
    Label5: TLabel;
    qryDepartamento: TADOQuery;
    qrySubcategoria: TADOQuery;
    qrySegmento: TADOQuery;
    qryCategoria: TADOQuery;
    qryMarca: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryCategorianCdCategoria: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qrySubcategorianCdSubCategoria: TIntegerField;
    qrySubcategoriacNmSubCategoria: TStringField;
    qrySegmentonCdSegmento: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    DBEdit1: TDBEdit;
    dsDepartamento: TDataSource;
    DBEdit2: TDBEdit;
    dsCategoria: TDataSource;
    DBEdit3: TDBEdit;
    dsSubcategoria: TDataSource;
    DBEdit4: TDBEdit;
    dsSegmento: TDataSource;
    DBEdit5: TDBEdit;
    dsMarca: TDataSource;
    rgStatusSku: TRadioGroup;
    cxGridTempProdWebDBTableView1DBColumn_cNmDepartamento: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cNmCategoria: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cNmSubCategoria: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cNmSegmento: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cNmMarca: TcxGridDBColumn;
    cxGridTempSkuWeb: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryTempProdWeb: TADOQuery;
    qryTempProdWebcNmDepartamento: TStringField;
    qryTempProdWebcNmCategoria: TStringField;
    qryTempProdWebcNmSubCategoria: TStringField;
    qryTempProdWebcNmSegmento: TStringField;
    qryTempProdWebcNmMarca: TStringField;
    qryTempProdWebnCdProduto: TIntegerField;
    qryTempProdWebcNmProdutoWeb: TStringField;
    qryTempProdWebcFlgProdWeb: TIntegerField;
    qryTempProdWebnValVendaWebDe: TBCDField;
    qryTempProdWebnValVendaWebPor: TBCDField;
    qryTempProdWebnCdDepartamento: TIntegerField;
    qryTempProdWebnCdCategoria: TIntegerField;
    qryTempProdWebnCdSubCategoria: TIntegerField;
    qryTempProdWebnCdSegmento: TIntegerField;
    qryTempProdWebnCdMarca: TIntegerField;
    dsTempProdWeb: TDataSource;
    qryTempSkuWeb: TADOQuery;
    qryTempSkuWebnCdProduto: TIntegerField;
    qryTempSkuWebnCdSku: TIntegerField;
    qryTempSkuWebcNmSkuWeb: TStringField;
    qryTempSkuWebcFlgSkuWeb: TIntegerField;
    qryTempSkuWebnValSkuVendaWebDe: TBCDField;
    qryTempSkuWebnValSkuVendaWebPor: TBCDField;
    qryTempSkuWebnDimensao: TBCDField;
    qryTempSkuWebnAltura: TBCDField;
    qryTempSkuWebnLargura: TBCDField;
    qryTempSkuWebnPesoLiquido: TBCDField;
    dsTempSkuWeb: TDataSource;
    qryTempSkuWebcFlgAtivo: TIntegerField;
    qryTempProdWebcFlgAtivo: TIntegerField;
    cxGridTempProdWebDBTableView1DBColumn_nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1nCdSku: TcxGridDBColumn;
    cxGridDBTableView1cNmSkuWeb: TcxGridDBColumn;
    cxGridDBTableView1cFlgSkuWeb: TcxGridDBColumn;
    cxGridDBTableView1nValSkuVendaWebDe: TcxGridDBColumn;
    cxGridDBTableView1nValSkuVendaWebPor: TcxGridDBColumn;
    cxGridDBTableView1nDimensao: TcxGridDBColumn;
    cxGridDBTableView1nAltura: TcxGridDBColumn;
    cxGridDBTableView1nLargura: TcxGridDBColumn;
    cxGridDBTableView1nPesoLiquido: TcxGridDBColumn;
    cxGridDBTableView1cFlgAtivo: TcxGridDBColumn;
    cxGridTempProdWebDBTableView1DBColumn_cFlgAtivo: TcxGridDBColumn;
    Splitter1: TSplitter;
    pnProduto: TPanel;
    btMarcar: TcxButton;
    btDesmarcar: TcxButton;
    btAtivarProduto: TcxButton;
    btInativarProduto: TcxButton;
    btAtivarSku: TcxButton;
    btInativarSku: TcxButton;
    btAplicaPreco: TcxButton;
    btAplicaDimensao: TcxButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure btProdutoWebClick(Sender: TObject);
    procedure meuOpcaoPopup(Sender: TObject);
    procedure btMarcarClick(Sender: TObject);
    procedure btDesmarcarClick(Sender: TObject);
    function TipoProdToInt(const tpProdWeb : TER2TipoProdWeb) : Integer;
    procedure ER2LookupMaskEdit2BeforeLookup(Sender: TObject);
    procedure qryDepartamentoAfterOpen(DataSet: TDataSet);
    procedure ER2LookupMaskEdit3BeforeLookup(Sender: TObject);
    procedure qryCategoriaAfterOpen(DataSet: TDataSet);
    procedure ER2LookupMaskEdit4BeforeLookup(Sender: TObject);
    procedure qrySubcategoriaAfterOpen(DataSet: TDataSet);
    procedure ER2LookupMaskEdit5BeforeLookup(Sender: TObject);
    procedure cxGridTempProdWebDBTableView1CellClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridTempProdWebEnter(Sender: TObject);
    procedure cxGridTempProdWebExit(Sender: TObject);
    procedure cxGridTempSkuWebEnter(Sender: TObject);
    procedure cxGridTempSkuWebExit(Sender: TObject);
    procedure btAplicaPrecoClick(Sender: TObject);
  private
    { Private declarations }
    tpProdWebSel : TER2TipoProdWeb;
    procedure prAtualizaSelecao(tpProdWeb : TER2TipoProdWeb; bStatus : Boolean);
    procedure prAtualizaPreco(tpProdWeb : TER2TipoProdWeb);
    procedure prAtualizaDimensao(tpProdWeb : TER2TipoProdWeb);
    procedure prAtualizaStatusWeb(tpProdWeb : TER2TipoProdWeb; bStatusWeb : Boolean);
  public
    { Public declarations }
  end;

var
  frmGerenciaProdutoWeb: TfrmGerenciaProdutoWeb;

implementation

{$R *.dfm}

uses
  fMenu, fProdutoWeb, fGerenciaProdutoWeb_AplicaDimensaoProduto, fGerenciaProdutoWeb_AplicaPrecoProduto,
  Math;

function TfrmGerenciaProdutoWeb.TipoProdToInt(const tpProdWeb : TER2TipoProdWeb) : Integer;
begin
  case (tpProdWeb) of
      tpProduto : Result := 2;
      tpSku     : Result := 3;
  end;
end;

procedure TfrmGerenciaProdutoWeb.meuOpcaoPopup(Sender: TObject);
begin
  inherited;

  btProdutoWeb.Enabled := frmMenu.fnValidaUsuarioAPL('FRMPRODUTOWEB');
end;

procedure TfrmGerenciaProdutoWeb.btProdutoWebClick(Sender: TObject);
var
  objProdutoWeb : TfrmProdutoWeb;
begin
  inherited;

  objProdutoWeb := TfrmProdutoWeb.Create( Self ) ;

  if (not qryTempProdWeb.IsEmpty) then
      PosicionaQuery(objProdutoWeb.qryMaster, qryTempProdWebnCdProduto.AsString);

  showForm(objProdutoWeb , True);
end;

procedure TfrmGerenciaProdutoWeb.ToolButton1Click(Sender: TObject);
begin
  inherited;

  cmdPreparaTemp.Execute;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdDepartamento').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdCategoria').Value    := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdSubcategoria').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit3.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdSegmento').Value     := frmMenu.ConvInteiro(ER2LookupMaskEdit4.Text);
  qryPopulaTemp.Parameters.ParamByName('nCdMarca').Value        := frmMenu.ConvInteiro(ER2LookupMaskEdit5.Text);
  qryPopulaTemp.Parameters.ParamByName('cFlgStatusSku').Value   := rgStatusSku.ItemIndex;
  qryPopulaTemp.ExecSQL;

  qryTempProdWeb.Close;
  qryTempProdWeb.Open;
  qryTempSkuWeb.Close;
end;

procedure TfrmGerenciaProdutoWeb.prAtualizaSelecao(tpProdWeb : TER2TipoProdWeb; bStatus : Boolean);
var
  nCdStatus : Integer;
begin
  case (tpProdWeb) of
      tpProduto : begin
          if (qryTempProdWeb.IsEmpty) then
              Exit;

          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE #TempProdWeb SET cFlgAtivo = ' + IntToStr(nCdStatus));
          qryAux.ExecSQL;

          qryTempProdWeb.Close;
          qryTempProdWeb.Open;
          qryTempProdWeb.Requery();
          qryTempSkuWeb.Close;
      end;
      tpSku : begin
          if (qryTempSkuWeb.IsEmpty) then
              Exit;

          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE #TempSkuWeb SET cFlgAtivo = ' + IntToStr(nCdStatus) + ' WHERE nCdProduto = ' + qryTempProdWebnCdProduto.AsString);
          qryAux.ExecSQL;

          qryTempSkuWeb.Close;
          qryTempSkuWeb.Open;
      end;
  end;
end;

procedure TfrmGerenciaProdutoWeb.prAtualizaPreco(tpProdWeb : TER2TipoProdWeb);
var
  objAplicaPrecoProduto : TfrmGerenciaProdutoWeb_AplicaPrecoProduto;
begin
  case (tpProdWeb) of
      tpProduto : begin
          if (qryTempProdWeb.IsEmpty) then
              Exit;
      end;
      tpSku : begin
          if (qryTempSkuWeb.IsEmpty) then
              Exit;
      end;
  end;

  try
      objAplicaPrecoProduto := TfrmGerenciaProdutoWeb_AplicaPrecoProduto.Create(nil);
      objAplicaPrecoProduto.nCdTabTipoProduto := IfThen(tpProdWeb = tpProduto, 2, 3);
      objAplicaPrecoProduto.ShowModal();
  finally
      FreeAndNil(objAplicaPrecoProduto);
  end;
end;

procedure TfrmGerenciaProdutoWeb.prAtualizaDimensao(tpProdWeb : TER2TipoProdWeb);
var
  objAplicaDimensao : TfrmGerenciaProdutoWeb_AplicaDimensaoProduto;
begin
  case (tpProdWeb) of
      tpProduto : begin
          if (qryTempProdWeb.IsEmpty) then
              Exit;
      end;
      tpSku : begin
          if (qryTempSkuWeb.IsEmpty) then
              Exit;
      end;
  end;

  try
      objAplicaDimensao := TfrmGerenciaProdutoWeb_AplicaDimensaoProduto.Create(nil);
      objAplicaDimensao.ShowModal();
  finally
      FreeAndNil(objAplicaDimensao);
  end;
end;

procedure TfrmGerenciaProdutoWeb.prAtualizaStatusWeb(tpProdWeb : TER2TipoProdWeb; bStatusWeb : Boolean);
var
  nCdStatus : Integer;
begin
  case (tpProdWeb) of
      tpProduto : begin
          if (qryTempProdWeb.IsEmpty) then
              Exit;
      end;
      tpSku : begin
          if (qryTempSkuWeb.IsEmpty) then
              Exit;
      end;
  end;

  nCdStatus := IfThen(bStatusWeb, 1, 0);
end;

//bot�es

procedure TfrmGerenciaProdutoWeb.btMarcarClick(Sender: TObject);
begin
  inherited;

  prAtualizaSelecao(tpProdWebSel, True);
end;

procedure TfrmGerenciaProdutoWeb.btDesmarcarClick(Sender: TObject);
begin
  inherited;

  prAtualizaSelecao(tpProdWebSel, False);
end;

procedure TfrmGerenciaProdutoWeb.btAplicaPrecoClick(Sender: TObject);
begin
  inherited;

  prAtualizaPreco(tpProdWebSel);
end;

//filtros

procedure TfrmGerenciaProdutoWeb.ER2LookupMaskEdit2BeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
  begin
      MensagemAlerta('Selecione um departamento.');
      ER2LookupMaskEdit1.SetFocus;
      Abort;
  end;
end;

procedure TfrmGerenciaProdutoWeb.qryDepartamentoAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := 0;
  ER2LookupMaskEdit2.WhereAdicional.Clear;

  if (qryDepartamento.IsEmpty) then
      Exit;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value;
  ER2LookupMaskEdit2.WhereAdicional.Add('nCdDepartamento = ' + qryDepartamentonCdDepartamento.AsString + ' AND nCdStatus = 1');
end;

procedure TfrmGerenciaProdutoWeb.ER2LookupMaskEdit3BeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
  begin
      MensagemAlerta('Selecione uma categoria.');
      ER2LookupMaskEdit2.SetFocus;
      Abort;
  end;
end;

procedure TfrmGerenciaProdutoWeb.qryCategoriaAfterOpen(DataSet: TDataSet);
begin
  inherited;

  qrySubcategoria.Parameters.ParamByName('nCdCategoria').Value := 0;
  ER2LookupMaskEdit3.WhereAdicional.Clear;

  if (qryCategoria.IsEmpty) then
      Exit;

  qrySubcategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value;
  ER2LookupMaskEdit3.WhereAdicional.Add('nCdCategoria = ' + qryCategorianCdCategoria.AsString + ' AND nCdStatus = 1');
end;

procedure TfrmGerenciaProdutoWeb.ER2LookupMaskEdit4BeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not qrySubcategoria.active or (qrySubcategorianCdSubCategoria.Value = 0) then
  begin
      MensagemAlerta('Selecione uma subcategoria.');
      ER2LookupMaskEdit3.SetFocus;
      Abort;
  end;
end;

procedure TfrmGerenciaProdutoWeb.qrySubcategoriaAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  qrySegmento.Parameters.ParamByName('nCdSubcategoria').Value := 0;
  qryMarca.Parameters.ParamByName('nCdSubcategoria').Value := 0;
  ER2LookupMaskEdit4.WhereAdicional.Clear;
  ER2LookupMaskEdit5.WhereAdicional.Clear;

  if (qrySubcategoria.IsEmpty) then
      Exit;

  qrySegmento.Parameters.ParamByName('nCdSubcategoria').Value := qrySubcategorianCdSubCategoria.Value;
  qryMarca.Parameters.ParamByName('nCdSubcategoria').Value := qrySubcategorianCdSubCategoria.Value;
  ER2LookupMaskEdit4.WhereAdicional.Add('nCdSubcategoria = ' + qrySubcategorianCdSubCategoria.AsString + ' AND nCdStatus = 1');
  ER2LookupMaskEdit5.WhereAdicional.Add('nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + qrySubcategorianCdSubCategoria.AsString + ')');
end;

procedure TfrmGerenciaProdutoWeb.ER2LookupMaskEdit5BeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not qrySubcategoria.active or (qrySubcategorianCdSubCategoria.Value = 0) then
  begin
      MensagemAlerta('Selecione uma subcategoria.');
      ER2LookupMaskEdit3.SetFocus;
      Abort;
  end;
end;

procedure TfrmGerenciaProdutoWeb.cxGridTempProdWebDBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if (qryTempProdWeb.IsEmpty) then
      Exit;

  qryTempSkuWeb.Close;
  qryTempSkuWeb.Parameters.ParamByName('nPK').Value := qryTempProdWebnCdProduto.Value;
  qryTempSkuWeb.Open;
end;

procedure TfrmGerenciaProdutoWeb.cxGridTempProdWebEnter(Sender: TObject);
begin
  inherited;

  tpProdWebSel := tpProduto;

  btAtivarProduto.Enabled   := True;
  btInativarProduto.Enabled := True;
  btAtivarSku.Enabled       := False;
  btInativarSku.Enabled     := False;
end;

procedure TfrmGerenciaProdutoWeb.cxGridTempProdWebExit(Sender: TObject);
begin
  inherited;

  btAtivarProduto.Enabled   := False;
  btInativarProduto.Enabled := False;
  btAtivarSku.Enabled       := False;
  btInativarSku.Enabled     := False;
end;

procedure TfrmGerenciaProdutoWeb.cxGridTempSkuWebEnter(Sender: TObject);
begin
  inherited;

  tpProdWebSel := tpSku;

  btAtivarProduto.Enabled   := False;
  btInativarProduto.Enabled := False;
  btAtivarSku.Enabled       := True;
  btInativarSku.Enabled     := True;
end;

procedure TfrmGerenciaProdutoWeb.cxGridTempSkuWebExit(Sender: TObject);
begin
  inherited;

  btAtivarProduto.Enabled   := False;
  btInativarProduto.Enabled := False;
  btAtivarSku.Enabled       := False;
  btInativarSku.Enabled     := False;
end;

initialization
  RegisterClass(TfrmGerenciaProdutoWeb);

end.
