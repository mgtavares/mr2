inherited frmImpBoletoNF: TfrmImpBoletoNF
  Left = 247
  Top = 176
  Width = 1152
  Height = 770
  Caption = 'Impress'#227'o de Boleto - por NF'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 113
    Width = 1144
    Height = 630
  end
  inherited ToolBar1: TToolBar
    Width = 1144
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 29
    Width = 1144
    Height = 84
    Align = alTop
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 15
      Top = 64
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data do Faturamento'
    end
    object Label2: TLabel
      Tag = 1
      Left = 8
      Top = 40
      Width = 110
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nome Terceiro Entrega'
    end
    object Label3: TLabel
      Tag = 1
      Left = 38
      Top = 16
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Portadora'
    end
    object edtDtFaturamento: TMaskEdit
      Left = 120
      Top = 56
      Width = 120
      Height = 21
      EditMask = '!90/90/0000;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
      OnChange = edtDtFaturamentoChange
    end
    object edtTerceiro: TEdit
      Left = 120
      Top = 32
      Width = 481
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      OnChange = edtTerceiroChange
    end
    object cxButton1: TcxButton
      Left = 248
      Top = 56
      Width = 121
      Height = 25
      Caption = 'Exibir Documentos'
      TabOrder = 3
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object edtContaBancaria: TMaskEdit
      Left = 120
      Top = 8
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnChange = edtContaBancariaChange
      OnExit = edtContaBancariaExit
      OnKeyDown = edtContaBancariaKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 184
      Top = 8
      Width = 65
      Height = 21
      DataField = 'nCdBanco'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 252
      Top = 8
      Width = 100
      Height = 21
      DataField = 'cNmBanco'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 354
      Top = 8
      Width = 65
      Height = 21
      DataField = 'cAgencia'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 420
      Top = 8
      Width = 77
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 499
      Top = 8
      Width = 238
      Height = 21
      DataField = 'cNmTitular'
      DataSource = DataSource1
      TabOrder = 8
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 113
    Width = 1144
    Height = 630
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsResultado
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdDoctoFiscal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cSerie'
        Footers = <>
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'iNrDocto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Visible = False
        Width = 66
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValFatura'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iParcelas'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtImpressao'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cImpresso'
        Footers = <>
        Width = 71
      end
      item
        EditButtons = <>
        FieldName = 'nCdBancoPortador'
        Footers = <>
        Width = 105
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cDtEmissao'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'cNmTerceiro'
        DataType = ftString
        Size = 1
        Value = 'A'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtEmissao datetime'
      '       ,@cNmTerceiro  varchar(50)'
      ''
      'Set @dDtEmissao  = Convert(DATETIME,:cDtEmissao,103)'
      'Set @cNmTerceiro = RTRIM(:cNmTerceiro)'
      ''
      'SELECT nCdDoctoFiscal '
      '      ,cSerie'
      '      ,iNrDocto'
      '      ,dDtEmissao'
      '      ,nCdTerceiro '
      '      ,cNmTerceiro'
      '      ,nValTotal'
      '      ,nValFatura'
      '      ,(SELECT Count(1)'
      '          FROM PrazoDoctoFiscal PDF'
      
        '         WHERE PDF.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFiscal) ' +
        'as iParcelas'
      '      ,dDtImpressao'
      '      ,cFlgBoletoImpresso'
      '      ,CASE WHEN cFlgBoletoImpresso = 1 THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END as cImpresso'
      '      ,(SELECT TOP 1 nCdBancoPortador'
      '          FROM Titulo'
      
        '         WHERE Titulo.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFisca' +
        'l) as nCdBancoPortador'
      '  FROM DoctoFiscal'
      ' WHERE nCdStatus  = 1'
      '   AND nValFatura > 0'
      '   AND iNrDocto   > 0'
      '   AND dDtEmissao >= @dDtEmissao'
      '   AND dDtEmissao < @dDtEmissao + 1'
      
        '   AND ((@cNmTerceiro = '#39#39') OR (cNmTerceiro LIKE LTRIM(@cNmTerce' +
        'iro)+'#39'%'#39'))'
      '   AND nCdEmpresa = :nCdEmpresa'
      ' ORDER BY cSerie,iNrDocto')
    Left = 320
    Top = 184
    object qryResultadonCdDoctoFiscal: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryResultadocSerie: TStringField
      DisplayLabel = 'S'#233'rie'
      FieldName = 'cSerie'
      FixedChar = True
      Size = 2
    end
    object qryResultadoiNrDocto: TIntegerField
      DisplayLabel = 'Nr. NF'
      FieldName = 'iNrDocto'
    end
    object qryResultadodDtEmissao: TDateTimeField
      DisplayLabel = 'Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryResultadonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryResultadocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryResultadonValTotal: TBCDField
      DisplayLabel = 'Valor Nota'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryResultadonValFatura: TBCDField
      DisplayLabel = 'Valor Fatura'
      FieldName = 'nValFatura'
      Precision = 12
      Size = 2
    end
    object qryResultadoiParcelas: TIntegerField
      DisplayLabel = 'Parcelas'
      FieldName = 'iParcelas'
      ReadOnly = True
    end
    object qryResultadodDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
    object qryResultadocFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
    object qryResultadocImpresso: TStringField
      DisplayLabel = 'Impresso'
      FieldName = 'cImpresso'
      ReadOnly = True
      Size = 3
    end
    object qryResultadonCdBancoPortador: TIntegerField
      DisplayLabel = 'Banco Portador'
      FieldName = 'nCdBancoPortador'
      ReadOnly = True
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 352
    Top = 184
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,Banco.nCdBanco'
      '      ,Banco.cNmBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.cNmTitular'
      '      ,ContaBancaria.iUltimoBoleto'
      '  FROM ContaBancaria'
      
        '       LEFT JOIN Banco ON Banco.nCdBanco = ContaBancaria.nCdBanc' +
        'o'
      ' WHERE nCdContaBancaria         = :nPK'
      '   AND cFlgEmiteBoleto          = 1'
      '   AND ContaBancaria.nCdEmpresa = :nCdEmpresa')
    Left = 448
    Top = 224
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancariaiUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
  end
  object cmd: TADOCommand
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 504
    Top = 328
  end
  object SP_GERA_LANCTOFIN_EMISSAO_BOLETO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_LANCTOFIN_EMISSAO_BOLETO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTituloEmissao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 568
    Top = 248
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 608
    Top = 336
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 624
    Top = 368
  end
  object SP_REGISTRA_NUMBOLETO_NF_TITULO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRA_NUMBOLETO_NF_TITULO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdBancoPortador'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iProximoBoleto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 176
    Top = 248
  end
end
