unit fCargaInicial_TitulosPagar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmCargaInicial_TitulosPagar = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    dsTitulos: TDataSource;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosnCdEmpresa: TIntegerField;
    qryTitulosnCdEspTit: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosnCdTerceiro: TIntegerField;
    qryTitulosnCdCategFinanc: TIntegerField;
    qryTitulosnCdMoeda: TIntegerField;
    qryTituloscSenso: TStringField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtReceb: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosdDtCad: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosnCdUnidadeNegocio: TIntegerField;
    qryTitulosnCdCC: TIntegerField;
    qryTituloscFlgDocCobranca: TIntegerField;
    qryTituloscNrNF: TStringField;
    qryTitulosnCdLojaTit: TIntegerField;
    qryTituloscFlgInclusaoManual: TIntegerField;
    qryTituloscNmTerceiro: TStringField;
    qryTituloscNmCategFinanc: TStringField;
    qryTituloscNmUnidadeNegocio: TStringField;
    qryTituloscNmCC: TStringField;
    usp_ProximoID: TADOStoredProc;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    procedure qryTitulosAfterInsert(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryTitulosBeforePost(DataSet: TDataSet);
    procedure qryTitulosCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    nCdEspTit : integer ;
  public
    { Public declarations }
  end;

var
  frmCargaInicial_TitulosPagar: TfrmCargaInicial_TitulosPagar;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCargaInicial_TitulosPagar.qryTitulosAfterInsert(
  DataSet: TDataSet);
begin
  inherited;

  qryTitulosnCdEmpresa.Value         := frmMenu.nCdEmpresaAtiva ;
  qryTitulosnCdEspTit.Value          := nCdEspTit ;
  qryTitulosiParcela.Value           := 1 ;
  qryTitulosnCdMoeda.Value           := 1 ;
  qryTituloscSenso.Value             := 'D' ;
  qryTitulosdDtCad.Value             := Now() ;
  qryTituloscFlgDocCobranca.Value    := 1 ;
  qryTituloscFlgInclusaoManual.Value := 1 ;

end;

procedure TfrmCargaInicial_TitulosPagar.FormShow(Sender: TObject);
begin
  inherited;

  nCdEspTit := StrToInt(frmMenu.LeParametro('ESPTITCARGAINI')) ;

  qryTitulos.Close ;
  qryTitulos.Open ;

end;

procedure TfrmCargaInicial_TitulosPagar.qryTitulosBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTitulosnCdTitulo.Value = 0) then
  begin

      usp_ProximoID.Close;
      usp_ProximoID.Parameters.ParamByName('@cNmTabela').Value := 'TITULO' ;
      usp_ProximoID.ExecProc ;

      qryTitulosnCdTitulo.Value := usp_ProximoID.Parameters.ParamByName('@iUltimoCodigo').Value ;

  end ;

  if (qryTituloscNrNF.Value = '') and (qryTituloscNrTit.Value = '') then
  begin
      qryTituloscNrTit.Value          := qryTitulosnCdTitulo.asString ;
      qryTituloscFlgDocCobranca.Value := 1 ;
  end ;

  if (qryTituloscNrTit.Value = '') and (qryTituloscNrNF.Value <> '') then
      qryTituloscNrTit.Value := qryTituloscNrNF.Value ; 

  if (qryTituloscNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe o terceiro credor.') ;
      abort ;
  end ;

  if (qryTituloscNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe a categoria financeira.') ;
      abort ;
  end ;

  if (qrytituloscNmUnidadeNegocio.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio.') ;
      abort ;
  end ;

  if (qryTituloscNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo.') ;
      abort ;
  end ;

  if (qryTitulosdDtEmissao.asString = '') and (qryTitulosdDtVenc.asString <> '') then
      qryTitulosdDtEmissao.Value := qryTitulosdDtVenc.Value ;

  if (qryTitulosdDtVenc.Value < qryTitulosdDtEmissao.Value) then
  begin
      MensagemAlerta('Data de Vencimento menor que Data de Emiss�o.') ;
      abort ;
  end ;

  if (qryTitulosnValTit.Value <= 0) then
  begin
      MensagemAlerta('Valor do t�tulo inv�lido.') ;
      abort ;
  end ;

  if (qryTitulosiParcela.Value < 1) then
      qryTitulosiParcela.Value := 1 ;

  qryTitulosnSaldoTit.Value := qryTitulosnValTit.Value ; 




end;

procedure TfrmCargaInicial_TitulosPagar.qryTitulosCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTitulosnCdTerceiro.Value > 0) and (qryTituloscNmTerceiro.Value = '') then
  begin

      qryTerceiro.Close ;
      PosicionaQuery(qryTerceiro, qryTitulosnCdTerceiro.AsString) ;

      if not qryTerceiro.eof then
          qryTituloscNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

  end ;

  if (qryTitulosnCdCategFinanc.Value > 0) and (qryTituloscNmCategFinanc.Value = '') then
  begin

      qryCategFinanc.Close ;
      PosicionaQuery(qryCategFinanc, qryTitulosnCdCategFinanc.AsString) ;

      if not qryCategFinanc.eof then
          qryTituloscNmCategFinanc.Value := qryCategFinanccNmCategFinanc.Value ;

  end ;

  if (qryTitulosnCdUnidadeNegocio.Value > 0) and (qryTituloscNmUnidadeNegocio.Value = '') then
  begin

      qryUnidadeNegocio.Close ;
      PosicionaQuery(qryUnidadeNegocio, qryTitulosnCdUnidadeNegocio.AsString) ;

      if not qryUnidadeNegocio.eof then
          qryTituloscNmUnidadeNegocio.Value := qryUnidadeNegociocNmUnidadeNegocio.Value ;

  end ;

  if (qryTitulosnCdCC.Value > 0) and (qryTituloscNmCC.Value = '') then
  begin

      qryCC.Close ;
      PosicionaQuery(qryCC, qryTitulosnCdCC.AsString) ;

      if not qryCC.eof then
          qryTituloscNmCC.Value := qryCCcNmCC.Value ;

  end ;

end;

procedure TfrmCargaInicial_TitulosPagar.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTitulos.State = dsBrowse) then
             qryTitulos.Edit ;

        if (qryTitulos.State = dsInsert) or (qryTitulos.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 8) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(17);

                If (nPK > 0) then
                begin
                    qryTitulosnCdTerceiro.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 10) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(109);

                If (nPK > 0) then
                begin
                    qryTitulosnCdCategFinanc.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 12) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(35);

                If (nPK > 0) then
                begin
                    qryTitulosnCdUnidadeNegocio.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 14) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(29);

                If (nPK > 0) then
                begin
                    qryTitulosnCdCC.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCargaInicial_TitulosPagar.ToolButton1Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a grava��o dos t�tulos ?' +#13#13 + 'Ap�s este processo n�o ser�o permitidas altera��es nos t�tulos.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      if (qryTitulos.State <> dsBrowse) then
          qryTitulos.Post ;

      qryTitulos.UpdateBatch();

      if qryTitulos.Active then
          qryTitulos.UpdateBatch();

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryTitulos.Close ;
  qryTitulos.Open ;

end;

initialization
    RegisterClass(TfrmCargaInicial_TitulosPagar) ;

end.
