inherited frmMemoText: TfrmMemoText
  Left = 298
  Top = 135
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Texto Memo'
  KeyPreview = False
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object campoMemo: TMemo [2]
    Left = 0
    Top = 29
    Width = 656
    Height = 433
    Align = alClient
    Lines.Strings = (
      'campoMemo')
    ScrollBars = ssBoth
    TabOrder = 1
    OnKeyDown = campoMemoKeyDown
  end
end
