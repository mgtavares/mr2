inherited frmProdutoWeb_AplicaPrecoProduto: TfrmProdutoWeb_AplicaPrecoProduto
  Left = 559
  Top = 275
  VertScrollBar.Range = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Aplica Pre'#231'o Produto Web'
  ClientHeight = 154
  ClientWidth = 290
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 290
    Height = 125
  end
  object Label2: TLabel [1]
    Left = 19
    Top = 107
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pre'#231'o de Venda Por'
  end
  object Label1: TLabel [2]
    Left = 22
    Top = 64
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pre'#231'o de Venda De'
  end
  inherited ToolBar1: TToolBar
    Width = 290
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object dxCurValVendaPor: TdxCurrencyEdit [4]
    Left = 118
    Top = 99
    Width = 147
    TabOrder = 2
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  object dxCurValVendaDe: TdxCurrencyEdit [5]
    Left = 118
    Top = 56
    Width = 147
    TabOrder = 1
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  inherited ImageList1: TImageList
    Left = 192
    Top = 40
  end
  object qryAplicaPreco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nValVendaDe'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nValVendaPor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Produto'
      '   SET nValVendaWebDe  = :nValVendaDe'
      '      ,nValVendaWebPor = :nValVendaPor'
      ' WHERE nCdProdutoPai = :nPK')
    Left = 224
    Top = 40
  end
  object qryValidaPreco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      '  FROM Produto'
      ' WHERE nCdProduto      = :nPK'
      '   AND nValVendaWebDe  = 0'
      '   AND nValVendaWebPor = 0')
    Left = 256
    Top = 40
  end
end
