unit fTransacaoTEF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxTextEdit, cxCurrencyEdit, cxControls, cxContainer,
  cxEdit, cxGroupBox, cxLookAndFeelPainters, cxButtons, jpeg, ExtCtrls, DB,
  ADODB;

type
  TfrmTransacaoTEF = class(TForm)
    cxGroupBox1: TcxGroupBox;
    edtValor: TcxCurrencyEdit;
    edtDesconto: TcxCurrencyEdit;
    edtAcrescimo: TcxCurrencyEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtValorTotal: TcxCurrencyEdit;
    Label4: TLabel;
    edtQtdeParcelas: TcxTextEdit;
    Label5: TLabel;
    cxGroupBox2: TcxGroupBox;
    cxGroupBox3: TcxGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    edtDocumento: TcxTextEdit;
    edtAutorizacao: TcxTextEdit;
    Label8: TLabel;
    edtNumCartao: TcxTextEdit;
    edtOperadora: TcxTextEdit;
    Label9: TLabel;
    lblOperacao: TLabel;
    cxGroupBox4: TcxGroupBox;
    cxGroupBox5: TcxGroupBox;
    memoVia1: TMemo;
    memoVia2: TMemo;
    btnIniciaTrans: TcxButton;
    btnFinalizar: TcxButton;
    btnCancelaTrans: TcxButton;
    Image1: TImage;
    memoInfo: TMemo;
    qryOperacao: TADOQuery;
    qryOperacaoiTipoPagto: TIntegerField;
    Label10: TLabel;
    memoLog: TMemo;
    qryOperadora: TADOQuery;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    btnTran: TcxButton;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscaldDtEmissao: TStringField;
    qryTransacaoTEF: TADOQuery;
    qryTransacaoTEFnCdTransacaoTEF: TIntegerField;
    qryTransacaoTEFnCdTransacaoCartao: TIntegerField;
    qryTransacaoTEFnCdDoctoFiscal: TIntegerField;
    qryTransacaoTEFdDtTransacao: TDateTimeField;
    qryTransacaoTEFnCdStatusTransacaoTEF: TIntegerField;
    procedure btnIniciaTransClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure btnCancelaTransClick(Sender: TObject);
    procedure qryOperacaoAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnTranClick(Sender: TObject);
    procedure IniciaTransacao(iFuncao: integer; pValor, pRestricoes: String; memoLogInfo : TMemo);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    pcFormaPagto : PChar;

    sData  : String;
    sHora  : String;
    sCupom : String;

    function ShowTransacao : boolean;
    function ConfiguraSiTef (pEnderecoIP, pCodigoLoja, pNumeroTerminal : PChar; iConfiguraResultado : smallint) : integer;
    function TestaPinPad : integer;
    function ObtemTransacoesPendentes : integer;
    procedure EscreveMsgPinPad (cMsg : string);
    procedure FinalizaTrans (iFinaliza : Word);
  end;

var
  frmTransacaoTEF : TfrmTransacaoTEF;
  bStatus         : boolean;

implementation

uses
    fMenu, fCaixa_Recebimento, fInputTEF, Mask, ACBrECF;

    function ConfiguraIntSiTefInterativo (pEnderecoIP: PChar; pCodigoLoja: PChar;pNumeroTerminal: PChar;ConfiguraResultado: smallint) : integer; far; stdcall;external 'CliSiTef32I.dll';
    function EscreveMensagemPermanentePinPad(Mensagem : String) : integer; far; stdcall; external 'CliSiTef32I.dll';
    function IniciaFuncaoSiTefInterativo (Funcao: integer; pValor: String ; pNumeroCupomFiscal: String; pDataFiscal: String ;pHorario: String ;pOperador: String; pRestricoes: String): integer; far; stdcall;external 'CliSiTef32I.dll';
    function ContinuaFuncaoSiTefInterativo (var ProximoComando: Integer; var TipoCampo: Integer; var TamanhoMinimo: smallint; var TamanhoMaximo: smallint; pBuffer: PChar; TamMaxBuffer: Integer; ContinuaNavegacao: Integer): integer; far; stdcall; external 'CliSiTef32I.dll';
    function LeCartaoInterativo(Mensagem : String) : longint; far; stdcall; external 'CliSiTef32I.dll';
    function VerificaPresencaPinPad : integer; far; stdcall; external 'CliSiTef32I.dll';
    function ObtemQuantidadeTransacoesPendentes(pDataFiscal : PChar; pNumeroCupomFiscal : String) : integer; far; stdcall;external 'CliSiTef32I.dll';
    procedure FinalizaTransacaoSiTefInterativo (smallint: Word;pNumeroCuponFiscal: PChar;pDataFiscal: PChar;pHorario: PChar); far; stdcall;external 'CliSiTef32I.dll';


var
    pcValorTrans, pcQtdeParc, pcOperador, pcIpSitef, pcIdLoja, pcIdTerminal: PChar;
    iFormaPagto, iResultado : integer;
    cInfo : string;

{$R *.dfm}

procedure TfrmTransacaoTEF.btnIniciaTransClick(Sender: TObject);
begin               //2 - D�BITO ou 3 - CR�DITO
    IniciaTransacao(qryOperacaoiTipoPagto.Value, pchar(edtValorTotal.Text), pchar(edtQtdeParcelas.Text), memoInfo);
end;

procedure TfrmTransacaoTEF.btnFinalizarClick(Sender: TObject);
begin
    qryOperadora.Close;
    qryOperadora.Parameters.ParamByName('cNmOperadora').Value := Trim(edtOperadora.Text);
    qryOperadora.Open;

    if (qryOperadora.IsEmpty) then
        frmMenu.MensagemAlerta('A operadora ' +  Trim(edtOperadora.Text) + ' n�o est� cadastrada no sistema.')

    else if ((frmMenu.MessageDLG('Confirma a finaliza��o da transa��o?', mtConfirmation, [mbYes, mbNo], 0)) = mrYes) then
    begin
        frmMenu.ShowMessage('Transa��o finalizada.');

        bStatus := True;

        Close;
    end;
end;

procedure TfrmTransacaoTEF.btnCancelaTransClick(Sender: TObject);
begin
    if ((frmMenu.MessageDLG('Confirma o cancelamento da transa��o?', mtConfirmation, [mbYes, mbNo], 0)) = mrYes) then
    begin
        bStatus := False;

        Close;
    end;
end;

procedure TfrmTransacaoTEF.qryOperacaoAfterOpen(DataSet: TDataSet);
begin
    if not (qryOperacao.IsEmpty) then
        if ((qryOperacaoiTipoPagto.Value) = 2) then
            lblOperacao.Caption := '2 - D�BITO'
        else
            lblOperacao.Caption := '3 - CR�DITO';
end;

procedure TfrmTransacaoTEF.FormShow(Sender: TObject);
begin
    edtDocumento.Clear;
    edtAutorizacao.Clear;
    edtOperadora.Clear;
    edtNumCartao.Clear;

    memoVia1.Clear;
    memoVia2.Clear;

    bStatus := False;

    btnIniciaTrans.SetFocus;
end;

function TfrmTransacaoTEF.ShowTransacao: boolean;
begin
    ShowModal;

    Result := bStatus;
end;

procedure TfrmTransacaoTEF.btnTranClick(Sender: TObject);
begin
    IniciaTransacao(110, '0', '0', memoInfo);
end;

procedure TfrmTransacaoTEF.IniciaTransacao(iFuncao: integer; pValor, pRestricoes: String; memoLogInfo : TMemo);
var
    cDigitado    : string;
    cTituloInput : string;
    cAux         : string;

    iRetorno     : integer;
    iProxComando : integer;
    iTipoCampo   : integer;

    iTamanhoMin  : smallint;
    iTamanhoMax  : smallint;

    cBuffer : array [0..20000] of char;
    cMsg    : array [0..22000] of char;

    bExibeBtnPrev : boolean;
    bImprimir     : boolean;

    objInput : TfrmInputTEF;
begin
    objInput := TfrmInputTEF.Create(nil);

    if ((iFuncao = 2) or (iFuncao = 3)) then
    begin
        qryTransacaoTEF.Close;
        qryTransacaoTEF.Open;
        qryTransacaoTEF.Insert;

        qryTransacaoTEFnCdTransacaoTEF.Value       := frmMenu.fnProximoCodigo('TRANSACAOTEF');
        qryTransacaoTEFdDtTransacao.Value          := Now;
        qryTransacaoTEFnCdStatusTransacaoTEF.Value := 1; // 1 - PENDENTE

        qryTransacaoTEF.Post;

        sData := FormatDateTime('YYYYMMDD', qryTransacaoTEFdDtTransacao.Value);
        sHora := FormatDateTime('HHMMSS', qryTransacaoTEFdDtTransacao.Value);
    end

    else
    begin
        sData  := FormatDateTime('YYYYMMDD', Date);
        sHora  := FormatDateTime('HHMMSS', Time);
    end;

    sCupom := IntToStr(StrToInt(frmMenu.ACBrECF1.NumCupom) + 1);

    pcQtdeParc := pchar(edtQtdeParcelas.Text);

                                            //FUN��O - iFuncao | pValor
    iRetorno := IniciaFuncaoSiTefInterativo(iFuncao, pValor, sCupom, sData, sHora, 'Operador Sitef', '');

    if (iRetorno <> 10000) then
    begin
        FreeAndNil(objInput);

        exit;
    end;

    iTamanhoMin   := 0;
    iTamanhoMax   := 0;
    iProxComando  := 0;
    iTipoCampo    := 0;
    iResultado    := 0;

    bExibeBtnPrev := False;
    bImprimir     := False;

    repeat
        iRetorno := ContinuaFuncaoSiTefInterativo(iProxComando, iTipoCampo, iTamanhoMin, iTamanhoMax, cBuffer, sizeof(cBuffer), iResultado);

        cInfo := 'ProximoComando = ' + IntToStr(iProxComando) + ' Tipo = ' + IntToStr(iTipoCampo);

        memoLog.Lines.Add(cInfo);

        cInfo := 'Buffer = ';

        FillChar(cMsg, sizeof(cMsg), 0);

        Move(cInfo[1], cMsg[0], length(cInfo));
        Move(cBuffer[0], cMsg[(length(cInfo))], sizeof(cBuffer));

        memoLog.Lines.Add(cMsg);

        if (iRetorno = 10000) then

            case (iProxComando) of

            0: begin
                   case (iTipoCampo) of

                   100: memoLog.Lines.Add('Modalidade Pagto       = ' + cBuffer);
                   101: memoLog.Lines.Add('Modalidade Pagto Texto = ' + cBuffer);
                   102: memoLog.Lines.Add('Modalidade Pagto Desc  = ' + cBuffer);
                   105: memoLog.Lines.Add('Data/Hora Transa��o    = ' + cBuffer);

                   121: memoVia1.Lines.Add(#13#10 + cBuffer);
                   122: memoVia2.Lines.Add(#13#10 + StringReplace(cBuffer, '_', '-', [rfReplaceAll, rfIgnoreCase]));
                   123: memoLog.Lines.Add('Tipo Comprovante       = ' + cBuffer);
                   131: memoLog.Lines.Add('C�digo Institui��o     = ' + cBuffer);
                   132: memoLog.Lines.Add('C�digo Tipo Cart�o     = ' + cBuffer);
                   133: memoLog.Lines.Add('NSU SiTef              = ' + cBuffer);
                   134: begin
                            memoLog.Lines.Add('NSU Host Autorizador   = ' + cBuffer);

                            edtDocumento.Text := cBuffer;
                        end;
                   135: begin
                            memoLog.Lines.Add('C�d Autoriza��o Cr�d.  = ' + cBuffer);

                            edtAutorizacao.Text := cBuffer;
                        end;
                   136: begin
                            memoLog.Lines.Add('Bin Cart�o             = ' + cBuffer);
                            edtNumCartao.Text := cBuffer + StringOfChar('*',10);
                        end;

                   140: memoLogInfo.Lines.Add('Data primeira parcela  = ' + cBuffer);

                   156: begin
                            memoLog.Lines.Add('Nome da Institui��o    = ' + cBuffer);

                            edtOperadora.Text := cBuffer;
                        end;

                   157: memoLog.Lines.Add('C�digo Estabelecimento = ' + cBuffer);
                   158: memoLog.Lines.Add('C�digo Rede Autor      = ' + cBuffer);
                   161: memoLog.Lines.Add('Num. Cupom Pagto       = ' + cBuffer);

                   505: memoLogInfo.Lines.Add('' + cBuffer);
                   524: memoLogInfo.Lines.Add('Prim. Parcela          = ' + cBuffer);
                   525: memoLogInfo.Lines.Add('Demais Parcelas        = ' + cBuffer);

                   end;
               end;

            1: memoLogInfo.Lines.Add(cBuffer); // Mensagem para o visor do operador

            3: begin  // Mensagem para os dois visores
                   memoLogInfo.Lines.Clear;

                   memoLogInfo.Lines.Add(cBuffer);
               end;

            4: cTituloInput := cBuffer;

            13: memoLogInfo.Lines.Clear; // Limpa o campo de informa��es

            20: begin
                    //Na reimpressao de comprovantes a mensagem original exibida � "Conf.reimpressao", sem espa�o entre as palavras e sem acento
                    if (Pos('reimpressao', cBuffer) > 0) then
                        cAux := 'Confirma a reimpress�o do comprovante?'
                    else
                        cAux := cBuffer;

                    if (frmMenu.MessageDLG(cAux, mtConfirmation, mbYesNoCancel, 0) = mrYes) then
                        StrPCopy(cBuffer, '0')
                    else
                        StrPCopy(cBuffer, '1');

                    iResultado := 0;
                end;

            21: begin
                    cInfo := cBuffer;
                    cInfo := StringReplace(cInfo, ';', #13#10, [rfReplaceAll]);
                    cInfo := StringReplace(cInfo, ':', ' - ',  [rfReplaceAll]);

                    if ((Pos('Magnetico', cInfo) > 0) or (Pos('Ultimo comprovante', cInfo) > 0)) then
                        bExibeBtnPrev := False
                    else
                        bExibeBtnPrev := True;

                    if (Pos('tipo de cancelamento', cTituloInput) > 0) then
                        bImprimir := True;
{
                    else if ((qryOperacaoiTipoPagto.Value = 2) and (Pos('A Vista', cInfo) > 0)) then
                        cDigitado := '1'
                    else if ((qryOperacaoiTipoPagto.Value = 2) and ((Pos('Financ.', cInfo) > 0) or (Pos('Parcelado', cInfo) > 0))) then
                        cDigitado := '2'
                    else
}

                    if (objInput.ShowInput(cTituloInput, cInfo, iTipoCampo, iTamanhoMin, iTamanhoMax, bExibeBtnPrev)) then
                    begin
                        if (objInput.bComandoAnt) then
                            iResultado := 1

                        else
                        begin
                            cDigitado := objInput.edtValor.Text;

                            StrPCopy (cBuffer, cDigitado);

                            iResultado := 0;
                        end;
                    end
                    else
                        iRetorno := -2;

                    cTituloInput := '';
                end;

            22: begin // Aguardando tecla do operador
                    memoLogInfo.Lines.Add(cBuffer);

                    frmMenu.ShowMessage(cBuffer + '. Pressione ENTER.');
                end;

            30: begin

                    case (iTipoCampo) of

                    505: begin
                             cDigitado := pcQtdeParc;

                             StrPCopy (cBuffer, cDigitado);
                         end
                    else
                    begin
                        if (objInput.ShowInput(cTituloInput, cBuffer, iTipoCampo, iTamanhoMin, iTamanhoMax, True)) then
                        begin
                            if (objInput.bComandoAnt) then
                                iResultado  := 1

                            else
                            begin
                                cDigitado := objInput.edtValor.Text;

                                if (iTipoCampo = 511) then
                                    bImprimir := True;

                                StrPCopy (cBuffer, cDigitado);

                                iResultado := 0;
                            end;
                        end

                        else
                            iRetorno := -2;
                    end;

                    end;
                end;

            31: begin
                    if (objInput.ShowInput(cTituloInput, cBuffer, iTipoCampo, iTamanhoMin, iTamanhoMax, True)) then
                    begin
                        if (objInput.bComandoAnt) then
                            iResultado  := 1

                        else
                        begin
                            cDigitado := objInput.edtValor.Text;

                            StrPCopy (cBuffer, cDigitado);

                            iResultado := 0;
                        end;
                    end

                    else
                        iRetorno := -2;

                end;


            34: begin
                    //ShowMessage(IntToStr(iTipoCampo));

                    if (objInput.ShowInput(cTituloInput, cBuffer, iTipoCampo, iTamanhoMin, iTamanhoMax, True)) then
                    begin
                        if (objInput.bComandoAnt) then
                            iResultado  := 1

                        else
                        begin
                            cDigitado := objInput.edtValor.Text;

                            StrPCopy (cBuffer, cDigitado);

                            iResultado := 0;
                        end;
                    end

                    else
                        iRetorno := -2;
                end;

            35: begin
                    if (objInput.ShowInput(cTituloInput, cBuffer, iTipoCampo, iTamanhoMin, iTamanhoMax, True)) then
                    begin
                        if (objInput.bComandoAnt) then
                            iResultado  := 1

                        else
                        begin
                            cDigitado := objInput.edtValor.Text;

                            StrPCopy (cBuffer, cDigitado);

                            iResultado := 0;
                        end;
                    end

                    else
                        iRetorno := -2;

                end;

            41: begin
                    if (objInput.ShowInput(cTituloInput, cBuffer, iTipoCampo, iTamanhoMin, iTamanhoMax, True)) then
                    begin
                        if (objInput.bComandoAnt) then
                            iResultado  := 1

                        else
                        begin
                            cDigitado := objInput.edtValor.Text;

                            StrPCopy (cBuffer, cDigitado);

                            iResultado := 0;
                        end;
                    end

                    else
                        iRetorno := -2;

                end;
            end;

    until (iRetorno <> 10000);

    if (iRetorno = 0) then
    begin
        //Cart�o de Cr�dito ou D�bito
        if (iFuncao = 2) or (iFuncao = 3) then
        begin
            frmMenu.ShowMessage('Transa��o aprovada.');

            btnIniciaTrans.Enabled  := False;
            btnCancelaTrans.Enabled := True;
            btnFinalizar.Enabled    := True;

            btnFinalizar.SetFocus;
        end

        else
        begin
            case (iFuncao) of
                //Teste de comunica��o com o servidor SiTef
                111: frmMenu.ShowMessage('Teste de comunica��o realizado com sucesso.');

                //Reimpress�o de comprovantes
                112: frmMenu.ACBrECF1.RelatorioGerencial(memoVia1.Lines);

                110: bImprimir := True;
            end;

            if (bImprimir) then
                frmMenu.ACBrECF1.RelatorioGerencial(memoVia1.Lines);

            FinalizaTransacaoSiTefInterativo(1, '0', PChar(sData), PChar(sHora));

            memoVia1.Clear;
            memoVia2.Clear;
        end;

        memoLogInfo.Clear;
    end

    else
    begin
        case (iRetorno) of

        -1:   frmMenu.MensagemErro('M�dulo n�o inicializado.');
        -2:   frmMenu.MensagemErro('Opera��o cancelada pelo operador.');
        -3:   frmMenu.MensagemErro('Fornecida uma modalidade inv�lida.');
        -4:   frmMenu.MensagemErro('Falta de mem�ria para rodar a fun��o.');
        -5:   frmMenu.MensagemErro('Sem comunica��o com o SiTef.');
        -6:   frmMenu.MensagemErro('Opera��o cancelada pelo usu�rio.');
        -40:  frmMenu.MensagemErro('Transa��o negada pelo SiTef.');

        end;
    end;

    memoLog.Lines.Add(frmMenu.ACBrECF1.NumCupom);

    FreeAndNil(objInput);
end;

function TfrmTransacaoTEF.TestaPinPad: integer;
begin
    Result := VerificaPresencaPinPad;
end;

function TfrmTransacaoTEF.ConfiguraSiTef(pEnderecoIP, pCodigoLoja,
  pNumeroTerminal: PChar; iConfiguraResultado: smallint): integer;
begin
    Result := ConfiguraIntSiTefInterativo(pEnderecoIP, pCodigoLoja, pNumeroTerminal, iConfiguraResultado);
end;

procedure TfrmTransacaoTEF.EscreveMsgPinPad(cMsg: string);
begin
    EscreveMensagemPermanentePinPad(cMsg);
end;

procedure TfrmTransacaoTEF.FinalizaTrans(iFinaliza : Word);
var
    pCupom, pData, pHora : PChar;
begin
    pCupom := PChar(sCupom);
    pData  := PChar(sData);
    pHora  := PChar(sHora);

    FinalizaTransacaoSiTefInterativo(iFinaliza, pCupom, pData, pHora);
end;

procedure TfrmTransacaoTEF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    btnIniciaTrans.Enabled  := True;
    btnFinalizar.Enabled    := False;
    btnCancelaTrans.Enabled := False;

    memoInfo.Clear;
    memoLog.Clear;
end;

function TfrmTransacaoTEF.ObtemTransacoesPendentes : integer;
var
    sDataFiscal : String;
    sNumCupom   : String;
    iResultado  : integer;
begin
    //== TRATAR QUEDA DE ENERGIA - IMPORTANTE!!! ==

    qryDoctoFiscal.Close;
    qryDoctoFiscal.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
    qryDoctoFiscal.Open;

    if not (qryDoctoFiscal.IsEmpty) then
    begin
        sDataFiscal := FormatDateTime('YYYYMMDD', qryDoctoFiscaldDtEmissao.AsDateTime);
        sNumCupom   := qryDoctoFiscaliNrDocto.AsString;

        iResultado := ObtemQuantidadeTransacoesPendentes(PChar(sDataFiscal), sNumCupom);

        if (iResultado > 0) then
        begin
            sData  := sDataFiscal;
            sCupom := sNumCupom;
            sHora  := FormatDateTime('HHMMSS', qryDoctoFiscaldDtEmissao.AsDateTime);
        end;

        Result := iResultado;
    end

    else
        Result := 0;
end;

end.
