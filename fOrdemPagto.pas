unit fOrdemPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, Mask, DBCtrls, StdCtrls, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, cxLookAndFeelPainters,
  cxButtons, GridsEh, DBGridEh, cxPC, cxControls, DB, ADODB;

type
  TfrmOrdemPagto = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    DBEdit9: TDBEdit;
    Label7: TLabel;
    cxTabSheet3: TcxTabSheet;
    DBMemo1: TDBMemo;
    btCancelar: TcxButton;
    btAutorizar: TcxButton;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOrdemPagto: TfrmOrdemPagto;

implementation

{$R *.dfm}

end.
