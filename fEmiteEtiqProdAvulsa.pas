unit fEmiteEtiqProdAvulsa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmEmiteEtiqProdAvulsa = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    DBGridEh1: TDBGridEh;
    qryPreparaTemp: TADOQuery;
    qryTempEtiqueta: TADOQuery;
    dsTempEtiqueta: TDataSource;
    qryTempEtiquetanCdProduto: TIntegerField;
    qryTempEtiquetanQtdeEtiqueta: TIntegerField;
    qryTempEtiquetacNmProduto: TStringField;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryAux: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure qryTempEtiquetaCalcFields(DataSet: TDataSet);
    procedure qryTempEtiquetaBeforePost(DataSet: TDataSet);
    procedure ToolButton5Click(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteEtiqProdAvulsa: TfrmEmiteEtiqProdAvulsa;

implementation

uses fModImpETP, fLookup_Padrao;

{$R *.dfm}

procedure TfrmEmiteEtiqProdAvulsa.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;

  qryPreparaTemp.ExecSQL;

  qryTempEtiqueta.Close;
  qryTempEtiqueta.Open;

  DBGridEh1.SetFocus;
  
end;

procedure TfrmEmiteEtiqProdAvulsa.qryTempEtiquetaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTempEtiquetacNmProduto.Value = '') and (qryTempEtiquetanCdProduto.AsString <> '') then
  begin
      qryProduto.Close;
      PosicionaQuery(qryProduto, qryTempEtiquetanCdProduto.AsString) ;

      if not qryProduto.eof then
          qryTempEtiquetacNmProduto.Value := qryProdutocNmProduto.Value ;
          
  end ;

end;

procedure TfrmEmiteEtiqProdAvulsa.qryTempEtiquetaBeforePost(
  DataSet: TDataSet);
begin

  if (qryTempEtiquetanQtdeEtiqueta.Value <= 0) then
      qryTempEtiquetanQtdeEtiqueta.Value := 1 ;

  if (qryTempEtiquetacNmProduto.Value = '') then
  begin
      MensagemAlerta('Selecione o produto.') ;
      abort ;
  end ;

  inherited;


end;

procedure TfrmEmiteEtiqProdAvulsa.ToolButton5Click(Sender: TObject);
var
    arrID : array of integer ;
    iAux,iAuxTotal  : integer ;
    objForm : TfrmModImpETP;
begin
  inherited;

  if (qryTempEtiqueta.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto selecionado.') ;
      abort ;
  end ;

  SetLength(arrID,0) ;

  qryTempEtiqueta.First;

  iAuxTotal := 0 ;

  while not qryTempEtiqueta.Eof do
  begin
      for iAux := 0 to (qryTempEtiquetanQtdeEtiqueta.AsInteger-1) do
      begin
          SetLength(arrID,Length(arrID)+1) ;
          arrID[iAuxTotal] := qryTempEtiquetanCdProduto.Value ;
          inc(iAuxTotal) ;
      end ;

      qryTempEtiqueta.Next;
  end ;

  qryTempEtiqueta.First ;

  objForm := TfrmModImpETP.Create(nil);
  
  objForm.emitirEtiquetas(arrID,1) ;

  if (MessageDlg('Deseja limpar a lista de produtos selecionados ?',mtConfirmation,[mbYes,mbNo],0) = MrYES) then
  begin

      qryAux.SQL.Clear;
      qryAux.SQL.Add('TRUNCATE TABLE #TempEtiqueta') ;
      qryAux.ExecSQL ;

      qryTempEtiqueta.Close;
      qryTempEtiqueta.Open ;
      
  end ;

end;

procedure TfrmEmiteEtiqProdAvulsa.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 1) then
        begin

            if (qryTempEtiqueta.State = dsBrowse) then
                 qryTempEtiqueta.Edit ;


            if (qryTempEtiqueta.State = dsInsert) or (qryTempEtiqueta.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'((nCdGrade IS NOT NULL AND nCdTabTipoProduto = 3) OR (nCdGrade IS NULL AND NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto)))');

                If (nPK > 0) then
                begin
                    qryTempEtiquetanCdProduto.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmEmiteEtiqProdAvulsa) ;
    
end.
