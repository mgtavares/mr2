unit fConfereUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, ADODB;

type
  TfrmSenhaAcesso = class(TfrmProcesso_Padrao)
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocFlgAtivo: TSmallintField;
    qryUsuariocSenha: TStringField;
    function Usuario() : integer ;
    procedure Edit2Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSenhaAcesso: TfrmSenhaAcesso;

implementation

uses fMenu;

{$R *.dfm}

function TfrmSenhaAcesso.Usuario() : integer ;
begin

    Edit1.Text := '' ;
    Edit2.Text := '' ;
    
    frmSenhaAcesso.ShowModal ;

    if (qryUsuario.eof) then
        Result := -1
    else Result := qryUsuarionCdUsuario.Value ;

    qryUsuario.Close ;

end ;

procedure TfrmSenhaAcesso.Edit2Exit(Sender: TObject);
begin
  inherited;

      if (trim(Edit1.Text) = '') then
      begin
          MensagemAlerta('Informe o login.') ;
          Edit1.SetFocus ;
          exit ;
      end ;

      if (trim(Edit2.Text) = '') then
      begin
          MensagemAlerta('Informe a senha.') ;
          Edit2.SetFocus ;
          exit ;
      end ;

      qryUsuario.Close ;
      qryUsuario.Parameters.ParamByName('cNmLogin').Value := trim(Edit1.Text) ;
      qryUsuario.Open ;

      if (qryUsuario.eof) then
      begin
          MensagemAlerta('Senha inv�lida ou usu�rio n�o cadastrado.') ;
          Edit1.Text := '' ;
          Edit2.Text := '' ;
          Edit1.SetFocus ;
          exit ;
      end ;

      if (qryUsuariocSenha.Value <> frmMenu.Cripto(Edit2.Text)) then
      begin
          MensagemAlerta('Senha inv�lida ou usu�rio n�o cadastrado.') ;
          Edit1.Text := '' ;
          Edit2.Text := '' ;
          Edit1.SetFocus ;
          exit ;
      end ;

      if (qryUsuariocFlgAtivo.Value = 0) then
      begin
          MensagemAlerta('Usu�rio inativo.') ;
          Edit1.Text := '' ;
          Edit2.Text := '' ;
          Edit1.SetFocus ;
          exit ;
      end ;

      close ;

end;

procedure TfrmSenhaAcesso.FormShow(Sender: TObject);
begin
  inherited;

  Edit1.SetFocus ;
  
end;

end.
