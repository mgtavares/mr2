inherited frmAreaVendas: TfrmAreaVendas
  Caption = #193'rea de Vendas'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 31
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 21
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 37
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 72
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdAreaVenda'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 72
    Top = 56
    Width = 641
    Height = 19
    DataField = 'cNmAreaVenda'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 72
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    OnEnter = DBEdit3Enter
    OnExit = DBEdit3Exit
  end
  object cxPageControl1: TcxPageControl [8]
    Left = 72
    Top = 104
    Width = 705
    Height = 281
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    ClientRectBottom = 277
    ClientRectLeft = 4
    ClientRectRight = 701
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Divis'#227'o de Vendas'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 697
        Height = 253
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsDivisaoVenda
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdDivisaoVenda'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmDivisaoVenda'
            Footers = <>
            Title.Caption = 'Descri'#231#227'o'
            Width = 229
          end
          item
            EditButtons = <>
            FieldName = 'nCdStatus'
            Footers = <>
            Title.Caption = 'Status'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit4: TDBEdit [9]
    Tag = 1
    Left = 144
    Top = 80
    Width = 145
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStatus
    TabOrder = 5
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM AreaVenda'
      ' WHERE nCdAreaVenda = :nPK')
    Left = 480
    Top = 168
    object qryMasternCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryMastercNmAreaVenda: TStringField
      FieldName = 'cNmAreaVenda'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  inherited dsMaster: TDataSource
    Left = 528
    Top = 168
  end
  inherited qryID: TADOQuery
    Left = 576
    Top = 168
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 584
    Top = 224
  end
  inherited qryStat: TADOQuery
    Left = 488
    Top = 240
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 552
    Top = 280
  end
  inherited ImageList1: TImageList
    Left = 688
    Top = 264
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdStatus'
      '      ,cNmStatus'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK')
    Left = 184
    Top = 512
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object qryDivisaoVenda: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryDivisaoVendaBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM DivisaoVenda'
      ' WHERE nCdAreaVenda = :nPK')
    Left = 224
    Top = 512
    object qryDivisaoVendanCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryDivisaoVendacNmDivisaoVenda: TStringField
      FieldName = 'cNmDivisaoVenda'
      Size = 50
    end
    object qryDivisaoVendanCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryDivisaoVendanCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object dsStatus: TDataSource
    DataSet = qryStatus
    Left = 200
    Top = 544
  end
  object dsDivisaoVenda: TDataSource
    DataSet = qryDivisaoVenda
    Left = 240
    Top = 544
  end
end
