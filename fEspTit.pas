unit fEspTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, ImgList, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxPC, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmEspTit = class(TfrmCadastro_Padrao)
    qryMasternCdEspTit: TIntegerField;
    qryMasternCdGrupoEspTit: TIntegerField;
    qryMastercNmEspTit: TStringField;
    qryMastercTipoMov: TStringField;
    qryMastercFlgPrev: TIntegerField;
    qryGrupoEspTit: TADOQuery;
    qryGrupoEspTitnCdGrupoEspTit: TIntegerField;
    qryGrupoEspTitcNmGrupoEspTit: TStringField;
    qryMastercNmGrupoEspTit: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label5: TLabel;
    qryMastercFlgCobranca: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgInadimplencia: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    qryUsuarioMovto: TADOQuery;
    qryUsuarioListar: TADOQuery;
    dsUsuarioMovto: TDataSource;
    dsUsuarioListar: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryUsuarioMovtonCdUsuario: TIntegerField;
    qryUsuarioMovtonCdEspTit: TIntegerField;
    qryUsuarioMovtocFlgTipoAcesso: TStringField;
    qryUsuarioListarnCdUsuario: TIntegerField;
    qryUsuarioListarnCdEspTit: TIntegerField;
    qryUsuarioListarcFlgTipoAcesso: TStringField;
    qryUsuarioMovtocNmUsuario: TStringField;
    qryUsuarioListarcNmUsuario: TStringField;
    qryUsuarioMovtonCdUsuarioEspTit: TIntegerField;
    qryUsuarioListarnCdUsuarioEspTit: TIntegerField;
    DBCheckBox4: TDBCheckBox;
    qryMastercFlgAdiantamento: TIntegerField;
    qryMastercFlgExibeRazaoAuxiliar: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    qryVerificaTituloProvisao: TADOQuery;
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioMovtoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryUsuarioListarBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioMovtoCalcFields(DataSet: TDataSet);
    procedure qryUsuarioListarCalcFields(DataSet: TDataSet);
    
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEspTit: TfrmEspTit;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmEspTit.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmEspTit.DBEdit2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(9);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEspTit.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(9);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEspTit.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdGrupoEspTit.Value = 0) or (DBEdit5.Text = '') then
  begin
      ShowMessage('Informe o Grupo de Esp�cie') ;
      DBedit2.SetFocus;
      Abort ;
  end ;

  if (qryMastercNmEspTit.Value = '') then
  begin
      ShowMessage('Informe a Descri��o da Esp�cie') ;
      DBedit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoMov.Value <> 'P') and (qryMastercTipoMov.Value <> 'R') then
  begin
      ShowMessage('Tipo de Movimento Inv�lido') ;
      DBedit4.SetFocus;
      Abort ;
  end ;

  if (qryMastercFlgAdiantamento.Value = 1) and (qryMastercTipoMov.Value <> 'R') then
  begin
      ShowMessage('Esp�cie Adiantamento, somente com Tipo de Movimento Receber') ;
      DBedit4.SetFocus;
      Abort ;
  end ;

  if (qryMastercFlgAdiantamento.Value = 1) and (qryMastercFlgPrev.Value = 1) then
  begin
      ShowMessage('Provis�o n�o pode ser Esp�cie Adiantamento') ;
      DBedit4.SetFocus;
      Abort ;
  end ;


  {-- Valida��o criada para n�o permitir que uma especie de titulo se torne uma provis�o
  ou vise e versa. Se houver no m�nimo um titulo vinculado a especie de titulo n�o ser�
  permitido alterar a flag cFlgPrev pois existe uma grande influencia. --}
  if qryMaster.State in [dsEdit] then
  begin
      PosicionaQuery(qryVerificaTituloProvisao,qryMasternCdEspTit.AsString);

      if (qryVerificaTituloProvisao.IsEmpty = false) and (qryMastercFlgPrev.OldValue <> qryMastercFlgPrev.Value) then
      begin
         if (qryMastercFlgPrev.OldValue = 0) then
             MensagemAlerta('Esp�cie de T�tulo n�o permite ser considerada como provis�o, pois existem t�tulos vinculados a esta esp�cie.')
         else
             MensagemAlerta('Esp�cie de T�tulo deve ser considerada como provis�o, pois existem t�tulos provisionados vinculados a esta esp�cie.');

         Abort;
      end;
  end;

  inherited;

end;

procedure TfrmEspTit.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'ESPTIT' ;
  nCdTabelaSistema  := 7 ;
  nCdConsultaPadrao := 10 ;
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmEspTit.DBGridEh2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioMovto.State = dsBrowse ) then
            qryUsuarioMovto.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioMovtonCdUsuario.Value := nPK;

            end ;

        end ;

    end ;

  end ;

procedure TfrmEspTit.qryUsuarioMovtoBeforePost(DataSet: TDataSet);
begin

  if (qryUsuarioMovtocNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um usu�rio.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;
  
  if (qryUsuarioMovto.State = dsInsert) then
      qryUsuarioMovtonCdUsuarioEspTit.Value := frmMenu.fnProximoCodigo('USUARIOESPTIT') ;

  qryUsuarioMovtonCdEspTit.Value      := qryMaster.FieldList[0].Value;
  qryUsuarioMovtocFlgTipoAcesso.Value := 'M';

end;

procedure TfrmEspTit.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioMovto.Close;
  qryUsuarioListar.Close;

end;

procedure TfrmEspTit.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioMovto.Close;
  qryUsuarioMovto.Parameters.ParamByName('nPK').Value := qryMasternCdEspTit.Value;
  qryUsuarioMovto.Open;

  qryUsuarioListar.Close;
  qryUsuarioListar.Parameters.ParamByName('nPK').Value := qryMasternCdEspTit.Value;
  qryUsuarioListar.Open;
end;

procedure TfrmEspTit.qryUsuarioListarBeforePost(DataSet: TDataSet);
begin

  if (qryUsuarioListarcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um usu�rio.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;
  qryUsuarioListarnCdEspTit.Value      := qryMaster.FieldList[0].Value;
  qryUsuarioListarcFlgTipoAcesso.Value := 'L';

  if (qryUsuarioListar.State = dsInsert) then
      qryUsuarioListarnCdUsuarioEspTit.Value := frmMenu.fnProximoCodigo('USUARIOESPTIT') ;

end;

procedure TfrmEspTit.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioListar.State = dsBrowse ) then
            qryUsuarioListar.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioListarnCdUsuario.Value := nPK;

            end ;

        end ;

    end ;

end;

procedure TfrmEspTit.qryUsuarioMovtoCalcFields(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryUsuario, qryUsuarioMovtonCdUsuario.AsString);
  qryUsuarioMovtocNmUsuario.Value := qryUsuariocNmUsuario.Value ;
end;

procedure TfrmEspTit.qryUsuarioListarCalcFields(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryUsuario, qryUsuarioListarnCdUsuario.AsString);
  qryUsuarioListarcNmUsuario.Value := qryUsuariocNmUsuario.Value ;
end;

initialization
    RegisterClass(tFrmEspTit) ;

end.
