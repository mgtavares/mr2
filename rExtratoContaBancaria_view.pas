unit rExtratoContaBancaria_view;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, jpeg;

type
  TrptExtratoContaBancaria_view = class(TQuickRep)
    QRBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    lblEmpresa: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel3: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    SPREL_EXTRATO_BANCARIO: TADOStoredProc;
    DetailBand1: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    SPREL_EXTRATO_BANCARIOnCdItemExtrato: TAutoIncField;
    SPREL_EXTRATO_BANCARIOnCdContaBancaria: TIntegerField;
    SPREL_EXTRATO_BANCARIOnCdBanco: TIntegerField;
    SPREL_EXTRATO_BANCARIOcAgencia: TIntegerField;
    SPREL_EXTRATO_BANCARIOnCdConta: TStringField;
    SPREL_EXTRATO_BANCARIOdDtLancto: TDateTimeField;
    SPREL_EXTRATO_BANCARIOcHistorico: TStringField;
    SPREL_EXTRATO_BANCARIOcDocumento: TStringField;
    SPREL_EXTRATO_BANCARIOnValLancto: TBCDField;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel13: TQRLabel;
    lblPeriodo: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape4: TQRShape;
    QRDBText1: TQRDBText;
    QRShape6: TQRShape;
    SPREL_EXTRATO_BANCARIOcNmTitular: TStringField;
    SPREL_EXTRATO_BANCARIOcNmTipoLancto: TStringField;
    QRLabel6: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel7: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    SPREL_EXTRATO_BANCARIOnSaldoLancto: TBCDField;
    QRDBText8: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel8: TQRLabel;
    SPREL_EXTRATO_BANCARIOnValCredito: TBCDField;
    SPREL_EXTRATO_BANCARIOnValDebito: TBCDField;
    QRBand3: TQRBand;
    qrexSaldoFinal: TQRExpr;
    qrexSaldoCred: TQRExpr;
    qrexSaldoAnt: TQRExpr;
    qrexSaldoDeb: TQRExpr;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel19: TQRLabel;
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRDBText1Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
  private

  public

  end;

var
  rptExtratoContaBancaria_view: TrptExtratoContaBancaria_view;

implementation

uses
  fMenu;

{$R *.DFM}

procedure TrptExtratoContaBancaria_view.QRDBText4Print(sender: TObject;
  var Value: String);
begin

  QRDBText4.Font.Style := [] ;

  if (Value = 'SALDO ANTERIOR') then
      QRDBText4.Font.Style := [fsbold] ;

  if (Value = 'SALDO POSTERIOR') then
      QRDBText4.Font.Style := [fsbold] ;

end;

procedure TrptExtratoContaBancaria_view.QRDBText1Print(sender: TObject;
  var Value: String);
begin

   //Agora mostra um coluna para d�bito e outra para cr�dito, n�o � necess�rio diferenciar pela cor....04/02 - Olivio
  //QRDBText1.Font.Color := clBlue ;

  //if (SPREL_EXTRATO_BANCARIOnValLancto.Value < 0) then
  //    QRDBText1.Font.Color := clRed ;

end;

procedure TrptExtratoContaBancaria_view.QRDBText8Print(sender: TObject;
  var Value: String);
begin

  //QRDBText8.Font.Color := clWindowFrame;
  QRDBText8.Font.Style := [];

  if (SPREL_EXTRATO_BANCARIOnSaldoLancto.Value < 0) then
      QRDBText8.Font.Color := clRed
  else
      QRDBText8.Font.Color := clWindowText;

  if (SPREL_EXTRATO_BANCARIOcNmTipoLancto.Value = 'SALDO ANTERIOR') then
      QRDBText8.Font.Style := [fsbold];

  if (SPREL_EXTRATO_BANCARIOcNmTipoLancto.Value = 'SALDO POSTERIOR') then
      QRDBText8.Font.Style := [fsbold];
      
end;

end.
