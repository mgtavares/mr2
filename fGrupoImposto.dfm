inherited frmGrupoImposto: TfrmGrupoImposto
  Left = 196
  Top = 83
  Width = 1175
  Height = 613
  Caption = 'Grupo de Imposto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Tag = 1
    Width = 1142
    Height = 571
  end
  object Label1: TLabel [1]
    Left = 72
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 44
    Top = 62
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Grupo'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 303
    Top = 134
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe Fiscal'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [4]
    Left = 158
    Top = 134
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Medida'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [5]
    Left = 43
    Top = 158
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de ICMS'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [6]
    Left = 35
    Top = 182
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Al'#237'quota de IPI'
    FocusControl = DBEdit9
  end
  object Label9: TLabel [7]
    Left = 605
    Top = 101
    Width = 310
    Height = 13
    Caption = '(Nomenclatura Comum do Mercosul - Obrigat'#243'rio para NFe) '
    FocusControl = DBEdit2
  end
  object Label10: TLabel [8]
    Left = 414
    Top = 134
    Width = 176
    Height = 13
    Caption = '(Reduzida para Nota Fiscal Antiga)'
    FocusControl = DBEdit2
  end
  object Label12: TLabel [9]
    Left = 11
    Top = 206
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Origem Mercadoria'
  end
  object Label5: TLabel [10]
    Left = 79
    Top = 134
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ex TIPI'
    FocusControl = DBEdit5
  end
  object Label11: TLabel [11]
    Left = 39
    Top = 109
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aliq. Nacional'
    FocusControl = DBEdit11
  end
  object Label13: TLabel [12]
    Left = 165
    Top = 109
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aliq. Importada'
    FocusControl = DBEdit12
  end
  object Label14: TLabel [13]
    Left = 52
    Top = 85
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tabela IBPT'
  end
  object Label3: TLabel [14]
    Left = 579
    Top = 85
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'NCM'
    FocusControl = DBEdit12
  end
  object Label15: TLabel [15]
    Left = 296
    Top = 109
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aliq. Estadual'
    FocusControl = DBEdit3
  end
  object Label16: TLabel [16]
    Left = 423
    Top = 109
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aliq. Municipal'
    FocusControl = DBEdit14
  end
  object Label17: TLabel [17]
    Left = 673
    Top = 84
    Width = 24
    Height = 13
    Caption = 'CEST'
    FocusControl = DBEdit15
  end
  inherited ToolBar2: TToolBar
    Width = 1142
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 113
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoImposto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [20]
    Left = 113
    Top = 56
    Width = 558
    Height = 19
    DataField = 'cNmGrupoImposto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit4: TDBEdit [21]
    Left = 369
    Top = 128
    Width = 41
    Height = 19
    DataField = 'cClasseFiscal'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit6: TDBEdit [22]
    Left = 248
    Top = 128
    Width = 41
    Height = 19
    DataField = 'cUnidadeMedida'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [23]
    Left = 113
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdTipoICMS'
    DataSource = dsMaster
    TabOrder = 8
    OnKeyDown = DBEdit7KeyDown
  end
  object DBEdit8: TDBEdit [24]
    Tag = 1
    Left = 181
    Top = 152
    Width = 490
    Height = 19
    DataField = 'cNmTipoICMS'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit9: TDBEdit [25]
    Left = 113
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nAliqIPI'
    DataSource = dsMaster
    TabOrder = 9
  end
  object cxPageControl1: TcxPageControl [26]
    Left = 8
    Top = 232
    Width = 1113
    Height = 364
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 13
    ClientRectBottom = 360
    ClientRectLeft = 4
    ClientRectRight = 1109
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Estrutura de Produtos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1105
        Height = 336
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsGrupoImpostoEstrutura
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdGrupoImpostoProduto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoImposto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdDepartamento'
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmDepartamento'
            Footers = <>
            ReadOnly = True
            Width = 259
          end
          item
            EditButtons = <>
            FieldName = 'nCdCategoria'
            Footers = <>
            Width = 31
          end
          item
            EditButtons = <>
            FieldName = 'cNmCategoria'
            Footers = <>
            ReadOnly = True
            Width = 259
          end
          item
            EditButtons = <>
            FieldName = 'nCdSubCategoria'
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmSubCategoria'
            Footers = <>
            ReadOnly = True
            Width = 259
          end
          item
            EditButtons = <>
            FieldName = 'nCdSegmento'
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmSegmento'
            Footers = <>
            ReadOnly = True
            Width = 259
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1105
        Height = 376
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsGrupoImpostoProduto
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdGrupoImposto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object ComboTipoMercadoria: TDBLookupComboBox [27]
    Left = 113
    Top = 200
    Width = 558
    Height = 19
    DataField = 'nCdTabTipoOrigemMercadoria'
    DataSource = dsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    KeyField = 'nCdTabTipoOrigemMercadoria'
    ListField = 'cNmTabTipoOrigemMercadoria'
    ListSource = dsTabTipoOrigemMercadoria
    ParentFont = False
    TabOrder = 10
  end
  object DBEdit5: TDBEdit [28]
    Left = 113
    Top = 128
    Width = 41
    Height = 19
    DataField = 'cEXTIPI'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBCheckBox1: TDBCheckBox [29]
    Left = 680
    Top = 203
    Width = 121
    Height = 17
    Caption = 'Apurar PIS/COFINS'
    DataField = 'cFlgRetemPisCofins'
    DataSource = dsMaster
    TabOrder = 11
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit10: TDBEdit [30]
    Tag = 1
    Left = 181
    Top = 80
    Width = 396
    Height = 19
    DataField = 'cDescricao'
    DataSource = dsTabIBPT
    TabOrder = 14
  end
  object DBEdit11: TDBEdit [31]
    Tag = 1
    Left = 113
    Top = 104
    Width = 41
    Height = 19
    DataField = 'nAliqNacional'
    DataSource = dsTabIBPT
    TabOrder = 17
  end
  object DBEdit12: TDBEdit [32]
    Tag = 1
    Left = 248
    Top = 104
    Width = 41
    Height = 19
    DataField = 'nAliqImportado'
    DataSource = dsTabIBPT
    TabOrder = 19
  end
  object ER2LookupDBEdit1: TER2LookupDBEdit [33]
    Left = 113
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTabIBPT'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = ER2LookupDBEdit1Exit
    CodigoLookup = 767
    QueryLookup = qryTabIBPT
  end
  object DBEdit13: TDBEdit [34]
    Tag = 1
    Left = 606
    Top = 80
    Width = 65
    Height = 19
    DataField = 'cNCM'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit3: TDBEdit [35]
    Tag = 1
    Left = 369
    Top = 104
    Width = 41
    Height = 19
    DataField = 'nAliqEstadual'
    DataSource = dsTabIBPT
    TabOrder = 16
  end
  object DBEdit14: TDBEdit [36]
    Tag = 1
    Left = 504
    Top = 104
    Width = 41
    Height = 19
    DataField = 'nAliqMunicipal'
    DataSource = dsTabIBPT
    TabOrder = 18
  end
  object DBEdit15: TDBEdit [37]
    Left = 698
    Top = 80
    Width = 91
    Height = 19
    DataField = 'cCEST'
    DataSource = dsMaster
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoImposto'
      'WHERE nCdGrupoImposto = :nPK')
    Left = 432
    Top = 384
    object qryMasternCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryMastercNmGrupoImposto: TStringField
      FieldName = 'cNmGrupoImposto'
      Size = 35
    end
    object qryMastercNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryMastercClasseFiscal: TStringField
      FieldName = 'cClasseFiscal'
      FixedChar = True
      Size = 2
    end
    object qryMastercCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 2
    end
    object qryMastercUnidadeMedida: TStringField
      DisplayWidth = 3
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryMasternCdTipoICMS: TIntegerField
      FieldName = 'nCdTipoICMS'
    end
    object qryMasternAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      DisplayFormat = '#,##0.00'
      Precision = 4
      Size = 2
    end
    object qryMastercNmTipoICMS: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTipoICMS'
      LookupDataSet = qryTipoICMS
      LookupKeyFields = 'nCdTipoICMS'
      LookupResultField = 'cNmTipoICMS'
      KeyFields = 'nCdTipoICMS'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercCdSTIPI: TStringField
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryMasternCdTabTipoOrigemMercadoria: TIntegerField
      FieldName = 'nCdTabTipoOrigemMercadoria'
    end
    object qryMastercFlgRetemPisCofins: TIntegerField
      FieldName = 'cFlgRetemPisCofins'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMastercEXTIPI: TStringField
      FieldName = 'cEXTIPI'
      FixedChar = True
      Size = 2
    end
    object qryMasternCdTabIBPT: TIntegerField
      FieldName = 'nCdTabIBPT'
    end
    object qryMastercCEST: TStringField
      FieldName = 'cCEST'
      Size = 7
    end
  end
  inherited dsMaster: TDataSource
    Left = 432
    Top = 416
  end
  inherited qryID: TADOQuery
    Left = 592
    Top = 384
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 688
    Top = 416
  end
  inherited qryStat: TADOQuery
    Left = 656
    Top = 384
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 688
    Top = 384
  end
  inherited ImageList1: TImageList
    Left = 720
    Top = 384
  end
  object qryTipoICMS: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoICMS')
    Left = 560
    Top = 384
    object qryTipoICMSnCdTipoICMS: TIntegerField
      FieldName = 'nCdTipoICMS'
    end
    object qryTipoICMScNmTipoICMS: TStringField
      FieldName = 'cNmTipoICMS'
      Size = 50
    end
  end
  object qryGrupoImpostoEstrutura: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryGrupoImpostoEstruturaBeforePost
    OnCalcFields = qryGrupoImpostoEstruturaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoImpostoProduto'
      'WHERE nCdGrupoImposto = :nPK'
      'AND nCdProduto IS NULL')
    Left = 496
    Top = 384
    object qryGrupoImpostoEstruturanCdGrupoImpostoProduto: TAutoIncField
      FieldName = 'nCdGrupoImpostoProduto'
    end
    object qryGrupoImpostoEstruturanCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryGrupoImpostoEstruturanCdDepartamento: TIntegerField
      DisplayLabel = 'Departamento|C'#243'd'
      FieldName = 'nCdDepartamento'
    end
    object qryGrupoImpostoEstruturacNmDepartamento: TStringField
      DisplayLabel = 'Departamento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmDepartamento'
      Size = 50
      Calculated = True
    end
    object qryGrupoImpostoEstruturanCdCategoria: TIntegerField
      DisplayLabel = 'Categoria|C'#243'd'
      FieldName = 'nCdCategoria'
    end
    object qryGrupoImpostoEstruturacNmCategoria: TStringField
      DisplayLabel = 'Categoria|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCategoria'
      Size = 50
      Calculated = True
    end
    object qryGrupoImpostoEstruturanCdSubCategoria: TIntegerField
      DisplayLabel = 'Sub Categoria|C'#243'd'
      FieldName = 'nCdSubCategoria'
    end
    object qryGrupoImpostoEstruturacNmSubCategoria: TStringField
      DisplayLabel = 'Sub Categoria|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmSubCategoria'
      Size = 50
      Calculated = True
    end
    object qryGrupoImpostoEstruturanCdSegmento: TIntegerField
      DisplayLabel = 'Segmento|C'#243'd'
      FieldName = 'nCdSegmento'
    end
    object qryGrupoImpostoEstruturacNmSegmento: TStringField
      DisplayLabel = 'Segmento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmSegmento'
      Size = 50
      Calculated = True
    end
    object qryGrupoImpostoEstruturanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      ',cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 592
    Top = 416
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      ',cNmCategoria'
      'FROM Categoria'
      'WHERE nCdDepartamento = :nCdDepartamento'
      'AND nCdCategoria = :nPK')
    Left = 624
    Top = 416
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      ',cNmSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdCategoria = :nCdCategoria'
      'AND nCdSubCategoria = :nPK')
    Left = 656
    Top = 416
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSegmento'
      ',cNmSegmento'
      'FROM Segmento'
      'WHERE nCdSubCategoria = :nCdSubCategoria'
      'AND nCdSegmento = :nPK')
    Left = 624
    Top = 384
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
  end
  object dsGrupoImpostoEstrutura: TDataSource
    DataSet = qryGrupoImpostoEstrutura
    Left = 496
    Top = 416
  end
  object qryGrupoImpostoProduto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryGrupoImpostoProdutoBeforePost
    OnCalcFields = qryGrupoImpostoProdutoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoImposto'
      ',nCdProduto'
      ',nCdGrupoImpostoProduto'
      'FROM GrupoImpostoProduto'
      'WHERE nCdGrupoImposto = :nPK'
      'AND nCdProduto IS NOT NULL')
    Left = 528
    Top = 384
    object qryGrupoImpostoProdutonCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryGrupoImpostoProdutonCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryGrupoImpostoProdutocNmProduto: TStringField
      DisplayLabel = 'Produto|Nome Produto'
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      Size = 150
      Calculated = True
    end
    object qryGrupoImpostoProdutonCdGrupoImpostoProduto: TIntegerField
      FieldName = 'nCdGrupoImpostoProduto'
    end
  end
  object dsGrupoImpostoProduto: TDataSource
    DataSet = qryGrupoImpostoProduto
    Left = 528
    Top = 416
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      ',cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 560
    Top = 416
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryTabTipoOrigemMercadoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoOrigemMercadoria')
    Left = 464
    Top = 384
    object qryTabTipoOrigemMercadorianCdTabTipoOrigemMercadoria: TIntegerField
      FieldName = 'nCdTabTipoOrigemMercadoria'
    end
    object qryTabTipoOrigemMercadoriacNmTabTipoOrigemMercadoria: TStringField
      FieldName = 'cNmTabTipoOrigemMercadoria'
      Size = 100
    end
  end
  object dsTabTipoOrigemMercadoria: TDataSource
    DataSet = qryTabTipoOrigemMercadoria
    Left = 464
    Top = 416
  end
  object qryTabIBPT: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryTabIBPTAfterOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM TabIBPT'
      ' WHERE  nCdTabIBPT = :nPK')
    Left = 400
    Top = 384
    object qryTabIBPTnCdTabIBPT: TIntegerField
      FieldName = 'nCdTabIBPT'
    end
    object qryTabIBPTcNCM: TStringField
      FieldName = 'cNCM'
      Size = 8
    end
    object qryTabIBPTiTabela: TIntegerField
      FieldName = 'iTabela'
    end
    object qryTabIBPTcDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 1000
    end
    object qryTabIBPTnAliqNacional: TBCDField
      FieldName = 'nAliqNacional'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTnAliqImportado: TBCDField
      FieldName = 'nAliqImportado'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTcFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryTabIBPTnAliqEstadual: TBCDField
      FieldName = 'nAliqEstadual'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTnAliqMunicipal: TBCDField
      FieldName = 'nAliqMunicipal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsTabIBPT: TDataSource
    DataSet = qryTabIBPT
    Left = 400
    Top = 416
  end
end
