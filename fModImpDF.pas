unit fModImpDF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  DBGridEhGrouping, ToolCtrlsEh, cxLookAndFeelPainters, cxButtons, Grids,
  ValEdit, cxSplitter, dxCalc, Menus;

type
  TfrmModImpDF = class(TfrmCadastro_Padrao)
    qryMasternCdModImpDF: TIntegerField;
    qryMastercNmModImpDF: TStringField;
    qryMastercQueryHeader: TMemoField;
    qryMastercQueryDetalhe: TMemoField;
    qryMastercQueryFatura: TMemoField;
    qryMastercQueryMensagem: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    tabFatura: TcxTabSheet;
    tabItem: TcxTabSheet;
    tabMensagem: TcxTabSheet;
    Label4: TLabel;
    DBMemo2: TDBMemo;
    DBMemo3: TDBMemo;
    Label7: TLabel;
    DBMemo4: TDBMemo;
    qryCampoC: TADOQuery;
    dsCampoC: TDataSource;
    qryCampoCnCdCampoModImpDF: TIntegerField;
    qryCampoCnCdModImpDF: TIntegerField;
    qryCampoCcParte: TStringField;
    qryCampoCiSequencia: TIntegerField;
    qryCampoCcNmCampo: TStringField;
    qryCampoCcDescricao: TStringField;
    qryCampoCiLinha: TIntegerField;
    qryCampoCiColuna: TIntegerField;
    qryCampoCcTipoDado: TStringField;
    qryCampoCcTamanhoFonte: TStringField;
    qryCampoCcEstiloFonte: TStringField;
    qryCampoCcLPP: TStringField;
    qryMastercQueryAutent: TMemoField;
    qryMasteriLinhaEntreItens: TIntegerField;
    tabAutenticacao: TcxTabSheet;
    Label9: TLabel;
    DBMemo5: TDBMemo;
    qryCampoA: TADOQuery;
    dsCampoA: TDataSource;
    qryCampoAnCdCampoModImpDF: TAutoIncField;
    qryCampoAnCdModImpDF: TIntegerField;
    qryCampoAcParte: TStringField;
    qryCampoAiSequencia: TIntegerField;
    qryCampoAcNmCampo: TStringField;
    qryCampoAcDescricao: TStringField;
    qryCampoAiLinha: TIntegerField;
    qryCampoAiColuna: TIntegerField;
    qryCampoAcTipoDado: TStringField;
    qryCampoAcTamanhoFonte: TStringField;
    qryCampoAcEstiloFonte: TStringField;
    qryCampoAcLPP: TStringField;
    qryMastercFlgTipoImpDF: TStringField;
    rgTipoImpDF: TDBRadioGroup;
    qryMasteriColuna: TIntegerField;
    qryMasteriEspaco: TIntegerField;
    qryMasteriBuffer: TIntegerField;
    qryMastercFlgTraduzirTag: TIntegerField;
    qryMastercFlgIgnorarTag: TIntegerField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    checkTraduzirTag: TDBCheckBox;
    checkIgnoraTag: TDBCheckBox;
    Label13: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    tabCabecalho: TcxTabSheet;
    Label3: TLabel;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    DBComboBox5: TDBComboBox;
    qryMastercPageCodImpDF: TStringField;
    Label8: TLabel;
    qryMasteriLarguraBarras: TIntegerField;
    qryMasteriAlturaBarras: TIntegerField;
    qryMastercFlgNumBarras: TIntegerField;
    qryMasteriTipoQRCode: TIntegerField;
    qryMasteriLarguraQRCode: TIntegerField;
    qryMasteriLevelErroQRCode: TIntegerField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    checkNumBarra: TDBCheckBox;
    Label15: TLabel;
    DBEdit9: TDBEdit;
    Label16: TLabel;
    DBEdit10: TDBEdit;
    Label17: TLabel;
    DBEdit11: TDBEdit;
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    DBRadioGroup1: TDBRadioGroup;
    cxPageControl2: TcxPageControl;
    tabTags: TcxTabSheet;
    qryTags: TADOQuery;
    qryTagsnCdTag: TAutoIncField;
    qryTagscTag: TStringField;
    qryTagscDescricao: TStringField;
    dsTags: TDataSource;
    DBGridEh1_: TDBGridEh;
    DBRadioGroup2: TDBRadioGroup;
    Panel2: TPanel;
    cxPageControl3: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2_: TDBGridEh;
    DBRadioGroup4: TDBRadioGroup;
    DBGridEh2: TDBGridEh;
    Panel3: TPanel;
    cxPageControl4: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh3_: TDBGridEh;
    DBRadioGroup3: TDBRadioGroup;
    DBGridEh3: TDBGridEh;
    Panel4: TPanel;
    cxPageControl5: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    DBGridEh4_: TDBGridEh;
    DBRadioGroup5: TDBRadioGroup;
    DBGridEh4: TDBGridEh;
    Panel5: TPanel;
    cxPageControl6: TcxPageControl;
    cxTabSheet4: TcxTabSheet;
    DBGridEh5_: TDBGridEh;
    DBRadioGroup6: TDBRadioGroup;
    DBGridEh5: TDBGridEh;
    qryCampoCcTagFormatacao: TStringField;
    qryCampoAcTagFormatacao: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    menuOpcao: TPopupMenu;
    btDuplicarModelo: TMenuItem;
    qryDuplicarModelo: TADOQuery;
    qryDuplicarModelonCdModImpDF: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryCampoCBeforePost(DataSet: TDataSet);
    procedure tabFaturaEnter(Sender: TObject);
    procedure tabItemEnter(Sender: TObject);
    procedure tabMensagemEnter(Sender: TObject);
    procedure tabCabecalhoEnter(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure tabAutenticacaoEnter(Sender: TObject);
    procedure qryCampoABeforePost(DataSet: TDataSet);
    procedure rgTipoImpDFChange(Sender: TObject);
    procedure cxPageControl2Enter(Sender: TObject);
    procedure cxPageControl2Exit(Sender: TObject);
    procedure btDuplicarModeloClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModImpDF: TfrmModImpDF;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmModImpDF.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MODIMPDF' ;
  nCdTabelaSistema  := 43 ;
  nCdConsultaPadrao := 92 ;
  bLimpaAposSalvar  := False ;

  DBMemo1.Font.Name := 'Consolas';
  DBMemo1.Font.Size := 8;
  DBMemo2.Font.Name := 'Consolas';
  DBMemo2.Font.Size := 8;
  DBMemo3.Font.Name := 'Consolas';
  DBMemo3.Font.Size := 8;
  DBMemo4.Font.Name := 'Consolas';
  DBMemo4.Font.Size := 8;
  DBMemo5.Font.Name := 'Consolas';
  DBMemo5.Font.Size := 8;

  cxPageControl1.ActivePage := tabCabecalho;
  cxPageControl2.Height := 23;
  cxPageControl3.Height := 23;
  cxPageControl4.Height := 23;
  cxPageControl5.Height := 23;
  cxPageControl6.Height := 23;

  qryTags.Close;
  qryTags.Open;
end;

procedure TfrmModImpDF.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
  rgTipoImpDF.ItemIndex := 0;
end;

procedure TfrmModImpDF.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.ActivePage := tabCabecalho;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'C' ;

  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;
end;

procedure TfrmModImpDF.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'C' ;

  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmModImpDF.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (not qryMaster.Active) then
      abort ;


  if (qryMasternCdModImpDF.Value = 0) then
      qryMaster.Post ;

end;

procedure TfrmModImpDF.qryCampoCBeforePost(DataSet: TDataSet);
begin
  {if (cxPageControl1.ActivePage <> tabAutenticacao) then
  begin
      if ((qryMastercFlgTipoImpDF.Value = 'T') and (Pos('@@value', qryCampoCcTagFormatacao.Value) = 0)) then
      begin
          MensagemAlerta('Informe o valor @@value na tag de formatação para que seja substituído pelo valor correspondente no momento de sua impressão.');
          Abort;
      end;
  end;

  if (cxPageControl1.ActivePage = tabAutenticacao) then
  begin
      if ((qryMastercFlgTipoImpDF.Value = 'T') and (Pos('@@value', qryCampoAcTagFormatacao.Value) = 0) and (cxPageControl1.ActivePage = tabAutenticacao)) then
      begin
          MensagemAlerta('Informe o valor @@value na tag de formatação para que seja substituído pelo valor correspondente no momento de sua impressão.');
          Abort;
      end;
  end;}

  inherited;

  qryCampoCnCdModImpDF.Value := qryMasternCdModImpDF.Value ;
  qryCampoCcParte.Value      := qryCampoC.Parameters.ParamByName('cParte').Value ;
end;

procedure TfrmModImpDF.tabFaturaEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'F' ;
  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;

end;

procedure TfrmModImpDF.tabItemEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'D' ;
  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;

end;

procedure TfrmModImpDF.tabMensagemEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'M' ;
  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;

end;

procedure TfrmModImpDF.tabCabecalhoEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryCampoC.Close ;
  qryCampoC.Parameters.ParamByName('cParte').Value := 'C' ;

  PosicionaQuery(qryCampoC, qryMasternCdModImpDF.AsString) ;
end;

procedure TfrmModImpDF.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCampoC.Close ;
end;

procedure TfrmModImpDF.tabAutenticacaoEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryCampoA.Close ;
  qryCampoA.Parameters.ParamByName('cParte').Value := 'A' ;
  PosicionaQuery(qryCampoA, qryMasternCdModImpDF.AsString) ;

end;

procedure TfrmModImpDF.qryCampoABeforePost(DataSet: TDataSet);
begin
  inherited;
  qryCampoAnCdModImpDF.Value := qryMasternCdModImpDF.Value ;
  qryCampoAcParte.Value      := qryCampoA.Parameters.ParamByName('cParte').Value ;

end;

procedure TfrmModImpDF.rgTipoImpDFChange(Sender: TObject);
var
  i                 : Integer;
  bVisibleMatricial : Boolean;
  bVisibleTermica   : Boolean;
begin
  inherited;

  case (rgTipoImpDF.ItemIndex) of
      //matricial
      0 : begin
          bVisibleMatricial := True;
          bVisibleTermica   := False;
      end;
      //termica
      1 : begin
          bVisibleMatricial := False;
          bVisibleTermica   := True;
      end;
  end;

  for i := 0 to ComponentCount - 1 do
  begin
      if (Components[i] is TDBGridEh) then
      begin
          if ((Components[i] as TDBGridEh).Tag <> 2) then
          begin
              (Components[i] as TDBGridEh).Columns[9].Visible  := bVisibleMatricial;
              (Components[i] as TDBGridEh).Columns[10].Visible := bVisibleMatricial;
              (Components[i] as TDBGridEh).Columns[11].Visible := bVisibleMatricial;
              (Components[i] as TDBGridEh).Columns[12].Visible := bVisibleTermica;
          end;
      end;
  end;
end;

procedure TfrmModImpDF.cxPageControl2Enter(Sender: TObject);
begin
  inherited;

  cxPageControl2.Height := 260;
  cxPageControl3.Height := 260;
  cxPageControl4.Height := 260;
  cxPageControl5.Height := 260;
  cxPageControl6.Height := 260;
end;

procedure TfrmModImpDF.cxPageControl2Exit(Sender: TObject);
begin
  inherited;

  cxPageControl2.Height := 23;
  cxPageControl3.Height := 23;
  cxPageControl4.Height := 23;
  cxPageControl5.Height := 23;
  cxPageControl6.Height := 23;
end;

procedure TfrmModImpDF.btDuplicarModeloClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsEdit) then
      qryMaster.Post;

  case MessageDlg('Confirma a duplicação do modelo ativo ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;
      frmMenu.mensagemUsuario('Duplicando modelo...');
      
      qryDuplicarModelo.Close;
      qryDuplicarModelo.Parameters.ParamByName('nPK').Value := qryMasternCdModImpDF.Value;
      qryDuplicarModelo.Open;

      frmMenu.Connection.CommitTrans;
      frmMenu.mensagemUsuario('');

      PosicionaPK(qryDuplicarModelonCdModImpDF.Value);

      ShowMessage('Novo modelo gerado com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

initialization
    RegisterClass(TfrmModImpDF) ;

end.
