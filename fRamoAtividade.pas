unit fRamoAtividade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmRamoAtividade = class(TfrmCadastro_Padrao)
    qryMasternCdRamoAtividade: TIntegerField;
    qryMastercNmRamoAtividade: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRamoAtividade: TfrmRamoAtividade;

implementation

{$R *.dfm}

procedure TfrmRamoAtividade.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus ;
  
end;

procedure TfrmRamoAtividade.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe o Ramo de Atividade.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  inherited;


end;

procedure TfrmRamoAtividade.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'RAMOATIVIDADE' ;
  nCdTabelaSistema  := 70 ;
  nCdConsultaPadrao := 158 ;

end;

initialization
    RegisterClass(TfrmRamoAtividade) ;

end.
