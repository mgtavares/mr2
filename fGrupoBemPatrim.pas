unit fGrupoBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping;

type
  TfrmGrupoBemPatrim = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoBemPatrim: TIntegerField;
    qryMastercNmGrupoBemPatrim: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qrySubGrupoBemPatrim: TADOQuery;
    dsSubGrupoBemPatrim: TDataSource;
    qrySubGrupoBemPatrimnCdSubGrupoBemPatrim: TAutoIncField;
    qrySubGrupoBemPatrimnCdGrupoBemPatrim: TIntegerField;
    qrySubGrupoBemPatrimcNmSubGrupoBemPatrim: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qrySubGrupoBemPatrimBeforePost(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoBemPatrim: TfrmGrupoBemPatrim;

implementation

uses fLookup_Padrao, fConfigSubGrupoBemPatrim;

{$R *.dfm}

procedure TfrmGrupoBemPatrim.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOBEMPATRIM' ;
  nCdTabelaSistema  := 500 ;
  nCdConsultaPadrao := 1009 ;

end;

procedure TfrmGrupoBemPatrim.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmGrupoBemPatrim.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o do Grupo.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

end;
procedure TfrmGrupoBemPatrim.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qrySubGrupoBemPatrim , qryMasternCdGrupoBemPatrim.AsString) ;
end;

procedure TfrmGrupoBemPatrim.qrySubGrupoBemPatrimBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  qrySubGrupoBemPatrimcNmSubGrupoBemPatrim.Value := UpperCase(qrySubGrupoBemPatrimcNmSubGrupoBemPatrim.Value);
  qrySubGrupoBemPatrimnCdGrupoBemPatrim.Value    := qryMasternCdGrupoBemPatrim.Value;
end;

procedure TfrmGrupoBemPatrim.DBGridEh1DblClick(Sender: TObject);
var
   objForm : TfrmConfigSubGrupoBemPatrim;
begin
  inherited;
  if not qrySubGrupoBemPatrim.Eof then
  begin

     objForm :=  TfrmConfigSubGrupoBemPatrim.Create(Nil);

     objForm.posicionaQuery(objForm.qrySubGrupoBemPatrimAux,qrySubGrupoBemPatrimnCdSubGrupoBemPatrim.AsString);
     objForm.posicionaQuery(objForm.qryConfigSubGrupoBemPatrim,qrySubGrupoBemPatrimnCdSubGrupoBemPatrim.AsString);
     showForm(objForm,True);
  end;

end;

procedure TfrmGrupoBemPatrim.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
     if (qryMaster.State = dsInsert) then
      qryMaster.Post ;
end;

procedure TfrmGrupoBemPatrim.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qrySubGrupoBemPatrim.Close;
  PosicionaQuery(qrySubGrupoBemPatrim , qryMasternCdGrupoBemPatrim.AsString) ;
end;

procedure TfrmGrupoBemPatrim.btCancelarClick(Sender: TObject);
begin
  inherited;
  qrySubGrupoBemPatrim.Close;
end;

initialization
    RegisterClass(TfrmGrupoBemPatrim) ;

end.
