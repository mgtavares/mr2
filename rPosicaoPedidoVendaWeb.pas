unit rPosicaoPedidoVendaWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, ER2Excel, DBCtrls;

type
  TrptPosicaoPedidoVendaWeb = class(TfrmRelatorio_Padrao)
    edtTerceiro: TER2LookupMaskEdit;
    edtStatus: TER2LookupMaskEdit;
    Label9: TLabel;
    edtDtPedIni: TMaskEdit;
    Label8: TLabel;
    edtDtPedFin: TMaskEdit;
    Label3: TLabel;
    edtDtIntegraIni: TMaskEdit;
    Label2: TLabel;
    edtDtIntegraFin: TMaskEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    dsTerceiro: TDataSource;
    qryTabStatusPed: TADOQuery;
    qryTabStatusPednCdTabStatusPed: TIntegerField;
    qryTabStatusPedcNmTabStatusPed: TStringField;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    dsTabStatusPed: TDataSource;
    Label4: TLabel;
    rgModoExibicao: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoPedidoVendaWeb: TrptPosicaoPedidoVendaWeb;

implementation

uses
    fMenu, rPosicaoPedidoVendaWeb_view;

{$R *.dfm}

procedure TrptPosicaoPedidoVendaWeb.ToolButton1Click(Sender: TObject);
var
  objRel  : TrptPosicaoPedidoVendaWeb_view;
  cFiltro : String;
  iLinha  : integer;
begin
  inherited;

  // Valida as Datas
  if (   ((Trim(edtDtPedIni.Text) <> '/  /') and (Trim(edtDtPedFin.Text)  = '/  /'))
      or ((Trim(edtDtPedIni.Text)  = '/  /') and (Trim(edtDtPedFin.Text) <> '/  /'))) then
  begin
      MensagemAlerta('Informe uma intervalo de pedido v�lido.');
      edtDtPedIni.SetFocus;
      Abort;
  end;

  if (   ((Trim(edtDtIntegraIni.Text) <> '/  /') and (Trim(edtDtIntegraFin.Text)  = '/  /'))
      or ((Trim(edtDtIntegraIni.Text)  = '/  /') and (Trim(edtDtIntegraFin.Text) <> '/  /'))) then
  begin
      MensagemAlerta('Informe uma intervalo de integra��o v�lido.');
      edtDtIntegraIni.SetFocus;
      Abort;
  end;

  // Concatena o Filtro
  if not (qryTerceiro.IsEmpty) then
      cFiltro := cFiltro + 'Cliente: ' + qryTerceironCdTerceiro.AsString + ' - ' + qryTerceirocNmTerceiro.Value + ' / ';

  if not (qryTabStatusPed.IsEmpty) then
      cFiltro := cFiltro + 'Status: ' + qryTabStatusPednCdTabStatusPed.AsString + ' - ' + qryTabStatusPedcNmTabStatusPed.Value + ' / ';

  if ((Trim(edtDtPedIni.Text) <> '/  /') and (Trim(edtDtPedFin.Text) <> '/  /')) then
      cFiltro := cFiltro + 'Dt. Pedido: ' + edtDtPedIni.Text + ' at� ' + edtDtPedFin.Text + ' / ';


  if ((Trim(edtDtIntegraIni.Text) <> '/  /') and (Trim(edtDtIntegraFin.Text) <> '/  /')) then
      cFiltro := cFiltro + 'Dt. Integra��o: ' + edtDtIntegraIni.Text + ' at� ' + edtDtIntegraFin.Text;

  try
      // Cria e retorna o relat�rio
      objRel := TrptPosicaoPedidoVendaWeb_view.Create(nil);

      try
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Close;
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@nCdCliente').Value      := frmMenu.ConvInteiro(edtTerceiro.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@nCdTabStatusPed').Value := frmMenu.ConvInteiro(edtStatus.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@cDtPedIni').Value       := frmMenu.ConvData(edtDtPedIni.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@cDtPedFin').Value       := frmMenu.ConvData(edtDtPedFin.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@cDtIntegraIni').Value   := frmMenu.ConvData(edtDtIntegraIni.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Parameters.ParamByName('@cDtIntegraFin').Value   := frmMenu.ConvData(edtDtIntegraFin.Text);
          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Open;
      except
          MensagemErro('Erro na cria��o do relat�rio.');
          Raise;
          Abort;        
      end;

      // Exibi��o em Relat�rio
      if (rgModoExibicao.ItemIndex = 0) then
      begin
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro.Caption  := cFiltro;

          objRel.QuickRep.PreviewModal;
      end

      // Exibi��o em Planilha
      else
      begin
          frmMenu.mensagemUsuario('Exportando Planilha...');

          ER2Excel.Celula['A1'].Text := 'ER2Soft - Solu��es inteligentes para o seu neg�cio.';
          ER2Excel.Celula['A2'].Text := 'Rel. Posi��o de Pedidos de Venda Web';
          ER2Excel.Celula['A3'].Text := 'Filtros: ' + cFiltro;

          ER2Excel.Celula['A5'].Text := 'C�d. Pedido Web';
          ER2Excel.Celula['B5'].Text := 'C�d. Pedido';
          ER2Excel.Celula['C5'].Text := 'Dt. Pedido';
          ER2Excel.Celula['D5'].Text := 'Status';
          ER2Excel.Celula['E5'].Text := 'Dt. Integra��o';
          ER2Excel.Celula['F5'].Text := 'Usu. Integra��o';
          ER2Excel.Celula['G5'].Text := 'Cliente';
          ER2Excel.Celula['H5'].Text := 'Tipo Entrega';
          ER2Excel.Celula['I5'].Text := 'Tracking Code';
          ER2Excel.Celula['J5'].Text := 'C�d. Cupom';
          ER2Excel.Celula['K5'].Text := 'Val. Produtos';
          ER2Excel.Celula['L5'].Text := 'Val. Descontos';
          ER2Excel.Celula['M5'].Text := 'Val. Frete';
          ER2Excel.Celula['N5'].Text := 'Val. Desc. Frete';
          ER2Excel.Celula['O5'].Text := 'Val. Tot. Frete';
          ER2Excel.Celula['P5'].Text := 'C�d. Produto';
          ER2Excel.Celula['Q5'].Text := 'Descri��o';
          ER2Excel.Celula['R5'].Text := 'Qtde. Pedido';
          ER2Excel.Celula['S5'].Text := 'Val. Unit�rio';
          ER2Excel.Celula['T5'].Text := 'Val. Desc. Item';
          ER2Excel.Celula['U5'].Text := 'Val. Total Item';

          ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A1'].Range('U5');
          ER2Excel.Celula['A1'].Congelar('A6');

          iLinha := 6;

          objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.First;

          while not (objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Eof) do
          begin
              ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcCdPedidoWeb.Value;
              ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnCdPedido.Value;
              ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBdDtPedido.AsString;
              ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcNmTabStatusPed.Value;
              ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBdDtIntegracao.AsString;
              ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcNmLogin.Value;
              ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcNmCliente.Value;
              ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcTipoEntrega.Value;
              ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcTrackingCode.Value;
              ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcCupom.Value;
              ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValProdutos.Value;
              ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescontos.Value;
              ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValFrete.Value;
              ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescFrete.Value;
              ER2Excel.Celula['O' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValTotalFrete.Value;
              ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnCdProduto.Value;
              ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBcNmItem.Value;
              ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnQtdePed.Value;
              ER2Excel.Celula['S' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValUnitario.Value;
              ER2Excel.Celula['T' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescontoItem.Value;
              ER2Excel.Celula['U' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_PEDIDO_VENDA_WEBnValTotalItem.Value;

              ER2Excel.Celula['K' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['L' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['M' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['N' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['O' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['R' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['S' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['T' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel.Celula['U' + IntToStr(iLinha)].HorizontalAlign := haRight;

              objRel.SPREL_POSICAO_PEDIDO_VENDA_WEB.Next;

              Inc(iLinha);
          end;

          { -- exporta planilha e limpa result do componente -- }
          ER2Excel.ExportXLS;
          ER2Excel.CleanupInstance;

          frmMenu.mensagemUsuario('');
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

initialization
    RegisterClass(TrptPosicaoPedidoVendaWeb);

end.
