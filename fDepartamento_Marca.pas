unit fDepartamento_Marca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmDepartamento_Marca = class(TfrmProcesso_Padrao)
    dsMarcaSub: TDataSource;
    qryMarcaSub: TADOQuery;
    qryMarcaSubnCdMarcaSubCategoria: TAutoIncField;
    qryMarcaSubnCdMarca: TIntegerField;
    qryMarcaSubnCdSubCategoria: TIntegerField;
    qryMarcaSubcNmMarca: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    DBGridEh1: TDBGridEh;
    qryAux: TADOQuery;
    procedure qryMarcaSubBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdSubCategoria : integer ;
  end;

var
  frmDepartamento_Marca: TfrmDepartamento_Marca;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmDepartamento_Marca.qryMarcaSubBeforePost(DataSet: TDataSet);
begin

  if (qryMarcaSub.State = dsInsert) then
  begin
      qryAux.Close ;
      qryAux.Parameters.ParamByName('nCdMarca').Value := qryMarcaSubnCdMarca.Value ;
      qryAux.Parameters.ParamByName('nCdSubCategoria').Value := nCdSubCategoria ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Marca j� vinculada.') ;
          abort ;
      end ;

      qryAux.Close ;

      qryMarcaSubnCdSubCategoria.Value := nCdSubCategoria ;
  end ;

  if (qryMarcaSub.State = dsInsert) then
      qryMarcaSubnCdMarcaSubCategoria.Value := frmMenu.fnProximoCodigo('MARCASUBCATEGORIA') ;

  inherited;


end;

procedure TfrmDepartamento_Marca.FormShow(Sender: TObject);
begin
  inherited;

  qryMarcaSub.Close ;
  Posicionaquery(qryMarcaSub, intToStr(nCdSubCategoria)) ;
  
end;

procedure TfrmDepartamento_Marca.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMarcaSub.State = dsBrowse) then
             qryMarcaSub.Insert ;

        if (qryMarcaSub.State = dsInsert) or (qryMarcaSub.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(49);

            If (nPK > 0) then
            begin
                qryMarcaSubnCdMarca.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepartamento_Marca.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDepartamento_Marca.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

end.
