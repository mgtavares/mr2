unit fRecebimento_Etiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBClient, GridsEh, DBGridEh, ADODB;

type
  TfrmRecebimento_Etiqueta = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    cdsProduto: TClientDataSet;
    dsProdutos: TDataSource;
    cdsProdutonCdProduto: TIntegerField;
    cdsProdutocReferencia: TStringField;
    cdsProdutocNmProduto: TStringField;
    cdsProdutocFlgOK: TIntegerField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutonCdGrade: TIntegerField;
    qryAux: TADOQuery;
    cdsProdutonCdItemRecebimento: TIntegerField;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRecebimento_Etiqueta: TfrmRecebimento_Etiqueta;

implementation

uses rRecebimento_Etiqueta_view, fMenu;

{$R *.dfm}

procedure TfrmRecebimento_Etiqueta.ToolButton6Click(Sender: TObject);
begin
  inherited;

  if not cdsProduto.Active then
      exit ;

  cdsProduto.First ;

  while not cdsProduto.eof do
  begin
      cdsProduto.Edit ;
      cdsProdutocFlgOK.Value := 0 ;
      cdsProduto.Post ;

      cdsProduto.Next ;
  end ;

  cdsProduto.First ;

end;

procedure TfrmRecebimento_Etiqueta.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if not cdsProduto.Active then
      exit ;

  cdsProduto.First ;

  while not cdsProduto.eof do
  begin
      cdsProduto.Edit ;
      cdsProdutocFlgOK.Value := 1 ;
      cdsProduto.Post ;

      cdsProduto.Next ;
  end ;

  cdsProduto.First ;

end;

procedure TfrmRecebimento_Etiqueta.ToolButton1Click(Sender: TObject);
var
    iEtiqueta    : integer ;
    iAux         : integer ;
    cNmLoja      : string[40]  ;
    cMsgEtiqueta : string[40]  ;
begin
  inherited;

  cNmLoja      := frmMenu.LeParametro('MSGETIQNMLOJA') ;
  cMsgEtiqueta := frmMenu.LeParametro('MSGETIQPROD') ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;

  cdsProduto.First ;

  iEtiqueta := 3 ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;
  rptRecebimento_Etiqueta_view.cdsEtiquetas.Open ;

  while not cdsProduto.Eof do
  begin

      PosicionaQuery(qryProduto, cdsProdutonCdProduto.AsString) ;

      qryAux.SQL.Clear ;
      qryAux.Close ;
      if (qryProdutonCdGrade.Value > 0) then
          qryAux.SQL.Add('SELECT RIGHT(' + Chr(39) + '0000000000' + Chr(39) + ' + RTRIM(cEAN),10), cNmItem, cReferencia, nQtde FROM ItemRecebimento INNER JOIN Produto ON Produto.nCdProduto = ItemRecebimento.nCdProduto WHERE ItemRecebimento.nCdItemRecebimentoPai = ' + cdsProdutonCdItemRecebimento.AsString + ' ORDER BY Produto.iTamanho') ;

      if (qryProdutonCdGrade.Value = 0) then
          qryAux.SQL.Add('SELECT RIGHT(' + Chr(39) + '0000000000' + Chr(39) + ' + RTRIM(cEAN),10), cNmItem, cReferencia, nQtde FROM ItemRecebimento INNER JOIN Produto ON Produto.nCdProduto = ItemRecebimento.nCdProduto WHERE ItemRecebimento.nCdItemRecebimento = ' + cdsProdutonCdItemRecebimento.AsString) ;
      qryAux.Open ;


      qryAux.First;

      while not qryAux.Eof do
      begin

          for iAux := 1 to qryAux.FieldList[3].Value do
          begin
          
              // coluna impar -- novo registro
              if (iEtiqueta mod 2) <> 0 then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetas.Insert ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra1.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto1.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia1.Value := qryAux.FieldList[2].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascMsg1.Value        := cMsgEtiqueta ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmLoja1.Value     := cNmLoja ;

              end ;

              // coluna par -- salva o registro
              if (iEtiqueta mod 2) = 0 then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra2.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto2.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia2.Value := qryAux.FieldList[2].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascMsg2.Value        := cMsgEtiqueta ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmLoja2.Value     := cNmLoja ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetas.Post ;

              end ;

              iEtiqueta := iEtiqueta + 1 ;
          end ;

          qryAux.Next ;


      end ;

      cdsProduto.Next ;
      
  end ;

  if (rptRecebimento_Etiqueta_view.cdsEtiquetas.State <> dsBrowse) then
      rptRecebimento_Etiqueta_view.cdsEtiquetas.Post ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.First ;

  rptRecebimento_Etiqueta_view.QuickRep1.PreviewModal;

  cdsProduto.First ;

  Close ;

end;

procedure TfrmRecebimento_Etiqueta.ToolButton7Click(Sender: TObject);
begin

  ShowMessage('Fun��o n�o implementada.') ;

end;

procedure TfrmRecebimento_Etiqueta.ToolButton8Click(Sender: TObject);
var
    iEtiqueta    : integer ;
    iAux         : integer ;
    cNmLoja      : string[40]  ;
    cMsgEtiqueta : string[40]  ;
begin
  inherited;

  cNmLoja      := frmMenu.LeParametro('MSGETIQNMLOJA') ;
  cMsgEtiqueta := frmMenu.LeParametro('MSGETIQPROD') ;

  if (cdsProduto.State <> dsBrowse) then
      cdsProduto.Post ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;

  cdsProduto.First ;

  iEtiqueta := 1 ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;
  rptRecebimento_Etiqueta_view.cdsEtiquetas.Open ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.First ;

  while not rptRecebimento_Etiqueta_view.cdsEtiquetas.eof do
  begin
      rptRecebimento_Etiqueta_view.cdsEtiquetas.Delete ;
      rptRecebimento_Etiqueta_view.cdsEtiquetas.First ;
  end ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;
  rptRecebimento_Etiqueta_view.cdsEtiquetas.Open ;

  while not cdsProduto.Eof do
  begin

      if (cdsProdutocFlgOK.Value = 1) then
      begin

      PosicionaQuery(qryProduto, cdsProdutonCdProduto.AsString) ;

      qryAux.SQL.Clear ;
      qryAux.Close ;
      if (qryProdutonCdGrade.Value > 0) then
      begin
          qryAux.SQL.Add('SELECT RIGHT(' + Chr(39) + '00000000' + Chr(39) + ' + CASE WHEN cEAN IS NULL THEN Convert(VARCHAR,Produto.nCdProduto) ELSE RTRIM(cEAN) END,8), cNmItem, cReferencia, nQtde, cNmTamanho FROM ItemRecebimento INNER JOIN Produto ON Produto.nCdProduto = ItemRecebimento.nCdProduto') ;
          qryAux.SQL.Add('INNER JOIN ItemGrade ON ItemGrade.nCdGrade = Produto.nCdGrade AND ItemGrade.iTamanho = Produto.iTamanho WHERE ItemRecebimento.nCdItemRecebimentoPai = ' + cdsProdutonCdItemRecebimento.AsString + ' ORDER BY Produto.nCdProduto') ;
      end ;

      if (qryProdutonCdGrade.Value = 0) then
      begin
          qryAux.SQL.Add('SELECT RIGHT(' + Chr(39) + '00000000' + Chr(39) + ' + CASE WHEN cEAN IS NULL THEN Convert(VARCHAR,Produto.nCdProduto) ELSE RTRIM(cEAN) END,8), cNmItem, cReferencia, nQtde, ' + Chr(39) + ' ' + Chr(39) + ' FROM ItemRecebimento INNER JOIN Produto ON Produto.nCdProduto = ItemRecebimento.nCdProduto') ;
          qryAux.SQL.Add(' WHERE ItemRecebimento.nCdItemRecebimento = ' + cdsProdutonCdItemRecebimento.AsString) ;
      end ;

      qryAux.Open ;

      qryAux.First;

      while not qryAux.Eof do
      begin

          for iAux := 1 to qryAux.FieldList[3].Value do
          begin

              if (iEtiqueta = 1) then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetas.Insert ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra1.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto1.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia1.Value := TRIM(qryAux.FieldList[2].Value) + ' - ' + TRIM(qryAux.FieldList[4].Value) ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascMsg1.Value        := cMsgEtiqueta ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmLoja1.Value     := cNmLoja ;

              end ;

              if (iEtiqueta = 2) then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra2.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto2.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia2.Value := TRIM(qryAux.FieldList[2].Value) + ' - ' + TRIM(qryAux.FieldList[4].Value) ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascMsg2.Value        := cMsgEtiqueta ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmLoja2.Value     := cNmLoja ;

              end ;

              if (iEtiqueta = 3) then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra3.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto3.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia3.Value := TRIM(qryAux.FieldList[2].Value) + ' - ' + TRIM(qryAux.FieldList[4].Value) ;

              end ;

              if (iEtiqueta = 4) then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra4.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto4.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia4.Value := TRIM(qryAux.FieldList[2].Value) + ' - ' + TRIM(qryAux.FieldList[4].Value) ;

              end ;

              if (iEtiqueta = 5) then
              begin

                   rptRecebimento_Etiqueta_view.cdsEtiquetascCdBarra5.Value    := qryAux.FieldList[0].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascNmProduto5.Value  := qryAux.FieldList[1].Value ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetascReferencia5.Value := TRIM(qryAux.FieldList[2].Value) + ' - ' + TRIM(qryAux.FieldList[4].Value) ;
                   rptRecebimento_Etiqueta_view.cdsEtiquetas.Post ;

                   iEtiqueta := 0 ;

              end ;

              iEtiqueta := iEtiqueta + 1 ;
          end ;

          qryAux.Next ;


      end ;

      end ;

      cdsProduto.Next ;

  end ;

  if (rptRecebimento_Etiqueta_view.cdsEtiquetas.State <> dsBrowse) then
      rptRecebimento_Etiqueta_view.cdsEtiquetas.Post ;

  rptRecebimento_Etiqueta_view.cdsEtiquetas.Close ;
  rptRecebimento_Etiqueta_view.cdsEtiquetas.Open ;
      
  rptRecebimento_Etiqueta_view.cdsEtiquetas.First ;

  rptRecebimento_Etiqueta_view.QuickRep1.PreviewModal;

  cdsProduto.First ;

  Close ;

end;

end.
