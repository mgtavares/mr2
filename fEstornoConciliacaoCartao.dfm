inherited frmEstornoConciliacaoCartao: TfrmEstornoConciliacaoCartao
  Left = 235
  Top = 183
  Caption = 'Estorno Concilia'#231#227'o Cart'#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 101
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 101
    end
    inherited ToolButton2: TToolButton
      Left = 109
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsLoteConciliacao
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1nCdLoteConciliacaoCartao: TcxGridDBColumn
        Caption = 'Lote'
        DataBinding.FieldName = 'nCdLoteConciliacaoCartao'
      end
      object cxGrid1DBTableView1nCdOperadoraCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdOperadoraCartao'
        Visible = False
      end
      object cxGrid1DBTableView1cNmOperadoraCartao: TcxGridDBColumn
        Caption = 'Operadora Cart'#227'o'
        DataBinding.FieldName = 'cNmOperadoraCartao'
      end
      object cxGrid1DBTableView1dDtConciliacaoCartao: TcxGridDBColumn
        Caption = 'Data Concilia'#231#227'o'
        DataBinding.FieldName = 'dDtConciliacaoCartao'
        Width = 124
      end
      object cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn
        DataBinding.FieldName = 'nCdUsuario'
        Visible = False
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio'
        DataBinding.FieldName = 'cNmUsuario'
      end
      object cxGrid1DBTableView1nValLote: TcxGridDBColumn
        Caption = 'Valor Lote'
        DataBinding.FieldName = 'nValLote'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryLoteConciliacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtConciliacao'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtConciliacao datetime'
      '       ,@dDtInicial     datetime'
      '       ,@iDias          int'
      ''
      'SELECT @iDias = Convert(int,cValor)'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'DIASESTCONCAR'#39
      ''
      'Set @dDtConciliacao = Convert(DATETIME,:dDtConciliacao,103)'
      'Set @dDtInicial     = @dDtConciliacao - @iDias'
      ''
      'SELECT nCdLoteConciliacaoCartao '
      '      ,OperadoraCartao.nCdOperadoraCartao '
      '      ,OperadoraCartao.cNmOperadoraCartao'
      '      ,dDtConciliacaoCartao    '
      '      ,Usuario.nCdUsuario  '
      '      ,Usuario.cNmUsuario'
      '      ,nValLote                                '
      '  FROM LoteConciliacaoCartao'
      
        '       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = LoteConciliacaoCartao.nCdOperadoraCartao'
      
        '       INNER JOIN Usuario         ON Usuario.nCdUsuario         ' +
        '        = LoteConciliacaoCartao.nCdUsuario'
      ' WHERE nCdEmpresa            = :nCdEmpresa'
      '   AND dDtConciliacaoCartao >=  @dDtInicial'
      '   AND dDtConciliacaoCartao <  (@dDtConciliacao+1)'
      '   AND dDtEstorno IS NULL'
      '')
    Left = 264
    Top = 184
    object qryLoteConciliacaonCdLoteConciliacaoCartao: TAutoIncField
      FieldName = 'nCdLoteConciliacaoCartao'
      ReadOnly = True
    end
    object qryLoteConciliacaonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryLoteConciliacaocNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryLoteConciliacaodDtConciliacaoCartao: TDateTimeField
      FieldName = 'dDtConciliacaoCartao'
    end
    object qryLoteConciliacaonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryLoteConciliacaocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLoteConciliacaonValLote: TBCDField
      FieldName = 'nValLote'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsLoteConciliacao: TDataSource
    DataSet = qryLoteConciliacao
    Left = 304
    Top = 184
  end
  object SP_ESTORNA_CONCILIACAO_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ESTORNA_CONCILIACAO_CARTAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoteConciliacaoCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 168
  end
end
