unit fContaContabil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmContaContabil = class(TfrmCadastro_Padrao)
    qryMasternCdPlanoConta: TIntegerField;
    qryMastercNmPlanoConta: TStringField;
    qryMasternCdGrupoPlanoConta: TIntegerField;
    qryMastercFlgRecDes: TStringField;
    qryMastercMascara: TStringField;
    qryMastercFlgLanc: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgExibeDRE: TIntegerField;
    qryMastercFlgTransfRecurso: TIntegerField;
    qryMastercNmPlanoContaSegundoIdioma: TStringField;
    qryMastercCdCtaRefSPED: TStringField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryMasterdDtInativacao: TDateTimeField;
    qryMastercFlgExigeCC: TIntegerField;
    qryMastercFlgExigeUnidadeNegocio: TIntegerField;
    qryMastercFlgGeraContabAux: TIntegerField;
    qryMastercFlgNaturezaCredDev: TStringField;
    qryMastercFlgOrcamento: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryMastercFlgContaIntegracao: TIntegerField;
    dsTelefone: TDataSource;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    DBRadioGroup5: TDBRadioGroup;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMastercExpClassificacao: TMemoField;
    Label7: TLabel;
    DBMemo1: TDBMemo;
    DBRadioGroup6: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContaContabil: TfrmContaContabil;

implementation

uses fContaContabil_Vinculacoes ;

{$R *.dfm}


procedure TfrmContaContabil.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PLANOCONTA' ;
  nCdTabelaSistema  := 63 ;
  nCdConsultaPadrao := 218 ;

end;

procedure TfrmContaContabil.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmContaContabil.qryMasterBeforePost(DataSet: TDataSet);
begin

  if ( trim(qryMastercNmPlanoConta.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o da conta.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  if (qryMastercFlgNaturezaCredDev.Value = '') then
  begin
      MensagemAlerta('Selecione a natureza da conta.') ;
      abort;
  end ;

  if ( trim(qryMastercNmPlanoContaSegundoIdioma.Value) = '') then
      qryMastercNmPlanoContaSegundoIdioma.Value := qryMastercNmPlanoConta.Value ;

  qryMastercFlgRecDes.Value := qryMastercFlgNaturezaCredDev.Value ;
  qryMasterdDtUltAlt.Value  := Now() ;

  if (qryMaster.State = dsInsert) then
      qryMasterdDtCad.Value := Now() ;
      
  inherited;

end;

procedure TfrmContaContabil.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmContaContabil_Vinculacoes;
begin
  inherited;

  if (qryMaster.isEmpty) or (qryMasternCdPlanoConta.Value = 0) then
      exit ;

  objForm := TfrmContaContabil_Vinculacoes.Create( Self ) ;
  objForm.exibeVinculacoes( qryMasternCdPlanoConta.Value ) ; 

end;

initialization
    RegisterClass(TfrmContaContabil) ;
    
end.
