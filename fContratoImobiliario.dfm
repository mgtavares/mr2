inherited frmContratoImobiliario: TfrmContratoImobiliario
  Left = -8
  Top = -8
  Width = 1152
  Height = 850
  Caption = 'Contrato Imobiliario'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 789
  end
  object Label1: TLabel [1]
    Left = 85
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 80
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 87
    Top = 86
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 35
    Top = 110
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empreendimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 14
    Top = 134
    Width = 109
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Casa/Apto/Lote'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 47
    Top = 158
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Contrato'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 331
    Top = 158
    Width = 73
    Height = 13
    Caption = 'Data Contrato'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 500
    Top = 158
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total Contrato'
    FocusControl = DBEdit8
  end
  object Label21: TLabel [9]
    Left = 45
    Top = 182
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = #205'ndice Reajuste'
    FocusControl = DBEdit21
  end
  inherited ToolBar2: TToolBar
    Width = 1136
    ButtonWidth = 96
    inherited ToolButton7: TToolButton
      Left = 96
    end
    inherited ToolButton1: TToolButton
      Left = 104
    end
    inherited btAlterar: TToolButton
      Left = 200
    end
    inherited ToolButton2: TToolButton
      Left = 296
    end
    inherited btSalvar: TToolButton
      Left = 300
    end
    inherited ToolButton6: TToolButton
      Left = 396
    end
    inherited btExcluir: TToolButton
      Left = 404
    end
    inherited ToolButton50: TToolButton
      Left = 500
    end
    inherited btCancelar: TToolButton
      Left = 508
    end
    inherited ToolButton11: TToolButton
      Left = 604
    end
    inherited btConsultar: TToolButton
      Left = 612
    end
    inherited ToolButton8: TToolButton
      Left = 708
    end
    inherited ToolButton5: TToolButton
      Left = 716
    end
    inherited ToolButton4: TToolButton
      Left = 812
    end
    inherited ToolButton9: TToolButton
      Left = 820
    end
    inherited btnAuditoria: TToolButton
      Left = 828
    end
    object ToolButton3: TToolButton
      Left = 924
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 932
      Top = 0
      Caption = 'Gerar Contrato'
      ImageIndex = 8
      OnClick = ToolButton10Click
    end
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 128
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdContratoEmpImobiliario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 128
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [13]
    Left = 128
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [14]
    Left = 128
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdBlocoEmpImobiliario'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [15]
    Left = 128
    Top = 128
    Width = 65
    Height = 19
    DataField = 'cNrUnidade'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [16]
    Left = 128
    Top = 152
    Width = 195
    Height = 19
    DataField = 'cNumContrato'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [17]
    Left = 408
    Top = 152
    Width = 73
    Height = 19
    DataField = 'dDtContrato'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [18]
    Left = 608
    Top = 152
    Width = 169
    Height = 19
    DataField = 'nValContrato'
    DataSource = dsMaster
    TabOrder = 8
  end
  object GroupBox1: TGroupBox [19]
    Left = 16
    Top = 216
    Width = 241
    Height = 121
    Caption = 'Sinal'
    TabOrder = 11
    object Label17: TLabel
      Left = 26
      Top = 30
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Vencto'
      FocusControl = DBEdit17
    end
    object Label16: TLabel
      Left = 17
      Top = 62
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor do Sinal'
      FocusControl = DBEdit16
    end
    object DBEdit17: TDBEdit
      Left = 96
      Top = 24
      Width = 73
      Height = 19
      DataField = 'dDtVenctoSinal'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit16: TDBEdit
      Left = 96
      Top = 56
      Width = 121
      Height = 19
      DataField = 'nValSinal'
      DataSource = dsMaster
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox [20]
    Left = 264
    Top = 216
    Width = 513
    Height = 121
    Caption = 'Parcelas'
    TabOrder = 12
    object Label11: TLabel
      Left = 15
      Top = 30
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor das Parcelas'
      FocusControl = DBEdit11
    end
    object Label9: TLabel
      Left = 8
      Top = 62
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Total Parcelas'
      FocusControl = DBEdit9
    end
    object Label10: TLabel
      Left = 8
      Top = 94
      Width = 99
      Height = 13
      Caption = 'N'#250'm. Parcela Inicial'
      FocusControl = DBEdit10
    end
    object Label13: TLabel
      Left = 296
      Top = 30
      Width = 115
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Vencto 1'#170' Parcela'
      FocusControl = DBEdit13
    end
    object Label15: TLabel
      Left = 330
      Top = 62
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Dia Vencimento'
      FocusControl = DBEdit15
    end
    object Label14: TLabel
      Left = 187
      Top = 46
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Caption = 'Dias entre Parcelas'
      FocusControl = DBEdit14
      Visible = False
    end
    object Label12: TLabel
      Left = 306
      Top = 92
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Parcela Chaves'
      FocusControl = DBEdit12
    end
    object DBEdit11: TDBEdit
      Left = 112
      Top = 24
      Width = 169
      Height = 19
      DataField = 'nValParcela'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit9: TDBEdit
      Left = 112
      Top = 56
      Width = 65
      Height = 19
      DataField = 'iParcelas'
      DataSource = dsMaster
      TabOrder = 2
    end
    object DBEdit10: TDBEdit
      Left = 112
      Top = 88
      Width = 65
      Height = 19
      DataField = 'iParcelaInicial'
      DataSource = dsMaster
      TabOrder = 4
    end
    object DBEdit13: TDBEdit
      Left = 416
      Top = 24
      Width = 73
      Height = 19
      DataField = 'dDtVenctoPrimParc'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit15: TDBEdit
      Left = 416
      Top = 56
      Width = 65
      Height = 19
      DataField = 'iDiaVencto'
      DataSource = dsMaster
      TabOrder = 3
      OnEnter = DBEdit15Enter
    end
    object DBEdit14: TDBEdit
      Left = 224
      Top = 56
      Width = 65
      Height = 19
      DataField = 'iDiasProxParc'
      DataSource = dsMaster
      TabOrder = 6
      Visible = False
    end
    object DBEdit12: TDBEdit
      Left = 416
      Top = 86
      Width = 65
      Height = 19
      DataField = 'iParcelaChaves'
      DataSource = dsMaster
      TabOrder = 5
    end
  end
  object DBEdit22: TDBEdit [21]
    Tag = 1
    Left = 200
    Top = 56
    Width = 577
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 14
  end
  object DBEdit23: TDBEdit [22]
    Tag = 1
    Left = 200
    Top = 80
    Width = 97
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = DataSource2
    TabOrder = 15
  end
  object DBEdit24: TDBEdit [23]
    Tag = 1
    Left = 300
    Top = 80
    Width = 477
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 16
  end
  object DBEdit25: TDBEdit [24]
    Tag = 1
    Left = 200
    Top = 104
    Width = 577
    Height = 19
    DataField = 'cNmBlocoEmpImobiliario'
    DataSource = DataSource3
    TabOrder = 17
  end
  object GroupBox3: TGroupBox [25]
    Left = 784
    Top = 216
    Width = 289
    Height = 121
    Caption = 'Outras Informa'#231#245'es'
    TabOrder = 18
    object Label20: TLabel
      Left = 7
      Top = 28
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Entrega Chaves'
      FocusControl = DBEdit20
    end
    object Label19: TLabel
      Left = 49
      Top = 60
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Cancel.'
      FocusControl = DBEdit19
    end
    object Label18: TLabel
      Left = 44
      Top = 92
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Gera'#231#227'o'
      FocusControl = DBEdit18
    end
    object DBEdit20: TDBEdit
      Tag = 1
      Left = 120
      Top = 22
      Width = 153
      Height = 19
      DataField = 'dDtChaves'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit19: TDBEdit
      Tag = 1
      Left = 120
      Top = 54
      Width = 153
      Height = 19
      DataField = 'dDtCancel'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit18: TDBEdit
      Tag = 1
      Left = 120
      Top = 86
      Width = 153
      Height = 19
      DataField = 'dDtGeracao'
      DataSource = dsMaster
      TabOrder = 2
    end
  end
  object pageParcelasIntermediarias: TcxPageControl [26]
    Left = 16
    Top = 400
    Width = 417
    Height = 249
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 19
    ClientRectBottom = 245
    ClientRectLeft = 4
    ClientRectRight = 413
    ClientRectTop = 24
    object pageSinal: TcxTabSheet
      Caption = 'Sinal'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 409
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsSinais
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu2
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 125
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object pageIntermediaria: TcxTabSheet
      Caption = 'Intermedi'#225'rias'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 409
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsIntermediarias
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu2
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 125
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxAnual: TcxTabSheet
      Caption = 'Anuais'
      ImageIndex = 2
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 409
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsAnuais
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu2
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 125
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxChaves: TcxTabSheet
      Caption = 'Chaves'
      ImageIndex = 3
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 0
        Width = 409
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsChaves
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu2
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 125
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'Financiamento Banc'#225'rio'
      ImageIndex = 4
      object DBGridEh6: TDBGridEh
        Left = 0
        Top = 0
        Width = 409
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsFinBancario
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu2
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
            Width = 111
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 125
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object btnGerarParcela: TcxButton [27]
    Left = 16
    Top = 352
    Width = 113
    Height = 33
    Caption = 'Gerar Parcelas'
    TabOrder = 13
    OnClick = btnGerarParcelaClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000C4C4C4E0E0E0E6E6E6DEDEDECFCFCFADADAD0000000000
      00000000000000000000000000000000000000B7B7B7F3F4F3ADCFB565B77C43
      AE6540AC6454AB717CA98AADB0AEC0C0C0000000000000000000000000000000
      CECECED9E7DD40B26317B14B409C612FAB6321C46B22C56C21C3692CAE629CBA
      A7D8D8D8000000000000000000000000E0EAE223B45310BD527EA68FD5D5D5C7
      CCCA39B5811AD1861AD18418CF7F19BF6CB6D4C3000000000000000000ECECEC
      55BA7408BD4A7BAA8EE6E6E6E7E7E7DFDFDFC1C9C62BB78412D18A11D0850FCD
      792AB86DF2F4F3000000000000E0E9E206AB3A67A77EE3E3E3E3E4E468CDA0B4
      DBCBE9E9E9C4D0CC24B57E0DC77C0BC47009BD5EAED3BB000000000000BFD7C5
      21B24DC4DCCBF6F6F676C99B08B66016B86FB9D6C9E6E6E6BECDC71AAA6908B6
      5E06B2517CBF94000000000000C2D7C75ABE7634B05949B66C04A13F05A44806
      A65018A85EC3D7CCE5E5E5B2C5BB11994A04A13E76B78A000000000000D9DFDA
      77BE8870BF844CB06812973B02913303923803933B209A50D5E1D9E9E9E9A8BD
      AF0A893097BCA1000000000000000000A4C9AC8BC59884C1927BBD8B54AB6B28
      964708862D0183282B9349E6EEE8E7ECE829833E000000000000000000000000
      D6DCD8A6CCAD9DCAA697C7A190C39B89C09581BB8E69AF7A55A46872B28074B0
      80A7C1AC000000000000000000000000000000DAE1DBB8D4BDAED2B5A8CFB0A2
      CCAA9CC8A595C59E8EC1988ABB93BFD2C2000000000000000000000000000000
      000000000000E7E8E7DCE8DEC6DDCAB8D5BEB3D2B9B7D4BCC8DCCBE0E4E10000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
  end
  object cxPageControl2: TcxPageControl [28]
    Left = 448
    Top = 400
    Width = 625
    Height = 249
    ActivePage = cxTabSheet4
    LookAndFeel.NativeStyle = True
    TabOrder = 20
    ClientRectBottom = 245
    ClientRectLeft = 4
    ClientRectRight = 621
    ClientRectTop = 24
    object cxTabSheet4: TcxTabSheet
      Caption = 'Parcelas'
      ImageIndex = 1
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 617
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsParcelas
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        PopupMenu = PopupMenu1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'iParcelaTotal'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencto'
            Footers = <>
            Width = 98
          end
          item
            EditButtons = <>
            FieldName = 'nValorParcela'
            Footers = <>
            Width = 139
          end
          item
            EditButtons = <>
            FieldName = 'cNmtabTipoParcContrato'
            Footers = <>
            Width = 199
          end
          item
            EditButtons = <>
            FieldName = 'cFlgLiquidada'
            Footers = <>
            Width = 70
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object btCancelarContrato: TcxButton [29]
    Left = 16
    Top = 664
    Width = 129
    Height = 33
    Caption = 'Cancelar Contrato'
    TabOrder = 21
    OnClick = btCancelarContratoClick
    Glyph.Data = {
      32040000424D3204000000000000360000002800000013000000110000000100
      180000000000FC03000000000000000000000000000000000000FAFBFFFFFBFC
      FFFAF6FFFDF5FFFFF5FFFFF4FFFFF4FFFDF1FFFDF1FFFFF5FFFFF7FFFAF4FFF8
      F0FFF6E8FFFDEDFFFBEFFFF5EAFFF6F0FFFEFC000000FFFEFFFFFBF7F1E0D7E5
      D0C1EBD1C0F9DCC7EAC9B5F4D1BDFDD8C4EBC5B3E7C3B1F9D5C5FDD9C7FFE2CE
      FFE1CBF3D3C0E3C6B7F0D7CDFFFAF3000000FFFFFBFFFAF1DFC8B9B193809B77
      5FB0886CA17558A97A5EB9896DB7876BB6856BB7866CAD7C62A7785DA0725A9E
      755EB69280E8CBBDFFFBF3000000FFFFF7FFFAEBE8CBB69F7A60623617572705
      743E1B6E34106327037438147437156629076A2F0F743D1E582406623318A97E
      69F6D4C4FFFDF2000000FFF9F0FFFAEAFDDDC6AA826655240467310EC28760AD
      7048692B036A2B05692B05753713B67A56B17551703815643111B4876CFFDDC9
      FFF7E9000000FFFFFBFFFFF4F4D6C5A57C635C2A0C6F3616C88C68FFCCA8CD93
      6F602805592501A67352E6B28EBA7D556529006A3510AA7C5DEAC8B0FFFFF000
      0000FFFFFCFFFFF4F6D5C5A77B635E2A0C6329067A3C18C48962FCC39CB47F5A
      C69470DEB08EB8866285471E75360A733C15B18463FAD8C0FFFDEB000000FFFF
      F9FFFFF1F9D6C2AB7C60612B0A8A4F2871330B6C2C03CA8C63D2976FE2AB84B4
      805B622E066E300784441B6C3510A87A5BFFDFC7FFFDED000000FFFFF7FFFFF0
      FDD7BFAC7D5E632C0775370F7031056E2D01C48559EBAE82E9AF85B68057672F
      0665270070310B6E3515AA7B5FF8D6BFFFFDEE000000FFFFF5FFFFEFFFD7BEAE
      7D5D632C076224007D3C0FC98659E6A578CA8C5EC68B5EFBC295D2986E74350F
      5617007A4225B98B73F0CCBAFFFBEC000000FFFFF5FFFFEFFDD7BFAB7D5E612B
      0882451DC38458FFC397B7794B7C3F136F3407CB9469FFC89FB87B596024066E
      381FAF826DF0CEBEFFFFF4000000FFFFF7FFFFF1F7D6C2A57C635A2B0B6B3512
      B47B54D1966E7A3F17713910632D046E3A12B9845FBC83646E361D5826109E73
      62F8D6C9FFFEF7000000FFFFFBFFFFF4F0D6C69E7B67532A1150210551200067
      3413461300582806744624491C005526075C290F5D2A16532513A47D6FFCDED3
      FFEFE9000000FFFEFEFFFBF8DCC7BFA88D7F9A7969A3806CA78169A98069A981
      68A98168A78268A58268A78169A37865A87B6DA67F71B39086DFC3BCFFF9F400
      0000FFFEFFFFFEFEF5E6E3E0CDC6E3CDC2EAD0C2EBD1C1EDD0C1EDD1C0EDD1C0
      EDD1C0EBD1C1EDD0C1F3D0C3F4D0C6EDCCC3E9CDC6F7E1DCFFFBFA000000FAFA
      FFFFFDFFFFF8F8FFF7F3FFFFF9FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FF
      FFF8FFFFF7FFFEF7FFFEF7FFFEF8FFFAF5FFF8F7FFFDFD000000F7FAFFFDFCFE
      FEFCFCFFFCFBFFFFFCFFFFFCFFFFFBFFFFFBFFFFFBFFFFFCFFFFFCFFFFFCFFFE
      FCFFFCF9FFFDFAFFFEFBFFFEFCFFFDFDFFFAFB000000}
    LookAndFeel.NativeStyle = True
  end
  object cxButton1: TcxButton [30]
    Left = 152
    Top = 664
    Width = 129
    Height = 33
    Caption = 'Extrato'
    TabOrder = 22
    OnClick = cxButton1Click
    Glyph.Data = {
      0A020000424D0A0200000000000036000000280000000C0000000D0000000100
      180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
      656464656464656464656464656464656464656464656464898A89B9BABAD5D5
      D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
      C6C68C908F191D23313332313332313332313332313332313332313332191D23
      040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
      BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
      85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
      A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
      FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
      FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
      C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
      CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
      C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
      939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
      23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
      A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
    LookAndFeel.NativeStyle = True
  end
  object cxButton2: TcxButton [31]
    Left = 288
    Top = 664
    Width = 129
    Height = 33
    Caption = '(destivado) Extrato Manual'
    TabOrder = 23
    Visible = False
    OnClick = cxButton2Click
    Glyph.Data = {
      0A020000424D0A0200000000000036000000280000000C0000000D0000000100
      180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
      656464656464656464656464656464656464656464656464898A89B9BABAD5D5
      D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
      C6C68C908F191D23313332313332313332313332313332313332313332191D23
      040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
      BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
      85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
      A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
      FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
      FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
      C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
      CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
      C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
      939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
      23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
      A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit21: TDBEdit [32]
    Left = 128
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdIndiceReajuste'
    DataSource = dsMaster
    TabOrder = 9
    OnExit = DBEdit21Exit
    OnKeyDown = DBEdit21KeyDown
  end
  object DBEdit26: TDBEdit [33]
    Tag = 1
    Left = 200
    Top = 176
    Width = 577
    Height = 19
    DataField = 'cNmIndiceReajuste'
    DataSource = DataSource4
    TabOrder = 10
  end
  object cxButton3: TcxButton [34]
    Left = 944
    Top = 664
    Width = 129
    Height = 33
    Caption = '(destivado) Ver Todas Parcelas'
    TabOrder = 24
    Visible = False
    OnClick = cxButton3Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object cxButton4: TcxButton [35]
    Left = 136
    Top = 352
    Width = 113
    Height = 33
    Caption = 'Impress'#227'o Ficha'
    TabOrder = 25
    OnClick = cxButton4Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000C4C4C4E0E0E0E6E6E6DEDEDECFCFCFADADAD0000000000
      00000000000000000000000000000000000000B7B7B7F3F4F3ADCFB565B77C43
      AE6540AC6454AB717CA98AADB0AEC0C0C0000000000000000000000000000000
      CECECED9E7DD40B26317B14B409C612FAB6321C46B22C56C21C3692CAE629CBA
      A7D8D8D8000000000000000000000000E0EAE223B45310BD527EA68FD5D5D5C7
      CCCA39B5811AD1861AD18418CF7F19BF6CB6D4C3000000000000000000ECECEC
      55BA7408BD4A7BAA8EE6E6E6E7E7E7DFDFDFC1C9C62BB78412D18A11D0850FCD
      792AB86DF2F4F3000000000000E0E9E206AB3A67A77EE3E3E3E3E4E468CDA0B4
      DBCBE9E9E9C4D0CC24B57E0DC77C0BC47009BD5EAED3BB000000000000BFD7C5
      21B24DC4DCCBF6F6F676C99B08B66016B86FB9D6C9E6E6E6BECDC71AAA6908B6
      5E06B2517CBF94000000000000C2D7C75ABE7634B05949B66C04A13F05A44806
      A65018A85EC3D7CCE5E5E5B2C5BB11994A04A13E76B78A000000000000D9DFDA
      77BE8870BF844CB06812973B02913303923803933B209A50D5E1D9E9E9E9A8BD
      AF0A893097BCA1000000000000000000A4C9AC8BC59884C1927BBD8B54AB6B28
      964708862D0183282B9349E6EEE8E7ECE829833E000000000000000000000000
      D6DCD8A6CCAD9DCAA697C7A190C39B89C09581BB8E69AF7A55A46872B28074B0
      80A7C1AC000000000000000000000000000000DAE1DBB8D4BDAED2B5A8CFB0A2
      CCAA9CC8A595C59E8EC1988ABB93BFD2C2000000000000000000000000000000
      000000000000E7E8E7DCE8DEC6DDCAB8D5BEB3D2B9B7D4BCC8DCCBE0E4E10000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContratoEmpImobiliario'
      'WHERE nCdContratoEmpImobiliario = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 944
    Top = 112
    object qryMasternCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercNumContrato: TStringField
      FieldName = 'cNumContrato'
      Size = 15
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdBlocoEmpImobiliario: TIntegerField
      FieldName = 'nCdBlocoEmpImobiliario'
    end
    object qryMastercNrUnidade: TStringField
      FieldName = 'cNrUnidade'
      Size = 15
    end
    object qryMasterdDtContrato: TDateTimeField
      FieldName = 'dDtContrato'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtChaves: TDateTimeField
      FieldName = 'dDtChaves'
    end
    object qryMasterdDtQuitacao: TIntegerField
      FieldName = 'dDtQuitacao'
    end
    object qryMasternValContrato: TBCDField
      FieldName = 'nValContrato'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryMasteriParcelaInicial: TIntegerField
      FieldName = 'iParcelaInicial'
    end
    object qryMasternValParcela: TBCDField
      FieldName = 'nValParcela'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriParcelaChaves: TIntegerField
      FieldName = 'iParcelaChaves'
    end
    object qryMasterdDtVenctoPrimParc: TDateTimeField
      FieldName = 'dDtVenctoPrimParc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasteriDiasProxParc: TIntegerField
      FieldName = 'iDiasProxParc'
    end
    object qryMasteriDiaVencto: TIntegerField
      FieldName = 'iDiaVencto'
    end
    object qryMasternValSinal: TBCDField
      FieldName = 'nValSinal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtVenctoSinal: TDateTimeField
      FieldName = 'dDtVenctoSinal'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMasterdDtGeracao: TDateTimeField
      FieldName = 'dDtGeracao'
    end
    object qryMasternCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
  end
  inherited dsMaster: TDataSource
    Left = 816
    Top = 80
  end
  inherited qryID: TADOQuery
    Left = 912
    Top = 112
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 784
    Top = 48
  end
  inherited qryStat: TADOQuery
    Left = 816
    Top = 112
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 784
    Top = 80
  end
  inherited ImageList1: TImageList
    Left = 848
    Top = 80
    Bitmap = {
      494C010109000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000FFFF000000000000
      C003000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      C003000000000000FFFF000000000000FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 848
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 464
    Top = 280
  end
  object qryCliente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cCNPJCPF'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM TerceiroTipoTerceiro TTT '
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 2)')
    Left = 784
    Top = 112
    object qryClientenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryClientecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryClientecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCliente
    Left = 544
    Top = 272
  end
  object qryBloco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdBlocoEmpImobiliario'
      
        '      ,RTRIM(cNmEmpImobiliario) + '#39' - '#39' + cNmBlocoEmpImobiliario' +
        ' as cNmBlocoEmpImobiliario'
      '  FROM BlocoEmpImobiliario'
      
        '       INNER JOIN EmpImobiliario ON EmpImobiliario.nCdEmpImobili' +
        'ario = BlocoEmpImobiliario.nCdEmpImobiliario'
      ' WHERE nCdBlocoEmpImobiliario = :nPK')
    Left = 880
    Top = 112
    object qryBloconCdBlocoEmpImobiliario: TAutoIncField
      FieldName = 'nCdBlocoEmpImobiliario'
      ReadOnly = True
    end
    object qryBlococNmBlocoEmpImobiliario: TStringField
      FieldName = 'cNmBlocoEmpImobiliario'
      ReadOnly = True
      Size = 153
    end
  end
  object DataSource3: TDataSource
    DataSet = qryBloco
    Left = 504
    Top = 280
  end
  object qryIntermediarias: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryIntermediariasBeforePost
    OnCalcFields = qryIntermediariasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 3 --Intermediaria')
    Left = 264
    Top = 360
    object qryIntermediariasnCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryIntermediariasnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryIntermediariasnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryIntermediariasiParcela: TIntegerField
      DisplayLabel = 'Parcelas Intermedi'#225'rias|Parcela'
      FieldName = 'iParcela'
    end
    object qryIntermediariasiParcelaTotal: TIntegerField
      DisplayLabel = 'Parcelas Intermedi'#225'rias|'
      FieldName = 'iParcelaTotal'
    end
    object qryIntermediariasdDtVencto: TDateTimeField
      DisplayLabel = 'Parcelas Intermedi'#225'rias|Data Vencimento'
      FieldName = 'dDtVencto'
      EditMask = '!99/99/9999;1;_'
    end
    object qryIntermediariasnValorParcela: TBCDField
      DisplayLabel = 'Parcelas Intermedi'#225'rias|Valor da Parcela'
      FieldName = 'nValorParcela'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryIntermediariascFlgLiquidada: TStringField
      DisplayLabel = 'Parcelas Intermedi'#225'rias|Liquidada'
      FieldKind = fkCalculated
      FieldName = 'cFlgLiquidada'
      Size = 3
      Calculated = True
    end
  end
  object dsIntermediarias: TDataSource
    DataSet = qryIntermediarias
    Left = 296
    Top = 360
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryParcelasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 2 --Parcelas'
      'ORDER BY dDtVencto')
    Left = 976
    Top = 192
    object qryParcelasnCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryParcelasnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryParcelasnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryParcelasiParcela: TIntegerField
      DisplayLabel = 'Parcelas Financiamento|Parcela'
      FieldName = 'iParcela'
    end
    object qryParcelasiParcelaTotal: TIntegerField
      DisplayLabel = 'Parcelas Financiamento|'
      FieldName = 'iParcelaTotal'
    end
    object qryParcelasdDtVencto: TDateTimeField
      DisplayLabel = 'Parcelas Financiamento|Data Vencimento'
      FieldName = 'dDtVencto'
    end
    object qryParcelasnValorParcela: TBCDField
      DisplayLabel = 'Parcelas Financiamento|Valor da Parcela'
      FieldName = 'nValorParcela'
      Precision = 12
      Size = 2
    end
    object qryParcelascFlgLiquidada: TStringField
      DisplayLabel = 'Parcelas Financiamento|Liquidada'
      FieldKind = fkCalculated
      FieldName = 'cFlgLiquidada'
      Size = 3
      Calculated = True
    end
    object qryParcelascNmtabTipoParcContrato: TStringField
      DisplayLabel = 'Parcelas Financiamento|Tipo de Parcelas'
      FieldKind = fkCalculated
      FieldName = 'cNmtabTipoParcContrato'
      Size = 50
      Calculated = True
    end
  end
  object dsParcelas: TDataSource
    DataSet = qryParcelas
    Left = 1016
    Top = 192
  end
  object qryConsultaSaldoTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nSaldoTit'
      'FROM Titulo'
      'WHERE nCdParcContratoEmpImobiliario = :nPK')
    Left = 1072
    Top = 160
    object qryConsultaSaldoTitnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 976
    Top = 392
  end
  object SP_GERA_CONTRATO_IMOBILIARIO: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_GERA_CONTRATO_IMOBILIARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContratoEmpImobiliario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 1056
    Top = 392
  end
  object SP_CANCELA_CONTRATO_IMOBILIARIO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_CONTRATO_IMOBILIARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContratoEmpImobiliario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 848
    Top = 376
  end
  object qryTabTipoParcContratoEmpImobiliario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TabTipoParcContratoEmpImobiliario'
      'WHERE nCdTabTipoParcContratoEmpImobiliario = :nPK')
    Left = 768
    Top = 368
    object qryTabTipoParcContratoEmpImobiliarionCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryTabTipoParcContratoEmpImobiliariocNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryIndiceReajuste
    Left = 704
    Top = 424
  end
  object qryIndiceReajuste: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Indicereajuste'
      'WHERE nCdIndiceReajuste = :nPK')
    Left = 1096
    Top = 240
    object qryIndiceReajustenCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryIndiceReajustecNmIndiceReajuste: TStringField
      FieldName = 'cNmIndiceReajuste'
      Size = 50
    end
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    Left = 772
    Top = 528
    object ImprimeRecibo: TMenuItem
      Caption = 'Imprime Recibo'
      OnClick = ImprimeReciboClick
    end
  end
  object qryConsultaParcelaTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTitulo, nCdEspTit'
      'FROM Titulo'
      'WHERE nCdParcContratoEmpImobiliario = :nPK'
      '')
    Left = 1072
    Top = 128
    object qryConsultaParcelaTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryConsultaParcelaTitulonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 228
    Top = 528
    object ImprimeRecibo1: TMenuItem
      Caption = 'Imprime Recibo'
      OnClick = ImprimeRecibo1Click
    end
  end
  object qrySinais: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySinaisBeforePost
    OnCalcFields = qryIntermediariasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 1 --Sinal')
    Left = 352
    Top = 360
    object qrySinaisnCdParcContratoEmpImobiliario: TAutoIncField
      FieldName = 'nCdParcContratoEmpImobiliario'
      ReadOnly = True
    end
    object qrySinaisnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qrySinaisnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qrySinaisiParcela: TIntegerField
      DisplayLabel = 'Parcelas Sinal|Parcela'
      FieldName = 'iParcela'
    end
    object qrySinaisiParcelaTotal: TIntegerField
      DisplayLabel = 'Parcelas Sinal|Parcela'
      FieldName = 'iParcelaTotal'
    end
    object qrySinaisdDtVencto: TDateTimeField
      DisplayLabel = 'Parcelas Sinal|Data Vencimento'
      FieldName = 'dDtVencto'
    end
    object qrySinaisnValorParcela: TBCDField
      DisplayLabel = 'Parcelas Sinal|Valor da Parcela'
      FieldName = 'nValorParcela'
      Precision = 12
      Size = 2
    end
  end
  object dsSinais: TDataSource
    DataSet = qrySinais
    Left = 384
    Top = 360
  end
  object qryAnuais: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryAnuaisBeforePost
    OnCalcFields = qryIntermediariasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 5 --Anuais')
    Left = 424
    Top = 360
    object qryAnuaisnCdParcContratoEmpImobiliario: TAutoIncField
      FieldName = 'nCdParcContratoEmpImobiliario'
      ReadOnly = True
    end
    object qryAnuaisnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryAnuaisnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryAnuaisiParcela: TIntegerField
      DisplayLabel = 'Parcelas Anuais|Parcela'
      FieldName = 'iParcela'
    end
    object qryAnuaisiParcelaTotal: TIntegerField
      DisplayLabel = 'Parcelas Anuais|Parcela'
      FieldName = 'iParcelaTotal'
    end
    object qryAnuaisdDtVencto: TDateTimeField
      DisplayLabel = 'Parcelas Anuais|Data Vencimento'
      FieldName = 'dDtVencto'
    end
    object qryAnuaisnValorParcela: TBCDField
      DisplayLabel = 'Parcelas Anuais|Valor da Parcela'
      FieldName = 'nValorParcela'
      Precision = 12
      Size = 2
    end
  end
  object dsAnuais: TDataSource
    DataSet = qryAnuais
    Left = 456
    Top = 360
  end
  object qryChaves: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryChavesBeforePost
    OnCalcFields = qryIntermediariasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 4 --Anuais')
    Left = 496
    Top = 360
    object qryChavesnCdParcContratoEmpImobiliario: TAutoIncField
      FieldName = 'nCdParcContratoEmpImobiliario'
      ReadOnly = True
    end
    object qryChavesnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryChavesnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryChavesiParcela: TIntegerField
      DisplayLabel = 'Parcelas Chaves|Parcela'
      FieldName = 'iParcela'
    end
    object qryChavesiParcelaTotal: TIntegerField
      DisplayLabel = 'Parcelas Chaves|Parcela'
      FieldName = 'iParcelaTotal'
    end
    object qryChavesdDtVencto: TDateTimeField
      DisplayLabel = 'Parcelas Chaves|Data Vencimento'
      FieldName = 'dDtVencto'
    end
    object qryChavesnValorParcela: TBCDField
      DisplayLabel = 'Parcelas Chaves|Valor da Parcela'
      FieldName = 'nValorParcela'
      Precision = 12
      Size = 2
    end
  end
  object dsChaves: TDataSource
    DataSet = qryChaves
    Left = 528
    Top = 360
  end
  object qryFinBancario: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryFinBancarioBeforePost
    OnCalcFields = qryIntermediariasCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario            = :nPK'
      '   AND nCdTabTipoParcContratoEmpImobiliario = 6 --Bancario')
    Left = 568
    Top = 360
    object qryFinBancarionCdParcContratoEmpImobiliario: TAutoIncField
      FieldName = 'nCdParcContratoEmpImobiliario'
      ReadOnly = True
    end
    object qryFinBancarionCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryFinBancarionCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryFinBancarioiParcela: TIntegerField
      DisplayLabel = 'Financiamento Banc'#225'rio|Parcela'
      FieldName = 'iParcela'
    end
    object qryFinBancarioiParcelaTotal: TIntegerField
      DisplayLabel = 'Financiamento Banc'#225'rio|Parcela'
      FieldName = 'iParcelaTotal'
    end
    object qryFinBancariodDtVencto: TDateTimeField
      DisplayLabel = 'Financiamento Banc'#225'rio|Data Vencimento'
      FieldName = 'dDtVencto'
    end
    object qryFinBancarionValorParcela: TBCDField
      DisplayLabel = 'Financiamento Banc'#225'rio|Valor da Parcela'
      FieldName = 'nValorParcela'
      Precision = 12
      Size = 2
    end
  end
  object dsFinBancario: TDataSource
    DataSet = qryFinBancario
    Left = 600
    Top = 360
  end
end
