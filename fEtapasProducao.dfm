inherited frmEtapasProducao: TfrmEtapasProducao
  Left = 220
  Top = 144
  Width = 924
  Height = 559
  Caption = 'Etapas de Produ'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 908
    Height = 498
  end
  object Label1: TLabel [1]
    Left = 133
    Top = 40
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 123
    Top = 64
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 61
    Top = 88
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo C'#225'lculo Tempo'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [4]
    Left = 86
    Top = 112
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servi'#231'o Externo ?'
    FocusControl = DBEdit2
  end
  object Label5: TLabel [5]
    Left = 10
    Top = 136
    Width = 162
    Height = 13
    Alignment = taRightJustify
    Caption = 'Gera Pedido de Compra/Sa'#237'da ?'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 908
  end
  object DBEdit1: TDBEdit [7]
    Left = 176
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdEtapaProducao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Left = 176
    Top = 56
    Width = 649
    Height = 19
    DataField = 'cNmEtapaProducao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object edtGrupoCalcTempo: TER2LookupDBEdit [9]
    Left = 176
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdGrupoCalculoTempo'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 740
    QueryLookup = qryGrupoCalcTempo
  end
  object DBLookupComboBox1: TDBLookupComboBox [10]
    Left = 176
    Top = 104
    Width = 65
    Height = 19
    DataField = 'cFlgServicoExterno'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsServicoExterno
    TabOrder = 4
  end
  object DBLookupComboBox2: TDBLookupComboBox [11]
    Left = 176
    Top = 128
    Width = 65
    Height = 19
    DataField = 'cFlgGeraPedidoComercial'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsGeraPedido
    TabOrder = 5
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 248
    Top = 80
    Width = 577
    Height = 19
    DataField = 'cNmGrupoCalculoTempo'
    DataSource = dsGrupoCalcTempo
    TabOrder = 6
  end
  inherited qryMaster: TADOQuery
    BeforeClose = qryMasterBeforeClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ETAPAPRODUCAO'
      'WHERE nCdEtapaProducao = :nPK')
    object qryMasternCdEtapaProducao: TIntegerField
      FieldName = 'nCdEtapaProducao'
    end
    object qryMastercNmEtapaProducao: TStringField
      FieldName = 'cNmEtapaProducao'
      Size = 50
    end
    object qryMasternCdGrupoCalculoTempo: TIntegerField
      FieldName = 'nCdGrupoCalculoTempo'
    end
    object qryMastercFlgServicoExterno: TIntegerField
      FieldName = 'cFlgServicoExterno'
    end
    object qryMastercFlgGeraPedidoComercial: TIntegerField
      FieldName = 'cFlgGeraPedidoComercial'
    end
  end
  object qryGrupoCalcTempo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoCalculoTempo'
      '             ,cNmGrupoCalculoTempo'
      '   FROM GrupoCalculoTempo'
      ' WHERE nCdGrupoCalculoTempo = :nPK')
    Left = 240
    Top = 208
    object qryGrupoCalcTemponCdGrupoCalculoTempo: TIntegerField
      FieldName = 'nCdGrupoCalculoTempo'
    end
    object qryGrupoCalcTempocNmGrupoCalculoTempo: TStringField
      FieldName = 'cNmGrupoCalculoTempo'
      Size = 50
    end
  end
  object dsGrupoCalcTempo: TDataSource
    DataSet = qryGrupoCalcTempo
    Left = 256
    Top = 240
  end
  object dsServicoExterno: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 160
    Top = 296
  end
  object dsGeraPedido: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 160
    Top = 328
  end
end
