inherited frmCaixa_CredErro: TfrmCaixa_CredErro
  Left = 346
  Top = 125
  VertScrollBar.Range = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Credi'#225'rio com Erro'
  ClientHeight = 475
  ClientWidth = 730
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 178
    Width = 730
    Height = 297
  end
  inherited ToolBar1: TToolBar
    Width = 730
    Height = 33
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 178
    Width = 730
    Height = 297
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsVerificaCrediario
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      object cxGrid1DBTableView1DBColumn_nCdCrediario: TcxGridDBColumn
        Caption = 'Carnet'
        DataBinding.FieldName = 'nCdCrediario'
        Width = 100
      end
      object cxGrid1DBTableView1DBColumn_cCdTitulo: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'cCdTitulo'
        Width = 100
      end
      object cxGrid1DBTableView1DBColumn_iParcela: TcxGridDBColumn
        Caption = 'Parc.'
        DataBinding.FieldName = 'iParcela'
        Width = 60
      end
      object cxGrid1DBTableView1DBColumn_dDtVenc: TcxGridDBColumn
        Caption = 'Dt. Vencto'
        DataBinding.FieldName = 'dDtVenc'
        Width = 120
      end
      object cxGrid1DBTableView1DBColumn_nValTit: TcxGridDBColumn
        Caption = 'Val. Parcela'
        DataBinding.FieldName = 'nValTit'
        Width = 120
      end
      object cxGrid1DBTableView1DBColumn_cFlgParcFaltando: TcxGridDBColumn
        DataBinding.FieldName = 'cFlgParcFaltando'
        Visible = False
        HeaderGlyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E7F79AA7E34C62CC36
          4FC5344DC3465DC795A1DEE1E5F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFBFC7EF4C63D15264D48490E795A0EE959FED838EE54C5DCE3D54C3B8C1
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1CAF14760D57584E3A1ACF47F8BEC5C
          67E45B66E37D87EA9FA8F16F7CDD324BC2B9C1EAFFFFFFFFFFFFFFFFFFE7EAFA
          5970DE7888E6A3B0F55767E75665E68992ED8892EC535FE2525DE19FA9F26F7D
          DD4157C6E2E6F6FFFFFFFFFFFFA8B4F06073E0A4B3F75A6EEB596CEA5869E8FC
          FCFCFCFCFC5562E55461E3535FE29FA9F25061D197A4E1FFFFFFFFFFFF6A82E9
          8E9FF08499F45C73EE5B70EC5A6EEB909DF1A6AFF35767E75665E65562E57D89
          EB8591E74E64CEFFFFFFFFFFFF5D76EAA0B3F76580F25F78F05D76EF5C73EEFC
          FCFCFCFCFC596CEA5869E85767E75D6CE799A5F13C55CCFFFFFFFFFFFF617BEE
          A1B6F86784F4607CF35F7AF15F78F0FCFCFCFCFCFC5B70EC5A6EEB596CEA5F6F
          E99BA8F14159D0FFFFFFFFFFFF768DF391A6F388A1F86280F4617EF3607CF3FC
          FCFCFCFCFC5D76EF5C73EE5B70EC8293F18998EC5970D8FFFFFFFFFFFFB2BFFA
          6C81ECA9BDFB6382F56281F56280F4FCFCFCFCFCFC5F7AF15F78F05D76EFA5B5
          F85D70DDA2AFEBFFFFFFFFFFFFEBEEFE758CF78397F0A9BDFB6382F56382F5FC
          FCFCFCFCFC617EF3607CF3A6B9F97B8DEA5C74E1E7EAFAFFFFFFFFFFFFFFFFFF
          CED7FD6D86F88497F1A9BDFB8AA3F86B89F66B89F689A2F8A8BCFA7F92EC5972
          E5C6CEF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCED7FD778EFA6E83EE92A6F4A0
          B4F8A0B4F891A6F3687DE96981EDC8D1F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEBEEFFB5C3FD7C94FA6C86F76A84F5778EF5B1BEF8E9ECFDFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Width = 40
        IsCaptionAssigned = True
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 33
    Width = 730
    Height = 88
    Align = alTop
    Caption = ' Informa'#231#245'es Credi'#225'rio '
    TabOrder = 2
    object Label7: TLabel
      Tag = 1
      Left = 30
      Top = 54
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
      FocusControl = DBEdit8
    end
    object Label8: TLabel
      Tag = 1
      Left = 137
      Top = 54
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Carnet'
      FocusControl = DBEdit8
    end
    object Label2: TLabel
      Tag = 1
      Left = 554
      Top = 54
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Dt. Venda'
      FocusControl = DBEdit8
    end
    object Label1: TLabel
      Tag = 1
      Left = 255
      Top = 54
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Parcelas'
      FocusControl = DBEdit8
    end
    object Label9: TLabel
      Tag = 1
      Left = 396
      Top = 54
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Val. Carnet'
      FocusControl = DBEdit8
    end
    object Label4: TLabel
      Tag = 1
      Left = 17
      Top = 29
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente'
      FocusControl = DBEdit8
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 56
      Top = 23
      Width = 65
      Height = 21
      DataField = 'nCdTerceiro'
      DataSource = dsVerificaCrediario
      TabOrder = 0
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 125
      Top = 23
      Width = 585
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsVerificaCrediario
      TabOrder = 1
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 56
      Top = 48
      Width = 65
      Height = 21
      DataField = 'nCdLoja'
      DataSource = dsVerificaCrediario
      TabOrder = 2
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 176
      Top = 48
      Width = 65
      Height = 21
      DataField = 'nCdCrediario'
      DataSource = dsVerificaCrediario
      TabOrder = 3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 608
      Top = 48
      Width = 102
      Height = 21
      DataField = 'dDtVenda'
      DataSource = dsVerificaCrediario
      TabOrder = 4
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 328
      Top = 48
      Width = 50
      Height = 21
      DataField = 'iTotalParcCred'
      DataSource = dsVerificaCrediario
      TabOrder = 5
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 456
      Top = 48
      Width = 80
      Height = 21
      DataField = 'nValCrediario'
      DataSource = dsVerificaCrediario
      TabOrder = 6
    end
  end
  object GroupBox2: TGroupBox [4]
    Left = 0
    Top = 121
    Width = 730
    Height = 57
    Align = alTop
    Caption = ' Legenda '
    TabOrder = 3
    object Label3: TLabel
      Tag = 2
      Left = 48
      Top = 28
      Width = 80
      Height = 13
      Caption = 'Parcela Faltando'
    end
    object cxTextEdit1: TcxTextEdit
      Left = 16
      Top = 20
      Width = 25
      Height = 21
      Enabled = False
      Style.Color = clBlue
      StyleDisabled.Color = clRed
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 176
    Top = 248
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 208
    Top = 248
    object FaltaParcela: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clRed
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Consolas'
      Font.Style = []
      TextColor = clDefault
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 176
    Top = 312
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 208
    Top = 312
  end
  object qryVerificaCrediario: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryVerificaCrediarioAfterScroll
    Parameters = <>
    SQL.Strings = (
      'DECLARE @nCdTerceiro     int'
      '       ,@cNmTerceiro     varchar(100)'
      '       ,@nCdLoja         int'
      '       ,@nCdCrediario    int'
      '       ,@nCdCrediarioAux int'
      '       ,@dDtVenda        datetime'
      '       ,@iTotalParcCred  int'
      '       ,@nValCrediario   decimal(12,2)'
      '       ,@iParcela        int'
      '       ,@iParcelaAux     int'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_CredErro'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_CredErro (nCdTerceiro      int'
      '                                ,cNmTerceiro      varchar(100)'
      '                                ,nCdLoja          int'
      '                                ,nCdCrediario     int'
      '                                ,dDtVenda         datetime'
      
        '                                ,iTotalParcCred   int           ' +
        'default 0'
      
        '                                ,nValCrediario    decimal(12,2) ' +
        'default 0.00'
      '                                ,cCdTitulo        varchar(20)'
      
        '                                ,iParcela         int           ' +
        'default 0'
      '                                ,dDtVenc          datetime'
      
        '                                ,nValTit          decimal(12,2) ' +
        'default 0.00'
      
        '                                ,cFlgParcFaltando int           ' +
        'default 0)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_CredErro'
      ''
      'INSERT INTO #Temp_CredErro (nCdTerceiro'
      '                           ,cNmTerceiro'
      '                           ,nCdLoja'
      '                           ,nCdCrediario'
      '                           ,dDtVenda'
      '                           ,iTotalParcCred'
      '                           ,nValCrediario'
      '                           ,cCdTitulo'
      '                           ,iParcela'
      '                           ,dDtVenc'
      '                           ,nValTit'
      '                           ,cFlgParcFaltando)'
      '                     SELECT Crediario.nCdTerceiro'
      '                           ,cNmTerceiro'
      '                           ,Crediario.nCdLoja'
      '                           ,Crediario.nCdCrediario'
      '                           ,Crediario.dDtVenda'
      '                           ,iParcelas'
      '                           ,Crediario.nValCrediario'
      '                           ,CAST(Titulo.nCdTitulo as varchar)'
      '                           ,Titulo.iParcela'
      '                           ,Titulo.dDtVenc'
      '                           ,Titulo.nValTit'
      '                           ,0'
      '                       FROM Crediario'
      
        #9#9#9#9#9#9'    INNER JOIN Titulo   ON Titulo.nCdCrediario  = Crediari' +
        'o.nCdCrediario'
      
        '                            INNER JOIN Terceiro ON Terceiro.nCdT' +
        'erceiro = Crediario.nCdTerceiro'
      
        #9#9#9#9#9'  WHERE Crediario.nCdCrediario IN(SELECT DISTINCT TempTit.n' +
        'CdCrediario               -- verifica credi'#225'rios que foram'
      
        #9#9#9#9#9'    '#9'                            FROM #Temp_Titulos TempTit' +
        '                       -- marcados para recebimento na'
      
        '                                                       WHERE Tem' +
        'pTit.cFlgMarcado = 1)                    -- tela do caixa'
      
        '                        AND Crediario.iParcelas <> (SELECT COUNT' +
        '(1)                                      -- onde o total de parc' +
        'elas'
      
        #9#9#9#9#9#9#9#9'   '#9#9#9'          FROM Titulo                             ' +
        '           -- difere do total informado no'
      
        '                                                     WHERE Titul' +
        'o.nCdCrediario = Crediario.nCdCrediario) -- credi'#225'rio           ' +
        '        */'
      ''
      'DECLARE curParcFaltando CURSOR FOR'
      ' SELECT Temp.nCdTerceiro'
      '       ,Temp.cNmTerceiro'
      '       ,Temp.nCdLoja'
      '       ,Temp.nCdCrediario'
      '       ,Temp.dDtVenda'
      '       ,Temp.iTotalParcCred'
      '       ,Temp.nValCrediario'
      '       ,Temp.iParcela'
      '       ,Temp.dDtVenc'
      '   FROM #Temp_CredErro Temp'
      '  ORDER BY Temp.nCdCrediario'
      ''
      'OPEN curParcFaltando'
      ''
      'FETCH NEXT'
      ' FROM curParcFaltando'
      ' INTO @nCdTerceiro'
      '     ,@cNmTerceiro'
      '     ,@nCdLoja'
      '     ,@nCdCrediario'
      '     ,@dDtVenda'
      '     ,@iTotalParcCred'
      '     ,@nValCrediario'
      '     ,@iParcela'
      '     ,@dDtVenda'
      ''
      'SET @nCdCrediarioAux = 0'
      'SET @iParcelaAux     = 0'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      
        '    -- armazena c'#243'digo do credi'#225'rio para verifica'#231#227'o das parcela' +
        's --'
      #9'IF (@nCdCrediario <> @nCdCrediarioAux)'
      #9'BEGIN'
      #9'    SET @nCdCrediarioAux = @nCdCrediario'
      #9'    SET @iParcelaAux     = 1'
      #9'END'
      ''
      
        #9'-- se n'#250'mero da parcela n'#227'o existir, inseri registro de parcela' +
        ' faltando at'#233' equiparar numera'#231#227'o da parce'#231'a --'
      '    IF (@iParcelaAux <> @iParcela)'
      '    BEGIN'
      '        WHILE (@iParcelaAux <> @iParcela)'
      '        BEGIN'
      #9'        INSERT INTO #Temp_CredErro (nCdTerceiro'
      '                                       ,cNmTerceiro'
      '                                       ,nCdLoja'
      '                                       ,nCdCrediario'
      '                                       ,dDtVenda'
      '                                       ,iTotalParcCred'
      '                                       ,nValCrediario'
      '                                       ,cCdTitulo'
      '                                       ,iParcela'
      '                                       ,dDtVenc'
      '                                       ,nValTit'
      '                                       ,cFlgParcFaltando)'
      #9'                             VALUES(@nCdTerceiro'
      '                                       ,@cNmTerceiro'
      '                                       ,@nCdLoja'
      '                                       ,@nCdCrediario'
      '                                       ,@dDtVenda'
      '                                       ,@iTotalParcCred'
      '                                       ,@nValCrediario'
      '                                       ,'#39'ND'#39
      '                                       ,@iParcelaAux'
      '                                       ,NULL'
      '                                       ,0.00'
      
        '                                       ,1) -- parcela inexistent' +
        'e'
      ''
      '            SET @iParcelaAux = (@iParcelaAux + 1)'
      '        END'
      ''
      
        '        -- ao equiparar a numera'#231#227'o da parcela atual (@iParcelaA' +
        'ux = @iParcela) verifica se as pr'#243'ximas parcelas'
      '       '#9'-- tamb'#233'm n'#227'o existem na base de dados e inseri na temp'
      '      '#9'IF (@iParcela = (SELECT MAX(iParcela)'
      #9#9'                   FROM #Temp_CredErro'
      
        '                          WHERE nCdCrediario = @nCdCrediario)) A' +
        'ND @iParcela <> @iTotalParcCred'
      '        BEGIN'
      #9#9'    WHILE (@iParcelaAux <> @iTotalParcCred)'
      #9#9#9'BEGIN'
      #9#9#9'    SET @iParcelaAux = @iParcelaAux + 1'
      ''
      '                INSERT INTO #Temp_CredErro (nCdTerceiro'
      '                                           ,cNmTerceiro'
      '                                           ,nCdLoja'
      '                                           ,nCdCrediario'
      '                                           ,dDtVenda'
      '                                           ,iTotalParcCred'
      '                                           ,nValCrediario'
      '                                           ,cCdTitulo'
      '                                           ,iParcela'
      '                                           ,dDtVenc'
      '                                           ,nValTit'
      '                                           ,cFlgParcFaltando)'
      #9'                                 VALUES(@nCdTerceiro'
      '                                           ,@cNmTerceiro'
      '                                           ,@nCdLoja'
      '                                           ,@nCdCrediario'
      '                                           ,@dDtVenda'
      '                                           ,@iTotalParcCred'
      '                                           ,@nValCrediario'
      '                                           ,'#39'ND'#39
      '                                           ,@iParcelaAux'
      '                                           ,NULL'
      '                                           ,0.00'
      
        '                                           ,1) -- parcela inexis' +
        'tente'
      ''
      #9#9'    END'
      #9#9'END'
      #9#9'ELSE'
      #9#9'    SET @iParcelaAux = @iParcelaAux + 1'
      '    END'
      '    ELSE'
      '        SET @iParcelaAux = @iParcelaAux + 1'
      ''
      '    FETCH NEXT'
      '     FROM curParcFaltando'
      '     INTO @nCdTerceiro'
      '         ,@cNmTerceiro'
      '         ,@nCdLoja'
      '         ,@nCdCrediario'
      '         ,@dDtVenda'
      '         ,@iTotalParcCred'
      '         ,@nValCrediario'
      '         ,@iParcela'
      '         ,@dDtVenda'
      ''
      'END'
      ''
      'CLOSE curParcFaltando'
      'DEALLOCATE curParcFaltando'
      ''
      'SELECT *'
      '  FROM #Temp_CredErro'
      ' ORDER BY nCdLoja'
      '         ,nCdTerceiro'
      '         ,nCdCrediario'
      '         ,iParcela')
    Left = 176
    Top = 280
    object qryVerificaCrediarionCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryVerificaCrediariocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryVerificaCrediarionCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryVerificaCrediarionCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryVerificaCrediariodDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object qryVerificaCrediarioiTotalParcCred: TIntegerField
      FieldName = 'iTotalParcCred'
    end
    object qryVerificaCrediarionValCrediario: TBCDField
      FieldName = 'nValCrediario'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVerificaCrediariocCdTitulo: TStringField
      FieldName = 'cCdTitulo'
    end
    object qryVerificaCrediarioiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryVerificaCrediariodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryVerificaCrediarionValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVerificaCrediariocFlgParcFaltando: TIntegerField
      FieldName = 'cFlgParcFaltando'
    end
  end
  object dsVerificaCrediario: TDataSource
    DataSet = qryVerificaCrediario
    Left = 208
    Top = 280
  end
end
