inherited frmMotivoCancSaldoPed: TfrmMotivoCancSaldoPed
  Left = 264
  Top = 279
  Height = 241
  Caption = 'Motivo Cancelamento Saldo Pedido'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 178
  end
  object Label1: TLabel [1]
    Left = 44
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 34
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 6
    Top = 86
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Pedido'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 86
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdMotivoCancSaldoPed'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 86
    Top = 56
    Width = 539
    Height = 19
    DataField = 'cNmMotivoCancSaldoPed'
    DataSource = dsMaster
    TabOrder = 2
  end
  object edtTipoPedido: TER2LookupDBEdit [7]
    Left = 86
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoPedido'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 55
    QueryLookup = qryTipoPedido
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 154
    Top = 80
    Width = 215
    Height = 19
    DataField = 'cNmTabTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM MotivoCancSaldoPed'
      ' WHERE nCdMotivoCancSaldoPed = :nPK')
    object qryMasternCdMotivoCancSaldoPed: TIntegerField
      FieldName = 'nCdMotivoCancSaldoPed'
    end
    object qryMastercNmMotivoCancSaldoPed: TStringField
      FieldName = 'cNmMotivoCancSaldoPed'
      Size = 50
    end
    object qryMasternCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoPedido'
      '      ,cNmTabTipoPedido'
      '  FROM TabTipoPedido'
      ' WHERE nCdTabTipoPedido = :nPK      ')
    Left = 680
    Top = 128
    object qryTipoPedidonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryTipoPedidocNmTabTipoPedido: TStringField
      FieldName = 'cNmTabTipoPedido'
      Size = 50
    end
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 680
    Top = 160
  end
end
