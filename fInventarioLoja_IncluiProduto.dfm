inherited frmInventarioLoja_IncluiProduto: TfrmInventarioLoja_IncluiProduto
  Left = 262
  Top = 217
  Width = 833
  Height = 377
  Caption = 'Inclus'#227'o de Produtos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 817
    Height = 310
  end
  inherited ToolBar1: TToolBar
    Width = 817
    ButtonWidth = 90
    inherited ToolButton1: TToolButton
      Caption = 'Limpar Filtros'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 90
    end
    inherited ToolButton2: TToolButton
      Left = 98
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 817
    Height = 310
    Align = alClient
    Caption = 'Filtro'
    TabOrder = 1
    object Label1: TLabel
      Left = 9
      Top = 24
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Departamento'
    end
    object Label2: TLabel
      Left = 31
      Top = 48
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Categoria'
    end
    object Label3: TLabel
      Left = 10
      Top = 72
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sub Categoria'
    end
    object Label4: TLabel
      Left = 30
      Top = 96
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Segmento'
    end
    object Label5: TLabel
      Left = 49
      Top = 120
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = 'Marca'
    end
    object Label6: TLabel
      Left = 53
      Top = 144
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Linha'
    end
    object Label7: TLabel
      Left = 47
      Top = 168
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Classe'
    end
    object Label8: TLabel
      Left = 8
      Top = 216
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo Produto'
    end
    object Label9: TLabel
      Left = 26
      Top = 240
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Refer'#234'ncia'
    end
    object Label10: TLabel
      Left = 40
      Top = 192
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cole'#231#227'o'
      FocusControl = DBEdit10
    end
    object edtDepartamento: TMaskEdit
      Left = 82
      Top = 16
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtDepartamentoExit
      OnKeyDown = edtDepartamentoKeyDown
    end
    object edtCategoria: TMaskEdit
      Left = 82
      Top = 40
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtCategoriaExit
    end
    object edtSubCategoria: TMaskEdit
      Left = 82
      Top = 64
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnExit = edtSubCategoriaExit
    end
    object edtSegmento: TMaskEdit
      Left = 82
      Top = 88
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 3
      Text = '      '
      OnExit = edtSegmentoExit
    end
    object edtMarca: TMaskEdit
      Left = 82
      Top = 112
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 4
      Text = '      '
      OnExit = edtMarcaExit
      OnKeyDown = edtMarcaKeyDown
    end
    object edtLinha: TMaskEdit
      Left = 82
      Top = 136
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 5
      Text = '      '
      OnExit = edtLinhaExit
      OnKeyDown = edtLinhaKeyDown
    end
    object edtClasse: TMaskEdit
      Left = 82
      Top = 160
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 6
      Text = '      '
      OnExit = edtClasseExit
      OnKeyDown = edtClasseKeyDown
    end
    object edtGrupoProduto: TMaskEdit
      Left = 82
      Top = 208
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 8
      Text = '      '
      OnExit = edtGrupoProdutoExit
      OnKeyDown = edtGrupoProdutoKeyDown
    end
    object edtReferencia: TEdit
      Left = 82
      Top = 232
      Width = 121
      Height = 21
      MaxLength = 15
      TabOrder = 9
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 152
      Top = 16
      Width = 654
      Height = 21
      DataField = 'cNmDepartamento'
      DataSource = DataSource1
      TabOrder = 10
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 152
      Top = 40
      Width = 654
      Height = 21
      DataField = 'cNmCategoria'
      DataSource = DataSource2
      TabOrder = 11
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 152
      Top = 64
      Width = 654
      Height = 21
      DataField = 'cNmSubCategoria'
      DataSource = DataSource3
      TabOrder = 12
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 152
      Top = 88
      Width = 654
      Height = 21
      DataField = 'cNmSegmento'
      DataSource = DataSource4
      TabOrder = 13
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 152
      Top = 112
      Width = 654
      Height = 21
      DataField = 'cNmMarca'
      DataSource = DataSource5
      TabOrder = 14
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 152
      Top = 136
      Width = 654
      Height = 21
      DataField = 'cNmLinha'
      DataSource = DataSource6
      TabOrder = 15
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 152
      Top = 160
      Width = 654
      Height = 21
      DataField = 'cNmClasseProduto'
      DataSource = DataSource7
      TabOrder = 16
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 152
      Top = 208
      Width = 654
      Height = 21
      DataField = 'cNmGrupoProduto'
      DataSource = DataSource8
      TabOrder = 17
    end
    object cxButton1: TcxButton
      Left = 5
      Top = 264
      Width = 113
      Height = 30
      Caption = 'Exibir Produtos'
      TabOrder = 18
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object edtColecao: TMaskEdit
      Left = 82
      Top = 184
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 7
      Text = '      '
      OnExit = edtColecaoExit
      OnKeyDown = edtColecaoKeyDown
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 152
      Top = 184
      Width = 654
      Height = 21
      DataField = 'cNmColecao'
      DataSource = dsColecao
      TabOrder = 19
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C
      6F009C8C6F009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED9000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F009C8C
      6F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000009C8C6F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFC030000
      FE00C003C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C007000000018001C00F000000038001C01F0000
      0077C003C03F0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 168
    Top = 40
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryDepartamento
    Left = 784
    Top = 72
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategoria, cNmCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 168
    Top = 64
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCategoria
    Left = 784
    Top = 40
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria, cNmSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdCategoria = :nCdCategoria'
      'AND nCdSubCategoria = :nPK')
    Left = 168
    Top = 88
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qrySubCategoria
    Left = 752
    Top = 232
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSegmento, cNmSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 168
    Top = 112
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qrySegmento
    Left = 752
    Top = 168
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 168
    Top = 136
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object DataSource5: TDataSource
    DataSet = qryMarca
    Left = 752
    Top = 136
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdMarca'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :nPK'
      'AND nCdMarca = :nCdMarca')
    Left = 168
    Top = 160
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryLinha
    Left = 752
    Top = 104
  end
  object qryClasse: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdClasseProduto, cNmClasseProduto'
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 168
    Top = 184
    object qryClassenCdClasseProduto: TIntegerField
      FieldName = 'nCdClasseProduto'
    end
    object qryClassecNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryClasse
    Left = 752
    Top = 72
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 168
    Top = 240
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryGrupoProduto
    Left = 752
    Top = 40
  end
  object qryAdicionaProdutos: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdDepartamento'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSegmento'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdMarca'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLinha'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdClasse'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdGrupoProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cReferencia'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdColecao'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdDepartamento int'
      '       ,@nCdCategoria    int'
      '       ,@nCdSubCategoria int'
      '       ,@nCdSegmento     int'
      '       ,@nCdMarca        int'
      '       ,@nCdLinha        int'
      '       ,@nCdClasse       int'
      '       ,@nCdGrupoProduto int'
      '       ,@cReferencia     char(15)'
      '       ,@nCdColecao      int'
      ''
      'Set @nCdDepartamento = :nCdDepartamento'
      'Set @nCdCategoria    = :nCdCategoria'
      'Set @nCdSubCategoria = :nCdSubCategoria'
      'Set @nCdSegmento     = :nCdSegmento'
      'Set @nCdMarca        = :nCdMarca'
      'Set @nCdLinha        = :nCdLinha'
      'Set @nCdClasse       = :nCdClasse'
      'Set @nCdGrupoProduto = :nCdGrupoProduto'
      'Set @cReferencia     = :cReferencia'
      'Set @nCdColecao      = :nCdColecao'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Produto_Sel'#39') IS NULL)'
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Produto_Sel (cFlgOK         int'
      #9#9#9#9#9#9#9#9'                 ,nCdProduto     int'
      #9#9#9#9#9#9#9#9'                 ,cReferencia    char(15)'
      #9#9#9#9#9#9#9#9'                 ,cNmProduto     varchar(150)'
      #9#9#9#9#9#9#9#9'                 ,cUnidadeMedida char(3)'
      '                                 ,PRIMARY KEY (nCdProduto))'
      ''
      'END'
      ''
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'TRUNCATE TABLE #Temp_Produto_Sel'
      ''
      'INSERT INTO #Temp_Produto_Sel (cFlgOK'
      '                              ,nCdProduto'
      #9#9#9#9#9#9#9#9'              ,cReferencia'
      #9#9#9#9#9#9#9#9'              ,cNmProduto'
      #9#9#9#9#9#9#9#9'              ,cUnidadeMedida)'
      'SELECT 1'
      #9#9'  ,nCdProduto'
      #9#9'  ,cReferencia'
      #9#9'  ,cNmProduto'
      '      ,cUnidadeMedida'
      '  FROM Produto WITH (NOLOCK)'
      
        '   '#9'   INNER JOIN Departamento  ON Departamento.nCdDepartamento ' +
        '  = Produto.nCdDepartamento'
      
        #9#9'   INNER JOIN Categoria     ON Categoria.nCdCategoria         ' +
        '= Produto.nCdCategoria'
      
        #9#9'   INNER JOIN SubCategoria  ON SubCategoria.nCdSubCategoria   ' +
        '= Produto.nCdSubCategoria'
      
        #9#9'   INNER JOIN Segmento      ON Segmento.nCdSegmento           ' +
        '= Produto.nCdSegmento'
      
        #9#9'   LEFT  JOIN Linha         ON Linha.nCdLinha                 ' +
        '= Produto.nCdLinha'
      
        #9#9'   LEFT  JOIN Marca         ON Marca.nCdMarca                 ' +
        '= Produto.nCdMarca'
      
        #9#9'   LEFT  JOIN GrupoProduto  ON GrupoProduto.nCdGrupoProduto   ' +
        '= Produto.nCdGrupo_Produto'
      
        #9#9'   LEFT  JOIN ClasseProduto ON ClasseProduto.nCdClasseProduto ' +
        '= Produto.nCdClasseProduto'
      
        '       LEFT  JOIN Status        ON Status.nCdStatus             ' +
        '  = Produto.nCdStatus'
      
        '       LEFT  JOIN Colecao       ON Colecao.nCdColecao           ' +
        '  = Produto.nCdColecao'
      #9' WHERE cFlgProdVenda      = 1'
      #9'     AND nCdTabTipoProduto <> 4'
      '       AND Produto.nCdStatus = 1'
      
        '       AND ((@nCdDepartamento = 0) OR (Produto.nCdDepartamento  ' +
        '= @nCdDepartamento))'
      
        '       AND ((@nCdCategoria    = 0) OR (Produto.nCdCategoria     ' +
        '= @nCdCategoria))'
      
        '       AND ((@nCdSubCategoria = 0) OR (Produto.nCdSubCategoria  ' +
        '= @nCdSubCategoria))'
      
        '       AND ((@nCdSegmento     = 0) OR (Produto.nCdSegmento      ' +
        '= @nCdSegmento))'
      
        '       AND ((@nCdMarca        = 0) OR (Produto.nCdMarca         ' +
        '= @nCdMarca))'
      
        '       AND ((@nCdLinha        = 0) OR (Produto.nCdLinha         ' +
        '= @nCdLinha))'
      
        '       AND ((@nCdClasse       = 0) OR (Produto.nCdClasseProduto ' +
        '= @nCdClasse))'
      
        '       AND ((@nCdGrupoProduto = 0) OR (Produto.nCdGrupo_Produto ' +
        '= @nCdGrupoProduto))'
      
        '       AND ((@nCdColecao      = 0) OR (Produto.nCdColecao       ' +
        '= @nCdColecao))'
      
        '       AND ((@cReferencia     = '#39#39') OR (Produto.cReferencia     ' +
        '= @cReferencia))'
      #9'     AND (   (nCdTabTipoProduto = 3  AND nCdGrade IS NOT NULL)'
      '            OR (nCdGrade IS NULL AND NOT EXISTS(SELECT 1'
      #9#9#9#9#9#9#9#9'                                  FROM Produto Filho'
      
        #9#9#9#9#9#9#9#9'                                 WHERE Filho.nCdProdutoP' +
        'ai = Produto.nCdProduto)))'
      ''
      'SELECT TOP 1 *'
      '  FROM #Temp_Produto_Sel')
    Left = 280
    Top = 96
    object qryAdicionaProdutoscFlgOK: TIntegerField
      FieldName = 'cFlgOK'
    end
    object qryAdicionaProdutosnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryAdicionaProdutoscReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryAdicionaProdutoscNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryAdicionaProdutoscUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Produto_Sel'#39') IS NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Produto_Sel (cFlgOK         int'
      #9#9#9#9#9#9#9#9'   ,nCdProduto     int'
      #9#9#9#9#9#9#9#9'   ,cReferencia    char(15)'
      #9#9#9#9#9#9#9#9'   ,cNmProduto     varchar(150)'
      #9#9#9#9#9#9#9#9'   ,cUnidadeMedida char(3)'
      #9#9#9#9#9#9#9#9'   ,PRIMARY KEY (nCdProduto))'
      ''
      'END')
    Left = 272
    Top = 168
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 304
    Top = 128
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao, cNmColecao'
      'FROM Colecao'
      'WHERE nCdColecao = :nPK')
    Left = 168
    Top = 213
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object dsColecao: TDataSource
    DataSet = qryColecao
    Left = 752
    Top = 200
  end
end
