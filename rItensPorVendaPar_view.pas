unit rItensPorVendaPar_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QuickRpt, QRCtrls, ExtCtrls;

type
  TrptItensPorVendaPar_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText8: TQRDBText;
    QRExpr1: TQRExpr;
    QRLabel29: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRDBText9: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr9: TQRExpr;
    QRShape9: TQRShape;
    QRLabel43: TQRLabel;
    QRExpr24: TQRExpr;
    DetailBand1: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText10: TQRDBText;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRDBText30: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRBand2: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRExpr10: TQRExpr;
    QRLabel39: TQRLabel;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    QRExpr19: TQRExpr;
    QRExpr20: TQRExpr;
    QRExpr21: TQRExpr;
    QRExpr22: TQRExpr;
    cmdPreparaTemp: TADOCommand;
    SPREL_ITENS_POR_VENDA_PAR: TADOStoredProc;
    qryResultado: TADOQuery;
    QRLabel44: TQRLabel;
    QRDBText12: TQRDBText;
    QRExpr23: TQRExpr;
    QRLabel45: TQRLabel;
    QRDBText13: TQRDBText;
    QRExpr25: TQRExpr;
    QRLabel46: TQRLabel;
    QRDBText16: TQRDBText;
    QRExpr26: TQRExpr;
    QRDBText22: TQRDBText;
    QRExpr27: TQRExpr;
    QRLabel47: TQRLabel;
    QRExpr28: TQRExpr;
    QRExpr29: TQRExpr;
    QRExpr30: TQRExpr;
    qryResultadonCdLoja: TStringField;
    qryResultadonCdTerceiroColab: TIntegerField;
    qryResultadonCdVendedor: TIntegerField;
    qryResultadocNmVendedor: TStringField;
    qryResultadonValPedidosGeral: TBCDField;
    qryResultadonPercentParticip: TBCDField;
    qryResultadoiTotalPedidosGeral: TIntegerField;
    qryResultadoiQtdItensGeral: TIntegerField;
    qryResultadonValPedidosVenda: TBCDField;
    qryResultadoiQtdVenda: TIntegerField;
    qryResultadoiQtdItensVenda: TIntegerField;
    qryResultadonValMedio: TBCDField;
    qryResultadonItemPorVenda: TBCDField;
    qryResultadonValItemMedio: TBCDField;
    qryResultadonValPedidosTroca: TBCDField;
    qryResultadonValVale: TBCDField;
    qryResultadoiTotalTrocas: TIntegerField;
    qryResultadoiQtdItensTrocas: TIntegerField;
    qryResultadonSumValPedidos: TBCDField;
    qryResultadonSumValPedidosGeral: TBCDField;
    qryResultadonSumTotalPedidos: TIntegerField;
    qryResultadonSumQtdItens: TIntegerField;
    qryResultadonValMedioTotal: TBCDField;
    qryResultadonItemPorVendaTotal: TBCDField;
    qryResultadonValItemMedioTotal: TBCDField;
    qryResultadoiQtdParGeral: TIntegerField;
    qryResultadoiQtdParVendas: TBCDField;
    qryResultadoiQtdParTrocas: TBCDField;
    qryResultadonParPorVenda: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptItensPorVendaPar_view: TrptItensPorVendaPar_view;

implementation

{$R *.dfm}

end.
