unit fSelecMotBloqPed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBCtrls, DBSumLst, Mask, DBCtrlsEh;

type
  TfrmSelecMotBloqPed = class(TfrmProcesso_Padrao)
    qryMotivo: TADOQuery;
    qryMotivonCdMotBloqPed: TIntegerField;
    qryMotivocNmMotBloqPed: TStringField;
    DataSource1: TDataSource;
    DBComboBoxEh1: TDBComboBoxEh;
    cmdBloquear: TADOCommand;
    Memo1: TMemo;
    Label1: TLabel;
    procedure ListBox_DrawItem(
       Control: TWinControl;
       Index: Integer;
       Rect: TRect;
       State: TOwnerDrawState) ;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido : integer ;
  end;

var
  frmSelecMotBloqPed: TfrmSelecMotBloqPed;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmSelecMotBloqPed.ListBox_DrawItem(
   Control: TWinControl;
   Index: Integer;
   Rect: TRect;
   State: TOwnerDrawState) ;
const
   IsSelected : array[Boolean] of Integer = (DFCS_BUTTONRADIO, DFCS_BUTTONRADIO or DFCS_CHECKED) ;
var
   optionButtonRect: TRect;
   listBox : TListBox;
begin
   listBox := Control as TListBox;
   with listBox.Canvas do
   begin
     FillRect(rect) ;

     optionButtonRect.Left := rect.Left + 1;
     optionButtonRect.Right := Rect.Left + 13;
     optionButtonRect.Bottom := Rect.Bottom;
     optionButtonRect.Top := Rect.Top;

     DrawFrameControl(Handle, optionButtonRect, DFC_BUTTON, IsSelected[odSelected in State]) ;

     TextOut(15, rect.Top + 3, listBox.Items[Index]) ;
   end;
end;

procedure TfrmSelecMotBloqPed.FormShow(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryMotivo, IntToStr(nCdPedido)) ;

end;

procedure TfrmSelecMotBloqPed.ToolButton1Click(Sender: TObject);
begin
  inherited;

  frmMenu.Connection.BeginTrans;

  try
      cmdBloquear.Parameters.ParamByName('nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      cmdBloquear.Parameters.ParamByName('nCdMotBloqPed').Value := qryMotivonCdMotBloqPed.Value ;
      cmdBloquear.Parameters.ParamByName('cNmMotBloqPed').Value := qryMotivocNmMotBloqPed.Value ;
      cmdBloquear.Parameters.ParamByName('nCdPedido').Value     := nCdPedido ;
      cmdBloquear.Parameters.ParamByName('cOBS').Value          := Memo1.Text ;
      cmdBloquear.Execute;
  except
      frmMenu.Connection.CommitTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;

end;

end.
