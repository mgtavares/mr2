unit fHistMov_HistPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, DB, ADODB, GridsEh,
  DBGridEh, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmHistMov_HistPagto = class(TfrmProcesso_Padrao)
    ListaInfPagto: TDBGridEh;
    dsInfPagto: TDataSource;
    qryInfPag: TADOQuery;
    qryInfPagcNrTit: TStringField;
    qryInfPagiParcela: TIntegerField;
    qryInfPagnValMov: TBCDField;
    qryInfPagdDtMov: TDateTimeField;
    qryInfPagnValJuro: TBCDField;
    qryInfPagnCdUsuario: TIntegerField;
    qryInfPagcNmUsuario: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistMov_HistPagto: TfrmHistMov_HistPagto;

implementation

uses fMenu;

{$R *.dfm}

end.
