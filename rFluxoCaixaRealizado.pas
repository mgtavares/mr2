unit rFluxoCaixaRealizado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask, ER2Lookup, cxCustomData;

type
  TrptFluxoCaixaRealizado = class(TfrmRelatorio_Padrao)
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    qryContaBancariaDebcFlgCaixa: TIntegerField;
    qryContaBancariaDebcFlgCofre: TIntegerField;
    DBEdit1: TDBEdit;
    dsContaBancariaDeb: TDataSource;
    Label2: TLabel;
    MaskEdit4: TMaskEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    MaskEdit5: TMaskEdit;
    DBEdit3: TDBEdit;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    RadioGroup1: TRadioGroup;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    edtLojaEmissora: TER2LookupMaskEdit;
    qryLojaEmissora: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    dsLojaEmissora: TDataSource;
    CheckBox1: TCheckBox;
    edtCdCC: TER2LookupMaskEdit;
    Label7: TLabel;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    DBEdit5: TDBEdit;
    dsCentroCusto: TDataSource;
    qryPreparaTemp: TADOQuery;
    qryCentroCustoiNivel: TSmallintField;
    Label8: TLabel;
    qryGrupoTotalCC: TADOQuery;
    qryGrupoTotalCCnCdGrupoTotalCC: TIntegerField;
    qryGrupoTotalCCcNmGrupoTotalCC: TStringField;
    DBEdit6: TDBEdit;
    DataSource3: TDataSource;
    edtGrupoTotalizador: TER2LookupMaskEdit;
    qryTempResumoCaixa: TADOQuery;
    qryColunaGrupoTotalCC: TADOQuery;
    qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField;
    chkListaContaTransf: TCheckBox;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtLojaEmissoraBeforePosicionaQry(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFluxoCaixaRealizado: TrptFluxoCaixaRealizado;

implementation

uses fMenu, fLookup_Padrao, fFluxoCaixaRealizado_View, cxGridTableView;

{$R *.dfm}

procedure TrptFluxoCaixaRealizado.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryContaBancariaDeb.Close ;

  if((Trim(MaskEdit4.Text)<>'') and (frmMenu.LeParametro('VAREJO')='N')) then
  begin
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := Trim(MaskEdit4.Text);
      qryContaBancariaDeb.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := 0;
      PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;
  end
  else if((Trim(MaskEdit4.Text)<>'') and (Trim(MaskEdit5.Text)<>'') and (frmMenu.LeParametro('VAREJO')='S')) then
  begin
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := Trim(MaskEdit4.Text);
      qryContaBancariaDeb.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := Trim(MaskEdit5.Text);
      PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;
  end
  else
  begin
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := 0;
      qryContaBancariaDeb.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := 0;
      PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;
  end;
end;

procedure TrptFluxoCaixaRealizado.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

                if((Trim(MaskEdit4.Text)<>'') and (frmMenu.LeParametro('VAREJO')='N')) then
                    nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa ='+ MaskEdit4.Text +' AND cFlgFluxo = 1')
                else if((Trim(MaskEdit4.Text)<>'') and (Trim(MaskEdit5.Text)<>'') and (frmMenu.LeParametro('VAREJO')='S')) then
                    nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja ='+ Trim(MaskEdit5.Text) +' AND ContaBancaria.nCdEmpresa ='+ MaskEdit4.Text +' AND cFlgFluxo = 1')
                else nPK := frmLookup_Padrao.ExecutaConsulta2(37,'(EXISTS(SELECT 1 FROM UsuarioEmpresa UE WHERE UE.nCdEmpresa = ContaBancaria.nCdEmpresa AND UE.nCdUsuario = @@Usuario)) AND (EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja = ContaBancaria.nCdLoja AND UL.nCdUsuario = @@Usuario)) AND cFlgFluxo = 1');
                If (nPK > 0) then
                begin
                    MaskEdit3.Text := IntToStr(nPK) ;
                end ;


    end ;

  end ;

end;

procedure TrptFluxoCaixaRealizado.ToolButton1Click(Sender: TObject);
var
  objRel : TfrmFluxoCaixaRealizado_view ;
  i : integer ;
begin
  inherited;

  if (Trim(MaskEdit1.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  if (DBEdit1.Text <> '') then
      RadioGroup1.ItemIndex := 0 ;

  objRel := TfrmFluxoCaixaRealizado_view.Create( Self ) ;

  qryPreparaTemp.ExecSQL;
  
  objRel.SP_FLUXOCAIXA_REALIZADO.Close ;
  if(Trim(MaskEdit4.Text)='') then
      objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdEmpresa').Value  := 0
  else objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdEmpresa').Value := MaskEdit4.Text ;

  if((Trim(MaskEdit5.Text)='') or (frmMenu.LeParametro('VAREJO')='N')) then
      objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdLoja').Value  := 0
  else objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdLoja').Value := MaskEdit5.Text ;

  if((Trim(edtLojaEmissora.Text)='') or (frmMenu.LeParametro('VAREJO')='N')) then
      objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdLojaEmissora').Value  := 0
  else objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdLojaEmissora').Value := edtLojaEmissora.Text ;

  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  if(Trim(MaskEdit3.Text)='') then
      objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdContaBancaria').Value  := 0
  else objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdContaBancaria').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;

  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cDtInicial').Value      := Maskedit1.Text ;
  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cDtFinal').Value        := Maskedit2.Text ;
  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cFlgTipoConta').Value   := RadioGroup1.ItemIndex;
  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdCC').Value           := frmMenu.ConvInteiro(edtCdCC.Text) ;
  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@nCdGrupoTotalCC').Value := frmMenu.ConvInteiro(edtGrupoTotalizador.Text) ;
  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cFlgDetalhaCC').Value   := 1 ;

  objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cFlgListaContaTransf').Value := 0 ;

  if (chkListaContaTransf.Checked) then
      objRel.SP_FLUXOCAIXA_REALIZADO.Parameters.ParamByName('@cFlgListaContaTransf').Value := 1 ;

  objRel.SP_FLUXOCAIXA_REALIZADO.Open ;

  {-- atualiza a lista de campos do retorno da procedure --}
  objRel.SP_FLUXOCAIXA_REALIZADO.FieldList.Update;

  {-- for�a o grid a criar todos os campos do dataset --}
  objRel.cxGrid1DBTableView1.DataController.CreateAllItems;

  {-- agora trata campo a campo --}

  {-- indice: 0 - cTipo --}
  objRel.cxGrid1DBTableView1.Columns[0].Caption    := 'Tipo' ;
  objRel.cxGrid1DBTableView1.Columns[0].GroupIndex := 0 ;
  objRel.cxGrid1DBTableView1.Columns[0].Visible    := False ;

  {-- indice: 1 - iSeqDRE --}
  objRel.cxGrid1DBTableView1.Columns[1].Visible := False ;

  {-- indice: 2 - cNmGrupoPlanoConta --}
  objRel.cxGrid1DBTableView1.Columns[2].Caption    := 'Grupo Conta' ;
  objRel.cxGrid1DBTableView1.Columns[2].Width      := 250 ;
  objRel.cxGrid1DBTableView1.Columns[2].GroupIndex := 1 ;
  objRel.cxGrid1DBTableView1.Columns[2].Visible    := False ;

  {-- indice: 3 - nCdPlanoConta --}
  objRel.cxGrid1DBTableView1.Columns[3].Visible := False ;

  {-- indice: 4 - cNmPlanoConta --}
  objRel.cxGrid1DBTableView1.Columns[4].Caption := 'Conta' ;
  objRel.cxGrid1DBTableView1.Columns[4].Width   := 250 ;

  {-- indice: 5 - cFlgRecDes --}
  objRel.cxGrid1DBTableView1.Columns[5].Caption := 'Senso' ;
  objRel.cxGrid1DBTableView1.Columns[5].Width   := 60 ;

  {-- indice: 6 - nCdCC --}
  objRel.cxGrid1DBTableView1.Columns[6].Visible := False ;

  {-- indice: 7 - cNmCC1 --}
  objRel.cxGrid1DBTableView1.Columns[7].Visible := False ;

  {-- indice: 8 - cNmCC2 --}
  objRel.cxGrid1DBTableView1.Columns[8].Visible := False ;

  {-- indice: 9 - cNmCC3 --}
  objRel.cxGrid1DBTableView1.Columns[9].Visible := False ;

  {-- indice: 10 - nValor --}
  objRel.cxGrid1DBTableView1.Columns[10].Caption := 'Total' ;
  objRel.cxGrid1DBTableView1.Columns[10].Width   := 90 ;
  (objRel.SP_FLUXOCAIXA_REALIZADO.FieldList[10] as TBCDField).DisplayFormat := '#,##0.00' ;

  with objRel.cxGrid1DBTableView1.Columns[10].Summary do
  begin
      GroupFooterKind   := skSum ;
      GroupFooterFormat := '#,##0.00' ;
  end ;

  {-- a partir do �ndice 11 (quando existir), � coluna variavel de acordo com o grupo totalizador selecionado. --}

  if (objRel.SP_FLUXOCAIXA_REALIZADO.FieldList.Count > 11) then
  begin

      for i := 11 to (objRel.SP_FLUXOCAIXA_REALIZADO.FieldList.Count - 1) do
      begin

          (objRel.SP_FLUXOCAIXA_REALIZADO.FieldList[i] as TBCDField).DisplayFormat := '#,##0.00' ;

          if (objRel.SP_FLUXOCAIXA_REALIZADO.FieldList[i].FieldName = '_GT#Outros#') then
              objRel.cxGrid1DBTableView1.Columns[i].Caption := 'Outros'
          else
          begin

              qryColunaGrupoTotalCC.Close;
              qryColunaGrupoTotalCC.Parameters.ParamByName('nCdGrupoTotalCC').Value       := frmMenu.ConvInteiro(edtGrupoTotalizador.Text) ;
              qryColunaGrupoTotalCC.Parameters.ParamByName('nCdColunaGrupoTotalCC').Value := strToInt( Copy(objRel.SP_FLUXOCAIXA_REALIZADO.FieldList[i].FieldName,5,10) ) ;
              qryColunaGrupoTotalCC.Open;

              if not qryColunaGrupoTotalCC.eof then
                  objRel.cxGrid1DBTableView1.Columns[i].Caption := qryColunaGrupoTotalCCcNmColunaGrupoTotalCC.Value ;

              qryColunaGrupoTotalCC.Close ;

          end ;

          objRel.cxGrid1DBTableView1.Columns[i].Width   := 80 ;

          with objRel.cxGrid1DBTableView1.Columns[i].Summary do
          begin
              GroupFooterKind   := skSum ;
              GroupFooterFormat := '#,##0.00' ;
          end ;

      end ;

  end ;


  objRel.Caption := 'Fluxo de Caixa Realizado.   Per�odo de : ' + MaskEdit1.Text + ' at� ' + MaskEdit2.Text + '   Empresa: ' + frmMenu.cNmEmpresaAtiva  ;
  objRel.cFiltro := 'Per�odo de Movimenta��o: ' + MaskEdit1.Text + ' at� ' + MaskEdit2.Text ;

  if ((frmMenu.LeParametro('VAREJO')='S') and (Trim(edtLojaEmissora.Text) <> '')) then
      objRel.cFiltro := (objRel.cFiltro + ' - Loja Emissora: ' + frmMenu.ZeroEsquerda(Trim(edtLojaEmissora.Text),3));

  if (DBEdit5.Text <> '') then
      objRel.cFiltro := (objRel.cFiltro + ' - Centro de Custo: ' + Trim(edtCdCC.Text) + ' - ' + Trim(DBEdit5.Text)) + ' (' + qryCentroCustoiNivel.asString + ')' ;

  if checkBox1.Checked then objRel.imprimeSaldoFinal := True
  else objRel.imprimeSaldoFinal := False ;

  showForm( objRel , TRUE ) ;

end;

procedure TrptFluxoCaixaRealizado.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                MaskEdit4.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;
end;

procedure TrptFluxoCaixaRealizado.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
            if(frmMenu.LeParametro('VAREJO')='S') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(59);

                If (nPK > 0) then
                begin
                    MaskEdit5.Text := IntToStr(nPK) ;
                end ;
            end;

    end ;

  end ;
end;

procedure TrptFluxoCaixaRealizado.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit4.Text ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryLoja, MaskEdit5.Text) ;
end;

procedure TrptFluxoCaixaRealizado.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEmpresa, MaskEdit4.Text) ;
end;

procedure TrptFluxoCaixaRealizado.FormShow(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO')='N') then
  begin

      edtLojaEmissora.ReadOnly   := True ;
      edtLojaEmissora.Color      := $00E9E4E4 ;
      edtLojaEmissora.Font.Color := clBlack ;
      edtLojaEmissora.TabStop    := False ;

      MaskEdit5.ReadOnly   := True ;
      MaskEdit5.Color      := $00E9E4E4 ;
      MaskEdit5.Font.Color := clBlack ;
      MaskEdit5.TabStop    := False ;
      Update;

  end;

  MaskEdit4.Text := intToStr(frmMenu.nCdEmpresaAtiva) ;
  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEmpresa, MaskEdit4.Text) ;

end;

procedure TrptFluxoCaixaRealizado.edtLojaEmissoraBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  qryLojaEmissora.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit4.Text ;
  qryLojaEmissora.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
end;

initialization
    RegisterClass(TrptFluxoCaixaRealizado) ;

end.
