unit rFichaCobradora_Individual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, DB, Mask, DBCtrls, ER2Lookup, ADODB,
  ImgList, ComCtrls, ToolWin, ExtCtrls, cxLookAndFeelPainters, cxButtons;

type
  TrelFichaCobradora_Individual = class(TfrmProcesso_Padrao)
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    qryCobradora: TADOQuery;
    dsCobradora: TDataSource;
    qryCobradoracNmCobradora: TStringField;
    Label2: TLabel;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  relFichaCobradora_Individual: TrelFichaCobradora_Individual;

implementation

{$R *.dfm}

uses rFichaCobradora_Individual_view,fMenu, DateUtils;

procedure TrelFichaCobradora_Individual.ToolButton1Click(Sender: TObject);
var
  objForm : TrptFichaCobradora_Individual_view;
begin
  if DBEdit2.Text = '' then
  begin
      MensagemAlerta('Cobradora n�o informada.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if ((DBEdit1.text = '') and ((MaskEdit1.Text = '  /  /    ') or (MaskEdit2.Text = '  /  /    ')))  then
  begin
      MensagemAlerta('Necess�rio informar um cliente ou um per�odo de no m�ximo de 30 dias.');
      Abort;
  end;

  if (MaskEdit1.Text <> '  /  /    ') and (MaskEdit1.Text <> '  /  /    ') then
  begin
      if frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text) then
      begin
          MensagemAlerta('Data inicial informada maior que data final.');
          MaskEdit1.SetFocus;
          Abort;
      end;

      if (DaysBetween(frmMenu.ConvData(MaskEdit2.Text),frmMenu.ConvData(MaskEdit1.Text)) > 30) then
      begin
          MensagemAlerta('Periodo informado maior que 30 dias, informe um novo periodo.');
          Abort;
      end;
  end;

  try
      ObjForm := TrptFichaCobradora_Individual_view.Create(nil);

      ObjForm.qryFicha.Close;
      ObjForm.qryFicha.Parameters.ParamByName('nCdCobradora').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);
      ObjForm.qryFicha.Parameters.ParamByName('nCdTerceiro').Value  := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);

      if (MaskEdit1.Text = '  /  /    ') or (MaskEdit1.Text = '  /  /    ') then
      begin
          ObjForm.qryFicha.Parameters.ParamByName('dDtIni').Value       := '01/01/1900';
          ObjForm.qryFicha.Parameters.ParamByName('dDtFim').Value       := '01/01/1900';
      end
      else
      begin
          ObjForm.qryFicha.Parameters.ParamByName('dDtIni').Value       := MaskEdit1.Text;
          ObjForm.qryFicha.Parameters.ParamByName('dDtFim').Value       := MaskEdit2.Text;
      end;

      ObjForm.qryFicha.Open;

      if ObjForm.qryFicha.IsEmpty then
      begin
          MensagemAlerta('Ficha cobradora n�o encontrado.');
          FreeAndNil(ObjForm);
      end
      else
      begin
          ObjForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          ObjForm.QuickRep1.PreviewModal;
      end;
  except
      MensagemErro('Erro no processamento.');
      FreeAndNil(ObjForm);
      raise;
  end;

  FreeAndNil(ObjForm);
end;

initialization
  RegisterClass(TrelFichaCobradora_Individual);
  
end.
