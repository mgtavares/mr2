unit fProdutoMRP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh;

type
  TfrmProdutoMRP = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryProdutoEmpresa: TADOQuery;
    qryProdutoEmpresanCdProdutoEmpresa: TAutoIncField;
    qryProdutoEmpresanCdProduto: TIntegerField;
    qryProdutoEmpresanCdEmpresa: TIntegerField;
    qryProdutoEmpresacNmEmpresa: TStringField;
    qryProdutoEmpresanCdLoja: TIntegerField;
    qryProdutoEmpresacNmLoja: TStringField;
    qryProdutoEmpresacFlgTipoPonto: TStringField;
    qryProdutoEmpresanEstoqueSeg: TBCDField;
    qryProdutoEmpresanEstoqueRessup: TBCDField;
    qryProdutoEmpresanEstoqueMax: TBCDField;
    qryProdutoEmpresanConsumoMedDia: TBCDField;
    qryProdutoEmpresaiDiaEntrega: TIntegerField;
    qryProdutoEmpresaiTipoEntrega: TIntegerField;
    qryProdutoEmpresanCdLocalEstoqueEntrega: TIntegerField;
    qryProdutoEmpresacNmLocalEstoque: TStringField;
    qryProdutoEmpresacFlgAtivoCompra: TIntegerField;
    dsProdutoEmpresa: TDataSource;
    DBGridEh4: TDBGridEh;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    procedure qryProdutoEmpresaCalcFields(DataSet: TDataSet);
    procedure qryProdutoEmpresaBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
    nCdEmpresa : integer ;
  end;

var
  frmProdutoMRP: TfrmProdutoMRP;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmProdutoMRP.qryProdutoEmpresaCalcFields(DataSet: TDataSet);
begin
  inherited;
  if (qryProdutoEmpresanCdEmpresa.Value > 0) then
  begin
      PosicionaQuery(qryEmpresa, qryProdutoEmpresanCdEmpresa.AsString) ;

      if not qryEmpresa.eof then
          qryProdutoEmpresacNmEmpresa.Value := qryEmpresacNmEmpresa.Value ;
  end ;

  if (qryProdutoEmpresanCdLoja.Value > 0) then
  begin
      PosicionaQuery(qryLoja, qryProdutoEmpresanCdLoja.AsString) ;

      if not qryLoja.eof then
          qryProdutoEmpresacNmLoja.Value := qryLojacNmLoja.Value ;
  end ;

  if (qryProdutoEmpresanCdLocalEstoqueEntrega.Value > 0) then
  begin
      PosicionaQuery(qryLocalEstoque, qryProdutoEmpresanCdLocalEstoqueEntrega.AsString) ;

      if not qryLocalEstoque.eof then
          qryProdutoEmpresacNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
  end ;

end;

procedure TfrmProdutoMRP.qryProdutoEmpresaBeforePost(DataSet: TDataSet);
begin
  if (qryProdutoEmpresacNmEmpresa.Value = '') then
  begin
      MensagemAlerta('Selecione a empresa.') ;
      abort ;
  end ;

  if (qryProdutoEmpresacNmLoja.Value = '') and (not DBGridEh4.Columns[4].ReadOnly) then
  begin
      MensagemAlerta('Selecione a loja.') ;
      abort ;
  end ;

  if (qryProdutoEmpresacFlgTipoPonto.Value <> 'D') and (qryProdutoEmpresacFlgTipoPonto.Value <> 'Q') then
  begin
      MensagemAlerta('Informe o tipo do gatilho para compra.') ;
      abort ;
  end ;

  if (qryProdutoEmpresaiTipoEntrega.Value <> 0) and (qryProdutoEmpresaiTipoEntrega.Value <> 1) and (qryProdutoEmpresaiDiaEntrega.Value > 0) then
  begin
      MensagemAlerta('Informe o tipo de c�lculo de entrega.') ;
      abort ;
  end ;

  inherited;

  qryProdutoEmpresanCdProduto.Value := nCdProduto ;

end;

procedure TfrmProdutoMRP.FormShow(Sender: TObject);
begin
  inherited;
  DBGridEh4.Columns[4].ReadOnly := True ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin

      DBGridEh4.Columns[4].ReadOnly := False ;

  end ;

end;

procedure TfrmProdutoMRP.DBGridEh4KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryProdutoEmpresa.State = dsBrowse) then
             qryProdutoEmpresa.Edit ;

        if (qryProdutoEmpresa.State = dsInsert) or (qryProdutoEmpresa.State = dsEdit) then
        begin

            if (DBGridEh4.Col = 5) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(147,'Loja.nCdEmpresa = ' + qryProdutoEmpresanCdEmpresa.asString);

                If (nPK > 0) then
                    qryProdutoEmpresanCdLoja.Value := nPK ;

            end ;

            if (DBGridEh4.Col = 15) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + qryProdutoEmpresanCdEmpresa.asString);

                If (nPK > 0) then
                    qryProdutoEmpresanCdLocalEstoqueEntrega.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

end.
