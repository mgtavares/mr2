inherited frmHistoricoLimiteCredito: TfrmHistoricoLimiteCredito
  Left = 193
  Top = 239
  Width = 969
  BorderIcons = [biSystemMenu]
  Caption = 'Hist'#243'rico Limite Cr'#233'dito'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 953
  end
  inherited ToolBar1: TToolBar
    Width = 953
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 953
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsLimite
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1dDtConcessao: TcxGridDBColumn
        Caption = 'Data Concess'#227'o'
        DataBinding.FieldName = 'dDtConcessao'
        Width = 150
      end
      object cxGrid1DBTableView1nValLimite: TcxGridDBColumn
        Caption = 'Limite Concedido'
        DataBinding.FieldName = 'nValLimite'
        HeaderAlignmentHorz = taRightJustify
        Width = 153
      end
      object cxGrid1DBTableView1nPercEntrada: TcxGridDBColumn
        Caption = '% Entrada'
        DataBinding.FieldName = 'nPercEntrada'
        HeaderAlignmentHorz = taRightJustify
        Width = 101
      end
      object cxGrid1DBTableView1cAutomatico: TcxGridDBColumn
        Caption = 'Calculo Automatico'
        DataBinding.FieldName = 'cAutomatico'
        Width = 157
      end
      object cxGrid1DBTableView1cOBS: TcxGridDBColumn
        Caption = 'Observa'#231#227'o'
        DataBinding.FieldName = 'cOBS'
        Width = 175
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio Concess'#227'o'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 209
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryLimite: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtConcessao'
      '      ,nValLimite'
      '      ,nPercEntrada'
      '      ,CASE WHEN cFlgAutomatico = 1 THEN '#39'SIM'#39
      '            ELSE '#39'NAO'#39
      '       END cAutomatico'
      '      ,cOBS'
      '      ,cNmUsuario'
      '  FROM LimiteCredito'
      
        '       LEFT JOIN Usuario ON Usuario.nCdUsuario = LimiteCredito.n' +
        'CdUsuario'
      ' WHERE nCdTerceiro =  :nPK'
      ' ORDER BY dDtConcessao DESC')
    Left = 248
    Top = 152
    object qryLimitedDtConcessao: TDateTimeField
      FieldName = 'dDtConcessao'
    end
    object qryLimitenValLimite: TBCDField
      FieldName = 'nValLimite'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLimitenPercEntrada: TBCDField
      FieldName = 'nPercEntrada'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLimitecAutomatico: TStringField
      FieldName = 'cAutomatico'
      ReadOnly = True
      Size = 3
    end
    object qryLimitecOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryLimitecNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsLimite: TDataSource
    DataSet = qryLimite
    Left = 312
    Top = 144
  end
end
