unit fManutPedido_BaixaManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmManutPedido_BaixaManual = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemPedido: TADOQuery;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdPedido: TIntegerField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdePed: TBCDField;
    qryItemPedidonQtdeExpRec: TBCDField;
    qryItemPedidonQtdeCanc: TBCDField;
    dsItemPedido: TDataSource;
    qryAtualizaPedido: TADOQuery;
    ToolButton4: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido : integer ;
  end;

var
  frmManutPedido_BaixaManual: TfrmManutPedido_BaixaManual;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmManutPedido_BaixaManual.FormShow(Sender: TObject);
begin
  inherited;

  qryItemPedidonQtdePed.DisplayFormat    := frmMenu.cMascaraCompras ;
  qryItemPedidonQtdeExpRec.DisplayFormat := frmMenu.cMascaraCompras ;
  qryItemPedidonQtdeCanc.DisplayFormat   := frmMenu.cMascaraCompras ;

end;

procedure TfrmManutPedido_BaixaManual.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a atualização das quantidades ?',mtConfirmation,[mbYes,mbNo],0)=MRNO) then
      exit ;

  frmMenu.Connection.BeginTrans;
  try
      qryItemPedido.UpdateBatch();

      qryAtualizaPedido.Parameters.ParamByName('nPK').Value := nCdPedido ;
      qryAtualizaPedido.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  close ;

end;

procedure TfrmManutPedido_BaixaManual.ToolButton4Click(Sender: TObject);
begin

  if (MessageDlg('Confirma a baixa de todos os itens ?',mtConfirmation,[mbYes,mbNo],0)=MRNO) then
      exit ;

  qryItemPedido.First ;

  while not qryItemPedido.eof do
  begin
      qryItempedido.edit ;
      qryItemPedidonQtdeExprec.Value := (qryItemPedidonQtdePed.Value - qryItemPedidonQtdeCanc.Value) ;
      qryItempedido.post ;
      qryItemPedido.Next ;
  end ;

  qryItemPedido.First ;

end;

end.
