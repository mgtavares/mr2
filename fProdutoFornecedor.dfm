inherited frmProdutoFornecedor: TfrmProdutoFornecedor
  Left = 44
  Top = 188
  Width = 1152
  Height = 445
  Caption = 'Produto - Fornecedores'
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1144
    Height = 385
  end
  inherited ToolBar1: TToolBar
    Width = 1144
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh2: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1144
    Height = 385
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsProdutoFornecedor
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnColEnter = DBGridEh2ColEnter
    OnKeyUp = DBGridEh2KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProdutoFornecedor'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 164
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoRessuprimentoFornec'
        Footers = <>
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoRessuprimento'
        Footers = <>
        ReadOnly = True
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'nPercentCompra'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nPrecoUnit'
        Footers = <>
        Tag = 1
        Width = 79
      end
      item
        EditButtons = <>
        FieldName = 'nPrecoUnitEsp'
        Footers = <>
        Tag = 1
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'nPercIPI'
        Footers = <>
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'nPercICMSSub'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nPercBCICMSSub'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nPercIVA'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nPercCredICMS'
        Footers = <>
        Width = 68
      end
      item
        AutoDropDown = True
        EditButtons = <>
        FieldName = 'cFlgDescST'
        Footers = <>
        KeyList.Strings = (
          '0'
          '1')
        PickList.Strings = (
          'N'#227'o'
          'Sim')
      end
      item
        EditButtons = <>
        FieldName = 'nCdMoeda'
        Footers = <>
        Width = 29
      end
      item
        EditButtons = <>
        FieldName = 'cNmMoeda'
        Footers = <>
        ReadOnly = True
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nCdCondPagto'
        Footers = <>
        Width = 26
      end
      item
        EditButtons = <>
        FieldName = 'cNmCondPagto'
        Footers = <>
        ReadOnly = True
        Width = 116
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoPedidoCompra'
        Footers = <>
        Width = 30
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoPedido'
        Footers = <>
        ReadOnly = True
        Width = 144
      end
      item
        EditButtons = <>
        FieldName = 'cNrContrato'
        Footers = <>
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeIni'
        Footers = <>
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeFim'
        Footers = <>
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'cCatalogo'
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'cReferencia'
        Footers = <>
        Width = 72
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Top = 144
  end
  object qryProdutoFornecedor: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryProdutoFornecedorBeforePost
    OnCalcFields = qryProdutoFornecedorCalcFields
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ProdutoFornecedor'
      'WHERE nCdProduto = :nPK')
    Left = 488
    Top = 248
    object qryProdutoFornecedornCdProdutoFornecedor: TAutoIncField
      FieldName = 'nCdProdutoFornecedor'
      ReadOnly = True
    end
    object qryProdutoFornecedornCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoFornecedornCdTerceiro: TIntegerField
      DisplayLabel = 'Fornecedor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryProdutoFornecedorcNmTerceiro: TStringField
      DisplayLabel = 'Fornecedor|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryProdutoFornecedornPercentCompra: TBCDField
      DisplayLabel = '%|Pedido'
      FieldName = 'nPercentCompra'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedorcNrContrato: TStringField
      DisplayLabel = 'N'#250'mero|Contrato'
      FieldName = 'cNrContrato'
      Size = 15
    end
    object qryProdutoFornecedornCdCondPagto: TIntegerField
      DisplayLabel = 'Condi'#231#227'o Pagto|C'#243'd'
      FieldName = 'nCdCondPagto'
    end
    object qryProdutoFornecedorcNmCondPagto: TStringField
      DisplayLabel = 'Condi'#231#227'o Pagto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCondPagto'
      Size = 50
      Calculated = True
    end
    object qryProdutoFornecedordDtValidadeIni: TDateTimeField
      DisplayLabel = 'Validade Contrato|In'#237'cio'
      FieldName = 'dDtValidadeIni'
      EditMask = '99/99/9999;1;_'
    end
    object qryProdutoFornecedordDtValidadeFim: TDateTimeField
      DisplayLabel = 'Validade Contrato|Fim'
      FieldName = 'dDtValidadeFim'
      EditMask = '99/99/9999;1;_'
    end
    object qryProdutoFornecedornCdMoeda: TIntegerField
      DisplayLabel = 'Moeda|C'#243'd'
      FieldName = 'nCdMoeda'
    end
    object qryProdutoFornecedorcNmMoeda: TStringField
      DisplayLabel = 'Moeda|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmMoeda'
      Size = 50
      Calculated = True
    end
    object qryProdutoFornecedornPercIPI: TBCDField
      DisplayLabel = 'Valor|% IPI'
      FieldName = 'nPercIPI'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedorcReferencia: TStringField
      DisplayLabel = 'Cat'#225'logo|Refer'#234'ncia'
      FieldName = 'cReferencia'
    end
    object qryProdutoFornecedorcCatalogo: TStringField
      DisplayLabel = 'Cat'#225'logo|N'#250'mero'
      FieldName = 'cCatalogo'
    end
    object qryProdutoFornecedornCdTipoPedidoCompra: TIntegerField
      DisplayLabel = 'Tipo de Pedido de Compra|C'#243'd'
      FieldName = 'nCdTipoPedidoCompra'
    end
    object qryProdutoFornecedorcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo de Pedido de Compra|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTipoPedido'
      Size = 50
      Calculated = True
    end
    object qryProdutoFornecedornCdTipoRessuprimentoFornec: TIntegerField
      DisplayLabel = 'Tipo Ressuprimento|C'#243'd'
      FieldName = 'nCdTipoRessuprimentoFornec'
    end
    object qryProdutoFornecedorcNmTipoRessuprimento: TStringField
      DisplayLabel = 'Tipo Ressuprimento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTipoRessuprimento'
      Size = 50
      Calculated = True
    end
    object qryProdutoFornecedornPercBCICMSSub: TBCDField
      DisplayLabel = 'Substitui'#231#227'o Tribut'#225'ria|% Base Calc.'
      FieldName = 'nPercBCICMSSub'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercIVA: TBCDField
      DisplayLabel = 'Substitui'#231#227'o Tribut'#225'ria|% IVA'
      FieldName = 'nPercIVA'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercICMSSub: TBCDField
      DisplayLabel = 'Substitui'#231#227'o Tribut'#225'ria|Aliq. ICMS Interna'
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercCredICMS: TBCDField
      DisplayLabel = 'Substitui'#231#227'o Tribut'#225'ria|% Cr'#233'dito ICMS'
      FieldName = 'nPercCredICMS'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedorcFlgDescST: TIntegerField
      DisplayLabel = 'Substitui'#231#227'o Tribut'#225'ria|Descontar ST'
      FieldName = 'cFlgDescST'
    end
    object qryProdutoFornecedornPrecoUnit: TFloatField
      DisplayLabel = 'Valor|Unit'#225'rio s/ Imposto'
      FieldName = 'nPrecoUnit'
    end
    object qryProdutoFornecedornPrecoUnitEsp: TFloatField
      DisplayLabel = 'Valor|Esperado'
      FieldName = 'nPrecoUnitEsp'
    end
  end
  object dsProdutoFornecedor: TDataSource
    DataSet = qryProdutoFornecedor
    Left = 528
    Top = 248
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMoeda, cNmMoeda'
      'FROM Moeda'
      'WHERE nCdMoeda = :nPK')
    Left = 528
    Top = 280
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 488
    Top = 280
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCondPagto, cNmCondPagto'
      'FROM CondPagto'
      'WHERE nCdCondPagto = :nPK'
      'AND cFlgPDV = 0')
    Left = 488
    Top = 312
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido, cNmTipoPedido'
      'FROM TipoPedido'
      'WHERE nCdTipoPedido = :nPK'
      'AND cFlgCompra = 1')
    Left = 592
    Top = 280
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryTipoRessuprimento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM TipoRessuprimento'
      'WHERE nCdTipoRessuprimento = :nPK')
    Left = 672
    Top = 176
    object qryTipoRessuprimentonCdTipoRessuprimento: TIntegerField
      FieldName = 'nCdTipoRessuprimento'
    end
    object qryTipoRessuprimentocNmTipoRessuprimento: TStringField
      FieldName = 'cNmTipoRessuprimento'
      Size = 50
    end
  end
end
