unit fConsultaNotaFiscalWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, Mask, DBCtrls;

type
  TfrmConsultaNotaFiscalWERP = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    Edit2: TEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TAutoIncField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalcNrNf: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalcCidade: TStringField;
    qryDoctoFiscalcStatus: TStringField;
    dsDoctoFiscal: TDataSource;
    cxGrid1DBTableView1nCdDoctoFiscal: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNf: TcxGridDBColumn;
    cxGrid1DBTableView1cTextoCFOP: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cCidade: TcxGridDBColumn;
    cxGrid1DBTableView1cStatus: TcxGridDBColumn;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaNotaFiscalWERP: TfrmConsultaNotaFiscalWERP;

implementation

uses fMenu, rRomaneio;

{$R *.dfm}

procedure TfrmConsultaNotaFiscalWERP.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(MaskEdit1.Text) = '/  /') then
  begin
      MaskEdit1.Text := DateToStr(Date-7) ;
      MaskEdit2.Text := DateToStr(Date) ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
      MaskEdit2.Text := DateToStr(Date) ;

  qryDoctoFiscal.Close ;
  qryDoctoFiscal.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.nCdTerceiroUsuario;
  qryDoctoFiscal.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryDoctoFiscal.Parameters.ParamByName('iNrNf').Value       := frmMenu.ConvInteiro(Edit2.Text);
  qryDoctoFiscal.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(Maskedit1.Text);
  qryDoctoFiscal.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text);
  qryDoctoFiscal.Open  ;

  if qryDoctoFiscal.eof then
      ShowMessage('Nenhum documento encontrado para o crit�rio utilizado.') ;

end;

procedure TfrmConsultaNotaFiscalWERP.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  PosicionaQuery(rptRomaneio.qryDoctoFiscal, qryDoctoFiscalnCdDoctoFiscal.AsString) ;
  rptRomaneio.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;
  rptRomaneio.QuickRep1.PreviewModal;

end;

procedure TfrmConsultaNotaFiscalWERP.FormShow(Sender: TObject);
begin
  inherited;

  MaskEdit3.Text := intToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, MaskEdit3.Text) ;
  
end;

initialization
    RegisterClass(TfrmConsultaNotaFiscalWERP) ;
    
end.
