inherited frmTipoResultadoContato: TfrmTipoResultadoContato
  Left = 299
  Top = 256
  Height = 233
  Caption = 'Tipo de Resultado de Contato'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 170
  end
  object Label1: TLabel [1]
    Left = 75
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 64
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 19
    Top = 110
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente Contatado'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [4]
    Left = 87
    Top = 134
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ativo'
    FocusControl = DBEdit2
  end
  object Label5: TLabel [5]
    Left = 2
    Top = 86
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Bloqueio Cliente'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 116
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTipoResultadoContato'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Left = 116
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTipoResultadoContato'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBLookupComboBox1: TDBLookupComboBox [9]
    Left = 116
    Top = 104
    Width = 65
    Height = 19
    DataField = 'cFlgContatado'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsFlgAtivo
    TabOrder = 4
  end
  object DBLookupComboBox2: TDBLookupComboBox [10]
    Left = 116
    Top = 128
    Width = 65
    Height = 19
    DataField = 'cFlgAtivo'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsFlgContatado
    TabOrder = 5
  end
  object DBEdit3: TDBEdit [11]
    Left = 116
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoRestricaoVenda'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [12]
    Tag = 1
    Left = 184
    Top = 80
    Width = 582
    Height = 19
    DataField = 'cNmTabTipoRestricaoVenda'
    DataSource = dsTabTipoRestricaoVenda
    TabOrder = 6
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoResultadoContato'
      'WHERE nCdTipoResultadoContato = :nPK')
    Left = 520
    object qryMasternCdTipoResultadoContato: TIntegerField
      FieldName = 'nCdTipoResultadoContato'
    end
    object qryMastercNmTipoResultadoContato: TStringField
      FieldName = 'cNmTipoResultadoContato'
      Size = 50
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMastercFlgContatado: TIntegerField
      FieldName = 'cFlgContatado'
    end
    object qryMasternCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
  end
  inherited dsMaster: TDataSource
    Left = 520
  end
  object dsFlgAtivo: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 648
    Top = 160
  end
  object dsFlgContatado: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 680
    Top = 160
  end
  object qryTabTipoRestricaoVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoRestricaoVenda'
      '      ,cNmTabTipoRestricaoVenda'
      '  FROM TabTipoRestricaoVenda'
      ' WHERE nCdTabTipoRestricaoVenda = :nPK')
    Left = 552
    Top = 128
    object qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
    object qryTabTipoRestricaoVendacNmTabTipoRestricaoVenda: TStringField
      FieldName = 'cNmTabTipoRestricaoVenda'
      Size = 50
    end
  end
  object dsTabTipoRestricaoVenda: TDataSource
    DataSet = qryTabTipoRestricaoVenda
    Left = 552
    Top = 160
  end
end
