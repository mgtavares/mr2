unit fCaixa_ListaParcelas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, jpeg,
  cxCheckBox, cxMaskEdit, cxDropDownEdit, cxCalendar, cxCurrencyEdit,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmCaixa_ListaParcelas = class(TfrmProcesso_Padrao)
    qryParcelas: TADOQuery;
    qryParcelasnCdTitulo: TIntegerField;
    qryParcelascNrTit: TStringField;
    qryParcelascNmEspTit: TStringField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelasnValJuro: TBCDField;
    qryParcelasnValDesconto: TBCDField;
    qryParcelasnSaldoTit: TBCDField;
    qryParcelascNmLoja: TStringField;
    dsParcelas: TDataSource;
    qryParcelascFlgMarcado: TIntegerField;
    qryParcelasiDiasAtraso: TIntegerField;
    qryParcelascFlgEntrada: TIntegerField;
    qryParcelasnCdCrediario: TIntegerField;
    qryParcelasiParcela: TIntegerField;
    Image2: TImage;
    Label1: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgMarcado: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasAtraso: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgEntrada: TcxGridDBColumn;
    cxGrid1DBTableView1nCdCrediario: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    qryParcelasdDtEmissao: TDateTimeField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    Atraso: TcxStyle;
    GroupBox1: TGroupBox;
    edtTotalVencido: TcxCurrencyEdit;
    edtTotalVencendo: TcxCurrencyEdit;
    edtTotalVencer: TcxCurrencyEdit;
    edtTotalGeral: TcxCurrencyEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edtDtPagto: TDateTimePicker;
    qrySaldoTotal: TADOQuery;
    qryTotalVencer: TADOQuery;
    qryTotalVencendo: TADOQuery;
    qryTotalVencido: TADOQuery;
    qryTotalVencidonTotal: TBCDField;
    qrySaldoTotalnTotal: TBCDField;
    qryTotalVencernTotal: TBCDField;
    qryTotalVencendonTotal: TBCDField;
    Label6: TLabel;
    qryAux: TADOQuery;
    btHoje: TcxButton;
    qryVerificaParcelaAnterior: TADOQuery;
    qryParcelasnCdTerceiro: TIntegerField;
    qryParcelascFlgDescontoJuros: TIntegerField;
    qryParcelasnValJurosDescontado: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryTotalSelecionado: TADOQuery;
    qryTotalSelecionadonTotal: TBCDField;
    qryParcelasnValTotal: TBCDField;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1EditKeyUp(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure edtDtPagtoCloseUp(Sender: TObject);
    procedure edtDtPagtoKeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton2Click(Sender: TObject);
    procedure edtTotalVencidoPropertiesChange(Sender: TObject);
    procedure edtTotalVencendoPropertiesChange(Sender: TObject);
    procedure btHojeClick(Sender: TObject);
    procedure atualizaTotais;
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_ListaParcelas: TfrmCaixa_ListaParcelas;

implementation

uses fCaixa_Recebimento, fCaixa_CredErro;

{$R *.dfm}

procedure TfrmCaixa_ListaParcelas.ToolButton1Click(Sender: TObject);
var
  objCaixa_CredErro : TfrmCaixa_CredErro;
begin
  inherited;

  if (qryParcelas.State = dsEdit) then
      qryParcelas.Post ;

  try
      objCaixa_CredErro := TfrmCaixa_CredErro.Create(nil);

      objCaixa_CredErro.qryVerificaCrediario.Close;
      objCaixa_CredErro.qryVerificaCrediario.Open;

      if (not objCaixa_CredErro.qryVerificaCrediario.IsEmpty) then
      begin
          MensagemAlerta('Existem informa��es inconsistentes no(s) credi�rio(s) selecionado(s).');
          objCaixa_CredErro.ShowModal;

          if (MessageDlg('Credi�rio(s) selecionado(s) n�o possui todas as parcelas sincronizadas no sistema. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
              Abort;
      end;
          
  finally
      FreeAndNil(objCaixa_CredErro);
  end;

  {-- em alguns casos o bot�o pode estar desativado para clicar mas o evento ser disparado --}
  {-- por teclas de atalho, ent�o, verifica se o bot�o est� desativado antes de prosseguir --}
  
  if not ToolButton1.Enabled then
      exit ;

  {-- verifica se as parcelas foram selecionadas na sequencia --}
  {-- isso garante que a operadora de caixa n�o selecione a parcela 2 sem ter selecionado a parcela 1 --}

  qryParcelas.First ;

  while not qryParcelas.eof do
  begin

      if (qryParcelascFlgMarcado.Value = 1) then
      begin

          qryVerificaParcelaAnterior.Close;
          qryVerificaParcelaAnterior.Parameters.ParamByName('nCdTerceiro').Value  := qryParcelasnCdTerceiro.Value ;
          qryVerificaParcelaAnterior.Parameters.ParamByName('cNrTit').Value       := qryParcelascNrTit.Value ;
          qryVerificaParcelaAnterior.Parameters.ParamByName('nCdCrediario').Value := qryParcelasnCdCrediario.Value ;
          qryVerificaParcelaAnterior.Parameters.ParamByName('iParcela').Value     := qryParcelasiParcela.Value ;
          qryVerificaParcelaAnterior.Open;

          if not qryVerificaParcelaAnterior.eof then
          begin
              MensagemAlerta('N�o � poss�vel selecionar o carnet ' + qryParcelascNrTit.Value + ' Parcela: ' + qryParcelasiParcela.asString + ' sem selecionar a parcela anterior deste carnet.') ;
              abort ;
          end ;

      end ;

      qryParcelas.Next ;
  end ;

  qryParcelas.First ;

  Close ;
end;

procedure TfrmCaixa_ListaParcelas.DBGridEh1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  case key of
      #9: if (qryParcelas.Active) and not (qryParcelas.eof) then
              ToolButton1.Click;
  end ;

end;

procedure TfrmCaixa_ListaParcelas.FormShow(Sender: TObject);
begin
  inherited;

  edtDtPagto.Date     := Now;
  ToolButton1.Enabled := true;

  atualizaTotais();

  cxGrid1.SetFocus;
  
end;

procedure TfrmCaixa_ListaParcelas.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
        vk_f4: ToolButton1.Click;
        vk_f6: ToolButton2.Click;
        vk_f7: ToolButton5.Click;
  end ;

end;

procedure TfrmCaixa_ListaParcelas.cxGrid1DBTableView1EditKeyUp(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
        vk_space: begin
          if (qryParcelas.Active) and not (qryParcelas.eof) then
          begin
              qryParcelas.Edit ;
              qryParcelascFlgMarcado.Value := 1 ;
              qryParcelas.Post ;
              qryParcelas.Next ;
          end ;
        end ;
  end ;
end;

procedure TfrmCaixa_ListaParcelas.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem <> cxGrid1DBTableView1cFlgMarcado) then
      if (StrToFloat(ARecord.Values[6]) > 0) then
          AStyle := Atraso ;

end;

procedure TfrmCaixa_ListaParcelas.edtDtPagtoCloseUp(Sender: TObject);
begin
  inherited;

  if (edtDtPagto.Date < Date()) then
  begin
      edtDtPagto.Date := Date;
      MensagemAlerta('A data de pagamento n�o pode ser menor que hoje. A data de pagamento foi alterada automaticamente.') ;
  end;

  qryParcelas.Close;
  qryParcelas.Parameters.ParamByName('dDtSimulacao').Value := DateToStr(edtDtPagto.Date);
  qryParcelas.Open;

  atualizaTotais();

end;

procedure TfrmCaixa_ListaParcelas.edtDtPagtoKeyPress(Sender: TObject;
  var Key: Char);
begin
  {Cancelado o evento para bloquear a altera��o da data manualmente,
  obrigando o usu�rio a selecion�-la no calend�rio}

  {inherited;}
  abort;

end;

procedure TfrmCaixa_ListaParcelas.ToolButton2Click(Sender: TObject);
begin
  qryAux.Close;
  qryAux.ExecSQL;

  inherited;
end;           

procedure TfrmCaixa_ListaParcelas.edtTotalVencidoPropertiesChange(
  Sender: TObject);
begin
  inherited;

  if (edtTotalVencido.Value > 0) then
  begin
      edtTotalVencido.Style.Color      := clRed ;
      edtTotalVencido.Style.Font.Color := clWhite ;
  end
  else
  begin
      edtTotalVencido.Style.Color      := clWhite ;
      edtTotalVencido.Style.Font.Color := clBlack ;
  end ;

end;

procedure TfrmCaixa_ListaParcelas.edtTotalVencendoPropertiesChange(
  Sender: TObject);
begin
  inherited;

  if (edtTotalVencendo.Value > 0) then
  begin
      edtTotalVencendo.Style.Color      := clBlue ;
      edtTotalVencendo.Style.Font.Color := clWhite ;
  end
  else
  begin
      edtTotalVencendo.Style.Color      := clWhite ;
      edtTotalVencendo.Style.Font.Color := clBlack ;
  end ;

end;

procedure TfrmCaixa_ListaParcelas.btHojeClick(Sender: TObject);
begin

  edtDtPagto.DateTime := Date ;
  edtDtPagtoCloseUp(nil) ;

end;

procedure TfrmCaixa_ListaParcelas.atualizaTotais;
begin
  ToolButton1.Enabled := (edtDtPagto.Date = Date);

  qryTotalVencido.Close;
  qryTotalVencido.Parameters.ParamByName('dDtSimulacao').Value := DateToStr(edtDtPagto.Date);
  qryTotalVencido.Open;

  qryTotalVencendo.Close;
  qryTotalVencendo.Parameters.ParamByName('dDtSimulacao').Value := DateToStr(edtDtPagto.Date);
  qryTotalVencendo.Open;

  qryTotalVencer.Close;
  qryTotalVencer.Parameters.ParamByName('dDtSimulacao').Value := DateToStr(edtDtPagto.Date);
  qryTotalVencer.Open;

  qrySaldoTotal.Close;
  qrySaldoTotal.Open;

  edtTotalVencido.Value  := qryTotalVencidonTotal.Value;
  edtTotalVencendo.Value := qryTotalVencendonTotal.Value;
  edtTotalVencer.Value   := qryTotalVencernTotal.Value;
  edtTotalGeral.Value    := qrySaldoTotalnTotal.Value;

end;

procedure TfrmCaixa_ListaParcelas.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if (qryParcelas.State = dsEdit) then
      qryParcelas.Post ;
  
  qryTotalSelecionado.Close;
  qryTotalSelecionado.Open;

  if not qryTotalSelecionado.Active or (qryTotalSelecionadonTotal.Value = 0) then
  begin
      MensagemAlerta('Nenhuma parcela selecionada.') ;
      abort ;
  end ;

  ShowMessage(Format('Total Seleciondao: %m', [qryTotalSelecionadonTotal.Value]));

  cxGrid1.SetFocus;

end;

end.
