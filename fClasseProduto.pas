unit fClasseProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmClasseProduto = class(TfrmCadastro_Padrao)
    qryMasternCdClasseProduto: TAutoIncField;
    qryMastercNmClasseProduto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClasseProduto: TfrmClasseProduto;

implementation

{$R *.dfm}

procedure TfrmClasseProduto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CLASSEPRODUTO' ;
  nCdTabelaSistema  := 45 ;
  nCdConsultaPadrao := 53 ;
end;

procedure TfrmClasseProduto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

initialization
    RegisterClass(TfrmClasseProduto) ;
    
end.
