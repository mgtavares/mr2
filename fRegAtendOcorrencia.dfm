inherited frmRegAtendOcorrencia: TfrmRegAtendOcorrencia
  Left = 145
  Top = 50
  Width = 1198
  Height = 648
  Caption = 'Registro/Atendimendo - Ocorr'#234'ncia'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1182
    Height = 585
  end
  object Label1: TLabel [1]
    Left = 77
    Top = 48
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 72
    Top = 72
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 94
    Top = 95
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 79
    Top = 120
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 34
    Top = 144
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Ocorr'#234'ncia'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 15
    Top = 167
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Resumo Ocorr'#234'ncia'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 7
    Top = 190
    Width = 108
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Ocorr'#234'ncia'
    FocusControl = DBMemo1
  end
  object Label8: TLabel [8]
    Left = 523
    Top = 119
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'CPF/CNPJ'
    FocusControl = DBEdit4
  end
  object Label9: TLabel [9]
    Left = 512
    Top = 143
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Atend.'
    FocusControl = DBEdit11
  end
  object Label10: TLabel [10]
    Left = 732
    Top = 48
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Ocorr'#234'ncia'
    FocusControl = DBEdit13
  end
  object Label11: TLabel [11]
    Left = 724
    Top = 190
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status Ocorr'#234'ncia'
  end
  object Label13: TLabel [12]
    Left = 726
    Top = 96
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Cadastro'
  end
  object Label15: TLabel [13]
    Left = 940
    Top = 70
    Width = 6
    Height = 13
    Caption = #224
    FocusControl = DBEdit18
  end
  object Label16: TLabel [14]
    Left = 731
    Top = 167
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Finaliza'#231#227'o'
    FocusControl = DBEdit19
  end
  object Label17: TLabel [15]
    Left = 736
    Top = 71
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Atend.'
    FocusControl = DBEdit4
  end
  object Label12: TLabel [16]
    Left = 708
    Top = 120
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Respons'#225'vel'
    FocusControl = DBEdit21
  end
  object Label14: TLabel [17]
    Left = 715
    Top = 144
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Finaliza'#231#227'o'
  end
  inherited ToolBar2: TToolBar
    Width = 1182
    ButtonWidth = 106
    inherited ToolButton7: TToolButton
      Left = 106
    end
    inherited ToolButton1: TToolButton
      Left = 114
    end
    inherited ToolButton9: TToolButton
      Left = 220
      Visible = False
    end
    inherited btAlterar: TToolButton
      Left = 228
    end
    inherited ToolButton2: TToolButton
      Left = 334
    end
    inherited btSalvar: TToolButton
      Left = 342
    end
    inherited ToolButton6: TToolButton
      Left = 448
    end
    inherited btExcluir: TToolButton
      Left = 456
    end
    inherited ToolButton50: TToolButton
      Left = 562
    end
    inherited btCancelar: TToolButton
      Left = 570
    end
    inherited ToolButton11: TToolButton
      Left = 676
    end
    inherited btConsultar: TToolButton
      Left = 684
    end
    inherited ToolButton8: TToolButton
      Left = 790
    end
    inherited ToolButton5: TToolButton
      Left = 798
    end
    inherited ToolButton4: TToolButton
      Left = 904
    end
    inherited btnAuditoria: TToolButton
      Left = 912
    end
    object ToolButton12: TToolButton
      Left = 1018
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object btGerarOcorrencia: TToolButton
      Left = 1026
      Top = 0
      Caption = 'Gerar Ocorr'#234'ncia'
      ImageIndex = 9
      OnClick = btGerarOcorrenciaClick
    end
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 120
    Top = 42
    Width = 65
    Height = 19
    DataField = 'nCdOcorrencia'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [20]
    Tag = 1
    Left = 120
    Top = 66
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    ReadOnly = True
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [21]
    Left = 120
    Top = 90
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [22]
    Left = 120
    Top = 114
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [23]
    Left = 120
    Top = 138
    Width = 65
    Height = 19
    DataField = 'nCdTipoOcorrencia'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [24]
    Left = 120
    Top = 162
    Width = 569
    Height = 19
    DataField = 'cResumoOcorrencia'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBMemo1: TDBMemo [25]
    Left = 120
    Top = 186
    Width = 569
    Height = 127
    DataField = 'cDescOcorrencia'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [26]
    Tag = 1
    Left = 188
    Top = 66
    Width = 501
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 8
  end
  object DBEdit8: TDBEdit [27]
    Tag = 1
    Left = 188
    Top = 90
    Width = 501
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 9
  end
  object DBEdit9: TDBEdit [28]
    Tag = 1
    Left = 188
    Top = 114
    Width = 303
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 10
  end
  object DBEdit10: TDBEdit [29]
    Tag = 1
    Left = 576
    Top = 114
    Width = 113
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 11
  end
  object DBEdit11: TDBEdit [30]
    Tag = 1
    Left = 576
    Top = 138
    Width = 113
    Height = 19
    DataField = 'iDiasAtendimento'
    DataSource = dsTipoOcorrencia
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [31]
    Tag = 1
    Left = 188
    Top = 138
    Width = 303
    Height = 19
    DataField = 'cNmTipoOcorrencia'
    DataSource = dsTipoOcorrencia
    TabOrder = 13
  end
  object cxPageControl1: TcxPageControl [32]
    Left = 120
    Top = 320
    Width = 1049
    Height = 281
    ActivePage = tabAtendimento
    LookAndFeel.NativeStyle = True
    TabOrder = 14
    ClientRectBottom = 277
    ClientRectLeft = 4
    ClientRectRight = 1045
    ClientRectTop = 24
    object tabAtendimento: TcxTabSheet
      Caption = 'Atendimento'
      ImageIndex = 0
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 47
        Width = 761
        Height = 206
        Align = alLeft
        DataGrouping.GroupLevels = <>
        DataSource = dsContatoOcorrencia
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdContatoOcorrencia'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusOcorrencia'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Status'
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'dDtAtendimento'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Data'
            Width = 90
          end
          item
            EditButtons = <>
            FieldName = 'cHoraAtendimento'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Hora'
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'cNmContatoOcorrencia'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Dados Cliente|Nome'
            Width = 130
          end
          item
            EditButtons = <>
            FieldName = 'cTelefoneContato'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Dados Cliente|Telefone'
            Width = 107
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuarioContato'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Usuario Atend.|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Title.Caption = 'Hist'#243'rico de Atendimentos|Usuario Atend.|Descri'#231#227'o'
            Width = 130
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 0
            Height = 47
            Align = alTop
            TabOrder = 0
            object Label23: TLabel
              Tag = 2
              Left = 726
              Top = 30
              Width = 76
              Height = 13
              Caption = 'Expedido Total'
            end
            object Label24: TLabel
              Tag = 2
              Left = 870
              Top = 30
              Width = 84
              Height = 13
              Caption = 'Expedido Parcial'
            end
            object Label29: TLabel
              Tag = 2
              Left = 1022
              Top = 30
              Width = 86
              Height = 13
              Caption = 'Saldo Cancelado'
            end
            object cxTextEdit1: TcxTextEdit
              Left = 694
              Top = 22
              Width = 25
              Height = 21
              Style.Color = clBlue
              StyleDisabled.Color = clBlue
              TabOrder = 0
            end
            object cxTextEdit2: TcxTextEdit
              Left = 838
              Top = 22
              Width = 25
              Height = 21
              Enabled = False
              Style.Color = clBlue
              StyleDisabled.Color = clYellow
              TabOrder = 1
            end
            object cxTextEdit3: TcxTextEdit
              Left = 990
              Top = 22
              Width = 25
              Height = 21
              Enabled = False
              Style.Color = clBlue
              StyleDisabled.Color = clRed
              TabOrder = 2
            end
            object cxButton2: TcxButton
              Left = 323
              Top = 9
              Width = 160
              Height = 33
              Caption = 'Consultar Doc. Entrada'
              TabOrder = 3
              TabStop = False
              Glyph.Data = {
                36030000424D360300000000000036000000280000000F000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
                393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
                00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
                F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
                83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
                68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
                FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
                9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
                8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
                D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
                B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
                5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
                A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
                9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
                488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
                FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
                FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
                FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
              LookAndFeel.NativeStyle = True
            end
            object btLeitura: TcxButton
              Left = 163
              Top = 9
              Width = 160
              Height = 33
              Caption = 'Leitura EAN/UPC'
              TabOrder = 4
              Glyph.Data = {
                46020000424D460200000000000036000000280000000F0000000B0000000100
                1800000000001002000000000000000000000000000000000000FFFFFF000000
                FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
                FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
                FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
                FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
                FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
                FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF7F7F7F
                FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
                FF7F7F7FFFFFFF0000000D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D
                06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF000000FFFFFF7F7F7F
                FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
                FF7F7F7FFFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
                FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
                FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
                FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
                FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
                FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
                FF000000FFFFFF000000}
              LookAndFeel.NativeStyle = True
            end
            object cxButton3: TcxButton
              Left = 3
              Top = 9
              Width = 160
              Height = 33
              Caption = 'Pre'#231'o Esperado'
              TabOrder = 5
              TabStop = False
              Glyph.Data = {
                06030000424D06030000000000003600000028000000100000000F0000000100
                180000000000D002000000000000000000000000000000000000C6DEC6C6D6DE
                C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C0000000000008C9C9C8C9C9CC6D6
                DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C00000000
                0000D6A58CD6A58C0000008C9C9CC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
                C6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFFEFDE0000008C9C9C8C9C
                9CC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFF
                EFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DE000000
                EFFFFFFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDE0000008C9C
                9C8C9C9CC6D6DEC6D6DEC6D6DE000000EFFFFFBDBDBDBDBDBDBDBDBDFFEFDEFF
                EFDEFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DE8C9C9C
                000000000000000000000000BDBDBDBDBDBDFFEFDEFFEFDEFFEFDEFFEFDE0000
                008C9C9C8C9C9CC6D6DEC6D6DE8C9C9C000000EFFFFFEFFFFFEFFFFF0000008C
                9C9CBDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DE000000
                EFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEFDEFFEF
                DE0000008C9C9C8C9C9C000000DEBDD6EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
                FFFF000000BDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9C000000DEBDD6
                EFFFFFEFFFFF000000000000EFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEF
                DED6A58C000000C6D6DE000000DEBDD6DEBDD6EFFFFF000000EFFFFFEFFFFFEF
                FFFF000000BDBDBDFFEFDEEFFFFF0000000000008C9C9CC6D6DEC6D6DE000000
                DEBDD6DEBDD6000000EFFFFFEFFFFF000000D6A58CEFFFFF0000000000008C9C
                9CC6D6DEC6D6DEC6D6DEC6DEC68C9C9C000000DEBDD6DEBDD6DEBDD60000008C
                9C9C0000000000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
                C6DEC60000000000000000000000000000008C9C9CC6D6DEC6D6DEC6DEC6C6D6
                DEC6D6DEC6D6DEC6DEC6}
              LookAndFeel.NativeStyle = True
            end
          end
        end
      end
      object DBMemo2: TDBMemo
        Tag = 1
        Left = 761
        Top = 47
        Width = 280
        Height = 206
        Align = alClient
        DataField = 'cDescAtendimento'
        DataSource = dsContatoOcorrencia
        TabOrder = 1
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1041
        Height = 47
        Align = alTop
        TabOrder = 2
        object btRegAtendimento: TcxButton
          Left = 3
          Top = 9
          Width = 160
          Height = 33
          Caption = 'Registrar Atendimento'
          TabOrder = 0
          TabStop = False
          OnClick = btRegAtendimentoClick
          Glyph.Data = {
            9E020000424D9E0200000000000036000000280000000E0000000E0000000100
            180000000000680200000000000000000000000000000000000063B2BF0F8BAC
            0D8AAC0C89AB0A88AB0586AA0586AA0586AB0485AB0284AB0184AA0083AA0082
            AA63B2BF00003AA8B9ACF4FFA0F1FEA0F1FEA0F1FEA0F1FEA0F1FEA0F1FEA0F1
            FEA0F1FEA0F1FEA0F1FEACF4FF3AA8B9000040B1C29BF0FD84EBFC84EBFC84EB
            FC84EBFC84EBFC84EBFC84EBFC84EBFC84EBFC84EBFC9BF0FD40B1C2000042B4
            C596EEFC80E8F980E8F980E8F980E8F980E8F980E8F980E8F980E8F980E8F980
            E8F996EEFC42B4C5000044B7C890EBFA7BE5F67BE5F67BE5F67BE5F67BE5F67B
            E5F67BE5F67BE5F67BE5F67BE5F690EBFA44B7C8000046BACB8AE8F777E2F377
            E2F377E2F377E2F377E2F377E2F377E2F377E2F377E2F377E2F38AE8F746BACB
            000048BDCE84E6F573DFF073DFF073DFF073DFF073DFF073DFF073DFF073DFF0
            73DFF073DFF084E6F548BDCE000049BFD07DE3F36FDCED6BD9EA69D3E464C9D9
            61C3D261C3D261C3D26FDCED6FDCED6FDCED7DE3F349BFD000004BC1D377E1F1
            6BD9EA6BD9EA69D3E464C9D961C3D261C3D26D979D6BD9EA6BD9EA6BD9EA77E1
            F14BC1D300004CC3D472DFF068D7E868D7E866D1E261C7D760B5C36D979D70D2
            E068D7E868D7E868D7E872DFF04CC3D400004DC5D673E1F26EDDEE529ADB1E1E
            B81E1EB85C72A470D2E06EDDEE6EDDEE6EDDEE6EDDEE73E1F24DC5D6000077D1
            DE3BC1DC35BDDB1E38DA6565F76565F71C37DA2BB7D929B6D926B5D923B3D820
            B2D71EB1D777D1DE0000F0F0F0F0F0F0F0F0F01E38DA6565F76565F71C37DAF0
            F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000F0F0F0F0F0F0F0F0F0B7
            B7ED4E4EE74E4EE7B7B7EDF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
            0000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object tabFollowUp: TcxTabSheet
      Caption = 'Follow-Up'
      ImageIndex = 1
      object DBMemo3: TDBMemo
        Left = 689
        Top = 0
        Width = 352
        Height = 253
        Align = alClient
        DataField = 'cOcorrencia'
        DataSource = dsFollow
        Enabled = False
        TabOrder = 0
      end
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 689
        Height = 253
        Align = alLeft
        DataGrouping.GroupLevels = <>
        DataSource = dsFollow
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFollowUp'
            Footers = <>
            Width = 141
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 172
          end
          item
            EditButtons = <>
            FieldName = 'cOcorrenciaResum'
            Footers = <>
            Width = 265
          end
          item
            EditButtons = <>
            FieldName = 'dDtProxAcao'
            Footers = <>
            Width = 92
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit13: TDBEdit [33]
    Tag = 1
    Left = 821
    Top = 42
    Width = 109
    Height = 19
    DataField = 'dDtOcorrencia'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit17: TDBEdit [34]
    Tag = 1
    Left = 821
    Top = 66
    Width = 109
    Height = 19
    DataField = 'dDtPrazoAtendIni'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit18: TDBEdit [35]
    Tag = 1
    Left = 956
    Top = 66
    Width = 109
    Height = 19
    DataField = 'dDtPrazoAtendFinal'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit19: TDBEdit [36]
    Tag = 1
    Left = 821
    Top = 162
    Width = 109
    Height = 19
    DataField = 'dDtUsuarioAtend'
    DataSource = dsMaster
    TabOrder = 18
  end
  object DBEdit20: TDBEdit [37]
    Tag = 1
    Left = 889
    Top = 90
    Width = 280
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuarioCad
    TabOrder = 19
  end
  object DBEdit15: TDBEdit [38]
    Tag = 1
    Left = 889
    Top = 138
    Width = 280
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuarioAtend
    TabOrder = 20
  end
  object DBEdit16: TDBEdit [39]
    Tag = 1
    Left = 821
    Top = 186
    Width = 348
    Height = 19
    DataField = 'cNmTabStatusOcorrencia'
    DataSource = dsTabStatusOcorrencia
    TabOrder = 21
  end
  object DBEdit14: TDBEdit [40]
    Tag = 1
    Left = 821
    Top = 90
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioCad'
    DataSource = dsMaster
    TabOrder = 22
  end
  object DBEdit21: TDBEdit [41]
    Tag = 1
    Left = 821
    Top = 114
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioResp'
    DataSource = dsMaster
    TabOrder = 23
  end
  object DBEdit22: TDBEdit [42]
    Tag = 1
    Left = 889
    Top = 114
    Width = 280
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuarioResp
    TabOrder = 24
  end
  object DBEdit23: TDBEdit [43]
    Tag = 1
    Left = 821
    Top = 138
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioAtend'
    DataSource = dsMaster
    TabOrder = 25
  end
  object cxPageControl2: TcxPageControl [44]
    Left = 704
    Top = 210
    Width = 465
    Height = 103
    ActivePage = tabContatos
    LookAndFeel.NativeStyle = True
    TabOrder = 26
    ClientRectBottom = 99
    ClientRectLeft = 4
    ClientRectRight = 461
    ClientRectTop = 24
    object tabContatos: TcxTabSheet
      Caption = 'Contatos Terceiro'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 457
        Height = 75
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsContatoTerceiro
        EvenRowColor = clWhite
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -13
        FooterFont.Name = 'Consolas'
        FooterFont.Style = []
        IndicatorOptions = []
        Options = [dgTitles, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentFont = False
        ReadOnly = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTerceiro'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoTelefone'
            Footers = <>
            Width = 104
          end
          item
            EditButtons = <>
            FieldName = 'cTelefone'
            Footers = <>
            Width = 107
          end
          item
            EditButtons = <>
            FieldName = 'cRamal'
            Footers = <>
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'cContato'
            Footers = <>
            Width = 76
          end
          item
            EditButtons = <>
            FieldName = 'cDepartamento'
            Footers = <>
            Width = 90
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOcorrencia'
      '      ,nCdEmpresa'
      '      ,nCdLoja'
      '      ,nCdTerceiro'
      '      ,nCdTipoOcorrencia'
      '      ,cResumoOcorrencia'
      '      ,cDescOcorrencia'
      '      ,dDtOcorrencia'
      '      ,nCdUsuarioCad'
      '      ,nCdTabStatusOcorrencia'
      '      ,nCdUsuarioAtend'
      ',dDtUsuarioAtend'
      '      ,dDtPrazoAtendIni'
      '      ,dDtPrazoAtendFinal'
      '     ,nCdUsuarioResp'
      '  FROM Ocorrencia'
      ' WHERE nCdOcorrencia = :nPK')
    Left = 152
    Top = 232
    object qryMasternCdOcorrencia: TIntegerField
      FieldName = 'nCdOcorrencia'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdTipoOcorrencia: TIntegerField
      FieldName = 'nCdTipoOcorrencia'
    end
    object qryMastercResumoOcorrencia: TStringField
      FieldName = 'cResumoOcorrencia'
      Size = 150
    end
    object qryMastercDescOcorrencia: TMemoField
      FieldName = 'cDescOcorrencia'
      BlobType = ftMemo
    end
    object qryMasterdDtOcorrencia: TDateTimeField
      FieldName = 'dDtOcorrencia'
    end
    object qryMasternCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMasternCdTabStatusOcorrencia: TIntegerField
      FieldName = 'nCdTabStatusOcorrencia'
    end
    object qryMasternCdUsuarioAtend: TIntegerField
      FieldName = 'nCdUsuarioAtend'
    end
    object qryMasterdDtPrazoAtendIni: TDateTimeField
      FieldName = 'dDtPrazoAtendIni'
    end
    object qryMasterdDtPrazoAtendFinal: TDateTimeField
      FieldName = 'dDtPrazoAtendFinal'
    end
    object qryMasterdDtUsuarioAtend: TDateTimeField
      FieldName = 'dDtUsuarioAtend'
    end
    object qryMastercNmUsuarioCad: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuarioCad'
      Calculated = True
    end
    object qryMasternCdUsuarioResp: TIntegerField
      FieldName = 'nCdUsuarioResp'
    end
  end
  inherited dsMaster: TDataSource
    Left = 152
    Top = 264
  end
  inherited qryID: TADOQuery
    Left = 184
    Top = 232
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 216
    Top = 232
  end
  inherited qryStat: TADOQuery
    Left = 184
    Top = 264
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 216
    Top = 264
  end
  inherited ImageList1: TImageList
    Left = 248
    Top = 232
    Bitmap = {
      494C01010A000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF00000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF00000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB009696960000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF800100000000FFFF800100000000
      8000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000C3C3000000000000C3C3000000008000C003000000000000C00300000000
      0100E007000000000000F00F00000000FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      '   FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 344
    Top = 232
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 344
    Top = 264
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 312
    Top = 264
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '  FROM Loja'
      
        '       INNER JOIN UsuarioLoja ON UsuarioLoja.nCdLoja = Loja.nCdL' +
        'oja'
      ' WHERE Loja.nCdLoja = :nPK'
      '   AND nCdUsuario   = :nCdUsuario')
    Left = 312
    Top = 232
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      '      ,cCNPJCPF'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK ')
    Left = 280
    Top = 232
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 280
    Top = 264
  end
  object dsTipoOcorrencia: TDataSource
    DataSet = qryTipoOcorrencia
    Left = 376
    Top = 264
  end
  object qryTipoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TipoOcorrencia.nCdTipoOcorrencia'
      '      ,TipoOcorrencia.cNmTipoOcorrencia'
      '      ,TipoOcorrencia.iDiasAtendimento'
      '  FROM TipoOcorrencia'
      
        '       INNER JOIN UsuarioRegTipoOcorrencia UsuarioReg ON Usuario' +
        'Reg.nCdTipoOcorrencia = TipoOcorrencia.nCdTipoOcorrencia'
      ' WHERE TipoOcorrencia.nCdTipoOcorrencia = :nPK'
      '   AND UsuarioReg.nCdUsuario            = :nCdUsuario')
    Left = 376
    Top = 232
    object qryTipoOcorrenciacNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
    object qryTipoOcorrenciaiDiasAtendimento: TIntegerField
      FieldName = 'iDiasAtendimento'
    end
  end
  object qryContatoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryContatoOcorrenciaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContatoOcorrencia'
      '      ,nCdUsuarioContato'
      '      ,cNmUsuario'
      '      ,nCdOcorrencia'
      '      ,cDescAtendimento'
      '      ,dbo.fn_OnlyDate(dDtAtendimento)     as dDtAtendimento'
      '      ,CONVERT(varchar,dDtAtendimento,108) as cHoraAtendimento'
      '      ,cNmContatoOcorrencia'
      '      ,cTelefoneContato'
      '      ,TabStatusOcorrencia.nCdTabStatusOcorrencia'
      '      ,TabStatusOcorrencia.cNmTabStatusOcorrencia'
      '  FROM ContatoOcorrencia         '
      
        '       INNER JOIN TabStatusOcorrencia ON TabStatusOcorrencia.nCd' +
        'TabStatusOcorrencia = ContatoOcorrencia.nCdTabStatusOcorrencia'
      
        '       INNER JOIN Usuario             ON Usuario.nCdUsuario     ' +
        '                    = ContatoOcorrencia.nCdUsuarioContato'
      ' WHERE nCdOcorrencia = :nPK')
    Left = 408
    Top = 232
    object qryContatoOcorrencianCdContatoOcorrencia: TIntegerField
      FieldName = 'nCdContatoOcorrencia'
    end
    object qryContatoOcorrencianCdUsuarioContato: TIntegerField
      FieldName = 'nCdUsuarioContato'
    end
    object qryContatoOcorrencianCdOcorrencia: TIntegerField
      FieldName = 'nCdOcorrencia'
    end
    object qryContatoOcorrenciacDescAtendimento: TMemoField
      FieldName = 'cDescAtendimento'
      BlobType = ftMemo
    end
    object qryContatoOcorrenciacNmContatoOcorrencia: TStringField
      FieldName = 'cNmContatoOcorrencia'
      Size = 30
    end
    object qryContatoOcorrenciacTelefoneContato: TStringField
      FieldName = 'cTelefoneContato'
      Size = 14
    end
    object qryContatoOcorrencianCdTabStatusOcorrencia: TIntegerField
      FieldName = 'nCdTabStatusOcorrencia'
    end
    object qryContatoOcorrenciadDtAtendimento: TDateTimeField
      FieldName = 'dDtAtendimento'
      ReadOnly = True
    end
    object qryContatoOcorrenciacHoraAtendimento: TStringField
      FieldName = 'cHoraAtendimento'
      ReadOnly = True
      Size = 30
    end
    object qryContatoOcorrenciacNmTabStatusOcorrencia: TStringField
      FieldName = 'cNmTabStatusOcorrencia'
      Size = 30
    end
    object qryContatoOcorrenciacNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsContatoOcorrencia: TDataSource
    DataSet = qryContatoOcorrencia
    Left = 408
    Top = 264
  end
  object qryContatoTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cTipoPessoa CHAR(1)'
      '       ,@nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      'Set @cTipoPessoa = '#39'F'#39
      ''
      'IF (EXISTS(SELECT 1'
      '             FROM PessoaFisica'
      
        '            WHERE nCdTerceiro = @ncdTerceiro)) Set @cTipoPessoa ' +
        '= '#39'F'#39
      ''
      'IF (OBJECT_ID('#39'tempdb..#ContatoCliente'#39') IS NULL)'
      'BEGIN'
      ''
      #9'CREATE TABLE #ContatoCliente (nCdTerceiro     int'
      #9#9#9#9#9#9#9#9' ,cNmTipoTelefone varchar(50)'
      #9#9#9#9#9#9#9#9' ,cTelefone       varchar(20)'
      #9#9#9#9#9#9#9#9' ,cRamal          varchar(5)'
      #9#9#9#9#9#9#9#9' ,cContato        varchar(50)'
      '                                 ,cDepartamento   varchar(50))'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    TRUNCATE TABLE #ContatoCliente'
      ''
      'END'
      ''
      '--'
      '-- Para Pessoa fisica'
      '--'
      'IF (@cTipoPessoa = '#39'F'#39')'
      'BEGIN'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Celular'#39
      #9#9#9#9#9#9#9#9',cTelefoneMovel'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneMovel IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Residencia'#39
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Endereco'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND nCdStatus      = 1'
      #9#9#9#9#9#9#9' AND cTelefone     IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Residencia 2'#39
      #9#9#9#9#9#9#9#9',cFax'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Endereco'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND nCdStatus      = 1'
      #9#9#9#9#9#9#9' AND cFax          IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Recado'#39
      #9#9#9#9#9#9#9#9',cTelefoneRec1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContatoRec1'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneRec1 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Recado 2'#39
      #9#9#9#9#9#9#9#9',cTelefoneRec2'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContatoRec2'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneRec2 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Trabalho'#39
      #9#9#9#9#9#9#9#9',cTelefoneEmpTrab'
      #9#9#9#9#9#9#9#9',cRamalEmpTrab'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneEmpTrab IS NOT NULL'
      ''
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Celular C'#244'njuge'#39
      #9#9#9#9#9#9#9#9',cTelefoneCelCjg'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',SUBSTRING(cNmCjg,1,50)'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneCelCjg IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Trabalho C'#244'njuge'#39
      #9#9#9#9#9#9#9#9',cTelefoneEmpTrabCjg'
      #9#9#9#9#9#9#9#9',cRamalEmpTrabCjg'
      #9#9#9#9#9#9#9#9',SUBSTRING(cNmCjg,1,50)'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneEmpTrabCjg IS NOT NULL'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Telefone'#39
      #9#9#9#9#9#9#9#9',cTelefone1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefone1 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Telefone 2'#39
      #9#9#9#9#9#9#9#9',cTelefone2'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefone2     IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato'
      '                                ,cDepartamento)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Contato'#39
      #9#9#9#9#9#9#9#9',cTelefone1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContato'
      '                                ,cDepartamento'
      #9#9#9#9#9#9#9'FROM ContatoTerceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      ''
      ''
      ''
      ''
      'SELECT * '
      '  FROM #ContatoCliente')
    Left = 440
    Top = 232
    object qryContatoTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryContatoTerceirocNmTipoTelefone: TStringField
      DisplayLabel = 'Tipo Contato'
      FieldName = 'cNmTipoTelefone'
      Size = 50
    end
    object qryContatoTerceirocTelefone: TStringField
      DisplayLabel = 'Telefone'
      FieldName = 'cTelefone'
      EditMask = '!\(99\) 9999-9999;0;_'
    end
    object qryContatoTerceirocRamal: TStringField
      DisplayLabel = 'Ramal'
      FieldName = 'cRamal'
      Size = 5
    end
    object qryContatoTerceirocContato: TStringField
      DisplayLabel = 'Contato'
      FieldName = 'cContato'
      Size = 50
    end
    object qryContatoTerceirocDepartamento: TStringField
      DisplayLabel = 'Departamento'
      FieldName = 'cDepartamento'
      Size = 50
    end
  end
  object dsContatoTerceiro: TDataSource
    DataSet = qryContatoTerceiro
    Left = 440
    Top = 264
  end
  object qryUsuarioCad: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 472
    Top = 232
    object qryUsuarioCadcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryUsuarioCadnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
  end
  object dsUsuarioCad: TDataSource
    DataSet = qryUsuarioCad
    Left = 472
    Top = 264
  end
  object qryTabStatusOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabStatusOcorrencia'
      '      ,cNmTabStatusOcorrencia'
      '  FROM TabStatusOcorrencia'
      ' WHERE nCdTabStatusOcorrencia = :nPK')
    Left = 568
    Top = 232
    object qryTabStatusOcorrenciacNmTabStatusOcorrencia: TStringField
      FieldName = 'cNmTabStatusOcorrencia'
      Size = 30
    end
    object qryTabStatusOcorrencianCdTabStatusOcorrencia: TIntegerField
      FieldName = 'nCdTabStatusOcorrencia'
    end
  end
  object dsTabStatusOcorrencia: TDataSource
    DataSet = qryTabStatusOcorrencia
    Left = 568
    Top = 264
  end
  object qryVerifTipoOcorrenciaUsuarioReg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPKUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmTipoOcorrencia'
      '      ,iDiasAtendimento'
      '  FROM TipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = :nPK'
      '   AND EXISTS (SELECT 1'
      #9#9#9#9' FROM UsuarioRegTipoOcorrencia'
      #9#9#9#9'WHERE nCdUsuario = :nPKUsuario'
      #9#9#9#9' AND  nCdTipoOcorrencia = TipoOcorrencia.nCdTipoOcorrencia) '
      '')
    Left = 600
    Top = 232
    object qryVerifTipoOcorrenciaUsuarioRegcNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
    object qryVerifTipoOcorrenciaUsuarioRegiDiasAtendimento: TIntegerField
      FieldName = 'iDiasAtendimento'
    end
  end
  object dsVerifTipoOcorrenciaUsuarioReg: TDataSource
    DataSet = qryVerifTipoOcorrenciaUsuarioReg
    Left = 600
    Top = 264
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 632
    Top = 264
  end
  object qryUsuarioLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '  FROM UsuarioLoja'
      ' WHERE nCdUsuario = :nCdUsuario')
    Left = 248
    Top = 264
    object qryUsuarioLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object qryGeraPrazoAtend: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoOcorrencia'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDtOcorrencia'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTipoOcorrencia  int'
      '       ,@cFlgDiaUtil        int'
      '       ,@dDtOcorrencia      datetime'
      '       ,@dDtPrazoAtendFinal datetime'
      ''
      'SET @nCdTipoOcorrencia = :nCdTipoOcorrencia'
      'SET @dDtOcorrencia     = CONVERT(datetime,:cDtOcorrencia,103)'
      ''
      'SELECT @cFlgDiaUtil        = cFlgContaDiasUteis'
      '      ,@dDtPrazoAtendFinal = @dDtOcorrencia + iDiasAtendimento'
      '  FROM TipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = @nCdTipoOcorrencia'
      ''
      'SELECT @dDtOcorrencia as dDtPrazoAtendIni'
      '      ,CASE WHEN @cFlgDiaUtil = 1'
      '            THEN dbo.fn_DiaUtil(@dDtPrazoAtendFinal,'#39'N'#39')'
      '            ELSE @dDtPrazoAtendFinal'
      '       END dDtPrazoAtendFinal')
    Left = 632
    Top = 232
    object qryGeraPrazoAtenddDtPrazoAtendIni: TDateTimeField
      FieldName = 'dDtPrazoAtendIni'
      ReadOnly = True
    end
    object qryGeraPrazoAtenddDtPrazoAtendFinal: TDateTimeField
      FieldName = 'dDtPrazoAtendFinal'
      ReadOnly = True
    end
  end
  object qryUsuarioResp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 504
    Top = 232
    object StringField1: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryUsuarioRespnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
  end
  object dsUsuarioResp: TDataSource
    DataSet = qryUsuarioResp
    Left = 504
    Top = 264
  end
  object dsUsuarioAtend: TDataSource
    DataSet = qryUsuarioAtend
    Left = 536
    Top = 264
  end
  object qryUsuarioAtend: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 536
    Top = 232
    object StringField2: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object IntegerField1: TIntegerField
      FieldName = 'nCdUsuario'
    end
  end
  object dsFollow: TDataSource
    DataSet = qryFollow
    Left = 664
    Top = 232
  end
  object qryFollow: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtFollowUp'
      '      ,cNmUsuario'
      '      ,cOcorrenciaResum'
      '      ,dDtProxAcao'
      ',cOcorrencia'
      '  FROM FollowUp'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = FollowUp.nCdUs' +
        'uario'
      ' WHERE nCdOcorrencia = :nPK'
      'ORDER BY dDtFollowUp DESC')
    Left = 664
    Top = 264
    object qryFollowdDtFollowUp: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data'
      FieldName = 'dDtFollowUp'
      ReadOnly = True
    end
    object qryFollowcNmUsuario: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryFollowcOcorrenciaResum: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Descri'#231#227'o Resumida'
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowdDtProxAcao: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data Pr'#243'x. A'#231#227'o'
      FieldName = 'dDtProxAcao'
    end
    object qryFollowcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
  end
end
