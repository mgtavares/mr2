unit fItemFaturar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, fNC;


type
  TfrmItemFaturar = class(TfrmProcesso_Padrao)
    qryItemPedido: TADOQuery;
    qryItemPedidoItemdoPedidoCdItem: TAutoIncField;
    qryItemPedidoProdutoCd: TIntegerField;
    qryItemPedidoProdutoDescrio: TStringField;
    qryItemPedidoItemdoPedidoTipodeItem: TStringField;
    qryItemPedidoItemdoPedidoStatus: TStringField;
    DataSource1: TDataSource;
    uSP_FATURA_PEDIDO: TADOStoredProc;
    qryItemPedidoItemdoPedidoQtdeAutorizada: TBCDField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ItemdoPedidoCdItem: TcxGridDBColumn;
    cxGrid1DBTableView1ProdutoCd: TcxGridDBColumn;
    cxGrid1DBTableView1ProdutoDescrio: TcxGridDBColumn;
    cxGrid1DBTableView1ItemdoPedidoTipodeItem: TcxGridDBColumn;
    cxGrid1DBTableView1ItemdoPedidoStatus: TcxGridDBColumn;
    cxGrid1DBTableView1ItemdoPedidoQtdeAutorizada: TcxGridDBColumn;
    SP_FATURA_PEDIDO_DEVOLUCAO: TADOStoredProc;
    qryItemPedidocFlgDevolucCompra: TIntegerField;
    qryItemPedidocFlgDevolucVenda: TIntegerField;
    SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA: TADOStoredProc;
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    objNC : TfrmNC ;
  public
    { Public declarations }
    nCdPedido: Integer ;
    dDtFaturar : TDate ;
    nPerTaxa1  : double ;
    nPerTaxa2  : double ;
  end;

var
  frmItemFaturar: TfrmItemFaturar;

implementation

uses fMenu;


{$R *.dfm}

procedure TfrmItemFaturar.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  case key of
    vk_F7 : Begin
        showForm(objNC,false);
    end ;
  end ;

end;

procedure TfrmItemFaturar.FormShow(Sender: TObject);
begin
  inherited;

  qryItemPedido.First ;

  ToolButton1.Enabled := False ;

  while not qryItemPedido.eof do
  begin

      if (qryItemPedidoItemdoPedidoQtdeAutorizada.Value > 0) then
          ToolButton1.Enabled := True ;

      qryItemPedido.Next ;
  end ;

  qryItemPedido.First ;

  objNC := TfrmNC.Create(nil);
  Try
     if nPerTaxa1=0 then
      objNC.MaskEdit3.Text := '100' else
      objNC.MaskEdit3.Text := formatcurr('##,##0.00',nPerTaxa1) ;

      objNC.MaskEdit4.Text := formatcurr('##,##0.00',nPerTaxa2)  ;
      objNC.nPerTaxa1 := nPerTaxa1;
      objNC.nPerTaxa2 := nPerTaxa2;

  except
      MensagemErro('Erro ao gerar o faturamento');
      raise;
  end;

end;

procedure TfrmItemFaturar.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(objNC.MaskEdit3.Text) = '') then
      objNC.MaskEdit3.Text := '0' ;

  if (Trim(objNC.MaskEdit4.Text) = '') then
      objNC.MaskEdit4.Text := '0' ;

  if (Trim(objNC.MaskEdit4.Text) = '0') and (Trim(objNC.MaskEdit3.Text) = '0') then
      objNC.MaskEdit3.Text := '100' ;


  frmMenu.Connection.BeginTrans ;


  try

      if ((qryItemPedidocFlgDevolucCompra.Value = 0) and (qryItemPedidocFlgDevolucVenda.Value = 0)) then
      begin

          usp_fatura_pedido.Close ;
          usp_fatura_pedido.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          usp_fatura_pedido.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido ;
          usp_fatura_pedido.Parameters.ParamByName('@dDtFatur').Value   := DateToStr(dDtFaturar) ;
          usp_fatura_pedido.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          usp_fatura_pedido.Parameters.ParamByName('@nPercN').Value     := StrToFloat(objNC.MaskEdit3.Text) ;
          usp_fatura_pedido.Parameters.ParamByName('@nPercC').Value     := StrToFloat(objNC.MaskEdit4.Text) ;
          usp_fatura_pedido.ExecProc ;

      end

      else if (qryItemPedidocFlgDevolucCompra.Value = 1) then
      begin
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Close ;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido ;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@dDtFatur').Value   := DateToStr(dDtFaturar) ;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@nPercN').Value     := StrToFloat(objNC.MaskEdit3.Text) ;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.Parameters.ParamByName('@nPercC').Value     := StrToFloat(objNC.MaskEdit4.Text) ;
          SP_FATURA_PEDIDO_DEVOLUCAO_COMPRA.ExecProc ;

      end

      else if (qryItemPedidocFlgDevolucVenda.Value = 1) then
      begin

          SP_FATURA_PEDIDO_DEVOLUCAO.Close ;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido ;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@dDtFatur').Value   := DateToStr(dDtFaturar) ;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@nPercN').Value     := StrToFloat(objNC.MaskEdit3.Text) ;
          SP_FATURA_PEDIDO_DEVOLUCAO.Parameters.ParamByName('@nPercC').Value     := StrToFloat(objNC.MaskEdit4.Text) ;
          SP_FATURA_PEDIDO_DEVOLUCAO.ExecProc ;

      end ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;
  Close ;

end;

procedure TfrmItemFaturar.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
    vk_F7 : Begin
        showForm(objNC,false);
    end ;
  end ;

end;

procedure TfrmItemFaturar.ToolButton2Click(Sender: TObject);
begin
  FreeAndNil(objNC);
  inherited;

end;

end.
