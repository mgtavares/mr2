inherited frmRegAtendOcorrencia_Atendimento_SelContato: TfrmRegAtendOcorrencia_Atendimento_SelContato
  Left = 441
  Top = 315
  Width = 552
  Height = 389
  BorderIcons = [biSystemMenu]
  Caption = 'Sele'#231#227'o de Contato'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 536
    Height = 322
  end
  inherited ToolBar1: TToolBar
    Width = 536
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 536
    Height = 322
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsListaContato
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoTelefone'
        Footers = <>
        Width = 135
      end
      item
        EditButtons = <>
        FieldName = 'cTelefone'
        Footers = <>
        Width = 128
      end
      item
        EditButtons = <>
        FieldName = 'cRamal'
        Footers = <>
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'cContato'
        Footers = <>
        Width = 90
      end
      item
        EditButtons = <>
        FieldName = 'cDepartamento'
        Footers = <>
        Width = 90
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryListaContato: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cTipoPessoa CHAR(1)'
      '       ,@nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      'Set @cTipoPessoa = '#39'F'#39
      ''
      'IF (EXISTS(SELECT 1'
      '             FROM PessoaFisica'
      
        '            WHERE nCdTerceiro = @ncdTerceiro)) Set @cTipoPessoa ' +
        '= '#39'F'#39
      ''
      'IF (OBJECT_ID('#39'tempdb..#ContatoCliente'#39') IS NULL)'
      'BEGIN'
      ''
      #9'CREATE TABLE #ContatoCliente (nCdTerceiro     int'
      #9#9#9#9#9#9#9#9' ,cNmTipoTelefone varchar(50)'
      #9#9#9#9#9#9#9#9' ,cTelefone       varchar(20)'
      #9#9#9#9#9#9#9#9' ,cRamal          varchar(5)'
      #9#9#9#9#9#9#9#9' ,cContato        varchar(50)'
      '                                 ,cDepartamento   varchar(50))'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    TRUNCATE TABLE #ContatoCliente'
      ''
      'END'
      ''
      '--'
      '-- Para Pessoa fisica'
      '--'
      'IF (@cTipoPessoa = '#39'F'#39')'
      'BEGIN'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Celular'#39
      #9#9#9#9#9#9#9#9',cTelefoneMovel'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneMovel IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Residencia'#39
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Endereco'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND nCdStatus      = 1'
      #9#9#9#9#9#9#9' AND cTelefone     IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Residencia 2'#39
      #9#9#9#9#9#9#9#9',cFax'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Endereco'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND nCdStatus      = 1'
      #9#9#9#9#9#9#9' AND cFax          IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Recado'#39
      #9#9#9#9#9#9#9#9',cTelefoneRec1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContatoRec1'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneRec1 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Recado 2'#39
      #9#9#9#9#9#9#9#9',cTelefoneRec2'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContatoRec2'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneRec2 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Trabalho'#39
      #9#9#9#9#9#9#9#9',cTelefoneEmpTrab'
      #9#9#9#9#9#9#9#9',cRamalEmpTrab'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneEmpTrab IS NOT NULL'
      ''
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Celular C'#244'njuge'#39
      #9#9#9#9#9#9#9#9',cTelefoneCelCjg'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',SUBSTRING(cNmCjg,1,50)'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneCelCjg IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Trabalho C'#244'njuge'#39
      #9#9#9#9#9#9#9#9',cTelefoneEmpTrabCjg'
      #9#9#9#9#9#9#9#9',cRamalEmpTrabCjg'
      #9#9#9#9#9#9#9#9',SUBSTRING(cNmCjg,1,50)'
      #9#9#9#9#9#9#9'FROM PessoaFisica'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefoneEmpTrabCjg IS NOT NULL'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Telefone'#39
      #9#9#9#9#9#9#9#9',cTelefone1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefone1 IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Telefone 2'#39
      #9#9#9#9#9#9#9#9',cTelefone2'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9'FROM Terceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro    = @nCdTerceiro'
      #9#9#9#9#9#9#9' AND cTelefone2     IS NOT NULL'
      ''
      #9'INSERT INTO #ContatoCliente (nCdTerceiro'
      #9#9#9#9#9#9#9#9',cNmTipoTelefone'
      #9#9#9#9#9#9#9#9',cTelefone'
      #9#9#9#9#9#9#9#9',cRamal'
      #9#9#9#9#9#9#9#9',cContato'
      '                                ,cDepartamento)'
      #9#9#9#9#9#9'  SELECT nCdTerceiro'
      #9#9#9#9#9#9#9#9','#39'Contato'#39
      #9#9#9#9#9#9#9#9',cTelefone1'
      #9#9#9#9#9#9#9#9',NULL'
      #9#9#9#9#9#9#9#9',cNmContato'
      '                                ,cDepartamento'
      #9#9#9#9#9#9#9'FROM ContatoTerceiro'
      #9#9#9#9#9#9'   WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #ContatoCliente'
      '')
    Left = 184
    Top = 200
    object qryListaContatonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryListaContatocNmTipoTelefone: TStringField
      DisplayLabel = 'Contatos|Tipo Contato'
      FieldName = 'cNmTipoTelefone'
      Size = 135
    end
    object qryListaContatocTelefone: TStringField
      DisplayLabel = 'Contatos|Telefone'
      FieldName = 'cTelefone'
    end
    object qryListaContatocRamal: TStringField
      DisplayLabel = 'Contatos|Ramal'
      FieldName = 'cRamal'
      Size = 5
    end
    object qryListaContatocContato: TStringField
      DisplayLabel = 'Contatos|Contato'
      FieldName = 'cContato'
      Size = 50
    end
    object qryListaContatocDepartamento: TStringField
      DisplayLabel = 'Contatos|Departamento'
      FieldName = 'cDepartamento'
      Size = 50
    end
  end
  object dsListaContato: TDataSource
    DataSet = qryListaContato
    Left = 224
    Top = 192
  end
end
