inherited frmGerarSintegra: TfrmGerarSintegra
  Caption = 'Gerar Sintegra'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 36
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 57
    Top = 72
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 29
    Top = 120
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Finalidade'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [4]
    Left = 41
    Top = 96
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
    FocusControl = DBEdit1
  end
  object Label5: TLabel [5]
    Left = 165
    Top = 96
    Width = 6
    Height = 13
    Alignment = taCenter
    Caption = #224
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TER2LookupMaskEdit [7]
    Left = 80
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnBeforePosicionaQry = edtEmpresaBeforePosicionaQry
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtLoja: TER2LookupMaskEdit [8]
    Left = 80
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnBeforePosicionaQry = edtLojaBeforePosicionaQry
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object edtDtInicial: TMaskEdit [9]
    Left = 80
    Top = 88
    Width = 79
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtDtFinal: TMaskEdit [10]
    Left = 184
    Top = 88
    Width = 79
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 152
    Top = 40
    Width = 353
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 152
    Top = 64
    Width = 353
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource2
    TabOrder = 6
  end
  object cbFinalidade: TComboBox [13]
    Left = 80
    Top = 112
    Width = 185
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 7
    Text = '1 - Normal'
    Items.Strings = (
      '1 - Normal'
      '2 - Retifica'#231#227'o Total de Arquivo'
      '3 - Retifica'#231#227'o Aditiva de Arquivo'
      '5 - Desfazimento de Arquivo')
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 56
  end
  object ACBrSintegra1: TACBrSintegra
    VersaoValidador = vv524
    Informa88SME = False
    Informa88SMS = False
    Informa88EAN = False
    Informa88C = False
    Left = 56
    Top = 320
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa      '
      '      ,cNmEmpresa'
      '     ,nCdTerceiroEmp'
      '  FROM Empresa   '
      ' WHERE Empresa.nCdEmpresa = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa                '
      
        '               WHERE UsuarioEmpresa.nCdEmpresa = Empresa.nCdEmpr' +
        'esa                   '
      '                 AND UsuarioEmpresa.nCdUsuario = :nCdUsuario)')
    Left = 520
    Top = 40
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryEmpresanCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja '
      '      ,cNmLoja'
      '     ,nCdTerceiro'
      '  FROM Loja   '
      ' WHERE Loja.nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja             '
      
        '               WHERE UsuarioLoja.nCdLoja    = Loja.nCdLoja      ' +
        '           '
      '                 AND UsuarioLoja.nCdUsuario = :nCdUsuario)')
    Left = 552
    Top = 40
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryLojanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 88
    Top = 320
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 400
    Top = 232
  end
  object DataSource2: TDataSource
    DataSet = qryLoja
    Left = 408
    Top = 240
  end
  object qryDadosInformante: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempRegistro10_11')
    Left = 248
    Top = 376
    object qryDadosInformantecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryDadosInformantecIE: TStringField
      FieldName = 'cIE'
      Size = 15
    end
    object qryDadosInformantecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryDadosInformantecCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryDadosInformantecUF: TStringField
      FieldName = 'cUF'
      Size = 2
    end
    object qryDadosInformantecFax: TStringField
      FieldName = 'cFax'
    end
    object qryDadosInformantecEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 50
    end
    object qryDadosInformanteiNumero: TIntegerField
      FieldName = 'iNumero'
    end
    object qryDadosInformantecComplemento: TStringField
      FieldName = 'cComplemento'
      Size = 35
    end
    object qryDadosInformantecBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryDadosInformantecCep: TStringField
      FieldName = 'cCep'
      Size = 8
    end
    object qryDadosInformantecNmContato: TStringField
      FieldName = 'cNmContato'
      Size = 35
    end
    object qryDadosInformantecTelefone: TStringField
      FieldName = 'cTelefone'
    end
  end
  object qryNotasFiscais: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempRegistro50'
      ' ORDER BY iNrDocto'
      '         ,cCFOP'
      '         ,nAliqICMS')
    Left = 280
    Top = 376
    object qryNotasFiscaisnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryNotasFiscaiscCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryNotasFiscaiscIE: TStringField
      FieldName = 'cIE'
      Size = 15
    end
    object qryNotasFiscaisdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryNotasFiscaiscUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryNotasFiscaiscModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryNotasFiscaiscSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryNotasFiscaisiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryNotasFiscaiscCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryNotasFiscaiscEmitente: TStringField
      FieldName = 'cEmitente'
      FixedChar = True
      Size = 1
    end
    object qryNotasFiscaisnValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaisnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaisnValICMS: TBCDField
      FieldName = 'nValICMS'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaisnValIsenta: TBCDField
      FieldName = 'nValIsenta'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaisnValOutras: TBCDField
      FieldName = 'nValOutras'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaisnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 12
      Size = 2
    end
    object qryNotasFiscaiscSituacaoCancel: TStringField
      FieldName = 'cSituacaoCancel'
      FixedChar = True
      Size = 1
    end
  end
  object qryItensDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempRegistro54'
      ' ORDER BY cEmitente'
      '         ,iNrDocto'
      '         ,cNmItem'
      '         ,cCFOP')
    Left = 312
    Top = 376
    object qryItensDoctoFiscalnCdRegistro54: TAutoIncField
      FieldName = 'nCdRegistro54'
      ReadOnly = True
    end
    object qryItensDoctoFiscalnCdItemDoctoFiscal: TIntegerField
      FieldName = 'nCdItemDoctoFiscal'
    end
    object qryItensDoctoFiscalcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryItensDoctoFiscalcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryItensDoctoFiscalcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryItensDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryItensDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryItensDoctoFiscalcCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 3
    end
    object qryItensDoctoFiscaliOrdemItem: TIntegerField
      FieldName = 'iOrdemItem'
    end
    object qryItensDoctoFiscalcCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 14
    end
    object qryItensDoctoFiscalcNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryItensDoctoFiscalcNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItensDoctoFiscalcUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItensDoctoFiscalnQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
      Size = 3
    end
    object qryItensDoctoFiscalnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnValDespesa: TBCDField
      FieldName = 'nValDespesa'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnPercRedBCICMS: TBCDField
      FieldName = 'nPercRedBCICMS'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 12
      Size = 2
    end
    object qryItensDoctoFiscalcEmitente: TStringField
      FieldName = 'cEmitente'
      FixedChar = True
      Size = 1
    end
  end
  object SP_GERA_DADOS_SINTEGRA: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_GERA_DADOS_SINTEGRA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 216
    Top = 344
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      '    IF (OBJECT_ID('#39'tempdb..#TempRegistro10_11'#39') IS NULL)'#13#10'    BE' +
      'GIN'#13#10#13#10'        CREATE TABLE #TempRegistro10_11 (cCNPJCPF     var' +
      'char(14)'#13#10'                                        ,cIE          ' +
      'varchar(15)'#13#10'                                        ,cNmTerceir' +
      'o  varchar(50)'#13#10'                                        ,cCidade' +
      '      varchar(35)'#13#10'                                        ,cUF ' +
      '         varchar(2)'#13#10'                                        ,cF' +
      'ax         varchar(20)'#13#10'                                        ' +
      ',cEndereco    varchar(50)'#13#10'                                     ' +
      '   ,iNumero      int'#13#10'                                        ,c' +
      'Complemento varchar(35)'#13#10'                                       ' +
      ' ,cBairro      varchar(35)'#13#10'                                    ' +
      '    ,cCep         varchar(8)'#13#10'                                  ' +
      '      ,cNmContato   varchar(35)'#13#10'                               ' +
      '         ,cTelefone    varchar(20))'#13#10'    END'#13#10#13#10'    IF (OBJECT_I' +
      'D('#39'tempdb..#TempRegistro50'#39') IS NULL)'#13#10'    BEGIN'#13#10#13#10'        CREA' +
      'TE TABLE #TempRegistro50 (nCdDoctoFiscal  int '#13#10'                ' +
      '                     ,cCNPJCPF        varchar(14)'#13#10'             ' +
      '                        ,cIE             varchar(15)'#13#10'          ' +
      '                           ,dDtEmissao      datetime'#13#10'          ' +
      '                           ,cUF             char(2)'#13#10'           ' +
      '                          ,cModelo         char(2)'#13#10'            ' +
      '                         ,cSerie          char(3)'#13#10'             ' +
      '                        ,iNrDocto        int'#13#10'                  ' +
      '                   ,cCFOP           char(4)'#13#10'                   ' +
      '                  ,cEmitente       char(1)'#13#10'                    ' +
      '                 ,nValTotal       decimal(12,2) default 0 not nu' +
      'll'#13#10'                                     ,nValBaseICMS    decima' +
      'l(12,2) default 0 not null'#13#10'                                    ' +
      ' ,nValICMS        decimal(12,2) default 0 not null'#13#10'            ' +
      '                         ,nValIsenta      decimal(12,2) default ' +
      '0 not null'#13#10'                                     ,nValOutras    ' +
      '  decimal(12,2) default 0 not null'#13#10'                            ' +
      '         ,nAliqICMS       decimal(12,2) default 0 not null'#13#10'    ' +
      '                                 ,cSituacaoCancel char(1)) '#13#10'   ' +
      ' END '#13#10#13#10'    IF (OBJECT_ID('#39'tempdb..#TempRegistro51'#39') IS NULL)'#13#10 +
      '    BEGIN'#13#10#13#10'        CREATE TABLE #TempRegistro51 (nCdDoctoFisca' +
      'l  int'#13#10'                                     ,cCNPJCPF        va' +
      'rchar(14)'#13#10'                                     ,cIE            ' +
      ' varchar(15)'#13#10'                                     ,dDtEmissao  ' +
      '    datetime'#13#10'                                     ,cUF         ' +
      '    char(2)'#13#10'                                     ,cSerie       ' +
      '   char(3)'#13#10'                                     ,iNrDocto      ' +
      '  int'#13#10'                                     ,cCFOP           cha' +
      'r(4)'#13#10'                                     ,nValTotal       deci' +
      'mal(12,2) default 0 not null'#13#10'                                  ' +
      '   ,nValIPI         decimal(12,2) default 0 not null'#13#10'          ' +
      '                           ,nValIsentaIPI   decimal(12,2) defaul' +
      't 0 not null'#13#10'                                     ,nValOutrasIP' +
      'I   decimal(12,2) default 0 not null'#13#10'                          ' +
      '           ,cSituacaoCancel char(1)) '#13#10'    END '#13#10#13#10'    IF (OBJEC' +
      'T_ID('#39'tempdb..#TempRegistro53'#39') IS NULL)'#13#10'    BEGIN'#13#10#13#10'        C' +
      'REATE TABLE #TempRegistro53 (nCdDoctoFiscal  int'#13#10'              ' +
      '                       ,cCNPJCPF        varchar(14)'#13#10'           ' +
      '                          ,cIE             varchar(15)'#13#10'        ' +
      '                             ,dDtEmissao      datetime'#13#10'        ' +
      '                             ,cUF             char(2)'#13#10'         ' +
      '                            ,cModelo         char(2)'#13#10'          ' +
      '                           ,cSerie          char(3)'#13#10'           ' +
      '                          ,iNrDocto        int'#13#10'                ' +
      '                     ,cCFOP           char(4)'#13#10'                 ' +
      '                    ,cEmitente       char(1)'#13#10'                  ' +
      '                   ,nValBaseICMSST  decimal(12,2) default 0 not ' +
      'null'#13#10'                                     ,nValICMSRetido  deci' +
      'mal(12,2) default 0 not null'#13#10'                                  ' +
      '   ,nValDespesas    decimal(12,2) default 0 not null'#13#10'          ' +
      '                           ,cSituacaoCancel char(1)'#13#10'           ' +
      '                          ,cCdAnteceipacao char(1)) '#13#10'    END '#13#10 +
      #13#10'    IF (OBJECT_ID('#39'tempdb..#TempRegistro54'#39') IS NULL)'#13#10'    BEG' +
      'IN'#13#10#13#10'        CREATE TABLE #TempRegistro54 (nCdRegistro54      i' +
      'nt primary key identity (1,1)'#13#10'                                 ' +
      '    ,nCdItemDoctoFiscal int'#13#10'                                   ' +
      '  ,cCNPJCPF           varchar(14)'#13#10'                             ' +
      '        ,cModelo            char(2)'#13#10'                           ' +
      '          ,cSerie             char(3)'#13#10'                         ' +
      '            ,iNrDocto           int                     not null' +
      #13#10'                                     ,cCFOP              char(' +
      '4)'#13#10'                                     ,cCdST              cha' +
      'r(3)'#13#10'                                     ,iOrdemItem         i' +
      'nt           default 0 not null'#13#10'                               ' +
      '      ,cCdProduto         varchar(14)'#13#10'                         ' +
      '            ,cNCM               char(8)'#13#10'                       ' +
      '              ,cNmItem            varchar(150)'#13#10'                ' +
      '                     ,cUnidadeMedida     char(3)'#13#10'              ' +
      '                       ,nQtde              decimal(12,3) default' +
      ' 0 not null'#13#10'                                     ,nValUnitario ' +
      '      decimal(12,2) default 0 not null'#13#10'                        ' +
      '             ,nValDesconto       decimal(12,2) default 0 not nul' +
      'l'#13#10'                                     ,nValDespesa        deci' +
      'mal(12,2) default 0 not null'#13#10'                                  ' +
      '   ,nValBaseICMS       decimal(12,2) default 0 not null'#13#10'       ' +
      '                              ,nValBaseICMSSub    decimal(12,2) ' +
      'default 0 not null'#13#10'                                     ,nValIP' +
      'I            decimal(12,2) default 0 not null'#13#10'                 ' +
      '                    ,nAliqIPI           decimal(12,2) default 0 ' +
      'not null'#13#10'                                     ,nPercRedBCICMS  ' +
      '   decimal(12,2) default 0 not null'#13#10'                           ' +
      '          ,nAliqICMS          decimal(12,2) default 0 not null'#13#10 +
      '                                     ,cEmitente          char(1)' +
      ')'#13#10#13#10'    END'#13#10#13#10'    IF (OBJECT_ID('#39'tempdb..#TempRegistro75'#39') IS ' +
      'NULL)'#13#10'    BEGIN'#13#10#13#10'        CREATE TABLE #TempRegistro75 (nCdReg' +
      'istro75      int primary key identity (1,1)'#13#10'                   ' +
      '                  ,dDtInicial         datetime'#13#10'                ' +
      '                     ,dDtFinal           datetime'#13#10'             ' +
      '                        ,cCdProduto         varchar(14)'#13#10'       ' +
      '                              ,cNCM               char(8)'#13#10'     ' +
      '                                ,cNmItem            varchar(150)' +
      #13#10'                                     ,cUnidadeMedida     char(' +
      '3)'#13#10'                                     ,nAliqIPI           dec' +
      'imal(12,2) default 0 not null'#13#10'                                 ' +
      '    ,nAliqICMS          decimal(12,2) default 0 not null'#13#10'      ' +
      '                               ,nPercRedBCICMS     decimal(12,2)' +
      ' default 0 not null'#13#10'                                     ,nValB' +
      'aseICMSSub    decimal(12,2) default 0 not null)'#13#10'    END'#13#10#13#10'    ' +
      'IF (OBJECT_ID('#39'tempdb..#TempMovtoECFItem'#39') IS NULL)'#13#10'    BEGIN'#13#10 +
      #13#10'        CREATE TABLE #TempMovtoECFItem (nCdMovtoECFItem int pr' +
      'imary key identity (1,1)'#13#10'                                      ' +
      ' ,nCdMovtoDiaECF  int'#13#10'                                       ,n' +
      'CdDoctoFiscal  int'#13#10'                                       ,dDtE' +
      'missao      datetime'#13#10'                                       ,cC' +
      'dNumSerieECF  varchar(20)'#13#10'                                     ' +
      '  ,cModelo         char(2)'#13#10'                                    ' +
      '   ,iNrDocto        int'#13#10'                                       ' +
      ',iOrdemItem      int'#13#10'                                       ,nC' +
      'dProduto      int'#13#10'                                       ,cNCM ' +
      '           char(8)'#13#10'                                       ,cNmI' +
      'tem         varchar(150)'#13#10'                                      ' +
      ' ,cUnidadeMedida  char(3)'#13#10'                                     ' +
      '  ,nQtde           decimal(12,3) default 0 not null'#13#10'           ' +
      '                            ,nValTotal       decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                       ,nValBaseI' +
      'CMS    decimal(12,2) default 0 not null'#13#10'                       ' +
      '                ,cCdSTAliq       varchar(4)'#13#10'                   ' +
      '                    ,nValICMS        decimal(12,2) default 0 not' +
      ' null'#13#10'                                       ,nValBaseICMSSub d' +
      'ecimal(12,2) default 0 not null'#13#10'                               ' +
      '        ,nValIPI         decimal(12,2) default 0 not null'#13#10'     ' +
      '                                  ,nAliqIPI        decimal(12,2)' +
      ' default 0 not null'#13#10'                                       ,nPe' +
      'rcRedBCICMS  decimal(12,2) default 0 not null'#13#10'                 ' +
      '                      ,nAliqICMS       decimal(12,2) default 0 n' +
      'ot null)'#13#10'    END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 184
    Top = 344
  end
  object qrySubTributaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempRegistro53')
    Left = 344
    Top = 376
    object qrySubTributarianCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qrySubTributariacCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qrySubTributariacIE: TStringField
      FieldName = 'cIE'
      Size = 15
    end
    object qrySubTributariadDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qrySubTributariacUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qrySubTributariacModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qrySubTributariacSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qrySubTributariaiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qrySubTributariacCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qrySubTributariacEmitente: TStringField
      FieldName = 'cEmitente'
      FixedChar = True
      Size = 1
    end
    object qrySubTributarianValBaseICMSST: TBCDField
      FieldName = 'nValBaseICMSST'
      Precision = 12
      Size = 2
    end
    object qrySubTributarianValICMSRetido: TBCDField
      FieldName = 'nValICMSRetido'
      Precision = 12
      Size = 2
    end
    object qrySubTributarianValDespesas: TBCDField
      FieldName = 'nValDespesas'
      Precision = 12
      Size = 2
    end
    object qrySubTributariacSituacaoCancel: TStringField
      FieldName = 'cSituacaoCancel'
      FixedChar = True
      Size = 1
    end
    object qrySubTributariacCdAnteceipacao: TStringField
      FieldName = 'cCdAnteceipacao'
      FixedChar = True
      Size = 1
    end
  end
  object qryDoctoIPI: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempRegistro51')
    Left = 376
    Top = 376
    object qryDoctoIPInCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoIPIcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryDoctoIPIcIE: TStringField
      FieldName = 'cIE'
      Size = 15
    end
    object qryDoctoIPIdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoIPIcUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryDoctoIPIcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryDoctoIPIiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoIPIcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoIPInValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoIPInValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoIPInValIsentaIPI: TBCDField
      FieldName = 'nValIsentaIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoIPInValOutrasIPI: TBCDField
      FieldName = 'nValOutrasIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoIPIcSituacaoCancel: TStringField
      FieldName = 'cSituacaoCancel'
      FixedChar = True
      Size = 1
    end
  end
  object qryTempMovtoECFItem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempMovtoECFItem'
      ' WHERE nCdMovtoDiaECF = :nPK')
    Left = 536
    Top = 376
    object qryTempMovtoECFItemnCdMovtoECFItem: TAutoIncField
      FieldName = 'nCdMovtoECFItem'
      ReadOnly = True
    end
    object qryTempMovtoECFItemnCdMovtoDiaECF: TIntegerField
      FieldName = 'nCdMovtoDiaECF'
    end
    object qryTempMovtoECFItemnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTempMovtoECFItemdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTempMovtoECFItemcCdNumSerieECF: TStringField
      FieldName = 'cCdNumSerieECF'
    end
    object qryTempMovtoECFItemcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryTempMovtoECFItemiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTempMovtoECFItemiOrdemItem: TIntegerField
      FieldName = 'iOrdemItem'
    end
    object qryTempMovtoECFItemnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryTempMovtoECFItemcNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryTempMovtoECFItemcNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryTempMovtoECFItemcUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryTempMovtoECFItemnQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
      Size = 3
    end
    object qryTempMovtoECFItemnValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemcCdSTAliq: TStringField
      FieldName = 'cCdSTAliq'
      Size = 4
    end
    object qryTempMovtoECFItemnValICMS: TBCDField
      FieldName = 'nValICMS'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnPercRedBCICMS: TBCDField
      FieldName = 'nPercRedBCICMS'
      Precision = 12
      Size = 2
    end
    object qryTempMovtoECFItemnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 12
      Size = 2
    end
  end
  object qryMovtoDiaECF: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial      varchar(10)'
      '       ,@dDtFinal        varchar(10)'
      '       ,@nCdEmpresa      int'
      '       ,@nCdLoja         int'
      ''
      'SET @dDtInicial = :dDtInicial'
      'SET @dDtFinal   = :dDtFinal'
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @nCdLoja    = :nCdLoja'
      ''
      'SELECT * '
      '  FROM MovtoDiaECF'
      ' WHERE dDtMovto  >= convert(datetime,@dDtInicial,103)'
      '   AND dDtMovto   < convert(datetime,@dDtFinal,103) + 1'
      '   AND nCdEmpresa = @nCdEmpresa'
      '   AND nCdLoja    = @nCdLoja')
    Left = 472
    Top = 376
    object qryMovtoDiaECFnCdMovtoDiaECF: TIntegerField
      FieldName = 'nCdMovtoDiaECF'
    end
    object qryMovtoDiaECFnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMovtoDiaECFnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMovtoDiaECFdDtMovto: TDateTimeField
      FieldName = 'dDtMovto'
    end
    object qryMovtoDiaECFcCdNumSerieECF: TStringField
      FieldName = 'cCdNumSerieECF'
    end
    object qryMovtoDiaECFiNrECF: TIntegerField
      FieldName = 'iNrECF'
    end
    object qryMovtoDiaECFcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryMovtoDiaECFiNrOrdemOperInicial: TIntegerField
      FieldName = 'iNrOrdemOperInicial'
    end
    object qryMovtoDiaECFiNrOrdemOperFinal: TIntegerField
      FieldName = 'iNrOrdemOperFinal'
    end
    object qryMovtoDiaECFiNrReducaoZ: TIntegerField
      FieldName = 'iNrReducaoZ'
    end
    object qryMovtoDiaECFiNrReinicioOper: TIntegerField
      FieldName = 'iNrReinicioOper'
    end
    object qryMovtoDiaECFnValVendaBruta: TBCDField
      FieldName = 'nValVendaBruta'
      Precision = 12
      Size = 2
    end
    object qryMovtoDiaECFnValTotalGeral: TBCDField
      FieldName = 'nValTotalGeral'
      Precision = 12
      Size = 2
    end
  end
  object qryMovtoDiaECFAnalitico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM MovtoDiaECFAnalitico'
      ' WHERE nCdMovtoDiaECF = :nPK')
    Left = 504
    Top = 376
    object qryMovtoDiaECFAnaliticonCdMovtoDiaECFAnalitico: TIntegerField
      FieldName = 'nCdMovtoDiaECFAnalitico'
    end
    object qryMovtoDiaECFAnaliticonCdMovtoDiaECF: TIntegerField
      FieldName = 'nCdMovtoDiaECF'
    end
    object qryMovtoDiaECFAnaliticocCdSTAliq: TStringField
      FieldName = 'cCdSTAliq'
      FixedChar = True
      Size = 4
    end
    object qryMovtoDiaECFAnaliticonValAcumTotalParcial: TBCDField
      FieldName = 'nValAcumTotalParcial'
      Precision = 12
      Size = 2
    end
  end
  object qryProdutosServicos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempRegistro75')
    Left = 408
    Top = 376
    object qryProdutosServicosnCdRegistro75: TAutoIncField
      FieldName = 'nCdRegistro75'
      ReadOnly = True
    end
    object qryProdutosServicosdDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryProdutosServicosdDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryProdutosServicoscCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 14
    end
    object qryProdutosServicoscNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryProdutosServicoscNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryProdutosServicoscUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutosServicosnAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      Precision = 12
      Size = 2
    end
    object qryProdutosServicosnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 12
      Size = 2
    end
    object qryProdutosServicosnPercRedBCICMS: TBCDField
      FieldName = 'nPercRedBCICMS'
      Precision = 12
      Size = 2
    end
    object qryProdutosServicosnValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      Precision = 12
      Size = 2
    end
  end
end
