unit fMapaCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxDBEdit, Menus, DBGridEhGrouping;

type
  TfrmMapaCompra = class(TfrmCadastro_Padrao)
    qryMasternCdMapaCompra: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmMapaCompra: TStringField;
    qryMasterdDtGeracao: TDateTimeField;
    qryMasternCdUsuarioGerador: TIntegerField;
    qryMasternCdTerceiroVenc: TIntegerField;
    qryMasternCdTipoRazaoEscolha: TIntegerField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtAutor: TDateTimeField;
    qryMastercOBS: TMemoField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryUsuarioGer: TADOQuery;
    qryUsuarioGernCdUsuario: TIntegerField;
    qryUsuarioGercNmUsuario: TStringField;
    qryUsuarioAutor: TADOQuery;
    qryUsuarioAutornCdUsuario: TIntegerField;
    qryUsuarioAutorcNmUsuario: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoRazaoEscolha: TADOQuery;
    qryTipoRazaoEscolhanCdTipoRazaoEscolha: TIntegerField;
    qryTipoRazaoEscolhacNmRazaoEscolha: TStringField;
    qryUsuarioCancel: TADOQuery;
    qryUsuarioCancelnCdUsuario: TIntegerField;
    qryUsuarioCancelcNmUsuario: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    qryFornMapaCompra: TADOQuery;
    qryFornMapaCompranCdFornMapaCompra: TAutoIncField;
    qryFornMapaCompranCdMapaCompra: TIntegerField;
    qryFornMapaCompranCdTerceiro: TIntegerField;
    qryFornMapaCompracNmFornecedor: TStringField;
    qryFornMapaCompracNmContato: TStringField;
    qryFornMapaCompracTelefone: TStringField;
    qryFornMapaCompracEmail: TStringField;
    dsFornMapaCompra: TDataSource;
    qryPrepara_Temp: TADOQuery;
    dsItem: TDataSource;
    qryItens: TADOQuery;
    qryItensiItem: TIntegerField;
    qryItenscCdProduto: TStringField;
    qryItenscNmItem: TStringField;
    qryItensnQtde: TBCDField;
    qryItensnCdDadosMapaCompra: TIntegerField;
    qryItensnCdFornMapaCompra: TIntegerField;
    qryItensnValUnitario: TBCDField;
    qryItensnValTotalItem: TBCDField;
    qryItensnValFrete: TBCDField;
    qryItensnValDesconto: TBCDField;
    qryItensnPercIPI: TBCDField;
    qryItensnValIPI: TBCDField;
    qryItensnValFinal: TBCDField;
    qryItenscCondPagto: TStringField;
    qryItenscPrazoEntrega: TStringField;
    qryItensdDtValidade: TDateTimeField;
    SP_ATUALIZA_DADOS_MAPA: TADOStoredProc;
    qryFornMapaCompranValTotalCotacao: TBCDField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryItensnPercICMSSub: TBCDField;
    qryItensnValICMSSub: TBCDField;
    qryFornMapaCompranCdFornVenc: TIntegerField;
    qryFornMapaCompranValFrete: TBCDField;
    qryFornMapaCompracCondPagto: TStringField;
    qryFornMapaCompracPrazoEntrega: TStringField;
    qryFornMapaCompradDtValidade: TDateTimeField;
    qryFornMapaCompracOBS: TStringField;
    qryFornMapaCompranValProdutos: TBCDField;
    qryFornMapaCompranValImpostos: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    DBMemo1: TDBMemo;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    Panel1: TPanel;
    cxDBTextEdit1: TcxDBTextEdit;
    qryCancelaMapaCompra: TADOQuery;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    qryMastercFlgGeradoPedido: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExibirRequisies1: TMenuItem;
    qryItensnCdItemMapaCompra: TIntegerField;
    qryPopulaTemp: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure qryFornMapaCompraAfterScroll(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryItensBeforePost(DataSet: TDataSet);
    procedure qryFornMapaCompraBeforePost(DataSet: TDataSet);
    procedure DBGridEh2Exit(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ExibirRequisies1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMapaCompra: TfrmMapaCompra;

implementation

uses fMenu, rMapaCompra, fMapaCompra_Itens;

{$R *.dfm}

procedure TfrmMapaCompra.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MAPACOMPRA' ;
  nCdTabelaSistema  := 60 ;
  nCdConsultaPadrao := 132 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmMapaCompra.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
  PosicionaQuery(qryUsuarioGer, qryMasternCdUsuarioGerador.AsString) ;
  PosicionaQuery(qryUsuarioAutor, qryMasternCdUsuarioAutor.AsString) ;
  PosicionaQuery(qryUsuarioCancel, qryMasternCdUsuarioCancel.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiroVenc.AsString) ;
  PosicionaQuery(qryTipoRazaoEscolha, qryMasternCdTipoRazaoEscolha.AsString) ;

  btSalvar.Enabled   := True ;
  DBGridEh2.ReadOnly := False ;

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      btSalvar.Enabled := False ;
      DBGridEh2.ReadOnly := True ;
  end ;

  qryPrepara_Temp.Close ;
  qryPrepara_Temp.ExecSQL;

  qryPopulaTemp.Close ;
  qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMasternCdMapaCompra.Value ;
  qryPopulaTemp.ExecSQL;

  PosicionaQuery(qryFornMapaCompra, qryMasternCdMapaCompra.asString) ;

end;

procedure TfrmMapaCompra.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  qryFornMapaCompra.Close ;
  qryItens.Close ;

end;

procedure TfrmMapaCompra.qryFornMapaCompraAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryItens, qryFornMapaCompranCdFornMapaCompra.AsString) ;
end;

procedure TfrmMapaCompra.btSalvarClick(Sender: TObject);
begin

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa somente para consulta.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString = '') and (qryMasterdDtCancel.AsString = '') then
  begin

      frmMenu.Connection.Begintrans ;

      try
          SP_ATUALIZA_DADOS_MAPA.Close ;
          SP_ATUALIZA_DADOS_MAPA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  inherited;

  case MessageDlg('Deseja imprimir o mapa agora ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: toolButton10.Click;
  end ;

  qryFornMapaCompra.Close ;
  qryItens.Close ;
  qryMaster.Close ;

end;

procedure TfrmMapaCompra.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;

  if (dsItem.State <> dsBrowse) and not DbGridEh2.ReadOnly then
  begin
      qryItensnValTotalItem.Value := qryItensnValUnitario.Value * qryItensnQtde.Value ;

      if (qryItensnPercIPI.Value > 0) then
      begin
          qryitensnValIPI.Value := qryItensnValTotalItem.Value * (qryItensnPercIPI.Value / 100) ;
      end ;

      qryItensnValFinal.Value := qryItensnValTotalItem.Value + qryItensnValIPI.Value + qryItensnValFrete.Value - qryItensnValDesconto.Value ;
  end ;

end;

procedure TfrmMapaCompra.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  qryFornMapaCompra.Close ;
  qryItens.Close ;

end;

procedure TfrmMapaCompra.ToolButton10Click(Sender: TObject);
var
  objRel : TrptMapaCompra;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione um mapa.') ;
      exit ;
  end ;

  objRel :=  TrptMapaCompra.Create(nil);
  Try
     Try
        objRel.usp_Relatorio.Close ;
        objRel.usp_Relatorio.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
        objRel.usp_Relatorio.Open ;

        objRel.SPREL_MAPA_HEADER.Close ;
        objRel.SPREL_MAPA_HEADER.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
        objRel.SPREL_MAPA_HEADER.Open ;

        objrel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

        objRel.QuickRep1.Preview;
     except
        MensagemErro('Erro na cria��o do relat�rio');
        raise;
     end;
  Finally;
     FreeAndNil(objRel);
  end;

end;

procedure TfrmMapaCompra.qryItensBeforePost(DataSet: TDataSet);
begin

  if (qryItensnValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do item.') ;
      abort ;
  end ;

  if (qryItensnPercIPI.Value < 0) then
  begin
      MensagemAlerta('Percentual de IPI inv�lido.') ;
      abort ;
  end ;

  if (qryItensnPercICMSSub.Value < 0) then
  begin
      MensagemAlerta('Percentual de ICMS de Substitui��o tribut�ria inv�lido.') ;
      abort ;
  end ;

  qryItensnValTotalItem.Value := qryItensnValUnitario.Value * qryItensnQtde.Value ;

  if (qryItensnPercIPI.Value > 0) then
      qryitensnValIPI.Value := qryItensnValTotalItem.Value * (qryItensnPercIPI.Value / 100) ;

  if (qryItensnPercICMSSub.Value > 0) then
      qryitensnValICMSSub.Value := qryItensnValTotalItem.Value * (qryItensnPercICMSSub.Value / 100) ;

  qryItensnValFinal.Value := qryItensnValTotalItem.Value + qryItensnValIPI.Value + qryItensnValICMSSub.Value ;

  inherited;

end;

procedure TfrmMapaCompra.qryFornMapaCompraBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryFornMapaCompranValTotalCotacao.Value := qryFornMapaCompranValProdutos.Value + qryFornMapaCompranValImpostos.Value + qryFornMapaCompranValFrete.Value ;
  
end;

procedure TfrmMapaCompra.DBGridEh2Exit(Sender: TObject);
begin
  inherited;

  if (qryItens.Active) then
      if (qryItens.State <> dsBrowse) then
          qryItens.Post ;
          
end;

procedure TfrmMapaCompra.ToolButton13Click(Sender: TObject);
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhum mapa ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa de compra j� est� cancelado.') ;
      exit ;
  end ;

  if (qryMastercFlgGeradoPedido.Value = 1) then
  begin
      MensagemAlerta('Este mapa de compra j� gerou pedidos e n�o pode ser cancelado.') ;
      exit ;
  end ;

  if (qryMasternCdUsuarioGerador.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Este Mapa de compra s� pode se cancelado por ' + Trim(DBEdit5.Text)) ;
      exit ;
  end ;

  case MessageDlg('Confirma o cancelamento deste mapa de compra ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if (qryMasterdDtAutor.AsString <> '') then
  begin

      case MessageDlg('Este Mapa de Compra est� Autorizado! Tem Certeza que deseja cancelar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryCancelaMapaCompra.Close ;
      qryCancelaMapaCompra.Parameters.ParamByName('nPK').Value        := qryMasternCdMapaCompra.Value ;
      qryCancelaMapaCompra.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryCancelaMapaCompra.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdMapaCompra.AsString) ;

  ShowMessage('Mapa Cancelado com Sucesso. As requisi��es foram reabertas.') ;

end;

procedure TfrmMapaCompra.ExibirRequisies1Click(Sender: TObject);
var
  objForm : TfrmMapaCompra_Itens;
begin
  inherited;

  if (qryItens.Active) and (not qryItens.eof) then
  begin
      objForm := TfrmMapaCompra_Itens.Create(nil);
      PosicionaQuery(objForm.qryRequisicoes, qryItensnCdItemMapaCompra.asString) ;
      ShowForm(objForm,True);
  end ;

end;

initialization
    RegisterClass(TfrmMapaCompra) ;

end.
