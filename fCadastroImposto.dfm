inherited frmCadastroImpostos: TfrmCadastroImpostos
  Left = 91
  Top = 117
  Width = 1021
  Height = 690
  Caption = 'Cadastro de Impostos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1005
    Height = 629
  end
  object Label1: TLabel [1]
    Left = 77
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 66
    Top = 61
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 9
    Top = 85
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria Financeira'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 37
    Top = 109
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Credor'
    FocusControl = DBEdit5
  end
  object Label5: TLabel [5]
    Left = 17
    Top = 133
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie T'#237'tulo Prov.'
    FocusControl = DBEdit7
  end
  object Label6: TLabel [6]
    Left = 9
    Top = 180
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Servico Imposto'
    FocusControl = DBEdit8
  end
  object Label8: TLabel [7]
    Left = 38
    Top = 204
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = '% Base C'#225'lculo'
    FocusControl = DBEdit10
  end
  object Label9: TLabel [8]
    Left = 199
    Top = 204
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = '% Al'#237'quota'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [9]
    Left = 10
    Top = 228
    Width = 105
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'd. Serv. Municipal'
    FocusControl = DBEdit14
  end
  object Label14: TLabel [10]
    Left = 354
    Top = 204
    Width = 105
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Dias Vencto'
    FocusControl = DBEdit16
  end
  object Label7: TLabel [11]
    Left = 555
    Top = 204
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dia Vencto'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [12]
    Left = 12
    Top = 157
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie T'#237'tulo Pagto'
    FocusControl = DBEdit13
  end
  inherited ToolBar2: TToolBar
    Width = 1005
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTipoImposto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [15]
    Left = 120
    Top = 55
    Width = 650
    Height = 19
    DataField = 'cNmTipoImposto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [16]
    Left = 120
    Top = 79
    Width = 65
    Height = 19
    DataField = 'nCdCategFinanc'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [17]
    Tag = 1
    Left = 190
    Top = 79
    Width = 650
    Height = 19
    DataField = 'cNmCategFinanc'
    DataSource = dsCategFinanc
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [18]
    Left = 120
    Top = 103
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroCredor'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 190
    Top = 103
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [20]
    Left = 120
    Top = 127
    Width = 65
    Height = 19
    DataField = 'nCdEspTitProv'
    DataSource = dsMaster
    TabOrder = 7
    OnExit = DBEdit7Exit
    OnKeyDown = DBEdit7KeyDown
  end
  object DBEdit8: TDBEdit [21]
    Left = 120
    Top = 174
    Width = 65
    Height = 19
    DataField = 'nCdTipoServicoImposto'
    DataSource = dsMaster
    TabOrder = 11
    OnExit = DBEdit8Exit
    OnKeyDown = DBEdit8KeyDown
  end
  object DBEdit10: TDBEdit [22]
    Left = 120
    Top = 198
    Width = 65
    Height = 19
    DataField = 'nPercBaseCalc'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit11: TDBEdit [23]
    Left = 259
    Top = 198
    Width = 65
    Height = 19
    DataField = 'nPercAliq'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit14: TDBEdit [24]
    Left = 120
    Top = 222
    Width = 65
    Height = 19
    DataField = 'cCdCodServMunicipal'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit16: TDBEdit [25]
    Left = 464
    Top = 198
    Width = 65
    Height = 19
    DataField = 'iNumeroDias'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBRadioGroup1: TDBRadioGroup [26]
    Left = 288
    Top = 253
    Width = 161
    Height = 41
    Caption = 'Abatimento'
    Columns = 2
    DataField = 'cFlgAbatimento'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 19
    Values.Strings = (
      '1'
      '0')
  end
  object DBRadioGroup2: TDBRadioGroup [27]
    Left = 456
    Top = 253
    Width = 161
    Height = 41
    Caption = 'Agrupa Pagamento'
    Columns = 2
    DataField = 'cFlgAgrupaPagamento'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 20
    Values.Strings = (
      '1'
      '0')
  end
  object DBRadioGroup3: TDBRadioGroup [28]
    Left = 119
    Top = 343
    Width = 330
    Height = 41
    Caption = 'Reten'#231#227'o por Data'
    Columns = 3
    DataField = 'cFlgTipoRetencao'
    DataSource = dsMaster
    Items.Strings = (
      'Emiss'#227'o'
      'Recebimento'
      'Vencimento')
    TabOrder = 23
    Values.Strings = (
      '0'
      '1'
      '2')
  end
  object DBRadioGroup4: TDBRadioGroup [29]
    Left = 456
    Top = 343
    Width = 161
    Height = 41
    Caption = 'Cooperado'
    Columns = 2
    DataField = 'cFlgCooperado'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 24
    Values.Strings = (
      '1'
      '0')
  end
  object DBEdit9: TDBEdit [30]
    Left = 616
    Top = 198
    Width = 65
    Height = 19
    DataField = 'iDiaVencto'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit12: TDBEdit [31]
    Tag = 1
    Left = 190
    Top = 127
    Width = 650
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTitProv
    TabOrder = 8
  end
  object DBEdit13: TDBEdit [32]
    Left = 120
    Top = 151
    Width = 65
    Height = 19
    DataField = 'nCdEspTitPagto'
    DataSource = dsMaster
    TabOrder = 9
    OnExit = DBEdit13Exit
    OnKeyDown = DBEdit13KeyDown
  end
  object DBEdit15: TDBEdit [33]
    Tag = 1
    Left = 190
    Top = 151
    Width = 650
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTitPagto
    TabOrder = 10
  end
  object DBEdit17: TDBEdit [34]
    Tag = 1
    Left = 259
    Top = 174
    Width = 581
    Height = 19
    DataField = 'cNmTipoServicoImposto'
    DataSource = dsTipoServicoImposto
    TabOrder = 12
  end
  object DBRadioGroup5: TDBRadioGroup [35]
    Left = 624
    Top = 253
    Width = 161
    Height = 41
    Caption = 'Pessoa F'#237'sica'
    Columns = 2
    DataField = 'cFlgPessoaFisica'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 21
    Values.Strings = (
      '1'
      '0')
  end
  object DBRadioGroup6: TDBRadioGroup [36]
    Left = 120
    Top = 253
    Width = 161
    Height = 41
    Caption = 'Protege Base'
    Columns = 2
    DataField = 'cFlgProtegeBase'
    DataSource = dsMaster
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 18
    Values.Strings = (
      '1'
      '0')
  end
  object DBRadioGroup7: TDBRadioGroup [37]
    Left = 120
    Top = 299
    Width = 665
    Height = 41
    Caption = 'Tipo de Imposto'
    Columns = 5
    DataField = 'cFlgTipoImposto'
    DataSource = dsMaster
    Items.Strings = (
      'I.R.R.F'
      'I.N.S.S'
      'Pis/Cofins/C.S.S.L'
      'I.S.S'
      'Outros')
    TabOrder = 22
    Values.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4')
  end
  object DBEdit18: TDBEdit [38]
    Tag = 1
    Left = 190
    Top = 174
    Width = 65
    Height = 19
    DataField = 'iNroTipoServicoImposto'
    DataSource = dsTipoServicoImposto
    TabOrder = 25
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoImposto'
      'WHERE nCdTipoImposto = :nPK')
    Top = 184
    object qryMasternCdTipoImposto: TIntegerField
      FieldName = 'nCdTipoImposto'
    end
    object qryMastercNmTipoImposto: TStringField
      FieldName = 'cNmTipoImposto'
      Size = 50
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdTipoServicoImposto: TIntegerField
      FieldName = 'nCdTipoServicoImposto'
    end
    object qryMasternCdTerceiroCredor: TIntegerField
      FieldName = 'nCdTerceiroCredor'
    end
    object qryMastercFlgAbatimento: TIntegerField
      FieldName = 'cFlgAbatimento'
    end
    object qryMastercFlgAgrupaPagamento: TIntegerField
      FieldName = 'cFlgAgrupaPagamento'
    end
    object qryMastercFlgTipoRetencao: TIntegerField
      FieldName = 'cFlgTipoRetencao'
    end
    object qryMastercFlgCooperado: TIntegerField
      FieldName = 'cFlgCooperado'
    end
    object qryMasternCdEspTitProv: TIntegerField
      FieldName = 'nCdEspTitProv'
    end
    object qryMasternCdEspTitPagto: TIntegerField
      FieldName = 'nCdEspTitPagto'
    end
    object qryMastercCdCodServMunicipal: TStringField
      FieldName = 'cCdCodServMunicipal'
    end
    object qryMastercFlgPessoaFisica: TIntegerField
      FieldName = 'cFlgPessoaFisica'
    end
    object qryMastercFlgProtegeBase: TIntegerField
      FieldName = 'cFlgProtegeBase'
    end
    object qryMastercFlgTipoImposto: TIntegerField
      FieldName = 'cFlgTipoImposto'
    end
    object qryMasternPercBaseCalc: TBCDField
      FieldName = 'nPercBaseCalc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercAliq: TBCDField
      FieldName = 'nPercAliq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriNumeroDias: TIntegerField
      FieldName = 'iNumeroDias'
    end
    object qryMasteriDiaVencto: TIntegerField
      FieldName = 'iDiaVencto'
    end
  end
  inherited dsMaster: TDataSource
    Top = 184
  end
  inherited qryID: TADOQuery
    Top = 184
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 240
  end
  inherited qryStat: TADOQuery
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 656
    Top = 280
  end
  inherited ImageList1: TImageList
    Top = 280
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM CategFinanc'
      'WHERE nCdCategFinanc = :nPK'
      'AND cFlgRecDes = '#39'D'#39)
    Left = 888
    Top = 128
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryCategFinancnCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryCategFinanccFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
    object qryCategFinancnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryCategFinancnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCategFinancdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object dsCategFinanc: TDataSource
    DataSet = qryCategFinanc
    Left = 920
    Top = 128
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdStatus'
      ',nCdMoeda'
      'FROM TERCEIRO'
      'WHERE nCdTerceiro = :nPK')
    Left = 888
    Top = 160
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 920
    Top = 160
  end
  object qryEspTitProv: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK'
      'AND cTipoMov = '#39'P'#39
      'AND cFlgPrev = 1')
    Left = 888
    Top = 192
    object qryEspTitProvnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitProvnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitProvcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitProvcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitProvcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object dsEspTitProv: TDataSource
    DataSet = qryEspTitProv
    Left = 920
    Top = 192
  end
  object qryEspTitPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK'
      'AND cTipoMov = '#39'P'#39
      'AND cFlgPrev = 0'
      '')
    Left = 888
    Top = 226
    object IntegerField4: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object IntegerField5: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object StringField3: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object IntegerField6: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object dsEspTitPagto: TDataSource
    DataSet = qryEspTitPagto
    Left = 920
    Top = 227
  end
  object qryTipoServicoImposto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoServicoImposto'
      'WHERE nCdTipoServicoImposto = :nPK')
    Left = 888
    Top = 264
    object qryTipoServicoImpostonCdTipoServicoImposto: TIntegerField
      FieldName = 'nCdTipoServicoImposto'
    end
    object qryTipoServicoImpostocNmTipoServicoImposto: TStringField
      FieldName = 'cNmTipoServicoImposto'
      Size = 150
    end
    object qryTipoServicoImpostoiNroTipoServicoImposto: TIntegerField
      FieldName = 'iNroTipoServicoImposto'
    end
  end
  object dsTipoServicoImposto: TDataSource
    DataSet = qryTipoServicoImposto
    Left = 920
    Top = 264
  end
end
