unit fEnvioClienteCobradora_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, StdCtrls,
  ImgList, ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmEnvioClienteCobradora_Titulos = class(TfrmProcesso_Padrao)
    qryTitulosPendentes: TADOQuery;
    dsTitulosPendentes: TDataSource;
    qryTitulosPendentesnCdTitulo: TIntegerField;
    qryTitulosPendentesnCdLojaTit: TStringField;
    qryTitulosPendentescNrTit: TStringField;
    qryTitulosPendentesiParcela: TIntegerField;
    qryTitulosPendentescNmEspTit: TStringField;
    qryTitulosPendentesdDtEmissao: TDateTimeField;
    qryTitulosPendentesdDtVenc: TDateTimeField;
    qryTitulosPendentesiDiasAtraso: TIntegerField;
    qryTitulosPendentesnValTit: TBCDField;
    qryTitulosPendentesnValLiq: TBCDField;
    qryTitulosPendentesnValDesconto: TBCDField;
    qryTitulosPendentesnSaldoTit: TBCDField;
    qryTitulosPendentescFlgAtraso: TIntegerField;
    DBGridEh1: TDBGridEh;
    qryAux: TADOQuery;
    cmd: TADOCommand;
    SP_REGISTRA_TITULO_COBRADORA_MANUAL: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdCobradora      : integer ;
    dDtEnvioCobradora : TDateTime;
    nCdTerceiro       : integer;
  end;

var
  frmEnvioClienteCobradora_Titulos: TfrmEnvioClienteCobradora_Titulos;

implementation

uses fMenu,rFichaCobradora_Individual_view;

{$R *.dfm}

procedure TfrmEnvioClienteCobradora_Titulos.ToolButton1Click(
  Sender: TObject);
var
  Objform : TrptFichaCobradora_Individual_view;
begin
  inherited;

  if (MessageDlg('Confirma o envio destes t�tulos para cobradora ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;


  qryTitulosPendentes.First;

  frmMenu.Connection.BeginTrans;

  try
      cmd.Execute;

      while not qryTitulosPendentes.Eof do
      begin

          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('INSERT INTO #TempTerceiroEnvioCobradora (nCdTerceiro,nCdTitulo) VALUES (' + IntToStr(nCdTerceiro) + ',' + qryTitulosPendentesnCdTitulo.AsString + ')') ;
          qryAux.ExecSQL ;

          qryTitulosPendentes.Next;

      end ;

      SP_REGISTRA_TITULO_COBRADORA_MANUAL.Close;
      SP_REGISTRA_TITULO_COBRADORA_MANUAL.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro;
      SP_REGISTRA_TITULO_COBRADORA_MANUAL.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      SP_REGISTRA_TITULO_COBRADORA_MANUAL.Parameters.ParamByName('@nCdCobradora').Value := nCdCobradora;
      SP_REGISTRA_TITULO_COBRADORA_MANUAL.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryTitulosPendentes.First;

  ShowMessage('T�tulos atualizados com sucesso.') ;



  try
      ObjForm := TrptFichaCobradora_Individual_view.Create(nil);

      ObjForm.qryFicha.Close;
      ObjForm.qryFicha.Parameters.ParamByName('nCdCobradora').Value := nCdCobradora;
      ObjForm.qryFicha.Parameters.ParamByName('nCdTerceiro').Value  := nCdTerceiro;
      ObjForm.qryFicha.Parameters.ParamByName('dDtIni').Value       := DateToStr(Date);
      ObjForm.qryFicha.Parameters.ParamByName('dDtFim').Value       := DateToStr(Date);
      ObjForm.qryFicha.Open;

      if ObjForm.qryFicha.IsEmpty then
      begin
          MensagemAlerta('Ficha cobradora n�o encontrado.');
          FreeAndNil(ObjForm);
      end
      else
      begin
          ObjForm.QuickRep1.PreviewModal;
      end;

  Except
      MensagemErro('Erro no processamento.');
      FreeAndNil(ObjForm);
      raise;
  end;

  FreeAndNil(ObjForm);


  Close;

end;

end.
