inherited frmEspTit: TfrmEspTit
  Left = 259
  Top = 56
  Width = 884
  Height = 629
  Caption = 'Esp'#233'cie de T'#237'tulo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 868
    Height = 566
  end
  object Label1: TLabel [1]
    Left = 60
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Esp'#233'cie'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 49
    Top = 94
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 15
    Top = 118
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Movimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 135
    Top = 118
    Width = 119
    Height = 13
    Caption = '(R - Receber / P - Pagar)'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    Width = 868
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdEspTit'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Left = 104
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdGrupoEspTit'
    DataSource = dsMaster
    TabOrder = 2
    OnKeyDown = DBEdit2KeyDown
    OnKeyUp = DBEdit2KeyUp
  end
  object DBEdit3: TDBEdit [9]
    Left = 104
    Top = 88
    Width = 505
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [10]
    Left = 104
    Top = 112
    Width = 25
    Height = 19
    DataField = 'cTipoMov'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [11]
    Tag = 1
    Left = 172
    Top = 64
    Width = 437
    Height = 19
    DataField = 'cNmGrupoEspTit'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBCheckBox1: TDBCheckBox [12]
    Left = 104
    Top = 136
    Width = 169
    Height = 17
    Caption = 'Somente para Provis'#227'o'
    DataField = 'cFlgPrev'
    DataSource = dsMaster
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [13]
    Left = 104
    Top = 160
    Width = 169
    Height = 17
    Caption = 'Incluir Rotina Cobran'#231'a'
    DataField = 'cFlgCobranca'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox3: TDBCheckBox [14]
    Left = 104
    Top = 184
    Width = 169
    Height = 17
    Caption = 'Apurar Inadimpl'#234'ncia'
    DataField = 'cFlgInadimplencia'
    DataSource = dsMaster
    TabOrder = 8
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [15]
    Left = 16
    Top = 216
    Width = 593
    Height = 361
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 11
    ClientRectBottom = 357
    ClientRectLeft = 4
    ClientRectRight = 589
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios Movimenta'#231#227'o'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 585
        Height = 333
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioMovto
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh2KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios|C'#243'd'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rios|Nome Usu'#225'rio'
            Width = 480
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Usu'#225'rios Consultar/ Listar'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 585
        Height = 333
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioListar
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios|C'#243'd'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rios|Nome Usu'#225'rio'
            Width = 480
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBCheckBox4: TDBCheckBox [16]
    Left = 282
    Top = 136
    Width = 169
    Height = 17
    Caption = 'Esp'#233'cie de Adiantamento'
    DataField = 'cFlgAdiantamento'
    DataSource = dsMaster
    TabOrder = 9
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox5: TDBCheckBox [17]
    Left = 282
    Top = 160
    Width = 169
    Height = 17
    Caption = 'Exibe Raz'#227'o/Di'#225'rio Auxiliar'
    DataField = 'cFlgExibeRazaoAuxiliar'
    DataSource = dsMaster
    TabOrder = 10
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ESPTIT'
      'WHERE nCdEspTit = :nPK')
    Left = 544
    Top = 168
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMasternCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryMastercNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryMastercTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryMastercFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
    object qryMastercNmGrupoEspTit: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmGrupoEspTit'
      LookupDataSet = qryGrupoEspTit
      LookupKeyFields = 'nCdGrupoEspTit'
      LookupResultField = 'cNmGrupoEspTit'
      KeyFields = 'nCdGrupoEspTit'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercFlgCobranca: TIntegerField
      FieldName = 'cFlgCobranca'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMastercFlgInadimplencia: TIntegerField
      FieldName = 'cFlgInadimplencia'
    end
    object qryMastercFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryMastercFlgExibeRazaoAuxiliar: TIntegerField
      FieldName = 'cFlgExibeRazaoAuxiliar'
    end
  end
  inherited dsMaster: TDataSource
    Left = 544
    Top = 200
  end
  inherited qryID: TADOQuery
    Left = 480
    Top = 200
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 448
    Top = 200
  end
  inherited qryStat: TADOQuery
    Left = 512
    Top = 200
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 448
    Top = 168
  end
  inherited ImageList1: TImageList
    Left = 576
    Top = 200
  end
  object qryGrupoEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOESPTIT')
    Left = 576
    Top = 168
    object qryGrupoEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryGrupoEspTitcNmGrupoEspTit: TStringField
      FieldName = 'cNmGrupoEspTit'
      Size = 50
    end
  end
  object qryUsuarioMovto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioMovtoBeforePost
    OnCalcFields = qryUsuarioMovtoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UsuarioEspTit'
      ' WHERE nCdEspTit =:nPK'
      '   AND cFlgTipoAcesso = '#39'M'#39
      'ORDER BY nCdUsuarioEspTit')
    Left = 640
    Top = 168
    object qryUsuarioMovtonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioMovtonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryUsuarioMovtocFlgTipoAcesso: TStringField
      FieldName = 'cFlgTipoAcesso'
      FixedChar = True
      Size = 1
    end
    object qryUsuarioMovtocNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioMovtonCdUsuarioEspTit: TIntegerField
      FieldName = 'nCdUsuarioEspTit'
    end
  end
  object qryUsuarioListar: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioListarBeforePost
    OnCalcFields = qryUsuarioListarCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioEspTit'
      'WHERE nCdEspTit =:nPK'
      'AND cFlgTipoAcesso = '#39'L'#39
      'ORDER BY nCdUsuarioEspTit')
    Left = 608
    Top = 168
    object qryUsuarioListarnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioListarnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryUsuarioListarcFlgTipoAcesso: TStringField
      FieldName = 'cFlgTipoAcesso'
      FixedChar = True
      Size = 1
    end
    object qryUsuarioListarcNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioListarnCdUsuarioEspTit: TIntegerField
      FieldName = 'nCdUsuarioEspTit'
    end
  end
  object dsUsuarioMovto: TDataSource
    DataSet = qryUsuarioMovto
    Left = 640
    Top = 200
  end
  object dsUsuarioListar: TDataSource
    DataSet = qryUsuarioListar
    Left = 608
    Top = 200
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Usuario'
      'WHERE nCdUsuario =:nPK')
    Left = 480
    Top = 168
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryVerificaTituloProvisao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM Titulo'
      ' WHERE nCdEspTit = :nPK')
    Left = 512
    Top = 168
  end
end
