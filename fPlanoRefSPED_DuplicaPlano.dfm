inherited frmPlanoRefSPED_DuplicaPlano: TfrmPlanoRefSPED_DuplicaPlano
  Left = 528
  Top = 304
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Duplicar Plano Referencial'
  ClientHeight = 151
  ClientWidth = 350
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 350
    Height = 122
  end
  object Label1: TLabel [1]
    Left = 37
    Top = 95
    Width = 70
    Height = 13
    Caption = 'Novo Exerc'#237'cio'
  end
  object Label5: TLabel [2]
    Left = 15
    Top = 63
    Width = 94
    Height = 13
    Caption = 'Exerc'#237'cio de Origem'
  end
  inherited ToolBar1: TToolBar
    Width = 350
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object mskAnoRef: TMaskEdit [4]
    Left = 112
    Top = 56
    Width = 209
    Height = 21
    EditMask = '####;1; '
    MaxLength = 4
    TabOrder = 1
    Text = '    '
  end
  object mskAno: TMaskEdit [5]
    Left = 112
    Top = 88
    Width = 209
    Height = 21
    EditMask = '####;1; '
    MaxLength = 4
    TabOrder = 2
    Text = '    '
  end
  inherited ImageList1: TImageList
    Left = 168
    Top = 8
  end
  object SP_DUPLICA_PLANO_REFERENCIAL_SPED: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_DUPLICA_PLANO_REFERENCIAL_SPED'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@iAnoRef'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iAno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 216
    Top = 8
  end
end
