unit fParam_TipoPedido_Ajuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls;

type
  TfrmParam_TipoPedido_Ajuda = class(TfrmProcesso_Padrao)
    Label3: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    Label12: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Label1: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParam_TipoPedido_Ajuda: TfrmParam_TipoPedido_Ajuda;

implementation

{$R *.dfm}

procedure TfrmParam_TipoPedido_Ajuda.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_ESCAPE) then
      ToolButton2.Click;
end;

end.
