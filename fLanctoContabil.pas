unit fLanctoContabil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, Buttons, cxLookAndFeelPainters, StdCtrls, cxButtons,
  DBGridEhGrouping, ToolCtrlsEh, cxPC, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, GridsEh, DBGridEh,
  Mask, DBCtrls, ER2Lookup;

type
  TfrmLanctoContabil = class(TfrmCadastro_Padrao)
    qryMasternCdLanctoContabil: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasterdDtLancto: TDateTimeField;
    qryMastercNrDocto: TStringField;
    qryMasternCdHistoricoPadrao: TIntegerField;
    qryMastercNmHistoricoPadrao: TStringField;
    qryMastercNmHistoricoDigitado: TStringField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdTabelaSistema: TIntegerField;
    qryMasteriIDRegistro: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit2: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit3: TDBEdit;
    qryHistPadrao: TADOQuery;
    qryHistPadraonCdHistoricoPadrao: TIntegerField;
    qryHistPadraocNmHistoricoPadrao: TStringField;
    dsHistPadrao: TDataSource;
    DBEdit6: TDBEdit;
    Label3: TLabel;
    dDtLancto: TDateTimePicker;
    qryMasterdDtCad: TDateTimeField;
    qryContaLanctoContabil: TADOQuery;
    dsContaLanctoContabil: TDataSource;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    qryMastercFlgProcessado: TIntegerField;
    qryPlanoConta: TADOQuery;
    qryCentroCusto: TADOQuery;
    qryUnidadeNegocio: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryProjeto: TADOQuery;
    qryProjetonCdProjeto: TIntegerField;
    qryProjetocNmProjeto: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Label7: TLabel;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    dsUsuario: TDataSource;
    edtStatus: TEdit;
    Label10: TLabel;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    qryTabelaSistema: TADOQuery;
    qryTabelaSistemanCdTabelaSistema: TIntegerField;
    qryTabelaSistemacNmTabela: TStringField;
    DBEdit11: TDBEdit;
    dsTabelaSistema: TDataSource;
    qryContaLanctoContabilnCdContaLanctoContabil: TIntegerField;
    qryContaLanctoContabilnCdLanctoContabil: TIntegerField;
    qryContaLanctoContabilnCdPlanoConta: TIntegerField;
    qryContaLanctoContabildDtLancto: TDateTimeField;
    qryContaLanctoContabilnValLancto: TBCDField;
    qryContaLanctoContabilcHistorico: TStringField;
    qryContaLanctoContabilnCdCC: TIntegerField;
    qryContaLanctoContabilnCdUnidadeNegocio: TIntegerField;
    qryContaLanctoContabilnCdProjeto: TIntegerField;
    qryContaLanctoContabilcFlgLanctoEncerramento: TIntegerField;
    qryContaLanctoContabilnValCredito: TBCDField;
    qryContaLanctoContabilnValDebito: TBCDField;
    qryContaLanctoContabilcNmPlanoConta: TStringField;
    qryContaLanctoContabilcNmCC: TStringField;
    qryContaLanctoContabilcNmUnidadeNegocio: TStringField;
    qryContaLanctoContabilcNmProjeto: TStringField;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    Image2: TImage;
    qryPlanoContacFlgExigeCC: TIntegerField;
    qryPlanoContacFlgExigeUnidadeNegocio: TIntegerField;
    SP_PROCESSA_LANCAMENTO_CONTABIL: TADOStoredProc;
    qryValidaSaldo: TADOQuery;
    qryValidaSaldonValDiferenca: TBCDField;
    btProcessarLancto: TToolButton;
    qryExecProc: TADOQuery;
    dsExecProc: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btProcessarLanctoClick(Sender: TObject);
    procedure qryContaLanctoContabilBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryContaLanctoContabilCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure qryContaLanctoContabilnValCreditoChange(Sender: TField);
    procedure qryContaLanctoContabilnValDebitoChange(Sender: TField);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterDelete(DataSet: TDataSet);
  private
    { Private declarations }
    procedure HabilitaCampos;
    procedure DesabilitaCampos;
  public
    { Public declarations }
  end;

var
  frmLanctoContabil: TfrmLanctoContabil;

implementation

uses
  fMenu, fLookup_Padrao, fLanctoContabil_LanctoErro;

{$R *.dfm}

procedure TfrmLanctoContabil.HabilitaCampos;
begin
  DBEdit4.ReadOnly   := False;
  DBEdit5.ReadOnly   := False;
  DBEdit7.ReadOnly   := False;
  DBGridEh1.ReadOnly := False;
  dDtLancto.Enabled  := True;
  edtStatus.Text     := 'Digitado';
end;

procedure TfrmLanctoContabil.DesabilitaCampos;
begin
  DBEdit4.ReadOnly   := True;
  DBEdit5.ReadOnly   := True;
  DBEdit7.ReadOnly   := True;
  DBGridEh1.ReadOnly := True;
  dDtLancto.Enabled  := False;
  edtStatus.Text     := 'Processado';
end;

procedure TfrmLanctoContabil.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LANCTOCONTABIL';
  nCdTabelaSistema  := 512;
  nCdConsultaPadrao := 762;

  bLimpaAposSalvar  := False;
  dDtLancto.Date    := Now();

end;

procedure TfrmLanctoContabil.DBEdit5Exit(Sender: TObject);
var
  cParametro      : String;
  cValor          : String;
  iPosicaoInicial : Integer;
  iPosicaoFinal   : Integer;
  iQtdCaracter    : Integer;
begin
  cParametro      := '';
  cValor          := '';
  iPosicaoInicial := 0;
  iPosicaoFinal   := 0;
  iQtdCaracter    := 0;

  if (qryMastercFlgProcessado.Value = 1) then
      Abort;

  if (qryMaster.State <> dsInactive) then
  begin
      qryHistPadrao.Close;
      qryHistPadrao.Parameters.ParamByName('nPK').Value := qryMasternCdHistoricoPadrao.Value;
      qryHistPadrao.Open;

      if (not qryHistPadrao.IsEmpty) then
      begin
          qryMastercNmHistoricoPadrao.Value   := qryHistPadraocNmHistoricoPadrao.Value;
          qryMastercNmHistoricoDigitado.Value := qryHistPadraocNmHistoricoPadrao.Value;
      end

      else
      begin
          qryMastercNmHistoricoPadrao.Value   := '';
          qryMastercNmHistoricoDigitado.Value := '';
          Abort;
      end;

      { -- substitui parametros pelo valor informado atrav�s do inputbox -- }
      while (Pos('[', qryMastercNmHistoricoDigitado.Value) > 0)  do
      begin
          iPosicaoInicial := Pos('[', qryMastercNmHistoricoDigitado.Value);
          iPosicaoFinal   := Pos(']', qryMastercNmHistoricoDigitado.Value);
          iQtdCaracter    := (iPosicaoFinal + 1) - (iPosicaoInicial - 1);

          cParametro := Copy(qryMastercNmHistoricoDigitado.Value, iPosicaoInicial, iQtdCaracter);
          cValor     := InputBox('Complemento do Hist�rico', cParametro, '');

          qryMastercNmHistoricoDigitado.Value := StringReplace(qryMastercNmHistoricoDigitado.Value, cParametro, cValor + ' ', [rfReplaceAll, rfIgnoreCase]);
      end;
  end;
end;

procedure TfrmLanctoContabil.btIncluirClick(Sender: TObject);
begin
  inherited;

  HabilitaCampos;

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva;
  qryEmpresa.Open;

  qryUsuario.Close;
  qryUsuario.Parameters.ParamByName('nPK').Value := frmMenu.nCdUsuarioLogado;
  qryUsuario.Open;

  qryMasternCdEmpresa.Value    := qryEmpresanCdEmpresa.Value;
  qryMasternCdUsuarioCad.Value := qryUsuarionCdUsuario.Value;
  qryMasterdDtCad.Value        := Now();
  qryContaLanctoContabil.Close;
  qryTabelaSistema.Close;
  qryHistPadrao.Close;

  DBEdit4.SetFocus;
end;

procedure TfrmLanctoContabil.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryMasterdDtCad.Value             := Now();
  qryMasterdDtLancto.Value          := dDtLancto.DateTime;
  qryMasternCdHistoricoPadrao.Value := qryHistPadraonCdHistoricoPadrao.Value;
end;

procedure TfrmLanctoContabil.btCancelarClick(Sender: TObject);
begin
  inherited;

  HabilitaCampos;
  edtStatus.Text := '';
  btProcessarLancto.Enabled := True;

  qryEmpresa.Close;
  qryUsuario.Close;
  qryTabelaSistema.Close;
  qryHistPadrao.Close;
  qryContaLanctoContabil.Close;
end;

procedure TfrmLanctoContabil.btProcessarLanctoClick(Sender: TObject);
var
  objLanctoErro : TfrmLanctoContabil_LanctoErro;
begin
  if (qryContaLanctoContabil.State = dsInactive) or (qryMaster.State = dsInactive) then
      Abort;

  if (MessageDlg('Confirma processamento deste lan�amento ?', mtConfirmation, [mbYes,mbNo], 0) = mrNo) then
      Exit;

  if (qryContaLanctoContabil.State = dsInsert) or (qryContaLanctoContabil.State = dsEdit) then
      qryContaLanctoContabil.Post;

  if (qryContaLanctoContabil.RecordCount = 0) and (qryMaster.State <> dsInactive)then
  begin
      MessageDLG('Este registro n�o possui lan�amentos para serem processados.', mtWarning, [mbOK], 0);
      Abort;
  end;

  qryValidaSaldo.Close;
  qryValidaSaldo.Parameters.ParamByName('nPK').Value := qryMasternCdLanctoContabil.Value;
  qryValidaSaldo.Open;

  if (qryValidaSaldonValDiferenca.Value <> 0) then
  begin
      MessageDLG('Diferen�a no saldo dos lan�amentos.', mtWarning, [mbOK], 0);
      Abort;
  end;

  qryExecProc.Close;
  qryExecProc.Parameters.ParamByName('nCdLanctoContabil').Value := qryMasternCdLanctoContabil.Value;
  qryExecProc.Open;

  if (qryExecProc.RecordCount > 0) then
  begin
      MessageDLG('Erro no processamento.' + #13#13 + 'Existe(m) lan�amento(s) inconsistente(s).', mtError, [mbOK], 0);

      objLanctoErro := TfrmLanctoContabil_LanctoErro.Create(nil);
      objLanctoErro.DBGridEh1.DataSource := dsExecProc;

      showForm(objLanctoErro, True);
  end
  else
  begin
      DesabilitaCampos;
      btProcessarLancto.Enabled := False;
      PosicionaPK(qryMasternCdLanctoContabil.Value);

      ShowMessage('Lan�amentos processados com sucesso.');
  end;
end;

procedure TfrmLanctoContabil.qryContaLanctoContabilBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryContaLanctoContabil.State = dsInsert) then
  begin
      qryContaLanctoContabilnCdContaLanctoContabil.Value := frmMenu.fnProximoCodigo('LANCTOCONTABIL');
      qryContaLanctoContabilnCdLanctoContabil.Value      := qryMasternCdLanctoContabil.Value;
  end;

  { -- verifica e atualiza saldo do lan�amento -- }
  if (qryContaLanctoContabilnValCredito.Value = 0) and (qryContaLanctoContabilnValDebito.Value = 0) then
  begin
      MessageDLG('Informe o valor de cr�dito ou d�bito.', mtWarning, [mbOK], 0);
      Abort;
  end;

  if (qryContaLanctoContabilnValDebito.Value > 0) then
      qryContaLanctoContabilnValLancto.Value := qryContaLanctoContabilnValDebito.Value * -1;

  if (qryContaLanctoContabilnValCredito.Value > 0) then
      qryContaLanctoContabilnValLancto.Value := qryContaLanctoContabilnValCredito.Value;

  { -- verifica flags obrigat�rias -- }
  if (qryPlanoContacFlgExigeCC.Value = 1) and (qryContaLanctoContabilnCdCC.IsNull) then
  begin
      MessageDLG('Exige centro de custo.', mtWarning, [mbOK], 0);
      Abort;
  end;

  if(qryPlanoContacFlgExigeCC.Value = 0) and (not qryContaLanctoContabilnCdCC.IsNull) then
  begin
      MessageDLG('N�o permite centro de custo.', mtWarning, [mbOK], 0);
      Abort;
  end;

  if (qryPlanoContacFlgExigeUnidadeNegocio.Value = 1) and (qryContaLanctoContabilnCdUnidadeNegocio.IsNull) then
  begin
      MessageDLG('Exige unidade do neg�cio.', mtWarning, [mbOK], 0);
      Abort;
  end;

  if (qryPlanoContacFlgExigeUnidadeNegocio.Value = 0) and (not qryContaLanctoContabilnCdUnidadeNegocio.IsNull) then
  begin
      MessageDLG('N�o permite unidade do neg�cio.', mtWarning, [mbOK], 0);
      Abort;
  end;

  qryContaLanctoContabildDtLancto.Value  := dDtLancto.DateTime;
  qryContaLanctoContabilcHistorico.Value := qryMastercNmHistoricoDigitado.Value;
end;

procedure TfrmLanctoContabil.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercFlgProcessado.Value = 1) then
      btProcessarLancto.Enabled := False
  else
      btProcessarLancto.Enabled := True;

  if (qryMaster.State = dsBrowse) then
  begin
      PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString);
      PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString);
      PosicionaQuery(qryHistPadrao, qryMasternCdHistoricoPadrao.AsString);

      qryContaLanctoContabil.Close;
      qryContaLanctoContabil.Parameters.ParamByName('nPK').Value := qryMasternCdLanctoContabil.Value;
      qryContaLanctoContabil.Open;

      qryTabelaSistema.Close;
      qryTabelaSistema.Parameters.ParamByName('nPK').Value := qryMasternCdTabelaSistema.Value;
      qryTabelaSistema.Open;

      dDtLancto.Date := qryMasterdDtLancto.Value;

      case (qryMastercFlgProcessado.Value) of
          0 : HabilitaCampos;
          1 : DesabilitaCampos;
      end;
  end

  else
  begin
      qryEmpresa.Close;
      qryUsuario.Close;
      qryTabelaSistema.Close;
      qryHistPadrao.Close;
      qryContaLanctoContabil.Close;
      edtStatus.Text := '';
  end; 
end;

procedure TfrmLanctoContabil.qryContaLanctoContabilCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryPlanoConta.Close;
  qryPlanoConta.Parameters.ParamByName('nPK').Value := qryContaLanctoContabilnCdPlanoConta.Value;
  qryPlanoConta.Open;
  qryContaLanctoContabilcNmPlanoConta.Value         := qryPlanoContacNmPlanoConta.Value;

  qryCentroCusto.Close;
  qryCentroCusto.Parameters.ParamByName('nPK').Value := qryContaLanctoContabilnCdCC.Value;
  qryCentroCusto.Open;
  qryContaLanctoContabilcNmCC.Value                  := qryCentroCustocNmCC.Value;

  qryUnidadeNegocio.Close;
  qryUnidadeNegocio.Parameters.ParamByName('nPK').Value := qryContaLanctoContabilnCdUnidadeNegocio.Value;
  qryUnidadeNegocio.Open;
  qryContaLanctoContabilcNmUnidadeNegocio.Value         := qryUnidadeNegociocNmUnidadeNegocio.Value;

  qryProjeto.Close;
  qryProjeto.Parameters.ParamByName('nPK').Value := qryContaLanctoContabilnCdProjeto.Value;
  qryProjeto.Open;
  qryContaLanctoContabilcNmProjeto.Value         := qryProjetocNmProjeto.Value;
end;

procedure TfrmLanctoContabil.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer;
begin
  inherited;

  case Key of
      VK_F4: begin
                 if (qryContaLanctoContabil.State = dsInsert) or (qryContaLanctoContabil.State = dsEdit) then
                 begin
                     if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdPlanoConta') then
                     begin
                         nPK := frmLookup_Padrao.ExecutaConsulta(218);

                         if (nPk > 0) then
                             qryContaLanctoContabilnCdPlanoConta.Value := nPK;
                     end;

                     if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdCC') then
                     begin
                         nPK := frmLookup_Padrao.ExecutaConsulta(223);

                         if (nPk > 0) then
                             qryContaLanctoContabilnCdCC.Value := nPK;
                     end;

                     if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdUnidadeNegocio') then
                     begin
                         nPK := frmLookup_Padrao.ExecutaConsulta(35);

                         if (nPk > 0) then
                             qryContaLanctoContabilnCdUnidadeNegocio.Value := nPK;
                     end;

                     if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProjeto') then
                     begin
                         nPK := frmLookup_Padrao.ExecutaConsulta(763);

                         if (nPk > 0) then
                             qryContaLanctoContabilnCdProjeto.Value := nPK;
                     end;
                 end;
             end;
  end;
end;

procedure TfrmLanctoContabil.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInactive) and (qryMaster.State = dsInsert) then
  begin
      qryMaster.Post;
      PosicionaQuery(qryContaLanctoContabil, qryMasternCdLanctoContabil.AsString);
      qryContaLanctoContabil.Active := True;
  end;
end;

procedure TfrmLanctoContabil.btSalvarClick(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryContaLanctoContabil, qryMasternCdLanctoContabil.AsString);
  qryContaLanctoContabil.Active := True;
end;

procedure TfrmLanctoContabil.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryMastercFlgProcessado.Value = 1) then
  begin
      DBGridEh1.Font.Color := clWindowText;
      Exit;
  end;

  { -- formata c�lulas da coluna cr�dito -- }
  if (Column.Field.FieldName = 'nValCredito') then
  begin
      if (qryContaLanctoContabilnValCredito.Value = 0) then
      begin
          DBGridEh1.Canvas.Font.Color := clWindow;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      end;
  end;

  { -- formata c�lulas da coluna d�bito -- }
  if (Column.Field.FieldName = 'nValDebito') then
  begin
      if (qryContaLanctoContabilnValDebito.Value = 0) then
      begin
          DBGridEh1.Canvas.Font.Color := clWindow;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
      end;
  end;
end;

procedure TfrmLanctoContabil.qryContaLanctoContabilnValCreditoChange(
  Sender: TField);
begin
  inherited;
  if (qryContaLanctoContabilnValCredito.Value > 0) then
      qryContaLanctoContabilnValDebito.Value := 0;
end;

procedure TfrmLanctoContabil.qryContaLanctoContabilnValDebitoChange(
  Sender: TField);
begin
  inherited;

  if (qryContaLanctoContabilnValDebito.Value > 0) then
      qryContaLanctoContabilnValCredito.Value := 0;
end;

procedure TfrmLanctoContabil.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  if (qryMastercFlgProcessado.Value = 1) then
      Abort;

  case Key of
      VK_F4: begin
                 if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
                 begin
                     nPK := frmLookup_Padrao.ExecutaConsulta(224);

                     if (nPk > 0) then
                         qryMasternCdHistoricoPadrao.Value := nPK;
                 end;
             end;
  end;
end;

procedure TfrmLanctoContabil.qryMasterAfterDelete(DataSet: TDataSet);
begin
  inherited;

  HabilitaCampos;
  edtStatus.Text := '';
end;

initialization
  RegisterClass(TfrmLanctoContabil);

end.
