unit fAlcadasSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmAlcadasSP = class(TfrmProcesso_Padrao)
    qryAlcadasSP: TADOQuery;
    qryAlcadasSPnCdEmpresa: TIntegerField;
    qryAlcadasSPnCdLoja: TIntegerField;
    qryAlcadasSPnCdUsuario: TIntegerField;
    qryAlcadasSPnCdTipoSP: TIntegerField;
    qryAlcadasSPnValAlcada: TBCDField;
    dsAlcadaSP: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryTipoSP: TADOQuery;
    qryTipoSPnCdTipoSP: TIntegerField;
    qryTipoSPcNmTipoSP: TStringField;
    qryAlcadasSPcNmTipoSP: TStringField;
    qryAlcadasSPcNmUsuario: TStringField;
    qryAlcadasSPcNmEmpresa: TStringField;
    qryAlcadasSPcNmLoja: TStringField;
    qryAlcadasSPnCdAlcadaAutorizSP: TIntegerField;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure qryAlcadasSPBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryAlcadasSPCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlcadasSP: TfrmAlcadasSP;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmAlcadasSP.FormShow(Sender: TObject);
begin
  inherited;
  qryAlcadasSP.Close;
  qryAlcadasSP.Open;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      qryAlcadasSPnCdLoja.ReadOnly := True;

  DBGridEh1.Align      := alClient;

end;

procedure TfrmAlcadasSP.qryAlcadasSPBeforePost(DataSet: TDataSet);
begin

  if (qryAlcadasSPcNmEmpresa.Value = '') then
  begin
      MensagemAlerta('Empresa inv�lida.') ;
      abort ;
  end;

  if (qryAlcadasSPcNmLoja.Value = '') and (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      MensagemAlerta('Loja inv�lida.') ;
      abort ;
  end ;

  if (qryAlcadasSPcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio inv�lido.') ;
      abort ;
  end;

  if (qryAlcadasSPcNmTipoSP.Value = '') then
  begin
      MensagemAlerta('Tipo de SP inv�lida.') ;
      abort ;
  end;

  if (qryAlcadasSP.State = dsInsert) then
      qryAlcadasSPnCdAlcadaAutorizSP.Value := frmMenu.fnProximoCodigo('ALCADAAUTORIZSP') ;

  inherited;

end;

procedure TfrmAlcadasSP.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        qryLoja.Close;
        qryUsuario.Close;
        qryTipoSP.Close;

        if (qryAlcadasSP.State = dsBrowse) then
             qryAlcadasSP.Edit ;

        if (qryAlcadasSP.State = dsInsert) or (qryAlcadasSP.State = dsEdit) then
        begin

            if (DbGridEh1.Col = 1) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(8);

                If (nPK > 0) then
                    qryAlcadasSPnCdEmpresa.Value := nPK ;
            end ;

            if (DbGridEh1.Col = 3) then
            begin
                if (frmMenu.LeParametro('VAREJO') = 'S') then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta(59);

                    If (nPK > 0) then qryAlcadasSPnCdLoja.Value := nPK ;
                end;
            end ;

            if (DbGridEh1.Col = 5) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(5);
                If (nPK > 0) then qryAlcadasSPnCdUsuario.Value := nPK ;
            end ;

            if (DbGridEh1.Col = 7) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(11);
                If (nPK > 0) then qryAlcadasSPnCdTipoSP.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAlcadasSP.qryAlcadasSPCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryEmpresa, qryAlcadasSPnCdEmpresa.AsString) ;
  if not qryEmpresa.eof then
      qryAlcadasSPcNmEmpresa.Value := qryEmpresacNmEmpresa.Value;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryAlcadasSPcNmLoja.Value := '';
      qryLoja.Close;
      PosicionaQuery(qryLoja, qryAlcadasSPnCdLoja.asString) ;
      if not qryLoja.eof then
          qryAlcadasSPcNmLoja.Value := qryLojacNmLoja.Value ;
  end;

  if qryAlcadasSPnCdUsuario.Value > 0 then
  begin
      PosicionaQuery(qryUsuario, qryAlcadasSPnCdUsuario.AsString) ;
      if not qryUsuario.eof then
          qryAlcadasSPcNmUsuario.Value := qryUsuariocNmUsuario.Value;
  end;

  if qryAlcadasSPnCdTipoSP.Value > 0 then
  begin
      PosicionaQuery(qryTipoSP, qryAlcadasSPnCdTipoSP.AsString) ;
      if not qryTipoSP.eof then
          qryAlcadasSPcNmTipoSP.Value := qryTipoSPcNmTipoSP.Value;
  end;

end;

initialization
    RegisterClass(TfrmAlcadasSP) ;

end.
