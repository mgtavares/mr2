unit fGeracaoBorderoRecebimento_titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxCheckBox, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB, cxLookAndFeelPainters, StdCtrls,
  cxButtons;

type
  TfrmGeracaoBorderoRecebimento_titulos = class(TfrmProcesso_Padrao)
    cmdPreparaTemp: TADOCommand;
    dsTemp: TDataSource;
    qryTemp: TADOQuery;
    qryTempnCdTitulo: TIntegerField;
    qryTempcFlgOK: TIntegerField;
    qryTempcNrTit: TStringField;
    qryTempcNrNF: TStringField;
    qryTempiParcela: TIntegerField;
    qryTempdDtEmissao: TDateTimeField;
    qryTempdDtVenc: TDateTimeField;
    qryTempnSaldoTit: TBCDField;
    qryTempnCdTerceiro: TIntegerField;
    qryTempcNmTerceiro: TStringField;
    qryTempnCdFormaPagto: TIntegerField;
    qryTempcNmFormaPagto: TStringField;
    qryTempnCdEspTit: TIntegerField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1cFlgOK: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    cxButton1: TcxButton;
    SP_GERA_BORDERO_RECEBIMENTO: TADOStoredProc;
    qryPopulaTemp: TADOQuery;
    qryVerificaSelecao: TADOQuery;
    qryValidaTitulo: TADOQuery;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdContaBancaria : integer;
  end;

var
  frmGeracaoBorderoRecebimento_titulos: TfrmGeracaoBorderoRecebimento_titulos;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmGeracaoBorderoRecebimento_titulos.cxButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryTemp.State <> dsBrowse) then
      qryTemp.Post;

  qryVerificaSelecao.Close;
  qryVerificaSelecao.Open;

  if (qryVerificaSelecao.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum titulo foi selecionado, impossivel Gerar o Border�.');
      abort;
  end;

  if (MessageDLG('Confirma a Gera��o do Boreder� para o(s) titulo(s) selecionado(s) ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  qryValidaTitulo.Close;
  qryValidaTitulo.Open;

  if (qryValidaTitulo.RecordCount > 0) then
  begin
      MensagemAlerta('Um ou mais titulos j� pertencem a um Border�, impossivel continuar.');
      abort;
  end;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_BORDERO_RECEBIMENTO.Close;
      SP_GERA_BORDERO_RECEBIMENTO.Parameters.ParamByName('@nCdEmpresa').Value           := frmMenu.nCdEmpresaAtiva;
      SP_GERA_BORDERO_RECEBIMENTO.Parameters.ParamByName('@nCdContaBancaria').Value     := nCdContaBancaria;
      SP_GERA_BORDERO_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value           := frmMenu.nCdUsuarioLogado;
      SP_GERA_BORDERO_RECEBIMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Border� gerado com sucesso.');

  Close;
end;

end.
