inherited frmGrupoOperadoraCartao: TfrmGrupoOperadoraCartao
  Left = 38
  Top = 119
  Caption = 'Grupo Operadora de Cart'#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 60
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 60
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [6]
    Left = 64
    Top = 96
    Width = 241
    Height = 17
    Caption = 'Mostrar valor Liquido no fechamento POS'
    DataField = 'cFlgTotalLiqFechPOS'
    DataSource = dsMaster
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [7]
    Left = 320
    Top = 96
    Width = 329
    Height = 17
    Caption = 'Mostrar data de Cr'#233'dito das parcelas no fechamento POS'
    DataField = 'cFlgDataCredFechPOS'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoOperadoraCartao'
      'WHERE nCdGrupoOperadoraCartao =:nPK')
    Left = 504
    Top = 200
    object qryMasternCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryMastercNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
    object qryMastercFlgDataCredFechPOS: TIntegerField
      FieldName = 'cFlgDataCredFechPOS'
    end
    object qryMastercFlgTotalLiqFechPOS: TIntegerField
      FieldName = 'cFlgTotalLiqFechPOS'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
end
