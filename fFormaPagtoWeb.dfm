inherited frmFormaPagtoWeb: TfrmFormaPagtoWeb
  Left = 287
  Top = 134
  Height = 254
  Caption = 'Forma de Pagamento Web'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 191
  end
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 35
    Top = 94
    Width = 22
    Height = 13
    Caption = 'Tipo'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit3: TDBEdit [5]
    Left = 64
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoFormaPagto'
    DataSource = dsMaster
    TabOrder = 1
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [6]
    Tag = 1
    Left = 132
    Top = 88
    Width = 581
    Height = 19
    DataField = 'cNmTabTipoFormaPagto'
    DataSource = dsTabTipoFormaPagto
    TabOrder = 2
  end
  object DBEdit2: TDBEdit [7]
    Left = 64
    Top = 64
    Width = 649
    Height = 19
    DataField = 'cNmFormaPagtoWeb'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdFormaPagtoWeb'
    DataSource = dsMaster
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM FormaPagtoWeb'
      ' WHERE nCdFormaPagtoWeb = :nPK'
      '')
    Left = 520
    object qryMasternCdFormaPagtoWeb: TIntegerField
      FieldName = 'nCdFormaPagtoWeb'
    end
    object qryMastercNmFormaPagtoWeb: TStringField
      FieldName = 'cNmFormaPagtoWeb'
      Size = 100
    end
    object qryMasternCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 520
  end
  object qryTabTipoFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoFormaPagto'
      'WHERE nCdTabTipoFormaPagto = :nPK')
    Left = 552
    Top = 128
    object qryTabTipoFormaPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
    object qryTabTipoFormaPagtocNmTabTipoFormaPagto: TStringField
      FieldName = 'cNmTabTipoFormaPagto'
      Size = 50
    end
    object qryTabTipoFormaPagtocFlgExigeCliente: TIntegerField
      FieldName = 'cFlgExigeCliente'
    end
  end
  object dsTabTipoFormaPagto: TDataSource
    DataSet = qryTabTipoFormaPagto
    Left = 552
    Top = 160
  end
end
