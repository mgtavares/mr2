inherited frmOrdemCompraVarejo_GradeAberta: TfrmOrdemCompraVarejo_GradeAberta
  Left = 315
  Top = 229
  Width = 706
  Height = 426
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Ordem Compra Varejo - Grade Aberta'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 690
    Height = 359
  end
  inherited ToolBar1: TToolBar
    Width = 690
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 690
    Height = 359
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 355
    ClientRectLeft = 4
    ClientRectRight = 686
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Quantidade do Produto por Loja'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 682
        Height = 331
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsLoja
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Top = 96
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #Temp_GradeAberta_Loja'
      ' ORDER BY cNmLoja')
    Left = 512
    Top = 152
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 488
    Top = 240
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @SQL           varchar(4000)'
      '       ,@iTamanho      int'
      '       ,@cNmTamanho    varchar(50)'
      '       ,@nCdProduto    int'
      '       ,@nCdItemPedido int'
      '       '
      'IF (OBJECT_ID('#39'tempdb..#Temp_GradeAberta_Loja'#39') IS NOT NULL)'
      'BEGIN'
      '    DROP TABLE #Temp_GradeAberta_Loja'
      'END'
      ''
      'CREATE TABLE #Temp_GradeAberta_Loja (nCdProduto int not null'
      '                                    ,nCdLoja int not null'
      
        '                                    ,cNmLoja varchar(50) not nul' +
        'l'
      
        '                                    ,PRIMARY KEY (nCdProduto, nC' +
        'dLoja))'
      '                                    '
      'SET @SQL = NULL'
      'SET @nCdItemPedido = :nCdItemPedido'
      ''
      'SET @nCdProduto = :nCdProduto'
      ''
      'DECLARE curGrade CURSOR'
      '    FOR SELECT ItemGrade.iTamanho,cNmTamanho'
      '          FROM ItemGrade'
      
        '               INNER JOIN Grade ON Grade.nCdGrade = ItemGrade.nC' +
        'dGrade'
      
        '               INNER JOIN Produto ON Produto.nCdGrade = Grade.nC' +
        'dGrade'
      '         WHERE nCdProduto = @nCdProduto'
      ''
      'OPEN curGrade'
      ''
      'FETCH NEXT'
      ' FROM curGrade'
      ' INTO @iTamanho'
      '     ,@cNmTamanho'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      
        '    SET @SQL = '#39'ALTER TABLE #Temp_GradeAberta_Loja ADD grade_'#39' +' +
        ' @cNmTamanho + '#39' int default 0 not null'#39
      ''
      '    EXEC (@SQL)'
      ''
      '    FETCH NEXT'
      '     FROM curGrade'
      '     INTO @iTamanho'
      '         ,@cNmTamanho'
      'END'
      ''
      'CLOSE curGrade'
      'DEALLOCATE curGrade'
      ''
      ''
      'TRUNCATE TABLE #Temp_GradeAberta_Loja'
      ''
      'INSERT INTO #Temp_GradeAberta_Loja (nCdProduto'
      '                                   ,nCdLoja'
      '                                   ,cNmLoja)'
      '                            SELECT @nCdProduto'
      '                                  ,nCdLoja'
      '                                  ,cNmLoja'
      '                              FROM Loja'
      '                              WHERE cFlgVenda = 1'
      ''
      'SELECT *'
      '  FROM #Temp_GradeAberta_Loja'
      ' ORDER BY cNmLoja')
    Left = 224
    Top = 192
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'DECLARE @SQL varchar(4000)'#13#10'       ,@iTamanho int'#13#10'       ,@cNmT' +
      'amanho varchar(50)'#13#10'       ,@nCdProduto int'#13#10'       ,@nCdItemPed' +
      'ido int'#13#10'       '#13#10'CREATE TABLE #Temp_GradeAberta_Loja (nCdProdut' +
      'o int not null'#13#10'                                    ,nCdLoja int' +
      ' not null'#13#10'                                    ,cNmLoja varchar(' +
      '50) not null'#13#10'                                    ,PRIMARY KEY (' +
      'nCdProduto, nCdLoja))'#13#10'                                    '#13#10'SET' +
      ' @SQL = NULL'#13#10'SET @nCdItemPedido = :nCdItemPedido'#13#10#13#10'SET @nCdPro' +
      'duto = :nCdProduto'#13#10#13#10'DECLARE curGrade CURSOR'#13#10'    FOR SELECT It' +
      'emGrade.iTamanho,cNmTamanho'#13#10'          FROM ItemGrade'#13#10'         ' +
      '      INNER JOIN Grade ON Grade.nCdGrade = ItemGrade.nCdGrade'#13#10' ' +
      '              INNER JOIN Produto ON Produto.nCdGrade = Grade.nCd' +
      'Grade'#13#10'         WHERE nCdProduto = @nCdProduto'#13#10#13#10'OPEN curGrade'#13 +
      #10#13#10'FETCH NEXT'#13#10' FROM curGrade'#13#10' INTO @iTamanho'#13#10'     ,@cNmTamanh' +
      'o'#13#10#13#10'WHILE (@@FETCH_STATUS = 0)'#13#10'BEGIN'#13#10#13#10'    SET @SQL = '#39'ALTER ' +
      'TABLE #Temp_GradeAberta_Loja ADD grade_'#39' + @cNmTamanho + '#39' int d' +
      'efault 0 not null'#39#13#10#13#10'    EXEC (@SQL)'#13#10#13#10'    FETCH NEXT'#13#10'     FR' +
      'OM curGrade'#13#10'     INTO @iTamanho'#13#10'         ,@cNmTamanho'#13#10'END'#13#10#13#10 +
      'CLOSE curGrade'#13#10'DEALLOCATE curGrade'#13#10#13#10#13#10'TRUNCATE TABLE #Temp_Gr' +
      'adeAberta_Loja'#13#10#13#10'INSERT INTO #Temp_GradeAberta_Loja (nCdProduto' +
      #13#10'                                   ,nCdLoja'#13#10'                 ' +
      '                  ,cNmLoja)'#13#10'                            SELECT ' +
      '@nCdProduto '#13#10'                                  ,nCdLoja'#13#10'      ' +
      '                            ,cNmLoja'#13#10'                          ' +
      '    FROM Loja'#13#10'                              WHERE cFlgVenda = 1'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    Left = 316
    Top = 213
  end
end
