inherited frmConciliaBancoManual_TituloMovimentado_Cartao: TfrmConciliaBancoManual_TituloMovimentado_Cartao
  Left = 98
  Top = 192
  Width = 978
  Height = 501
  Caption = 'Comprovantes de Cart'#227'o Conciliados para Este Movimento'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 962
    Height = 436
  end
  inherited ToolBar1: TToolBar
    Width = 962
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 962
    Height = 436
    Align = alClient
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValTit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nSaldoTit
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1iNrDocto: TcxGridDBColumn
        Caption = 'Nr. Docto'
        DataBinding.FieldName = 'iNrDocto'
        Width = 74
      end
      object cxGrid1DBTableView1cNrLotePOS: TcxGridDBColumn
        Caption = 'Nr. Lote POS'
        DataBinding.FieldName = 'cNrLotePOS'
      end
      object cxGrid1DBTableView1iParcela: TcxGridDBColumn
        Caption = 'Parcela'
        DataBinding.FieldName = 'iParcela'
      end
      object cxGrid1DBTableView1iParcelas: TcxGridDBColumn
        Caption = 'Parcelas'
        DataBinding.FieldName = 'iParcelas'
      end
      object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
        Caption = 'Dt. Emissao'
        DataBinding.FieldName = 'dDtEmissao'
        Width = 85
      end
      object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
        Caption = 'Dt. Venc'
        DataBinding.FieldName = 'dDtVenc'
        Width = 71
      end
      object cxGrid1DBTableView1nValTit: TcxGridDBColumn
        Caption = 'Valor'
        DataBinding.FieldName = 'nValTit'
      end
      object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
        Caption = 'Saldo'
        DataBinding.FieldName = 'nSaldoTit'
      end
      object cxGrid1DBTableView1nCdLoteConciliacaoCartao: TcxGridDBColumn
        Caption = 'C'#243'd. Lote'
        DataBinding.FieldName = 'nCdLoteConciliacaoCartao'
      end
      object cxGrid1DBTableView1dDtConciliacaoCartao: TcxGridDBColumn
        Caption = 'Dt. Concilia'#231#227'o Cart'#227'o'
        DataBinding.FieldName = 'dDtConciliacaoCartao'
        Width = 137
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 114
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      ''
      'SET @nCdLanctoFin = :nPK'
      ''
      'SELECT TransacaoCartao.iNrDocto'
      '      ,TransacaoCartao.cNrLotePOS'
      '      ,Titulo.iParcela'
      '      ,TransacaoCartao.iParcelas'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.nCdLoteConciliacaoCartao'
      '      ,LoteConciliacaoCartao.dDtConciliacaoCartao'
      '      ,cNmUsuario'
      '  FROM Titulo '
      
        '       LEFT JOIN TransacaoCartao       ON TransacaoCartao.nCdTra' +
        'nsacaoCartao             = Titulo.nCdTransacaoCartao'
      
        '       LEFT JOIN LoteConciliacaoCartao ON LoteConciliacaoCartao.' +
        'nCdLoteConciliacaoCartao = Titulo.nCdLoteConciliacaoCartao'
      
        '       LEFT JOIN Usuario               ON Usuario.nCdUsuario    ' +
        '                         = LoteConciliacaoCartao.nCdUsuario'
      ' WHERE Titulo.nCdLanctoFin = @nCdLanctoFin'
      ' ORDER BY iNrDocto')
    Left = 184
    Top = 144
    object qryTitulosiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTituloscNrLotePOS: TStringField
      FieldName = 'cNrLotePOS'
      FixedChar = True
      Size = 15
    end
    object qryTitulosiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulosiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulosnValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosnCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTitulosdDtConciliacaoCartao: TDateTimeField
      FieldName = 'dDtConciliacaoCartao'
    end
    object qryTituloscNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTitulos
    Left = 200
    Top = 176
  end
end
