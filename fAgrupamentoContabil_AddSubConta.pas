unit fAgrupamentoContabil_AddSubConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, ER2Lookup, DB, DBCtrls, ADODB;

type
  TfrmAgrupamentoContabil_AddSubConta = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtMascaraSuperior: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtMascara: TEdit;
    Label3: TLabel;
    edtDescricao: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtDescricaoSegIdioma: TEdit;
    Label6: TLabel;
    edtNivel: TEdit;
    RadioGroup1: TRadioGroup;
    edtTipoNaturezaSPED: TER2LookupMaskEdit;
    Label7: TLabel;
    qryTipoNaturezaSPED: TADOQuery;
    qryTipoNaturezaSPEDnCdTabTipoNaturezaContaSPED: TIntegerField;
    qryTipoNaturezaSPEDcNmTabTipoNaturezaContaSPED: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryContaPai: TADOQuery;
    qryContaPainCdContaAgrupamentoContabil: TIntegerField;
    qryContaPainCdContaAgrupamentoContabilPai: TIntegerField;
    qryContaPainCdAgrupamentoContabil: TIntegerField;
    qryContaPaicMascara: TStringField;
    qryContaPaicNmContaAgrupamentoContabil: TStringField;
    qryContaPaicNmContaSegundoIdioma: TStringField;
    qryContaPaiiNivel: TIntegerField;
    qryContaPaiiSequencia: TIntegerField;
    qryContaPainCdTabTipoNaturezaContaSPED: TIntegerField;
    qryContaPainCdPlanoConta: TIntegerField;
    qryContaPaicFlgAnalSintetica: TStringField;
    qryContaPaicFlgNaturezaCredDev: TStringField;
    edtCodigo: TMaskEdit;
    qryAgrupamentoContabil: TADOQuery;
    qryAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField;
    qryAgrupamentoContabilnCdEmpresa: TIntegerField;
    qryAgrupamentoContabilcNmAgrupamentoContabil: TStringField;
    qryAgrupamentoContabilcMascara: TStringField;
    qryAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField;
    qryAgrupamentoContabilnCdStatus: TIntegerField;
    qryAgrupamentoContabilcFlgAnaliseVertical: TIntegerField;
    qryAgrupamentoContabiliMaxNiveis: TIntegerField;
    qryContaAgrupamentoContabil: TADOQuery;
    qryContaAgrupamentoContabilnCdContaAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai: TIntegerField;
    qryContaAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentoContabilcMascara: TStringField;
    qryContaAgrupamentoContabilcNmContaAgrupamentoContabil: TStringField;
    qryContaAgrupamentoContabilcNmContaSegundoIdioma: TStringField;
    qryContaAgrupamentoContabiliNivel: TIntegerField;
    qryContaAgrupamentoContabiliSequencia: TIntegerField;
    qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED: TIntegerField;
    qryContaAgrupamentoContabilnCdPlanoConta: TIntegerField;
    qryContaAgrupamentoContabilcFlgAnalSintetica: TStringField;
    qryContaAgrupamentoContabilcFlgNaturezaCredDev: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryAux: TADOQuery;
    procedure limpaCampos;
    procedure adicionaSubConta( nCdAgrupamentoContabil , nCdContaAgrupamentoContabilPai : integer ; bInclusao : boolean) ;
    procedure FormShow(Sender: TObject);
    procedure edtCodigoChange(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    function fnRemoveMascara( cString : string ) : string;
  private
    { Private declarations }
    bInsert : boolean ;
    nCdContaAgrupamentoContabilPai__ : integer ;
    nCdAgrupamentoContabil__ : integer ;
  public
    { Public declarations }
    cFormatoMascara : string ;
  end;

var
  frmAgrupamentoContabil_AddSubConta: TfrmAgrupamentoContabil_AddSubConta;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmAgrupamentoContabil_AddSubConta }

procedure TfrmAgrupamentoContabil_AddSubConta.adicionaSubConta( nCdAgrupamentoContabil , nCdContaAgrupamentoContabilPai : integer ; bInclusao : boolean) ;
var
  iNivelCorrente, i : integer ;
  cMascaraFormat    : string ;
begin

    bInsert := bInclusao ;

    if (bInsert) then
        ativaMaskEdit( edtCodigo )
    else desativaMaskEdit( edtCodigo ) ;
    
    nCdContaAgrupamentoContabilPai__ := nCdContaAgrupamentoContabilPai ;
    nCdAgrupamentoContabil__         := nCdAgrupamentoContabil ;

    qryAgrupamentoContabil.Close;
    PosicionaQuery( qryAgrupamentoContabil , intToStr( nCdAgrupamentoContabil ) ) ;

    {-- se for a inclus�o de uma subconta, localiza as informa��es da conta pai/superior e posiciona na tela --}
    if ( bInsert ) then
    begin

        qryContaPai.Close;
        PosicionaQuery( qryContaPai , intToStr( nCdContaAgrupamentoContabilPai ) ) ;

        edtNivel.Text := '1' ;

        if not qryContaPai.IsEmpty then
        begin

            if (qryContaPaicFlgAnalSintetica.Value = 'A') then
            begin
                MensagemAlerta('N�o � poss�vel adicionar uma subconta em uma conta anal�tica.') ;
                close;
                abort;
            end ;

            edtMascaraSuperior.Text  := frmMenu.fnAplicaMascaraContabil( qryContaPaicMascara.Value , cFormatoMascara ) ;
            edtNivel.Text            := intToStr( qryContaPaiiNivel.Value + 1 ) ;

            if ( qryContaPaicFlgNaturezaCredDev.Value = 'D') then
                RadioGroup1.ItemIndex := 0 ;

            if ( qryContaPaicFlgNaturezaCredDev.Value = 'C') then
                RadioGroup1.ItemIndex := 1 ;

            edtTipoNaturezaSPED.Text := qryContaPainCdTabTipoNaturezaContaSPED.AsString ;
            edtTipoNaturezaSPED.PosicionaQuery;

            if not qryAgrupamentoContabil.IsEmpty then
            begin

                if (strToInt( edtNivel.Text ) > qryAgrupamentoContabiliMaxNiveis.Value ) then
                begin

                    MensagemAlerta('Limite de n�veis suportados na m�scara do agrupamento cont�bil excedido.') ;
                    close;
                    abort;

                end ;

            end ;

        end ;

    end
    else
    begin

        qryContaAgrupamentoContabil.Close;
        PosicionaQuery( qryContaAgrupamentoContabil , intToStr( nCdContaAgrupamentoContabilPai ) ) ;

        if not qryContaAgrupamentoContabil.IsEmpty then
        begin

            qryContaPai.Close;
            PosicionaQuery( qryContaPai , qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai.AsString ) ;

            if not qryContaPai.IsEmpty then
                edtMascaraSuperior.Text  := frmMenu.fnAplicaMascaraContabil( qryContaPaicMascara.Value , cFormatoMascara ) ;

            edtCodigo.Text             := qryContaAgrupamentoContabiliSequencia.asString;
            edtDescricao.Text          := qryContaAgrupamentoContabilcNmContaAgrupamentoContabil.Value;
            edtDescricaoSegIdioma.Text := qryContaAgrupamentoContabilcNmContaSegundoIdioma.Value;
            edtNivel.Text              := qryContaAgrupamentoContabiliNivel.AsString;

            if ( qryContaAgrupamentoContabilcFlgNaturezaCredDev.Value = 'D') then
                RadioGroup1.ItemIndex := 0 ;

            if ( qryContaAgrupamentoContabilcFlgNaturezaCredDev.Value = 'C') then
                RadioGroup1.ItemIndex := 1 ;

            edtTipoNaturezaSPED.Text := qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED.AsString ;
            edtTipoNaturezaSPED.PosicionaQuery;
        end ;

        if (qryContaAgrupamentoContabilcFlgAnalSintetica.Value = 'A') then
        begin
            MensagemAlerta('N�o � poss�vel alterar uma conta anal�tica.') ;
            close;
            abort;
        end ;

    end ;

    {-- de acordo com o n�vel da subconta, gera a m�scara no campo c�digo --}
    cMascaraFormat := '' ;
    iNivelCorrente := 1 ;

    for i := 1 to length( qryAgrupamentoContabilcMascara.Value ) do
    begin

        if ( Copy( qryAgrupamentoContabilcMascara.Value , i , 1 ) = '.' ) then
        begin

            inc( iNivelCorrente ) ;

            if ( iNivelCorrente > strToInt( edtNivel.Text ) ) then
                break ;

        end
        else
        begin

            if ( iNivelCorrente = strToInt( edtNivel.Text ) ) then
                cMascaraFormat := cMascaraFormat + Copy( qryAgrupamentoContabilcMascara.Value , i , 1 ) ;

        end ;

    end ;

    edtCodigo.EditMask := trim( cMascaraFormat ) + ';0;_'  ;

    if not bInsert then
        edtCodigo.Text := frmMenu.ZeroEsquerda(edtCodigo.Text,(length(edtCodigo.EditMask)-4)) ;

    if not Self.Showing then
        Self.ShowModal
    else
    begin
        if not edtCodigo.ReadOnly then
            edtCodigo.SetFocus
        else edtDescricao.SetFocus ;
    end ;

end;

procedure TfrmAgrupamentoContabil_AddSubConta.limpaCampos;
begin

    edtMascaraSuperior.Text    := '' ;
    edtCodigo.Text             := '' ;
    edtMascara.Text            := '' ;
    edtDescricao.Text          := '' ;
    edtDescricaoSegIdioma.Text := '' ;
    edtNivel.Text              := '' ;
    edtTipoNaturezaSPED.Text   := '' ;
    RadioGroup1.ItemIndex      := -1 ;
    qryTipoNaturezaSPED.Close;

end;

procedure TfrmAgrupamentoContabil_AddSubConta.FormShow(Sender: TObject);
begin
  inherited;

  if not edtCodigo.ReadOnly then
      edtCodigo.SetFocus
  else edtDescricao.SetFocus ;

end;

procedure TfrmAgrupamentoContabil_AddSubConta.edtCodigoChange(
  Sender: TObject);
begin
  inherited;

  if (edtMascaraSuperior.Text <> '') then
      edtMascara.Text := edtMascaraSuperior.Text + '.' + edtCodigo.Text
  else edtMascara.Text := edtCodigo.Text ;
  
end;

procedure TfrmAgrupamentoContabil_AddSubConta.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if ( trim( edtCodigo.Text ) = '' ) then
  begin
      MensagemAlerta('Informe o c�digo da subconta.') ;
      edtCodigo.SetFocus;
      abort ;
  end ;

  if ( length( trim( edtCodigo.Text ) ) <> (length( edtCodigo.EditMask ) -4 ) ) then
  begin
      MensagemAlerta('C�digo da subconta incompleto. Preencha todos os d�gitos ou complete com zeros a esquerda.') ;
      edtCodigo.SetFocus;
      abort ;
  end ;

  if ( trim( edtDescricao.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o da subconta.') ;
      edtDescricao.SetFocus;
      abort ;
  end ;

  if ( trim( edtDescricaoSegIdioma.Text ) = '' ) then
      edtDescricaoSegIdioma.Text := edtDescricao.Text;

  if ( RadioGroup1.ItemIndex < 0) or ( RadioGroup1.ItemIndex > 1 ) then
  begin
      MensagemAlerta('Informe a natureza da subconta.') ;
      abort ;
  end ;

  if ( DBEdit1.Text = '' ) then
  begin
      MensagemAlerta('Selecione a Natureza do SPED da subconta.') ;
      edtTipoNaturezaSPED.SetFocus;
      abort ;
  end ;

  if (bInsert) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoContabil WHERE nCdAgrupamentoContabil = ' + intToStr( nCdAgrupamentoContabil__ ) + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end
  else
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoContabil WHERE nCdAgrupamentoContabil = ' + intToStr( nCdAgrupamentoContabil__ ) + ' AND nCdContaAgrupamentoContabil != ' + intToStr( nCdContaAgrupamentoContabilPai__ ) + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end ;

  if ( MessageDlg('Confirma a inclus�o desta subconta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
  begin
      edtCodigo.SetFocus;
      exit;
  end ;

  try

      if ( bInsert ) then
      begin
          qryContaAgrupamentoContabil.Close;
          PosicionaQuery( qryContaAgrupamentoContabil , '0' ) ;
          
          qryContaAgrupamentoContabil.Insert ;
          qryContaAgrupamentoContabilnCdContaAgrupamentoContabil.Value := frmMenu.fnProximoCodigo('CONTAAGRUPAMENTOCONTABIL') ;
          qryContaAgrupamentoContabilnCdAgrupamentoContabil.Value      := nCdAgrupamentoContabil__ ;

          if ( nCdContaAgrupamentoContabilPai__ > 0 ) then
              qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai.Value := nCdContaAgrupamentoContabilPai__ ;
      end
      else qryContaAgrupamentoContabil.Edit ;

      qryContaAgrupamentoContabilcMascara.Value                       := fnRemoveMascara( edtMascara.Text );
      qryContaAgrupamentoContabilcNmContaAgrupamentoContabil.Value    := edtDescricao.Text ;
      qryContaAgrupamentoContabilcNmContaSegundoIdioma.Value          := edtDescricaoSegIdioma.Text ;
      qryContaAgrupamentoContabiliNivel.Value                         := strToInt( edtNivel.Text ) ;
      qryContaAgrupamentoContabiliSequencia.Value                     := strToInt( edtCodigo.Text ) ;
      qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED.Value    := strToInt( trim( edtTipoNaturezaSPED.Text ) ) ;
      qryContaAgrupamentoContabilcFlgAnalSintetica.Value              := 'S' ;

      if (RadioGroup1.ItemIndex = 0) then
          qryContaAgrupamentoContabilcFlgNaturezaCredDev.Value := 'D'
      else qryContaAgrupamentoContabilcFlgNaturezaCredDev.Value := 'C' ;

      qryContaAgrupamentoContabil.Post;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  if ( not bInsert ) then
      Close
  else
  begin
  
      limpaCampos;

      adicionaSubConta( nCdAgrupamentoContabil__
                      , nCdContaAgrupamentoContabilPai__
                      , TRUE ) ;

  end ;

end;

function TfrmAgrupamentoContabil_AddSubConta.fnRemoveMascara(
  cString: string): string;
var
  x: Integer;
begin

  result := '';

  for x := 1 to length( cString ) do
      if ( Copy( cString , x , 1 ) <> '.' ) then
          result := result + Copy( cString , x , 1 ) ;

end;

end.
