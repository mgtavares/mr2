inherited rptChequeRecebido: TrptChequeRecebido
  Left = 68
  Top = 129
  Caption = 'Relat'#243'rio de Cheque Recebido'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 68
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 29
    Top = 96
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Portadora'
  end
  object Label3: TLabel [3]
    Left = 32
    Top = 168
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Emiss'#227'o'
  end
  object Label6: TLabel [4]
    Left = 191
    Top = 168
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 15
    Top = 192
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Vencimento'
  end
  object Label4: TLabel [6]
    Left = 191
    Top = 192
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label7: TLabel [7]
    Left = 6
    Top = 120
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Respons'#225'vel'
  end
  object Label8: TLabel [8]
    Left = 38
    Top = 72
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Portadora'
  end
  object Label9: TLabel [9]
    Left = 22
    Top = 144
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'CNPJ/CPF Emissor'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 11
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 13
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 256
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 12
  end
  object edtDtEmissaoIni: TMaskEdit [13]
    Left = 112
    Top = 160
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object edtDtEmissaoFim: TMaskEdit [14]
    Left = 211
    Top = 160
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object edtEmpresa: TMaskEdit [15]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtEmpresaExit
    OnKeyDown = edtEmpresaKeyDown
  end
  object edtContaBancaria: TMaskEdit [16]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtContaBancariaExit
    OnKeyDown = edtContaBancariaKeyDown
  end
  object edtDtVenctoIni: TMaskEdit [17]
    Left = 112
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object edtDtVenctoFim: TMaskEdit [18]
    Left = 211
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 88
    Width = 69
    Height = 21
    DataField = 'nCdBanco'
    DataSource = DataSource6
    TabOrder = 14
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 256
    Top = 88
    Width = 65
    Height = 21
    DataField = 'cAgencia'
    DataSource = DataSource6
    TabOrder = 15
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 328
    Top = 88
    Width = 200
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource6
    TabOrder = 16
  end
  object DBEdit6: TDBEdit [22]
    Tag = 1
    Left = 536
    Top = 88
    Width = 374
    Height = 21
    DataField = 'cNmTitular'
    DataSource = DataSource6
    TabOrder = 17
  end
  object RadioGroup1: TRadioGroup [23]
    Left = 112
    Top = 216
    Width = 185
    Height = 257
    Caption = 'Situacao'
    ItemIndex = 1
    Items.Strings = (
      'Todos'
      'Dispon'#237'vel para Dep'#243'sito'
      'Depositado'
      'Compensado'
      'Negociado'
      'Primeira Devolu'#231#227'o'
      'Segunda Devolu'#231#227'o'
      'Devolvido para Emissor'
      'Enviado para Loja'
      'Depositado em Cust'#243'dia'
      'Enviado para Protesto')
    TabOrder = 9
  end
  object RadioGroup2: TRadioGroup [24]
    Left = 304
    Top = 216
    Width = 185
    Height = 105
    Caption = 'Ordena'#231#227'o'
    ItemIndex = 2
    Items.Strings = (
      'N'#250'mero de Cheque'
      'Data de Emiss'#227'o'
      'Data de Vencimento')
    TabOrder = 10
  end
  object edtTerceiro: TMaskEdit [25]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '##########;1; '
    MaxLength = 10
    TabOrder = 3
    Text = '          '
    OnExit = edtTerceiroExit
    OnKeyDown = edtTerceiroKeyDown
  end
  object DBEdit7: TDBEdit [26]
    Tag = 1
    Left = 184
    Top = 112
    Width = 137
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource7
    TabOrder = 18
  end
  object DBEdit8: TDBEdit [27]
    Tag = 1
    Left = 328
    Top = 112
    Width = 582
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource7
    TabOrder = 19
  end
  object edtLoja: TMaskEdit [28]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object DBEdit9: TDBEdit [29]
    Tag = 1
    Left = 184
    Top = 64
    Width = 726
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource8
    TabOrder = 20
  end
  object edtCNPJEmissor: TMaskEdit [30]
    Left = 112
    Top = 136
    Width = 96
    Height = 21
    MaxLength = 14
    TabOrder = 4
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 136
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 336
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdContaBancaria'
      '      ,nCdEmpresa'
      '      ,nCdBanco'
      '      ,cAgencia'
      '      ,nCdConta'
      '      ,cNmTitular'
      '  FROM ContaBancaria'
      ' WHERE nCdEmpresa       = :nCdEmpresa'
      '   AND nCdContaBancaria = :nPK'
      '   AND ((nCdLoja = @nCdLoja) OR (@nCdLoja = 0))')
    Left = 336
    Top = 360
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryContaBancaria
    Left = 496
    Top = 296
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cCNPJCPF, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 448
    Top = 392
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryTerceiro
    Left = 504
    Top = 304
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 568
    Top = 160
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryLoja
    Left = 496
    Top = 272
  end
end
