unit fPlanoRefSPED_DuplicaPlano;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBCtrls, Mask, ER2Lookup;

type
  TfrmPlanoRefSPED_DuplicaPlano = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    mskAnoRef: TMaskEdit;
    mskAno: TMaskEdit;
    Label5: TLabel;
    SP_DUPLICA_PLANO_REFERENCIAL_SPED: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanoRefSPED_DuplicaPlano: TfrmPlanoRefSPED_DuplicaPlano;

implementation

uses
    fMenu;

{$R *.dfm}

procedure TfrmPlanoRefSPED_DuplicaPlano.ToolButton1Click(Sender: TObject);
begin
  if (Trim(mskAnoRef.Text) = '') or (Trim(mskAno.Text) = '') then
  begin
      MessageDLG('Exerc�cio referencial ou novo exerc�cio n�o pode ser nulo.', mtWarning, [mbYes], 0);
      mskAnoRef.SetFocus;
      Abort;
  end;

  if (mskAnoRef.Text = mskAno.Text) then
  begin
      MessageDLG('Novo exerc�cio deve ser diferente do ano referencial.', mtWarning, [mbYes], 0);
      mskAno.SetFocus;
      Abort;
  end;

  if (MessageDlg('Confirma duplica��o do Plano Referencial ?', mtConfirmation, [mbYes,mbNo], 0) = mrYes) then
  begin
      try
          frmMenu.Connection.BeginTrans;

          SP_DUPLICA_PLANO_REFERENCIAL_SPED.Close;
          SP_DUPLICA_PLANO_REFERENCIAL_SPED.Parameters.ParamByName('@iAnoRef').Value := mskAnoRef.Text;
          SP_DUPLICA_PLANO_REFERENCIAL_SPED.Parameters.ParamByName('@iAno').Value    := mskAno.Text;
          SP_DUPLICA_PLANO_REFERENCIAL_SPED.ExecProc;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Duplica��o do Plano Referencial efetuada com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MessageDLG('Erro no processamento', mtError, [mbOK], 0);
          Raise;
          Abort;
      end;
  end;

  Close;
end;

end.
