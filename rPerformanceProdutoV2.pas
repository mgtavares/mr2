unit rPerformanceProdutoV2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, ToolWin, DBCtrls, StdCtrls, Mask, ADODB, cxGridDBTableView, cxGrid, cxGraphics, cxLookAndFeels, cxCurrencyEdit, cxPC,
  cxLookAndFeelPainters, cxButtons, DB, FileCtrl;

type
  TrptPerformanceProdutoV2 = class(TForm)
    ToolBar1: TToolBar;
    ToolButtonImp: TToolButton;
    ToolButtonFechar: TToolButton;
    ImageList1: TImageList;
    Image1: TImage;
    ToolButton3: TToolButton;
    EditQDepto: TEdit;
    EditQMarca: TEdit;
    EditQLoja: TEdit;
    EditQCateg: TEdit;
    MaskEditDe: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    MaskEditAte: TMaskEdit;
    GroupBox1: TGroupBox;
    RadioButtonTodos: TRadioButton;
    RadioButtonInline: TRadioButton;
    ADOQuerySP: TADOQuery;
    ADOQueryDepto: TADOQuery;
    ADOQueryCateg: TADOQuery;
    ADOQueryMarca: TADOQuery;
    ADOQueryLoja: TADOQuery;
    ProgressBar: TProgressBar;
    MaskEditDepto: TMaskEdit;
    MaskEditCateg: TMaskEdit;
    MaskEditLoja: TMaskEdit;
    MaskEditMarca: TMaskEdit;
    CheckBoxAtivos: TCheckBox;
    SaveDialog1: TSaveDialog;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ToolButtonFecharClick(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure showForm(objForm: TForm; bDestroirObjeto : boolean);
    procedure desativaDBEdit (obj : TDBEdit) ;
    procedure ativaDBEdit (obj : TDBEdit) ;
    procedure desativaMaskEdit (obj : TMaskEdit) ;
    procedure ativaMaskEdit (obj : TMaskEdit) ;
    procedure ToolButtonImpClick(Sender: TObject);
    procedure MaskEditDeptoChange(Sender: TObject);
    procedure MaskEditCategChange(Sender: TObject);
    procedure MaskEditMarcaChange(Sender: TObject);
    procedure MaskEditLojaChange(Sender: TObject);
    procedure MaskEditDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEditCategKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEditMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEditLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPerformanceProdutoV2: TrptPerformanceProdutoV2;

implementation

uses fMenu, fCadastro_Template, fMensagem, fLookup_Padrao;

{$R *.dfm}

function TrptPerformanceProdutoV2.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;


procedure TrptPerformanceProdutoV2.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
    {frmMenu.nShowMessage(cTexto);}
end;


procedure TrptPerformanceProdutoV2.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
    {frmMenu.nMensagemErro(cTexto);}
end;

procedure TrptPerformanceProdutoV2.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
    {frmMenu.nMensagemAlerta(cTexto);}
end;

procedure TrptPerformanceProdutoV2.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TrptPerformanceProdutoV2.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
i:integer ;  
begin

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;
end;

procedure TrptPerformanceProdutoV2.FormCreate(Sender: TObject);
var
    i : Integer;
    i2 : integer ;
begin

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TcxPageControl) then
    begin

        (Components [I] as TcxPageControl).Font.Name  := 'Calibri' ;
        (Components [I] as TcxPageControl).Font.Size  := 9 ;
        (Components [I] as TcxPageControl).Font.Style := [] ;
        (Components [I] as TcxPageControl).LookAndFeel.NativeStyle := True ;

    end ;

    if (Components [I] is TGroupBox) then
    begin
       (Components [I] as TGroupBox).Font.Name    := 'Calibri' ;
       (Components [I] as TGroupBox).Font.Size    := 8 ;
       (Components [I] as TGroupBox).Font.Style   := [] ;
    end ;

    if (Components [I] is TCheckBox) then
    begin
       (Components [I] as TCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TCheckBox).Font.Size    := 8 ;
       (Components [I] as TCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDBCheckBox) then
    begin
       (Components [I] as TDBCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TDBCheckBox).Font.Size    := 8 ;
       (Components [I] as TDBCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TADOStoredProc) then
    begin
        (Components [I] as TADOStoredProc).CommandTimeOut := 600 ;
    end ;

    if (Components [I] is TDBEdit) then
    begin
      //(Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := true    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TDBEdit).Font.Size   := 9 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;
      (Components [I] as TDBEdit).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ReadOnly := True ;
          (Components [I] as TDBEdit).Color      := $00E9E4E4 ;
          (Components [I] as TDBEdit).Font.Color := clBlack ;
          (Components [I] as TDBEdit).TabStop  := False ;
          Update;
      end;

    end;

    if (Components [I] is TDBMemo) then
    begin
      (Components [I] as TDBMemo).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBMemo).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBMemo).Color       := clWhite ;
      (Components [I] as TDBMemo).BevelInner  := bvSpace ;
      (Components [I] as TDBMemo).BevelKind   := bkNone ;
      (Components [I] as TDBMemo).BevelOuter  := bvLowered ;
      (Components [I] as TDBMemo).Ctl3D       := True    ;
      (Components [I] as TDBMemo).BorderStyle := bsSingle ;
      (Components [I] as TDBMemo).Font.Name   := 'Calibri' ;
      (Components [I] as TDBMemo).Font.Size   := 9 ;
      (Components [I] as TDBMemo).Font.Style  := [] ;
      (Components [I] as TDBMemo).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if ((Components [I] as TDBMemo).Tag = 1) then
      begin
          (Components [I] as TDBMemo).ParentFont := False ;
          (Components [I] as TDBMemo).Enabled := False ;
          (Components [I] as TDBMemo).Color   := clSilver ;
          (Components [I] as TDBMemo).Font.Color := clYellow ;
          Update;
      end ;

      if ((Components [I] as TDBEdit).Tag = 0) then
      begin
          if (Copy((Components [I] as TDBEdit).DataField,1,3) = 'dDt') then
             (Components [I] as TDBEdit).Width := 76 ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      //(Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TEdit).Font.Size   := 9 ;
      (Components [I] as TEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TEdit).OnKeyPress  := frmMenu.NavegaEnter ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      //(Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TMaskEdit).Font.Size   := 9 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TMaskEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if Trim((Components[I] as TMaskEdit).EditMask) = '######;1;' then
          (Components[I] as TMaskEdit).EditMask := '##########;1; ' ;
          
      if (Components[I] as TMaskEdit).EditMask = '!99/99/9999;1;_' then
          (Components[I] as TMaskEdit).Width := 76 ;

      if ((Components [I] as TMaskEdit).Tag = 1) then
      begin
          (Components [I] as TMaskEdit).ReadOnly := True ;
          (Components [I] as TMaskEdit).Color      := $00E9E4E4 ;
          (Components [I] as TMaskEdit).Font.Color := clBlack ;
          (Components [I] as TMaskEdit).TabStop  := False ;
          Update;
      end;


    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clWhite ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Calibri' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    If (Components [I] is TDBCheckBox) then
      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
        (Components [I] as TDBCheckBox).OnKeyPress  := frmMenu.NavegaEnter ;

    If (Components [I] is TcxGridDBTableView) then
    begin
        (Components [I] as TcxGridDBTableView).OptionsView.GridLineColor := clAqua ;
        (Components [I] as TcxGridDBTableView).OptionsView.GridLines     := glVertical ;
        (Components [I] as TcxGridDBTableView).Styles.Header             := frmMenu.Header ;
    end ;

    If (Components [I] is TcxGrid) then
    begin
        (Components [I] as TcxGrid).LookAndFeel.Kind        := lfFlat ;
        (Components [I] as TcxGrid).LookAndFeel.NativeStyle := True ;
        (Components [I] as TcxGrid).Font.Name               := 'Calibri' ;
    end ;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;

      (Components [I] as TDBLookupComboBox).Height      := 12 ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;
    end;

    if (Components [I] is TcxCurrencyEdit) then
    begin
        (Components [I] as TcxCurrencyEdit).Style.Font.Name := 'Calibri' ;
        (Components [I] as TcxCurrencyEdit).Style.Font.Size := 9 ;
    end ;
        
  end ;

end;

procedure TrptPerformanceProdutoV2.ToolButtonFecharClick(Sender: TObject);
begin
    Close ;
end;



procedure TrptPerformanceProdutoV2.FormActivate(Sender: TObject);
begin
    ToolBar1.Enabled := True ;
    frmMenu.StatusBar1.Panels[5].Text := Self.ClassName ;
end;

procedure TrptPerformanceProdutoV2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    frmMenu.StatusBar1.Panels[5].Text := '' ;
end;

procedure TrptPerformanceProdutoV2.FormDeactivate(Sender: TObject);
begin
    ToolBar1.Enabled := False ;
end;

procedure TrptPerformanceProdutoV2.showForm(objForm: TForm;
  bDestroirObjeto: boolean);
begin

    frmMenu.showForm(objForm , bDestroirObjeto);

end;

procedure TrptPerformanceProdutoV2.ativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptPerformanceProdutoV2.desativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptPerformanceProdutoV2.ativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptPerformanceProdutoV2.desativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptPerformanceProdutoV2.ToolButtonImpClick(Sender: TObject);
var
  depto, categ, marca, loja, inlin, ativos : Integer;
  dtIni, dtFin, cLinha, path, arq, query : String;
  F: TextFile;
begin
  if SaveDialog1.Execute then
  begin
    ProgressBar.Max := 100;
    ProgressBar.Position := 20;
  try
    ADOQuerySP.Close;
    ADOQuerySP.SQL.Clear;
    if MaskEditDepto.Text = '' then depto := 0 else depto := StrToInt(MaskEditDepto.Text);
    if MaskEditCateg.Text = '' then categ := 0 else categ := StrToInt(MaskEditCateg.Text);
    if MaskEditMarca.Text = '' then marca :=0 else marca := StrToInt(MaskEditMarca.Text);
    if MaskEditLoja.Text = '' then loja := 0 else loja := StrToInt(MaskEditLoja.Text);
    if MaskEditDe.Text = '  /  /    ' then dtIni := DateToStr(Date-30) else dtIni := MaskEditDe.Text;
    if MaskEditAte.Text = '  /  /    ' then dtFin := DateToStr(Date-1) else dtFin := MaskEditAte.Text;
    if MaskEditDe.Text = '  /  /    ' then MaskEditDe.Text := DateToStr(Date-31);
    if MaskEditAte.Text = '  /  /    ' then MaskEditAte.Text := DateToStr(Date-1);
    if RadioButtonInline.Checked = True then inlin := 1 else inlin := 0;
    if CheckBoxAtivos.Checked = True then ativos := 1 else ativos := 0;
    query := 'EXEC SPREL_PRODUCT_PERFORMANCE  @depto='  + IntToStr(depto) +
                                            ',@categ='  + IntToStr(categ) +
                                            ',@marca='  + IntToStr(marca) +
                                            ',@loja='   + IntToStr(loja)  +
                                            ',@inline=' + IntToStr(inlin) +
                                            ',@dtIni='  + chr(39) + dtIni + chr(39) +
                                            ',@dtFin='  + chr(39) + dtFin + chr(39) +
                                            ',@status=' + IntToStr(ativos)+ ';';
    ADOQuerySP.SQL.Add(query);
    ADOQuerySP.Open;
    ProgressBar.Max := ADOQuerySP.RecordCount;
    ProgressBar.Position := Round(ADOQuerySP.RecordCount * 0.2);
    AssignFile(F, SaveDialog1.FileName);
    Rewrite(F);
    Writeln(F, 'C�d. Produto;Departamento;Categoria;Marca;Modelo;Descri��o;Qtd. Vendas;Qtd. Estoque;Qtd. Carteira;Vl. Custo;Vl. Venda;Vl. Promocional;Dias em Estoque;�lt. Recebimento;');
    ADOQuerySP.First;
    while not ADOQuerySP.Eof do
      begin
        cLinha :=
          ADOQuerySP.FieldByName('ProductCode').Text + ';' +
          ADOQuerySP.FieldByName('Department').Text + ';' +
          ADOQuerySP.FieldByName('Category').Text + ';' +
          ADOQuerySP.FieldByName('Brand').Text + ';' +
          ADOQuerySP.FieldByName('Model').Text + ';' +
          ADOQuerySP.FieldByName('Description').Text + ';' +
          ADOQuerySP.FieldByName('TotalSaleAmout').Text + ';' +
          ADOQuerySP.FieldByName('AmoutStockTotal').Text + ';' +
          ADOQuerySP.FieldByName('AmoutOrderTotal').Text + ';' +
          ADOQuerySP.FieldByName('UnitCost').Text + ';' +
          ADOQuerySP.FieldByName('UnitSalesValue').Text + ';' +
          ADOQuerySP.FieldByName('Offer').Text + ';' +
          ADOQuerySP.FieldByName('StockDays').Text + ';' +
          ADOQuerySP.FieldByName('LastEntry').Text + ';';
        Writeln(F, cLinha);
        ADOQuerySP.Next;
        ProgressBar.Position :=  ProgressBar.Position + 1;
      end;
      CloseFile(F);
      ShowMessage('O relat�rio foi criado com sucesso no diret�rio escolhido!');
      ProgressBar.Position := 0;
    except
      ShowMessage('Algo deu errado :( Tente novamente, se o erro persistir contate o desenvolvedor.');
      ProgressBar.Position := 0;
    end;
    end;
end;

procedure TrptPerformanceProdutoV2.MaskEditDeptoChange(Sender: TObject);
var
  qryDepto : String;
begin
  try
    if MaskEditDepto.Text <> '' then
      begin
        ADOQueryDepto.Close;
        ADOQueryDepto.SQL.Clear;
        qryDepto := 'SELECT TOP 1 cNmDepartamento FROM Departamento WHERE nCdDepartamento =  ' + MaskEditDepto.Text;
        ADOQueryDepto.SQL.Add(qryDepto);
        ADOQueryDepto.Open;
        EditQDepto.Text := ADOQueryDepto.FieldByName('cNmDepartamento').Text;
      end
    else
      EditQDepto.Text := '';
  except
    if MaskEditDepto.Text <> '' then
    begin
      ShowMessage('Apenas n�meros s�o permitidos.');
      MaskEditDepto.Text := '';
      EditQDepto.Text := '';
    end;
  end;
end;

procedure TrptPerformanceProdutoV2.MaskEditCategChange(Sender: TObject);
var
  qryCateg : String;
begin
  try
    if MaskEditCateg.Text <> '' then
      begin
        ADOQueryCateg.Close;
        ADOQueryCateg.SQL.Clear;
        qryCateg := 'SELECT TOP 1 cNmCategoria FROM Categoria WHERE nCdCategoria =  ' + MaskEditCateg.Text;
        ADOQueryCateg.SQL.Add(qryCateg);
        ADOQueryCateg.Open;
        EditQCateg.Text := ADOQueryCateg.FieldByName('cNmCategoria').Text;
      end
    else
      EditQCateg.Text := '';
  except
    if MaskEditCateg.Text <> '' then
    begin
      ShowMessage('Apenas n�meros s�o permitidos.');
      MaskEditCateg.Text := '';
      EditQCateg.Text := '';
    end;
  end;
end;

procedure TrptPerformanceProdutoV2.MaskEditMarcaChange(Sender: TObject);
var
  qryMarca : String;
begin
  try
    if MaskEditMarca.Text <> '' then
      begin
        ADOQueryMarca.Close;
        ADOQueryMarca.SQL.Clear;
        qryMarca := 'SELECT TOP 1 cNmMarca FROM Marca WHERE nCdMarca =  ' + MaskEditMarca.Text;
        ADOQueryMarca.SQL.Add(qryMarca);
        ADOQueryMarca.Open;
        EditQMarca.Text := ADOQueryMarca.FieldByName('cNmMarca').Text;
      end
    else
      EditQMarca.Text := '';
  except
    if MaskEditMarca.Text <> '' then
    begin
      ShowMessage('Apenas n�meros s�o permitidos.');
      MaskEditMarca.Text := '';
      EditQMarca.Text := '';
    end;
  end;
end;

procedure TrptPerformanceProdutoV2.MaskEditLojaChange(Sender: TObject);
var
  qryLoja : String;
begin
  try
    if MaskEditLoja.Text <> '' then
      begin
        ADOQueryLoja.Close;
        ADOQueryLoja.SQL.Clear;
        qryLoja := 'SELECT TOP 1 cNmLoja FROM Loja WHERE nCdLoja =  ' + MaskEditLoja.Text;
        ADOQueryLoja.SQL.Add(qryLoja);
        ADOQueryLoja.Open;
        EditQLoja.Text := ADOQueryLoja.FieldByName('cNmLoja').Text;
      end
    else
      EditQLoja.Text := '';
  except
    if MaskEditLoja.Text <> '' then
    begin
      ShowMessage('Apenas n�meros s�o permitidos.');
      MaskEditLoja.Text := '';
      EditQLoja.Text := '';
    end;
  end;
end;

procedure TrptPerformanceProdutoV2.MaskEditDeptoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
            MaskEditDepto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPerformanceProdutoV2.MaskEditCategKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (MaskEditDepto.Text <> '') then
          nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdDepartamento = ' + MaskEditDepto.Text)
        else
          nPK := frmLookup_Padrao.ExecutaConsulta(46);

        If (nPK > 0) then
            MaskEditCateg.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPerformanceProdutoV2.MaskEditMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
            MaskEditMarca.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPerformanceProdutoV2.MaskEditLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            MaskEditLoja.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
  RegisterClass(TrptPerformanceProdutoV2);

end.
