inherited frmMapaCompra_Itens: TfrmMapaCompra_Itens
  Left = 160
  Top = 214
  Width = 901
  Height = 417
  BorderIcons = [biSystemMenu]
  Caption = 'Requisi'#231#245'es do Item do Mapa'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 177
    Width = 885
    Height = 204
  end
  inherited ToolBar1: TToolBar
    Width = 885
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 885
    Height = 148
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdRequisicao: TcxGridDBColumn
        Caption = 'Requisi'#231#227'o'
        DataBinding.FieldName = 'nCdRequisicao'
        Width = 76
      end
      object cxGrid1DBTableView1cNmSetor: TcxGridDBColumn
        Caption = 'Setor'
        DataBinding.FieldName = 'cNmSetor'
        Width = 143
      end
      object cxGrid1DBTableView1cNmSolicitante: TcxGridDBColumn
        Caption = 'Solicitante'
        DataBinding.FieldName = 'cNmSolicitante'
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Autorizador'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 164
      end
      object cxGrid1DBTableView1nQtde: TcxGridDBColumn
        Caption = 'Quantidade'
        DataBinding.FieldName = 'nQtde'
        HeaderGlyphAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1cNmCC: TcxGridDBColumn
        Caption = 'Centro de Custo'
        DataBinding.FieldName = 'cNmCC'
        Width = 212
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 177
    Width = 885
    Height = 204
    Align = alClient
    Caption = 'Informa'#231#245'es Adicionais'
    TabOrder = 2
    object Label1: TLabel
      Left = 74
      Top = 24
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Urgente'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 56
      Top = 48
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Justificativa'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 9
      Top = 72
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nota para Comprador'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 28
      Top = 96
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o T'#233'cnica'
      FocusControl = DBMemo1
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 120
      Top = 16
      Width = 43
      Height = 21
      DataField = 'cFlgUrgente'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 120
      Top = 40
      Width = 394
      Height = 21
      DataField = 'cJustificativa'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 120
      Top = 64
      Width = 394
      Height = 21
      DataField = 'cNotaComprador'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBMemo1: TDBMemo
      Tag = 1
      Left = 120
      Top = 88
      Width = 745
      Height = 105
      DataField = 'cDescricaoTecnica'
      DataSource = DataSource1
      TabOrder = 3
    end
  end
  object qryRequisicoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT ItemRequisicao.nCdRequisicao'
      '      ,cNmSetor'
      '      ,cNmSolicitante'
      '      ,cNmUsuario'
      '      ,nQtde'
      '      ,CASE WHEN cFlgUrgente = 1 THEN '#39'Sim'#39
      '            ELSE '#39'N'#227'o'#39
      '       END cFlgUrgente'
      '      ,cJustificativa'
      '      ,cNotaComprador'
      '      ,cDescricaoTecnica'
      '      ,cNmCC'
      '  FROM ItemMapaCompraItemRequisicao'
      
        '       INNER JOIN ItemRequisicao ON ItemRequisicao.nCdItemRequis' +
        'icao = ItemMapaCompraItemRequisicao.nCdItemRequisicao'
      
        '       INNER JOIN Requisicao     ON Requisicao.nCdRequisicao    ' +
        '     = ItemRequisicao.nCdRequisicao'
      
        '       LEFT  JOIN CentroCusto    ON CentroCusto.nCdCC           ' +
        '     = Requisicao.nCdCC'
      
        '       INNER JOIN Setor          ON Setor.nCdSetor              ' +
        '     = Requisicao.nCdSetor'
      
        '       LEFT  JOIN Usuario        ON Usuario.nCdUsuario          ' +
        '     = Requisicao.nCdUsuarioAutor'
      ' WHERE ItemMapaCompraItemRequisicao.nCdItemMapaCompra = :nPK')
    Left = 208
    Top = 176
    object qryRequisicoesnCdRequisicao: TIntegerField
      FieldName = 'nCdRequisicao'
    end
    object qryRequisicoescNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
    object qryRequisicoescNmSolicitante: TStringField
      FieldName = 'cNmSolicitante'
      Size = 30
    end
    object qryRequisicoescNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryRequisicoesnQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
      Size = 2
    end
    object qryRequisicoescFlgUrgente: TStringField
      FieldName = 'cFlgUrgente'
      ReadOnly = True
      Size = 3
    end
    object qryRequisicoescJustificativa: TStringField
      FieldName = 'cJustificativa'
      Size = 30
    end
    object qryRequisicoescNotaComprador: TStringField
      FieldName = 'cNotaComprador'
      Size = 30
    end
    object qryRequisicoescDescricaoTecnica: TMemoField
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
    object qryRequisicoescNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryRequisicoes
    Left = 256
    Top = 176
  end
end
