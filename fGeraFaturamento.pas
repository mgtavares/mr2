unit fGeraFaturamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, Mask, DBCtrls, ADODB, cxLookAndFeelPainters, cxButtons,
  GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmGeraFaturamento = class(TfrmProcesso_Padrao)
    qryGrupoPedido: TADOQuery;
    qryGrupoPedidonCdGrupoPedido: TIntegerField;
    qryGrupoPedidocNmGrupoPedido: TStringField;
    qryGrupoPedidocFlgFaturar: TIntegerField;
    dsGrupoPedido: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    dsTerceiro: TDataSource;
    qryPedido: TADOQuery;
    dsPedido: TDataSource;
    qryAux: TADOQuery;
    qryPedidoPedidoNmero: TIntegerField;
    qryPedidoPedidoData: TDateTimeField;
    qryPedidoPedidoTipo: TStringField;
    qryPedidoPrevisodeEntregaIncio: TDateTimeField;
    qryPedidoPrevisodeEntregaFim: TDateTimeField;
    qryPedidoPedidoStatus: TStringField;
    qryPedidoPedidoSaldoFaturar: TBCDField;
    qryPedidoPedidoTerceiroEntrega: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1PedidoNmero: TcxGridDBColumn;
    cxGrid1DBTableView1PedidoData: TcxGridDBColumn;
    cxGrid1DBTableView1PedidoTipo: TcxGridDBColumn;
    cxGrid1DBTableView1PrevisodeEntregaIncio: TcxGridDBColumn;
    cxGrid1DBTableView1PrevisodeEntregaFim: TcxGridDBColumn;
    cxGrid1DBTableView1PedidoStatus: TcxGridDBColumn;
    cxGrid1DBTableView1PedidoSaldoFaturar: TcxGridDBColumn;
    cxGrid1DBTableView1PedidoTerceiroEntrega: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    MaskEdit2: TMaskEdit;
    Label3: TLabel;
    Label2: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    qryPedidocFlgFaturar: TIntegerField;
    qryTerceironPerTaxa1: TFloatField;
    qryTerceironPerTaxa2: TFloatField;
    qryPedidoPedidoCodigoTerceiro: TIntegerField;
    qryPedidoTerceironPerTaxa1: TFloatField;
    qryPedidoTerceironPerTaxa2: TFloatField;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure Me(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraFaturamento: TfrmGeraFaturamento;

implementation

uses fLookup_Padrao, fItemFaturar, fMenu;

{$R *.dfm}

procedure TfrmGeraFaturamento.FormShow(Sender: TObject);
begin
  inherited;

  qryGrupoPedido.Close ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdGrupoPedido FROM GrupoPedido WHERE cFlgFaturar = 1') ;
  qryAux.Open ;

  if (qryAux.RecordCount = 1) then
  begin
      PosicionaQuery(qryGrupoPedido,qryAux.FieldList[0].AsString) ;
      MaskEdit1.Text := qryGrupoPedidonCdGrupoPedido.AsString ;
  end ;

  qryAux.Close ;

end;

procedure TfrmGeraFaturamento.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryPedido.Close;
  qryGrupoPedido.Close ;
  PosicionaQuery(qryGrupoPedido, MaskEdit1.Text) ;
end;

procedure TfrmGeraFaturamento.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(79,'cFlgFaturar = 1');

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoPedido, IntToStr(nPK)) ;
        end ;

    end ;

  end ;

end;

procedure TfrmGeraFaturamento.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;

end;

procedure TfrmGeraFaturamento.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(80);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, IntToStr(nPK)) ;
        end ;

    end ;

  end ;

end;

procedure TfrmGeraFaturamento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Selecione um grupo de pedidos') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now());
  end ;

  qryPedido.Close;
  qryPedido.Parameters.ParamByName('nPK').Value         := frmMenu.ConvInteiro(MaskEdit1.Text);
  qryPedido.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryPedido.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
  qryPedido.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit3.Text);
  qryPedido.Open;

  if (qryPedido.eof) then
  begin
      MensagemAlerta('Nenhum pedido dispon�vel para faturamento.') ;
  end ;
end;

procedure TfrmGeraFaturamento.Me(Sender: TObject);
var
  objItemFaturar : TfrmItemFaturar;
begin
  inherited;

  if (qryPedidoPedidoNmero.Value > 0) then
  begin

      if (qryPedidocFlgFaturar.Value = 0) then
      begin
          MensagemAlerta('O Status atual do pedido n�o permite faturamento.') ;
          exit ;
      end ;

      if (StrToDate(MaskEdit2.Text) <= StrToDate(frmMenu.LeParametro('DTCONTAB'))) then
      begin
          MensagemAlerta('Data de faturamento deve ser maior que o per�odo da �ltima contabiliza��o efetuada no sistema.' + #13#13 + '�lt. Contabil.: ' + frmMenu.LeParametro('DTCONTAB') + '.');
          MaskEdit2.SetFocus;
          Abort;
      end;

      objItemFaturar := TfrmItemFaturar.Create(nil);

      Try
          Try
             objItemFaturar.nCdPedido  := qryPedidoPedidoNmero.Value ;
             objItemFaturar.dDtFaturar := StrToDate(MaskEdit2.Text) ;
             objItemFaturar.nPerTaxa1  := qryPedidoTerceironPerTaxa1.Value;
             objItemFaturar.nPerTaxa2  := qryPedidoTerceironPerTaxa2.Value;
             PosicionaQuery(objItemFaturar.qryItemPedido,qryPedidoPedidoNmero.AsString) ;
             showForm(objItemFaturar,false);
          except
             MensagemErro('Erro ao gerar o Faturamento');
             raise;
          end
      Finally
          FreeAndNil(objItemFaturar);
      end;

      qryPedido.Close ;
      qryPedido.Open ;

  end ;


end;

procedure TfrmGeraFaturamento.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  if (Trim(DBEdit2.Text) <> '') then
      ToolButton1.Click
  else
      MaskEdit1.SetFocus;
end;

initialization
    RegisterClass(TfrmGeraFaturamento) ;

end.
