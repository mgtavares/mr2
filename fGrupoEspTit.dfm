inherited frmGrupoEspTit: TfrmGrupoEspTit
  Caption = 'Grupo Esp'#233'cie T'#237'tulo'
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoEspTit'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoEspTit'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOESPTIT'
      'WHERE nCdGrupoEspTit = :nPK')
    object qryMasternCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryMastercNmGrupoEspTit: TStringField
      FieldName = 'cNmGrupoEspTit'
      Size = 50
    end
  end
end
