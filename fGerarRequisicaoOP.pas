unit fGerarRequisicaoOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxControls, cxPC, DBGridEhGrouping, DB, ADODB, GridsEh,
  DBGridEh, Mask, DBCtrls;

type
  TfrmGerarRequisicaoOP = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    GroupBox1: TGroupBox;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryEtapas: TADOQuery;
    qryEtapasnCdEtapaOrdemProducao: TIntegerField;
    qryEtapasiSeq: TIntegerField;
    qryEtapasnCdCentroProdutivo: TIntegerField;
    qryEtapascNmCentroProdutivo: TStringField;
    qryEtapasnCdEtapaProducao: TIntegerField;
    qryEtapascNmEtapaProducao: TStringField;
    qryEtapasnQtdePlanejada: TBCDField;
    qryEtapasnQtdeAtendida: TBCDField;
    qryEtapasiQtdeRequisicoes: TIntegerField;
    qryEtapasnSaldoRequisitar: TFloatField;
    dsEtapas: TDataSource;
    qryOrdemProducao: TADOQuery;
    qryOrdemProducaocNumeroOP: TStringField;
    qryOrdemProducaocNmTipoOP: TStringField;
    qryOrdemProducaocNmTabTipoStatusOP: TStringField;
    qryOrdemProducaonCdProduto: TIntegerField;
    qryOrdemProducaocNmProduto: TStringField;
    qryOrdemProducaocReferencia: TStringField;
    qryOrdemProducaonQtdePlanejada: TBCDField;
    qryOrdemProducaocUnidadeMedida: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    cxTabSheet2: TcxTabSheet;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryPreparaTemp: TADOQuery;
    qryTemp: TADOQuery;
    qryTempnCdProdutoEtapaOrdemProducao: TIntegerField;
    qryTempnCdProduto: TIntegerField;
    qryTempcNmProduto: TStringField;
    qryTempcReferencia: TStringField;
    qryTempcUnidadeMedida: TStringField;
    qryTempnQtdePlanejada: TBCDField;
    qryTempnQtdeAtendida: TBCDField;
    qryTempnQtdeReqPend: TBCDField;
    qryTempnSaldoReq: TBCDField;
    qryTempnSaldoEstoque: TBCDField;
    qryTempnQtdeOrdem: TBCDField;
    qryTempnQtdeEmpenhado: TBCDField;
    qryTempnQtdeEmpenhadoOP: TBCDField;
    qryTempnSaldoDisponivel: TBCDField;
    qryTempnCdLocalEstoque: TIntegerField;
    qryTempcNmLocalEstoque: TStringField;
    dsTemp: TDataSource;
    SP_PREPARA_REQUISICAO_OP: TADOStoredProc;
    ToolButton7: TToolButton;
    btAtualizarDados: TToolButton;
    qryTempcUnidadeMedidaEstoque: TStringField;
    SP_GERA_REQUISICAO_CONSUMO_OP: TADOStoredProc;
    ToolButton8: TToolButton;
    btExibeReqs: TToolButton;
    procedure exibeOrdemProducao( nCdOrdemProducaoAux : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure qryEtapasAfterScroll(DataSet: TDataSet);
    procedure cxTabSheet2Show(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton6Click(Sender: TObject);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure exibirProdutosEtapa( bExibirTela : boolean );
    procedure btAtualizarDadosClick(Sender: TObject);
    procedure btExibeReqsClick(Sender: TObject);

  private
    { Private declarations }
    nCdOrdemProducao : integer ;
  public
    { Public declarations }
  end;

var
  frmGerarRequisicaoOP: TfrmGerarRequisicaoOP;

implementation

uses fOP , fMenu , fGerarRequisicaoOP_ExibeRequisicoes;

{$R *.dfm}

{ TfrmGerarRequisicaoOP }

procedure TfrmGerarRequisicaoOP.exibeOrdemProducao(
  nCdOrdemProducaoAux: integer);
begin

    nCdOrdemProducao := nCdOrdemProducaoAux ;

    PosicionaQuery( qryOrdemProducao , intToStr( nCdOrdemProducao ) ) ;
    PosicionaQuery( qryEtapas        , intToStr( nCdOrdemProducao ) ) ;

    Self.ShowModal ;

end;

procedure TfrmGerarRequisicaoOP.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh2.TitleFont.Size := 8 ;
  DBGridEh2.Font.Size      := 8 ;

  cxPageControl1.ActivePageIndex := 0;
  DBGridEh1.SetFocus;

end;

procedure TfrmGerarRequisicaoOP.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( nCdOrdemProducao );

  showForm( objForm , TRUE ) ;

  qryEtapas.Requery();

  exibirProdutosEtapa( FALSE ) ;

end;

procedure TfrmGerarRequisicaoOP.qryEtapasAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryEtapas.Active) then
      cxTabSheet2.Caption := 'Produtos da Etapa : ' + qryEtapascNmEtapaProducao.Value ;

end;

procedure TfrmGerarRequisicaoOP.cxTabSheet2Show(Sender: TObject);
begin
  inherited;

  exibirProdutosEtapa( TRUE ) ;

end;

procedure TfrmGerarRequisicaoOP.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if (qryEtapas.Active) and (qryEtapasnCdEtapaOrdemProducao.Value > 0) then
      cxPageControl1.ActivePageIndex := 1 ;
      
end;

procedure TfrmGerarRequisicaoOP.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmGerarRequisicaoOP.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmGerarRequisicaoOP.ToolButton6Click(Sender: TObject);
var
  nQtdeReq : double ;
begin
  inherited;

  if (qryTemp.State = dsEdit) then
      qryTemp.Post ;

  if (not qryTemp.Active) or (qryTemp.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto dispon�vel para requisitar.') ;
      abort ;
  end ;

  nQtdeReq := 0 ;

  qryTemp.First ;

  while not qryTemp.eof do
  begin

      nQtdeReq := nQtdeReq + qryTempnSaldoReq.Value ;

      qryTemp.Next ;

  end ;

  qryTemp.First ;

  if (nQtdeReq <= 0) then
  begin
      MensagemAlerta('Nenhuma quantidade informada para requisi��o.') ;
      DBGridEh2.Col := 9 ;
      abort ;
  end ;

  if (MessageDlg('Confirma a gera��o da requisi��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;


  frmMenu.Connection.BeginTrans ;

  try

      SP_GERA_REQUISICAO_CONSUMO_OP.Close ;
      SP_GERA_REQUISICAO_CONSUMO_OP.Parameters.ParamByName('@nCdEtapaOrdemProducao').Value := qryEtapasnCdEtapaOrdemProducao.Value;
      SP_GERA_REQUISICAO_CONSUMO_OP.Parameters.ParamByName('@nCdUsuario').Value            := frmMenu.nCdUsuarioLogado;
      SP_GERA_REQUISICAO_CONSUMO_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans ;

  btAtualizarDados.Click;

  ShowMessage('Requisi��es Geradas com Sucesso!') ;

  btExibeReqs.Click;

end;

procedure TfrmGerarRequisicaoOP.qryTempBeforePost(DataSet: TDataSet);
begin

  if (qryTempnSaldoReq.Value < 0) then
  begin
      MensagemAlerta('A quantidade requisitada n�o pode ser negativa.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmGerarRequisicaoOP.exibirProdutosEtapa( bExibirTela : boolean );
begin

  qryTemp.Close;

  qryPreparaTemp.ExecSQL;

  try

      SP_PREPARA_REQUISICAO_OP.Close;
      SP_PREPARA_REQUISICAO_OP.Parameters.ParamByName('@nCdEtapaOrdemProducao').Value := qryEtapasnCdEtapaOrdemProducao.Value ;
      SP_PREPARA_REQUISICAO_OP.ExecProc;

  except

      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  qryTemp.Open;

  if ( bExibirTela ) then
  begin

      DBGridEh2.Col := 9 ;
      DBGridEh2.SetFocus;
      
  end ;

end;

procedure TfrmGerarRequisicaoOP.btAtualizarDadosClick(Sender: TObject);
begin
  inherited;

  exibirProdutosEtapa( TRUE ) ;
  
end;

procedure TfrmGerarRequisicaoOP.btExibeReqsClick(Sender: TObject);
var
  objForm : TfrmGerarRequisicaoOP_ExibeRequisicoes;
begin
  inherited;

  objForm := TfrmGerarRequisicaoOP_ExibeRequisicoes.Create( Self ) ;

  objForm.exibeRequisicoes( nCdOrdemProducao
                           ,qryEtapasnCdEtapaOrdemProducao.Value );

  freeAndNil( objForm ) ;

  DBGridEh2.SetFocus ;
  
end;

end.
