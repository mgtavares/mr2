inherited rptOrcamentoPrevReal: TrptOrcamentoPrevReal
  Left = 74
  Top = 161
  Caption = 'Rel. Or'#231'amento Previsto x Realizado'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 16
    Top = 48
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Or'#231'amento'
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 72
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Conta'
  end
  object Label3: TLabel [3]
    Left = 40
    Top = 96
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtOrcamento: TER2LookupMaskEdit [5]
    Left = 72
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnExit = edtOrcamentoExit
    CodigoLookup = 210
    WhereAdicional.Strings = (
      'EXISTS(SELECT 1'
      '         FROM UsuarioOrcamento UO'
      '        WHERE UO.nCdOrcamento = Orcamento.nCdOrcamento'
      '          AND UO.nCdUsuario   = @@Usuario)')
    QueryLookup = qryOrcamento
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 144
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmOrcamento'
    DataSource = DataSource1
    TabOrder = 5
  end
  object RadioGroup1: TRadioGroup [7]
    Left = 72
    Top = 120
    Width = 337
    Height = 57
    Caption = 'Periodicidade'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Mensal'
      'Trimestral'
      'Semestral')
    TabOrder = 4
  end
  object RadioGroup2: TRadioGroup [8]
    Left = 416
    Top = 120
    Width = 337
    Height = 57
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Tela')
    TabOrder = 6
  end
  object edtGrupoConta: TER2LookupMaskEdit [9]
    Left = 72
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnExit = edtGrupoContaExit
    CodigoLookup = 40
    QueryLookup = qryGrupoPlanoConta
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 144
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmGrupoPlanoConta'
    DataSource = DataSource2
    TabOrder = 7
  end
  object edtPlanoConta: TER2LookupMaskEdit [11]
    Left = 72
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    OnExit = edtPlanoContaExit
    OnBeforeLookup = edtPlanoContaBeforeLookup
    CodigoLookup = 41
    QueryLookup = qryPlanoConta
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 144
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmPlanoConta'
    DataSource = DataSource3
    TabOrder = 8
  end
  object qryOrcamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdOrcamento'
      '      ,cNmOrcamento'
      '  FROM Orcamento'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioOrcamento UO'
      '               WHERE UO.nCdOrcamento = Orcamento.nCdOrcamento'
      '                 AND UO.nCdUsuario   = :nCdUsuario)'
      '   AND nCdOrcamento = :nPK')
    Left = 384
    Top = 144
    object qryOrcamentonCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryOrcamentocNmOrcamento: TStringField
      FieldName = 'cNmOrcamento'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryOrcamento
    Left = 496
    Top = 272
  end
  object qryGrupoPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoPlanoConta, cNmGrupoPlanoConta'
      '  FROM GrupoPlanoConta'
      ' WHERE nCdGrupoPlanoConta = :nPK')
    Left = 544
    Top = 264
    object qryGrupoPlanoContanCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryGrupoPlanoContacNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryGrupoPlanoConta
    Left = 504
    Top = 280
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdGrupoPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdGrupoPlanoConta int'
      ''
      'Set @nCdGrupoPlanoConta = :nCdGrupoPlanoConta'
      ''
      'SELECT nCdPlanoConta'
      '      ,cNmPlanoConta'
      '  FROM PlanoConta'
      ' WHERE nCdPlanoConta = :nPK'
      
        '   AND ((nCdGrupoPlanoConta = @nCdGrupoPlanoConta) OR (@nCdGrupo' +
        'PlanoConta = 0))')
    Left = 592
    Top = 256
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryPlanoConta
    Left = 512
    Top = 288
  end
end
