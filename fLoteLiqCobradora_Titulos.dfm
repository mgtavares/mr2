inherited frmLoteLiqCobradora_Titulos: TfrmLoteLiqCobradora_Titulos
  Width = 968
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos Movimentados'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 952
  end
  inherited ToolBar1: TToolBar
    Width = 952
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 952
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulos
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDrawColumnCell = DBGridEh1DrawColumnCell
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 103
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 260
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoAnterior'
        Footers = <>
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'nValLiq'
        Footers = <>
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoFinal'
        Footers = <>
        Width = 95
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TituloLoteLiqCobradora.nCdTitulo'
      '      ,dDtVenc'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,TituloLoteLiqCobradora.nSaldoAnterior'
      '      ,TituloLoteLiqCobradora.nValLiq'
      
        '      ,(TituloLoteLiqCobradora.nSaldoAnterior - TituloLoteLiqCob' +
        'radora.nValLiq) as nSaldoFinal'
      '  FROM TituloLoteLiqCobradora'
      
        '       INNER JOIN Titulo ON Titulo.nCdTitulo = TituloLoteLiqCobr' +
        'adora.nCdTitulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE TituloLoteLiqCobradora.nCdItemLoteLiqCobradora = :nPK'
      ' ORDER BY 2')
    Left = 368
    Top = 168
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Movimentados|C'#243'd'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Movimentados|Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Movimentados|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos Movimentados|Parc.'
      FieldName = 'iParcela'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Movimentados|Esp'#233'cie T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosnSaldoAnterior: TBCDField
      DisplayLabel = 'T'#237'tulos Movimentados|Saldo Aberto'
      FieldName = 'nSaldoAnterior'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValLiq: TBCDField
      DisplayLabel = 'T'#237'tulos Movimentados|Valor Liq.'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoFinal: TBCDField
      DisplayLabel = 'T'#237'tulos Movimentados|Saldo Final'
      FieldName = 'nSaldoFinal'
      ReadOnly = True
      Precision = 13
      Size = 2
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 440
    Top = 152
  end
end
