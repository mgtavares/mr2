inherited frmProdutoERP_UltimasCompras: TfrmProdutoERP_UltimasCompras
  Left = 119
  Top = 139
  Width = 918
  Height = 582
  Caption = #218'ltimas Compras'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 902
    Height = 517
  end
  inherited ToolBar1: TToolBar
    Width = 902
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 902
    Height = 517
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsUltimasCompras
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = ',0'
          Kind = skSum
        end
        item
          Format = ',0'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtde
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.ConteudoCaixa
      object cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn
        DataBinding.FieldName = 'nCdRecebimento'
        Width = 53
      end
      object cxGrid1DBTableView1dDtReceb: TcxGridDBColumn
        DataBinding.FieldName = 'dDtReceb'
        Width = 90
      end
      object cxGrid1DBTableView1cNrDocto: TcxGridDBColumn
        DataBinding.FieldName = 'cNrDocto'
        Width = 75
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 133
      end
      object cxGrid1DBTableView1nQtde: TcxGridDBColumn
        DataBinding.FieldName = 'nQtde'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdPedido'
        HeaderAlignmentHorz = taRightJustify
        Width = 108
      end
      object cxGrid1DBTableView1nValUnitario: TcxGridDBColumn
        DataBinding.FieldName = 'nValUnitario'
        HeaderAlignmentHorz = taRightJustify
        Width = 85
      end
      object cxGrid1DBTableView1nPercIPI: TcxGridDBColumn
        DataBinding.FieldName = 'nPercIPI'
        HeaderAlignmentHorz = taRightJustify
        Width = 57
      end
      object cxGrid1DBTableView1nPercICMSSub: TcxGridDBColumn
        DataBinding.FieldName = 'nPercICMSSub'
        HeaderAlignmentHorz = taRightJustify
        Width = 55
      end
      object cxGrid1DBTableView1nValCustoFinal: TcxGridDBColumn
        DataBinding.FieldName = 'nValCustoFinal'
        HeaderAlignmentHorz = taRightJustify
        Width = 117
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryUltimasCompras: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Recebimento.dDtReceb'
      '      ,ItemRecebimento.nCdRecebimento'
      '      ,Recebimento.cNrDocto'
      '      ,Terceiro.cNmTerceiro'
      '      ,ItemRecebimento.nValUnitario'
      '      ,ItemRecebimento.nPercIPI'
      '      ,ItemRecebimento.nPercICMSSub'
      '      ,ItemRecebimento.nValCustoFinal'
      '      ,ItemPedido.nCdPedido'
      '      ,ItemRecebimento.nQtde'
      '  FROM ItemRecebimento '
      
        '       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = It' +
        'emRecebimento.nCdRecebimento'
      
        '       LEFT  JOIN ItemPedido  ON ItemPedido.nCdItemPedido   = It' +
        'emRecebimento.nCdItemPedido'
      
        '       LEFT  JOIN Terceiro    ON Terceiro.nCdTerceiro       = Re' +
        'cebimento.nCdTerceiro'
      ' WHERE ItemRecebimento.nCdProduto = :nCdProduto'
      '   AND Recebimento.nCdTabStatusReceb IN (3,4,5)'
      ' ORDER BY Recebimento.dDtReceb DESC')
    Left = 240
    Top = 184
    object qryUltimasComprasdDtReceb: TDateTimeField
      DisplayLabel = 'Dt. Recebto'
      FieldName = 'dDtReceb'
    end
    object qryUltimasComprasnCdRecebimento: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdRecebimento'
    end
    object qryUltimasComprascNrDocto: TStringField
      DisplayLabel = 'N'#250'm. NF'
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryUltimasComprascNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryUltimasComprasnQtde: TBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'nQtde'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryUltimasComprasnCdPedido: TIntegerField
      DisplayLabel = 'Pedido Compra'
      FieldName = 'nCdPedido'
    end
    object qryUltimasComprasnValUnitario: TBCDField
      DisplayLabel = 'Valor Unit.'
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryUltimasComprasnPercIPI: TBCDField
      DisplayLabel = '% IPI'
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryUltimasComprasnPercICMSSub: TBCDField
      DisplayLabel = '% ST'
      FieldName = 'nPercICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryUltimasComprasnValCustoFinal: TBCDField
      DisplayLabel = 'Custo Unit. Final.'
      FieldName = 'nValCustoFinal'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
  end
  object dsUltimasCompras: TDataSource
    DataSet = qryUltimasCompras
    Left = 256
    Top = 216
  end
end
