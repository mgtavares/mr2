unit fCancDoctoFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmCancDoctoFiscal = class(TfrmCadastro_Padrao)
    qryMasternCdDoctoFiscal: TAutoIncField;
    qryMastercSerie: TStringField;
    qryMasteriNrDocto: TIntegerField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMastercNmTerceiro: TStringField;
    qryMasternValTotal: TBCDField;
    qryMastercCFOP: TStringField;
    qryMastercTextoCFOP: TStringField;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMastercNmTipoDoctoFiscal: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    cxButton1: TcxButton;
    usp_cancela: TADOStoredProc;
    qryMastercNrProtocoloNFe: TStringField;
    qryMastercNrReciboNFe: TStringField;
    Label8: TLabel;
    DBEdit11: TDBEdit;
    Label10: TLabel;
    DBEdit12: TDBEdit;
    qryVerificaComplemento: TADOQuery;
    qryVerificaComplementonCdDoctoFiscal: TAutoIncField;
    qryEnvioNFeEmail: TADOQuery;
    qryEnvioNFeEmailcFlgEnviaNFeEmail: TIntegerField;
    qryEnvioNFeEmailcEmailNFe: TStringField;
    qryEnvioNFeEmailcEmailCopiaNFe: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasteriStatusRetorno: TIntegerField;
    qryMastercNmStatusRetorno: TStringField;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label11: TLabel;
    SP_PROCESSA_NUMDOCTOFISCAL: TADOStoredProc;
    qryMasternCdSerieFiscal: TIntegerField;
    qryMastercFlgDocPendente: TIntegerField;
    qryAux: TADOQuery;
    qryMastercFlgUsoDenegado: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCancDoctoFiscal: TfrmCancDoctoFiscal;

implementation

uses fMenu, fEmiteNFe2;

{$R *.dfm}

procedure TfrmCancDoctoFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'DOCTOFISCAL' ;
  nCdTabelaSistema  := 42 ;
  nCdConsultaPadrao := 91 ;
end;

procedure TfrmCancDoctoFiscal.cxButton1Click(Sender: TObject);
var
  objCancNFe : TfrmEmiteNfe2;
begin
    try
        if (not qryMaster.Active) or (qryMasternCdDoctoFiscal.Value = 0) then
        begin
            ShowMessage('Selecione um documento fiscal.') ;
            exit ;
        end ;

        if (qryMastercNrProtocoloNFe.Value = '') then
        begin
            qryAux.Close;
            qryAux.SQL.Add('SELECT 1 FROM DoctoFiscal WHERE nCdDoctoFiscal = ' + qryMasternCdDoctoFiscal.AsString + ' AND cNrProtocoloNFe IS NOT NULL');
            qryAux.Open;

            if (not qryAux.IsEmpty) then
            begin
                MensagemAlerta('Documento fiscal impresso anteriormente, tente novamente.');
                PosicionaPK(qryMasternCdDoctoFiscal.Value);
                Exit;
            end;
        end;

        case MessageDlg('Confirma o cancelamento deste documento fiscal ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
        end ;

        qryVerificaComplemento.Close;
        qryVerificaComplemento.Parameters.ParamByName('nCdDoctoFiscal').Value := qryMasternCdDoctoFiscal.Value ;
        qryVerificaComplemento.Open;

        if not (qryVerificaComplemento.Eof) then
        begin
            MensagemAlerta('Este documento fiscal tem documento complementar ativo. N�mero Docto Complementar : ' + qryVerificaComplementonCdDoctoFiscal.AsString + '. Cancele o documento complementar antes de cancelar este.');
            abort;
        end;

        objCancNFe := TfrmEmiteNfe2.Create(nil);

        if ((qryMastercNrProtocoloNFe.Value <> '') and (qryMastercFlgUsoDenegado.Value = 1)) then
            ShowMessage('Documento fiscal com situa��o de USO DENEGADO n�o permite cancelamento eletr�nico, o processo ser� feito apenas no sistema.');

        try
            frmMenu.Connection.BeginTrans;

            { -- cancela a nota no sistema -- }
            usp_cancela.Close;
            usp_Cancela.Parameters.ParamByName('@nCdDoctoFiscal').Value := qryMasternCdDoctoFiscal.Value;
            usp_Cancela.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
            usp_Cancela.ExecProc;

            if (((qryMastercNrProtocoloNFe.Value <> '') and (qryMastercFlgUsoDenegado.Value = 0)) or (qryMastercFlgDocPendente.Value = 1)) then
            begin
                { -- cancela a nota eletronicamente -- }
                objCancNFe.cancelarNFe(qryMasternCdDoctoFiscal.Value);
            end;

            { -- se documento n�o foi impresso, ent�o remove registro tempor�rio de numera��o e atualiza cadastro da s�rie -- }
            if (qryMasteriNrDocto.Value = 0) then
            begin
                SP_PROCESSA_NUMDOCTOFISCAL.Close;
                SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@nCdSerieFiscal').Value := qryMasternCdSerieFiscal.Value;
                SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value := qryMasternCdDoctoFiscal.Value;
                SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@cAcao').Value          := 'C';
                SP_PROCESSA_NUMDOCTOFISCAL.ExecProc;
            end;

            frmMenu.Connection.CommitTrans;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.');
            Raise;
        end;

        ShowMessage('Documento fiscal cancelado com sucesso.');

        qryEnvioNFeEmail.Close;
        qryEnvioNFeEmail.Parameters.ParamByName('nPK').Value := qryMasternCdTerceiro.Value;
        qryEnvioNFeEmail.Open;

        if ((qryEnvioNFeEmailcFlgEnviaNFeEmail.Value = 1) and (qryMastercNrProtocoloNFe.Value <> '')) then
        begin
            case MessageDlg('Desejar enviar o protocolo de cancelmanto da NFe por e-mail ?',mtConfirmation,[mbYes,mbNo],0) of
                mrNo:exit ;
            end ;

            try
                objCancNFe.enviaEmailCancNFe(qryMasternCdDoctoFiscal.Value,qryEnvioNFeEmailcEmailNFe.Value,qryEnvioNFeEmailcEmailCopiaNFe.Value);
            except
                MensagemErro('N�o foi poss�vel enviar a NF-e por e-mail, verifique as configura��es de email e/ou sua conex�o.');
                raise;
            end;
               if not(Trim(qryEnvioNFeEmailcEmailCopiaNFe.Value) = '') then
                   ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value + ' e ' + qryEnvioNFeEmailcEmailCopiaNFe.Value)
               else ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value) ;
        end;

    finally

        qryEnvioNFeEmail.Close;
        qryMaster.Close ;
        freeAndNil( objCancNFe ) ;

    end ;

end;

procedure TfrmCancDoctoFiscal.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

initialization
    RegisterClass(TfrmCancDoctoFiscal) ;

end.
