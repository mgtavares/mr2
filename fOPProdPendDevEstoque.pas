unit fOPProdPendDevEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ImgList, ComCtrls, ToolWin,
  ExtCtrls;

type
  TfrmOPProdPendDevEstoque = class(TfrmProcesso_Padrao)
    cxGrid5: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridLevel5: TcxGridLevel;
    qryProdutoPendente: TADOQuery;
    dsProdutoPendente: TDataSource;
    qryProdutoPendentenCdOrdemProducao: TIntegerField;
    qryProdutoPendentecNumeroOP: TStringField;
    qryProdutoPendentenCdPedido: TIntegerField;
    qryProdutoPendentecNmTerceiro: TStringField;
    qryProdutoPendentedDtRealConclusao: TDateTimeField;
    qryProdutoPendentenCdProduto: TIntegerField;
    qryProdutoPendentecNmProduto: TStringField;
    qryProdutoPendentecUnidadeMedida: TStringField;
    qryProdutoPendentenQtdePlanejada: TBCDField;
    qryProdutoPendentenQtdeAtendida: TBCDField;
    qryProdutoPendentenQtdeConsumida: TBCDField;
    qryProdutoPendentenSaldoDevolver: TBCDField;
    qryProdutoPendentenCdLocalEstoque: TIntegerField;
    qryProdutoPendentecNmLocalEstoque: TStringField;
    cxGridDBTableView5nCdOrdemProducao: TcxGridDBColumn;
    cxGridDBTableView5cNumeroOP: TcxGridDBColumn;
    cxGridDBTableView5nCdPedido: TcxGridDBColumn;
    cxGridDBTableView5cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView5dDtRealConclusao: TcxGridDBColumn;
    cxGridDBTableView5nCdProduto: TcxGridDBColumn;
    cxGridDBTableView5cNmProduto: TcxGridDBColumn;
    cxGridDBTableView5cUnidadeMedida: TcxGridDBColumn;
    cxGridDBTableView5nQtdePlanejada: TcxGridDBColumn;
    cxGridDBTableView5nQtdeAtendida: TcxGridDBColumn;
    cxGridDBTableView5nQtdeConsumida: TcxGridDBColumn;
    cxGridDBTableView5nSaldoDevolver: TcxGridDBColumn;
    cxGridDBTableView5nCdLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView5cNmLocalEstoque: TcxGridDBColumn;
    qryProdutoPendentecNmTipoOP: TStringField;
    cxGridDBTableView5cNmTipoOP: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOPProdPendDevEstoque: TfrmOPProdPendDevEstoque;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmOPProdPendDevEstoque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryProdutoPendente.Close;
  PosicionaQuery( qryProdutoPendente , intToStr( frmMenu.nCdEmpresaAtiva ) ) ;
  
end;

procedure TfrmOPProdPendDevEstoque.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
  
end;

initialization
    RegisterClass( TfrmOPProdPendDevEstoque ) ;
    
end.
