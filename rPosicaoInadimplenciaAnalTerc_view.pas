unit rPosicaoInadimplenciaAnalTerc_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TrelPosicaoInadimplenciaAnalTerc_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    SPREL_POSICAO_INADIMPLENCIA: TADOStoredProc;
    SPREL_POSICAO_INADIMPLENCIAcNmTerceiro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAcCNPJCPF: TStringField;
    SPREL_POSICAO_INADIMPLENCIAdDtCadastro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAnValTotal: TBCDField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloTotal: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimp: TBCDField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimp: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimpAtual: TBCDField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimpAtual: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnMediaCompetencia: TBCDField;
    SPREL_POSICAO_INADIMPLENCIAnMediaAtual: TBCDField;
    QRBand3: TQRBand;
    QRDBText7: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRBand4: TQRBand;
    QRLabel14: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    QRDBText4: TQRDBText;
    QRLabel5: TQRLabel;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  relPosicaoInadimplenciaAnalTerc_view: TrelPosicaoInadimplenciaAnalTerc_view;

implementation

{$R *.dfm}

procedure TrelPosicaoInadimplenciaAnalTerc_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
    if (QRBand3.Color = clWhite) then
        QRBand3.Color := clSilver
    else QRBand3.Color := clWhite ;
end;

end.
