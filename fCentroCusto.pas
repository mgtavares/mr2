unit fCentroCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, ImgList;

type
  TfrmCentroCusto = class(TfrmCadastro_Padrao)
    qryMasternCdCC: TIntegerField;
    qryMastercNmCC: TStringField;
    qryMasternCdCC1: TIntegerField;
    qryMasternCdCC2: TIntegerField;
    qryMastercCdCC: TStringField;
    qryMastercCdHie: TStringField;
    qryMasteriNivel: TSmallintField;
    qryMastercFlgLanc: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryCC1: TADOQuery;
    qryCC1nCdCC: TIntegerField;
    qryCC1cNmCC: TStringField;
    qryCC1nCdCC1: TIntegerField;
    qryCC1nCdCC2: TIntegerField;
    qryCC1cCdCC: TStringField;
    qryCC1cCdHie: TStringField;
    qryCC1iNivel: TSmallintField;
    qryCC1cFlgLanc: TIntegerField;
    qryCC1nCdStatus: TIntegerField;
    qryCC2: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    qryCC2nCdCC1: TIntegerField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    SmallintField1: TSmallintField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    qryMastercNmCC1: TStringField;
    qryMastercCdCC1: TStringField;
    qryMastercNmCC2: TStringField;
    qryMastercCdCC2: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    qryMastercNmStatus: TStringField;
    DBEdit9: TDBEdit;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    Label9: TLabel;
    qryAux: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit6Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCentroCusto: TfrmCentroCusto;

implementation

uses fLookup_Padrao, fArvoreCC;

{$R *.dfm}

procedure TfrmCentroCusto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CENTROCUSTO' ;
  nCdTabelaSistema  := 18 ;
  nCdConsultaPadrao := 28 ;
end;

procedure TfrmCentroCusto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmCentroCusto.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmCC.Value = '') then
  begin
      ShowMessage('Informe o nome do centro de custo') ;
      DBEdit2.SetFocus;
      Abort ;
  end ;

  If (qryMasteriNivel.Value < 1) or (qryMasteriNivel.Value > 3) then
  begin
      ShowMessage('N�vel inv�lido. O n�vel deve ser 1, 2 ou 3.') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  If ((qryMasteriNivel.Value = 1) and (qryMasternCdCC1.Value <> 0)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 1, o Centro de Custo Hierarquico 1 deve ficar em branco.') ;
      DBEdit3.SetFocus ;
      Abort ;
  end ;

  If ((qryMasteriNivel.Value = 2) and (qryMasternCdCC1.Value = 0)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 2, o Centro de Custo Hierarquico 1 deve ser preenchido.') ;
      DBEdit3.SetFocus ;
      Abort ;
  end ;

  If ((qryMasteriNivel.Value = 2) and (qryMasternCdCC2.Value > 0)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 2, o Centro de Custo Hierarquico 2 deve ficar em branco.') ;
      DBEdit4.SetFocus ;
      Abort ;
  end ;

  If ((qryMasteriNivel.Value = 3) and (qryMasternCdCC1.Value = 0)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 3, o Centro de Custo Hierarquico 1 deve ser preenchido.') ;
      DBEdit3.SetFocus ;
      Abort ;
  end ;

  If ((qryMasteriNivel.Value = 3) and (qryMasternCdCC2.Value = 0)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 3, o Centro de Custo Hierarquico 2 deve ser preenchido.') ;
      DBEdit4.SetFocus ;
      Abort ;
  end ;

  If ( (qryMasteriNivel.Value = 3) and (qryCC2nCdCC1.Value <> qryCC1nCdCC.Value)) then
  begin
      ShowMessage('A Centro de Custo do N�vel 2 n�o pertence a hierarquia do Nivel 1. Verifique.') ;
      DBEdit4.SetFocus;
      Abort ;
  end ;

  if ((qryMasteriNivel.Value = 1) And (Length(Trim(DBEdit6.Text)) <> 1)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 1, o C�digo Hierarquico deve ter uma posi��o.') ;
      DBEdit6.SetFocus;
      Abort;
  end ;

  if ((qryMasteriNivel.Value = 2) And (Length(Trim(DBEdit6.Text)) <> 3)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 2, o C�digo Hierarquico deve ter tr�s posi��es.') ;
      DBEdit6.SetFocus;
      Abort;
  end ;

  if ((qryMasteriNivel.Value = 3) And (Length(Trim(DBEdit6.Text)) <> 4)) then
  begin
      ShowMessage('Para Centro de Custo do N�vel 3, o C�digo Hierarquico deve ter quatro posi��es.') ;
      DBEdit6.SetFocus;
      Abort;
  end ;

  if (qryMasternCdStatus.Value = 0) then
      qryMasternCdStatus.Value := 1 ;

  if (qryMasteriNivel.Value = 1) then
      qryMastercCdCC.Value := qryMastercCdHie.Value ;

  if (qryMasteriNivel.Value = 2) then
      qryMastercCdCC.Value := Trim(qryMastercCdCC1.Value) + Trim(qryMastercCdHie.Value) ;

  if (qryMasteriNivel.Value = 3) then
      qryMastercCdCC.Value := Trim(qryMastercCdCC2.Value) + Trim(qryMastercCdHie.Value) ;

  if (qryMastercFlgLanc.Value > 0) and (qryMasteriNivel.Value <> 3) then
  begin
      qryMastercFlgLanc.Value := 0 ;
  end ;

  inherited;

end;

procedure TfrmCentroCusto.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(26);

            If (nPK > 0) then
            begin
                qryMasternCdCC1.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmCentroCusto.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdCC1.AsString = '') then
            begin
                MensagemAlerta('Selecione o centro de custo n�vel 1.') ;
                dbEdit3.SetFocus;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(27,'nCdCC1 = ' + qryMasternCdCC1.AsString);

            If (nPK > 0) then
            begin
                qryMasternCdCC2.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCentroCusto.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCentroCusto.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmArvoreCC ;
begin
  inherited;

  objForm := TfrmArvoreCC.Create(Self) ;

  showForm( objForm, TRUE ) ;

end;

procedure TfrmCentroCusto.DBEdit6Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;

      if (qryMasteriNivel.Value < 1) or (qryMasteriNivel.Value > 3) then
      begin
          MensagemAlerta('N�vel inv�lido. Selecione entre 1, 2 e 3.');
          DbEdit5.SetFocus ;
          exit ;
      end ;

      if (qryMasteriNivel.Value = 3) then
      begin
          if (qryMasternCdCC2.AsString = '') then
          begin
              MensagemAlerta('Selecione um centro de custo de n�vel 2') ;
              DbEdit4.SetFocus ;
              exit ;
          end ;

          if (qryMasternCdCC1.AsString = '') then
          begin
              MensagemAlerta('Selecione um centro de custo de n�vel 1') ;
              DbEdit3.SetFocus ;
              exit ;
          end ;

          qryAux.SQL.Add('SELECT RIGHT(REPLICATE(' + Chr(39) + '0' + Chr(39) + ',4) + Convert(varchar,IsNull(Max(cCdHie),0) + 1),4)') ;
          qryAux.SQL.Add('  FROM CentroCusto') ;
          qryAux.SQL.Add(' WHERE iNivel = 3') ;
          qryAux.SQL.Add('   AND nCdCC2 = ' + qryMasternCdCC2.AsString);
          qryAux.SQL.Add('   AND nCdCC1 = ' + qryMasternCdCC1.AsString);

      end ;

      if (qryMasteriNivel.Value = 2) then
      begin

          if (qryMasternCdCC1.AsString = '') then
          begin
              MensagemAlerta('Selecione um centro de custo de n�vel 1') ;
              DbEdit3.SetFocus ;
              exit ;
          end ;

          qryAux.SQL.Add('SELECT RIGHT(REPLICATE(' + Chr(39) + '0' + Chr(39) + ',3) + Convert(varchar,IsNull(Max(cCdHie),0) + 1),3)') ;
          qryAux.SQL.Add('  FROM CentroCusto') ;
          qryAux.SQL.Add(' WHERE iNivel = 2') ;
          qryAux.SQL.Add('   AND nCdCC1 = ' + qryMasternCdCC1.AsString);

      end ;

      if (qryMasteriNivel.Value = 1) then
      begin

          qryAux.SQL.Add('SELECT Convert(varchar,IsNull(Max(cCdHie),0) + 1)') ;
          qryAux.SQL.Add('  FROM CentroCusto') ;
          qryAux.SQL.Add(' WHERE iNivel = 1') ;

      end ;

      qryAux.Open ;

      if not qryAux.eof then
          qryMastercCdHie.Value := qryAux.FieldList[0].Value ;

      qryAux.Close ;

  end ;



end;

initialization
    RegisterClass(tFrmCentroCusto) ;
    
end.
