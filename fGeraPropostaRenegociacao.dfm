inherited frmGeraPropostaRenegociacao: TfrmGeraPropostaRenegociacao
  Left = 524
  Top = 234
  Width = 978
  Height = 584
  BorderIcons = [biSystemMenu]
  Caption = 'Gera'#231#227'o Proposta Renegocia'#231#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 181
    Width = 970
    Height = 376
  end
  inherited ToolBar1: TToolBar
    Width = 970
    ButtonWidth = 96
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 96
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 104
    end
    object ToolButton4: TToolButton
      Left = 200
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 208
      Top = 0
      Caption = 'Gerar Proposta'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 970
    Height = 108
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Tag = 1
      Left = 10
      Top = 24
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja Cobradora'
    end
    object Label5: TLabel
      Tag = 1
      Left = 51
      Top = 48
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente'
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 89
      Top = 16
      Width = 65
      Height = 21
      DataField = 'nCdLoja'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 158
      Top = 16
      Width = 656
      Height = 21
      DataField = 'cNmLoja'
      DataSource = DataSource1
      TabOrder = 3
    end
    object edtCliente: TMaskEdit
      Left = 89
      Top = 40
      Width = 65
      Height = 21
      EditMask = '##################;1; '
      MaxLength = 18
      TabOrder = 0
      Text = '                  '
      OnExit = edtClienteExit
      OnKeyDown = edtClienteKeyDown
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 158
      Top = 40
      Width = 129
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = DataSource2
      TabOrder = 4
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 292
      Top = 40
      Width = 522
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource2
      TabOrder = 5
    end
    object btExibeTitulo: TcxButton
      Left = 88
      Top = 66
      Width = 113
      Height = 29
      Caption = 'Exibir T'#237'tulos'
      TabOrder = 1
      OnClick = btExibeTituloClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object rgFormaReneg: TRadioGroup
      Left = 293
      Top = 64
      Width = 185
      Height = 35
      Caption = ' Forma de Renegocia'#231#227'o '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        #218'nica'
        'M'#250'ltipla')
      TabOrder = 6
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 181
    Width = 970
    Height = 376
    Align = alClient
    Caption = 'T'#237'tulos'
    TabOrder = 2
    object DBGridEh1: TDBGridEh
      Left = 2
      Top = 15
      Width = 966
      Height = 359
      Align = alClient
      DataGrouping.GroupLevels = <>
      DataSource = dsTitulos
      Flat = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -13
      FooterFont.Name = 'Consolas'
      FooterFont.Style = []
      FooterRowCount = 1
      IndicatorOptions = [gioShowRowIndicatorEh]
      ParentFont = False
      SumList.Active = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      OnDrawColumnCell = DBGridEh1DrawColumnCell
      Columns = <
        item
          Checkboxes = True
          EditButtons = <>
          FieldName = 'cFlgAux'
          Footers = <>
          Width = 36
        end
        item
          EditButtons = <>
          FieldName = 'nCdTitulo'
          Footers = <>
          ReadOnly = True
          Visible = False
        end
        item
          EditButtons = <>
          FieldName = 'cNrTit'
          Footers = <>
          ReadOnly = True
          Width = 81
        end
        item
          EditButtons = <>
          FieldName = 'iParcela'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clWhite
          Footer.Font.Height = -13
          Footer.Font.Name = 'Consolas'
          Footer.Font.Style = []
          Footers = <>
          ReadOnly = True
          Width = 43
        end
        item
          EditButtons = <>
          FieldName = 'cNmEspTit'
          Footers = <>
          ReadOnly = True
          Width = 82
        end
        item
          EditButtons = <>
          FieldName = 'dDtEmissao'
          Footers = <>
          ReadOnly = True
          Width = 77
        end
        item
          EditButtons = <>
          FieldName = 'dDtVenc'
          Footers = <>
          ReadOnly = True
          Width = 77
        end
        item
          EditButtons = <>
          FieldName = 'iDiasAtraso'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clWhite
          Footer.Font.Height = -13
          Footer.Font.Name = 'Consolas'
          Footer.Font.Style = []
          Footers = <>
          ReadOnly = True
          Width = 51
        end
        item
          EditButtons = <>
          FieldName = 'nValTit'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
          Width = 78
        end
        item
          EditButtons = <>
          FieldName = 'nValAbatimento'
          Footers = <>
          ReadOnly = True
        end
        item
          EditButtons = <>
          FieldName = 'nValLiq'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
        end
        item
          EditButtons = <>
          FieldName = 'nValJuro'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
          Width = 75
        end
        item
          EditButtons = <>
          FieldName = 'nValMulta'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
          Visible = False
          Width = 59
        end
        item
          EditButtons = <>
          FieldName = 'nValDesconto'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
        end
        item
          EditButtons = <>
          FieldName = 'nSaldoTit'
          Footers = <>
          MRUList.Active = True
          ReadOnly = True
          Width = 90
        end
        item
          EditButtons = <>
          FieldName = 'nCdCobradora'
          Footers = <>
          Title.Caption = 'C'#243'digo da Cobradora'
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 137
    Width = 970
    Height = 44
    Align = alTop
    Caption = 'Legenda'
    TabOrder = 3
    object Label1: TLabel
      Tag = 1
      Left = 40
      Top = 23
      Width = 171
      Height = 13
      Caption = 'T'#237'tulo em processo de renegocia'#231#227'o'
    end
    object cxTextEdit1: TcxTextEdit
      Left = 8
      Top = 15
      Width = 25
      Height = 21
      Style.Color = clRed
      StyleDisabled.Color = clBlue
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 456
    Top = 272
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      00008080800000000000DEBED400000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      000000000000DEBED400DEBED40000000000DEBED40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000999999009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000000000000000000000000000E6E6
      E6000000000000000000DEBED40000000000DEBED400DEBED400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000E6E6
      E60080808000000000000000000000000000DEBED40000000000808080004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E6000000000099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000080
      000000800000E6E6E600E6E6E60099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E6009999990000000000000000004E7EA6000000
      0000000000004E7EA60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFD7F0000
      FE00C003F01F000000008001F19F000000008001C04F000000008001845F0000
      00008001A40F000000008001A307000000008001A001000000008001A0430000
      00008001A001000000008001A043000000018001A067000000038001BF7F0000
      0077C003C0FF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryLojaCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja '
      'WHERE nCdLoja = :nPK')
    Left = 336
    Top = 272
    object qryLojaCobradoranCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojaCobradoracNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLojaCobradora
    Left = 336
    Top = 304
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCNPJCPF'
      '  FROM Terceiro         '
      ' WHERE EXISTS(SELECT 1                   '
      
        '                FROM TerceiroTipoTerceiro TTT                   ' +
        '               '
      
        '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro     ' +
        '                                 '
      '                 AND TTT.nCdTipoTerceiro = 2)  '
      '   AND Terceiro.nCdTerceiro = :nPK')
    Left = 296
    Top = 272
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 296
    Top = 304
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValAbatimento  decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null'
      '                                         ,nCdCobradora int )'
      ''
      'END'
      ''
      ''
      'DECLARE @nCdLoja          int'
      '       ,@nCdTitulo        int'
      '       ,@nCdPropostaReneg int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'TRUNCATE TABLE #TempTituloRenegociacao'
      ''
      'INSERT INTO #TempTituloRenegociacao (nCdTitulo'
      '                                    ,cNrTit'
      '                                    ,iParcela'
      '                                    ,cNmEspTit'
      '                                    ,dDtEmissao'
      '                                    ,dDtVenc'
      '                                    ,iDiasAtraso'
      '                                    ,nValTit'
      '                                    ,nValLiq'
      '                                    ,nValJuro '
      '                                    ,nValMulta'
      '                                    ,nValDesconto'
      '                                    ,nSaldoTit'
      '                                    ,cFlgAux'
      '                                    ,nCdCobradora)'
      #9#9#9#9#9#9#9'SELECT nCdTitulo'
      
        '                                                                ' +
        '                                                                ' +
        '  ,cNrTit'
      #9#9#9#9#9#9#9#9'  ,iParcela'
      #9#9#9#9#9#9#9#9'  ,cNmEspTit'
      #9#9#9#9#9#9#9#9'  ,dDtEmissao'
      #9#9#9#9#9#9#9#9'  ,dDtVenc'
      
        #9#9#9#9#9#9#9#9'  ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN C' +
        'onvert(int,dbo.fn_OnlyDate(GetDate())-dDtVenc)'
      #9#9#9#9#9#9#9#9#9#9'ELSE 0'
      #9#9#9#9#9#9#9#9'   END '
      #9#9#9#9#9#9#9#9'  ,nValTit'
      #9#9#9#9#9#9#9#9'  ,nValLiq'
      
        #9#9#9#9#9#9#9#9'  ,dbo.fn_SimulaJurosTitulo(nCdTitulo,dbo.fn_OnlyDate(Ge' +
        'tDate()))+dbo.fn_SimulaMultaTitulo(nCdTitulo)'
      #9#9#9#9#9#9#9#9'  ,0'
      #9#9#9#9#9#9#9#9'  ,nValDesconto'
      #9#9#9#9#9#9#9#9'  ,nSaldoTit'
      #9#9#9#9#9#9#9#9'  ,1'
      
        '                                                                ' +
        '                                                                ' +
        '  ,nCdCobradora'
      #9#9#9#9#9#9#9'  FROM Titulo'
      
        #9#9#9#9#9#9#9#9'   INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEsp' +
        'Tit'
      
        #9#9#9#9#9#9#9#9'   LEFT  JOIN Loja   ON Loja.nCdLoja     = Titulo.nCdLoj' +
        'aTit'
      #9#9#9#9#9#9#9' WHERE EspTit.cFlgCobranca  = 1   '
      
        #9#9#9#9#9#9#9'   AND ((Loja.nCdLojaCobradora = @nCdLoja) OR (@nCdLoja =' +
        ' 0))'
      #9#9#9#9#9#9#9'   AND Titulo.nCdEmpresa    = :nCdEmpresa'
      #9#9#9#9#9#9#9'   AND Titulo.nCdTerceiro   = :nCdTerceiro'
      #9#9#9#9#9#9#9'   AND Titulo.nSaldoTit     > 0'
      #9#9#9#9#9#9#9'   AND Titulo.cSenso        = '#39'C'#39
      #9#9#9#9#9#9#9'   AND Titulo.dDtCancel    IS NULL'
      
        #9#9#9#9#9#9#9'   AND ( Titulo.cFlgCobradora = 0 OR EXISTS( SELECT TOP 1' +
        ' nCdCobradora FROM Cobradora WHERE nCdCobradora = Titulo.nCdCobr' +
        'adora AND cFlgPermiteLiqLoja = 1 ) )'
      #9#9#9#9#9#9#9' ORDER BY dDtVenc'
      ''
      'DECLARE curTitulos CURSOR '
      '    FOR SELECT nCdTitulo'
      '          FROM #TempTituloRenegociacao Temp'
      ''
      'OPEN curTitulos'
      ''
      'FETCH NEXT'
      ' FROM curTitulos'
      ' INTO @nCdTitulo'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      '    UPDATE #TempTituloRenegociacao'
      '       SET nSaldoTit = nSaldoTit + nValJuro'
      '     WHERE nCdTitulo = @nCdTitulo'
      ''
      '    Set @nCdPropostaReneg = NULL'
      ''
      
        '    SELECT TOP 1 @nCdPropostaReneg = PropostaReneg.nCdPropostaRe' +
        'neg'
      '      FROM TituloPropostaReneg'
      
        '           INNER JOIN PropostaReneg ON PropostaReneg.nCdProposta' +
        'Reneg = TituloPropostaReneg.nCdPropostaReneg '
      '     WHERE TituloPropostaReneg.nCdTitulo            = @nCdTitulo'
      '       AND PropostaReneg.nCdTabStatusPropostaReneg IN (1,2,3)'
      ''
      '    IF (@nCdPropostaReneg IS NOT NULL)'
      '    BEGIN'
      ''
      '        UPDATE #TempTituloRenegociacao'
      '           SET cFlgRenegociado = 1'
      '              ,cFlgAux         = 0'
      '         WHERE #TempTituloRenegociacao.nCdTitulo = @nCdTitulo'
      ''
      '    END'
      ''
      '    FETCH NEXT'
      '     FROM curTitulos'
      '     INTO @nCdTitulo'
      'END'
      ''
      'CLOSE curTitulos'
      'DEALLOCATE curTitulos'
      ''
      '')
    Left = 416
    Top = 305
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValAbatimento  decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null'
      '                                         ,nCdCobradora int )'
      ''
      'END'
      ''
      '')
    Left = 416
    Top = 273
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTitulosAfterScroll
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValAbatimento  decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null'
      '                                         ,nCdCobradora int )'
      ''
      'END'
      ''
      'SELECT nCdTitulo'
      '    ,cNrTit'
      #9'  ,iParcela'
      #9'  ,cNmEspTit'
      #9'  ,dDtEmissao'
      #9'  ,dDtVenc'
      #9'  ,iDiasAtraso'
      #9'  ,nValTit'
      #9'  ,nValLiq'
      #9'  ,nValJuro'
      #9'  ,nValMulta'
      #9'  ,nValDesconto'
      #9'  ,nSaldoTit'
      #9'  ,cFlgAux'
      '    ,cFlgRenegociado'
      '    ,nValAbatimento'
      '    ,nCdCobradora'
      '  FROM #TempTituloRenegociacao'
      ' ORDER BY dDtVenc'
      '')
    Left = 376
    Top = 273
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Pendentes|C'#243'd'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Pendentes|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos Pendentes|Parc.'
      FieldName = 'iParcela'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Pendentes|Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos Pendentes|Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Pendentes|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTitulosiDiasAtraso: TIntegerField
      DisplayLabel = 'T'#237'tulos Pendentes|Atraso'
      FieldName = 'iDiasAtraso'
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Valor Original'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValLiq: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Valor Pago'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValJuro: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Juros'
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValMulta: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Multa'
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValDesconto: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Desconto'
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Saldo em Aberto'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloscFlgAux: TIntegerField
      DisplayLabel = 'T'#237'tulos Pendentes|OK'
      FieldName = 'cFlgAux'
    end
    object qryTituloscFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTitulosnValAbatimento: TBCDField
      DisplayLabel = 'T'#237'tulos Pendentes|Abatimentos'
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTitulosnCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 376
    Top = 305
  end
  object qrySomaTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValAbatimento  decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null'
      '                                         ,nCdCobradora int)'
      ''
      'END'
      ''
      ''
      
        'SELECT Count(1)                                         as iQtde' +
        'Titulos'
      
        '      ,Max(iDiasAtraso)                                 as iMaxD' +
        'iaAtraso'
      
        '      ,Sum(nValTit-nValDesconto-nValLiq-nValAbatimento) as nValT' +
        'otalTit'
      
        '      ,Sum(nValJuro+nValMulta)                          as nValT' +
        'otalJuro'
      
        '      ,Sum(nSaldoTit)                                   as nTota' +
        'lSaldoTit'
      '  FROM #TempTituloRenegociacao'
      ' WHERE cFlgAux = 1')
    Left = 376
    Top = 337
    object qrySomaTitulosiQtdeTitulos: TIntegerField
      FieldName = 'iQtdeTitulos'
      ReadOnly = True
    end
    object qrySomaTitulosiMaxDiaAtraso: TIntegerField
      FieldName = 'iMaxDiaAtraso'
      ReadOnly = True
    end
    object qrySomaTitulosnValTotalTit: TBCDField
      FieldName = 'nValTotalTit'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qrySomaTitulosnValTotalJuro: TBCDField
      FieldName = 'nValTotalJuro'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qrySomaTitulosnTotalSaldoTit: TBCDField
      FieldName = 'nTotalSaldoTit'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object QryTestaCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValAbatimento  decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null'
      '                                         ,nCdCobradora int)'
      ''
      'END'
      ''
      ''
      
        'SELECT Count( DISTINCT nCdCobradora )  + count( DISTINCT case wh' +
        'en nCdCobradora is NULL then 1 end ) as iQuantCobradoras'
      '  FROM #TempTituloRenegociacao'
      ' WHERE cFlgAux = 1'
      ' ')
    Left = 416
    Top = 337
    object QryTestaCobradoraiQuantCobradoras: TIntegerField
      FieldName = 'iQuantCobradoras'
    end
  end
end
