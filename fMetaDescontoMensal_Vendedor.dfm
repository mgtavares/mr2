inherited frmMetaDescontoMensal_vendedor: TfrmMetaDescontoMensal_vendedor
  Left = 256
  Top = 189
  Width = 820
  Height = 469
  Caption = 'Distribui'#231#227'o de Desconto - Por Vendedor'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 804
    Height = 402
  end
  inherited ToolBar1: TToolBar
    Width = 804
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 804
    Height = 402
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 398
    ClientRectLeft = 4
    ClientRectRight = 800
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Vendedores'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 796
        Height = 374
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsMaster
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 3
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTerceiroColab'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Vendedor|C'#243'd.'
            Width = 104
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <
              item
                Alignment = taRightJustify
                Value = 'Total de descontos'
                ValueType = fvtStaticText
              end
              item
                Alignment = taRightJustify
                Value = 'Total de desconto distribuido'
                ValueType = fvtStaticText
              end
              item
                Alignment = taRightJustify
                Value = 'Saldo a distribuir'
                ValueType = fvtStaticText
              end>
            ReadOnly = True
            Title.Caption = 'Vendedor|Nome'
            Width = 493
          end
          item
            EditButtons = <>
            FieldName = 'nValCotaDesconto'
            Footers = <
              item
                FieldName = 'nValCotaDesconto'
                Value = '0,00'
                ValueType = fvtStaticText
              end
              item
                FieldName = 'nValCotaDesconto'
                Value = '0,00'
                ValueType = fvtStaticText
              end
              item
                Value = '0,00'
                ValueType = fvtStaticText
              end>
            Title.Caption = 'Desconto Mensal|Valor'
            Width = 157
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 648
    Top = 240
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMasterBeforePost
    AfterPost = qryMasterAfterPost
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdMetaDescontoMes'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiroResponsavel  int'
      '       ,@nCdUsuario              int'
      '       ,@nCdMetaVendedorDesconto int'
      '       ,@nCdLoja                 int'
      '       ,@nCdMetaDescontoMes      int'
      ''
      'SET'#9'@nCdUsuario         = :nCdUsuario'
      'SET @nCdLoja            = :nCdLoja'
      'SET @nCdMetaDescontoMes = :nCdMetaDescontoMes'
      ''
      'DECLARE curAux CURSOR'
      '    FOR SELECT nCdTerceiroResponsavel'
      '          FROM Usuario'
      '         WHERE EXISTS(SELECT 1'
      '                        FROM UsuarioLoja UL'
      '                       WHERE UL.nCdUsuario = Usuario.nCdUsuario'
      '                         AND UL.nCdLoja    = @nCdLoja)'
      '           AND cFlgVendedor            = 1'
      '           AND nCdStatus               = 1'
      '           AND nCdTerceiroResponsavel IS NOT NULL'
      ''
      'OPEN curAux'
      ''
      'FETCH NEXT'
      ' FROM curAux'
      ' INTO @nCdTerceiroResponsavel'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF ((SELECT TOP 1 1'
      '           FROM MetaVendedorDesconto '
      '          WHERE nCdMetaDescontoMes = @nCdMetaDescontoMes'
      
        '            AND nCdTerceiroColab   = @nCdTerceiroResponsavel) IS' +
        ' NULL)'
      '    BEGIN'
      #9'    '
      #9'    EXEC usp_ProximoID '#39'METADESCONTOVENDEDOR'#39
      #9'                      ,@nCdMetaVendedorDesconto OUTPUT'
      ''
      '        INSERT INTO MetaVendedorDesconto(nCdMetaVendedorDesconto'
      '                                        ,nCdMetaDescontoMes'
      '                                        ,nCdUsuario'
      '                                        ,nCdTerceiroColab'
      '                                        ,nValCotaDesconto'
      '                                        ,dDtCriacao)'
      
        '                                  VALUES(@nCdMetaVendedorDescont' +
        'o'
      '                                        ,@nCdMetaDescontoMes'
      '                                        ,@nCdUsuario'
      '                                        ,@nCdTerceiroResponsavel'
      '                                        ,0'
      '                                        ,GETDATE())'
      ''
      '    END'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdTerceiroResponsavel'
      ''
      'END'
      ''
      'CLOSE curAux'
      'DEALLOCATE curAux'
      ''
      'SELECT MD.nCdMetaVendedorDesconto'
      '      ,MD.nCdMetaDescontoMes'
      '      ,MD.nCdTerceiroColab'
      '      ,MD.nValCotaDesconto'
      '      ,T.cNmTerceiro'
      '  FROM MetaVendedorDesconto MD'
      
        '       INNER JOIN Terceiro T ON T.nCdTerceiro = MD.nCdTerceiroCo' +
        'lab'
      ' WHERE MD.nCdMetaDescontoMes = @nCdMetaDescontoMes'
      ' ORDER BY cNmTerceiro')
    Left = 608
    Top = 204
    object qryMasternCdMetaVendedorDesconto: TIntegerField
      FieldName = 'nCdMetaVendedorDesconto'
    end
    object qryMasternCdMetaDescontoMes: TIntegerField
      FieldName = 'nCdMetaDescontoMes'
    end
    object qryMasternCdTerceiroColab: TIntegerField
      FieldName = 'nCdTerceiroColab'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryMasternValCotaDesconto: TBCDField
      FieldName = 'nValCotaDesconto'
      Precision = 12
      Size = 2
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 608
    Top = 236
  end
  object qryTotalSaldo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT MM.nValMetaDesconto'
      '      ,SUM(MVD.nValCotaDesconto) nDescontoUtilizado'
      '      ,(MM.nValMetaDesconto - SUM(MVD.nValCotaDesconto)) nSaldo'
      '  FROM MetaDescontoMes MM'
      
        ' INNER JOIN MetaVendedorDesconto MVD ON MVD.nCdMetaDescontoMes =' +
        ' MM.nCdMetaDescontoMes'
      ' WHERE MM.nCdMetaDescontoMes = :nPK'
      ' GROUP BY MM.nValMetaDesconto')
    Left = 648
    Top = 204
    object qryTotalSaldonValMetaDesconto: TBCDField
      FieldName = 'nValMetaDesconto'
      Precision = 12
      Size = 2
    end
    object qryTotalSaldonDescontoUtilizado: TBCDField
      FieldName = 'nDescontoUtilizado'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryTotalSaldonSaldo: TBCDField
      FieldName = 'nSaldo'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
end
