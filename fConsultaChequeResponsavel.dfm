inherited frmConsultaChequeResponsavel: TfrmConsultaChequeResponsavel
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Consulta de Cheque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 158
    Width = 1152
    Height = 656
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 129
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 203
      Top = 24
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Respons'#225'vel'
    end
    object Label2: TLabel
      Tag = 1
      Left = 219
      Top = 48
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'CNPJ/CPF Emissor'
    end
    object Label7: TLabel
      Tag = 1
      Left = 241
      Top = 72
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Cheque'
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 16
      Width = 183
      Height = 105
      Caption = 'Filtro'
      ItemIndex = 0
      Items.Strings = (
        'Pendente de Dep'#243'sito'
        'Enviado para Banco'
        'Enviado para Terceiro'
        'Devolvido')
      TabOrder = 0
    end
    object edtTerceiro: TMaskEdit
      Left = 312
      Top = 16
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtTerceiroExit
      OnKeyDown = edtTerceiroKeyDown
    end
    object edtCNPJEmissor: TMaskEdit
      Left = 312
      Top = 40
      Width = 96
      Height = 21
      MaxLength = 14
      TabOrder = 2
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 384
      Top = 16
      Width = 128
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 520
      Top = 16
      Width = 536
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 4
    end
    object edtNumCheque: TMaskEdit
      Left = 312
      Top = 64
      Width = 96
      Height = 21
      EditMask = '!######;0; '
      MaxLength = 6
      TabOrder = 5
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 158
    Width = 1152
    Height = 656
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 652
    ClientRectLeft = 4
    ClientRectRight = 1148
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Resultado da Consulta'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1144
        Height = 628
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsResultado
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cConta'
            Footers = <>
            Width = 173
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cCNPJCPF'
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'nValCheque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtDeposito'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiroPort'
            Footers = <>
            Width = 155
          end
          item
            EditButtons = <>
            FieldName = 'dDtRemessaPort'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtDevol'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 992
    Top = 272
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 808
    Top = 112
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 896
    Top = 112
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 680
    Top = 120
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cCNPJCPF'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iNrCheque'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iTipoSel'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiroResp'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'DECLARE @cCNPJCPF  varchar(15)'
      '       ,@iNrCheque int'
      '       ,@iTipoSel  int'
      ''
      'Set @cCNPJCPF  = :cCNPJCPF'
      'Set @iNrCheque = :iNrCheque'
      'Set @iTipoSel  = :iTipoSel'
      ''
      
        'SELECT dbo.fn_ZeroEsquerda(Cheque.nCdBanco,3) + '#39' '#39' + dbo.fn_Zer' +
        'oEsquerda(Cheque.cAgencia,4) + '#39' '#39' + dbo.fn_ZeroEsquerda(Cheque.' +
        'cConta,15) + '#39'-'#39' + Cheque.cDigito as cConta'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.cCNPJCPF'
      '      ,Cheque.nValCheque'
      '      ,dbo.fn_OnlyDate(Cheque.dDtDeposito) dDtDeposito'
      
        '      ,CASE WHEN Terc2.cNmTerceiro IS NULL THEN dbo.fn_ZeroEsque' +
        'rda(ContaBancaria.nCdBanco,3) + '#39'-'#39' + dbo.fn_ZeroEsquerda(ContaB' +
        'ancaria.cAgencia,4) + '#39' - '#39' + ContaBancaria.nCdConta'
      '            ELSE Terc2.cNmTerceiro'
      '       END cNmTerceiroPort'
      '      ,dbo.fn_OnlyDate(Cheque.dDtRemessaPort) dDtRemessaPort'
      '      ,dbo.fn_OnlyDate(Cheque.dDtDevol) dDtDevol'
      '  FROM Cheque'
      
        '       LEFT  JOIN Terceiro Terc2 ON Terc2.nCdTerceiro           ' +
        '   = Cheque.nCdTerceiroPort'
      
        '       LEFT  JOIN ContaBancaria  ON ContaBancaria.nCdContaBancar' +
        'ia = Cheque.nCdContaBancariaDep'
      ' WHERE nCdTerceiroResp = :nCdTerceiroResp'
      '   AND ((Cheque.cCNPJCPF  = @cCNPJCPF)  OR (@cCNPJCPF = '#39#39'))'
      '   AND ((Cheque.iNrCheque = @iNrCheque) OR (@iNrCheque = 0))'
      
        '   AND ((@iTipoSel = 0 AND Cheque.nCdContaBancariaDep IS NULL AN' +
        'D Cheque.nCdTerceiroPort IS NULL) OR (@iTipoSel <> 0))'
      
        '   AND ((@iTipoSel = 1 AND Cheque.nCdContaBancariaDep IS NOT NUL' +
        'L)                                OR (@iTipoSel <> 1))'
      
        '   AND ((@iTipoSel = 2 AND Cheque.nCdTerceiroPort IS NOT NULL)  ' +
        '                                  OR (@iTipoSel <> 2))'
      
        '   AND ((@iTipoSel = 3 AND Cheque.dDtDevol IS NOT NULL)         ' +
        '                                  OR (@iTipoSel <> 3))'
      '   AND nCdTabTipoCheque = 2')
    Left = 576
    Top = 120
    object qryResultadocConta: TStringField
      DisplayLabel = 'Dados Banc'#225'rios Cheque'
      FieldName = 'cConta'
      ReadOnly = True
      Size = 49
    end
    object qryResultadoiNrCheque: TIntegerField
      DisplayLabel = 'Nr Cheque'
      FieldName = 'iNrCheque'
    end
    object qryResultadocCNPJCPF: TStringField
      DisplayLabel = 'CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryResultadonValCheque: TBCDField
      DisplayLabel = 'Valor Cheque'
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryResultadodDtDeposito: TDateTimeField
      DisplayLabel = 'Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
      ReadOnly = True
    end
    object qryResultadocNmTerceiroPort: TStringField
      DisplayLabel = 'Terceiro Portador'
      FieldName = 'cNmTerceiroPort'
      ReadOnly = True
      Size = 50
    end
    object qryResultadodDtRemessaPort: TDateTimeField
      DisplayLabel = 'Data Remessa'
      FieldName = 'dDtRemessaPort'
      ReadOnly = True
    end
    object qryResultadodDtDevol: TDateTimeField
      DisplayLabel = 'Data Devolu'#231#227'o'
      FieldName = 'dDtDevol'
      ReadOnly = True
    end
  end
end
