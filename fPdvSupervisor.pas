unit fPdvSupervisor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, cxTextEdit, StdCtrls, cxControls, cxContainer,
  cxEdit, cxCurrencyEdit, jpeg, ExtCtrls;

type
  TfrmPdvSupervisor = class(TForm)
    Image1: TImage;
    Image2: TImage;
    edtCodigo: TcxCurrencyEdit;
    Label2: TLabel;
    Label1: TLabel;
    edtSenha: TcxTextEdit;
    qryConfereUsuario: TADOQuery;
    qryConfereUsuarionPercMaxDesc: TBCDField;
    qryConfereUsuariocNmUsuario: TStringField;
    procedure edtSenhaExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoKeyPress(Sender: TObject; var Key: Char);
    procedure edtSenhaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLoja : integer ;
    cResultado : boolean ;    
  end;

var
  frmPdvSupervisor: TfrmPdvSupervisor;

implementation

uses fMenu, fPdv;

{$R *.dfm}

procedure TfrmPdvSupervisor.edtSenhaExit(Sender: TObject);
begin

  cResultado := False ;

  if (Trim(edtSenha.Text) <> '') then
  begin

      qryConfereUsuario.Close ;
      qryConfereUsuario.Parameters.ParamByName('nCdUsuario').Value := edtCodigo.Value ;
      qryConfereUsuario.Parameters.ParamByName('cSenha').Value     := frmMenu.Cripto(UpperCase(EdtSenha.Text)) ;
      qryConfereUsuario.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
      qryConfereUsuario.Open ;

      if (qryConfereUsuario.Eof) then
      begin
          frmPDV.MensagemAlerta('Gerente/Senha inv�lido.') ;
          edtSenha.Text := '';
          exit ;
      end ;

      cResultado := True ;

      close ;

  end
  else
      Close ;


end;

procedure TfrmPdvSupervisor.FormShow(Sender: TObject);
begin

    edtCodigo.Value := 0  ;
    edtSenha.Text   := '' ;

    edtCodigo.SetFocus ;

end;

procedure TfrmPdvSupervisor.edtCodigoKeyPress(Sender: TObject; var Key: Char);
begin

    if (key=#13) then
        edtSenha.SetFocus;

end;

procedure TfrmPdvSupervisor.edtSenhaKeyPress(Sender: TObject; var Key: Char);
begin

    if (key=#13) then
        edtCodigo.SetFocus;

end;

end.
