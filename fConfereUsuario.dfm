inherited frmSenhaAcesso: TfrmSenhaAcesso
  Left = 499
  Top = 337
  Width = 211
  Height = 142
  BorderIcons = [biSystemMenu]
  Caption = 'Senha para Acesso'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 195
    Height = 77
  end
  object Label1: TLabel [1]
    Left = 13
    Top = 48
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Login'
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 80
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Senha'
  end
  inherited ToolBar1: TToolBar
    Width = 195
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object Edit1: TEdit [4]
    Left = 48
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Edit2: TEdit [5]
    Left = 48
    Top = 72
    Width = 121
    Height = 21
    PasswordChar = '#'
    TabOrder = 2
    OnExit = Edit2Exit
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmLogin'
        DataType = ftString
        Precision = 20
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cFlgAtivo'
      '      ,cSenha'
      '  FROM Usuario'
      ' WHERE cNmLogin = :cNmLogin')
    Left = 64
    Top = 48
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocFlgAtivo: TSmallintField
      FieldName = 'cFlgAtivo'
    end
    object qryUsuariocSenha: TStringField
      FieldName = 'cSenha'
    end
  end
end
