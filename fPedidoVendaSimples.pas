unit fPedidoVendaSimples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit, Menus,
  DBGridEhGrouping, ER2Lookup, ToolCtrlsEh, fMontaGrade;

type
  TfrmPedidoVendaSimples = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DBEdit14: TDBEdit;
    DataSource3: TDataSource;
    DBEdit15: TDBEdit;
    DataSource4: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DataSource5: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    DataSource8: TDataSource;
    DataSource9: TDataSource;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    TabAD: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    DataSource11: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    cxButton1: TcxButton;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    btSugParcela: TcxButton;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    bFinalizar: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    DBGridEh4: TDBGridEh;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnQtdeExpRec: TBCDField;
    qryItemADnQtdeCanc: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnCdPedido: TIntegerField;
    qryItemADnCdTipoItemPed: TIntegerField;
    qryItemADnCdItemPedido: TAutoIncField;
    dsItemAD: TDataSource;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    ToolButton12: TToolButton;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    DataSource12: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    TabFollowUp: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    DBMemo1: TDBMemo;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    DataSource13: TDataSource;
    TabFormula: TcxTabSheet;
    qryItemFormula: TADOQuery;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanQtdeExpRec: TBCDField;
    qryItemFormulanQtdeCanc: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanCdPedido: TIntegerField;
    qryItemFormulanCdTipoItemPed: TIntegerField;
    qryItemFormulanCdItemPedido: TAutoIncField;
    qryItemFormulacSiglaUnidadeMedida: TStringField;
    dsItemFormula: TDataSource;
    DBGridEh6: TDBGridEh;
    Label33: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label34: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label35: TLabel;
    cxTextEdit9: TcxTextEdit;
    qryItemFormulanCdProduto: TIntegerField;
    qryProdutoFormulado: TADOQuery;
    qryProdutoFormuladonCdProduto: TIntegerField;
    qryProdutoFormuladocNmProduto: TStringField;
    usp_gera_item_embalagem: TADOStoredProc;
    cxButton2: TcxButton;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    cxTabSheet1: TcxTabSheet;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    DBEdit40: TDBEdit;
    Image2: TImage;
    qryMastercOBSFinanc: TMemoField;
    Label37: TLabel;
    DBMemo2: TDBMemo;
    Label38: TLabel;
    DBMemo3: TDBMemo;
    cxTabSheet2: TcxTabSheet;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Label9: TLabel;
    Image4: TImage;
    ToolButton13: TToolButton;
    usp_valida_credito: TADOStoredProc;
    sp_tab_preco: TADOStoredProc;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit13: TDBEdit;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    Label14: TLabel;
    DBEdit20: TDBEdit;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostonCdGrupoImposto: TIntegerField;
    qryGrupoImpostocNmGrupoImposto: TStringField;
    qryItemADnCdGrupoImposto: TIntegerField;
    qryItemADcNmGrupoImposto: TStringField;
    Label15: TLabel;
    DBEdit21: TDBEdit;
    DBEdit23: TDBEdit;
    TabDesTecnica: TcxTabSheet;
    DBGridEh7: TDBGridEh;
    qryDescrTecnica: TADOQuery;
    qryDescrTecnicanCdItemPedido: TAutoIncField;
    qryDescrTecnicanCdProduto: TIntegerField;
    qryDescrTecnicacNmItem: TStringField;
    qryDescrTecnicanQtdePed: TBCDField;
    qryDescrTecnicacDescricaoTecnica: TMemoField;
    DBMemo4: TDBMemo;
    DataSource14: TDataSource;
    cxTabSheet3: TcxTabSheet;
    qryMastercAnotacao: TMemoField;
    DBMemo5: TDBMemo;
    qryMastercFlgOrcamento: TIntegerField;
    SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc;
    qryMasternValAdiantamento: TBCDField;
    qryMastercFlgAdiantConfirm: TIntegerField;
    qryMasternCdUsuarioConfirmAdiant: TIntegerField;
    Label16: TLabel;
    DBEdit24: TDBEdit;
    PopupMenu1: TPopupMenu;
    ExibirAtendimentosdoItem1: TMenuItem;
    Label17: TLabel;
    DBEdit25: TDBEdit;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    DBEdit26: TDBEdit;
    DataSource15: TDataSource;
    qryItemFormulanValUnitario: TBCDField;
    qryItemFormulanValDesconto: TBCDField;
    qryItemFormulanValAcrescimo: TBCDField;
    Label18: TLabel;
    DBEdit27: TDBEdit;
    Label19: TLabel;
    DBEdit28: TDBEdit;
    Label20: TLabel;
    DBEdit29: TDBEdit;
    Label12: TLabel;
    DBEdit38: TDBEdit;
    DBEdit22: TDBEdit;
    Label21: TLabel;
    qryTipoPedidocLimiteCredito: TIntegerField;
    qryPrecoAux: TADOQuery;
    qryPrecoAuxnPrecoAux: TBCDField;
    qryItemEstoquenValUnitarioEsp: TBCDField;
    qryItemFormulanValUnitarioEsp: TBCDField;
    qryTerceirocFlgTipoFat: TIntegerField;
    qryItemADnValUnitarioEsp: TBCDField;
    btContatoPadrao: TcxButton;
    qryContatoPadrao: TADOQuery;
    qryContatoPadraocNmContato: TStringField;
    qryMasternValDevoluc: TBCDField;
    qryTipoPedidocFlgPermAbatDevVenda: TIntegerField;
    DBEdit31: TDBEdit;
    Label25: TLabel;
    qryItemEstoquenValDescAbatUnit: TBCDField;
    qryItemFormulanValDescAbatUnit: TBCDField;
    qryMasternCdEnderecoEntrega: TIntegerField;
    qryEnderecoEntrega: TADOQuery;
    qryEnderecoEntreganCdEndereco: TAutoIncField;
    qryEnderecoEntregacNmstatus: TStringField;
    qryEnderecoEntregacNmTipoEnd: TStringField;
    qryEnderecoEntregacEnderecoCompleto: TStringField;
    Label27: TLabel;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DataSource16: TDataSource;
    qrySugereEnderecoEntrega: TADOQuery;
    qrySugereEnderecoEntreganCdEndereco: TIntegerField;
    qryItemEstoquecCdProduto: TStringField;
    uspBuscaProduto: TADOStoredProc;
    uspBuscaProdutonCdProduto: TIntegerField;
    uspBuscaProdutocNmProduto: TStringField;
    btDescontoTotalPedido: TcxButton;
    btSituacaoEstoque: TcxButton;
    qryUsuario: TADOQuery;
    qryUsuarionCdTerceiroResponsavel: TIntegerField;
    qryUsuariocFlgRepresentante: TIntegerField;
    btAcompanharProducao: TcxButton;
    qryTipoPedidocFlgGeraOP: TIntegerField;
    Label51: TLabel;
    edtAreaVenda: TER2LookupDBEdit;
    DBEdit63: TDBEdit;
    Label52: TLabel;
    edtDivisaoVenda: TER2LookupDBEdit;
    DBEdit64: TDBEdit;
    Label53: TLabel;
    edtCanalDistribuicao: TER2LookupDBEdit;
    DBEdit65: TDBEdit;
    qryMasternCdAreaVenda: TIntegerField;
    qryMasternCdDivisaoVenda: TIntegerField;
    qryMasternCdCanalDistribuicao: TIntegerField;
    qryAreaVenda: TADOQuery;
    qryAreaVendanCdAreaVenda: TIntegerField;
    qryAreaVendacNmAreaVenda: TStringField;
    qryDivisaoVenda: TADOQuery;
    qryDivisaoVendanCdDivisaoVenda: TIntegerField;
    qryDivisaoVendacNmDivisaoVenda: TStringField;
    qryCanalDistribuicao: TADOQuery;
    qryCanalDistribuicaonCdCanalDistribuicao: TIntegerField;
    qryCanalDistribuicaocNmCanalDistribuicao: TStringField;
    dsCanalDistribuicao: TDataSource;
    dsDivisaoVenda: TDataSource;
    dsAreaVenda: TDataSource;
    qryTerceironCdAreaVenda: TIntegerField;
    qryTerceironCdDivisaoVenda: TIntegerField;
    qryTerceironCdCanalDistribuicao: TIntegerField;
    qryTerceiroAreaVenda: TADOQuery;
    qryTerceiroAreaVendanCdAreaVenda: TIntegerField;
    qryTerceiroAreaVendanCdDivisaoVenda: TIntegerField;
    qryTerceiroAreaVendanCdCanalDistribuicao: TIntegerField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItensPedido: TADOQuery;
    qryTipoPedidocFlgCalcComissao: TIntegerField;
    qryTerceironCdTipoTabPreco: TIntegerField;
    qryItensPedidoiAux: TIntegerField;
    Label13: TLabel;
    DBEdit41: TDBEdit;
    DBEdit39: TDBEdit;
    cxTabSheet4: TcxTabSheet;
    DBGridEh8: TDBGridEh;
    qryRestricoes: TADOQuery;
    dsRestricoes: TDataSource;
    qryRestricoesnCdRestricaoVendaTerceiro: TIntegerField;
    qryRestricoescNmTabTipoRestricaoVenda: TStringField;
    qryRestricoesnCdPedido: TIntegerField;
    qryRestricoescPendenteLiberacao: TStringField;
    qryGrupoCredito: TADOQuery;
    qryGrupoCreditonCdGrupoCredito: TIntegerField;
    qryGrupoCreditocNmGrupoCredito: TStringField;
    DataSource17: TDataSource;
    qryTerceiroPagadornCdGrupoCredito: TIntegerField;
    qryTerceironCdGrupoCredito: TIntegerField;
    qryTituloAtraso: TADOQuery;
    qryChequeRisco: TADOQuery;
    qrySaldoLimite: TADOQuery;
    qrySaldoLimitenSaldoLimite: TBCDField;
    SP_GERA_DIVERG_PED: TADOStoredProc;
    qryChequeDevolv: TADOQuery;
    qryGrupoCreditocFlgPedidosAnaliseFin: TIntegerField;
    PopupMenu2: TPopupMenu;
    DuplicarPedido1: TMenuItem;
    SP_DUPLICA_PEDIDO: TADOStoredProc;
    CancelarPedido1: TMenuItem;
    cxPageControl2: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    qryPedidoPromotor: TADOQuery;
    qryPedidoPromotornCdPedidoPromotor: TIntegerField;
    qryPedidoPromotornCdPedido: TIntegerField;
    qryPedidoPromotornCdTerceiroPromotor: TIntegerField;
    qryPedidoPromotorcNmTerceiro: TStringField;
    DBGridEh9: TDBGridEh;
    dsPedidoPromotor: TDataSource;
    qryTerceiroPromotor: TADOQuery;
    qryTerceiroPromotorcNmTerceiro: TStringField;
    qryExcluirItens: TADOQuery;
    lblHelp: TLabel;
    N1: TMenuItem;
    btTerceiro: TMenuItem;
    btTipodePedido: TMenuItem;
    btCondPagto: TMenuItem;
    btTabelaPreco: TMenuItem;
    N2: TMenuItem;
    btProdERP: TMenuItem;
    N3: TMenuItem;
    btEmitirPicking: TMenuItem;
    btSeparaoEmb: TMenuItem;
    qrySumTotalPed: TADOQuery;
    qrySumTotalPednValTotalDescPed: TBCDField;
    qryAplicaDesconto: TADOQuery;
    qryTerceirocOBS: TMemoField;
    qryTerceiroRepresentante: TADOQuery;
    qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField;
    qryVerificaQtdRepres: TADOQuery;
    qryVerificaQtdRepresiQtdRepres: TIntegerField;
    qryTerceiroRepresUnico: TADOQuery;
    qryTerceiroRepresUniconCdTerceiro: TIntegerField;
    qryTerceiroRepresUnicocNmTerceiro: TStringField;
    qryTipoPedidocFlgExigeRepres: TIntegerField;
    ToolButton10: TToolButton;
    ToolButton14: TToolButton;
    qryInseriItemGrade: TADOQuery;
    DBEdit30: TDBEdit;
    Label22: TLabel;
    DBEdit36: TDBEdit;
    dsTabTipoModFrete: TDataSource;
    qryTabTipoModFrete: TADOQuery;
    qryTabTipoModFretenCdTabTipoModFrete: TIntegerField;
    qryTabTipoModFretecNmTabTipoModFrete: TStringField;
    DBEdit42: TDBEdit;
    qryMasternCdTabTipoModFrete: TIntegerField;
    Label28: TLabel;
    DBEdit43: TDBEdit;
    qryItemEstoquenValFrete: TBCDField;
    Label39: TLabel;
    DBEdit44: TDBEdit;
    qryItemEstoquenCdItemTransfEst: TIntegerField;
    qryVerificaTransfEst: TADOQuery;
    qryProdutonValVenda: TBCDField;
    qryProdutonValCusto: TBCDField;
    qryItemEstoquenValCMV: TBCDField;
    btProdGrade: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure bFinalizarClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADBeforePost(DataSet: TDataSet);
    procedure qryItemADAfterPost(DataSet: TDataSet);
    procedure qryItemADBeforeDelete(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaBeforePost(DataSet: TDataSet);
    procedure qryItemFormulaAfterPost(DataSet: TDataSet);
    procedure qryItemFormulaBeforeDelete(DataSet: TDataSet);
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure DBGridEh6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh6ColExit(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure btDescontoTotalPedidoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemADCalcFields(DataSet: TDataSet);
    procedure DBMemo4Exit(Sender: TObject);
    procedure TabDesTecnicaEnter(Sender: TObject);
    procedure ExibirAtendimentosdoItem1Click(Sender: TObject);
    procedure DBEdit25Exit(Sender: TObject);
    procedure DBEdit11Enter(Sender: TObject);
    procedure btContatoPadraoClick(Sender: TObject);
    procedure qryTipoPedidoAfterScroll(DataSet: TDataSet);
    procedure DBEdit32Exit(Sender: TObject);
    procedure DBEdit32Enter(Sender: TObject);
    procedure DBEdit32KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CalculaDescontoTotalPedido(nPercDesconto, nValDesconto, nValLiquido : double);
    procedure btSituacaoEstoqueClick(Sender: TObject);
    procedure btAcompanharProducaoClick(Sender: TObject);
    procedure qryTipoPedidoAfterClose(DataSet: TDataSet);
    procedure edtDivisaoVendaBeforePosicionaQry(Sender: TObject);
    procedure edtDivisaoVendaBeforeLookup(Sender: TObject);
    procedure DuplicarPedido1Click(Sender: TObject);
    procedure CancelarPedido1Click(Sender: TObject);
    procedure qryPedidoPromotorCalcFields(DataSet: TDataSet);
    procedure qryPedidoPromotornCdTerceiroPromotorChange(Sender: TField);
    procedure qryPedidoPromotorBeforePost(DataSet: TDataSet);
    procedure DBGridEh9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure incluirProdutoMultiplo;
    procedure PopupMenu2Popup(Sender: TObject);
    procedure btTerceiroClick(Sender: TObject);
    procedure btTipodePedidoClick(Sender: TObject);
    procedure btCondPagtoClick(Sender: TObject);
    procedure btTabelaPrecoClick(Sender: TObject);
    procedure btProdERPClick(Sender: TObject);
    procedure btEmitirPickingClick(Sender: TObject);
    procedure btSeparaoEmbClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBEdit30Exit(Sender: TObject);
    procedure DBEdit30KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit43KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit43Exit(Sender: TObject);
    procedure btProdGradeClick(Sender: TObject);

  private
    { Private declarations }
    bPosicionaItens  : boolean;
    bRepresentante   : boolean ;
    bPermiteDupProd  : boolean ;
    nCdTerceiro      : integer;
    nCdGrupoCredito  : integer;
    bInserePed       : Boolean;
    function ArredondaDecimal(Value: extended; Decimals: integer): extended;
  public
    { Public declarations }
  end;

var
  frmPedidoVendaSimples : TfrmPedidoVendaSimples;

implementation

uses
  Math, fMenu, fLookup_Padrao, fEmbFormula, fPrecoEspPedCom, rPedidoVenda,
  fItemPedidoAtendido, fPedidoVendaSimplesDesconto, fSituacaoEstoqueProduto,
  fAcompProducaoPedidoVenda, fMotivoCancelaSaldoPedido, fPedidoVendaSimples_SelecaoAgrupada,
  fTerceiro, fTipoPedido, fCondPagto, fTabPreco, fProdutoERP, fGerarPickingList,
  fSeparaEmbalaPedido, fProduto;

{$R *.dfm}


procedure TfrmPedidoVendaSimples.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedidoVendaSimples.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 96 ;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmPedidoVendaSimples.btIncluirClick(Sender: TObject);
begin
  inherited;

  btAcompanharProducao.Visible := False ;

  cxPageControl1.ActivePageIndex := 0 ;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      MensagemAlerta('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
      DBEdit4.OnExit(nil) ;
  end ;

  qryUsuarioTipoPedido.Close ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          MensagemAlerta('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;

  end;

  if (qryMasternCdTipoPedido.Value = 0) then
  begin
      if (not DBEdit30.ReadOnly) then
          DBEdit30.SetFocus
      else
          DbEdit4.SetFocus;
  end
  else
      DBEdit4.SetFocus;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly       := False ;
//DBGridEh3.ReadOnly       := False ;
  btSugParcela.Enabled     := True ;
  btSalvar.Enabled         := True ;
  bFinalizar.Enabled       := True ;
  qryMasterdDtPedido.Value := StrToDate(DateToStr(Now())) ;
  DBEdit24.Enabled         := True ;

  if (bRepresentante) then
  begin
      qryTerceiroAreaVenda.Close;
      PosicionaQuery(qryTerceiroAreaVenda, IntToStr(frmMenu.nCdTerceiroUsuario));

      qryMasternCdAreaVenda.AsString         := qryTerceiroAreaVendanCdAreaVenda.AsString;
      qryMasternCdDivisaoVenda.AsString      := qryTerceiroAreaVendanCdDivisaoVenda.AsString;

      edtAreaVenda.PosicionaQuery;
      edtDivisaoVenda.PosicionaQuery;
  end;

end;

procedure TfrmPedidoVendaSimples.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmPedidoVendaSimples.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(60,'(nCdTabTipoPedido = 1 OR cFlgDevolucVenda = 1)');

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                MensagemAlerta('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            if (bRepresentante) then
                nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro) AND EXISTS(SELECT 1 FROM TerceiroRepresentante WHERE nCdTerceiroRepres = ' + intToStr(frmMenu.nCdTerceiroUsuario) + ' AND nCdTerceiro = vTerceiros.nCdTerceiro)')
            else
                nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
    { -- atualiza terceiro pagador -- }
    if (qryTerceironCdTerceiroPagador.Value <> qryMasternCdTerceiroPagador.Value) then
    begin
        qryTerceiroPagador.Close;
        qryMasternCdTerceiroPagador.Clear;

        if (qryTerceironCdTerceiroPagador.Value > 0) then
        begin
            PosicionaQuery(qryTerceiroPagador,qryTerceironCdTerceiroPagador.AsString);
            qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value;
        end;
    end;

    { -- atualiza endere�o de entrega -- }
    if ((qryMaster.state = dsInsert) or (qryMaster.state = dsEdit)) then
    begin

        qrySugereEnderecoEntrega.Close;
        qrySugereEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
        qrySugereEnderecoEntrega.Open ;

        if not qrySugereEnderecoEntrega.Eof then
        begin
            qryMasternCdEnderecoEntrega.Value := qrySugereEnderecoEntreganCdEndereco.Value ;
            qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
            PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;
        end ;
    end ;
  end;
end;


procedure TfrmPedidoVendaSimples.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if (bRepresentante) then
                nPK := frmLookup_Padrao.ExecutaConsulta2(103, 'nCdTerceiro = ' + IntToStr(frmMenu.nCdTerceiroUsuario))
            else
                nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroRepres.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active = False) then
      Exit;

  // Se o terceiro informado n�o mudou, n�o executa nenhum procedimento
  if not (qryTerceiro.IsEmpty) then
      if (StrToIntDef(Trim(DBEdit5.Text),0) = qryTerceironCdTerceiro.Value) then
          Exit;

  // Se n�o foi informado o Tipo de Pedido
  if (qryTipoPedido.IsEmpty) then
      Exit;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTerceiroTransp.asString  := '' ;
      qryMasternCdIncoterms.asString       := '' ;
      qryMasternCdCondPagto.asString       := '' ;
      qryMasternCdTerceiroRepres.asString  := '' ;
      qryMasternCdTerceiroPagador.asString := '' ;
      nCdTerceiro                          := 0;
      nCdGrupoCredito                      := 0;

      qryTerceiroTransp.Close;
      qryIncoterms.Close;
      qryCondPagto.Close;
      qryTerceiroRepres.Close;
      qryTerceiroPagador.Close;

  end ;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  // Se n�o foi encontrado nenhum terceiro
  if (qryTerceiro.IsEmpty) then
      Exit ;

  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
  begin

      If (frmMenu.LeParametro('OBRTPTABTERPDV') = 'S') and (qryTipoPedidocFlgVenda.Value = 1) then
      begin

          if (not qryTerceiro.eof) and (qryTerceironCdTipoTabPreco.Value = 0) then
          begin

              MensagemAlerta('Terceiro sem tipo de tabela de pre�o. Imposs�vel continuar.') ;
              qryTerceiro.Close;
              DbEdit5.SetFocus ;
              exit ;

          end;

      end;

  end;

  if (qryMaster.State = dsInsert) then
  begin
      if (frmMenu.LeParametro('CARREGAREPRPED') = 'S') then
      begin
          if (bRepresentante) then
          begin
              { verifica se representante est� vinculado ao terceiro do pedido }
              qryTerceiroRepresentante.Close;
              qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.nCdTerceiroUsuario;
              qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiro').Value       := qryTerceironCdTerceiro.Value;
              qryTerceiroRepresentante.Open;

              if (qryTerceiroRepresentante.IsEmpty) then
              begin
                  MensagemAlerta('Representante n�o pertence ao Terceiro do pedido.');
                  qryTerceiro.Close;
                  DbEdit5.Text := '';
                  DbEdit5.SetFocus;
                  Abort;
              end;

              if (qryTerceiroRepresentantenCdTerceiroRepres.Value > 0) then
                  qryMasternCdTerceiroRepres.Value := frmMenu.nCdTerceiroUsuario;
          end
          else
          begin
              { verifica se terceiro do pedido cont�m apenas um representante }
              qryVerificaQtdRepres.Close;
              qryVerificaQtdRepres.Parameters.ParamByName('nPK').Value := qryTerceironCdTerceiro.Value;
              qryVerificaQtdRepres.Open;

              { se houver um �nico representante vincula o mesmo ao pedido }
              if (qryVerificaQtdRepresiQtdRepres.Value = 1) then
              begin
                  PosicionaQuery(qryTerceiroRepresUnico, qryTerceironCdTerceiro.AsString);
                  qryMasternCdTerceiroRepres.Value := qryTerceiroRepresUniconCdTerceiro.Value;
              end;
          end;

          PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.AsString);
      end;

      if (not bRepresentante) then
      begin
          qryTerceiroAreaVenda.Close;
          PosicionaQuery(qryTerceiroAreaVenda, qryTerceironCdTerceiro.AsString);

          qryMasternCdAreaVenda.AsString    := qryTerceiroAreaVendanCdAreaVenda.AsString;
          qryMasternCdDivisaoVenda.AsString := qryTerceiroAreaVendanCdDivisaoVenda.AsString;

          edtAreaVenda.PosicionaQuery;
          edtDivisaoVenda.PosicionaQuery;
      end;

      {-- Sempre o Canal de distribui��o ser� o do Cliente,
       -- independente se o usu�rio for um Representante --}

      qryTerceiroAreaVenda.Close;
      PosicionaQuery(qryTerceiroAreaVenda, qryTerceironCdTerceiro.AsString);

      qryMasternCdCanalDistribuicao.AsString := qryTerceiroAreaVendanCdCanalDistribuicao.AsString;
      edtCanalDistribuicao.PosicionaQuery;

      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

      qryMasternPercDesconto.Value  := qryTerceironPercDesconto.Value ;
      qryMasternPercAcrescimo.Value := qryTerceironPercAcrescimo.Value ;
      qryMastercOBS.Value           := qryTerceirocOBS.Value;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      if ((qryTipoPedidocGerarFinanc.Value = 1) and (qryTipoPedidocFlgVenda.Value = 1)) then
      begin

          if ((not qryTerceiroPagador.Active) or (qryTerceiroPagadornCdTerceiro.Value = 0)) then
          begin
              nCdGrupoCredito := qryTerceironCdGrupoCredito.Value;
              nCdTerceiro     := qryTerceironCdTerceiro.Value;
          end
          else begin
              nCdGrupoCredito := qryTerceiroPagadornCdGrupoCredito.Value;
              nCdTerceiro     := qryTerceiroPagadornCdTerceiro.Value;
          end;

          qryGrupoCredito.Close;
          PosicionaQuery(qryGrupoCredito,IntToStr(nCdGrupoCredito));

          if ((frmMenu.LeParametro('OBRIGAGRPCRDPED') = 'S') and (nCdGrupoCredito = 0)) then
          begin
              MensagemAlerta('Grupo de Cr�dito n�o informado para este cliente. Imposs�vel Continuar.');
              abort;
          end;

          qryTituloAtraso.Close;
          qryTituloAtraso.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
          qryTituloAtraso.Open;

          if (qryTituloAtraso.RecordCount > 0) then
          begin
              MensagemAlerta('Cliente tem t�tulos em atraso.');
          end;

          qryChequeRisco.Close;
          qryChequeRisco.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
          qryChequeRisco.Open;

          if (qryChequeRisco.RecordCount > 0) then
          begin
              MensagemAlerta('Concentra��o de Cheque/Risco do Cliente Acima do Limite Permitido.');
          end;

          qryChequeDevolv.Close;
          qryChequeDevolv.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
          qryChequeDevolv.Open;

          if (qryChequeDevolv.RecordCount > 0) then
          begin
              MensagemAlerta('Cliente tem cheque devolvido pendente.');
          end;

          qrySaldoLimite.Close;
          PosicionaQuery(qrySaldoLimite,IntToStr(nCdTerceiro));

          if (qrySaldoLimitenSaldoLimite.Value < 0) then
          begin
              MensagemAlerta('Cliente com limite de cr�dito excedido.');
          end;

      end;

  end;

end;

procedure TfrmPedidoVendaSimples.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.Enabled := False ;
  TabAD.Enabled          := False ;
  TabFormula.Enabled     := False ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
        TabItemEstoque.Enabled := True ;

    if (qryTipoPedidocFlgItemAD.Value = 1) then
        TabAD.Enabled := True ;

    if (qryTipoPedidocFlgItemFormula.Value = 1) then
        TabFormula.Enabled := True ;

    if (qryTipoPedidocGerarFinanc.Value = 0) and (qryMaster.State = dsInsert) then
    begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbNo,mbYes],0) of
          mrNo: begin
              DBEdit4.SetFocus ;
              exit ;
          end ;
      end ;
    end ;

  end ;

  if not TabItemEstoque.Enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoVendaSimples.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;

  if (not qryEmpresa.isEmpty) then
      qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmPedidoVendaSimples.DBEdit38Exit(Sender: TObject);
begin
  inherited;

  qryCondPagto.Close;
  PosicionaQuery(qryCondPagto, DBEdit38.Text) ;
end;

procedure TfrmPedidoVendaSimples.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh4.ReadOnly   := False ;
  DBGridEh6.ReadOnly   := False ;

  btSugParcela.Enabled := True ;
  btSalvar.Enabled     := True ;
  bFinalizar.Enabled := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemErro('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  if (qryMastercFlgOrcamento.Value <> 0) then
  begin
      MensagemAlerta('Este pedido � um or�amento e n�o pode ser visualizado nesta tela.') ;
      btCancelar.Click;
      exit ;
  end ;

  PosicionaQuery(qryTerceiroPagador,qryMasternCdTerceiroPagador.asString) ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Open;

  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      MensagemErro('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;

  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString);
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;
  PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryFollow,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;

  PosicionaQuery(qryRestricoes,qryMasternCdPedido.AsString);
  PosicionaQuery(qryPedidoPromotor, qryMasternCdPedido.asString) ;

  if (qryMasternCdTabStatusPed.Value > 1) and (frmMenu.LeParametro('ALTPEDFIN') = 'N') then
  begin
      DBGridEh1.ReadOnly   := True ;
      //DBGridEh3.ReadOnly   := True ;
      DBGridEh4.ReadOnly   := True ;
      DBGridEh6.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
      btSalvar.Enabled     := False ;
      bFinalizar.Enabled   := False ;
  end ;

  TabItemEstoque.Enabled := False ;
  TabAD.Enabled          := False ;
  TabFormula.Enabled     := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
      TabItemEstoque.Enabled := True ;

  if (qryTipoPedidocFlgItemAD.Value = 1) then
      TabAD.Enabled := True ;

  if (qryTipoPedidocFlgItemFormula.Value = 1) then
      TabFormula.Enabled := True ;

  if not TabItemEstoque.enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

  DBEdit24.Enabled := True ;

  if (qryMastercFlgAdiantConfirm.Value = 1) then
      DBEdit24.Enabled := False ;

  PosicionaQuery(qryAreaVenda, qryMasternCdAreaVenda.asString) ;
  PosicionaQuery(qryCanalDistribuicao, qryMasternCdCanalDistribuicao.asString) ;

  qryDivisaoVenda.Close;
  qryDivisaoVenda.Parameters.ParamByName('nCdAreaVenda').Value := qryMasternCdAreaVenda.Value ;
  qryDivisaoVenda.Open;

  PosicionaQuery(qryDivisaoVenda, qryMasternCdDivisaoVenda.asString) ;

end;

procedure TfrmPedidoVendaSimples.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiroColab.Close ;
  qryTerceiroRepres.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryIncoterms.Close ;
  qryTerceiroTransp.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryItemAD.Close ;
  qryDadoAutorz.Close ;
  qryFollow.Close ;
  qryItemFormula.Close ;
  qryDescrTecnica.Close ;
  qryEnderecoEntrega.Close;
  qryRestricoes.Close;
  qryPedidoPromotor.Close;

  DBEdit24.Enabled := True ;

  DBGridEh1.Columns[2].Color := clWindow;

  btAcompanharProducao.Visible := False ;

  qryAreaVenda.Close;
  qryDivisaoVenda.Close;
  qryCanalDistribuicao.Close;
end;

procedure TfrmPedidoVendaSimples.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if not (qryItemEstoquenCdItemTransfEst.IsNull) then
  begin
      MensagemAlerta('O pedido foi gerado atrav�s de uma transfer�ncia de estoque. N�o � poss�vel alterar seus itens.');
      Abort;
  end;

  if (qryItemEstoquenValDesconto.AsString = '') then qryItemEstoquenValDesconto.Value  := 0;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe o produto.');
      DBGridEh1.SelectedField := qryItemEstoquecNmItem;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoque.State = dsInsert) then
  begin

      if (frmMenu.LeParametro('USACDBPDVERP') = 'S') then
      begin

          uspBuscaProduto.Close;
          uspBuscaProduto.Parameters.ParamByName('@cEAN').Value := qryItemEstoquecCdProduto.Value ;
          uspBuscaProduto.Open;

          if not uspBuscaProduto.eof then
              qryItemEstoquenCdProduto.Value := uspBuscaProdutonCdProduto.Value

      end
      else qryItemEstoquenCdProduto.Value := qryProdutonCdProduto.Value;

      qryItemEstoquenCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;
      qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

      if (qryProdutonCdGrade.Value = 0) then
          qryItemEstoquenCdTipoItemPed.Value := 2
      else
          qryItemEstoquenCdTipoItemPed.Value := 1;
  end ;

  if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') then
  begin

      qryItemEstoquenValUnitario.Value  := qryItemEstoquenValSugVenda.Value ;
      qryItemEstoquenValTotalItem.Value := ArredondaDecimal((qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value),2) ;

  end ;

  if (qryTerceirocFlgTipoFat.Value = 1) then
  begin
      qryPrecoAux.Close ;
      qryPrecoAux.Parameters.ParamByName('nCdEmpresa').Value    := frmMenu.nCdEmpresaAtiva;
      qryPrecoAux.Parameters.ParamByName('nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
      qryPrecoAux.Parameters.ParamByName('nCdTerceiro').Value   := qryMasternCdTerceiro.Value ;
      qryPrecoAux.Parameters.ParamByName('nValorInicial').Value := (qryItemEstoquenValUnitario.Value-qryItemEstoquenValDesconto.Value+qryItemEstoquenValACrescimo.Value) ;
      qryPrecoAux.Open ;

      if not qryPrecoAux.Eof then
          qryItemEstoquenValUnitarioEsp.Value := qryPrecoAuxnPrecoAux.Value ;
  end ;

  if (qryItemEstoquenQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do produto.') ;
      DBGridEh1.SelectedField := qryItemEstoquenQtdePed;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do produto.') ;
      DBGridEh1.SelectedField := qryItemEstoquenValUnitario;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValDesconto.Value > qryItemEstoquenValUnitario.Value) then
  begin
      MensagemAlerta('Valor de Desconto maior que o valor unit�rio.') ;
      DBGridEh1.SelectedField := qryItemEstoquenValDesconto;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  inherited;

  if (qryItemEstoquenValFrete.IsNull) then
      qryItemEstoquenValFrete.Value := 0;

  qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value + qryItemEstoquenValFrete.Value;
  qryItemEstoquenValTotalItem.Value := ArredondaDecimal((qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value),2) ; //+ 0.005  ;

  if (qryItemEstoque.State = dsEdit) then
  begin

      qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (StrToFloat(qryItemEstoquenQtdePed.OldValue) * StrToFloat(qryItemEstoquenValUnitario.OldValue));
      qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (StrToFloat(qryItemEstoquenQtdePed.oldValue) * StrToFloat(qryItemEstoquenValDesconto.oldValue))  ;
      qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (StrToFloat(qryItemEstoquenQtdePed.oldValue) * StrToFloat(qryItemEstoquenValAcrescimo.oldValue)) ;
      qryMasternValFrete.Value     := qryMasternValFrete.Value     - (StrToFloat(qryItemEstoquenQtdePed.OldValue) * StrToFloat(qryItemEstoquenValFrete.OldValue))  ;

      if (qryMasternValDesconto.Value   < 0) then qryMasternValDesconto.Value  := 0;
      if (qryMasternValProdutos.Value   < 0) then qryMasternValProdutos.Value  := 0;
      if (qryMasternValAcrescimo.Value  < 0) then qryMasternValAcrescimo.Value := 0;
      if (qryMasternValFrete.Value      < 0) then qryMasternValFrete.Value     := 0;
  end ;

  qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

end;

procedure TfrmPedidoVendaSimples.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      qryProduto.Close ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof then
          qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;

  if (DBGridEh1.Col = 1) then
  begin
      if (qryItemEstoque.State <> dsBrowse) then
      begin
          DBGridEh1.Columns[2].ReadOnly := True;
          DBGridEh1.Columns[2].Color    := clSilver;

          if (frmMenu.LeParametro('USACDBPDVERP') = 'S') then
          begin

              uspBuscaProduto.Close;
              uspBuscaProduto.Parameters.ParamByName('@cEAN').Value := qryItemEstoquecCdProduto.Value ;
              uspBuscaProduto.Open;

              PosicionaQuery(qryProduto, uspBuscaProdutonCdProduto.asString) ;

              if not uspBuscaProduto.eof then
              begin
                  qryItemEstoquenCdProduto.Value := uspBuscaProdutonCdProduto.Value;
                  qryItemEstoquecNmItem.Value    := uspBuscaProdutocNmProduto.Value ;
                  DBGridEh1.Columns[2].ReadOnly  := False ;
                  DBGridEh1.Columns[2].Color     := clWindow;
                  DBGridEh1.Col                  := 3 ;
              end ;

              if uspBuscaProduto.eof then
              begin
                  DBGridEh1.Columns[2].ReadOnly := False ;
                  DBGridEh1.Columns[2].Color    := clWindow;
                  DBGridEh1.Col                 := 2 ;
              end;

              qryItemEstoquenValUnitario.Value  := 0 ;
              qryItemEstoquenValTotalItem.Value := 0 ;
              qryItemEstoquenValCustoUnit.Value := 0 ;
              qryItemEstoquenValSugVenda.Value  := 0 ;

          end;

          if (frmMenu.LeParametro('USACDBPDVERP') = 'N') then
          begin
             qryItemEstoquenValUnitario.Value  := 0 ;
             qryItemEstoquenValTotalItem.Value := 0 ;
             qryItemEstoquenValCustoUnit.Value := 0 ;
             qryItemEstoquenValSugVenda.Value  := 0 ;
             qryItemEstoquenValCMV.Value       := 0 ;

             qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

             try
                 strToint(qryItemEstoquecCdProduto.asString) ;
             except
                 MensagemErro('C�digo de produto inv�lido. Utilize apenas n�meros.') ;
                 abort ;
             end ;

             PosicionaQuery(qryProduto, qryItemEstoquecCdProduto.asString) ;

             { -- produto grade -- }
             if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
             begin
                 qryItemEstoquenCdProduto.Value   := qryProdutonCdProduto.Value;
                 qryItemEstoquecNmItem.Value      := qryProdutocNmProduto.Value;
                 qryItemEstoquenValUnitario.Value := qryProdutonValVenda.Value;
                 qryItemEstoquenValSugVenda.Value := qryProdutonValVenda.Value;
                 qryItemEstoquenValCMV.Value      := qryProdutonValCusto.Value;

                 DBGridEh1.Columns[2].ReadOnly := False ;
                 DBGridEh1.Columns[2].Color    := clWindow;

                 { -- removido utiliza��o do bloco abaixo devido nova forma de inclus�o dos sub itens atrav�s da fMontaGrade -- }
                 {DBGridEh2.AutoFitColWidths := True ;

                 usp_Grade.Close ;
                 usp_Grade.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
                 usp_Grade.Parameters.ParamByName('@nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
                 usp_Grade.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
                 usp_Grade.ExecProc ;

                 qryTemp.Close ;
                 qryTemp.SQL.Clear ;
                 qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
                 qryTemp.Open ;

                 qryTemp.Edit ;

                 DBGridEh2.Columns.Items[0].Visible := False ;

                 DBGridEh2.Visible := True ;

                 DBGridEh2.SetFocus ;

                 objGrade.qryValorAnterior.SQL.Text := '' ;

                 frmMenu.mensagemUsuario('Montando Grade...');

                 if (qryItemEstoquenCdItemPedido.Value > 0) then
                     objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

                 objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

                 { -- se pedido j� foi finalizado, monta grade apenas para exibi��o --
                 if (qryMasternCdTabStatusPed.Value > 1) then
                     objGrade.DBGridEh1.ReadOnly := True ;

                 frmMenu.mensagemUsuario('Renderizando...');

                 objGrade.Renderiza;
                 objGrade.DBGridEh1.ReadOnly := False ;


                 { -- se pedido esta como digitado, atualiza saldo do item --
                 if (qryMasternCdTabStatusPed.Value = 1) then
                 begin
                     frmMenu.mensagemUsuario('Atualizando Item...');

                     qryItemEstoque.Edit ;
                     qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;

                     DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
                     DBGridEh1.SetFocus;
                 end ;

                 }
                 frmMenu.mensagemUsuario('');

                 DBGridEh1.Col := 3 ;
             end ;

             { -- produto ERP -- }
             if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
             begin
                qryItemEstoquenCdProduto.Value := qryProdutonCdProduto.Value;
                qryItemEstoquecNmItem.Value    := qryProdutocNmProduto.Value ;
                DBGridEh1.Columns[2].ReadOnly  := False ;
                DBGridEh1.Columns[2].Color     := clWindow;
                DBGridEh1.Col                  := 3;
             end ;
          end;
      end ;

      if ((qryItemEstoque.State <> dsBrowse) and (not bPermiteDupProd)) then
      begin

         qryItensPedido.Close;
         qryItensPedido.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value;
         qryItensPedido.Parameters.ParamByName('nCdProduto').Value    := qryItemEstoquenCdProduto.Value;
         qryItensPedido.Parameters.ParamByName('nCdPedido').Value     := qryMasternCdPedido.Value;
         qryItensPedido.Open;

         if (not qryItensPedido.eof) then
         begin
             MensagemAlerta('Este produto j� foi incluso neste pedido.') ;
             DBGridEh1.Col := 1 ;
             abort;
         end;

      end;

  end ;

end;

procedure TfrmPedidoVendaSimples.FormShow(Sender: TObject);
begin
  inherited;

  bPosicionaItens := True;

  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.LeParametro('USADESCTECPED') <> 'S') then
  begin
      TabDesTecnica.Enabled := False ;
  end ;

  btDescontoTotalPedido.Enabled := (frmMenu.LeParametro('PMDSCTTPEDV') = 'S') ;

  bRepresentante := False;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, intToStr(frmMenu.nCdUsuarioLogado));

  cWhereAddConsultaPadrao := '' ;

  if (qryUsuariocFlgRepresentante.Value = 1) then
  begin
      bRepresentante := True;

      {-- adiciona um WHERE adicional na CONSULTA da tela. a variavel cWhereAddConsultaPadrao esta definida no FormPadrao --}
      cWhereAddConsultaPadrao := 'Pedido.nCdTerceiroRepres = ' + qryUsuarionCdTerceiroResponsavel.asString ;
  end ;

  qryUsuario.Close;

  bPermiteDupProd := (frmMenu.LeParametro('PERMDUPPRODPEDV') = 'S') ;

  lblHelp.Font.Style := [fsBold] ;
  lblHelp.Font.Color := clBlue ;

  DBGridEh1.Columns[DBGridEh1.FieldColumns['nValDesconto'].Index].DisplayFormat  := '#,##0.0000' ;
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nValCustoUnit'].Index].DisplayFormat := '#,##0.0000' ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      desativaDBEdit(DBEdit30);
  end;

  { -- cria objetos globais --
  objGrade := TfrmMontaGrade.Create(Self); }
end;

procedure TfrmPedidoVendaSimples.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryItemAD.Active then
      PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;

  if not qryItemFormula.Active then
      PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;

  {-- gera as divergencias no pedido --}
  if (bInserePed) then
  begin

      if ((qryTipoPedidocGerarFinanc.Value = 1) and (qryTipoPedidocFlgVenda.Value = 1)) then
      begin

          frmMenu.Connection.BeginTrans;

          try
              SP_GERA_DIVERG_PED.Close;
              SP_GERA_DIVERG_PED.Parameters.ParamByName('@nCdTerceiro').Value     := nCdTerceiro;
              SP_GERA_DIVERG_PED.Parameters.ParamByName('@nCdPedido').Value       := qryMasternCdPedido.Value;
              SP_GERA_DIVERG_PED.Parameters.ParamByName('@nCdUsuarioAutor').Value := frmMenu.nCdUsuarioLogado;
              SP_GERA_DIVERG_PED.ExecProc;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no Processamento.');
              raise;
          end;

          frmMenu.Connection.CommitTrans;

      end ;

  end;

  bInserePed := False;

  if not qryRestricoes.Active then
      PosicionaQuery(qryRestricoes,qryMasternCdPedido.asString) ;

  if not qryPedidoPromotor.Active then
      PosicionaQuery(qryPedidoPromotor,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoVendaSimples.qryItemEstoqueAfterPost(DataSet: TDataSet);
var
  i : Integer;
begin
  { -- removido utiliza��o da procedure devido nova forma de inclus�o dos sub itens -- }
  {usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;}

  { -- se pedido utiliza produto grade, gera os sub itens --
  if (objGrade.bMontada) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString) ;
      qryAux.ExecSQL;

      objGrade.cdsQuantidade.First;
      objGrade.cdsCodigo.First ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value    := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value         := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      objGrade.LiberaGrade;

  end ;
  }

  inherited;

  {-- Calculo total produtos e frete --}
  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
  qryMasternValFrete.Value     := qryMasternValFrete.Value     + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValFrete.Value);

  {-- ap�s atualizar o produto recalcula o desconto --}
  qrySumTotalPed.Close;
  PosicionaQuery(qrySumTotalPed,qryMasternCdPedido.AsString);

  qryMasternValDesconto.Value  := ArredondaDecimal(qrySumTotalPednValTotalDescPed.Value,2); //qryMasternValDesconto.Value  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value) ;

  {-- calcula acrescimo --}
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;

  qryMaster.Post ;
end;

procedure TfrmPedidoVendaSimples.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdLoja.Value = 0) and (not DBEdit30.ReadOnly)) then
  begin
      MensagemAlerta('Informe a loja.') ;
      DbEdit30.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit33.Text = '') then
  begin
      MensagemAlerta('Informe o endere�o de entrega do pedido.') ;
      DBEdit32.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDesconto.Value < 0) then
  begin
      MensagemErro('Percentual de desconto inv�lido.') ;
      DbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercAcrescimo.Value < 0) then
  begin
      MensagemErro('Percentual de acr�scimo inv�lido.') ;
      DbEdit9.SetFocus ;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;  

  if (qryMasterdDtPrevEntIni.AsString = '') then
  begin
      qryMasterdDtPrevEntIni.Value := qryMasterdDtPedido.Value ;
  end ;

  if (qryMasterdDtPrevEntFim.AsString = '') then
  begin
      qryMasterdDtPrevEntFim.Value := qryMasterdDtPedido.Value ;
  end ;

  if (qryMasterdDtPrevEntIni.IsNull) then
  begin
      MensagemAlerta('Informe a data inicial da previs�o de entrega.');

      DBEdit21.SetFocus;

      Abort;
  end;

  if (qryMasterdDtPrevEntFim.IsNull) then
  begin
      MensagemAlerta('Informe a data final da previs�o de entrega.');

      DBEdit23.SetFocus;

      Abort;
  end;

  if (qryMasterdDtPrevEntIni.Value > qryMasterdDtPrevEntFim.Value) then
  begin
      MensagemAlerta('A data inicial da previs�o de entrega � maior do que a data final.');

      DBEdit23.SetFocus;

      Abort;
  end;

  if (qryMasterdDtPrevEntIni.Value < qryMasterdDtPedido.Value) then
  begin
      MensagemAlerta('A data da previs�o de entrega inicial � menor do que a data do pedido.');

      DBEdit21.SetFocus;

      Abort;
  end;

  if (qryMaster.State = dsInsert) then
  begin

      if ((qryTipoPedidocGerarFinanc.Value = 1) and (qryTipoPedidocFlgVenda.Value = 1)) then
          if ((frmMenu.LeParametro('OBRIGAGRPCRDPED') = 'S') and (nCdGrupoCredito = 0)) then
          begin
              MensagemAlerta('Grupo de Cr�dito n�o informado para este cliente. Imposs�vel Continuar.');
              DBEdit5.SetFocus;
              abort;
          end;

  end;

  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabStatusPed.Value := 1;

  // se estiver aprovado, volta para digitado
  {if (qryMasternCdTabStatusPed.Value = 3) then
  begin
      qryMasternCdTabStatusPed.Value := 1;
  end ;}

  if (qryMaster.State = dsInsert) then
      bInserePed := true;

  qryMasternValPedido.Value := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value + qryMasternValFrete.Value;

end;

procedure TfrmPedidoVendaSimples.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoVendaSimples.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  if not (qryItemEstoquenCdItemTransfEst.IsNull) then
  begin
      MensagemAlerta('O pedido foi gerado atrav�s de uma transfer�ncia de estoque. N�o � poss�vel alterar seus itens.');
      Abort;
  end;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value);
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value)  ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;
  qryMasternValFrete.Value     := qryMasternValFrete.Value     - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValFrete.Value)     ;
end;

procedure TfrmPedidoVendaSimples.cxButton1Click(Sender: TObject);
begin
{
  if not qryItemEstoque.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      ShowMessage('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;

  DBGridEh2.AutoFitColWidths := True ;

  usp_Grade.Close ;
  usp_Grade.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Grade.Parameters.ParamByName('@nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
  usp_Grade.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Grade.ExecProc ;

  qryTemp.Close ;
  qryTemp.SQL.Clear ;
  qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
  qryTemp.Open ;

  qryTemp.First ;

  if (qryTemp.State = dsBrowse) then
      qryTemp.Edit
  else qryTemp.Insert ;

  qryTemp.FieldList[0].Value := qryTemp.FieldList[0].Value ;

  DBGridEh2.Columns.Items[0].Visible := False ;

  DBGridEh2.Visible := True ;
  DBGridEh2.SetFocus ;

  objGrade.qryValorAnterior.SQL.Text := '' ;

  frmMenu.mensagemUsuario('Montando Grade...');

  if (qryItemEstoquenCdItemPedido.Value > 0) then
      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

  objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

  { -- se pedido j� foi finalizado, monta grade apenas para exibi��o --
  if (qryMasternCdTabStatusPed.Value > 1) then
      objGrade.DBGridEh1.ReadOnly := True ;

  frmMenu.mensagemUsuario('Renderizando...');

  objGrade.Renderiza;
  objGrade.DBGridEh1.ReadOnly := False ;


  { -- se pedido esta como digitado, atualiza saldo do item --
  if (qryMasternCdTabStatusPed.Value = 1) then
  begin
      frmMenu.mensagemUsuario('Atualizando Item...');

      qryItemEstoque.Edit ;
      qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;

      DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
      DBGridEh1.SetFocus;
  end ;

  frmMenu.mensagemUsuario('');
  }
end;

procedure TfrmPedidoVendaSimples.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmPedidoVendaSimples.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value + qryItemEstoquenValFrete.Value) ;
  qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value ;

  qryItemEstoquenValTotalItem.Value := ArredondaDecimal((qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value),2) ;

end;

procedure TfrmPedidoVendaSimples.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  
  qryMaster.Post;
end;

procedure TfrmPedidoVendaSimples.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmPedidoVendaSimples.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      ShowMessage('Pedido sem valor.') ;
      exit ;
  end ;

  //if (qryMaster.State = dsInsert) then
  //begin
  //    MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.');
  //end;

  if (qryMaster.State in [dsEdit,dsInsert]) then
  begin
      qryMaster.Post ;
  end ;

  if (qryTipoPedidocGerarFinanc.Value = 0) then
  begin
      MensagemAlerta('Tipo de Pedido n�o configurado para gerar t�tulos financeiros.');
      Exit;
  end;

  if (qryMasternCdCondPagto.Value = 0) then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.');
      DBEdit38.SetFocus;
      Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoVendaSimples.bFinalizarClick(Sender: TObject);
var
  nValProdutos, nValDesconto, nValAcrescimo : double ;
  bAlerta         : boolean ;
  nCdGrupoCredito : integer;
  nCdTerceiro     : integer;
begin

  nValProdutos  := 0 ;
  nValDesconto  := 0 ;
  nValAcrescimo := 0 ;
  bAlerta       := false ;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  if qryItemAD.Active then
      qryItemAD.First ;

  if qryItemFormula.Active then
      qryItemformula.First ;

  { -- calcula o total do pedido -- }
  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos  := nValProdutos  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
          nValDesconto  := nValDesconto  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDescAbatUnit.Value)  ;
          nValDesconto  := nValDesconto  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value)  ;
          nValAcrescimo := nValAcrescimo + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  if qryItemAD.Active and not qryItemAD.eof then
  begin

      qryItemAD.First ;

      while not qryItemAD.Eof do
      begin
          nValProdutos  := nValProdutos + qryItemADnValTotalItem.Value ;
          qryItemAD.Next ;
      end ;

      qryItemAD.First ;

  end ;

  if qryItemFormula.Active and not qryItemFormula.eof then
  begin

      qryItemFormula.First ;

      while not qryItemFormula.Eof do
      begin
          nValProdutos  := nValProdutos  + (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value) ;
          nValDesconto  := nValDesconto  + (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
          nValAcrescimo := nValAcrescimo + (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;
          qryItemFormula.Next ;
      end ;

      qryItemFormula.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value  := nValProdutos  ;
  qryMasternValDesconto.Value  := ArredondaDecimal(nValDesconto,2) ;
  qryMasternValAcrescimo.Value := nValAcrescimo ;
  qryMaster.Post ;

  if (frmMenu.LeParametro('OBRIGAREAVDAPED') = 'S') then
  begin
      if (qryMasternCdAreaVenda.AsString = '') then
      begin
          MensagemAlerta('� obrigat�rio a sele��o de uma �rea de Venda.');
          cxPageControl1.ActivePageIndex := 3;
          edtAreaVenda.SetFocus;
          abort;
      end;

      if (qryMasternCdDivisaoVenda.AsString = '') then
      begin
          MensagemAlerta('� obrigat�rio a sele��o de uma Divis�o de Venda.');
          cxPageControl1.ActivePageIndex := 3;
          edtDivisaoVenda.SetFocus;
          abort;
      end;

      if (qryMasternCdCanalDistribuicao.AsString = '') then
      begin
          MensagemAlerta('� obrigat�rio a sele��o de um Canal de Distribui��o.');
          cxPageControl1.ActivePageIndex := 3;
          edtCanalDistribuicao.SetFocus;
          abort;
      end;
  end;

  { -- verifica se exige informar representante no pedido de venda -- }
  if ((qryTipoPedido.Active) and ((qryTipoPedidocFlgCalcComissao.Value = 1) or (qryTipoPedidocFlgExigeRepres.Value = 1))) then
  begin
      if (qryMasternCdTerceiroRepres.Value = 0) then
      begin
          MensagemAlerta('Informe o representante do pedido.');
          cxPageControl1.ActivePageIndex := 3;
          DBEdit25.SetFocus;
          abort;
      end;

      { verifica se representante est� vinculado ao terceiro do pedido }
      qryTerceiroRepresentante.Close;
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.ConvInteiro(DBEdit25.Text);
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(DBEdit5.Text);
      qryTerceiroRepresentante.Open;

      if (qryTerceiroRepresentante.IsEmpty) then
      begin
          MensagemAlerta('Representante n�o pertence ao Terceiro do pedido.');
          qryTerceiroRepres.Close;
          cxPageControl1.ActivePageIndex := 3;
          DbEdit25.Text                  := '';
          DbEdit25.SetFocus;
          Abort;
      end;

      { -- verifica se representante possui grupo de comiss�o para efeitos de c�lculo de comiss�o -- }
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM Terceiro WHERE nCdTerceiro = ' + qryMasternCdTerceiroRepres.AsString + ' AND nCdGrupoComissao IS NOT NULL');
      qryAux.Open;

      if (qryAux.IsEmpty) then
      begin
          MensagemAlerta('Este representante n�o possui grupo de comiss�o.' + #13 + 'Necess�rio que seja efetuado este v�nculo para efeitos de c�lculo de comiss�o.');
          Abort;
      end;
  end;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin

      if (dbEdit38.Text = '') then
      begin
          MensagemAlerta('Informe a condi��o de pagamento.') ;
          exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          usp_Sugere_Parcela.Close ;
          usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
          usp_Sugere_Parcela.ExecProc ;
      except
          frmMenu.Connection.RollbackTrans ;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans ;

      PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
      qryAux.Open ;

      If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor da soma das parcelas � diferente do valor total do pedido') ;
          exit ;
      end ;

      qryAux.Close ;
  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

    if (qryTipoPedidocGerarFinanc.Value = 0) then
    begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
    end ;

  {-- descobre o Grupo de Cr�dito --}
  if ((not qryTerceiroPagador.Active) or (qryTerceiroPagadornCdTerceiro.Value = 0)) then
      nCdGrupoCredito := qryTerceironCdGrupoCredito.Value
  else nCdGrupoCredito := qryTerceiroPagadornCdGrupoCredito.Value;

  qryGrupoCredito.Close;
  PosicionaQuery(qryGrupoCredito,IntToStr(nCdGrupoCredito));


  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;

      {-- quando o cliente n�o tem um grupo de cr�dito   --
       -- verifica se o tipo do pedido exige autoriza��o --}

      if (qryGrupoCreditonCdGrupoCredito.Value = 0) or (qryTipoPedidocFlgVenda.Value = 0) then
      begin
          if (qryTipoPedidocExigeAutor.Value = 1) then
              qryMasternCdTabStatusPed.Value := 2  // Aguardando aprova��o
          else begin
              qryMasternCdTabStatusPed.Value := 3 ; // Aprovado
              qryMasterdDtAutor.Value        := Now() ;
              qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;
          end;
      end ;

      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;
      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;

      {-- quando o cliente n�o tem um grupo de cr�dito   --
       -- verifica se o tipo do pedido exige autoriza��o --}

      if (qryGrupoCreditonCdGrupoCredito.Value = 0) then
      begin
          if (qryTipoPedidocExigeAutor.Value = 1) and (qryTipoPedidocLimiteCredito.Value = 1) then
          begin
              usp_valida_credito.Close ;
              usp_valida_credito.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
              usp_valida_credito.ExecProc;
          end ;
      end;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

  // se o pedido � de venda e j� est� autorizado, empenha estoque.
  if (qryTipoPedidocFlgVenda.Value = 1) and (qryMasterdDtAutor.AsString <> '') then
  begin

      frmMenu.Connection.BeginTrans;

      try
          SP_EMPENHA_ESTOQUE_PEDIDO.Close ;
          SP_EMPENHA_ESTOQUE_PEDIDO.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
          SP_EMPENHA_ESTOQUE_PEDIDO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  if ((qryTipoPedidocExigeAutor.Value = 1) and (qryMasterdDtAutor.asString = '')) or (qryMasternCdTabStatusPed.Value = 2) then
  begin
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 2 ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'LIBERAR PEDIDO DE VENDA' ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'No Pedido: ' + qryMasternCdPedido.AsString + '  Terceiro: ' + qryTerceirocNmTerceiro.Value + '   Valor Pedido : ' + Format('%m',[qryMasternValPedido.Value]) ;
      frmMenu.SP_GERA_ALERTA.ExecProc;
  end;

  if (frmMenu.LeParametroEmpresa('LIMITECREDITO') = 'S') and (qryTipoPedidocLimiteCredito.Value = 1) and (qryMasterdDtAutor.AsString = '') then
  begin
      MensagemAlerta('Limite de cr�dito excedido para este cliente, o pedido n�o foi autorizado, contate o financeiro.') ;

      if (qryTipoPedidocFlgGeraOP.Value = 0) then
          btCancelar.Click;

      exit ;
  end ;

  ShowMessage('Pedido finalizado com sucesso.') ;

  case MessageDlg('Deseja imprimir o pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: ToolButton12.Click;
  end ;

  if (qryTipoPedidocFlgGeraOP.Value = 0) then
      btCancelar.Click;
      
end;

procedure TfrmPedidoVendaSimples.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoVendaSimples.qryItemADBeforePost(DataSet: TDataSet);
begin

  if (qryItemADcNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemADcSiglaUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItemADnQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemADnValCustoUnit.Value < 0) then
  begin
      MensagemAlerta('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  if (qryItemADnCdGrupoImposto.Value = 0) or (qryItemADcNmGrupoImposto.Value = '') then
  begin
      MensagemAlerta('Informe o grupo de imposto') ;
      abort ;
  end ;

  inherited;

  if (qryItemAD.State = dsInsert) then
  begin
      qryItemADnCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemADnCdTipoItemPed.Value := 5 ;
      qryItemADnCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO');
      qryItemADcCdProduto.Value     := 'AD' + qryItemADnCdItemPedido.AsString;
  end ;

  qryItemADcNmItem.Value             := UpperCase(qryItemADcNmItem.Value) ;
  qryItemADcSiglaUnidadeMedida.Value := UpperCase(qryItemADcSiglaUnidadeMedida.Value) ;

  qryItemADnValUnitarioEsp.Value := qryItemADnValCustoUnit.Value ;
  qryItemADnValTotalItem.Value   := ArredondaDecimal((qryItemADnQtdePed.Value * qryItemADnValCustoUnit.Value),2) ;

  if (qryItemAD.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemADnValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmPedidoVendaSimples.qryItemADAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryItemAD.Edit ;
  qryItemADcCdProduto.Value   := 'AD' + qryItemADnCdItemPedido.AsString;
  //qryItemAD.Post ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemADnValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedidoVendaSimples.qryItemADBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemADnValTotalItem.Value ;

end;

procedure TfrmPedidoVendaSimples.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedidoVendaSimples.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F3 : incluirProdutoMultiplo;
    vk_F5 : btSituacaoEstoque.Click;
    vk_F4 : begin

        if (not qryMaster.Active) or (qryMasternCdPedido.Value = 0) then
        begin
            MensagemAlerta('Nenhum pedido ativo.');
            Abort;
        end;

        if (qryMasternCdTabStatusPed.Value > 1) then
        begin
            MensagemAlerta('O Status do pedido n�o permite altera��o. Utilize a reabertura de pedidos para alter�-lo.');
            Abort;
        end;

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(141,'Produto.cFlgProdVenda = 1 AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecCdProduto.Value := qryProdutonCdProduto.AsString;
                    qryItemEstoquecNmItem.Value    := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Columns[9].ReadOnly := True;
  DBGridEh1.Columns[9].Color    := clSilver;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
  begin
      DBGridEh1.Columns[9].ReadOnly := False;
      DBGridEh1.Columns[9].Color    := clWindow;
  end;
end;

procedure TfrmPedidoVendaSimples.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoVendaSimples.ToolButton12Click(Sender: TObject);
var
  objRel :  TrptPedidoVenda ;

begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  objRel :=  TrptPedidoVenda.Create(nil);

  Try
      Try

          PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;
          objRel.QRSubDetail3.Enabled := True ;

         if (objRel.qryItemEstoque_Grade.eof) then
             objRel.QRSubDetail1.Enabled := False ;

         if (objRel.qryItemAD.eof) then
             objRel.QRSubDetail2.Enabled := False ;

         if (objRel.qryItemFormula.eof) then
             objRel.QRSubDetail3.Enabled := False ;

         objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

         objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
         raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmPedidoVendaSimples.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) < qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clYellow;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) >= qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clBlue;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemADnQtdeCanc.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clRed;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryItemFormulaBeforePost(DataSet: TDataSet);
begin

  if (qryItemFormulacNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemFormulanQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemFormulanValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemFormula.State = dsInsert) then
  begin
      qryItemFormulanCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemFormulanCdTipoItemPed.Value := 6 ;
  end ;

  qryItemFormulanValTotalItem.Value := ArredondaDecimal((qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value),2) ;

  if (qryItemFormula.State = dsEdit) then
  begin
      qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValUnitario.OldValue))  ;
      qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValDesconto.OldValue))  ;
      qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValAcrescimo.OldValue)) ;

      if (qryMasternValDesconto.Value < 0) then qryMasternValDesconto.Value := 0 ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryItemFormulaAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  + (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value)  ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  + (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value + (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;

  qryMaster.Post ;

  if (bPosicionaItens = True) then
  begin
     qryItemFormula.Requery([]);
     qryItemFormula.Last;
  end;
  bPosicionaItens := True;

end;

procedure TfrmPedidoVendaSimples.qryItemFormulaBeforeDelete(
  DataSet: TDataSet);
begin

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value)  ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;

  if (qryMasternValDesconto.Value < 0) then qryMasternValDesconto.Value := 0 ;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemFormulanCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  inherited;

end;

procedure TfrmPedidoVendaSimples.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) < qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clYellow;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) >= qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clBlue;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemFormulanQtdeCanc.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clRed;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemFormula.State = dsBrowse) then
             qryItemFormula.Edit ;


        if (qryItemFormula.State = dsInsert) or (qryItemFormula.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(70,'cFlgProdVenda = 1 AND EXISTS(SELECT 1 FROM FormulaTipoPedido PTP WHERE PTP.nCdProdutoPai = Produto.nCdProduto AND nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemFormulanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoVendaSimples.DBGridEh6ColExit(Sender: TObject);
var
  objForm : TfrmEmbFormula ;
begin
  inherited;

  if (DBGridEh6.Col = 1) then
  begin

      if (qryItemFormula.State <> dsBrowse) then
      begin

        PosicionaQuery(qryProdutoFormulado, qryItemFormulanCdProduto.AsString) ;

        if not qryProdutoFormulado.eof then
        begin
            qryItemFormulacNmItem.Value := qryProdutoFormuladocNmProduto.Value ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
            qryAux.Open ;

            if not qryAux.eof then
            begin

                if (qryItemFormulanCdItemPedido.Value = 0) then
                    qryItemFormulanCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO');

                objForm := TfrmEmbFormula.Create(nil);
                objForm.ToolButton1.Visible := True ;
                objForm.nCdPedido           := qryMasternCdPedido.Value          ;
                objForm.nCdProduto          := qryItemFormulanCdProduto.Value    ;
                objForm.nCdItemPedido       := qryItemFormulanCdItemPedido.Value ;
                objForm.nCdTabStatusPed     := qryMasternCdTabStatusPed.Value    ;
                objForm.nCdTerceiro         := qryMasternCdTerceiro.Value        ;
                objForm.cFlgTipoFat         := qryTerceirocFlgTipoFat.Value      ;
                objForm.RenderizaGrade() ;

                // calcula o pre�o
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValProduto        ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValDescontoProd,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValDescontoProd   ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValAcrescimoProd,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAcrescimoProd  ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAmostra        ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValDescontoAmos,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValDescontoAmos   ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValAcrescimoAmos,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAcresimoAmos   ') ;
                qryAux.SQL.Add('  FROM #Temp_Grade_Embalagem          ') ;
                qryAux.Open ;

                qryItemFormulanValUnitario.Value  := qryAux.FieldList[0].Value + qryAux.FieldList[3].Value ;
                qryItemFormulanValDesconto.Value  := qryAux.FieldList[1].Value + qryAux.FieldList[4].Value ;
                qryItemFormulanValAcrescimo.Value := qryAux.FieldList[2].Value + qryAux.FieldList[5].Value ;
                qryItemFormulanValCustoUnit.Value := qryItemFormulanValUnitario.Value - qryItemFormulanValDesconto.Value + qryItemFormulanValAcrescimo.Value ;

                // calcula o pre�o esperado
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitarioProdEsp,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValProdutoEsp        ') ;
                qryAux.SQL.Add('  FROM #Temp_Grade_Embalagem          ') ;
                qryAux.Open ;

                qryItemFormulanValUnitarioEsp.Value  := qryAux.FieldList[0].Value ;

                FreeAndNil(objForm);
                
            end ;

            qryAux.Close ;

        end ;

      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.cxButton2Click(Sender: TObject);
var
  objForm : TfrmEmbFormula ;

begin

  if not qryItemFormula.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  objForm := TfrmEmbFormula.Create(nil);

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
  qryAux.Open ;

  if not qryAux.eof then
  begin

      objForm.ToolButton1.Visible := True ;
      objForm.nCdPedido           := qryMasternCdPedido.Value          ;
      objForm.nCdProduto          := qryItemFormulanCdProduto.Value    ;
      objForm.nCdItemPedido       := qryItemFormulanCdItemPedido.Value ;
      objForm.nCdTabStatusPed     := qryMasternCdTabStatusPed.Value    ;
      objForm.RenderizaGrade() ;

      // calcula o pre�o
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValProduto        ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValDescontoProd,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValDescontoProd   ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValAcrescimoProd,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValAcrescimoProd  ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValAmostra        ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValDescontoAmos,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValDescontoAmos   ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValAcrescimoAmos,0))') ;
      qryAux.SQL.Add('                ELSE 0                ') ;
      qryAux.SQL.Add('           END) as nValAcresimoAmos   ') ;
      qryAux.SQL.Add('  FROM #Temp_Grade_Embalagem          ') ;
      qryAux.Open ;

      if (qryMasternCdTabStatusPed.Value <= 1) then
      begin

          if (qryItemFormula.State = dsBrowse) then
              qryItemFormula.Edit ;

          qryItemFormulanValUnitario.Value  := qryAux.FieldList[0].Value + qryAux.FieldList[3].Value ;
          qryItemFormulanValDesconto.Value  := qryAux.FieldList[1].Value + qryAux.FieldList[4].Value ;
          qryItemFormulanValAcrescimo.Value := qryAux.FieldList[2].Value + qryAux.FieldList[5].Value ;
          qryItemFormulanValCustoUnit.Value := qryItemFormulanValUnitario.Value - qryItemFormulanValDesconto.Value + qryItemFormulanValAcrescimo.Value ;

          {qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value - qryAux.FieldList[1].Value + qryAux.FieldList[2].Value + qryAux.FieldList[3].Value - qryAux.FieldList[4].Value + qryAux.FieldList[5].Value;}

          DbGridEh6.SetFocus ;
          DBGridEh6.Col := 3 ;

      end ;

  end
  else begin
      ShowMessage('Produto n�o tem configura��o de embalagem.') ;
  end ;

  qryAux.Close ;
  FreeAndNil(objForm);

end;

procedure TfrmPedidoVendaSimples.DBGridEh6Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoVendaSimples.btDescontoTotalPedidoClick(Sender: TObject);
var
  nValDesconto        : double;
  nValTotalPed        : double;
  nValDescontoGrade   : double;
  nValTotalPedGrade   : double;
  nValDescontoFormula : double;
  nValTotalPedFormula : double;
  nValPercMaxDesconto : double;
  nValLiquido         : double;
  nPercDesconto       : double;

  objForm : TfrmPedidoVendaSimplesDesconto ;
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    if (qryMasternCdTabStatusPed.Value > 1) then
    begin
        ShowMessage('Pedido finalizado n�o pode ser alterado.') ;
        exit ;
    end ;

    objForm :=  TfrmPedidoVendaSimplesDesconto.Create(nil);

    // verificar se o pedido ja tem desconto
    // itens de estoque grade
    nValDesconto := 0;

    qryItemEstoque.First ;
    while (qryItemEstoque.Active and not qryItemEstoque.eof) do
    begin
        nValDesconto := (nValDesconto + qryItemEstoquenValDesconto.Value);
        qryItemEstoque.Next ;
    end ;

    //itens formulado
    qryItemFormula.First ;
    while (qryItemFormula.Active and not qryItemFormula.eof) do
    begin
        nValDesconto := (nValDesconto + qryItemFormulanValDesconto.Value);
        qryItemFormula.Next ;
    end ;

    if (nValDesconto > 0) then
    begin
        case MessageDlg('O desconto concedido ser� desconsiderado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
           mrNo: begin
                 Exit;
           end;
           //mrYes:
        end;
    end;

    objForm.edtValAcrescimo.Enabled := False;
    nValPercMaxDesconto             := objForm.nPercMaxDesc;
    objForm.nPercMaxDesc            := 100;
    objForm.edtValVenda.Value       := qryMasternValProdutos.Value;

    showForm(objForm,false);

    objForm.edtValAcrescimo.Enabled := True;
    objForm.nPercMaxDesc            := nValPercMaxDesconto;

    nValDesconto := 0;
    nPercDesconto:= objForm.edtPercent.Value;
    nValDesconto := objForm.edtValDesconto.Value;
    nValLiquido  := objForm.edtValLiquido.Value;

    CalculaDescontoTotalPedido(nPercDesconto, nValDesconto , nValLiquido);

    cxPageControl1.ActivePageIndex := 0;

    DBGridEh1.SetFocus;

end;

procedure TfrmPedidoVendaSimples.btSalvarClick(Sender: TObject);
begin
  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasternCdTabStatusPed.Value > 1) and (frmMenu.LeParametro('ALTPEDFIN') = 'N') then
      begin
          MensagemAlerta('Pedido Finalizado n�o pode ser alterado.') ;
          PosicionaQuery(qryMaster, DBEdit1.Text);
          abort ;
      end ;
  end ;

  inherited;

end;

procedure TfrmPedidoVendaSimples.DBEdit19Exit(Sender: TObject);
begin
  inherited;

  {-- Se codigo incoterms for em branco, limpa os campos e habilita o campo modelo de frete --}
  if (Trim(DBEdit19.Text) = '') then
  begin
      ativaDBEdit(DBEdit43);

      qryMasternCdTabTipoModFrete.Clear;
      qryIncoterms.Close;
      qryTabTipoModFrete.Close;

      Abort;
  end;

  {-- Posiciona query incoterms--}
  PosicionaQuery(qryIncoterms,DbEdit19.Text) ;

  {-- Utilizando o incoterms o modo do frete � setado automaticamente conforme o incoterms selecionado --}
  if (not qryIncoterms.IsEmpty) then
  begin
      case (qryMasternCdIncoterms.Value) of
          1 : PosicionaQuery(qryTabTipoModFrete, '0');
          2 : PosicionaQuery(qryTabTipoModFrete, '1');
      end;

      qryMasternCdTabTipoModFrete.Value := qryTabTipoModFretenCdTabTipoModFrete.Value;
      desativaDBEdit(DBEdit43);
  end;
end;

procedure TfrmPedidoVendaSimples.DBEdit8Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroTransp, DbEdit8.Text) ;

end;

procedure TfrmPedidoVendaSimples.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;
  if (dbgridEh1.Col = 6) and (qryItemEstoque.State <> dsBrowse) and (qryItemEstoquenValTotalItem.Value = 0) then
  begin

      sp_tab_preco.Close;
      sp_tab_preco.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
      sp_tab_preco.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
      sp_tab_preco.ExecProc;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value = -1.00 ) then
      begin

          if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') then
          begin
              MensagemAlerta('Tabela de pre�o n�o encontrada. Verifique.') ;
              abort ;
          end ;

      end ;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value > 0) then
      begin

          qryItemEstoquenValUnitario.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValSugVenda.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValDesconto.Value  := sp_tab_preco.Parameters.ParamByName('@nValDesconto').Value  ;
          qryItemEstoquenValAcrescimo.Value := sp_tab_preco.Parameters.ParamByName('@nValAcrescimo').Value ;
          qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value + qryItemEstoquenValFrete.Value) ;
          qryItemEstoquenValTotalItem.Value := ArredondaDecimal((qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value),2) ;

      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh4KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemAD.State = dsBrowse) then
             qryItemAD.Edit ;
        
        if (qryItemAD.State = dsInsert) or (qryItemAD.State = dsEdit) then
        begin

            if (dbGridEh4.Col = 12) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(73);

                If (nPK > 0) then
                begin
                    qryItemADnCdGrupoImposto.Value := nPK ;
                    PosicionaQuery(qryGrupoImposto, qryItemADnCdGrupoImposto.AsString) ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryItemADCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryGrupoImposto, qryItemADnCdGrupoImposto.AsString) ;

  if not qryGrupoImposto.eof then
      qryItemADcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value ;
      
end;

procedure TfrmPedidoVendaSimples.DBMemo4Exit(Sender: TObject);
begin
  inherited;

  if (qryDescrTecnica.Active) then
  begin
      if (qryDescrTecnica.State = dsEdit) then
      begin
          qryDescrTecnica.Post ;
      end ;
  end;    
end;

procedure TfrmPedidoVendaSimples.TabDesTecnicaEnter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active = false then exit;
  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;

end;

procedure TfrmPedidoVendaSimples.ExibirAtendimentosdoItem1Click(
  Sender: TObject);
var
objForm :  TfrmItemPedidoAtendido ;
begin
  inherited;

  objForm := TfrmItemPedidoAtendido.Create(nil);
  Try
      Try

          if (cxPageControl1.ActivePage = TabItemEstoque) then
          begin
              if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
              begin
                  PosicionaQuery(objForm.qryItem, qryItemEstoquenCdItemPedido.AsString) ;
                  showForm(objForm,false);
              end;
          end ;

          if (cxPageControl1.ActivePage = TabAD) then
          begin
              if (qryItemAD.Active) and (qryItemADnCdItemPedido.Value > 0) then
              begin
                  PosicionaQuery(objForm.qryItem, qryItemADnCdItemPedido.AsString) ;
                  showForm(objForm,false);
              end ;
          end ;

          if (cxPageControl1.ActivePage = TabFormula) then
          begin
              if (qryItemFormula.Active) and (qryItemFormulanCdItemPedido.Value > 0) then
              begin
                  PosicionaQuery(objForm.qryItem, qryItemFormulanCdItemPedido.AsString) ;
                  showForm(objForm,false);
              end ;
          end ;
      except
          MensagemErro('Erro na cria��o da Consulta');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

end;

procedure TfrmPedidoVendaSimples.DBEdit25Exit(Sender: TObject);
begin
  inherited;
  if qryMaster.Active = false then exit;

  qryTerceiroRepres.Close ;

  if (bRepresentante) and (Trim(DBEdit25.Text) <> '') then
  begin
      PosicionaQuery(qryTerceiroRepres, IntToStr(frmMenu.nCdTerceiroUsuario));
      qryMasternCdTerceiroRepres.Value := qryTerceiroRepresnCdTerceiro.Value;
  end
  else
      PosicionaQuery(qryTerceiroRepres, Trim(DBEdit25.Text));

  if (Trim(DBEdit25.Text) <> '') then
  begin
      { verifica se representante est� vinculado ao terceiro do pedido }
      qryTerceiroRepresentante.Close;
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.ConvInteiro(DBEdit25.Text);
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(DBEdit5.Text);
      qryTerceiroRepresentante.Open;

      if (qryTerceiroRepresentante.IsEmpty) then
      begin
          MensagemAlerta('Representante n�o pertence ao Terceiro do pedido.');
          qryTerceiroRepres.Close;
          DbEdit25.Text := '';
          DbEdit25.SetFocus;
          Abort;
      end;
  end;
end;

procedure TfrmPedidoVendaSimples.DBEdit11Enter(Sender: TObject);
begin
  inherited;
  if (DBEdit11.Text = '') and (qryMaster.State = dsInsert) then
      btContatoPadrao.Click;

end;

procedure TfrmPedidoVendaSimples.btContatoPadraoClick(Sender: TObject);
begin
  inherited;
  if not (qryMaster.Active) then
      exit ;

  if (qryMaster.State = dsBrowse) then
      qryMaster.Edit ;

  if (not qryTerceiro.Active) then
  begin
      MensagemAlerta('Necessario informar Terceiro.');
      DBEdit5.SetFocus;
      exit;
  end;

  qryContatoPadrao.Close ;
  PosicionaQuery(qryContatoPadrao, qryTerceironCdTerceiro.AsString) ;

  if not qryContatoPadrao.Eof then
      qryMastercNmContato.Value := qryContatoPadraocNmContato.Value ;

end;

procedure TfrmPedidoVendaSimples.qryTipoPedidoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  {DBEdit30.ReadOnly   := false ;
  DBEdit30.Color      := clWhite ;
  DBEdit30.Font.Color := clBlack ;

  if (qryTipoPedido.eof) or (qryTipoPedidocFlgPermAbatDevVenda.Value = 0) then
  begin
      DBEdit30.ReadOnly   := True ;
      DBEdit30.Color      := $00E9E4E4 ;
      DBEdit30.Font.Color := clBlack ;

      if (qryMaster.State in [dsInsert,dsEdit]) then
      begin
          qryMasternValDevoluc.Value := 0 ;
      end ;
      
  end ; } {-- bloco removido pois nao sera utilizado o valor de devolu��o --}

  btAcompanharProducao.Visible := (qryTipoPedidocFlgGeraOP.Value = 1) ;

end;

procedure TfrmPedidoVendaSimples.DBEdit32Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active = False) then
      Exit;

  if not (qryTerceiro.IsEmpty) then
  begin
      qryEnderecoEntrega.Close;
      qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      PosicionaQuery(qryEnderecoEntrega, DBEdit32.Text) ;

      if (qryEnderecoEntregacNmStatus.Value = 'Inativo') then
          MensagemAlerta('Este endere�o est� inativo. Verifique.') ;
  end;
end;

procedure TfrmPedidoVendaSimples.DBEdit32Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active = False) then
      Exit;

  if (qryTerceiro.IsEmpty) then
  begin
      if not (qryEnderecoEntrega.IsEmpty) then
          qryEnderecoEntrega.Close;

      Exit;
  end;

end;

procedure TfrmPedidoVendaSimples.DBEdit32KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit15.Text) = '') then
            begin
                MensagemAlerta('Informe o Terceiro.') ;
                DBEdit5.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(206,'Endereco.nCdTerceiro = ' + qryMasternCdTerceiro.AsString);

            If (nPK > 0) then
            begin
                qryMasternCdEnderecoEntrega.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;


procedure TfrmPedidoVendaSimples.CalculaDescontoTotalPedido(
  nPercDesconto, nValDesconto, nValLiquido: double);
var
  nValProdutosAux, nValDescontoAux, nValAcrescimoAux : double;
begin
    //remover o desconto
    PosicionaQuery(qryItemFormula,intToStr(qryMasternCdPedido.Value));
    PosicionaQuery(qryItemEstoque,intToStr(qryMasternCdPedido.Value));

    qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
    qryProduto.Close ;
    PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

    qryMaster.Edit;
    qryMasternValDesconto.Value  := 0;
    qryMasternPercDesconto.Value := 0;
    qryMaster.Post;

    // itens grade
    qryItemEstoque.First ;
    while (qryItemEstoque.Active and not qryItemEstoque.eof) do
    begin
        qryItemEstoque.Edit;
        qryItemEstoquenValDesconto.Value  := 0 ;

        if (nPercDesconto > 0) then
            qryItemEstoquenValDesconto.Value := ArredondaDecimal((qryItemEstoquenValUnitario.Value * (nPercDesconto / 100)),2) ;

        qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValFrete.Value;

        qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value);
        qryItemEstoque.Post;
        qryItemEstoque.Next ;
    end ;

    //itens formulado
    qryItemFormula.First ;
    while (qryItemFormula.Active and not qryItemFormula.eof) do
    begin

        bPosicionaItens := false;

        qryItemFormula.Edit;
        qryItemFormulanValDesconto.Value  := 0 ;
        qryItemFormulanValCustoUnit.Value := qryItemFormulanValUnitario.Value;
        qryItemFormulanValTotalItem.Value := (qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value);
        qryItemFormula.Post;
        qryItemFormula.Next ;
    end ;


  // calcula o total do pedido
  {-- Verificar porque faz esse calculo novamente --}
  {-- consultar tambem, porque o valor do pedido muda quando manda finalizar --}

  nValProdutosAux  := 0 ;
  nValDescontoAux  := 0 ;
  nValAcrescimoAux := 0 ;

  qryItemEstoque.First ;

  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      while not qryItemEstoque.Eof do
      begin
          nValProdutosAux  := nValProdutosAux  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
          nValDescontoAux  := nValDescontoAux  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDescAbatUnit.Value)  ;
          nValDescontoAux  := nValDescontoAux  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value)  ;
          nValAcrescimoAux := nValAcrescimoAux + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  qryItemAD.First ;

  if qryItemAD.Active and not qryItemAD.eof then
  begin

      qryItemAD.First ;

      while not qryItemAD.Eof do
      begin
          nValProdutosAux  := nValProdutosAux + qryItemADnValTotalItem.Value ;
          qryItemAD.Next ;
      end ;

      qryItemAD.First ;

  end ;

  qryItemFormula.First ;

  if qryItemFormula.Active and not qryItemFormula.eof then
  begin

      while not qryItemFormula.Eof do
      begin
          nValProdutosAux  := nValProdutosAux  + (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value) ;
          nValDescontoAux  := nValDescontoAux  + (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
          nValAcrescimoAux := nValAcrescimoAux + (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;
          qryItemFormula.Next ;
      end ;

      qryItemFormula.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value  := nValProdutosAux  ;
  qryMasternValDesconto.Value  := nValDescontoAux  ;
  qryMasternValAcrescimo.Value := nValAcrescimoAux ;
  qryMaster.Post ;


    {-- arredonda o percentual do desconto e passa para valor decimal --}
    //nValPercDesconto := frmMenu.TBRound(nValPercDesconto,2)/100;

    //calcular o desconto
{    if ((nValDesconto > 0) and (nValDesconto < qryMasternValProdutos.Value)) then
    begin

        qryAplicaDesconto.Close;
        qryAplicaDesconto.Parameters.ParamByName('nValLiquido').Value := nValLiquido;
        qryAplicaDesconto.Parameters.ParamByName('nCdPedido').Value   := qryMasternCdPedido.Value;
        qryAplicaDesconto.ExecSQL;

      {
        PosicionaQuery(qryItemFormula,intToStr(qryMasternCdPedido.Value));
        PosicionaQuery(qryItemEstoque,intToStr(qryMasternCdPedido.Value));

        // itens grade
        qryItemEstoque.First ;
        while (qryItemEstoque.Active and not qryItemEstoque.eof) do
        begin

            qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
            qryProduto.Close ;
            PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

            qryItemEstoque.Edit;
            qryItemEstoquenValDesconto.Value  := (nValPercDesconto                  * qryItemEstoquenValUnitario.Value);
            qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value  - qryItemEstoquenValDesconto.Value);
            qryItemEstoquenValTotalItem.Value := ArredondaDecimal((qryItemEstoquenQtdePed.Value      * qryItemEstoquenValCustoUnit.Value),2);
            qryItemEstoque.Post;
            qryItemEstoque.Next ;
        end ;

        //itens formulado
        qryItemFormula.First ;
        while (qryItemFormula.Active and not qryItemFormula.eof) do
        begin

            qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
            qryProduto.Close ;
            PosicionaQuery(qryProduto, qryItemFormulanCdProduto.asString) ;

            bPosicionaItens := false;

            qryItemFormula.Edit;
            qryItemFormulanValDesconto.Value  := (nValPercDesconto * qryItemFormulanValUnitario.Value)  ;
            qryItemFormulanValCustoUnit.Value := (qryItemFormulanValUnitario.Value - qryItemFormulanValDesconto.Value);
            qryItemFormulanValTotalItem.Value := ArredondaDecimal((qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value),2);
            qryItemFormula.Post;
            qryItemFormula.Next ;
        end ;

        }
{
    end;}

    PosicionaQuery(qryMaster,intToStr(qryMasternCdPedido.Value));

    PosicionaQuery(qryItemFormula,intToStr(qryMasternCdPedido.Value));
    PosicionaQuery(qryItemEstoque,intToStr(qryMasternCdPedido.Value));

    qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
    qryProduto.Close ;
    PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

end;

procedure TfrmPedidoVendaSimples.btSituacaoEstoqueClick(Sender: TObject);
var
  objCons : TfrmSituacaoEstoqueProduto ;
begin
  inherited;
  objCons := TfrmSituacaoEstoqueProduto.Create(Self);

  if not qryItemEstoque.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  Try
      Try
          if (qryItemEstoquenCdProduto.Value > 0) then
              objCons.exibeSituacaoProduto(frmMenu.nCdEmpresaAtiva, qryItemEstoquenCdProduto.Value) ;
      except
          MensagemErro('Erro na cria��o da Consulta');
          raise;
      end;
  finally
      FreeAndNil(objCons);
  end;

end;

procedure TfrmPedidoVendaSimples.btAcompanharProducaoClick(Sender: TObject);
var
    objForm  : TfrmAcompProducaoPedidoVenda ;
    bGerarOP : boolean ;
begin
  inherited;

  if (not qryMaster.Active) or (qryMasternCdPedido.Value = 0) then
  begin

      MensagemAlerta('Nehum Pedido Ativo.') ;
      abort ;

  end ;

  bGerarOP := True ;

  if (qryMasternCdTabStatusPed.Value = 1) then
  begin
      MensagemAlerta('O pedido n�o foi finalizado. Nenhuma ordem de produ��o ser� gerada.') ;
      bGerarOP := False ;
  end ;

  if (qryMasternCdTabStatusPed.Value = 2) then
      MensagemAlerta('O pedido est� aguardando autoriza��o do financeiro. A Ordem de produ��o ser� gerada como planejada.') ;

  if (qryMasternCdTabStatusPed.Value = 5) then
  begin
      MensagemAlerta('O pedido est� atendido totalmente. Nenhuma ordem de produ��o ser� gerada.') ;
      bGerarOP := False ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 5) then
  begin
      MensagemAlerta('O status do pedido n�o permite a gera��o de nenhuma ordem de produ��o.') ;
      bGerarOP := False ;
  end ;

  objForm          := TfrmAcompProducaoPedidoVenda.Create( Self ) ;
  objForm.bGerarOP := bGerarOP ;

  objForm.exibeAcompanhamento( qryMasternCdPedido.Value ) ;

  freeAndNil( objForm ) ;

end;

procedure TfrmPedidoVendaSimples.qryTipoPedidoAfterClose(
  DataSet: TDataSet);
begin
  inherited;
  btAcompanharProducao.Visible := False ;

end;

procedure TfrmPedidoVendaSimples.edtDivisaoVendaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
      exit;
      
  qryDivisaoVenda.Parameters.ParamByName('nCdAreaVenda').Value := edtAreaVenda.Text;
end;

procedure TfrmPedidoVendaSimples.edtDivisaoVendaBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
  begin
      MensagemAlerta('Para selecionar a Divis�o de Venda, selecione a �rea de Venda.');
      edtAreaVenda.SetFocus;
      abort;
  end;

  edtDivisaoVenda.WhereAdicional.Text := 'nCdStatus = 1 AND nCdAreaVenda = ' + edtAreaVenda.Text;

end;

procedure TfrmPedidoVendaSimples.DuplicarPedido1Click(Sender: TObject);
var
  nCdPedidoGerado : integer;
begin
  inherited;

  if (not qryMaster.Active) then
      abort;
      
  if (MessageDLG('Confirma a Duplica��o deste Pedido ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  frmMenu.Connection.BeginTrans;

  try
      SP_DUPLICA_PEDIDO.Close;
      SP_DUPLICA_PEDIDO.Parameters.ParamByName('@nCdPedidoDuplicar').Value := qryMasternCdPedido.Value;
      SP_DUPLICA_PEDIDO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento');
      raise
  end;

  frmMenu.Connection.CommitTrans;

  nCdPedidoGerado := SP_DUPLICA_PEDIDO.Parameters.ParamByName('@nCdPedido').Value;

  btCancelar.Click;
  
  PosicionaQuery(qryMaster,IntToStr(nCdPedidoGerado));


end;

procedure TfrmPedidoVendaSimples.CancelarPedido1Click(Sender: TObject);
var
  objForm : TfrmMotivoCancelaSaldoPedido ;
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 3) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  {-- abre o form que solicita o motivo de cancelamento e PROCESSA o cancelamento do pedido --}
  objForm := TfrmMotivoCancelaSaldoPedido.Create( Self ) ;

  try
      objForm.cancelaSaldoPedido( qryMasternCdPedido.Value , 0 ) ;
  finally
      freeAndNil( objForm ) ;
  end ;

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

end;

procedure TfrmPedidoVendaSimples.qryPedidoPromotorCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPedidoPromotor.Active) and (qryPedidoPromotor.State = dsCalcFields) then
  begin

      if (qryPedidoPromotornCdTerceiroPromotor.Value > 0) and (qryPedidoPromotorcNmTerceiro.Value = '') then
      begin
          qryTerceiroPromotor.Close;
          PosicionaQuery( qryTerceiroPromotor, qryPedidoPromotornCdTerceiroPromotor.AsString );

          if not qryTerceiroPromotor.eof then
              qryPedidoPromotorcNmTerceiro.Value := qryTerceiroPromotorcNmTerceiro.Value ;
      end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.qryPedidoPromotornCdTerceiroPromotorChange(
  Sender: TField);
begin
  inherited;

  qryPedidoPromotorcNmTerceiro.Value := '' ;

  if (qryPedidoPromotornCdTerceiroPromotor.Value > 0) and (qryPedidoPromotorcNmTerceiro.Value = '') then
  begin
      qryTerceiroPromotor.Close;
      PosicionaQuery( qryTerceiroPromotor, qryPedidoPromotornCdTerceiroPromotor.AsString );

      if not qryTerceiroPromotor.eof then
          qryPedidoPromotorcNmTerceiro.Value := qryTerceiroPromotorcNmTerceiro.Value ;
  end ;

  
end;

procedure TfrmPedidoVendaSimples.qryPedidoPromotorBeforePost(
  DataSet: TDataSet);
begin

  if (qryPedidoPromotorcNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Terceiro promotor inv�lido.') ;
      abort;
  end ;

  inherited;

  if (qryPedidoPromotor.State = dsInsert) then
  begin
      qryPedidoPromotornCdPedido.Value         := qryMasternCdPedido.Value ;
      qryPedidoPromotornCdPedidoPromotor.Value := frmMenu.fnProximoCodigo('PEDIDOPROMOTOR') ;
  end ;

end;

procedure TfrmPedidoVendaSimples.DBGridEh9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryPedidoPromotor.State = dsBrowse) then
             qryPedidoPromotor.Edit ;

        if (qryPedidoPromotor.State = dsInsert) or (qryPedidoPromotor.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(215);

            If (nPK > 0) then
            begin
                qryPedidoPromotornCdTerceiroPromotor.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.incluirProdutoMultiplo;
var
  objFormAgrupado : TfrmPedidoVendaSimples_SelecaoAgrupada ;
begin
  inherited;

  { -- produtos grade n�o utilizam est� funcionalidade, para habilitar esta fun��o � necess�rio -- }
  { -- corrigir o modo de inser��o abaixo devido a utiliza��o de sub itens (tipo 4 ) no pedido  -- }
  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      MensagemAlerta('Produtos Grade n�o podem utilizar est� fun��o.');
      Abort;
  end;

  if (not qryMaster.Active) or (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      abort ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O Status do pedido n�o permite altera��o. Utilize a reabertura de pedidos para alter�-lo.') ;
      abort ;
  end ;

  objFormAgrupado := TfrmPedidoVendaSimples_SelecaoAgrupada.Create( Self ) ;

  try
      objFormAgrupado.nCdTipoPedido := qryMasternCdTipoPedido.Value ;
      objFormAgrupado.nCdPedido     := qryMasternCdPedido.Value ;
      objFormAgrupado.preparaSelecaoAgrupada ;

      if (objFormAgrupado.qryTempAnalitica.Active) and (objFormAgrupado.qryTempAnalitica.RecordCount > 0) then
      begin

          frmMenu.Connection.BeginTrans ;

          try
              {-- exclui os itens atuais do pedido --}
              qryExcluirItens.Close;
              qryExcluirItens.Parameters.ParamByName('nCdPedido').Value := qryMasternCdPedido.Value;
              qryExcluirItens.ExecSQL;

              objFormAgrupado.qryTempAnalitica.First;

              qryMaster.DisableControls;
              qryItemEstoque.DisableControls;

              PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

              while not objFormAgrupado.qryTempAnalitica.eof do
              begin
                  qryMaster.Edit;
                  qryItemEstoque.Insert;
                  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;
                  qryItemEstoquenCdProduto.Value     := objFormAgrupado.qryTempAnaliticanCdProduto.Value ;
                  qryItemEstoquecCdProduto.Value     := objFormAgrupado.qryTempAnaliticanCdProduto.asString;
                  qryItemEstoquenCdTipoItemPed.Value := 2 ; {-- item estoque sem grade --}
                  qryItemEstoquecNmItem.Value        := objFormAgrupado.qryTempAnaliticacNmProduto.Value ;
                  qryItemEstoquenQtdePed.Value       := objFormAgrupado.qryTempAnaliticanQtdePed.Value ;
                  qryItemEstoquenValUnitario.Value   := objFormAgrupado.qryTempAnaliticanValUnitario.Value ;
                  qryItemEstoquenValSugVenda.Value   := objFormAgrupado.qryTempAnaliticanValUnitario.Value ;
                  qryItemEstoquenValDesconto.Value   := objFormAgrupado.qryTempAnaliticanValDescontoUnit.Value ;
                  qryItemEstoquenValCustoUnit.Value  := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValFrete.Value) ;
                  qryItemEstoquenValTotalItem.Value  := objFormAgrupado.qryTempAnaliticanValTotalItem.Value ;
                  qryItemEstoque.Post;

                  objFormAgrupado.qryTempAnalitica.Next ;
              end ;

              objFormAgrupado.qryTempAnalitica.Close;

              qryMaster.EnableControls;
              qryItemEstoque.EnableControls;

              PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

              cxPageControl1.ActivePageIndex := 0 ;
              qryItemEstoque.Last;

              DBGridEh1.SetFocus;
              DBGridEh1.Col := 1 ;

          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise;
          end ;

          frmMenu.Connection.CommitTrans;

      end ;

  finally
      freeAndNil( objFormAgrupado ) ;
  end ;

end;

procedure TfrmPedidoVendaSimples.PopupMenu2Popup(Sender: TObject);
begin
  inherited;

  btTerceiro.Enabled      := frmMenu.fnValidaUsuarioAPL('FRMTERCEIRO');
  btTipodePedido.Enabled  := frmMenu.fnValidaUsuarioAPL('FRMTIPOPEDIDO');
  btCondPagto.Enabled     := frmMenu.fnValidaUsuarioAPL('FRMCONDPAGTO');
  btTabelaPreco.Enabled   := frmMenu.fnValidaUsuarioAPL('FRMTABPRECO');
  btProdGrade.Enabled     := frmMenu.fnValidaUsuarioAPL('FRMPRODUTO');
  btProdERP.Enabled       := frmMenu.fnValidaUsuarioAPL('FRMPRODUTOERP');
  btEmitirPicking.Enabled := frmMenu.fnValidaUsuarioAPL('FRMSEPARAEMBALAPEDIDO');
  btSeparaoEmb.Enabled    := frmMenu.fnValidaUsuarioAPL('FRMSEPARAEMBALAPEDIDO');
end;

procedure TfrmPedidoVendaSimples.btTerceiroClick(Sender: TObject);
var
  objForm : TfrmTerceiro ;
begin
  inherited;

  objForm := TfrmTerceiro.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmPedidoVendaSimples.btTipodePedidoClick(Sender: TObject);
var
  objForm : TfrmTipoPedido ;
begin
  inherited;

  objForm := TfrmTipoPedido.Create( Self ) ;
  showForm( objForm , TRUE ) ;
end;

procedure TfrmPedidoVendaSimples.btCondPagtoClick(Sender: TObject);
var
  objForm : TfrmCondPagto ;
begin
  inherited;

  objForm := TfrmCondPagto.Create( Self ) ;
  showForm( objForm , TRUE ) ;
end;

procedure TfrmPedidoVendaSimples.btTabelaPrecoClick(Sender: TObject);
var
  objForm : TfrmTabPreco ;
begin
  inherited;

  objForm := TfrmTabPreco.Create( Self ) ;
  showForm( objForm , TRUE ) ;
end;

procedure TfrmPedidoVendaSimples.btProdGradeClick(Sender: TObject);
var
  objForm : TfrmProduto;
begin
  inherited;

  objForm := TfrmProduto.Create( Self ) ;
  showForm( objForm , TRUE ) ;
end;

procedure TfrmPedidoVendaSimples.btProdERPClick(Sender: TObject);
var
  objForm : TfrmProdutoERP ;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self ) ;
  showForm( objForm , TRUE ) ;
end;

procedure TfrmPedidoVendaSimples.btEmitirPickingClick(Sender: TObject);
var
  objForm : TfrmGerarPickingList ;
begin
  inherited;

  if (qryMaster.isEmpty) then
  begin
      MensagemAlerta('Nenhum pedido selecionado.') ;
      abort;
  end ;

  if (    (qryMasternCdTabStatusPed.Value <> 3)
      and (qryMasternCdTabStatusPed.Value <> 4)) then
  begin
      MensagemAlerta('O Status do pedido n�o permite gera��o de picking list.') ;
      abort;
  end ;

  objForm := TfrmGerarPickingList.Create( Self ) ;

  objForm.nCdPedido         := qryMasternCdPedido.Value;
  objForm.cFlgOrdemProducao := 0; {-- informa que o pedido n�o tem OP Vinculada --}
  objForm.nCdOrdemProducao  := 0;
  objForm.cOBS              := qryMastercOBS.Value;

  showForm( objForm , True ) ;

end;

procedure TfrmPedidoVendaSimples.btSeparaoEmbClick(Sender: TObject);
var
  objForm : TfrmSeparaEmbalaPedido ;
begin
  inherited;

  objForm := TfrmSeparaEmbalaPedido.Create( Self ) ;
  showForm( objForm , True ) ;

end;

function TfrmPedidoVendaSimples.ArredondaDecimal(Value: extended;
  Decimals: integer): extended;
var
    Factor,Fraction : extended ;
    i, Rest         : integer;
begin
    Factor   := IntPower(10,Decimals);
    Value    := StrToFloat(FloatToStr(Value * Factor));
    Result   := Int(Value);
    Fraction := Frac(Value);

    for i := 3 downto 1 do
    Begin
        Fraction := Fraction * intPower(10,i);

        if (frac(Fraction) > 0.5) then
            Fraction :=Fraction + 1 - Frac(Fraction)
        else if (Frac(Fraction) < 0.5) then
            Fraction := Fraction - Frac(Fraction);

        Fraction := Fraction / intPower(10,i);
    end;

    if ((((Result/2) - Int(Result/2) > 0) OR ( Fraction > 0.5))) then
    BEGIN
        IF (Fraction >= 0.5) then
            Result := Result +1
        Else
            if (Fraction <= -0.5) then
                Result :=Result - 1;
    END;

    Result := Result/Factor;

end;

procedure TfrmPedidoVendaSimples.btConsultarClick(Sender: TObject);
var
  nPK : Integer;
begin
  //inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta2(96,'cFlgPedidoWeb = 0');

  PosicionaPK(nPK);
end;

procedure TfrmPedidoVendaSimples.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  { -- destr�i objetos globais da mem�ria --
  FreeAndNil(objGrade);                      }
end;

procedure TfrmPedidoVendaSimples.DBEdit30Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Open;
end;

procedure TfrmPedidoVendaSimples.DBEdit30KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  if (DBEdit30.ReadOnly) then
      Exit;

  case key of
      vk_F4 : begin
                  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
                  begin
                      nPK := frmLookup_Padrao.ExecutaConsulta(59);

                      if (nPK > 0) then
                          qryMasternCdLoja.Value := nPK;
                  end;
              end;
  end;
end;

procedure TfrmPedidoVendaSimples.DBEdit43KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
 nPK  : integer;  
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(776);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoModFrete.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaSimples.DBEdit43Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTabTipoModFrete,qryMasternCdTabTipoModFrete.AsString) ;
end;

initialization
    RegisterClass(TfrmPedidoVendaSimples) ;

end.
