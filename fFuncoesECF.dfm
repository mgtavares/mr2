inherited frmFuncoesECF: TfrmFuncoesECF
  Left = 282
  Top = 183
  Width = 740
  BorderIcons = [biSystemMenu]
  Caption = 'Fun'#231#245'es ECF'
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 46
    Width = 724
    Height = 396
  end
  object Label2: TLabel [1]
    Left = 0
    Top = 29
    Width = 724
    Height = 17
    Align = alTop
    AutoSize = False
    Caption = 'Resposta'
    Layout = tlBottom
  end
  inherited ToolBar1: TToolBar
    Width = 724
    Visible = False
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object mResp: TMemo [3]
    Left = 0
    Top = 46
    Width = 724
    Height = 396
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 13
    Font.Name = 'Courier'
    Font.Pitch = fpVariable
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 1
    WantReturns = False
    WordWrap = False
  end
  object MainMenu1: TMainMenu
    Left = 72
    Top = 341
    object Principal1: TMenuItem
      Caption = 'Principal'
      object Ativcar1: TMenuItem
        Caption = 'Ativar'
        OnClick = Ativcar1Click
      end
      object Desativar1: TMenuItem
        Caption = 'Desativar'
        OnClick = Desativar1Click
      end
      object Testar1: TMenuItem
        Caption = 'Testar'
        OnClick = Testar1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Sair1: TMenuItem
        Caption = 'Sair'
        OnClick = Sair1Click
      end
    end
    object Variaveis1: TMenuItem
      Caption = 'Variaveis'
      object Equipamento1: TMenuItem
        Caption = 'Equipamento'
        object Estado1: TMenuItem
          Caption = 'Estado'
          OnClick = Estado1Click
        end
        object DataHora1: TMenuItem
          Caption = 'Data Hora'
          OnClick = DataHora1Click
        end
        object NumECF1: TMenuItem
          Caption = 'Num ECF'
          OnClick = NumECF1Click
        end
        object NumLoja1: TMenuItem
          Caption = 'Num Loja'
          OnClick = NumLoja1Click
        end
        object NSrie1: TMenuItem
          Caption = 'Num S'#233'rie'
          OnClick = NSrie1Click
        end
        object NVerso1: TMenuItem
          Caption = 'Num Vers'#227'o'
          OnClick = NVerso1Click
        end
        object CNPJIE1: TMenuItem
          Caption = 'CNPJ'
          OnClick = CNPJIE1Click
        end
        object IE1: TMenuItem
          Caption = 'IE'
          OnClick = IE1Click
        end
      end
      object N19: TMenuItem
        Caption = '-'
      end
      object Aliquotas1: TMenuItem
        Caption = 'Aliquotas'
        object AliquotasICMS1: TMenuItem
          Caption = 'Carrega Aliquotas (ICMS)'
          OnClick = AliquotasICMS1Click
        end
        object LerTotaisAliquotas1: TMenuItem
          Caption = 'Ler Totais Aliquotas'
          OnClick = LerTotaisAliquotas1Click
        end
      end
      object FormasdePagamento2: TMenuItem
        Caption = 'Formas de Pagamento'
        object FormasdePagamento1: TMenuItem
          Caption = 'Carrega Formas Pagamento'
          OnClick = FormasdePagamento1Click
        end
        object LerTotaisFormadePagamento1: TMenuItem
          Caption = 'Ler Totais Forma de Pagamento'
          OnClick = LerTotaisFormadePagamento1Click
        end
      end
      object ComprovantesNaoFiscais1: TMenuItem
        Caption = 'Comprovantes Nao Fiscais'
        object CarregaComprovantesNAOFiscais1: TMenuItem
          Caption = 'Carrega Comprovantes Nao Fiscais'
          OnClick = CarregaComprovantesNAOFiscais1Click
        end
        object LerTotaisComprovanetNaoFiscal1: TMenuItem
          Caption = 'Ler Totais Comprovante Nao Fiscal'
          OnClick = LerTotaisComprovanetNaoFiscal1Click
        end
      end
      object CarregaUnidadesdeMedida1: TMenuItem
        Caption = 'Carrega Unidades de Medida'
        OnClick = CarregaUnidadesdeMedida1Click
      end
    end
    object Relatrios1: TMenuItem
      Caption = 'Relat'#243'rios'
      object LeituraX1: TMenuItem
        Caption = 'Leitura X'
        OnClick = LeituraX1Click
      end
      object ReduoZ1: TMenuItem
        Caption = 'Redu'#231#227'o Z'
        OnClick = ReduoZ1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object MapaResumo1: TMenuItem
        Caption = 'Mapa Resumo'
        OnClick = MapaResumo1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object PularLinhas1: TMenuItem
        Caption = 'Pular Linhas'
        OnClick = PularLinhas1Click
      end
      object CortaPapel1: TMenuItem
        Caption = 'Corta Papel'
        OnClick = CortaPapel1Click
      end
    end
    object Utilitrios1: TMenuItem
      Caption = 'Utilit'#225'rios'
      object ProgramaAliquota1: TMenuItem
        Caption = 'Programa Aliquota'
        OnClick = ProgramaAliquota1Click
      end
      object ProgramaFormadePagamento1: TMenuItem
        Caption = 'Programa Forma de Pagamento'
        OnClick = ProgramaFormadePagamento1Click
      end
      object ProgramaUnidadeMedida1: TMenuItem
        Caption = 'Programa Unidade Medida'
        OnClick = ProgramaUnidadeMedida1Click
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object HorarioVerao1: TMenuItem
        Caption = 'Muda Hor'#225'rio Ver'#227'o'
        OnClick = HorarioVerao1Click
      end
      object MudaArredondamento1: TMenuItem
        Caption = 'Muda Arredondamento'
        OnClick = MudaArredondamento1Click
      end
      object ImpactoAgulhas1: TMenuItem
        Caption = 'Impacto Agulhas'
        OnClick = ImpactoAgulhas1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object CorrigeEstadodeErro1: TMenuItem
        Caption = 'Corrige Estado de Erro'
        OnClick = CorrigeEstadodeErro1Click
      end
    end
  end
  object SP_REGISTRO_MESTRE_ECF_SINTEGRA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRO_MESTRE_ECF_SINTEGRA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMovtoDiaECF'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cCdNumSerieECF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@iNrECF'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cModelo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@iNrOrdemOperInicial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrOrdemOperFinal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrReducaoZ'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrReinicioOper'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValVendaBruta'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValTotalGeral'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 296
    Top = 192
  end
  object SP_REGISTRO_ANALITICO_ECF_SINTEGRA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRO_ANALITICO_ECF_SINTEGRA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMovtoDiaECF'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cCdSTAliq'
        Attributes = [paNullable]
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = '@nValAcumTotalParcial'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 328
    Top = 192
  end
  object qryUpdateMovtoECF: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iNrOrdemOperInicial'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iNrOrdemOperFinal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iNrReducaoZ'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iNrReinicioOper'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdMovtoDiaECF'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE MovtoDiaECF'
      '   SET iNrOrdemOperInicial = :iNrOrdemOperInicial'
      '      ,iNrOrdemOperFinal   = :iNrOrdemOperFinal'
      '      ,iNrReducaoZ         = :iNrReducaoZ'
      '      ,iNrReinicioOper     = :iNrReinicioOper'
      ' WHERE nCdMovtoDiaECF = :nCdMovtoDiaECF'
      '')
    Left = 368
    Top = 192
  end
end
