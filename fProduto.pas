unit fProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, cxPC, cxControls, Menus,
  DBGridEhGrouping, ToolCtrlsEh, ACBrBase, ACBrValidador;

type
  TfrmProduto = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TAutoIncField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    dsCategoria: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    dsSubCategoria: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBEdit9: TDBEdit;
    dsSegmento: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TAutoIncField;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdStatus: TIntegerField;
    Label3: TLabel;
    DBEdit10: TDBEdit;
    Label5: TLabel;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    dsMarca: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhanCdMarca: TIntegerField;
    qryLinhacNmLinha: TStringField;
    qryLinhanCdStatus: TIntegerField;
    DBEdit14: TDBEdit;
    dsLinha: TDataSource;
    qryGrade: TADOQuery;
    qryColecao: TADOQuery;
    qryGradenCdGrade: TAutoIncField;
    qryGradecNmGrade: TStringField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    Label10: TLabel;
    DBEdit16: TDBEdit;
    Label11: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    dsGrade: TDataSource;
    qryColecaonCdColecao: TAutoIncField;
    qryColecaocNmColecao: TStringField;
    DBEdit19: TDBEdit;
    dsColecao: TDataSource;
    Label12: TLabel;
    DBEdit20: TDBEdit;
    Label13: TLabel;
    DBEdit21: TDBEdit;
    qryClasse: TADOQuery;
    qryStatus: TADOQuery;
    qryClassenCdClasseProduto: TAutoIncField;
    qryClassecNmClasseProduto: TStringField;
    DBEdit22: TDBEdit;
    dsClasse: TDataSource;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit23: TDBEdit;
    dsStatus: TDataSource;
    qryProdutoIntermediario: TADOQuery;
    qryProdutoIntermediarionCdProduto: TAutoIncField;
    qryProdutoIntermediariocNmCor: TStringField;
    qryProdutoIntermediariocNmMaterial: TStringField;
    dsProdutoIntermediario: TDataSource;
    qryProdutoIntermediarionQtdeEstoque: TBCDField;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdDepartamento: TIntegerField;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    qryMasternCdLinha: TIntegerField;
    qryMastercReferencia: TStringField;
    qryMasternCdTabTipoProduto: TIntegerField;
    qryMasternCdProdutoPai: TIntegerField;
    qryMasternCdGrade: TIntegerField;
    qryMasternCdColecao: TIntegerField;
    qryMastercNmProduto: TStringField;
    qryMasternCdClasseProduto: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdCor: TIntegerField;
    qryMasternCdMaterial: TIntegerField;
    qryMasteriTamanho: TIntegerField;
    qryMasternQtdeEstoque: TBCDField;
    qryMasternValCusto: TBCDField;
    qryMasternValVenda: TBCDField;
    qryMastercEAN: TStringField;
    qryMastercEANFornec: TStringField;
    qryMasterdDtUltReceb: TDateTimeField;
    qryMasterdDtUltVenda: TDateTimeField;
    qryProdutoIntermediariodDtUltReceb: TDateTimeField;
    qryProdutoIntermediariodDtUltVenda: TDateTimeField;
    qryProdutoIntermediarionCdCor: TAutoIncField;
    qryProdutoIntermediarionCdMaterial: TAutoIncField;
    qryConsultaProd: TADOQuery;
    qryConsultaProdnCdProduto: TIntegerField;
    Label14: TLabel;
    DBEdit24: TDBEdit;
    Label15: TLabel;
    DBEdit25: TDBEdit;
    Label16: TLabel;
    DBEdit26: TDBEdit;
    Label17: TLabel;
    DBEdit27: TDBEdit;
    Label18: TLabel;
    DBEdit28: TDBEdit;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryProdutoIntermediariocNmProduto: TStringField;
    qryMastercUnidadeMedida: TStringField;
    qryMastercFlgProdVenda: TIntegerField;
    qryMastercUnidadeMedidaCompra: TStringField;
    qryMasternFatorCompra: TBCDField;
    qryMasternQtdeMinimaCompra: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryDepartamentonCdGrupoProduto: TIntegerField;
    qryMasternCdGrupo_Produto: TIntegerField;
    Label19: TLabel;
    DBEdit29: TDBEdit;
    Label20: TLabel;
    DBEdit30: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Panel1: TPanel;
    btGerarGrade: TcxButton;
    cxButton1: TcxButton;
    qryMastercFlgProdEstoque: TIntegerField;
    qryMastercFlgProdCompra: TIntegerField;
    qryMastercFlgAtivoFixo: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExcluirProduto1: TMenuItem;
    qryExcluiProdutoIntermediario: TADOQuery;
    ativar: TMenuItem;
    qryStatusGrade: TADOQuery;
    dsStatusGrade: TDataSource;
    qryStatusGradenCdStatus: TIntegerField;
    qryProdutoIntermediarionCdStatus: TIntegerField;
    desativar: TMenuItem;
    qryProdutoIntermediariocNmStatus: TStringField;
    qryMastercFlgProdWeb: TIntegerField;
    qryMasternCdIdExternoWeb: TIntegerField;
    N1: TMenuItem;
    ProdutoWeb2: TMenuItem;
    cbTipoCodProdFat: TDBLookupComboBox;
    Label55: TLabel;
    dsTabTipoCodProdFat: TDataSource;
    qryTabTipoCodProdFat: TADOQuery;
    qryTabTipoCodProdFatnCdTabTipoCodProdFat: TIntegerField;
    qryTabTipoCodProdFatcNmTabTipoCodProdFat: TStringField;
    qryMasternCdTabTipoCodProdFat: TIntegerField;
    Label56: TLabel;
    ACBrValidador: TACBrValidador;
    qryProdutoIntermediariodDtUltRecebCD: TDateTimeField;
    qryMasterdDtUltRecebCD: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBEdit16Exit(Sender: TObject);
    procedure DBEdit20Exit(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure DBEdit17Enter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btGerarGradeClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ExcluirProduto1Click(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure ativarClick(Sender: TObject);
    procedure desativarClick(Sender: TObject);
    procedure ProdutoWeb2Click(Sender: TObject);
    procedure qryMasterAfterOpen(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProduto: TfrmProduto;

implementation

uses fGerarGrade, fLookup_Padrao, fIncLinha, fProduto_Duplicar, fMenu,
  fProdutoPosicaoEstoque, fProduto_DetalheGrade, fProdutoWeb;

{$R *.dfm}

procedure TfrmProduto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster         := 'PRODUTO' ;
  nCdTabelaSistema        := 25 ;
  nCdConsultaPadrao       := 42 ;
  cWhereAddConsultaPadrao := 'nCdGrade IS NOT NULL' ;
  bLimpaAposSalvar        := False ;
end;

procedure TfrmProduto.DBEdit2Exit(Sender: TObject);
begin
  PosicionaQuery(qryDepartamento, DBEdit2.Text) ;

  if (qryDepartamento.Active) then
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TfrmProduto.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmProduto.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategoria, DBEdit4.Text) ;

  if (qryCategoria.Active) then
      qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

end;

procedure TfrmProduto.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubCategoria, DBEdit6.Text) ;

  if (qrySubCategoria.Active) then
  begin
    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
    qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  end;

end;

procedure TfrmProduto.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qrySegmento, DBEdit7.Text) ;
end;

procedure TfrmProduto.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMarca, DBEdit10.Text) ;

  if (qryMarca.Active) then
    qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

end;

procedure TfrmProduto.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLinha, DBEdit11.Text) ;
end;

procedure TfrmProduto.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryGrade, DBEdit15.Text) ;
end;

procedure TfrmProduto.DBEdit16Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryColecao, DBEdit16.Text) ;
end;

procedure TfrmProduto.DBEdit20Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryClasse, DBEdit20.Text) ;

end;

procedure TfrmProduto.DBEdit21Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryStatus, DBEdit21.Text) ;

end;

procedure TfrmProduto.DBEdit17Enter(Sender: TObject);
var
  cDescricao : String;
  cParametro : String;

begin
  inherited;

  if (DbEdit17.Text = '') and (qryMaster.state = dsInsert) then
  begin
      cParametro := frmMenu.LeParametro('FORMATNOMEPROD');

      if (cParametro = '') then
          qryMastercNmProduto.Value := Trim(DBEdit5.Text) + ' ' + Trim(DBEdit8.Text) + ' ' + Trim(DBEdit9.Text) + ' ' + Trim(DBEdit13.Text) + ' ' + Trim(DBEdit19.Text) + ' ' + Trim(DBEdit12.Text)

      else
      begin
          { -- atribui departamento -- }
          if (Pos('DE', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit3.Text)) + ' ';

          { -- atribui categoria -- }
          if (Pos('CA', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit5.Text)) + ' ';

          { -- atribui subcategoria -- }
          if (Pos('SU', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit8.Text)) + ' ';

          { -- atribui segmento -- }
          if (Pos('SE', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit9.Text)) + ' ';

          { -- atribui marca -- }
          if (Pos('MA', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit13.Text)) + ' ';

          { -- atribui linha -- }
          if (Pos('LI', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit14.Text)) + ' ';

          { -- atribui grade -- }
          if (Pos('GR', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit18.Text)) + ' ';

          { -- atribui cole��o -- }
          if (Pos('CO', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit19.Text)) + ' ';

          { -- atribui refer�ncia -- }
          if (Pos('RE', cParametro) > 0) then
              cDescricao := cDescricao + TrimLeft(TrimRight(DBEdit12.Text)) + ' ';

          qryMastercNmProduto.Value := cDescricao;
      end;
  end;
end;

procedure TfrmProduto.btSalvarClick(Sender: TObject);
var
    bInclusao : Boolean ;
begin

  bInclusao := false ;

  if (qryMaster.State = dsInsert) then
  begin
      bInclusao := true ;
      qryMasternCdTabTipoProduto.Value    := 1 ;
      qryMasternCdStatus.Value            := 1 ;
      qryMastercFlgProdVenda.Value        := 1 ;
      qryMastercUnidadeMedida.Value       := 'UN' ;
      qryMastercUnidadeMedidaCompra.Value := 'UN' ;
      qryMasternFatorCompra.Value         := 1 ;
      qryMasternQtdeMinimaCompra.Value    := 1 ;
      qryMasterdDtCad.Value               := Now() ;
      qryMasternCdUsuarioCad.Value        := frmMenu.nCdUsuarioLogado;
      qryMasternCdGrupo_Produto.Value     := qryDepartamentonCdGrupoProduto.Value ;
  end ;

  inherited;

  if (bInclusao) and (qryMasternCdProduto.Value > 0) and (qryMasternCdGrade.Value > 0) then
  begin
  
    case MessageDlg('Deseja gerar grade para deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: btGerarGrade.Click;
    end;

  end ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;

end;

procedure TfrmProduto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;
      
  PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


  PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
  qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryMasternCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, qryMasternCdSubCategoria.asString) ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, qryMasternCdSegmento.asString) ;

  PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;

  qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

  PosicionaQuery(qryLinha, qryMasternCdLinha.asString) ;

  PosicionaQuery(qryGrade, qryMasternCdGrade.asString) ;

  PosicionaQuery(qryColecao, qryMasternCdColecao.asString) ;

  PosicionaQuery(qryClasse, qryMasternCdClasseProduto.asString) ;

  PosicionaQuery(qryStatus, qryMasternCdStatus.asString) ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;

end;

procedure TfrmProduto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryLinha.Close ;
  qryGrade.Close ;
  qryColecao.Close ;
  qryClasse.Close ;
  qryStatus.Close ;
  qryProdutoIntermediario.Close ;
  qryTabTipoCodProdFat.Close ;
end;

procedure TfrmProduto.btGerarGradeClick(Sender: TObject);
var
  objForm : TfrmGerarGrade;
begin
  inherited;
  If (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin
      objForm := TfrmGerarGrade.Create(nil);

      objForm.nCdProduto := qryMasternCdProduto.Value ;
      objForm.qryMaterial.Parameters.ParamByName('nCdMarca').Value := qryMasternCdMarca.Value ;
      showForm(objForm,true);

      PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;
  end ;
end;

procedure TfrmProduto.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(45);
            If (nPK > 0) then
            begin
                qryMasternCdDepartamento.Value := nPK ;
                PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;
                qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

                // consulta de categoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
                If (nPK > 0) then
                begin
                    qryMasternCdCategoria.Value := nPK ;
                    PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
                    qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                    // consulta de subcategoria
                    nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                    If (nPK > 0) then
                    begin
                        qryMasternCdSubCategoria.Value := nPK ;
                        PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.asString) ;

                        qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
                        qryMarca.Parameters.ParamByName('nCdSubCategoria').Value    := qrySubCategorianCdSubCategoria.Value ;

                        // Consulta de segmento
                        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                        If (nPK > 0) then
                        begin
                            qryMasternCdSegmento.Value := nPK ;
                            PosicionaQuery(qrySegmento,qryMasternCdSegmento.AsString) ;

                            // consulta de marca
                            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
                            If (nPK > 0) then
                            begin
                                qryMasternCdMarca.Value := nPK ;
                                PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;
                                qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

                                DBEdit11.SetFocus ;
                            end ;

                        end ;



                    end ;

                end ;

            end ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
            begin
                MensagemAlerta('Selecione um departamento.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
                qryMasternCdCategoria.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSubCategoria.Value := nPK ;
        end ;
    end ;
  end ;
end;

procedure TfrmProduto.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qrySubCategoria.active or (qrySubCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSegmento.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
    objForm : TfrmIncLinha;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryMarca.active or (qryMarcanCdMarca.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Marca.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdStatus = 1 and nCdMarca = ' + qryMarcanCdMarca.asString);
            If (nPK > 0) then
                qryMasternCdLinha.Value := nPK ;
        end ;
    end ;

    73 : begin
        if (qryMaster.State <> dsInsert) then
        begin
            MensagemAlerta('Fun��o s� permitida na inclus�o de produtos.') ;
            exit ;
        end ;

        objForm := TfrmIncLinha.Create(nil);

        objForm.nCdMarca   := qryMarcanCdMarca.Value ;
        objForm.Edit1.Text := '' ;
        showForm(objForm,false) ;

        if (objForm.nCdLinha > 0) then
        begin
            qryMasternCdLinha.Value := objForm.nCdLinha ;
            PosicionaQuery(qryLinha, IntToStr(objForm.nCdLinha)) ;
            //DBEdit12.SetFocus ;
        end ;

    end ;

  end ;

end;

procedure TfrmProduto.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(51,'EXISTS (SELECT 1 FROM ItemGrade WHERE ItemGrade.nCdGrade = Grade.nCdGrade)');
            If (nPK > 0) then
                qryMasternCdGrade.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(52);
            If (nPK > 0) then
                qryMasternCdColecao.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(53);
            If (nPK > 0) then
                qryMasternCdClasseProduto.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            If (nPK > 0) then
                qryMasternCdStatus.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProduto.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) then
      exit ;
      
  qryConsultaProd.Close ;

  if (Trim(dbEdit2.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdDepartamento').Value := StrToInt(Trim(DbEdit2.Text)) ;

  if (Trim(dbEdit4.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdCategoria').Value    := StrToInt(Trim(DbEdit4.Text)) ;

  if (Trim(dbEdit6.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSubCategoria').Value := StrToInt(Trim(DbEdit6.Text)) ;

  if (Trim(dbEdit7.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSegmento').Value     := StrToInt(Trim(DbEdit7.Text)) ;

  if (Trim(dbEdit10.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdMarca').Value        := StrToInt(Trim(DbEdit10.Text)) ;

  if (Trim(dbEdit11.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdLinha').Value        := StrToInt(Trim(DbEdit11.Text)) ;

  if (Trim(dbEdit12.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('cReferencia').Value     := Trim(DbEdit12.Text) ;

  qryConsultaProd.Open ;

  if not qryConsultaProd.eof then
  begin
      PosicionaPK(qryConsultaProdnCdProduto.Value) ;
  end ;

end;

procedure TfrmProduto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
end;

procedure TfrmProduto.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (dbEdit2.Text = '') or (dbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o departamento.') ;
      dbEdit2.SetFocus ;
      Abort ;
  end ;

  If (dbEdit4.Text = '') or (dbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a categoria.') ;
      dbEdit4.SetFocus ;
      Abort ;
  end ;

  If (dbEdit6.Text = '') or (dbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe a Sub Categoria.') ;
      dbEdit6.SetFocus ;
      Abort ;
  end ;

  If (dbEdit7.Text = '') or (dbEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o Segmento.') ;
      dbEdit7.SetFocus ;
      Abort ;
  end ;

  If (dbEdit10.Text = '') or (dbEdit13.Text = '') then
  begin
      MensagemAlerta('Informe a marca.') ;
      dbEdit10.SetFocus ;
      Abort ;
  end ;

  If (dbEdit11.Text = '') or (dbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe a Linha.') ;
      dbEdit11.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If ((dbEdit15.Text <> '') and (dbEdit18.Text = '')) then
  begin
      MensagemAlerta('Informe a Grade.') ;
      dbEdit15.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') and (dbEdit15.Text <> '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If (dbEdit16.Text = '') or (dbEdit19.Text = '') then
  begin
      MensagemAlerta('Informe a Cole��o.') ;
      dbEdit16.SetFocus ;
      Abort ;
  end ;

  If (dbEdit17.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o.') ;
      dbEdit17.SetFocus ;
      Abort ;
  end ;

  { -- valida c�digo de barras -- }
  if (frmMenu.LeParametro('VALIDAEAN') = 'S') then
  begin
      ACBrValidador.TipoDocto   := docGTIN;
      ACBrValidador.Documento   := Trim(qryMastercEAN.Value);
      ACBrValidador.IgnorarChar := '';

      if ((Trim(qryMastercEAN.Value) <> '') and (not ACBrValidador.Validar)) then
      begin
          MensagemAlerta('C�digo de barras inv�lido.');
          DBEdit29.SetFocus;
          Abort;
      end;
  end;

  qryMastercFlgProdEstoque.Value := 1 ;
  qryMastercFlgProdCompra.Value  := 1 ;
  qryMastercFlgAtivoFixo.Value   := 0 ;
  qryMasterdDtCad.Value          := Now ;

  inherited;

end;

procedure TfrmProduto.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmProduto_Duplicar;
begin
  inherited;

  if qryMaster.eof then
  begin
      MensagemAlerta('Nenhum produto ativo para duplicar.') ;
      exit ;
  end ;

  objForm := TfrmProduto_Duplicar.Create(nil);

  objForm.nCdProduto         := qryMasternCdProduto.Value ;
  objForm.cDescricaoAnterior := qryMastercNmproduto.Value ;
  showForm(objForm,false);

  if (objForm.nCdProdutoNovo > 0) then
      PosicionaQuery(qryMaster, IntToStr(objForm.nCdProdutoNovo)) ;

end;

procedure TfrmProduto.DBEdit28Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  begin

      btSalvar.Click;

  end ;

end;

procedure TfrmProduto.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmProduto_DetalheGrade;
begin
  inherited;

  if (qryProdutoIntermediario.Active) then
  begin
      objForm := TfrmProduto_DetalheGrade.Create(nil);

      PosicionaQuery(objForm.qryProdutoFinal, qryProdutoIntermediarionCdProduto.AsString) ;
      showForm(objForm,true) ;
  end ;
  
end;

procedure TfrmProduto.cxButton1Click(Sender: TObject);
var
  objForm : TfrmProduto_DetalheGrade;
begin

  if (qryProdutoIntermediario.Active) then
  begin
      objForm := TfrmProduto_DetalheGrade.Create(nil);

      PosicionaQuery(objForm.qryProdutoFinal, qryProdutoIntermediarionCdProduto.AsString) ;
      showForm(objForm,true) ;
  end ;

end;

procedure TfrmProduto.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  { -- desabilita popup se n�o existir registro carregado -- }
  if (qryMaster.Eof) then
  begin
      PopupMenu1.Items.Items[0].Enabled := False; //Excluir Produto
      PopupMenu1.Items.Items[1].Enabled := False; //Ativar Produto
      PopupMenu1.Items.Items[2].Enabled := False; //Desativar Produto
      PopupMenu1.Items.Items[4].Enabled := False; //Produto Web
      Exit;
  end
  else
  begin
      PopupMenu1.Items.Items[0].Enabled := True; //Excluir Produto
      PopupMenu1.Items.Items[1].Enabled := True; //Ativar Produto
      PopupMenu1.Items.Items[2].Enabled := True; //Desativar Produto
      PopupMenu1.Items.Items[4].Enabled := True; //Produto Web
  end;

  if qryProdutoIntermediario.Active then
  begin

      // Troca os bot�es do menu popup
      if qryProdutoIntermediarionCdStatus.Value = 1 then
      begin
          ativar.Visible := false;
          desativar.Visible := true;
      end;

      if qryProdutoIntermediarionCdStatus.Value = 2 then
      begin
          ativar.Visible := true;
          desativar.Visible := false;
      end;
      
  end;


  ExcluirProduto1.Enabled := (qryProdutoIntermediario.RecordCount > 0) ;

end;

procedure TfrmProduto.ExcluirProduto1Click(Sender: TObject);
begin
  inherited;

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediarionCdProduto.Value > 0) then
  begin

      if (MessageDlg('Confirma a exclus�o do produto ' + qryProdutoIntermediariocNmProduto.Value + '  ?',mtConfirmation,[mbYes,mbNo],0)=MRNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          qryExcluiProdutoIntermediario.Close;
          qryExcluiProdutoIntermediario.Parameters.ParamByName('nCdProduto').Value := qryProdutoIntermediarionCdProduto.Value ;
          qryExcluiProdutoIntermediario.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro ao tentar excluir o produto, o mesmo pode ter depend�ncias.') ;
          raise ;
      end ;

      frmmenu.Connection.CommitTrans;

      qryProdutoIntermediario.Requery();
      
  end ;

end;

procedure TfrmProduto.btExcluirClick(Sender: TObject);
begin

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediario.RecordCount > 0) then
  begin
      MensagemErro('Este produto tem depend�ncias e n�o pode ser exclu�do.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmProduto.ativarClick(Sender: TObject);
begin
  inherited;

  if MessageDLG('Deseja realmente ativar esse produto?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
  begin
      //Inicia uma transa��o

      frmMenu.Connection.BeginTrans;

      try

          // Passa o novo status e o codigo do produto
          qryStatusGrade.Close;
          qryStatusGrade.Parameters.ParamByName('status').Value := 1;
          qryStatusGrade.Parameters.ParamByName('nPK').Value    := qryProdutoIntermediarionCdProduto.Value;
          qryStatusGrade.ExecSQL;

      except

          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento');
          raise;

      end;

      //Conclui a transa��o com sucesso

      frmMenu.Connection.CommitTrans;

      // Atualiza o grid

      qryProdutoIntermediario.Close;
      qryProdutoIntermediario.Open;

  end;

end;

procedure TfrmProduto.desativarClick(Sender: TObject);
begin
  inherited;

  if MessageDLG('Deseja realmente desativar esse produto?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
  begin

      //Inicia uma transa��o

      frmMenu.Connection.BeginTrans;

      try

          // Passa o novo status e o codigo do produto
          qryStatusGrade.Close;
          qryStatusGrade.Parameters.ParamByName('status').Value := 2;
          qryStatusGrade.Parameters.ParamByName('nPK').Value    := qryProdutoIntermediarionCdProduto.Value;
          qryStatusGrade.ExecSQL;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento');
          raise;
      end;

      // Finaliza a trasa��o

      frmMenu.Connection.CommitTrans;

      // Atualiza o grid

      qryProdutoIntermediario.Close;
      qryProdutoIntermediario.Open;

  end;

end;

procedure TfrmProduto.ProdutoWeb2Click(Sender: TObject);
var
  objForm : TfrmProdutoWeb;
begin
  inherited;

  { -- valida permiss�o de usu�rio -- }
  frmMenu.qryUsuarioAplicacao.Close;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := 'FRMPRODUTOWEB';
  frmMenu.qryUsuarioAplicacao.Open;

  if (frmMenu.qryUsuarioAplicacao.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmProdutoWeb.Create(nil);

  PosicionaQuery(objForm.qryMaster,qryProdutoIntermediarionCdProduto.AsString);
  showForm(objForm,True);
end;

procedure TfrmProduto.qryMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoCodProdFat.Open;
end;

initialization
    RegisterClass(TfrmProduto) ;

end.
