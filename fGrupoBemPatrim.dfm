inherited frmGrupoBemPatrim: TfrmGrupoBemPatrim
  Left = 106
  Top = 156
  Caption = 'Grupo Bem Patrimonial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 52
    Top = 39
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 5
    Top = 65
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Grupo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 36
    Width = 65
    Height = 19
    DataField = 'nCdGrupoBemPatrim'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 61
    Width = 650
    Height = 19
    DataField = 'cNmGrupoBemPatrim'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 16
    Top = 104
    Width = 633
    Height = 265
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 265
    ClientRectRight = 633
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'SubGrupo'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 633
        Height = 242
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsSubGrupoBemPatrim
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnDblClick = DBGridEh1DblClick
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSubGrupoBemPatrim'
            Footers = <>
            Title.Caption = 'C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmSubGrupoBemPatrim'
            Footers = <>
            Title.Caption = 'Descri'#231#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoBemPatrim'
      'WHERE nCdGrupoBemPatrim = :nPK'
      '')
    object qryMasternCdGrupoBemPatrim: TIntegerField
      FieldName = 'nCdGrupoBemPatrim'
    end
    object qryMastercNmGrupoBemPatrim: TStringField
      FieldName = 'cNmGrupoBemPatrim'
      Size = 50
    end
  end
  object qrySubGrupoBemPatrim: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySubGrupoBemPatrimBeforePost
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from SubGrupoBemPatrim where nCdGrupoBemPatrim = :nPk')
    Left = 704
    Top = 184
    object qrySubGrupoBemPatrimnCdSubGrupoBemPatrim: TAutoIncField
      FieldName = 'nCdSubGrupoBemPatrim'
      ReadOnly = True
    end
    object qrySubGrupoBemPatrimnCdGrupoBemPatrim: TIntegerField
      FieldName = 'nCdGrupoBemPatrim'
    end
    object qrySubGrupoBemPatrimcNmSubGrupoBemPatrim: TStringField
      FieldName = 'cNmSubGrupoBemPatrim'
      Size = 50
    end
  end
  object dsSubGrupoBemPatrim: TDataSource
    DataSet = qrySubGrupoBemPatrim
    Left = 736
    Top = 184
  end
end
