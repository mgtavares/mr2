inherited dcmVendaLoja: TdcmVendaLoja
  Left = 205
  Top = 48
  Width = 855
  Height = 658
  Caption = 'An'#225'lise de Vendas - por Loja'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 839
    Height = 596
  end
  object Label1: TLabel [1]
    Left = 81
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 32
    Top = 72
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label3: TLabel [3]
    Left = 54
    Top = 96
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label4: TLabel [4]
    Left = 33
    Top = 120
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
  end
  object Label5: TLabel [5]
    Left = 53
    Top = 144
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label6: TLabel [6]
    Left = 72
    Top = 168
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label7: TLabel [7]
    Left = 31
    Top = 264
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
  end
  object Label10: TLabel [8]
    Left = 49
    Top = 336
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Refer'#234'ncia'
  end
  object Label11: TLabel [9]
    Left = 76
    Top = 192
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label12: TLabel [10]
    Left = 63
    Top = 216
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cole'#231#227'o'
  end
  object Label13: TLabel [11]
    Left = 70
    Top = 240
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label14: TLabel [12]
    Left = 8
    Top = 288
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campanha Promoc.'
  end
  object Label8: TLabel [13]
    Left = 188
    Top = 360
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label9: TLabel [14]
    Left = 32
    Top = 360
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda'
  end
  object Label15: TLabel [15]
    Left = 188
    Top = 384
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label16: TLabel [16]
    Left = 23
    Top = 384
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda 2'
  end
  object Label17: TLabel [17]
    Left = 300
    Top = 384
    Width = 49
    Height = 13
    Caption = '(Opcional)'
  end
  object Label18: TLabel [18]
    Left = 23
    Top = 408
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda 3'
  end
  object Label19: TLabel [19]
    Left = 188
    Top = 408
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label20: TLabel [20]
    Left = 300
    Top = 408
    Width = 49
    Height = 13
    Caption = '(Opcional)'
  end
  object Label21: TLabel [21]
    Left = 46
    Top = 312
    Width = 55
    Height = 13
    Caption = 'Tipo Pedido'
    FocusControl = DBEdit12
  end
  inherited ToolBar1: TToolBar
    Width = 839
    ButtonWidth = 79
    TabOrder = 32
    inherited ToolButton1: TToolButton
      Caption = '&Processar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 79
    end
    inherited ToolButton2: TToolButton
      Left = 87
    end
  end
  object edtLoja: TMaskEdit [23]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtDepartamento: TMaskEdit [24]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtDepartamentoExit
    OnKeyDown = edtDepartamentoKeyDown
  end
  object edtCategoria: TMaskEdit [25]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtCategoriaExit
    OnKeyDown = edtCategoriaKeyDown
  end
  object edtSubCategoria: TMaskEdit [26]
    Left = 104
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = edtSubCategoriaExit
    OnKeyDown = edtSubCategoriaKeyDown
  end
  object edtSegmento: TMaskEdit [27]
    Left = 104
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = edtSegmentoExit
    OnKeyDown = edtSegmentoKeyDown
  end
  object edtMarca: TMaskEdit [28]
    Left = 104
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtMarcaExit
    OnKeyDown = edtMarcaKeyDown
  end
  object DBEdit1: TDBEdit [29]
    Tag = 1
    Left = 172
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 14
  end
  object DBEdit2: TDBEdit [30]
    Tag = 1
    Left = 172
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 22
  end
  object DBEdit3: TDBEdit [31]
    Tag = 1
    Left = 172
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = dsCategoria
    TabOrder = 23
  end
  object DBEdit4: TDBEdit [32]
    Tag = 1
    Left = 172
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = dsSubCategoria
    TabOrder = 24
  end
  object DBEdit5: TDBEdit [33]
    Tag = 1
    Left = 172
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = dsSegmento
    TabOrder = 25
  end
  object DBEdit6: TDBEdit [34]
    Tag = 1
    Left = 172
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 26
  end
  object edtGrupoProduto: TMaskEdit [35]
    Left = 104
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object DBEdit7: TDBEdit [36]
    Tag = 1
    Left = 172
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = dsGrupoProduto
    TabOrder = 27
  end
  object edtReferencia: TEdit [37]
    Left = 104
    Top = 328
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 12
  end
  object edtLinha: TMaskEdit [38]
    Left = 104
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtLinhaExit
    OnKeyDown = edtLinhaKeyDown
  end
  object edtColecao: TMaskEdit [39]
    Left = 104
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtColecaoExit
    OnKeyDown = edtColecaoKeyDown
  end
  object edtClasseProduto: TMaskEdit [40]
    Left = 104
    Top = 232
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = edtClasseProdutoExit
    OnKeyDown = edtClasseProdutoKeyDown
  end
  object DBEdit8: TDBEdit [41]
    Tag = 1
    Left = 172
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 28
  end
  object DBEdit9: TDBEdit [42]
    Tag = 1
    Left = 172
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmColecao'
    DataSource = dsColecao
    TabOrder = 29
  end
  object DBEdit10: TDBEdit [43]
    Tag = 1
    Left = 172
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 30
  end
  object edtCdCampanhaPromoc: TMaskEdit [44]
    Left = 104
    Top = 280
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 10
    Text = '      '
    OnExit = edtCdCampanhaPromocExit
    OnKeyDown = edtCdCampanhaPromocKeyDown
  end
  object DBEdit11: TDBEdit [45]
    Tag = 1
    Left = 172
    Top = 280
    Width = 654
    Height = 21
    DataField = 'cNmCampanhaPromoc'
    DataSource = dsCampanhaPromoc
    TabOrder = 31
  end
  object edtDtFinal: TMaskEdit [46]
    Left = 216
    Top = 352
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 15
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [47]
    Left = 104
    Top = 352
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 34
    Text = '  /  /    '
  end
  object edtDtFinal2: TMaskEdit [48]
    Left = 216
    Top = 376
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 17
    Text = '  /  /    '
  end
  object edtDtInicial2: TMaskEdit [49]
    Left = 104
    Top = 376
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 16
    Text = '  /  /    '
  end
  object edtDtInicial3: TMaskEdit [50]
    Left = 104
    Top = 400
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 18
    Text = '  /  /    '
  end
  object edtDtFinal3: TMaskEdit [51]
    Left = 216
    Top = 400
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 19
    Text = '  /  /    '
  end
  object GroupBox1: TGroupBox [52]
    Left = 8
    Top = 432
    Width = 289
    Height = 153
    Caption = ' Dimens'#245'es '
    TabOrder = 20
    object chkDimAno: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Ano'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chkDimMes: TCheckBox
      Left = 8
      Top = 32
      Width = 97
      Height = 17
      Caption = 'M'#234's'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkDimLoja: TCheckBox
      Left = 8
      Top = 48
      Width = 145
      Height = 17
      Caption = 'Loja (Exibe Totaliza'#231#227'o)'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chkDimDataPedido: TCheckBox
      Left = 8
      Top = 80
      Width = 97
      Height = 17
      Caption = 'Data Pedido'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object chkDimVendedor: TCheckBox
      Left = 8
      Top = 96
      Width = 97
      Height = 17
      Caption = 'Vendedor'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object chkDimFormaPagto: TCheckBox
      Left = 8
      Top = 112
      Width = 121
      Height = 17
      Caption = 'Forma Pagamento'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object chkDimCondPagto: TCheckBox
      Left = 8
      Top = 128
      Width = 121
      Height = 17
      Caption = 'Condi'#231#227'o Pagamento'
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
    object chkDimDepartamento: TCheckBox
      Left = 160
      Top = 16
      Width = 121
      Height = 17
      Caption = 'Departamento'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object chkDimCategoria: TCheckBox
      Left = 160
      Top = 32
      Width = 121
      Height = 17
      Caption = 'Categoria'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object chkDimSubCategoria: TCheckBox
      Left = 160
      Top = 48
      Width = 121
      Height = 17
      Caption = 'SubCategoria'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object chkDimSegmento: TCheckBox
      Left = 160
      Top = 64
      Width = 121
      Height = 17
      Caption = 'Segmento'
      Checked = True
      State = cbChecked
      TabOrder = 10
    end
    object chkDimMarca: TCheckBox
      Left = 160
      Top = 80
      Width = 121
      Height = 17
      Caption = 'Marca'
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    object chkDimProdutoEstrut: TCheckBox
      Left = 160
      Top = 96
      Width = 121
      Height = 17
      Caption = 'Produto Estruturado'
      Checked = True
      State = cbChecked
      TabOrder = 12
    end
    object chkDimProdutoFinal: TCheckBox
      Left = 160
      Top = 112
      Width = 121
      Height = 17
      Caption = 'Produto Final'
      Checked = True
      State = cbChecked
      TabOrder = 13
    end
    object chkDimNumPedido: TCheckBox
      Left = 8
      Top = 64
      Width = 97
      Height = 17
      Caption = 'N'#250'mero Pedido'
      Checked = True
      State = cbChecked
      TabOrder = 14
    end
  end
  object GroupBox2: TGroupBox [53]
    Left = 304
    Top = 432
    Width = 161
    Height = 153
    Caption = ' Medidas '
    TabOrder = 21
    object chkMedQtdeVenda: TCheckBox
      Left = 8
      Top = 16
      Width = 145
      Height = 17
      Caption = 'Quantidade Vendida'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chkMedQtdeEstoque: TCheckBox
      Left = 8
      Top = 32
      Width = 145
      Height = 17
      Caption = 'Quantidade em Estoque'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkMedValVenda: TCheckBox
      Left = 8
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Valor Venda'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chkMedValCustoVenda: TCheckBox
      Left = 8
      Top = 64
      Width = 121
      Height = 17
      Caption = 'Valor Custo Venda'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object chkMedMarkUp: TCheckBox
      Left = 8
      Top = 80
      Width = 97
      Height = 17
      Caption = '% MarkUp'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object chkMedValBruto: TCheckBox
      Left = 8
      Top = 96
      Width = 121
      Height = 17
      Caption = 'Valor Lucro Bruto'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object chkMedMC: TCheckBox
      Left = 8
      Top = 112
      Width = 145
      Height = 17
      Caption = '% Margem Contribui'#231#227'o'
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
  end
  object DBEdit12: TDBEdit [54]
    Tag = 1
    Left = 172
    Top = 304
    Width = 654
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 33
  end
  object edtTipoPedido: TMaskEdit [55]
    Left = 104
    Top = 304
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 11
    Text = '      '
    OnExit = edtTipoPedidoExit
    OnKeyDown = edtTipoPedidoKeyDown
  end
  object chkConsidAcresc: TCheckBox [56]
    Left = 240
    Top = 332
    Width = 233
    Height = 17
    Hint = 'Considera valor de acr'#233'scimo no valor da venda.'
    Caption = 'Considerar acr'#233'scimo no valor de venda ? '
    Checked = True
    ParentShowHint = False
    ShowHint = True
    State = cbChecked
    TabOrder = 13
  end
  inherited ImageList1: TImageList
    Left = 792
    Top = 392
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A8900000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC005157570065646400000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D42420000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D42420000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE00000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FFFFFE00FFFF0000
      E003FE00C0030000C003000080010000C003000080010000C003000080010000
      C003000080010000C003000080010000C003000080010000C007000080010000
      C007000080010000C003000080010000C003000180010000C003000380010000
      E0030077C0030000FFFF007FFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 440
    Top = 392
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 440
    Top = 424
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmDepartamento, nCdDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 472
    Top = 392
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 472
    Top = 424
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategoria, nCdCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 504
    Top = 392
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 504
    Top = 424
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSubCategoria, nCdSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 536
    Top = 392
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 536
    Top = 424
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmSegmento, nCdSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 568
    Top = 392
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 568
    Top = 424
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca, nCdMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 600
    Top = 392
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 600
    Top = 424
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 728
    Top = 392
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object dsGrupoProduto: TDataSource
    DataSet = qryGrupoProduto
    Left = 728
    Top = 424
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha '
      'WHERE nCdLinha = :nPK')
    Left = 632
    Top = 392
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 632
    Top = 424
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao, cNmColecao'
      'FROM Colecao'
      'WHERE nCdColecao = :nPK')
    Left = 664
    Top = 392
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object dsColecao: TDataSource
    DataSet = qryColecao
    Left = 664
    Top = 424
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdClasseProduto, cNmClasseProduto'
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 696
    Top = 392
    object qryClasseProdutonCdClasseProduto: TIntegerField
      FieldName = 'nCdClasseProduto'
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 696
    Top = 424
  end
  object qryCampanhaPromoc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdCampanhaPromoc'
      '      ,cNmCampanhaPromoc '
      '      ,cFlgAtivada'
      '      ,cFlgSuspensa'
      '  FROM CampanhaPromoc'
      ' WHERE nCdEmpresa        = :nCdEmpresa'
      '   AND nCdCampanhaPromoc = :nPK')
    Left = 760
    Top = 392
    object qryCampanhaPromocnCdCampanhaPromoc: TIntegerField
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryCampanhaPromoccNmCampanhaPromoc: TStringField
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryCampanhaPromoccFlgAtivada: TIntegerField
      FieldName = 'cFlgAtivada'
    end
    object qryCampanhaPromoccFlgSuspensa: TIntegerField
      FieldName = 'cFlgSuspensa'
    end
  end
  object dsCampanhaPromoc: TDataSource
    DataSet = qryCampanhaPromoc
    Left = 760
    Top = 424
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido = :nPK')
    Left = 408
    Top = 392
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 408
    Top = 424
  end
end
