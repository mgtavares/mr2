unit rPosicaoInadimplencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP, ER2Lookup;

type
  TrptPosicaoInadimplencia = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtEspTit: TMaskEdit;
    edtCliente: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryEspTit: TADOQuery;
    DBEdit2: TDBEdit;
    dsEspTit: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    edtCadIni: TMaskEdit;
    edtCadFim: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    rgTipoRel: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtEspTitExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEspTitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoInadimplencia: TrptPosicaoInadimplencia;

implementation

uses fMenu, fLookup_Padrao, rVendaCrediario_view,
  rPosicaoInadimplencia_view, rPosicaoInadimplenciaAnal_view,
  rPosicaoInadimplenciaAnalTerc_view;

{$R *.dfm}

procedure TrptPosicaoInadimplencia.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptPosicaoInadimplencia.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptPosicaoInadimplencia.edtEspTitExit(Sender: TObject);
begin
  inherited;

  qryEspTit.Close;
  PosicionaQuery(qryEspTit, edtEspTit.Text) ;

end;

procedure TrptPosicaoInadimplencia.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoInadimplencia.edtEspTitKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(202) ;

        If (nPK > 0) then
        begin

            edtEspTit.Text := IntToStr(nPK) ;
            PosicionaQuery(qryEspTit, edtEspTit.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoInadimplencia.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  
  objRel : TrptPosicaoInadimplencia_view ;
  objRel_2 : TrelPosicaoInadimplenciaAnalTerc_view ;
  objRel_3 : TrelPosicaoInadimplenciaAnal_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial de an�lise.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final de an�lise.') ;
      edtDtFinal.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtFinal.Text) < strToDate(edtDtInicial.Text)) then
  begin
      MensagemAlerta('Per�odo de an�lise inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtFinal.Text) > (Date-1)) then
  begin
      MensagemAlerta('A data final de an�lise n�o pode ser maior que ' + DateToStr(Date-1)) ;
      edtDtFinal.SetFocus;
      exit ;
  end ;

  cFiltro := '' ;

  if (DBEdit1.Text <> '') then
      cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

  if (DBEdit2.Text <> '') then
      cFiltro := cFiltro + '/ Esp�cie T�tulo: ' + trim(edtEspTit.Text) + '-' + DbEdit2.Text ;

  cFiltro := cFiltro + ' / Per�odo An�lise: ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text + ' / Per�odo Cadastro: ' + edtCadIni.Text + ' a ' + edtCadFim.Text;

  case (rgTipoRel.ItemIndex) of

  { -- Rel. Compet�ncia -- }
  0: begin
         objRel := TrptPosicaoInadimplencia_view.Create(Self) ;

         objRel.SPREL_POSICAO_INADIMPLENCIA.Close;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEspTit').Value         := frmMenu.ConvInteiro(edtEspTit.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@cFlgTipoRel').Value       := rgTipoRel.ItemIndex;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(edtDtInicial.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(edtDtFinal.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroIni').Value := frmMenu.ConvData(edtCadIni.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroFin').Value := frmMenu.ConvData(edtCadFim.Text) ;
         objRel.SPREL_POSICAO_INADIMPLENCIA.Open;

         objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
         objRel.lblFiltro1.Caption := cFiltro ;

         try
             try
                 {--visualiza o relat�rio--}

                 objRel.QuickRep1.PreviewModal;

             except
                 MensagemErro('Erro na cria��o do relat�rio');
                 raise;
             end;
          finally
             FreeAndNil(objRel);
          end;

     end;

  { -- Rel. Terceiro -- }
  1: begin
         objRel_2 := TrelPosicaoInadimplenciaAnalTerc_view.Create(Self) ;

         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Close;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEspTit').Value         := frmMenu.ConvInteiro(edtEspTit.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@cFlgTipoRel').Value       := rgTipoRel.ItemIndex;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(edtDtInicial.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(edtDtFinal.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroIni').Value := frmMenu.ConvData(edtCadIni.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroFin').Value := frmMenu.ConvData(edtCadFim.Text) ;
         objRel_2.SPREL_POSICAO_INADIMPLENCIA.Open;

         objRel_2.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
         objRel_2.lblFiltro1.Caption := cFiltro ;

         try
             try
                 {--visualiza o relat�rio--}

                 objRel_2.QuickRep1.PreviewModal;

             except
                 MensagemErro('Erro na cria��o do relat�rio');
                 raise;
             end;
          finally
             FreeAndNil(objRel_2);
          end;
     end;

  { -- Rel. Terceiro / Compet�ncia -- }
  2: begin
         objRel_3 := TrelPosicaoInadimplenciaAnal_view.Create(Self) ;

         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Close;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@nCdEspTit').Value         := frmMenu.ConvInteiro(edtEspTit.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@cFlgTipoRel').Value       := rgTipoRel.ItemIndex;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(edtDtInicial.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(edtDtFinal.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroIni').Value := frmMenu.ConvData(edtCadIni.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Parameters.ParamByName('@dDtCadTerceiroFin').Value := frmMenu.ConvData(edtCadFim.Text) ;
         objRel_3.SPREL_POSICAO_INADIMPLENCIA.Open;

         objRel_3.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
         objRel_3.lblFiltro1.Caption := cFiltro ;


         try
             try
                 {--visualiza o relat�rio--}

                 objRel_3.QuickRep1.PreviewModal;

             except
                 MensagemErro('Erro na cria��o do relat�rio');
                 raise;
             end;
          finally
             FreeAndNil(objRel_3);
          end;

     end;
  end;


end;

initialization
    RegisterClass(TrptPosicaoInadimplencia) ;

end.
