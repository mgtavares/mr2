unit fTransfEst_Recebimento_Processo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, GridsEh, DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTransfEst_Recebimento_Processo = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryPrepara_Temp: TADOQuery;
    qryItens: TADOQuery;
    qryItensnCdItem: TAutoIncField;
    qryItensnCdTransfEst: TIntegerField;
    qryItensnCdProduto: TIntegerField;
    qryItenscEAN: TStringField;
    qryItenscNmProduto: TStringField;
    qryItensnQtde: TBCDField;
    dsItens: TDataSource;
    cxTabSheet3: TcxTabSheet;
    qryResumo: TADOQuery;
    qryResumonCdProduto: TIntegerField;
    qryResumocEAN: TStringField;
    qryResumocNmProduto: TStringField;
    qryResumonQtde: TBCDField;
    qryResumonQtdeTransf: TBCDField;
    DBGridEh2: TDBGridEh;
    dsResumo: TDataSource;
    SP_CONFERE_RECEBIMENTO_TRANSFERENCIA: TADOStoredProc;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryZeraTemp: TADOQuery;
    SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA: TADOStoredProc;
    DBGridEh3: TDBGridEh;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryItenscFlgTipo: TStringField;
    qryItensEAN: TADOQuery;
    dsItensEAN: TDataSource;
    qryProdutoEAN: TADOQuery;
    qryProdutoEANcNmProduto: TStringField;
    qryProdutoEANnCdProduto: TIntegerField;
    qryItensEANnCdItem: TAutoIncField;
    qryItensEANnCdTransfEst: TIntegerField;
    qryItensEANnCdProduto: TIntegerField;
    qryItensEANcEAN: TStringField;
    qryItensEANcNmProduto: TStringField;
    qryItensEANnQtde: TBCDField;
    qryItensEANcFlgDiverg: TIntegerField;
    qryItensEANcFlgInvalido: TIntegerField;
    qryItensEANcFlgTipo: TStringField;
    procedure qryItensBeforePost(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryItensEANBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTransfEst : integer ;
  end;

var
  frmTransfEst_Recebimento_Processo: TfrmTransfEst_Recebimento_Processo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmTransfEst_Recebimento_Processo.qryItensBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  If (qryItenscEAN.Value <> '')  then
      qryItensnQtde.Value := 1 ;

  if (qryItensnQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade recebida.') ;
      abort ;
  end ;

  qryItensnCdTransfEst.Value := nCdTransfEst ;
  qryItenscNmProduto.Value   := '' ;
  qryItenscFlgTipo.Value     := 'C' ;

  qryProduto.Close;
  PosicionaQuery(qryProduto, qryItensnCdProduto.AsString) ;

  if not qryProduto.eof then
      qryItenscNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmTransfEst_Recebimento_Processo.FormActivate(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := cxTabSheet1 ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
      cxPageControl1.ActivePage := cxTabSheet2 ;
      
end;

procedure TfrmTransfEst_Recebimento_Processo.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}
  // do nothing

end;

procedure TfrmTransfEst_Recebimento_Processo.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  try
      SP_CONFERE_RECEBIMENTO_TRANSFERENCIA.Close ;
      SP_CONFERE_RECEBIMENTO_TRANSFERENCIA.Parameters.ParamByName('@nCdTransfEst').Value := nCdTransfEst ;
      SP_CONFERE_RECEBIMENTO_TRANSFERENCIA.Open ;
  except
      raise ;
  end ;

  qryResumo.Close ;
  qryResumo.Open ;

  qryItens.Close ;
  qryItens.Open ;

  qryItensEAN.Close ;
  qryItensEAN.Open ;

  cxPageControl1.ActivePage := cxTabSheet3 ;

  If (SP_CONFERE_RECEBIMENTO_TRANSFERENCIA.FieldList[0].Value = 0) then
  begin

      case MessageDlg('Confirma a entrada destes itens no estoque ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.Close ;
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.Parameters.ParamByName('@nCdTransfEst').Value := nCdTransfEst ;
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Transferência Recebida com Sucesso.') ;
      close ;

  end
  else begin
      MensagemAlerta(SP_CONFERE_RECEBIMENTO_TRANSFERENCIA.FieldList[1].Value) ;
  end ;

end;

procedure TfrmTransfEst_Recebimento_Processo.DBGridEh2DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 2) then
  begin

      if (qryResumonQtde.Value <> qryResumonQtdeTransf.Value) then
      begin
        DBGridEh2.Canvas.Brush.Color := clRed;
        DBGridEh2.Canvas.Font.Color  := clWhite ;
        DBGridEh2.Canvas.FillRect(Rect);
        DBGridEh2.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmTransfEst_Recebimento_Processo.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a exclusão dos itens digitados ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryZeraTemp.Close ;
  qryZeraTemp.ExecSQL;

  qryItens.Close ;
  qryItens.Open  ;

  qryItensEAN.Close ;
  qryItensEAN.Open  ;

  qryResumo.Close ;
  qryResumo.Open ;

end;

procedure TfrmTransfEst_Recebimento_Processo.FormShow(Sender: TObject);
begin
  inherited;

  frmTransfEst_Recebimento_Processo.Caption := 'Recebimento da transferência número: ' + IntToStr(nCdTransfEst) ;

end;

procedure TfrmTransfEst_Recebimento_Processo.qryItensEANBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  If (qryItensEANcEAN.Value <> '')  then
      qryItensEANnQtde.Value := 1 ;

  if (qryItensEANnQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade recebida.') ;
      abort ;
  end ;

  qryItensEANnCdTransfEst.Value := nCdTransfEst ;
  qryItensEANcNmProduto.Value   := '' ;
  qryItensEANcFlgTipo.Value     := 'E' ;

  {qryProdutoEAN.Close;
  PosicionaQuery(qryProdutoEAN, qryItensEANcEAN.Value) ;

  if not qryProdutoEAN.eof then
  begin
      qryItensEANcNmProduto.Value := qryProdutoEANcNmProduto.Value ;
      qryItensEANnCdProduto.Value := qryProdutoEANnCdProduto.Value ;
  end ;}

end;

end.
