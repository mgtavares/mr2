inherited frmImagemDigital_Visualizar: TfrmImagemDigital_Visualizar
  Caption = 'Visualizar Imagem'
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 152
    Top = 104
    Width = 43
    Height = 13
    Caption = 'cImagem'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxDBImage1: TcxDBImage [3]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    DataBinding.DataField = 'cImagem'
    DataBinding.DataSource = DataSource1
    Properties.PopupMenuLayout.MenuItems = []
    Properties.ReadOnly = True
    TabOrder = 1
  end
  object qryImagemDigital: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cImagem'
      'FROM ImagemDigital'
      'WHERE nCdImagemDigital = :nPK')
    Left = 240
    Top = 128
    object qryImagemDigitalcImagem: TBlobField
      FieldName = 'cImagem'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryImagemDigital
    Left = 384
    Top = 240
  end
end
