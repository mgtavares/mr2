unit rPosicaoPedidoVendaWeb_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, QuickRpt, ExtCtrls;

type
  TrptPosicaoPedidoVendaWeb_view = class(TForm)
    SPREL_POSICAO_PEDIDO_VENDA_WEB: TADOStoredProc;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcCdPedidoWeb: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBdDtPedido: TDateTimeField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBdDtIntegracao: TDateTimeField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcNmTabStatusPed: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcNmCliente: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcCupom: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValProdutos: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescontos: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValPedido: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValFrete: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescFrete: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValTotalFrete: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcTipoEntrega: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcTrackingCode: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcNmLogin: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnCdProduto: TIntegerField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBcNmItem: TStringField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnQtdePed: TIntegerField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValUnitario: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValDescontoItem: TBCDField;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnValTotalItem: TBCDField;
    QuickRep: TQuickRep;
    QRBand1: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel1: TQRLabel;
    QRShape3: TQRShape;
    QRDBText11: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    SPREL_POSICAO_PEDIDO_VENDA_WEBnCdPedido: TIntegerField;
    QRShape2: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel26: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRBand4: TQRBand;
    QRLabel28: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRLabel29: TQRLabel;
    QRLabel27: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoPedidoVendaWeb_view: TrptPosicaoPedidoVendaWeb_view;

implementation

uses
    fMenu;

{$R *.dfm}

end.
