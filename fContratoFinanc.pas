unit fContratoFinanc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmContratoFinanc = class(TfrmCadastro_Padrao)
    qryMasternCdContratoFinanc: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMasternCdCC: TIntegerField;
    qryMasternCdTipoSP: TIntegerField;
    qryMastercDescricaoContrato: TMemoField;
    qryMasterdDtInicio: TDateTimeField;
    qryMasterdDtFim: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternValMensal: TBCDField;
    qryMasteriDiaVencto: TIntegerField;
    qryMasterdDtUltGeracao: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBMemo1: TDBMemo;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit14: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit15: TDBEdit;
    DataSource2: TDataSource;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    DBEdit16: TDBEdit;
    DataSource3: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit17: TDBEdit;
    DataSource4: TDataSource;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    DBEdit18: TDBEdit;
    DataSource5: TDataSource;
    qryTipoSP: TADOQuery;
    qryTipoSPnCdTipoSP: TIntegerField;
    qryTipoSPcNmTipoSP: TStringField;
    DBEdit19: TDBEdit;
    DataSource6: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContratoFinanc: TfrmContratoFinanc;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmContratoFinanc.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONTRATOFINANC' ;
  nCdTabelaSistema  := 53  ;
  nCdConsultaPadrao := 108 ;
end;

procedure TfrmContratoFinanc.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  
  DBEdit3.SetFocus ;
  
end;

procedure TfrmContratoFinanc.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DbEdit3.Text) ;
  
end;

procedure TfrmContratoFinanc.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  qryCategFinanc.Close ;
  PosicionaQuery(qryCategFinanc, DBedit4.Text) ;
  
end;

procedure TfrmContratoFinanc.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, DbEdit5.Text) ;
  
end;

procedure TfrmContratoFinanc.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryCC.Close ;
  PosicionaQuery(qryCC, DbEdit6.Text) ;
  
end;

procedure TfrmContratoFinanc.DBEdit7Exit(Sender: TObject);
begin
  inherited;
  qryTipoSP.Close ;
  PosicionaQuery(qryTipoSP, DbEdit7.Text) ;
  
end;

procedure TfrmContratoFinanc.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryCategFinanc.Close ;
  qryUnidadeNegocio.Close ;
  qryCC.Close ;
  qryTipoSP.Close ;

end;

procedure TfrmContratoFinanc.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryCategFinanc, qryMasternCdCategFinanc.asString) ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.asString) ;
  PosicionaQuery(qryCC, qryMasternCdCC.asString) ;
  PosicionaQuery(qryTipoSP, qryMasternCdTipoSP.asString) ;

end;

procedure TfrmContratoFinanc.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryCategFinanc.Close ;
  qryUnidadeNegocio.Close ;
  qryCC.Close ;
  qryTipoSP.Close ;

end;

procedure TfrmContratoFinanc.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DbEdit3.Text = '') or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Selecione um Terceiro.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DbEdit4.Text = '') or (DbEdit16.Text = '') then
  begin
      MensagemAlerta('Selecione uma Categoria Financeira.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (DbEdit5.Text = '') or (DbEdit17.Text = '') then
  begin
      MensagemAlerta('Selecione uma Unidade de Neg�cio.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (DbEdit6.Text = '') or (DbEdit18.Text = '') then
  begin
      MensagemAlerta('Selecione um Centro de Custo.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (DbEdit7.Text = '') or (DbEdit19.Text = '') then
  begin
      MensagemAlerta('Selecione um Tipo de SP.') ;
      DbEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMastercDescricaoContrato.Value = '') then
  begin
      MensagemAlerta('Informe uma descri��o para o contrato.') ;
      DbMemo1.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtInicio.Value > qryMasterdDtFim.Value) then
  begin
      MensagemAlerta('Per�odo de validade inv�lido.') ;
      DbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternValMensal.Value <= 0) then
  begin
      MensagemAlerta('Valor mensal inv�lido.') ;
      DbEdit11.SetFocus ;
      abort ;
  end ;

  if (qryMasteriDiaVencto.Value < 1) or (qryMasteriDiaVencto.Value > 31) then
  begin
      MensagemAlerta('Dia de vencimento inv�lido.') ;
      DbEdit12.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmContratoFinanc.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoFinanc.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(109);

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoFinanc.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(110);

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoFinanc.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdCategFinanc.Value = 0) then
            begin
                MensagemAlerta('Selecione uma categoria financeira.') ;
                DbEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(33,'EXISTS(SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = CentroCusto.nCdCC AND CCCF.nCdCategFinanc = ' + qryMasternCdCategFinanc.AsString + ')');

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoFinanc.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(111);

            If (nPK > 0) then
            begin
                qryMasternCdTipoSP.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmContratoFinanc) ;
         
end.
