unit fProduto_DetalheGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh, ACBrBase,
  ACBrValidador;

type
  TfrmProduto_DetalheGrade = class(TfrmProcesso_Padrao)
    DBGridEh2: TDBGridEh;
    qryProdutoFinal: TADOQuery;
    qryProdutoFinalnCdProduto: TIntegerField;
    qryProdutoFinalcNmTamanho: TStringField;
    qryProdutoFinalnQtdeEstoque: TBCDField;
    qryProdutoFinalnValCusto: TBCDField;
    qryProdutoFinalnValVenda: TBCDField;
    qryProdutoFinalcEAN: TStringField;
    qryProdutoFinalcEANFornec: TStringField;
    qryProdutoFinalcNmProduto: TStringField;
    dsProdutoFinal: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    qryHistPrecoProd: TADOQuery;
    dsHistPrecoProd: TDataSource;
    ToolButton9: TToolButton;
    ACBrValidador: TACBrValidador;
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryProdutoFinalBeforeEdit(DataSet: TDataSet);
    procedure qryProdutoFinalBeforePost(DataSet: TDataSet);
    procedure prGeraHistoricoAlteracao(nValPrecoAnt, nValPrecoAtu : Currency; cOBS : String);
  private
    { Private declarations }
  public
    { Public declarations }
    nValPrecoAntCust : currency;
    nValPrecoAntVend : currency;
  end;

var
  frmProduto_DetalheGrade: TfrmProduto_DetalheGrade;

implementation

uses fProdutoPosicaoEstoque, fProduto_HistPrecoVenda, fModImpETP,fMenu;

{$R *.dfm}

procedure TfrmProduto_DetalheGrade.prGeraHistoricoAlteracao(nValPrecoAnt, nValPrecoAtu : Currency; cOBS : String);
begin
    qryHistPrecoProd.Close;
    qryHistPrecoProd.Parameters.ParamByName('nCdProduto').Value             := qryProdutoFinalnCdProduto.Value;
    qryHistPrecoProd.Parameters.ParamByName('nCdUsuario').Value             := frmMenu.nCdUsuarioLogado;
    qryHistPrecoProd.Parameters.ParamByName('nValPrecoAtual').Value         := nValPrecoAtu;
    qryHistPrecoProd.Parameters.ParamByName('nValPrecoAnterior').Value      := nValPrecoAnt;
    qryHistPrecoProd.Parameters.ParamByName('cOBS').Value                   := cOBS; // OBS
    qryHistPrecoProd.ExecSQL;
end;
procedure TfrmProduto_DetalheGrade.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmProdutoPosicaoEstoque;
begin
  inherited;

  if (qryProdutoFinal.Active) then
  begin

      objForm := TfrmProdutoPosicaoEstoque.Create(nil);

      PosicionaQuery(objForm.qryPosicaoEstoque, qryProdutoFinalnCdProduto.AsString) ;

      showForm(objForm,true);
  end ;

end;

procedure TfrmProduto_DetalheGrade.ToolButton6Click(Sender: TObject);
var
  objForm : TfrmProduto_HistPrecoVenda;
begin

    if (qryProdutoFinal.Active) then
    begin

        objForm := TfrmProduto_HistPrecoVenda.Create(nil);

        PosicionaQuery(objForm.qryHistorico, qryProdutoFinalnCdProduto.AsString) ;
        showForm(objForm,true);

    end ;

end;

procedure TfrmProduto_DetalheGrade.ToolButton8Click(Sender: TObject);
var

    arrID : array of integer ;
    bImprimirUnidadeEstoque : boolean;
    iAux , iAuxTotal        : integer ;
    objForm : TfrmModImpETP;
begin
  inherited;

  if (MessageDlg('Confirma a impress�o das etiquetas desta grade ?',mtConfirmation,[mbYes,mbNo],0)=MrNO) then
      exit ;

  bImprimirUnidadeEstoque := True ;

  if (MessageDlg('Deseja imprimir uma etiqueta para cada unidade em estoque ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
  begin
      if (MessageDlg('Deseja imprimir uma etiqueta para cada produto ?',mtConfirmation,[mbYes,mbNo],0)=MrYes) then
          bImprimirUnidadeEstoque := False
      else
          exit ;
  end ;

  SetLength(arrID,0) ;

  iAuxTotal := 0 ;
  {-- imprimir uma etiqueta para cada item em estoque --}
  if (bImprimirUnidadeEstoque) then
  begin

      qryProdutoFinal.First;

      while not qryProdutoFinal.Eof do
      begin

          if (qryProdutoFinalnQtdeEstoque.Value > 0) then
              for iAux := 0 to (qryProdutoFinalnQtdeEstoque.AsInteger-1) do
              begin
                  SetLength(arrID,Length(arrID)+1) ;
                  arrID[iAuxTotal] := qryProdutoFinalnCdProduto.Value ;
                  inc(iAuxTotal) ;
              end ;

          qryProdutoFinal.Next;
      end ;

      qryProdutoFinal.First;

  end ;

  {-- imprimir uma etiqueta para cada produto, independente da quantidade em estoque --}
  if (not bImprimirUnidadeEstoque) then
  begin

      qryProdutoFinal.First;

      while not qryProdutoFinal.Eof do
      begin
          SetLength(arrID,Length(arrID)+1) ;
          arrID[iAuxTotal] := qryProdutoFinalnCdProduto.Value ;
          inc(iAuxTotal) ;

          qryProdutoFinal.Next;
      end ;

      qryProdutoFinal.First;

  end ;

  if not qryProdutoFinal.eof then
  begin
      objForm := TfrmModImpETP.Create(nil);
      objForm.emitirEtiquetas(arrID,1) ;
  end;

end;

procedure TfrmProduto_DetalheGrade.FormCreate(Sender: TObject);
begin
  inherited;

  {-- Par�metro criado para permitir a altera��o do pre�o --
   -- de custo e de venda do produto direto no grid.      --}
  if frmMenu.LeParametro('PERMITEALTPRECO') = 'N' then 
  begin
       DBGridEh2.Columns[3].ReadOnly := True;
       DBGridEh2.Columns[4].ReadOnly := True;
  end;
end;

procedure TfrmProduto_DetalheGrade.qryProdutoFinalBeforeEdit(
  DataSet: TDataSet);
begin
  inherited;

  {-- Atribui os valores as variaveis antes de ser editado --
   -- para que tenha pre�o anterior.                       --}
  nValPrecoAntCust := 0;
  nValPrecoAntVend := 0;
  
  nValPrecoAntCust := qryProdutoFinalnValCusto.Value;
  nValPrecoAntVend := qryProdutoFinalnValVenda.Value;
end;

procedure TfrmProduto_DetalheGrade.qryProdutoFinalBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  { -- gera hist�rico de altera��o de pre�o de custo -- }
  if (qryProdutoFinalnValCusto.Value <> nValPrecoAntCust) then
  begin
      prGeraHistoricoAlteracao(nValPrecoAntCust, qryProdutoFinalnValCusto.Value, 'ALTERA��O DE PRE�O DE CUSTO MANUAL');
  end;

  { -- gera hist�rico de altera��o de pre�o de venda -- }
  if (qryProdutoFinalnValVenda.Value <> nValPrecoAntVend) then
  begin
      prGeraHistoricoAlteracao(nValPrecoAntVend, qryProdutoFinalnValVenda.Value, 'ALTERA��O DE PRE�O DE VENDA MANUAL');
  end;

  qryProdutoFinalcEAN.Value := Trim(qryProdutoFinalcEAN.Value);

  { -- se c�digo EAN n�o for preenchido, considera c�digo do produto -- }
  if (qryProdutoFinalcEAN.Value = '') then
  begin
      qryProdutoFinalcEAN.Value := qryProdutoFinalnCdProduto.AsString;
  end
  else
  begin
      { -- valida C�digo de Barras -- }
      ACBrValidador.TipoDocto   := docGTIN;
      ACBrValidador.Documento   := Trim(qryProdutoFinalcEAN.Value);
      ACBrValidador.IgnorarChar := '';

      if ((Trim(qryProdutoFinalcEAN.Value) <> '') and (not ACBrValidador.Validar)) then
          MensagemAlerta('C�digo de barras inv�lido.');
  end;
end;

end.
