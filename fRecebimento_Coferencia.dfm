inherited frmRecebimento_Conferencia: TfrmRecebimento_Conferencia
  Left = 215
  Top = 153
  Caption = 'Recebimento - Confer'#234'ncia de Produtos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 433
    ActivePage = cxTabSheet1
    Align = alClient
    TabOrder = 1
    ClientRectBottom = 433
    ClientRectRight = 784
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Produto Grade'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 410
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsProdutoGrade
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid1DBTableView1DBColumn2: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProdutoPai'
            Visible = False
            GroupIndex = 1
            Width = 341
          end
          object cxGrid1DBTableView1DBColumn3: TcxGridDBColumn
            Caption = 'Grade'
            DataBinding.FieldName = 'cNmProduto'
            Width = 304
          end
          object cxGrid1DBTableView1DBColumn4: TcxGridDBColumn
            Caption = 'Qtde pedida'
            DataBinding.FieldName = 'nQtde'
          end
          object cxGrid1DBTableView1DBColumn5: TcxGridDBColumn
            Caption = 'Qtde. Conferida'
            DataBinding.FieldName = 'nQtdeConf'
            Width = 101
          end
          object cxGrid1DBTableView1DBColumn6: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldo'
            Width = 52
          end
          object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
            Caption = 'Diverg'#234'ncia'
            DataBinding.FieldName = 'cFlgDivergencia'
            Visible = False
            GroupIndex = 0
            Width = 109
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Produto Sem Grade'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 410
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = dsProdutoSemGrade
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProdutoPai'
            Width = 333
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = 'Qtde Pedida'
            DataBinding.FieldName = 'nQtde'
            Width = 81
          end
          object cxGridDBColumn4: TcxGridDBColumn
            Caption = 'Qtde. Conferida'
            DataBinding.FieldName = 'nQtdeConf'
            Width = 106
          end
          object cxGridDBColumn5: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldo'
            Width = 55
          end
          object cxGridDBTableView1DBColumn1: TcxGridDBColumn
            Caption = 'Diverg'#234'ncia'
            DataBinding.FieldName = 'cFlgDivergencia'
            Visible = False
            GroupIndex = 0
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 304
    Top = 152
  end
  object qryProdutoGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT IR2.nCdItemRecebimento'
      '       ,IR2.cNmItem            as cNmProdutoPai'
      '       ,IR1.cNmItem            as cNmProduto'
      '       ,IR1.nQtde'
      '       ,IR1.nQtdeConf'
      '       ,ABS(IR1.nQtde - IR1.nQtdeConf) as nSaldo'
      '       ,CASE WHEN ABS(IR1.nQtde - IR1.nQtdeConf) > 0'
      '             THEN '#39'SIM'#39
      '             ELSE '#39'N'#195'O'#39
      '        END cFlgDivergencia'
      '   FROM ItemRecebimento IR1'
      
        '        INNER JOIN ItemRecebimento IR2 ON IR2.nCdItemRecebimento' +
        ' = IR1.nCdItemRecebimentoPai       '
      '  WHERE IR1.nCdRecebimento = :nPK    '
      '    AND (IR1.nCdTipoItemPed = 4)      ')
    Left = 240
    Top = 152
    object qryProdutoGradenCdItemRecebimento: TIntegerField
      FieldName = 'nCdItemRecebimento'
    end
    object qryProdutoGradecNmProdutoPai: TStringField
      FieldName = 'cNmProdutoPai'
      Size = 150
    end
    object qryProdutoGradecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoGradenQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryProdutoGradenQtdeConf: TBCDField
      FieldName = 'nQtdeConf'
      Precision = 12
    end
    object qryProdutoGradenSaldo: TBCDField
      FieldName = 'nSaldo'
      ReadOnly = True
      Precision = 13
    end
    object qryProdutoGradecFlgDivergencia: TStringField
      FieldName = 'cFlgDivergencia'
      ReadOnly = True
      Size = 3
    end
  end
  object dsProdutoGrade: TDataSource
    DataSet = qryProdutoGrade
    Left = 240
    Top = 184
  end
  object qryProdutoSemGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT IR1.cNmItem            as cNmProduto'
      '       ,IR1.nCdProduto'
      '       ,IR1.nQtde'
      '       ,IR1.nQtdeConf'
      '       ,ABS(IR1.nQtde - IR1.nQtdeConf) as nSaldo'
      '       ,CASE WHEN ABS(IR1.nQtde - IR1.nQtdeConf) > 0'
      '             THEN '#39'SIM'#39
      '             ELSE '#39'N'#195'O'#39
      '        END cFlgDivergencia'
      '   FROM ItemRecebimento IR1        '
      '  WHERE IR1.nCdRecebimento = :nPK'
      '    AND (IR1.nCdTipoItemPed = 2)')
    Left = 272
    Top = 152
    object qryProdutoSemGradecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoSemGradenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoSemGradenQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryProdutoSemGradenQtdeConf: TBCDField
      FieldName = 'nQtdeConf'
      Precision = 12
    end
    object qryProdutoSemGradenSaldo: TBCDField
      FieldName = 'nSaldo'
      ReadOnly = True
      Precision = 13
    end
    object qryProdutoSemGradecFlgDivergencia: TStringField
      FieldName = 'cFlgDivergencia'
      ReadOnly = True
      Size = 3
    end
  end
  object dsProdutoSemGrade: TDataSource
    DataSet = qryProdutoSemGrade
    Left = 272
    Top = 184
  end
end
