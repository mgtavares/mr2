inherited frmTipoDocumentoFiscal: TfrmTipoDocumentoFiscal
  Left = 163
  Top = 167
  Caption = 'Tipo Documento Fiscal'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 23
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 12
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 36
    Top = 94
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sigla'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 202
    Top = 94
    Width = 172
    Height = 13
    Caption = '(Sigla da Esp'#233'cie para Livro Fiscal)'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [6]
    Left = 66
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 66
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox13: TDBCheckBox [8]
    Left = 66
    Top = 115
    Width = 239
    Height = 17
    Caption = 'Permitir Documento Fiscal Complementar'
    DataField = 'cFlgAceitaComplemento'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit3: TDBEdit [9]
    Left = 66
    Top = 88
    Width = 130
    Height = 19
    DataField = 'cSiglaLivroFiscal'
    DataSource = dsMaster
    TabOrder = 3
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TIPODOCTOFISCAL'
      'WHERE nCdTipoDoctoFiscal = :nPK'
      '')
    object qryMasternCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryMastercNmTipoDoctoFiscal: TStringField
      FieldName = 'cNmTipoDoctoFiscal'
      Size = 50
    end
    object qryMastercFlgAceitaComplemento: TIntegerField
      FieldName = 'cFlgAceitaComplemento'
    end
    object qryMastercSiglaLivroFiscal: TStringField
      FieldName = 'cSiglaLivroFiscal'
      Size = 10
    end
  end
end
