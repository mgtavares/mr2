unit fCaixa_ReembolsoVale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxCurrencyEdit, StdCtrls, Mask, DBCtrls, DB, ADODB,
  cxButtons, jpeg, ExtCtrls;

type
  TfrmCaixa_ReembolsoVale = class(TForm)
    Image1: TImage;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    qryVale: TADOQuery;
    DataSource1: TDataSource;
    SP_REEMBOLSA_VALE: TADOStoredProc;
    qryValenCdVale: TAutoIncField;
    qryValenCdLoja: TIntegerField;
    qryValedDtVale: TDateTimeField;
    qryValenCdLanctoFin: TIntegerField;
    qryValenSaldoVale: TBCDField;
    qryValecFlgLiqVenda: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    edtVale: TcxCurrencyEdit;
    Label5: TLabel;
    qryValenCdLanctoFinRecompra: TIntegerField;
    qryLanctoFin: TADOQuery;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdLoja: TStringField;
    qryLanctoFincNmUsuario: TStringField;
    qryValenCdTerceiro: TIntegerField;
    qryValecNmTerceiro: TStringField;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    procedure edtValeKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdResumoCaixa : integer ;
  end;

var
  frmCaixa_ReembolsoVale: TfrmCaixa_ReembolsoVale;

implementation

uses fMenu, fCaixa_Pendencias, fCaixa_Recebimento;

{$R *.dfm}

procedure TfrmCaixa_ReembolsoVale.edtValeKeyPress(Sender: TObject; var Key: Char);
begin

    if (Key = #13) and (trim(edtVale.Text) <> '')then
    begin

        qryVale.Close ;
        qryVale.Parameters.ParamByName('nPK').Value := frmMenu.ConvInteiro(edtVale.Text) ;
        qryVale.Open ;

        if (qryVale.eof) then
        begin
            qryVale.Close ;
            frmMenu.MensagemAlerta('Vale inexistente ou vale sem saldo dispon�vel.') ;
            edtVale.Text := '' ;
            qryVale.Close ;
            abort ;
        end ;

        if (qryValecFlgLiqVenda.Value = 0) then
        begin
            frmMenu.MensagemAlerta('Este tipo de vale n�o pode ser reembolsado.') ;
            edtVale.Text := '' ;
            qryVale.Close ;
            abort ;
        end ;

        if (qryValenCdLanctoFinRecompra.Value > 0) then
        begin

            if (frmMenu.MessageDlg('Este vale j� foi reembolsado. Deseja visualizar os detalhes do movimento ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
            begin
                qryVale.Close ;
                edtVale.Text := '' ;
                edtVale.SetFocus;
                exit ;
            end ;

            qryLanctoFin.Close;
            qryLanctoFin.Parameters.ParamByName('nPK').Value := qryValenCdLanctoFinRecompra.Value ;
            qryLanctoFin.Open;

            if not qryLanctoFin.Eof then
            begin

                frmMenu.ShowMessage('Data do Movimento : ' + qryLanctoFindDtLancto.AsString +#13#13 +
                                    'Loja Reembolso      : ' + qryLanctoFinnCdLoja.AsString   +#13#13 +
                                    'Usu�rio Operador  : ' + qryLanctoFincNmUsuario.Value) ;


                qryVale.Close ;
                edtVale.Text := '' ;
                edtVale.SetFocus;
                exit ;

            end ;

            qryVale.Close ;
            edtVale.Text := '' ;
            exit ;

        end ;

        if (qryValenSaldoVale.Value <= 0) then
        begin
            frmMenu.MensagemAlerta('Vale n�o tem saldo dispon�vel.');
            edtVale.Text := '' ;
            edtVale.SetFocus;
            exit ;
        end ;

        btnConfirmar.SetFocus;

    end;

end;

procedure TfrmCaixa_ReembolsoVale.btnConfirmarClick(Sender: TObject);
var
  objCaixa_Pendencias : TfrmCaixa_Pendencias ;
  objCaixa            : TfrmCaixa_Recebimento;
begin

  if (qryVale.eof) then
  begin
      frmMenu.MensagemErro('Informe o n�mero do vale.') ;
      edtVale.SetFocus;
      exit ;
  end ;

  //verifica se o cliente tem alguma pend�ncia financeira.
  if (qryValenCdTerceiro.Value > 0) and (qryValenCdTerceiro.asString <> frmMenu.LeParametro('TERCPDV')) then
  begin

      objCaixa_Pendencias := TfrmCaixa_Pendencias.Create( Self ) ;

      objCaixa_Pendencias.qryCrediario.Close ;
      objCaixa_Pendencias.qryCrediario.Parameters.ParamByName('nCdTerceiro').Value := qryValenCdTerceiro.Value ;
      objCaixa_Pendencias.qryCrediario.Open ;

      objCaixa_Pendencias.qryCheque.Close ;
      objCaixa_Pendencias.qryCheque.Parameters.ParamByName('nCdTerceiro').Value := qryValenCdTerceiro.Value ;
      objCaixa_Pendencias.qryCheque.Open ;

      try
          if (not objCaixa_Pendencias.qryCrediario.Eof) or (not objCaixa_Pendencias.qryCheque.Eof) then
          begin
              frmMenu.MensagemAlerta('O Cliente do vale tem pend�ncias financeiras e o vale n�o poder� ser reembolsado.') ;
              objCaixa_Pendencias.ShowModal;
              edtVale.Text := '' ;
              qryVale.Close ;
              edtVale.SetFocus;
              exit ;
          end ;
      finally
          freeAndNil( objCaixa_Pendencias ) ;
      end
      
  end ;

  if not qryVale.Eof then
  begin

      case frmMenu.MessageDlg('Confirma o reembolso deste vale ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: begin
              qryVale.Close ;
              edtVale.Text := '' ;
              edtVale.SetFocus ;
              exit ;
          end
      end ;

      objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

      try
          if not objCaixa.AutorizacaoGerente(22) then
          begin
              frmMenu.MensagemAlerta('Autentica��o falhou.') ;
              exit ;
          end ;
      finally

          freeAndNil( objCaixa ) ;

      end ;

      frmMenu.Connection.BeginTrans;

      try
          SP_REEMBOLSA_VALE.Close ;
          SP_REEMBOLSA_VALE.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
          SP_REEMBOLSA_VALE.Parameters.ParamByName('@nCdVale').Value        := frmMenu.ConvInteiro(edtVale.Text) ;
          SP_REEMBOLSA_VALE.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
          SP_REEMBOLSA_VALE.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
          SP_REEMBOLSA_VALE.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          frmMenu.MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      frmMenu.ShowMessage('Processo conclu�do. Um lan�amento de d�bito foi gerado no caixa no valor do vale.') ;

      qryVale.Close ;
      edtVale.Text := '' ;
      close ;

  end ;

end;

procedure TfrmCaixa_ReembolsoVale.FormShow(Sender: TObject);
begin

    qryVale.Close ;
    edtVale.Text := '' ;
    edtVale.SetFocus;

end;

procedure TfrmCaixa_ReembolsoVale.btnCancelarClick(Sender: TObject);
begin
    close ;
end;

end.
