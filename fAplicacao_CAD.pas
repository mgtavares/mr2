unit fAplicacao_CAD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, StdCtrls, ExtCtrls, DBCtrls,
  ToolWin, ComCtrls, Mask, Buttons, ImgList, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxControls, cxNavigator, cxDBNavigator,
  cxCurrencyEdit, cxDBEdit, GridsEh, DBGridEh, DBGridEhGrouping, cxPC,
  ToolCtrlsEh;

type
  TfrmAplicacao_Cad = class(TfrmCadastro_Padrao)
    qryMasternCdAplicacao: TIntegerField;
    qryMastercNmAplicacao: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdModulo: TIntegerField;
    qryModulo: TADOQuery;
    qryModulonCdModulo: TIntegerField;
    qryModulocNmModulo: TStringField;
    qryModulonCdStatus: TIntegerField;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    dsStatus: TDataSource;
    dsModulo: TDataSource;
    qryMastercNmModulo: TStringField;
    DBEdit5: TDBEdit;
    qryMastercNmStatus: TStringField;
    DBEdit6: TDBEdit;
    qryMastercNmObjeto: TStringField;
    Label1: TLabel;
    DBEdit7: TDBEdit;
    qryParticulAplicacao: TADOQuery;
    dsParticulAplicacao: TDataSource;
    qryParticulAplicacaocSiglaParticul: TStringField;
    qryParticulAplicacaonCdAplicacao: TIntegerField;
    qryParticulAplicacaocNmParticul: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PosicionaPK(cNmCampo: String ;nCdPK : Integer) ;
    procedure dsMasterDataChange(Sender: TObject; Field: TField);
    procedure btConsultarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryParticulAplicacaoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);


  private
    { Private declarations }
    //cNmTabelaMaster   : String;
    //nCdConsultaPadrao : Integer;

  public
    { Public declarations }
  end;

var
  frmAplicacao_Cad: TfrmAplicacao_Cad;


implementation

uses fLookup_Padrao;

procedure TfrmAplicacao_Cad.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmAplicacao_Cad.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(3);

            If (nPK > 0) then
            begin
                qryMasternCdModulo.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

  end;

procedure TfrmAplicacao_Cad.PosicionaPK(cNmCampo: String ;nCdPK : Integer) ;
begin

    qryMaster.Close ;
    qryMaster.DisableControls;

    qryMaster.Parameters.ParamByName('nPK').Value := nCdPK ;

    qryMaster.Open ;

    qryMaster.EnableControls ;

end ;

procedure TfrmAplicacao_Cad.dsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btAlterar.Click ;
end;

procedure TfrmAplicacao_Cad.btConsultarClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmAplicacao_Cad.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmAplicacao_Cad.btSalvarClick(Sender: TObject);
begin
  inherited;

{  DBEdit2.SetFocus;}

end;

procedure TfrmAplicacao_Cad.btCancelarClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;

end;

procedure TfrmAplicacao_Cad.FormShow(Sender: TObject);
begin
  qryModulo.Open;
  inherited;

  DBEdit2.CharCase := ecNormal ;
end;

procedure TfrmAplicacao_Cad.qryParticulAplicacaoBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryParticulAplicacaonCdAplicacao.Value := qryMasternCdAplicacao.Value ;

  inherited;

end;

procedure TfrmAplicacao_Cad.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryParticulAplicacao.Close ;
  qryParticulAplicacao.Parameters.ParamByName('nCdAplicacao').Value := qryMasternCdAplicacao.Value ;
  qryParticulAplicacao.Open ;

end;

procedure TfrmAplicacao_Cad.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryParticulAplicacao.Close ;

end;

procedure TfrmAplicacao_Cad.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'APLICACAO' ;
  nCdTabelaSistema  := 1 ;
  nCdConsultaPadrao := 1 ;

end;

procedure TfrmAplicacao_Cad.ToolButton10Click(Sender: TObject);
var
  objForm : TForm;
  cScript : TMemo;
begin
  inherited;

  if not (qryMaster.Active) then
      exit;

   objForm := TForm.Create(nil);

   objForm.Width    := 650;
   objForm.Height   := 350;
   objForm.Position := poDesktopCenter;
   objForm.Caption  := 'Script SQL';

   cScript            := TMemo.Create(nil);
   cScript.Parent     := objForm;
   cScript.Align      := alClient;
   cScript.ScrollBars := ssVertical;
   cScript.Font.Name  := 'Consolas';

   cScript.Lines.Add('INSERT INTO Aplicacao (nCdAplicacao');
   cScript.Lines.Add('                      ,cNmAplicacao');
   cScript.Lines.Add('                      ,nCdStatus');
   cScript.Lines.Add('                      ,nCdModulo');
   cScript.Lines.Add('                      ,cNmObjeto)');
   cScript.Lines.Add('               VALUES (' + qryMasternCdAplicacao.AsString);
   cScript.Lines.Add('                      ,' + #39 + qryMastercNmAplicacao.AsString + #39);
   cScript.Lines.Add('                      ,' + qryMasternCdStatus.AsString);
   cScript.Lines.Add('                      ,' + qryMasternCdModulo.AsString);
   cScript.Lines.Add('                      ,' + #39 + qryMastercNmObjeto.AsString + #39 + ')');

   if not (qryParticulAplicacao.Eof) then
   begin

       while not qryParticulAplicacao.Eof do
       begin

           qryParticulAplicacao.First;

           cScript.Lines.Add('');
           cScript.Lines.Add('INSERT INTO ParticulAplicacao (cSiglaParticul');
           cScript.Lines.Add('                              ,nCdAplicacao');
           cScript.Lines.Add('                              ,cNmParticul)');
           cScript.Lines.Add('                       VALUES (' + #39 + qryParticulAplicacaocSiglaParticul.AsString + #39);
           cScript.Lines.Add('                              ,' + qryParticulAplicacaonCdAplicacao.AsString);
           cScript.Lines.Add('                              ,' + #39 + qryParticulAplicacaocNmParticul.AsString + #39 + ')');

           qryParticulAplicacao.Next;
       end;
   end;

   showForm(objForm,True);

end;

initialization
  RegisterClass(TfrmAplicacao_Cad) ;


{$R *.dfm}


end.
