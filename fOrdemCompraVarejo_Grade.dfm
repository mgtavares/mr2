inherited frmOrdemCompraVarejo_Grade: TfrmOrdemCompraVarejo_Grade
  Left = 315
  Top = 227
  Width = 706
  Height = 426
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Ordem Compra Varejo - Grade'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 46
    Width = 690
    Height = 325
  end
  inherited ToolBar1: TToolBar
    Width = 690
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 46
    Width = 690
    Height = 325
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsLoja
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Width = 347
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeGrade'
        Footers = <>
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeEstoque'
        Footers = <>
        ReadOnly = True
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'nVendaMarca'
        Footers = <>
        ReadOnly = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nVendaLinha'
        Footers = <>
        ReadOnly = True
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'nVendaProduto'
        Footers = <>
        ReadOnly = True
        Width = 57
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText1: TStaticText [3]
    Left = 0
    Top = 29
    Width = 690
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Quantidade de Grades por Loja'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
  end
  object StaticText2: TStaticText [4]
    Left = 0
    Top = 371
    Width = 690
    Height = 17
    Align = alBottom
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Legenda'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 3
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdItemPedido int'
      ''
      'Set @nCdItemPedido = :nCdItemPedido'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Grade_Loja'#39') IS NULL)'
      'BEGIN'
      ''
      
        #9'CREATE TABLE #Temp_Grade_Loja (nCdProduto    int           not ' +
        'null'
      #9#9#9#9#9#9#9#9'   ,nCdLoja       int           not null'
      #9#9#9#9#9#9#9#9'   ,cNmLoja       varchar(50)   not null'
      #9#9#9#9#9#9#9#9'   ,nQtdeGrade    int           default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nQtdeEstoque  int           default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaMarca   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaLinha   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaProduto decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,PRIMARY KEY (nCdProduto, nCdLoja))'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Grade_Loja'
      ''
      'INSERT INTO #Temp_Grade_Loja (nCdProduto'
      '                              ,nCdLoja'
      '                              ,cNmLoja)'
      '                        SELECT :nCdProduto'
      '                              ,nCdLoja'
      '                              ,cNmLoja'
      '                          FROM Loja'
      '                         WHERE cFlgVenda = 1'
      ''
      'UPDATE #Temp_Grade_Loja'
      '   SET nQtdeGrade = IsNull((SELECT nQtdePed'
      
        '                              FROM ItemPedido with (INDEX = IN01' +
        '_ItemPedido)'
      
        '                             WHERE nCdItemPedidoPai = @nCdItemPe' +
        'dido'
      
        '                               AND nCdLoja          = #Temp_Grad' +
        'e_Loja.nCdLoja'
      '                               AND nCdTipoItemPed   = 15),0)'
      ''
      'SELECT *'
      '  FROM #Temp_Grade_Loja'
      ' ORDER BY cNmLoja')
    Left = 512
    Top = 152
    object qryLojanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      DisplayLabel = 'Loja'
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryLojanQtdeGrade: TIntegerField
      DisplayLabel = 'Qtde Grades'
      FieldName = 'nQtdeGrade'
    end
    object qryLojanQtdeEstoque: TIntegerField
      DisplayLabel = 'Informa'#231#245'es|Estoque'
      FieldName = 'nQtdeEstoque'
    end
    object qryLojanVendaMarca: TBCDField
      DisplayLabel = 'Informa'#231#245'es|% Marca'
      FieldName = 'nVendaMarca'
      Precision = 12
      Size = 2
    end
    object qryLojanVendaLinha: TBCDField
      DisplayLabel = 'Informa'#231#245'es|% Linha'
      FieldName = 'nVendaLinha'
      Precision = 12
      Size = 2
    end
    object qryLojanVendaProduto: TBCDField
      DisplayLabel = 'Informa'#231#245'es|% Produto'
      FieldName = 'nVendaProduto'
      Precision = 12
      Size = 2
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 488
    Top = 240
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Grade_Loja'#39') IS NULL)'
      'BEGIN'
      ''
      
        #9'CREATE TABLE #Temp_Grade_Loja (nCdProduto    int           not ' +
        'null'
      #9#9#9#9#9#9#9#9'   ,nCdLoja       int           not null'
      #9#9#9#9#9#9#9#9'   ,cNmLoja       varchar(50)   not null'
      #9#9#9#9#9#9#9#9'   ,nQtdeGrade    int           default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nQtdeEstoque  int           default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaMarca   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaLinha   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,nVendaProduto decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9'   ,PRIMARY KEY (nCdProduto, nCdLoja))'
      ''
      'END')
    Left = 224
    Top = 192
  end
end
