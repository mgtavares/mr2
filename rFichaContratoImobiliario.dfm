object rptFichaContratoImobiliario: TrptFichaContratoImobiliario
  Left = -8
  Top = -8
  Width = 1152
  Height = 850
  Caption = 'rptFichaContratoImobiliario'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object QuickRep1: TQuickRep
    Left = 0
    Top = 0
    Width = 794
    Height = 1123
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    DataSet = uspRelatorio
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE'
      'NUMEROFICHA')
    Functions.DATA = (
      '0'
      '0'
      #39#39
      'NUMEROFICHA=NUMEROFICHA+1')
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      20.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    object QRGroup1: TQRGroup
      Left = 38
      Top = 8
      Width = 718
      Height = 225
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Frame.Style = psDot
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        595.312500000000000000
        1899.708333333333000000)
      Expression = 'uspRelatorio.nPaginaFicha'
      FooterBand = QRBand5
      Master = QuickRep1
      ReprintOnNewPage = False
      object QRLabel1: TQRLabel
        Left = 526
        Top = 104
        Width = 57
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1391.708333333333000000
          275.166666666666700000
          150.812500000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Contrato'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText8: TQRDBText
        Left = 589
        Top = 104
        Width = 97
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1558.395833333333000000
          275.166666666666700000
          256.645833333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cNumContrato'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRDBText1: TQRDBText
        Left = 589
        Top = 85
        Width = 89
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1558.395833333333000000
          224.895833333333300000
          235.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'dDtContrato'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRLabel14: TQRLabel
        Left = 498
        Top = 85
        Width = 85
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1317.625000000000000000
          224.895833333333300000
          224.895833333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Dt. Contrato'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText11: TQRDBText
        Left = 562
        Top = 122
        Width = 153
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1486.958333333333000000
          322.791666666666700000
          404.812500000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'nValContrato'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Mask = '#,##0.00'
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel16: TQRLabel
        Left = 484
        Top = 122
        Width = 99
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1280.583333333333000000
          322.791666666666700000
          261.937500000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Valor Contrato'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel17: TQRLabel
        Left = 18
        Top = 85
        Width = 50
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          47.625000000000000000
          224.895833333333300000
          132.291666666666700000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Cliente'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel19: TQRLabel
        Left = 18
        Top = 104
        Width = 50
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          47.625000000000000000
          275.166666666666700000
          132.291666666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'C'#244'njuge'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText13: TQRDBText
        Left = 76
        Top = 158
        Width = 149
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          418.041666666666700000
          394.229166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cTelefoneRes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText15: TQRDBText
        Left = 76
        Top = 122
        Width = 120
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          322.791666666666700000
          317.500000000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cCNPJCPFFormatado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel18: TQRLabel
        Left = 32
        Top = 122
        Width = 36
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          84.666666666666670000
          322.791666666666700000
          95.250000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'C.P.F'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel20: TQRLabel
        Left = 4
        Top = 158
        Width = 64
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          418.041666666666700000
          169.333333333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Fone Res.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText16: TQRDBText
        Left = 324
        Top = 122
        Width = 85
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          857.250000000000000000
          322.791666666666700000
          224.895833333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cRGFormatado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel21: TQRLabel
        Left = 291
        Top = 122
        Width = 22
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          769.937500000000000000
          322.791666666666700000
          58.208333333333330000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'R.G'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText17: TQRDBText
        Left = 324
        Top = 158
        Width = 149
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          857.250000000000000000
          418.041666666666700000
          394.229166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cTelefoneMovelFormatado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel22: TQRLabel
        Left = 263
        Top = 158
        Width = 50
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          695.854166666666700000
          418.041666666666700000
          132.291666666666700000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Celular'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel23: TQRLabel
        Left = 512
        Top = 158
        Width = 71
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1354.666666666667000000
          418.041666666666700000
          187.854166666666700000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Fone Coml.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText18: TQRDBText
        Left = 588
        Top = 158
        Width = 125
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1555.750000000000000000
          418.041666666666700000
          330.729166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryPessoaFisica
        DataField = 'cTelefoneEmpTrabFormatado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel4: TQRLabel
        Left = 25
        Top = 176
        Width = 43
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          66.145833333333330000
          465.666666666666700000
          113.770833333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Im'#243'vel'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText19: TQRDBText
        Left = 76
        Top = 176
        Width = 197
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          465.666666666666700000
          521.229166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cNmEmpImobiliario'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRDBText20: TQRDBText
        Left = 324
        Top = 176
        Width = 149
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          857.250000000000000000
          465.666666666666700000
          394.229166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cNmBlocoEmpImobiliario'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel5: TQRLabel
        Left = 277
        Top = 176
        Width = 36
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          732.895833333333300000
          465.666666666666700000
          95.250000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Bloco'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText21: TQRDBText
        Left = 588
        Top = 176
        Width = 100
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1555.750000000000000000
          465.666666666666700000
          264.583333333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cNrUnidade'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRLabel6: TQRLabel
        Left = 484
        Top = 176
        Width = 99
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1280.583333333333000000
          465.666666666666700000
          261.937500000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'N'#250'mero/Unidade'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel8: TQRLabel
        Left = 4
        Top = 205
        Width = 50
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          542.395833333333300000
          132.291666666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Parcela'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel9: TQRLabel
        Left = 121
        Top = 206
        Width = 71
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          320.145833333333300000
          545.041666666666700000
          187.854166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Vencimento'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel10: TQRLabel
        Left = 206
        Top = 206
        Width = 78
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          545.041666666666700000
          545.041666666666700000
          206.375000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Valor Parc.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel12: TQRLabel
        Left = 300
        Top = 206
        Width = 43
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          793.750000000000000000
          545.041666666666700000
          113.770833333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = #205'ndice'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel24: TQRLabel
        Left = 384
        Top = 206
        Width = 85
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1016.000000000000000000
          545.041666666666700000
          224.895833333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Valor Atual.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRDBText22: TQRDBText
        Left = 76
        Top = 140
        Width = 300
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          370.416666666666700000
          793.750000000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cEmail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel27: TQRLabel
        Left = 32
        Top = 140
        Width = 36
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          84.666666666666670000
          370.416666666666700000
          95.250000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Email'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel26: TQRLabel
        Left = 488
        Top = 206
        Width = 71
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1291.166666666667000000
          545.041666666666700000
          187.854166666666700000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Valor Pago'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel25: TQRLabel
        Left = 578
        Top = 206
        Width = 71
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1529.291666666667000000
          545.041666666666700000
          187.854166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Data Pagto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 9
      end
      object QRLabel11: TQRLabel
        Left = 7
        Top = 60
        Width = 281
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          18.520833333333330000
          158.750000000000000000
          743.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Ficha Contrato Financiamento Imobili'#225'rio'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object lblEmpresa: TQRLabel
        Left = 7
        Top = 45
        Width = 71
        Height = 15
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          39.687500000000000000
          18.520833333333330000
          119.062500000000000000
          187.854166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'lblEmpresa'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRSysData3: TQRSysData
        Left = 589
        Top = 50
        Width = 78
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1558.395833333333000000
          132.291666666666700000
          206.375000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        Color = clWhite
        Data = qrsDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        FontSize = 8
      end
      object QRShape1: TQRShape
        Left = 0
        Top = 195
        Width = 713
        Height = 4
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          10.583333333333330000
          0.000000000000000000
          515.937500000000000000
          1886.479166666667000000)
        Shape = qrsRectangle
      end
      object QRDBText9: TQRDBText
        Left = 76
        Top = 83
        Width = 413
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          219.604166666666700000
          1092.729166666667000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryTerceiro
        DataField = 'cNmTerceiro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = [fsBold, fsUnderline]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRDBText12: TQRDBText
        Left = 76
        Top = 104
        Width = 43
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          201.083333333333300000
          275.166666666666700000
          113.770833333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = qryPessoaFisica
        DataField = 'cNmCjg'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRDBText23: TQRDBText
        Left = 588
        Top = 140
        Width = 120
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1555.750000000000000000
          370.416666666666700000
          317.500000000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = qryIndiceReajuste
        DataField = 'cNmIndiceReajuste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRLabel2: TQRLabel
        Left = 477
        Top = 140
        Width = 106
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1262.062500000000000000
          370.416666666666700000
          280.458333333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = #205'ndice Reajuste'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRShape11: TQRShape
        Left = 0
        Top = 74
        Width = 720
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          0.000000000000000000
          195.791666666666700000
          1905.000000000000000000)
        Pen.Width = 4
        Shape = qrsHorLine
      end
    end
    object QRBand3: TQRBand
      Left = 38
      Top = 233
      Width = 718
      Height = 25
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.145833333333330000
        1899.708333333333000000)
      BandType = rbDetail
      object QRShape2: TQRShape
        Left = 473
        Top = 0
        Width = 99
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          1251.479166666667000000
          0.000000000000000000
          261.937500000000000000)
        Shape = qrsRectangle
      end
      object QRShape3: TQRShape
        Left = 571
        Top = 0
        Width = 86
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          1510.770833333333000000
          0.000000000000000000
          227.541666666666700000)
        Shape = qrsRectangle
      end
      object QRShape4: TQRShape
        Left = 368
        Top = 0
        Width = 106
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          973.666666666666700000
          0.000000000000000000
          280.458333333333300000)
        Shape = qrsRectangle
      end
      object QRShape5: TQRShape
        Left = 284
        Top = 0
        Width = 85
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          751.416666666666700000
          0.000000000000000000
          224.895833333333300000)
        Shape = qrsRectangle
      end
      object QRShape6: TQRShape
        Left = 202
        Top = 0
        Width = 83
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          534.458333333333300000
          0.000000000000000000
          219.604166666666700000)
        Shape = qrsRectangle
      end
      object QRDBText2: TQRDBText
        Left = 201
        Top = 4
        Width = 81
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          531.812500000000000000
          10.583333333333330000
          214.312500000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'nValTit'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Mask = '##,###,#00.00'
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRShape7: TQRShape
        Left = 102
        Top = 0
        Width = 101
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          269.875000000000000000
          0.000000000000000000
          267.229166666666700000)
        Shape = qrsRectangle
      end
      object QRDBText4: TQRDBText
        Left = 122
        Top = 4
        Width = 50
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          322.791666666666700000
          10.583333333333330000
          132.291666666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'dDtVenc'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRShape8: TQRShape
        Left = 45
        Top = 0
        Width = 73
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          119.062500000000000000
          0.000000000000000000
          193.145833333333300000)
        Shape = qrsRectangle
      end
      object QRDBText5: TQRDBText
        Left = 63
        Top = 4
        Width = 53
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          166.687500000000000000
          10.583333333333330000
          140.229166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cTipoParcela'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
      object QRShape9: TQRShape
        Left = 0
        Top = 0
        Width = 60
        Height = 25
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          66.145833333333330000
          0.000000000000000000
          0.000000000000000000
          158.750000000000000000)
        Shape = qrsRectangle
      end
      object QRDBText3: TQRDBText
        Left = 2
        Top = 4
        Width = 55
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          5.291666666666667000
          10.583333333333330000
          145.520833333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = uspRelatorio
        DataField = 'cParcelas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 8
      end
    end
    object QRBand5: TQRBand
      Left = 38
      Top = 258
      Width = 718
      Height = 25
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Frame.Width = 2
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        66.145833333333330000
        1899.708333333333000000)
      BandType = rbGroupFooter
      object QRLabel15: TQRLabel
        Left = 640
        Top = 8
        Width = 76
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1693.333333333333000000
          21.166666666666670000
          201.083333333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'www.er2soft.com.br'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 7
      end
      object QRLabel36: TQRLabel
        Left = 4
        Top = 8
        Width = 190
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          10.583333333333330000
          21.166666666666670000
          502.708333333333300000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'ER2SOFT - Solu'#231#245'es inteligentes para o seu neg'#243'cio.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 7
      end
      object QRShape10: TQRShape
        Left = 0
        Top = -1
        Width = 720
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          0.000000000000000000
          -2.645833333333333000
          1905.000000000000000000)
        Pen.Width = 4
        Shape = qrsHorLine
      end
    end
  end
  object uspRelatorio: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_FICHA_CONTRATO_IMOBILIARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdContrato'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 784
    Top = 128
    object uspRelatorionCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object uspRelatoriocNumContrato: TStringField
      FieldName = 'cNumContrato'
      Size = 15
    end
    object uspRelatoriodDtContrato: TDateTimeField
      FieldName = 'dDtContrato'
    end
    object uspRelatorionCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object uspRelatorionCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object uspRelatoriodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object uspRelatorionValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object uspRelatoriodDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object uspRelatorionSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValContrato: TBCDField
      FieldName = 'nValContrato'
      Precision = 12
      Size = 2
    end
    object uspRelatoriocNmEmpImobiliario: TStringField
      FieldName = 'cNmEmpImobiliario'
      Size = 100
    end
    object uspRelatoriocNmBlocoEmpImobiliario: TStringField
      FieldName = 'cNmBlocoEmpImobiliario'
      Size = 50
    end
    object uspRelatoriocNrUnidade: TStringField
      FieldName = 'cNrUnidade'
      Size = 15
    end
    object uspRelatoriorank: TLargeintField
      FieldName = 'rank'
    end
    object uspRelatorionPaginaFicha: TIntegerField
      FieldName = 'nPaginaFicha'
    end
    object uspRelatoriocTipoParcela: TStringField
      FieldName = 'cTipoParcela'
      Size = 9
    end
    object uspRelatoriocParcelas: TStringField
      FieldName = 'cParcelas'
      Size = 21
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '      ,(SELECT TOP 1 dbo.fn_formatarTelefoneDDD(cTelefone)'
      '          FROM Endereco'
      '         WHERE Endereco.nCdTerceiro = Terceiro.nCdTerceiro'
      '           AND Endereco.nCdStatus   = 1) as cTelefoneRes'
      
        '      ,dbo.fn_FormatarCPF(Terceiro.cCNPJCPF) as cCNPJCPFFormatad' +
        'o'
      '      ,dbo.fn_FormatarRG(Terceiro.cRG) as cRGFormatado'
      
        '      ,dbo.fn_formatarTelefoneDDD(cTelefoneMovel) as cTelefoneMo' +
        'velFormatado'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro =  :nPK'
      '')
    Left = 784
    Top = 168
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocIDExterno: TStringField
      FieldName = 'cIDExterno'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceirocNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirodDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryTerceirocFlgMicroEmpresa: TIntegerField
      FieldName = 'cFlgMicroEmpresa'
    end
    object qryTerceirocFlgOptSimples: TIntegerField
      FieldName = 'cFlgOptSimples'
    end
    object qryTerceirocFlgContICMS: TIntegerField
      FieldName = 'cFlgContICMS'
    end
    object qryTerceironCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
    object qryTerceironCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryTerceironCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryTerceironCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceirocFlgIsentoCNPJCPF: TIntegerField
      FieldName = 'cFlgIsentoCNPJCPF'
    end
    object qryTerceironCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryTerceirocAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryTerceirocNumConta: TStringField
      FieldName = 'cNumConta'
      Size = 11
    end
    object qryTerceirocDigitoConta: TStringField
      FieldName = 'cDigitoConta'
      FixedChar = True
      Size = 1
    end
    object qryTerceironCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryTerceironCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryTerceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTerceironValCredFinanc: TBCDField
      FieldName = 'nValCredFinanc'
      Precision = 12
      Size = 2
    end
    object qryTerceironCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryTerceironPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      Precision = 5
      Size = 2
    end
    object qryTerceironPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 5
      Size = 2
    end
    object qryTerceiroiQtdeChequeDev: TIntegerField
      FieldName = 'iQtdeChequeDev'
    end
    object qryTerceirodDtUltChequeDev: TDateTimeField
      FieldName = 'dDtUltChequeDev'
    end
    object qryTerceironValMaiorChequeDev: TBCDField
      FieldName = 'nValMaiorChequeDev'
      Precision = 12
      Size = 2
    end
    object qryTerceirocFlgCobrarJuro: TIntegerField
      FieldName = 'cFlgCobrarJuro'
    end
    object qryTerceironValMultaAtraso: TBCDField
      FieldName = 'nValMultaAtraso'
      Precision = 12
      Size = 2
    end
    object qryTerceironPercJuroDia: TBCDField
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
    object qryTerceironCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryTerceironValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      Precision = 12
      Size = 2
    end
    object qryTerceironValCreditoUtil: TBCDField
      FieldName = 'nValCreditoUtil'
      Precision = 12
      Size = 2
    end
    object qryTerceironValPedidoAberto: TBCDField
      FieldName = 'nValPedidoAberto'
      Precision = 12
      Size = 2
    end
    object qryTerceironValRecAtraso: TBCDField
      FieldName = 'nValRecAtraso'
      Precision = 12
      Size = 2
    end
    object qryTerceirodMaiorRecAtraso: TDateTimeField
      FieldName = 'dMaiorRecAtraso'
    end
    object qryTerceirocFlgBloqPedVenda: TIntegerField
      FieldName = 'cFlgBloqPedVenda'
    end
    object qryTerceirocRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryTerceironCdBanco2: TIntegerField
      FieldName = 'nCdBanco2'
    end
    object qryTerceirocAgencia2: TStringField
      FieldName = 'cAgencia2'
      FixedChar = True
      Size = 4
    end
    object qryTerceirocNumConta2: TStringField
      FieldName = 'cNumConta2'
      Size = 11
    end
    object qryTerceirocDigitoConta2: TStringField
      FieldName = 'cDigitoConta2'
      FixedChar = True
      Size = 1
    end
    object qryTerceirocNmContato: TStringField
      FieldName = 'cNmContato'
      Size = 50
    end
    object qryTerceirocTelefone1: TStringField
      FieldName = 'cTelefone1'
    end
    object qryTerceirocTelefone2: TStringField
      FieldName = 'cTelefone2'
    end
    object qryTerceirocTelefoneMovel: TStringField
      FieldName = 'cTelefoneMovel'
    end
    object qryTerceirocFax: TStringField
      FieldName = 'cFax'
    end
    object qryTerceirocEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryTerceirocWebSite: TStringField
      FieldName = 'cWebSite'
      Size = 50
    end
    object qryTerceirocOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryTerceironCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryTerceironCdGrupoComissao: TIntegerField
      FieldName = 'nCdGrupoComissao'
    end
    object qryTerceirocNmTitularConta1: TStringField
      FieldName = 'cNmTitularConta1'
      Size = 50
    end
    object qryTerceirocNmTitularConta2: TStringField
      FieldName = 'cNmTitularConta2'
      Size = 50
    end
    object qryTerceironCdLojaTerceiro: TIntegerField
      FieldName = 'nCdLojaTerceiro'
    end
    object qryTerceirocFlgTipoFat: TIntegerField
      FieldName = 'cFlgTipoFat'
    end
    object qryTerceirodDtUltNegocio: TDateTimeField
      FieldName = 'dDtUltNegocio'
    end
    object qryTerceironValChequePendComp: TBCDField
      FieldName = 'nValChequePendComp'
      Precision = 12
      Size = 2
    end
    object qryTerceirocFlgHomologado: TIntegerField
      FieldName = 'cFlgHomologado'
    end
    object qryTerceironCdTipoTabPreco: TIntegerField
      FieldName = 'nCdTipoTabPreco'
    end
    object qryTerceirodDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTerceirocTelefoneRes: TStringField
      FieldName = 'cTelefoneRes'
      ReadOnly = True
    end
    object qryTerceirocCNPJCPFFormatado: TStringField
      FieldName = 'cCNPJCPFFormatado'
      ReadOnly = True
      Size = 14
    end
    object qryTerceirocRGFormatado: TStringField
      FieldName = 'cRGFormatado'
      ReadOnly = True
      Size = 14
    end
    object qryTerceirocTelefoneMovelFormatado: TStringField
      FieldName = 'cTelefoneMovelFormatado'
      ReadOnly = True
    end
  end
  object qryPessoaFisica: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      
        '      ,dbo.fn_formatarTelefoneDDD(cTelefoneEmpTrab) as cTelefone' +
        'EmpTrabFormatado'
      '  FROM PessoaFisica'
      ' WHERE nCdTerceiro = :nPK')
    Left = 784
    Top = 208
    object qryPessoaFisicanCdPessoaFisica: TAutoIncField
      FieldName = 'nCdPessoaFisica'
      ReadOnly = True
    end
    object qryPessoaFisicanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryPessoaFisicadDtNasc: TDateTimeField
      FieldName = 'dDtNasc'
    end
    object qryPessoaFisicacUFRG: TStringField
      FieldName = 'cUFRG'
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoComprovRG: TIntegerField
      FieldName = 'nCdTabTipoComprovRG'
    end
    object qryPessoaFisicacOBSRG: TStringField
      FieldName = 'cOBSRG'
      Size = 30
    end
    object qryPessoaFisicadDtEmissaoRG: TDateTimeField
      FieldName = 'dDtEmissaoRG'
    end
    object qryPessoaFisicanCdTabTipoSexo: TIntegerField
      FieldName = 'nCdTabTipoSexo'
    end
    object qryPessoaFisicanCdTabTipoEstadoCivil: TIntegerField
      FieldName = 'nCdTabTipoEstadoCivil'
    end
    object qryPessoaFisicaiNrDependentes: TIntegerField
      FieldName = 'iNrDependentes'
    end
    object qryPessoaFisicacNaturalidade: TStringField
      FieldName = 'cNaturalidade'
      Size = 50
    end
    object qryPessoaFisicanCdTabTipoGrauEscola: TIntegerField
      FieldName = 'nCdTabTipoGrauEscola'
    end
    object qryPessoaFisicaiDiaVenctoPref: TIntegerField
      FieldName = 'iDiaVenctoPref'
    end
    object qryPessoaFisicacNmEmpTrab: TStringField
      FieldName = 'cNmEmpTrab'
      Size = 50
    end
    object qryPessoaFisicacTelefoneEmpTrab: TStringField
      FieldName = 'cTelefoneEmpTrab'
    end
    object qryPessoaFisicacRamalEmpTrab: TStringField
      FieldName = 'cRamalEmpTrab'
      Size = 5
    end
    object qryPessoaFisicacEnderecoEmpTrab: TStringField
      FieldName = 'cEnderecoEmpTrab'
      Size = 50
    end
    object qryPessoaFisicaiNrEnderecoEmpTrab: TIntegerField
      FieldName = 'iNrEnderecoEmpTrab'
    end
    object qryPessoaFisicacBairroEmpTrab: TStringField
      FieldName = 'cBairroEmpTrab'
      Size = 50
    end
    object qryPessoaFisicacCidadeEmpTrab: TStringField
      FieldName = 'cCidadeEmpTrab'
      Size = 50
    end
    object qryPessoaFisicacUFEmpTrab: TStringField
      FieldName = 'cUFEmpTrab'
      Size = 2
    end
    object qryPessoaFisicacCEPEmpTrab: TStringField
      FieldName = 'cCEPEmpTrab'
      FixedChar = True
      Size = 8
    end
    object qryPessoaFisicanCdTabTipoClasseProf: TIntegerField
      FieldName = 'nCdTabTipoClasseProf'
    end
    object qryPessoaFisicanCdTabTipoProfissao: TIntegerField
      FieldName = 'nCdTabTipoProfissao'
    end
    object qryPessoaFisicacCargoTrab: TStringField
      FieldName = 'cCargoTrab'
      Size = 30
    end
    object qryPessoaFisicadDtAdmissao: TDateTimeField
      FieldName = 'dDtAdmissao'
    end
    object qryPessoaFisicanCdTabTipoComprovAdm: TIntegerField
      FieldName = 'nCdTabTipoComprovAdm'
    end
    object qryPessoaFisicacOBSAdmissao: TStringField
      FieldName = 'cOBSAdmissao'
      Size = 30
    end
    object qryPessoaFisicanValRendaBruta: TBCDField
      FieldName = 'nValRendaBruta'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoComprovRenda: TIntegerField
      FieldName = 'nCdTabTipoComprovRenda'
    end
    object qryPessoaFisicacOBSRenda: TStringField
      FieldName = 'cOBSRenda'
      Size = 30
    end
    object qryPessoaFisicacNmCjg: TStringField
      FieldName = 'cNmCjg'
      Size = 50
    end
    object qryPessoaFisicadDtNascCjg: TDateTimeField
      FieldName = 'dDtNascCjg'
    end
    object qryPessoaFisicacRGCjg: TStringField
      FieldName = 'cRGCjg'
      Size = 15
    end
    object qryPessoaFisicacUFRGCjg: TStringField
      FieldName = 'cUFRGCjg'
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoComprovRGCjg: TIntegerField
      FieldName = 'nCdTabTipoComprovRGCjg'
    end
    object qryPessoaFisicacOBSRGCjg: TStringField
      FieldName = 'cOBSRGCjg'
      Size = 30
    end
    object qryPessoaFisicadDtEmissaoRGCjg: TDateTimeField
      FieldName = 'dDtEmissaoRGCjg'
    end
    object qryPessoaFisicacCPFCjg: TStringField
      FieldName = 'cCPFCjg'
      Size = 11
    end
    object qryPessoaFisicacTelefoneCelCjg: TStringField
      FieldName = 'cTelefoneCelCjg'
    end
    object qryPessoaFisicacNmEmpresaTrabCjg: TStringField
      FieldName = 'cNmEmpresaTrabCjg'
      Size = 50
    end
    object qryPessoaFisicacTelefoneEmpTrabCjg: TStringField
      FieldName = 'cTelefoneEmpTrabCjg'
    end
    object qryPessoaFisicacRamalEmpTrabCjg: TStringField
      FieldName = 'cRamalEmpTrabCjg'
      Size = 5
    end
    object qryPessoaFisicanCdTabTipoClasseProfCjg: TIntegerField
      FieldName = 'nCdTabTipoClasseProfCjg'
    end
    object qryPessoaFisicanCdTabTipoProfissaoCjg: TIntegerField
      FieldName = 'nCdTabTipoProfissaoCjg'
    end
    object qryPessoaFisicacCargoTrabCjg: TStringField
      FieldName = 'cCargoTrabCjg'
      Size = 30
    end
    object qryPessoaFisicadDtAdmissaoCjg: TDateTimeField
      FieldName = 'dDtAdmissaoCjg'
    end
    object qryPessoaFisicanCdTabTipoComprovCjg: TIntegerField
      FieldName = 'nCdTabTipoComprovCjg'
    end
    object qryPessoaFisicacOBSAdmissaoCjg: TStringField
      FieldName = 'cOBSAdmissaoCjg'
      Size = 30
    end
    object qryPessoaFisicanValRendaBrutaCjg: TBCDField
      FieldName = 'nValRendaBrutaCjg'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoComprovRendaCjg: TIntegerField
      FieldName = 'nCdTabTipoComprovRendaCjg'
    end
    object qryPessoaFisicacOBSRendaCjg: TStringField
      FieldName = 'cOBSRendaCjg'
      Size = 30
    end
    object qryPessoaFisicacNmRendaAdd1: TStringField
      FieldName = 'cNmRendaAdd1'
      Size = 30
    end
    object qryPessoaFisicanValRendaAdd1: TBCDField
      FieldName = 'nValRendaAdd1'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoRendaAdd1: TIntegerField
      FieldName = 'nCdTabTipoRendaAdd1'
    end
    object qryPessoaFisicanCdTabTipoFonteRendaAdd1: TIntegerField
      FieldName = 'nCdTabTipoFonteRendaAdd1'
    end
    object qryPessoaFisicanCdTabTipoComprovRendaAdd1: TIntegerField
      FieldName = 'nCdTabTipoComprovRendaAdd1'
    end
    object qryPessoaFisicacOBSRendaAdd1: TStringField
      FieldName = 'cOBSRendaAdd1'
      Size = 30
    end
    object qryPessoaFisicacNmRendaAdd2: TStringField
      FieldName = 'cNmRendaAdd2'
      Size = 30
    end
    object qryPessoaFisicanValRendaAdd2: TBCDField
      FieldName = 'nValRendaAdd2'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoRendaAdd2: TIntegerField
      FieldName = 'nCdTabTipoRendaAdd2'
    end
    object qryPessoaFisicanCdTabTipoFonteRendaAdd2: TIntegerField
      FieldName = 'nCdTabTipoFonteRendaAdd2'
    end
    object qryPessoaFisicanCdTabTipoComprovRendaAdd2: TIntegerField
      FieldName = 'nCdTabTipoComprovRendaAdd2'
    end
    object qryPessoaFisicacOBSRendaAdd2: TStringField
      FieldName = 'cOBSRendaAdd2'
      Size = 30
    end
    object qryPessoaFisicacNmRendaAdd3: TStringField
      FieldName = 'cNmRendaAdd3'
      Size = 30
    end
    object qryPessoaFisicanValRendaAdd3: TBCDField
      FieldName = 'nValRendaAdd3'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicanCdTabTipoRendaAdd3: TIntegerField
      FieldName = 'nCdTabTipoRendaAdd3'
    end
    object qryPessoaFisicanCdTabTipoFonteRendaAdd3: TIntegerField
      FieldName = 'nCdTabTipoFonteRendaAdd3'
    end
    object qryPessoaFisicanCdTabTipoComprovRendaAdd3: TIntegerField
      FieldName = 'nCdTabTipoComprovRendaAdd3'
    end
    object qryPessoaFisicacOBSRendaAdd3: TStringField
      FieldName = 'cOBSRendaAdd3'
      Size = 30
    end
    object qryPessoaFisicacNmRefPessoal1: TStringField
      FieldName = 'cNmRefPessoal1'
      Size = 50
    end
    object qryPessoaFisicacTelefoneRefPessoal1: TStringField
      FieldName = 'cTelefoneRefPessoal1'
    end
    object qryPessoaFisicacNmRefPessoal2: TStringField
      FieldName = 'cNmRefPessoal2'
      Size = 50
    end
    object qryPessoaFisicacTelefoneRefPessoal2: TStringField
      FieldName = 'cTelefoneRefPessoal2'
    end
    object qryPessoaFisicacNmRefPessoal3: TStringField
      FieldName = 'cNmRefPessoal3'
      Size = 50
    end
    object qryPessoaFisicacTelefoneRefPessoal3: TStringField
      FieldName = 'cTelefoneRefPessoal3'
    end
    object qryPessoaFisicacNmRefComercial1: TStringField
      FieldName = 'cNmRefComercial1'
      Size = 50
    end
    object qryPessoaFisicacRefComercial1: TStringField
      FieldName = 'cRefComercial1'
      Size = 50
    end
    object qryPessoaFisicacNmRefComercial2: TStringField
      FieldName = 'cNmRefComercial2'
      Size = 50
    end
    object qryPessoaFisicacRefComercial2: TStringField
      FieldName = 'cRefComercial2'
      Size = 50
    end
    object qryPessoaFisicacNmRefComercial3: TStringField
      FieldName = 'cNmRefComercial3'
      Size = 50
    end
    object qryPessoaFisicacRefComercial3: TStringField
      FieldName = 'cRefComercial3'
      Size = 50
    end
    object qryPessoaFisicanCdUsuarioCadastro: TIntegerField
      FieldName = 'nCdUsuarioCadastro'
    end
    object qryPessoaFisicanCdLojaCadastro: TIntegerField
      FieldName = 'nCdLojaCadastro'
    end
    object qryPessoaFisicanCdUsuarioUltAlt: TIntegerField
      FieldName = 'nCdUsuarioUltAlt'
    end
    object qryPessoaFisicanCdLojaUltAlt: TIntegerField
      FieldName = 'nCdLojaUltAlt'
    end
    object qryPessoaFisicadDtUltAlt: TDateTimeField
      FieldName = 'dDtUltAlt'
    end
    object qryPessoaFisicanCdTabTipoOrigem: TIntegerField
      FieldName = 'nCdTabTipoOrigem'
    end
    object qryPessoaFisicanCdTabTipoSituacao: TIntegerField
      FieldName = 'nCdTabTipoSituacao'
    end
    object qryPessoaFisicacNmMae: TStringField
      FieldName = 'cNmMae'
      Size = 50
    end
    object qryPessoaFisicacNmPai: TStringField
      FieldName = 'cNmPai'
      Size = 50
    end
    object qryPessoaFisicanCdUsuarioAprova: TIntegerField
      FieldName = 'nCdUsuarioAprova'
    end
    object qryPessoaFisicadDtUsuarioAprova: TDateTimeField
      FieldName = 'dDtUsuarioAprova'
    end
    object qryPessoaFisicacCaixaPostal: TStringField
      FieldName = 'cCaixaPostal'
      Size = 15
    end
    object qryPessoaFisicacTelefoneRec1: TStringField
      FieldName = 'cTelefoneRec1'
    end
    object qryPessoaFisicacTelefoneRec2: TStringField
      FieldName = 'cTelefoneRec2'
    end
    object qryPessoaFisicacNmContatoRec1: TStringField
      FieldName = 'cNmContatoRec1'
      Size = 35
    end
    object qryPessoaFisicacNmContatoRec2: TStringField
      FieldName = 'cNmContatoRec2'
      Size = 35
    end
    object qryPessoaFisicacCNPJEmpTrab: TStringField
      FieldName = 'cCNPJEmpTrab'
      Size = 14
    end
    object qryPessoaFisicacComplementocEndEmpTrab: TStringField
      FieldName = 'cComplementocEndEmpTrab'
      Size = 35
    end
    object qryPessoaFisicacCNPJEmpTrabCjg: TStringField
      FieldName = 'cCNPJEmpTrabCjg'
      Size = 14
    end
    object qryPessoaFisicacFlgRendaCjgLimite: TIntegerField
      FieldName = 'cFlgRendaCjgLimite'
    end
    object qryPessoaFisicacRefComercialGeral: TStringField
      FieldName = 'cRefComercialGeral'
      Size = 200
    end
    object qryPessoaFisicanCdBancoRef: TIntegerField
      FieldName = 'nCdBancoRef'
    end
    object qryPessoaFisicacAgenciaRef: TStringField
      FieldName = 'cAgenciaRef'
      Size = 4
    end
    object qryPessoaFisicacContaBancariaRef: TStringField
      FieldName = 'cContaBancariaRef'
      Size = 15
    end
    object qryPessoaFisicanCdTipoContaBancariaRef: TIntegerField
      FieldName = 'nCdTipoContaBancariaRef'
    end
    object qryPessoaFisicacFlgCartaoCredito: TIntegerField
      FieldName = 'cFlgCartaoCredito'
    end
    object qryPessoaFisicacFlgTalaoCheque: TIntegerField
      FieldName = 'cFlgTalaoCheque'
    end
    object qryPessoaFisicacNrDocOutro: TStringField
      FieldName = 'cNrDocOutro'
    end
    object qryPessoaFisicanCdTabTipoComprovDocOutro: TIntegerField
      FieldName = 'nCdTabTipoComprovDocOutro'
    end
    object qryPessoaFisicacCepCxPostal: TStringField
      FieldName = 'cCepCxPostal'
      FixedChar = True
      Size = 8
    end
    object qryPessoaFisicadDtProxSPC: TDateTimeField
      FieldName = 'dDtProxSPC'
    end
    object qryPessoaFisicacOBSSituacaoCadastro: TStringField
      FieldName = 'cOBSSituacaoCadastro'
      Size = 50
    end
    object qryPessoaFisicanPercEntrada: TBCDField
      FieldName = 'nPercEntrada'
      Precision = 12
      Size = 2
    end
    object qryPessoaFisicacFlgBloqAtuLimAutom: TIntegerField
      FieldName = 'cFlgBloqAtuLimAutom'
    end
    object qryPessoaFisicacTelefoneEmpTrabFormatado: TStringField
      FieldName = 'cTelefoneEmpTrabFormatado'
      ReadOnly = True
    end
  end
  object qryIndiceReajuste: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Indicereajuste'
      'WHERE nCdIndiceReajuste = :nPK')
    Left = 784
    Top = 248
    object qryIndiceReajustenCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryIndiceReajustecNmIndiceReajuste: TStringField
      FieldName = 'cNmIndiceReajuste'
      Size = 50
    end
  end
end
