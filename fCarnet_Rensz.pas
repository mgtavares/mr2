unit fCarnet_Rensz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fMenu;

type
  TCarnet_Rensz_Form = class(TForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
    TCarnetRensz = class(TInterfacedObject, ICarnet)
    protected
        function ImprimirCarnet (nCdCrediario: integer) : boolean;
    end ;

var
  Carnet_Rensz_Form: TCarnet_Rensz_Form;

implementation

uses fImpDFPadrao;

{$R *.dfm}

function TCarnetRensz.ImprimirCarnet (nCdCrediario: integer) : boolean ;
begin
    frmImpDFPadrao.ImprimirCarnet(nCdCrediario) ;
end ;

end.
