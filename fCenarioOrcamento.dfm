inherited frmCenarioOrcamento: TfrmCenarioOrcamento
  Left = 63
  Top = -1
  Caption = 'Cen'#225'rio de Or'#231'amento'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 70
    Top = 38
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'd.'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 45
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 51
    Top = 86
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label4: TLabel [4]
    Left = 73
    Top = 110
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label5: TLabel [5]
    Left = 10
    Top = 134
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Or'#231'amento Base'
  end
  object Label6: TLabel [6]
    Left = 54
    Top = 158
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '% In'#237'cio'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 7
    Top = 182
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Or'#231'amento'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 46
    Top = 206
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ano Base'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 40
    Top = 230
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#234's Inicial'
  end
  object Label10: TLabel [10]
    Left = 45
    Top = 254
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#234's Final'
  end
  object Label11: TLabel [11]
    Left = 10
    Top = 278
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdOrcamento'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [14]
    Left = 96
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmOrcamento'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit6: TDBEdit [15]
    Left = 96
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nPercentInicio'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [16]
    Tag = 1
    Left = 96
    Top = 176
    Width = 169
    Height = 19
    DataField = 'nValOrcamento'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [17]
    Left = 96
    Top = 200
    Width = 65
    Height = 19
    DataField = 'iAnoBase'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit12: TDBEdit [18]
    Tag = 1
    Left = 168
    Top = 80
    Width = 578
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [19]
    Tag = 1
    Left = 168
    Top = 104
    Width = 578
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource2
    TabOrder = 13
  end
  object DBEdit14: TDBEdit [20]
    Tag = 1
    Left = 168
    Top = 128
    Width = 578
    Height = 19
    DataField = 'cNmOrcamento'
    DataSource = DataSource3
    TabOrder = 14
  end
  object DBEdit15: TDBEdit [21]
    Tag = 1
    Left = 168
    Top = 224
    Width = 177
    Height = 19
    DataField = 'cNmTabTipoMes'
    DataSource = DataSource4
    TabOrder = 15
  end
  object DBEdit16: TDBEdit [22]
    Tag = 1
    Left = 168
    Top = 248
    Width = 177
    Height = 19
    DataField = 'cNmTabTipoMes'
    DataSource = DataSource5
    TabOrder = 16
  end
  object DBEdit17: TDBEdit [23]
    Tag = 1
    Left = 168
    Top = 272
    Width = 104
    Height = 19
    DataField = 'cCdCC'
    DataSource = DataSource6
    TabOrder = 17
  end
  object DBEdit18: TDBEdit [24]
    Tag = 1
    Left = 280
    Top = 272
    Width = 466
    Height = 19
    DataField = 'cNmCC'
    DataSource = DataSource6
    TabOrder = 18
  end
  object edtCdEmpresa: TER2LookupDBEdit [25]
    Left = 96
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 25
    QueryLookup = qryEmpresa
  end
  object edtCdLoja: TER2LookupDBEdit [26]
    Left = 96
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 4
    CodigoLookup = 86
    QueryLookup = qryLoja
  end
  object edtCdOrcamentoPai: TER2LookupDBEdit [27]
    Left = 96
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdOrcamentoPai'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = edtCdOrcamentoPaiExit
    CodigoLookup = 210
    QueryLookup = qryOrcamentoBase
  end
  object edtCdMesInicial: TER2LookupDBEdit [28]
    Left = 96
    Top = 224
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoMesIni'
    DataSource = dsMaster
    TabOrder = 9
    CodigoLookup = 211
    QueryLookup = qryMesInicial
  end
  object edtCdMesFinal: TER2LookupDBEdit [29]
    Left = 96
    Top = 248
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoMesFim'
    DataSource = dsMaster
    TabOrder = 10
    CodigoLookup = 211
    QueryLookup = qryMesFinal
  end
  object edtCdCC: TER2LookupDBEdit [30]
    Left = 96
    Top = 272
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 11
    CodigoLookup = 28
    QueryLookup = qryCentroCusto
  end
  object cxButton1: TcxButton [31]
    Left = 96
    Top = 301
    Width = 177
    Height = 41
    Caption = 'Distribui'#231#227'o Or'#231'ament'#225'ria'
    TabOrder = 19
    OnClick = cxButton1Click
    Glyph.Data = {
      AA040000424DAA04000000000000360000002800000014000000130000000100
      1800000000007404000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000800000FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF800000FFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999999999999999999999999999FFFF
      FF800000FFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000999999FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFF000000009DAA009DAA009DAA009DAA000000999999FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      00000000FFFF9EF0FF9EF0FF009DAA000000999999FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000
      FFFF9EF0FF9EF0FF009DAA000000999999800000800000800000800000800000
      800000800000800000800000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FF
      FF00FFFF00FFFF000000999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF800000FFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      800000FFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
      0000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000008000008000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object cxPageControl1: TcxPageControl [32]
    Left = 16
    Top = 345
    Width = 745
    Height = 288
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 20
    ClientRectBottom = 284
    ClientRectLeft = 4
    ClientRectRight = 741
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios com Acesso Permitido ao Cen'#225'rio de Or'#231'amento'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 737
        Height = 260
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioOrcamento
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rio|C'#243'd.'
            Width = 103
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rio|Nome Usu'#225'rio'
            Width = 523
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Orcamento'
      'WHERE nCdOrcamento = :nPK')
    Left = 488
    Top = 184
    object qryMasternCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryMastercNmOrcamento: TStringField
      FieldName = 'cNmOrcamento'
      Size = 50
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdOrcamentoPai: TIntegerField
      FieldName = 'nCdOrcamentoPai'
    end
    object qryMasternPercentInicio: TBCDField
      FieldName = 'nPercentInicio'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValOrcamento: TBCDField
      FieldName = 'nValOrcamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriAnoBase: TIntegerField
      FieldName = 'iAnoBase'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMasternCdTabTipoMesIni: TIntegerField
      FieldName = 'nCdTabTipoMesIni'
    end
    object qryMasternCdTabTipoMesFim: TIntegerField
      FieldName = 'nCdTabTipoMesFim'
    end
    object qryMastercFlgDistribIniciada: TIntegerField
      FieldName = 'cFlgDistribIniciada'
    end
  end
  inherited dsMaster: TDataSource
    Left = 536
    Top = 184
  end
  inherited qryID: TADOQuery
    Left = 584
    Top = 184
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 592
    Top = 240
  end
  inherited qryStat: TADOQuery
    Left = 496
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 560
    Top = 296
  end
  inherited ImageList1: TImageList
    Left = 696
    Top = 280
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 528
    Top = 336
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 560
    Top = 400
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 472
    Top = 408
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryLoja
    Left = 568
    Top = 408
  end
  object qryOrcamentoBase: TADOQuery
    Connection = frmMenu.Connection
    AfterClose = qryOrcamentoBaseAfterClose
    AfterScroll = qryOrcamentoBaseAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdEmpresa,nCdOrcamento, cNmOrcamento, iAnoBase, nCdTabTi' +
        'poMesIni, nCdTabTipoMesFim'
      'FROM Orcamento'
      'WHERE nCdOrcamento = :nPK')
    Left = 448
    Top = 336
    object qryOrcamentoBasenCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryOrcamentoBasecNmOrcamento: TStringField
      FieldName = 'cNmOrcamento'
      Size = 50
    end
    object qryOrcamentoBaseiAnoBase: TIntegerField
      FieldName = 'iAnoBase'
    end
    object qryOrcamentoBasenCdTabTipoMesIni: TIntegerField
      FieldName = 'nCdTabTipoMesIni'
    end
    object qryOrcamentoBasenCdTabTipoMesFim: TIntegerField
      FieldName = 'nCdTabTipoMesFim'
    end
    object qryOrcamentoBasenCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
  end
  object DataSource3: TDataSource
    DataSet = qryOrcamentoBase
    Left = 576
    Top = 416
  end
  object qryMesInicial: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoMes'
      'WHERE nCdTabTipoMes = :nPK')
    Left = 376
    Top = 328
    object qryMesInicialnCdTabTipoMes: TIntegerField
      FieldName = 'nCdTabTipoMes'
    end
    object qryMesInicialcNmTabTipoMes: TStringField
      FieldName = 'cNmTabTipoMes'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryMesInicial
    Left = 584
    Top = 424
  end
  object qryMesFinal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoMes'
      'WHERE nCdTabTipoMes = :nPK')
    Left = 344
    Top = 416
    object qryMesFinalnCdTabTipoMes: TIntegerField
      FieldName = 'nCdTabTipoMes'
    end
    object qryMesFinalcNmTabTipoMes: TStringField
      FieldName = 'cNmTabTipoMes'
      Size = 50
    end
  end
  object DataSource5: TDataSource
    DataSet = qryMesFinal
    Left = 592
    Top = 432
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC, cNmCC '
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK')
    Left = 344
    Top = 320
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryCentroCusto
    Left = 600
    Top = 440
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempItemOrcamento'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempItemOrcamento (nCdItemOrcamento          i' +
        'nt'
      
        '                                    ,nCdPlanoConta             i' +
        'nt'
      
        '                                    ,cNmPlanoConta             v' +
        'archar(50)'
      
        '                                    ,nCdGrupoPlanoConta        i' +
        'nt'
      
        '                                    ,cNmGrupoPlanoConta        v' +
        'archar(50)'
      
        '                                    ,nCdPlanoContaBase         i' +
        'nt'
      
        '                                    ,cNmPlanoContaBase         v' +
        'archar(50)'
      
        '                                    ,nPercentBase              d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,nCdMetodoDistribOrcamento i' +
        'nt                     not null'
      
        '                                    ,cNmMetodoDistribOrcamento v' +
        'archar(50)'
      
        '                                    ,nValorOrcamentoConta      d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,cOBS                      v' +
        'archar(100)'
      
        '                                    ,cOBSGeral                 t' +
        'ext)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempItemOrcamento')
    Left = 408
    Top = 328
  end
  object SP_EXCLUI_CONTA_SEM_ORCAMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_EXCLUI_CONTA_SEM_ORCAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOrcamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 616
    Top = 352
  end
  object qryUsuarioOrcamento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioOrcamentoBeforePost
    OnCalcFields = qryUsuarioOrcamentoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT UsuarioOrcamento.nCdUsuarioOrcamento'
      '      ,UsuarioOrcamento.nCdOrcamento'
      '      ,UsuarioOrcamento.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM UsuarioOrcamento'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = UsuarioOrcamen' +
        'to.nCdUsuario'
      ' WHERE UsuarioOrcamento.nCdOrcamento = :nPK')
    Left = 120
    Top = 472
    object qryUsuarioOrcamentonCdUsuarioOrcamento: TIntegerField
      FieldName = 'nCdUsuarioOrcamento'
    end
    object qryUsuarioOrcamentonCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryUsuarioOrcamentonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioOrcamentocNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Calculated = True
    end
  end
  object dsUsuarioOrcamento: TDataSource
    DataSet = qryUsuarioOrcamento
    Left = 152
    Top = 472
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario FROM Usuario WHERE nCdUsuario = :nPK')
    Left = 144
    Top = 512
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
