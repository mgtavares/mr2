unit fServidorReplicacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmServidorReplicacao = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdServidor: TIntegerField;
    qryMastercNmServidor: TStringField;
    qryMastercFlgTipoServidor: TStringField;
    qryMastercIPPrimario: TStringField;
    qryMastercIPSecundario: TStringField;
    qryMastercNmDatabase: TStringField;
    qryMastercFlgAtivoReplicacao: TIntegerField;
    qryMasterdDtUltReplicacao: TDateTimeField;
    DBGridEh1: TDBGridEh;
    dsMaster: TDataSource;
    PopupMenu1: TPopupMenu;
    estarConexo1: TMenuItem;
    FiladeTransmisso1: TMenuItem;
    LogdeErro1: TMenuItem;
    Connection: TADOConnection;
    estarConexoIPPrimrio1: TMenuItem;
    qryMasteriIntervalo: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure estarConexo1Click(Sender: TObject);
    procedure testaConexao(cIPServidor:string;cNmDataBase:string);
    procedure efetivaConexao(cIPServidor:string;cIPSecundarioServidor:string;cNmDataBase:string);
    procedure estarConexoIPPrimrio1Click(Sender: TObject);
    procedure FiladeTransmisso1Click(Sender: TObject);
    procedure LogdeErro1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmServidorReplicacao: TfrmServidorReplicacao;

implementation

uses fServidorReplicacao_FilaTransmissao, fRegistrosRecebidos, fMenu,
    fServidorReplicacao_FilaTransmissao_Novo;

{$R *.dfm}

procedure TfrmServidorReplicacao.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  DBGridEh1.SetFocus ;

  ToolButton1.Click;
  
end;

procedure TfrmServidorReplicacao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdServidor.Value <= 0) then
  begin
      MensagemAlerta('Informe o c�digo do servidor.') ;
      abort ;
  end ;

  if (Trim(qryMastercNmServidor.Value) = '') then
  begin
      MensagemAlerta('Informe o nome do servidor.') ;
      abort ;
  end ;

  if (Trim(qryMastercFlgTipoServidor.Value) = '') then
  begin
      MensagemAlerta('Informe o tipo de servidor.') ;
      abort ;
  end ;

  if (Trim(qryMastercIPPrimario.Value) = '') then
  begin
      MensagemAlerta('Informe o IP prim�rio do servidor.') ;
      abort ;
  end ;

  if (Trim(qryMastercNmDatabase.Value) = '') then
  begin
      MensagemAlerta('Informe o database name.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmServidorReplicacao.estarConexo1Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
  begin
      qryMaster.Post ;
  end ;

  if (qryMasternCdServidor.Value <= 0) then
  begin
      MensagemAlerta('Nenhum Servidor Selecionado.') ;
      abort ;
  end ;

  testaConexao(qryMastercIPSecundario.Value, qryMastercNmDatabase.Value) ;

end;

procedure TfrmServidorReplicacao.testaConexao(cIPServidor,
  cNmDataBase: string);
var
    iCursor : integer ;
begin

  Connection.Close;
  Connection.ConnectionString := 'provider=MSDASQL.1;driver={SQL Server};server=' + cIPServidor + ';uid=ADMSoft;pwd=admsoft101580;database=' + cNmDataBase ;
  Connection.LoginPrompt := false ;

  try
      iCursor := Screen.Cursor ;
      Screen.Cursor := crSQLWait ;
      Connection.Open();

      ShowMessage('Conex�o realizada com sucesso!') ;
  except
      Screen.Cursor := iCursor ;
      MensagemErro('N�o foi poss�vel estabelecer uma conex�o com o servidor.' +#13#13+'IP : ' + cIPServidor + '  DataBase: ' + cNmDataBase) ;
      Raise ;
  end ;

  Screen.Cursor := iCursor ;

end;

procedure TfrmServidorReplicacao.estarConexoIPPrimrio1Click(
  Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
  begin
      qryMaster.Post ;
  end ;

  if (qryMasternCdServidor.Value <= 0) then
  begin
      MensagemAlerta('Nenhum Servidor Selecionado.') ;
      abort ;
  end ;

  testaConexao(qryMastercIPPrimario.Value, qryMastercNmDatabase.Value) ;

end;

procedure TfrmServidorReplicacao.FiladeTransmisso1Click(Sender: TObject);
var
  objForm : TfrmServidorReplicacao_FilaTransmissao ;
  objForm2 : TfrmServidorReplicacao_FilaTransmissao_novo ;
  verSync : String;
begin
  inherited;

  objForm := TfrmServidorReplicacao_FilaTransmissao.Create( Self ) ;
  objForm2 := TfrmServidorReplicacao_FilaTransmissao_novo.Create( Self );

  verSync := frmMenu.LeParametro('VERSYNCSERVER');
  {-- se o servidor selecionado n�o for o servidor conectado pelo ER2, efetua uma conex�o auxiliar --}
  if (qryMasternCdServidor.Value <> strToint(frmMenu.LeParametro('REPLCDSERVR'))) then
  begin

      efetivaConexao(qryMastercIPPrimario.Value, qryMastercIPSecundario.Value, qryMastercNmDatabase.Value) ;

      if (verSync = '1.0.0') then
      begin
          objForm.qryTempRegistro.Connection      := Connection ;
          objForm.qryDescartados.Connection       := Connection ;
          objForm.qryAux.Connection               := Connection ;
          objForm.qryPendentesTabela.Connection   := Connection ;
          objForm.qryPendentesServidor.Connection := Connection ;
      end else
      begin
          objForm2.qryTempRegistro.Connection      := Connection ;
          objForm2.qryDescartados.Connection       := Connection ;
          objForm2.qryAux.Connection               := Connection ;
          objForm2.qryPendentesTabela.Connection   := Connection ;
          objForm2.qryPendentesServidor.Connection := Connection ;
      end;

  end
  else
  begin

      if (verSync = '1.0.0') then
      begin
          objForm.qryTempRegistro.Connection      := frmMenu.Connection ;
          objForm.qryDescartados.Connection       := frmMenu.Connection ;
          objForm.qryAux.Connection               := frmMenu.Connection ;
          objForm.qryPendentesTabela.Connection   := frmMenu.Connection ;
          objForm.qryPendentesServidor.Connection := frmMenu.Connection ;
      end else
      begin
          objForm2.qryTempRegistro.Connection      := frmMenu.Connection ;
          objForm2.qryDescartados.Connection       := frmMenu.Connection ;
          objForm2.qryAux.Connection               := frmMenu.Connection ;
          objForm2.qryPendentesTabela.Connection   := frmMenu.Connection ;
          objForm2.qryPendentesServidor.Connection := frmMenu.Connection ;
      end;
  end ;

  if (verSync = '1.0.0') then
      showForm( objForm , TRUE )
  else showForm( objForm2 , TRUE );

end;

procedure TfrmServidorReplicacao.efetivaConexao(cIPServidor,
  cIPSecundarioServidor, cNmDataBase: string);
var
    iCursor : integer ;
begin

  Connection.Close;
  Connection.ConnectionString := 'provider=MSDASQL.1;driver={SQL Server};server=' + cIPServidor + ';uid=ADMSoft;pwd=admsoft101580;database=' + cNmDataBase ;
  Connection.LoginPrompt := false ;

  try
      iCursor := Screen.Cursor ;
      Screen.Cursor := crSQLWait ;
      Connection.Open();

  except
      if (cIPSecundarioServidor <> '') then
      begin
          try
              Connection.ConnectionString := 'provider=MSDASQL.1;driver={SQL Server};server=' + cIPSecundarioServidor + ';uid=ADMSoft;pwd=admsoft101580;database=' + cNmDataBase ;
              Connection.Open();

          except
              Screen.Cursor := iCursor ;
              MensagemErro('N�o foi poss�vel estabelecer uma conex�o com o servidor.') ;
              Raise ;
          end ;
      end
      else
      begin
          Screen.Cursor := iCursor ;
          MensagemErro('N�o foi poss�vel estabelecer uma conex�o com o servidor.') ;
          Raise ;
      end ;
  end ;

  Screen.Cursor := iCursor ;


end;

procedure TfrmServidorReplicacao.LogdeErro1Click(Sender: TObject);
var
  objForm : TfrmRegistrosRecebidos ;
begin
  inherited;

  efetivaConexao(qryMastercIPPrimario.Value, qryMastercIPSecundario.Value, qryMastercNmDatabase.Value) ;

  objForm := TfrmRegistrosRecebidos.Create( Self ) ;

  objForm.qryRecebidosHoje.Connection   := Connection ;
  objForm.qryRecebidosQuinze.Connection := Connection ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmServidorReplicacao.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryMaster.Close;
  qryMaster.Open ;

  if (frmMenu.LeParametro('REPLPAUSADA') = 'S') then
  begin
      MensagemErro('A replica��o entre os servidores est� interrompida.') ;
  end ;

end;

initialization
    RegisterClass(TfrmServidorReplicacao) ;

end.
