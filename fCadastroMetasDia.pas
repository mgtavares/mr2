unit fCadastroMetasDia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, DB, ADODB, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBCtrls, Mask, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, DBGridEhGrouping,
  ToolCtrlsEh, GridsEh, DBGridEh, cxLookAndFeelPainters, cxButtons;

type
  TfrmCadastroMetasDia = class(TfrmProcesso_Padrao)
    qryLoja: TADOQuery;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryMetas: TADOQuery;
    dsMetas: TDataSource;
    qryMetasdata: TDateTimeField;
    qryMetasloja: TIntegerField;
    qryMetasvisitantes: TIntegerField;
    qryMetastickets: TIntegerField;
    qryMetasreceita: TFloatField;
    qryMetasticketMedio: TFloatField;
    qryMetasitensTicket: TFloatField;
    qryMetasconversao: TFloatField;
    qryMetasassociacao: TFloatField;
    DBGridEh1: TDBGridEh;
    cxButton1: TcxButton;
    SaveDialog1: TSaveDialog;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1Change(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
    nCdLoja : Integer;
    dDTini, dDtFim : String;
  public
    { Public declarations }
  end;

var
  frmCadastroMetasDia: TfrmCadastroMetasDia;

implementation

{$R *.dfm}
uses
  fMenu, fLookup_Padrao;
  
procedure TfrmCadastroMetasDia.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            Edit1.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmCadastroMetasDia.Edit1Change(Sender: TObject);
begin
  inherited;
    try
      if (trim(Edit1.Text) <> '') then
        nCdLoja := StrToInt(Edit1.Text)
      else
        nCdLoja := 0
    except
      Edit1.Text := '';
      nCdLoja := 0;
    end;

    qryLoja.Close;
    qryLoja.Parameters.ParamByName('nCdLoja').Value := nCdLoja;
    qryLoja.Open;

end;

procedure TfrmCadastroMetasDia.ToolButton1Click(Sender: TObject);
begin
  inherited;
    if (MaskEdit1.Text <> '  /  /    ') then dDtIni := MaskEdit1.Text else dDtIni := '01/01/1900';
    if (MaskEdit2.Text <> '  /  /    ') then dDtFim := MaskEdit2.Text else dDtFim := '01/01/1900';
    if (trim(Edit1.Text) = '') then nCdLoja := 0;
    qryMetas.Close;
    qryMetas.Parameters.ParamByName('dDtIni').Value := dDtIni;
    qryMetas.Parameters.ParamByName('dDtFim').Value := dDtFim;
    qryMetas.Parameters.ParamByName('nCdLoja').Value := nCdLoja;
    qryMetas.Open;
end;

procedure TfrmCadastroMetasDia.cxButton1Click(Sender: TObject);
var
  F : TStringList;
  l : String;
begin
  inherited;
  try
  if SaveDialog1.Execute then
  begin
  F := TStringList.Create;
  F.Add('Data;Loja;Visitantes;Tickets;Receita de Venda;Ticket M�dio;Itens p/ Ticket;% Convers�o;% Associa��o');
  if (qryMetas.Active) then
  begin
  qryMetas.First;
  while not (qryMetas.Eof) do
  begin
    l := qryMetasdata.AsString         + ';' +
         qryMetasloja.AsString         + ';' +
         qryMetasvisitantes.AsString   + ';' +
         qryMetastickets.AsString      + ';' +
         qryMetasreceita.AsString      + ';' +
         qryMetasticketMedio.AsString  + ';' +
         qryMetasitensTicket.AsString  + ';' +
         qryMetasconversao.AsString    + ';' +
         qryMetasassociacao.AsString   + ';';
    F.Add(l);
    qryMetas.Next;
  end;
  F.SaveToFile(SaveDialog1.FileName);
  F.Free;
  end;
  frmMenu.showMessage('O arquivo foi gerado com sucesso.');
  end;
  except
    frmMenu.showMessage('Erro na gera��o do arquivo. Tente novamente.');
    F.Free;
  end;
end;

initialization
  RegisterClass(TfrmCadastroMetasDia);

end.
