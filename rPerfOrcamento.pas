unit rPerfOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptPerfOrcamento = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit4: TDBEdit;
    DataSource6: TDataSource;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPerfOrcamento: TrptPerfOrcamento;

implementation

uses fMenu, fLookup_Padrao, rPerfOrcamento_view;

{$R *.dfm}

procedure TrptPerfOrcamento.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;


procedure TrptPerfOrcamento.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptPerfOrcamento.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptPerfOrcamento.ToolButton1Click(Sender: TObject);
begin


  rptPerfOrcamento_view.nTotalAprovado  := 0 ;
  rptPerfOrcamento_view.nTotalReprovado := 0 ;
  rptPerfOrcamento_view.nTotalPendente  := 0 ;

  rptPerfOrcamento_view.iQtdeAprovado  := 0 ;
  rptPerfOrcamento_view.iQtdeReprovado := 0 ;
  rptPerfOrcamento_view.iQtdePendente  := 0 ;

  rptPerfOrcamento_view.usp_Relatorio.Close ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@nCdUnidadeNegocio').Value := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
  rptPerfOrcamento_view.usp_Relatorio.Open  ;

  rptPerfOrcamento_view.QRGroup1.Enabled := True ;
  rptPerfOrcamento_view.QRBand3.Enabled  := True ;
  rptPerfOrcamento_view.QRBand2.Enabled  := True ;

  rptPerfOrcamento_view.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  rptPerfOrcamento_view.lblFiltro1.Caption := 'Unidade Neg: '        + Trim(MaskEdit5.Text) + ' ' + DbEdit4.Text ;
  rptPerfOrcamento_view.lblFiltro2.Caption := 'Terceiro   : '        + String(MaskEdit6.Text) + ' ' + DbEdit10.Text ;
  rptPerfOrcamento_view.lblFiltro3.Caption := 'Grupo Economico: ' + String(MaskEdit4.Text) + ' ' + DbEdit1.Text ;
  rptPerfOrcamento_view.lblFiltro4.Caption := 'Per�odo        : ' + String(MaskEdit1.Text) + ' ' + String(MaskEdit2.Text) ;

  rptPerfOrcamento_view.QuickRep1.PreviewModal;

end;


procedure TrptPerfOrcamento.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptPerfOrcamento.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptPerfOrcamento.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptPerfOrcamento.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;
end;

procedure TrptPerfOrcamento.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;
  
end;

procedure TrptPerfOrcamento.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(14);

        If (nPK > 0) then
        begin
            MaskEdit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptPerfOrcamento) ;

end.
