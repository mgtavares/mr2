unit fPMP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, GridsEh, DBGridEh, cxPC,
  cxControls, ER2Lookup, cxLookAndFeelPainters, cxButtons, ER2Excel,
  ToolCtrlsEh;

type
  TfrmPMP = class(TfrmCadastro_Padrao)
    qryMasternCdPMP: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasterdDtInicial: TDateTimeField;
    qryMasterdDtFinal: TDateTimeField;
    qryMastercNmPMP: TStringField;
    qryMastercDocumento: TStringField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryMasternCdUsuarioUltAlt: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    qryProduto: TADOQuery;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    DBEdit10: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DataSource1: TDataSource;
    dsPlanDiario: TDataSource;
    qryPlanDiario: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    ToolButton15: TToolButton;
    qryInsereItemPMP: TADOQuery;
    ER2Excel1: TER2Excel;
    qryExcluiItem: TADOQuery;
    btExportarPlan: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure qryPlanDiarioBeforeDelete(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
    procedure btExportarPlanClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
    nColumn : integer;
    iCursor : Integer;
    ProdutoImportado : boolean;
    procedure GeraGrid_ItemPMP(DataInicial,DataFinal : String ; nCdPMP : Integer; Metodo : String);
  public
    { Public declarations }
  end;

var
  frmPMP: TfrmPMP;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmPMP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PMP' ;
  nCdTabelaSistema  := 316 ;
  nCdConsultaPadrao := 745 ;
  bCodigoAutomatico := True ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmPMP.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if ((DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProduto') and (qryPlanDiario.State <> dsBrowse)) then
  begin

      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nPK').Value := qryPlanDiario.FieldByName('nCdProduto').Value;
      qryProduto.Open;

      qryPlanDiario.FieldByName('cDescricao').Value      := qryProdutocNmProduto.Value;
      qryPlanDiario.FieldByName('cUM').Value             := qryProdutocUnidadeMedida.Value;
      qryPlanDiario.FieldByName('nQtdeLoteMinimo').Value := qryProdutonQtdeMinimaCompra.Value;

  end;

end;

procedure TfrmPMP.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin


        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProduto')  then
        begin

            if (qryPlanDiario.State = dsBrowse) then
                qryPlanDiario.Edit ;

            if (qryPlanDiario.State = dsInsert) or (qryPlanDiario.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(176); // verificar consulta correta

                If (nPK > 0) then
                begin
                    qryPlanDiario.FieldByName('nCdProduto').Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end;

end;

procedure TfrmPMP.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if ((qryMaster.Active) and (qryMaster.State = dsInsert)) then
  begin
      if (MessageDlg('Ap�s salvar o per�odo n�o poder� ser alterado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      begin
          DBEdit4.SetFocus;
          abort;
      end;

      qryMaster.Post;
  end ;
end;

procedure TfrmPMP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit4.Text) = '/  /') then
  begin
      MensagemAlerta('Insira a data inicial do Per�odo de Produ��o.');
      DBEdit4.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit5.Text) = '/  /') then
  begin
      MensagemAlerta('Insira a data final do Per�odo de Produ��o.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (frmMenu.ConvData(DBEdit4.Text) > frmMenu.ConvData(DBEdit5.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final');
      DBEdit4.SetFocus;
      Abort;
  end;

  qryMasterdDtUltAlt.Value := Now();
  qryMasternCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioUltAlt.AsString);

  inherited;
end;

procedure TfrmPMP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);

  qryUsuario.Close;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioUltAlt.AsString);

  if (qryMaster.State = dsBrowse) then
      GeraGrid_ItemPMP (qryMasterdDtInicial.AsString,qryMasterdDtFinal.AsString,qryMasternCdPMP.Value,'INSERT');

  if (qryMasterdDtUltAlt.AsString <> '') then
  begin
      DBEdit4.ReadOnly   := True;
      DBEdit4.Color      := $00E9E4E4;
      DBEdit4.Font.Color := clBlack ;
      DBEdit4.TabStop    := False ;
      DBEdit4.Update;

      DBEdit5.ReadOnly   := True;
      DBEdit5.Color      := $00E9E4E4;
      DBEdit5.Font.Color := clBlack ;
      DBEdit5.TabStop    := False ;
      DBEdit5.Update;
  end
  else
  begin

      DBEdit4.ReadOnly   := False;
      DBEdit4.Color      := clWhite;
      DBEdit4.TabStop    := True ;
      DBEdit4.Update;

      DBEdit5.ReadOnly   := False;
      DBEdit5.Color      := clWhite;
      DBEdit5.TabStop    := True ;
      DBEdit5.Update;

  end;

end;

procedure TfrmPMP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  GeraGrid_ItemPMP (DBEdit4.Text,DBEdit5.Text,qryMasternCdPMP.Value,'INSERT') ;

end;

procedure TfrmPMP.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  PosicionaQuery(qryEmpresa,DBEdit3.Text);

  DBEdit4.SetFocus;
end;

procedure TfrmPMP.qryMasterBeforeClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuario.Close;
  qryEmpresa.Close;
  qryPlanDiario.Close;
  DBGridEH1.Columns.Clear;
end;

procedure TfrmPMP.GeraGrid_ItemPMP(DataInicial, DataFinal: String; nCdPMP : Integer ; Metodo : String);
var
  i             : integer;
  cHead,cSemana : String;
begin
  inherited;

  cmdPreparaTemp.Execute;

  {-- aqui gera as colunas que ser�o apresentadas no grid --}

  try
      qryPlanDiario.Close;
      qryPlanDiario.Parameters.ParamByName('dDtInicial').Value := DataInicial;
      qryPlanDiario.Parameters.ParamByName('dDtFinal').Value   := DataFinal;
      qryPlanDiario.Parameters.ParamByName('nCdPMP').Value     := nCdPMP;
      qryPlanDiario.Parameters.ParamByName('cMetodo').Value    := UpperCase(Metodo);
      qryPlanDiario.Open;
  except
      MensagemErro('Erro no Processamento');
      raise;
  end;

  qryPlanDiario.DisableControls;
  qryPlanDiario.FieldList.Update;

  {-- adiciona as colunas no grid --}
  DBGridEh1.Columns.AddAllColumns(true);

  {-- tratamento das colunas --}
  DBGridEh1.Columns[0].Visible          := False;

  DBGridEh1.Columns[1].Title.Caption    := 'Produto|C�d.';
  DBGridEh1.Columns[1].Width            := 74;
  DBGridEh1.FrozenCols                  := 5;

  DBGridEh1.Columns[2].Title.Caption    := 'Produto|Descri��o';
  DBGridEh1.Columns[2].Width            := 338;
  DBGridEh1.Columns[2].ReadOnly         := True;
  DBGridEh1.Columns[2].Title.Font.Color := clRed;
  DBGridEh1.Columns[2].Font.Color       := clTeal;

  DBGridEh1.Columns[3].Title.Caption    := 'Produto|U.M';
  DBGridEh1.Columns[3].Width            := 48;
  DBGridEh1.Columns[3].ReadOnly         := True;
  DBGridEh1.Columns[3].Title.Font.Color := clRed;
  DBGridEh1.Columns[3].Font.Color       := clTeal;

  DBGridEh1.Columns[4].Title.Caption    := 'Produto|Lote M�nimo';
  DBGridEh1.Columns[4].Width            := 73;
  DBGridEh1.Columns[4].ReadOnly         := True;
  DBGridEh1.Columns[4].Title.Font.Color := clRed;
  DBGridEh1.Columns[4].Font.Color       := clTeal;

  nColumn := DBGridEh1.Columns.Count - 1;

  for i := 5 to nColumn do
  begin

      cHead := DBGridEh1.Columns[i].Title.Caption;
      cHead := Copy(cHead,16,Length(cHead)-15);

      cHead   := StringReplace(cHead,'_','/',[rfReplaceAll]);
      cSemana := Copy(FormatDateTime('DDDD',StrToDate(cHead)),1,3);
      cHead   := cSemana + ' ' + FormatDateTime('dd/mm/yyyy',StrToDate(cHead));

      DBGridEh1.Columns[i].Title.Caption := cHead;
      DBGridEh1.Columns[i].Width         := 73;

      if ((cSemana = 's�b') or (cSemana = 'dom')) then
          DBGridEh1.Columns[i].Color := clSilver;


  end;

  qryPlanDiario.EnableControls;

  DBGridEh1.SetFocus;
end;

procedure TfrmPMP.qryPlanDiarioBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  qryExcluiItem.Close;
  qryExcluiItem.Parameters.ParamByName('nCdPMP').Value     := qryMasternCdPMP.Value;
  qryExcluiItem.Parameters.ParamByName('nCdProduto').Value := qryPlanDiario.FieldValues['nCdProduto'];
  qryExcluiItem.ExecSQL;
  
end;

procedure TfrmPMP.btSalvarClick(Sender: TObject);
begin
  cxButton3.Click;

  inherited;

end;

procedure TfrmPMP.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  iCursor := Screen.Cursor ;
  Screen.Cursor := crSQLWait ;
  frmMenu.StatusBar1.Panels.Items[6].Text := 'Exportando planilha...';
end;

procedure TfrmPMP.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  frmMenu.StatusBar1.Panels.Items[6].Text := '';
  Screen.Cursor := iCursor ;

  ShowMessage('PMP Exportado com sucesso.');
end;

procedure TfrmPMP.btExportarPlanClick(Sender: TObject);
var
  i, n : integer;
  head : string;
begin
  inherited;

  if not (qryMaster.Active) then
      abort;

  n := 10;

  {-- mescla as celulas --}
  ER2Excel1.Celula['B2'].Mesclar('C2');
  ER2Excel1.Celula['B3'].Mesclar('C3');
  ER2Excel1.Celula['B4'].Mesclar('C4');
  ER2Excel1.Celula['B5'].Mesclar('C5');
  ER2Excel1.Celula['B6'].Mesclar('C6');
  ER2Excel1.Celula['D5'].Mesclar('L5');
  ER2Excel1.Celula['D6'].Mesclar('L6');

  {-- defino os textos do cabe�alho da planilha --}
  ER2Excel1.Celula['B2'].Text := 'C�digo:';
  ER2Excel1.Celula['B3'].Text := 'Empresa:';
  ER2Excel1.Celula['B4'].Text := 'Per�odo de Produ��o:';
  ER2Excel1.Celula['B5'].Text := 'Descri��o PMP:';
  ER2Excel1.Celula['B6'].Text := 'Documento:';

  ER2Excel1.Celula['B9'].Text := 'C�d.';
  ER2Excel1.Celula['C9'].Text := 'Descri��o';
  ER2Excel1.Celula['K9'].Text := 'U.M';
  ER2Excel1.Celula['L9'].Text := 'Lote M�nimo';


  ER2Excel1.Celula['D2'].Text := qryMasternCdPMP.Value;
  ER2Excel1.Celula['D3'].Text := qryMasternCdEmpresa.AsString + ' - ' + DBEdit10.Text; 
  ER2Excel1.Celula['D4'].Text := qryMasterdDtInicial.AsString + ' � ' + qryMasterdDtFinal.AsString;
  ER2Excel1.Celula['D5'].Text := qryMastercNmPMP.Value;
  ER2Excel1.Celula['D6'].Text := qryMastercDocumento.Value;


  {-- formata as celulas --}
  ER2Excel1.Celula['B2'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B3'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B4'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B5'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B6'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B9'].HorizontalAlign := haRight;
  ER2Excel1.Celula['K9'].HorizontalAlign := haCenter;
  
  ER2Excel1.Celula['B2'].Negrito;
  ER2Excel1.Celula['B3'].Negrito;
  ER2Excel1.Celula['B4'].Negrito;
  ER2Excel1.Celula['B5'].Negrito;
  ER2Excel1.Celula['B6'].Negrito;
  ER2Excel1.Celula['B9'].Negrito;
  ER2Excel1.Celula['C9'].Negrito;
  ER2Excel1.Celula['K9'].Negrito;
  ER2Excel1.Celula['L9'].Negrito;

  ER2Excel1.Celula['B9'].Border       := [EdgeBottom];
  ER2Excel1.Celula['B9'].BorderWeight := xlMedium;
  ER2Excel1.Celula['B9'].Range('L9');

  {-- aqui insere os registros --}

  qryPlanDiario.First;

  while not qryPlanDiario.Eof do
  begin

      for i := 1 to nColumn do
      begin

          if (i > 4) then
          begin
              Head := Copy(DBGridEh1.Columns[i].Title.Caption,1,9);
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Text            := Head;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Border          := [EdgeBottom];
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].BorderWeight    := xlMedium;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '1'].Width           := 21;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Negrito;

              if ((Copy(Head,1,3) = 's�b') or (Copy(Head,1,3) = 'dom')) then
                  ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Background := clSilver;

          end;

          if (i = 1) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(2) + intToStr(n)].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(2) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
          end;

          if (i = 2) then
          begin
              ER2Excel1.Celula['C' + intToStr(n)].Mesclar('J' + intToStr(n));
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(3) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
          end;

          if (i > 3) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].HorizontalAlign := haRight;
          end;

          if (i = 3) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].HorizontalAlign := haCenter;
          end;

      end;

      n := n + 1;
      qryPlanDiario.Next;

  end;

  ER2Excel1.Celula['B1'].Width := 10;
  ER2Excel1.Celula['K1'].Width := 5;
  ER2Excel1.Celula['A1'].Width := 2;
  ER2Excel1.Celula['L1'].Width := 12;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

procedure TfrmPMP.cxButton2Click(Sender: TObject);
var
  nPK : integer;
begin
  inherited;

  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta(745);

      If (nPK > 0) then
      begin

          if (MessageDlg('Ao realizar a importa��o os produtos digitados ser�o removidos. Confirma a importa��o ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
              abort;

          GeraGrid_ItemPMP (qryMasterdDtInicial.AsString,qryMasterdDtFinal.AsString,nPK,'IMPORT');

          ProdutoImportado := True;

          ShowMessage('Produtos Importados com sucesso');

      end ;

  end ;

end;

procedure TfrmPMP.cxButton3Click(Sender: TObject);
var
  i : integer;
  cData : String;
begin
  inherited;

  if (MessageDlg('Confirma o Processamento dos Itens ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  qryPlanDiario.First;

  frmMenu.Connection.BeginTrans;

  if(ProdutoImportado) then
  begin
      qryExcluiItem.Close;
      qryExcluiItem.Parameters.ParamByName('nCdPMP').Value     := qryMasternCdPMP.Value;
      qryExcluiItem.Parameters.ParamByName('nCdProduto').Value := 0;
      qryExcluiItem.ExecSQL;
  end;

  while not qryPlanDiario.Eof do
  begin

      for i := 5 to nColumn do
      begin

          cData := Copy(DBGridEh1.Columns[i].Title.Caption,5,10);

          try
              qryInsereItemPMP.Close;
              qryInsereItemPMP.Parameters.ParamByName('nCdPMP').Value         := qryMasternCdPMP.Value;
              qryInsereItemPMP.Parameters.ParamByName('nCdProduto').Value     := qryPlanDiario.FieldValues['nCdProduto'];
              qryInsereItemPMP.Parameters.ParamByName('dDtPMP').Value         := cData;
              qryInsereItemPMP.Parameters.ParamByName('nQtdePlanejada').Value := DBGridEH1.Columns[i].Field.Value;
              qryInsereItemPMP.ExecSQL;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.');
              raise
          end;
      end;

      qryPlanDiario.Next;
  end;

  frmMenu.Connection.CommitTrans;

  ProdutoImportado := False;
  ShowMessage('Itens Processados com Sucesso.');

end;

initialization
    RegisterClass(TfrmPMP);

end.
