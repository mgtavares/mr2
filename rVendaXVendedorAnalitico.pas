unit rVendaXVendedorAnalitico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, Mask, StdCtrls, DBCtrls;

type
  TrptVendaXVendedorAnalitico = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    edtDtInicial: TMaskEdit;
    edtDtFinal: TMaskEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    qryVendedor: TADOQuery;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    dsVendedor: TDataSource;
    edtLoja: TMaskEdit;
    edtVendedor: TMaskEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtVendedorExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaXVendedorAnalitico: TrptVendaXVendedorAnalitico;

implementation

uses rVendaXVendedorAnalitico_view, fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TrptVendaXVendedorAnalitico.ToolButton1Click(Sender: TObject);
var
  cFiltro : string;
  objRel  : TrptVendaXVendedorAnalitico_view;
begin
  inherited;

  if (Trim(edtDtInicial.Text) = '/  /') then
       edtDtInicial.Text := DateToStr(Now());

  if (Trim(edtDtFinal.Text) = '/  /') then
       edtDtFinal.Text := DateToStr(Now());


  objRel := TrptVendaXVendedorAnalitico_view.Create(nil);;

  try
      try
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Close;

          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@nCdVendedor').Value := frmMenu.ConvInteiro(edtVendedor.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtInicial').Value  := frmMenu.ConvData(edtDtInicial.Text);
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.Parameters.ParamByName('@dDtFinal').Value    := frmMenu.ConvData(edtDtFinal.Text);

          objRel.cmd_PreparaTemp.Execute;
          objRel.SPREL_VENDA_VENDEDOR_ANALITICO.ExecProc;

          objRel.qryDescricaoPedido.Close;
          objRel.qryDescricaoPedido.Open;

            {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit1.Text <> '') then
              cFiltro := cFiltro + 'Loja: ' + DBEdit1.Text + '/ ';

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + 'Vendedor: ' + DBEdit2.Text + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + edtDtInicial.Text + ' � ' + edtDtFinal.Text;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;

end;

procedure TrptVendaXVendedorAnalitico.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text);
end;

procedure TrptVendaXVendedorAnalitico.edtVendedorExit(Sender: TObject);
begin
  inherited;
  qryVendedor.Close;
  qryVendedor.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(edtLoja.Text);
  PosicionaQuery(qryVendedor,edtVendedor.Text);
end;

procedure TrptVendaXVendedorAnalitico.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, edtLoja.Text);
            end ;

    end ;

  end ;

end;

procedure TrptVendaXVendedorAnalitico.edtVendedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        If (Trim(DBEdit1.Text)='') then
        begin
        
            MensagemAlerta('Para selecionar um vendedor, selecione primeiro uma loja');
            Abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(703,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = Usuario.nCdUsuario AND UL.nCdLoja = ' + edtLoja.Text + ')');
        end;

        If (nPK > 0) then
        begin
            edtVendedor.Text := IntToStr(nPK) ;
            qryVendedor.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(edtLoja.Text);
            PosicionaQuery(qryVendedor, edtVendedor.Text) ;
        end ;

    end ;
    end;
end;

initialization
    RegisterClass(TrptVendaXVendedorAnalitico);

end.
