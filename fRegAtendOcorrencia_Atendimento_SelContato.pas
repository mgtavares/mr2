unit fRegAtendOcorrencia_Atendimento_SelContato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmRegAtendOcorrencia_Atendimento_SelContato = class(TfrmProcesso_Padrao)
    qryListaContato: TADOQuery;
    dsListaContato: TDataSource;
    qryListaContatonCdTerceiro: TIntegerField;
    qryListaContatocNmTipoTelefone: TStringField;
    qryListaContatocTelefone: TStringField;
    qryListaContatocRamal: TStringField;
    qryListaContatocContato: TStringField;
    qryListaContatocDepartamento: TStringField;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegAtendOcorrencia_Atendimento_SelContato: TfrmRegAtendOcorrencia_Atendimento_SelContato;

implementation

{$R *.dfm}

procedure TfrmRegAtendOcorrencia_Atendimento_SelContato.FormShow(
  Sender: TObject);
begin
  inherited;

  DBGridEh1.SetFocus;
  
end;

procedure TfrmRegAtendOcorrencia_Atendimento_SelContato.DBGridEh1KeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
      close ;
end;

procedure TfrmRegAtendOcorrencia_Atendimento_SelContato.ToolButton2Click(
  Sender: TObject);
begin
  qryListaContato.Close;

  inherited;

end;

procedure TfrmRegAtendOcorrencia_Atendimento_SelContato.DBGridEh1DblClick(
  Sender: TObject);
begin
  inherited;

  close ;

end;

end.
