unit fLiquidaParcela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, DB, ADODB, cxLookAndFeelPainters, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit;

type
  TfrmLiquidaParcela = class(TfrmProcesso_Padrao)
    qryLiquidaParcela: TADOQuery;
    DataSource1: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    qryMovtoParcela: TADOQuery;
    DataSource2: TDataSource;
    qryMovtoParcelanCdMTitulo: TAutoIncField;
    qryMovtoParcelanCdTitulo: TIntegerField;
    qryMovtoParcelanCdContaBancaria: TIntegerField;
    qryMovtoParcelanCdOperacao: TIntegerField;
    qryMovtoParcelanCdFormaPagto: TIntegerField;
    qryMovtoParcelaiNrCheque: TIntegerField;
    qryMovtoParcelanValMov: TBCDField;
    qryMovtoParceladDtMov: TDateTimeField;
    qryMovtoParceladDtCad: TDateTimeField;
    qryMovtoParceladDtCancel: TDateTimeField;
    qryMovtoParcelacNrDoc: TStringField;
    qryMovtoParceladDtContab: TDateTimeField;
    qryMovtoParcelanCdUsuarioCad: TIntegerField;
    qryMovtoParcelanCdUsuarioCancel: TIntegerField;
    qryMovtoParcelacObsMov: TMemoField;
    qryMovtoParcelanCdCC: TIntegerField;
    qryMovtoParcelacFlgMovAutomatico: TIntegerField;
    qryMovtoParceladDtBomPara: TDateTimeField;
    qryMovtoParcelanCdLanctoFin: TIntegerField;
    qryMovtoParcelacFlgIntegrado: TIntegerField;
    qryMovtoParcelanCdProvisaoTit: TIntegerField;
    qryMovtoParcelacFlgMovDA: TIntegerField;
    qryMovtoParceladDtIntegracao: TDateTimeField;
    usp_ProximoID: TADOStoredProc;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    Label14: TLabel;
    DataSource3: TDataSource;
    DBEdit15: TDBEdit;
    edtFormaPagto: TMaskEdit;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdEmpresa: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancarianCdCC: TIntegerField;
    qryContaBancariacFlgFluxo: TIntegerField;
    qryContaBancariaiUltimoCheque: TIntegerField;
    qryContaBancariaiUltimoBordero: TIntegerField;
    qryContaBancariaDadosConta: TStringField;
    qryContaBancarianValLimiteCredito: TBCDField;
    qryContaBancariacFlgDeposito: TIntegerField;
    qryContaBancariacFlgEmiteCheque: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    edtContaBancaria: TMaskEdit;
    Label15: TLabel;
    DBEdit14: TDBEdit;
    DataSource4: TDataSource;
    Label16: TLabel;
    edtNrDoc: TEdit;
    Label17: TLabel;
    qryContaBancariacNmTitular: TStringField;
    Label18: TLabel;
    qryLiquidaParcelanCdTitulo: TIntegerField;
    qryLiquidaParcelanCdEspTit: TIntegerField;
    qryLiquidaParcelacNmEspTit: TStringField;
    qryLiquidaParcelanCdSP: TIntegerField;
    qryLiquidaParcelacNrTit: TStringField;
    qryLiquidaParcelaiParcela: TIntegerField;
    qryLiquidaParcelanCdTerceiro: TIntegerField;
    qryLiquidaParcelacNmTerceiro: TStringField;
    qryLiquidaParcelacCNPJCPF: TStringField;
    qryLiquidaParcelacRG: TStringField;
    qryLiquidaParcelanCdMoeda: TIntegerField;
    qryLiquidaParcelacSenso: TStringField;
    qryLiquidaParceladDtEmissao: TDateTimeField;
    qryLiquidaParceladDtLiq: TDateTimeField;
    qryLiquidaParceladDtVenc: TDateTimeField;
    qryLiquidaParcelanValTit: TBCDField;
    qryLiquidaParcelanSaldoTit: TBCDField;
    qryLiquidaParcelanValIndiceCorrecao: TBCDField;
    qryLiquidaParcelanValCorrecao: TBCDField;
    qryLiquidaParcelanValJuro: TBCDField;
    qryLiquidaParcelanValMulta: TBCDField;
    qryLiquidaParcelanvalDesconto: TBCDField;
    qryLiquidaParcelanValAbatimento: TBCDField;
    qryLiquidaParceladDtCancel: TDateTimeField;
    qryLiquidaParcelanValLiq: TBCDField;
    qryLiquidaParcelaiParcelas: TIntegerField;
    qryLiquidaParcelacNrUnidade: TStringField;
    qryLiquidaParceladDtContrato: TDateTimeField;
    qryLiquidaParcelacNmBlocoEmpImobiliario: TStringField;
    qryLiquidaParcelacNmEmpImobiliario: TStringField;
    qryLiquidaParcelacEndereco: TStringField;
    qryLiquidaParcelacNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryLiquidaParcelanValRecebido: TCurrencyField;
    qryLiquidaParcelanCdIndiceReajuste: TIntegerField;
    Label20: TLabel;
    DBEdit18: TDBEdit;
    qryCotacao: TADOQuery;
    DataSource5: TDataSource;
    qryLiquidaParcelacNmIndiceReajuste: TStringField;
    DBEdit19: TDBEdit;
    qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField;
    qryCotacaonCdIndiceReajuste: TIntegerField;
    qryCotacaoiAnoReferencia: TIntegerField;
    qryCotacaoiMesReferencia: TIntegerField;
    qryCotacaonIndice: TBCDField;
    edtDtLiq: TMaskEdit;
    qryLiquidaParcelacDtCotacao: TStringField;
    edtDiasAtraso: TMaskEdit;
    Label19: TLabel;
    edtValCorrecao: TcxCurrencyEdit;
    edtValJuro: TcxCurrencyEdit;
    edtvalMulta: TcxCurrencyEdit;
    edtValAbatimento: TcxCurrencyEdit;
    edtValDesconto: TcxCurrencyEdit;
    edtValReceber: TcxCurrencyEdit;
    edtNrCheque: TMaskEdit;
    usp_atualizaBaixaEmpImobiliario: TADOStoredProc;
    qryUltimaParcelaPaga: TADOQuery;
    qryUltimaParcelaPaganCdTitulo: TIntegerField;
    qryUltimaParcelaPagaiparcela: TIntegerField;
    qryUltimaParcelaPaganValIndiceCorrecao: TBCDField;
    qryUltimaParcelaPaganValCorrecao: TBCDField;
    qryUltimaParcelaPaganCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryUltimaParcelaPagacNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryUltimaParcelaPaganSaldoTit: TBCDField;
    qryUltimaParcelaPaganValTit: TBCDField;
    DataSource6: TDataSource;
    edtValBaseCorrecao: TcxCurrencyEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtFormaPagtoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFormaPagtoExit(Sender: TObject);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtDtLiqExit(Sender: TObject);
    procedure atualizaTotal();
    procedure edtValCorrecaoExit(Sender: TObject);
    procedure imprimeRecibo(nCdTitulo,nCdTerceiro,nCdEspTit:Integer);
    procedure edtValAbatimentoExit(Sender: TObject);
    procedure edtValDescontoExit(Sender: TObject);
    procedure edtNrChequeExit(Sender: TObject);
    procedure edtNrDocExit(Sender: TObject);
    procedure edtDtLiqEnter(Sender: TObject);
    procedure edtValBaseCorrecaoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    valorReceb     : Double;
    valorCorrigido : Double ;
  end;

var
  frmLiquidaParcela: TfrmLiquidaParcela;
  nCdFormaPagtoCheque: integer ;
  nCdNrCheque : integer;

implementation

uses fMenu, fBaixaParcela, fLookup_Padrao, rReciboView, fAtualizacaoIndice;

{$R *.dfm}
procedure TfrmLiquidaParcela.ToolButton1Click(Sender: TObject);
begin
  inherited;

  try
     StrToDate(edtDtLiq.Text) ;
  except
     MensagemErro('Data Liquida��o inv�lida. Utilize o formado dia/mes/ano.' +#13#13 + 'Exemplo: ' + DateToStr(Date)) ;
     edtDtLiq.SetFocus ;
     exit ;
  end ;

  if (Trim(edtFormaPagto.Text) = '') then
  begin
      MensagemAlerta('Informe a Forma de Pagamento.') ;
      edtFormaPagto.SetFocus ;
      exit ;
  end ;

  if (Trim(edtContaBancaria.Text) = '') then
  begin
      MensagemAlerta('Informe a Conta Bancaria.') ;
      edtContaBancaria.SetFocus ;
      exit ;
  end ;

  if (edtValCorrecao.Value = 0) then
  begin
      MensagemAlerta('Parcela n�o foi atualizada pelo �ndice.') ;
      edtDtLiq.SetFocus ;
      exit ;
  end ;

  if (MessageDlg('Confirma a baixa da parcela?' + #13#13 ,mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  frmMenu.Connection.BeginTrans;
  try

      usp_atualizaBaixaEmpImobiliario.Close;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdTitulo').Value             := qryLiquidaParcelanCdTitulo.Value;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValCorrecao').Value          := edtValCorrecao.value;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValJuro').Value              := edtValJuro.value;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValMulta').Value             := edtValMulta.value;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValDesconto').Value          := edtValDesconto.value;

      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValAbatimento').Value        := edtValAbatimento.value;
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValCorrecaoMonetaria').Value := (edtValCorrecao.value - qryLiquidaParcelanSaldoTit.Value);

      // usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nValIndiceCorrecao').Value    := edtValIndiceCorrecao.value;

      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoJuro').Value              := strToInt(frmMenu.LeParametro('OPERBXPARCJURO'));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoMulta').Value             := strToInt(frmMenu.LeParametro('OPERBXPARCMULTA'));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoDesconto').Value          := strToInt(frmMenu.LeParametro('OPERBXPARCDESC'));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoAbatimento').Value        := strToInt(frmMenu.LeParametro('OPERBXPARCABAT'));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoLiquidacao').Value        := strToInt(frmMenu.LeParametro('OPERBXPARCLIQ'));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdOperacaoCorrecaoMonetaria').Value := strToInt(frmMenu.LeParametro('OPERBXPARCMONET'));

      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdFormaPagto').Value         := strToInt(Trim(edtFormaPagto.Text));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nCdContaBancaria').Value      := strToInt(Trim(edtContaBancaria.Text));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@nNrCheque').Value             := nCdNrCheque; // strToInt(Trim(edtNrCheque.Text));
      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@cNrDoc').Value                := edtNrDoc.Text;

      usp_atualizaBaixaEmpImobiliario.Parameters.ParamByName('@dDtLiq').Value                := edtDtLiq.Text;

      usp_atualizaBaixaEmpImobiliario.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  if (MessageDlg('Deseja Imprimir o Recibo ?' + #13#13 ,mtConfirmation,[mbYes,mbNo],0) = MRYES) then
     imprimeRecibo(qryLiquidaParcelanCdTitulo.Value,qryLiquidaParcelanCdTerceiro.Value,qryLiquidaParcelanCdEspTit.Value);

  Close;

end;

//juros
procedure TfrmLiquidaParcela.FormShow(Sender: TObject);
var
  objCorrecao : TfrmAtualizacaoMonetaria ;

  nValParcela_corrigido             : Double;
  nValParcela_corrigido_juros       : Double;
  nValParcela_corrigido_juros_Multa : Double;

begin
  inherited;

  edtDtLiq.Clear;
  edtValjuro.Clear;
  edtValMulta.Clear;
  edtValDesconto.Clear;
  edtValAbatimento.Clear;
  edtValReceber.Clear;
  edtDiasAtraso.Clear;
  edtFormaPagto.Clear;
  edtContaBancaria.Clear;
  edtNrCheque.Clear;
  edtNrDoc.Clear;

  if (qryContaBancaria.Active) then
  begin
      qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
      qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;
  end;

  nCdFormaPagtoCheque := strToint(frmMenu.LeParametro('FORMAPAGTOCHE')) ;
  nCdNrCheque := 0;

  // se for parcela tipo financiamento / intermediaria

  if (qryLiquidaParcela.Active) then
  begin
      if (qryLiquidaParcelaiParcela.Value > 0) then
      begin

          edtDtLiq.Text := DateToStr(Date());
          objCorrecao := TfrmAtualizacaoMonetaria.Create(nil) ;

          // atualizacao de parcela
          nValParcela_Corrigido := objCorrecao.atualizaIndiceParcela(date(),qryLiquidaParcelanCdTitulo.Value,0);
          edtValCorrecao.Value := nValParcela_Corrigido;

          //if ((objCorrecao.qryUltimaParcelaPaga.Active) and (not objCorrecao.qryUltimaParcelaPaga.Eof)) then
          //    edtValBaseCorrecao.Value := objCorrecao.qryUltimaParcelaPaganValCorrecao.Value
          //else
          
          edtValBaseCorrecao.Value := qryLiquidaParcelanSaldoTit.Value;

          //if (edtValCorrecao.Value = 0) then
          //    MensagemAlerta('O Valor da parcela n�o foi corrigido. Verifique se os valores dos �ndices de reajuste foram atualizados.') ;

          edtDiasAtraso.Text := IntToStr(objCorrecao.calculaDiasDecorrido(date(),qryLiquidaParceladDtVenc.Value));

          // parcelas vencidas calcula juros e multa
          if (strToInt(edtDiasAtraso.Text) > 0) then
          begin

              nValParcela_Corrigido_juros  := objCorrecao.atualizaJurosParcelaAtraso(date()
                                                                                    ,qryLiquidaParcelanCdTitulo.Value
                                                                                    ,edtValCorrecao.Value);

              edtValjuro.Value := (nValParcela_Corrigido_juros - nValParcela_Corrigido);

              nValParcela_Corrigido_juros_multa  := objCorrecao.calculaMulta(nValParcela_Corrigido_juros);
              edtValMulta.Value                  := (nValParcela_Corrigido_juros_multa - nValParcela_Corrigido_juros);

          end;

         // qryCotacao.Close;
         // qryCotacao.Parameters.ParamByName('nCdIndice').Value := objCorrecao.qryCotacaonCdCotacaoIndiceReajuste.Value;
         // qryCotacao.Parameters.ParamByName('iAno').Value      := objCorrecao.qryCotacaoiAnoReferencia.Value;
         // qryCotacao.Parameters.ParamByName('iMes').Value      := objCorrecao.qryCotacaoiMesReferencia.Value;
         // qryCotacao.Open;

         //edtAno.Text := objCorrecao.qryCotacaoiAnoReferencia.AsString;
         //edtMes.Text := objCorrecao.qryCotacaoiMesReferencia.AsString;

         //edtValIndiceCorrecao.Value := objCorrecao.qryCotacaonIndice.Value ;

          freeAndNil(objCorrecao) ;
     end
     else
     begin
         //parcela de sinal n�o tem correcao // s� assina o contrato depois de liquidar a parcela sinal.
         edtValCorrecao.Value := qryLiquidaParcelanSaldoTit.Value ;
     end;
  end;
  atualizaTotal();

end;

procedure TfrmLiquidaParcela.edtFormaPagtoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro = 1');

        If (nPK > 0) then
        begin
            edtFormaPagto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLiquidaParcela.edtFormaPagtoExit(Sender: TObject);
begin
  inherited;

  qryFormaPagto.Close;
  PosicionaQuery(qryFormaPagto, edtFormaPagto.Text) ;

  edtNrCheque.Clear;

  edtNrCheque.ReadOnly := True ;
  nCdNrCheque          := 0;

  if not qryFormaPagto.Eof then
      if (qryFormaPagtonCdFormaPagto.Value = nCdFormaPagtoCheque) then
      begin
          edtNrCheque.ReadOnly  := False ;
          edtNrCheque.Color     := clWhite ;
      end ;

  if qryFormaPagto.eof then
  begin
     MensagemAlerta('Informe a Forma de Pagamento');
     EdtFormaPagto.SetFocus;
     abort;
  end;

end;

procedure TfrmLiquidaParcela.edtContaBancariaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(39);

            If (nPK > 0) then
            begin
                edtContaBancaria.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmLiquidaParcela.edtContaBancariaExit(Sender: TObject);
begin
  inherited;
  qryContaBancaria.Close;

  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryContaBancaria.Parameters.ParamByName('nPk').Value := Trim(edtContaBancaria.Text);
  qryContaBancaria.Open;

  if qryContaBancaria.eof then
  begin
     MensagemAlerta('Informe a Conta Banc�ria');
     EdtContaBancaria.SetFocus;
     abort;
  end;
end;

procedure TfrmLiquidaParcela.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qryLiquidaParcela.Close;
  qryCotacao.Close;
  qryMovtoParcela.Close;
  qryFormaPagto.Close;
  qryContaBancaria.Close;
end;

procedure TfrmLiquidaParcela.edtDtLiqExit(Sender: TObject);
var
  objCorrecao : TfrmAtualizacaoMonetaria ;
  nValParcela_corrigido             : Double;
  nValParcela_corrigido_juros       : Double;
  nValParcela_corrigido_juros_Multa : Double;

begin
  inherited;

  objCorrecao := TfrmAtualizacaoMonetaria.Create(nil) ;

  //correcao a parcela
  nValParcela_Corrigido := objCorrecao.atualizaIndiceParcela(strToDate(edtDtLiq.Text),qryLiquidaParcelanCdTitulo.Value,edtValBaseCorrecao.Value);
  edtValCorrecao.Value := nValParcela_Corrigido;

  //if (edtValCorrecao.Value = 0) then
  //    MensagemAlerta('O Valor da parcela n�o foi corrigido. Verifique se os valores dos �ndices de reajuste foram atualizados.') ;

  edtDiasAtraso.Text := IntToStr(objCorrecao.calculaDiasDecorrido(strToDate(edtDtLiq.Text),qryLiquidaParceladDtVenc.Value));

  edtValJuro.Clear;
  edtValMulta.Clear;

  // parcelas vencidas calcula juros e multa
  if (strToInt(edtDiasAtraso.Text) > 0) then
  begin

      nValParcela_Corrigido_juros  := objCorrecao.atualizaJurosParcelaAtraso(strToDate(edtDtLiq.Text)
                                                                            ,qryLiquidaParcelanCdTitulo.Value
                                                                            ,edtValCorrecao.Value);

      edtValjuro.Value := (nValParcela_Corrigido_juros - nValParcela_Corrigido);

      nValParcela_Corrigido_juros_multa  := objCorrecao.calculaMulta(nValParcela_Corrigido_juros);
      edtValMulta.Value                  := (nValParcela_Corrigido_juros_multa - nValParcela_Corrigido_juros);

  end;

  atualizaTotal();

  freeAndNil(objCorrecao) ;

end;

procedure TfrmLiquidaParcela.atualizaTotal;
begin
  edtValReceber.Value := (qryLiquidaParcelanValCorrecao.Value + qryLiquidaParcelanValjuro.Value + qryLiquidaParcelanValMulta.Value - (qryLiquidaParcelanValDesconto.Value + qryLiquidaParcelanValAbatimento.Value));
  edtValReceber.Value := (edtValCorrecao.Value + edtValjuro.Value + edtValMulta.Value - (edtValDesconto.Value + edtValAbatimento.Value));
end;

procedure TfrmLiquidaParcela.edtValCorrecaoExit(Sender: TObject);
begin
  inherited;
  atualizaTotal();
end;

procedure TfrmLiquidaParcela.imprimeRecibo(nCdTitulo, nCdTerceiro, nCdEspTit: Integer);
var
  objRel : TrptReciboView ;
begin

  objRel := TrptReciboView.Create(nil) ;

  objRel.usp_Relatorio.Close;

  objRel.qryEmpresa.Close;
  objRel.qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  objRel.qryEmpresa.Open;

  objRel.qryTerceiro.Close;
  objRel.qryTerceiro.Parameters.ParamByName('nCdTerceiroEmpresa').Value  := objRel.qryEmpresanCdTerceiroEmp.Value ;
  objRel.qryTerceiro.Open;

  objRel.qryEndereco.Close;
  objRel.qryEndereco.Parameters.ParamByName('nCdTerceiro').Value  := objRel.qryTerceironCdTerceiro.Value ;
  objRel.qryEndereco.Open;

  objRel.usp_Relatorio.Close;
  objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value         := 'C' ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTitulo').Value      := nCdTitulo ;
  objRel.usp_Relatorio.Open;

  try
      try

          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmLiquidaParcela.edtValAbatimentoExit(Sender: TObject);
begin
  inherited;
    atualizaTotal();
end;

procedure TfrmLiquidaParcela.edtValDescontoExit(Sender: TObject);
begin
  inherited;
    atualizaTotal();
end;

procedure TfrmLiquidaParcela.edtNrChequeExit(Sender: TObject);
begin
  inherited;
  if (Trim(edtNrCheque.Text) <> '') then nCdNrCheque := strToInt(Trim(edtNrCheque.text));
end;

procedure TfrmLiquidaParcela.edtNrDocExit(Sender: TObject);
begin
  inherited;
  edtDtLiq.SetFocus;
end;

procedure TfrmLiquidaParcela.edtDtLiqEnter(Sender: TObject);
begin
  inherited;

  if (trim(edtDtLiq.Text) = '/  /') then
      edtDtLiq.Text := DateToStr(Date) ;

end;

procedure TfrmLiquidaParcela.edtValBaseCorrecaoExit(Sender: TObject);
var
  objCorrecao : TfrmAtualizacaoMonetaria ;
  nValParcela_corrigido             : Double;
  nValParcela_corrigido_juros       : Double;
  nValParcela_corrigido_juros_Multa : Double;

begin
  inherited;

  objCorrecao := TfrmAtualizacaoMonetaria.Create(nil) ;

  //correcao a parcela
  nValParcela_Corrigido := objCorrecao.atualizaIndiceParcela(strToDate(edtDtLiq.Text),qryLiquidaParcelanCdTitulo.Value,edtValBaseCorrecao.Value);
  edtValCorrecao.Value := nValParcela_Corrigido;

  if (edtValCorrecao.Value = 0) then
      MensagemAlerta('O Valor da parcela n�o foi corrigido. Verifique se os valores dos �ndices de reajuste foram atualizados.') ;

  edtDiasAtraso.Text := IntToStr(objCorrecao.calculaDiasDecorrido(strToDate(edtDtLiq.Text),qryLiquidaParceladDtVenc.Value));

  edtValJuro.Clear;
  edtValMulta.Clear;

  // parcelas vencidas calcula juros e multa
  if (strToInt(edtDiasAtraso.Text) > 0) then
  begin

      nValParcela_Corrigido_juros  := objCorrecao.atualizaJurosParcelaAtraso(strToDate(edtDtLiq.Text)
                                                                            ,qryLiquidaParcelanCdTitulo.Value
                                                                            ,edtValCorrecao.Value);

      edtValjuro.Value := (nValParcela_Corrigido_juros - nValParcela_Corrigido);

      nValParcela_Corrigido_juros_multa  := objCorrecao.calculaMulta(nValParcela_Corrigido_juros);
      edtValMulta.Value                  := (nValParcela_Corrigido_juros_multa - nValParcela_Corrigido_juros);

  end;

  atualizaTotal();

  freeAndNil(objCorrecao) ;

end;

initialization
     RegisterClass(TfrmLiquidaParcela) ;

end.
