unit fConciliacaoBancariaManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmConciliacaoBancariaManual = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacNmBanco: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdContaBancaria: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cNmBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cAgencia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTitular: TcxGridDBColumn;
    qryContaBancariadDtUltConciliacao: TDateTimeField;
    cxGrid1DBTableView1dDtUltConciliacao: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliacaoBancariaManual: TfrmConciliacaoBancariaManual;

implementation

uses fMenu, fConciliacaoBancariaManual_Ultimas;

{$R *.dfm}

procedure TfrmConciliacaoBancariaManual.FormShow(Sender: TObject);
begin
  inherited;

  cxGrid1.Align := alClient ;
  PosicionaQuery(qryContaBancaria, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

end;

procedure TfrmConciliacaoBancariaManual.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConciliacaoBancariaManual_Ultimas ;
begin
  inherited;

  if not qryContaBancaria.Eof then
  begin

      objForm := TfrmConciliacaoBancariaManual_Ultimas.Create( Self ) ;

      PosicionaQuery(objForm.qryConciliacao,qryContaBancarianCdContaBancaria.AsString) ;
      showForm( objForm , TRUE ) ;

  end ;

end;

initialization
    RegisterClass(TfrmConciliacaoBancariaManual) ;

end.
