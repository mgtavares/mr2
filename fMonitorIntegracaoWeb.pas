unit fMonitorIntegracaoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DB, ADODB, GridsEh, DBGridEh, cxPC,
  cxControls, Menus, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxImageComboBox;

type
  TfrmMonitorIntegracaoWeb = class(TfrmProcesso_Padrao)
    qryPedidoWeb: TADOQuery;
    dsPedidoWeb: TDataSource;
    SP_INTEGRA_PEDIDO_WEB: TADOStoredProc;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn7: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn8: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn9: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    cxGrid1DBTableView1DBColumn10: TcxGridDBColumn;
    Image2: TImage;
    cxGrid: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdPedidoWeb: TcxGridDBColumn;
    cxGridDBTableView1dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1nValPedidoWeb: TcxGridDBColumn;
    cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1cCdCliente: TcxGridDBColumn;
    cxGridDBTableView1cNmCliente: TcxGridDBColumn;
    cxGridDBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView1cNmTabStatusPed: TcxGridDBColumn;
    cxGridDBTableView1cCdTabStatusPed: TcxGridDBColumn;
    cxGridDBTableView1cStatusIntegra: TcxGridDBColumn;
    qryAux: TADOQuery;
    SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO: TADOStoredProc;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB: TADOStoredProc;
    ToolButton6: TToolButton;
    btIntegrarPedido: TToolButton;
    qryVerificaAutPedido: TADOQuery;
    qryPedidocFlgLibParcial: TIntegerField;
    qryPedidocFlgProdutoLiberado: TIntegerField;
    qryPedidocNmTerceiroEntrega: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidodDtAutor: TDateTimeField;
    qryPedidonCdUsuarioAutor: TIntegerField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocAtuCredito: TIntegerField;
    qryPedidocCreditoLibMan: TIntegerField;
    qryPedidonCdTerceiroPagador: TIntegerField;
    qryPedidocGerarFinanc: TIntegerField;
    qryPedidocFlgVenda: TIntegerField;
    qryPedidonCdTabTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    PopupMenu1: TPopupMenu;
    btVisualizarPedidoWeb: TMenuItem;
    qryPedidocExigeAutor: TIntegerField;
    qryPedidocFlgPedidoWeb: TIntegerField;
    qryPedidoWebdDtPedido: TDateTimeField;
    qryPedidoWebnValPedidoWeb: TBCDField;
    qryPedidoWebcNmFormaPagto: TStringField;
    qryPedidoWebcCdCliente: TStringField;
    qryPedidoWebcNmCliente: TStringField;
    qryPedidoWebcCNPJCPF: TStringField;
    qryPedidoWebnCdTabStatusPedWeb: TIntegerField;
    qryPedidoWebcNmTabStatusPedWeb: TStringField;
    qryPedidoWebcStatusIntegra: TStringField;
    qryPedidoWebnCdPedido: TIntegerField;
    qryPedidoWebcNumeroEnderecoCobranca: TStringField;
    qryPedidoWebcNumeroEnderecoEntrega: TStringField;
    qryPedidoWebcCdPedidoWeb: TStringField;
    qryPedidoWebnValProdutosWeb: TBCDField;
    qryPedidoWebnValFreteWeb: TBCDField;
    qryPedidoWebnValDescontoWeb: TBCDField;
    cxGridDBTableView1nValDescontoWeb: TcxGridDBColumn;
    cxGridDBTableView1nValFreteWeb: TcxGridDBColumn;
    cxGridDBTableView1nValProdutosWeb: TcxGridDBColumn;
    qryVerificaIntegracao: TADOQuery;
    qryVerificaIntegracaocMsgRetorno: TStringField;
    qryPedidoWebdDtUltAtualizacao: TDateTimeField;
    ToolButton7: TToolButton;
    btCancelarIntegracao: TToolButton;
    qryCancel: TADOQuery;
    btCancelarIntegracaoPed: TMenuItem;
    N1: TMenuItem;
    qryPedidoWebnCdPedidoWeb: TIntegerField;
    qryPedidoWebdDtCancel: TDateTimeField;
    cxGridDBTableView1cCdPedidoWebOrig: TcxGridDBColumn;
    qryPedidoWebcOrigPedidoWeb: TStringField;
    cxGridDBTableView1cOrigPedidoWeb: TcxGridDBColumn;
    qryPedidocNmTabStatusPed: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure btIntegrarPedidoClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure btVisualizarPedidoWebClick(Sender: TObject);
    procedure btCancelarIntegracaoClick(Sender: TObject);
    procedure btCancelarIntegracaoPedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AutorizarPedido(nCdPedido : Integer);
    procedure CancelarPedido(nCdPedido : Integer);
  end;

var
  frmMonitorIntegracaoWeb: TfrmMonitorIntegracaoWeb;

implementation

{$R *.dfm}
uses
  fMenu,fPedidoVendaWeb,rPedidoCom_Simples,fMotivoCancelaSaldoPedido, fAutoriza_Pedido,rPedidoVenda ;

procedure TfrmMonitorIntegracaoWeb.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryPedidoWeb.Close;
  qryPedidoWeb.Open;
end;

procedure TfrmMonitorIntegracaoWeb.ToolButton5Click(Sender: TObject);
var
  objRel : TrptPedidoVenda;
begin
  if (qryPedidoWeb.IsEmpty) then
      Exit;

  if (qryPedidoWebnCdPedido.IsNull) then
  begin
      MensagemAlerta('Pedido n�o pode ser impresso devido ainda n�o estar importado no sistema.');
      Abort;
  end;

  try
      try
          objRel := TrptPedidoVenda.Create(nil);

          PosicionaQuery(objRel.qryPedido,qryPedidoWebnCdPedido.AsString);
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryPedidoWebnCdPedido.AsString);

          objRel.QRSubDetail1.Enabled := True;
          objRel.QRSubDetail2.Enabled := False;
          objRel.QRSubDetail3.Enabled := False;

         if (objRel.qryItemEstoque_Grade.eof) then
             objRel.QRSubDetail1.Enabled := False;

         objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

         objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmMonitorIntegracaoWeb.AutorizarPedido(nCdPedido:Integer);
begin
  case MessageDlg('Confirma a autoriza��o deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  if (qryPedidocFlgLibParcial.Value = 1) and (qryPedidocFlgProdutoLiberado.Value = 1) then
  begin
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 13;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'FATURAR PEDIDO';
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'O Pedido ' + qryPedidonCdPedido.AsString + ',  Terceiro: ' + qryPedidocNmTerceiroEntrega.Value + ' j� est� liberado para faturamento.';
      frmMenu.SP_GERA_ALERTA.ExecProc;
  end;

  qryPedido.Edit;
  qryPedidonCdTabStatusPed.Value := 3;
  qryPedidodDtAutor.Value        := Now();
  qryPedidonCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT cLimiteCredito FROM TipoPedido WHERE nCdTipoPedido = ' + qryPedidonCdTipoPedido.asString);
  qryAux.Open;

  if not qryAux.eof and (qryAux.FieldList[0].Value = 1) then
  begin
      qryPedidocAtuCredito.Value    := 1;
      qryPedidocCreditoLibMan.Value := 1;
  end;

  qryAux.Close;
  qryPedido.Post;

  if (qryPedidocGerarFinanc.Value = 1) and (qryPedidocFlgVenda.Value = 1) then
  begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE Terceiro SET nValPedidoAbertoWeb = nValPedidoAbertoWeb + IsNull((SELECT nValPedido FROM Pedido WHERE nCdPedido = ' + qryPedidonCdPedido.AsString + '),0) WHERE nCdTerceiro = ' + qryPedidonCdTerceiroPagador.AsString);
      qryAux.ExecSQL;
      qryAux.Close;
  end;

  { -- envia o pedido para as lojas -- }
  SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Close;
  SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Parameters.ParamByName('@nCdPedido').Value := qryPedidonCdPedido.Value;
  SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.ExecProc;
end;

procedure TfrmMonitorIntegracaoWeb.CancelarPedido(nCdPedido : Integer);
var
  objForm : TfrmMotivoCancelaSaldoPedido;
begin
  if (not qryPedidoWeb.Active) then
  begin
      MensagemAlerta('Nenhum pedido ativo.');
      Exit;
  end;

  try
      objForm := TfrmMotivoCancelaSaldoPedido.Create(Self);

      objForm.cancelaSaldoPedido(nCdPedido, 0);
  finally
      FreeAndNil(objForm);
  end;
end;

procedure TfrmMonitorIntegracaoWeb.btIntegrarPedidoClick(Sender: TObject);
var
  objFormPedido   : TfrmPedidoVendaWeb;
  cNumeroEntrega  : String;
  cNumeroCobranca : String;
  i               : Integer;
label
  atualizarStatus;
begin
  inherited;

  try
      if (qryPedidoWeb.IsEmpty) then
          Exit;

      qryPedido.Close;
      qryPedido.Parameters.ParamByName('nPK').Value        := qryPedidoWebnCdPedido.Value;
      qryPedido.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryPedido.Open;

      { -- efetua integra��o do pedido web -- }
      if (qryPedido.IsEmpty) then
      begin
          { -- trata informa��es do pedido antes de sua integra��o devido plataforma n�o haver estes tratamentos -- }
          { -- verifica n�mero de end. de entrega -- }
          {for i := 1 to Length(qryPedidoWebcNumeroEnderecoEntrega.Value) do
              if (qryPedidoWebcNumeroEnderecoEntrega.Value[i] in ['0'..'9']) then
                  cNumeroEntrega := cNumeroEntrega + qryPedidoWebcNumeroEnderecoEntrega.Value[i];}
          try
              StrToInt(qryPedidoWebcNumeroEnderecoEntrega.Value);
          except
              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('UPDATE PedidoWeb                                                                                                                                  ');
              qryAux.SQL.Add('   SET cComplementoEnderecoEntrega = SUBSTRING(UPPER(cNumeroEnderecoEntrega + ' + #39 + ' Compl. ' + #39 + ' + cComplementoEnderecoEntrega),1,100)');
              qryAux.SQL.Add('      ,cNumeroEnderecoEntrega      = ' + #39 + '0' + #39                                                                                           );
              qryAux.SQL.Add(' WHERE nCdPedidoWeb = ' + qryPedidoWebnCdPedidoWeb.AsString                                                                                        );
              qryAux.ExecSQL;
          end;

          { -- verifica n�mero de end. de cobran�a -- }
          {for i := 1 to Length(qryPedidoWebcNumeroEnderecoCobranca.Value) do
              if (qryPedidoWebcNumeroEnderecoCobranca.Value[i] in ['0'..'9']) then
                  cNumeroCobranca := cNumeroCobranca + qryPedidoWebcNumeroEnderecoCobranca.Value[i];}

          try
              StrToInt(qryPedidoWebcNumeroEnderecoCobranca.Value);
          except
              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('UPDATE PedidoWeb                                                                                                                                     ');
              qryAux.SQL.Add('   SET cComplementoEnderecoCobranca = SUBSTRING(UPPER(cNumeroEnderecoCobranca + ' + #39 + ' Compl. ' + #39 + ' + cComplementoEnderecoCobranca),1,100)');
              qryAux.SQL.Add('      ,cNumeroEnderecoCobranca      = ' + #39 + '0' + #39                                                                                             );
              qryAux.SQL.Add(' WHERE nCdPedidoWeb = ' + qryPedidoWebnCdPedidoWeb.AsString                                                                                           );
              qryAux.ExecSQL;
          end;
          
          if (MessageDLG('Confirma integra��o do pedido de venda ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
              Exit;

          try
              frmMenu.Connection.BeginTrans;

              SP_INTEGRA_PEDIDO_WEB.Close;
              SP_INTEGRA_PEDIDO_WEB.Parameters.ParamByName('@nCdPedidoWeb').Value := qryPedidoWebnCdPedidoWeb.Value;
              SP_INTEGRA_PEDIDO_WEB.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
              SP_INTEGRA_PEDIDO_WEB.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
              SP_INTEGRA_PEDIDO_WEB.Parameters.ParamByName('@nCdLoja').Value      := StrToInt(frmMenu.LeParametro('LOJAWEB'));
              SP_INTEGRA_PEDIDO_WEB.ExecProc;

              frmMenu.Connection.CommitTrans;

              { -- atualiza inf. query -- }
              qryPedido.Close;
              qryPedido.Parameters.ParamByName('nPK').Value        := SP_INTEGRA_PEDIDO_WEB.Parameters.ParamByName('@nCdPedido_Out').Value;
              qryPedido.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
              qryPedido.Open;

              frmMenu.LogAuditoria(30,1,qryPedidonCdPedido.Value,'Integra��o do Pedido');

              { -- chama processo de visualiza��o do pedido web se o usu�rio possuir permiss�o de acesso -- }
              if (frmMenu.fnValidaUsuarioAPL('FRMPEDIDOVENDAWEB')) then
              begin
                  if (MessageDLG('Pedido integrado com sucesso. Deseja visualizar ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
                  begin
                      objFormPedido := TfrmPedidoVendaWeb.Create(nil);

                      objFormPedido.PosicionaQuery(objFormPedido.qryMaster, IntToStr(qryPedidonCdPedido.Value));
                      showForm(objFormPedido, True);
                  end;
              end;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no Processamento.');
              Raise;
              Exit;
          end;
      end;

      { -- verifica se pedido web n�o foi processado por outro usu�rio -- }
      qryVerificaIntegracao.Close;
      qryVerificaIntegracao.Parameters.ParamByName('nCdPedidoWeb').Value      := qryPedidoWebnCdPedidoWeb.Value;
      qryVerificaIntegracao.Parameters.ParamByName('dDtUltAtualizacao').Value := DateTimeToStr(qryPedidoWebdDtUltAtualizacao.Value);
      qryVerificaIntegracao.Open;

      if (qryVerificaIntegracaocMsgRetorno.Value <> 'Ok') then
      begin
          MensagemErro('Erro ao integrar o pedido web No ' + qryPedidoWebnCdPedidoWeb.AsString + ' devido existir uma atualiza��o mais recente.');
          Exit;
      end;

      { -- verifica se status do pedido integrado esta numa fase posterior ao pedido web -- }
      if ((qryPedidoWebnCdTabStatusPedWeb.Value <> 0) and (qryPedidoWebnCdTabStatusPedWeb.Value < qryPedidonCdTabStatusPed.Value)) then
      begin
          if (MessageDLG('Status do pedido web � inferior ao status do pedido gerado no ERP. Deseja atualizar o status do pedido web ?'
                        +#13#13
                        +'Status Web : ' + qryPedidoWebcNmTabStatusPedWeb.Value + #13
                        +'Status ERP  : ' + qryPedidocNmTabStatusPed.Value, mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
              Exit;

          goto atualizarStatus;
      end;

      { -- se pedido web j� foi integrado, efetua processo de atualiza��o do pedido efetivo -- }
      case (qryPedidoWebnCdTabStatusPedWeb.Value) of
          { -- status n�o parametrizados -- }
          0 : begin
                  if (MessageDLG('Status do pedido web n�o parametrizado. Deseja atualizar o status do pedido web para o status do pedido atual ?'
                               + #13#13
                               + 'Status Web : ' + qryPedidoWebcNmTabStatusPedWeb.Value, mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
                      Exit;

                  atualizarStatus:

                  try
                      frmMenu.Connection.BeginTrans;

                      { -- equipara status do pedido web -- }
                      qryAux.Close;
                      qryAux.SQL.Clear;
                      qryAux.SQL.Add('UPDATE PedidoWeb                                                                       ');
                      qryAux.SQL.Add('   SET cCdTabStatusPed      = CAST(Pedido.nCdTabStatusPed as varchar)                  ');
                      qryAux.SQL.Add('      ,cStatusPed           = cNmTabStatusPed                                          ');
                      qryAux.SQL.Add('      ,cDtUltAtualizacaoPed = CONVERT(varchar, GETDATE(),120)                          ');
                      qryAux.SQL.Add('  FROM PedidoWeb                                                                       ');
                      qryAux.SQL.Add('       INNER JOIN Pedido       ON Pedido.nCdPedido             = PedidoWeb.nCdPedido   ');
                      qryAux.SQL.Add('       INNER JOIN TabStatusPed ON TabStatusPed.nCdTabStatusPed = Pedido.nCdTabStatusPed');
                      qryAux.SQL.Add(' WHERE PedidoWeb.nCdPedido = ' + qryPedidonCdPedido.AsString                            );
                      qryAux.ExecSQL;

                      frmMenu.LogAuditoria(30,2,qryPedidonCdPedido.Value,'Atualiza��o de Status - ' + qryPedidoWebcNmTabStatusPedWeb.Value);

                      ShowMessage('Status do Pedido atualizado com sucesso.');

                      frmMenu.Connection.CommitTrans;
                  except
                      frmMenu.Connection.RollbackTrans;
                      MensagemErro('Erro no Processamento.');
                      Raise;
                      Exit;
                  end;
              end;
          { -- autoriza pedido (3	: Autorizado) -- }
          3 : begin
                  { -- n�o permite autoriza��o caso o pedido ainda n�o foi finalizado (1 : Digitado) -- }
                  if (qryPedidonCdTabStatusPed.Value = 1) then
                  begin
                      MensagemAlerta('Necess�rio finalizar o pedido antes de efetuar sua autoriza��o.');
                      Exit;
                  end;

                  { -- se tipo de pedido exige autoriza��o, verifica se usu�rio possui permiss�o para autorizar este tipo de pedido -- }
                  if (qryPedidocExigeAutor.Value = 1) then
                  begin
                      qryVerificaAutPedido.Close;
                      qryVerificaAutPedido.Parameters.ParamByName('nPK').Value        := qryPedidonCdTipoPedido.Value;
                      qryVerificaAutPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                      qryVerificaAutPedido.Open;

                      if (qryVerificaAutPedido.IsEmpty) then
                      begin
                          MensagemAlerta('Usu�rio n�o possui permiss�o para autorizar este tipo de pedido.' + #13 + 'Tipo de Pedido: ' + UpperCase(qryPedidonCdTipoPedido.AsString + ' - ' + qryPedidocNmTipoPedido.Value));
                          Abort;
                      end;
                  end;

                  try
                      frmMenu.Connection.BeginTrans;

                      { -- chama processo de autoriza��o de pedido -- }
                      AutorizarPedido(qryPedidonCdPedido.Value);

                      { -- verifica se pedido foi aprovado -- }
                      qryAux.Close;
                      qryAux.SQL.Clear;
                      qryAux.SQL.Add('SELECT 1 FROM Pedido WHERE nCdTabStatusPed = 3 AND nCdPedido = ' + qryPedidonCdPedido.asString);
                      qryAux.Open;

                      if (not qryAux.IsEmpty) then
                      begin
                          { -- liquida adiantamento de cr�dito do cliente e gera t�tulos para concilia��o no financeiro -- }
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Close;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdPedido').Value  := qryPedidonCdPedido.Value;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.ExecProc;

                          frmMenu.LogAuditoria(30,2,qryPedidonCdPedido.Value,'Atualiza��o de Status - ' + qryPedidoWebcNmTabStatusPedWeb.Value);

                          ShowMessage('Pedido Autorizado com sucesso.');
                      end;

                      frmMenu.Connection.CommitTrans;
                  except
                      frmMenu.Connection.RollbackTrans;
                      MensagemErro('Erro no Processamento.');
                      Raise;
                      Exit;
                  end;
              end;
          { -- cancela pedido (10	: Cancelado) -- }
          10 : begin
                   { -- n�o permite cancelamento caso o pedido ainda n�o foi finalizado (1 : Digitado) -- }
                   if (qryPedidonCdTabStatusPed.Value = 1) then
                   begin
                       MensagemAlerta('Necess�rio finalizar o pedido antes de efetuar seu cancelamento.');
                       Exit;
                   end;

                   if (qryPedidonCdTabStatusPed.Value > 3) then
                   begin
                       MensagemAlerta('O status do pedido n�o permite mais cancelamento.');
                       Exit;
                   end;

                   { -- verifica se pedido j� foi aprovado anteriormente -- }
                   if (qryPedidonCdTabStatusPed.Value = 3) then
                   begin
                       if (MessageDLG('Pedido j� foi aprovado e possui adiantamento de cr�dito vinculado ao cliente '
                           + qryPedidonCdTerceiroPagador.AsString + ' - ' + qryPedidocNmTerceiroEntrega.Value + '.'
                           + #13#13
                           + 'Deseja cancelar o saldo do pedido ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
                           Exit;
                   end;

                   { -- chama processo de cancelamento de pedido -- }
                   { -- obs: executa procedure SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB dentro do processo de cancelamento do pedido -- }
                   CancelarPedido(qryPedidonCdPedido.Value);

                   { -- verifica se pedido foi cancelado -- }
                   qryAux.Close;
                   qryAux.SQL.Clear;
                   qryAux.SQL.Add('SELECT 1 FROM Pedido WHERE nCdTabStatusPed = 10 AND nCdPedido = ' + qryPedidonCdPedido.asString);
                   qryAux.Open;

                   if (not qryAux.IsEmpty) then
                   begin
                       frmMenu.LogAuditoria(30,2,qryPedidonCdPedido.Value,'Atualiza��o de Status - ' + qryPedidoWebcNmTabStatusPedWeb.Value);

                       ShowMessage('Pedido Cancelado com Sucesso.');
                   end;
               end;
      end;
  finally
      { -- atualiza grid -- }
      ToolButton1.Click;
  end;
end;
procedure TfrmMonitorIntegracaoWeb.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  btIntegrarPedido.Click;
end;

procedure TfrmMonitorIntegracaoWeb.btVisualizarPedidoWebClick(
  Sender: TObject);
var
  objFormPedido : TfrmPedidoVendaWeb;
begin
  if (qryPedidoWeb.IsEmpty) then
      Exit;

  if (qryPedidoWebnCdPedido.IsNull) then
  begin
      MensagemAlerta('Pedido n�o pode ser visualizado devido ainda n�o estar importado no sistema.');
      Abort;
  end;

  if (not frmMenu.fnValidaUsuarioAPL('FRMPEDIDOVENDAWEB')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Exit;
  end;

  objFormPedido := TfrmPedidoVendaWeb.Create(nil);

  objFormPedido.PosicionaQuery(objFormPedido.qryMaster, qryPedidoWebnCdPedido.AsString);
  showForm(objFormPedido, True);

  { -- atualiza grid -- }
  ToolButton1.Click;
end;

procedure TfrmMonitorIntegracaoWeb.btCancelarIntegracaoClick(Sender: TObject);
begin
  inherited;

  if (qryPedidoWeb.IsEmpty) then
      Exit;

  if (MessageDLG('Deseja realmente cancelar a integra��o do pedido web No ' + qryPedidoWebnCdPedidoWeb.AsString + ' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
      try
          frmMenu.Connection.BeginTrans;

          qryCancel.Close;
          qryCancel.Parameters.ParamByName('nCdUsuarioCancel').Value := frmMenu.nCdUsuarioLogado;
          qryCancel.Parameters.ParamByName('nCdPedidoWeb').Value     := qryPedidoWebnCdPedidoWeb.Value;
          qryCancel.ExecSQL;

          frmMenu.Connection.CommitTrans;

          ToolButton1.Click;

          ShowMessage('Registro de integra��o cancelado com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
          Abort;
      end;
  end;
end;

procedure TfrmMonitorIntegracaoWeb.btCancelarIntegracaoPedClick(Sender: TObject);
begin
  inherited;

  btCancelarIntegracao.Click;
end;

initialization
    RegisterClass(TfrmMonitorIntegracaoWeb);
end.
