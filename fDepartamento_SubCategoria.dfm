inherited frmDepartamento_SubCategoria: TfrmDepartamento_SubCategoria
  Left = 432
  Top = 115
  Width = 561
  Height = 456
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'SubCategoria'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 545
    Height = 389
  end
  inherited ToolBar1: TToolBar
    Width = 545
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 545
    Height = 389
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsSubCategoria
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
        ReadOnly = True
        Tag = 1
      end
      item
        EditButtons = <>
        FieldName = 'cNmSubCategoria'
        Footers = <>
        Width = 300
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'nCdIdExternoWeb'
        Footers = <>
        Title.Caption = 'C'#243'd. Externo Web'
        Width = 65
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 400
    Top = 144
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySubCategoriaBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM SubCategoria'
      ' WHERE nCdCategoria = :nPK')
    Left = 336
    Top = 176
    object qrySubCategorianCdSubCategoria: TAutoIncField
      DisplayLabel = 'Clique com o bot'#227'o direito para ver as op'#231#245'es dispon'#237'veis|C'#243'd'
      FieldName = 'nCdSubCategoria'
    end
    object qrySubCategorianCdCategoria: TIntegerField
      FieldName = 'nCdCategoria'
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      DisplayLabel = 
        'Clique com o bot'#227'o direito para ver as op'#231#245'es dispon'#237'veis|Descri' +
        #231#227'o'
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdStatus: TIntegerField
      DisplayLabel = 'Clique com o bot'#227'o direito para ver as op'#231#245'es dispon'#237'veis|Status'
      FieldName = 'nCdStatus'
    end
    object qrySubCategorianCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 368
    Top = 176
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmSubCategoria'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM SubCategoria'
      'WHERE cNmSubCategoria = :cNmSubCategoria'
      'AND nCdCategoria = :nCdCategoria ')
    Left = 368
    Top = 144
  end
  object PopupMenu1: TPopupMenu
    Left = 336
    Top = 144
    object ExibirSegmentos1: TMenuItem
      Caption = 'Exibir Segmentos'
      OnClick = ExibirSegmentos1Click
    end
    object ExibirMarcasVinculadas1: TMenuItem
      Caption = 'Exibir Marcas Vinculadas'
      OnClick = ExibirMarcasVinculadas1Click
    end
  end
end
