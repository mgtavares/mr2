inherited frmCanalDistribuicao: TfrmCanalDistribuicao
  Caption = 'Canal de Distribui'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 21
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 10
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 27
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCanalDistribuicao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 64
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmCanalDistribuicao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 64
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    OnEnter = DBEdit3Enter
    OnExit = DBEdit3Exit
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 133
    Top = 80
    Width = 145
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStatus
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CanalDistribuicao'
      ' WHERE nCdCanalDistribuicao = :nPK')
    object qryMasternCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
    object qryMastercNmCanalDistribuicao: TStringField
      FieldName = 'cNmCanalDistribuicao'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdStatus'
      '      ,cNmStatus'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK')
    Left = 400
    Top = 240
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsStatus: TDataSource
    DataSet = qryStatus
    Left = 416
    Top = 272
  end
end
