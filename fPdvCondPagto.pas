unit fPdvCondPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, GridsEh, DBGridEh, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCurrencyEdit, StdCtrls, ExtCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, jpeg, cxLookAndFeelPainters,
  cxButtons, cxCheckBox;

type
    TRegistro = record
    nCdFormaPagto: integer ;
    nCdCondPagto : integer;
    cNmCondPagto: string;
end;


type
  TfrmPdvCondPagto = class(TForm)
    qryCondPagto: TADOQuery;
    dsCondPagto: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    edtValorPagar: TcxCurrencyEdit;
    edtSaldo: TcxCurrencyEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdFormaPagto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdCondPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgPermDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nPercDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nPercAcrescimo: TcxGridDBColumn;
    cxGrid1DBTableView1nValorTOtal: TcxGridDBColumn;
    cxGrid1DBTableView1dPrimeiraParcela: TcxGridDBColumn;
    cxGrid1DBTableView1iParcelas: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgExigeCliente: TcxGridDBColumn;
    Image1: TImage;
    btFechar: TcxButton;
    cxGrid1DBTableView1nValEntrada: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    qryCondPagtonCdFormaPagto: TIntegerField;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryCondPagtocFlgPermDesconto: TIntegerField;
    qryCondPagtonPercDesconto: TBCDField;
    qryCondPagtonPercAcrescimo: TBCDField;
    qryCondPagtodPrimeiraParcela: TDateTimeField;
    qryCondPagtonValorTotal: TBCDField;
    qryCondPagtonPercEntrada: TBCDField;
    qryCondPagtonValEntrada: TBCDField;
    qryCondPagtonValParcelas: TBCDField;
    qryCondPagtoiParcelas: TIntegerField;
    qryCondPagtocFlgExigeCliente: TIntegerField;
    function SelecionaCondPagto(): integer ;
    procedure edtValorPagarExit(Sender: TObject);
    procedure edtValorPagarKeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure ExibeCondicoesPagamento;
    procedure FormShow(Sender: TObject);
    procedure edtValorPagarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFecharClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    dDtUltAtualizacao : TDateTime ;
    nCdFormaPagto : integer ;
    nCdLoja : integer ;
    nCdNumeroRegistros : integer ;
    nCdCondPagtoSelec : integer ;
    cNmCondPagtoSelec : String ;
    DPRegistros : array[1..1000] of TRegistro;
    Registro : TRegistro;
    bTemParcela      : boolean  ;
    cFlgLiqCrediario : integer  ;
    dDtPrimParcela   : TDateTime ;
    nValPrimParcela  : double   ;
    nValProxParcela  : double   ;
    nValorFinal      : double   ;
    cPermDesconto    : string  ;
    cFlgExigeCliente : integer ;
    nCdCondPagtoPre  : integer ;
    bItemPromocao    : boolean ;

  end;

var
  frmPdvCondPagto: TfrmPdvCondPagto;

implementation

uses fPdv,fMenu;

{$R *.dfm}

function TfrmPdvCondPagto.SelecionaCondPagto(): integer ;
var
    i:integer ;
    iAux2:integer ;
begin

    qryCondPagto.Close ;

    if (edtValorPagar.Value > 0) and (not edtValorPagar.Enabled) then
        ExibeCondicoesPagamento ;

    Self.ShowModal() ;

    if not qryCondPagto.Eof then
        Result := qryCondPagtonCdCondPagto.Value
    else Result := 0 ;

end;

procedure TfrmPdvCondPagto.edtValorPagarExit(Sender: TObject);
begin

    if (edtValorPagar.Value <= 0) then
    begin
        frmPDV.MensagemErro('Informe o valor do pagamento.') ;
        edtValorPagar.SetFocus ;
        abort ;
    end ;

    if (edtValorPagar.Value > edtSaldo.Value) then
    begin
        frmPDV.ShowMessage('Valor n�o pode ser maior que o saldo a pagar.') ;
        edtValorPagar.Value := edtSaldo.Value ;
        edtValorPagar.SetFocus;
        abort ;
    end ;

    ExibeCondicoesPagamento;

    cxGrid1.SetFocus ;

end;

procedure TfrmPdvCondPagto.edtValorPagarKeyPress(Sender: TObject;
  var Key: Char);
begin

  if (key = #13) then
      edtValorPagar.OnExit(nil) ;

end;

procedure TfrmPdvCondPagto.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin

    case Key of
        #13 : begin
            nValorFinal       := qryCondPagtonValorTotal.Value        ;
            dDtPrimParcela    := qryCondPagtodPrimeiraParcela.Value   ;
            nValProxParcela   := qryCondPagtonValParcelas.Value ;
            cFlgExigeCliente  := qryCondPagtocFlgExigeCliente.Value  ;
            cNmCondPagtoSelec := qryCondPagtocNmCondPagto.Value ;
            close ;
        end ;
    end ;


end;

procedure TfrmPdvCondPagto.ExibeCondicoesPagamento;
begin

    qryCondPagto.Close ;
    qryCondPagto.Parameters.ParamByName('nCdLoja').Value          := nCdLoja ;
    qryCondPagto.Parameters.ParamByName('cFlgLiqCrediario').Value := cFlgLiqCrediario ;
    qryCondPagto.Parameters.ParamByName('nCdFormaPagto').Value    := nCdFormaPagto ;
    qryCondPagto.Parameters.ParamByName('nValorTotal').Value      := edtValorPagar.Value ;
    qryCondPagto.Parameters.ParamByName('cPermDesconto').Value    := 'S' ;
    qryCondPagto.Parameters.ParamByName('cFlgItemPromocao').Value := 'N' ;

    if (bItemPromocao) then
        qryCondPagto.Parameters.ParamByName('cFlgItemPromocao').Value := 'S' ;

    if (edtValorPagar.Value < edtSaldo.Value) then
        qryCondPagto.Parameters.ParamByName('cPermDesconto').Value := 'N' ;

    qryCondPagto.Open ;

    if (qryCondPagto.eof) then
    begin
        if not bItemPromocao then
            frmPDV.MensagemErro('Nenhuma condi��o de pagamento ativa para esta forma de pagamento.')
        else frmPDV.MensagemErro('Nenhuma condi��o de pagamento ativa para esta forma de pagamento com produtos em promo��o.') ;

        PostMessage(Self.Handle, WM_CLOSE, 0, 0);
        exit ;
    end ;

    qryCondPagto.First ;

    if (nCdCondPagtoPre > 0) then
    begin

        while (nCdCondPagtoPre <> qryCondPagtonCdCondPagto.Value) and (not qryCondPagto.Eof) do
        begin
            qryCondPagto.next ;
        end ;

    end ;

end;

procedure TfrmPdvCondPagto.FormShow(Sender: TObject);
begin

    if (not qryCondPagto.Eof) then
        cxGrid1.Setfocus
    else if (edtValorPagar.Enabled) then
        edtValorPagar.setFocus;

     if frmMenu.LeParametro('CONDACRESEXIB') = 'N' then
     begin
         cxGrid1DBTableView1nPercAcrescimo.Visible := false;
     end;

end;

procedure TfrmPdvCondPagto.edtValorPagarKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (key = VK_ESCAPE) then
      btFechar.Click;


end;

procedure TfrmPdvCondPagto.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  if (key = VK_ESCAPE) then
      btFechar.Click;

end;

procedure TfrmPdvCondPagto.btFecharClick(Sender: TObject);
begin
    qryCondPagto.Close ;
    Close ;
end;

end.
