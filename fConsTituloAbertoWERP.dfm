inherited frmConsTituloAbertoWERP: TfrmConsTituloAbertoWERP
  Left = 0
  Top = 0
  Width = 1296
  Height = 786
  Caption = 'Consulta de T'#237'tulos em aberto - WERP'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 169
    Width = 1280
    Height = 581
  end
  inherited ToolBar1: TToolBar
    Width = 1280
    ButtonWidth = 98
    inherited ToolButton1: TToolButton
      Caption = 'Exibir Consulta'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 98
    end
    inherited ToolButton2: TToolButton
      Left = 106
    end
  end
  object cxButton1: TcxButton [2]
    Left = 960
    Top = 448
    Width = 25
    Height = 25
    Hint = 'Exportar resultado para Microsoft Excel'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Visible = False
    OnClick = cxButton1Click
    Glyph.Data = {
      26040000424D2604000000000000360000002800000012000000120000000100
      180000000000F003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFCFCFCF4F4F4EBEB
      EBE6E6E6E2E2E2DCDCDCD5D5D5CDCDCDC7C7C7BFBFBFB8B8B8B3B3B3C4C4C4D8
      D8D8FAFAFAFFFFFF0000FFFFFFFFFFFFF5F5F5D9D9D9BDBDBDB1B1B1A9A9A9AD
      AEAEB2B3B2C0C4C1C5CBC7CED5D1DBE5DFE1EDE6D8E1DBC2C2C2E7E7E7FFFFFF
      0000FFFFFF1D6B1E1D6B1E87A688C5D2C6D5DED5FAFFFCF9FFFBF8FFFAF7FFFA
      F6FFF9F4FFF8F3FFF8F2FFF7C6D9CABFBFBFE6E6E6FFFFFF0000FFFFFF1D6B1E
      9BB99DC3D0C5FDFFFDFCFFFDFBFFFCF9FFFBF7FEF9F6FEF88BC0990B6E310B6E
      31E8F8EFD8E9DCBCBCBCE3E3E3FFFFFF0000FFFFFF1F6F20B6CDB882A285FDFF
      FEB9DDBB7CBF816BB7706BB7705FAE681172350B6E3157A667DCF2E0EEFBF3B8
      B8B8E1E1E1FFFFFF0000FFFFFF217221BAD1BB3B733FFEFFFEFDFFFEBEE0C06E
      B9736AB66F1F7D3E0B6E315EA37661AF699BCEA2F4FFF8BABBBBDFDFDFFFFFFF
      0000FFFFFF247523BBD2BC1C5E21EAEFEBFEFFFEFDFFFEDDEFDE4296570B6E31
      1A793BB7DDBCF8FFFAF7FFFAF6FFF9BEC0BFDDDDDDFFFFFF0000FFFFFF267825
      BBD2BC1D6022B6CBB7FFFFFFFEFFFEC1DBCC0E70330E703359A9646BB770A1D2
      A5F5FDF7F7FFFAC5C8C6DBDBDBFFFFFF0000FFFFFF287C26BBD2BC1E632388A9
      8AFFFFFFEEF5F10E70340B6E318EBDA0E1F0E26BB7706BB7708AC68EE9F7EBCD
      D1CEDBDBDBFFFFFF0000FFFFFF2B8027D0E3D173AB768AB38CFFFFFF3C8C5A2C
      834D5DA372FDFFFEFDFFFDFCFFFDFBFFFCFAFFFCF9FFFBDDE1DEE0E0E0FFFFFF
      0000FFFFFF2D832AD5E6D585BA877EB281FFFFFFFFFFFFFFFFFFFFFFFFFEFFFE
      FDFFFEFDFFFDFCFFFDFBFFFCFAFFFCEFF4F1E6E6E6FFFFFF0000FFFFFF3A8D36
      CFE4CE9ACA9C82BB84D1E4D1CADFCBCBE0CBCBE2CCBBDABCB3D6B4A2CFA495CA
      98D1E5D16DAB6AFEFFFEFFFFFFFFFFFF0000FFFFFF73AF6F99C697E5F2E690C8
      9390C89390C89390C89390C89390C89390C89390C89394CA97EBF7EB2D8D24FF
      FFFFFFFFFFFFFFFF0000FFFFFFD6E8D52B8724C0DDBFE8F5E9AEDAB09CD29F9C
      D29F9CD29F9CD29F9CD29F9CD29F97CC9AEBF7EB2D8D24FFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFAFD3AC328E2B93C590D7EBD6EBF7EBEBF7EBEBF7EBEBF7EB
      EBF7EBEBF7EBEBF7EBEBF7EB2D8D24FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFE3F0E277B57143993B2D8D242D8D242D8D242D8D242D8D242D8D242D8D
      242D8D242D8D24FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000}
    LookAndFeel.NativeStyle = True
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1280
    Height = 140
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 52
      Top = 24
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label2: TLabel
      Tag = 1
      Left = 13
      Top = 48
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Esp'#233'cie de T'#237'tulo'
    end
    object Label5: TLabel
      Tag = 1
      Left = 54
      Top = 72
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label8: TLabel
      Tag = 1
      Left = 27
      Top = 96
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero T'#237'tulo'
    end
    object Label3: TLabel
      Tag = 1
      Left = 236
      Top = 96
      Width = 102
      Height = 13
      Caption = 'Intervalo Vencimento'
    end
    object Label6: TLabel
      Tag = 1
      Left = 420
      Top = 96
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 40
      Top = 120
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero NF'
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 176
      Top = 16
      Width = 64
      Height = 21
      DataField = 'cSigla'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 248
      Top = 16
      Width = 649
      Height = 21
      Color = clWhite
      DataField = 'cNmEmpresa'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 176
      Top = 40
      Width = 649
      Height = 21
      DataField = 'cNmEspTit'
      DataSource = DataSource3
      TabOrder = 8
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 176
      Top = 64
      Width = 124
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = DataSource4
      TabOrder = 9
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 312
      Top = 64
      Width = 649
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource4
      TabOrder = 10
    end
    object MaskEdit1: TMaskEdit
      Left = 344
      Top = 88
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object MaskEdit2: TMaskEdit
      Left = 440
      Top = 88
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Tag = 1
      Left = 104
      Top = 16
      Width = 63
      Height = 21
      Enabled = False
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 11
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object MaskEdit5: TMaskEdit
      Left = 104
      Top = 40
      Width = 64
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit5Exit
      OnKeyDown = MaskEdit5KeyDown
    end
    object MaskEdit6: TMaskEdit
      Tag = 1
      Left = 104
      Top = 64
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
    end
    object Edit1: TEdit
      Left = 104
      Top = 88
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 2
    end
    object Edit2: TEdit
      Left = 104
      Top = 112
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 5
    end
  end
  object cxGrid1: TcxGrid [4]
    Left = 0
    Top = 169
    Width = 1280
    Height = 581
    Align = alClient
    TabOrder = 3
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource6
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Position = spFooter
          Column = cxGrid1DBTableView1nValTit
        end
        item
          Format = '#,##0.00'
          Position = spFooter
          Column = cxGrid1DBTableView1nSaldoTit
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
          Column = cxGrid1DBTableView1nValTit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
          Column = cxGrid1DBTableView1nSaldoTit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValLiq
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValJuro
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValAbatimento
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValDesconto
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'nCdTitulo'
        Width = 45
      end
      object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'cNrTit'
        Width = 74
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        Caption = 'Nr NF'
        DataBinding.FieldName = 'cNrNF'
        Width = 62
      end
      object cxGrid1DBTableView1iParcela: TcxGridDBColumn
        Caption = 'Parcela'
        DataBinding.FieldName = 'iParcela'
      end
      object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
        Caption = 'Esp'#233'cie de T'#237'tulo'
        DataBinding.FieldName = 'cNmEspTit'
        Width = 150
      end
      object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
        Caption = 'Vencimento'
        DataBinding.FieldName = 'dDtVenc'
        Width = 92
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 121
      end
      object cxGrid1DBTableView1nValTit: TcxGridDBColumn
        Caption = 'Valor T'#237'tulo'
        DataBinding.FieldName = 'nValTit'
        Width = 131
      end
      object cxGrid1DBTableView1nValLiq: TcxGridDBColumn
        Caption = 'Valor Liquidado'
        DataBinding.FieldName = 'nValLiq'
        Width = 131
      end
      object cxGrid1DBTableView1nValJuro: TcxGridDBColumn
        Caption = 'Juros'
        DataBinding.FieldName = 'nValJuro'
        Width = 89
      end
      object cxGrid1DBTableView1nValAbatimento: TcxGridDBColumn
        Caption = 'Abatimento'
        DataBinding.FieldName = 'nValAbatimento'
        Width = 101
      end
      object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
        Caption = 'Desconto'
        DataBinding.FieldName = 'nValDesconto'
        Width = 88
      end
      object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
        Caption = 'Saldo em Aberto'
        DataBinding.FieldName = 'nSaldoTit'
        Width = 131
      end
      object cxGrid1DBTableView1cSigla: TcxGridDBColumn
        Caption = 'Moeda'
        DataBinding.FieldName = 'cSigla'
      end
      object cxGrid1DBTableView1cSenso: TcxGridDBColumn
        Caption = 'Senso'
        DataBinding.FieldName = 'cSenso'
        Width = 54
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 936
    Top = 40
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nCdUnidadeNegocio')
    Left = 984
    Top = 40
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEspTit'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '   FROM ESPTIT'
      'WHERE nCdEspTit = :nCdEspTit')
    Left = 1024
    Top = 40
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 1064
    Top = 32
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Moeda'
      ' WHERE nCdMoeda = :nCdMoeda')
    Left = 1112
    Top = 32
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 1144
    Top = 40
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 1152
    Top = 48
  end
  object DataSource3: TDataSource
    DataSet = qryEspTit
    Left = 1160
    Top = 56
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 1168
    Top = 64
  end
  object DataSource5: TDataSource
    DataSet = qryMoeda
    Left = 1176
    Top = 72
  end
  object usp_Consulta: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONSULTA_TITULO_ABERTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEspTit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Size = 17
        Value = Null
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cSenso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@cNrNF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@cProvisao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 928
    Top = 136
    object usp_ConsultanCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulo|ID'
      FieldName = 'nCdTitulo'
    end
    object usp_ConsultacSiglaEmp: TStringField
      DisplayLabel = 'T'#237'tulo|Emp'
      FieldName = 'cSiglaEmp'
      FixedChar = True
      Size = 5
    end
    object usp_ConsultanCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulo|Nr. SP'
      FieldName = 'nCdSP'
    end
    object usp_ConsultacNrTit: TStringField
      DisplayLabel = 'T'#237'tulo|Nr T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object usp_ConsultaiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulo|Parc.'
      FieldName = 'iParcela'
    end
    object usp_ConsultadDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulo|Vencimento'
      FieldName = 'dDtVenc'
    end
    object usp_ConsultanValTit: TBCDField
      DisplayLabel = 'T'#237'tulo|Valor Total'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ConsultanSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulo|Saldo'
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ConsultacSenso: TStringField
      DisplayLabel = 'T'#237'tulo|Senso'
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object usp_ConsultanCdEspTit: TIntegerField
      DisplayLabel = 'Esp'#233'cie|C'#243'd'
      FieldName = 'nCdEspTit'
    end
    object usp_ConsultacNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulo|Esp'#233'cie de T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object usp_ConsultanCdTerceiro: TIntegerField
      DisplayLabel = 'Terceiro|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object usp_ConsultacNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulo|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object usp_ConsultacNrNF: TStringField
      DisplayLabel = 'T'#237'tulo|Nr NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object usp_ConsultanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object usp_ConsultacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object usp_ConsultadDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object usp_ConsultanCdProvisaoTit: TIntegerField
      DisplayLabel = 'Dados da Provis'#227'o|C'#243'digo'
      FieldName = 'nCdProvisaoTit'
    end
    object usp_ConsultadDtPagto: TDateTimeField
      DisplayLabel = 'Dados da Provis'#227'o|Data Pagamento'
      FieldName = 'dDtPagto'
    end
    object usp_ConsultanCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object usp_ConsultanValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object usp_ConsultanValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ConsultanValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ConsultanValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object DataSource6: TDataSource
    DataSet = usp_Consulta
    Left = 1096
    Top = 72
  end
end
