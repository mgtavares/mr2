unit fPrecoEspPedCom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmPrecoEspPedCom = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemPedido: TADOQuery;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdePed: TBCDField;
    dsItemPedido: TDataSource;
    qryAux: TADOQuery;
    qryItemPedidocFlgComplemento: TIntegerField;
    qryItemPedidonValUnitario: TBCDField;
    qryItemPedidonValUnitarioEsp: TFloatField;
    qryItemPedidonValTotalEsp: TFloatField;
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryItemPedidoBeforePost(DataSet: TDataSet);
    procedure qryItemPedidoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido : Integer ;
  end;

var
  frmPrecoEspPedCom: TfrmPrecoEspPedCom;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmPrecoEspPedCom.ToolButton2Click(Sender: TObject);
begin

  if (qryItemPedido.State <> dsBrowse) then
      qryItemPedido.Post ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE ItemPedido                                                                                    ');
  qryAux.SQL.Add('   SET nValUnitarioEsp = ISNULL((SELECT nValUnitarioEsp                                              ');
  qryAux.SQL.Add('                                   FROM ItemPedido ItemPedidoPai                                     ');
  qryAux.SQL.Add('                                  WHERE ItemPedidoPai.nCdItemPedido = ItemPedido.nCdItemPedidoPai),0)');
  qryAux.SQL.Add(' WHERE nCdPedido      = ' + IntToStr(nCdPedido)                                                       );
  qryAux.SQL.Add('   AND nCdTipoItemPed = 4                                                                            ');

  frmMenu.Connection.BeginTrans;

  try
      qryAux.ExecSQL;
  except
      qryAux.Close;
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;

  frmMenu.Connection.CommitTrans ;

  qryItemPedido.Close ;
  qryAux.Close ;

  inherited;
end;

procedure TfrmPrecoEspPedCom.FormShow(Sender: TObject);
begin
  inherited;

  qryItemPedidonValUnitarioEsp.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemPedidonValTotalEsp.DisplayFormat    := frmMenu.cMascaraCompras;

  PosicionaQuery(qryItemPedido, IntToStr(nCdPedido)) ;
end;

procedure TfrmPrecoEspPedCom.qryItemPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryItemPedidonValUnitarioEsp.Value < 0) then
  begin
      MensagemAlerta('O pre�o esperado n�o pode ser negativo.') ;
      abort ;
  end ;

  qryItemPedidocFlgComplemento.Value := 0 ;

  if (qryItemPedidonValUnitarioEsp.Value > 0) then
      qryItemPedidocFlgComplemento.Value := 1 ;
end;

procedure TfrmPrecoEspPedCom.qryItemPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryItemPedidonValTotalEsp.Value := qryItemPedidonValUnitarioEsp.Value * qryItemPedidonQtdePed.Value;
end;

end.
