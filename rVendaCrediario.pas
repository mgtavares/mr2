unit rVendaCrediario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP, ER2Excel;

type
  TrptVendaCrediario = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtTerceiro: TMaskEdit;
    edtCliente: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    RadioGroup4: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaCrediario: TrptVendaCrediario;

implementation

uses fMenu, fLookup_Padrao, rVendaCrediario_view;

{$R *.dfm}

procedure TrptVendaCrediario.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptVendaCrediario.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptVendaCrediario.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

end;

procedure TrptVendaCrediario.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaCrediario.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(180);

        If (nPK > 0) then
        begin

            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaCrediario.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID,iLinha : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        : TrptVendaCrediario_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  objRel := TrptVendaCrediario_view.Create(nil);

  try
      try
          objRel.SPREL_RELVENDA_CREDIARIO.Close;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@nCdLoja').Value            := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@nCdTerceiro').Value        := frmMenu.ConvInteiro(edtTerceiro.Text) ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@dDtInicial').Value         := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@dDtFinal').Value           := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@cFlgExibeRestricao').Value := RadioGroup3.ItemIndex ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@cFlgSoRestricao').Value    := RadioGroup2.ItemIndex ;
          objRel.SPREL_RELVENDA_CREDIARIO.Parameters.ParamByName('@cFlgTipoCrediario').Value  := RadioGroup1.ItemIndex ;
          objRel.SPREL_RELVENDA_CREDIARIO.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          cFiltro := '' ;

          if (DBEdit1.Text <> '') then
              cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + '/ Cliente: ' + trim(edtTerceiro.Text) + '-' + DbEdit2.Text ;

          cFiltro := cFiltro + ' / Per�odo Venda: ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text;

          if (RadioGroup1.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Sele��o: TODOS CREDI�RIOS ' ;

          if (RadioGroup1.ItemIndex = 1) then
              cFiltro := cFiltro + ' / Sele��o: CLIENTES NOVOS ' ;

          if (RadioGroup1.ItemIndex = 2) then
              cFiltro := cFiltro + ' / Sele��o: CLIENTES ANTIGOS ' ;

          if (RadioGroup1.ItemIndex = 3) then
              cFiltro := cFiltro + ' / Sele��o: RENEGOCIA��O ' ;

          if (RadioGroup2.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Somente credi�rio com restri��o liberada: SIM ' ;

          if (RadioGroup2.ItemIndex = 1) then
              cFiltro := cFiltro + ' / Somente credi�rio com restri��o liberada: N�O ' ;

          if (RadioGroup3.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Exibir Detalhe Restri��o: SIM ' ;

          if (RadioGroup3.ItemIndex = 1) then
              cFiltro := cFiltro + ' / Exibir Detalhe Restri��o: N�O ' ;

          objRel.lblFiltro1.Caption := cFiltro ;

          case (RadioGroup4.ItemIndex) of
              0 : begin
                      objRel.QuickRep1.PreviewModal;
                  end;
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A1'].Range('AG1');
                      ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A2'].Range('AG2');
                      ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A3'].Range('AG3');
                      ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A4'].Range('AG4');
                      ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A5'].Range('AG5');
                      ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A6'].Range('AG6');

                      ER2Excel.Celula['A1'].Congelar('A7');

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel.Celula['A1'].Text  := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel.Celula['A2'].Text  := 'Rel. Venda Credi�rio.';
                      ER2Excel.Celula['A4'].Text  := 'Loja Cadastro: ' + DBEdit1.Text;
                      ER2Excel.Celula['F4'].Text  := 'Per�odo: ' + edtDtInicial.Text + ' at� ' + edtDtFinal.Text;
                      ER2Excel.Celula['I4'].Text  := 'Cliente: ' + DBEdit2.Text;
                      ER2Excel.Celula['A6'].Text  := 'Dt. Venda';
                      ER2Excel.Celula['C6'].Text  := 'Loja';
                      ER2Excel.Celula['D6'].Text  := 'Cliente';
                      ER2Excel.Celula['I6'].Text  := 'Nr. Carnet';
                      ER2Excel.Celula['J6'].Text  := 'Parcela';
                      ER2Excel.Celula['K6'].Text  := 'Val. Carnet';
                      ER2Excel.Celula['L6'].Text  := 'Val. Entrada';
                      ER2Excel.Celula['M6'].Text  := 'Caixa';
                      ER2Excel.Celula['P6'].Text  := 'Reneg.';
                      ER2Excel.Celula['Q6'].Text  := 'Restri��o';
                      ER2Excel.Celula['U6'].Text  := 'Autoriz.';
                      ER2Excel.Celula['X6'].Text  := 'Justificativa';
                      ER2Excel.Celula['AB6'].Text := 'Vendedor';

                      iLinha := 7;

                      objRel.SPREL_RELVENDA_CREDIARIO.First;

                      while (not objRel.SPREL_RELVENDA_CREDIARIO.Eof) do
                      begin
                          ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOdDtVenda.Value;
                          ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnCdLoja.Value;
                          ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnCdTerceiro.Value;
                          ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcNmTerceiro.Value;
                          ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnCdCrediario.Value;
                          ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOiParcelas.Value;
                          ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnValCrediario.Value;
                          ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnValEntrada.Value;
                          ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOnCdConta.Value;
                          ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcFlgRenegociacao.Value;
                          ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcFlgRestricao.Value;
                          ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcNmTabTipoRestricaoVenda.Value;
                          ER2Excel.Celula['U' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcNmUsuario.Value;
                          ER2Excel.Celula['X' + IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcJustificativa.Value;
                          ER2Excel.Celula['AB'+ IntToStr(iLinha)].Text := objRel.SPREL_RELVENDA_CREDIARIOcNmVendedor.Value;

                          ER2Excel.Celula['K' + IntToStr(iLinha)].Mascara := '#.##0,00';
                          ER2Excel.Celula['L' + IntToStr(iLinha)].Mascara := '#.##0,00';

                          ER2Excel.Celula['K' + IntToStr(iLinha)].HorizontalAlign := haRight;
                          ER2Excel.Celula['L' + IntToStr(iLinha)].HorizontalAlign := haRight;

                          objRel.SPREL_RELVENDA_CREDIARIO.Next;

                          Inc(iLinha);
                      end;

                      ER2Excel.ExportXLS;
                      ER2Excel.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          Raise;
      end;
  finally
      FreeAndNil(objRel) ;
  end;
end;

initialization
    RegisterClass(TrptVendaCrediario) ;

end.
