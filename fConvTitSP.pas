unit fConvTitSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB;

type
  TfrmConvTitSP = class(TfrmProcesso_Padrao)
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Label5: TLabel;
    SP_CONVERTE_PROV_SP: TADOStoredProc;
    Edit2: TEdit;
    Label6: TLabel;
    Edit3: TEdit;
    Label7: TLabel;
    CheckBox1: TCheckBox;
    procedure MaskEdit5Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTitulo : integer ;
  end;

var
  frmConvTitSP: TfrmConvTitSP;

implementation

uses fMenu, rImpSP, fSP;

{$R *.dfm}

procedure TfrmConvTitSP.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  If (trim(MaskEdit5.Text) <> '') then
      MaskEdit5.text := formatcurr('##,##0.00',StrToFloat(StringReplace(MaskEdit5.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmConvTitSP.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmSP ;
begin
  inherited;

  if (Trim(MaskEdit3.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de vencimento.') ;
      MaskEdit3.SetFocus;
      exit ;
  end ;

  If (Trim(MaskEdit5.Text) = '0') or (Trim(MaskEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o valor do t�tulo.') ;
      MaskEdit5.SetFocus;
      exit ;
  end ;

  if (Edit3.Text = '') then
      Edit3.Text := Edit1.Text ;
      
  case MessageDlg('Confirma a gera��o da solicita��o de pagamento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
  
      SP_CONVERTE_PROV_SP.Close ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@nCdTitulo').Value       := nCdTitulo ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@cNrTit').Value          := Trim(Edit3.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@cSerieTit').Value       := Trim(Edit2.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@dDtEmissao').Value      := frmMenu.ConvData(MaskEdit1.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@dDtReceb').Value        := frmMenu.ConvData(MaskEdit2.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@dDtVencto').Value       := frmMenu.ConvData(MaskEdit3.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@nValTit').Value         := MaskEdit5.Text ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@cNrNF').Value           := Trim(Edit1.Text) ;
      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@cFlgDocCobranca').Value := 0 ;

      if (checkBox1.Checked) then
          SP_CONVERTE_PROV_SP.Parameters.ParamByName('@cFlgDocCobranca').Value := 1 ;

      SP_CONVERTE_PROV_SP.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
      SP_CONVERTE_PROV_SP.ExecProc ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Processamento OK. SP Gerada: ' + IntToStr(SP_CONVERTE_PROV_SP.Parameters.ParamByName('@nCdSP').Value)) ;

  objForm := TfrmSP.Create(Self) ;

  objForm.qryMaster.Parameters.ParamByName('nPk').Value        := (SP_CONVERTE_PROV_SP.Parameters.ParamByName('@nCdSP').Value) ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  objForm.qryMaster.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  objForm.qryMaster.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  objForm.qryMaster.Open;

  showForm( objForm , TRUE ) ;

  Close ;
end;

procedure TfrmConvTitSP.FormShow(Sender: TObject);
begin
  inherited;

  Edit1.Text        := '' ;
  Edit2.Text        := '' ;
  MaskEdit1.Text    := '' ;
  MaskEdit2.text    := '' ;
  MaskEdit3.Text    := '' ;
  MaskEdit5.Text    := '' ;
  Edit3.Text        := '' ;
  CheckBox1.Checked := False ;
end;

end.
