inherited frmServidorReplicacao: TfrmServidorReplicacao
  Left = 0
  Top = 0
  Width = 1152
  Height = 738
  Caption = 'Servidor'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1329
    Height = 661
  end
  inherited ToolBar1: TToolBar
    Width = 1329
    ButtonWidth = 68
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 68
    end
    inherited ToolButton2: TToolButton
      Left = 76
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = -8
    Top = 29
    Width = 1337
    Height = 435
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdServidor'
        Footers = <>
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'cNmServidor'
        Footers = <>
        Width = 195
      end
      item
        EditButtons = <>
        FieldName = 'cFlgTipoServidor'
        Footers = <>
        KeyList.Strings = (
          'CP'
          'LP'
          'LB')
        PickList.Strings = (
          'CENTRAL PRODU'#199#195'O'
          'LOJA PRODU'#199#195'O'
          'LOJA BACKUP')
        Width = 125
      end
      item
        EditButtons = <>
        FieldName = 'cIPPrimario'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'cIPSecundario'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'cNmDatabase'
        Footers = <>
        Width = 100
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgAtivoReplicacao'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'dDtUltReplicacao'
        Footers = <>
        ReadOnly = True
        Tag = 2
        Width = 166
      end
      item
        EditButtons = <>
        FieldName = 'iIntervalo'
        Footers = <>
        Width = 95
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMasterBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      
        '      ,CASE WHEN cFlgAtivoReplicacao = 1 THEN DateDiff(minute,dD' +
        'tUltReplicacao,GetDate())'
      '            ELSE NULL'
      '       END iIntervalo'
      'FROM Servidor'
      'ORDER BY nCdServidor')
    Left = 272
    Top = 224
    object qryMasternCdServidor: TIntegerField
      DisplayLabel = 'Lista de Servidores|C'#243'd'
      FieldName = 'nCdServidor'
    end
    object qryMastercNmServidor: TStringField
      DisplayLabel = 'Lista de Servidores|Nome Servidor'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryMastercFlgTipoServidor: TStringField
      DisplayLabel = 'Lista de Servidores|Tipo Servidor'
      FieldName = 'cFlgTipoServidor'
      FixedChar = True
      Size = 2
    end
    object qryMastercIPPrimario: TStringField
      DisplayLabel = 'Lista de Servidores|IP Prim'#225'rio'
      FieldName = 'cIPPrimario'
      Size = 50
    end
    object qryMastercIPSecundario: TStringField
      DisplayLabel = 'Lista de Servidores|IP Secundario'
      FieldName = 'cIPSecundario'
      Size = 50
    end
    object qryMastercNmDatabase: TStringField
      DisplayLabel = 'Lista de Servidores|Database Name'
      FieldName = 'cNmDatabase'
      Size = 50
    end
    object qryMastercFlgAtivoReplicacao: TIntegerField
      DisplayLabel = 'Lista de Servidores|Ativo'
      FieldName = 'cFlgAtivoReplicacao'
    end
    object qryMasterdDtUltReplicacao: TDateTimeField
      DisplayLabel = 'Lista de Servidores|Dt. '#218'lt. Replica'#231#227'o Recebida'
      FieldName = 'dDtUltReplicacao'
    end
    object qryMasteriIntervalo: TIntegerField
      DisplayLabel = 'Lista de Servidores| Intervalo '#218'lt. Receb. (minutos)'
      FieldName = 'iIntervalo'
      ReadOnly = True
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 312
    Top = 232
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 168
    object estarConexoIPPrimrio1: TMenuItem
      Caption = 'Testar Conex'#227'o - IP Prim'#225'rio'
      OnClick = estarConexoIPPrimrio1Click
    end
    object estarConexo1: TMenuItem
      Caption = 'Testar Conex'#227'o - IP Secund'#225'rio'
      OnClick = estarConexo1Click
    end
    object FiladeTransmisso1: TMenuItem
      Caption = 'Fila de Transmiss'#227'o'
      OnClick = FiladeTransmisso1Click
    end
    object LogdeErro1: TMenuItem
      Caption = 'Registros Recebidos'
      OnClick = LogdeErro1Click
    end
  end
  object Connection: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=admsoft101580;Persist Security Info=' +
      'True;User ID=ADMSoft;Extended Properties="driver={SQL Server};se' +
      'rver=er2-note-edgar;password=admsoft101580;uid=ADMSoft;database=' +
      'ADMSoft";Initial Catalog=ADMSoft'
    ConnectionTimeout = 10
    IsolationLevel = ilReadUncommitted
    LoginPrompt = False
    Mode = cmReadWrite
    Provider = 'MSDASQL.1'
    Left = 368
    Top = 288
  end
end
