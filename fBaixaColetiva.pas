unit fBaixaColetiva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons,
  GridsEh, DBGridEh, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, cxPC, DBGridEhGrouping;

type
  TfrmBaixaColetiva = class(TfrmCadastro_Padrao)
    qryMasternCdLoteLiq: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercTipoLiq: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternValDinheiro: TBCDField;
    qryMasternValChTerceiro: TBCDField;
    qryMasternValRecursoProp: TBCDField;
    qryMasternValCredAnterior: TBCDField;
    qryMasternValTotalLote: TBCDField;
    qryMasternValTitLiq: TBCDField;
    qryMasternValCredTerceiro: TBCDField;
    qryMasternSaldoLote: TBCDField;
    qryMasterdDtFecham: TDateTimeField;
    qryMasternCdUsuarioFecham: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacNmContaBancaria: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryMastercSiglaEmp: TStringField;
    qryMastercNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    qryChequeLote: TADOQuery;
    qryChequeLotenCdChequeLoteLiq: TAutoIncField;
    qryChequeLotenCdLoteLiq: TIntegerField;
    qryChequeLotenCdBanco: TIntegerField;
    qryChequeLotecAgencia: TStringField;
    qryChequeLotecConta: TStringField;
    qryChequeLotecDigito: TStringField;
    qryChequeLotecCNPJCPF: TStringField;
    qryChequeLotenValCheque: TBCDField;
    qryChequeLotedDtDeposito: TDateTimeField;
    dsChequeLote: TDataSource;
    qryRecurso: TADOQuery;
    qryRecursonCdRecursoLoteLiq: TAutoIncField;
    qryRecursonCdLoteLiq: TIntegerField;
    qryRecursonCdContaBancaria: TIntegerField;
    qryRecursonCdFormaPagto: TIntegerField;
    qryRecursoiNrCheque: TIntegerField;
    qryRecursonValRecurso: TBCDField;
    dsRecurso: TDataSource;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryTituloLoteLiq: TADOQuery;
    dsTitulos: TDataSource;
    qryChequeLoteiNrCheque: TIntegerField;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulonCdSP: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTituloiParcela: TIntegerField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulocNmTerceiro: TStringField;
    qryTituloLoteLiqnCdTituloLoteLiq: TAutoIncField;
    qryTituloLoteLiqnCdLoteLiq: TIntegerField;
    qryTituloLoteLiqnCdTitulo: TIntegerField;
    qryTituloLoteLiqnValLiq: TBCDField;
    qryTituloLoteLiqcNrTit: TStringField;
    qryTituloLoteLiqiParcela: TIntegerField;
    qryTituloLoteLiqcNmTerceiro: TStringField;
    qryTituloLoteLiqdDtVenc: TDateField;
    qryVerificaDup: TADOQuery;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    usp_Processa_LoteLiq: TADOStoredProc;
    qryTitulonCdEmpresa: TIntegerField;
    qryLocalizaCheque: TADOQuery;
    qryLocalizaChequecCNPJCPF: TStringField;
    qryLocalizaChequenValCheque: TBCDField;
    qryLocalizaChequenCdTerceiroPort: TIntegerField;
    qryLocalizaChequenCdEmpresa: TIntegerField;
    qryLocalizaChequedDtDeposito: TDateTimeField;
    qryContaBancariacFlgDeposito: TIntegerField;
    Label17: TLabel;
    usp_Sugerir_Titulo: TADOStoredProc;
    qryRecursodDtBomPara: TDateTimeField;
    qryLocalizaConta: TADOQuery;
    qryLocalizaContacCNPJCPF: TStringField;
    DataSource1: TDataSource;
    DBEdit22: TDBEdit;
    DBEdit18: TDBEdit;
    DataSource2: TDataSource;
    qryContaBancariaRecurso: TADOQuery;
    qryContaBancariaRecursonCdContaBancaria: TIntegerField;
    qryContaBancariaRecursocNmContaBancaria: TStringField;
    qryContaBancariaRecursocFlgDeposito: TIntegerField;
    qryRecursocNmContaBancaria: TStringField;
    qryFormaPagtonCdTabTipoFormaPagto: TIntegerField;
    qryRecursocNmFormaPagto: TStringField;
    ToolButton12: TToolButton;
    qryChequeLotenValTaxaAdm: TBCDField;
    qryChequeLoteiDiasVenc: TIntegerField;
    qryTituloLoteLiqnValJuro: TBCDField;
    qryTituloLoteLiqnSaldoTit: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh3: TDBGridEh;
    DBGridEh2: TDBGridEh;
    btMaqCheque: TcxButton;
    cxButton1: TcxButton;
    btSugerirCheques: TcxButton;
    btSugerirTitulos: TcxButton;
    Panel1: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Edit1: TEdit;
    edtCPF: TEdit;
    edtDtDeposito: TMaskEdit;
    edtValorCheque: TcxCurrencyEdit;
    cxButton2: TcxButton;
    btAddTitulo: TcxButton;
    qryMasternCdProvisaoTit: TIntegerField;
    qryMasternValTaxaAdm: TBCDField;
    qryMasterdDtLoteLiq: TDateTimeField;
    Label22: TLabel;
    DBEdit19: TDBEdit;
    qryTitulocNmEspTit: TStringField;
    qryTitulonValJuro: TBCDField;
    qryTituloLoteLiqcNmEspTit: TStringField;
    qryTitulonValDesconto: TBCDField;
    qryTituloLoteLiqnValDesconto: TBCDField;
    cxButton3: TcxButton;
    qryConfereBanco: TADOQuery;
    qryConfereBancocNmBanco: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryChequeLoteBeforePost(DataSet: TDataSet);
    procedure qryChequeLoteAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryChequeLoteAfterDelete(DataSet: TDataSet);
    procedure qryChequeLoteBeforeDelete(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryRecursoBeforePost(DataSet: TDataSet);
    procedure qryRecursoAfterPost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryRecursoBeforeDelete(DataSet: TDataSet);
    procedure qryRecursoAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryTituloLoteLiqBeforePost(DataSet: TDataSet);
    procedure qryTituloLoteLiqCalcFields(DataSet: TDataSet);
    procedure qryTituloLoteLiqAfterDelete(DataSet: TDataSet);
    procedure qryTituloLoteLiqBeforeDelete(DataSet: TDataSet);
    procedure btSugerirTitulosClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btSugerirChequesClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBGridEh3Enter(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure edtDtDepositoExit(Sender: TObject);
    procedure btMaqChequeClick(Sender: TObject);
    procedure edtValorChequeKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton2Click(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryRecursoCalcFields(DataSet: TDataSet);
    procedure ToolButton12Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btAddTituloClick(Sender: TObject);
    function calculaTaxaAdm(nValCheque:double; iDias:integer):double;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCPFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorChequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDtDepositoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    nCdBancoAux    : integer ;
    cAgenciaAux    : string ;
    cContaAux      : string ;
    cDVAux         : string ;
    iNrChequeAux   : integer ;
  public
    { Public declarations }
    nCdProvisaoTit : integer ;
  end;

var
  frmBaixaColetiva: TfrmBaixaColetiva;
  nPercTaxaAdm: double ;
  iDiasCarenciaCheque: integer;

implementation

uses fMenu, fLookup_Padrao, fSugerirCheques, rBaixaColetiva_view, DateUtils,
  fBaixaColetiva_ConsTitulo;

{$R *.dfm}

procedure TfrmBaixaColetiva.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LOTELIQ' ;
  nCdTabelaSistema  := 24 ;
  nCdConsultaPadrao := 116 ;
  bLimpaAposSalvar  := false ;
end;

procedure TfrmBaixaColetiva.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercTipoLiq.Value <> 'R') and (qryMastercTipoLiq.Value <> 'P') then
  begin
      ShowMessage('Tipo de lote inv�lido. Informe (R) para recebimento ou (P) para pagamento') ;
      DbEdit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoLiq.Value = 'R') and (qryMasternCdTerceiro.Value = 0) then
  begin
      ShowMessage('Para lote de recebimentos, � obrigat�rio informar o terceiro pagador.') ;
      DBEdit4.SetFocus ;
      Abort ;
  end ;

  if (qryMastercTipoLiq.Value = 'R') and (qryMasternCdContaBancaria.Value = 0) then
  begin
      ShowMessage('Informe a conta banc�ria de recebimento dos recursos.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (qryMastercTipoLiq.Value = 'D') and (qryMasternCdContaBancaria.Value = 0) then
  begin
      ShowMessage('Informe a conta banc�ria de recebimento dos recursos.') ;
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoLiq.Value = 'D') and (qryContaBancariacFlgDeposito.Value = 0) then
  begin
      ShowMessage('Esta conta banc�ria n�o permite dep�sitos.') ;
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoLiq.Value = 'P') and (qryMasternValDinheiro.Value > 0) then
  begin
      ShowMessage('Em lotes de pagamentos, o valor pago em dinheiro deve ser lan�ando na parte de recurso pr�rio.') ;
      qryMasternValDinheiro.Value := 0 ;
  end ;

  qryMasternValTotalLote.Value := qryMasternValDinheiro.Value  + qryMasternValChTerceiro.Value   + qryMasternValRecursoProp.Value  ;
  qryMasternValTotalLote.Value := qryMasternValTotalLote.Value + qryMasternValCredAnterior.Value - qryMasternValTaxaAdm.Value      ;
  qryMasternSaldoLote.Value    := qryMasternValTotalLote.Value - qryMasternValTitLiq.Value       + qryMasternValCredTerceiro.Value ;

  inherited;

end;

procedure TfrmBaixaColetiva.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Open ;

  nPercTaxaAdm        := StrTofloat(frmMenu.LeParametro('TAXAADMCHQTERC')) ;
  iDiasCarenciaCheque := StrToInt(frmMenu.LeParametro('DIASCARADMCHQ')) ;

  cxPageControl1.ActivePageIndex := 0 ;

  DBGridEh3.ReadOnly := False ;

  if (nCdProvisaoTit > 0) then
  begin
      DBGridEh1.SetFocus ;
      DBGridEh3.ReadOnly := True ;
  end ;

end;

procedure TfrmBaixaColetiva.qryChequeLoteBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  if (qryChequeLotenCdBanco.Value <= 0) then
  begin
      MensagemAlerta('Informe o c�digo do banco.') ;
      abort ;
  end ;

  qryConfereBanco.Close;
  PosicionaQuery(qryConfereBanco, qryChequeLotenCdBanco.AsString) ;

  if qryConfereBanco.eof then
  begin
      MensagemAlerta('C�digo de banco inv�lido ou n�o cadastrado.') ;
      abort ;
  end ;

  if (trim(qryChequeLotecAgencia.Value) = '') then
  begin
      MensagemAlerta('Informe o c�digo da ag�ncia.') ;
      abort ;
  end ;

  if (trim(qryChequeLotecConta.Value) = '') then
  begin
      MensagemAlerta('Informe o n�mero da conta.') ;
      abort ;
  end ;

  if (trim(qryChequeLotecDigito.Value) = '') then
  begin
      MensagemAlerta('Informe o d�gito da conta.') ;
      abort ;
  end ;

  if (qryChequeLoteiNrCheque.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero do cheque.') ;
      abort ;
  end ;

  qryChequeLotenCdLoteLiq.Value := qryMasternCdLoteLiq.Value ;
  qryChequeLotecAgencia.Value   := FormatFloat('0000',StrToInt(qryChequeLotecAgencia.Value)) ;
  //qryChequeLotecConta.Value     := FormatFloat('000000000000000',StrToInt(qryChequeLotecConta.Value)) ;

  qryVerificaDup.Close ;
  qryVerificaDup.SQL.Clear ;
  qryVerificaDup.SQL.Add('SELECT 1 FROM ChequeLoteLiq WHERE nCdLoteLiq = ' + qryMasternCdLoteLiq.AsString) ;
  qryVerificaDup.SQL.Add(' AND nCdBanco  = ' + qryChequeLotenCdBanco.asString) ;
  qryVerificaDup.SQL.Add(' AND cAgencia  = ' + Chr(39) + qryChequeLotecAgencia.asString + Chr(39)) ;
  qryVerificaDup.SQL.Add(' AND cConta    = ' + Chr(39) + qryChequeLotecConta.asString   + Chr(39)) ;
  qryVerificaDup.SQL.Add(' AND cDigito   = ' + Chr(39) + qryChequeLotecDigito.asString   + Chr(39)) ;
  qryVerificaDup.SQL.Add(' AND iNrCheque = ' + qryChequeLoteiNrCheque.asString) ;

  if (qryChequeLotenCdChequeLoteLiq.Value > 0) then
  begin
      qryVerificaDup.SQL.Add(' AND nCdChequeLoteLiq <> ' + qryChequeLotenCdChequeLoteLiq.asString) ;
  end ;

  qryVerificaDup.Open ;

  if not qryVerificaDup.Eof then
  begin
      ShowMessage('Este cheque j� est� lan�ado neste lote.') ;
      abort ;
  end ;

  If (qryMastercTipoLiq.Value = 'P') or (qryMastercTipoLiq.Value = 'D') then
  begin

      qryLocalizaCheque.Close ;
      qryLocalizaCheque.Parameters.ParamByName('nCdBanco').Value  := qryChequeLotenCdBanco.Value ;
      qryLocalizaCheque.Parameters.ParamByName('cAgencia').Value  := qryChequeLotecAgencia.Value ;
      qryLocalizaCheque.Parameters.ParamByName('cConta').Value    := qryChequeLotecConta.Value ;
      qryLocalizaCheque.Parameters.ParamByName('cDigito').Value   := qryChequeLotecDigito.Value ;
      qryLocalizaCheque.Parameters.ParamByName('iNrCheque').Value := qryChequeLoteiNrCheque.Value ;
      qryLocalizaCheque.Open ;

      if qryLocalizaCheque.Eof then
      begin
          ShowMessage('Este cheque n�o existe no arquivo de cheques de terceiros.') ;
          abort ;
      end ;

      if (qryLocalizaChequenCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
      begin
          ShowMessage('Este cheque n�o pertence a esta empresa.') ;
          abort ;
      end ;

      if (qryLocalizaChequenCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
      begin
          ShowMessage('Este cheque j� foi enviado para outro portador e n�o pode ser utilizado.') ;
          abort ;
      end ;

      qryChequeLotecCNPJCPF.Value    := qryLocalizaChequecCNPJCPF.Value    ;
      qryChequeLotenValCheque.Value  := qryLocalizaChequenValCheque.Value  ;
      qryChequeLotedDtDeposito.Value := qryLocalizaChequedDtDeposito.Value ;

  end ;

  qryChequeLotecCNPJCPF.Value := Uppercase(qryChequeLotecCNPJCPF.Value) ;
  
  if (TRIM(qryChequeLotecCNPJCPF.Value) <> 'ISENTO') then
  begin
      if (frmMenu.TestaCpfCgc(Trim(qryChequeLotecCNPJCPF.Value)) = '') then
      begin
          abort;
      end ;
  end ;

  if (qryChequeLotenValCheque.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do cheque.') ;
      abort ;
  end ;

  if (qryChequeLotedDtDeposito.Value < Date) and (qryMastercTipoLiq.Value = 'R') then
  begin
      MensagemAlerta('Data de dep�sito n�o pode ser menor que hoje.') ;
      abort ;
  end ;

  if (qryChequeLote.State = dsInsert) then
      qryChequeLotenCdChequeLoteLiq.Value := frmMenu.fnProximoCodigo('CHEQUELOTELIQ') ;

  if (qryChequeLote.State = dsEdit) then
  begin
      qryMasternValChTerceiro.Value := qryMasternValChTerceiro.Value - StrToFloat(qryChequeLotenValCheque.OldValue) ;
      qryMasternValTaxaAdm.Value    := qryMasternValTaxaAdm.Value    - StrToFloat(qryChequeLotenValTaxaAdm.OldValue) ;
  end ;

  if (qryChequeLotedDtDeposito.Value > Date) then
      qryChequeLoteiDiasVenc.Value := DaysBetween(Date,qryChequeLotedDtDeposito.Value)
  else qryChequeLoteiDiasVenc.Value := 0 ;

  qryChequeLotenValTaxaAdm.Value := 0 ;

  if (qryChequeLoteiDiasVenc.Value > 0) then
  begin
      qryChequeLotenValTaxaAdm.Value := calculaTaxaAdm(qryChequeLotenValCheque.Value , qryChequeLoteiDiasVenc.Value) ;
  end ;

  inherited;

end;

procedure TfrmBaixaColetiva.qryChequeLoteAfterPost(DataSet: TDataSet);
begin
  qryMasternValChTerceiro.Value := qryMasternValChTerceiro.Value + qryChequeLotenValCheque.Value ;
  qryMasternValTaxaAdm.Value    := qryMasternValTaxaAdm.Value    + qryChequeLotenValTaxaAdm.Value ;
  qryMaster.Post ;
  inherited;
end;

procedure TfrmBaixaColetiva.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryChequeLote.Close ;
  qryChequeLote.Parameters.ParamByName('nCdLoteLiq').Value := qryMasternCdLoteLiq.Value ;
  qryChequeLote.Open ;

  qryRecurso.Close ;
  qryRecurso.Parameters.ParamByName('nCdLoteLiq').Value := qryMasternCdLoteLiq.Value ;
  qryRecurso.Open ;

  qryTituloLoteLiq.Close ;
  qryTituloLoteLiq.Parameters.ParamByName('nCdLoteLiq').Value := qryMasternCdLoteLiq.Value ;
  qryTituloLoteLiq.Open ;

  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;
  DBGridEh3.ReadOnly := False ;

  if (qryMastercTipoLiq.Value = 'R') then
      DBGridEh2.ReadOnly := True
  else begin
      DBGridEh2.ReadOnly := False ;
  end;

  if (qryMastercTipoLiq.Value = 'D') then
  begin
      DBGridEh2.ReadOnly := True ;
      DBGridEh3.ReadOnly := True ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      DBGridEh1.ReadOnly := True ;
      DBGridEh2.ReadOnly := True ;
      DBGridEh3.ReadOnly := True ;
  end ;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancaria.AsString) ;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmBaixaColetiva.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryChequeLote.Close ;
  qryRecurso.Close ;
  qryTituloLoteLiq.Close ;
  qryTerceiro.Close ;
  qryContaBancaria.Close ;

end;

procedure TfrmBaixaColetiva.qryChequeLoteAfterDelete(DataSet: TDataSet);
begin
  qryMaster.Post ;
  inherited;

end;

procedure TfrmBaixaColetiva.qryChequeLoteBeforeDelete(DataSet: TDataSet);
begin
  qryMasternValChTerceiro.Value := qryMasternValChTerceiro.Value - qryChequeLotenValCheque.Value ;
  qryMasternValTaxaAdm.Value    := qryMasternValTaxaAdm.Value    - qryChequeLotenValTaxaAdm.Value ;

  inherited;

end;

procedure TfrmBaixaColetiva.btIncluirClick(Sender: TObject);
var
  iAux : Integer ;
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
  iAux := frmMenu.nCdEmpresaAtiva;

  qryMasterdDtLoteLiq.Value := Date ;
  
  if (iAux > 0) then
  begin
      qryMasternCdEmpresa.Value := iAux ;
      DbEdit3.SetFocus ;
  end
  else begin
      DbEdit2.SetFocus ;
  end ;


end;

procedure TfrmBaixaColetiva.qryRecursoBeforePost(DataSet: TDataSet);
begin

  if (qryRecursonCdContaBancaria.Value = 0) or (qryRecursocNmContaBancaria.Value = '') then
  begin
      MensagemAlerta('Informe a conta banc�ria.') ;
      abort ;
  end ;

  if (qryRecursonCdFormaPagto.Value = 0) or (qryRecursocNmFormaPagto.Value = '') then
  begin
      MensagemAlerta('Informe a Forma de Liquida��o.') ;
      abort ;
  end ;

  if (qryRecursonCdFormaPagto.Value = 3) and (qryRecursoiNrCheque.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero do cheque.') ;
      abort ;
  end ;

  if (qryRecursonCdFormaPagto.Value = 3) and (qryRecursodDtBomPara.AsString = '') then
  begin
      MensagemAlerta('Informa data de dep�sito do cheque.') ;
      abort
  end ;

  if (qryRecursonValRecurso.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do pagamento.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryRecursonCdLoteLiq.Value := qryMasternCdLoteLiq.Value ;

  if (qryRecurso.State = dsEdit) then
  begin
      qryMasternValRecursoProp.Value := qryMasternValRecursoProp.Value - StrToFloat(qryRecursonValRecurso.OldValue) ;
  end ;

  if not qryFormaPagto.eof and (qryFormaPagtonCdTabTipoFormaPagto.Value <> 2) and (qryRecurso.State <> dsBrowse) then
  begin
      qryRecursoiNrCheque.Value     := 0 ;
      qryRecursodDtBomPara.asString := '' ;
  end ;

  if (qryRecurso.State = dsInsert) then
      qryRecursonCdRecursoLoteLiq.Value := frmMenu.fnProximoCodigo('RECURSOLOTELIQ') ;
      
  inherited;

end;

procedure TfrmBaixaColetiva.qryRecursoAfterPost(DataSet: TDataSet);
begin
  qryMasternValRecursoProp.Value := qryMasternValRecursoProp.Value + qryRecursonValRecurso.Value ;
  qryMaster.Post ;

  inherited;

end;

procedure TfrmBaixaColetiva.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh2.Col = 3) then
        begin

            if (qryRecurso.State = dsBrowse) then
                qryRecurso.Edit ;


            if (qryRecurso.State = dsInsert) or (qryRecurso.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(39) ;

                If (nPK > 0) then
                begin
                    qryRecursonCdContaBancaria.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh2.Col = 5) then
        begin

            if (qryRecurso.State = dsBrowse) then
                qryRecurso.Edit ;


            if (qryRecurso.State = dsInsert) or (qryRecurso.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(15) ;

                If (nPK > 0) then
                begin
                    qryRecursonCdFormaPagto.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBaixaColetiva.qryRecursoBeforeDelete(DataSet: TDataSet);
begin
  qryMasternValRecursoProp.Value := qryMasternValRecursoProp.Value - qryRecursonValRecurso.Value ;
  inherited;

end;

procedure TfrmBaixaColetiva.qryRecursoAfterDelete(DataSet: TDataSet);
begin
  qryMaster.Post ;
  inherited;

end;

procedure TfrmBaixaColetiva.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

  DBGridEh1.Columns[7].ReadOnly := False ;
  DBGridEh1.Columns[8].ReadOnly := False ;
  DBGridEh1.Columns[9].ReadOnly := False ;

  if (DbEdit3.Text = 'P') or (DbEdit3.Text = 'D') then
  begin
      DBGridEh1.Columns[7].ReadOnly := True ;
      DBGridEh1.Columns[8].ReadOnly := True ;
      DBGridEh1.Columns[9].ReadOnly := True ;
  end ;

end;

procedure TfrmBaixaColetiva.qryTituloLoteLiqBeforePost(DataSet: TDataSet);
begin

  if (qryTituloLoteLiqiParcela.Value = 0) then
  begin
      MensagemAlerta('Selecione um t�tulo v�lido.') ;
      abort ;
      
  end ;
  if (qryTituloLoteLiqnValLiq.Value > qryTituloLoteLiqnSaldoTit.Value) then
  begin
      qryTituloLoteLiqnValLiq.Value := qryTituloLoteLiqnSaldoTit.Value ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryTituloLoteLiqnCdLoteLiq.Value := qryMasternCdLoteLiq.Value ;

  qryVerificaDup.Close ;
  qryVerificaDup.SQL.Clear;

  if (qryTituloLoteLiqnCdTituloLoteLiq.AsString <> '') then
      qryVerificaDup.SQL.Add('SELECT 1 FROM TituloLoteLiq WHERE nCdLoteLiq = ' + qryMasternCdLoteLiq.asString + ' AND nCdTitulo = ' + qryTituloLoteLiqnCdTitulo.AsString + ' AND nCdTituloLoteLiq <> ' + String(qryTituloLoteLiqnCdTituloLoteLiq.AsInteger))
  else begin
      qryVerificaDup.SQL.Add('SELECT 1 FROM TituloLoteLiq WHERE nCdLoteLiq = ' + qryMasternCdLoteLiq.asString + ' AND nCdTitulo = ' + qryTituloLoteLiqnCdTitulo.AsString) ;
  end ;

  qryVerificaDup.Open ;

  if not qryVerificaDup.Eof then
  begin
      qryVerificaDup.Close ;
      MensagemAlerta('T�tulo j� registrado neste lote. Opera��o cancelada.') ;
      Abort ;
  end ;

  qryVerificaDup.Close ;

  if (qryTituloLoteLiq.State = dsEdit) then
  begin
      qryMasternValTitLiq.Value := qryMasternValTitLiq.Value - StrToFloat(qryTituloLoteLiqnValLiq.OldValue) ;
  end ;

  inherited;

  if (qryTituloLoteLiqnValLiq.Value <= 0) then
  begin
      MensagemAlerta('Informe o Valor Liquidado.') ;
      Abort ;
  end;

  if (qryTituloLoteLiq.State = dsInsert) then
    qryTituloLoteLiqnCdTituloLoteLiq.Value := frmMenu.fnProximoCodigo('TITULOLOTELIQ') ;

  qryMasternValTitLiq.Value := qryMasternValTitLiq.Value + qryTituloLoteLiqnValLiq.Value ;

  try
      qryMaster.Post ;
  except
      abort ;
  end ;

end;

procedure TfrmBaixaColetiva.qryTituloLoteLiqCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryTitulo.Close;
  qryTitulo.Parameters.ParamByName('nPK').Value := qryTituloLoteLiqnCdTitulo.Value ;

  if (qryMastercTipoLiq.Value = 'R') then
      qryTitulo.Parameters.ParamByName('cSenso').Value := 'C' ;

  if (qryMastercTipoLiq.Value = 'P') then
      qryTitulo.Parameters.ParamByName('cSenso').Value := 'D' ;

  qryTitulo.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryTitulo.Parameters.ParamByName('dDtMov').Value     := qryMasterdDtLoteLiq.AsString;

  qryTitulo.Open ;

  qryTituloLoteLiqcNrTit.Value      := qryTitulocNrTit.Value ;
  qryTituloLoteLiqiParcela.Value    := qryTituloiParcela.Value ;
  qryTituloLoteLiqcNmTerceiro.Value := qryTitulocNmTerceiro.Value ;
  qryTituloLoteLiqdDtVenc.Value     := qryTitulodDtVenc.Value ;
  qryTituloLoteLiqcNmEspTit.Value   := qryTitulocNmEspTit.Value ;

end;

procedure TfrmBaixaColetiva.qryTituloLoteLiqAfterDelete(DataSet: TDataSet);
begin

  try
      qryMaster.Post ;
  except
      abort ;
  end ;

  inherited;

end;

procedure TfrmBaixaColetiva.qryTituloLoteLiqBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValTitLiq.Value := qryMasternValTitLiq.Value - qryTituloLoteLiqnValLiq.Value ;

end;

procedure TfrmBaixaColetiva.btSugerirTitulosClick(Sender: TObject);
var
  cChequeDev : string ;
begin
  inherited;
  
  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

  if not qryMaster.Active or (qryMasternCdLoteLiq.Value = 0) then
  begin
      MensagemAlerta('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;
  
  if (qryMasternCdTerceiro.asString = '') then
  begin
      MensagemAlerta('S� � poss�vel utilizar este recurso para lotes quem tenham o terceiro informado.') ;
      exit ;
  end ;

  if (qryMasternSaldoLote.Value <= 0) then
  begin
      MensagemAlerta('Imposs�vel sugerir t�tulos em lote sem saldo dispon�vel.') ;
      exit ;
  end ;

  if (qryTituloLoteLiq.Active) and (qryTituloLoteLiq.Recordcount > 0) then
  begin
      MensagemAlerta('Exclua os t�tulos antes de utilizar esta fun��o.') ;
      exit ;
  end ;

  case MessageDlg('Os t�tulos ser�o priorizados por data de vencimento. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo: exit
  end;

  cChequeDev := 'N' ;

  case MessageDlg('Incluir cheques devolvidos pendentes na lista de t�tulos selecionados ?',mtConfirmation,[mbYes,mbNo],0) of
    mrYes: cChequeDev := 'S' ;
  end;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugerir_Titulo.Close ;
      usp_Sugerir_Titulo.Parameters.ParamByName('@nCdLoteLiq').Value  := qryMasternCdLoteLiq.Value ;
      usp_Sugerir_Titulo.Parameters.ParamByName('@nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      usp_Sugerir_Titulo.Parameters.ParamByName('@cTipoLiq').Value    := qryMastercTipoLiq.Value ;
      usp_Sugerir_Titulo.Parameters.ParamByName('@cChequeDev').Value  := cChequeDev ;
      usp_Sugerir_Titulo.Parameters.ParamByName('@dDtMov').Value      := qryMasterdDtLoteLiq.AsString;
      usp_Sugerir_Titulo.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdLoteLiq.asString) ;

end;

procedure TfrmBaixaColetiva.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active or (qryMasternCdLoteLiq.Value = 0) then
  begin
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  if (qryMaster.State = dsEdit) or (qryMaster.State = dsInsert) then
  begin

      try
          qryMaster.Post ;
      except
          abort ;
      end ;

  end ;

  if (qryMastercTipoLiq.Value = 'P') and (qryMasternSaldoLote.Value > 0) then
  begin
      if (frmMenu.LeParametro('FINLOTPAGDIF') <> 'S') then
      begin
          MensagemAlerta('Lote de pagamento n�o pode ter saldo residual. O Campo Saldo Pendente deve ficar zerado.') ;
          exit ;
      end ;
  end ;

  if (qryMasternSaldoLote.Value < 0) then
  begin
      MensagemAlerta('O saldo do lote n�o pode ser menor que zero.') ;
      exit ;
  end ;

  case MessageDlg('O Lote ser� encerrado e n�o ser� permitida altera��o ap�s. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo: exit
  end;

  if (qryMasternSaldoLote.Value > 0) and (qryMastercTipoLiq.Value = 'R') then
  begin
      case MessageDlg('O Res�duo do lote ser� creditado na ficha financeira do terceiro. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: exit
      end;
  end ;

  if (qryMasternValTitLiq.Value = 0) and (qryMastercTipoLiq.Value = 'P') then
  begin
      MensagemAlerta('N�o � poss�vel processar o lote sem nenhum t�tulo vinculado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Processa_LoteLiq.Close ;
      usp_Processa_LoteLiq.Parameters.ParamByName('@nCdLoteLiq').Value := qryMasternCdLoteLiq.Value ;
      usp_Processa_LoteLiq.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      usp_Processa_LoteLiq.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento do lote.') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdLoteLiq.AsString) ;

  ShowMessage('Lote finalizado com sucesso!') ;

  case MessageDlg('Deseja imprimir o comprovante ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: ToolButton12.Click;
  end ;

  if (nCdProvisaoTit = 0) then
      btCancelar.Click
  else Close ;

end;

procedure TfrmBaixaColetiva.btSalvarClick(Sender: TObject);
begin

  if (qryMasterdDtFecham.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmBaixaColetiva.btSugerirChequesClick(Sender: TObject);
var
  objSugerirCheques : TfrmSugerirCheques ;
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

  if not qryMaster.Active then
  begin
      ShowMessage('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      ShowMessage('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  if (qryMastercTipoLiq.Value = 'R') then
  begin
      ShowMessage('N�o � poss�vel utilizar a sugest�o de cheques em lote de recebimento.') ;
      exit ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) and (qryMastercTipoLiq.Value = 'P') then
  begin
      ShowMessage('S� � permitido utilizar a Sugest�o de Cheques em lote com terceiro definido.') ;
      exit ;
  end ;

  if (qryChequeLote.RecordCount > 0) then
  begin
      ShowMessage('Exclua todos os cheques antes de utilizar a sugest�o de cheques.') ;
      exit ;
  end ;

  if (qryMasternCdLoteLiq.Value = 0) then
      qryMaster.Post ;

  objSugerirCheques := TfrmSugerirCheques.Create(Self) ;

  objSugerirCheques.nCdLoteLiq := qryMasternCdLoteLiq.Value ;

  objSugerirCheques.edtTipoSimulacao.Enabled  := False;
  objSugerirCheques.edtValorPagamento.Enabled := False;

  if (qryMastercTipoLiq.Value = 'D') then
  begin
      objSugerirCheques.SugerirCheques('D',0);
  end ;

  if (qryMastercTipoLiq.Value = 'P') then
  begin
      objSugerirCheques.edtTipoSimulacao.Text := '1' ;
      objSugerirCheques.edtTipoSimulacao.Enabled  := True;
      objSugerirCheques.edtValorPagamento.Enabled := True;
      objSugerirCheques.SugerirCheques('P',qryMasternValTitLiq.Value);
  end ;

  freeAndNil( objSugerirCheques ) ;

  PosicionaPK(qryMasternCdLoteLiq.Value) ;

end;

procedure TfrmBaixaColetiva.cxButton1Click(Sender: TObject);
begin
  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

  if not qryMaster.Active then
  begin
      ShowMessage('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      ShowMessage('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  if (qryChequeLote.RecordCount > 0) then
  begin

      case MessageDlg('Todos os cheques do lote ser�o exclu�dos. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: exit
      end;

      frmMenu.Connection.BeginTrans ;

      try
          qryChequeLote.First ;

          while not qryChequeLote.Eof do
          begin
              qryChequeLote.Delete;
              qryChequeLote.First;
          end ;

      except
          frmMenu.Connection.RollbackTrans;
          ShowMessage('Erro no processamento.') ;
          raise ;
          exit ;
      end ;

      frmMenu.Connection.CommitTrans ;

      PosicionaPK(qryMasternCdLoteLiq.Value) ;


  end ;


end;

procedure TfrmBaixaColetiva.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmBaixaColetiva.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(39);

            If (nPK > 0) then
            begin
                qryMasternCdContaBancaria.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBaixaColetiva.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTituloLoteLiq.State = dsBrowse) then
             qryTituloLoteLiq.Edit ;


        if (qryTituloLoteLiq.State = dsInsert) or (qryTituloLoteLiq.State = dsEdit) then
        begin

            if (qryMasternCdTerceiro.Value > 0) then
            begin

              if (qryMastercTipoLiq.Value = 'P') then
                  nPK := frmLookup_Padrao.ExecutaConsulta2(65,'Titulo.nCdTerceiro = ' + qryMasternCdTerceiro.asString)
              else if (qryMastercTipoLiq.Value = 'R') then
                  nPK := frmLookup_Padrao.ExecutaConsulta2(66,'Titulo.nCdTerceiro = ' + qryMasternCdTerceiro.asString);

            end
            else begin

              if (qryMastercTipoLiq.Value = 'P') then
                  nPK := frmLookup_Padrao.ExecutaConsulta(65)
              else if (qryMastercTipoLiq.Value = 'R') then
                  nPK := frmLookup_Padrao.ExecutaConsulta(66);

            end ;

            If (nPK > 0) then
            begin
                qryTituloLoteLiqnCdTitulo.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmBaixaColetiva.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  DBEdit4.Enabled := True  ;
  DBEdit6.Enabled := False ;

  if (DBEdit3.Text = 'D') then
  begin
      DBEdit4.Enabled := False ;
      DBEdit5.SetFocus ;
  end ;

  if (DbEdit3.Text = 'R') then
  begin
      DBEdit6.Enabled := True ;
  end ;

end;

procedure TfrmBaixaColetiva.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmBaixaColetiva.DBGridEh3Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

end;

procedure TfrmBaixaColetiva.Edit1Exit(Sender: TObject);
begin
  inherited;

  nCdBancoAux  := 0  ;
  cAgenciaAux  := '' ;
  cContaAux    := '' ;
  cDVAux       := '' ;
  iNrChequeAux := 0  ;

  if (Edit1.Text <> '') then
  begin

      try
          nCdBancoaux := StrToInt(Copy(Edit1.Text,2,3)) ;
          iNrChequeAux:= StrToInt(Copy(Edit1.Text,14,6)) ;
      except
          MensagemAlerta('C�digo de Barra do Cheque inv�lido.') ;
          Edit1.Text := '' ;
          Edit1.SetFocus ;
          abort ;
      end ;

      cAgenciaAux := Copy(Edit1.Text,5,4) ;
      cContaAux   := Copy(Edit1.Text,23,10) ;
      cDVAux      := Copy(Edit1.Text,33,1) ;

      if (DBEdit3.Text = 'R') then
      begin
          qryLocalizaConta.Close ;
          qryLocalizaConta.Parameters.ParamByName('nCdBanco').Value := nCdBancoAux ;
          qryLocalizaConta.Parameters.ParamByName('cAgencia').Value := cAgenciaAux ;
          qryLocalizaConta.Parameters.ParamByName('cConta').Value   := cContaAux   ;
          qryLocalizaConta.Parameters.ParamByName('cDigito').Value  := cDVAux      ;
          qryLocalizaConta.Open ;

          if not qryLocalizaConta.Eof then
              edtCPF.Text := qryLocalizaContacCNPJCPF.Value ;

          qryLocalizaConta.Close ;
      end
      else
      begin

          qryLocalizaCheque.Close ;
          qryLocalizaCheque.Parameters.ParamByName('nCdBanco').Value  := nCdBancoAux ;
          qryLocalizaCheque.Parameters.ParamByName('cAgencia').Value  := cAgenciaAux ;
          qryLocalizaCheque.Parameters.ParamByName('cConta').Value    := cContaAux ;
          qryLocalizaCheque.Parameters.ParamByName('cDigito').Value   := cDVAux ;
          qryLocalizaCheque.Parameters.ParamByName('iNrCheque').Value := iNrChequeAux ;
          qryLocalizaCheque.Open ;

          if qryLocalizaCheque.Eof then
          begin
              ShowMessage('Este cheque n�o existe no arquivo de cheques de terceiros.') ;
              abort ;
          end ;

          if (qryLocalizaChequenCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
          begin
              ShowMessage('Este cheque n�o pertence a esta empresa.') ;
              abort ;
          end ;

          if (qryLocalizaChequenCdTerceiroPort.Value > 0) then
          begin
              ShowMessage('Este cheque j� foi enviado para outro portador e n�o pode ser utilizado.') ;
              abort ;
          end ;

          edtCPF.Text          := qryLocalizaChequecCNPJCPF.Value    ;
          edtValorCheque.Value := qryLocalizaChequenValCheque.Value  ;
          edtDtDeposito.Text   := qryLocalizaChequedDtDeposito.asString ;

      end ;

  end
  else
  begin
      Panel1.Visible := False ;
      DBGridEh1.SetFocus ;
  end ;

end;

procedure TfrmBaixaColetiva.edtDtDepositoExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtDtDeposito.Text) = '/  /') then
      edtDtDeposito.Text := DateToStr(Date) ;

  if (nCdBancoAux <> 0) then
  begin

      qryConfereBanco.Close;
      PosicionaQuery(qryConfereBanco, intToStr(nCdBancoAux)) ;

      if qryConfereBanco.eof then
      begin
          MensagemAlerta('C�digo de banco inv�lido ou n�o cadastrado.') ;
          edit1.Text := '' ;
          edit1.SetFocus;
          abort ;
      end ;

      qryChequeLote.Insert ;
      qryChequeLotenCdBanco.Value    := nCdBancoAux  ;
      qryChequeLotecAgencia.Value    := cAgenciaAux  ;
      qryChequeLotecConta.Value      := cContaAux    ;
      qryChequeLotecDigito.Value     := cDVAux       ;
      qryChequeLotecCNPJCPF.Value    := edtCPF.Text  ;
      qryChequeLoteiNrCheque.Value   := iNrChequeAux ;

      if (DbEdit3.Text = 'R') then
      begin

          if (StrToDate(edtDtDeposito.Text) < Date) then
          begin
              MensagemAlerta('A data de dep�sito n�o pode ser menor que hoje.') ;
              edtDtDeposito.SetFocus;
              abort ;
          end ;

          qryChequeLotedDtDeposito.Value := StrToDate(edtDtDeposito.Text) ;
          qryChequeLotenValCheque.Value  := edtValorCheque.Value ;
      end ;

      try
          qryChequeLote.Post ;
      except
          qryChequeLote.Cancel ;
          MensagemErro('Erro na grava��o do cheque.') ;
          edit1.Text           := '' ;
          raise ;
          edit1.SetFocus ;
      end ;

      edit1.Text           := '' ;
      edtCPF.Text          := '' ;
      edtDtDeposito.Text   := '' ;
      edtValorCheque.Value := 0 ;

      nCdBancoAux  := 0  ;
      cAgenciaAux  := '' ;
      cContaAux    := '' ;
      cDVAux       := '' ;
      iNrChequeAux := 0  ;

  end ;

  Edit1.SetFocus ;


end;

procedure TfrmBaixaColetiva.btMaqChequeClick(Sender: TObject);
begin
  inherited;
  if not qryMaster.Active then
  begin
      ShowMessage('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      ShowMessage('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  edit1.Text           := '' ;
  edtCPF.Text          := '' ;
  edtDtDeposito.Text   := '' ;
  edtValorCheque.Value := 0 ;

  Panel1.Visible := True ;
  Edit1.SetFocus ;
  
end;

procedure TfrmBaixaColetiva.edtValorChequeKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if (key = #13) then
      edtDtDeposito.SetFocus;

end;

procedure TfrmBaixaColetiva.cxButton2Click(Sender: TObject);
begin
  inherited;

  Panel1.Visible := False ;
end;

procedure TfrmBaixaColetiva.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit4.Text) ;
  
end;

procedure TfrmBaixaColetiva.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  PosicionaQuery(qryContaBancaria, DBEdit5.Text) ;
  
end;

procedure TfrmBaixaColetiva.qryRecursoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryRecursonCdContaBancaria.Value > 0) and (qryRecursocNmContaBancaria.Value = '') then
  begin

      qryContaBancariaRecurso.Close ;
      qryContaBancariaRecurso.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryContaBancariaRecurso.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      qryContaBancariaRecurso.Parameters.ParamByName('nPK').Value        := qryRecursonCdContaBancaria.Value ;
      qryContaBancariaRecurso.Open ;

      if not qryContaBancariaRecurso.Eof then
          qryRecursocNmContaBancaria.Value := qryContaBancariaRecursocNmContaBancaria.Value ;

  end ;

  if (qryRecursonCdFormaPagto.Value > 0) and (qryRecursocNmFormaPagto.Value = '') then
  begin
      qryFormaPagto.Close ;
      PosicionaQuery(qryFormaPagto, qryRecursonCdFormaPagto.AsString) ;

      if not qryFormaPagto.Eof then
          qryRecursocNmFormaPagto.Value := qryFormaPagtocNmFormaPagto.Value ;

  end ;

  // desativar campo de cheque
  DBGridEh2.Columns[7].Title.Font.Color := clRed ;
  DBGridEh2.Columns[8].Title.Font.Color := clRed ;

  DBGridEh2.Columns[7].ReadOnly := True ;
  DBGridEh2.Columns[8].ReadOnly := True ;

  if not qryFormaPagto.eof and (qryFormaPagtonCdTabTipoFormaPagto.Value = 2) then
  begin

      DBGridEh2.Columns[7].Title.Font.Color := clBlack ;
      DBGridEh2.Columns[8].Title.Font.Color := clBlack ;

      DBGridEh2.Columns[7].ReadOnly := False ;
      DBGridEh2.Columns[8].ReadOnly := False ;

  end ;

end;

procedure TfrmBaixaColetiva.ToolButton12Click(Sender: TObject);
var
  objRel : TrptBaixaColetiva_view ;
begin
  inherited;

  if not qryMaster.Active or (qryMasternCdLoteLiq.Value = 0) then
  begin
      exit ;
  end ;

  if (qryMasterdDtFecham.asString = '') then
  begin
      MensagemAlerta('Encerre o lote antes de imprimir o comprovante.') ;
      exit ;
  end ;

  objRel := TrptBaixaColetiva_view.Create(Self) ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  objRel.QRLabel47.Caption  := frmMenu.cNmEmpresaAtiva;

  PosicionaQuery(objRel.qryLoteLiq, qryMasternCdLoteLiq.asString) ;
  PosicionaQuery(objRel.qryChequeLoteLiq, qryMasternCdLoteLiq.asString) ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QRCompositeReport1.Preview;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;


end;

procedure TfrmBaixaColetiva.cxButton3Click(Sender: TObject);
begin
  inherited;
  if not qryMaster.Active then
  begin
      ShowMessage('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      ShowMessage('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;

  if (qryTituloLoteLiq.RecordCount > 0) then
  begin

      case MessageDlg('Todos os t�tulos do lote ser�o exclu�dos. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: exit
      end;

      frmMenu.Connection.BeginTrans ;

      try
          qryTituloLoteLiq.First ;

          while not qryTituloLoteLiq.Eof do
          begin
              qryTituloLoteLiq.Delete;
              qryTituloLoteLiq.First;
          end ;

      except
          frmMenu.Connection.RollbackTrans;
          ShowMessage('Erro no processamento.') ;
          raise ;
          exit ;
      end ;

      frmMenu.Connection.CommitTrans ;

      PosicionaPK(qryMasternCdLoteLiq.Value) ;

  end ;

end;

procedure TfrmBaixaColetiva.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmBaixaColetiva.btAddTituloClick(Sender: TObject);
var
  objBaixaColetiva_ConsTitulo : TfrmBaixaColetiva_ConsTitulo ;
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

  if not qryMaster.Active or (qryMasternCdLoteLiq.Value = 0) then
  begin
      MensagemAlerta('Salve o lote antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasterdDtFecham.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar um lote j� encerrado.') ;
      exit ;
  end ;
  
  if (qryMasternCdTerceiro.asString = '') then
  begin
      MensagemAlerta('S� � poss�vel utilizar este recurso para lotes quem tenham o terceiro informado.') ;
      exit ;
  end ;

  if (qryMasternSaldoLote.Value <= 0) then
  begin
      MensagemAlerta('O lote n�o tem saldo dispon�vel.') ;
      exit ;
  end ;

  objBaixaColetiva_ConsTitulo := TfrmBaixaColetiva_ConsTitulo.Create(Self) ;

  objBaixaColetiva_ConsTitulo.nCdLoteLiq := qryMasternCdLoteLiq.Value ;
  
  objBaixaColetiva_ConsTitulo.qryPreparaTemp.ExecSQL;

  objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('dDtMov').Value      := qryMasterdDtLoteLiq.AsString ;
  objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
  objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('cSenso').Value      := 'C' ;
  objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('nCdLoteLiq').Value  := qryMasternCdLoteLiq.Value ;

  if (qryMastercTipoLiq.Value = 'P') then
      objBaixaColetiva_ConsTitulo.qryPopulaTemp.Parameters.ParamByName('cSenso').Value := 'D' ;

  objBaixaColetiva_ConsTitulo.qryPopulaTemp.ExecSQL;

  objBaixaColetiva_ConsTitulo.qryTituloPend.Close;
  objBaixaColetiva_ConsTitulo.qryTituloPend.Open;

  objBaixaColetiva_ConsTitulo.qryTituloSel.Close;
  objBaixaColetiva_ConsTitulo.qryTituloSel.Open;

  showForm( objBaixaColetiva_ConsTitulo , TRUE ) ;

  PosicionaQuery(qryMaster, qryMasternCdLoteLiq.asString) ;
 

end;

function TfrmBaixaColetiva.calculaTaxaAdm(nValCheque: double;
  iDias: integer): double;
begin

    if (iDias <= iDiasCarenciaCheque) then
        exit ;

    if (nPercTaxaAdm > 0) and (nValCheque > 0) then
        Result := ((nValCheque * (nPercTaxaAdm/100)/30) * iDias)
    else Result := 0 ;

end;

procedure TfrmBaixaColetiva.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if(key = VK_RETURN) then
      edtCPF.SetFocus;
end;

procedure TfrmBaixaColetiva.edtCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if(key = VK_RETURN) then
      edtValorCheque.SetFocus;
end;

procedure TfrmBaixaColetiva.edtValorChequeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if(key = VK_RETURN) then
      edtDtDeposito.SetFocus;
end;

procedure TfrmBaixaColetiva.edtDtDepositoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if(key = VK_RETURN) then
      Edit1.SetFocus;
end;

initialization
    RegisterClass(TfrmBaixaColetiva);
end.
