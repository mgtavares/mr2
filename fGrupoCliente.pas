unit fGrupoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup, DBGridEhGrouping,
  ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmGrupoCliente = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit4: TDBEdit;
    dsStatus: TDataSource;
    Label4: TLabel;
    qryCondPagtoGrupoClienteDesc: TADOQuery;
    dsCondPagtoGrupoClienteDesc: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryCondPagtoGrupoClienteDescnCdCondPagtoGrupoClienteDesc: TIntegerField;
    qryCondPagtoGrupoClienteDescnCdGrupoClienteDesc: TIntegerField;
    qryCondPagtoGrupoClienteDescnCdCondPagto: TIntegerField;
    qryCondPagtoGrupoClienteDescnPercDesconto: TBCDField;
    qryCondPagtoGrupoClienteDescnCdServidorOrigem: TIntegerField;
    qryCondPagtoGrupoClienteDescdDtReplicacao: TDateTimeField;
    qryCondPagto: TADOQuery;
    qryCondPagtocFlgAtivo: TIntegerField;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryCondPagtoGrupoClienteDesccNmCondPagto: TStringField;
    qryMasternCdGrupoClienteDesc: TIntegerField;
    qryMastercNmGrupoClienteDesc: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMasternPercDesconto: TBCDField;
    DBEdit5: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryCondPagtoGrupoClienteDescBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoCliente: TfrmGrupoCliente;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmGrupoCliente.FormCreate(Sender: TObject);
begin

  cNmTabelaMaster   := 'GRUPOCLIENTEDESC';
  nCdTabelaSistema  := 551;
  nCdConsultaPadrao := 787;
  bAlterarDados     := True;

  inherited;
end;

procedure TfrmGrupoCliente.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  case key of
    vk_F4 : begin
        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            if (nPK > 0) then
                qryMasternCdStatus.Value := nPK;
        end;
    end;
  end;
end;

procedure TfrmGrupoCliente.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryStatus, qryMasternCdStatus.AsString);
end;

procedure TfrmGrupoCliente.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmGrupoCliente.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryStatus.Close;
end;

procedure TfrmGrupoCliente.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
  qryCondPagtoGrupoClienteDesc.Close;

  if (qryMaster.IsEmpty) then
      Exit;

  PosicionaQuery(qryStatus, qryMasternCdStatus.AsString);
  PosicionaQuery(qryCondPagtoGrupoClienteDesc, qryMasternCdGrupoClienteDesc.AsString);
end;

procedure TfrmGrupoCliente.qryMasterBeforePost(DataSet: TDataSet);
begin
  if(DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do grupo.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if(DBEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o status.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if(DBEdit5.Text = '') then
  begin
      MensagemAlerta('Informe o valor percentual de desconto do grupo.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (StrToFloat(DBEdit5.Text) < 1) OR (StrToFloat(DBEdit5.Text) > 100) then
  begin
      MensagemAlerta('O valor do desconto deve ser entre 1% � 100%.');
      Abort;
  end;
  
  inherited;
end;

procedure TfrmGrupoCliente.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
  qryCondPagtoGrupoClienteDesc.Close;
end;

procedure TfrmGrupoCliente.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmGrupoCliente.qryCondPagtoGrupoClienteDescBeforePost(
  DataSet: TDataSet);
begin
  if (qryCondPagtoGrupoClienteDesccNmCondPagto.Value = '') then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.');
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (qryCondPagtoGrupoClienteDescnPercDesconto.Value < 1) OR (qryCondPagtoGrupoClienteDescnPercDesconto.Value > 100) then
  begin
      MensagemAlerta('O valor do desconto deve ser entre 1% � 100%.');
      Abort;
  end;

  inherited;

  if (qryCondPagtoGrupoClienteDesc.State = dsInsert) then
  begin
      qryCondPagtoGrupoClienteDescnCdCondPagtoGrupoClienteDesc.Value := frmMenu.fnProximoCodigo('CONDPAGTOGRUPOCLIENTEDESC');
      qryCondPagtoGrupoClienteDescnCdGrupoClienteDesc.Value          := qryMasternCdGrupoClienteDesc.Value;
  end;
end;

procedure TfrmGrupoCliente.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if (qryCondPagtoGrupoClienteDesc.State = dsBrowse) then
              qryCondPagtoGrupoClienteDesc.Edit;

          nPK := frmLookup_Padrao.ExecutaConsulta(138);

          if (nPK > 0) then
              qryCondPagtoGrupoClienteDescnCdCondPagto.Value := nPK;
      end;
  end;
end;

initialization
  RegisterClass(TfrmGrupoCliente) ;

end.

