unit fAtendRequisicao_Local;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Mask,
  StdCtrls, DBCtrlsEh, DB, ADODB, GridsEh, DBGridEh, DBCtrls, DBLookupEh;

type
  TfrmAtendRequisicao_Local = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit1: TMaskEdit;
    qryLocalEstoque: TADOQuery;
    dsLocalEstoque: TDataSource;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    dbComboBoxEh1: TDBLookupComboboxEh;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto        : integer ;
    nCdItemRequisicao : integer ;
    nSaldo            : double ;
  end;

var
  frmAtendRequisicao_Local: TfrmAtendRequisicao_Local;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAtendRequisicao_Local.FormShow(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;

  if (nCdProduto > 0) then
  begin

      qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryLocalEstoque.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      qryLocalEstoque.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
      qryLocalEstoque.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryLocalEstoque.Open ;

      if (qryLocalEstoque.eof) then
      begin
          MensagemAlerta('Nenhum local de estoque encontrado para o produto ou que voc� tenha acesso para movimenta��o.') ;
          qryLocalEstoque.Close ;
          close ;
      end ;

      dbComboBoxEh1.Enabled := True ;
      dbComboBoxEh1.SetFocus;

      if (qryLocalEstoque.RecordCount = 1) then
      begin
          dbComboBoxEh1.Enabled := False ;
          qryLocalEstoque.First ;
          MaskEdit1.SetFocus;
      end ;

  end ;

end;

procedure TfrmAtendRequisicao_Local.MaskEdit1Exit(Sender: TObject);
begin
  inherited;
  If (trim(MaskEdit1.Text) <> '') then
      MaskEdit1.text := formatcurr('0.0000',StrToFloat(StringReplace(MaskEdit1.text,',','.',[rfReplaceAll,rfIgnoreCase]))) ;

  ToolButton1.Click;
end;

procedure TfrmAtendRequisicao_Local.ToolButton2Click(Sender: TObject);
begin
  qryLocalEstoque.Close ;
  MaskEdit1.Text := '0' ;

  inherited;
end;

procedure TfrmAtendRequisicao_Local.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (nCdProduto > 0) then
  begin
      if (dbComboBoxEh1.Enabled) and (dbComboBoxEh1.Text = '') then
      begin
          MensagemAlerta('Selecione um local de estoque.') ;
          dbComboBoxEh1.SetFocus;
          exit ;
      end ;
  end ;

  if (StrToFloat(MaskEdit1.Text) > nSaldo) then
  begin
      MensagemAlerta('Quantidade maior que o saldo do item.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  case MessageDlg('Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  close ;

end;

end.
