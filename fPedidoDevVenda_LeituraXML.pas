unit fPedidoDevVenda_LeituraXML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBGridEhGrouping, cxLookAndFeelPainters, DBGridEh,
  DBCtrls, Mask, DBCtrlsEh, DBLookupEh, cxButtons, GridsEh, cxPC,
  cxControls, ToolCtrlsEh;

type
  TfrmPedidoDevVenda_LeituraXML = class(TfrmProcesso_Padrao)
    qryNFeXML: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsNFeXML: TDataSource;
    qryProdutoFornecedor: TADOQuery;
    qryTerceiro: TADOQuery;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonCdProduto: TIntegerField;
    qryItemPedido: TADOQuery;
    qryProdutoFornecedorAux: TADOQuery;
    qryProdutoEAN: TADOQuery;
    qryProdutoEANnCdProduto: TIntegerField;
    qryProdutoEANcNmProduto: TStringField;
    qryProdutoEANcUnidadeMedida: TStringField;
    ToolButton4: TToolButton;
    btCadProduto: TToolButton;
    btCadProdutoGrade: TToolButton;
    ToolButton5: TToolButton;
    btAtualizarVinc: TToolButton;
    ToolButton7: TToolButton;
    qryNFeXMLnCdProdutoFornecedor: TAutoIncField;
    qryNFeXMLcCdProdutoFornecedor: TStringField;
    qryNFeXMLcEAN: TStringField;
    qryNFeXMLcNmProdutoFornecedor: TStringField;
    qryNFeXMLnQtdeFornecedor: TBCDField;
    qryNFeXMLcUnMedidaFornecedor: TStringField;
    qryNFeXMLnCdProduto: TIntegerField;
    qryNFeXMLcNmProduto: TStringField;
    qryNFeXMLcUnMedida: TStringField;
    qryNFeXMLnCdPedido: TIntegerField;
    qryNFeXMLcCFOP: TStringField;
    qryNFeXMLcNCM: TStringField;
    qryNFeXMLnValUnitario: TBCDField;
    qryNFeXMLnValDesconto: TBCDField;
    qryNFeXMLnValTotal: TBCDField;
    qryNFeXMLnValBaseIcms: TBCDField;
    qryNFeXMLnValIcms: TBCDField;
    qryNFeXMLnPercAliqIcms: TBCDField;
    qryNFeXMLnPercRedBaseIcms: TBCDField;
    qryNFeXMLnValBaseIPI: TBCDField;
    qryNFeXMLnPercAliqIpi: TBCDField;
    qryNFeXMLnValIPI: TBCDField;
    qryNFeXMLcCSTIPI: TStringField;
    qryNFeXMLnPercIVA: TBCDField;
    qryNFeXMLnPercAliqIcmsST: TBCDField;
    qryNFeXMLnPercRedBaseIcmsST: TBCDField;
    qryNFeXMLnValIcmsST: TBCDField;
    qryNFeXMLnValBaseIcmsSt: TBCDField;
    qryNFeXMLnValSeguro: TBCDField;
    qryNFeXMLnValDespAcessorias: TBCDField;
    qryNFeXMLnvalFrete: TBCDField;
    qryNFeXMLcOrigemMercadoria: TStringField;
    qryNFeXMLcCSTPIS: TStringField;
    qryNFeXMLnValBasePIS: TBCDField;
    qryNFeXMLnAliqPIS: TBCDField;
    qryNFeXMLnValPIS: TBCDField;
    qryNFeXMLcCSTCOFINS: TStringField;
    qryNFeXMLnValBaseCOFINS: TBCDField;
    qryNFeXMLnAliqCOFINS: TBCDField;
    qryNFeXMLnValCOFINS: TBCDField;
    qryNFeXMLnValBaseII: TBCDField;
    qryNFeXMLnValDespAdu: TBCDField;
    qryNFeXMLnValII: TBCDField;
    qryNFeXMLnValIOFImp: TBCDField;
    qryNFeXMLcNossoEAN: TStringField;
    qryNFeXMLcFlgItemAD: TIntegerField;
    qryItemPedidonCdItemPedido: TIntegerField;
    qryItemPedidonCdPedido: TIntegerField;
    qryItemPedidonCdItemPedidoPai: TIntegerField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocCdProduto: TStringField;
    qryItemPedidonCdTipoItemPed: TIntegerField;
    qryItemPedidonCdEstoqueMov: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdePed: TFloatField;
    qryItemPedidonQtdeExpRec: TFloatField;
    qryItemPedidonQtdeCanc: TFloatField;
    qryItemPedidonValUnitario: TFloatField;
    qryItemPedidonPercIPI: TFloatField;
    qryItemPedidonValIPI: TFloatField;
    qryItemPedidonValDesconto: TFloatField;
    qryItemPedidonValCustoUnit: TFloatField;
    qryItemPedidonValSugVenda: TFloatField;
    qryItemPedidonValTotalItem: TFloatField;
    qryItemPedidonValAcrescimo: TFloatField;
    qryItemPedidocSiglaUnidadeMedida: TStringField;
    qryItemPedidoiColuna: TIntegerField;
    qryItemPedidonCdTabStatusItemPed: TIntegerField;
    qryItemPedidonQtdeLibFat: TFloatField;
    qryItemPedidonValUnitarioEsp: TFloatField;
    qryItemPedidonCdGrupoImposto: TIntegerField;
    qryItemPedidocFlgPromocional: TIntegerField;
    qryItemPedidonCdTabPreco: TIntegerField;
    qryItemPedidonCdLoja: TIntegerField;
    qryItemPedidocFlgTrocado: TIntegerField;
    qryItemPedidonCdItemPedidoTroca: TIntegerField;
    qryItemPedidonCdTerceiroColab: TIntegerField;
    qryItemPedidodDtPedidoItem: TDateTimeField;
    qryItemPedidonCdVale: TIntegerField;
    qryItemPedidocDescricaoTecnica: TMemoField;
    qryItemPedidonDimensao: TFloatField;
    qryItemPedidonAltura: TFloatField;
    qryItemPedidonLargura: TFloatField;
    qryItemPedidodDtEntregaIni: TDateTimeField;
    qryItemPedidodDtEntregaFim: TDateTimeField;
    qryItemPedidonQtdePrev: TFloatField;
    qryItemPedidonPercentCompra: TFloatField;
    qryItemPedidonPercICMSSub: TFloatField;
    qryItemPedidonValICMSSub: TFloatField;
    qryItemPedidocFlgComplemento: TIntegerField;
    qryItemPedidonPercDescontoItem: TFloatField;
    qryItemPedidonValDescAbatUnit: TFloatField;
    qryItemPedidocFlgDefeito: TIntegerField;
    qryItemPedidonCdItemRecebimentoDev: TIntegerField;
    qryItemPedidonCdCampanhaPromoc: TIntegerField;
    qryItemPedidonValCMV: TFloatField;
    qryItemPedidonCdServidorOrigem: TIntegerField;
    qryItemPedidodDtReplicacao: TDateTimeField;
    qryItemPedidonFatorConvUnidadeEstoque: TFloatField;
    qryItemPedidonCdMotivoCancSaldoPed: TIntegerField;
    qryItemPedidodDtCancelSaldoItemPed: TDateTimeField;
    qryItemPedidonCdUsuarioCancelSaldoItemPed: TIntegerField;
    qryItemPedidonValFrete: TFloatField;
    qryItemPedidonValSeguro: TFloatField;
    qryItemPedidonValAcessorias: TFloatField;
    qryItemPedidocCdSTICMS: TStringField;
    qryItemPedidonPercRedBaseICMS: TFloatField;
    qryItemPedidonValBaseICMS: TFloatField;
    qryItemPedidonPercAliqICMS: TFloatField;
    qryItemPedidonValICMS: TFloatField;
    qryItemPedidonPercIVA: TFloatField;
    qryItemPedidonPercAliqICMSInt: TFloatField;
    qryItemPedidonValBaseICMSSub: TFloatField;
    qryItemPedidonPercIncFiscal: TFloatField;
    qryItemPedidocNCM: TStringField;
    qryItemPedidocCdSTIPI: TStringField;
    qryItemPedidocDoctoCompra: TStringField;
    qryItemPedidocCFOPItemPedido: TStringField;
    qryItemPedidonPercBaseCalcIPI: TFloatField;
    qryItemPedidonValBaseIPI: TFloatField;
    qryItemPedidocCSTPIS: TStringField;
    qryItemPedidonValBasePIS: TFloatField;
    qryItemPedidonAliqPIS: TFloatField;
    qryItemPedidonValPIS: TFloatField;
    qryItemPedidocCSTCOFINS: TStringField;
    qryItemPedidonValBaseCOFINS: TFloatField;
    qryItemPedidonAliqCOFINS: TFloatField;
    qryItemPedidonValCOFINS: TFloatField;
    qryItemPedidonCdCategFinancItemPedido: TIntegerField;
    qryItemPedidonCdProjetoItemPedido: TIntegerField;
    qryItemPedidocFlgMovEstoqueConsignado: TIntegerField;
    qryProdutoFornecedornCdProduto: TIntegerField;
    qryProdutoFornecedornCdTerceiro: TIntegerField;
    qryProdutoFornecedornPrecoUnit: TBCDField;
    qryProdutoFornecedornCdMoeda: TIntegerField;
    qryProdutoFornecedornPercIPI: TBCDField;
    qryProdutoFornecedornPercICMSSub: TBCDField;
    qryProdutoFornecedornPercBCICMSSub: TBCDField;
    qryProdutoFornecedorcCdProdutoFornecedor: TStringField;
    qryProdutonCdGrade: TIntegerField;
    qryNFeXMLcCdST: TStringField;
    qryNFeXMLcNrDocto: TStringField;
    qryNFeXMLcFlgItemSelecionado: TIntegerField;
    cmdRecalcPed: TADOCommand;
    btnMarcarTodos: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    btnDesmarcarTodos: TToolButton;
    qryAux: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocFlgTerceiroGrupo: TIntegerField;
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryNFeXMLBeforeDelete(DataSet: TDataSet);
    procedure qryNFeXMLnCdProdutoChange(Sender: TField);
    procedure FormShow(Sender: TObject);
    procedure btAtualizarVincClick(Sender: TObject);
    procedure btCadProdutoClick(Sender: TObject);
    procedure btCadProdutoGradeClick(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure qryNFeXMLBeforePost(DataSet: TDataSet);
    procedure SelecionarItens(cFlgSelecionar : integer);
    procedure btnMarcarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodosClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido     : Integer;
    nCdTipoPedido : Integer;
  end;

var
  frmPedidoDevVenda_LeituraXML: TfrmPedidoDevVenda_LeituraXML;

implementation

uses fMenu, fLookup_Padrao, fProdutoERP, fProduto, Math;

{$R *.dfm}

procedure TfrmPedidoDevVenda_LeituraXML.ToolButton2Click(Sender: TObject);
begin
  inherited;
  qryNFeXML.Close;
  qryProdutoFornecedor.Close;
  qryProduto.Close;
  qryTerceiro.Close;
end;

procedure TfrmPedidoDevVenda_LeituraXML.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryNFeXML.IsEmpty) then
      Exit;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT 1                      ');
  qryAux.SQL.Add('  FROM #Temp_NFeXML           ');
  qryAux.SQL.Add(' WHERE cFlgItemSelecionado = 1');
  qryAux.Open;

  if (qryAux.IsEmpty) then
  begin
      MensagemAlerta('Selecione no m�nimo um item para ser importado.');
      Abort;
  end;

  if (MessageDLG('Confirma importa��o dos itens selecionados ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
      Exit;

  if (qryNFeXML.RecordCount > 0) then
  begin

      PosicionaQuery(qryItemPedido,qryNFeXMLnCdPedido.AsString);

      { -- se terceiro emitente for um fornecedor externo, faz consist�ncias          -- }
      { -- caso seja do mesmo grupo, pula processo pois produtos j� est�o cadastrados -- }
      if (qryTerceirocFlgTerceiroGrupo.Value = 0) then
      begin
          { -- verifica itens do pedido sem produto -- }
          qryNFeXML.First;

          while (not qryNFeXML.Eof) do
          begin
              if (qryNFeXMLcFlgItemSelecionado.Value = 1) then
              begin

                  if ((qryNFeXMLnCdProduto.Value = 0) or (qryNFeXMLcNmProduto.Value = '')) and (qryNFeXMLcFlgItemAD.Value = 0) then
                  begin
                      MensagemAlerta('Selecione o produto correspondente ao produto do fornecedor.' + #13#13 + 'Produto: ' + qryNFeXMLcNmProdutoFornecedor.Value);
                      DBGridEh1.SelectedField := qryNFeXMLnCdProduto;
                      DBGridEh1.SetFocus;
                      Abort;
                  end;

                  if ((qryNFeXMLnCdProduto.Value > 0) or (qryNFeXMLcNmProduto.Value <> '')) and (qryNFeXMLcFlgItemAD.Value = 1) then
                  begin
                      MensagemAlerta('N�o � permitido informar o produto para itens AD.' + #13#13 + 'Produto: ' + qryNFeXMLcNmProdutoFornecedor.Value);
                      DBGridEh1.SelectedField := qryNFeXMLnCdProduto;
                      DBGridEh1.SetFocus;
                      Abort;
                  end;

                  { -- verifica se c�digo de barra do forneceddor diferente do c�digo do produto cadastrado -- }
                  if (Trim(qryNFeXMLcEAN.Value) <> '') and (Trim(qryNFeXMLcNossoEAN.Value) <> '') then
                  begin
                      if (Trim(qryNFeXMLcEAN.Value) <> Trim(qryNFeXMLcNossoEAN.Value)) then
                      begin
                          if ( MessageDlg('O c�digo EAN do fornecedor difere do c�digo EAN do produto informado. Deseja continuar ?' + #13#13 + 'Produto: ' + qryNFeXMLcNmProdutoFornecedor.Value,mtConfirmation,[mbYes,mbNo],0) = MrNo) then
                              Exit;
                      end;
                  end;
              end;
          
              qryNFeXML.Next;
          end;

          { -- atualiza os produtos na tabela ProdutoFornecedor -- }
          qryNFeXML.First;

          while (not qryNFeXML.Eof) do
          begin
              if (qryNFeXMLcFlgItemSelecionado.Value = 1) then
              begin

                  if (qryNFeXMLcFlgItemAD.Value = 0) then
                  begin
                      qryProdutoFornecedor.Close;
                      qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
                      qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := qryNFeXMLcCdProdutoFornecedor.Value;
                      qryProdutoFornecedor.Open;

                      qryProdutoFornecedorAux.Close;
                      qryProdutoFornecedorAux.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
                      qryProdutoFornecedorAux.Parameters.ParamByName('nCdProduto').Value  := qryNFeXMLnCdProduto.Value;
                      qryProdutoFornecedorAux.Open;

                      if ((qryProdutoFornecedor.Eof) and (qryProdutoFornecedorAux.Eof)) then
                      begin
                          qryProdutoFornecedor.Insert;
                          qryProdutoFornecedornCdProduto.Value           := qryNFeXMLnCdProduto.Value;
                          qryProdutoFornecedorcCdProdutoFornecedor.Value := qryNFeXMLcCdProdutoFornecedor.Value;
                          qryProdutoFornecedornCdTerceiro.Value          := qryTerceironCdTerceiro.Value;
                          qryProdutoFornecedornPrecoUnit.Value           := qryNFeXMLnValUnitario.Value;
                          qryProdutoFornecedornCdMoeda.Value             := 1;
                          qryProdutoFornecedornPercICMSSub.Value         := qryNFeXMLnpercAliqICMSSt.Value;
                          qryProdutoFornecedornPercIPI.Value             := qryNFeXMLnPercAliqIPI.Value;
                          qryProdutoFornecedornPercBCICMSSub.Value       := qryNFeXMLnPercRedBaseICMSSt.Value;
                          qryProdutoFornecedor.Post;
                      end;
                  end;
              end;

              qryNFeXML.Next;
          end;
      end;

      { -- importa itens do xml no pedido -- }
      try
          frmMenu.Connection.BeginTrans;

          qryNFeXML.First;

          while (not qryNFeXML.Eof) do
          begin
              if (qryNFeXMLcFlgItemSelecionado.Value = 1) then
              begin

                  qryItemPedido.Insert;
                  qryItemPedidonCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO');
                  qryItemPedidonCdPedido.Value     := qryNFeXMLnCdPedido.Value;
                  qryItemPedidocDoctoCompra.Value  := qryNFeXMLcNrDocto.Value;

                  if (qryNFeXMLcFlgItemAD.Value = 0) then
                  begin
                       qryItemPedidonCdProduto.Value := qryNFeXMLnCdProduto.Value;
                       qryItemPedidocCdProduto.Value := intToStr(qryNFeXMLnCdProduto.Value);
                       qryItemPedidocNmItem.Value    := qryNFeXMLcNmProduto.Value;

                       { -- verifica classifica��o do item -- }
                       PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

                       if (qryProdutonCdGrade.Value > 0) then
                           qryItemPedidonCdTipoItemPed.Value := 1  //produto grade
                       else
                           qryItemPedidonCdTipoItemPed.Value := 2; //produto ERP
                   end
                   else
                   begin
                       qryItemPedidocCdProduto.Value     := 'AD';
                       qryItemPedidocNmItem.Value        := qryNFeXMLcNmProdutoFornecedor.Value;
                       qryItemPedidonCdTipoItemPed.Value := 5;
                   end;

                   qryItemPedidocSiglaUnidadeMedida.Value := Uppercase( qryNFeXMLcUnMedidaFornecedor.Value );
                   qryItemPedidonQtdePed.Value            := qryNFeXMLnQtdeFornecedor.Value;
                   qryItemPedidonValUnitario.Value        := qryNFeXMLnValUnitario.Value;
                   qryItemPedidonValTotalItem.Value       := qryNFeXMLnValTotal.Value - qryNFeXMLnValDesconto.Value + qryNFeXMLnValFrete.Value;
                   qryItemPedidonValDesconto.Value        := frmMenu.TBRound((qryNFeXMLnValDesconto.Value / qryNFeXMLnQtdeFornecedor.Value),2);
                   qryItemPedidonValFrete.Value           := frmMenu.TBRound((qryNFeXMLnValFrete.Value / qryNFeXMLnQtdeFornecedor.Value),2);
                   qryItemPedidonValSeguro.Value          := qryNFeXMLnValSeguro.Value;
                   qryItemPedidonValAcessorias.Value      := qryNFeXMLnValDespAcessorias.Value;
                   qryItemPedidonValCustoUnit.Value       := qryNFeXMLnValUnitario.Value - qryItemPedidonValDesconto.Value + qryItemPedidonValFrete.Value;

                   if (qryNFeXMLnValTotal.Value > 0) then
                       qryItemPedidonPercRedBaseICMS.Value := frmMenu.TBRound(((qryNFeXMLnValBaseICMS.Value / qryNFeXMLnValTotal.Value ) * 100), 2);

                   qryItemPedidonValBaseICMS.Value := qryNFeXMLnValBaseICMS.Value;
                   qryItemPedidonPercAliqICMS.Value := qryNFeXMLnPercAliqICMS.Value;
                   qryItemPedidonValICMS.Value      := qryNFeXMLnValICMS.Value;
                   qryItemPedidonPercIVA.Value      := qryNFeXMLnPercIVA.Value;

                   if ((qryNFeXMLnPercIVA.Value = 0) and (qryNFeXMLnValBaseICMSSt.Value > 0)) then
                       qryItemPedidonPercIVA.Value := frmMenu.TBRound((((qryNFeXMLnValBaseICMSSt.Value / (qryNFeXMLnValTotal.Value + qryNFeXMLnValIPI.Value) ) * 100) - 100) ,0) ;

                   qryItemPedidonValBaseICMSSub.Value := qryNFeXMLnValBaseICMSSt.Value;
                   qryItemPedidonPercICMSSub.Value    := qryNFeXMLnpercAliqICMSSt.Value;
                   qryItemPedidonValICMSSub.Value     := qryNFeXMLnValICMSSt.Value;
                   qryItemPedidocCdSTICMS.Value       := qryNFeXMLcOrigemMercadoria.Value + qryNFeXMLcCdSt.Value;
                   qryItemPedidocNCM.Value            := qryNFeXMLcNCM.Value;
                   qryItemPedidonValBaseIPI.Value     := qryNFeXMLnValBaseIPI.Value;

                   if (qryNFeXMLnValTotal.Value > 0) then
                       qryItemPedidonPercBaseCalcIPI.Value := frmMenu.TBRound(((qryNFeXMLnValBaseIPI.Value / qryNFeXMLnValTotal.Value ) * 100),2);

                   qryItemPedidonPercIPI.Value        := qryNFeXMLnPercAliqIPI.Value;
                   qryItemPedidonValIPI.Value         := qryNFeXMLnValIPI.Value;
                   qryItemPedidocCdSTIPI.Value        := qryNFeXMLcCSTIPI.Value;
                   qryItemPedidocCFOPItemPedido.Value := qryNFeXMLcCFOP.Value;

                   if (qryItemPedidonPercRedBaseICMS.Value = 100) then
                       qryItemPedidonPercRedBaseICMS.Value := 0;

                   if (qryItemPedidonPercBaseCalcIPI.Value = 100) then
                       qryItemPedidonPercBaseCalcIPI.Value := 0;

                   qryItemPedidocCSTPIS.Value        := qryNFeXMLcCSTPIS.Value;
                   qryItemPedidonValBasePIS.Value    := qryNFeXMLnValBasePIS.Value;
                   qryItemPedidonAliqPIS.Value       := qryNFeXMLnAliqPIS.Value;
                   qryItemPedidonValPIS.Value        := qryNFeXMLnValPIS.Value;
                   qryItemPedidocCSTCOFINS.Value     := qryNFeXMLcCSTCOFINS.Value;
                   qryItemPedidonValBaseCOFINS.Value := qryNFeXMLnValBaseCOFINS.Value;
                   qryItemPedidonAliqCOFINS.Value    := qryNFeXMLnAliqCOFINS.Value;
                   qryItemPedidonValCOFINS.Value     := qryNFeXMLnValCOFINS.Value;
                   qryItemPedido.Post;
              end;

              qryNFeXML.Next;
          end;

          frmMenu.Connection.CommitTrans;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro ao importar itens do xml.');
          Raise;
      end;
  end;

  { -- se houve importa��o, recalcula saldo do pedido -- }
  if (qryItemPedido.RecordCount > 0) then
  begin
      cmdRecalcPed.Parameters.ParamByName('nPK').Value := nCdPedido;
      cmdRecalcPed.Execute;

      Close;
  end;
end;

procedure TfrmPedidoDevVenda_LeituraXML.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 7) then
        begin

            if (qryNFeXML.State = dsBrowse) then
                 qryNFeXML.Edit ;

            if ((qryNFeXML.State = dsInsert) or (qryNFeXML.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto) AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + IntToStr(nCdTipoPedido) + ')');

                if (nPK > 0) then
                begin
                    qryNFeXMLnCdProduto.Value := nPK ;

                    PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);
                    qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
                    qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
                end;
            end ;

        end ;

    end ;

   end;

end ;


procedure TfrmPedidoDevVenda_LeituraXML.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmPedidoDevVenda_LeituraXML.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}
end;

procedure TfrmPedidoDevVenda_LeituraXML.qryNFeXMLBeforeDelete(
  DataSet: TDataSet);
begin
    MensagemAlerta('Item n�o pode ser Excluido.');
    Abort;
    inherited;
end;

procedure TfrmPedidoDevVenda_LeituraXML.qryNFeXMLnCdProdutoChange(
  Sender: TField);
begin
  inherited;

  qryProduto.Close;
  PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

  qryNFeXMLcNmProduto.Value := '';
  qryNFeXMLcUnMedida.Value  := '';

  if (not qryProduto.Eof) then
  begin
      qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
      qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
  end;

end;

procedure TfrmPedidoDevVenda_LeituraXML.FormShow(Sender: TObject);
begin
  inherited;

  btCadProduto.Enabled      := ( frmMenu.fnValidaUsuarioAPL('FRMPRODUTOERP') ) ;
  btCadProdutoGrade.Enabled := ( frmMenu.fnValidaUsuarioAPL('FRMPRODUTO') ) ;

  { -- se terceiro for do mesmo grupo empresarial, remove visibilidade de colunas -- }
  if (qryTerceirocFlgTerceiroGrupo.Value = 1) then
  begin
      btAtualizarVinc.Visible := False;
      ToolButton8.Visible     := False;

      DBGridEh1.FieldColumns['cNmProdutoFornecedor'].Width := 500;
      DBGridEh1.FieldColumns['nCdProduto'].Visible         := False;
      DBGridEh1.FieldColumns['cNossoEAN'].Visible          := False;
      DBGridEh1.FieldColumns['cNmProduto'].Visible         := False;
      DBGridEh1.FieldColumns['cFlgItemAD'].Visible         := False;
  end;

  qryNFeXML.First;
  DBGridEh1.SetFocus;
end;

procedure TfrmPedidoDevVenda_LeituraXML.btAtualizarVincClick(Sender: TObject);
begin
  inherited;

  qryNFeXML.First;

  while not qryNFeXML.Eof do
  begin

      {-- s� vincula os produtos que n�o tem v�nculo --}
      if (qryNFeXMLnCdProduto.Value = 0) then
      begin

          qryNFeXML.Edit;
          
          {-- primeiro procura pelo c�digo EAN do produto para ver se encontra no cadastro de produtos --}
          if (qryNFeXMLcEAN.Value <> '') then
          begin

              qryProdutoEAN.Close;
              PosicionaQuery( qryProdutoEAN , qryNFeXMLcEAN.Value);

              if (not qryProdutoEAN.eof) then
              begin
                  qryNFeXMLnCdProduto.Value := qryProdutoEANnCdProduto.Value;
                  qryNFeXMLcNmProduto.Value := qryProdutoEANcNmProduto.Value;
                  qryNFeXMLcUnMedida.Value  := qryProdutoEANcUnidadeMedida.Value;
              end ;

          end ;

          {-- se n�o achou pelo c�digo de barras, tenta vinculando por c�digo fabricante --}
          if (qryNFeXMLnCdProduto.Value = 0) then
          begin

            qryProdutoFornecedor.Close;
            qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := qryNFeXMLcCdProdutoFornecedor.Value;
            qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
            qryProdutoFornecedor.Open;

            if not qryProdutoFornecedor.eof then
            begin

                qryProduto.Close;
                qryProduto.Parameters.ParamByName('nPk').Value := qryProdutoFornecedornCdProduto.Value;
                qryProduto.Open;

                if not qryProduto.eof then
                begin

                    qryNFeXMLnCdProduto.Value := qryProdutonCdProduto.Value;
                    qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
                    qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;

                end ;

            end ;

          end ;

          qryNFeXML.Post;

      end ;

      qryNFeXML.Next ;
      
  end ;

  qryNFeXML.First ;

end;

procedure TfrmPedidoDevVenda_LeituraXML.btCadProdutoClick(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmPedidoDevVenda_LeituraXML.btCadProdutoGradeClick(
  Sender: TObject);
var
  objForm : TfrmProduto;
begin
  inherited;

  objForm := TfrmProduto.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmPedidoDevVenda_LeituraXML.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProduto') then
  begin
      qryNFeXML.Edit;
      qryProduto.Close;

      PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

      if (not qryProduto.IsEmpty) then
      begin
          qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
          qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
      end
      else
      begin
          qryNFeXMLcNmProduto.Value := '';
          qryNFeXMLcUnMedida.Value  := '';
      end;
  end;
end;

procedure TfrmPedidoDevVenda_LeituraXML.qryNFeXMLBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close;
  PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

  if (not qryProduto.IsEmpty) then
  begin
      qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
      qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
  end
  else
  begin
      qryNFeXMLcNmProduto.Value := '';
      qryNFeXMLcUnMedida.Value  := '';
  end;
end;

procedure TfrmPedidoDevVenda_LeituraXML.SelecionarItens(
  cFlgSelecionar: integer);
begin
    qryNFeXML.DisableControls;

    qryNFeXML.First;

    // Atualiza a sele��o dos itens
    while not (qryNFeXML.Eof) do
    begin
        if (qryNFeXMLcFlgItemSelecionado.Value <> cFlgSelecionar) then
        begin
            qryNFeXML.Edit;
            qryNFeXMLcFlgItemSelecionado.Value := cFlgSelecionar;
            qryNFeXML.Post;
        end;

        qryNFeXML.Next;
    end;

    qryNFeXML.EnableControls;
end;

procedure TfrmPedidoDevVenda_LeituraXML.btnMarcarTodosClick(
  Sender: TObject);
begin
  inherited;

  SelecionarItens(1);
end;

procedure TfrmPedidoDevVenda_LeituraXML.btnDesmarcarTodosClick(
  Sender: TObject);
begin
  inherited;

  SelecionarItens(0);
end;

end.
