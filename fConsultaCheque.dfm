inherited frmConsultaCheque: TfrmConsultaCheque
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Consulta de Cheque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 185
    Width = 1152
    Height = 629
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 185
    Width = 1152
    Height = 629
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 625
    ClientRectLeft = 4
    ClientRectRight = 1148
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Resultado da Consulta'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1144
        Height = 601
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsResultado
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cConta'
            Footers = <>
            Width = 161
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cCNPJCPF'
            Footers = <>
            Width = 101
          end
          item
            EditButtons = <>
            FieldName = 'nValCheque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtDeposito'
            Footers = <>
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            Width = 198
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiroPort'
            Footers = <>
            Width = 169
          end
          item
            EditButtons = <>
            FieldName = 'dDtRemessaPort'
            Footers = <>
            Width = 114
          end
          item
            EditButtons = <>
            FieldName = 'dDtDevol'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1152
    Height = 156
    Align = alTop
    Caption = 'Filtro'
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 219
      Top = 32
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Portador'
    end
    object Label2: TLabel
      Tag = 1
      Left = 216
      Top = 56
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'CNPJ/CPF Emissor'
    end
    object Label3: TLabel
      Tag = 1
      Left = 234
      Top = 80
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Remessa'
    end
    object Label4: TLabel
      Tag = 1
      Left = 218
      Top = 104
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Simula'#231#227'o'
    end
    object Label5: TLabel
      Tag = 1
      Left = 336
      Top = 104
      Width = 260
      Height = 13
      Caption = '1= do menor para o maior / 2= do maior para o menor'
    end
    object Label6: TLabel
      Tag = 1
      Left = 222
      Top = 128
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Pagamento'
    end
    object Label7: TLabel
      Tag = 1
      Left = 442
      Top = 56
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Cheque'
    end
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 24
      Width = 193
      Height = 121
      Caption = 'Filtro'
      ItemIndex = 0
      Items.Strings = (
        'Todos disponiveis para dep'#243'sito'
        'Por terceiro portador'
        'Por CNPJ/CPF'
        'Por data remessa portador'
        'Simula'#231#227'o de Pagamento')
      TabOrder = 0
      OnClick = RadioGroup1Click
    end
    object edtTerceiro: TMaskEdit
      Left = 312
      Top = 24
      Width = 60
      Height = 21
      Enabled = False
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtTerceiroExit
      OnKeyDown = edtTerceiroKeyDown
    end
    object edtCNPJEmissor: TMaskEdit
      Left = 312
      Top = 48
      Width = 94
      Height = 21
      Enabled = False
      MaxLength = 14
      TabOrder = 2
    end
    object edtDtDeposito: TMaskEdit
      Left = 312
      Top = 72
      Width = 69
      Height = 21
      Enabled = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object edtTipoSimulacao: TMaskEdit
      Left = 312
      Top = 96
      Width = 16
      Height = 21
      Enabled = False
      EditMask = '!#;0;_'
      MaxLength = 1
      TabOrder = 5
    end
    object edtValorPagamento: TMaskEdit
      Left = 312
      Top = 120
      Width = 96
      Height = 21
      Enabled = False
      TabOrder = 6
      OnExit = edtValorPagamentoExit
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 384
      Top = 24
      Width = 128
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 520
      Top = 24
      Width = 536
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 8
    end
    object edtNumCheque: TMaskEdit
      Left = 520
      Top = 48
      Width = 96
      Height = 21
      EditMask = '!######;0; '
      MaxLength = 6
      TabOrder = 3
    end
  end
  inherited ImageList1: TImageList
    Left = 992
    Top = 272
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 808
    Top = 112
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 896
    Top = 112
  end
  object dsResultado: TDataSource
    DataSet = usp_Resultado
    Left = 680
    Top = 120
  end
  object usp_Resultado: TADOStoredProc
    Connection = frmMenu.Connection
    CursorType = ctStatic
    ProcedureName = 'SP_CONSULTA_CHEQUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@cTipoConsulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cCNPJCPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@iNrCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtDeposito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cTpSimulacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@nValPagamento'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdLoteLiq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 856
    Top = 112
    object usp_ResultadocConta: TStringField
      DisplayLabel = 'Dados Conta Cheque'
      FieldName = 'cConta'
      ReadOnly = True
      Size = 49
    end
    object usp_ResultadoiNrCheque: TIntegerField
      DisplayLabel = 'Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object usp_ResultadocCNPJCPF: TStringField
      DisplayLabel = 'CNPJ/CPF Emissor'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object usp_ResultadonValCheque: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ResultadodDtDeposito: TDateTimeField
      DisplayLabel = 'Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
      ReadOnly = True
    end
    object usp_ResultadocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Respons'#225'vel'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object usp_ResultadocNmTerceiroPort: TStringField
      DisplayLabel = 'Portador'
      FieldName = 'cNmTerceiroPort'
      ReadOnly = True
      Size = 50
    end
    object usp_ResultadodDtRemessaPort: TDateTimeField
      DisplayLabel = 'Data Rem. Portador'
      FieldName = 'dDtRemessaPort'
      ReadOnly = True
    end
    object usp_ResultadodDtDevol: TDateTimeField
      DisplayLabel = 'Data Devolu'#231#227'o'
      FieldName = 'dDtDevol'
    end
  end
end
