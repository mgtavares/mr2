unit fDepartamento_Segmento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmDepartamento_Segmento = class(TfrmProcesso_Padrao)
    dsSegmento: TDataSource;
    qryAux: TADOQuery;
    DBGridEh1: TDBGridEh;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    qrySegmentonCdIdExternoWeb: TIntegerField;
    procedure qrySegmentoBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdSubCategoria : integer ;
  end;

var
  frmDepartamento_Segmento: TfrmDepartamento_Segmento;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmDepartamento_Segmento.qrySegmentoBeforePost(
  DataSet: TDataSet);
begin

  if (qrySegmentocNmSegmento.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do segmento.') ;
      abort ;
  end ;

  qrySegmentocNmSegmento.Value := Uppercase(qrySegmentocNmSegmento.Value) ;

  if (qrySegmento.State = dsInsert) then
  begin
      qrySegmentonCdSubCategoria.Value := nCdSubCategoria ;
      qrySegmentonCdStatus.Value       := 1 ;

      qryAux.Close ;
      qryAux.Parameters.ParamByName('cNmSegmento').Value     := qrySegmentocNmSegmento.Value ;
      qryAux.Parameters.ParamByName('nCdSubCategoria').Value := nCdSubCategoria ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Segmento j� existente, verifique.') ;
          abort ;
      end ;

      qryAux.Close ;
  end ;

  if (qrySegmento.State = dsInsert) then
      qrySegmentonCdSegmento.Value := frmMenu.fnProximoCodigo('SEGMENTO') ;
      
  inherited;

end;

procedure TfrmDepartamento_Segmento.FormShow(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, IntToStr(nCdSubCategoria)) ;

end;

procedure TfrmDepartamento_Segmento.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDepartamento_Segmento.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

end.
