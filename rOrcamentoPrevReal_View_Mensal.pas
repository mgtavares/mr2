unit rOrcamentoPrevReal_View_Mensal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptOrcamentoPrevReal_View_Mensal = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_ORCAMENTO_PREVREAL: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel11: TQRLabel;
    SPREL_ORCAMENTO_PREVREALcNmOrcamento: TStringField;
    SPREL_ORCAMENTO_PREVREALcNmCC: TStringField;
    SPREL_ORCAMENTO_PREVREALnCdGrupoPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREALcNmGrupoPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREALnCdPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREALcNmPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREALnValPrevJaneiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealJaneiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercJaneiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevFevereiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealFevereiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercFevereiro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevMarco: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealMarco: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercMarco: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevAbril: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealAbril: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercAbril: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevMaio: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealMaio: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercMaio: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevJunho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealJunho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercJunho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevJulho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealJulho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercJulho: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevAgosto: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealAgosto: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercAgosto: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevSetembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealSetembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercSetembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevOutubro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealOutubro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercOutubro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevNovembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealNovembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercNovembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevDezembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealDezembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercDezembro: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValPrevTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREALnValRealTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREALnPercTotal: TFloatField;
    QRGroup1: TQRGroup;
    QRDBText5: TQRDBText;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel59: TQRLabel;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptOrcamentoPrevReal_View_Mensal: TrptOrcamentoPrevReal_View_Mensal;

implementation

{$R *.dfm}

procedure TrptOrcamentoPrevReal_View_Mensal.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
