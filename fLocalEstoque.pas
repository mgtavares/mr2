unit fLocalEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping;

type
  TfrmLocalEstoque = class(TfrmCadastro_Padrao)
    qryMasternCdLocalEstoque: TIntegerField;
    qryMastercNmLocalEstoque: TStringField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdGrupoEstoque: TIntegerField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryGrupoEstoque: TADOQuery;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryMastercNmEmpresa: TStringField;
    qryMastercNmLoja: TStringField;
    qryMastercNmGrupoEstoque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    qryGrupoProdutoLocalEstoque: TADOQuery;
    dsGrupoProdutoLocalEstoque: TDataSource;
    qryGrupoProdutoLocalEstoquenCdGrupoProdutoLocalEstoque: TAutoIncField;
    qryGrupoProdutoLocalEstoquenCdLocalEstoque: TIntegerField;
    qryGrupoProdutoLocalEstoquenCdGrupoProduto: TIntegerField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    qryGrupoProdutoLocalEstoquecNmGrupoProduto: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    Label6: TLabel;
    DBEdit9: TDBEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit10: TDBEdit;
    DataSource1: TDataSource;
    qryMastercFlgEstoqueDisp: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    qryUsuarioLocalEstoque: TADOQuery;
    qryUsuarioLocalEstoquenCdLocalEstoque: TIntegerField;
    qryUsuarioLocalEstoquenCdUsuario: TIntegerField;
    qryUsuarioLocalEstoquecNmUsuario: TStringField;
    dsUsuarioLocalEstoque: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryMastercEnderecoLocalEntrega: TStringField;
    Label7: TLabel;
    DBEdit11: TDBEdit;
    qryMasternCdCC: TIntegerField;
    Label8: TLabel;
    DBEdit12: TDBEdit;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    DBEdit13: TDBEdit;
    DataSource2: TDataSource;
    qryMastercFlgEstoqueBloqueado: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryUsuarioLocalEstoquenCdUsuarioLocalEstoque: TAutoIncField;
    qryMastercFlgInfluenciaMRP: TIntegerField;
    DBCheckBox3: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryGrupoProdutoLocalEstoqueBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9Exit(Sender: TObject);
    procedure qryUsuarioLocalEstoqueBeforePost(DataSet: TDataSet);
    procedure qryUsuarioLocalEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLocalEstoque: TfrmLocalEstoque;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmLocalEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LOCALESTOQUE' ;
  nCdTabelaSistema  := 40 ;
  nCdConsultaPadrao := 87 ;
end;

procedure TfrmLocalEstoque.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmLocalEstoque.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmLocalEstoque.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DBEdit3.Text) = '') then
            begin
               ShowMessage('Selecione a empresa.') ;
               DBEdit3.SetFocus ;
               abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Empresa.nCdEmpresa = ' + Trim(DBEdit3.Text));

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalEstoque.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(85);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalEstoque.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DbEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do local do estoque.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (Trim(DbEdit3.Text) = '') then
  begin
      MensagemAlerta('Informe a Empresa.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin

    if (Trim(DbEdit4.Text) = '') then
    begin
      MensagemAlerta('Informe a Loja.') ;
      DBEdit4.SetFocus ;
      abort ;
    end ;

  end ;

  if (Trim(DbEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o Grupo de Estoque.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (Trim(DbEdit13.Text) = '') then
  begin
      MensagemAlerta('Informe o centro de custo do local de estoque.') ;
      DBEdit13.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgEstoqueDisp.Value = 0) and (qryMastercFlgInfluenciaMRP.Value = 1) then
  begin
      MensagemAlerta('Para influenciar no MRP o local de estoque precisar estar marcado como dispon�vel.');
      abort;
  end ;

  inherited;
  
end;

procedure TfrmLocalEstoque.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryGrupoProdutoLocalEstoque, qryMasternCdLocalEstoque.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryUsuarioLocalEstoque, qryMasternCdLocalEstoque.AsString) ;
  PosicionaQuery(qryCC, qryMasternCdCC.asString) ;

end;

procedure TfrmLocalEstoque.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryGrupoProdutoLocalEstoque, qryMasternCdLocalEstoque.AsString) ;
  PosicionaQuery(qryUsuarioLocalEstoque, qryMasternCdLocalEstoque.AsString) ;

end;

procedure TfrmLocalEstoque.qryGrupoProdutoLocalEstoqueBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryGrupoProdutoLocalEstoquenCdLocalEstoque.Value := qryMasternCdLocalEstoque.Value ;

  inherited;

  if (qryGrupoProdutoLocalEstoque.State = dsInsert) then
      qryGrupoProdutoLocalEstoquenCdGrupoProdutoLocalEstoque.Value := frmMenu.fnProximoCodigo('GRUPOPRODUTOLOCALESTOQUE') ;

end;

procedure TfrmLocalEstoque.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryGrupoProdutoLocalEstoque.Close ;
  qryTerceiro.Close ;
  qryUsuarioLocalEstoque.Close ;
  qryCC.Close;
end;

procedure TfrmLocalEstoque.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryGrupoProdutoLocalEstoque.State = dsBrowse) then
             qryGrupoProdutoLocalEstoque.Edit ;


        if (qryGrupoProdutoLocalEstoque.State = dsInsert) or (qryGrupoProdutoLocalEstoque.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryGrupoProdutoLocalEstoquenCdGrupoProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalEstoque.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryGrupoProdutoLocalEstoque.Close ;
  qryTerceiro.Close ;
  qryUsuarioLocalEstoque.Close ;
  qryCC.Close;
end;

procedure TfrmLocalEstoque.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
    if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmLocalEstoque.FormShow(Sender: TObject);
begin
  inherited;

  DBEdit4.Enabled := True ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
      DBEdit4.Enabled := False ;

  cxPageControl1.ActivePageIndex := 0;

end;

procedure TfrmLocalEstoque.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(101);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalEstoque.DBEdit9Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DbEdit9.Text) ;
end;

procedure TfrmLocalEstoque.qryUsuarioLocalEstoqueBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioLocalEstoquecNmUsuario.Value = '') then
  begin
      MensagemAlerta('Informe o usu�rio.') ;
      abort ;
  end ;

  qryUsuarioLocalEstoquenCdLocalEstoque.Value := qryMasternCdLocalEstoque.Value ;

  if (qryUsuarioLocalEstoque.State = dsInsert) then
      qryUsuarioLocalEstoquenCdUsuarioLocalEstoque.Value := frmMenu.fnProximoCodigo('USUARIOLOCALESTOQUE') ;

end;

procedure TfrmLocalEstoque.qryUsuarioLocalEstoqueCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuario, qryUsuarioLocalEstoquenCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioLocalEstoquecNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmLocalEstoque.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioLocalEstoque.State = dsBrowse) then
             qryUsuarioLocalEstoque.Edit ;

        if (qryUsuarioLocalEstoque.State = dsInsert) or (qryUsuarioLocalEstoque.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioLocalEstoquenCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalEstoque.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit12.Text) ;
end;

procedure TfrmLocalEstoque.DBEdit12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29) ;

            If (nPK > 0) then
                qryMasternCdCC.Value := nPK ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmLocalEstoque) ;

end.
