unit fMonitorEmpenho_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DBCtrls, StdCtrls,
  Mask, ER2Lookup, DB, ADODB, Menus, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxCheckBox,
  cxLookAndFeelPainters, cxButtons;


type

  TfrmMonitorEmpenho_Itens = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    edtDtProx: TMaskEdit;
    edtDtUlt: TMaskEdit;
    dsLoja: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryItensEmpenho: TADOQuery;
    dsItensEmpenho: TDataSource;
    qryItensEmpenhonCdEstoqueEmpenho: TIntegerField;
    qryItensEmpenhonCdProduto: TIntegerField;
    qryItensEmpenhocNmProduto: TStringField;
    qryItensEmpenhonQtdeEstoqueEmpenho: TIntegerField;
    qryItensEmpenhocFlgSelecionado: TIntegerField;
    qryPopulaTemp: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    DBEdit1: TDBEdit;
    PopupMenu1: TPopupMenu;
    btEfetivarCancItem: TMenuItem;
    qryAux: TADOQuery;
    qryVerificaItemSelect: TADOQuery;
    SP_CANCELA_SALDO_ITEMREQUISICAO: TADOStoredProc;
    qryItensEmpenhonCdItemRequisicao: TIntegerField;
    qryItemPreSelecao: TADOQuery;
    qryItensEmpenhocNmTabTipoOrigemEmpenho: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryItensEmpenhocNmDepartamento: TStringField;
    qryItensEmpenhocNmCategoria: TStringField;
    qryItensEmpenhocNmMarca: TStringField;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueEmpenho: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgSelecionado: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabTipoOrigemEmpenho: TcxGridDBColumn;
    cxGrid1DBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategoria: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMarca: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    qryItensEmpenhodDtEmpenho: TStringField;
    qryGeraLista: TADOQuery;
    qryGeraListanCdOrdemSeparacao: TIntegerField;
    DBEdit2: TDBEdit;
    dsGeraLista: TDataSource;
    Label2: TLabel;
    qryItensEmpenhonQtdeConf: TIntegerField;
    qryItensEmpenhonQtdeDiverg: TIntegerField;
    cxGrid1DBTableView1nQtdeConf: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeDiverg: TcxGridDBColumn;
    panel: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    qryExcluiItemProcessado: TADOQuery;
    qryExcluiItemProcessadocMsgRetorno: TStringField;
    ToolSeparador2: TToolButton;
    btEfetivarCanc: TToolButton;
    btDesfazerCanc: TToolButton;
    btDesfazerCancItem: TMenuItem;
    N1: TMenuItem;
    btExcluirItemOrdem: TMenuItem;
    qryItensSelecionado: TADOQuery;
    qryItensSelecionadonCdEstoqueEmpenho: TIntegerField;
    qryItensSelecionadonCdItemRequisicao: TIntegerField;
    ToolSeparador3: TToolButton;
    qryItensEmpenhonCdUsuarioCancel: TIntegerField;
    qryItensEmpenhodDtCancel: TDateTimeField;
    qryItensEmpenhocMotivoCancel: TStringField;
    qryItensEmpenhocNmUsuario: TStringField;
    cxGrid1DBTableView1dDtCancel: TcxGridDBColumn;
    cxGrid1DBTableView1cMotivoCancel: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    qryItensSelecionadocMotivoCancel: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure prAtualizaTempItem(nCdEstoqueEmpenhado : Integer);
    procedure prCancelaEmpenho(nCdEstoqueEmpenhado, nCdItemRequisicao : Integer; cStatusCanc, cFlgPendCanc, cMotivoCancel : String);
    procedure prDesfazerCancelaEmpenho(nCdEstoqueEmpenhado : Integer);
    procedure btExcluirItemOrdemClick(Sender: TObject);
    procedure btEfetivarCancClick(Sender: TObject);
    procedure btEfetivarCancItemClick(Sender: TObject);
    procedure btDesfazerCancClick(Sender: TObject);
    procedure btDesfazerCancItemClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }

    nCdCentroDistribuicao : integer       ;
    cModoExib             : string        ;
    procedure ModoExibicao(cModo : string);
  end;

var
  frmMonitorEmpenho_Itens: TfrmMonitorEmpenho_Itens;

implementation

{$R *.dfm}

uses
  fMenu, rItensSeparacaoDistribuicao, fConferenciaProdutoPadrao, Math, fCaixa_Recebimento;

procedure TfrmMonitorEmpenho_Itens.ModoExibicao(cModo : string);
begin
  { --          modo de exibi��o           -- }
  { -- I : itens pendentes de atendimento  -- }
  { -- V : visualiza��o de ordem de separ. -- }
  { -- C : cancelamento de empenhos        -- }

  cModoExib := cModo;

  if (cModo = 'I') then
  begin
      DBEdit2.Visible            := False;
      Label2.Visible             := False;
      btEfetivarCanc.Caption     := 'Cancelar Empenho';
      btEfetivarCancItem.Caption := 'Cancelar este empenho';
      label4.Top                 := 26;
      ER2LookupMaskEdit2.Top     := 18;
      DBEdit1.Top                := 18;
      label1.Top                 := 51;
      label5.Top                 := 51;
      edtDtProx.Top              := 43;
      edtDtUlt.Top               := 43;
      GroupBox1.Height           := 76;
      Exit;
  end;

  if (cModo = 'V') then
  begin
      desativaMaskEdit(edtDtProx);
      Caption                                    := 'Visualizar Ordem de Separa��o';
      ToolButton1.Visible                        := False;
      ToolButton3.Visible                        := False;
      N1.Visible                                 := False;
      btEfetivarCanc.Visible                     := False;
      btEfetivarCancItem.Visible                 := False;
      btExcluirItemOrdem.Visible                 := True;
      cxGrid1DBTableView1cFlgSelecionado.Visible := False;
      panel.Visible                              := False;
      cxGrid1DBTableView1nQtdeConf.Visible       := True ;
      cxGrid1DBTableView1nQtdeDiverg.Visible     := True ;
      cxGrid1DBTableView1.OptionsData.Editing    := False;
      Exit;
  end;

  if (cModo = 'C') then
  begin
      Caption := 'Efetiva��o de Cancelamento';

      if (frmMenu.LeParametro('CONFCANCEMP') <> 'S') then
      begin
          btDesfazerCanc.Caption     := 'Cancelar Empenho';
          btDesfazerCancItem.Caption := 'Cancelar este empenho';
      end;

      ToolSeparador3.Visible     := True;
      btDesfazerCanc.Visible     := True;
      btDesfazerCancItem.Visible := True;
      Label2.Visible             := False;
      DBEdit2.Visible            := False;
      Label5.Visible             := False;
      edtDtUlt.Visible           := False;
      Label1.Visible             := False;
      edtDtProx.Visible          := False;
      GroupBox1.Height           := 76;
      
      cxGrid1DBTableView1cNmDepartamento.Visible := False;
      cxGrid1DBTableView1cNmCategoria.Visible    := False;
      cxGrid1DBTableView1cNmMarca.Visible        := False;
      cxGrid1DBTableView1dDtCancel.Visible       := True;
      cxGrid1DBTableView1cMotivoCancel.Visible   := True;
      cxGrid1DBTableView1cNmUsuario.Visible      := True;
      Exit;
  end;
end;

procedure TfrmMonitorEmpenho_Itens.ToolButton1Click(Sender: TObject);
var
  ObjRel : TrelItensSeparacaoDistribuicao_view;
begin
  //inherited;
  if qryItensEmpenho.IsEmpty then
      exit;

  if edtDtProx.Text = '  /  /  ' then
  begin
      MensagemAlerta('Data da pr�xima entrega n�o informado.');
      Abort;
  end;

  { -- verifica se algum item selecionado esta vinculado a outra ordem de separa��o -- }
  { -- ou se o mesmo entrou em processo de cancelamento pelo usu�rio                -- }
  { -- se item estiver nesta situa��o exclui item da temp e informa ao usu�rio       -- }
  qryExcluiItemProcessado.Close;
  qryExcluiItemProcessado.Parameters.ParamByName('cAcao').Value   := 'N';
  qryExcluiItemProcessado.Parameters.ParamByName('nCdItem').Value := 0;
  qryExcluiItemProcessado.Open;

  if (qryExcluiItemProcessadocMsgRetorno.Value <> 'Ok') then
  begin
      qryItensEmpenho.Requery;

      MensagemErro(qryExcluiItemProcessadocMsgRetorno.Value);
      
      Abort;
  end;

  case MessageDlg('Confirma a inclus�o da ordem de separa��o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort;
  end;

  try
      frmMenu.Connection.BeginTrans;

      qryGeraLista.Close;
      qryGeraLista.Parameters.ParamByName('nCdEmpresa').Value            := frmMenu.nCdEmpresaAtiva;
      qryGeraLista.Parameters.ParamByName('nCdLoja').Value               := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);
      qryGeraLista.Parameters.ParamByName('nCdCentroDistribuicao').Value := nCdCentroDistribuicao;
      qryGeraLista.Parameters.ParamByName('nCdUsuario').Value            := frmMenu.nCdUsuarioLogado;
      qryGeraLista.Parameters.ParamByName('dDtEntrega').Value            := edtDtProx.Text;
      qryGeraLista.Open;

      frmMenu.Connection.CommitTrans;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;

  ShowMessage('Ordem de separa��o gerada com sucesso.');

  try

      ObjRel := TrelItensSeparacaoDistribuicao_view.Create(nil);

      ObjRel.qryResultado.Close;
      ObjRel.qryResultado.Parameters.ParamByName('nCdOrdemSeparacao').Value := qryGeraListanCdOrdemSeparacao.Value;
      ObjRel.qryResultado.Open;

      ObjRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      ObjRel.QuickRep1.PreviewModal;

      FreeAndNil(ObjRel);

      Close;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end;
end;

procedure TfrmMonitorEmpenho_Itens.cxButton1Click(Sender: TObject);
begin

  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE #Temp_MonitorEmpenho_Itens SET cFlgSelecionado = 1 WHERE cFlgSelecionado = 0');
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.');
      raise
  end;

  qryItensEmpenho.Requery();

end;

procedure TfrmMonitorEmpenho_Itens.cxButton2Click(Sender: TObject);
begin

  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE #Temp_MonitorEmpenho_Itens SET cFlgSelecionado = 0 WHERE cFlgSelecionado = 1');
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.');
      raise
  end;

  qryItensEmpenho.Requery();

end;

procedure TfrmMonitorEmpenho_Itens.prAtualizaTempItem(nCdEstoqueEmpenhado : Integer);
begin
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('DELETE FROM #Temp_MonitorEmpenho_Itens WHERE nCdEstoqueEmpenho = ' + IntToStr(nCdEstoqueEmpenhado));
  qryAux.ExecSQL;

  qryItensEmpenho.Close;
  qryItensEmpenho.Open;
end;

procedure TfrmMonitorEmpenho_Itens.prCancelaEmpenho(nCdEstoqueEmpenhado, nCdItemRequisicao : Integer; cStatusCanc, cFlgPendCanc, cMotivoCancel : String);
begin
  inherited;
  
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE EstoqueEmpenhado                                                 ');
  qryAux.SQL.Add('   SET cFlgPendCanc        = ' + cFlgPendCanc                            );
  qryAux.SQL.Add('      ,nCdStatus           = ' + cStatusCanc                             );
  qryAux.SQL.Add('      ,dDtCancel           = GETDATE()                                  ');
  qryAux.SQL.Add('      ,cMotivoCancel       = ' + #39 + AnsiUpperCase(cMotivoCancel) + #39);
  qryAux.SQL.Add('      ,nCdUsuarioCancel    = ' + IntToStr(frmMenu.nCdUsuarioLogado)      );
  qryAux.SQL.Add(' WHERE nCdEstoqueEmpenhado = ' + IntToStr(nCdEstoqueEmpenhado)           );
  qryAux.ExecSQL;

  if (nCdItemRequisicao > 0) then
  begin
      SP_CANCELA_SALDO_ITEMREQUISICAO.Close;
      SP_CANCELA_SALDO_ITEMREQUISICAO.Parameters.ParamByName('@nCdItemRequisicao').Value := nCdItemRequisicao;
      SP_CANCELA_SALDO_ITEMREQUISICAO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_CANCELA_SALDO_ITEMREQUISICAO.ExecProc;
  end;
end;

procedure TfrmMonitorEmpenho_Itens.prDesfazerCancelaEmpenho(nCdEstoqueEmpenhado : Integer);
begin
  inherited;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE EstoqueEmpenhado                                      ');
  qryAux.SQL.Add('   SET cFlgPendCanc        = 0                               ');
  qryAux.SQL.Add('      ,dDtCancel           = NULL                            ');
  qryAux.SQL.Add('      ,cMotivoCancel       = NULL                            ');
  qryAux.SQL.Add('      ,nCdUsuarioCancel    = NULL                            ');
  qryAux.SQL.Add(' WHERE nCdEstoqueEmpenhado = ' + IntToStr(nCdEstoqueEmpenhado));
  qryAux.ExecSQL;
end;

procedure TfrmMonitorEmpenho_Itens.btEfetivarCancClick(Sender: TObject);
var
  objAutorizacao : TfrmCaixa_Recebimento;
  cMessageDialog : String;
  cMotivoCancel  : String;
  cStatusCanc    : String;
  cFlgPendCanc   : String;
  cAcao          : String;
begin
  inherited;

  qryItensSelecionado.Close;
  qryItensSelecionado.Open;

  if (qryItensSelecionado.IsEmpty) then
  begin
      MensagemAlerta('Selecione pelo menos um item para efetivar seu cancelamento.');
      Abort;
  end;

  try
      if (frmMenu.LeParametro('CONFCANCEMP') = 'S') then
      begin
          cStatusCanc    := '1';
          cFlgPendCanc   := '1';
          cAcao          := 'P';
          cMessageDialog := 'Confirma o envio do(s) empenho(s) selecionado(s) para o monitor de pr� cancelamento ?';

          if (cModoExib = 'I') then
          begin
              InputQuery('Motivo Cancelamento Empenho','Informe o motivo de cancelamento:', cMotivoCancel);

              if (Trim(cMotivoCancel) = '') then
              begin
                  MensagemAlerta('Motivo de cancelamento n�o informado.');
                  Exit;
              end;
          end;

          if (cModoExib = 'C') then
          begin
              cStatusCanc    := '2';
              cFlgPendCanc   := '0';
              cAcao          := 'C';
              cMessageDialog := 'Confirma o cancelamento do(s) empenho(s) selecionado(s) ?';
          end;
      end
      else
      begin
          cStatusCanc    := '2';
          cFlgPendCanc   := '0';
          cAcao          := 'C';
          cMessageDialog := 'Confirma o cancelamento do(s) empenho(s) selecionado(s) ?';
      end;

      case MessageDlg(cMessageDialog, mtConfirmation, [mbYes,mbNo], 0) of
          mrNo : Exit;
      end;

      objAutorizacao := TfrmCaixa_Recebimento.Create(nil);

      if (not objAutorizacao.AutorizacaoGerente(70)) then
      begin
          MensagemAlerta('Autentica��o Falhou');
          Exit;
      end;

      try
          { -- verifica se algum item selecionado esta vinculado a outra ordem de separa��o -- }
          { -- ou se o mesmo entrou em processo de cancelamento pelo usu�rio                -- }
          { -- se item estiver nesta situa��o exclui item da temp e informa ao usu�rio      -- }
          qryExcluiItemProcessado.Close;
          qryExcluiItemProcessado.Parameters.ParamByName('cAcao').Value   := cAcao;
          qryExcluiItemProcessado.Parameters.ParamByName('nCdItem').Value := 0;
          qryExcluiItemProcessado.Open;

          if (qryExcluiItemProcessadocMsgRetorno.Value <> 'Ok') then
          begin
              qryItensEmpenho.Requery;

              MensagemErro(qryExcluiItemProcessadocMsgRetorno.Value);

              Exit;
          end;

          frmMenu.Connection.BeginTrans;

          qryItensSelecionado.First;

          while (not qryItensSelecionado.Eof)do
          begin
              if (cMotivoCancel = '') then
                  cMotivoCancel := qryItensSelecionadocMotivoCancel.Value;

              prCancelaEmpenho(qryItensSelecionadonCdEstoqueEmpenho.Value, qryItensSelecionadonCdItemRequisicao.Value, cStatusCanc, cFlgPendCanc, cMotivoCancel);

              prAtualizaTempItem(qryItensSelecionadonCdEstoqueEmpenho.Value);

              if (cModoExib = 'C') then
                  cMotivoCancel := '';

              qryItensSelecionado.Next;
          end;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Itens processados com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objAutorizacao);
  end;
end;

procedure TfrmMonitorEmpenho_Itens.btEfetivarCancItemClick(Sender: TObject);
var
  objAutorizacao : TfrmCaixa_Recebimento;
  cMessageDialog : String;
  cMotivoCancel  : String;
  cStatusCanc    : String;
  cFlgPendCanc   : String;
  cAcao          : String;
begin
  inherited;

  if (qryItensEmpenho.IsEmpty) then
      Exit;

  try
      if (frmMenu.LeParametro('CONFCANCEMP') = 'S') then
      begin
          cStatusCanc    := '1';
          cFlgPendCanc   := '1';
          cAcao          := 'P';
          cMessageDialog := 'Confirma o envio do empenho selecionado para o monitor de pr� cancelamento ?';

          if (cModoExib = 'I') then
          begin
              InputQuery('Motivo Cancelamento Empenho','Informe o motivo de cancelamento:', cMotivoCancel);

              if (Trim(cMotivoCancel) = '') then
              begin
                  MensagemAlerta('Motivo de cancelamento n�o informado.');
                  Exit;
              end;
          end;

          if (cModoExib = 'C') then
          begin
              cStatusCanc    := '2';
              cFlgPendCanc   := '0';
              cAcao          := 'C';
              cMessageDialog := 'Confirma o cancelamento do empenho selecionado ?';
          end;
      end
      else
      begin
          cStatusCanc    := '2';
          cFlgPendCanc   := '0';
          cAcao          := 'C';
          cMessageDialog := 'Confirma o cancelamento do empenho selecionado ?';
      end;

      case MessageDlg(cMessageDialog, mtConfirmation, [mbYes,mbNo], 0) of
          mrNo : Exit;
      end;
      
      objAutorizacao := TfrmCaixa_Recebimento.Create(nil);

      if (not objAutorizacao.AutorizacaoGerente(70)) then
      begin
          MensagemAlerta('Autentica��o Falhou');
          Exit;
      end;

      try
          { -- verifica se algum item selecionado esta vinculado a outra ordem de separa��o -- }
          { -- ou se o mesmo entrou em processo de cancelamento pelo usu�rio                -- }
          { -- se item estiver nesta situa��o exclui item da temp e informa ao usu�rio       -- }
          qryExcluiItemProcessado.Close;
          qryExcluiItemProcessado.Parameters.ParamByName('cAcao').Value   := cAcao;
          qryExcluiItemProcessado.Parameters.ParamByName('nCdItem').Value := qryItensEmpenhonCdEstoqueEmpenho.Value;
          qryExcluiItemProcessado.Open;

          if (qryExcluiItemProcessadocMsgRetorno.Value <> 'Ok') then
          begin
              qryItensEmpenho.Requery;

              MensagemErro(qryExcluiItemProcessadocMsgRetorno.Value);

              Exit;
          end;

          frmMenu.Connection.BeginTrans;

          if (cMotivoCancel = '') then
              cMotivoCancel := qryItensEmpenhocMotivoCancel.Value;

          prCancelaEmpenho(qryItensEmpenhonCdEstoqueEmpenho.Value, qryItensEmpenhonCdItemRequisicao.Value, cStatusCanc, cFlgPendCanc, cMotivoCancel);

          prAtualizaTempItem(qryItensEmpenhonCdEstoqueEmpenho.Value);

          frmMenu.Connection.CommitTrans;

          ShowMessage('Item processado com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objAutorizacao);
  end;
end;

procedure TfrmMonitorEmpenho_Itens.btDesfazerCancClick(Sender: TObject);
begin
  inherited;

  qryItensSelecionado.Close;
  qryItensSelecionado.Open;

  if (qryItensSelecionado.IsEmpty) then
  begin
      MensagemAlerta('Selecione pelo menos um item para desfazer seu pr� cancelamento.');
      Abort;
  end;

  case MessageDlg('Confirma a remo��o do(s) empenho(s) selecionado(s) do pr� cancelamento ?', mtConfirmation, [mbYes,mbNo], 0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;

      qryItensSelecionado.First;

      while (not qryItensSelecionado.Eof)do
      begin
          prDesfazerCancelaEmpenho(qryItensSelecionadonCdEstoqueEmpenho.Value);
              
          prAtualizaTempItem(qryItensSelecionadonCdEstoqueEmpenho.Value);

          qryItensSelecionado.Next;
      end;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Itens processados com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmMonitorEmpenho_Itens.btDesfazerCancItemClick(
  Sender: TObject);
begin
  inherited;

  if (qryItensEmpenho.IsEmpty) then
      Exit;

  case MessageDlg('Confirma a remo��o do empenho selecionado do pr� cancelamento ?', mtConfirmation, [mbYes,mbNo], 0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;

      prDesfazerCancelaEmpenho(qryItensEmpenhonCdEstoqueEmpenho.Value);

      prAtualizaTempItem(qryItensEmpenhonCdEstoqueEmpenho.Value);

      frmMenu.Connection.CommitTrans;

      ShowMessage('Item processado com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmMonitorEmpenho_Itens.btExcluirItemOrdemClick(
  Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma exclus�o do item selecionado da ordem de separa��o ?', mtConfirmation, [mbYes,mbNo], 0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE EstoqueEmpenhado SET nCdOrdemSeparacao = NULL  WHERE nCdEstoqueEmpenhado = ' + IntToStr(qryItensEmpenhonCdEstoqueEmpenho.Value));
      qryAux.ExecSQL;

      prAtualizaTempItem(qryItensEmpenhonCdEstoqueEmpenho.Value);

      frmMenu.Connection.CommitTrans;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;

  qryItensEmpenho.Close;
  qryItensEmpenho.Open;

  { -- se n�o existe mais item na ordem ativa, exclui a mesma -- }
  if (qryItensEmpenho.IsEmpty) then
  begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('DELETE FROM OrdemSeparacao WHERE nCdOrdemSeparacao = ' + DBEdit2.text);
      qryAux.ExecSQL;

      Close;
  end;
end;

end.
