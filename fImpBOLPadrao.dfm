object frmImpBOLPadrao: TfrmImpBOLPadrao
  Left = 282
  Top = 198
  Width = 870
  Height = 500
  Caption = 'Impress'#227'o Boleto - Padr'#227'o'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxButton1: TcxButton
    Left = 168
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Testar'
    TabOrder = 0
    OnClick = cxButton1Click
  end
  object cxButton2: TcxButton
    Left = 288
    Top = 288
    Width = 75
    Height = 25
    Caption = 'cxButton2'
    TabOrder = 1
  end
  object qryModImpBOL: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ModImpBOL'
      'WHERE nCdBanco = :nPK')
    Left = 208
    Top = 24
    object qryModImpBOLnCdModImpBOL: TIntegerField
      FieldName = 'nCdModImpBOL'
    end
    object qryModImpBOLcNmModImpBOL: TStringField
      FieldName = 'cNmModImpBOL'
      Size = 50
    end
    object qryModImpBOLnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryModImpBOLcQuery: TMemoField
      FieldName = 'cQuery'
      BlobType = ftMemo
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 272
    Top = 24
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 208
    Top = 112
  end
  object qryCampos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CampoModImpBOL'
      'WHERE nCdModImpBOL = :nPK'
      'ORDER BY iSequencia')
    Left = 264
    Top = 112
    object qryCamposnCdCampoModImpBOL: TAutoIncField
      FieldName = 'nCdCampoModImpBOL'
      ReadOnly = True
    end
    object qryCamposnCdModImpBOL: TIntegerField
      FieldName = 'nCdModImpBOL'
    end
    object qryCamposiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryCamposcNmCampo: TStringField
      FieldName = 'cNmCampo'
      Size = 30
    end
    object qryCamposcDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 50
    end
    object qryCamposiLinha: TIntegerField
      FieldName = 'iLinha'
    end
    object qryCamposiColuna: TIntegerField
      FieldName = 'iColuna'
    end
    object qryCamposcTipoDado: TStringField
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryCamposcTamanhoFonte: TStringField
      FieldName = 'cTamanhoFonte'
      Size = 10
    end
    object qryCamposcEstiloFonte: TStringField
      FieldName = 'cEstiloFonte'
      Size = 10
    end
    object qryCamposcLPP: TStringField
      FieldName = 'cLPP'
      Size = 10
    end
  end
  object RDprint1: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = False
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'EDGAR DE SOUZA'
    RegistroUsuario.SerieProduto = 'SINGLE-0708/01624'
    RegistroUsuario.AutorizacaoKey = 'BWVI-4846-SAHU-8864-PAPQ'
    About = 'RDprint 4.0e - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = True
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    Left = 112
    Top = 88
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,cCedenteAgencia'
      '      ,cCedenteAgenciaDigito'
      '      ,cCedenteConta'
      '      ,cCedenteContaDigito'
      '      ,cCedenteCodCedente'
      '      ,cDiretorioArquivoLicenca'
      '      ,cPrimeiraInstrucaoBoleto'
      '      ,cSegundaInstrucaoBoleto'
      '      ,iUltimoBoleto'
      '      ,cFlgAceite'
      '      ,iDiasProtesto'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK')
    Left = 416
    Top = 224
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariacCedenteAgencia: TStringField
      FieldName = 'cCedenteAgencia'
      Size = 5
    end
    object qryContaBancariacCedenteAgenciaDigito: TStringField
      FieldName = 'cCedenteAgenciaDigito'
      FixedChar = True
      Size = 1
    end
    object qryContaBancariacCedenteConta: TStringField
      FieldName = 'cCedenteConta'
      Size = 10
    end
    object qryContaBancariacCedenteContaDigito: TStringField
      FieldName = 'cCedenteContaDigito'
      FixedChar = True
      Size = 1
    end
    object qryContaBancariacCedenteCodCedente: TStringField
      FieldName = 'cCedenteCodCedente'
    end
    object qryContaBancariacDiretorioArquivoLicenca: TStringField
      FieldName = 'cDiretorioArquivoLicenca'
      Size = 50
    end
    object qryContaBancariacPrimeiraInstrucaoBoleto: TStringField
      FieldName = 'cPrimeiraInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancariacSegundaInstrucaoBoleto: TStringField
      FieldName = 'cSegundaInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancariaiUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
    object qryContaBancariacFlgAceite: TIntegerField
      FieldName = 'cFlgAceite'
    end
    object qryContaBancariaiDiasProtesto: TIntegerField
      FieldName = 'iDiasProtesto'
    end
  end
  object qryEndereco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@nCdEndereco int'
      ''
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      ''
      'IF (@nCdEndereco IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdEndereco = nCdEndereco'
      #9'  FROM Endereco'
      #9' WHERE nCdTerceiro = @nCdTerceiro'
      #9'   AND nCdTipoEnd  = 2 -- Cobran'#231'a'
      #9'   AND nCdStatus   = 1 -- Ativo'
      ''
      #9'IF (@nCdEndereco IS NULL) '
      #9'BEGIN'
      ''
      #9#9'SELECT TOP 1 @nCdEndereco = nCdEndereco'
      #9#9'  FROM Endereco'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      #9#9'   AND nCdStatus   = 1 -- Ativo'
      #9#9' ORDER BY nCdTipoEnd'
      ''
      #9'END'
      ''
      'END'
      ''
      'SELECT nCdEndereco'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCNPJCPF'
      '      ,cEndereco + '#39', '#39' +  Convert(varchar,iNumero) cEndereco'
      '      ,cBairro'
      '      ,cCidade'
      '      ,cUF'
      '      ,Substring(cCep,1,5) + '#39'-'#39' + Substring(cCep,6,3) cCEP'
      '  FROM Endereco'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Endereco.nC' +
        'dTerceiro'
      ' WHERE nCdEndereco = @nCdEndereco')
    Left = 448
    Top = 224
    object qryEndereconCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
    end
    object qryEndereconCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryEnderecocEndereco: TStringField
      FieldName = 'cEndereco'
      ReadOnly = True
      Size = 82
    end
    object qryEnderecocBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryEnderecocCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryEnderecocUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryEnderecocCEP: TStringField
      FieldName = 'cCEP'
      ReadOnly = True
      Size = 9
    end
    object qryEnderecocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryEnderecocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.nCdTerceiro'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,cFlgBoletoImpresso'
      '  FROM Titulo'
      ' WHERE nCdTitulo = :nPK')
    Left = 416
    Top = 256
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
  end
  object qryInstrucaoContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TituloInstrucaoBoleto'#39') IS NULL)'
      'BEGIN'
      
        '   '#9'CREATE TABLE #Temp_TituloInstrucaoBoleto(nCdTituloInstrucaoB' +
        'oleto int PRIMARY KEY identity'
      
        '                                            ,cNmMensagem        ' +
        '      varchar(100)'
      
        '                                            ,cFlgMensagemPadrao ' +
        '      int default 0 not null )'
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_TituloInstrucaoBoleto'
      ' WHERE cNmMensagem IS NOT NULL'
      '   AND cFlgMensagemPadrao = 1 ')
    Left = 447
    Top = 255
    object qryInstrucaoContaBancarianCdTituloInstrucaoBoleto: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'nCdTituloInstrucaoBoleto'
      ReadOnly = True
    end
    object qryInstrucaoContaBancariacNmMensagem: TStringField
      DisplayLabel = 'Instru'#231#227'o Banc'#225'ria'
      FieldName = 'cNmMensagem'
      Size = 100
    end
    object qryInstrucaoContaBancariacFlgMensagemPadrao: TIntegerField
      FieldName = 'cFlgMensagemPadrao'
    end
  end
  object qryTabelaJuros: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nPercMulta'
      '      ,nPercJuroDia '
      '  FROM TabelaJuros'
      ' WHERE nCdEspTit = dbo.fn_LeParametro('#39'CDESPTIT-BOLETO'#39')'
      '   AND dDtValidade > GetDate() - 1')
    Left = 416
    Top = 288
    object qryTabelaJurosnPercMulta: TBCDField
      FieldName = 'nPercMulta'
      Precision = 12
      Size = 2
    end
    object qryTabelaJurosnPercJuroDia: TBCDField
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
  end
end
