unit fSugerirCheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ExtCtrls, ImgList, ComCtrls, ToolWin,
  DB, ADODB, Mask;

type
  TfrmSugerirCheques = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    edtTipoSimulacao: TMaskEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtValorPagamento: TMaskEdit;
    Label6: TLabel;
    usp_Resultado: TADOStoredProc;
    usp_ResultadocConta: TStringField;
    usp_ResultadoiNrCheque: TIntegerField;
    usp_ResultadocCNPJCPF: TStringField;
    usp_ResultadonValCheque: TBCDField;
    usp_ResultadodDtDeposito: TDateTimeField;
    usp_ResultadocNmTerceiro: TStringField;
    usp_ResultadocNmTerceiroPort: TStringField;
    usp_ResultadodDtRemessaPort: TDateTimeField;
    qryChequeLote: TADOQuery;
    qryChequeLotenCdChequeLoteLiq: TAutoIncField;
    qryChequeLotenCdLoteLiq: TIntegerField;
    qryChequeLotenCdBanco: TIntegerField;
    qryChequeLotecAgencia: TStringField;
    qryChequeLotecConta: TStringField;
    qryChequeLotecDigito: TStringField;
    qryChequeLotecCNPJCPF: TStringField;
    qryChequeLotenValCheque: TBCDField;
    qryChequeLotedDtDeposito: TDateTimeField;
    qryChequeLoteiNrCheque: TIntegerField;
    procedure edtValorPagamentoExit(Sender: TObject);
    procedure SugerirCheques(cTipoLiq: String; nValPagto: Double);
    procedure ToolButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    nCdLoteLiq : Integer ;
  end;

var
  frmSugerirCheques: TfrmSugerirCheques;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmSugerirCheques.edtValorPagamentoExit(Sender: TObject);
begin
  inherited;
  If (trim(edtValorPagamento.Text) <> '') then
      try
          edtValorPagamento.text := formatcurr('0.00',StrToFloat(StringReplace(edtValorPagamento.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;
      except
          raise ;
          exit ;
      end ;

end;

procedure TfrmSugerirCheques.SugerirCheques(cTipoLiq: String; nValPagto: Double);
begin

    if (cTipoLiq = 'D') then
        RadioGroup1.ItemIndex := 0 ;

    if (cTipoLiq = 'P') then
        RadioGroup1.ItemIndex := 1 ;

    if (nValPagto > 0) then
        edtValorPagamento.Text := FloatToStr(nValPagto) ;

    edtTipoSimulacao.Text := '2' ;

    Self.ShowModal ;

end ;

procedure TfrmSugerirCheques.ToolButton1Click(Sender: TObject);
begin
  inherited;

  usp_Resultado.Close ;

  // todos os disponiveis para deposito
  If (RadioGroup1.ItemIndex = 0) then
      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '5' ;

  // simulacao pagamento
  If (RadioGroup1.ItemIndex = 1) then
  begin
      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '6' ;
      usp_Resultado.Parameters.ParamByName('@cTpSimulacao').Value  := Trim(edtTipoSimulacao.Text) ;
      usp_Resultado.Parameters.ParamByName('@nValPagamento').Value := StrToFloat(Trim(edtValorPagamento.Text)) ;
  end ;

  usp_Resultado.Parameters.ParamByName('@nCdLoteLiq').Value    := nCdLoteLiq ;
  
  frmMenu.Connection.BeginTrans ;

  try
    usp_Resultado.ExecProc
  except
    frmMenu.Connection.RollbackTrans;
    ShowMessage('Erro no processamento') ;
    raise;
    exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;
  
end;

end.
