unit rCampanhaPromoc_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptCampanhaPromoc_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand5: TQRBand;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TStringField;
    qryCampanhaPromoccChaveCampanha: TStringField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccDescricaoCampanha: TMemoField;
    qryCampanhaPromocdDtValidadeIni: TDateTimeField;
    qryCampanhaPromocdDtValidadeFim: TDateTimeField;
    qryCampanhaPromoccFlgSituacao: TStringField;
    qryCampanhaPromoccLojaParticipante: TMemoField;
    qryCampanhaPromocnCdProduto: TIntegerField;
    qryCampanhaPromoccNmProduto: TStringField;
    qryCampanhaPromocnQtde: TBCDField;
    qryCampanhaPromoccFlgTipoValor: TStringField;
    qryCampanhaPromocnValor: TBCDField;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText4: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptCampanhaPromoc_view: TrptCampanhaPromoc_view;

implementation

{$R *.dfm}

end.
