inherited frmAliquotaPisCofins: TfrmAliquotaPisCofins
  Left = 276
  Top = 170
  Caption = 'frmAliquotaPisCofins'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Pis/Cofins'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 776
        Height = 407
        Align = alClient
        DataSource = dsPisCofins
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdEmpresa'
            Footers = <>
            Title.Caption = 'Empresa|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmEmpresa'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Empresa|Nome'
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Title.Caption = 'Loja|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Loja|Nome'
          end
          item
            EditButtons = <>
            FieldName = 'nValAliquotaPis'
            Footers = <>
            Title.Caption = 'Al'#237'quota|Pis'
          end
          item
            EditButtons = <>
            FieldName = 'nValAliquotaCofins'
            Footers = <>
            Title.Caption = 'Al'#237'quota|Cofins'
          end>
      end
    end
  end
  object qryPisCofins: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPisCofinsBeforePost
    OnCalcFields = qryPisCofinsCalcFields
    Parameters = <
      item
        Name = 'nPk'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM  AliquotaPisCofins'
      'WHERE nCdEmpresa = :nPk'
      'ORDER BY nCdEmpresa, nCdLoja')
    Left = 496
    Top = 212
    object qryPisCofinsnCdAliquotaPisCofins: TIntegerField
      FieldName = 'nCdAliquotaPisCofins'
    end
    object qryPisCofinsnCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryPisCofinsnCdLoja: TIntegerField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLoja'
    end
    object qryPisCofinsnValAliquotaPis: TBCDField
      DisplayLabel = 'Aliquota Pis'
      FieldName = 'nValAliquotaPis'
      Precision = 12
      Size = 2
    end
    object qryPisCofinsnValAliquotaCofins: TBCDField
      DisplayLabel = 'Al'#237'quota Cofins'
      FieldName = 'nValAliquotaCofins'
      Precision = 12
      Size = 2
    end
    object qryPisCofinscNmEmpresa: TStringField
      DisplayLabel = 'Nome Empresa'
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 30
      Calculated = True
    end
    object qryPisCofinscNmLoja: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 30
      Calculated = True
    end
  end
  object dsPisCofins: TDataSource
    DataSet = qryPisCofins
    Left = 536
    Top = 212
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPk')
    Left = 496
    Top = 244
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPk')
    Left = 496
    Top = 276
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
end
