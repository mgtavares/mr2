unit fGeraSPComissaoRepres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, cxLookAndFeelPainters, cxButtons;

type
  TfrmGeraSPComissaoRepres = class(TfrmProcesso_Padrao)
    Label3: TLabel;
    edtDtInicial: TMaskEdit;
    Label6: TLabel;
    edtDtFinal: TMaskEdit;
    btProcessar: TcxButton;
    edtDtPagto: TMaskEdit;
    Label1: TLabel;
    procedure btProcessarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraSPComissaoRepres: TfrmGeraSPComissaoRepres;

implementation

uses fGeraSPComissaoRepres_Resumo, fMenu;

{$R *.dfm}

procedure TfrmGeraSPComissaoRepres.btProcessarClick(Sender: TObject);
var
   objForm : TfrmGeraSPComissaoRepres_Resumo;
begin
  inherited;

  objForm := TfrmGeraSPComissaoRepres_Resumo.Create(nil);

  if (trim(edtDtInicial.Text) = '/  /') or (trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial e a data final de faturamento.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtInicial.Text) > strToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Data final menor que data inicial.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (trim(edtDtPagto.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de pagamento da comiss�o.') ;
      edtDtPagto.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtPagto.Text) < Date) then
  begin
      MensagemAlerta('A data de pagamento n�o pode ser menor que hoje.') ;
      edtDtPagto.SetFocus;
      exit ;
  end ;

  Try
      Try

          objForm.qryResumoComissao.Close;
          objForm.qryResumoComissao.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          objForm.qryResumoComissao.Parameters.ParamByName('dDtInicial').Value := DateToStr(frmMenu.ConvData(edtDtInicial.Text)) ;
          objForm.qryResumoComissao.Parameters.ParamByName('dDtFinal').Value   := DateToStr(frmMenu.ConvData(edtDtFinal.Text)) ;
          objForm.qryResumoComissao.Open;

          if (objForm.qryResumoComissao.eof) then
          begin
              ShowMessage('Nenhuma comiss�o pendente de pagamento para o per�odo informado.') ;
              exit ;
          end;

          objForm.dDtPagto := frmMenu.ConvData(edtDtPagto.Text) ;
          objForm.ShowModal;

      except
          MensagemErro('Erro na cria��o do Formul�rio');
          raise;
      end;
  finally
          FreeAndNil(objForm);
  end;

end;

initialization
    RegisterClass(TfrmGeraSPComissaoRepres) ;

end.
