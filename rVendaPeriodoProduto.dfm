inherited rptVendaPeriodoProduto: TrptVendaPeriodoProduto
  Caption = 'Venda por Per'#237'odo x Produto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 96
    Width = 33
    Height = 13
    Caption = 'Cliente'
  end
  object Label3: TLabel [3]
    Left = 37
    Top = 232
    Width = 69
    Height = 13
    Caption = 'Per'#237'odo Venda'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 232
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 24
    Top = 72
    Width = 82
    Height = 13
    Caption = 'Grupo Economico'
  end
  object Label4: TLabel [6]
    Left = 37
    Top = 136
    Width = 69
    Height = 13
    Caption = 'Departamento'
  end
  object Label7: TLabel [7]
    Left = 77
    Top = 160
    Width = 29
    Height = 13
    Caption = 'Marca'
  end
  object Label8: TLabel [8]
    Left = 81
    Top = 184
    Width = 25
    Height = 13
    Caption = 'Linha'
  end
  object Label9: TLabel [9]
    Left = 75
    Top = 208
    Width = 31
    Height = 13
    Caption = 'Classe'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 256
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit9: TDBEdit [13]
    Tag = 1
    Left = 184
    Top = 88
    Width = 129
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 12
  end
  object DBEdit10: TDBEdit [14]
    Tag = 1
    Left = 320
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 13
  end
  object MaskEdit1: TMaskEdit [15]
    Left = 112
    Top = 224
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [16]
    Left = 208
    Top = 224
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [17]
    Left = 112
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit6: TMaskEdit [18]
    Left = 112
    Top = 88
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 64
    Width = 653
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 14
  end
  object MaskEdit4: TMaskEdit [20]
    Left = 112
    Top = 64
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit5: TMaskEdit [21]
    Left = 112
    Top = 128
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [22]
    Tag = 1
    Left = 184
    Top = 128
    Width = 653
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 15
  end
  object MaskEdit7: TMaskEdit [23]
    Left = 112
    Top = 152
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [24]
    Tag = 1
    Left = 184
    Top = 152
    Width = 653
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 16
  end
  object MaskEdit8: TMaskEdit [25]
    Left = 112
    Top = 176
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [26]
    Tag = 1
    Left = 184
    Top = 176
    Width = 653
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 17
  end
  object MaskEdit9: TMaskEdit [27]
    Left = 112
    Top = 200
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [28]
    Tag = 1
    Left = 184
    Top = 200
    Width = 653
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 18
  end
  object RadioGroup1: TRadioGroup [29]
    Left = 112
    Top = 250
    Width = 201
    Height = 41
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 19
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 336
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 288
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 480
    Top = 280
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 784
    Top = 256
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 824
    Top = 256
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 864
    Top = 256
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 904
    Top = 248
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 784
    Top = 288
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 824
    Top = 296
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 864
    Top = 296
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 912
    Top = 296
  end
end
