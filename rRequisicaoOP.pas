unit rRequisicaoOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, QuickRpt, ExtCtrls, SvQrBarcode;

type
  TrptRequisicaoOP = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel5: TQRLabel;
    qryRequisicaoOP: TADOQuery;
    QRLabel1: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel12: TQRLabel;
    barraNumeroOP: TSvQRBarcode;
    QRLabel13: TQRLabel;
    QRDBText15: TQRDBText;
    qryRequisicaoOPnCdRequisicao: TStringField;
    qryRequisicaoOPdDtRequisicao: TDateTimeField;
    qryRequisicaoOPcNmSetor: TStringField;
    qryRequisicaoOPcNmSolicitante: TStringField;
    qryRequisicaoOPcNumeroOP: TStringField;
    qryRequisicaoOPcNmCentroProdutivo: TStringField;
    qryRequisicaoOPcNmEtapaProducao: TStringField;
    qryRequisicaoOPnCdProduto: TIntegerField;
    qryRequisicaoOPcNmItem: TStringField;
    qryRequisicaoOPcReferencia: TStringField;
    qryRequisicaoOPnQtdeReq: TBCDField;
    qryRequisicaoOPcUnidadeMedida: TStringField;
    qryRequisicaoOPnSaldoReq: TBCDField;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel19: TQRLabel;
    barraCdRequisicao: TSvQRBarcode;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText8: TQRDBText;
    SummaryBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    qryRequisicaoOPcOBS: TMemoField;
    QRLabel14: TQRLabel;
    QRDBText10: TQRDBText;
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRequisicaoOP: TrptRequisicaoOP;

implementation

{$R *.dfm}

procedure TrptRequisicaoOP.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

    barraNumeroOP.Text     := qryRequisicaoOPcNumeroOP.Value ;
    barraCdRequisicao.Text := qryRequisicaoOPnCdRequisicao.Value ;

end;

end.
