unit rItensSeparacaoDistribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, ExtCtrls, DB, ADODB;

type
  TrelItensSeparacaoDistribuicao_view = class(TForm)
    QuickRep1: TQuickRep;
    qryResultado: TADOQuery;
    QRBand1: TQRBand;
    QRSysData1: TQRSysData;
    QRSysData3: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel11: TQRLabel;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmProduto: TStringField;
    qryResultadonCdProdutoPai: TIntegerField;
    qryResultadocNmProdutoPai: TStringField;
    QRDBText1: TQRDBText;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape3: TQRShape;
    qryResultadocNmCategoria: TStringField;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    qryResultadonCdEstoqueEmpenhado: TIntegerField;
    qryResultadonQtdeEstoqueEmpenhado: TBCDField;
    qryResultadocQtdeConf: TStringField;
    qryResultadocQtdeDiv: TStringField;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    qryResultadonCdOrdemSeparacao: TIntegerField;
    qryResultadonCdLoja: TIntegerField;
    qryResultadocNmLoja: TStringField;
    QRLoja: TQRDBText;
    QRDBText8: TQRDBText;
    SummaryBand1: TQRBand;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRLabel12: TQRLabel;
    qryResultadonQtdeConf: TBCDField;
    qryResultadonQtdeDiv: TBCDField;
    QRShape2: TQRShape;
    qryResultadodDtEntrega: TDateTimeField;
    qryResultadocNmUsuarioGeracao: TStringField;
    qryResultadodDtGeracao: TDateTimeField;
    qryResultadocNmUsuarioBaixa: TStringField;
    qryResultadodDtBaixa: TDateTimeField;
    QRLabel14: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText15: TQRDBText;
    qryResultadocNmUsuarioConf: TStringField;
    qryResultadodDtConfIni: TDateTimeField;
    qryResultadodDtConfFim: TDateTimeField;
    QRDBText16: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel13: TQRLabel;
    qryResultadodDtEmpenho: TDateTimeField;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLojaAux : integer;
    nCdLoja    : integer;

  end;

var
  relItensSeparacaoDistribuicao_view: TrelItensSeparacaoDistribuicao_view;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TrelItensSeparacaoDistribuicao_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

   if (nCdLojaAux = 0) then
       nCdLojaAux := qryResultadonCdLoja.Value;

   if QRBand3.ForceNewPage = true then
       QRBand3.ForceNewPage := false;

   if  nCdLojaAux <> qryResultadonCdLoja.Value then
   begin
       QRBand3.ForceNewPage := True;
       nCdLojaAux := qryResultadonCdLoja.Value;
   end;

end;

end.
