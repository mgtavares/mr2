unit fModImpETP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  ACBrBase, ACBrETQ, ACBrDevice, DBGridEhGrouping, ER2Lookup, ToolCtrlsEh,
  ExtDlgs;

type
  TfrmModImpETP = class(TfrmCadastro_Padrao)
    qryMasternCdModImpETP: TIntegerField;
    qryMastercNmModImpETP: TStringField;
    qryMasteriQtdeEtiquetas: TIntegerField;
    qryMasteriAvancoEtiqueta: TIntegerField;
    qryMasteriModelo: TIntegerField;
    qryMasteriTemperatura: TIntegerField;
    qryMastercFlgLimpaMemoria: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label6: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    Label7: TLabel;
    Label8: TLabel;
    qryCampoModImpETP: TADOQuery;
    qryCampoModImpETPnCdCampoModImpETP: TAutoIncField;
    qryCampoModImpETPnCdModImpETP: TIntegerField;
    qryCampoModImpETPcFlgTipoCampo: TStringField;
    qryCampoModImpETPiOrientacao: TIntegerField;
    qryCampoModImpETPcTexto: TStringField;
    qryCampoModImpETPiTamanhoFonte: TIntegerField;
    qryCampoModImpETPcMultiplicadorH: TStringField;
    qryCampoModImpETPcMultiplicadorV: TStringField;
    qryCampoModImpETPiSubFonte: TIntegerField;
    qryCampoModImpETPcFlgTipoBarra: TStringField;
    qryCampoModImpETPcLarguraBarraLarga: TStringField;
    qryCampoModImpETPcLarguraBarraFina: TStringField;
    qryCampoModImpETPiAlturaCodBarras: TIntegerField;
    qryCampoModImpETPiVertical: TIntegerField;
    qryCampoModImpETPiHorizontal: TIntegerField;
    qryCampoModImpETPiLargura: TIntegerField;
    qryCampoModImpETPiAltura: TIntegerField;
    qryCampoModImpETPiExpessuraVert: TIntegerField;
    qryCampoModImpETPiExpessuraHoriz: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    DBGridEh1: TDBGridEh;
    dsCampoModImpETP: TDataSource;
    qryMastercQuery: TMemoField;
    cxTabSheet2: TcxTabSheet;
    DBMemo1: TDBMemo;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryCampoModImpETPTeste: TADOQuery;
    qryCampoModImpETPTestenCdCampoModImpETP: TAutoIncField;
    qryCampoModImpETPTestenCdModImpETP: TIntegerField;
    qryCampoModImpETPTestecFlgTipoCampo: TStringField;
    qryCampoModImpETPTesteiOrientacao: TIntegerField;
    qryCampoModImpETPTestecTexto: TStringField;
    qryCampoModImpETPTesteiTamanhoFonte: TIntegerField;
    qryCampoModImpETPTestecMultiplicadorH: TStringField;
    qryCampoModImpETPTestecMultiplicadorV: TStringField;
    qryCampoModImpETPTesteiSubFonte: TIntegerField;
    qryCampoModImpETPTestecFlgTipoBarra: TStringField;
    qryCampoModImpETPTestecLarguraBarraLarga: TStringField;
    qryCampoModImpETPTestecLarguraBarraFina: TStringField;
    qryCampoModImpETPTesteiAlturaCodBarras: TIntegerField;
    qryCampoModImpETPTesteiVertical: TIntegerField;
    qryCampoModImpETPTesteiHorizontal: TIntegerField;
    qryCampoModImpETPTesteiLargura: TIntegerField;
    qryCampoModImpETPTesteiAltura: TIntegerField;
    qryCampoModImpETPTesteiExpessuraVert: TIntegerField;
    qryCampoModImpETPTesteiExpessuraHoriz: TIntegerField;
    ACBrETQ1: TACBrETQ;
    qryMasteriEspacoEntreEtiquetas: TIntegerField;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    qryAuxImp: TADOQuery;
    qryMasternCdTabTipoModEtiq: TIntegerField;
    edtTipoEtiqueta: TER2LookupDBEdit;
    qryTipoEtiqueta: TADOQuery;
    qryTipoEtiquetanCdTabTipoModEtiq: TIntegerField;
    qryTipoEtiquetacNmTabTipoModEtiq: TStringField;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    qryCampoModImpETPcCaminhoImagem: TStringField;
    qryCampoModImpETPTestecCaminhoImagem: TStringField;
    OpenDialog: TOpenDialog;
    qryImgEtq: TADOQuery;
    qryImgEtqiVertical: TIntegerField;
    qryImgEtqiHorizontal: TIntegerField;
    qryImgEtqnCdCampoModImpETP: TAutoIncField;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryCampoModImpETPBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure emitirEtiquetas(arrID: array of integer; nCdTabTipoModEtiq : integer);
    procedure DBGridEh1Columns19EditButtons0Click(Sender: TObject;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModImpETP: TfrmModImpETP;

implementation

uses fSelPortaEtiqueta, pasEPL2, fMenu;

{$R *.dfm}

procedure TfrmModImpETP.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryMasteriModelo.Value          := 1 ;
  qryMasteriAvancoEtiqueta.Value  := 600 ;
  qryMasteriQtdeEtiquetas.Value   := 1 ;
  qryMasteriTemperatura.Value     := 10 ;
  qryMastercFlgLimpaMemoria.Value := 1 ;
  DBEdit2.SetFocus;

end;

procedure TfrmModImpETP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MODIMPETP' ;
  nCdTabelaSistema  := 204 ;
  nCdConsultaPadrao := 203 ;
end;

procedure TfrmModImpETP.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  { 5 tamanho fonte
    6 multiplicador horiz
    7 multiplicador vertic
    8 sub fonte
    9 tipo barra
   10 larg barra larga
   11 larg barra fina
   12 altura cod barras
   13 vertical
   14 horizontal
   15 largura
   16 altura
   17 expessura vertical
   18 expessura horizontal
   19 descri��o imagem}

    DBGridEh1.Columns[3].Visible  := False ;  // Orienta�a� / Tipo Linha
    DBGridEh1.Columns[4].Visible  := False ;  // texto
    DBGridEh1.Columns[5].Visible  := False ;  // tamanho fonte
    DBGridEh1.Columns[6].Visible  := False ;  // multiplicador horiz.
    DBGridEh1.Columns[7].Visible  := False ;  // multiplicador vertical
    DBGridEh1.Columns[8].Visible  := False ;  // sub fonte
    DBGridEh1.Columns[9].Visible  := False ; // tipo barra
    DBGridEh1.Columns[10].Visible := False ; // largura barra larga
    DBGridEh1.Columns[11].Visible := False ; // largura barra fina
    DBGridEh1.Columns[12].Visible := False ; // altura c�d. barraas
    DBGridEh1.Columns[13].Visible := False ; // vertical
    DBGridEh1.Columns[14].Visible := False ; // horizontal
    DBGridEh1.Columns[15].Visible := False ; // largura
    DBGridEh1.Columns[16].Visible := False ; // altura
    DBGridEh1.Columns[17].Visible := False ; // expessura vertical
    DBGridEh1.Columns[18].Visible := False ; // expessura horizontal
    DBGridEh1.Columns[19].Visible := False ; // caminho imagem

    DBGridEh1.Columns[5].Width  := 95 ;  // tamanho fonte
    DBGridEh1.Columns[6].Width  := 95 ;  // multiplicador horiz.
    DBGridEh1.Columns[7].Width  := 95 ;  // multiplicador vertical
    DBGridEh1.Columns[8].Width  := 95 ;  // sub fonte
    DBGridEh1.Columns[9].Width  := 95 ; // tipo barra
    DBGridEh1.Columns[10].Width := 95 ; // largura barra larga
    DBGridEh1.Columns[11].Width := 95 ; // largura barra fina
    DBGridEh1.Columns[12].Width := 95 ; // altura c�d. barraas
    DBGridEh1.Columns[13].Width := 95 ; // vertical
    DBGridEh1.Columns[14].Width := 95 ; // horizontal
    DBGridEh1.Columns[15].Width := 95 ; // largura
    DBGridEh1.Columns[16].Width := 95 ; // altura
    DBGridEh1.Columns[17].Width := 95 ; // expessura vertical
    DBGridEh1.Columns[18].Width := 95 ; // expessura horizontal

   {--campo de c�d barra--}
    if (RadioGroup1.ItemIndex = 0) then
    begin

        qryCampoModImpETP.Parameters.ParamByName('cFlgTipoCampo').Value := 'B' ;

        DBGridEh1.Columns[3].PickList.Clear;
        DBGridEh1.Columns[3].PickList.Add('Normal');
        DBGridEh1.Columns[3].PickList.Add('90 graus');
        DBGridEh1.Columns[3].PickList.Add('180 graus');
        DBGridEh1.Columns[3].PickList.Add('270 graus');
        DBGridEh1.Columns[3].Title.Caption := 'Orienta��o';

        DBGridEh1.Columns[3].Visible  := True ;  // Orienta�a�
        DBGridEh1.Columns[4].Visible  := True ;  // texto
        DBGridEh1.Columns[9].Visible  := True ; // tipo barra
        DBGridEh1.Columns[10].Visible := True ; // largura barra larga
        DBGridEh1.Columns[11].Visible := True ; // largura barra fina
        DBGridEh1.Columns[12].Visible := True ; // altura c�d. barraas
        DBGridEh1.Columns[13].Visible := True ; // vertical
        DBGridEh1.Columns[14].Visible := True ; // horizontal

        PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
    end ;

   {--campo de texto--}
    if (RadioGroup1.ItemIndex = 1) then
    begin

        qryCampoModImpETP.Parameters.ParamByName('cFlgTipoCampo').Value := 'C' ;

        DBGridEh1.Columns[3].PickList.Clear;
        DBGridEh1.Columns[3].PickList.Add('Normal');
        DBGridEh1.Columns[3].PickList.Add('90 graus');
        DBGridEh1.Columns[3].PickList.Add('180 graus');
        DBGridEh1.Columns[3].PickList.Add('270 graus');
        DBGridEh1.Columns[3].Title.Caption := 'Orienta��o';

        DBGridEh1.Columns[3].Visible  := True ;  // Orienta�a�
        DBGridEh1.Columns[4].Visible  := True ;  // texto
        DBGridEh1.Columns[5].Visible  := True ;  // tamanho fonte
        DBGridEh1.Columns[6].Visible  := True ;  // multiplicador horizontal
        DBGridEh1.Columns[7].Visible  := True ;  // multiplicador vertical
        DBGridEh1.Columns[13].Visible := True ;  // posi��o vertical
        DBGridEh1.Columns[14].Visible := True ;  // posi��o horizontal

        PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
    end ;

   {--campo de linha--}
    if (RadioGroup1.ItemIndex = 2) then
    begin

        qryCampoModImpETP.Parameters.ParamByName('cFlgTipoCampo').Value := 'L' ;

        DBGridEh1.Columns[3].PickList.Clear;
        DBGridEh1.Columns[3].PickList.Add('Normal');
        DBGridEh1.Columns[3].PickList.Add('Branca');
        DBGridEh1.Columns[3].PickList.Add('Exclusiva');
//        DBGridEh1.Columns[3].PickList.Add('Diagonal');
        DBGridEh1.Columns[3].Title.Caption := 'Tipo';

        DBGridEh1.Columns[3].Visible  := True ;  // Tipo Linha
        DBGridEh1.Columns[13].Visible := True ; // vertical
        DBGridEh1.Columns[14].Visible := True ; // horizontal
        DBGridEh1.Columns[15].Visible := True ; // largura
        DBGridEh1.Columns[16].Visible := True ; // altura

        PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
    end ;

   {--campo de box--}
    if (RadioGroup1.ItemIndex = 3) then
    begin

        qryCampoModImpETP.Parameters.ParamByName('cFlgTipoCampo').Value := 'X' ;

        DBGridEh1.Columns[13].Visible := True ; // vertical
        DBGridEh1.Columns[14].Visible := True ; // horizontal
        DBGridEh1.Columns[15].Visible := True ; // largura
        DBGridEh1.Columns[16].Visible := True ; // altura
        DBGridEh1.Columns[17].Visible := True ; // expessura vertical
        DBGridEh1.Columns[18].Visible := True ; // expessura horizontal

        PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
    end ;

    {--campo de imagem--}
    if (RadioGroup1.ItemIndex = 4) then
    begin
        qryCampoModImpETP.Parameters.ParamByName('cFlgTipoCampo').Value := 'I';

        DBGridEh1.Columns[13].Visible := True ; // vertical
        DBGridEh1.Columns[14].Visible := True ; // horizontal
        DBGridEh1.Columns[19].Visible := True ; // caminho imagem

        PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
    end;

end;

procedure TfrmModImpETP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  RadioGroup1.ItemIndex := 0 ;
  RadioGroup1Click(Self) ;
  PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;

end;

procedure TfrmModImpETP.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryCampoModImpETP.Close ;

end;

procedure TfrmModImpETP.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryCampoModImpETP.Close;
end;

procedure TfrmModImpETP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCampoModImpETP.Close;
  qryTipoEtiqueta.Close;
end;

procedure TfrmModImpETP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

end;

procedure TfrmModImpETP.qryCampoModImpETPBeforePost(DataSet: TDataSet);
var
  cExt : String;
begin
  inherited;

  qryCampoModImpETPnCdModImpETP.Value := qryMasternCdModImpETP.Value ;

  if (RadioGroup1.ItemIndex = 0) then
      qryCampoModImpETPcFlgTipoCampo.Value := 'B' ;

  if (RadioGroup1.ItemIndex = 1) then
      qryCampoModImpETPcFlgTipoCampo.Value := 'C' ;

  if (RadioGroup1.ItemIndex = 2) then
      qryCampoModImpETPcFlgTipoCampo.Value := 'L' ;

  if (RadioGroup1.ItemIndex = 3) then
      qryCampoModImpETPcFlgTipoCampo.Value := 'X' ;

  if (RadioGroup1.ItemIndex = 4) then
  begin
      if (Trim(qryCampoModImpETPcCaminhoImagem.Value) = '') then
      begin
          MensagemAlerta('Informe o caminho da imagem.');

          Abort;
      end;

      // Valida se o arquivo f�sico existe
      if not (FileExists(qryCampoModImpETPcCaminhoImagem.Value)) then
      begin
          MensagemAlerta('O caminho da imagem n�o corresponde � um arquivo f�sico.');

          Abort;
      end;

      // Testa a extens�o da imagem
      cExt := ExtractFileExt(qryCampoModImpETPcCaminhoImagem.Value);

      if ((cExt <> '.bmp') and (cExt <> '.pcx')) then
      begin
          MensagemAlerta('Somente imagens do tipo BITMAP (*.bmp) e PCX (*.pcx) s�o suportadas.');

          Abort;
      end;

      qryCampoModImpETPcFlgTipoCampo.Value := 'I' ;
  end;

end;

procedure TfrmModImpETP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  RadioGroup1.ItemIndex := 0 ;
  RadioGroup1Click(Self) ;
  PosicionaQuery(qryCampoModImpETP , qryMasternCdModImpETP.AsString) ;
  PosicionaQuery(qryTipoEtiqueta , qryMasternCdTabTipoModEtiq.AsString) ;

end;

procedure TfrmModImpETP.FormShow(Sender: TObject);
begin
  inherited;

  bLimpaAposSalvar               := False;
  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmModImpETP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmModImpETP.Value = '') then
  begin
      MensagemAlerta('Informe o nome do modelo.');
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTabTipoModEtiq.AsString = '') then
  begin
      MensagemAlerta('Selecione o Tipo de etiqueta do modelo.');
      edtTipoEtiqueta.SetFocus;
      abort ;
  end;

  if (qryMasteriModelo.Value > 4) then
  begin
      MensagemAlerta('Selecione o modelo.');
      RadioGroup1.SetFocus ;
      abort ;
  end ;

  if (qryMasteriQtdeEtiquetas.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade de etiquetas por linha.');
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasteriQtdeEtiquetas.Value > 1) and (qryMasteriEspacoEntreEtiquetas.Value <= 0) then
  begin
      MensagemAlerta('Informe o espacejamento lateral entre as etiquetas.');
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasteriQtdeEtiquetas.Value <= 1) then
      qryMasteriEspacoEntreEtiquetas.Value := 0 ;

  if (qryMasteriAvancoEtiqueta.Value <= 0) then
  begin
      MensagemAlerta('Informe o avan�o entre as etiquetas.');
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasteriTemperatura.Value < 10) or (qryMasteriTemperatura.Value > 20) then
  begin
      MensagemAlerta('A temperatura deve estar entre 10 e 20.');
      DBEdit5.SetFocus ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmModImpETP.ToolButton10Click(Sender: TObject);
var
    cPorta        : string ;
    iVezes        : integer ;
    iAux2         : integer ;
    arrID         : array of integer ;
    cTexto        : string ;
    cOrientacao   : string;
    cCdTextoBarra : string;
    cExtImagem    : string;
begin
  inherited;

  if (qryMaster.IsEmpty) then
  begin
      MensagemAlerta('Selecione um modelo de etiqueta para imprimir uma teste.') ;
      abort ;
  end ;

  qryCampoModImpETPTeste.Close;
  PosicionaQuery(qryCampoModImpETPTeste, qryMasternCdModImpETP.AsString) ;

  if qryCampoModImpETPTeste.Eof then
  begin
      MensagemAlerta('Este modelo de etiqueta n�o tem nenhum campo informado.') ;
      abort ;
  end ;

  if (qryMasteriModelo.Value = 0) then
      Exit;

  qryImgEtq.Close;
  qryImgEtq.Parameters.ParamByName('nPK').Value := qryMasternCdModImpETP.Value;
  qryImgEtq.Open;

  {-- EPL2 - Sem Imagem // Processo n�o removido pois h� clientes utilizando modelos de impress�o configurados dessa forma --}
  if ((qryMasteriModelo.Value = 4) and (qryImgEtq.IsEmpty)) then
  begin

      InputQuery('Porta Impressora','Porta Impressora',cPorta) ;

      if (trim(cPorta) = '') then
      begin
          MensagemAlerta('Nenhuma porta selecionada.') ;
          abort ;
      end ;

      cTexto := Abrir_Etiqueta_EPL2;

      for iVezes := 0 to qryMasteriQtdeEtiquetas.Value-1 do
      begin
          qryCampoModImpETPTeste.First;

          while not qryCampoModImpETPTeste.Eof do
          begin

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'B') then
              begin

                  cOrientacao := qryCampoModImpETPTesteiOrientacao.asString;

                  if (cOrientacao = '2') then
                      cCdTextoBarra := 'N'
                  else cCdTextoBarra := 'B';

                  cTexto := cTexto + Codigo_Barras_EPL2(intToStr(qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value))     //margem_Superior
                                                       ,qryCampoModImpETPTesteiVertical.asString         //margem_Esquerda
                                                       ,cOrientacao                                      //Rotacao
                                                       ,qryCampoModImpETPTestecFlgTipoBarra.Value        //Tipo_Codigo
                                                       ,qryCampoModImpETPTestecLarguraBarraFina.Value    //Barra_Fina
                                                       ,qryCampoModImpETPTestecLarguraBarraLarga.Value   //Barra_Grossa
                                                       ,qryCampoModImpETPTesteiAlturaCodBarras.asString  //Altura_Barra
                                                       ,cCdTextoBarra                                    //Imprime Legivel
                                                       ,qryCampoModImpETPTestecTexto.Value);             //Texto
              end;

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'C') then
                  cTexto := cTexto + Texto_EPL2(intToStr(qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value))
                                               ,qryCampoModImpETPTesteiVertical.asString
                                               ,qryCampoModImpETPTesteiOrientacao.asString
                                               ,qryCampoModImpETPTesteiTamanhoFonte.asString
                                               ,qryCampoModImpETPTestecMultiplicadorH.Value
                                               ,qryCampoModImpETPTestecMultiplicadorV.Value
                                               ,'N'
                                               ,qryCampoModImpETPTestecTexto.Value) ;


              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'L') then
                  cTexto := cTexto + Linha_EPL2(intToStr(qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value))
                                               ,qryCampoModImpETPTesteiVertical.AsString
                                               ,qryCampoModImpETPTesteiLargura.AsString
                                               ,qryCampoModImpETPTesteiAltura.AsString
                                               ,qryCampoModImpETPTesteiOrientacao.Value) ;

              qryCampoModImpETPTeste.Next ;

          end ;
      end ;

      qryCampoModImpETPTeste.Close ;

      cTexto := cTexto + ImprimeEtiquetaEPL2(1) ;
      EnviarImpressoraEPL2(cPorta,cTexto) ;
  end

  {-- PPLA, PPLB, ZPL e EPL2 --}
  else
  begin
      InputQuery('Porta Impressora','Porta Impressora',cPorta) ;

      if (trim(cPorta) = '') then
      begin
          MensagemAlerta('Nenhuma porta selecionada.') ;
          abort ;
      end ;

      ACBrETQ1.Porta         := cPorta ;
      ACBrETQ1.Modelo        := TACBrETQModelo(qryMasteriModelo.Value) ;
      ACBrETQ1.LimparMemoria := (qryMastercFlgLimpaMemoria.Value = 1) ;
      ACBrETQ1.Avanco        := qryMasteriAvancoEtiqueta.Value ;
      ACBrETQ1.Temperatura   := qryMasteriTemperatura.Value ;
      ACBrETQ1.Ativar;

      for iVezes := 0 to qryMasteriQtdeEtiquetas.Value-1 do
      begin
          qryCampoModImpETPTeste.First;

          while not qryCampoModImpETPTeste.Eof do
          begin

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'B') then
                  ACBrETQ1.ImprimirBarras(TACBrETQOrientacao(qryCampoModImpETPTesteiOrientacao.Value)
                                         ,Char(qryCampoModImpETPTestecFlgTipoBarra.Value[1])
                                         ,Char(qryCampoModImpETPTestecLarguraBarraLarga.Value[1])
                                         ,Char(qryCampoModImpETPTestecLarguraBarraFina.Value[1])
                                         ,qryCampoModImpETPTesteiVertical.Value
                                         ,qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value)
                                         ,qryCampoModImpETPTestecTexto.Value
                                         ,qryCampoModImpETPTesteiAlturaCodBarras.Value);

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'C') then
                  ACBrETQ1.ImprimirTexto(TACBrETQOrientacao(qryCampoModImpETPTesteiOrientacao.Value)
                                        ,qryCampoModImpETPTesteiTamanhoFonte.Value
                                        ,strToInt(trim(qryCampoModImpETPTestecMultiplicadorH.Value))
                                        ,strToInt(trim(qryCampoModImpETPTestecMultiplicadorV.Value))
                                        ,qryCampoModImpETPTesteiVertical.Value
                                        ,qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value)
                                        ,qryCampoModImpETPTestecTexto.Value
                                        ,qryCampoModImpETPTesteiSubFonte.Value);

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'L') then
                  ACBrETQ1.ImprimirLinha(qryCampoModImpETPTesteiVertical.Value
                                        ,qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value)
                                        ,qryCampoModImpETPTesteiLargura.Value
                                        ,qryCampoModImpETPTesteiAltura.Value) ;

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'X') then
                  ACBrETQ1.ImprimirCaixa(qryCampoModImpETPTesteiVertical.Value
                                        ,qryCampoModImpETPTesteiHorizontal.Value + (iVezes * qryMasteriEspacoEntreEtiquetas.Value)
                                        ,qryCampoModImpETPTesteiLargura.Value
                                        ,qryCampoModImpETPTesteiAltura.Value
                                        ,qryCampoModImpETPTesteiExpessuraVert.Value
                                        ,qryCampoModImpETPTesteiExpessuraHoriz.Value) ;

              if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'I') then
              begin
                  cExtImagem := ExtractFileExt(qryCampoModImpETPTestecCaminhoImagem.Value);

                  // Valida as extens�es das imagens
                  if (qryMasteriModelo.Value = 4) then
                  begin
                      if (cExtImagem <> '.pcx') then
                      begin
                          MensagemAlerta('O modelo EPL2 s� permite imagens do tipo PCX (*.pcx).');

                          Abort;
                      end;
                  end

                  else
                  begin
                      if (cExtImagem <> '.bmp') then
                      begin
                          MensagemAlerta('O modelo de impressora selecionado s� permite imagens do tipo BITMAP (*.bmp).');

                          Abort;
                      end;
                  end;

                  // Carrega a imagem na impressora
                  ACBrETQ1.CarregarImagem(qryCampoModImpETPTestecCaminhoImagem.Value, qryCampoModImpETPTestenCdCampoModImpETP.AsString);
              end;

              qryCampoModImpETPTeste.Next ;

          end ;

      end ;

      qryCampoModImpETPTeste.Close ;

      // Envia o comando de impress�o de imagens
      if not (qryImgEtq.IsEmpty) then
      begin
          qryImgEtq.First;

          while not (qryImgEtq.Eof) do
          begin
              ACBrETQ1.ImprimirImagem(1, qryImgEtqiVertical.Value, qryImgEtqiHorizontal.Value, qryImgEtqnCdCampoModImpETP.AsString);

              qryImgEtq.Next;
          end;
      end;

      ACBRETQ1.Imprimir(1,ACBrETQ1.Avanco);

      // Limpa as imagens da mem�ria gr�fica - EPL2
      if ((qryImgEtq.IsEmpty = False) and (qryMasteriModelo.Value = 4)) then
          EnviarImpressoraEPL2(cPorta, 'GK"*"' + #13#10);

      ACBrETQ1.Desativar;
  end ;
end;

procedure TfrmModImpETP.emitirEtiquetas(arrID: array of integer; nCdTabTipoModEtiq : integer);
var
    iIDs,iCampos   : integer ;
    iColuna        : integer ;
    cTexto,cTextoEtiqueta : string ;
    iTotal         : integer ;
    nCdModImpETP   : integer ;
    objForm        : TfrmSelPortaEtiqueta;
    cQryModeloETQ  : TStrings;
    cExtImagem     : string;
begin

    objForm := TfrmSelPortaEtiqueta.Create(nil);
    cQryModeloETQ := TStringList.Create;

    {-- lista apenas os modelos de etiqueta de acordo com o tipo a ser impresso --}
    objForm.qryModImpETP.Close;
    objForm.qryModImpETP.Parameters.ParamByName('nCdTabTipoModEtiq').Value := nCdTabTipoModEtiq;
    objForm.qryModImpETP.Open;

    showForm(objForm,false);

    if not objForm.bImprimir then
        exit ;

    nCdModImpETP := objForm.qryModImpETPnCdModImpETP.Value ;

    PosicionaQuery(qryMaster, intToStr(nCdModImpETP)) ;

    if (qryMaster.eof) then
    begin
        MensagemAlerta('Modelo de etiqueta ' + intToStr(nCdModImpETP) + ' n�o encontrado.') ;
        exit ;
    end ;

    {-- descobre os campos da etiqueta --}
    qryCampoModImpETPTeste.Close;
    PosicionaQuery(qryCampoModImpETPTeste, qryMasternCdModImpETP.AsString) ;

    if qryCampoModImpETPTeste.Eof then
    begin
        MensagemAlerta('Este modelo de etiqueta n�o tem nenhum campo informado.') ;
        abort ;
    end ;

    qryImgEtq.Close;
    qryImgEtq.Parameters.ParamByName('nPK').Value := qryMasternCdModImpETP.Value;
    qryImgEtq.Open;

    {-- EPL2 - Sem Imagem // Processo n�o removido pois h� clientes utilizando modelos de impress�o configurados dessa forma --}
    if ((qryMasteriModelo.Value = 4) and (qryImgEtq.IsEmpty)) then
    begin

        cTextoEtiqueta := Abrir_Etiqueta_EPL2;

        iColuna := 0 ;
        iTotal  := 0 ;

        {-- substitui as variaveis de sistema --}
        cQryModeloETQ.Add(qryMastercQuery.Value);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Empresa',intToStr(frmMenu.nCdEmpresaAtiva),[rfReplaceAll,rfIgnoreCase]);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Loja',intToStr(frmMenu.nCdLojaAtiva),[rfReplaceAll,rfIgnoreCase]);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Usuario',intToStr(frmMenu.nCdUsuarioLogado),[rfReplaceAll,rfIgnoreCase]);

        {-- fecha um loop em todas as entradas do array --}
        for iIDs := 0 to Length(arrID)-1 do
        begin

            {-- se o modelo da etiqueta tem uma instru��o SQL, executa a instru��o para cada entrada do array arrID --}
            {-- substituindo a constante @@Codigo da instru��o SQL pelo valor do c�digo do array                    --}

            if (arrID[iIDs] > 0) then
            begin

                if (qryMastercQuery.Value <> '') then
                begin
                    qryAuxImp.Close;
                    qryAuxImp.SQL.Clear ;
                    qryAuxImp.SQL.Add(cQryModeloETQ.Text) ;
                    qryAuxImp.SQL.Text := StringReplace(qryAuxImp.SQL.Text,'@@Codigo',intToStr(arrID[iIDs]),[rfReplaceAll,rfIgnoreCase]) ;
                    qryAuxImp.Open;

                    if (qryAuxImp.Eof) then
                        MensagemAlerta('O Retorno da query do modelo de etiqueta para o c�digo ' + intToStr(arrID[iIDs]) + ' foi vazio.' + intToStr(iIDs)) ;

                end ;

                {-- come�a a impress�o da etiqueta --}

                qryCampoModImpETPTeste.First;

                while not qryCampoModImpETPTeste.Eof do
                begin
                  {-- verifica se o campo texto da etiqueta utiliza algum metadado da instru��o SQL --}
                  {-- se existir, substitui o metadado pelo valor da query --}

                  cTexto := qryCampoModImpETPTestecTexto.Value ;

                  for iCampos := 0 to qryAuxImp.FieldCount-1 do
                  begin
                      if (qryAuxImp.FieldList[iCampos].Value = Null) then
                          cTexto := StringReplace(cTexto,'@@'+qryAuxImp.FieldList[iCampos].FieldName,'',[rfReplaceAll,rfIgnoreCase])
                      else
                          cTexto := StringReplace(cTexto,'@@'+qryAuxImp.FieldList[iCampos].FieldName,qryAuxImp.FieldList[iCampos].Value,[rfReplaceAll,rfIgnoreCase]) ;
                  end;

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'B') then
                      cTextoEtiqueta := cTextoEtiqueta + Codigo_Barras_EPL2(intToStr(qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value))     //margem_Superior
                                                                           ,qryCampoModImpETPTesteiVertical.asString
                                                                           ,qryCampoModImpETPTesteiOrientacao.AsString
                                                                           ,qryCampoModImpETPTestecFlgTipoBarra.Value
                                                                           ,qryCampoModImpETPTestecLarguraBarraFina.Value
                                                                           ,qryCampoModImpETPTestecLarguraBarraLarga.Value
                                                                           ,qryCampoModImpETPTesteiAlturaCodBarras.asString
                                                                           ,'B'
                                                                           ,cTexto);

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'C') then
                      cTextoEtiqueta := cTextoEtiqueta + Texto_EPL2(intToStr(qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value))
                                                                   ,qryCampoModImpETPTesteiVertical.asString
                                                                   ,qryCampoModImpETPTesteiOrientacao.AsString
                                                                   ,qryCampoModImpETPTesteiTamanhoFonte.asString
                                                                   ,qryCampoModImpETPTestecMultiplicadorH.Value
                                                                   ,qryCampoModImpETPTestecMultiplicadorV.Value
                                                                   ,'N'
                                                                   ,cTexto) ;

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'L') then
                      cTextoEtiqueta := cTextoEtiqueta + Linha_EPL2(IntToStr(qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value))
                                                                   ,qryCampoModImpETPTesteiVertical.AsString
                                                                   ,qryCampoModImpETPTesteiLargura.AsString
                                                                   ,qryCampoModImpETPTesteiAltura.AsString
                                                                   ,qryCampoModImpETPTesteiOrientacao.Value) ;


                  qryCampoModImpETPTeste.Next ;
                end ;

                inc(iTotal) ;
                inc(iColuna) ;

                if (iColuna >= qryMasteriQtdeEtiquetas.Value) then
                begin
                  cTextoEtiqueta := cTextoEtiqueta + ImprimeEtiquetaEPL2(1) ;
                  iColuna := 0 ;
                end ;

           end ;

        end ;

        qryCampoModImpETPTeste.Close ;

        if (iColuna > 0) then
            cTextoEtiqueta := cTextoEtiqueta + ImprimeEtiquetaEPL2(1) ;

        EnviarImpressoraEPL2(objForm.cmbPorta.Text,cTextoEtiqueta) ;

    end

    {-- PPLA, PPLB, ZPL e EPL2 --}
    else
    begin

        ACBrETQ1.Porta         := objForm.cmbPorta.Text ;
        ACBrETQ1.Modelo        := TACBrETQModelo(qryMasteriModelo.Value) ;
        ACBrETQ1.LimparMemoria := (qryMastercFlgLimpaMemoria.Value = 1) ;
        ACBrETQ1.Avanco        := qryMasteriAvancoEtiqueta.Value ;
        ACBrETQ1.Temperatura   := qryMasteriTemperatura.Value ;
        ACBrETQ1.Ativar;

        iColuna := 0 ;
        iTotal  := 0 ;

        {-- substitui as variaveis de sistema --}
        cQryModeloETQ.Add(qryMastercQuery.Value);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Empresa',intToStr(frmMenu.nCdEmpresaAtiva),[rfReplaceAll,rfIgnoreCase]);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Loja',intToStr(frmMenu.nCdLojaAtiva),[rfReplaceAll,rfIgnoreCase]);
        cQryModeloETQ.Text := StringReplace(cQryModeloETQ.Text, '@@Usuario',intToStr(frmMenu.nCdUsuarioLogado),[rfReplaceAll,rfIgnoreCase]);


        {-- fecha um loop em todas as entradas do array --}
        for iIDs := 0 to Length(arrID)-1 do
        begin

            {-- se o modelo da etiqueta tem uma instru��o SQL, executa a instru��o para cada entrada do array arrID --}
            {-- substituindo a constante @@Codigo da instru��o SQL pelo valor do c�digo do array                    --}

            if (arrID[iIDs] > 0) then
            begin

                if (qryMastercQuery.Value <> '') then
                begin
                    qryAuxImp.Close;
                    qryAuxImp.SQL.Clear ;
                    qryAuxImp.SQL.Add(cQryModeloETQ.Text) ;
                    qryAuxImp.SQL.Text := StringReplace(qryAuxImp.SQL.Text,'@@Codigo',intToStr(arrID[iIDs]),[rfReplaceAll,rfIgnoreCase]) ;
                    qryAuxImp.Open;

                    if (qryAuxImp.Eof) then
                    begin
                        MensagemErro('O Retorno da query do modelo de etiqueta para o c�digo ' + intToStr(arrID[iIDs]) + ' foi vazio.' + intToStr(iIDs)) ;

                        Abort;
                    end;

                end ;

                {-- come�a a impress�o da etiqueta --}

                qryCampoModImpETPTeste.First;

                while not qryCampoModImpETPTeste.Eof do
                begin
                  {-- verifica se o campo texto da etiqueta utiliza algum metadado da instru��o SQL --}
                  {-- se existir, substitui o metadado pelo valor da query --}

                  cTexto := qryCampoModImpETPTestecTexto.Value ;

                  for iCampos := 0 to qryAuxImp.FieldCount-1 do
                  begin
                      if (qryAuxImp.FieldList[iCampos].Value = Null) then
                          cTexto := StringReplace(cTexto,'@@'+qryAuxImp.FieldList[iCampos].FieldName,'',[rfReplaceAll,rfIgnoreCase])
                      else
                          cTexto := StringReplace(cTexto,'@@'+qryAuxImp.FieldList[iCampos].FieldName,qryAuxImp.FieldList[iCampos].Value,[rfReplaceAll,rfIgnoreCase]) ;
                  end;

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'B') then
                      ACBrETQ1.ImprimirBarras(TACBrETQOrientacao(qryCampoModImpETPTesteiOrientacao.Value)
                                             ,Char(qryCampoModImpETPTestecFlgTipoBarra.Value[1])
                                             ,Char(qryCampoModImpETPTestecLarguraBarraLarga.Value[1])
                                             ,Char(qryCampoModImpETPTestecLarguraBarraFina.Value[1])
                                             ,qryCampoModImpETPTesteiVertical.Value
                                             ,qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value)
                                             ,cTexto
                                             ,qryCampoModImpETPTesteiAlturaCodBarras.Value);

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'C') then
                      ACBrETQ1.ImprimirTexto(TACBrETQOrientacao(qryCampoModImpETPTesteiOrientacao.Value)
                                            ,qryCampoModImpETPTesteiTamanhoFonte.Value
                                            ,strToInt( trim( qryCampoModImpETPTestecMultiplicadorH.Value ) )
                                            ,strToInt( trim( qryCampoModImpETPTestecMultiplicadorV.Value ) )
                                            ,qryCampoModImpETPTesteiVertical.Value
                                            ,qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value)
                                            ,cTexto
                                            ,qryCampoModImpETPTesteiSubFonte.Value);

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'L') then
                      ACBrETQ1.ImprimirLinha(qryCampoModImpETPTesteiVertical.Value
                                            ,qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value)
                                            ,qryCampoModImpETPTesteiLargura.Value
                                            ,qryCampoModImpETPTesteiAltura.Value) ;

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'X') then
                      ACBrETQ1.ImprimirCaixa(qryCampoModImpETPTesteiVertical.Value
                                            ,qryCampoModImpETPTesteiHorizontal.Value + (iColuna * qryMasteriEspacoEntreEtiquetas.Value)
                                            ,qryCampoModImpETPTesteiLargura.Value
                                            ,qryCampoModImpETPTesteiAltura.Value
                                            ,qryCampoModImpETPTesteiExpessuraVert.Value
                                            ,qryCampoModImpETPTesteiExpessuraHoriz.Value) ;

                  if (qryCampoModImpETPTestecFlgTipoCampo.Value = 'I') then
                  begin
                      cExtImagem := ExtractFileExt(qryCampoModImpETPTestecCaminhoImagem.Value);

                      // Valida as extens�es das imagens
                      if (qryMasteriModelo.Value = 4) then
                      begin
                          if (cExtImagem <> '.pcx') then
                          begin
                              MensagemAlerta('O modelo EPL2 s� permite imagens do tipo PCX (*.pcx).');

                              Abort;
                          end;
                      end

                      else
                      begin
                          if (cExtImagem <> '.bmp') then
                          begin
                              MensagemAlerta('O modelo de impressora selecionado s� permite imagens do tipo BITMAP (*.bmp).');

                              Abort;
                          end;
                      end;

                      // Carrega a imagem na impressora
                      ACBrETQ1.CarregarImagem(qryCampoModImpETPTestecCaminhoImagem.Value, qryCampoModImpETPTestenCdCampoModImpETP.AsString);
                  end;

                  qryCampoModImpETPTeste.Next ;
              end ;

              inc(iTotal) ;
              inc(iColuna) ;

              if (iColuna >= qryMasteriQtdeEtiquetas.Value) then
              begin
                  // Envia o comando de impress�o de imagens
                  if not (qryImgEtq.IsEmpty) then
                  begin
                      qryImgEtq.First;

                      while not (qryImgEtq.Eof) do
                      begin
                          ACBrETQ1.ImprimirImagem(1, qryImgEtqiVertical.Value, qryImgEtqiHorizontal.Value, qryImgEtqnCdCampoModImpETP.AsString);

                          qryImgEtq.Next;
                      end;
                  end;

                  ACBRETQ1.Imprimir(strToInt(trim(objForm.edtQtdeCopias.Text)),ACBrETQ1.Avanco);

                  // Limpa as imagens da mem�ria gr�fica - EPL2
                  if ((qryImgEtq.IsEmpty = False) and (qryMasteriModelo.Value = 4)) then
                      EnviarImpressoraEPL2(ACBrETQ1.Porta, 'GK"*"' + #13#10);

                  iColuna := 0 ;
              end ;

           end ;

        end ;

        qryCampoModImpETPTeste.Close ;

        if (ACBrETQ1.Avanco > 1) then
          ACBRETQ1.Imprimir(strToInt(trim(objForm.edtQtdeCopias.Text)),ACBrETQ1.Avanco);

        ACBrETQ1.Desativar;

    end ;

    {-- armazena nas configura��es do ambiente a �ltima porta utilizada --}
    objForm.qryConfigAmbiente.Edit;
    objForm.qryConfigAmbientecPortaEtiqueta.Value := ACBrETQ1.Porta;
    objForm.qryConfigAmbiente.Post;
end;

procedure TfrmModImpETP.DBGridEh1Columns19EditButtons0Click(
  Sender: TObject; var Handled: Boolean);
begin
  inherited;

  OpenDialog.Execute;

  if (Trim(OpenDialog.FileName) <> '') then
  begin

      if (qryCampoModImpETP.IsEmpty) then
          qryCampoModImpETP.Insert
      else
          qryCampoModImpETP.Edit;

      qryCampoModImpETPcCaminhoImagem.Value := OpenDialog.FileName;
  end;
end;

initialization
    RegisterClass(TfrmModImpETP) ;

end.
