unit fGerarPickingList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB, StdCtrls, ToolCtrlsEh;

type
  TfrmGerarPickingList = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItensPicking: TADOQuery;
    dsItensPicking: TDataSource;
    qryItensPickingnCdProduto: TIntegerField;
    qryItensPickingcNmProduto: TStringField;
    qryItensPickingnQtdeDisponivel: TBCDField;
    qryItensPickingnQtdeSelecionado: TBCDField;
    qryItensPickingcNmLocalEstoque: TStringField;
    qryItensPickingnCdItensPicking: TAutoIncField;
    qryItensPickingnCdItemPedido: TIntegerField;
    qryItensPickingnCdLocalEstoque: TIntegerField;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    SP_GERA_PICKLIST: TADOStoredProc;
    qryItensPickingnCdEmpresa: TIntegerField;
    GroupBox1: TGroupBox;
    edtOBS: TEdit;
    Label1: TLabel;
    qryRegHoraProd: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItensPickingBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido         : integer;
    cFlgOrdemProducao : integer;
    nCdOrdemProducao  : integer;
    cOBS              : String;
    procedure ImprimePickList (nCdPickList : integer; PrimImp : Boolean);
  end;

var
  frmGerarPickingList: TfrmGerarPickingList;

implementation

uses fMenu,rPickList,rPickList_Retrato_novo;

{$R *.dfm}

procedure TfrmGerarPickingList.ToolButton1Click(Sender: TObject);
var
  nCdPickList         : integer;
  rptPickList         : TrptPickList;
  rptPickList_Retrato : TrptPickList_Retrato_novo;
begin
  inherited;

  if(qryItensPicking.RecordCount = 0)then
  begin
      MensagemAlerta('Pedido n�o possui saldo dispon�vel para gera��o do Picking List.');
      Exit;
  end;

  if (MessageDlg('Confirma a gera��o do Picking List ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_PICKLIST.Close;
      SP_GERA_PICKLIST.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido;
      SP_GERA_PICKLIST.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_GERA_PICKLIST.Parameters.ParamByName('@cOBS').Value       := edtOBS.Text;
      SP_GERA_PICKLIST.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  nCdPickList := SP_GERA_PICKLIST.Parameters.ParamByName('@nCdPickList').Value;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pick List Gerado com sucesso!');

  if (MessageDlg('Deseja imprimir o Pick List ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
      ImprimePickList(nCdPickList,True); {-- Realiza primeira Impress�o do PickList --}

  Close;

end;

procedure TfrmGerarPickingList.FormShow(Sender: TObject);
begin
  inherited;

  cmdPreparaTemp.Execute;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdPedido').Value         := nCdPedido;
  qryPopulaTemp.Parameters.ParamByName('cFlgOrdemProducao').Value := cFlgOrdemProducao;
  qryPopulaTemp.Parameters.ParamByName('nCdOrdemProducao').Value  := nCdOrdemProducao;
  qryPopulaTemp.ExecSQL;

  qryItensPicking.Close;
  qryItensPicking.Open;

  edtOBS.Text := cOBS;

  DBGridEh1.SelectedIndex := 3;
end;

procedure TfrmGerarPickingList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmGerarPickingList.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmGerarPickingList.ImprimePickList(nCdPickList: integer; PrimImp : Boolean);
var
    rptPickList_Retrato : TrptPickList_Retrato_novo;
begin

  {--picking em paisagem--}

  if (frmMenu.LeParametro('TPPICKINGLIST') = 'P') then
  begin

      rptPickList := TrptPickList.Create(nil);

      rptPickList.SPREL_PICK_LIST.Close;
      rptPickList.SPREL_PICK_LIST.Parameters.ParamByName('@nCdPickList').Value := nCdPickList;
      rptPickList.SPREL_PICK_LIST.Open;

      rptPickList.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      try
          try

              rptPickList.QuickRep1.Prepare;
              rptPickList.QRTotalPagina.Caption := '/' + IntToStr(rptPickList.QuickRep1.QRPrinter.PageCount) ;
              rptPickList.QuickRep1.QRPrinter.Free;
              rptPickList.QuickRep1.QRPrinter := nil ;

              rptPickList.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o da tela. Classe:  ' + rptPickList.ClassName) ;
              raise ;
          end;
      finally
          FreeAndNil(rptPickList);
      end;

  end
  else
  begin

      rptPickList_Retrato := TrptPickList_Retrato_novo.Create(nil);

      rptPickList_Retrato.qryPedido.Close;
      rptPickList_Retrato.qryPedido.Parameters.ParamByName('nPK').Value := nCdPickList;
      rptPickList_Retrato.qryPedido.Open;

      try
          try

              rptPickList_Retrato.QuickRep1.Prepare;
              rptPickList_Retrato.QRTotalPagina.Caption := '/' + IntToStr(rptPickList_Retrato.QuickRep1.QRPrinter.PageCount) ;
              rptPickList_Retrato.QuickRep1.QRPrinter.Free;
              rptPickList_Retrato.QuickRep1.QRPrinter := nil ;

              rptPickList_Retrato.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o da tela. Classe:  ' + rptPickList_Retrato.ClassName) ;
              raise ;
          end;
      finally
          FreeAndNil(rptPickList_Retrato);
      end;

  end ;

  if (not PrimImp) then
      exit;

  if (MessageDlg('Deseja registrar a hora de impress�o do Picking ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryRegHoraProd.Close;
      qryRegHoraProd.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      qryRegHoraProd.Parameters.ParamByName('nCdPickList').Value := nCdPickList;
      qryRegHoraProd.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;
end;

procedure TfrmGerarPickingList.qryItensPickingBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItensPickingnQtdeSelecionado.Value < 0) then
  begin
      MensagemAlerta('A Quantidade Selecionada n�o pode ser menor que 0.');
      abort;
  end;

  if (qryItensPickingnQtdeSelecionado.Value > qryItensPickingnQtdeDisponivel.Value) then
  begin
      MensagemAlerta('O Quantidade Dispon�vel n�o � suficiente para atender este Picking.');
      abort;
  end;

end;

end.
