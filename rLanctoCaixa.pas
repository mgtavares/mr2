unit rLanctoCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptLanctoCaixa = class(TfrmRelatorio_Padrao)
    qryTipoLancto: TADOQuery;
    qryContaCaixa: TADOQuery;
    DataSource1: TDataSource;
    dsTerceiro: TDataSource;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit6: TMaskEdit;
    RadioGroup1: TRadioGroup;
    MaskEdit4: TMaskEdit;
    DBEdit4: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    Image2: TImage;
    dsLoja: TDataSource;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    qryContaCaixanCdContaBancaria: TIntegerField;
    qryContaCaixanCdConta: TStringField;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    CheckBox1: TCheckBox;
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptLanctoCaixa: TrptLanctoCaixa;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rLanctoCaixa_view;

{$R *.dfm}

procedure TrptLanctoCaixa.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryContaCaixa.Close ;

  if ((Trim(MaskEdit4.Text) = '') AND (Trim(MaskEdit6.Text) <> '')) then
  begin
      MensagemAlerta('Para selecionar uma Conta/Caixa Selecione uma loja.');
      MaskEdit6.Text := '';
      MaskEdit4.SetFocus;
      exit;
  end ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin
      qryContaCaixa.Parameters.ParamByName('nCdLoja').Value := MaskEdit4.Text ;
      PosicionaQuery(qryContaCaixa, MaskEdit6.text);
  end

end;

procedure TrptLanctoCaixa.ToolButton1Click(Sender: TObject);
var
  objRel : TrptLanctoCaixa_view ;
begin

  objRel := TrptLanctoCaixa_view.Create( Self ) ;

  objRel.usp_Relatorio.Close ;

  {-- Envia os dados para a procedure --}

  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value                 := frmMenu.ConvInteiro(MaskEdit4.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdContaBancaria').Value        := frmMenu.ConvInteiro(MaskEdit6.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTipoLancto').Value           := frmMenu.ConvInteiro(MaskEdit3.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value              := frmMenu.ConvData(MaskEdit1.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value                := frmMenu.ConvData(MaskEdit2.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@cTipo').Value                   := RadioGroup1.ItemIndex;

  if(CheckBox1.Checked) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cExibeSangriaSuprimento').Value := 1
  else objRel.usp_Relatorio.Parameters.ParamByName('@cExibeSangriaSuprimento').Value := 0;
  {-- abre a procedure para exibi��o dos dados no relat�rio --}

  objRel.usp_Relatorio.Open ;

  {-- aqui preenche os filtros do relat�rio--}

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (Trim(MaskEdit4.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  'Loja : ' + trim(MaskEdit4.Text) + '-' + DbEdit4.Text ;

  if (Trim(MaskEdit6.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  '/ Caixa/Cofre : ' + trim(MaskEdit6.Text) + '-' + DbEdit2.Text ;

  if (Trim(MaskEdit3.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  '/ Tipo de Lan�amento : ' + trim(MaskEdit3.Text) + '-' + DbEdit1.Text ;

  case RadioGroup1.ItemIndex of
      0: objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  '/ Tipo de Lan�amento : Todos' ;
      1: objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  '/ Tipo de Lan�amento : Entrada' ;
      2: objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  '/ Tipo de Lan�amento : Sa�da' ;
  end;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Lan�amento: ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptLanctoCaixa.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        If ((Trim(MaskEdit4.Text) <> '')) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(131,'ContaBancaria.nCdLoja = ' + MaskEdit4.Text);
        end
        else begin
            MensagemAlerta('Para selecionar uma Conta/Caixa Selecione uma loja.');
            MaskEdit4.SetFocus;
            abort;
        end ;


        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptLanctoCaixa.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptLanctoCaixa.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TrptLanctoCaixa.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(130,'TipoLancto.cFlgContaCaixa = 1');

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;
end;

procedure TrptLanctoCaixa.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, MaskEdit3.Text) ;
end ;


initialization
     RegisterClass(TrptLanctoCaixa) ;

end.
