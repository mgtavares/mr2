unit fCaixa_Funcoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, jpeg, ExtCtrls, StdCtrls, cxButtons;

type
  TfrmCaixa_Funcoes = class(TForm)
    btSair: TcxButton;
    btBordero: TcxButton;
    btFechamento: TcxButton;
    btLancto: TcxButton;
    btEstorno: TcxButton;
    btReimpressao: TcxButton;
    btECF: TcxButton;
    btSangria: TcxButton;
    btSuprimento: TcxButton;
    btReembolso: TcxButton;
    btTEF: TcxButton;
    btSAT: TcxButton;
    procedure btBorderoClick(Sender: TObject);
    procedure btFechamentoClick(Sender: TObject);
    procedure BorderoCaixa;
    procedure btSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TrataTeclaFuncao(Key: Word);
    procedure btBorderoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFechamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btReimpressaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSangriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSuprimentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btLanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btEstornoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btECFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTEFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btReembolsoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSairKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSangriaClick(Sender: TObject);
    procedure btSuprimentoClick(Sender: TObject);
    procedure btLanctoClick(Sender: TObject);
    procedure btECFClick(Sender: TObject);
    procedure btTEFClick(Sender: TObject);
    procedure btReembolsoClick(Sender: TObject);
    procedure btEstornoClick(Sender: TObject);
    procedure btReimpressaoClick(Sender: TObject);
    procedure btSATKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSATClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cFlgUsaECF       : boolean ;
    cFlgUsaGaveta    : boolean ;
    cFlgUsaCFe       : Boolean;
    cFlgUsaCFeComp   : Boolean;
    nCdResumoCaixa   : integer ;
    nCdContaBancaria : integer ;
    dDtAbertura      : TDateTime ;
    cNmCaixa         : string ;
  end;

var
  frmCaixa_Funcoes: TfrmCaixa_Funcoes;

implementation

uses fCaixa_Fechamento, fCaixa_Recebimento, fMenu, rBorderoCaixa_view,
  fCaixa_Sangria, fCaixa_Suprimento, fCaixa_LanctoMan, fFuncoesECF,
  fCaixa_ReembolsoVale, pasModuloTEF, fCaixa_EstornoDia,
  fReimpressaoCarnet, fCaixa_Reimpressao, rBorderoCaixaNovo,
  fPdvSupervisor,fBuscaBorderoData, fFuncoesSAT;

{$R *.dfm}

var
    objCaixa_Recebimento : TfrmCaixa_Recebimento ;

procedure TfrmCaixa_Funcoes.BorderoCaixa;
begin
    rptBorderoCaixa_view.cmdPreparaTemp.Execute ;

    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.Close ;
    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@dDtInicial').Value       := dateToStr(dDtAbertura);
    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@dDtFinal').Value         := dateToStr(dDtAbertura);
    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.nCdLojaAtiva;
    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@nCdContaBancaria').Value := nCdContaBancaria;
    rptBorderoCaixa_view.SPREL_BORDERO_CAIXA.ExecProc ;

    rptBorderoCaixa_view.qryTemp_Resumo_Vendas.Close ;
    rptBorderoCaixa_view.qryTemp_Resumo_Vendas.Open ;

    rptBorderoCaixa_view.qryTemp_Resumo_Recebimento.Close ;
    rptBorderoCaixa_view.qryTemp_Resumo_Recebimento.Open ;

    rptBorderoCaixa_view.qryTemp_Resumo_Lanctos.Close ;
    rptBorderoCaixa_view.qryTemp_Resumo_Lanctos.Open ;

    rptBorderoCaixa_view.qryTemp_Resumo_Caixa.Close ;
    rptBorderoCaixa_view.qryTemp_Resumo_Caixa.Open ;

    rptBorderoCaixa_view.lblLoja.Caption  := frmMenu.cNmLojaAtiva;
    rptBorderoCaixa_view.lblCaixa.Caption := cNmCaixa ;

    rptBorderoCaixa_view.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

    rptBorderoCaixa_view.lblInicial.Caption := dateToStr(dDtAbertura);
    rptBorderoCaixa_view.lblFinal.Caption   := dateToStr(dDtAbertura);
    rptBorderoCaixa_view.lblUsuario.Caption := frmMenu.cNmUsuarioLogado;

    rptBorderoCaixa_view.Preview;

end;

procedure TfrmCaixa_Funcoes.btBorderoClick(Sender: TObject);
var
  objRel : TrptBorderoCaixaNovo ;
  objBusca : TfrmBuscaBorderoData;
begin

  objBusca := TfrmBuscaBorderoData.Create(nil);

  objBusca.nCdContaBancaria := nCdContaBancaria;
  objBusca.ShowModal;

  nCdResumoCaixa := objBusca.nCdResumoCaixa;

  if (nCdResumoCaixa <= 0) then
  begin
      FreeAndNil(objBusca);
      exit;
  end;

  if not objCaixa_Recebimento.AutorizacaoGerente(25) then
  begin
      frmMenu.MensagemAlerta('Esta transa��o requer uma autoriza��o n�vel gerente para que seja conclu�da.') ;
      exit ;
  end ;

  objRel := TrptBorderoCaixaNovo.Create( Self ) ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.BorderoIndividual(ncdResumocaixa) ;

      except
          frmMenu.MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
      FreeAndNil(objBusca);
  end;

end;

procedure TfrmCaixa_Funcoes.btFechamentoClick(Sender: TObject);
var
  objFechamentoCaixa : TfrmCaixa_Fechamento;
begin

    if (nCdResumoCaixa = 0) then
    begin
        frmMenu.MensagemErro('Nenhum caixa aberto.') ;
        abort ;
    end ;

    objFechamentoCaixa := TfrmCaixa_Fechamento.Create( Self ) ;

    objFechamentoCaixa.nCdResumoCaixa   := nCdResumoCaixa ;
    objFechamentoCaixa.cFlgUsaECF       := cFlgUsaECF     ;
    objFechamentoCaixa.nCdContaBancaria := nCdContaBancaria ;
    objFechamentoCaixa.dDtAbertura      := dDtAbertura ;
    objFechamentoCaixa.cNmCaixa         := cNmCaixa ;

    { -- verifica se utiliza gaveta eletr�nica -- }
    if (cFlgUsaGaveta) then
        frmMenu.prAbreGaveta;

    { -- valida par�metros de falta/sobra -- }
    try
        StrToFloat(frmMenu.LeParametro('PERMFALTACXDIN'));
        StrToFloat(frmMenu.LeParametro('PERMSOBRACXDIN'));
    except
        FreeAndNil(objFechamentoCaixa);
        frmMenu.MensagemErro('Parametro PERMFALTACXDIN/PERMSOBRACXDIN n�o configurado corretamente.' + #13#13 + 'Exemplo: 0,00');
        Abort;
    end;

    objFechamentoCaixa.ShowModal ;

    objCaixa_Recebimento.btNovoPedido.Enabled := false ;

    try
        if (objFechamentoCaixa.nCdResumoCaixa = -1) then
        begin
            nCdResumoCaixa := -1 ;
            close ;
        end ;
    finally

        freeAndNil( objFechamentoCaixa ) ;

    end ;
    
end;

procedure TfrmCaixa_Funcoes.btSairClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmCaixa_Funcoes.FormShow(Sender: TObject);
begin

    {-- cria um vinculo com o objeto pai para referencias --}
    objCaixa_Recebimento := (Self.Owner as TfrmCaixa_Recebimento) ;
    
    btFechamento.Enabled := (nCdResumoCaixa > 0) ;
    btLancto.Enabled     := (nCdResumoCaixa > 0) ;
    btEstorno.Enabled    := (nCdResumoCaixa > 0) ;
    btSangria.Enabled    := (nCdResumoCaixa > 0) ;
    btSuprimento.Enabled := (nCdResumoCaixa > 0) ;
    btReembolso.Enabled  := (nCdResumoCaixa > 0) ;
    btSAT.Enabled        := (nCdResumoCaixa > 0) ;
    btBordero.Enabled    := (nCdContaBancaria > 0);

    if (nCdContaBancaria > 0) then
        btBordero.SetFocus;
end;

procedure TfrmCaixa_Funcoes.TrataTeclaFuncao(Key: Word);
begin
    case Key of
        VK_F1  : if (btBordero.Enabled)     then btBordero.Click;
        VK_F2  : if (btFechamento.Enabled)  then btFechamento.Click ;
        VK_F3  : if (btReimpressao.Enabled) then btReimpressao.Click ;
        VK_F4  : if (btSangria.Enabled)     then btSangria.Click ;
        VK_F5  : if (btSuprimento.Enabled)  then btSuprimento.Click ;
        VK_F6  : if (btLancto.Enabled)      then btLancto.Click ;
        VK_F7  : if (btEstorno.Enabled)     then btEstorno.Click ;
        VK_F8  : if (btECF.Enabled)         then btECF.Click ;
        VK_F9  : if (btSAT.Enabled)         then btSAT.Click ;
        VK_F10 : if (btReembolso.Enabled)   then btReembolso.Click ;
        VK_F11 : if (btTEF.Enabled)         then btTEF.Click ;
        VK_F12 : btSair.Click;

    end ;
end;

procedure TfrmCaixa_Funcoes.btBorderoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key);
end;

procedure TfrmCaixa_Funcoes.btFechamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btReimpressaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btSangriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btSuprimentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btLanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btEstornoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btECFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btTEFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btReembolsoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btSairKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Funcoes.btSangriaClick(Sender: TObject);
var
    objForm : TfrmCaixa_Sangria ;
begin

    objForm := TfrmCaixa_Sangria.Create( Self );

    objForm.cFlgUsaECF     := cFlgUsaECF;
    objForm.nCdResumoCaixa := nCdResumoCaixa;
    objForm.bSenha         := True ;

    { -- verifica se utiliza gaveta eletr�nica -- }
    if (cFlgUsaGaveta) then
        frmMenu.prAbreGaveta;

    objForm.ShowModal;
    

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Funcoes.btSuprimentoClick(Sender: TObject);
var
    objForm : TfrmCaixa_Suprimento ;
begin

    objForm := TfrmCaixa_Suprimento.Create( Self );

    objForm.cFlgUsaECF     := cFlgUsaECF;
    objForm.nCdResumoCaixa := nCdResumoCaixa;
    objForm.bSenha         := True ;

    { -- verifica se utiliza gaveta eletr�nica -- }
    if (cFlgUsaGaveta) then
        frmMenu.prAbreGaveta;

    objForm.ShowModal;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Funcoes.btLanctoClick(Sender: TObject);
var
    objForm : TfrmCaixa_LanctoMan ;
begin

    objForm := TfrmCaixa_LanctoMan.Create( Self ) ;

    objForm.nCdResumoCaixa := nCdResumoCaixa ;
    objForm.cFlgUsaECF     := cFlgUsaECF     ;

    { -- verifica se utiliza gaveta eletr�nica -- }
    if (cFlgUsaGaveta) then
        frmMenu.prAbreGaveta;

    objForm.ShowModal;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Funcoes.btECFClick(Sender: TObject);
var
    objForm : TfrmFuncoesECF ;
begin

    objForm := TfrmFuncoesECF.Create( Self ) ;

    objForm.mResp.Clear;

    objForm.ShowModal;

    freeAndNil( objForm ) ;

    exit ;
end;

procedure TfrmCaixa_Funcoes.btTEFClick(Sender: TObject);
begin
  frmMenu.MensagemAlerta('M�dulo TEF em constru��o.');
  Exit;

  if (frmMenu.MessageDlg('Deseja acessar o m�dulo de administra��o do TEF ?',mtConfirmation,[mbYes,mbNo],0) = mrYes) then
  begin
      FuncaoAdministrativaTEF( Date );
      ImprimeGerencial(0) ;
  end ;
end;

procedure TfrmCaixa_Funcoes.btReembolsoClick(Sender: TObject);
var
    objForm : TfrmCaixa_ReembolsoVale ;
begin

    objForm := TfrmCaixa_ReembolsoVale.Create( Self ) ;

    objForm.nCdResumoCaixa := nCdResumoCaixa ;

    { -- verifica se utiliza gaveta eletr�nica -- }
    if (cFlgUsaGaveta) then
        frmMenu.prAbreGaveta;

    objForm.ShowModal;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Funcoes.btEstornoClick(Sender: TObject);
var
    objForm : TfrmCaixa_EstornoDia ;
begin

    if objCaixa_Recebimento.AutorizacaoGerente(26) then
    begin

        objForm := TfrmCaixa_EstornoDia.Create( Self );

        {-- passa o c�digo do usu�rio que autorizou o estorno para a tela de estorno --}
        objForm.nCdUsuarioAutorizador := objCaixa_Recebimento.nCdUsuarioSupervisor;
        objForm.nCdResumoCaixa        := nCdResumoCaixa ;
        objForm.cFlgUsaGaveta         := cFlgUsaGaveta;
        objForm.cFlgUsaCFe            := cFlgUsaCFe;
        
        objForm.ShowModal;

        freeAndNil( objForm ) ;

    end
    else frmMenu.mensagemAlerta('Autentica��o Falhou.') ;

end;

procedure TfrmCaixa_Funcoes.btReimpressaoClick(Sender: TObject);
var
    objForm : TfrmCaixa_Reimpressao ;
begin

    objForm := TfrmCaixa_Reimpressao.Create( Self ) ;

    objForm.nCdResumoCaixa := nCdResumoCaixa ;

    objForm.ShowModal;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Funcoes.btSATKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TrataTeclaFuncao(Key);
end;

procedure TfrmCaixa_Funcoes.btSATClick(Sender: TObject);
var
  objFuncoesSAT : TfrmFuncoesSAT;
begin
  if (not cFlgUsaCFe) then
  begin
      frmMenu.MensagemAlerta('M�dulo SAT n�o habilitado para este terminal.');
      Abort;
  end;

  if (cFlgUsaCFeComp) then
  begin
      frmMenu.MensagemAlerta('SAT compartilhado n�o permite utilizar fun��es remotas.');
      Abort;
  end;

  objFuncoesSAT := TfrmFuncoesSAT.Create(Self);

  frmMenu.showForm(objFuncoesSAT, True);
end;

end.
