unit rComissaoRepres;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, DBCtrls, ADODB;

type
  TrptComissaoRepres = class(TfrmRelatorio_Padrao)
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    qryRepresentante: TADOQuery;
    qryRepresentantenCdTerceiro: TIntegerField;
    qryRepresentantecNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    qryUsuario: TADOQuery;
    qryUsuarionCdTerceiroResponsavel: TIntegerField;
    qryUsuariocFlgRepresentante: TIntegerField;
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptComissaoRepres: TrptComissaoRepres;

implementation

uses fLookup_Padrao, rComissaoRepres_view, fMenu,
  rComissaoRepresSintetico_View;

{$R *.dfm}

procedure TrptComissaoRepres.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (MaskEdit3.ReadOnly) then
            exit ;
            
        nPK := frmLookup_Padrao.ExecutaConsulta(103);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRepresentante, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptComissaoRepres.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryRepresentante.Close ;
  PosicionaQuery(qryRepresentante, MaskEdit3.Text) ;

end;

procedure TrptComissaoRepres.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, MaskEdit6.Text) ;
  
end;

procedure TrptComissaoRepres.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptComissaoRepres.ToolButton1Click(Sender: TObject);
var
  cFiltro    : String ;
  objRel     : TrptComissaoRepres_view;
  objRelSin  : TrptComissaoRepresSintetico_view;

begin
  inherited;

  objRel    := TrptComissaoRepres_view.Create(nil);
  objRelSin := TrptComissaoRepresSintetico_view.Create(nil);

  Try
      Try
          if RadioButton1.Checked then
          begin
              objRel.qryResultado.Close ;
              objRel.qryResultado.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
              objRel.qryResultado.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
              objRel.qryResultado.Parameters.ParamByName('nCdMarca').Value    := frmMenu.ConvInteiro(MaskEdit6.Text) ;
              objRel.qryResultado.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
              objRel.qryResultado.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text) ;
              objRel.qryResultado.Open ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

              cFiltro := ' ' ;

              if (DBedit1.Text <> '') then
                  cFiltro := cFiltro + '/Representante : ' + trim(MaskEdit3.Text) + ' - ' + DbEdit1.Text ;

              if (DBedit2.Text <> '') then
                  cFiltro := cFiltro + '/Marca : ' + MaskEdit6.Text + ' - ' + DbEdit2.Text ;

              cFiltro := cFiltro + ' Per�odo de Faturamento: ' + MaskEdit1.Text + ' a ' + MaskEdit2.Text ;

              objRel.lblFiltro1.Caption := cFiltro ;

              objRel.QuickRep1.PreviewModal;

          end;

          if RadioButton2.Checked then
          begin
              MaskEdit6.Clear;
              DBEdit2.Text := '';

              objRelSin.qryResultado.Close ;
              objRelSin.qryResultado.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
              objRelSin.qryResultado.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
              objRelSin.qryResultado.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
              objRelSin.qryResultado.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text) ;
              objRelSin.qryResultado.Open ;

              objRelSin.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

              cFiltro := ' ' ;

              if (DBedit1.Text <> '') then
              cFiltro := cFiltro + '/Representante : ' + trim(MaskEdit3.Text) + ' - ' + DbEdit1.Text ;

              cFiltro := cFiltro + ' Per�odo de Faturamento: ' + MaskEdit1.Text + ' a ' + MaskEdit2.Text ;

              objRelSin.lblFiltro1.Caption := cFiltro ;

              objRelSin.QuickRep1.PreviewModal;
          end;

      except
          MensagemErro('Erro na cria��o do Relat�rio');
         raise;
      end;
  finally
      FreeAndNil(objRel);
      FreeAndNil(objRelSin);
  end;

end;

procedure TrptComissaoRepres.FormCreate(Sender: TObject);
begin
  inherited;
  RadioButton1.Checked := true;
end;

procedure TrptComissaoRepres.RadioButton2Click(Sender: TObject);
begin
  inherited;
  MaskEdit6.Clear;
  DBEdit2.Text := '';
end;

procedure TrptComissaoRepres.FormShow(Sender: TObject);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, intToStr(frmMenu.nCdUsuarioLogado));

  MaskEdit3.SetFocus;

  {-- se o usu�rio logado for representante, j� posiciona o mesmo na tela e n�o deixa alterar --}
  if (qryUsuariocFlgRepresentante.Value = 1) then
  begin
      desativaMaskEdit( MaskEdit3 ) ;
      MaskEdit3.Text := qryUsuarionCdTerceiroResponsavel.asString ;
      PosicionaQuery( qryRepresentante , qryUsuarionCdTerceiroResponsavel.asString) ;
      MaskEdit6.SetFocus;
  end ;

end;

initialization
    RegisterClass(TrptComissaoRepres) ;

end.
