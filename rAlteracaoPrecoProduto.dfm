inherited rptAlteracaoPrecoProduto: TrptAlteracaoPrecoProduto
  Left = 77
  Top = 171
  Caption = 'Rel. Altera'#231#227'o Pre'#231'o Produto'
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel [1]
    Left = 188
    Top = 48
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 48
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Altera'#231#227'o'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtDtFinal: TMaskEdit [4]
    Left = 216
    Top = 40
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [5]
    Left = 104
    Top = 40
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
end
