unit fMRP_ListaPedidosDia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB;

type
  TfrmMRP_ListaPedidoDia = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryPedido: TADOQuery;
    dsPedidos: TDataSource;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidocNmLoja: TStringField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonValPedido: TBCDField;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPedido: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_ListaPedidoDia: TfrmMRP_ListaPedidoDia;

implementation

uses rPedidoCom_Simples, fMenu, rPedCom_Contrato;

{$R *.dfm}

procedure TfrmMRP_ListaPedidoDia.FormShow(Sender: TObject);
begin
  inherited;

  qryPedido.Close ;
  qryPedido.Open ;

  if (qryPedido.eof) then
      MensagemAlerta('Nenhum pedido dispon�vel para impress�o.') ;
      
end;

procedure TfrmMRP_ListaPedidoDia.cxGrid1DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var
  objRel : TrptPedCom_Contrato;
begin
  inherited;

  if (qryPedido.Active) then
  begin

      objRel := TrptPedCom_Contrato.Create(nil);

      try
          try
              PosicionaQuery(objRel.qryPedido,qryPedidonCdPedido.asString) ;
              PosicionaQuery(objRel.qryItemEstoque_Grade,qryPedidonCdPedido.asString) ;
              PosicionaQuery(objRel.qryItemAD,qryPedidonCdPedido.asString) ;
              PosicionaQuery(objRel.qryItemFormula,qryPedidonCdPedido.asString) ;

              objRel.QRSubDetail1.Enabled := True ;
              objRel.QRSubDetail2.Enabled := True ;
              objRel.QRSubDetail3.Enabled := True ;

              if (objRel.qryItemEstoque_Grade.eof) then
                  objRel.QRSubDetail1.Enabled := False ;

              if (objRel.qryItemAD.eof) then
                  objRel.QRSubDetail2.Enabled := False ;

              if (objRel.qryItemFormula.eof) then
                  objRel.QRSubDetail3.Enabled := False ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

              objRel.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
              raise;
          end;
      finally
          FreeAndNil(objRel);
      end;

  end;
end;
end.
