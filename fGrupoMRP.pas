unit fGrupoMRP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh;

type
  TfrmGrupoMRP = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoMRP: TIntegerField;
    qryMastercNmGrupoMRP: TStringField;
    qryUsuarioGrupoMRP: TADOQuery;
    qryUsuarioGrupoMRPnCdUsuarioGrupoMRP: TAutoIncField;
    qryUsuarioGrupoMRPnCdGrupoMRP: TIntegerField;
    qryUsuarioGrupoMRPnCdUsuario: TIntegerField;
    qryUsuarioGrupoMRPcNmUsuario: TStringField;
    dsUsuarioGrupoMRP: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBGridEh1: TDBGridEh;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryMasternCdTipoRequisicao: TIntegerField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryTipoRequisicao: TADOQuery;
    qryTipoRequisicaonCdTipoRequisicao: TIntegerField;
    qryTipoRequisicaocNmTipoRequisicao: TStringField;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    qryUsuarioGrupoMRPnCdEmpresa: TIntegerField;
    qryUsuarioGrupoMRPnCdLoja: TIntegerField;
    qryUsuarioGrupoMRPcNmEmpresa: TStringField;
    qryUsuarioGrupoMRPcNmLoja: TStringField;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryMasternCdSetor: TIntegerField;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    qrySetor: TADOQuery;
    qrySetornCdSetor: TIntegerField;
    qrySetorcNmSetor: TStringField;
    DBEdit6: TDBEdit;
    DataSource2: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryUsuarioGrupoMRPBeforePost(DataSet: TDataSet);
    procedure qryUsuarioGrupoMRPCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoMRP: TfrmGrupoMRP;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmGrupoMRP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOMRP' ;
  nCdTabelaSistema  := 76 ;
  nCdConsultaPadrao := 174 ;

end;

procedure TfrmGrupoMRP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.post ;
  
end;

procedure TfrmGrupoMRP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioGrupoMRP.Close ;
  qryTipoRequisicao.Close ;
  qrySetor.Close ;

  PosicionaQuery(qryTipoRequisicao, qryMasternCdTipoRequisicao.AsString) ;
  PosicionaQuery(qrySetor, qryMasternCdSetor.AsString) ;
  PosicionaQuery(qryUsuarioGrupoMRP, qryMasternCdGrupoMRP.AsString) ;

end;

procedure TfrmGrupoMRP.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGrupoMRP.Close ;
  qryTipoRequisicao.Close ;
  qrySetor.Close ;
end;

procedure TfrmGrupoMRP.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus ;
  
end;

procedure TfrmGrupoMRP.qryUsuarioGrupoMRPBeforePost(DataSet: TDataSet);
begin

  if (qryUsuarioGrupoMRPcNmEmpresa.Value = '') then
  begin
      MensagemAlerta('Selecione uma empresa.') ;
      abort ;
  end ;

  if (qryUsuarioGrupoMRPcNmLoja.Value = '') and (not DBGridEh1.Columns[4].ReadOnly) then
  begin
      MensagemAlerta('Selecione uma Loja.') ;
      abort ;
  end ;

  if (qryUsuarioGrupoMRPcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um Usu�rio.') ;
      abort ;
  end ;

  inherited;

  qryUsuarioGrupoMRPnCdGrupoMRP.Value := qryMasternCdGrupoMRP.Value ;

end;

procedure TfrmGrupoMRP.qryUsuarioGrupoMRPCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioGrupoMRPcNmEmpresa.Value = '') and (qryUsuarioGrupoMRPnCdEmpresa.Value > 0) then
  begin

      qryEmpresa.Close ;
      PosicionaQuery(qryEmpresa, qryUsuarioGrupoMRPnCdEmpresa.AsString) ;

      if not qryEmpresa.Eof then
          qryUsuarioGrupoMRPcNmEmpresa.Value := qryEmpresacNmEmpresa.Value ;

  end ;

  if (qryUsuarioGrupoMRPcNmLoja.Value = '') and (qryUsuarioGrupoMRPnCdLoja.Value > 0) then
  begin

      qryLoja.Close ;
      qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryUsuarioGrupoMRPnCdEmpresa.Value ;

      PosicionaQuery(qryLoja, qryUsuarioGrupoMRPnCdLoja.AsString) ;

      if not qryLoja.Eof then
          qryUsuarioGrupoMRPcNmLoja.Value := qryLojacNmLoja.Value ;

  end ;

  if (qryUsuarioGrupoMRPcNmUsuario.Value = '') and (qryUsuarioGrupoMRPnCdUsuario.Value > 0) then
  begin
      qryUsuario.Close ;
      PosicionaQuery(qryUsuario, qryUsuarioGrupoMRPnCdUsuario.AsString) ;

      if not qryUsuario.Eof then
          qryUsuarioGrupoMRPcNmUsuario.Value := qryUsuariocNmUsuario.Value ;

  end ;

end;

procedure TfrmGrupoMRP.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioGrupoMRP.State = dsBrowse) then
             qryUsuarioGrupoMRP.Edit ;

        if (qryUsuarioGrupoMRP.State = dsInsert) or (qryUsuarioGrupoMRP.State = dsEdit) then
        begin

            // empresa
            if (dbGridEh1.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(25);

                If (nPK > 0) then
                begin
                    qryUsuarioGrupoMRPnCdEmpresa.Value := nPK ;
                end ;
            end ;


            // loja
            if (dbGridEh1.Col = 5) then
            begin
                if (qryUsuarioGrupoMRPcNmEmpresa.Value = '') then
                begin
                    MensagemAlerta('Selecione uma empresa.') ;
                    DBGridEh1.Col := 3 ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(147,'Loja.nCdEmpresa = ' + qryUsuarioGrupoMRPnCdEmpresa.asString);

                If (nPK > 0) then
                begin
                    qryUsuarioGrupoMRPnCdLoja.Value := nPK ;
                end ;
            end ;


            // usuario
            if (dbGridEh1.Col = 7) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(5);

                If (nPK > 0) then
                begin
                    qryUsuarioGrupoMRPnCdUsuario.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoMRP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryUsuarioGrupoMRP, qryMasternCdGrupoMRP.AsString) ;

end;

procedure TfrmGrupoMRP.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTipoRequisicao.Close ;
  PosicionaQuery(qryTipoRequisicao, DBEdit3.Text) ;
  
end;

procedure TfrmGrupoMRP.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(121,'nCdTabTipoRequis = 2');

            If (nPK > 0) then
            begin
                qryMasternCdTipoRequisicao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoMRP.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do grupo.') ;
      DBEDit2.SetFocus ;
      abort ;
  end ;

  if (DBEdit3.Text <> '') and (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione um tipo de requisi��o v�lido ou deixa em branco.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (DBEdit4.Text <> '') and (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Selecione um setor para o tipo de requisi��o.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit6.Text <> '') and (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione um tipo de requisi��o para o setor.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmGrupoMRP.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      DBGridEh1.Columns[4].ReadOnly         := True ;
      DBGridEh1.Columns[4].Title.Font.Color := clRed ;
  end ;

end;

procedure TfrmGrupoMRP.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qrySetor.Close ;
  PosicionaQuery(qrySetor, DBEdit5.Text) ;
  
end;

procedure TfrmGrupoMRP.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(122);

            If (nPK > 0) then
            begin
                qryMasternCdSetor.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmGrupoMRP) ;
    
end.
