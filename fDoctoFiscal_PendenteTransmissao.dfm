inherited frmDoctoFiscal_PendenteTransmissao: TfrmDoctoFiscal_PendenteTransmissao
  Left = 63
  Top = 30
  Width = 1256
  Height = 648
  Caption = 'Docto Fiscal Pendente de Transmiss'#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1240
    Height = 581
  end
  inherited ToolBar1: TToolBar
    Width = 1240
    ButtonWidth = 76
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 76
    end
    inherited ToolButton2: TToolButton
      Left = 84
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1240
    Height = 581
    ActivePage = tabDPEC
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 577
    ClientRectLeft = 4
    ClientRectRight = 1236
    ClientRectTop = 24
    object tabDPEC: TcxTabSheet
      Caption = 'Conting'#234'ncia - DPEC'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1232
        Height = 553
        Align = alClient
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsDoctoFiscal
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1nCdDoctoFiscal: TcxGridDBColumn
            DataBinding.FieldName = 'nCdDoctoFiscal'
            Width = 80
          end
          object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
            DataBinding.FieldName = 'dDtEmissao'
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 239
          end
          object cxGrid1DBTableView1nValTotal: TcxGridDBColumn
            DataBinding.FieldName = 'nValTotal'
            Width = 83
          end
          object cxGrid1DBTableView1cCFOP: TcxGridDBColumn
            DataBinding.FieldName = 'cCFOP'
            Width = 61
          end
          object cxGrid1DBTableView1cTextoCFOP: TcxGridDBColumn
            DataBinding.FieldName = 'cTextoCFOP'
          end
          object cxGrid1DBTableView1cFlgEntSai: TcxGridDBColumn
            DataBinding.FieldName = 'cFlgEntSai'
            Width = 89
          end
          object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTerceiro'
            Visible = False
          end
          object cxGrid1DBTableView1cNmCondPagto: TcxGridDBColumn
            DataBinding.FieldName = 'cNmCondPagto'
            Visible = False
          end
          object cxGrid1DBTableView1cSigla: TcxGridDBColumn
            DataBinding.FieldName = 'cSigla'
            Visible = False
            Width = 57
          end
          object cxGrid1DBTableView1nCdTerceiroTransp: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTerceiroTransp'
            Visible = False
          end
          object cxGrid1DBTableView1cNrProtocoloDPEC: TcxGridDBColumn
            DataBinding.FieldName = 'cNrProtocoloDPEC'
            Visible = False
          end
          object cxGrid1DBTableView1cCaminhoXML: TcxGridDBColumn
            DataBinding.FieldName = 'cCaminhoXML'
            Visible = False
          end
          object cxGrid1DBTableView1cArquivoXML: TcxGridDBColumn
            DataBinding.FieldName = 'cArquivoXML'
            Visible = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object tabCancelado: TcxTabSheet
      Caption = 'Cancelamento'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1232
        Height = 553
        Align = alClient
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridDBTableView1DblClick
          DataController.DataSource = dsDoctoCancelado
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGridDBColumn_nCdDoctoFiscal: TcxGridDBColumn
            Caption = 'C'#243'd. Docto'
            DataBinding.FieldName = 'nCdDoctoFiscal'
            Width = 80
          end
          object cxGridDBColumn_dDtEmissao: TcxGridDBColumn
            Caption = 'Data Emiss'#227'o'
            DataBinding.FieldName = 'dDtEmissao'
          end
          object cxGridDBColumn_cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro Entrega'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 239
          end
          object cxGridDBColumn_nValTotal: TcxGridDBColumn
            Caption = 'Valor Docto'
            DataBinding.FieldName = 'nValTotal'
            Width = 83
          end
          object cxGridDBColumn_cCFOP: TcxGridDBColumn
            Caption = 'CFOP'
            DataBinding.FieldName = 'cCFOP'
            Width = 61
          end
          object cxGridDBColumn_cTextoCFOP: TcxGridDBColumn
            Caption = 'Texto CFOP'
            DataBinding.FieldName = 'cTextoCFOP'
          end
          object cxGridDBColumn_cFlgEntSai: TcxGridDBColumn
            Caption = 'Tipo Oper.'
            DataBinding.FieldName = 'cFlgEntSai'
            Width = 89
          end
          object cxGridDBTableView1DBColumn_cNrProtocoloNFe: TcxGridDBColumn
            Caption = 'Protocolo NFe'
            DataBinding.FieldName = 'cNrProtocoloNFe'
            Width = 120
          end
          object cxGridDBTableView1DBColumn_cNrReciboNFe: TcxGridDBColumn
            Caption = 'Recibo NFe'
            DataBinding.FieldName = 'cNrReciboNFe'
            Width = 120
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 216
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDoctoFiscal'
      '      ,dDtEmissao'
      '      ,cCFOP'
      '      ,cTextoCFOP'
      '      ,CASE WHEN cFlgEntSai = '#39'S'#39' THEN '#39'Sa'#237'da'#39
      '            ELSE '#39'Entrada'#39
      '       END cFlgEntSai'
      '      ,nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,nValTotal'
      '      ,cNmCondPagto'
      '      ,cSigla'
      '      ,nCdTerceiroTransp'
      '      ,cNrProtocoloDPEC'
      '      ,cCaminhoXML'
      '      ,cArquivoXML'
      '      ,cNrProtocoloNFe'
      '      ,cNrReciboNFe'
      '      ,cFlgDPECTransmitida'
      '  FROM DoctoFiscal'
      
        '       LEFT JOIN CondPagto ON CondPagto.nCdCondPagto = DoctoFisc' +
        'al.nCdCondPagto'
      
        '       LEFT JOIN Incoterms ON Incoterms.nCdIncoterms = DoctoFisc' +
        'al.nCdIncoterms'
      ' WHERE nCdEmpresa                   = :nCdEmpresa'
      '   AND iNrDocto                     > 0'
      '   AND nCdStatus                    = 1'
      '   AND nCdTabTipoEmissaoDoctoFiscal = 4'
      '   AND cFlgDPECTransmitida          = 0')
    Left = 320
    Top = 216
    object qryDoctoFiscalnCdDoctoFiscal: TIntegerField
      DisplayLabel = 'C'#243'd. Docto'
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      DisplayLabel = 'Data Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscalcCFOP: TStringField
      DisplayLabel = 'CFOP'
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoFiscalcTextoCFOP: TStringField
      DisplayLabel = 'Texto CFOP'
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryDoctoFiscalcFlgEntSai: TStringField
      DisplayLabel = 'Tipo Oper.'
      FieldName = 'cFlgEntSai'
      ReadOnly = True
      Size = 7
    end
    object qryDoctoFiscalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoFiscalcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryDoctoFiscalnValTotal: TBCDField
      DisplayLabel = 'Valor Docto'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryDoctoFiscalcSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 3
    end
    object qryDoctoFiscalnCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryDoctoFiscalcNrProtocoloDPEC: TStringField
      FieldName = 'cNrProtocoloDPEC'
      Size = 100
    end
    object qryDoctoFiscalcCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryDoctoFiscalcArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryDoctoFiscalcNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryDoctoFiscalcFlgDPECTransmitida: TIntegerField
      FieldName = 'cFlgDPECTransmitida'
    end
    object qryDoctoFiscalcNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
  end
  object dsDoctoFiscal: TDataSource
    DataSet = qryDoctoFiscal
    Left = 320
    Top = 248
  end
  object qryDoctoCancelado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDoctoFiscal'
      '      ,dDtEmissao'
      '      ,cCFOP'
      '      ,cTextoCFOP'
      '      ,CASE WHEN cFlgEntSai = '#39'S'#39' THEN '#39'Sa'#237'da'#39
      '            ELSE '#39'Entrada'#39
      '       END cFlgEntSai'
      '      ,nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,nValTotal'
      '      ,cNrProtocoloNFe'
      '      ,cNrReciboNFe'
      '      ,cCaminhoXML'
      '      ,cArquivoXML'
      '  FROM DoctoFiscal'
      
        '       LEFT JOIN Incoterms ON Incoterms.nCdIncoterms = DoctoFisc' +
        'al.nCdIncoterms'
      ' WHERE nCdEmpresa               = :nCdEmpresa'
      '   AND nCdTipoDoctoFiscal       = 1'
      '   AND nCdStatus                = 2'
      '   AND cNrProtocoloNFe          IS NOT NULL'
      '   AND cNrProtocoloCancNFe      IS NULL'
      '   AND ISNULL(iStatusRetorno,0) <> 302')
    Left = 352
    Top = 216
    object qryDoctoCanceladonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoCanceladodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoCanceladocCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoCanceladocTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryDoctoCanceladocFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      ReadOnly = True
      Size = 7
    end
    object qryDoctoCanceladonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoCanceladocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryDoctoCanceladonValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoCanceladocNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryDoctoCanceladocNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
    object qryDoctoCanceladocCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryDoctoCanceladocArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
  end
  object dsDoctoCancelado: TDataSource
    DataSet = qryDoctoCancelado
    Left = 352
    Top = 248
  end
end
