unit fConsultaRequisAberta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmConsultaRequisAberta = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    dsRequisicao: TDataSource;
    qryRequisicao: TADOQuery;
    qryRequisicaonCdRequisicao: TIntegerField;
    qryRequisicaodDtRequisicao: TDateTimeField;
    qryRequisicaocNmSetor: TStringField;
    qryRequisicaocNmSolicitante: TStringField;
    qryRequisicaodDtAutor: TDateTimeField;
    qryRequisicaocNmUsuarioAutor: TStringField;
    qryRequisicaocNmTabStatusRequis: TStringField;
    qryRequisicaocNmTipoRequisicao: TStringField;
    qryRequisicaonCdTipoRequisicao: TIntegerField;
    qryRequisicaonCdLoja: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaRequisAberta: TfrmConsultaRequisAberta;

implementation

uses fMenu, fConsultaRequisAberta_Itens, fRequisicao;

{$R *.dfm}

procedure TfrmConsultaRequisAberta.ToolButton1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryRequisicao, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  
end;

procedure TfrmConsultaRequisAberta.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmRequisicao;
begin
  inherited;

  if (qryRequisicao.Active) then
  begin

      objForm := TfrmRequisicao.Create(nil);

      objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      objForm.PosicionaQuery(objForm.qryMaster, qryRequisicaonCdRequisicao.asString);

      objForm.btSalvar.Enabled    := false ;
      objForm.btExcluir.Enabled   := false ;
      objForm.btIncluir.Enabled   := false ;
      objForm.btConsultar.Enabled := false ;
      objForm.ToolButton1.Enabled := false ;
      showForm(objForm,true) ;

  end ;

end;

initialization
    RegisterClass(TfrmConsultaRequisAberta) ;
    
end.
