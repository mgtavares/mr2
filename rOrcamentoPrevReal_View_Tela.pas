unit rOrcamentoPrevReal_View_Tela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TrptOrcamentoPrevReal_View_Tela = class(TfrmProcesso_Padrao)
    SPREL_ORCAMENTO_PREVREAL: TADOStoredProc;
    SPREL_ORCAMENTO_PREVREALcNmOrcamento: TStringField;
    SPREL_ORCAMENTO_PREVREALcNmCC: TStringField;
    SPREL_ORCAMENTO_PREVREALnCdGrupoPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREALcNmGrupoPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREALnCdPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREALcNmPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREALnValPrevJaneiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealJaneiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercJaneiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevFevereiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealFevereiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercFevereiro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevMarco: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealMarco: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercMarco: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevAbril: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealAbril: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercAbril: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevMaio: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealMaio: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercMaio: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevJunho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealJunho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercJunho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevJulho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealJulho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercJulho: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevAgosto: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealAgosto: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercAgosto: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevSetembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealSetembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercSetembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevOutubro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealOutubro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercOutubro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevNovembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealNovembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercNovembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevDezembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealDezembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercDezembro: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValPrevTotal: TBCDField;
    SPREL_ORCAMENTO_PREVREALnValRealTotal: TBCDField;
    SPREL_ORCAMENTO_PREVREALnPercTotal: TBCDField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsSPREL_ORCAMENTO_PREVREAL: TDataSource;
    cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevJaneiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealJaneiro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercJaneiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevFevereiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealFevereiro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercFevereiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevMarco: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealMarco: TcxGridDBColumn;
    cxGrid1DBTableView1nPercMarco: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevAbril: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealAbril: TcxGridDBColumn;
    cxGrid1DBTableView1nPercAbril: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevMaio: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealMaio: TcxGridDBColumn;
    cxGrid1DBTableView1nPercMaio: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevJunho: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealJunho: TcxGridDBColumn;
    cxGrid1DBTableView1nPercJunho: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevJulho: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealJulho: TcxGridDBColumn;
    cxGrid1DBTableView1nPercJulho: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevAgosto: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealAgosto: TcxGridDBColumn;
    cxGrid1DBTableView1nPercAgosto: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevSetembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealSetembro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercSetembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevOutubro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealOutubro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercOutubro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevNovembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealNovembro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercNovembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevDezembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealDezembro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercDezembro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPrevTotal: TcxGridDBColumn;
    cxGrid1DBTableView1nValRealTotal: TcxGridDBColumn;
    cxGrid1DBTableView1nPercTotal: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOrcamentoPrevReal_View_Tela: TrptOrcamentoPrevReal_View_Tela;

implementation

{$R *.dfm}

end.
