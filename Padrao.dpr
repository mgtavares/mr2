program Padrao;

uses
  Forms,
  fCadastro_Template in 'fCadastro_Template.pas' {frmCadastro_Padrao},
  fMenu in 'fMenu.pas' {frmMenu},
  fConsulta_Template in 'fConsulta_Template.pas' {frmConsulta_Template},
  About in 'C:\Arquivos de programas\Borland\Delphi7\ObjRepos\About.pas' {AboutBox},
  fAplicacao_CONS in 'fAplicacao_CONS.pas' {frmAplicacao_CONS},
  fAplicacao_CAD in 'fAplicacao_CAD.pas' {frmAplicacao_Cad},
  fLookup_Padrao in 'fLookup_Padrao.pas' {frmLookup_Padrao},
  fBuscaPK in 'fBuscaPK.pas' {frmBuscaPK},
  fAuditoria in 'fAuditoria.pas' {frmAuditoria},
  fMenu_Cad in 'fMenu_Cad.pas' {frmMenu_Cad},
  fLogin in 'fLogin.pas' {frmLogin},
  fUsuario in 'fUsuario.pas' {frmUsuario},
  fGrupoUsuario in 'fGrupoUsuario.pas' {frmGrupoUsuario_Cad},
  fTabelaConsulta in 'fTabelaConsulta.pas' {frmTabelaConsulta},
  fSelecEmpresa in 'fSelecEmpresa.pas' {frmSelecEmpresa},
  fGrupoEspTit in 'fGrupoEspTit.pas' {frmGrupoEspTit},
  fEspTit in 'fEspTit.pas' {frmEspTit},
  fTipoSP in 'fTipoSP.pas' {frmTipoSP},
  fGrupoCategFinanc in 'fGrupoCategFinanc.pas' {frmGrupoCategFinanc},
  fCategFinanc in 'fCategFinanc.pas' {frmCategFinanc},
  fUnidadeNegocio in 'fUnidadeNegocio.pas' {frmUnidadeNegocio},
  fFormaPagto in 'fFormaPagto.pas' {frmFormaPagto},
  fMoeda in 'fMoeda.pas' {frmMoeda};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'ER2Soft';
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmCadastro_Padrao, frmCadastro_Padrao);
  Application.CreateForm(TfrmConsulta_Template, frmConsulta_Template);
  Application.CreateForm(TfrmAplicacao_CONS, frmAplicacao_CONS);
  Application.CreateForm(TfrmAplicacao_Cad, frmAplicacao_Cad);
  Application.CreateForm(TfrmLookup_Padrao, frmLookup_Padrao);
  Application.CreateForm(TfrmBuscaPK, frmBuscaPK);
  Application.CreateForm(TfrmAuditoria, frmAuditoria);
  Application.CreateForm(TfrmMenu_Cad, frmMenu_Cad);
  Application.CreateForm(TfrmUsuario, frmUsuario);
  Application.CreateForm(TfrmGrupoUsuario_Cad, frmGrupoUsuario_Cad);
  Application.CreateForm(TfrmTabelaConsulta, frmTabelaConsulta);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TfrmGrupoEspTit, frmGrupoEspTit);
  Application.CreateForm(TfrmEspTit, frmEspTit);
  Application.CreateForm(TfrmTipoSP, frmTipoSP);
  Application.CreateForm(TfrmGrupoCategFinanc, frmGrupoCategFinanc);
  Application.CreateForm(TfrmCategFinanc, frmCategFinanc);
  Application.CreateForm(TfrmUnidadeNegocio, frmUnidadeNegocio);
  Application.CreateForm(TfrmFormaPagto, frmFormaPagto);
  Application.CreateForm(TfrmMoeda, frmMoeda);
  Application.Run;
end.
