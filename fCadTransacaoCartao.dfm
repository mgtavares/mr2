inherited frmCadTransacaoCartao: TfrmCadTransacaoCartao
  Left = 224
  Width = 1034
  Caption = 'Transa'#231#227'o Cart'#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1018
  end
  object Label1: TLabel [1]
    Left = 96
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 59
    Top = 62
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Transa'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 40
    Top = 182
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data da Transa'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 86
    Width = 126
    Height = 13
    Alignment = taRightJustify
    Caption = 'Condi'#231#227'o de Pagamento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 13
    Top = 158
    Width = 121
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero do Documento'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 284
    Top = 158
    Width = 121
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero da Autoriza'#231#227'o'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 41
    Top = 110
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Operadora Cart'#227'o'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 308
    Top = 182
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor da Transa'#231#227'o'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 72
    Top = 134
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cofre'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 557
    Top = 158
    Width = 112
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status do Documento'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 568
    Top = 86
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero de Parcelas'
  end
  object Label12: TLabel [12]
    Left = 10
    Top = 206
    Width = 124
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero do Lote do POS'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 540
    Top = 110
    Width = 129
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Operadora Cart'#227'o'
  end
  object Label14: TLabel [14]
    Left = 867
    Top = 158
    Width = 98
    Height = 13
    Caption = '1-OK / 2-Extraviado'
    FocusControl = DBEdit10
  end
  inherited ToolBar2: TToolBar
    Width = 1018
    TabOrder = 11
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 138
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTransacaoCartao'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [17]
    Left = 138
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdLojaCartao'
    DataSource = dsMaster
    TabOrder = 1
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit3: TDBEdit [18]
    Left = 138
    Top = 176
    Width = 127
    Height = 19
    DataField = 'dDtTransacao'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit4: TDBEdit [19]
    Left = 138
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdCondPagtoCartao'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [20]
    Left = 138
    Top = 152
    Width = 127
    Height = 19
    DataField = 'iNrDocto'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [21]
    Left = 409
    Top = 152
    Width = 127
    Height = 19
    DataField = 'iNrAutorizacao'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [22]
    Left = 138
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit7Exit
    OnKeyDown = DBEdit7KeyDown
  end
  object DBEdit8: TDBEdit [23]
    Left = 409
    Top = 176
    Width = 127
    Height = 19
    DataField = 'nValTransacao'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit9: TDBEdit [24]
    Left = 138
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdContaBancaria'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit9Exit
    OnKeyDown = DBEdit9KeyDown
  end
  object DBEdit10: TDBEdit [25]
    Left = 672
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdStatusDocto'
    DataSource = dsMaster
    TabOrder = 7
    OnExit = DBEdit10Exit
  end
  object DBEdit12: TDBEdit [26]
    Left = 138
    Top = 200
    Width = 127
    Height = 19
    DataField = 'cNrLotePOS'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit14: TDBEdit [27]
    Tag = 1
    Left = 741
    Top = 152
    Width = 121
    Height = 19
    DataField = 'cNmStatusDocto'
    DataSource = DataSource1
    TabOrder = 12
  end
  object DBEdit15: TDBEdit [28]
    Tag = 1
    Left = 208
    Top = 80
    Width = 329
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = DataSource2
    TabOrder = 13
  end
  object DBEdit11: TDBEdit [29]
    Tag = 1
    Left = 672
    Top = 80
    Width = 65
    Height = 19
    DataField = 'iQtdeParcelas'
    DataSource = DataSource2
    TabOrder = 14
  end
  object DBEdit16: TDBEdit [30]
    Tag = 1
    Left = 208
    Top = 104
    Width = 329
    Height = 19
    DataField = 'cNmOperadoraCartao'
    DataSource = DataSource3
    TabOrder = 15
  end
  object DBEdit13: TDBEdit [31]
    Tag = 1
    Left = 672
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdGrupoOperadoraCartao'
    DataSource = DataSource3
    TabOrder = 16
  end
  object DBEdit17: TDBEdit [32]
    Tag = 1
    Left = 741
    Top = 104
    Width = 242
    Height = 19
    DataField = 'cNmGrupoOperadoraCartao'
    DataSource = DataSource3
    TabOrder = 17
  end
  object DBEdit18: TDBEdit [33]
    Tag = 1
    Left = 208
    Top = 128
    Width = 195
    Height = 19
    DataField = 'nCdConta'
    DataSource = DataSource4
    TabOrder = 18
  end
  object DBEdit19: TDBEdit [34]
    Tag = 1
    Left = 208
    Top = 56
    Width = 329
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource5
    TabOrder = 19
  end
  object btEncerrar: TcxButton [35]
    Left = 8
    Top = 232
    Width = 137
    Height = 33
    Caption = '&Gerar Financeiro'
    TabOrder = 20
    OnClick = btEncerrarClick
    Glyph.Data = {
      72020000424D720200000000000036000000280000000E0000000D0000000100
      1800000000003C020000000000000000000000000000000000008C9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      008C9C9C00000000000000840000840000840000840000840000840000840000
      8400008400008400008400008400000000000000009C9CFF0000FF0000FF0000
      FFEFFFFF0000840000840000840000840000FF0000FF00008400000000000000
      009C9CFF0000FF0000FFEFFFFFEFFFFFEFFFFFEFFFFF00008400008400008400
      008400008400000000000000009C9CFF0000FFEFFFFFEFFFFFEFFFFFEFFFFFEF
      FFFFEFFFFFEFFFFF0000840000FF00008400000000000000009C9CFF0000FF00
      00FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF000084000000
      00000000009C9CFF0000FF0000FF0000FF0000FF0000FFEFFFFFEFFFFF000084
      0000840000FF00008400000000000000009C9CFF0000FF0000FFEFFFFFEFFFFF
      EFFFFFEFFFFFEFFFFFEFFFFF0000FF0000FF00008400000000000000009C9CFF
      0000FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF0000FF0000
      8400000000000000009C9CFF0000FF0000FF0000FF0000FFEFFFFFEFFFFF0000
      840000840000FF0000FF00008400000000000000009C9CFF0000FF0000FF0000
      FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF00008400000000000000
      009C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C
      9CFF9C9CFF00000000008C9C9C00000000000000000000000000000000000000
      00000000000000000000000000000000008C9C9C0000}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TransacaoCartao'
      ' WHERE nCdTransacaoCartao =:nPK'
      '   AND cFlgInclusoManual  = 1')
    Left = 352
    Top = 232
    object qryMasternCdTransacaoCartao: TAutoIncField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryMasterdDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtCredito: TDateTimeField
      FieldName = 'dDtCredito'
    end
    object qryMasteriNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryMasteriNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryMasteriNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryMasternCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasternValTransacao: TBCDField
      FieldName = 'nValTransacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMastercFlgConferencia: TIntegerField
      FieldName = 'cFlgConferencia'
    end
    object qryMasternCdUsuarioConf: TIntegerField
      FieldName = 'nCdUsuarioConf'
    end
    object qryMasterdDtConferencia: TDateTimeField
      FieldName = 'dDtConferencia'
    end
    object qryMasternCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryMasteriParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryMastercNrLotePOS: TStringField
      FieldName = 'cNrLotePOS'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryMastercFlgPOSEncerrado: TIntegerField
      FieldName = 'cFlgPOSEncerrado'
    end
    object qryMasternCdLojaCartao: TIntegerField
      FieldName = 'nCdLojaCartao'
    end
    object qryMasternValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryMasternCdCondPagtoCartao: TIntegerField
      FieldName = 'nCdCondPagtoCartao'
    end
    object qryMastercFlgInclusoManual: TIntegerField
      FieldName = 'cFlgInclusoManual'
    end
    object qryMastercFlgGeradoFinanceiro: TIntegerField
      FieldName = 'cFlgGeradoFinanceiro'
    end
  end
  inherited dsMaster: TDataSource
    Left = 352
    Top = 264
  end
  inherited qryID: TADOQuery
    Left = 384
    Top = 232
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 416
    Top = 232
  end
  inherited qryStat: TADOQuery
    Left = 384
    Top = 264
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 416
    Top = 264
  end
  inherited ImageList1: TImageList
    Left = 320
    Top = 264
  end
  object qryStatusDocto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdStatusDocto'
      '      ,cNmStatusDocto'
      '  FROM StatusDocto'
      ' WHERE nCdStatusDocto =:nPK')
    Left = 288
    Top = 232
    object qryStatusDoctonCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryStatusDoctocNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCondPagto'
      '      ,cNmCondPagto'
      '      ,iQtdeParcelas '
      '  FROM CondPagto'
      
        '       INNER JOIN FormaPagto             ON FormaPagto.nCdFormaP' +
        'agto  = CondPagto.nCdFormaPagto'
      
        '       INNER JOIN TabTipoFormaPagto TTFP ON TTFP.nCdTabTipoForma' +
        'Pagto = FormaPagto.nCdTabTipoFormaPagto'
      'WHERE nCdCondPagto = :nPK'
      '  AND TTFP.nCdTabTipoFormaPagto IN (3,4)')
    Left = 192
    Top = 232
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCondPagtoiQtdeParcelas: TIntegerField
      FieldName = 'iQtdeParcelas'
    end
  end
  object qryOperCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT OC.nCdOperadoraCartao'
      '      ,OC.cNmOperadoraCartao'
      '      ,OC.iFloating'
      '      ,GOC.nCdGrupoOperadoraCartao'
      '      ,GOC.cNmGrupoOperadoraCartao'
      '  FROM OperadoraCartao OC'
      
        '       INNER JOIN GrupoOperadoraCartao GOC ON GOC.nCdGrupoOperad' +
        'oraCartao = OC.nCdGrupoOperadoraCartao'
      ' WHERE nCdOperadoraCartao = :nPK'
      '       AND nCdStatus = 1 ')
    Left = 224
    Top = 232
    object qryOperCartaonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryOperCartaocNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryOperCartaonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryOperCartaocNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
    object qryOperCartaoiFloating: TIntegerField
      FieldName = 'iFloating'
    end
  end
  object qryLojaCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja '
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE nCdUsuario = :nCdUsuario'
      '                  AND UL.nCdLoja = Loja.nCdLoja)')
    Left = 160
    Top = 232
    object qryLojaCartaonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojaCartaocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta '
      '  FROM ContaBancaria '
      ' WHERE nCdContaBancaria = :nPK'
      '   AND cFlgCofre        = 1'
      '   AND nCdLoja          = :nCdLoja')
    Left = 256
    Top = 232
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryStatusDocto
    Left = 288
    Top = 264
  end
  object DataSource2: TDataSource
    DataSet = qryCondPagto
    Left = 192
    Top = 264
  end
  object DataSource3: TDataSource
    DataSet = qryOperCartao
    Left = 224
    Top = 264
  end
  object DataSource4: TDataSource
    DataSet = qryContaBancaria
    Left = 256
    Top = 264
  end
  object DataSource5: TDataSource
    DataSet = qryLojaCartao
    Left = 160
    Top = 264
  end
  object SP_GERA_TITULO_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_TITULO_CARTAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransacaoCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 320
    Top = 232
  end
end
