unit rInadimplencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, ToolWin, DBCtrls, StdCtrls, Mask, ADODB, cxGridDBTableView, cxGrid, cxGraphics, cxLookAndFeels, cxCurrencyEdit, cxPC,
  cxLookAndFeelPainters, cxButtons, DB, FileCtrl;

type
  TrptInadimplencia = class(TForm)
    ToolBar1: TToolBar;
    ToolButtonImp: TToolButton;
    ToolButtonFechar: TToolButton;
    ImageList1: TImageList;
    Image1: TImage;
    ToolButton3: TToolButton;
    ProgressBar: TProgressBar;
    GroupBox2: TGroupBox;
    EditDir: TEdit;
    cxButtonPesq: TcxButton;
    MaskEditIni: TMaskEdit;
    GroupBox1: TGroupBox;
    RadioButtonA: TRadioButton;
    RadioButtonI: TRadioButton;
    MaskEditFim: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    qryExec: TADOQuery;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ToolButtonFecharClick(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure showForm(objForm: TForm; bDestroirObjeto : boolean);
    procedure desativaDBEdit (obj : TDBEdit) ;
    procedure ativaDBEdit (obj : TDBEdit) ;
    procedure desativaMaskEdit (obj : TMaskEdit);
    procedure ativaMaskEdit (obj : TMaskEdit);
    procedure cxButtonPesqClick(Sender: TObject);
    procedure ToolButtonImpClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptInadimplencia: TrptInadimplencia;

implementation

uses fMenu, fCadastro_Template, fMensagem, fLookup_Padrao;

{$R *.dfm}

function TrptInadimplencia.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;


procedure TrptInadimplencia.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
    {frmMenu.nShowMessage(cTexto);}
end;


procedure TrptInadimplencia.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
    {frmMenu.nMensagemErro(cTexto);}
end;

procedure TrptInadimplencia.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
    {frmMenu.nMensagemAlerta(cTexto);}
end;

procedure TrptInadimplencia.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TrptInadimplencia.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
i:integer ;  
begin

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;
end;

procedure TrptInadimplencia.FormCreate(Sender: TObject);
var
    i : Integer;
    i2 : integer ;
begin

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TcxPageControl) then
    begin

        (Components [I] as TcxPageControl).Font.Name  := 'Calibri' ;
        (Components [I] as TcxPageControl).Font.Size  := 9 ;
        (Components [I] as TcxPageControl).Font.Style := [] ;
        (Components [I] as TcxPageControl).LookAndFeel.NativeStyle := True ;

    end ;

    if (Components [I] is TGroupBox) then
    begin
       (Components [I] as TGroupBox).Font.Name    := 'Calibri' ;
       (Components [I] as TGroupBox).Font.Size    := 8 ;
       (Components [I] as TGroupBox).Font.Style   := [] ;
    end ;

    if (Components [I] is TCheckBox) then
    begin
       (Components [I] as TCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TCheckBox).Font.Size    := 8 ;
       (Components [I] as TCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDBCheckBox) then
    begin
       (Components [I] as TDBCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TDBCheckBox).Font.Size    := 8 ;
       (Components [I] as TDBCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TADOStoredProc) then
    begin
        (Components [I] as TADOStoredProc).CommandTimeOut := 600 ;
    end ;

    if (Components [I] is TDBEdit) then
    begin
      //(Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := true    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TDBEdit).Font.Size   := 9 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;
      (Components [I] as TDBEdit).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ReadOnly := True ;
          (Components [I] as TDBEdit).Color      := $00E9E4E4 ;
          (Components [I] as TDBEdit).Font.Color := clBlack ;
          (Components [I] as TDBEdit).TabStop  := False ;
          Update;
      end;

    end;

    if (Components [I] is TDBMemo) then
    begin
      (Components [I] as TDBMemo).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBMemo).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBMemo).Color       := clWhite ;
      (Components [I] as TDBMemo).BevelInner  := bvSpace ;
      (Components [I] as TDBMemo).BevelKind   := bkNone ;
      (Components [I] as TDBMemo).BevelOuter  := bvLowered ;
      (Components [I] as TDBMemo).Ctl3D       := True    ;
      (Components [I] as TDBMemo).BorderStyle := bsSingle ;
      (Components [I] as TDBMemo).Font.Name   := 'Calibri' ;
      (Components [I] as TDBMemo).Font.Size   := 9 ;
      (Components [I] as TDBMemo).Font.Style  := [] ;
      (Components [I] as TDBMemo).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if ((Components [I] as TDBMemo).Tag = 1) then
      begin
          (Components [I] as TDBMemo).ParentFont := False ;
          (Components [I] as TDBMemo).Enabled := False ;
          (Components [I] as TDBMemo).Color   := clSilver ;
          (Components [I] as TDBMemo).Font.Color := clYellow ;
          Update;
      end ;

      if ((Components [I] as TDBEdit).Tag = 0) then
      begin
          if (Copy((Components [I] as TDBEdit).DataField,1,3) = 'dDt') then
             (Components [I] as TDBEdit).Width := 76 ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      //(Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TEdit).Font.Size   := 9 ;
      (Components [I] as TEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TEdit).OnKeyPress  := frmMenu.NavegaEnter ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      //(Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TMaskEdit).Font.Size   := 9 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TMaskEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if Trim((Components[I] as TMaskEdit).EditMask) = '######;1;' then
          (Components[I] as TMaskEdit).EditMask := '##########;1; ' ;
          
      if (Components[I] as TMaskEdit).EditMask = '!99/99/9999;1;_' then
          (Components[I] as TMaskEdit).Width := 76 ;

      if ((Components [I] as TMaskEdit).Tag = 1) then
      begin
          (Components [I] as TMaskEdit).ReadOnly := True ;
          (Components [I] as TMaskEdit).Color      := $00E9E4E4 ;
          (Components [I] as TMaskEdit).Font.Color := clBlack ;
          (Components [I] as TMaskEdit).TabStop  := False ;
          Update;
      end;


    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clWhite ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Calibri' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    If (Components [I] is TDBCheckBox) then
      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
        (Components [I] as TDBCheckBox).OnKeyPress  := frmMenu.NavegaEnter ;

    If (Components [I] is TcxGridDBTableView) then
    begin
        (Components [I] as TcxGridDBTableView).OptionsView.GridLineColor := clAqua ;
        (Components [I] as TcxGridDBTableView).OptionsView.GridLines     := glVertical ;
        (Components [I] as TcxGridDBTableView).Styles.Header             := frmMenu.Header ;
    end ;

    If (Components [I] is TcxGrid) then
    begin
        (Components [I] as TcxGrid).LookAndFeel.Kind        := lfFlat ;
        (Components [I] as TcxGrid).LookAndFeel.NativeStyle := True ;
        (Components [I] as TcxGrid).Font.Name               := 'Calibri' ;
    end ;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;

      (Components [I] as TDBLookupComboBox).Height      := 12 ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;
    end;

    if (Components [I] is TcxCurrencyEdit) then
    begin
        (Components [I] as TcxCurrencyEdit).Style.Font.Name := 'Calibri' ;
        (Components [I] as TcxCurrencyEdit).Style.Font.Size := 9 ;
    end ;
        
  end ;

end;

procedure TrptInadimplencia.ToolButtonFecharClick(Sender: TObject);
begin
    Close ;
end;



procedure TrptInadimplencia.FormActivate(Sender: TObject);
begin
    ToolBar1.Enabled := True ;
    frmMenu.StatusBar1.Panels[5].Text := Self.ClassName ;
end;

procedure TrptInadimplencia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    frmMenu.StatusBar1.Panels[5].Text := '' ;
end;

procedure TrptInadimplencia.FormDeactivate(Sender: TObject);
begin
    ToolBar1.Enabled := False ;
end;

procedure TrptInadimplencia.showForm(objForm: TForm;
  bDestroirObjeto: boolean);
begin

    frmMenu.showForm(objForm , bDestroirObjeto);

end;

procedure TrptInadimplencia.ativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptInadimplencia.desativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptInadimplencia.ativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptInadimplencia.desativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptInadimplencia.cxButtonPesqClick(Sender: TObject);
var
  dir: string;
const
  SELDIRHELP = 1000;
begin
  dir := 'C:';
  if SelectDirectory(dir, [sdAllowCreate, sdPerformCreate, sdPrompt], SELDIRHELP) then
    EditDir.Text := dir;
end;

{ ##EXEMPLO DE LOOKUP##

procedure TrptModelo.MaskEditCategKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (MaskEditDepto.Text <> '') then
          nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdDepartamento = ' + MaskEditDepto.Text)
        else
          nPK := frmLookup_Padrao.ExecutaConsulta(46);

        If (nPK > 0) then
            MaskEditCateg.Text := IntToStr(nPK) ;

    end ;

  end ;

end; /}

procedure TrptInadimplencia.ToolButtonImpClick(Sender: TObject);
var
  ini, fim, qry, path, arq, linha: String;
  g: integer;
  F: TextFile;
begin
   ProgressBar.Max := 100;
   ProgressBar.Position := 20;
   try
    qryExec.Close;
    qryExec.SQL.Clear;
    if MaskEditIni.Text = '  /  /    ' then ini := DateToStr(Date-30) else ini := MaskEditIni.Text;
    if MaskEditFim.Text = '  /  /    ' then fim := DateToStr(Date-1) else fim := MaskEditFim.Text;
    if RadioButtonI.Checked = True then g := 0 else g := 1;
    qry := 'EXEC SPREL_INADIMPLENTES @dtini = ' + chr(39) + ini + chr(39) + ',@dtfim = ' + chr(39) + fim + chr(39) +  ',@cluster = ' + IntToStr(g);
    qryExec.SQL.Add(qry);
    qryExec.Open;
    ProgressBar.Max := qryExec.RecordCount;
    ProgressBar.Position := Round(qryExec.RecordCount * 0.2);
    path := EditDir.Text;
    arq := path + '\Rel. de Inadimpl�ncia D' + FormatDateTime('yyyy-mm-dd', Date) + 'T' + FormatDateTime('hh-nn-ss',Time) + '.csv';
    AssignFile(F, arq);
    Rewrite(F);
    Writeln(F, 'CD_CLIENTE;NM_CLIENTE;DT_NASC;TP_GENERO;NM_CIDADE;DD_ATRASO;CD_LOJA;CL_PROF;DT_ADMISSAO;VL_RENDA;VL_TOTAL;QT_QUITADO;QT_ABERTO;VL_QUITADO;PR_COMPRA;UL_CONSULTA;TP_RETORNO;');
    qryExec.First;
    while not qryExec.Eof do
    begin
      linha :=  qryExec.FieldByName('CD_CLIENTE').Text +  ';' +
                qryExec.FieldByName('NM_CLIENTE').Text +  ';' +
                qryExec.FieldByName('DT_NASC').Text +  ';' +
                qryExec.FieldByName('TP_GENERO').Text +  ';' +
                qryExec.FieldByName('NM_CIDADE').Text +  ';' +
                qryExec.FieldByName('DD_ATRASO').Text +  ';' +
                qryExec.FieldByName('CD_LOJA').Text +  ';' +
                qryExec.FieldByName('CL_PROF').Text +  ';' +
                qryExec.FieldByName('DT_ADMISSAO').Text +  ';' +
                qryExec.FieldByName('VL_RENDA').Text +  ';' +
                qryExec.FieldByName('VL_TOTAL').Text +  ';' +
                qryExec.FieldByName('QT_QUITADO').Text +  ';' +
                qryExec.FieldByName('QT_ABERTO').Text +  ';' +
                qryExec.FieldByName('VL_QUITADO').Text +  ';' +
                qryExec.FieldByName('PR_COMPRA').Text +  ';' +
                qryExec.FieldByName('DT_CONSULTA').Text +  ';' +
                qryExec.FieldByName('RETORNO').Text +  ';';
      Writeln(F, linha);
      qryExec.Next;
      ProgressBar.Position :=  ProgressBar.Position + 1;
    end;
    CloseFile(F);
    ShowMessage('O relat�rio foi criado com sucesso no diret�rio escolhido!');
    ProgressBar.Position := 0;
   except
    ShowMessage('Algo deu errado :( Tente novamente, se o erro persistir contate o desenvolvedor.');
    ProgressBar.Position := 0;
   end;
end;

initialization
  RegisterClass(TrptInadimplencia);

end.
