inherited frmVinculacaoAgrupamentoContabil: TfrmVinculacaoAgrupamentoContabil
  Caption = 'Vincula'#231#227'o Agrupamento Cont'#225'bil'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 32
    Width = 745
    Height = 409
    DataGrouping.GroupLevels = <>
    DataSource = dsAgrupamento
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdAgrupamentoContabil'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmAgrupamentoContabil'
        Footers = <>
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'cMascara'
        Footers = <>
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'iMaxNiveis'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cFlgAtivo'
        Footers = <>
        Width = 65
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryAgrupamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT AgrupamentoContabil.nCdAgrupamentoContabil'
      '      ,AgrupamentoContabil.cNmAgrupamentoContabil'
      '      ,AgrupamentoContabil.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,AgrupamentoContabil.cMascara'
      '      ,AgrupamentoContabil.iMaxNiveis'
      '      ,CASE WHEN AgrupamentoContabil.nCdStatus = 1 THEN '#39'Sim'#39
      '            ELSE '#39'N'#227'o'#39
      '       END as cFlgAtivo'
      '  FROM AgrupamentoContabil'
      
        '       INNER JOIN Empresa             ON Empresa.nCdEmpresa     ' +
        '                    = AgrupamentoContabil.nCdEmpresa'
      ' ORDER BY 2')
    Left = 400
    Top = 216
    object qryAgrupamentonCdAgrupamentoContabil: TIntegerField
      DisplayLabel = 'Agrupamento Cont'#225'bil|C'#243'd.'
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryAgrupamentocNmAgrupamentoContabil: TStringField
      DisplayLabel = 'Agrupamento Cont'#225'bil|Descri'#231#227'o'
      FieldName = 'cNmAgrupamentoContabil'
      Size = 50
    end
    object qryAgrupamentonCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa Cont'#225'bil|C'#243'd.'
      FieldName = 'nCdEmpresa'
    end
    object qryAgrupamentocNmEmpresa: TStringField
      DisplayLabel = 'Empresa Cont'#225'bil|Nome'
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryAgrupamentocMascara: TStringField
      DisplayLabel = 'Estrutura|M'#225'scara'
      FieldName = 'cMascara'
      Size = 30
    end
    object qryAgrupamentoiMaxNiveis: TIntegerField
      DisplayLabel = 'Estrutura|N'#237'vel M'#225'ximo'
      FieldName = 'iMaxNiveis'
    end
    object qryAgrupamentocFlgAtivo: TStringField
      DisplayLabel = 'Estrutura|Ativo'
      FieldName = 'cFlgAtivo'
      ReadOnly = True
      Size = 3
    end
  end
  object dsAgrupamento: TDataSource
    DataSet = qryAgrupamento
    Left = 440
    Top = 216
  end
end
