inherited frmProvisaoPagto_DetalheProvisao: TfrmProvisaoPagto_DetalheProvisao
  Left = 89
  Top = 173
  Width = 1079
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1063
  end
  inherited ToolBar1: TToolBar
    Width = 1063
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1063
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulos
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footers = <>
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'nCdSP'
        Footers = <>
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdTitulo, cNrTit, nCdSP,dDtVenc, cNmTerceiro, nSaldoTit,' +
        ' cNrNF, cNmEspTit, nCdProvisaoTit'
      '   FROM Titulo'
      'INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdTerceiro'
      'INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      'WHERE nCdProvisaoTit = :nPK')
    Left = 216
    Top = 104
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Provisionados|ID'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosnCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. SP'
      FieldName = 'nCdSP'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Provisionados|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTituloscNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos Provisionados|Saldo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloscNrNF: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|Esp'#233'cie de T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosnCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 248
    Top = 104
  end
  object PopupMenu1: TPopupMenu
    Left = 416
    Top = 152
    object ExcluirItemProviso1: TMenuItem
      Caption = 'Excluir Item Provis'#227'o'
      OnClick = ExcluirItemProviso1Click
    end
  end
  object qryConfereBaixaProvisao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgBaixado'
      '  FROM ProvisaoTit'
      ' WHERE nCdProvisaoTit = :nPK')
    Left = 528
    Top = 207
    object qryConfereBaixaProvisaocFlgBaixado: TIntegerField
      FieldName = 'cFlgBaixado'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 344
    Top = 370
  end
end
