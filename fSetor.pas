unit fSetor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh, cxPC, cxControls;

type
  TfrmSetor = class(TfrmCadastro_Padrao)
    qryMasternCdSetor: TIntegerField;
    qryMastercNmSetor: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryUsuarioAutorSetor: TADOQuery;
    dsUsuarioAutorSetor: TDataSource;
    qryUsuarioAutorSetornCdSetor: TIntegerField;
    qryUsuarioAutorSetornCdUsuario: TIntegerField;
    qryUsuarioAutorSetorcNmUsuario: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryMasternCdCC: TIntegerField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcNmCC: TStringField;
    DBGridEh1: TDBGridEh;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryUsuarioSetor: TADOQuery;
    dsUsuarioSetor: TDataSource;
    qryUsuarioSetornCdSetor: TIntegerField;
    qryUsuarioSetornCdUsuario: TIntegerField;
    qryUsuarioSetorcNmUsuario: TStringField;
    dsCC: TDataSource;
    qryUsuarioSetornCdUsuarioSetor: TIntegerField;
    qryAux: TADOQuery;
    qryUsuarioAutorSetornCdUsuarioAutorSetor: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryUsuarioAutorSetorCalcFields(DataSet: TDataSet);
    procedure qryUsuarioAutorSetorBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryUsuarioSetorCalcFields(DataSet: TDataSet);
    procedure qryUsuarioSetorBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSetor: TfrmSetor;

implementation

uses fLookup_Padrao,fMenu;

{$R *.dfm}



procedure TfrmSetor.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'SETOR' ;
  nCdTabelaSistema  := 56 ;
  nCdConsultaPadrao := 122 ;
end;

procedure TfrmSetor.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmSetor.qryUsuarioAutorSetorCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, qryUsuarioAutorSetornCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioAutorSetorcNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmSetor.qryUsuarioAutorSetorBeforePost(DataSet: TDataSet);
begin
  if (qryUsuarioAutorSetorcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio Invalido. ');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM UsuarioAutorSetor WHERE nCdUsuario = ' + IntToStr(qryUsuarioAutorSetornCdUsuario.Value) + ' AND nCdSetor = ' + IntToStr(qryMasternCdSetor.Value)) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuariocNmUsuario.Value + ' j� vinculado a este setor.');
      Abort;
  end;

  inherited;

  qryUsuarioAutorSetornCdSetor.Value             := qryMasternCdSetor.Value ;
  qryUsuarioAutorSetornCdUsuarioAutorSetor.Value := frmMenu.fnProximoCodigo('USUARIOAUTORSETOR');
end;

procedure TfrmSetor.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuarioAutorSetor, qryMasternCdSetor.AsString) ;
  PosicionaQuery(qryUsuarioSetor, qryMasternCdSetor.AsString) ;
end;

procedure TfrmSetor.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioAutorSetor.Close ;
  PosicionaQuery(qryUsuarioAutorSetor, qryMasternCdSetor.AsString) ;

  qryCC.Close ;
  PosicionaQuery(qryCC, qryMasternCdCC.asString) ;

  qryUsuarioSetor.Close;
  PosicionaQuery(qryUsuarioSetor,qryMasternCdSetor.AsString);
  
end;

procedure TfrmSetor.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioAutorSetor.Close ;
  qryCC.CLose ;
  
end;

procedure TfrmSetor.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioAutorSetor.State = dsBrowse) then
             qryUsuarioAutorSetor.Edit ;
        
        if (qryUsuarioAutorSetor.State = dsInsert) or (qryUsuarioAutorSetor.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioAutorSetornCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSetor.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmSetor.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
                PosicionaQuery(qryCC, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSetor.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DbEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do setor.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (DbEdit3.Text = '') or (DbEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione um centro de custo para o setor.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

end;

procedure TfrmSetor.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit3.Text) ;
  
end;

procedure TfrmSetor.qryUsuarioSetorCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, qryUsuarioSetornCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioSetorcNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmSetor.qryUsuarioSetorBeforePost(DataSet: TDataSet);
begin

  if (qryUsuarioSetorcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio Invalido.');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM UsuarioSetor WHERE nCdUsuario = ' + IntToStr(qryUsuarioSetornCdUsuario.Value) + ' AND nCdSetor = ' + IntToStr(qryMasternCdSetor.Value)) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuariocNmUsuario.Value + ' j� vinculado a este setor.');
      Abort;
  end;

  inherited;

  qryUsuarioSetornCdSetor.Value        := qryMasternCdSetor.Value ;
  qryUsuarioSetornCdUsuarioSetor.Value := frmMenu.fnProximoCodigo('USUARIOSETOR');
end;

procedure TfrmSetor.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioSetor.State = dsBrowse) then
             qryUsuarioSetor.Edit ;

        if (qryUsuarioSetor.State = dsInsert) or (qryUsuarioSetor.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioSetornCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSetor.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

initialization
    RegisterClass(TfrmSetor) ;

end.
