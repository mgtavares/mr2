unit fDoctoFiscal_PendenteTransmissao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, pcnConversao, cxPC;

type
  TfrmDoctoFiscal_PendenteTransmissao = class(TfrmProcesso_Padrao)
    qryDoctoFiscal: TADOQuery;
    dsDoctoFiscal: TDataSource;
    qryDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalcNmCondPagto: TStringField;
    qryDoctoFiscalcSigla: TStringField;
    qryDoctoFiscalnCdTerceiroTransp: TIntegerField;
    qryDoctoFiscalcNrProtocoloDPEC: TStringField;
    qryDoctoFiscalcCaminhoXML: TStringField;
    qryDoctoFiscalcArquivoXML: TStringField;
    qryDoctoFiscalcNrProtocoloNFe: TStringField;
    qryDoctoFiscalcFlgDPECTransmitida: TIntegerField;
    qryDoctoFiscalcNrReciboNFe: TStringField;
    cxPageControl1: TcxPageControl;
    tabDPEC: TcxTabSheet;
    tabCancelado: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdDoctoFiscal: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    cxGrid1DBTableView1cCFOP: TcxGridDBColumn;
    cxGrid1DBTableView1cTextoCFOP: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgEntSai: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cSigla: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiroTransp: TcxGridDBColumn;
    cxGrid1DBTableView1cNrProtocoloDPEC: TcxGridDBColumn;
    cxGrid1DBTableView1cCaminhoXML: TcxGridDBColumn;
    cxGrid1DBTableView1cArquivoXML: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    qryDoctoCancelado: TADOQuery;
    qryDoctoCanceladonCdDoctoFiscal: TIntegerField;
    qryDoctoCanceladodDtEmissao: TDateTimeField;
    qryDoctoCanceladocCFOP: TStringField;
    qryDoctoCanceladocTextoCFOP: TStringField;
    qryDoctoCanceladocFlgEntSai: TStringField;
    qryDoctoCanceladonCdTerceiro: TIntegerField;
    qryDoctoCanceladocNmTerceiro: TStringField;
    qryDoctoCanceladonValTotal: TBCDField;
    qryDoctoCanceladocNrProtocoloNFe: TStringField;
    qryDoctoCanceladocNrReciboNFe: TStringField;
    qryDoctoCanceladocCaminhoXML: TStringField;
    qryDoctoCanceladocArquivoXML: TStringField;
    dsDoctoCancelado: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn_nCdDoctoFiscal: TcxGridDBColumn;
    cxGridDBColumn_dDtEmissao: TcxGridDBColumn;
    cxGridDBColumn_cNmTerceiro: TcxGridDBColumn;
    cxGridDBColumn_nValTotal: TcxGridDBColumn;
    cxGridDBColumn_cCFOP: TcxGridDBColumn;
    cxGridDBColumn_cTextoCFOP: TcxGridDBColumn;
    cxGridDBColumn_cFlgEntSai: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1DBColumn_cNrProtocoloNFe: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNrReciboNFe: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDoctoFiscal_PendenteTransmissao: TfrmDoctoFiscal_PendenteTransmissao;

implementation

uses fMenu, fEmiteNFE2;

{$R *.dfm}

procedure TfrmDoctoFiscal_PendenteTransmissao.FormShow(Sender: TObject);
begin
  inherited;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryDoctoFiscal.Open;

  qryDoctoCancelado.Close;
  qryDoctoCancelado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryDoctoCancelado.Open;
end;

procedure TfrmDoctoFiscal_PendenteTransmissao.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objDoctoFiscal  : TFrmEmiteNFE2;
  cNmModoOperacao : String;
  cNmLote         : String;
begin
  inherited;

  objDoctoFiscal := TfrmEmiteNFe2.Create(nil);

  try
      try
          objDoctoFiscal.preparaNFe;

          objDoctoFiscal.qryStatusDPEC.Close;
          objDoctoFiscal.qryStatusDPEC.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva;
          objDoctoFiscal.qryStatusDPEC.Open;

          if (objDoctoFiscal.qryStatusDPECnCdStatusDPEC.Value = 1) then
              objDoctoFiscal.ACBrNFe1.Configuracoes.Geral.FormaEmissao := teDPEC
          else begin
              if (objDoctoFiscal.StatusWebService(1).nCdStatusSEFAZ <> 107) then
                  objDoctoFiscal.ACBrNFe1.Configuracoes.Geral.FormaEmissao := teSCAN;
          end;

          case objDoctoFiscal.ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo of
              3 :
                  cNmModoOperacao := 'SCAN';
              4 :
                  cNmModoOperacao := 'DPEC';
              5 :
                  cNmModoOperacao := 'FS-DA';
          end;

          if (objDoctoFiscal.ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo <> 1) then
          begin
              MensagemAlerta('N�o � poss�vel transmitir um documento Fiscal Pendente enquanto estiver em Modo de Conting�ncia'
                            + #13#13 + 'Modo de Opera��o atual: ' + cNmModoOperacao);
              exit;
          end;

          objDoctoFiscal.ACBrNFe1.NotasFiscais.Clear;
          objDoctoFiscal.ACBrNFe1.NotasFiscais.LoadFromFile(frmMenu.cPathSistema + qryDoctoFiscalcCaminhoXML.Value + qryDoctoFiscalcArquivoXML.Value);
          objDoctoFiscal.ACBrNFe1.DANFE.ProtocoloNFe := qryDoctoFiscalcNrProtocoloDPEC.Value;

          cNmLote := qryDoctoFiscalcNrProtocoloDPEC.Value;

          cNmLote := Copy(cNmLote,1,15);

          objDoctoFiscal.ACBrNFe1.Enviar(cNmLote,False);

          qryDoctoFiscal.Edit;
          qryDoctoFiscalcNrProtocoloNFe.Value     := objDoctoFiscal.ACBrNFe1.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
          qryDoctoFiscalcNrReciboNFe.Value        := objDoctoFiscal.ACBrNFe1.WebServices.Retorno.NFeRetorno.nRec;
          qryDoctoFiscalcFlgDPECTransmitida.Value := 1;
          qryDoctoFiscal.Post;

      except
          MensagemErro('Erro ao processar informa��es. Classe: ' + objDoctoFiscal.ClassName);
          raise;
      end;
  finally
      FreeAndNil(objDoctoFiscal);
  end;

  ShowMessage('NFe Transmitida com sucesso para a SEFAZ Virtual.');

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Open;

end;

procedure TfrmDoctoFiscal_PendenteTransmissao.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Open;

  qryDoctoCancelado.Close;
  qryDoctoCancelado.Open;
end;

procedure TfrmDoctoFiscal_PendenteTransmissao.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objEmiteNFe2 : TfrmEmiteNFe2;
begin
  inherited;

  case MessageDlg('Confirma a transmiss�o deste documento fiscal ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      objEmiteNFe2 := TfrmEmiteNFe2.Create(Nil);
      
      try
          objEmiteNFe2.cancelarNFe(qryDoctoCanceladonCdDoctoFiscal.Value);
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objEmiteNFe2);
      qryDoctoCancelado.Close;
      qryDoctoCancelado.Open;
  end;
end;

initialization
    RegisterClass(TfrmDoctoFiscal_PendenteTransmissao);

end.
