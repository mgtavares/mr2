inherited frmGerenciaProdutoWeb_AplicaPrecoProduto: TfrmGerenciaProdutoWeb_AplicaPrecoProduto
  Left = 559
  Top = 275
  VertScrollBar.Range = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Aplica Pre'#231'o Produto Web'
  ClientHeight = 154
  ClientWidth = 290
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 290
    Height = 125
  end
  object Label2: TLabel [1]
    Left = 19
    Top = 107
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pre'#231'o de Venda Por'
  end
  object Label1: TLabel [2]
    Left = 22
    Top = 64
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pre'#231'o de Venda De'
  end
  inherited ToolBar1: TToolBar
    Width = 290
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object dxCurValVendaPor: TdxCurrencyEdit [4]
    Left = 118
    Top = 99
    Width = 147
    TabOrder = 2
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  object dxCurValVendaDe: TdxCurrencyEdit [5]
    Left = 118
    Top = 56
    Width = 147
    TabOrder = 1
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  inherited ImageList1: TImageList
    Left = 192
    Top = 40
  end
  object qryAplicaPreco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoProd'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nValVendaDe'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nValVendaPor'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTabTipoProduto int'
      '       ,@nValVendaDe       decimal(12,2)'
      '       ,@nValVendaPor      decimal(12,2)'
      ''
      'SET @nCdTabTipoProduto = :nCdTipoProd'
      'SET @nValVendaDe       = :nValVendaDe '
      'SET @nValVendaPor      = :nValVendaPor'
      ''
      '--'
      '-- atualiza pre'#231'o produto web'
      '--'
      'IF (@nCdTabTipoProduto = 2)'
      'BEGIN'
      ''
      '    UPDATE #TempProdWeb'
      '       SET nValVendaWebDe  = @nValVendaDe'
      '          ,nValVendaWebPor = @nValVendaPor'
      '     WHERE cFlgAtivo       = 1'
      ''
      'END'
      ''
      '--'
      '-- atualiza pre'#231'o sku'
      '--'
      'IF (@nCdTabTipoProduto = 3)'
      'BEGIN'
      ''
      '    UPDATE #TempSkuWeb'
      '       SET nValVendaWebDe  = @nValVendaDe'
      '          ,nValVendaWebPor = @nValVendaPor'
      '     WHERE cFlgAtivo       = 1'
      ''
      'END')
    Left = 224
    Top = 40
  end
end
