unit rLiberacaoDiverReceb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, ER2Lookup, ER2Excel;

type
  TrptLiberacaoDiverReceb = class(TfrmRelatorio_Padrao)
    dsEmpresa: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    Loja: TLabel;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    Label1: TLabel;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceirocNmTerceiro: TStringField;
    dsTipoReceb: TDataSource;
    qryTipoReceb: TADOQuery;
    qryTipoRecebcNmTipoReceb: TStringField;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    qryTabTipoDiverg: TADOQuery;
    dsTabTipoDiverg: TDataSource;
    Label3: TLabel;
    qryUsuario: TADOQuery;
    dsUsuario: TDataSource;
    qryUsuariocNmUsuario: TStringField;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ER2LookupLoja: TER2LookupMaskEdit;
    ER2LookupTerceiro: TER2LookupMaskEdit;
    ER2LookupTipoReceb: TER2LookupMaskEdit;
    ER2LookupTipoDiverg: TER2LookupMaskEdit;
    ER2LookupUsuarioAutori: TER2LookupMaskEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    qryTabTipoDivergcNmTabTipoDiverg: TStringField;
    DBEdit5: TDBEdit;
    edtDtInicialAutorizacao: TMaskEdit;
    edtDtFinalAutorizacao: TMaskEdit;
    edtDtInicialReceb: TMaskEdit;
    edtDtFinalReceb: TMaskEdit;
    rgModeloImp: TRadioGroup;
    ER2Excel: TER2Excel;
    rgRecebDiv: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptLiberacaoDiverReceb: TrptLiberacaoDiverReceb;

implementation

{$R *.dfm}
uses
  fMenu,rLiberacaoDiverReceb_view;
procedure TrptLiberacaoDiverReceb.ToolButton1Click(Sender: TObject);
var
  objRel            : TrptLiberacaoDiverReceb_view;
  ilinha            : integer;
  cFiltros          : String;
  nCdRecebimentoAux : integer;
begin
  inherited;

  if edtDtInicialAutorizacao.Text > edtDtFinalAutorizacao.Text  then
  begin
      MensagemAlerta('Data de autoriza��o inicial deve ser menor do que data final.');
      Abort;
  end;

  if edtDtInicialReceb.Text > edtDtFinalReceb.Text  then
  begin
      MensagemAlerta('Data do recebimento inicial deve ser menor do que data final.');
      Abort;
  end;

  {-- prepara e executa procedure --}
  objRel := TrptLiberacaoDiverReceb_view.Create(nil);

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  {-- Inclui os filtros que foram preenchidos em tela. --}
  if DBEdit2.Text <> '' then
  begin
      cFiltros := 'Loja: ' + DBEdit2.Text + ' / ';
  end;

  if DBEdit3.Text <> '' then
  begin
      cFiltros := cFiltros + ' Terceiro: ' + DBEdit3.Text  + ' / ';
  end;

  if DBEdit4.Text <> '' then
  begin
      cFiltros := cFiltros + ' Tipo Receb.: ' + DBEdit4.Text + ' / ';
  end;

  if DBEdit5.Text <> '' then
  begin
      cFiltros := cFiltros + ' Tipo Diverg: '    + DBEdit5.Text + ' / ';
  end;

  if DBEdit6.Text <> '' then
  begin
      cFiltros := cFiltros + ' Usuario: ' + DBEdit6.Text + ' / ';
  end;

  if (edtDtInicialAutorizacao.Text <> '  /  /    ') and (edtDtFinalAutorizacao.Text <> '  /  /    ') then
  begin
      cFiltros := cFiltros + ' Data. Auto.: ' + edtDtInicialAutorizacao.Text + ' at� ' + edtDtFinalAutorizacao.Text
  end;

  if (edtDtInicialReceb.Text <> '  /  /    ') and (edtDtFinalReceb.Text <> '  /  /    ') then
  begin
      cFiltros := cFiltros + ' Data. Receb.: ' + edtDtInicialReceb.Text + ' at� ' + edtDtFinalReceb.Text
  end;

  objRel.lblfiltros.Caption := cFiltros;

  try
      try
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Close;
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva;
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdLoja').Value         := frmMenu.ConvInteiro(ER2LookupLoja.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdTerceiro').Value     := frmMenu.ConvInteiro(ER2LookupTerceiro.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdTipoReceb').Value    := frmMenu.ConvInteiro(ER2LookupTipoReceb.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdTipoDiverg').Value   := frmMenu.ConvInteiro(ER2LookupTipoDiverg.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.ConvInteiro(ER2LookupUsuarioAutori.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@cFlgRecebDiv').Value    := rgRecebDiv.ItemIndex;
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@dDtAutInicial').Value   := frmMenu.ConvData(edtDtInicialAutorizacao.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@dDtAutFinal').Value     := frmMenu.ConvData(edtDtFinalAutorizacao.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@dDtRecebInicial').Value := frmMenu.ConvData(edtDtInicialReceb.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Parameters.ParamByName('@dDtRecebFinal').Value   := frmMenu.ConvData(edtDtFinalReceb.Text);
          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Open;

          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.First;

          { -- exibe relat�rio -- }
          case (rgModeloImp.ItemIndex) of
              0 : objRel.QuickRep1.PreviewModal;
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      ER2Excel.Celula['A1'].Mesclar('N1');
                      ER2Excel.Celula['A2'].Mesclar('N2');
                      ER2Excel.Celula['A3'].Mesclar('N3');
                      ER2Excel.Celula['A4'].Mesclar('N4');

                      ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A1'].Range('N1');
                      ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A2'].Range('N2');
                      ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A3'].Range('N3');
                      ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A4'].Range('N4');

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel.Celula['A2'].Text := 'Rel. Libera��o de Diverg�ncia no Recebimento.';
                      ER2Excel.Celula['A3'].Text := 'Empresa: ' + frmMenu.cNmEmpresaAtiva + '.';
                      ER2Excel.Celula['A4'].Text := 'Filtros: ' + cFiltros + '.';

                      ER2Excel.Celula['A1'].Congelar('A5');

                      iLinha := 4;

                      objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.First;

                      while (not objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Eof) do
                      begin
                          {-- Atribui ncdrecebimento para variavel auxiliar e depois
                          verifica se � diferente para imprimir o cabe�alho do recebimento --}

                          if (objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdRecebimento.Value <> nCdRecebimentoAux) then
                          begin
                              inc(ilinha);
                              inc(ilinha);
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text :=  'C�digo Rec.: ' + inttostr(objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdRecebimento.Value)	+	' Usu�rio Fech.: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmUsuarioFech.Value;
                              inc(ilinha);
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text :=  'Loja: '	+ objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdLoja.Value + ' Status: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTabStatusReceb.Value ;
                              inc(ilinha);
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text :=  'Terceiro: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTerceiro.Value;
                              inc(ilinha);
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text :=  'Tipo Rec.: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTipoReceb.Value + 'Dt. Rec.: ' + DateToStr(objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtReceb.Value);
                              inc(ilinha);
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text :=  'N�mero NF: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNrDocto.Value + ' S�rie: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcSerieDocto.Value + ' Mod. NF: ' + objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcModeloNF.Value + ' Dt. Emiss�o: ' + DateToStr(objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtDocto.Value);
                              inc(ilinha);

                              ER2Excel.Celula['A' + IntToStr(iLinha)].Text := 'C�d.';
                              ER2Excel.Celula['B' + IntToStr(iLinha)].Text := 'Descri��o.';
                              ER2Excel.Celula['G' + IntToStr(iLinha)].Text := 'Tipo Diverg.';
                              ER2Excel.Celula['I' + IntToStr(iLinha)].Text := 'Usu�rio Lib.';
                              ER2Excel.Celula['J' + IntToStr(iLinha)].Text := 'Dt. Autoriza��o.';
                              ER2Excel.Celula['L' + IntToStr(iLinha)].Text := 'Autoriza��o Auto.';
                              ER2Excel.Celula['N' + IntToStr(iLinha)].Text := 'Observa��o';
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Border := [EdgeBottom];
                              ER2Excel.Celula['A' + IntToStr(iLinha)].Range('N' + IntToStr(iLinha));
                          end;

                          ER2Excel.Celula['A' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdProduto.Value;
                          ER2Excel.Celula['B' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmItem.Value;
                          ER2Excel.Celula['G' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmTabTipoDiverg.Value;
                          ER2Excel.Celula['I' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcNmUsuario.Value;
                          ER2Excel.Celula['J' + IntToStr(iLinha + 1)].Text := frmMenu.ConvData(DateToStr(objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOdDtAutorizacao.Value));
                          ER2Excel.Celula['L' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcAutorizaAut.Value;
                          ER2Excel.Celula['N' + IntToStr(iLinha + 1)].Text := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOcOBS.Value;

                          nCdRecebimentoAux := objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTOnCdRecebimento.Value;

                          objRel.SPREL_LIBERACAO_DIVERGENCIA_RECEBIMENTO.Next;

                          Inc(iLinha);
                      end;

                      { -- exporta planilha e limpa result do componente -- }
                      ER2Excel.ExportXLS;
                      ER2Excel.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;
      except
          MensagemErro('Erro na cria��o do relat�rio. Classe:  ' + objRel.ClassName);
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptLiberacaoDiverReceb.FormCreate(Sender: TObject);
begin
  inherited;
  
  if frmMenu.LeParametro('VAREJO') = 'N' then
  begin
      ER2LookupLoja.Enabled := False;
      ER2LookupLoja.Color   := $00E9E4E4;
  end;
end;

initialization
    RegisterClass(TrptLiberacaoDiverReceb);
end.
