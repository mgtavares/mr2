unit fProvisaoPagto_SaldoProjetado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh;

type
  TfrmProvisaoPagtoSaldoProjetado = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    uspConsulta: TADOStoredProc;
    uspConsultadDiaSaldo: TDateTimeField;
    uspConsultanValProvisaoPagto: TBCDField;
    uspConsultanValReceitaPrevista: TBCDField;
    uspConsultanSaldoDia: TBCDField;
  private        
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProvisaoPagtoSaldoProjetado: TfrmProvisaoPagtoSaldoProjetado;

implementation

{$R *.dfm}

end.
