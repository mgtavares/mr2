inherited frmConsultaLoteSerial: TfrmConsultaLoteSerial
  Left = 272
  Top = 109
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Consulta Lote/Serial'
  ClientHeight = 604
  ClientWidth = 748
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 121
    Width = 748
    Height = 247
  end
  inherited ToolBar1: TToolBar
    Width = 748
    ButtonWidth = 103
    inherited ToolButton1: TToolButton
      Caption = '&Gravar Sele'#231#227'o'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 103
    end
    inherited ToolButton2: TToolButton
      Left = 111
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 748
    Height = 92
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 40
      Top = 20
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transa'#231#227'o'
    end
    object Label2: TLabel
      Tag = 1
      Left = 52
      Top = 44
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Tag = 1
      Left = 9
      Top = 68
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Local de Estoque'
      FocusControl = DBEdit3
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 96
      Top = 12
      Width = 293
      Height = 21
      DataField = 'cNmTabTipoOrigemMovLote'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 96
      Top = 36
      Width = 633
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource2
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 96
      Top = 60
      Width = 633
      Height = 21
      DataField = 'cNmLocalEstoque'
      DataSource = DataSource3
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 121
    Width = 748
    Height = 247
    Align = alClient
    Caption = 'Lotes/Seriais Dispon'#237'veis'
    ParentBackground = False
    TabOrder = 2
    object Label4: TLabel
      Tag = 1
      Left = 44
      Top = 27
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pesquisar'
      FocusControl = DBEdit3
    end
    object Edit1: TEdit
      Left = 96
      Top = 19
      Width = 393
      Height = 21
      TabOrder = 0
      OnChange = Edit1Change
    end
    object DBGridEh2: TDBGridEh
      Left = 2
      Top = 48
      Width = 743
      Height = 197
      AllowedOperations = [alopUpdateEh]
      DataGrouping.GroupLevels = <>
      DataSource = DataSource4
      Flat = False
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      FooterRowCount = 1
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      OnDblClick = DBGridEh2DblClick
      Columns = <
        item
          EditButtons = <>
          FieldName = 'cNrLote'
          Footers = <>
          ReadOnly = True
          Title.Caption = 'N'#250'mero Lote/ Serial'
        end
        item
          EditButtons = <>
          FieldName = 'dDtFabricacao'
          Footers = <>
          ReadOnly = True
          Title.Caption = 'Data Fabrica'#231#227'o'
        end
        item
          EditButtons = <>
          FieldName = 'dDtDisponibilidade'
          Footers = <>
          ReadOnly = True
          Title.Caption = 'Data Disponibilidade'
          Width = 129
        end
        item
          EditButtons = <>
          FieldName = 'dDtValidade'
          Footers = <>
          ReadOnly = True
          Title.Caption = 'Data Validade'
        end
        item
          EditButtons = <>
          FieldName = 'nSaldoLote'
          Footers = <
            item
              ValueType = fvtSum
            end>
          ReadOnly = True
          Title.Caption = 'Quantidade|Saldo Lote'
          Width = 93
        end
        item
          EditButtons = <>
          FieldName = 'nQtdeSelecionado'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clWindow
          Footer.Font.Height = -11
          Footer.Font.Name = 'Tahoma'
          Footer.Font.Style = []
          Footers = <>
          Title.Caption = 'Quantidade|Selecionada'
          Width = 86
          OnUpdateData = DBGridEh2Columns5UpdateData
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 368
    Width = 748
    Height = 236
    Align = alBottom
    Caption = 'Lotes/Seriais Selecionados'
    TabOrder = 3
    object DBGridEh1: TDBGridEh
      Left = 2
      Top = 15
      Width = 744
      Height = 219
      Align = alClient
      AllowedOperations = []
      DataGrouping.GroupLevels = <>
      DataSource = DataSource5
      Flat = False
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      FooterRowCount = 1
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      OnDblClick = DBGridEh1DblClick
      Columns = <
        item
          EditButtons = <>
          FieldName = 'cNrLote'
          Footers = <>
          Title.Caption = 'N'#250'mero Lote/ Serial'
          Width = 185
        end
        item
          EditButtons = <>
          FieldName = 'dDtFabricacao'
          Footers = <>
          Title.Caption = 'Data Fabrica'#231#227'o'
        end
        item
          EditButtons = <>
          FieldName = 'dDtDisponibilidade'
          Footers = <>
          Title.Caption = 'Data Disponibilidade'
          Width = 130
        end
        item
          EditButtons = <>
          FieldName = 'dDtValidade'
          Footers = <>
          Title.Caption = 'Data Validade'
          Width = 110
        end
        item
          EditButtons = <>
          FieldName = 'nQtdeSelecionado'
          Footers = <
            item
              ValueType = fvtSum
            end>
          Title.Caption = 'Quantidade|Selecionada'
          Width = 94
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 584
    Top = 32
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      
        '      ,Convert(VARCHAR(10),nCdProduto) + '#39' - '#39' + cNmProduto as c' +
        'NmProduto'
      '      ,nCdTabTipoModoGestaoProduto'
      '      ,cFlgControlaValidadeLote'
      '      ,iDiasDisponibilidadeLote'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 832
    Top = 40
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 163
    end
    object qryProdutonCdTabTipoModoGestaoProduto: TIntegerField
      FieldName = 'nCdTabTipoModoGestaoProduto'
    end
    object qryProdutocFlgControlaValidadeLote: TIntegerField
      FieldName = 'cFlgControlaValidadeLote'
    end
    object qryProdutoiDiasDisponibilidadeLote: TIntegerField
      FieldName = 'iDiasDisponibilidadeLote'
    end
  end
  object qryTabTipoOrigemMovLote: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoOrigemMovLote'
      
        '      ,(dbo.fn_ZeroEsquerda(nCdTabTipoOrigemMovLote,3) + '#39' - '#39' +' +
        ' cNmTabTipoOrigemMovLote) as cNmTabTipoOrigemMovLote'
      '      ,cFlgEntradaSaida'
      'FROM TabTipoOrigemMovLote'
      'WHERE nCdTabTipoOrigemMovLote = :nPK')
    Left = 800
    Top = 40
    object qryTabTipoOrigemMovLotenCdTabTipoOrigemMovLote: TIntegerField
      FieldName = 'nCdTabTipoOrigemMovLote'
    end
    object qryTabTipoOrigemMovLotecNmTabTipoOrigemMovLote: TStringField
      FieldName = 'cNmTabTipoOrigemMovLote'
      Size = 50
    end
    object qryTabTipoOrigemMovLotecFlgEntradaSaida: TStringField
      FieldName = 'cFlgEntradaSaida'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = qryProduto
    Left = 840
    Top = 72
  end
  object DataSource1: TDataSource
    DataSet = qryTabTipoOrigemMovLote
    Left = 808
    Top = 72
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque '
      '  FROM LocalEstoque '
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 864
    Top = 41
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryLocalEstoque
    Left = 872
    Top = 73
  end
  object qryLoteDisponivel: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Filtered = True
    AfterPost = qryLoteDisponivelAfterPost
    AfterScroll = qryLoteDisponivelAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempLoteProduto'
      ' WHERE cFlgSelecionado = 0')
    Left = 248
    Top = 233
    object qryLoteDisponivelnCdLoteProduto: TAutoIncField
      FieldName = 'nCdLoteProduto'
      ReadOnly = True
    end
    object qryLoteDisponivelcNrLote: TStringField
      FieldName = 'cNrLote'
      Size = 30
    end
    object qryLoteDisponiveldDtFabricacao: TDateTimeField
      FieldName = 'dDtFabricacao'
    end
    object qryLoteDisponiveldDtDisponibilidade: TDateTimeField
      FieldName = 'dDtDisponibilidade'
    end
    object qryLoteDisponiveldDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryLoteDisponivelnSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      Precision = 12
      Size = 2
    end
    object qryLoteDisponivelnQtdeSelecionado: TBCDField
      FieldName = 'nQtdeSelecionado'
      Precision = 12
      Size = 2
    end
    object qryLoteDisponivelcFlgNumSerie: TIntegerField
      FieldName = 'cFlgNumSerie'
    end
    object qryLoteDisponivelcFlgSelecionado: TIntegerField
      FieldName = 'cFlgSelecionado'
    end
  end
  object qryLoteSelecionado: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempLoteProduto'
      ' WHERE cFlgSelecionado = 1')
    Left = 256
    Top = 448
    object qryLoteSelecionadonCdLoteProduto: TAutoIncField
      FieldName = 'nCdLoteProduto'
      ReadOnly = True
    end
    object qryLoteSelecionadocNrLote: TStringField
      FieldName = 'cNrLote'
      Size = 30
    end
    object qryLoteSelecionadodDtFabricacao: TDateTimeField
      FieldName = 'dDtFabricacao'
    end
    object qryLoteSelecionadodDtDisponibilidade: TDateTimeField
      FieldName = 'dDtDisponibilidade'
    end
    object qryLoteSelecionadodDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryLoteSelecionadonSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      Precision = 12
      Size = 2
    end
    object qryLoteSelecionadonQtdeSelecionado: TBCDField
      FieldName = 'nQtdeSelecionado'
      Precision = 12
      Size = 2
    end
    object qryLoteSelecionadocFlgNumSerie: TIntegerField
      FieldName = 'cFlgNumSerie'
    end
    object qryLoteSelecionadocFlgSelecionado: TIntegerField
      FieldName = 'cFlgSelecionado'
    end
  end
  object DataSource4: TDataSource
    DataSet = qryLoteDisponivel
    Left = 264
    Top = 265
  end
  object DataSource5: TDataSource
    DataSet = qryLoteSelecionado
    Left = 272
    Top = 480
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLocalEstoque'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'TRUNCATE TABLE #TempLoteProduto'
      ''
      'DECLARE @nCdProduto     int'
      '       ,@cFlgNumSerie   int'
      '       ,@nCdLocalEstoque int'
      ''
      'SET @nCdProduto      = :nCdProduto'
      'SET @nCdLocalEstoque = :nCdLocalEstoque'
      
        'SET @cFlgNumSerie = (CASE WHEN ((SELECT PD.nCdTabTipoModoGestaoP' +
        'roduto '
      '                                     FROM Produto PD'
      
        '                                    WHERE PD.nCdProduto = @nCdPr' +
        'oduto) = 2) '
      '                          THEN 1'
      '                          ELSE 0'
      '                     END)'
      ''
      'INSERT INTO #TempLoteProduto (cNrLote         '
      '                             ,dDtFabricacao      '
      '                             ,dDtDisponibilidade '
      '                             ,dDtValidade       '
      '                             ,nSaldoLote         '
      '                             ,nQtdeSelecionado  '
      '                             ,cFlgNumSerie)'
      ''
      'SELECT cNrLote'
      '      ,dDtFabricacao'
      '      ,dDtDisponibilidade'
      '      ,dDtValidade'
      '      ,nSaldoLote '
      
        '      ,@cFlgNumSerie /* A quantidade seleciona '#233' igualada a vari' +
        'avel porque '
      
        '                        quando for controlado por numero de seri' +
        'e a quantidade '
      '                        selecionada sempre ser'#225' 1 */'
      '      ,@cFlgNumSerie'
      '  FROM LoteProduto'
      ' WHERE nCdProduto      = @nCdProduto'
      '   AND nCdLocalEstoque = @nCdLocalEstoque'
      '   AND nSaldoLote      > 0')
    Left = 120
    Top = 265
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempLoteProduto'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'   ' +
      ' CREATE TABLE #TempLoteProduto(nCdLoteProduto     int PRIMARY KE' +
      'Y IDENTITY(1,1)'#13#10'                                 ,cNrLote      ' +
      '      varchar(30)'#13#10'                                 ,dDtFabricac' +
      'ao      datetime'#13#10'                                 ,dDtDisponibi' +
      'lidade datetime'#13#10'                                 ,dDtValidade  ' +
      '      datetime'#13#10'                                 ,nSaldoLote    ' +
      '     decimal(12,2) default 0 not null'#13#10'                         ' +
      '        ,nQtdeSelecionado   decimal(12,2) default 0 not null'#13#10'  ' +
      '                               ,cFlgNumSerie       int'#13#10'        ' +
      '                         ,cFlgSelecionado    int default 0 not n' +
      'ull)'#13#10#13#10'END'#13#10#13#10
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 88
    Top = 265
  end
end
