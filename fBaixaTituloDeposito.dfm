inherited frmBaixaTituloDeposito: TfrmBaixaTituloDeposito
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Baixa de T'#237'tulo por Dep'#243'sito'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 137
    Width = 1152
    Height = 677
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    ButtonWidth = 80
    inherited ToolButton1: TToolButton
      Caption = '&Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 80
    end
    inherited ToolButton2: TToolButton
      Left = 88
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 108
    Align = alTop
    Caption = 'Filtro'
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 16
      Top = 48
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Banco Dep'#243'sito'
    end
    object Label3: TLabel
      Tag = 1
      Left = 9
      Top = 72
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Dep'#243'sito'
    end
    object Label6: TLabel
      Tag = 1
      Left = 172
      Top = 72
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label1: TLabel
      Tag = 1
      Left = 51
      Top = 24
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object edtBanco: TMaskEdit
      Left = 96
      Top = 40
      Width = 60
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnExit = edtBancoExit
      OnKeyDown = edtBancoKeyDown
    end
    object edtDtInicial: TMaskEdit
      Left = 96
      Top = 64
      Width = 72
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edtDtFinal: TMaskEdit
      Left = 192
      Top = 64
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object edtTerceiro: TMaskEdit
      Left = 96
      Top = 16
      Width = 60
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnExit = edtTerceiroExit
      OnKeyDown = edtTerceiroKeyDown
    end
    object RadioGroup1: TRadioGroup
      Left = 272
      Top = 64
      Width = 353
      Height = 33
      Caption = 'Situa'#231#227'o'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Todos Dep'#243'sitos'
        'Somente com Saldo em Aberto')
      TabOrder = 4
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 160
      Top = 16
      Width = 553
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 160
      Top = 40
      Width = 553
      Height = 21
      DataField = 'cNmBanco'
      DataSource = DataSource2
      TabOrder = 6
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 137
    Width = 1152
    Height = 677
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsLanctos
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn
        Caption = 'Lancto'
        DataBinding.FieldName = 'nCdLanctoFin'
      end
      object cxGrid1DBTableView1dDtExtrato: TcxGridDBColumn
        Caption = 'Data Dep'#243'sito'
        DataBinding.FieldName = 'dDtExtrato'
      end
      object cxGrid1DBTableView1nCdBanco: TcxGridDBColumn
        Caption = 'Banco'
        DataBinding.FieldName = 'nCdBanco'
      end
      object cxGrid1DBTableView1cAgencia: TcxGridDBColumn
        Caption = 'Ag'#234'ncia'
        DataBinding.FieldName = 'cAgencia'
      end
      object cxGrid1DBTableView1nCdConta: TcxGridDBColumn
        Caption = 'Conta'
        DataBinding.FieldName = 'nCdConta'
      end
      object cxGrid1DBTableView1dDtConciliacao: TcxGridDBColumn
        Caption = 'Data Concilia'#231#227'o'
        DataBinding.FieldName = 'dDtConciliacao'
      end
      object cxGrid1DBTableView1cNmUsuarioConciliacao: TcxGridDBColumn
        Caption = 'Usu'#225'rio Concilia'#231#227'o'
        DataBinding.FieldName = 'cNmUsuarioConciliacao'
        Width = 188
      end
      object cxGrid1DBTableView1nValLancto: TcxGridDBColumn
        Caption = 'Valor Dep'#243'sito'
        DataBinding.FieldName = 'nValLancto'
        Width = 125
      end
      object cxGrid1DBTableView1nSaldoPendenteIdent: TcxGridDBColumn
        Caption = 'Saldo '#224' Abater'
        DataBinding.FieldName = 'nSaldoPendenteIdent'
        Width = 125
      end
      object cxGrid1DBTableView1nCdTerceiroDep: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTerceiroDep'
        Visible = False
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Credor'
        DataBinding.FieldName = 'cNmTerceiro'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 496
    Top = 253
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 680
    Top = 96
  end
  object qryBanco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdBanco, cNmBanco'
      'FROM Banco'
      'WHERE nCdBanco = :nPK')
    Left = 648
    Top = 253
    object qryBanconCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryBancocNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryBanco
    Left = 648
    Top = 104
  end
  object qryLanctos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdBanco'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'cTipo'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdBanco    int'
      '       ,@nCdEmpresa  int'
      '       ,@nCdTerceiro int'
      '       ,@dDtInicial  varchar(10)'
      '       ,@dDtFinal    varchar(10)'
      '       ,@cTipo       int'
      ''
      'Set @nCdEmpresa  = :nCdEmpresa'
      'Set @nCdBanco    = :nCdBanco'
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @dDtInicial  = :dDtInicial'
      'Set @dDtFinal    = :dDtFinal'
      'Set @cTipo       = :cTipo'
      ''
      '--'
      '-- @cTipo'
      '-- 0 - Todos os depositos'
      '-- 1 - Somente os depositos com saldo'
      '--'
      ''
      'SELECT LanctoFin.nCdLanctoFin'
      '      ,LanctoFin.dDtExtrato'
      '      ,ContaBancaria.nCdBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,LanctoFin.dDtConciliacao'
      '      ,Usuario.cNmUsuario cNmUsuarioConciliacao'
      '      ,LanctoFin.nValLancto'
      '      ,LanctoFin.nSaldoPendenteIdent'
      '      ,LanctoFin.nCdTerceiroDep'
      '      ,Terceiro.cNmTerceiro'
      '  FROM LanctoFin'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = LanctoFin.nCdContaBancaria'
      
        '       INNER JOIN Terceiro      ON Terceiro.nCdTerceiro         ' +
        '  = LanctoFin.nCdTerceiroDep'
      
        '       LEFT  JOIN Usuario       ON Usuario.nCdUsuario           ' +
        '  = LanctoFin.nCdUsuarioConciliacao'
      ' WHERE ContaBancaria.nCdEmpresa    = @nCdEmpresa'
      
        '   AND ((ContaBancaria.nCdBanco    = @nCdBanco)    OR (@nCdBanco' +
        '    = 0))'
      
        '   AND ((LanctoFin.nCdTerceiroDep  = @nCdTerceiro) OR (@nCdTerce' +
        'iro = 0))'
      '   AND LanctoFin.cFlgCredNaoIdent  = 1'
      '   AND LanctoFin.cFlgConciliado    = 1'
      
        '   AND (LanctoFin.dDtExtrato      >= Convert(DATETIME,@dDtInicia' +
        'l,103) OR (@dDtInicial = '#39'01/01/1900'#39'))'
      
        '   AND (LanctoFin.dDtExtrato       < Convert(DATETIME,@dDtFinal,' +
        '103)+1 OR (@dDtFinal   = '#39'01/01/1900'#39'))'
      '   AND ((@cTipo = 0) OR (LanctoFin.nSaldoPendenteIdent > 0))'
      '')
    Left = 288
    Top = 232
    object qryLanctosnCdLanctoFin: TAutoIncField
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryLanctosdDtExtrato: TDateTimeField
      FieldName = 'dDtExtrato'
    end
    object qryLanctosnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryLanctoscAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryLanctosnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryLanctosdDtConciliacao: TDateTimeField
      FieldName = 'dDtConciliacao'
    end
    object qryLanctoscNmUsuarioConciliacao: TStringField
      FieldName = 'cNmUsuarioConciliacao'
      Size = 50
    end
    object qryLanctosnValLancto: TBCDField
      FieldName = 'nValLancto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLanctosnSaldoPendenteIdent: TBCDField
      FieldName = 'nSaldoPendenteIdent'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLanctosnCdTerceiroDep: TIntegerField
      FieldName = 'nCdTerceiroDep'
    end
    object qryLanctoscNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsLanctos: TDataSource
    DataSet = qryLanctos
    Left = 336
    Top = 232
  end
end
