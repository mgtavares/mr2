inherited frmMonitorProducao_Exibe_ItensPedido: TfrmMonitorProducao_Exibe_ItensPedido
  Left = 178
  Top = 215
  Width = 971
  Caption = 'Itens do Pedido'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 955
  end
  inherited ToolBar1: TToolBar
    Width = 955
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 955
    Height = 433
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 429
    ClientRectLeft = 4
    ClientRectRight = 951
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens do Pedido em Aberto'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 947
        Height = 405
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource1
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 583
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeSaldo'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Todos os Itens do Pedido'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 947
        Height = 405
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource2
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 583
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeSaldo'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryItemPedidoAberto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,(nQtdePed - nQtdeExpRec) as nQtdeSaldo'
      '  FROM ItemPedido'
      ' WHERE (nQtdePed - nQtdeExpRec - nQtdeCanc) > 0'
      '   AND nCdPedido = :nCdPedido'
      ' ORDER BY cNmItem')
    Left = 260
    Top = 165
    object qryItemPedidoAbertonCdProduto: TIntegerField
      DisplayLabel = 'Item do Pedido|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidoAbertocNmItem: TStringField
      DisplayLabel = 'Item do Pedido|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidoAbertonQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidoAbertonQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Faturado'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemPedidoAbertonQtdeSaldo: TBCDField
      DisplayLabel = 'Quantidade|Saldo'
      FieldName = 'nQtdeSaldo'
      ReadOnly = True
      Precision = 13
    end
  end
  object DataSource1: TDataSource
    DataSet = qryItemPedidoAberto
    Left = 308
    Top = 165
  end
  object qryItemPedidoTotal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,(nQtdePed - nQtdeExpRec - nQtdeCanc) as nQtdeSaldo'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nCdPedido'
      ' ORDER BY cNmItem')
    Left = 252
    Top = 245
    object IntegerField1: TIntegerField
      DisplayLabel = 'Item do Pedido|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object StringField1: TStringField
      DisplayLabel = 'Item do Pedido|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object BCDField1: TBCDField
      DisplayLabel = 'Quantidade|Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object BCDField2: TBCDField
      DisplayLabel = 'Quantidade|Faturado'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object BCDField3: TBCDField
      DisplayLabel = 'Quantidade|Saldo'
      FieldName = 'nQtdeSaldo'
      ReadOnly = True
      Precision = 13
    end
  end
  object DataSource2: TDataSource
    DataSet = qryItemPedidoTotal
    Left = 284
    Top = 245
  end
end
