unit fTipoAlerta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmTipoAlerta = class(TfrmCadastro_Padrao)
    qryMasternCdTipoAlerta: TAutoIncField;
    qryMastercNmTipoAlerta: TStringField;
    qryMasternCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryMastercNmStatus: TStringField;
    DBEdit4: TDBEdit;
    qryUsuarioTipoAlerta: TADOQuery;
    qryUsuarioTipoAlertanCdUsuario: TIntegerField;
    qryUsuarioTipoAlertanCdTipoAlerta: TIntegerField;
    DBGridEh1: TDBGridEh;
    dsUsuarioTipoAlerta: TDataSource;
    qryUsuarioTipoAlertacNmUsuario: TStringField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryUsuarioTipoAlertaCalcFields(DataSet: TDataSet);
    procedure qryUsuarioTipoAlertaBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoAlerta: TfrmTipoAlerta;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoAlerta.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOALERTA' ;
  nCdTabelaSistema  := 68 ;
  nCdConsultaPadrao := 156 ;
  bCodigoAutomatico := False ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmTipoAlerta.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus ;
end;

procedure TfrmTipoAlerta.qryUsuarioTipoAlertaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioTipoAlertacNmUsuario.Value = '') then
  begin
      If (qryUsuarioTipoAlertanCdUsuario.Value > 0) then
      begin
          PosicionaQuery(qryUsuario, qryUsuarioTipoAlertanCdUsuario.AsString) ;

          if not qryUsuario.eof then
              qryUsuarioTipoAlertacNmUsuario.Value := qryUsuariocNmUsuario.Value ;
              
      end ;
  end ;

end;

procedure TfrmTipoAlerta.qryUsuarioTipoAlertaBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioTipoAlertanCdTipoAlerta.Value := qryMasternCdTipoAlerta.Value ;
  
end;

procedure TfrmTipoAlerta.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTipoAlerta.State = dsBrowse) then
             qryUsuarioTipoAlerta.Edit ;
        
        if (qryUsuarioTipoAlerta.State = dsInsert) or (qryUsuarioTipoAlerta.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTipoAlertanCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoAlerta.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdTipoAlerta.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmTipoAlerta.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdTipoAlerta.Value <= 0) then
  begin
      MensagemAlerta('Informe o c�digo do alerta.') ;
      abort ;
  end ;

  if (qryMastercNmTipoAlerta.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do alerta.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmTipoAlerta.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuarioTipoAlerta, qryMasternCdTipoAlerta.asString) ;
  
end;

procedure TfrmTipoAlerta.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioTipoAlerta.Close ;
end;

initialization
    RegisterClass(TfrmTipoAlerta) ;

end.
