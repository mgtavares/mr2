unit fAnaliseQualidade_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB;

type
  TfrmAnaliseQualidade_Itens = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryItens: TADOQuery;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtde: TBCDField;
    DBGridEh1: TDBGridEh;
    dsItens: TDataSource;
    qryRecebimento: TADOQuery;
    qryRecebimentonCdRecebimento: TIntegerField;
    qryRecebimentonCdEmpresa: TIntegerField;
    qryRecebimentonCdLoja: TIntegerField;
    qryRecebimentonCdTipoReceb: TIntegerField;
    qryRecebimentodDtReceb: TDateTimeField;
    qryRecebimentonCdTerceiro: TIntegerField;
    qryRecebimentocNrDocto: TStringField;
    qryRecebimentocSerieDocto: TStringField;
    qryRecebimentodDtDocto: TDateTimeField;
    qryRecebimentonValDocto: TBCDField;
    qryRecebimentonCdTabStatusReceb: TIntegerField;
    qryRecebimentonCdUsuarioFech: TIntegerField;
    qryRecebimentodDtFech: TDateTimeField;
    qryRecebimentocOBS: TStringField;
    qryRecebimentonValTotalNF: TBCDField;
    qryRecebimentonValTitulo: TBCDField;
    qryRecebimentonPercDescontoVencto: TBCDField;
    qryRecebimentonPercAcrescimoVendor: TBCDField;
    qryRecebimentonCdUsuarioAutorFinanc: TIntegerField;
    qryRecebimentodDtAutorFinanc: TDateTimeField;
    qryRecebimentocCFOP: TStringField;
    qryRecebimentocCNPJEmissor: TStringField;
    qryRecebimentocIEEmissor: TStringField;
    qryRecebimentocUFEmissor: TStringField;
    qryRecebimentocModeloNF: TStringField;
    qryRecebimentonValBaseICMS: TBCDField;
    qryRecebimentonValICMS: TBCDField;
    qryRecebimentonValIsenta: TBCDField;
    qryRecebimentonValOutras: TBCDField;
    qryRecebimentonValIPI: TBCDField;
    qryRecebimentonValTotalItensPag: TBCDField;
    qryRecebimentonValDescontoNF: TBCDField;
    qryRecebimentodDtContab: TDateTimeField;
    qryRecebimentocFlgIntegrado: TIntegerField;
    qryRecebimentocFlgParcelaAutom: TIntegerField;
    qryRecebimentonValICMSSub: TBCDField;
    qryRecebimentonValDespesas: TBCDField;
    qryRecebimentonValFrete: TBCDField;
    qryRecebimentonPercDescProduto: TBCDField;
    qryRecebimentocFlgAnaliseQualidade: TIntegerField;
    qryRecebimentocFlgAnaliseQualidadeOK: TIntegerField;
    qryRecebimentodDtAnaliseQualidade: TDateTimeField;
    qryRecebimentonCdUsuarioAnaliseQualidade: TIntegerField;
    qryRecebimentocOBSQualidade: TStringField;
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdRecebimento : integer ;
  end;

var
  frmAnaliseQualidade_Itens: TfrmAnaliseQualidade_Itens;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAnaliseQualidade_Itens.ToolButton5Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a inspe��o de qualidade dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  PosicionaQuery(qryRecebimento, IntToStr(nCdRecebimento)) ;
  
  frmMenu.Connection.BeginTrans;

  try
      qryRecebimento.Edit ;
      qryRecebimentocFlgAnaliseQualidadeOK.Value     := 1 ;
      qryRecebimentonCdUsuarioAnaliseQualidade.Value := frmMenu.nCdUsuarioLogado;
      qryRecebimentodDtAnaliseQualidade.Value        := Now() ;
      qryRecebimentonCdTabStatusReceb.Value          := 3 ;
      qryRecebimento.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Recebimento liberado!') ;

  Close ;


end;

procedure TfrmAnaliseQualidade_Itens.ToolButton6Click(Sender: TObject);
var
    cMotivo : String ;
begin
  inherited;

  case MessageDlg('Confirma a rejei��o dos itens ? TODOS os itens do recebimento ser�o rejeitados.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  InputQuery('Motivo','Motivo Rejei��o dos Itens',cMotivo) ;


  PosicionaQuery(qryRecebimento, IntToStr(nCdRecebimento)) ;

  frmMenu.Connection.BeginTrans;

  try
      qryRecebimento.Edit ;
      qryRecebimentocFlgAnaliseQualidadeOK.Value     := 0 ;
      qryRecebimentonCdUsuarioAnaliseQualidade.Value := frmMenu.nCdUsuarioLogado;
      qryRecebimentodDtAnaliseQualidade.Value        := Now() ;
      qryRecebimentocOBSQualidade.Value              := cMotivo ;
      qryRecebimentonCdTabStatusReceb.Value          := 7 ;
      qryRecebimento.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Recebimento rejeitado!') ;

  Close ;

end;

end.
