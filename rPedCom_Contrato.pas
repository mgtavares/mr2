unit rPedCom_Contrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPedCom_Contrato = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidocNmEmpresa: TStringField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidocNmTabStatusPed: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel5: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    qryItemEstoque_Grade: TADOQuery;
    qryItemEstoque_GradenCdProduto: TIntegerField;
    qryItemEstoque_GradecNmItem: TStringField;
    qryItemEstoque_GradenQtdePed: TBCDField;
    qryItemEstoque_GradenValTotalItem: TBCDField;
    qryItemEstoque_GradenCdGrade: TIntegerField;
    qryItemEstoque_GradecTituloGrade: TStringField;
    qryItemEstoque_GradecQtdeGrade: TStringField;
    QRSubDetail1: TQRSubDetail;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText30: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText15: TQRDBText;
    QRLabel22: TQRLabel;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    QRSubDetail2: TQRSubDetail;
    QRDBText16: TQRDBText;
    QRGroup3: TQRGroup;
    qryItemADnCdPedido: TIntegerField;
    QRShape3: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    qryPedidocNmContato: TStringField;
    QRSubDetail3: TQRSubDetail;
    QRGroup4: TQRGroup;
    qryItemFormula: TADOQuery;
    qryItemFormulanCdProduto: TIntegerField;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulacComposicao: TStringField;
    QRShape4: TQRShape;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    qryPedidocNmTerceiroRepres: TStringField;
    qryItemEstoque_GradedDtEntregaIni: TDateTimeField;
    qryItemEstoque_GradedDtEntregaFim: TDateTimeField;
    qryItemEstoque_GradenQtdePrev: TBCDField;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel37: TQRLabel;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    SummaryBand1: TQRBand;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel27: TQRLabel;
    QRLabel34: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel36: TQRLabel;
    QRShape5: TQRShape;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    qryItemEstoque_GradenPercIPI: TBCDField;
    qryItemEstoque_GradenPercICMSSub: TBCDField;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    qryPedidonValImposto: TBCDField;
    qryPedidonValProdutos: TBCDField;
    qryPedidonValFrete: TBCDField;
    qryPedidonValOutros: TBCDField;
    qryPedidocNmUsuarioComprador: TStringField;
    QRLabel39: TQRLabel;
    QRDBText29: TQRDBText;
    qryItemEstoque_GradecSiglaUnidadeMedida: TStringField;
    QRLabel15: TQRLabel;
    QRDBText33: TQRDBText;
    QRDBText39: TQRDBText;
    QRLabel47: TQRLabel;
    qryPedidocEnderecoLocalEntrega: TStringField;
    qryItemEstoque_GradenValCustoUnit: TFloatField;
    qryItemEstoque_GradenValUnitario: TFloatField;
    qryItemEstoque_GradenValDesconto: TFloatField;
    qryItemEstoque_GradenValAcrescimo: TFloatField;
    qryItemEstoque_GradenValIPIUnitario: TFloatField;
    qryItemEstoque_GradenPercDescontoItem: TFloatField;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedCom_Contrato: TrptPedCom_Contrato;

implementation

uses fMenu;

{$R *.dfm}

procedure TrptPedCom_Contrato.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

  QRLabel22.Caption   := 'Grade' ;
  QRSubDetail1.Height := 32 ;

  if (qryItemEstoque_GradecTituloGrade.Value = '') then
  begin
      QRLabel22.Caption := 'Item Estoque' ;
      QRSubDetail1.Height := 16 ;
  end ;


end;

procedure TrptPedCom_Contrato.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin

  QRDBText6.Mask  := frmMenu.cMascaraCompras;
  QRDBText18.Mask := frmMenu.cMascaraCompras;
  QRDBText24.Mask := frmMenu.cMascaraCompras;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      QRLabel8.Enabled  := False ;
      QRDBText2.Enabled := False ;
  end ;

end;

end.
