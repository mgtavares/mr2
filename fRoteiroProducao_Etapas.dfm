inherited frmRoteiroProducao_Etapas: TfrmRoteiroProducao_Etapas
  BorderIcons = [biSystemMenu]
  Caption = 'Roteiro Produ'#231#227'o - Etapas'
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 98
    Height = 366
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 69
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 24
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object Label3: TLabel
      Tag = 1
      Left = 8
      Top = 48
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo OP'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 49
      Top = 16
      Width = 89
      Height = 21
      DataField = 'nCdProduto'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 145
      Top = 16
      Width = 617
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 49
      Top = 40
      Width = 65
      Height = 21
      DataField = 'nCdTipoOP'
      DataSource = DataSource2
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 121
      Top = 40
      Width = 310
      Height = 21
      DataField = 'cNmTipoOP'
      DataSource = DataSource2
      TabOrder = 3
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 98
    Width = 784
    Height = 366
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 362
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Etapas da Produ'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 45
        Width = 776
        Height = 293
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRoteiroEtapaProducao
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        OnKeyDown = DBGridEh1KeyDown
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRoteiroEtapaProducao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdRoteiroProducao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iSeq'
            Footers = <>
            Width = 35
          end
          item
            EditButtons = <>
            FieldName = 'nCdCentroProdutivo'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmCentroProdutivo'
            Footers = <>
            ReadOnly = True
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nCdLinhaProducao'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmLinhaProducao'
            Footers = <>
            ReadOnly = True
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoMaquinaPCP'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoMaquinaPCP'
            Footers = <>
            ReadOnly = True
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nCdEtapaProducao'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmEtapaProducao'
            Footers = <>
            ReadOnly = True
            Width = 99
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoCalculoEtapa'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoCalculoEtapa'
            Footers = <>
            ReadOnly = True
            Width = 67
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeLotePadrao'
            Footers = <>
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'iTempoPreparacao'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'iTempoSetup'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'iTempoProcesso'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'iTempoEspera'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cInstrucoesProducao'
            Footers = <>
            ShowImageAndText = True
            Width = 112
          end
          item
            EditButtons = <>
            FieldName = 'cArquivoMidia'
            Footers = <>
            Width = 196
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 776
        Height = 45
        Align = alTop
        TabOrder = 1
        object btExibirProdutoEtapa: TcxButton
          Left = 3
          Top = 8
          Width = 153
          Height = 33
          Caption = 'Exibir Produtos da Etapa'
          TabOrder = 0
          OnClick = btExibirProdutoEtapaClick
          Glyph.Data = {
            06030000424D060300000000000036000000280000000F0000000F0000000100
            180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
            00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
            FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
            80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
            0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
            000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
            00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
            E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
            E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
            99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
            E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
            E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
            99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
            E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
            999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
            99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
            E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
            999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
            9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 440
    Top = 56
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryTipoOP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoOP, cNmTipoOP'
      'FROM TipoOP'
      'WHERE nCdTipoOP = :nPK')
    Left = 472
    Top = 56
    object qryTipoOPnCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryTipoOPcNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 536
    Top = 56
  end
  object DataSource2: TDataSource
    DataSet = qryTipoOP
    Left = 504
    Top = 56
  end
  object qryRoteiroEtapaProducao: TADOQuery
    AutoCalcFields = False
    Connection = frmMenu.Connection
    BeforePost = qryRoteiroEtapaProducaoBeforePost
    OnCalcFields = qryRoteiroEtapaProducaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM RoteiroEtapaProducao'
      ' WHERE nCdRoteiroProducao = :nPK')
    Left = 484
    Top = 178
    object qryRoteiroEtapaProducaonCdRoteiroEtapaProducao: TIntegerField
      FieldName = 'nCdRoteiroEtapaProducao'
    end
    object qryRoteiroEtapaProducaonCdRoteiroProducao: TIntegerField
      FieldName = 'nCdRoteiroProducao'
    end
    object qryRoteiroEtapaProducaoiSeq: TIntegerField
      DisplayLabel = 'Seq.'
      FieldName = 'iSeq'
    end
    object qryRoteiroEtapaProducaonCdCentroProdutivo: TIntegerField
      DisplayLabel = 'Centro Produtivo|C'#243'd.'
      FieldName = 'nCdCentroProdutivo'
      OnChange = qryRoteiroEtapaProducaonCdCentroProdutivoChange
    end
    object qryRoteiroEtapaProducaocNmCentroProdutivo: TStringField
      DisplayLabel = 'Centro Produtivo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCentroProdutivo'
      Size = 50
      Calculated = True
    end
    object qryRoteiroEtapaProducaonCdLinhaProducao: TIntegerField
      DisplayLabel = 'Linha Produ'#231#227'o|C'#243'd.'
      FieldName = 'nCdLinhaProducao'
      OnChange = qryRoteiroEtapaProducaonCdLinhaProducaoChange
    end
    object qryRoteiroEtapaProducaocNmLinhaProducao: TStringField
      DisplayLabel = 'Linha Produ'#231#227'o|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLinhaProducao'
      Size = 50
      Calculated = True
    end
    object qryRoteiroEtapaProducaonCdEtapaProducao: TIntegerField
      DisplayLabel = 'Etapa Produ'#231#227'o|C'#243'd.'
      FieldName = 'nCdEtapaProducao'
      OnChange = qryRoteiroEtapaProducaonCdEtapaProducaoChange
    end
    object qryRoteiroEtapaProducaocNmEtapaProducao: TStringField
      DisplayLabel = 'Etapa Produ'#231#227'o|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmEtapaProducao'
      Size = 50
      Calculated = True
    end
    object qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa: TIntegerField
      DisplayLabel = 'Tipo C'#225'lculo Etapa|C'#243'd.'
      FieldName = 'nCdTabTipoCalculoEtapa'
      OnChange = qryRoteiroEtapaProducaonCdTabTipoCalculoEtapaChange
    end
    object qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa: TStringField
      DisplayLabel = 'Tipo C'#225'lculo Etapa|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTabTipoCalculoEtapa'
      Size = 50
      Calculated = True
    end
    object qryRoteiroEtapaProducaoiTempoPreparacao: TBCDField
      DisplayLabel = 'Tempos|Prepara'#231#227'o'
      FieldName = 'iTempoPreparacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRoteiroEtapaProducaonQtdeLotePadrao: TBCDField
      DisplayLabel = 'Quantidade|Lote'
      FieldName = 'nQtdeLotePadrao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 6
    end
    object qryRoteiroEtapaProducaoiTempoSetup: TBCDField
      DisplayLabel = 'Tempos|Setup'
      FieldName = 'iTempoSetup'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRoteiroEtapaProducaoiTempoProcesso: TBCDField
      DisplayLabel = 'Tempos|Processo'
      FieldName = 'iTempoProcesso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRoteiroEtapaProducaoiTempoEspera: TBCDField
      DisplayLabel = 'Tempos|Espera'
      FieldName = 'iTempoEspera'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRoteiroEtapaProducaocInstrucoesProducao: TMemoField
      DisplayLabel = 'Instru'#231#227'o Produ'#231#227'o'
      FieldName = 'cInstrucoesProducao'
      BlobType = ftMemo
    end
    object qryRoteiroEtapaProducaocArquivoMidia: TStringField
      DisplayLabel = 'Arquivo de M'#237'dia'
      FieldName = 'cArquivoMidia'
      Size = 150
    end
    object qryRoteiroEtapaProducaonCdTipoMaquinaPCP: TIntegerField
      DisplayLabel = 'M'#225'quina|C'#243'd.'
      FieldName = 'nCdTipoMaquinaPCP'
      OnChange = qryRoteiroEtapaProducaonCdTipoMaquinaPCPChange
    end
    object qryRoteiroEtapaProducaocNmTipoMaquinaPCP: TStringField
      DisplayLabel = 'M'#225'quina|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTipoMaquinaPCP'
      Size = 50
      Calculated = True
    end
  end
  object dsRoteiroEtapaProducao: TDataSource
    DataSet = qryRoteiroEtapaProducao
    Left = 524
    Top = 178
  end
  object qryCentroProdutivo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCentroProdutivo '
      'FROM CentroProdutivo'
      'WHERE nCdCentroProdutivo = :nPK')
    Left = 508
    Top = 258
    object qryCentroProdutivocNmCentroProdutivo: TStringField
      FieldName = 'cNmCentroProdutivo'
      Size = 50
    end
  end
  object qryLinhaProducao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCentroProdutivo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLinhaProducao'
      'FROM LinhaProducao'
      'WHERE nCdCentroProdutivo = :nCdCentroProdutivo'
      'AND nCdLinhaProducao = :nPK')
    Left = 516
    Top = 298
    object qryLinhaProducaocNmLinhaProducao: TStringField
      FieldName = 'cNmLinhaProducao'
      Size = 50
    end
  end
  object qryEtapaProducao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEtapaProducao'
      '      ,cFlgGeraPedidoComercial'
      '  FROM EtapaProducao'
      ' WHERE nCdEtapaProducao = :nPK')
    Left = 524
    Top = 338
    object qryEtapaProducaocNmEtapaProducao: TStringField
      FieldName = 'cNmEtapaProducao'
      Size = 50
    end
    object qryEtapaProducaocFlgGeraPedidoComercial: TIntegerField
      FieldName = 'cFlgGeraPedidoComercial'
    end
  end
  object qryTipoCalculoEtapa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTabTipoCalculoEtapa'
      '  FROM TabTipoCalculoEtapa'
      ' WHERE nCdTabTipoCalculoEtapa = :nPK')
    Left = 524
    Top = 378
    object qryTipoCalculoEtapacNmTabTipoCalculoEtapa: TStringField
      FieldName = 'cNmTabTipoCalculoEtapa'
      Size = 50
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 348
    Top = 250
  end
  object qryTipoMaquinaPCP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoMaquinaPCP'
      'WHERE nCdTipoMaquinaPCP = :nPK')
    Left = 588
    Top = 258
    object qryTipoMaquinaPCPnCdTipoMaquinaPCP: TIntegerField
      FieldName = 'nCdTipoMaquinaPCP'
    end
    object qryTipoMaquinaPCPcNmTipoMaquinaPCP: TStringField
      FieldName = 'cNmTipoMaquinaPCP'
      Size = 50
    end
    object qryTipoMaquinaPCPnCdCentroProdutivo: TIntegerField
      FieldName = 'nCdCentroProdutivo'
    end
    object qryTipoMaquinaPCPnCdLinhaProducao: TIntegerField
      FieldName = 'nCdLinhaProducao'
    end
  end
end
