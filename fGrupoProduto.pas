unit fGrupoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, cxPC, cxControls, ER2Lookup;

type
  TfrmGrupoProduto = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoProduto: TIntegerField;
    qryMastercNmGrupoProduto: TStringField;
    qryMastercFlgExpPDV: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Image2: TImage;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMasternCdGrupoMRPGP: TIntegerField;
    qryMasternCdGrupoInventarioGP: TIntegerField;
    qryMasternCdTipoPlanejamentoGP: TIntegerField;
    qryMasternCdTipoObtencaoGP: TIntegerField;
    qryMasteriLeadTimeGP: TIntegerField;
    qryMasternLoteMinimoGP: TBCDField;
    qryMasternLoteMultiploGP: TBCDField;
    qryMastercFlgAlertarInventarioGP: TIntegerField;
    qryMastercFlgProdVendaGP: TIntegerField;
    qryMastercFlgProdEstoqueGP: TIntegerField;
    qryMastercFlgProdCompraGP: TIntegerField;
    qryMastercFlgAtivoFixoGP: TIntegerField;
    edtPlanejamento: TER2LookupDBEdit;
    edtObtencao: TER2LookupDBEdit;
    edtGrupoInventario: TER2LookupDBEdit;
    edtGrupoMRP: TER2LookupDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    qryTipoPlanejamento: TADOQuery;
    qryTipoPlanejamentonCdTipoPlanejamento: TIntegerField;
    qryTipoPlanejamentocNmTipoPlanejamento: TStringField;
    DBEdit3: TDBEdit;
    DataSource1: TDataSource;
    qryTipoObtencao: TADOQuery;
    qryTipoObtencaonCdTipoObtencao: TIntegerField;
    qryTipoObtencaocNmTipoObtencao: TStringField;
    DBEdit4: TDBEdit;
    DataSource2: TDataSource;
    qryGrupoInventario: TADOQuery;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    DBEdit5: TDBEdit;
    DataSource3: TDataSource;
    qryGrupoMRP: TADOQuery;
    qryGrupoMRPnCdGrupoMRP: TIntegerField;
    qryGrupoMRPcNmGrupoMRP: TStringField;
    qryGrupoMRPnCdTipoRequisicao: TIntegerField;
    qryGrupoMRPnCdSetor: TIntegerField;
    DBEdit6: TDBEdit;
    DataSource4: TDataSource;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    GroupBox1: TGroupBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoProduto: TfrmGrupoProduto;

implementation

{$R *.dfm}

procedure TfrmGrupoProduto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOPRODUTO' ;
  nCdTabelaSistema  := 33 ;
  nCdConsultaPadrao := 69 ;
end;

procedure TfrmGrupoProduto.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternLoteMinimoGP.Value   := 1 ;
  qryMasternLoteMultiploGP.Value := 1 ;
  
  DBEdit2.SetFocus ;
end;

procedure TfrmGrupoProduto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTipoPlanejamento.Close;
  qryTipoObtencao.Close;
  qryGrupoInventario.Close;
  qryGrupoMRP.Close;
  
end;

procedure TfrmGrupoProduto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTipoPlanejamento.Close;
  qryTipoObtencao.Close;
  qryGrupoInventario.Close;
  qryGrupoMRP.Close;

  PosicionaQuery( qryTipoPlanejamento , qryMasternCdTipoPlanejamentoGP.AsString) ;
  PosicionaQuery( qryTipoObtencao , qryMasternCdTipoObtencaoGP.asString) ;
  PosicionaQuery( qryGrupoInventario, qryMasternCdGrupoInventarioGP.AsString) ;
  PosicionaQuery( qryGrupoMRP, qryMasternCdGrupoMRPGP.asString) ;

end;

procedure TfrmGrupoProduto.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryTipoPlanejamento.Close;
  qryTipoObtencao.Close;
  qryGrupoInventario.Close;
  qryGrupoMRP.Close;

end;

initialization
    RegisterClass(TfrmGrupoProduto) ;

end.
