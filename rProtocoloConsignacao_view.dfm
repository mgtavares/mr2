�
 TRPTPROTOCOLOCONSIGNACAO_VIEW 0Ӥ  TPF0TrptProtocoloConsignacao_viewrptProtocoloConsignacao_viewLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetqryItemConsignacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLEQRSTRINGSBAND1 Functions.DATA00'''' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1Left&Top&Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      ܐ@������v�	@ BandTyperbPageHeader TQRLabelQRLabel1LeftTop"WidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@�������@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   PROTOCOLO DE CONSIGNAÇÃOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�Top"Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      H�	@�������@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel3Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Pág.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData2Left�TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@TUUUUU��	@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel2LeftTop@WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@UUUUUUU�@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Cód. Consignação:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13Left3TopPWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@��������@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionEmpresa:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape6Left TopWidth�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          ������*�@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRShapeQRShape1Left Top/Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          TUUUUU��@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel16LeftBTop`WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionLoja:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18Left.ToppWidth.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@������*�@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption	Vendedor:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel19Left8Top� Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionStatus:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel20LeftQToppWidth[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@������*�@TUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionData Cancelamento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel21LeftVTop`WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      8�@       �@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Data Finalização:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel22LeftoTop@Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@UUUUUUU�@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionData Pedido:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel23Left� Top@Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Cód. Pedido:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel5Left`TopPWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@��������@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Data Devolução:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText7Left`Top@WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@UUUUUUU�@������ڻ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldnCdConsignacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8Left`TopPWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@��������@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataField
cNmEmpresaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText9Left`Top`Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@       �@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcNmLojaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText10Left`ToppWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@������*�@������*�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcNmVendedorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText11Left`Top� WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcTipoStatusConsigFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText14Left� Top@Width.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUUU�@������j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataField	nCdPedidoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText15Left�Top@Width.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@UUUUUUU�@������j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataField	dDtPedidoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText16Left�TopPWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@��������@UUUUUUe�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFielddDtDevolucaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText17Left�Top`WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@       �@������ڻ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFielddDtFinalizacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText18Left�ToppWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@������*�@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFielddDtCancelamentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblEmpresaLeftTopWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4Left3Top� Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@UUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionCliente:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText12Left`Top� Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@UUUUUU�@������*�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcNmTerceiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText13Left`Top� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@     @�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcEnderecoNumCompFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel8Left�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������B�	@UUUUUU�@UUUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionRG:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel9Left�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��	@     @�@     @�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionCPF:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText25Left�Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@UUUUUU�@UUUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcRGFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText26Left�Top� Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@     @�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcCNPJCPFFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText27Left`Top� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@       �@������j�@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataFieldcCepBairroCidadeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText28Left� Top� Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@������J�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryConsignacao	DataField
cTelefonesFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel6LeftbTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@������J�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionTel.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2Left&Top�Width�Height9Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      Ж@������v�	@ BandTyperbPageFooter TQRShapeQRShape5Left Top
Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          ��������@UUUUUU��	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel36LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU�@������Z�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption6   ER2SOFT - Soluções inteligentes para o seu negócio.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel15Left|TopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      X�	@UUUUUU�@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionwww.er2soft.com.brColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRBandDetailBand1Left&Top)Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ForceNewColumnForceNewPage
ParentFontSize.ValuesVUUUUU��@������v�	@ BandTyperbDetail 	TQRDBText	QRDBText4LeftXTop Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@          UUUUUU)�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataField
cNmProdutoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText5LeftTop WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@          ��������@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataField
nCdProdutoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText6Left�Top Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataFieldnQtdeRetiradaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText3Left�Top Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataFieldnQtdeDevolvidaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText19Left�Top Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataFieldnQtdePedidoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText20Left(Top WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@          ��������@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataFieldnValUnitarioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText21LeftxTop Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�	@                Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryItemConsignacao	DataField	nValTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00
ParentFontTransparentWordWrap	FontSize   TQRBandColumnHeaderBand1Left&TopWidth�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@������v�	@ BandTyperbColumnHeader TQRLabel	QRLabel11Left$TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��@UUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionProdutoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel12LeftXTopWidth1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@UUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   DescriçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14Left�TopWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@       �@UUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionRetiradaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel30Left�TopWidth1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��	@UUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption	DevolvidaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel31Left�TopWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��	@UUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionPedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel32Left8TopWidth1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������ڻ	@UUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption	   UnitárioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel33Left�TopWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��	@UUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionTotalColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape2Left Top$Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @                ��@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel10Left�TopWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@UUUUUU��	@       �@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionQtde.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel17Left�TopWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@������*�	@       �@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionQtde.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel25Left�TopWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��	@       �@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionQtde.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel28Left@TopWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      ��	@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionValorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel29Left�TopWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@      P�	@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionValorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel34Left3TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��@       �@     @�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Cód.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand3Left&Top4Width�HeightqFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUU}�@������v�	@ BandType	rbSummary 	TQRDBText
QRDBText24LeftUTopWidth\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      r�	@UUUUUUU�@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTotal	DataFieldnValTotalItensFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel7Left�TopWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������	@UUUUUUU�@������ڻ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
TOTAL - R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape3Left TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @                 �@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel24Left�TopCWidthcHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      	@UUUUUUE�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTAL - PedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel26Left�Top2WidthxHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ж	@������J�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTAL - DevolvidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel27Left�Top!WidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������ �	@      ��@UUUUUU}�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTAL - RetiradoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape4Left TopkWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUU� @          UUUUUU��@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRExprQRExpr2LeftUTop!Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      r�	@      ��@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
ExpressionXIF(SUM(qryItemConsignacao.nQtdeRetirada)   >= 0,SUM(qryItemConsignacao.nQtdeRetirada),0)FontSize  TQRExprQRExpr1LeftUTop2Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      r�	@������J�@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
ExpressionZIF(SUM(qryItemConsignacao.nQtdeDevolvida)   >= 0,SUM(qryItemConsignacao.nQtdeDevolvida),0)FontSize  TQRExprQRExpr3LeftUTopCWidth\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      r�	@UUUUUUE�@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
ExpressionTIF(SUM(qryItemConsignacao.nQtdePedido)   >= 0,SUM(qryItemConsignacao.nQtdePedido),0)FontSize   TQRChildBandQRChildBand1Left&Top�Width�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@������v�	@   	TADOQueryqryConsignacao
ConnectionfrmMenu.Connection	AfterOpenqryConsignacaoAfterOpen
Parameters SQL.StringsSELECT nCdConsignacao      ,cNmEmpresa      ,cNmLoja      ,cNmVendedor      ,nCdPedido      ,cNmTerceiro
      ,cRG      ,cCNPJCPF^      ,cEndereco + ', ' + convert(varchar, iNumero) + ' - ' + cComplemento AS cEnderecoNumCompQ      ,cCEP + ' - ' + cBairro + ' - ' + cCidade + ' - ' + cUF AS cCepBairroCidade.      ,(CASE WHEN (cTelefone1 IS NULL) THEN ''C            ELSE CASE WHEN (RTRIM(LTRIM(cTelefone1)) = '') THEN '' 0                     ELSE cTelefone1 + ' TEL / '                 END6        END) + (CASE WHEN (cTelefone2 IS NULL) THEN ''J                    ELSE CASE WHEN (RTRIM(LTRIM(cTelefone2)) = '') THEN ''8                             ELSE cTelefone2 + ' TEL / '                         ENDB                END) + (CASE WHEN (cTelefoneMovel IS NULL) THEN ''V                            ELSE CASE WHEN (RTRIM(LTRIM(cTelefoneMovel)) = '') THEN ''B                                     ELSE cTelefoneMovel + ' CEL '$                                 END*                        END) AS cTelefonesO      ,convert(varchar, dDtPedido, 103) + ' - ' + cNmUsuarioPed    AS dDtPedidoR      ,convert(varchar, dDtDevolucao, 103) + ' - ' + cNmUsuarioDev AS dDtDevolucao[      ,convert(varchar, dDtCancelamento, 103) + ' - ' + cNmUsuarioCancel AS dDtCancelamento[      ,convert(varchar, dDtFinalizacao, 103) + ' - ' + cNmUsuarioFinaliza AS dDtFinalizacao      ,cTipoStatusConsig  FROM #TempConsignacao Left�Top TIntegerFieldqryConsignacaonCdConsignacao	FieldNamenCdConsignacao  TStringFieldqryConsignacaocNmEmpresa	FieldName
cNmEmpresaSize2  TStringFieldqryConsignacaocNmLoja	FieldNamecNmLojaSize2  TStringFieldqryConsignacaocNmVendedor	FieldNamecNmVendedorSize2  TIntegerFieldqryConsignacaonCdPedido	FieldName	nCdPedido  TStringFieldqryConsignacaocNmTerceiro	FieldNamecNmTerceiroSized  TStringFieldqryConsignacaocRG	FieldNamecRGSize  TStringFieldqryConsignacaocCNPJCPF	FieldNamecCNPJCPFSize  TStringFieldqryConsignacaocEnderecoNumComp	FieldNamecEnderecoNumCompReadOnly	Sizex  TStringFieldqryConsignacaocCepBairroCidade	FieldNamecCepBairroCidadeReadOnly	SizeY  TStringFieldqryConsignacaocTelefones	FieldName
cTelefonesReadOnly	SizeO  TStringFieldqryConsignacaodDtPedido	FieldName	dDtPedidoReadOnly	SizeS  TStringFieldqryConsignacaodDtDevolucao	FieldNamedDtDevolucaoReadOnly	SizeS  TStringFieldqryConsignacaodDtCancelamento	FieldNamedDtCancelamentoReadOnly	SizeS  TStringFieldqryConsignacaodDtFinalizacao	FieldNamedDtFinalizacaoReadOnly	SizeS  TStringFieldqryConsignacaocTipoStatusConsig	FieldNamecTipoStatusConsigSize2   	TADOQueryqryItemConsignacao
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT nCdItemConsig      ,nCdConsignacao      ,nCdProduto      ,cNmProduto      ,cReferencia      ,nQtdePedido      ,nQtdeDevolvida      ,nQtdeRetirada      ,nValUnitario      ,nValTotal  FROM #TempItemConsignacao Left�Top TIntegerFieldqryItemConsignacaonCdItemConsig	FieldNamenCdItemConsig  TIntegerField qryItemConsignacaonCdConsignacao	FieldNamenCdConsignacao  TIntegerFieldqryItemConsignacaonCdProduto	FieldName
nCdProduto  TStringFieldqryItemConsignacaocNmProduto	FieldName
cNmProdutoSize�   TStringFieldqryItemConsignacaocReferencia	FieldNamecReferencia	FixedChar	Size  TIntegerFieldqryItemConsignacaonQtdePedido	FieldNamenQtdePedido  TIntegerField qryItemConsignacaonQtdeDevolvida	FieldNamenQtdeDevolvida  TIntegerFieldqryItemConsignacaonQtdeRetirada	FieldNamenQtdeRetirada  	TBCDFieldqryItemConsignacaonValUnitario	FieldNamenValUnitario	PrecisionSize  	TBCDFieldqryItemConsignacaonValTotal	FieldName	nValTotal	PrecisionSize   TADOCommandcmdPreparaTempCommandText�  IF (OBJECT_ID('tempdb..#TempConsignacao') IS NULL)
BEGIN

    CREATE TABLE #TempConsignacao(nCdConsignacao       int
                                 ,nCdEmpresa           int
                                 ,cNmEmpresa           varchar(50)
                                 ,nCdLoja              int
                                 ,cNmLoja              varchar(50)
                                 ,nCdPedido            int
                                 ,nCdTerceiro          int
                                 ,cNmTerceiro          varchar(100)
                                 ,cRG                  varchar(15)
                                 ,cCNPJCPF             varchar(14)
                                 ,nCdEndereco          int
                                 ,cEndereco            varchar(50)
                                 ,iNumero              int
                                 ,cComplemento         varchar(35)
                                 ,cCEP                 char(8)
                                 ,cBairro              varchar(35)
                                 ,cCidade              varchar(35)
                                 ,cUF                  char(2)
                                 ,cTelefone1           varchar(20)
                                 ,cTelefone2           varchar(20)
                                 ,cTelefoneMovel       varchar(20)
                                 ,nCdVendedor          int
                                 ,cNmVendedor          varchar(50)           
                                 ,nCdUsuarioPed        int
                                 ,cNmUsuarioPed        varchar(50)
                                 ,nCdUsuarioDev        int
                                 ,cNmUsuarioDev        varchar(50)
                                 ,nCdUsuarioFinaliza   int
                                 ,cNmUsuarioFinaliza   varchar(50)
                                 ,nCdUsuarioCancel     int
                                 ,cNmUsuarioCancel     varchar(50)
                                 ,dDtPedido            datetime
                                 ,dDtDevolucao         datetime
                                 ,dDtCancelamento      datetime
                                 ,dDtFinalizacao       datetime
                                 ,nCdStatusConsig      int
                                 ,cTipoStatusConsig    varchar(50))
END

TRUNCATE TABLE #TempConsignacao

IF (OBJECT_ID('tempdb..#TempItemConsignacao') IS NULL)
BEGIN

    CREATE TABLE #TempItemConsignacao(nCdItemConsig  int
                                     ,nCdConsignacao int
                                     ,nCdProduto     int
                                     ,cNmProduto     varchar(150)
                                     ,cReferencia    char(30)
                                     ,nQtdePedido    int
                                     ,nQtdeDevolvida int
                                     ,nQtdeRetirada  int
                                     ,nValUnitario   decimal(14,6)
                                     ,nValTotal      decimal(12,2))

END

TRUNCATE TABLE #TempItemConsignacao

ConnectionfrmMenu.Connection
Parameters Left�Top  TADOStoredProcSPREL_CONSIGNACAO
ConnectionfrmMenu.ConnectionProcedureNameSPREL_CONSIGNACAO;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@nCdConsignacao
Attributes
paNullable DataType	ftInteger	Precision
Value   Left�Top  	TADOQueryqryTotal
ConnectionfrmMenu.Connection
Parameters SQL.Strings'SELECT Sum(nValTotal) AS nValTotalItens  FROM #TempItemConsignacao   LeftxTop  	TADOQueryqryTotalPedido
ConnectionfrmMenu.Connection
Parameters SQL.Strings)SELECT Sum(nQtdePedido) AS nQtdePedidoTot  FROM #TempItemConsignacao LeftTop  	TADOQueryqryTotalDevolvida
ConnectionfrmMenu.Connection
Parameters SQL.Strings/SELECT Sum(nQtdeDevolvida) AS nQtdeDevolvidaTot  FROM #TempItemConsignacao Left8Top  	TADOQueryqryTotalRetirada
ConnectionfrmMenu.Connection
Parameters SQL.Strings-SELECT Sum(nQtdeRetirada) AS nQtdeRetiradaTot  FROM #TempItemConsignacao LeftXTop   