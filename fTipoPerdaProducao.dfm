inherited frmTipoPerdaProducao: TfrmTipoPerdaProducao
  Left = 36
  Top = 103
  Caption = 'Tipo Perda Produ'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [1]
    Left = 17
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
  end
  object Label1: TLabel [2]
    Left = 28
    Top = 37
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object DBEdit2: TDBEdit [4]
    Left = 73
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTipoPerdaProducao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBCheckBox1: TDBCheckBox [5]
    Left = 73
    Top = 82
    Width = 97
    Height = 17
    Caption = 'Gerar Sucata'
    DataField = 'cFlgGerarSucata'
    DataSource = dsMaster
    TabOrder = 2
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 73
    Top = 31
    Width = 65
    Height = 19
    DataField = 'nCdTipoPerdaProducao'
    DataSource = dsMaster
    TabOrder = 3
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoPerdaProducao'
      'WHERE nCdTipoPerdaProducao = :nPk'
      '')
    object qryMasternCdTipoPerdaProducao: TIntegerField
      FieldName = 'nCdTipoPerdaProducao'
    end
    object qryMastercNmTipoPerdaProducao: TStringField
      FieldName = 'cNmTipoPerdaProducao'
      Size = 50
    end
    object qryMastercFlgGerarSucata: TIntegerField
      FieldName = 'cFlgGerarSucata'
    end
  end
  inherited dsMaster: TDataSource
    Left = 544
  end
end
