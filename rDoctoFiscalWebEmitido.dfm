inherited rptDoctoFiscalWebEmitido: TrptDoctoFiscalWebEmitido
  Left = 287
  Top = 187
  Width = 745
  Height = 360
  Caption = 'Documento Fiscal Web Emitido'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 729
    Height = 298
  end
  object Label1: TLabel [1]
    Left = 61
    Top = 45
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 82
    Top = 69
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 44
    Top = 93
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Destinat'#225'rio'
    FocusControl = DBEdit3
  end
  object Label6: TLabel [4]
    Left = 10
    Top = 142
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Emiss'#227'o'
  end
  object Label7: TLabel [5]
    Left = 188
    Top = 142
    Width = 9
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = #224
  end
  object Label4: TLabel [6]
    Left = 27
    Top = 117
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transportadora'
    FocusControl = DBEdit4
  end
  inherited ToolBar1: TToolBar
    Width = 729
    TabOrder = 11
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TMaskEdit [8]
    Left = 105
    Top = 37
    Width = 65
    Height = 21
    TabOrder = 0
    OnExit = edtEmpresaExit
    OnKeyDown = edtEmpresaKeyDown
  end
  object edtLoja: TMaskEdit [9]
    Left = 105
    Top = 61
    Width = 65
    Height = 21
    TabOrder = 2
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtTerceiro: TMaskEdit [10]
    Left = 105
    Top = 85
    Width = 65
    Height = 21
    TabOrder = 4
    OnExit = edtTerceiroExit
    OnKeyDown = edtTerceiroKeyDown
  end
  object edtDataInicial: TMaskEdit [11]
    Left = 105
    Top = 134
    Width = 62
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object edtDataFinal: TMaskEdit [12]
    Left = 209
    Top = 134
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 173
    Top = 37
    Width = 549
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [14]
    Tag = 1
    Left = 173
    Top = 61
    Width = 549
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 3
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 173
    Top = 85
    Width = 549
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 5
  end
  object rdgSituacao: TRadioGroup [16]
    Left = 104
    Top = 166
    Width = 233
    Height = 33
    Caption = ' Situa'#231#227'o '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Ativo'
      'Cancelado')
    TabOrder = 10
  end
  object edtTransp: TMaskEdit [17]
    Left = 105
    Top = 109
    Width = 65
    Height = 21
    TabOrder = 6
    OnExit = edtTranspExit
    OnKeyDown = edtTranspKeyDown
  end
  object DBEdit4: TDBEdit [18]
    Tag = 1
    Left = 173
    Top = 109
    Width = 549
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiroTransp
    TabOrder = 7
  end
  inherited ImageList1: TImageList
    Left = 232
    Top = 256
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '              ,cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa =:nPK')
    Left = 112
    Top = 256
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '             ,cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja =:nPK')
    Left = 136
    Top = 256
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '              ,cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro =:nPK')
    Left = 168
    Top = 256
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 104
    Top = 288
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 136
    Top = 288
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 168
    Top = 288
  end
  object qryTerceiroTransp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '  FROM Terceiro'
      
        '       INNER JOIN TerceiroTipoTerceiro TTT ON TTT.nCdTerceiro = ' +
        'Terceiro.nCdTerceiro'
      ' WHERE Terceiro.nCdTerceiro = :nPK'
      '   AND TTT.nCdTipoTerceiro  = 3')
    Left = 199
    Top = 256
    object qryTerceiroTranspnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroTranspcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsTerceiroTransp: TDataSource
    DataSet = qryTerceiroTransp
    Left = 200
    Top = 288
  end
end
