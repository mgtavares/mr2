unit fTipoPerdaProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoPerdaProducao = class(TfrmCadastro_Padrao)
    Label2: TLabel;
    qryMasternCdTipoPerdaProducao: TIntegerField;
    qryMastercNmTipoPerdaProducao: TStringField;
    qryMastercFlgGerarSucata: TIntegerField;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoPerdaProducao: TfrmTipoPerdaProducao;

implementation

{$R *.dfm}

procedure TfrmTipoPerdaProducao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
end;

procedure TfrmTipoPerdaProducao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOPERDAPRODUCAO' ;
  nCdTabelaSistema  := 421 ;
  nCdConsultaPadrao := 1014 ;

end;

procedure TfrmTipoPerdaProducao.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do tipo de perda de produ��o.') ;
      DBedit2.SetFocus;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(TfrmTipoPerdaProducao) ;

end.
