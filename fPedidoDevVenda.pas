unit fPedidoDevVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StrUtils, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBCtrlsEh, DBLookupEh, Menus, DBGridEhGrouping, fTransfEst_Itens,
  ToolCtrlsEh, ACBrNFe, pcnConversao, ACBrBase, ACBrDFe;

type
  TfrmPedidoDevVenda = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    dsEmpresa: TDataSource;
    DBEdit13: TDBEdit;
    dsLoja: TDataSource;
    DBEdit14: TDBEdit;
    dsTipoPedido: TDataSource;
    DBEdit15: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    dsStatusPed: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    dsTerceiroTransp: TDataSource;
    dsEstoque: TDataSource;
    dsIcoterms: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit36: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    qryItemEstoque: TADOQuery;
    dsItemEstoque: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    ToolButton12: TToolButton;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    dsDadoAutorz: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    usp_gera_item_embalagem: TADOStoredProc;
    qryMastercOBSFinanc: TMemoField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    usp_copia_cc: TADOStoredProc;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    ToolButton13: TToolButton;
    PopupMenu1: TPopupMenu;
    btExibirAtendItem: TMenuItem;
    qryMasternCdUsuarioComprador: TIntegerField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBEdit23: TDBEdit;
    qryLojanCdLocalEstoquePadrao: TIntegerField;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryLocalEstoqueSaida: TADOQuery;
    qryLocalEstoqueSaidanCdLocalEstoque: TIntegerField;
    qryLocalEstoqueSaidacNmLocalEstoque: TStringField;
    dsLocalEstoqueSaida: TDataSource;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryUltRecebItem: TADOQuery;
    qryUltRecebItemnCdItemRecebimento: TAutoIncField;
    qryUltRecebItemnPercIPI: TBCDField;
    tabFaturamento: TcxTabSheet;
    Image2: TImage;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    DBEdit2: TDBEdit;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    qryDadosReceb: TADOQuery;
    qryDadosRecebcNrDocto: TStringField;
    cxButton4: TcxButton;
    qryUltRecebItemnValUnitario: TFloatField;
    Label8: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit41: TDBEdit;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit18: TDBEdit;
    dsTerceiroPagador: TDataSource;
    qryLocalEstoqueSaidanCdLoja: TIntegerField;
    qryProdutonValCusto: TBCDField;
    qryGrupoImposto: TADOQuery;
    qryCFOP: TADOQuery;
    qryCFOPnCdCFOP: TIntegerField;
    qryCFOPcCFOP: TStringField;
    qryCFOPcNmCFOP: TStringField;
    qryCFOPnCdTipoICMS: TIntegerField;
    qryCFOPnCdTipoIPI: TIntegerField;
    qryCST_ICMS: TADOQuery;
    qryCST_ICMSnCdTipoTributacaoICMS: TAutoIncField;
    qryCST_ICMScCdST: TStringField;
    qryCST_ICMScNmTipoTributacaoICMS: TStringField;
    qryTipoPedidonCdNaturezaOperacao: TIntegerField;
    qryNaturezaOperacao: TADOQuery;
    qryNaturezaOperacaocCFOPInterno: TStringField;
    qryNaturezaOperacaocCFOPExterno: TStringField;
    qryEndereco: TADOQuery;
    qryRegraICMS: TADOQuery;
    qryUFEmissao: TADOQuery;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryUfDestino: TADOQuery;
    qryUFEmissaonCdEndereco: TIntegerField;
    qryUFEmissaonCdTerceiro: TIntegerField;
    qryUFEmissaocUF: TStringField;
    qryUfDestinonCdEndereco: TIntegerField;
    qryUfDestinonCdTerceiro: TIntegerField;
    qryUfDestinocUF: TStringField;
    qryRegraICMSnPercIva: TBCDField;
    qryRegraICMSnPercBCIva: TBCDField;
    qryRegraICMSnAliqICMS: TBCDField;
    qryRegraICMSnAliqICMSInterna: TBCDField;
    qryRegraICMSnCdTipoTributacaoICMS: TStringField;
    qryRegraICMSnPercBC: TBCDField;
    qryItemEstoquenCdItemPedido: TIntegerField;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdItemPedidoPai: TIntegerField;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecCdProduto: TStringField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryItemEstoquenCdEstoqueMov: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TFloatField;
    qryItemEstoquenQtdeExpRec: TFloatField;
    qryItemEstoquenQtdeCanc: TFloatField;
    qryItemEstoquenValUnitario: TFloatField;
    qryItemEstoquenPercIPI: TFloatField;
    qryItemEstoquenValIPI: TFloatField;
    qryItemEstoquenValDesconto: TFloatField;
    qryItemEstoquenValCustoUnit: TFloatField;
    qryItemEstoquenValSugVenda: TFloatField;
    qryItemEstoquenValTotalItem: TFloatField;
    qryItemEstoquenValAcrescimo: TFloatField;
    qryItemEstoquecSiglaUnidadeMedida: TStringField;
    qryItemEstoqueiColuna: TIntegerField;
    qryItemEstoquenCdTabStatusItemPed: TIntegerField;
    qryItemEstoquenQtdeLibFat: TFloatField;
    qryItemEstoquenValUnitarioEsp: TFloatField;
    qryItemEstoquenCdGrupoImposto: TIntegerField;
    qryItemEstoquecFlgPromocional: TIntegerField;
    qryItemEstoquenCdTabPreco: TIntegerField;
    qryItemEstoquenCdLoja: TIntegerField;
    qryItemEstoquecFlgTrocado: TIntegerField;
    qryItemEstoquenCdItemPedidoTroca: TIntegerField;
    qryItemEstoquenCdTerceiroColab: TIntegerField;
    qryItemEstoquedDtPedidoItem: TDateTimeField;
    qryItemEstoquenCdVale: TIntegerField;
    qryItemEstoquecDescricaoTecnica: TMemoField;
    qryItemEstoquenDimensao: TFloatField;
    qryItemEstoquenAltura: TFloatField;
    qryItemEstoquenLargura: TFloatField;
    qryItemEstoquedDtEntregaIni: TDateTimeField;
    qryItemEstoquedDtEntregaFim: TDateTimeField;
    qryItemEstoquenQtdePrev: TFloatField;
    qryItemEstoquenPercentCompra: TFloatField;
    qryItemEstoquenPercICMSSub: TFloatField;
    qryItemEstoquenValICMSSub: TFloatField;
    qryItemEstoquecFlgComplemento: TIntegerField;
    qryItemEstoquenPercDescontoItem: TFloatField;
    qryItemEstoquenValDescAbatUnit: TFloatField;
    qryItemEstoquecFlgDefeito: TIntegerField;
    qryItemEstoquenCdItemRecebimentoDev: TIntegerField;
    qryItemEstoquenCdCampanhaPromoc: TIntegerField;
    qryItemEstoquenValCMV: TFloatField;
    qryItemEstoquenCdServidorOrigem: TIntegerField;
    qryItemEstoquedDtReplicacao: TDateTimeField;
    qryItemEstoquenFatorConvUnidadeEstoque: TFloatField;
    qryItemEstoquenCdMotivoCancSaldoPed: TIntegerField;
    qryItemEstoquedDtCancelSaldoItemPed: TDateTimeField;
    qryItemEstoquenCdUsuarioCancelSaldoItemPed: TIntegerField;
    qryItemEstoquenValFrete: TFloatField;
    qryItemEstoquenValSeguro: TFloatField;
    qryItemEstoquenValAcessorias: TFloatField;
    qryItemEstoquenPercRedBaseICMS: TFloatField;
    qryItemEstoquenValBaseICMS: TFloatField;
    qryItemEstoquenPercAliqICMS: TFloatField;
    qryItemEstoquenValICMS: TFloatField;
    qryItemEstoquenPercIVA: TFloatField;
    qryItemEstoquenPercAliqICMSInt: TFloatField;
    qryItemEstoquenValBaseICMSSub: TFloatField;
    qryItemEstoquenPercIncFiscal: TFloatField;
    qryItemEstoquecNCM: TStringField;
    qryItemEstoquecCdSTIPI: TStringField;
    qryItemEstoquecCFOPItemPedido: TStringField;
    qryItemEstoquecDoctoCompra: TStringField;
    GroupBox1: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    Label23: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    btConsultarDocSaida: TcxButton;
    qryItemEstoquenPercBaseCalcIPI: TFloatField;
    qryItemEstoquenValBaseIPI: TFloatField;
    qryUnidadeMedida: TADOQuery;
    qryUnidadeMedidanCdUnidadeMedida: TAutoIncField;
    qryGrupoImpostocNCM: TStringField;
    qryItemEstoquecCSTPIS: TStringField;
    qryItemEstoquenValBasePIS: TFloatField;
    qryItemEstoquenAliqPIS: TFloatField;
    qryItemEstoquenValPIS: TFloatField;
    qryItemEstoquecCSTCOFINS: TStringField;
    qryItemEstoquenValBaseCOFINS: TFloatField;
    qryItemEstoquenAliqCOFINS: TFloatField;
    qryItemEstoquenValCOFINS: TFloatField;
    qryCST_IPI: TADOQuery;
    qryCST_IPInCdTipoTributacaoIPI: TIntegerField;
    qryCST_IPIcCdStIPI: TStringField;
    qryCST_IPIcNmTipoTributacaoIPI: TStringField;
    qryCST_PISCOFINS: TADOQuery;
    qryCST_PISCOFINSnCdTipoTributacaoPISCOFINS: TIntegerField;
    qryCST_PISCOFINScCdStPISCOFINS: TStringField;
    qryCST_PISCOFINScNmTipoTributacaoPISCOFINS: TStringField;
    DBEdit21: TDBEdit;
    Label27: TLabel;
    qryEnderecoEntrega: TADOQuery;
    qryEnderecoEntreganCdEndereco: TAutoIncField;
    qryEnderecoEntregacNmstatus: TStringField;
    qryEnderecoEntregacNmTipoEnd: TStringField;
    qryEnderecoEntregacEnderecoCompleto: TStringField;
    DBEdit22: TDBEdit;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    qryMasternValDevoluc: TBCDField;
    qryMasternValValePres: TBCDField;
    qryMasternValValeMerc: TBCDField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdMapaCompra: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercAnotacao: TMemoField;
    qryMastercFlgOrcamento: TIntegerField;
    qryMastercOrcamentoFin: TIntegerField;
    qryMasternCdPedidoOrcamento: TIntegerField;
    qryMasternValAdiantamento: TBCDField;
    qryMastercFlgAdiantConfirm: TIntegerField;
    qryMasternCdUsuarioConfirmAdiant: TIntegerField;
    qryMasternValAdiantUsado: TBCDField;
    qryMastercFlgDevPag: TIntegerField;
    qryMastercFlgMovEstoque: TIntegerField;
    qryMasternCdGrupoEconomico: TIntegerField;
    qryMasterdDtPrevEntIniOriginal: TDateTimeField;
    qryMasterdDtPrevEntFimOriginal: TDateTimeField;
    qryMastercFlgLibParcial: TIntegerField;
    qryMasterdDtIntegracao: TDateTimeField;
    qryMasternValDescontoCondPagto: TBCDField;
    qryMasternValJurosCondPagto: TBCDField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgReplicado: TIntegerField;
    qryMasternCdEnderecoEntrega: TIntegerField;
    qryMasternCdAreaVenda: TIntegerField;
    qryMasternCdDivisaoVenda: TIntegerField;
    qryMasternCdCanalDistribuicao: TIntegerField;
    qrySugereEnderecoEntrega: TADOQuery;
    qrySugereEnderecoEntreganCdEndereco: TIntegerField;
    qryItemEstoquecCdSTICMS: TStringField;
    dsEnderecoEntrega: TDataSource;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    qryGrupoPedido: TADOQuery;
    qryGrupoPedidocFlgFaturar: TIntegerField;
    qryTipoPedidonCdGrupoPedido: TIntegerField;
    Label12: TLabel;
    qryProdutonValVenda: TBCDField;
    DBEdit25: TDBEdit;
    Label14: TLabel;
    btLerArquivoNFe: TcxButton;
    qryTerceironCdTabTipoEnquadTributario: TIntegerField;
    OpenDialog: TOpenDialog;
    qryNatOperacaoVinculo: TADOQuery;
    qryNatOperacaoVinculocCFOP: TStringField;
    qryTerceiroEmitente: TADOQuery;
    qryTerceiroEmitentenCdTerceiro: TIntegerField;
    qryTerceiroEmitentenCdTabTipoEnquadTributario: TIntegerField;
    qryTerceiroEmitentenCdEstado: TIntegerField;
    dsTerceiroEmitente: TDataSource;
    qryTabTipoModFrete: TADOQuery;
    qryTabTipoModFretenCdTabTipoModFrete: TIntegerField;
    qryTabTipoModFretecNmTabTipoModFrete: TStringField;
    dsTabTipoModFrete: TDataSource;
    qryMasternCdTabTipoModFrete: TIntegerField;
    Label11: TLabel;
    DBEdit26: TDBEdit;
    DBEdit24: TDBEdit;
    tabParcela: TcxTabSheet;
    Image3: TImage;
    DBGridEh3: TDBGridEh;
    btSugParcela: TcxButton;
    qryPrazoPedido: TADOQuery;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidonValPagto: TBCDField;
    qryPrazoPedidonValDif: TBCDField;
    dsPrazoPedido: TDataSource;
    usp_Sugere_Parcela: TADOStoredProc;
    qryPrazoPedidoDif: TADOQuery;
    DBEdit20: TDBEdit;
    Label15: TLabel;
    DBEdit27: TDBEdit;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    dsCondPagto: TDataSource;
    ACBrNFe: TACBrNFe;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure ToolButton13Click(Sender: TObject);
    procedure btExibirAtendItemClick(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure DBEdit9Enter(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConsultarDocSaidaClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure DBEdit41KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit41Exit(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10Exit(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure CompletaDadosFaturamento();
    function RightStr(Const Str: String; Size: Word): String;
    procedure qryItemEstoquecCdProdutoChange(Sender: TField);
    procedure verificaControlesICMS(cCST: string);
    procedure qryItemEstoqueAfterScroll(DataSet: TDataSet);
    procedure qryItemEstoquecCdSTICMSChange(Sender: TField);
    procedure calculaImpostosItem( bGravar : boolean ) ;
    procedure ativaEdicaoProduto( bPermitirEditar : boolean ) ;
    procedure DBEdit21Enter(Sender: TObject);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21Exit(Sender: TObject);
    procedure qryTipoPedidoAfterOpen(DataSet: TDataSet);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure btLerArquivoNFeClick(Sender: TObject);
    procedure DBEdit20Exit(Sender: TObject);
    procedure DBEdit26KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit26Exit(Sender: TObject);
    procedure btSugParcelaClick(Sender: TObject);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
  private
    objForm : TfrmTransfEst_Itens;
    { Private declarations }

  public
    { Public declarations }
    bFaturaDev : Boolean;
  end;

var
  frmPedidoDevVenda: TfrmPedidoDevVenda;

implementation

uses Math, fMenu, fLookup_Padrao, rPedidoCom_Simples, fEmbFormula, fPrecoEspPedCom,
  fItemPedidoCompraAtendido, fPedidoComercial_ProgEntrega, rPedCom_Contrato,
  fItemPedidoAtendido, rPedidoDevCompra, fPedidoDevVenda_ConsultaItens,
  fPedidoDevVenda_LeituraXML, pcnNFe;

{$R *.dfm}

procedure TfrmPedidoDevVenda.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedidoDevVenda.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 143 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmPedidoDevVenda.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit1.SetFocus;
  cxPageControl1.ActivePageIndex := 0;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryMasterdDtPedido.Value := Date;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      ShowMessage('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
  end ;

  qryUsuarioTipoPedido.Close ;

  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else begin
      DBEdit3.Enabled := False ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else begin
      DbEdit4.OnExit(nil) ;
      DBEdit5.SetFocus ;
  end ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly   := False ;

  if (DbEdit3.Text = '') and DbEdit3.Enabled then
      DBEdit3.SetFocus ;

  qryMasternCdTabTipoModFrete.Value := 0;

end;

procedure TfrmPedidoDevVenda.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;
  qryTabTipoModFrete.Close;
end;

procedure TfrmPedidoDevVenda.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(142,'EXISTS (SELECT 1'
                                                       + '          FROM UsuarioTipoPedido UTP'
                                                       + '         WHERE UTP.nCdTipoPedido = TipoPedido.nCdTipoPedido'
                                                       + '           AND UTP.nCdUsuario    =  ' + IntToStr(frmMenu.nCdUsuarioLogado) + ')');

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                ShowMessage('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(61,'cFlgPdv = 0');

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(61,'cFlgPdv = 0');

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if not qryTipoPedido.Active then
  begin
      ShowMessage('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end ;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdTerceiroPagador.Value > 0) then
      begin
          qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
          PosicionaQuery(qryTerceiroPagador, qryTerceironCdTerceiroPagador.asString) ;
      end
      else
      begin
          qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiro.Value ;
          PosicionaQuery(qryTerceiroPagador, qryTerceironCdTerceiro.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(62);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoDevVenda.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.TabVisible := True ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
    begin
        TabItemEstoque.TabVisible := True ;
        TabItemEstoque.Enabled    := True ;
    end ;

    if (bFaturaDev) then
        TabFaturamento.Enabled := True ;

    if (qryTipoPedidocGerarFinanc.Value = 1) then
    begin
        tabParcela.Enabled := True ;
    end ;

  end ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoDevVenda.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;

  if not qryLoja.eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;

end;

procedure TfrmPedidoDevVenda.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly   := False ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      ShowMessage('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      ShowMessage('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;

  qryTerceiroEmitente.Close;
  qryTerceiroEmitente.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value;
  qryTerceiroEmitente.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value;
  qryTerceiroEmitente.Open;

  if not qryLoja.Eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryEstoque, qryMasternCdEstoqueMov.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryTerceiroTransp,qryMasternCdTerceiroTransp.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroPagador, qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;

  qryLocalEstoqueSaida.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryLocalEstoqueSaida, qryMasternCdEstoqueMov.AsString) ;

  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString) ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly := True;
  end ;

  TabItemEstoque.Enabled := False ;

  TabItemEstoque.TabStop := False ;
  TabFaturamento.TabStop := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
  begin
      TabItemEstoque.TabVisible := True ;
      TabItemEstoque.Enabled    := True ;
  end ;

  if (bFaturaDev) then
      TabFaturamento.Enabled := True ;

  qryEnderecoEntrega.Close;
  qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      tabParcela.Enabled := True ;
  end ;

  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmPedidoDevVenda.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryDadoAutorz.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryPrazoPedido.Close ;

  qryLocalEstoqueSaida.Close ;
  qryEnderecoEntrega.Close;

  cxPageControl1.ActivePageIndex := 0 ;

  TabItemEstoque.Enabled := False ;
  TabFaturamento.Enabled := False ;
  TabParcela.Enabled     := False ;

  TabItemEstoque.TabStop := False ;
  TabFaturamento.TabStop := False ;
end;

procedure TfrmPedidoDevVenda.qryItemEstoqueBeforePost(DataSet: TDataSet);
var
  iOrigem : Integer;
  iCST    : Integer;
  strCST  : TStringList;
begin

  qryItemEstoquecCdProduto.Value          := Uppercase( trim( qryItemEstoquecCdProduto.Value ) ) ;
  qryItemEstoquecSiglaUnidadeMedida.Value := Uppercase( trim( qryItemEstoquecSiglaUnidadeMedida.Value ) ) ;
  qryItemEstoquecCdStICMS.Value           := StringReplace(qryItemEstoquecCdStICMS.Value,' ','',[rfReplaceAll]);

  if ( trim( qryItemEstoquecCdProduto.Value ) = '' ) then
  begin
      MensagemAlerta('Informe o c�digo do produto. Informe "AD" para itens n�o cadastrados.') ;
      DBGridEh1.SelectedField := qryItemEstoquecCdProduto;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquecCdProduto.Value <> 'AD') then
  begin

      try
          strToInt( qryItemEstoquecCdProduto.Value ) ;
      except
          MensagemAlerta('C�digo de produto inv�lido. Informe "AD" para itens n�o cadastrados.') ;
          DBGridEh1.SelectedField := qryItemEstoquecCdProduto;
          DBGridEh1.SetFocus;
          Abort ;
      end ;

      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.Value;
      PosicionaQuery( qryProduto , qryItemEstoquecCdProduto.Value ) ;

      if ( qryProduto.eof ) then
      begin
          MensagemAlerta('C�digo de produto n�o cadastrado ou sem permiss�o de uso neste tipo de pedido.') ;
          DBGridEh1.SelectedField := qryItemEstoquecCdProduto;
          DBGridEh1.SetFocus;
          Abort;
      end ;

      if (qryItemEstoque.State = dsInsert) then
      begin
          // Valida a duplicidade de itens no pedido
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Append('SELECT 1');
          qryAux.SQL.Append('  FROM ItemPedido');
          qryAux.SQL.Append(' WHERE nCdPedido  = ' + qryMasternCdPedido.AsString);
          qryAux.SQL.Append('   AND nCdProduto = ' + qryProdutonCdProduto.AsString);
          qryAux.Open;

          if not (qryAux.IsEmpty) then
          begin
              MensagemAlerta('O Produto ' + qryProdutocNmProduto.Value + ' j� consta como item deste pedido.');
              DBGridEh1.SelectedField := qryItemEstoquecCdProduto;
              DBGridEh1.SetFocus;
              Abort;
          end;
      end;
  end ;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do produto.') ;
      DBGridEh1.SelectedField := qryItemEstoquecNmItem;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquecSiglaUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida.') ;
      DBGridEh1.SelectedField := qryItemEstoquecSiglaUnidadeMedida;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  qryUnidadeMedida.Close;
  PosicionaQuery( qryUnidadeMedida , qryItemEstoquecSiglaUnidadeMedida.Value ) ;

  if (qryUnidadeMedida.Eof) then
  begin
      MensagemAlerta('Unidade de Medida n�o cadastrada.');
      DBGridEh1.SelectedField := qryItemEstoquecSiglaUnidadeMedida;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade devolvida.') ;
      DBGridEh1.SelectedField := qryItemEstoquenQtdePed;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do produto.');
      DBGridEh1.SelectedField := qryItemEstoquenValUnitario;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  {
  if ((qryItemEstoquenPercDescontoItem.Value < 0) or (qryItemEstoquenPercDescontoItem.Value >= 100)) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenPercDescontoItem;
      DBGridEh1.SetFocus;
      Abort;
  end ;
  }

  if ((qryItemEstoquenValDesconto.Value < 0) or (qryItemEstoquenValDesconto.Value > qryItemEstoquenValUnitario.Value)) then
  begin
      MensagemAlerta('Valor de desconto inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenValDesconto;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValFrete.Value < 0) then
  begin
      MensagemAlerta('Valor de frete inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenValFrete;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValAcessorias.Value < 0) then
  begin
      MensagemAlerta('Valor de outros inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenValAcessorias;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenValSeguro.Value < 0) then
  begin
      MensagemAlerta('Valor do seguro inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenValSeguro;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  // Calcula o Desconto Manual do Item
  if (qryItemEstoquenValDesconto.Value > 0) then
      //qryItemEstoquenPercDescontoItem.Value := frmMenu.TBRound((((qryItemEstoquenValDesconto.Value / qryItemEstoquenQtdePed.Value) / qryItemEstoquenValUnitario.Value) * 100),2)
  // Calcula o Desconto Percentual do Item
  else
      qryItemEstoquenValDesconto.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercDescontoItem.Value/100)) * qryItemEstoquenQtdePed.Value),2);

  if ( bFaturaDev ) then
  begin

      if     (length(Trim(qryItemEstoquecNCM.Value)) <> 2)
         and (length(Trim(qryItemEstoquecNCM.Value)) <> 8) then
      begin
          MensagemAlerta('NCM n�o informado ou inv�lido.') ;
          DBGridEh1.SelectedField := qryItemEstoquecNCM;
          DBGridEh1.SetFocus;
          Abort;
      end;

     { -- valida NCM -- }
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TabIBPT WHERE cNCM = ' + #39 + qryItemEstoquecNCM.Value + #39);
      qryAux.Open;

      if (qryAux.IsEmpty) then
      begin
          MensagemAlerta('NCM ' + qryItemEstoquecNCM.Value + ' n�o cadastrado.');
          DBGridEh1.SelectedField := qryItemEstoquecNCM;
          DBGridEh1.SetFocus;
          Abort;
      end;

      if (length(Trim(qryItemEstoquecCdSTICMS.Value)) < 3) then
      begin
          MensagemAlerta('Informe o c�digo CST ou CSOSN do item. (Origem da Mercadoria e C�digo CST/CSOSN) Ex. 000.') ;
          DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
          DBGridEh1.SetFocus;
          Abort;
      end;

      { -- se terceiro � optante do simples nacional valida CSOSN de 4 d�gitos -- }
      if (qryTerceiroEmitentenCdTabTipoEnquadTributario.Value = 1) then
      begin
          if (Length(Trim(qryItemEstoquecCdSTICMS.Value)) < 4) then
          begin
              MensagemAlerta('O Terceiro emitente � Optante do Simples Nacional. Informe o CSOSN de 4 d�gitos v�lido.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;

          iOrigem := StrToInt(LeftStr(qryItemEstoquecCdSTICMS.Value, 1));
          iCST    := StrToInt(RightStr(qryItemEstoquecCdSTICMS.Value, 3));

          if ((iOrigem < 0) or (iOrigem > 8)) then
          begin
              MensagemAlerta('CST de origem inv�lida.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;

          { -- lista de CST's v�lidos -- }
          strCST := TStringList.Create;
          strCST.Add('101'); //tributada pelo simples nacional com permiss�o de cr�dito
          strCST.Add('102'); //tributada pelo simples nacional sem permiss�o de cr�dito
          strCST.Add('103'); //isen��o do ICMS no simples nacional para faixa de receita bruta
          strCST.Add('201'); //tributada pelo simples nacional com permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria
          strCST.Add('202'); //tributada pelo simples nacional sem permiss�o de cr�dito e com cobran�a do ICMS por substitui��o tribut�ria
          strCST.Add('203'); //isen��o do ICMS no simples nacional para faixa de receita bruta e com cobran�a do ICMS por substitui��o tribut�ria
          strCST.Add('300'); //imune
          strCST.Add('400'); //n�o tributada pelo simples nacional
          strCST.Add('500'); //ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o
          strCST.Add('900'); //outros

          if (strCST.IndexOf(IntToStr(iCST)) = -1) then
          begin
              MensagemAlerta('O CST informado � inv�lido.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;
      end
      { -- se terceiro n�o � optante do simples nacional valida CST de 3 d�gitos -- }
      else
      begin
          if (Length(Trim(qryItemEstoquecCdSTICMS.Value)) > 3) then
          begin
              MensagemAlerta('O Terceiro emitente n�o � Optante do Simples Nacional. Informe o CST de 3 d�gitos v�lido.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;
      end;

      if (Trim(qryItemEstoquecCFOPItemPedido.Value) = '') then
      begin
          MensagemAlerta('Informe o C�digo CFOP de devolu��o.');
          DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
          DBGridEh1.SetFocus;
          Abort;
      end;

      PosicionaQuery(qryCFOP,qryItemEstoquecCFOPItemPedido.AsString);

      if (qryCFOP.Eof) then
      begin
          MensagemAlerta('CFOP n�o cadastrado.');
          DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
          DBGridEh1.SetFocus;
          Abort;
      end;

      // Valida se o CFOP � de entrada
      if (strToInt(Copy(qryItemEstoquecCFOPItemPedido.Value,1,1)) > 3) then
      begin
          MensagemAlerta('O CFOP informado � um CFOP de sa�da. Informe um CFOP de entrada.');
          DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
          DBGridEh1.SetFocus;
          Abort;
      end;
  end;

  if (qryItemEstoquenPercIPI.Value < 0) then
  begin
      MensagemAlerta('Al�quota de IPI inv�lida.') ;
      DBGridEh1.SelectedField := qryItemEstoquenPercIPI;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  qryItemEstoquenCdPedido.Value := qryMasternCdPedido.Value ;

  { -- verifica classifica��o do item -- }
  if (qryProdutonCdGrade.Value > 0) then
      qryItemEstoquenCdTipoItemPed.Value := 1   //produto grade
  else
      qryItemEstoquenCdTipoItemPed.Value := 2 ; //produto ERP

  qryItemEstoquecCdProduto.Value := Uppercase(Trim(qryItemEstoquecCdProduto.Value)) ;

  if (qryItemEstoquecCdProduto.Value = 'AD') then
      qryItemEstoquenCdTipoItemPed.Value := 5 ;

  inherited;

  {-- chama a rotina que calcula os impostos do item --}
  if ( bFaturaDev ) then
      calculaImpostosItem( TRUE );

  if (qryItemEstoquenValFrete.IsNull) then
      qryItemEstoquenValFrete.Value := 0;

  qryItemEstoquecNmItem.Value       := Uppercase(qryItemEstoquecNmItem.Value);
  qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValFrete.Value + qryItemEstoquenValAcrescimo.Value,4);
  qryItemEstoquenValTotalItem.Value := qryItemEstoquenValCustoUnit.Value * qryItemEstoquenQtdePed.Value;

  if (qryItemEstoque.State = dsInsert) then
      qryItemEstoquenCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      // Atualiza os totais da devolu��o
      //qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloatDef(qryItemEstoquenValTotalItem.OldValue, 0);
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - (StrToFloatDef(qryItemEstoquenValUnitario.OldValue, 0) * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValFrete.Value    := qryMasternValFrete.Value    - (StrToFloatDef(qryItemEstoquenValFrete.OldValue, 0)    * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValDesconto.Value := qryMasternValDesconto.Value - (StrToFloatDef(qryItemEstoquenValDesconto.OldValue, 0) * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - (StrToFloatDef(qryItemEstoquenValIPI.OldValue, 0)      + StrToFloatDef(qryItemEstoquenValICMSSub.OldValue, 0));
      qryMasternValOutros.Value   := qryMasternValOutros.Value   - (StrToFloatDef(qryItemEstoquenValSeguro.OldValue, 0)   + StrToFloatDef(qryItemEstoquenValAcessorias.OldValue, 0));
  end ;

  // Atualiza os totais da devolu��o
  //qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemEstoquenValTotalItem.Value;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value + (qryItemEstoquenValUnitario.Value * qryItemEstoquenQtdePed.Value);
  qryMasternValFrete.Value    := qryMasternValFrete.Value    + (qryItemEstoquenValFrete.Value    * qryItemEstoquenQtdePed.Value);
  qryMasternValDesconto.Value := qryMasternValDesconto.Value + (qryItemEstoquenValDesconto.Value * qryItemEstoquenQtdePed.Value);
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + (qryItemEstoquenValIPI.Value      + qryItemEstoquenValICMSSub.Value);
  qryMasternValOutros.Value   := qryMasternValOutros.Value   + (qryItemEstoquenValSeguro.Value   + qryItemEstoquenValAcessorias.Value);
end;

procedure TfrmPedidoDevVenda.DBGridEh1ColExit(Sender: TObject);
var
  i            : integer;
  nValUnitario : double;
  nValDesconto : double;
  nValFrete    : double;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusPed.Value <> 1) then
      Exit;

  nValUnitario := 0;
  nValDesconto := 0;
  nValFrete    := 0;

  if (DBGridEh1.Col = 3) then
  begin
     if (qryItemEstoque.State = dsBrowse) then
         qryItemEstoque.Edit;

     completaDadosFaturamento();

  end;

  if (qryItemEstoque.State in ([dsInsert,dsEdit])) then
  begin
      { -- produto -- }
      if (DBGridEh1.Col = 1) then
      begin
          ativaEdicaoProduto( (Uppercase( qryItemEstoquecCdProduto.Value ) = 'AD') ) ;

          if ( Uppercase( qryItemEstoquecCdProduto.Value ) = 'AD' ) then
              DBGridEh1.Col := 2 ;

      end ;

      { -- desconto em percentual --
      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nPercDescontoItem') then
      begin
          if not (qryItemEstoquenPercDescontoItem.IsNull) then
              qryItemEstoquenValDesconto.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercDescontoItem.Value/100)) * qryItemEstoquenQtdePed.Value),2) ;
      end; }

      if not (qryItemEstoquenValDesconto.IsNull) then
          nValDesconto := qryItemEstoquenValDesconto.Value;

      if not (qryItemEstoquenValFrete.IsNull) then
          nValFrete := qryItemEstoquenValFrete.Value;

      if not (qryItemEstoquenValUnitario.IsNull) then
          nValUnitario := qryItemEstoquenValUnitario.Value;

      if (dbGridEh1.Col = 3) and (qryItemEstoque.State = dsInsert) and (qryItemEstoquenValUnitario.Value = 0) then
      begin

          qryUltRecebItem.Close ;
          qryUltRecebItem.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
          qryUltRecebItem.Parameters.ParamByName('nCdProduto').Value  := qryItemEstoquenCdProduto.Value ;
          qryUltRecebItem.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
          qryUltRecebItem.Open ;

          if not qryUltRecebItem.Eof then
              qryItemEstoquenValUnitario.Value := qryultRecebItemnValUnitario.Value ;

          qryUltRecebItem.Close;

      end ;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOPItemPedido') then
      begin

          if (qryItemEstoquecCFOPItemPedido.Value <> '') then
          begin

              PosicionaQuery( qryCFOP , qryItemEstoquecCFOPItemPedido.Value );

              if (qryCFOP.eof) then
              begin

                  MensagemAlerta('CFOP inv�lido ou n�o cadastrado.') ;
                  abort ;

              end ;

              if (    (Copy(qryItemEstoquecCFOPItemPedido.Value, 1, 1) <> '1')
                  and (Copy(qryItemEstoquecCFOPItemPedido.Value, 1, 1) <> '2')
                  and (Copy(qryItemEstoquecCFOPItemPedido.Value, 1, 1) <> '3')) then
              begin
                  MensagemAlerta('Informe um CFOP de entrada.') ;
                  abort;
              end ;

          end ;

      end;

      {-- chama a rotina que calcula os impostos do item --}
      calculaImpostosItem( FALSE );

      qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(nValUnitario - nValDesconto + nValFrete,4);
      qryItemEstoquenValTotalItem.Value := qryItemEstoquenValCustoUnit.Value * qryItemEstoquenQtdePed.Value;
  end;

end;

procedure TfrmPedidoDevVenda.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  bFaturaDev := False;

  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else begin
      desativaDBEdit(DBEdit3);
      DBEdit3.Enabled := False;
  end ;

  TabItemEstoque.TabStop := False ;
  TabFaturamento.TabStop := False ;

  {-- formata a casa da quantidade sem decimal --}
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtdePed'].Index].DisplayFormat := ',0' ;
  
  qryItemEstoquenValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItemEstoquenValCustoUnit.DisplayFormat := frmMenu.cMascaraCompras;

  DBGridEh1.Columns[DBGridEh1.FieldColumns['nValUnitario'].Index].DisplayFormat := frmMenu.cMascaraCompras;

end;

procedure TfrmPedidoDevVenda.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmPedidoDevVenda.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryPrazoPedido.Active then
      PosicionaQuery(qryPrazoPedido, qryMasternCdPedido.AsString);

end;

procedure TfrmPedidoDevVenda.qryItemEstoqueAfterPost(DataSet: TDataSet);
begin
  usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;

  qryMaster.Post;

  inherited;
end;

procedure TfrmPedidoDevVenda.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if ((qryMasternCdLoja.Value = 0) or (DbEdit13.Text = '')) and (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdEstoqueMov.Value = 0) or (DBEdit23.Text = '') then
  begin
      MensagemAlerta('Informe o Estoque de Sa�da dos produtos.') ;
      DbEdit9.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro do Pedido.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit22.Text = '') then
  begin
      MensagemAlerta('Informe o endere�o de entrega da devolu��o.') ;
      DBEdit21.SetFocus;
      abort ;
  end ;

  if ((tabFaturamento.Enabled) and (DBEdit24.Text = '')) then
  begin
      MensagemAlerta('Informe o modo do frete.');
      cxPageControl1.ActivePage := tabFaturamento;
      DBEdit26.SetFocus;
      Abort;
  end;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) and (DBEDit27.Text = '') then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.') ;
      DBEdit20.SetFocus;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusPed.Value := 1;
      qryMasternCdEmpresa.Value      := frmMenu.nCdEmpresaAtiva;
  end ;

  qryMasternValPedido.Value := qryMasternValOutros.Value
                             + qryMasternValAcrescimo.Value
                             - qryMasternValDesconto.Value
                             + qryMasternValImposto.Value
                             + qryMasternValServicos.Value
                             + qryMasternValFrete.Value
                             + qryMasternValProdutos.Value;
end;

procedure TfrmPedidoDevVenda.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmPedidoDevVenda.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  //qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemEstoquenValTotalItem.Value;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - (qryItemEstoquenValUnitario.Value   * qryItemEstoquenQtdePed.Value);
  qryMasternValFrete.Value    := qryMasternValFrete.Value    - (qryItemEstoquenValFrete.Value      * qryItemEstoquenQtdePed.Value);
  qryMasternValDesconto.Value := qryMasternValDesconto.Value - (qryItemEstoquenValDesconto.Value   * qryItemEstoquenQtdePed.Value);
  qryMasternValImposto.Value  := qryMasternValImposto.Value  - (qryItemEstoquenValIPI.Value        + qryItemEstoquenValICMSSub.Value);
  qryMasternValOutros.Value   := qryMasternValOutros.Value   - (qryItemEstoquenValAcessorias.Value + qryItemEstoquenValSeguro.Value);
end;

procedure TfrmPedidoDevVenda.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;

  qryMaster.Post;
end;

procedure TfrmPedidoDevVenda.ToolButton10Click(Sender: TObject);
var
    nValProdutos, nValImpostos : double ;
begin

  nValProdutos := 0 ;
  nValImpostos := 0 ;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value <> 1) then
  begin
      MensagemAlerta('Pedido j� finalizado.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor informado.' + #13#13 + 'Deseja continuar ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
      qryAux.Open ;

      If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
      begin

          frmMenu.Connection.BeginTrans;

          try
              usp_Sugere_Parcela.Close ;
              usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
              usp_Sugere_Parcela.ExecProc ;
          except
              frmMenu.Connection.RollbackTrans ;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans ;

          PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

      end ;

      qryAux.Close ;

  end ;

  {-- verifica se todos os impostos foram digitados corretamente --}
  if ( bFaturaDev ) then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          if     (length(Trim(qryItemEstoquecNCM.Value)) <> 2)
             and (length(Trim(qryItemEstoquecNCM.Value)) <> 8) then
          begin
              MensagemAlerta('NCM n�o informado ou inv�lido.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;

          if (length(Trim(qryItemEstoquecCdSTICMS.Value)) < 3) then
          begin
              MensagemAlerta('Informe o C�digo CST do item. (Origem da Mercadoria e C�digo Cst) Ex. 000.');
              DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
              DBGridEh1.SetFocus;
              Abort;
          end;

          if (Trim(qryItemEstoquecCFOPItemPedido.Value) = '') then
          begin
              MensagemAlerta('Informe o C�digo CFOP de devolu��o.');
              DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
              DBGridEh1.SetFocus;
              Abort;
          end;

          PosicionaQuery(qryCFOP,qryItemEstoquecCFOPItemPedido.AsString);

          if (qryCFOP.Eof) then
          begin
              MensagemAlerta('CFOP n�o cadastrado.');
              DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
              DBGridEh1.SetFocus;
              Abort;
          end;

          { -- verifica se o CFOP � de entrada -- }
          if ( StrToInt( Copy(qryItemEstoquecCFOPItemPedido.Value,1,1) ) > 5 ) then
          begin
              MensagemAlerta('O CFOP informado � um CFOP de sa�da. Informe um CFOP de entrada.');
              DBGridEh1.SelectedField := qryItemEstoquecCFOPItemPedido;
              DBGridEh1.SetFocus;
              Abort;
          end;

          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      // verifica se o tipo do pedido exige autoriza��o
      if (qryTipoPedidocExigeAutor.Value = 1) then
          qryMasternCdTabStatusPed.Value := 2  // Aguardando aprova��o
      else begin
          qryMasternCdTabStatusPed.Value := 3 ; // Aprovado
          qryMasterdDtAutor.Value        := Now() ;
          qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

      end ;

      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;
      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido finalizado com sucesso.') ;

  btCancelar.Click;

end;

procedure TfrmPedidoDevVenda.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmPedidoDevVenda.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedidoDevVenda.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCdProduto') then
            begin

                If (qryMasternCdTipoPedido.asString = '') then
                begin
                    ShowMessage('Selecione um tipo de pedido.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto) AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.asString + ')');

                If (nPK > 0) then
                begin
                    qryItemEstoquenCdProduto.Value := nPK ;
                    qryItemEstoquecCdProduto.Value := qryItemEstoquenCdProduto.asString ;

                    qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                    PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                    if not qryProduto.eof then
                    begin
                        qryItemEstoquecNmItem.Value             := qryProdutocNmProduto.Value ;
                        qryItemEstoquecSiglaUnidadeMedida.Value := qryProdutocUnidadeMedida.Value;
                    end;

                end ;

            end;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOP') then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(747,'WHERE Convert(INTEGER,cCFOP) > 4000');

                If (nPK > 0) then
                    qryItemEstoquecCFOPItemPedido.Value := intToStr(nPk);
            end;

        end;


    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;

  qryTerceiroEmitente.Close;
  qryTerceiroEmitente.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value;
  qryTerceiroEmitente.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value;
  qryTerceiroEmitente.Open;

  DBGridEh1.Col := 1 ;
end;

procedure TfrmPedidoDevVenda.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoDevVenda.ToolButton12Click(Sender: TObject);
var
   objRel : TrptPedidoDevCompra;

begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) and (qryMasternCdTabStatusPed.Value = 1) then
  begin
      qryMaster.Post ;
  end ;

  objRel := TrptPedidoDevCompra.Create(nil);

  Try
      Try
          PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;
          objRel.QRSubDetail3.Enabled := True ;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False ;

          if (objRel.qryItemAD.eof) then
              objRel.QRSubDetail2.Enabled := False ;

          if (objRel.qryItemFormula.eof) then
              objRel.QRSubDetail3.Enabled := False ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;
          objRel.QRLabel2.Caption   := 'Pedido de Devolu��o de Venda';

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

procedure TfrmPedidoDevVenda.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.Font.Color  := clBlue ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.Font.Color  := clWhite ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.Font.Color  := clWhite ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoDevVenda.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoDevVenda.ToolButton13Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  
  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmPedidoDevVenda.btExibirAtendItemClick(
  Sender: TObject);
  var
    objCons : TfrmItemPedidoAtendido;
begin
  inherited;

  objCons :=  TfrmItemPedidoAtendido.Create(nil);

  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin
      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin
          PosicionaQuery(objCons.qryItem, qryItemEstoquenCdItemPedido.AsString) ;
          showForm(objCons,True) ;
      end ;
  end ;

end;

procedure TfrmPedidoDevVenda.cxButton6Click(Sender: TObject);
begin
  inherited;
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  PosicionaQuery(rptPedCom_Contrato.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemFormula,qryMasternCdPedido.asString) ;

  rptPedCom_Contrato.QRSubDetail1.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail2.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail3.Enabled := True ;

  if (rptPedCom_Contrato.qryItemEstoque_Grade.eof) then
      rptPedCom_Contrato.QRSubDetail1.Enabled := False ;

  if (rptPedCom_Contrato.qryItemAD.eof) then
      rptPedCom_Contrato.QRSubDetail2.Enabled := False ;

  if (rptPedCom_Contrato.qryItemFormula.eof) then
      rptPedCom_Contrato.QRSubDetail3.Enabled := False ;

  rptPedCom_Contrato.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  rptPedCom_Contrato.QuickRep1.PreviewModal;

end;

procedure TfrmPedidoDevVenda.DBEdit9Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  IF (DBEdit3.Enabled) and (DbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  qryLocalEstoqueSaida.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmPedidoDevVenda.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.isEmpty) then
      Exit;

  qryLocalEstoqueSaida.Close ;
  PosicionaQuery(qryLocalEstoqueSaida, DBEdit9.Text) ;

  if (DBEdit3.Enabled) then
  begin
      if (DBEdit13.Text = '') then
      begin
          MensagemAlerta('Selecione a loja.') ;
          DBEdit3.SetFocus ;
          abort ;
      end ;

      if (qryMaster.State = dsInsert) then
      begin

          if (qryLocalEstoqueSaidanCdLoja.Value <> qryLojanCdLoja.Value) then
          begin

              if (MessageDlg('O local de estoque selecionado n�o pertence a loja informada. Tem certeza que deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNo) then
              begin
                  DBEdit3.SetFocus;
                  abort ;
              end ;

          end ;

      end ;

  end ;
  
end;

procedure TfrmPedidoDevVenda.DBEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdEstoqueMov.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.btConsultarDocSaidaClick(Sender: TObject);

var
  objConsultaItens : TfrmPedidoDevVenda_ConsultaItens;
begin

  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Salve o pedido antes de consultar o documento de sa�da.');
      Exit;
  end;


  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.');
      Exit;
  end;

  objConsultaItens := TfrmPedidoDevVenda_ConsultaItens.Create(nil);

  objConsultaItens.nCdPedido     := qryMasternCdPedido.Value;
  objConsultaItens.nCdTipoPedido := qryMasternCdTipoPedido.Value;
  objConsultaItens.nCdTerceiro   := qryMasternCdterceiro.Value;

  showForm(objConsultaItens,True);

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString);

  DBGridEh1.SetFocus;
  DBGridEh1.Col := 3;

  if (qryItemEstoque.Active) then
      qryItemEstoque.Last;

  //completaDadosFaturamento();
end;

procedure TfrmPedidoDevVenda.cxButton4Click(Sender: TObject);
var
  cOBS    : string ;
  iQtdeNF : integer ;
begin

  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.') ;
      exit ;
  end ;

  if (not qryItemEstoque.Active) or (qryItemEstoque.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado para devolu��o.') ;
      exit ;
  end ;

  qryItemEstoque.DisableControls;
  qryItemEstoque.First ;

  cOBS    := 'DEV CF NF ' ;
  iQtdeNF := 0 ;

  while not qryItemEstoque.eof do
  begin

      if (Pos(cOBS,Trim(qryItemEstoquecDoctoCompra.asString)) = 0) and (trim(qryItemEstoquecDoctoCompra.AsString) <> '') then
      begin
          if (iQtdeNF = 0) then
              cOBS := cOBS + Trim( qryItemEstoquecDoctoCompra.AsString )
          else cOBS := cOBS + ' / ' + Trim( qryItemEstoquecDoctoCompra.AsString ) ;

          iQtdeNF := iQtdeNF + 1 ;
      end ;

      qryItemEstoque.Next ;
  end ;

  qryItemEstoque.First ;
  qryItemEstoque.EnableControls;

  qryMaster.Edit ;
  qryMastercMsgNF1.Value := Copy(cOBS,1,150) ;
  qryMastercMsgNF2.Value := Copy(cOBS,151,150) ;

end;

procedure TfrmPedidoDevVenda.DBEdit41KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit41Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroPagador,DBEdit41.Text) ;

end;

procedure TfrmPedidoDevVenda.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryIncoterms,DbEdit19.Text) ;

end;

procedure TfrmPedidoDevVenda.DBEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit10Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroTransp, DbEdit10.Text) ;

end;

procedure TfrmPedidoDevVenda.btSalvarClick(Sender: TObject);
begin
  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.') ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmPedidoDevVenda.ToolButton5Click(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 0 ;
  inherited;

end;

procedure TfrmPedidoDevVenda.CompletaDadosFaturamento;
begin
    {if (qryItemEstoquenCdProduto.Value > 0) then
    begin
        qryGrupoImposto.Close;
        qryGrupoImposto.Parameters.ParamByName('nPk').Value := qryItemEstoquenCdProduto.Value;
        qryGrupoImposto.Open;

        if (qryItemEstoquecNCM.Value = '') then
            qryItemEstoquecNCM.Value := qryGrupoImpostocNCM.Value;

        qryUFEmissao.Close;
        qryUFEmissao.Parameters.ParamByName('nCdTerceiroEmp').Value  := qryEmpresanCdTerceiroEmp.Value;
        qryUFEmissao.Open;

        qryUFDestino.Close;
        qryUFDestino.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
        qryUFDestino.Open;

        qryRegraICMS.Close;
        qryRegraICMS.Parameters.ParamByName('nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva;
        qryRegraICMS.Parameters.ParamByName('cUFOrigem').Value       := qryUFEmissaocUF.Value;
        qryRegraICMS.Parameters.ParamByName('cUFDestino').Value      := qryUFDestinocUF.Value;
        qryRegraICMS.Parameters.ParamByName('nCdGrupoImposto').Value := qryGrupoImpostonCdGrupoImposto.Value;
        qryRegraICMS.Parameters.ParamByName('nCdTipoPedido').Value   := qryMasternCdTipoPedido.Value;
        qryRegraICMs.Open;

        if (qryItemEstoque.State = dsBrowse) then
            qryItemEstoque.Edit;

        qryItemEstoquenPercIva.Value         := qryRegraICMSnPercIva.Value;
        qryItemEstoquenPercAliqICMS.Value    := qryRegraICMSnAliqICMS. Value;
        qryItemEstoquenPercAliqICMSInt.Value := qryRegraICMSnAliqICMSInterna.Value;
        qryItemEstoquenPercRedBaseIcms.Value := (100 - qryRegraICMSnPercBC.Value);
        qryItemEstoquecCdSTICMS.Value        := qryGrupoImpostonCdTabTipoOrigemMercadoria.AsString + qryRegraICMSnCdTipoTributacaoICMS.Value;

    end;}

end;

function TfrmPedidoDevVenda.RightStr(const Str: String;
  Size: Word): String;
begin
  if Size > Length(Str) then Size := Length(Str) ;
  RightStr := Copy(Str, Length(Str)-Size+1, Size)
end;

procedure TfrmPedidoDevVenda.qryItemEstoquecCdProdutoChange(
  Sender: TField);
begin
  inherited;

  if ( trim( qryItemEstoquecCdProduto.Value ) <> '' ) and ( trim( Uppercase( qryItemEstoquecCdProduto.Value ) ) <> 'AD' )  then
  begin

      try
          strToInt( qryItemEstoquecCdProduto.Value ) ;
      except
          MensagemAlerta('C�digo de produto inv�lido. Informe "AD" para itens n�o cadastrados.') ;
          abort ;
      end ;

      qryItemEstoquenCdProduto.Value := strToInt( qryItemEstoquecCdProduto.Value ) ;

      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof and (Trim( qryItemEstoquecNmItem.Value ) <> Trim( qryProdutocNmProduto.Value ) ) then
      begin
          qryItemEstoquecNmItem.Value             := qryProdutocNmProduto.Value ;
          qryItemEstoquecSiglaUnidadeMedida.Value := qryProdutocUnidadeMedida.Value;
      end;

  end ;

end;

procedure TfrmPedidoDevVenda.verificaControlesICMS(cCST: string);
var
    bICMS   , bICMSST    : boolean ;
    bICMSRed, bICMSSTRed : boolean ;
    i                    : integer ;

begin

    if (not qryMaster.Active) then
        exit ;

    bICMS   := true ;
    bICMSST := false ;

    if (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '40') or (trim(RightStr(cCST,2)) = '41') or (trim(RightStr(cCST,2)) = '50') then
        bICMS := false ;

    if (trim(RightStr(cCST,2)) = '10') or (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '70') then
    begin
        bICMSST := true ;
    end ;

    bICMSRed   := (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '70') ;
    bICMSSTRed := (trim(RightStr(cCST,2)) = '70') ;

    // formata as colunas do grid
    with DBGridEh1 do
    begin

        {-- ICMS NORMAL --}
        // % reducao base calc icms
        Columns[FieldColumns['nPercRedBaseICMS'].Index].ReadOnly := (not bICMSRed) ;

        if (bICMSRed)  then
            Columns[FieldColumns['nPercRedBaseICMS'].Index].Title.Font.Color := clBlack
        else
        begin
            Columns[FieldColumns['nPercRedBaseICMS'].Index].Title.Font.Color := clRed ;
        end ;


        {-- as colunas de base de c�lculo do ICMS de Valor do ICMS SEMPRE ficam somente leitura --}
        Columns[FieldColumns['nValBaseICMS'].Index].ReadOnly  := TRUE ;
        Columns[FieldColumns['nPercAliqICMS'].Index].ReadOnly := TRUE ;
        Columns[FieldColumns['nValICMS'].Index].ReadOnly      := TRUE ;

        Columns[FieldColumns['nValBaseICMS'].Index].Title.Font.Color  := clRed ;
        Columns[FieldColumns['nPercAliqICMS'].Index].Title.Font.Color := clRed ;
        Columns[FieldColumns['nValICMS'].Index].Title.Font.Color      := clRed ;

        {-- *** Substitui��o Tributaria *** --}
        {-- se bICMSST for TRUE, indica que o CST do item tem substitui��o tribut�ria --}
        {-- quando bICMSST for TRUE, ele precisa deixar o ReadOnly como false, por isso � atribuido o valor de (not bICMSST) --}

        Columns[FieldColumns['nPercIVA'].Index].ReadOnly         := (not bICMSST) ;
        Columns[FieldColumns['nPercAliqICMSInt'].Index].ReadOnly := (not bICMSST) ;
        Columns[FieldColumns['nValBaseICMSSub'].Index].ReadOnly  := (not bICMSST) ;
        Columns[FieldColumns['nValICMSSub'].Index].ReadOnly      := (not bICMSST) ;

        if (bICMS) then
        begin
            Columns[FieldColumns['nPercAliqICMS'].Index].ReadOnly         := FALSE ;
            Columns[FieldColumns['nPercAliqICMS'].Index].Title.Font.Color := clBlack ;
        end;

        if (bICMSST)  then
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color         := clBlack ;
            Columns[FieldColumns['nPercAliqICMSInt'].Index].Title.Font.Color := clBlack ;
            Columns[FieldColumns['nValBaseICMSSub'].Index].Title.Font.Color  := clBlack ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color      := clBlack ;

        end
        else
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color         := clRed ;
            Columns[FieldColumns['nPercAliqICMSInt'].Index].Title.Font.Color := clRed ;
            Columns[FieldColumns['nValBaseICMSSub'].Index].Title.Font.Color  := clRed ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color      := clRed ;

        end ;

    end ;

    if (qryItemEstoque.State in [dsInsert,dsEdit]) then
    begin
         if not (bICMS) Then
         Begin
             qryItemEstoquenPercAliqICMS.Value    := 0;
             qryItemEstoquenValBaseICMS.Value     := 0;
             qryItemEstoquenValICMS.Value         := 0;
             qryItemEstoquenPercRedBaseICMS.Value := 0;
         end;

         if not (bICMSRed) then
             qryItemEstoquenPercRedBaseICMS.Value := 0;

         if not (bICMSST) then
         begin
             qryItemEstoquenPercIVA.Value                := 0;
             qryItemEstoquenPercAliqICMSInt.Value        := 0;
             qryItemEstoquenValBaseICMSSub.Value         := 0;
             qryItemEstoquenPercICMSSub.Value            := 0;
             qryItemEstoquenValICMSSub.Value             := 0;
         end ;

    end;

    calculaImpostosItem( FALSE );

end;

procedure TfrmPedidoDevVenda.qryItemEstoqueAfterScroll(DataSet: TDataSet);
begin
  inherited;

  {if (qryItemEstoque.State = dsBrowse) then
      verificaControlesICMS( qryItemEstoquecCdSTICMS.Value ) ;}

  ativaEdicaoProduto( (qryItemEstoquecCdProduto.Value = 'AD') ) ;

end;

procedure TfrmPedidoDevVenda.qryItemEstoquecCdSTICMSChange(
  Sender: TField);
begin
  inherited;

  {verificaControlesICMS( qryItemEstoquecCdSTICMS.Value ) ;}

end;

procedure TfrmPedidoDevVenda.calculaImpostosItem( bGravar : boolean ) ;
begin

    { -- se grupo de pedido n�o permite faturamento, n�o valida os impostos porque n�o ser� emitida a nota de devolu��o -- }
    if (not bFaturaDev) then
        exit ;

    if (qryItemEstoque.State in ([dsInsert,dsEdit])) then
    begin
        {-- calcula o IPI --}
        if (qryItemEstoquenPercIPI.Value > 0) then
        begin

            qryItemEstoquenValBaseIPI.Value := qryItemEstoquenValTotalItem.Value ;

            if (qryItemEstoquenPercBaseCalcIPI.Value > 0) then
                qryItemEstoquenValBaseIPI.Value := (qryItemEstoquenValTotalItem.Value * ( qryItemEstoquenPercBaseCalcIPI.Value / 100 ) );

            qryItemEstoquenValIPI.Value := (qryItemEstoquenValBaseIPI.Value * (qryItemEstoquenPercIPI.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCdSTIPI.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do IPI.') ;
                    abort ;
                end ;

                qryCST_IPI.Close;
                PosicionaQuery( qryCST_IPI , qryItemEstoquecCdSTIPI.Value ) ;

                if ( qryCST_IPI.eof ) then
                begin
                    MensagemAlerta('CST do IPI inv�lido.') ;
                    abort ;
                end
                else
                begin

                    {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
                    {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
                    {-- num�ricos e mantidos pela ER2.                                                         --}
                    if ( strToint( qryCST_IPIcCdStIPI.Value ) < 50 ) then
                    begin
                        MensagemAlerta('Informe um CST de Sa�da para o IPI.') ;
                        abort ;
                    end ;

                end ;

            end ;

        end
        else
        begin

            if (bGravar) then
            begin

                qryItemEstoquenValBaseIPI.Value      := 0 ;
                qryItemEstoquenPercBaseCalcIPI.Value := 0 ;
                qryItemEstoquenValIPI.Value          := 0 ;

            end ;

        end ;

        // Processo de Valida��o do CST
        qryCST_ICMS.Close;

        qryItemEstoquecCdStICMS.Value := StringReplace(qryItemEstoquecCdStICMS.Value,' ','',[rfReplaceAll]);

        { -- valida CST -- }
        if (Length(qryItemEstoquecCdStICMS.Value) = 3) then
            PosicionaQuery( qryCST_ICMS, RightStr(qryItemEstoquecCdStICMS.Value,2));

        { -- valida CSOSN -- }
        if (Length(qryItemEstoquecCdStICMS.Value) = 4) then
            PosicionaQuery( qryCST_ICMS, RightStr(qryItemEstoquecCdStICMS.Value,3));

        if (qryCST_ICMS.IsEmpty) then
        begin
            MensagemAlerta('CST/CSOSN do ICMS inv�lido.');
            DBGridEh1.SelectedField := qryItemEstoquecCdSTICMS;
            DBGridEh1.SetFocus;
            Abort;
        end;

        //icms
        {-- se a aliquota est� somente leitura � porque o CST do ICMS diz que n�o tem ICMS --}
        if DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercAliqICMS'].Index].ReadOnly
           or (qryItemEstoquenPercAliqICMS.Value = 0) then
        begin

            if (bGravar) then
            begin

                qryItemEstoquenValBaseICMS.Value     := 0 ;
                qryItemEstoquenPercRedBaseICMS.Value := 0 ;
                qryItemEstoquenPercAliqICMS.Value    := 0 ;
                qryItemEstoquenValICMS.Value         := 0 ;

            end ;

        end
        else
        begin

            qryItemEstoquenValBaseICMS.Value := qryItemEstoquenValTotalItem.Value ;

            if (qryItemEstoquenPercRedBaseICMS.Value > 0) then
                qryItemEstoquenValBaseICMS.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercRedBaseICMS.Value/100) );

            qryItemEstoquenValICMS.Value := (qryItemEstoquenValBaseICMS.Value * (qryItemEstoquenPercAliqICMS.Value/100));

        end;

        {-- PIS --}
        if (qryItemEstoquenAliqPIS.Value = 0) then
        begin

            if (bGravar) then
            begin
                if (qryItemEstoquenValBasePIS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do PIS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemEstoquenValPIS.Value := 0 ;
                qryItemEstoquecCSTPIS.Value := '' ;

            end ;

        end
        else
        begin

            qryItemEstoquenValBasePIS.Value := qryItemEstoquenValTotalItem.Value + qryItemEstoquenValIPI.Value ;
            qryItemEstoquenValPIS.Value     := (qryItemEstoquenValBasePIS.Value * (qryItemEstoquenAliqPIS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCSTPIS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do PIS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemEstoquecCSTPIS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do PIS inv�lido.') ;
                    abort ;
                end ;

            end ;

        end ;


        {-- COFINS --}
        if (qryItemEstoquenAliqCOFINS.Value = 0) then
        begin

            if (bGravar) then
            begin
                if (qryItemEstoquenValBaseCOFINS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do COFINS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemEstoquenValCOFINS.Value := 0 ;
                qryItemEstoquecCSTCOFINS.Value := '' ;

            end ;

        end
        else
        begin

            qryItemEstoquenValBaseCOFINS.Value := qryItemEstoquenValTotalItem.Value + qryItemEstoquenValIPI.Value ;
            qryItemEstoquenValCOFINS.Value     := (qryItemEstoquenValBaseCOFINS.Value * (qryItemEstoquenAliqCOFINS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCSTCOFINS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do COFINS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemEstoquecCSTCOFINS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do COFINS inv�lido.') ;
                    abort ;
                end ;

            end ;

        end ;

    end ;

end;

procedure TfrmPedidoDevVenda.ativaEdicaoProduto(bPermitirEditar: boolean);
begin

    if (bPermitirEditar) then
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := False ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clWhite ;
    end
    else
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clRed ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := True ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clSilver ;
    end ;

end;

procedure TfrmPedidoDevVenda.DBEdit21Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.state = dsInsert) and (DBEdit22.Text = '') then
  begin

      qrySugereEnderecoEntrega.Close;
      qrySugereEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qrySugereEnderecoEntrega.Open ;

      if not qrySugereEnderecoEntrega.Eof then
      begin
          qryMasternCdEnderecoEntrega.Value := qrySugereEnderecoEntreganCdEndereco.Value ;
          qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
          PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;
      end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit21KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit15.Text) = '') then
            begin
                MensagemAlerta('Informe o Terceiro.') ;
                DBEdit5.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(206,'Endereco.nCdTerceiro = ' + qryMasternCdTerceiro.AsString);

            If (nPK > 0) then
            begin
                qryMasternCdEnderecoEntrega.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevVenda.DBEdit21Exit(Sender: TObject);
begin
  inherited;

  qryEnderecoEntrega.Close;
  qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryEnderecoEntrega, DBEdit21.Text) ;

  if (qryEnderecoEntregacNmStatus.Value = 'Inativo') then
      MensagemAlerta('Este endere�o est� inativo. Verifique.') ;

end;


procedure TfrmPedidoDevVenda.qryTipoPedidoAfterOpen(DataSet: TDataSet);
begin
  inherited;

  if not (qryTipoPedido.IsEmpty) then
  begin
      qryGrupoPedido.Close;
      PosicionaQuery(qryGrupoPedido, qryTipoPedidonCdGrupoPedido.AsString);

      { -- verifica se grupo de pedido gera nota fiscal -- }
      bFaturaDev := (qryGrupoPedidocFlgFaturar.Value = 1);
  end;
end;

procedure TfrmPedidoDevVenda.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if ((DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nValUnitario') and (qryItemEstoque.State <> dsBrowse) and (qryItemEstoquenValTotalItem.Value = 0)) then
  begin

      qryItemEstoquenValUnitario.Value := qryProdutonValVenda.Value;

  end;
end;

procedure TfrmPedidoDevVenda.btLerArquivoNFeClick(Sender: TObject);
var
  objLerXML : TfrmPedidoDevVenda_LeituraXML;
  i         : Integer;
begin
  inherited;

  if (not qryMaster.Active) or (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Salve o pedido antes de importar XML.');
      Abort;
  end;

  if (qryMasternCdTabStatusPed.Value <> 1) then
  begin
      MensagemAlerta('Status do pedido n�o permite importar XML.');
      Abort;
  end;

  if (qryTerceiro.IsEmpty) then
  begin
      MensagemAlerta('Informe o terceiro da NF-e antes de importar o XML.');
      DBEdit5.setFocus;
      Abort;
  end;

  objLerXML := TfrmPedidoDevVenda_LeituraXML.Create(Self);

  objLerXML.nCdPedido     := qryMasternCdPedido.Value;
  objLerXML.nCdTipoPedido := qryMasternCdTipoPedido.Value;

  OpenDialog.FileName   :=  '';
  OpenDialog.Title      := 'Selecione um arquivo NFE XML';
  OpenDialog.DefaultExt := '*-nfe.XML';
  OpenDialog.Filter     := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Arquivos TXT (*.TXT)|*.TXT|Todos os Arquivos (*.*)|*.*';

  if (OpenDialog.Execute) then
  begin
      if (OpenDialog.FileName <> '') then
      begin
          try
              ACBrNFe.NotasFiscais.Clear;
              ACBrNFe.NotasFiscais.LoadFromFile(OpenDialog.FileName);
          except
              MensagemAlerta('Arquivo NF-e inv�lido.');
              Exit;
          end;
      end
      else
      begin
          Exit;
      end;
  end
  else
  begin
      Exit;
  end;

  with (ACBrNFe.NotasFiscais.Items[0].NFe) do
  begin
     objLerXML.qryTerceiro.Close;
     objLerXML.qryTerceiro.Parameters.ParamByName('cCNPJCPF').Value := Trim(Emit.CNPJCPF);
     objLerXML.qryTerceiro.Open;

     if (qryTerceirocCNPJCPF.Value <> Trim(Emit.CNPJCPF)) then
     begin
         MensagemAlerta('O CNPJ do emitente da NF-e � diferente do CNPJ do terceiro selecionado.');
         DBEdit5.setFocus;
         Abort;
     end;

     objLerXML.qryNFeXML.Close;
     objLerXML.qryNFeXML.ExecSQL;
     objLerXML.qryNFeXML.Open;

     with (ACBrNFe.NotasFiscais.Items[0].NFe) do
     begin
         for i := 0 to Det.Count-1 do
         begin

             with (Det.Items[i]) do
             begin
                 objLerXML.qryNFeXML.Insert;
                 objLerXML.qryNFeXMLnCdPedido.Value            := qryMasternCdPedido.Value;
                 objLerXML.qryNFeXMLcNrDocto.Value             := procNFe.chNFe;
                 objLerXML.qryNFeXMLcCdProdutoFornecedor.Value := Prod.cProd;
                 objLerXML.qryNFeXMLcNmProdutoFornecedor.Value := Prod.xProd;
                 objLerXML.qryNFeXMLnQtdeFornecedor.Value      := Prod.qCom;
                 objLerXML.qryNFeXMLcUnMedidaFornecedor.Value  := Prod.uCom;
                 objLerXML.qryNFeXMLnValUnitario.Value         := Prod.vUnCom;
                 objLerXML.qryNFeXMLnValTotal.Value            := Prod.vProd;
                 objLerXML.qryNFeXMLnValDesconto.Value         := Prod.vDesc;
                 objLerXML.qryNFeXMLcNCM.Value                 := Prod.NCM;
                 objLerXML.qryNFeXMLnValSeguro.Value           := Prod.vSeg;
                 objLerXML.qryNFeXMLnValFrete.Value            := Prod.vFrete;
                 objLerXML.qryNFeXMLcOrigemMercadoria.Value    := OrigToStr(Imposto.ICMS.Orig);

                 { -- CFOP -- }
                 qryNatOperacaoVinculo.Close;
                 qryNatOperacaoVinculo.Parameters.ParamByName('cUFOrigem').Value  := Emit.EnderEmit.UF;
                 qryNatOperacaoVinculo.Parameters.ParamByName('cUFDestino').Value := Dest.EnderDest.UF;
                 qryNatOperacaoVinculo.Parameters.ParamByName('cCFOPNF').Value    := Prod.CFOP;
                 qryNatOperacaoVinculo.Open;

                 if (not qryNatOperacaoVinculo.IsEmpty) then
                     objLerXML.qryNFeXMLcCFOP.Value := qryNatOperacaoVinculocCFOP.Value;

                 { -- CSOSN -- }
                 if (Emit.CRT = crtSimplesNacional) then
                     objLerXML.qryNFeXMLcCdST.Value := CSOSNIcmsToStr(Imposto.ICMS.CSOSN) //CSOSNToStrTagPos(Imposto.ICMS.CSOSN)
                 { -- CST -- }
                 else
                     objLerXML.qryNFeXMLcCdST.Value := CSTICMSToStr(Imposto.ICMS.CST); //CSTICMSToStrTagPos(Imposto.ICMS.CST);

                 { -- ICMS -- }
                 objLerXML.qryNFeXMLnValBaseICMS.Value  := Imposto.ICMS.vBC;
                 objLerXML.qryNFeXMLnValICMS.Value      := Imposto.ICMS.vICMS;
                 objLerXML.qryNFeXMLnPercAliqICMS.Value := Imposto.ICMS.pICMS;

                 if (Imposto.ICMS.pRedBC > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseIcms.Value := (100 - Imposto.ICMS.pRedBC) ;

                 { -- ICMS ST -- }
                 objLerXML.qryNFeXMLnValBaseICMSST.Value  := Imposto.ICMS.vBCST;
                 objLerXML.qryNFeXMLnValICMSST.Value      := Imposto.ICMS.vICMSST;
                 objLerXML.qryNFeXMLnpercAliqICMSST.Value := Imposto.ICMS.pICMSST;
                 objLerXML.qryNFeXMLnPercIVA.Value        := Imposto.ICMS.pMVAST;

                 if (Imposto.ICMS.pRedBCST > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseICMSSt.Value := (100 - Imposto.ICMS.pRedBCST) ;

                 { -- IPI -- }
                 objLerXML.qryNFeXMLnValBaseIPI.Value  := Imposto.IPI.vBC ;
                 objLerXML.qryNFeXMLnPercAliqIPI.Value := Imposto.IPI.pIPI;
                 objLerXML.qryNFeXMLnValIPI.Value      := Imposto.IPI.vIPI;
                 objLerXML.qryNFeXMLcCSTIPI.Value      := CSTIPIToStr( Imposto.IPI.CST) ;

                 { -- se produto j� importado anteriormente ou terceiro emit. pertence ao mesmo grupo empresarial, efetua v�nculo autom�tico -- }
                 objLerXML.qryProdutoFornecedor.Close;
                 objLerXML.qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := Prod.cProd;
                 objLerXML.qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := objLerXML.qryTerceironCdTerceiro.Value;
                 objLerXML.qryProdutoFornecedor.Open;

                 if ((not objLerXML.qryProdutoFornecedor.Eof) or (objLerXML.qryTerceirocFlgTerceiroGrupo.Value = 1)) then
                 begin

                     objLerXML.qryProduto.Close;

                     if (objLerXML.qryTerceirocFlgTerceiroGrupo.Value = 1) then
                         objLerXML.qryProduto.Parameters.ParamByName('nPK').Value := StrToIntDef(Prod.cProd,0)
                     else
                         objLerXML.qryProduto.Parameters.ParamByName('nPK').Value := objLerXML.qryProdutoFornecedornCdProduto.Value;

                     objLerXML.qryProduto.Open;

                     if (not objLerXML.qryProduto.Eof) then
                     begin

                         objLerXML.qryNFeXMLnCdProduto.Value := objLerXML.qryProdutonCdProduto.Value;
                         objLerXML.qryNFeXMLcNmProduto.Value := objLerXML.qryProdutocNmProduto.Value;
                         objLerXML.qryNFeXMLcUnMedida.Value  := objLerXML.qryProdutocUnidadeMedida.Value;

                     end ;

                 end ;

                 objLerXML.qryNFeXML.Post;

             end;

         end;

     end;

     objLerXML.qryNFeXML.Open;

     showForm(objLerXML,False);

     PosicionaQuery(qryMaster, qryMasternCdPedido.AsString);
     PosicionaQuery(qryItemEstoque,qryMasternCdPedido.AsString);
 end;
end;

procedure TfrmPedidoDevVenda.DBEdit20Exit(Sender: TObject);
begin
  inherited;

  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.AsString) ;
end;

procedure TfrmPedidoDevVenda.DBEdit26KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := -1;
              nPK := frmLookup_Padrao.ExecutaConsulta(776);

              if (nPK > -1) then
                  qryMasternCdTabTipoModFrete.Value := nPK;

              {if ((qryMasternValFrete.Value = 0) and (nPK <> 9)) then
              begin
                  MensagemAlerta('Modo de Frete inv�lido devido pedido n�o possuir valor de frete informado.');
                  Abort;
              end;

              if ((qryMasternValFrete.Value > 0) and (nPK = 9)) then
              begin
                  MensagemAlerta('Modo de Frete inv�lido devido pedido possuir valor de frete informado.');
                  Abort;
              end;}
          end;
      end;
  end;
end;

procedure TfrmPedidoDevVenda.DBEdit26Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTabTipoModFrete.Close;
  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString);
end;

procedure TfrmPedidoDevVenda.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      MensagemAlerta('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;

      qryPrazoPedidoDif.Close;
      qryPrazoPedidoDif.Parameters.ParamByName('nPK').Value := qryMasternCdPedido.Value;
      qryPrazoPedidoDif.ExecSQL;

  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
end;

procedure TfrmPedidoDevVenda.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  inherited;

  if (qryPrazoPedido.State = dsInsert) then
  begin
      qryPrazoPedidonCdPrazoPedido.Value := frmMenu.fnProximoCodigo('PRAZOPEDIDO');
      qryPrazoPedidonCdPedido.Value      := qryMasternCdPedido.Value ;
  end;
end;

initialization
    RegisterClass(TfrmPedidoDevVenda) ;

end.
