unit fManutLimiteCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmManutLimiteCliente = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMastercRG: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasterdDtNasc: TDateTimeField;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternPercEntrada: TBCDField;
    qryMasternSaldoLimiteCredito: TBCDField;
    qryMastercNmTabTipoSituacao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    qryMasterdDtNegativacao: TDateTimeField;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    qryMastercFlgBloqAtuLimAutom: TIntegerField;
    btCalcular: TcxButton;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    SP_GERA_LIMITE_CREDITO: TADOStoredProc;
    qryMasternCdTabTipoSituacao: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit16Change(Sender: TObject);
    procedure btCalcularClick(Sender: TObject);
    procedure btnAuditoriaClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutLimiteCliente: TfrmManutLimiteCliente;

implementation

uses fAprovacaoClienteVarejo_LimiteInicial, fTrilhaClienteVarejo,
  fHistoricoLimiteCredito, fMenu;

{$R *.dfm}

procedure TfrmManutLimiteCliente.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'CLIENTECREDIARIO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 180 ;
  bCodigoAutomatico := False ;
  bAutoEdit         := False ;

end;

procedure TfrmManutLimiteCliente.DBEdit16Change(Sender: TObject);
begin
  inherited;
  if (DBedit16.Text <> '') then
  begin
      DBEdit16.Color      := clRed ;
      DBEdit16.Font.Color := clWhite ;
  end
  else
  begin
    DBEDit16.Color      := $00E9E4E4  ;
    DBEdit16.Font.Color := clBlack ;
  end ;

end;

procedure TfrmManutLimiteCliente.btCalcularClick(Sender: TObject);
var
  objForm : TfrmAprovacaoClienteVarejo_LimiteInicial;
begin
  {inherited;}
  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoSituacao.Value <> 2) then
  begin
      MensagemErro('A Situa��o cadastral deste cliente n�o est� aprovada.') ;
      exit ;
  end ;

  if (qryMasterdDtNegativacao.AsString <> '') then
      if (MessageDlg('Este cliente est� negativado pela empresa. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
          exit ;

  objForm := TfrmAprovacaoClienteVarejo_LimiteInicial.Create(nil);

  objForm.nCdTerceiro            := qryMasternCdTerceiro.Value ;
  objForm.cOBS                   := 'REAJUSTE MANUAL' ;
  objForm.edtLimiteCredito.Value := qryMasternValLimiteCred.Value ;
  objForm.edtPercEntrada.Value   := qryMasternPercEntrada.Value ;
  objForm.bAprovacao             := false ;
  objForm.bLimiteManual          := false ;
  showForm(objForm,true) ;

  PosicionaQuery(qryMaster, qryMasternCdTerceiro.asstring) ;

end;

procedure TfrmManutLimiteCliente.btnAuditoriaClick(Sender: TObject);
var
  objForm : TfrmTrilhaClienteVarejo;
begin

  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  objForm := TfrmTrilhaClienteVarejo.Create(nil);

  PosicionaQuery(objForm.qryLog, qryMasternCdTerceiro.asString);

  if (objForm.qryLog.Eof) then
  begin
      MensagemAlerta('Nenhum registro de altera��o para este cliente.') ;
      exit ;
  end ;

  objForm.qryLog.Last;
  showForm(objForm,true);

end;

procedure TfrmManutLimiteCliente.cxButton1Click(Sender: TObject);
var
  objForm : TfrmHistoricoLimiteCredito;
begin
  {inherited;}
  
  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  objForm := TfrmHistoricoLimiteCredito.Create(nil);

  PosicionaQuery(objForm.qryLimite, qryMasternCdTerceiro.AsString) ;
  showForm(objForm,true) ;

end;

procedure TfrmManutLimiteCliente.cxButton2Click(Sender: TObject);
var
    cMotivo : string ;
begin
  inherited;
  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  if (qryMasternValLimiteCred.Value <= 0) then
  begin
      MensagemErro('Cliente n�o tem limite ativo.') ;
      exit ;
  end ;

  InputQuery('Remover Limite','Motivo',cMotivo) ;

  if (MessageDlg('Deseja realmente remover o limite deste cliente ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
  
      SP_GERA_LIMITE_CREDITO.Close ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nCdTerceiro').Value    := qryMasternCdTerceiro.Value ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nValLimite').Value     := 0 ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@nPercEntrada').Value   := 0 ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cFlgAutomatico').Value := 0 ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cFlgAutomatico').Value := 0 ;
      SP_GERA_LIMITE_CREDITO.Parameters.ParamByName('@cOBS').Value           := Uppercase(cMotivo) ;
      SP_GERA_LIMITE_CREDITO.ExecProc ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Limite removido com sucesso.');

  PosicionaQuery(qryMaster, qryMasternCdTerceiro.asstring) ;


end;

initialization
    RegisterClass(TfrmManutLimiteCliente) ;

end.
