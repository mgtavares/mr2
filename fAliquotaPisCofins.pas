unit fAliquotaPisCofins;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxPC, GridsEh, DBGridEh, ADODB;

type
  TfrmAliquotaPisCofins = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryPisCofins: TADOQuery;
    dsPisCofins: TDataSource;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryPisCofinsnCdAliquotaPisCofins: TIntegerField;
    qryPisCofinsnCdEmpresa: TIntegerField;
    qryPisCofinsnCdLoja: TIntegerField;
    qryPisCofinsnValAliquotaPis: TBCDField;
    qryPisCofinsnValAliquotaCofins: TBCDField;
    qryPisCofinscNmEmpresa: TStringField;
    qryPisCofinscNmLoja: TStringField;
    qryLojacNmLoja: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryPisCofinsBeforePost(DataSet: TDataSet);
    procedure qryPisCofinsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAliquotaPisCofins: TfrmAliquotaPisCofins;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}
procedure TfrmAliquotaPisCofins.FormShow(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryPisCofins,IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then qryPisCofinsnCdLoja.ReadOnly := True;

end;

procedure TfrmAliquotaPisCofins.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        qryLoja.Close;

        if (qryPisCofins.State = dsBrowse) then
             qryPisCofins.Edit ;


        if (qryPisCofins.State = dsInsert) or (qryPisCofins.State = dsEdit) then
        begin

            if (DbGridEh1.Col = 1) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(8);

                If (nPK > 0) then
                    qryPisCofinsnCdEmpresa.Value := nPK ;
            end ;

            if (DbGridEh1.Col = 3) then
            begin
                if (frmMenu.LeParametro('VAREJO') = 'S') then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta(59);

                    If (nPK > 0) then qryPisCofinsnCdLoja.Value := nPK ;
                end;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAliquotaPisCofins.qryPisCofinsBeforePost(DataSet: TDataSet);
begin

  if (qryPisCofinsnCdEmpresa.Value = 0) then
  begin
      ShowMessage('Empresa inv�lida.') ;
      abort ;
  end ;

  if (qryPisCofinsnValAliquotaPis.Value < 0) then
  begin
      ShowMessage('Al�quota de Pis inv�lida.') ;
      abort ;
  end ;

  if (qryPisCofinsnValAliquotaCofins.Value < 0) then
  begin
      ShowMessage('Al�quota de Cofins inv�lida.') ;
      abort ;
  end ;

  if ((frmMenu.LeParametro('VAREJO') = 'S') and (qryPisCofinsnCdLoja.Value = 0)) then
  begin
      ShowMessage('Loja inv�lida.') ;
      abort ;
  end ;

  if (qryPisCofins.State = dsInsert) then
      qryPisCofinsnCdAliquotaPisCofins.Value := frmMenu.fnProximoCodigo('ALIQUOTAPISCOFINS') ;

  inherited;

end;

procedure TfrmAliquotaPisCofins.qryPisCofinsCalcFields(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryEmpresa, qryPisCofinsnCdEmpresa.asString) ;
  if not qryEmpresa.eof then
      qryPisCofinscNmEmpresa.Value := qryEmpresacNmEmpresa.Value;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryPisCofinscNmLoja.Value := '';
      qryLoja.Close;
      PosicionaQuery(qryLoja, qryPisCofinsnCdLoja.asString) ;
      if not qryLoja.eof then
          qryPisCofinscNmLoja.Value := qryLojacNmLoja.Value ;
  end;
end;

initialization
    RegisterClass(TfrmAliquotaPisCofins) ;

end.
