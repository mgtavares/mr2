unit fConciliaBancoManual_TituloMovimentado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ImgList, ComCtrls, ToolWin,
  ExtCtrls, ADODB;

type
  TfrmConciliaBancoManual_TituloMovimentado = class(TfrmProcesso_Padrao)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosnCdEmpresa: TStringField;
    qryTitulosnCdLojaTit: TStringField;
    qryTitulosnCdSP: TIntegerField;
    qryTitulosnCdRecebimento: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTituloscNrNF: TStringField;
    qryTitulosnCdTerceiro: TIntegerField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnValMov: TBCDField;
    dsTitulos: TDataSource;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn;
    cxGrid1DBTableView1nCdSP: TcxGridDBColumn;
    cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValMov: TcxGridDBColumn;
    qryTituloscNmEspTit: TStringField;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure exibeTitulos(nCdLanctoFin:integer) ;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaBancoManual_TituloMovimentado: TfrmConciliaBancoManual_TituloMovimentado;

implementation

uses fTitulo;

{$R *.dfm}

procedure TfrmConciliaBancoManual_TituloMovimentado.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmTitulo ;
begin
  inherited;

  if qryTitulos.Active and (qryTitulosnCdTitulo.Value > 0) then
  begin

      objForm := TfrmTitulo.Create( Self ) ;

      objForm.PosicionaQuery(objForm.qryMaster,qryTitulosnCdTitulo.asString) ;

      objForm.btIncluir.Visible   := False ;
      objForm.btSalvar.Visible    := False ;
      objForm.btConsultar.Visible := False ;
      objForm.btExcluir.Visible   := False ;
      objForm.btCancelar.Visible  := False ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmConciliaBancoManual_TituloMovimentado.exibeTitulos(
  nCdLanctoFin: integer);
begin

    qryTitulos.Close;
    PosicionaQuery(qryTitulos, intTostr(nCdLanctoFin)) ;

    Self.ShowModal ;

    qryTitulos.Close;

end;

end.
