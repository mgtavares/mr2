unit fCaixa_AlteraCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxButtons, jpeg, ExtCtrls;

type
  TfrmCaixa_AlteraCartao = class(TForm)
    Image2: TImage;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    edtFormaPagto_Atual: TcxTextEdit;
    Label1: TLabel;
    edtCondPagto_Atual: TcxTextEdit;
    Label2: TLabel;
    Label8: TLabel;
    edtDocto_Atual: TcxTextEdit;
    edtAutorizacao_Atual: TcxTextEdit;
    Label9: TLabel;
    edtOperadora_Atual: TcxTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtFormaPagto_Nova: TcxTextEdit;
    Label5: TLabel;
    edtCondPagto_Nova: TcxTextEdit;
    Label6: TLabel;
    Label7: TLabel;
    edtDocto_Novo: TcxTextEdit;
    edtAutorizacao_Nova: TcxTextEdit;
    Label10: TLabel;
    edtOperadora_Nova: TcxTextEdit;
    Label11: TLabel;
    Label12: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_AlteraCartao: TfrmCaixa_AlteraCartao;

implementation

{$R *.dfm}

end.
