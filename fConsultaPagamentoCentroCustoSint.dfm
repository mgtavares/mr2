inherited frmConsultaPagamentoCentroCustoSint: TfrmConsultaPagamentoCentroCustoSint
  Left = 80
  Top = 115
  Width = 1060
  Height = 573
  Caption = 'Consulta Pagamento Centro Custo Sint'#233'tico'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1044
    Height = 508
  end
  inherited ToolBar1: TToolBar
    Width = 1044
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1044
    Height = 508
    Align = alClient
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGrid1DBTableView1nValCC
        end
        item
          Format = '#,##0.00'
          Column = cxGrid1DBTableView1nValCC
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValCC
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clWhite
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.GroupRowStyle = grsOffice11
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGrid1DBTableView1cNmCC2: TcxGridDBColumn
        Caption = 'C.Custo N'#237'vel 1'
        DataBinding.FieldName = 'cNmCC2'
        Visible = False
        GroupIndex = 0
      end
      object cxGrid1DBTableView1cNmCC3: TcxGridDBColumn
        Caption = 'C.Custo N'#237'vel 2'
        DataBinding.FieldName = 'cNmCC3'
        Visible = False
        GroupIndex = 1
      end
      object cxGrid1DBTableView1nCdCC: TcxGridDBColumn
        Caption = 'C'#243'd.C.Custo'
        DataBinding.FieldName = 'nCdCC'
        Visible = False
        Width = 71
      end
      object cxGrid1DBTableView1cNmCC: TcxGridDBColumn
        Caption = 'Centro de Custo'
        DataBinding.FieldName = 'cNmCC'
        Width = 469
      end
      object cxGrid1DBTableView1nValCC: TcxGridDBColumn
        Caption = 'Valor C.Custo'
        DataBinding.FieldName = 'nValCC'
      end
      object cxGrid1DBTableView1nValPercNivelAnt: TcxGridDBColumn
        Caption = '% s/N'#237'vel Ant.'
        DataBinding.FieldName = 'nValPercNivelAnt'
        Width = 117
      end
      object cxGrid1DBTableView1nValPercTotalGer: TcxGridDBColumn
        Caption = '% s/Total Geral '
        DataBinding.FieldName = 'nValPercTotalGer'
        Width = 121
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 776
    Top = 144
  end
  object uspConsulta: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_CONSULTA_PAGAMENTOS_CENTROCUSTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmiteCCNivel2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmiteCCNivel3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmiteCCZerado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 576
    Top = 336
    object uspConsultanCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object uspConsultaiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object uspConsultacNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 150
    end
    object uspConsultanValCC: TBCDField
      FieldName = 'nValCC'
      DisplayFormat = '#,##0.00'
      Precision = 18
      Size = 2
    end
    object uspConsultanCdOrdem: TIntegerField
      FieldName = 'nCdOrdem'
    end
    object uspConsultacCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 10
    end
    object uspConsultanValPercNivelAnt: TBCDField
      FieldName = 'nValPercNivelAnt'
      DisplayFormat = '#,##0.00'
      Precision = 6
      Size = 2
    end
    object uspConsultanValPercTotalGer: TBCDField
      FieldName = 'nValPercTotalGer'
      DisplayFormat = '#,##0.00'
      Precision = 6
      Size = 2
    end
    object uspConsultanCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object uspConsultanCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object uspConsultanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object uspConsultacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object uspConsultacNmCC2: TStringField
      FieldName = 'cNmCC2'
      Size = 50
    end
    object uspConsultacNmCC3: TStringField
      FieldName = 'cNmCC3'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = uspConsulta
    Left = 624
    Top = 336
  end
end
