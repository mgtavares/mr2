unit fSitSincServidor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB;

type
  TfrmSitSincServidor = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    edtSituacao: TEdit;
    btAcao: TcxButton;
    qryParametro: TADOQuery;
    qryAtualizaParametro: TADOQuery;
    qryParametrocValor: TStringField;
    procedure atualizaSituacao;
    procedure btAcaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSitSincServidor: TfrmSitSincServidor;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmSitSincServidor }

procedure TfrmSitSincServidor.atualizaSituacao;
begin

    qryParametro.Close;
    qryParametro.Open;

    {--replicação pausada--}
    if (qryParametrocValor.Value =  'S') then
    begin
        edtSituacao.Text := '*** PAUSADA ***' ;
        btAcao.Caption   := 'Ativar' ;
    end
    else
    begin
        edtSituacao.Text := 'EM PROCESSAMENTO' ;
        btAcao.Caption   := 'Pausar' ;
    end ;

end;

procedure TfrmSitSincServidor.btAcaoClick(Sender: TObject);
begin
  inherited;

  if (edtSituacao.Text = 'EM PROCESSAMENTO') then
  begin

      if (MessageDlg('Confirma a pausa na sincronização dos servidores ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          qryAtualizaParametro.Close;
          qryAtualizaParametro.Parameters.ParamByName('cValor').Value := 'S' ;
          qryAtualizaParametro.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          raise;
      end ;

      frmMenu.Connection.CommitTrans;

  end
  else
  begin

      if (MessageDlg('Confirma a ativação na sincronização dos servidores ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          qryAtualizaParametro.Close;
          qryAtualizaParametro.Parameters.ParamByName('cValor').Value := 'N' ;
          qryAtualizaParametro.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          raise;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  atualizaSituacao;

end;

procedure TfrmSitSincServidor.FormShow(Sender: TObject);
begin
  inherited;

  atualizaSituacao;

end;

initialization
    RegisterClass(TfrmSitSincServidor) ;

end.
