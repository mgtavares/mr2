inherited frmAdmListaCobranca_Listas_SelLista: TfrmAdmListaCobranca_Listas_SelLista
  Left = 168
  Top = 212
  Width = 1039
  Caption = 'Sele'#231#227'o de Lista de Cobran'#231'a'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1031
  end
  inherited ToolBar1: TToolBar
    Width = 1031
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1031
    Height = 440
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsListas
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdListaCobranca'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtCobranca'
        Footers = <>
        Width = 124
      end
      item
        EditButtons = <>
        FieldName = 'dDtVencto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iSeq'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmListaCobranca'
        Footers = <>
        Width = 235
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteTotal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteRestante'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 205
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryListas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdListaCobrancaAtual'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja               int'
      '       ,@nCdListaCobrancaAtual int'
      ''
      'Set @nCdLoja               = :nCdLoja'
      'Set @nCdListaCobrancaAtual = :nCdListaCobrancaAtual'
      ''
      'SELECT nCdListaCobranca'
      '      ,dDtCobranca'
      '      ,dDtVencto'
      '      ,iSeq'
      '      ,cNmListaCobranca  '
      '      ,iQtdeClienteTotal'
      
        '      ,(iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContat' +
        'o) iQtdeClienteRestante'
      '      ,cNmUsuario'
      '  FROM ListaCobranca'
      
        '       LEFT JOIN Usuario ON Usuario.nCdUsuario = ListaCobranca.n' +
        'CdUsuarioResp'
      ' WHERE ListaCobranca.nCdEmpresa       = :nPK'
      
        '   AND ((ListaCobranca.nCdLoja          = @nCdLoja) OR (@nCdLoja' +
        ' = 0))'
      '   AND ListaCobranca.nCdListaCobranca  <> @nCdListaCobrancaAtual'
      ' ORDER BY 2,7'
      '')
    Left = 184
    Top = 112
    object qryListasnCdListaCobranca: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|C'#243'd'
      FieldName = 'nCdListaCobranca'
    end
    object qryListasdDtCobranca: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Data Cobran'#231'a'
      FieldName = 'dDtCobranca'
    end
    object qryListasdDtVencto: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Data Vencto.'
      FieldName = 'dDtVencto'
    end
    object qryListasiSeq: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Seq'
      FieldName = 'iSeq'
    end
    object qryListascNmListaCobranca: TStringField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Nome Lista'
      FieldName = 'cNmListaCobranca'
      Size = 50
    end
    object qryListasiQtdeClienteTotal: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Total Clientes'
      FieldName = 'iQtdeClienteTotal'
    end
    object qryListasiQtdeClienteRestante: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Contatos Restantes'
      FieldName = 'iQtdeClienteRestante'
      ReadOnly = True
    end
    object qryListascNmUsuario: TStringField
      DisplayLabel = 'Listas de Cobran'#231'a Dispon'#237'veis|Analista Cobran'#231'a'
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsListas: TDataSource
    DataSet = qryListas
    Left = 216
    Top = 112
  end
end
