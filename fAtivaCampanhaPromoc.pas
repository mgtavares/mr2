unit fAtivaCampanhaPromoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, Menus, DBGridEhGrouping;

type
  TfrmAtivaCampanhaPromoc = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdCampanhaPromoc: TIntegerField;
    qryMastercChaveCampanha: TStringField;
    qryMastercNmCampanhaPromoc: TStringField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasterdDtValidadeFim: TDateTimeField;
    qryMastercNmTipoTabPreco: TStringField;
    DBGridEh1: TDBGridEh;
    dsMaster: TDataSource;
    qryAux: TADOQuery;
    PopupMenu1: TPopupMenu;
    AtivarCampanha1: TMenuItem;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure AtivarCampanha1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAtivaCampanhaPromoc: TfrmAtivaCampanhaPromoc;

implementation

uses fMenu, fCampanhaPromoc;

{$R *.dfm}

procedure TfrmAtivaCampanhaPromoc.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryMaster.Close ;
  PosicionaQuery(qryMaster, intToStr(frmMenu.nCdEmpresaAtiva)) ;
  
end;

procedure TfrmAtivaCampanhaPromoc.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  ToolButton1.Click;

end;

procedure TfrmAtivaCampanhaPromoc.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmCampanhaPromoc;
begin
  inherited;

  if qryMaster.Active and (not qryMaster.eof) then
  begin

      objForm := TfrmCampanhaPromoc.Create(nil);

      try
          try

              objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
              objForm.PosicionaQuery(objForm.qryMaster, qryMasternCdCampanhaPromoc.AsString) ;

              objForm.ShowModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objForm);
      end;
  end ;

end;

procedure TfrmAtivaCampanhaPromoc.AtivarCampanha1Click(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (not qryMaster.eof) then
  begin
      if (MessageDlg('Confirma a ativa��o desta campanha ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryAux.Close ;
      qryAux.SQL.clear ;
      qryAux.SQL.Add('UPDATE CampanhaPromoc SET cFlgAtivada = 1 , dDtAtivacao = GetDate(), nCdUsuarioAtivacao = ' + intToStr(frmMenu.nCdUsuarioLogado) + ' WHERE nCdCampanhaPromoc = ' + qryMasternCdCampanhaPromoc.AsString) ;

      try
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      ShowMessage('Campanha Ativada com Sucesso.') ;

      ToolButton1.Click;

  end ;

end;

initialization
    RegisterClass(TfrmAtivaCampanhaPromoc) ;
    
end.
