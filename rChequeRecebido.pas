unit rChequeRecebido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptChequeRecebido = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    edtDtEmissaoIni: TMaskEdit;
    Label6: TLabel;
    edtDtEmissaoFim: TMaskEdit;
    edtEmpresa: TMaskEdit;
    edtContaBancaria: TMaskEdit;
    Label2: TLabel;
    edtDtVenctoIni: TMaskEdit;
    Label4: TLabel;
    edtDtVenctoFim: TMaskEdit;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdEmpresa: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DBEdit1: TDBEdit;
    DataSource6: TDataSource;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    edtTerceiro: TMaskEdit;
    Label7: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    edtLoja: TMaskEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit9: TDBEdit;
    DataSource8: TDataSource;
    Label9: TLabel;
    edtCNPJEmissor: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptChequeRecebido: TrptChequeRecebido;

implementation

uses fMenu, fLookup_Padrao, rChequeEmitido_view, rChequeRecebido_view;

{$R *.dfm}

procedure TrptChequeRecebido.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  edtEmpresa.SetFocus ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;


procedure TrptChequeRecebido.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;

  if (Trim(edtEmpresa.Text) <> '') then
  begin

      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := edtEmpresa.Text ;
      qryEmpresa.Open ;
    
  end ;

end;

procedure TrptChequeRecebido.edtContaBancariaExit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close;

  If (Trim(edtContaBancaria.Text) <> '') then
  begin

    if (DBEdit3.Text = '') then
    begin
        MensagemAlerta('Selecione uma empresa.') ;
        edtContaBancaria.Text := '' ;
        edtEmpresa.SetFocus;
        abort ;
    end ;

    if (DBEdit9.Text = '') and (not edtLoja.ReadOnly) then
    begin
        MensagemAlerta('Selecione uma loja.') ;
        edtContaBancaria.Text := '' ;
        edtLoja.SetFocus;
        abort ;
    end ;

    qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text) ;
    qryContaBancaria.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(edtLoja.Text) ;

    PosicionaQuery(qryContaBancaria, edtContaBancaria.Text) ;

  end ;

end;

procedure TrptChequeRecebido.ToolButton1Click(Sender: TObject);
var
    cFiltro : String ;
    objRel  : TrptChequeRecebido_view ;
begin

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Selecione uma empresa.') ;
      edtEmpresa.SetFocus;
      abort ;
  end ;

  objRel := TrptChequeRecebido_view.Create( Self ) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value                := frmMenu.ConvInteiro(edtEmpresa.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLojaPortadora').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdContaBancariaPortadora').Value := frmMenu.ConvInteiro(edtContaBancaria.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiroResponsavel').Value    := frmMenu.ConvInteiro(edtTerceiro.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtEmissaoIni').Value             := frmMenu.ConvData(edtDtEmissaoIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtEmissaoFim').Value             := frmMenu.ConvData(edtDtEmissaoFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtVenctoIni').Value              := frmMenu.ConvData(edtDtVenctoIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtVenctoFim').Value              := frmMenu.ConvData(edtDtVenctoFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTabStatusCheque').Value        := RadioGroup1.ItemIndex;
  objRel.usp_Relatorio.Parameters.ParamByName('@cFlgOrdenacao').Value             := RadioGroup2.ItemIndex;
  objRel.usp_Relatorio.Parameters.ParamByName('@cCNPJCPF').Value                  := trim( edtCNPJEmissor.Text );
  objRel.usp_Relatorio.Open  ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  cFiltro := 'Filtros: Empresa: ' + Trim(edtEmpresa.Text) + ' - ' + Trim(DBEdit3.Text) ;

  if (DBEdit9.Text <> '') then
      cFiltro := cFiltro + '/ Loja Portadora: ' + Trim(edtLoja.Text) + ' - ' + DBEdit9.Text;

  if (DBEdit6.Text <> '') then
      cFiltro := cFiltro + '/ Conta Portadora: ' + DBEdit1.Text + ' - ' + DBEdit4.Text + ' - ' + Trim(DBEdit5.Text) + ' - ' + DBEdit6.Text ;

  if (DBEdit8.Text <> '') then
      cFiltro := cFiltro + '/ Terceiro Respons�vel: ' + Trim(edtTerceiro.Text) + ' - ' + DBEdit8.Text ;

  if (trim( edtCNPJEmissor.Text ) <> '') then
      cFiltro := cFiltro + '/ CNPJ/CPF Emissor: ' + Trim(edtCNPJEmissor.Text) ;

  cFiltro := cFiltro + '/ Per�odo Emiss�o: '    + Trim(edtDtEmissaoIni.Text)  + ' - ' + Trim(edtDtEmissaoFim.Text) ;
  cFiltro := cFiltro + '/ Per�odo Vencimento: ' + Trim(edtDtVenctoIni.Text)   + ' - ' + Trim(edtDtVenctoFim.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      cFiltro := cFiltro + '/ Situa��o: TODOS' ;

  if (RadioGroup1.ItemIndex = 1) then
      cFiltro := cFiltro + '/ Situa��o: DISPONIVEL PARA DEPOSITO' ;

  if (RadioGroup1.ItemIndex = 2) then
      cFiltro := cFiltro + '/ Situa��o: DEPOSITADO' ;

  if (RadioGroup1.ItemIndex = 3) then
      cFiltro := cFiltro + '/ Situa��o: COMPENSADO' ;

  if (RadioGroup1.ItemIndex = 4) then
      cFiltro := cFiltro + '/ Situa��o: NEGOCIADO' ;

  if (RadioGroup1.ItemIndex = 5) then
      cFiltro := cFiltro + '/ Situa��o: PRIMEIRA DEVOLUCAO' ;

  if (RadioGroup1.ItemIndex = 6) then
      cFiltro := cFiltro + '/ Situa��o: SEGUNDA DEVOLUCAO' ;

  if (RadioGroup1.ItemIndex = 7) then
      cFiltro := cFiltro + '/ Situa��o: DEVOLVIDO PARA EMISSOR' ;

  if (RadioGroup1.ItemIndex = 8) then
      cFiltro := cFiltro + '/ Situa��o: ENVIADO PARA LOJA' ;

  if (RadioGroup1.ItemIndex = 9) then
      cFiltro := cFiltro + '/ Situa��o: DEPOSITADO EM CUSTODIA' ;

  if (RadioGroup1.ItemIndex = 10) then
      cFiltro := cFiltro + '/ Situa��o: ENVIADO PARA PROTESTO' ;


  if (RadioGroup2.ItemIndex = 0) then
      cFiltro := cFiltro + '/ Ordena��o: N�mero Cheque' ;

  if (RadioGroup2.ItemIndex = 1) then
      cFiltro := cFiltro + '/ Ordena��o: Data Emiss�o' ;

  if (RadioGroup2.ItemIndex = 2) then
      cFiltro := cFiltro + '/ Ordena��o: Data Vencimento' ;

  objRel.lblFiltro.Caption := cFiltro ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptChequeRecebido.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            edtEmpresa.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptChequeRecebido.edtContaBancariaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma empresa.') ;
            edtContaBancaria.Text := '' ;
            edtEmpresa.SetFocus;
            abort ;
        end ;

        if not (edtLoja.ReadOnly) then
        begin

            if (DBEdit9.Text = '') then
            begin
                MensagemAlerta('Selecione uma loja.') ;
                edtContaBancaria.Text := '' ;
                edtLoja.SetFocus;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + edtEmpresa.Text + ' AND ContaBancaria.nCdLoja = ' + edtLoja.Text);

        end
        else
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + edtEmpresa.Text);

        end ;

        If (nPK > 0) then
        begin
            edtContaBancaria.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptChequeRecebido.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text);
  
end;

procedure TrptChequeRecebido.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
  
end;

procedure TrptChequeRecebido.edtLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(86) ;

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptChequeRecebido.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
            edtTerceiro.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptChequeRecebido) ;

end.
