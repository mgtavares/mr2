unit fMonitorTransferencia_Item;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DB, ADODB, GridsEh, DBGridEh, StdCtrls,
  Mask, DBCtrls;

type
  TfrmMonitorTransferencia_Item = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemTransfEst: TADOQuery;
    qryItemTransfEstnCdItemTransfEst: TIntegerField;
    qryItemTransfEstnCdProduto: TIntegerField;
    qryItemTransfEstcNmProduto: TStringField;
    qryItemTransfEstnQtde: TBCDField;
    qryItemTransfEstnQtdeConf: TBCDField;
    qryItemTransfEstnValTotal: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsItemTransfEst: TDataSource;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTransfEst : Integer;
  end;

var
  frmMonitorTransferencia_Item: TfrmMonitorTransferencia_Item;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmMonitorTransferencia_Item.FormShow(Sender: TObject);
begin
  inherited;

  qryItemTransfEst.Close;
  PosicionaQuery(qryItemTransfEst, IntToStr(nCdTransfEst));
end;

end.
