unit rEmitePisCofinsVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB;

type
  TfrmEmitePisCofinsVendas = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label9: TLabel;
    MaskEdit7: TMaskEdit;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmitePisCofinsVendas: TfrmEmitePisCofinsVendas;

implementation

uses fLookup_Padrao, fMenu, rEmitePisCofinsVendas_View, rMovTit_view,
  rEmitePisCofinsVendasSint_View;

{$R *.dfm}

procedure TfrmEmitePisCofinsVendas.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmEmitePisCofinsVendas.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TfrmEmitePisCofinsVendas.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmEmitePisCofinsVendas.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit7.Text ;
    qryLoja.Open ;
  end ;

end;

procedure TfrmEmitePisCofinsVendas.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit7.Enabled := False ;

end;

procedure TfrmEmitePisCofinsVendas.FormShow(Sender: TObject);
var
iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit7.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit7.text) ;
  end
  else
  begin
      MasKEdit7.ReadOnly := True ;
      MasKEdit7.Color    := $00E9E4E4 ;
  end ;

  RadioButton1.Checked := True;
  MaskEdit3.SetFocus ;

end;

procedure TfrmEmitePisCofinsVendas.ToolButton1Click(Sender: TObject);
var
   objRel : TrEmitePisCofins_view;
   objRelSin : TrEmitePisCofinsSint_view;

begin
  inherited;
  qryEmpresa.Close ;
  If (Trim(MaskEdit3.Text) <> '') then
  begin
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;
  if qryEmpresa.Eof then MaskEdit3.Text := '';

  qryLoja.Close ;
  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit7.Text ;
    qryLoja.Open ;
  end ;
  if qryLoja.Eof then MaskEdit7.Text := '';

  if (RadioButton1.Checked) then
  begin

      objRel := TrEmitePisCofins_view.Create(nil);
      Try
          Try
              objRel.uspRelatorio.Close ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(MaskEdit3.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;

              if (frmMenu.LeParametro('VAREJO') = 'N') then
                  objRel.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value       := 0
              else objRel.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(MaskEdit7.Text) ;

              objRel.uspRelatorio.Open  ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              objRel.lblFiltro1.Caption := '';

              if (Trim(MaskEdit3.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + 'Empresa : ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

              if ((Trim(MaskEdit1.Text) <> '/  /') or (Trim(MaskEdit2.Text) <> '/  /')) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo : ' + Trim(MaskEdit1.Text) + '-' + String(MaskEdit2.Text) ;

              if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit7.Text)) <> '')) then
                  objRel.lblFiltro1.Caption :=  objRel.lblFiltro1.Caption + ' / Loja : ' + Trim(MaskEdit7.Text) + '-' + DbEdit1.Text;

              objRel.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      Finally;
          FreeAndNil(objRel);
      end;
  end;

  if (RadioButton2.Checked) then
  begin

      objRelSin := TrEmitePisCofinsSint_view.Create(nil);
      Try
          Try
              objRelSin.uspRelatorio.Close ;
              objRelSin.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(MaskEdit3.Text) ;
              objRelSin.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text) ;
              objRelSin.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text) ;
              objRelSin.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;

              if (frmMenu.LeParametro('VAREJO') = 'N') then
                  objRelSin.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value       := 0
              else objRelSin.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(MaskEdit7.Text) ;

              objRelSin.uspRelatorio.Open  ;

              objRelSin.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              objRelSin.lblFiltro1.Caption := '';

              if (Trim(MaskEdit3.Text) <> '') then
                  objRelSin.lblFiltro1.Caption := objRelSin.lblFiltro1.Caption + 'Empresa : ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

              if ((Trim(MaskEdit1.Text) <> '/  /') or (Trim(MaskEdit2.Text) <> '/  /')) then
                  objRelSin.lblFiltro1.Caption := objRelSin.lblFiltro1.Caption + ' / Per�odo : ' + Trim(MaskEdit1.Text) + '-' + String(MaskEdit2.Text) ;

              if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit7.Text)) <> '')) then
                  objRelSin.lblFiltro1.Caption :=  objRelSin.lblFiltro1.Caption + ' / Loja : ' + Trim(MaskEdit7.Text) + '-' + DbEdit1.Text;

              objRelSin.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      Finally;
          FreeAndNil(objRel);
      end;

  end;

end;

initialization
    RegisterClass(TfrmEmitePisCofinsVendas) ;

end.
