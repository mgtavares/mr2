inherited frmGerarGradeSimples: TfrmGerarGradeSimples
  Left = 368
  Top = 319
  Width = 302
  Height = 110
  Caption = 'Grade Simples'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 286
    Height = 45
  end
  object Label4: TLabel [1]
    Left = 136
    Top = 48
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Venda'
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 48
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Custo'
  end
  inherited ToolBar1: TToolBar
    Width = 286
    ButtonWidth = 94
    inherited ToolButton1: TToolButton
      Caption = 'Gerar Grade'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 94
    end
    inherited ToolButton2: TToolButton
      Left = 102
    end
  end
  object MaskEdit4: TMaskEdit [4]
    Left = 200
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 2
  end
  object MaskEdit3: TMaskEdit [5]
    Left = 64
    Top = 40
    Width = 65
    Height = 21
    BiDiMode = bdRightToLeft
    ParentBiDiMode = False
    TabOrder = 1
  end
  object SP_GERA_GRADE_SIMPLES: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_GRADE_SIMPLES;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nValCusto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end
      item
        Name = '@nValVenda'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end>
    Left = 216
    Top = 16
  end
end
