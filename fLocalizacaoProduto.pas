unit fLocalizacaoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, GridsEh, DBGridEh, DB, DBCtrls, ADODB, DBGridEhGrouping;

type
  TfrmLocalizacaoProduto = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    edtLocalEstoque: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtGrupoInventario: TMaskEdit;
    Label3: TLabel;
    edtGrupoProduto: TMaskEdit;
    edtNmProduto: TEdit;
    Label4: TLabel;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryGrupoInventario: TADOQuery;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    RadioGroup1: TRadioGroup;
    qryResultado: TADOQuery;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocReferencia: TStringField;
    qryResultadocNmProduto: TStringField;
    dsResultado: TDataSource;
    procedure edtLocalEstoqueExit(Sender: TObject);
    procedure edtGrupoInventarioExit(Sender: TObject);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLocalEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoInventarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLocalizacaoProduto: TfrmLocalizacaoProduto;

implementation

uses fLookup_Padrao, fMenu, fLocalizacaoProduto_Vinculo;

{$R *.dfm}

procedure TfrmLocalizacaoProduto.edtLocalEstoqueExit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
  
end;

procedure TfrmLocalizacaoProduto.edtGrupoInventarioExit(Sender: TObject);
begin
  inherited;

  qryGrupoInventario.Close ;
  PosicionaQuery(qryGrupoInventario, edtGrupoInventario.Text) ;
  
end;

procedure TfrmLocalizacaoProduto.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TfrmLocalizacaoProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdLocalEstoque').Value    := frmMenu.ConvInteiro(edtLocalEstoque.Text) ;
  qryResultado.Parameters.ParamByName('nCdGrupoInventario').Value := frmMenu.ConvInteiro(edtGrupoInventario.Text) ;
  qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value    := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
  qryResultado.Parameters.ParamByName('cNmProduto').Value         := edtNmProduto.Text ;
  qryResultado.Parameters.ParamByName('cFlgTipo').Value           := RadioGroup1.ItemIndex ;
  qryResultado.Open ;

  if qryResultado.eof then
      MensagemAlerta('Nenhum produto encontrado para o crit�rio utilizado.') ;

end;

procedure TfrmLocalizacaoProduto.edtLocalEstoqueKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

        If (nPK > 0) then
        begin
            edtLocalEstoque.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLocalizacaoProduto.edtGrupoInventarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(153);

        If (nPK > 0) then
        begin
            edtGrupoInventario.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLocalizacaoProduto.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            edtGrupoProduto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLocalizacaoProduto.FormShow(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmLocalizacaoProduto.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmLocalizacaoProduto_Vinculo;
begin
  inherited;

  if not qryResultado.eof then
  begin
      objForm := TfrmLocalizacaoProduto_Vinculo.Create(nil);

      objForm.nCdProduto := qryResultadonCdProduto.Value ;
      objForm.ShowModal;
  end ;

end;

initialization
    RegisterClass(TfrmLocalizacaoProduto) ;

end.
