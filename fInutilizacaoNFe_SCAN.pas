unit fInutilizacaoNFe_SCAN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, ER2Lookup, DBCtrls, StdCtrls, Mask, pcnConversao;

type
  TfrmInutilizacaoNFe_SCAN = class(TfrmProcesso_Padrao)
    qrySerieFiscal: TADOQuery;
    qrySerieFiscalnCdSerieFiscal: TIntegerField;
    qrySerieFiscalcModelo: TStringField;
    qrySerieFiscalcSerie: TStringField;
    qryNFeInutilizada: TADOQuery;
    edtNumInicial: TMaskEdit;
    edtNumFinal: TMaskEdit;
    cJustificativa: TEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    edtCodSerie: TER2LookupMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    SP_INUTILIZA_NFE: TADOStoredProc;
    qryNFeInutilizadanCdNFeInutilizada: TIntegerField;
    qryNFeInutilizadanCdEmpresa: TIntegerField;
    qryNFeInutilizadacCNPJEmitente: TStringField;
    qryNFeInutilizadacJustificativa: TStringField;
    qryNFeInutilizadaiAno: TIntegerField;
    qryNFeInutilizadanCdSerieFiscal: TIntegerField;
    qryNFeInutilizadacSerie: TStringField;
    qryNFeInutilizadacModelo: TStringField;
    qryNFeInutilizadadDtInutilizacao: TDateTimeField;
    qryNFeInutilizadanCdUsuarioInutilizacao: TIntegerField;
    qryNFeInutilizadacNrProtocoloInutilizacao: TStringField;
    qryNFeInutilizadaiNrInicial: TIntegerField;
    qryNFeInutilizadaiNrFinal: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInutilizacaoNFe_SCAN: TfrmInutilizacaoNFe_SCAN;

implementation

uses fMenu, fEmiteNFe2;

{$R *.dfm}

procedure TfrmInutilizacaoNFe_SCAN.ToolButton1Click(Sender: TObject);
var
  objForm           : TfrmEmiteNFe2;
  nCdNFeInutilizada : integer;
begin
  inherited;

  edtCodSerie.PosicionaQuery;

  if ((not qrySerieFiscal.Active) or (Trim(qrySerieFiscalcSerie.Text) = '')) then
  begin
      MensagemAlerta('Selecione o C�digo da S�rie.');
      edtCodSerie.SetFocus;
      abort;
  end;

  if (Length(Trim(cJustificativa.Text)) < 15) then
  begin
      MensagemAlerta('A justificativa para inutiliza��o deve conter 15 caracteres ou mais.');
      cJustificativa.SetFocus;
      abort;
  end;

  if (Trim(edtNumInicial.Text) = '') then
  begin
      MensagemAlerta('Digite o N�mero inicial.');
      edtNumInicial.SetFocus;
      abort;
  end;

  if (Trim(edtNumFinal.Text) = '') then
  begin
      MensagemAlerta('Digite o N�mero final.');
      edtNumFinal.SetFocus;
      abort;
  end;

  if (StrToInt(Trim(edtNumInicial.Text)) > StrToInt(Trim(edtNumFinal.Text))) then
  begin
      MensagemAlerta('N�mero da NFe Inicial n�o pode ser Maior que o Final.');
      edtNumInicial.SetFocus;
      abort;
  end;

  if ((StrToInt(Trim(edtNumFinal.Text)) - StrToInt(Trim(edtNumInicial.Text))) > 9) then
  begin
      MensagemAlerta('N�o � poss�vel inutilizar um intervalo maior que 10 n�meros.');
      edtNumInicial.SetFocus;
      abort;
  end;

  if (MessageDLG('A sequ�ncia de ' + Trim(edtNumInicial.Text) + ' at� ' + Trim(edtNumFinal.Text) + ' ser� inutilizada. Esta a��o n�o poder� ser desfeita, Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;


  objForm := TfrmEmiteNFe2.Create(nil);

  frmMenu.Connection.BeginTrans;

  try
      try

          SP_INUTILIZA_NFE.Close;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@nCdEmpresa').Value             := frmMenu.nCdEmpresaAtiva ;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@cJustificativa').Value         := Trim(cJustificativa.Text) ;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@nCdSerieFiscal').Value         := qrySerieFiscalnCdSerieFiscal.Value ;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@iNrInicial').Value             := frmMenu.ConvInteiro(edtNumInicial.Text) ;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@iNrFinal').Value               := frmMenu.ConvInteiro(edtNumFinal.Text) ;
          SP_INUTILIZA_NFE.Parameters.ParamByName('@nCdUsuarioInutilizacao').Value := frmMenu.nCdUsuarioLogado ;
          SP_INUTILIZA_NFE.ExecProc;

          nCdNFeInutilizada := SP_INUTILIZA_NFE.Parameters.ParamByName('@nCdNFeInutilizada').Value;

          qryNFeInutilizada.Close;
          PosicionaQuery(qryNFeInutilizada, IntToStr(nCdNFeInutilizada));

          objForm.preparaNFe;
          frmMenu.mensagemUsuario('Inutilizando sequ�ncia...');

          objForm.ACBrNFe1.Configuracoes.Geral.FormaEmissao := teSCAN;

          objForm.ACBrNFe1.WebServices.Inutiliza(qryNFeInutilizadacCNPJEmitente.Value
                                                ,qryNFeInutilizadacJustificativa.Value
                                                ,qryNFeInutilizadaiAno.Value
                                                ,StrToInt(Trim(qryNFeInutilizadacModelo.Value))
                                                ,StrToInt(Trim(qryNFeInutilizadacSerie.Value))
                                                ,qryNFeInutilizadaiNrInicial.Value
                                                ,qryNFeInutilizadaiNrFinal.Value);
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          raise;
      end;

      qryNFeInutilizada.Edit;
      qryNFeInutilizadacNrProtocoloInutilizacao.Value := objForm.ACBrNFe1.WebServices.Inutilizacao.Protocolo;
      qryNFeInutilizada.Post;

      frmMenu.Connection.CommitTrans;
      
  finally
      frmMenu.mensagemUsuario('');
      FreeAndNil(objForm);
  end;

  qryNFeInutilizada.Close;
  qrySerieFiscal.Close;
  cJustificativa.Text := '';
  edtNumInicial.Text  := '';
  edtNumFinal.Text    := '';
  edtCodSerie.Text    := '';

  ShowMessage('N�mera��o inutilizada com Sucesso');

end;

procedure TfrmInutilizacaoNFe_SCAN.FormShow(Sender: TObject);
begin
  inherited;

  edtCodSerie.SetFocus;
end;

initialization
  RegisterClass(TfrmInutilizacaoNFe_SCAN);

end.
