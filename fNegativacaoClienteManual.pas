unit fNegativacaoClienteManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmNegativacaoClienteManual = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMastercRG: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    qryMasterdDtNegativacao: TDateTimeField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTituloNegativado: TADOQuery;
    qryTituloNegativadocNrTit: TStringField;
    qryTituloNegativadocNmEspTit: TStringField;
    qryTituloNegativadodDtEmissao: TDateTimeField;
    qryTituloNegativadodDtVenc: TDateTimeField;
    qryTituloNegativadoiDiasAtraso: TIntegerField;
    qryTituloNegativadonValTit: TBCDField;
    qryTituloNegativadonSaldoTit: TBCDField;
    qryTituloNegativadonValLiq: TBCDField;
    qryTituloNegativadonValAbatimento: TBCDField;
    qryTituloNegativadonValJuro: TBCDField;
    qryTituloNegativadonCdLanctoFin: TIntegerField;
    qryTituloNegativadoiParcela: TIntegerField;
    qryTituloNegativadonCdTitulo: TIntegerField;
    dsTituloNegativado: TDataSource;
    DBGridEh2: TDBGridEh;
    qryTituloVencido: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    IntegerField1: TIntegerField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    dsTituloVencido: TDataSource;
    btReabilitar: TcxButton;
    qryReabilitaCliente: TADOQuery;
    qryTituloNegativadocSigla: TStringField;
    qryTituloVencidocSigla: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btReabilitarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNegativacaoClienteManual: TfrmNegativacaoClienteManual;

implementation

uses fMenu, rTituloReabilitado_view;

{$R *.dfm}

procedure TfrmNegativacaoClienteManual.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TERCEIRO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 200 ;

end;

procedure TfrmNegativacaoClienteManual.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryTituloNegativado.Close;
  PosicionaQuery(qryTituloNegativado, qryMasternCdTerceiro.AsString) ;

  qryTituloVencido.Close;
  PosicionaQuery(qryTituloVencido, qryMasternCdTerceiro.AsString) ;

  btReabilitar.Enabled := (qryMasterdDtNegativacao.AsString <> '') ;

  if (qryMaster.RecordCount > 0) and (qryMasterdDtNegativacao.AsString = '') then
      MensagemAlerta('Este cliente n�o est� negativado.') ;

end;

procedure TfrmNegativacaoClienteManual.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmNegativacaoClienteManual.qryMasterAfterClose(
  DataSet: TDataSet);
begin
  inherited;

  qryTituloNegativado.Close;
  qryTituloVencido.Close;

end;

procedure TfrmNegativacaoClienteManual.btReabilitarClick(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a reabilita��o deste cliente ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (qryTituloVencido.RecordCount > 0) then
      if (MessageDlg('O Cliente tem t�tulos vencidos. Tem certeza que deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryReabilitaCliente.Close;
      qryReabilitaCliente.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
      qryReabilitaCliente.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      qryReabilitaCliente.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Cliente reabilitado com sucesso.') ;

  rptTituloReabilitado_view.qryTitulos.Close;
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('dDtInicial').Value := DateToStr(Date);
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('dDtFinal').Value   := DateToStr(Date);
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value := frmMenu.nCdLojaAtiva;
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('nCdLoja').Value    := 0;
  rptTituloReabilitado_view.qryTitulos.Parameters.ParamByName('nCdTerceiro').Value:= 0;
  rptTituloReabilitado_view.qryTitulos.Open;

  rptTituloReabilitado_view.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  rptTituloReabilitado_view.lblFiltro1.Caption := 'Cliente : ' + frmMenu.ZeroEsquerda(qryMasternCdTerceiro.AsString,10) + '-' + qryMastercNmTerceiro.Value ;
  rptTituloReabilitado_view.lblFiltro1.Caption := rptTituloReabilitado_view.lblFiltro1.Caption + ' / Usu�rio : ' + frmMenu.cNmUsuarioLogado + ' - Reabilita��o Manual';

  rptTituloReabilitado_View.QuickRep1.PreviewModal;

  btCancelar.Click;

end;

initialization
    RegisterClass(TfrmNegativacaoClienteManual) ;

end.
