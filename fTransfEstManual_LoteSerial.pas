unit fTransfEstManual_LoteSerial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB;

type
  TfrmTransfEstManual_LoteSerial = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemLoteSerial: TADOQuery;
    qryItemLoteSerialnCdItemTransfEst: TIntegerField;
    qryItemLoteSerialnCdProduto: TIntegerField;
    qryItemLoteSerialnQtde: TBCDField;
    qryItemLoteSerialnValUnitario: TBCDField;
    qryItemLoteSerialnValTotal: TBCDField;
    qryItemLoteSerialnCdTransfEst: TIntegerField;
    qryItemLoteSerialnQtdeConf: TBCDField;
    dsItemLoteSerial: TDataSource;
    qryItemLoteSerialcNmProduto: TStringField;
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
    nCdLocalEstoque         : integer;
    bReadOnly               : Boolean;
    nCdTransfEst            : integer;
    nCdTabTipoOrigemMovLote : integer;
    bPermiteCriar           : Boolean;
    bValidaLoteSerial       : Boolean;
  public
    { Public declarations }
    procedure ItemLoteSerial(nCdTransfEstAux,nCdTabTipoOrigemMovLoteAux,nCdLocalEstoqueAux : integer; bPermiteCriarAux, bReadOnlyAux : boolean; bValidaLoteSerialAux : boolean = False);
  end;

var
  frmTransfEstManual_LoteSerial: TfrmTransfEstManual_LoteSerial;

implementation

uses fRegistroMovLote;

{$R *.dfm}

procedure TfrmTransfEstManual_LoteSerial.DBGridEh1DblClick(
  Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryItemLoteSerialnCdProduto.Value
                         , 0
                         , nCdTabTipoOrigemMovLote {-- Tipo de movimentação que esta sendo realizada --}
                         , qryItemLoteSerialnCdItemTransfEst.Value
                         , nCdLocalEstoque
                         , qryItemLoteSerialnQtde.Value
                         , 'ITEM TRANSFERÊNCIA ENTRE ESTOQUES MANUAL No. ' + Trim( qryItemLoteSerialnCdItemTransfEst.AsString )
                         , bPermiteCriar
                         , bReadOnly
                         , bValidaLoteSerial) ;

  freeAndNil( objForm ) ;
end;

procedure TfrmTransfEstManual_LoteSerial.ItemLoteSerial(nCdTransfEstAux,
  nCdTabTipoOrigemMovLoteAux, nCdLocalEstoqueAux: integer; bPermiteCriarAux, bReadOnlyAux: boolean; bValidaLoteSerialAux : boolean);
begin

    nCdTransfEst            := nCdTransfEstAux;
    nCdTabTipoOrigemMovLote := nCdTabTipoOrigemMovLoteAux;
    nCdLocalEstoque         := nCdLocalEstoqueAux;
    bReadOnly               := bReadOnlyAux;
    bPermiteCriar           := bPermiteCriarAux;
    bValidaLoteSerial       := bValidaLoteSerialAux ;
    
    qryItemLoteSerial.Close;
    qryItemLoteSerial.Parameters.ParamByName('nPK').Value := nCdTransfEst;
    qryItemLoteSerial.Open;

    Self.ShowModal;

end;

end.
