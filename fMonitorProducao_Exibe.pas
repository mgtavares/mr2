unit fMonitorProducao_Exibe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, DBGridEhGrouping, Menus, ToolCtrlsEh;

type
  TfrmMonitorProducao_Exibe = class(TfrmProcesso_Padrao)
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidodDtIniProducao: TDateTimeField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonValPedido: TBCDField;
    qryPedidonSaldoFat: TBCDField;
    dsPedido: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryItens: TADOQuery;
    dsItens: TDataSource;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnSaldo: TBCDField;
    qryItenscNmTipoItemPed: TStringField;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntIni: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntFim: TcxGridDBColumn;
    cxGrid1DBTableView1dDtIniProducao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoFat: TcxGridDBColumn;
    EstiloMonitor: TcxStyleRepository;
    LinhaNormal: TcxStyle;
    LinhaDestaque: TcxStyle;
    qryPedidocNmGrupoEconomico: TStringField;
    cxGrid1DBTableView1cNmGrupoEconomico: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    PossibilidadeAtendimentoPedido1: TMenuItem;
    PossibilidadeAtendimentoCliente1: TMenuItem;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidonCdEmpresa: TStringField;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure exibePossibilidadeAtendimento( nCdPedido , nCdTerceiro : integer ) ;
    procedure PossibilidadeAtendimentoPedido1Click(Sender: TObject);
    procedure PossibilidadeAtendimentoCliente1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdEmpresa         : integer ;
    nCdTerceiro        : integer ;
    nCdGrupoEconomico  : integer ;
    nCdGrupoTerceiro   : integer ;
    bFlgPedidoNovo     : boolean ;
    bFlgPedidoIniciado : boolean ;
    bFlgPedidoParcial  : boolean ;
  end;

var
  frmMonitorProducao_Exibe: TfrmMonitorProducao_Exibe;

implementation

uses fMenu, rPicking, rPicking_Retrato, rPossibilidadeFaturamento_view, fMonitorProducao_Exibe_ItensPedido;

{$R *.dfm}

procedure TfrmMonitorProducao_Exibe.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryPedido.Close;
  qryPedido.Parameters.ParamByName('nCdUsuario').Value         := frmMenu.nCdUsuarioLogado;
  qryPedido.Parameters.ParamByName('nCdEmpresa').Value         := nCdEmpresa;
  qryPedido.Parameters.ParamByName('nCdTerceiro').Value        := nCdTerceiro;
  qryPedido.Parameters.ParamByName('nCdGrupoEconomico').Value  := nCdGrupoEconomico;
  qryPedido.Parameters.ParamByName('nCdGrupoTerceiro').Value   := nCdGrupoTerceiro;
  qryPedido.Parameters.ParamByName('cFlgPedidoNovo').Value     := 0;
  qryPedido.Parameters.ParamByName('cFlgPedidoIniciado').Value := 0;
  qryPedido.Parameters.ParamByName('cFlgPedidoParcial').Value  := 0;

  if (bFlgPedidoNovo) then
      qryPedido.Parameters.ParamByName('cFlgPedidoNovo').Value := 1;

  if (bFlgPedidoIniciado) then
      qryPedido.Parameters.ParamByName('cFlgPedidoIniciado').Value := 1;

  if (bFlgPedidoParcial) then
      qryPedido.Parameters.ParamByName('cFlgPedidoParcial').Value := 1;

  qryPedido.Open;

end;

procedure TfrmMonitorProducao_Exibe.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if not qryPedido.Active then
      exit ;

  if (qryPedidodDtIniProducao.asString <> '') then
  begin
      MensagemAlerta('Produ��o j� iniciada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o in�cio da produ��o deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryPedido.Edit ;
      qryPedidodDtIniProducao.Value := Now() ;
      qryPedido.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmMonitorProducao_Exibe.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VALMONPROD') = 'N') then
  begin
      cxGrid1DBTableView1nSaldoFat.Visible  := False ;
      cxGrid1DBTableView1nValPedido.Visible := False ;
  end ;

  ToolButton1.Click;

end;

procedure TfrmMonitorProducao_Exibe.ToolButton6Click(Sender: TObject);
var
  objTrptPicking         : TrptPicking;
  objTrptPicking_Retrato : TrptPicking_Retrato;
begin
  inherited;

  if (qryPedido.Active) and (qryPedidonCdPedido.Value > 0) then
  begin

      {--picking em paisagem--}
      if (frmMenu.LeParametro('TPPICKINGLIST') = 'P') then
      begin

          objTrptPicking := TrptPicking.Create(nil);

          try
              try
                  PosicionaQuery(objTrptPicking.qryPedido,qryPedidonCdPedido.AsString) ;

                  objTrptPicking.QuickRep1.Prepare;
                  objTrptPicking.QRTotalPagina.Caption := '/' + IntToStr(objTrptPicking.QuickRep1.QRPrinter.PageCount) ;
                  objTrptPicking.QuickRep1.QRPrinter.Free;
                  objTrptPicking.QuickRep1.QRPrinter := nil ;

                  objTrptPicking.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

                  objTrptPicking.QuickRep1.PreviewModal;
              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objTrptPicking.ClassName) ;
                  raise ;
              end;
          finally
              FreeAndNil(objTrptPicking);
          end;

      end
      else
      begin
      
          objTrptPicking_Retrato := TrptPicking_Retrato.Create(nil);

          try
              try

                  PosicionaQuery(objTrptPicking_Retrato.qryPedido,qryPedidonCdPedido.AsString) ;

                  objTrptPicking_Retrato.QuickRep1.Prepare;
                  objTrptPicking_Retrato.QRTotalPagina.Caption := '/' + IntToStr(objTrptPicking_Retrato.QuickRep1.QRPrinter.PageCount) ;
                  objTrptPicking_Retrato.QuickRep1.QRPrinter.Free;
                  objTrptPicking_Retrato.QuickRep1.QRPrinter := nil ;

                  objTrptPicking_Retrato.QuickRep1.PreviewModal;
              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objTrptPicking_Retrato.ClassName) ;
                  raise ;
              end;
          finally
              FreeAndNil(objTrptPicking_Retrato);
          end;

      end ;

      if (qryPedidodDtIniProducao.AsString <> '') then
          exit ;

      case MessageDlg('Deseja registrar a hora de in�cio da produ��o ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          qryPedido.Edit ;
          qryPedidodDtIniProducao.Value := Now() ;
          qryPedido.Post ;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryPedido.Close ;
      qryPedido.Open ;
      
  end ;

end;

procedure TfrmMonitorProducao_Exibe.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGrid1DBTableView1nCdPedido) then
  begin

      if (ARecord.Values[3] < Date) then
          AStyle := LinhaDestaque
      else AStyle := LinhaNormal ;

  end ;

end;

procedure TfrmMonitorProducao_Exibe.cxGrid1DBTableView1DblClick(Sender: TObject);
var
  objForm : TfrmMonitorProducao_Exibe_ItensPedido;
begin
  inherited;

  objForm := TfrmMonitorProducao_Exibe_ItensPedido.Create( Self );

  try
      objForm.exibeItensPedido( qryPedidonCdPedido.Value );
  finally
      freeAndNil( objForm ) ;
  end ;


end;

procedure TfrmMonitorProducao_Exibe.exibePossibilidadeAtendimento(nCdPedido,
  nCdTerceiro: integer);
var
  objRel : TrptPossibilidadeFaturamento_view ;
begin

  objRel := TrptPossibilidadeFaturamento_view.Create( Self ) ;

  objRel.SPREL_POSSIB_FATURAMENTO.Close;
  objRel.SPREL_POSSIB_FATURAMENTO.Parameters.ParamByName('@nCdPedidoAux').Value:= nCdPedido ;
  objRel.SPREL_POSSIB_FATURAMENTO.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro ;
  objRel.SPREL_POSSIB_FATURAMENTO.Open;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  objRel.QuickRep1.PreviewModal;

end;

procedure TfrmMonitorProducao_Exibe.PossibilidadeAtendimentoPedido1Click(
  Sender: TObject);
begin
  inherited;

  exibePossibilidadeAtendimento(qryPedidonCdPedido.Value
                               ,0) ;

end;

procedure TfrmMonitorProducao_Exibe.PossibilidadeAtendimentoCliente1Click(
  Sender: TObject);
begin
  inherited;

  exibePossibilidadeAtendimento(0
                               ,qryPedidonCdTerceiro.Value) ;

end;

end.
