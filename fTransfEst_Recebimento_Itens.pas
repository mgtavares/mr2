unit fTransfEst_Recebimento_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTransfEst_Recebimento_Itens = class(TfrmProcesso_Padrao)
    qryItens: TADOQuery;
    dsItens: TDataSource;
    DBGridEh1: TDBGridEh;
    qryItensnCdProduto: TIntegerField;
    qryItenscEAN: TStringField;
    qryItenscNmProduto: TStringField;
    qryItensnQtde: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransfEst_Recebimento_Itens: TfrmTransfEst_Recebimento_Itens;

implementation

{$R *.dfm}

end.
