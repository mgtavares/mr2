unit fAutorizaSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, cxLookAndFeelPainters, cxButtons, GridsEh,
  DBGridEh, DBGridEhGrouping, cxPC, cxControls, ToolCtrlsEh;

type
  TfrmAutorizaSP = class(TfrmCadastro_Padrao)
    qryMasternCdSP: TIntegerField;
    qryMasternCdTipoSP: TIntegerField;
    qryMastercNmTipoSP: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMastercSerieTit: TStringField;
    qryMasternValSP: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    qryMastercSigla: TStringField;
    qryMastercNmEmpresa: TStringField;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    btAutorizarSP: TcxButton;
    cxButton2: TcxButton;
    usp_AutorizaSP: TADOStoredProc;
    qryMasterdDtLiber: TDateTimeField;
    qryMasterdDtAutor: TDateTimeField;
    Label3: TLabel;
    DBEdit12: TDBEdit;
    Label5: TLabel;
    DBEdit13: TDBEdit;
    usp_Cancela_AutorizaSP: TADOStoredProc;
    qryMasterdDtContab: TDateTimeField;
    Label10: TLabel;
    DBEdit14: TDBEdit;
    qryParcela: TADOQuery;
    dsParcela: TDataSource;
    qryParcelanCdPrazoSP: TAutoIncField;
    qryParcelanCdSP: TIntegerField;
    qryParceladDtVenc: TDateTimeField;
    qryParcelanValPagto: TBCDField;
    DBGridEh1: TDBGridEh;
    qryMasterdDtCancel: TDateTimeField;
    qryAlcadaSP: TADOQuery;
    qryAlcadaSPnCdAlcadaAutorizSP: TIntegerField;
    qryMasternCdLojaSP: TIntegerField;
    btImprimirSP: TcxButton;
    qryValidaTitulo: TADOQuery;
    qryMastercOBSSP: TMemoField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryOutrosTitulos: TADOQuery;
    dsOutrosTitulos: TDataSource;
    qryOutrosTitulosnCdTitulo: TIntegerField;
    qryOutrosTituloscNrTit: TStringField;
    qryOutrosTituloscNrNF: TStringField;
    qryOutrosTitulosdDtEmissao: TDateTimeField;
    qryOutrosTitulosdDtVenc: TDateTimeField;
    qryOutrosTitulosnCdSP: TIntegerField;
    qryOutrosTitulosnCdRecebimento: TIntegerField;
    qryOutrosTitulosnValTit: TBCDField;
    qryOutrosTitulosnSaldoTit: TBCDField;
    cxButton3: TcxButton;
    qryMasternValProvisao: TBCDField;
    qryMasternValOscilacao: TBCDField;
    Label12: TLabel;
    DBEdit15: TDBEdit;
    Label13: TLabel;
    DBEdit16: TDBEdit;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    qryProjeto: TADOQuery;
    qryProjetonCdProjeto: TIntegerField;
    qryProjetocNmProjeto: TStringField;
    qryProjetonCdStatus: TIntegerField;
    qryProjetoCategFinanc: TADOQuery;
    qryProjetoCategFinanciRetorno: TIntegerField;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryItemSP: TADOQuery;
    qryItemSPnCdItemSP: TAutoIncField;
    qryItemSPnCdSP: TIntegerField;
    qryItemSPdDtRef: TDateTimeField;
    qryItemSPnValPagto: TBCDField;
    qryItemSPnCdCategFinanc: TIntegerField;
    qryItemSPnCdCC: TIntegerField;
    qryItemSPnCdUnidadeNegocio: TIntegerField;
    qryItemSPcNmCategFinanc: TStringField;
    qryItemSPcNmCC: TStringField;
    qryItemSPcNmUnidadeNegocio: TStringField;
    qryItemSPcObs: TStringField;
    qryItemSPnCdProjeto: TIntegerField;
    qryItemSPcNmProjeto: TStringField;
    dsItemSP: TDataSource;
    DBGridEh3: TDBGridEh;
    cxTabSheet3: TcxTabSheet;
    DBMemo1: TDBMemo;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryMastercFlgExigeNF: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btAutorizarSPClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure cxButton2Click(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btImprimirSPClick(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure qryItemSPCalcFields(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemSPBeforePost(DataSet: TDataSet);
    procedure DBEdit16Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAutorizaSP: TfrmAutorizaSP;

implementation

uses fMenu, rImpSP, fTitulo, fSP, fLookup_Padrao;

{$R *.dfm}

procedure TfrmAutorizaSP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'SP' ;
  nCdTabelaSistema  := 20 ;
  nCdConsultaPadrao := 165 ;
  
end;

procedure TfrmAutorizaSP.btAutorizarSPClick(Sender: TObject);
var
    qryAux   : TADOQuery ;
    iNrNFAux : integer ;
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtLiber.AsString = '') then
  begin
      MensagemAlerta('A SP precisa ser liberada antes de autorizada.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString <> '') then
  begin
      MensagemAlerta('SP j� foi autorizada.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('SP est� cancelada.') ;
      exit ;
  end ;

  qryAux := TADOQuery.Create(Self) ;

  qryAux.Connection := frmMenu.Connection ;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED') ;
  qryAux.SQL.Add('SELECT Sum(nValPagto) FROM ItemSP WHERE nCdSP = ' + qryMasternCdSP.asString) ;
  qryAux.Open ;

  If qryAux.FieldList[0].Value <> qryMasternValSP.Value then
  begin
      qryAux.Close ;
      qryAux := nil ;
      MensagemAlerta('O total dos itens da SP n�o confere com o valor total da SP.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED') ;
  qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoSP WHERE nCdSP = ' + qryMasternCdSP.asString) ;
  qryAux.Open ;

  If qryAux.FieldList[0].Value <> qryMasternValSP.Value then
  begin
      qryAux.Close ;
      qryAux := nil ;
      MensagemAlerta('O total das parcelas da SP n�o confere com o valor total da SP.') ;
      exit ;
  end ;

  qryAux := nil;

  qryAlcadaSP.Close;
  qryAlcadaSP.Parameters.ParamByName('nCdEmpresaAtiva').Value  := qryMasternCdEmpresa.Value;
  qryAlcadaSP.Parameters.ParamByName('nCdLoja').Value          := qryMasternCdLojaSP.Value;
  qryAlcadaSP.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
  qryAlcadaSP.Parameters.ParamByName('nCdTipoSP').Value        := qryMasternCdTipoSp.Value;
  qryAlcadaSP.Parameters.ParamByName('nValorSP').Value         := qryMasternValSP.Value;
  qryAlcadaSP.Open;

  if (qryAlcadaSP.Eof) then
  begin
      MensagemAlerta('Voc� n�o tem al�ada para aprovar esta SP.') ;
      exit ;
  end;

  try
      iNrNFAux := strToint( trim( qryMastercNrTit.Value ) ) ;

      if ( intToStr( iNrNFAux ) <> trim( qryMastercNrTit.Value ) ) then
      begin
          qryMaster.Edit;
          qryMastercNrTit.Value := intToStr( strToint( trim( qryMastercNrTit.Value ) ) ) ;
      end ;
          
  except

      if ( qryMastercFlgExigeNF.Value = 1 ) then
      begin
          MensagemErro('N�mero de documento fiscal inv�lido.') ;
          abort ;
      end;

  end ;

  if (qryMaster.State = dsEdit) then
      qryMaster.Post;

  qryValidaTitulo.Close ;
  qryValidaTitulo.Parameters.ParamByName('cNrTit').Value      := qryMastercNrTit.Value      ;
  qryValidaTitulo.Parameters.ParamByName('cSerieTit').Value   := qryMastercSerieTit.Value   ;
  qryValidaTitulo.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryValidaTitulo.Parameters.ParamByName('nCdSP').Value       := qryMasternCdSP.Value       ;
  qryValidaTitulo.Open ;

  If not qryValidaTitulo.Eof then
  begin
      MensagemAlerta('J� existe uma SP autorizada para este credor com este n�mero de nota fiscal.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a Autoriza��o desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      usp_AutorizaSP.Close ;
      usp_AutorizaSP.Parameters.ParamByName('@nCdSP').Value := qryMasternCdSP.Value ;
      usp_AutorizaSP.Parameters.ParamByName('@nCdUsuarioAutor').Value := frmMenu.nCdUsuarioLogado ;
      usp_AutorizaSP.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro na autoriza��o da SP.') ;
      Raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP autorizada com sucesso.') ;

  btCancelar.Click;

end;

procedure TfrmAutorizaSP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) and (qryMaster.State <> dsInsert) then
  begin
      qryMaster.Close ;
      MensagemAlerta('Esta SP n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Esta SP est� cancelada e n�o pode ser autorizada.') ;
      btCancelar.Click;
      exit ;
  end ;

  PosicionaQuery(qryItemSP, qryMasternCdSP.AsString) ;
  PosicionaQuery(qryParcela, qryMasternCdSP.AsString) ;

  qryOutrosTitulos.Close;
  qryOutrosTitulos.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
  qryOutrosTitulos.Parameters.ParamByName('nCdSP').Value       := qryMasternCdSP.Value;
  qryOutrosTitulos.Open;

  btAutorizarSP.SetFocus;

end;

procedure TfrmAutorizaSP.cxButton2Click(Sender: TObject);
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtAutor.AsString = '') then
  begin
      MensagemAlerta('SP n�o est� autorizada.') ;
      exit ;
  end ;

  if (qryMasterdDtContab.AsString <> '') then
  begin
      MensagemAlerta('SP j� contabilizada, imposs�vel estornar autoriza��o.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o Estorno da Autoriza��o desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      usp_Cancela_AutorizaSP.Close ;
      usp_Cancela_AutorizaSP.Parameters.ParamByName('@nCdSP').Value := qryMasternCdSP.Value ;
      usp_Cancela_AutorizaSP.Parameters.ParamByName('@nCdUsuarioCancel').Value := frmMenu.nCdUsuarioLogado ;
      usp_Cancela_AutorizaSP.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no estorno da autoriza��o da SP.') ;
      Raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP desautorizada com sucesso.') ;

  btCancelar.Click;

end;

procedure TfrmAutorizaSP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryParcela.Close ;
  qryOutrosTitulos.Close;
  qryItemSP.Close;

  desativaDBedit( DBEdit16 ) ;

end;

procedure TfrmAutorizaSP.btImprimirSPClick(Sender: TObject);
var
  objRel : TrptImpSP ;
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtLiber.AsString = '') then
  begin
      MensagemAlerta('� necess�rio liberar a SP antes de imprimir.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('N�o � poss�vel imprimir uma SP cancelada.') ;
      exit ;
  end ;

  objRel := TrptImpSP.Create(Self) ;

  objRel.qryImpSP.Close ;
  objRel.qryImpSP.Parameters.ParamByName('nCdSP').Value := qryMasternCdSP.Value ;
  objRel.qryImpSP.Open ;

  objRel.qryPrazo.Close ;
  objRel.qryPrazo.Parameters.ParamByName('nCdSP').Value := qryMasternCdSP.Value ;
  objRel.qryPrazo.Open ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmAutorizaSP.DBGridEh2DblClick(Sender: TObject);
var
  objForm : TfrmTitulo ;
begin
  inherited;

  if (not qryOutrosTitulos.isEmpty) then
  begin
      objForm := TfrmTitulo.Create(nil) ;

      objForm.PosicionaQuery(objForm.qryMaster,qryOutrosTitulosnCdTitulo.AsString);

      objForm.btIncluir.Visible   := False ;
      objForm.btSalvar.Visible    := False ;
      objForm.btConsultar.Visible := False ;
      objForm.btExcluir.Visible   := False ;
      objForm.ToolButton1.Visible := False ;
      objForm.btCancelar.Visible  := False ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmAutorizaSP.cxButton3Click(Sender: TObject);
var
  frmSP : TfrmSP ;
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtAutor.AsString <> '') then
  begin
      MensagemAlerta('SP j� foi autorizada.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('SP est� cancelada.') ;
      exit ;
  end ;

  frmSP := TfrmSP.Create( Self );

  try
      frmSP.bEdicao := TRUE ;
      frmSP.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      frmSP.qryMaster.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      frmSP.qryMaster.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

      frmSP.PosicionaQuery( frmSP.qryMaster , qryMasternCdSP.AsString );

      frmSP.ShowModal;
  finally
      freeAndNil( frmSP ) ;
  end ;

  PosicionaQuery( qryMaster , qryMasternCdSP.AsString ) ;

end;

procedure TfrmAutorizaSP.qryItemSPCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemSPnCdCategFinanc.Value > 0) then
  begin
      PosicionaQuery(qryCategFinanc, qryItemSPnCdCategFinanc.AsString) ;

      if not qryCategFinanc.eof then
          qryItemSPcNmCategFinanc.Value := qryCategFinanccNmCategFinanc.Value ;
  end ;

  if (qryItemSPnCdCC.Value > 0) then
  begin
      qryCC.Parameters.ParamByName('nCdCategFinanc').Value := qryItemSPnCdCategFinanc.Value ;

      PosicionaQuery(qryCC, qryItemSPnCdCC.AsString) ;

      if not qryCC.Eof then
          qryItemSPcNmCC.Value := qryCCcNmCC.Value ;

  end ;

  if (qryItemSPnCdUnidadeNegocio.Value > 0) then
  begin
      qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
      PosicionaQuery(qryUnidadeNegocio, qryItemSPnCdUnidadeNegocio.AsString) ;

      if not qryUnidadeNegocio.Eof then
          qryItemSPcNmUnidadeNegocio.Value := qryUnidadeNegociocNmUnidadeNegocio.Value ;

  end ;

  if (qryItemSPnCdProjeto.Value > 0) then
  begin
  
      qryProjeto.Close;
      PosicionaQuery( qryProjeto , qryItemSPnCdProjeto.AsString ) ;

      if not qryProjeto.eof then
          qryItemSPcNmProjeto.Value := qryProjetocNmProjeto.Value ;

  end ;

end;

procedure TfrmAutorizaSP.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        // categoria financeira
        if (DBGridEh3.Col = 4) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                {-- verifica no parametro se deve ou n�o filtrar as categorias financeiras por usu�rio --}
                if (frmMenu.LeParametro('CATEGFINUSUSP') = 'N') then
                    nPK := frmLookup_Padrao.ExecutaConsulta(32)
                else nPK := frmLookup_Padrao.ExecutaConsulta2(32,'EXISTS (SELECT 1 FROM UsuarioCategFinanc UCF WHERE UCF.nCdCategFinanc = CategFinanc.nCdCategFinanc AND UCF.nCdUsuario = ' + intToStr(frmMenu.nCdUsuarioLogado) + ')') ;

                If (nPK > 0) then
                begin
                    qryItemSPnCdCategFinanc.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh3.Col = 6) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(33,'EXISTS(SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = CentroCusto.nCdCC AND CCCF.nCdCategFinanc = ' + qryItemSPnCdCategFinanc.AsString + ')');

                If (nPK > 0) then
                begin
                    qryItemSPnCdCC.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh3.Col = 8) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = @@Empresa)');

                If (nPK > 0) then
                begin
                    qryItemSPnCdUnidadeNegocio.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh3.Col = 10) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(227,'nCdStatus = 1') ;

                If (nPK > 0) then
                begin
                    qryItemSPnCdProjeto.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAutorizaSP.qryItemSPBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  if (qryItemSPdDtRef.asString = '') then
  begin
      MensagemAlerta('Informe a data de refer�ncia') ;
      Abort ;
  end ;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open ;

  if (qryItemSPdDtRef.Value <= StrToDateTime(qryParametrocValor.Value)) then
  begin
      MensagemAlerta('A data de refer�ncia da despesa n�o pode ser inferior a data da �ltima contabiliza��o financeira.') ;
      Abort ;
  end ;

  if (qryItemSPnValPagto.Value <= 0) then
  begin
      MensagemAlerta('Valor inv�lido ou n�o informado') ;
      Abort ;
  end ;

  if (qryItemSPnCdCategFinanc.Value = 0) or (qryItemSPcNmCategFinanc.Value = '') then
  begin
      MensagemAlerta('Informe a categoria financeira') ;
      Abort ;
  end ;

  if (qryItemSPnCdCC.Value = 0) or (qryItemSPcNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo') ;
      Abort ;
  end ;

  if (qryItemSPnCdUnidadeNegocio.Value = 0) or (qryItemSPcNmUnidadeNegocio.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio') ;
      Abort ;
  end ;

  if (qryItemSPcNmProjeto.Value <> '') then
  begin

      if (qryProjetonCdStatus.Value > 1) then
      begin
          MensagemAlerta('Projeto inativo n�o pode ser movimentado.');
          abort;
      end ;

      qryProjetoCategFinanc.Close;
      qryProjetoCategFinanc.Parameters.ParamByName('nCdProjeto').Value     := qryItemSPnCdProjeto.Value ;
      qryProjetoCategFinanc.Parameters.ParamByName('nCdCategFinanc').Value := qryItemSPnCdCategFinanc.Value ;
      qryProjetoCategFinanc.Open;

      if qryProjetoCategFinanc.IsEmpty then
      begin
          MensagemAlerta('Este projeto n�o tem v�nculo com essa categoria financeira.') ;
          abort ;
      end ;

  end ;

  if (Trim(qryItemSPcOBS.Value) = '') then
  begin
      MensagemAlerta('Informe o hist�rico da despesa.') ;
      Abort ;
  end ;

  inherited;

  qryItemSPcOBS.Value := Uppercase( qryItemSPcOBS.Value ) ;

  if (qryItemSP.State = dsInsert) then
      qryItemSPnCdItemSP.Value := frmMenu.fnProximoCodigo('ITEMSP') ;

  qryItemSPnCdSP.Value := qryMasternCdSP.Value ;

end;

procedure TfrmAutorizaSP.DBEdit16Change(Sender: TObject);
begin
  inherited;

  if (qryMasternValOscilacao.Value = 0) then
  begin
      desativaDBedit( DBEdit16 ) ;
  end ;

  if (qryMasternValOscilacao.Value > 0) then
  begin
      dbEdit16.Color      := clRed ;
      dbEdit16.Font.Color := clWhite;
  end ;

  if (qryMasternValOscilacao.Value < 0) then
  begin
      dbEdit16.Color      := clBlue ;
      dbEdit16.Font.Color := clWhite;
  end ;

end;

initialization
    RegisterClass(tFrmAutorizaSP) ;

end.
