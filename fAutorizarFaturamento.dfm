inherited frmAutorizarFaturamento: TfrmAutorizarFaturamento
  Left = 58
  Top = 68
  Width = 1152
  Height = 652
  Caption = 'Autorizar Faturamento'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 585
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 119
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 119
    end
    inherited ToolButton2: TToolButton
      Left = 127
    end
    object ToolButton4: TToolButton
      Left = 246
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object btRegistroLoteSerial: TToolButton
      Left = 254
      Top = 0
      Caption = 'Registro Lote/Serial'
      ImageIndex = 2
      OnClick = btRegistroLoteSerialClick
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 585
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 581
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens Faturar'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 557
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItensFaturar
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            ReadOnly = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Width = 341
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePick'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeLibFat'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Itens Faturar - Confer'#234'ncia'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 557
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItensFaturar
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh2ColExit
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto Farurar|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto Farurar|Descri'#231#227'o'
            Width = 333
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePick'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto Farurar|Quant.'
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'cLocalizacao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto Farurar|Localiza'#231#227'o'
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'cCdProdutoConf'
            Footers = <>
            Title.Caption = 'Produto Conferido|C'#243'd.'
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'cNmProdutoConf'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto Conferido|Descri'#231#227'o'
            Width = 315
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeLibFat'
            Footers = <>
            Title.Caption = 'Produto Conferido|Quant.'
            Width = 74
          end
          item
            DropDownShowTitles = True
            EditButtons = <>
            FieldName = 'cNmTipoSaida'
            Footers = <>
            PickList.Strings = (
              '1-Sa'#237'da Total'
              '2-Saida Parcial'
              '3-N'#227'o Enviado')
            Title.Caption = 'Tipo Sa'#237'da'
            Width = 104
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 176
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000090490A0090490A0090490A009049
      0A0090490A0090490A0090490A0090490A0090490A0090490A0090490A009049
      0A0090490A00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000944D0E00DAA52500CC8D2100CC8D
      2100CC8D2100DAA5250098551A00DAA52500CC8D2100CC8D2100CC8D2100DAA5
      2500944D0E00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000097521200DBA82F00CE912A00CE91
      2A00CE912A00DBA82F009D592000DBA82F00CE912A00CE912A00CE912A00DBA8
      2F0097521200FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000009B561800DCAB3900D0953300D095
      3300D0953300DCAB3900A05E2600DCAB3900D0953300D0953300D0953300DCAB
      39009B561800FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000A05C1D00DEAE4300D2993C00D299
      3C00D2993C00DEAE4300A6642C00DEAE4300D2993C00D2993C00D2993C00DEAE
      4300A05C1D00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000A4622300FAE06200FAE06200FAE0
      6200FAE06200FAE06200A96A3300FAE06200FAE06200FAE06200FAE06200FAE0
      6200A4622300FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000AA672A00AF703900AF703900AF70
      3900AF703900AF703900A3602100AA672A00AA672A00AA672A00AA672A00AA67
      2A00AA672A00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000AE6D3100E3B86500D9A55A00D9A5
      5A00D9A55A00E3B86500AE6D3100FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000B3733800E4BB6F00DBA96300DBA9
      6300DBA96300E4BB6F00B3733800FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000B7793F00E6BE7900DDAD6C00DDAD
      6C00DDAD6C00E6BE7900B7793F00FFFFFF00FFFFFF000043A4000043A4000043
      A4000043A4000043A4000043A400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000BB7F4600E7C18200DFB17400DFB1
      7400DFB17400E7C18200BB7F4600FFFFFF00FFFFFF00004CBA002980FF000B6F
      FF000B6FFF002980FF00004CBA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000BF844C00FBE7AC00FBE7AC00FBE7
      AC00FBE7AC00FBE7AC00BF844C00FFFFFF00FFFFFF000056D2004390FF002B81
      FF002B81FF004390FF000056D200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000C2885200C2885200C2885200C288
      5200C2885200C2885200C2885200FFFFFF00FFFFFF000060EC00599DFF004792
      FF004792FF00599DFF000060EC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00056BFF00A1C7FF00A1C7
      FF00A1C7FF00A1C7FF00056BFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001977FF001977FF001977
      FF001977FF001977FF001977FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFF90000FE00FFFF00010000
      FE00C00300010000000080010001000000008001000100000000800100010000
      00008001000100000000800100010000000080010001000000008001007F0000
      0000800100010000000080010001000000018001000100000003800100010000
      0077C00300010000007FFFFF0001000000000000000000000000000000000000
      000000000000}
  end
  object qryItensFaturar: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensFaturarBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempItensFaturar'
      ' WHERE nQtdePick > 0')
    Left = 160
    Top = 192
    object qryItensFaturarnCdItemFaturar: TAutoIncField
      FieldName = 'nCdItemFaturar'
      ReadOnly = True
    end
    object qryItensFaturarnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryItensFaturarcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItensFaturarnQtdePick: TBCDField
      DisplayLabel = 'Quant. Pick'
      FieldName = 'nQtdePick'
      Precision = 12
    end
    object qryItensFaturarnQtdeLibFat: TBCDField
      DisplayLabel = 'Quant. p/ Faturar'
      FieldName = 'nQtdeLibFat'
      Precision = 12
    end
    object qryItensFaturarnCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryItensFaturarnCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryItensFaturarnCdPickList: TIntegerField
      FieldName = 'nCdPickList'
    end
    object qryItensFaturarcLocalizacao: TStringField
      FieldName = 'cLocalizacao'
    end
    object qryItensFaturarcNmTipoSaida: TStringField
      FieldName = 'cNmTipoSaida'
      Size = 15
    end
    object qryItensFaturarcCdProdutoConf: TStringField
      FieldName = 'cCdProdutoConf'
      OnChange = qryItensFaturarcCdProdutoConfChange
    end
    object qryItensFaturarnCdProdutoConf: TIntegerField
      FieldName = 'nCdProdutoConf'
    end
    object qryItensFaturarcNmProdutoConf: TStringField
      FieldName = 'cNmProdutoConf'
      Size = 150
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempItensFaturar'#39') IS NULL)'#13#10'    BEGIN'#13#10 +
      '        CREATE TABLE #TempItensFaturar (nCdItemFaturar  int IDEN' +
      'TITY(1,1) PRIMARY KEY'#13#10'                                       ,n' +
      'CdItemPedido   int'#13#10'                                       ,nCdP' +
      'ickList     int'#13#10'                                       ,nCdProd' +
      'uto      int'#13#10'                                       ,cNmProduto' +
      '      varchar(150)'#13#10'                                       ,nQtd' +
      'ePick       decimal(12,4)'#13#10'                                     ' +
      '  ,nQtdeLibFat     decimal(12,4)'#13#10'                              ' +
      '         ,nCdLocalEstoque int'#13#10'                                 ' +
      '      ,cLocalizacao    varchar(20)'#13#10'                            ' +
      '           ,cCdProdutoConf  varchar(20)'#13#10'                       ' +
      '                ,cNmTipoSaida     varchar(15)'#13#10'                 ' +
      '                      ,nCdProdutoConf   int'#13#10'                   ' +
      '                    ,cNmProdutoConf  varchar(150))'#13#10'    END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 104
    Top = 160
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdPickList'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgOrdemProducao'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdOrdemProducao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido          int'
      '       ,@nCdPickList        int'
      '       ,@nCdLocalEstoque    int'
      '       ,@nCdEmpresa         int'
      '       ,@nCdGrupoProduto    int'
      '       ,@nCdOperacaoEstoque int'
      '       ,@cFlgOrdemProducao  int'
      '       ,@nCdOrdemProducao   int'
      '       ,@nQtdePed           decimal(12,4)'
      ''
      'SET @nCdPedido         = :nCdPedido'
      'SET @nCdPickList       = :nCdPickList'
      'SET @cFlgOrdemProducao = :cFlgOrdemProducao'
      'SET @nCdOrdemProducao  = :nCdOrdemProducao'
      ''
      'TRUNCATE TABLE #TempItensFaturar'
      ''
      'INSERT INTO #TempItensFaturar (nCdItemPedido'
      '                              ,nCdPickList'
      '                              ,nCdProduto  '
      '                              ,cNmProduto    '
      '                              ,nQtdePick      '
      '                              ,nQtdeLibFat'
      '                              ,nCdLocalEstoque'
      '                              ,cNmTipoSaida)'
      '                        SELECT ItemPedido.nCdItemPedido'
      '                              ,ItemPickList.nCdPickList'
      '                              ,ItemPedido.nCdProduto'
      '                              ,ItemPedido.cNmItem'
      '                              ,ItemPickList.nQtdePick'
      '                              ,ItemPickList.nQtdePick'
      '                              ,ItemPickList.nCdLocalEstoque'
      '                              ,'#39'1-Sa'#237'da Total'#39
      '                         FROM ItemPickList'
      
        '                              INNER JOIN ItemPedido ON ItemPedid' +
        'o.nCdItemPedido = ItemPickList.nCdItemPedido'
      
        '                        WHERE ItemPickList.nCdPickList = @nCdPic' +
        'kList'
      
        '                          AND ItemPedido.nCdTipoItemPed   IN (1,' +
        '2,5,6)'
      ''
      'IF (@@ROWCOUNT = 0)'
      'BEGIN'
      ''
      '    SELECT @nCdEmpresa         = Pedido.nCdEmpresa'
      '          ,@nCdGrupoProduto    = Produto.nCdGrupo_Produto'
      '          ,@nCdOperacaoEstoque = TipoPedido.nCdOperacaoEstoque'
      '      FROM ItemPedido'
      
        '           INNER JOIN Pedido           ON Pedido.nCdPedido      ' +
        '   = ItemPedido.nCdPedido'
      
        '           INNER JOIN Produto          ON Produto.nCdProduto    ' +
        '   = ItemPedido.nCdProduto'
      
        '           INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedi' +
        'do = Pedido.nCdTipoPedido'
      '     WHERE Pedido.nCdPedido = @nCdPedido'
      '       AND ItemPedido.nCdTipoItemPed   IN (1,2,5,6)'
      ''
      '    SELECT @nCdLocalEstoque = nCdLocalEstoque'
      '      FROM OperacaoLocalEstoque'
      '     WHERE nCdEmpresa         = @nCdEmpresa'
      '       AND nCdLoja           IS NULL'
      '       AND nCdGrupoProduto    = @nCdGrupoProduto'
      '       AND nCdOperacaoEstoque = @nCdOperacaoEstoque'
      ''
      '    IF (@nCdLocalEstoque IS NULL)'
      '    BEGIN'
      ''
      '        SELECT @nCdLocalEstoque = nCdLocalEstoque'
      '          FROM OperacaoLocalEstoque'
      '         WHERE nCdEmpresa          = @nCdEmpresa'
      '           AND nCdLoja            IS NULL'
      '           AND nCdOperacaoEstoque  = @nCdOperacaoEstoque'
      ''
      '    END'
      ''
      ''
      '    INSERT INTO #TempItensFaturar (nCdItemPedido'
      '                                  ,nCdProduto'
      '                                  ,cNmProduto'
      '                                  ,nQtdePick'
      '                                  ,nQtdeLibFat'
      '                                  ,nCdLocalEstoque'
      '                                  ,cNmTipoSaida)'
      '                            SELECT ItemPedido.nCdItemPedido'
      '                                  ,ItemPedido.nCdProduto'
      '                                  ,ItemPedido.cNmItem'
      
        '                                  ,isNull((SELECT isNull(OP.nQtd' +
        'ePlanejada,ItemPedido.nQtdePed)'
      
        '                                                          - isNu' +
        'll((SELECT SUM(ItemPickList.nQtdePick)'
      
        '                                                                ' +
        '      FROM ItemPickList'
      
        '                                                                ' +
        '           INNER JOIN PickList ON PickList.nCdPickList = ItemPic' +
        'kList.nCdPickList'
      
        '                                                                ' +
        '     WHERE PickList.nCdPedido         = ItemPedido.nCdPedido'
      
        '                                                                ' +
        '       AND PickList.dDtCancel        IS NULL'
      
        '                                                                ' +
        '       AND ItemPickList.nCdItemPedido = ItemPedido.nCdItemPedido'
      
        '                                                                ' +
        '       AND PickList.cFlgBaixado       = 0),0)'
      
        '                                                          - Item' +
        'Pedido.nQtdeLibFat'
      
        '                                                          - Item' +
        'Pedido.nQtdeExpRec'
      
        '                                                          - Item' +
        'Pedido.nQtdeCanc'
      
        '                                                          - (isN' +
        'ull(OP.nQtdePlanejada,0) - isNull(OP.nQtdeConcluida,0))),0)'
      
        '                                  ,isNull((SELECT isNull(OP.nQtd' +
        'ePlanejada,ItemPedido.nQtdePed)'
      
        '                                                          - isNu' +
        'll((SELECT SUM(ItemPickList.nQtdePick)'
      
        '                                                                ' +
        '      FROM ItemPickList'
      
        '                                                                ' +
        '           INNER JOIN PickList ON PickList.nCdPickList = ItemPic' +
        'kList.nCdPickList'
      
        '                                                                ' +
        '     WHERE PickList.nCdPedido         = ItemPedido.nCdPedido'
      
        '                                                                ' +
        '       AND PickList.dDtCancel        IS NULL'
      
        '                                                                ' +
        '       AND ItemPickList.nCdItemPedido = ItemPedido.nCdItemPedido'
      
        '                                                                ' +
        '       AND PickList.cFlgBaixado       = 0),0)'
      
        '                                                          - Item' +
        'Pedido.nQtdeLibFat'
      
        '                                                          - Item' +
        'Pedido.nQtdeExpRec'
      
        '                                                          - Item' +
        'Pedido.nQtdeCanc'
      
        '                                                          - (isN' +
        'ull(OP.nQtdePlanejada,0) - isNull(OP.nQtdeConcluida,0))),0)'
      '                                  ,@nCdLocalEstoque'
      '                                  ,'#39'1-Sa'#237'da Total'#39
      '                             FROM ItemPedido'
      
        '                                  LEFT  JOIN OrdemProducao OP ON' +
        ' OP.nCdPedido = ItemPedido.nCdPedido AND ItemPedido.nCdProduto =' +
        ' OP.nCdProduto'
      
        '                            WHERE ItemPedido.nCdPedido = @nCdPed' +
        'ido'
      
        '                              AND ItemPedido.nCdTipoItemPed   IN' +
        ' (1,2,5,6)'
      
        '                              AND (((@cFlgOrdemProducao = 0) AND' +
        ' (OP.nCdOrdemProducao IS NULL)) OR ((OP.nCdProduto = ItemPedido.' +
        'nCdProduto) AND (OP.nCdOrdemProducao = @nCdOrdemProducao)))'
      ''
      'END'
      ''
      'UPDATE #TempItensFaturar'
      '   SET cLocalizacao = (SELECT cLocalizacao'
      '                         FROM PosicaoEstoque'
      
        '                        WHERE PosicaoEstoque.nCdProduto      = #' +
        'TempItensFaturar.nCdProduto'
      
        '                          AND PosicaoEstoque.nCdLocalEstoque = #' +
        'TempItensFaturar.nCdLocalEstoque)'
      
        '   ,nQtdeLibFat = CASE WHEN (dbo.fn_LeParametro('#39'USACONFAUTFAT'#39')' +
        ' = '#39'N'#39')'
      '                       THEN nQtdeLibFat'
      '                       ELSE 0'
      '                  END')
    Left = 128
    Top = 192
  end
  object dsItensFaturar: TDataSource
    DataSet = qryItensFaturar
    Left = 176
    Top = 224
  end
  object SP_AUTORIZAR_FATURAMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AUTORIZAR_FATURAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 232
    Top = 192
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cEAN'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cEAN       varchar(20)'
      '       ,@nCdProduto int'
      '       ,@cNmProduto varchar(150)'
      ''
      'SET @cEAN = :cEAN'
      ''
      'IF (RTRIM(@cEAN) = '#39#39')'
      '    SET @nCdProduto = 0'
      ''
      'SELECT @nCdProduto    = nCdProduto'
      '  FROM Produto'
      ' WHERE cEAN = RTRIM(@cEAN)'
      ''
      'IF (@nCdProduto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdProduto    = nCdProduto'
      '      FROM Produto'
      '     WHERE cEANFornec = RTRIM(@cEAN)'
      ''
      'END'
      ''
      'IF (@nCdProduto IS NULL)'
      'BEGIN'
      '    BEGIN TRY'
      '        SELECT @nCdProduto    = nCdProduto'
      '          FROM Produto'
      '         WHERE nCdProduto = Convert(int,RTRIM(@cEAN))'
      '    END TRY'
      '    BEGIN CATCH'
      '    END CATCH'
      'END'
      ''
      'SELECT nCdProduto '
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto')
    Left = 236
    Top = 237
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
end
