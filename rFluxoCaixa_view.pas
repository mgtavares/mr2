unit rFluxoCaixa_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFluxoCaixa_View = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand5: TQRBand;
    lblFiltro1: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    usp_RelatorioCOLUMN1: TStringField;
    usp_RelatorioCOLUMN2: TStringField;
    usp_RelatorioCOLUMN3: TStringField;
    usp_RelatorioCOLUMN4: TStringField;
    usp_RelatorioCOLUMN5: TStringField;
    usp_RelatorioCOLUMN6: TStringField;
    usp_RelatorioCOLUMN7: TStringField;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    usp_RelatorioCOLUMN8: TStringField;
    usp_RelatorioCOLUMN9: TStringField;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFluxoCaixa_View: TrptFluxoCaixa_View;

implementation

{$R *.dfm}

end.
