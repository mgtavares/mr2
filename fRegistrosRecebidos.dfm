inherited frmRegistrosRecebidos: TfrmRegistrosRecebidos
  Caption = 'Registros Recebidos'
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Hoje'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 776
        Height = 407
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRecebidosHoje
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        PopupMenu = PopupMenu1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cNmServidor'
            Footers = <>
            Width = 301
          end
          item
            EditButtons = <>
            FieldName = 'cTabela'
            Footers = <>
            Width = 251
          end
          item
            EditButtons = <>
            FieldName = 'iIDRegistro'
            Footers = <>
            Width = 111
          end
          item
            EditButtons = <>
            FieldName = 'cFlgAcao'
            Footers = <>
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'dDtInclusao'
            Footers = <>
            Width = 165
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #218'ltimos 15 dias'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 776
        Height = 407
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRecebidosQuinze
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        PopupMenu = PopupMenu2
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cNmServidor'
            Footers = <>
            Width = 300
          end
          item
            EditButtons = <>
            FieldName = 'cTabela'
            Footers = <>
            Width = 250
          end
          item
            EditButtons = <>
            FieldName = 'iIDRegistro'
            Footers = <>
            Width = 112
          end
          item
            EditButtons = <>
            FieldName = 'cFlgAcao'
            Footers = <>
            Width = 102
          end
          item
            EditButtons = <>
            FieldName = 'dDtInclusao'
            Footers = <>
            Width = 139
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryRecebidosHoje: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT Servidor.cNmServidor'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,dDtInclusao'
      '  FROM TempRegistroRecebido'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistroRecebido.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistroRecebido.nCdServidorOrigem'
      ' WHERE dDtInclusao >= dbo.fn_OnlyDate(GetDate())'
      ' ORDER BY dDtInclusao DESC'
      '')
    Left = 232
    Top = 152
    object qryRecebidosHojecNmServidor: TStringField
      DisplayLabel = 'Registros Recebidos Hoje|Servidor Origem'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryRecebidosHojecTabela: TStringField
      DisplayLabel = 'Registros Recebidos Hoje|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object qryRecebidosHojeiIDRegistro: TIntegerField
      DisplayLabel = 'Registros Recebidos Hoje|ID Registro'
      FieldName = 'iIDRegistro'
    end
    object qryRecebidosHojecFlgAcao: TStringField
      DisplayLabel = 'Registros Recebidos Hoje|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object qryRecebidosHojedDtInclusao: TDateTimeField
      DisplayLabel = 'Registros Recebidos Hoje|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
  end
  object dsRecebidosHoje: TDataSource
    DataSet = qryRecebidosHoje
    Left = 268
    Top = 149
  end
  object qryRecebidosQuinze: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT Servidor.cNmServidor'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,dDtInclusao'
      '  FROM TempRegistroRecebido'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistroRecebido.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistroRecebido.nCdServidorOrigem'
      ' WHERE dDtInclusao >= (dbo.fn_OnlyDate(GetDate())-15)'
      ' ORDER BY dDtInclusao DESC'
      '')
    Left = 232
    Top = 200
    object qryRecebidosQuinzecNmServidor: TStringField
      DisplayLabel = 'Registros Recebidos '#218'ltimos 15 dias|Servidor Origem'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryRecebidosQuinzecTabela: TStringField
      DisplayLabel = 'Registros Recebidos '#218'ltimos 15 dias|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object qryRecebidosQuinzeiIDRegistro: TIntegerField
      DisplayLabel = 'Registros Recebidos '#218'ltimos 15 dias|ID Registro'
      FieldName = 'iIDRegistro'
    end
    object qryRecebidosQuinzecFlgAcao: TStringField
      DisplayLabel = 'Registros Recebidos '#218'ltimos 15 dias|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object qryRecebidosQuinzedDtInclusao: TDateTimeField
      DisplayLabel = 'Registros Recebidos '#218'ltimos 15 dias|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
  end
  object dsRecebidosQuinze: TDataSource
    DataSet = qryRecebidosQuinze
    Left = 268
    Top = 197
  end
  object PopupMenu1: TPopupMenu
    Left = 236
    Top = 245
    object Atualizar1: TMenuItem
      Caption = 'Atualizar'
      OnClick = Atualizar1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 276
    Top = 245
    object MenuItem1: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem1Click
    end
  end
end
