inherited frmReabilitacaoClienteManual: TfrmReabilitacaoClienteManual
  Left = 218
  Top = 16
  Width = 1040
  Caption = 'Reabilita'#231#227'o Cliente - Manual'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1024
  end
  object Label1: TLabel [1]
    Left = 60
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 68
    Top = 62
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 50
    Top = 86
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'CNPJ/CPF'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 280
    Top = 86
    Width = 18
    Height = 13
    Alignment = taRightJustify
    Caption = 'R.G'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 8
    Top = 110
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Negativa'#231#227'o'
    FocusControl = DBEdit5
  end
  inherited ToolBar2: TToolBar
    Width = 1024
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
    inherited ToolButton50: TToolButton
      Visible = False
    end
    inherited ToolButton4: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited btnAuditoria: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 104
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [9]
    Tag = 1
    Left = 104
    Top = 80
    Width = 145
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [10]
    Tag = 1
    Left = 304
    Top = 80
    Width = 145
    Height = 19
    DataField = 'cRG'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [11]
    Tag = 1
    Left = 104
    Top = 104
    Width = 145
    Height = 19
    DataField = 'dDtNegativacao'
    DataSource = dsMaster
    TabOrder = 5
  end
  object cxPageControl1: TcxPageControl [12]
    Left = 8
    Top = 144
    Width = 801
    Height = 385
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 6
    ClientRectBottom = 381
    ClientRectLeft = 4
    ClientRectRight = 797
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'T'#237'tulos Negativados'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 793
        Height = 357
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTituloNegativado
        EvenRowColor = clWhite
        Flat = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -13
        FooterFont.Name = 'Consolas'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentFont = False
        ReadOnly = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cSigla'
            Footers = <>
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'iDiasAtraso'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValLiq'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValAbatimento'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValJuro'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'T'#237'tulos Vencidos'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 793
        Height = 357
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTituloVencido
        EvenRowColor = clWhite
        Flat = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -13
        FooterFont.Name = 'Consolas'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        ParentFont = False
        ReadOnly = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cSigla'
            Footers = <>
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'iDiasAtraso'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValLiq'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValAbatimento'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValJuro'
            Footers = <>
            MRUList.Active = True
            Width = 60
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object btReabilitar: TcxButton [13]
    Left = 8
    Top = 536
    Width = 145
    Height = 33
    Caption = 'Reabilitar Cliente'
    Enabled = False
    TabOrder = 7
    OnClick = btReabilitarClick
    Glyph.Data = {
      2E020000424D2E0200000000000036000000280000000C0000000E0000000100
      180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
      EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
      000000000000000000000000000000000000000000000000000000000000009C
      8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
      0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
      D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
      EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
      F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
      D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
      EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
      F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
      00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
      8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
      000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
      6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
      8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
      000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCNPJCPF'
      '      ,Terceiro.cRG'
      '      ,Terceiro.dDtNegativacao'
      '  FROM Terceiro         '
      ' WHERE EXISTS(SELECT 1                   '
      
        '                FROM TerceiroTipoTerceiro TTT                   ' +
        '               '
      
        '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro     ' +
        '                                 '
      '                 AND TTT.nCdTipoTerceiro = 2)  '
      '   AND Terceiro.nCdTerceiro = :nPK')
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMastercRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryMasterdDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
  end
  inherited ImageList1: TImageList
    Left = 728
  end
  object qryTituloNegativado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNrTit'
      '      ,iParcela'
      '      ,cNmEspTit'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      
        '      ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN Conve' +
        'rt(int,(GetDate() - dDtVenc))'
      '            ELSE 0'
      '       END iDiasAtraso'
      '      ,nValTit'
      '      ,nSaldoTit'
      '      ,nValLiq'
      '      ,nValAbatimento'
      '      ,(nValJuro + nValMulta) as nValJuro'
      '      ,nCdLanctoFin'
      '      ,Titulo.nCdTitulo'
      '      ,cSigla'
      '  FROM Titulo'
      
        '       INNER JOIN EspTit  ON EspTit.nCdEspTit   = Titulo.nCdEspT' +
        'it'
      
        '       INNER JOIN Empresa ON Empresa.nCdEmpresa = Titulo.nCdEmpr' +
        'esa'
      
        '       LEFT  JOIN TituloNegativado ON Titulo.nCdTitulo = TituloN' +
        'egativado.nCdTitulo'
      ' WHERE Titulo.cSenso        = '#39'C'#39
      '   AND Titulo.nCdTerceiro   = :nPK'
      
        '   AND ISNULL(TituloNegativado.dDtNegativacao, Titulo.dDtNegativ' +
        'acao) IS NOT NULL'
      
        '   AND ISNULL(TituloNegativado.dDtReabNeg, Titulo.dDtReabNeg)   ' +
        '  IS NULL'
      ' ORDER BY dDtVenc')
    Left = 384
    Top = 304
    object qryTituloNegativadocNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Negativados|T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloNegativadocNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Negativados|Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTituloNegativadodDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos Negativados|Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTituloNegativadodDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Negativados|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTituloNegativadoiDiasAtraso: TIntegerField
      DisplayLabel = 'T'#237'tulos Negativados|Atraso'
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryTituloNegativadonValTit: TBCDField
      DisplayLabel = 'T'#237'tulos Negativados|Valor Total'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloNegativadonSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos Negativados|Saldo Aberto'
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloNegativadonValLiq: TBCDField
      DisplayLabel = 'T'#237'tulos Negativados|Valor Pago'
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloNegativadonValAbatimento: TBCDField
      DisplayLabel = 'T'#237'tulos Negativados|Abatimento'
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloNegativadonValJuro: TBCDField
      DisplayLabel = 'T'#237'tulos Negativados|Juros + Multa'
      FieldName = 'nValJuro'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object qryTituloNegativadonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTituloNegativadoiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos Negativados|Parc.'
      FieldName = 'iParcela'
    end
    object qryTituloNegativadonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloNegativadocSigla: TStringField
      DisplayLabel = 'T'#237'tulos Negativados|Empresa'
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
  end
  object dsTituloNegativado: TDataSource
    DataSet = qryTituloNegativado
    Left = 416
    Top = 304
  end
  object qryTituloVencido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNrTit'
      '      ,iParcela'
      '      ,cNmEspTit'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      
        '      ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN Conve' +
        'rt(int,(GetDate() - dDtVenc))'
      '            ELSE 0'
      '       END iDiasAtraso'
      '      ,nValTit'
      '      ,nSaldoTit'
      '      ,nValLiq'
      '      ,nValAbatimento'
      '      ,(nValJuro + nValMulta) as nValJuro'
      '      ,nCdLanctoFin'
      '      ,nCdTitulo'
      '      ,cSigla'
      '  FROM Titulo'
      
        '       INNER JOIN EspTit  ON EspTit.nCdEspTit   = Titulo.nCdEspT' +
        'it'
      
        '       INNER JOIN Empresa ON Empresa.nCdEmpresa = Titulo.nCdEmpr' +
        'esa'
      ' WHERE Titulo.cSenso        = '#39'C'#39
      '   AND Titulo.nCdTerceiro   = :nPK'
      '   AND Titulo.nSaldoTit     > 0'
      '   AND Titulo.dDtCancel     IS NULL'
      '   AND Titulo.dDtVenc       <  dbo.fn_OnlyDate(GetDate())'
      ' ORDER BY dDtVenc')
    Left = 384
    Top = 344
    object StringField1: TStringField
      DisplayLabel = 'T'#237'tulos Vencidos|T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField2: TStringField
      DisplayLabel = 'T'#237'tulos Vencidos|Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object DateTimeField1: TDateTimeField
      DisplayLabel = 'T'#237'tulos Vencidos|Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object DateTimeField2: TDateTimeField
      DisplayLabel = 'T'#237'tulos Vencidos|Vencimento'
      FieldName = 'dDtVenc'
    end
    object IntegerField1: TIntegerField
      DisplayLabel = 'T'#237'tulos Vencidos|Atraso'
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object BCDField1: TBCDField
      DisplayLabel = 'T'#237'tulos Vencidos|Valor Total'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object BCDField2: TBCDField
      DisplayLabel = 'T'#237'tulos Vencidos|Saldo Aberto'
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object BCDField3: TBCDField
      DisplayLabel = 'T'#237'tulos Vencidos|Valor Pago'
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object BCDField4: TBCDField
      DisplayLabel = 'T'#237'tulos Vencidos|Abatimento'
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object BCDField5: TBCDField
      DisplayLabel = 'T'#237'tulos Vencidos|Juros + Multa'
      FieldName = 'nValJuro'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object IntegerField2: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'T'#237'tulos Vencidos|Parc.'
      FieldName = 'iParcela'
    end
    object IntegerField4: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloVencidocSigla: TStringField
      DisplayLabel = 'T'#237'tulos Vencidos|Empresa'
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
  end
  object dsTituloVencido: TDataSource
    DataSet = qryTituloVencido
    Left = 416
    Top = 344
  end
  object qryReabilitaCliente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@nCdUsuario  int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdUsuario  = :nCdUsuario'
      ''
      '/* apaga o registro da negativa'#231#227'o no terceiro */'
      'UPDATE Terceiro'
      '   SET dDtNegativacao = NULL'
      ' WHERE nCdTerceiro    = @nCdTerceiro'
      ''
      '/* apaga o registro de negativa'#231#227'o no t'#237'tulo */'
      'UPDATE Titulo'
      '   SET dDtReabNeg             = GetDate()'
      '      ,cFlgReabManual         = 1'
      '      ,nCdUsuarioReabManual   = @nCdUsuario'
      ' WHERE Titulo.cSenso          = '#39'C'#39
      '   AND Titulo.nCdTerceiro     = @nCdTerceiro'
      '   AND Titulo.dDtNegativacao IS NOT NULL'
      '   AND Titulo.dDtReabNeg     IS NULL'
      ''
      ''
      'UPDATE TituloNegativado'
      '   SET dDtReabNeg             = GetDate()'
      '      ,cFlgReabManual         = 1'
      '      ,nCdUsuarioReabManual   = @nCdUsuario'
      ' WHERE TituloNegativado.nCdTerceiro = @nCdTerceiro'
      '   AND TituloNegativado.dDtReabNeg IS NULL'
      ''
      '/* apaga a tabela que armazena os t'#237'tulos negativados'
      '--DELETE'
      '--  FROM TituloNegativado'
      '-- WHERE nCdTerceiro = @nCdTerceiro'
      '*/'
      '')
    Left = 388
    Top = 392
  end
end
