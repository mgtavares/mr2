inherited frmConsultaMovtoCaixa_Parcelas: TfrmConsultaMovtoCaixa_Parcelas
  Left = 121
  Top = 152
  Caption = 'Consulta Parcelas Baixadas'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 780
      Height = 418
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsParcelas
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            Column = cxGrid1DBTableView1nValPago
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.ConteudoCaixa
        Styles.Header = frmMenu.HeaderCaixa
        object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
          Caption = 'T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
          Width = 90
        end
        object cxGrid1DBTableView1iParcela: TcxGridDBColumn
          Caption = 'Parc.'
          DataBinding.FieldName = 'iParcela'
        end
        object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Vencto'
          DataBinding.FieldName = 'dDtVenc'
          Width = 85
        end
        object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
          Caption = 'Cliente'
          DataBinding.FieldName = 'nCdTerceiro'
        end
        object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
          Caption = 'Nome Cliente'
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 232
        end
        object cxGrid1DBTableView1nValTit: TcxGridDBColumn
          Caption = 'Val. Original'
          DataBinding.FieldName = 'nValTit'
          HeaderAlignmentHorz = taRightJustify
        end
        object cxGrid1DBTableView1nValPago: TcxGridDBColumn
          Caption = 'Val. Pago'
          DataBinding.FieldName = 'nValPago'
          HeaderAlignmentHorz = taRightJustify
          Width = 98
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,MTitulo.nValMov + IsNull((SELECT Sum(nValMov)'
      '          FROM MTitulo MTitulo2'
      '         WHERE MTitulo2.nCdTitulo = MTitulo.nCdTitulo'
      '           AND MTitulo2.nCdLanctoFin = MTitulo.nCdLanctoFin'
      
        '           AND MTitulo2.nCdOperacao  = Convert(int,dbo.fn_LePara' +
        'metro('#39'OPERJUROCRED'#39'))),0) as nValPago'
      '  FROM MTitulo'
      
        '       INNER JOIN Titulo   ON Titulo.nCdTitulo = MTitulo.nCdTitu' +
        'lo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE MTitulo.nCdLanctoFin = :nPK'
      '   AND MTitulo.nCdOperacao  = 1'
      ' ORDER BY Titulo.cNrTit, Titulo.iParcela')
    Left = 384
    Top = 157
    object qryParcelascNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelasnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryParcelascNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryParcelasdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryParcelasnValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValPago: TBCDField
      FieldName = 'nValPago'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object dsParcelas: TDataSource
    DataSet = qryParcelas
    Left = 424
    Top = 157
  end
end
