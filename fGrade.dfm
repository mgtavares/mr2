inherited frmGrade: TfrmGrade
  Left = 327
  Top = 71
  Width = 876
  Height = 605
  Caption = 'Grade'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 860
    Height = 542
  end
  object Label1: TLabel [1]
    Left = 40
    Top = 54
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 29
    Top = 78
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 860
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 88
    Top = 48
    Width = 65
    Height = 19
    DataField = 'nCdGrade'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 88
    Top = 72
    Width = 650
    Height = 19
    DataField = 'cNmGrade'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBGridEh1: TDBGridEh [6]
    Left = 32
    Top = 104
    Width = 193
    Height = 449
    DataGrouping.GroupLevels = <>
    DataSource = dsItemGrade
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemGrade'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrade'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iTamanho'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTamanho'
        Footers = <>
        Width = 82
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Grade'
      'WHERE nCdGrade = :nPK')
    Left = 520
    object qryMasternCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryMastercNmGrade: TStringField
      FieldName = 'cNmGrade'
      Size = 50
    end
  end
  inherited dsMaster: TDataSource
    Left = 520
  end
  object qryItemGrade: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemGradeBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemGrade'
      'WHERE nCdGrade = :nPK')
    Left = 552
    Top = 128
    object qryItemGradenCdItemGrade: TAutoIncField
      FieldName = 'nCdItemGrade'
    end
    object qryItemGradenCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryItemGradeiTamanho: TIntegerField
      DisplayLabel = 'Grade|Tamanho'
      FieldName = 'iTamanho'
    end
    object qryItemGradecNmTamanho: TStringField
      DisplayLabel = 'Grade|Descri'#231#227'o'
      FieldName = 'cNmTamanho'
      FixedChar = True
      Size = 5
    end
  end
  object dsItemGrade: TDataSource
    DataSet = qryItemGrade
    Left = 552
    Top = 160
  end
end
