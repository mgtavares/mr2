inherited frmEmpresa: TfrmEmpresa
  Caption = 'Empresa'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 61
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 74
    Top = 70
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sigla'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 33
    Top = 94
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Oficial'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 13
    Top = 118
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Empresa'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 24
    Top = 142
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Matriz'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 67
    Top = 166
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 27
    Top = 187
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Logo Empresa'
    FocusControl = DBImage1
  end
  object Label8: TLabel [8]
    Left = 23
    Top = 352
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Logo Relat'#243'rio'
    FocusControl = DBImage2
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [11]
    Left = 104
    Top = 64
    Width = 65
    Height = 19
    DataField = 'cSigla'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [12]
    Left = 104
    Top = 88
    Width = 625
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [13]
    Left = 104
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroEmp'
    DataSource = dsMaster
    TabOrder = 4
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [14]
    Left = 104
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroMatriz'
    DataSource = dsMaster
    TabOrder = 5
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [15]
    Left = 104
    Top = 160
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 6
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [16]
    Tag = 1
    Left = 172
    Top = 112
    Width = 557
    Height = 19
    DataField = 'cNmTerceiroEmp'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [17]
    Tag = 1
    Left = 172
    Top = 136
    Width = 557
    Height = 19
    DataField = 'cNmTerceiroMatriz'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [18]
    Tag = 1
    Left = 172
    Top = 160
    Width = 557
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBImage1: TDBImage [19]
    Left = 104
    Top = 184
    Width = 225
    Height = 161
    DataField = 'imgLogoEmpresaGrande'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBImage2: TDBImage [20]
    Left = 104
    Top = 350
    Width = 89
    Height = 33
    DataField = 'imgLogoEmpresaRelat'
    DataSource = dsMaster
    TabOrder = 11
  end
  object cxButton1: TcxButton [21]
    Left = 333
    Top = 183
    Width = 89
    Height = 30
    Caption = 'Upload'
    TabOrder = 12
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FF76B2E6
      3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
      DA85B9E9FF00FFFF00FF4799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
      D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBFF00FFFF00FF3B97DBEFFAFE
      A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
      FB3B97DBFF00FFFF00FF3C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
      E0F77CE0F762DAF554D6F347D3F2E8F9FD3594DAFF00FFFF00FF3BA3DBF6FCFE
      C8F2FCB9EFFB91E5F88CE4F88AE3F882E1F781E2F86DDDF661DAF557D7F4E7F8
      FD3594DAFF00FFFF00FF3BA8DBFEFFFFF8FDFFF6FDFFF6FDFFE8FAFEAFECFA8E
      E4F889E4F87DE0F772DDF668DBF5E9F9FD3594DAFF00FFFF00FF39ADDBE8F6FB
      7EC5EA4AA3DF459FDE4CA3DFEDEFEDECEBE9E7E4E2E7E5E2E7E3E0E7E5E2F2E9
      E33594DAC69F85D7BFAF40AEDCF1FAFD94DEF593DCF481D5F260C0E95696C535
      94DA3594DA3594DA3594DA3594DA3594DA3594DAF2EEEAC89F8641B4DCF7FCFE
      8EE4F891DEF59FE0F5ACE1F6C29579F6F1ED47A270429D6B3C9766369160318B
      5A2C8655F7EDE6C59A7F3CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFC39679F7
      F1ECDCBE9EDEBD9B4F9ECC3178A1DAB791D6B28DF6EEE6C59B8059C2E061C3E2
      63C4E363C4E363C4E362C4E3BD987EF7F3EEDFC7AE65A66C39AD6A2FA85E4E97
      4ED2AE89F6EEE7C7A086FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
      F4EFE1CFBB40C08B4FC79948C28D31AA63CBAA89F6EEE7C7A086FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFC3967AF7F5F0E7D9C848C89C5ACFA853CA9D39B4
      75D2B79AF5EFE7C49B80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
      F5F4E9DBCC5EBC904ACA9F45C49558AE77DCC8B1F7F0E9C8A086FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFC8A791EBEBEAF7F5F4F7F5F4F7F5F4F7F5F4F7F5
      F4F7F5F4F2EEE8C69F85FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD8C3B5C8
      A692C69A7EC79B7FC39678C79A7FC79A7FC79A7FC59B80D7BFAF}
    LookAndFeel.NativeStyle = True
  end
  object cxButton2: TcxButton [22]
    Left = 197
    Top = 350
    Width = 89
    Height = 30
    Caption = 'Upload'
    TabOrder = 13
    OnClick = cxButton2Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FF76B2E6
      3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
      DA85B9E9FF00FFFF00FF4799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
      D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBFF00FFFF00FF3B97DBEFFAFE
      A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
      FB3B97DBFF00FFFF00FF3C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
      E0F77CE0F762DAF554D6F347D3F2E8F9FD3594DAFF00FFFF00FF3BA3DBF6FCFE
      C8F2FCB9EFFB91E5F88CE4F88AE3F882E1F781E2F86DDDF661DAF557D7F4E7F8
      FD3594DAFF00FFFF00FF3BA8DBFEFFFFF8FDFFF6FDFFF6FDFFE8FAFEAFECFA8E
      E4F889E4F87DE0F772DDF668DBF5E9F9FD3594DAFF00FFFF00FF39ADDBE8F6FB
      7EC5EA4AA3DF459FDE4CA3DFEDEFEDECEBE9E7E4E2E7E5E2E7E3E0E7E5E2F2E9
      E33594DAC69F85D7BFAF40AEDCF1FAFD94DEF593DCF481D5F260C0E95696C535
      94DA3594DA3594DA3594DA3594DA3594DA3594DAF2EEEAC89F8641B4DCF7FCFE
      8EE4F891DEF59FE0F5ACE1F6C29579F6F1ED47A270429D6B3C9766369160318B
      5A2C8655F7EDE6C59A7F3CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFC39679F7
      F1ECDCBE9EDEBD9B4F9ECC3178A1DAB791D6B28DF6EEE6C59B8059C2E061C3E2
      63C4E363C4E363C4E362C4E3BD987EF7F3EEDFC7AE65A66C39AD6A2FA85E4E97
      4ED2AE89F6EEE7C7A086FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
      F4EFE1CFBB40C08B4FC79948C28D31AA63CBAA89F6EEE7C7A086FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFC3967AF7F5F0E7D9C848C89C5ACFA853CA9D39B4
      75D2B79AF5EFE7C49B80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC79B80F7
      F5F4E9DBCC5EBC904ACA9F45C49558AE77DCC8B1F7F0E9C8A086FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFC8A791EBEBEAF7F5F4F7F5F4F7F5F4F7F5F4F7F5
      F4F7F5F4F2EEE8C69F85FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD8C3B5C8
      A692C69A7EC79B7FC39678C79A7FC79A7FC79A7FC59B80D7BFAF}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 512
    Top = 224
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
    object qryMasternCdTerceiroMatriz: TIntegerField
      FieldName = 'nCdTerceiroMatriz'
    end
    object qryMastercSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryMastercNmTerceiroEmp: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTerceiroEmp'
      LookupDataSet = qryTerceiro
      LookupKeyFields = 'nCdTerceiro'
      LookupResultField = 'cNmFantasia'
      KeyFields = 'nCdTerceiroEmp'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmTerceiroMatriz: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTerceiroMatriz'
      LookupDataSet = qryTerceiroMatriz
      LookupKeyFields = 'nCdTerceiro'
      LookupResultField = 'cNmFantasia'
      KeyFields = 'nCdTerceiroMatriz'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasterimgLogoEmpresaGrande: TBlobField
      FieldName = 'imgLogoEmpresaGrande'
    end
    object qryMasterimgLogoEmpresaRelat: TBlobField
      FieldName = 'imgLogoEmpresaRelat'
    end
  end
  inherited dsMaster: TDataSource
    Left = 512
    Top = 256
  end
  inherited qryID: TADOQuery
    Left = 544
    Top = 224
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 576
    Top = 224
  end
  inherited qryStat: TADOQuery
    Left = 544
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 576
    Top = 256
  end
  inherited ImageList1: TImageList
    Left = 608
    Top = 224
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmFantasia, cNmTerceiro'
      'FROM TERCEIRO')
    Left = 640
    Top = 224
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryTerceiroMatriz: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmFantasia, cNmTerceiro'
      'FROM TERCEIRO')
    Left = 872
    Top = 288
    object qryTerceiroMatriznCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroMatrizcNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryTerceiroMatrizcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 608
    Top = 256
  end
end
