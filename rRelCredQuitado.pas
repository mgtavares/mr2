unit rRelCredQuitado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, DBCtrls, StdCtrls, Mask,
  ER2Lookup, ImgList, ComCtrls, ToolWin, ExtCtrls, ER2Excel;

type
  TrptRelCredQuitado = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    qryTipoLoja: TADOQuery;
    qryTipoLojacNmLoja: TStringField;
    qryTipoLojanCdLoja: TIntegerField;
    dsTipoLoja: TDataSource;
    lblDtQuitacao: TLabel;
    MaskEdit1: TMaskEdit;
    lblAte: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit6: TMaskEdit;
    Label3: TLabel;
    Label6: TLabel;
    MaskEdit7: TMaskEdit;
    RadioGroup1: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelCredQuitado: TrptRelCredQuitado;

implementation

uses fMenu,rRelCredQuitado_view;

{$R *.dfm}

procedure TrptRelCredQuitado.ToolButton1Click(Sender: TObject);
var
  ObjRel    : TrptRelCredQuitado_view;
  MsgFiltro : string;
  iLinha    : integer;
begin
  inherited;

  ObjRel := TrptRelCredQuitado_view.Create(NIl);
  MsgFiltro := '';

  if (trim(MaskEdit1.Text)= '/  /') or (trim(MaskEdit2.Text)= '/  /')then
  begin
      MensagemAlerta('Preencha a data da liquida��o');
      abort;
  end;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  MsgFiltro := 'Loja ' + DBEdit1.Text + #13 + 'Data de Venda       '+ MaskEdit6.Text + ' At�   ' + MaskEdit7.Text+ #13 + 'Data de Liquida��o ' + MaskEdit1.Text  + '  At�  ' + MaskEdit2.Text;;

  ObjRel.qryRelCredQuitado.Close;
  ObjRel.lblFiltro1.Caption := MsgFiltro;

  if (trim(ER2LookupMaskEdit1.text) <> '/  /') then
      ObjRel.qryRelCredQuitado.Parameters.ParamByName('Loja').value       := ER2LookupMaskEdit1.Text;

  if (trim(MaskEdit6.Text) <> '/  /') then
      ObjRel.qryRelCredQuitado.Parameters.ParamByName('DtVendaIni').value := MaskEdit6.Text;

  if (trim(MaskEdit7.Text) <> '/  /') then
      ObjRel.qryRelCredQuitado.Parameters.ParamByName('DtVendaFin').value := MaskEdit7.Text;

  if (trim(MaskEdit1.Text) <> '/  /') then
      ObjRel.qryRelCredQuitado.Parameters.ParamByName('DtLiqIni').value   := MaskEdit1.Text;

  if (trim(MaskEdit2.Text) <> '/  /') then
      ObjRel.qryRelCredQuitado.Parameters.ParamByName('DtLiqFin').value   := MaskEdit2.Text;

  ObjRel.qryRelCredQuitado.Open;

  try
      try
          case (RadioGroup1.ItemIndex) of

          0: begin
                 ObjRel.QuickRep1.PreviewModal;
             end;

          1: begin
                frmMenu.mensagemUsuario('Exportando Planilha...');

                { -- formata c�lulas -- }
                ER2Excel.Celula['A1'].Mesclar('J1');
                ER2Excel.Celula['A2'].Mesclar('J2');
                ER2Excel.Celula['A3'].Mesclar('J3');
                ER2Excel.Celula['A4'].Mesclar('J4');
                ER2Excel.Celula['A5'].Mesclar('J5');

                ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A1'].Range('J4');
                ER2Excel.Celula['D4'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A5'].Range('J5');
                ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
                ER2Excel.Celula['A6'].Range('J6');

                { -- inseri informa��es do cabe�alho -- }
                ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                ER2Excel.Celula['A2'].Text := 'Rel. Credi�rio Quitado';
                ER2Excel.Celula['A4'].Text := 'Loja Emissora: ' + DBEdit1.Text + ' - Dt. Venda: ' + MaskEdit6.Text + ' at� ' + MaskEdit7.Text + ' - Dt. Liq: ' + MaskEdit1.Text + ' at� ' + MaskEdit2.Text;
                ER2Excel.Celula['A6'].Text := 'C�d. Credi�rio';
                ER2Excel.Celula['B6'].Text := 'Data Venda';
                ER2Excel.Celula['C6'].Text := 'C�d. Cliente';
                ER2Excel.Celula['D6'].Text := 'Nome Cliente';
                ER2Excel.Celula['E6'].Text := 'Parcela';
                ER2Excel.Celula['F6'].Text := 'Val. Credi�rio';
                ER2Excel.Celula['G6'].Text := 'Data Liq.';

                iLinha := 7;

                ER2Excel.Celula['A1'].Congelar('A7');

                objRel.qryRelCredQuitado.First;

                while (not objRel.qryRelCredQuitado.Eof) do
                begin
                    ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadonCdCrediario.Value;
                    ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadoDataVenda.Value;
                    ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadonCdTerceiro.Value;
                    ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadocNmTerceiro.Value;
                    ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadoiParcelas.Value;
                    ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadonValCrediario.Value;
                    ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.qryRelCredQuitadoDataLiq.Value;

                    objRel.qryRelCredQuitado.Next;

                    Inc(iLinha);
                end;

                ER2Excel.AutoSizeCol := true;

                { -- exporta planilha e limpa result do componente -- }
                ER2Excel.ExportXLS;
                ER2Excel.CleanupInstance;
             end;
          end;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
      end;
   finally
      FreeAndNil(objRel);
   end;


end;

initialization
  RegisterClass(TrptRelCredQuitado) ;

end.
