unit fRecebimentoLeituraXML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBGridEhGrouping, cxLookAndFeelPainters, DBGridEh,
  DBCtrls, Mask, DBCtrlsEh, DBLookupEh, cxButtons, GridsEh, cxPC,
  cxControls, ToolCtrlsEh;

type
  TfrmRecebimentoLeituraXML = class(TfrmProcesso_Padrao)
    qryNFeXML: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsNFeXML: TDataSource;
    qryProdutoFornecedor: TADOQuery;
    qryProdutoFornecedornCdProduto: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutoFornecedorAux: TADOQuery;
    qryProdutoFornecedorAuxnCdProdutoFornecedor: TAutoIncField;
    qryProdutoFornecedorAuxnCdProduto: TIntegerField;
    qryProdutoFornecedorAuxnCdTerceiro: TIntegerField;
    qryProdutoFornecedorAuxnPercentCompra: TBCDField;
    qryProdutoFornecedorAuxcNrContrato: TStringField;
    qryProdutoFornecedorAuxnPrecoUnit: TBCDField;
    qryProdutoFornecedorAuxnCdMoeda: TIntegerField;
    qryProdutoFornecedorAuxnCdCondPagto: TIntegerField;
    qryProdutoFornecedorAuxdDtValidadeIni: TDateTimeField;
    qryProdutoFornecedorAuxdDtValidadeFim: TDateTimeField;
    qryProdutoFornecedorAuxnPercIPI: TBCDField;
    qryProdutoFornecedorAuxnPercICMSSub: TBCDField;
    qryProdutoFornecedorAuxcReferencia: TStringField;
    qryProdutoFornecedorAuxcCatalogo: TStringField;
    qryProdutoFornecedorAuxnPrecoUnitEsp: TBCDField;
    qryProdutoFornecedorAuxnCdTipoPedidoCompra: TIntegerField;
    qryProdutoFornecedorAuxnCdTipoRessuprimentoFornec: TIntegerField;
    qryProdutoFornecedorAuxnPercBCICMSSub: TBCDField;
    qryProdutoFornecedorAuxnPercIVA: TBCDField;
    qryProdutoFornecedorAuxnPercCredICMS: TBCDField;
    qryProdutoFornecedorAuxcFlgDescST: TIntegerField;
    qryProdutoFornecedorAuxcCdProdutoFornecedor: TStringField;
    qryItemRecebimento: TADOQuery;
    qryItemRecebimentonCdItemRecebimento: TIntegerField;
    qryItemRecebimentonCdRecebimento: TIntegerField;
    qryItemRecebimentonCdItemRecebimentoPai: TIntegerField;
    qryItemRecebimentonCdPedido: TIntegerField;
    qryItemRecebimentonCdTipoItemPed: TIntegerField;
    qryItemRecebimentonCdProduto: TIntegerField;
    qryItemRecebimentocNmItem: TStringField;
    qryItemRecebimentonCdItemPedido: TIntegerField;
    qryItemRecebimentonQtde: TBCDField;
    qryItemRecebimentonValTotal: TBCDField;
    qryItemRecebimentocEANFornec: TStringField;
    qryItemRecebimentonCdTabStatusItemPed: TIntegerField;
    qryItemRecebimentocNmTabStatusItemPed: TStringField;
    qryItemRecebimentocFlgDiverg: TIntegerField;
    qryItemRecebimentonCdTabTipoDiverg: TIntegerField;
    qryItemRecebimentocNmTabTipoDiverg: TStringField;
    qryItemRecebimentonCdUsuarioAutorDiverg: TIntegerField;
    qryItemRecebimentodDtAutorDiverg: TDateTimeField;
    qryItemRecebimentonPercIPI: TBCDField;
    qryItemRecebimentonValIPI: TBCDField;
    qryItemRecebimentonPercICMSSub: TBCDField;
    qryItemRecebimentonValICMSSub: TBCDField;
    qryItemRecebimentonPercDesconto: TBCDField;
    qryItemRecebimentonValDesconto: TBCDField;
    qryItemRecebimentonPercIPIPed: TBCDField;
    qryItemRecebimentonPercICMSSubPed: TBCDField;
    qryItemRecebimentonValTotalLiq: TFloatField;
    qryItemRecebimentocFlgDescAbatUnitario: TIntegerField;
    qryItemRecebimentonValDescAbatUnit: TBCDField;
    qryItemRecebimentonValUnitario: TFloatField;
    qryItemRecebimentonValUnitarioEsp: TFloatField;
    qryItemRecebimentonValUnitarioPed: TFloatField;
    qryItemRecebimentonValCustoFinal: TFloatField;
    qryItemRecebimentonCdServidorOrigem: TIntegerField;
    qryItemRecebimentodDtReplicacao: TDateTimeField;
    qryItemRecebimentonPercRedBaseCalcICMS: TFloatField;
    qryItemRecebimentonBaseCalcICMS: TFloatField;
    qryItemRecebimentonPercRedBaseCalcSubTrib: TFloatField;
    qryItemRecebimentonPercAliqICMS: TFloatField;
    qryItemRecebimentonValSeguro: TFloatField;
    qryItemRecebimentonValICMS: TFloatField;
    qryItemRecebimentonValDifAliqICMS: TFloatField;
    qryItemRecebimentonValFrete: TFloatField;
    qryItemRecebimentonValAcessorias: TFloatField;
    qryItemRecebimentocCdProduto: TStringField;
    qryItemRecebimentocCdST: TStringField;
    qryItemRecebimentonCdTipoICMS: TIntegerField;
    qryItemRecebimentonCdTipoIPI: TIntegerField;
    qryItemRecebimentocFlgImportacao: TIntegerField;
    qryItemRecebimentocFlgGeraLivroFiscal: TIntegerField;
    qryItemRecebimentocCFOP: TStringField;
    qryItemRecebimentonValBaseCalcSubTrib: TFloatField;
    qryItemRecebimentocFlgGeraCreditoICMS: TIntegerField;
    qryItemRecebimentocFlgGeraCreditoIPI: TIntegerField;
    qryItemRecebimentonValAliquotaII: TFloatField;
    qryItemRecebimentocNmTipoICMS: TStringField;
    qryItemRecebimentocNmTipoIPI: TStringField;
    qryTerceirocCnpjCpf: TStringField;
    qryProdutoFornecedorAux1: TADOQuery;
    qryItemRecebimentonAliqICMSInterna: TFloatField;
    qryItemRecebimentonPercIVA: TFloatField;
    qryItemRecebimentonPercBaseCalcIPI: TFloatField;
    qryItemRecebimentonValBaseIPI: TFloatField;
    qryItemRecebimentocNCM: TStringField;
    qryItemRecebimentocCFOPNF: TStringField;
    qryItemRecebimentocCdSTIPI: TStringField;
    qryNFeXMLnCdProdutoFornecedor: TAutoIncField;
    qryNFeXMLcCdProdutoFornecedor: TStringField;
    qryNFeXMLcNmProdutoFornecedor: TStringField;
    qryNFeXMLnQtdeFornecedor: TBCDField;
    qryNFeXMLcUnMedidaFornecedor: TStringField;
    qryNFeXMLnCdProduto: TIntegerField;
    qryNFeXMLcNmProduto: TStringField;
    qryNFeXMLcUnMedida: TStringField;
    qryNFeXMLnCdRecebimento: TIntegerField;
    qryNFeXMLcCdSt: TStringField;
    qryNFeXMLcCFOP: TStringField;
    qryNFeXMLcNCM: TStringField;
    qryNFeXMLnValUnitario: TBCDField;
    qryNFeXMLnValDesconto: TBCDField;
    qryNFeXMLnValTotal: TBCDField;
    qryNFeXMLnValBaseIcms: TBCDField;
    qryNFeXMLnValIcms: TBCDField;
    qryNFeXMLnPercAliqIcms: TBCDField;
    qryNFeXMLnPercRedBaseIcms: TBCDField;
    qryNFeXMLnValBaseIPI: TBCDField;
    qryNFeXMLnPercAliqIpi: TBCDField;
    qryNFeXMLnValIPI: TBCDField;
    qryNFeXMLcCSTIPI: TStringField;
    qryNFeXMLnPercIVA: TBCDField;
    qryNFeXMLnPercAliqIcmsST: TBCDField;
    qryNFeXMLnPercRedBaseIcmsST: TBCDField;
    qryNFeXMLnValIcmsST: TBCDField;
    qryNFeXMLnValBaseIcmsSt: TBCDField;
    qryNFeXMLnValSeguro: TBCDField;
    qryNFeXMLnValDespAcessorias: TBCDField;
    qryNFeXMLnvalFrete: TBCDField;
    qryNFeXMLcOrigemMercadoria: TStringField;
    qryNFeXMLcCSTPIS: TStringField;
    qryNFeXMLnValBasePIS: TBCDField;
    qryNFeXMLnAliqPIS: TBCDField;
    qryNFeXMLnValPIS: TBCDField;
    qryNFeXMLcCSTCOFINS: TStringField;
    qryNFeXMLnValBaseCOFINS: TBCDField;
    qryNFeXMLnAliqCOFINS: TBCDField;
    qryNFeXMLnValCOFINS: TBCDField;
    qryNFeXMLnValBaseII: TBCDField;
    qryNFeXMLnValDespAdu: TBCDField;
    qryNFeXMLnValII: TBCDField;
    qryNFeXMLnValIOFImp: TBCDField;
    qryNFeXMLcEAN: TStringField;
    qryProdutoEAN: TADOQuery;
    qryProdutoEANnCdProduto: TIntegerField;
    qryProdutoEANcNmProduto: TStringField;
    qryProdutoEANcUnidadeMedida: TStringField;
    qryItemRecebimentocCSTPIS: TStringField;
    qryItemRecebimentonValBasePIS: TFloatField;
    qryItemRecebimentonAliqPIS: TFloatField;
    qryItemRecebimentonValPIS: TFloatField;
    qryItemRecebimentocCSTCOFINS: TStringField;
    qryItemRecebimentonValBaseCOFINS: TFloatField;
    qryItemRecebimentonAliqCOFINS: TFloatField;
    qryItemRecebimentonValCOFINS: TFloatField;
    qryItemRecebimentonValBaseII: TFloatField;
    qryItemRecebimentonValDespAdu: TFloatField;
    qryItemRecebimentonValII: TFloatField;
    qryItemRecebimentonValIOFImp: TFloatField;
    qryItemRecebimentocFlgGeraCreditoPIS: TIntegerField;
    qryItemRecebimentocFlgGeraCreditoCOFINS: TIntegerField;
    ToolButton4: TToolButton;
    btCadProduto: TToolButton;
    btCadProdutoGrade: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    qryNFeXMLcNossoEAN: TStringField;
    qryItemRecebimentocCdSTIPIEntrada: TStringField;
    qryItemRecebimentocUnidadeMedida: TStringField;
    qryNFeXMLcFlgItemAD: TIntegerField;
    ToolButton7: TToolButton;
    qryNaturezaOperacao: TADOQuery;
    qryNaturezaOperacaocCFOP: TStringField;
    qryInsereItemPai: TADOQuery;
    qryProdutonCdGrade: TIntegerField;
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryNFeXMLBeforeDelete(DataSet: TDataSet);
    procedure qryNFeXMLnCdProdutoChange(Sender: TField);
    procedure FormShow(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure btCadProdutoClick(Sender: TObject);
    procedure btCadProdutoGradeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdRecebimento : integer ;
  end;

var
  frmRecebimentoLeituraXML: TfrmRecebimentoLeituraXML;

implementation

uses fMenu, fLookup_Padrao, fProdutoERP, fProduto;

{$R *.dfm}

procedure TfrmRecebimentoLeituraXML.ToolButton2Click(Sender: TObject);
begin
  inherited;
  qryNFeXML.Close;
  qryProdutoFornecedor.Close;
  qryProduto.Close;
  qryTerceiro.Close;
end;

procedure TfrmRecebimentoLeituraXML.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryNFeXML.RecordCount > 0) then
  begin

       PosicionaQuery(qryItemRecebimento,qryNFeXMLnCdRecebimento.AsString);

       if (not qryItemRecebimento.eof) then
       begin
           MensagemAlerta('Exclua os itens do recebimento antes de processar a leitura do arquivo XML.');
           DbGridEh1.Col := 5;
           DbGridEh1.SetFocus;
           Exit;
       end;

       //verifica itens sem produto

       qryNFeXML.First ;

       while not qryNFeXML.Eof do
       begin
           if ((qryNFeXMLnCdProduto.Value = 0) or (qryNFeXMLcNmProduto.Value = '')) and (qryNFeXMLcFlgItemAD.Value = 0) then
           begin
               MensagemAlerta('Selecione o NOSSO Produto para o Produto ' + qryNFeXMLcNmProdutoFornecedor.Value);
               DbGridEh1.Col := 6;
               DbGridEh1.SetFocus;
               Abort;
           end;

           if ((qryNFeXMLnCdProduto.Value > 0) or (qryNFeXMLcNmProduto.Value <> '')) and (qryNFeXMLcFlgItemAD.Value = 1) then
           begin
               MensagemAlerta('N�o � permitido informar NOSSO PRODUTO para itens AD.' +#13#13+ 'Item do Fornecedor: ' + qryNFeXMLcNmProdutoFornecedor.Value);
               DbGridEh1.Col := 6;
               DbGridEh1.SetFocus;
               Abort;
           end;

           // verifica se algum c�digo de barra de fornecedor � diferente do NOSSO c�digo de barra.

           if ( trim( qryNFeXMLcEAN.Value ) <> '' ) and ( trim( qryNFeXMLcNossoEAN.Value ) <> '' ) then
           begin

               if ( trim( qryNFeXMLcEAN.Value ) <> trim( qryNFeXMLcNossoEAN.Value ) ) then
               begin

                   if ( MessageDlg('O C�digo EAN do Fornecedor � diferente do NOSSO c�digo EAN. Tem certeza de deseja continuar ?' + #13#13 + 'Produto: ' + qryNFeXMLcNmProdutoFornecedor.Value,mtConfirmation,[mbYes,mbNo],0) = MrNo) then
                       exit ;

               end ;

           end ;

           qryNFeXML.Next;
       end;

       qryNFeXML.First ;

      //atualiza os produtos na tabela ProdutoFornecedor
      qryProdutoFornecedorAux.Close;
      qryProdutoFornecedorAux.Open;

      qryNFeXML.First;
      while not qryNFeXML.Eof do
      begin

          if (qryNFeXMLcFlgItemAD.Value = 0) then
          begin

              qryProdutoFornecedor.Close;
              qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
              qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := qryNFeXMLcCdProdutoFornecedor.Value;
              qryProdutoFornecedor.Open;

              qryProdutoFornecedorAux1.Close;
              qryProdutoFornecedorAux1.Parameters.ParamByName('nCdProduto').Value  := qryNFeXMLnCdProduto.Value;
              qryProdutoFornecedorAux1.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
              qryProdutoFornecedorAux1.Open;

              if ((qryProdutoFornecedor.Eof) and (qryProdutoFornecedorAux1.Eof)) then
              begin

                  qryProdutoFornecedorAux.Insert;
                  qryProdutoFornecedorAuxnCdProduto.Value           := qryNFeXMLnCdProduto.Value;
                  qryProdutoFornecedorAuxcCdProdutoFornecedor.Value := qryNFeXMLcCdProdutoFornecedor.Value;
                  qryProdutoFornecedorAuxnCdTerceiro.Value          := qryTerceironCdTerceiro.Value;
                  qryProdutoFornecedorAuxnPrecoUnit.Value           := qryNFeXMLnValUnitario.Value;
                  qryProdutoFornecedorAuxnCdMoeda.Value             := 1;
                  qryProdutoFornecedorAuxnPercICMSSub.Value         := qryNFeXMLnpercAliqICMSSt.Value;
                  qryProdutoFornecedorAuxnPercIPI.Value             := qryNFeXMLnPercAliqIPI.Value;
                  qryProdutoFornecedorAuxnPercBCICMSSub.Value       := qryNFeXMLnPercRedBaseICMSSt.Value;
                  qryProdutoFornecedorAux.Post;
                  
              end;

          end ;

          qryNFeXML.Next;

      end;

       //gravar os itens da nfe no recebimento

       qryNFeXML.First;

       while not qryNFeXML.Eof do
       begin
               qryItemRecebimento.Insert;

               qryItemRecebimentonCdItemRecebimento.Value      := frmMenu.fnProximoCodigo('ITEMRECEBIMENTO') ;
               qryItemRecebimentonCdRecebimento.Value          := qryNFeXMLnCdRecebimento.Value;

               if (qryNFeXMLcFlgItemAD.Value = 0) then
               begin
                   qryItemRecebimentonCdProduto.Value     := qryNFeXMLnCdProduto.Value;
                   qryItemRecebimentocCdProduto.Value     := intToStr(qryNFeXMLnCdProduto.Value);
                   qryItemRecebimentocNmItem.Value        := qryNFeXMLcNmProduto.Value;

                   { -- verifica classifica��o do item -- }
                   PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

                   if (qryProdutonCdGrade.Value > 0) then
                       qryItemRecebimentonCdTipoItemPed.Value := 4  //produto grade
                   else
                       qryItemRecebimentonCdTipoItemPed.Value := 2; //produto ERP
               end
               else
               begin
                   qryItemRecebimentocCdProduto.Value     := 'AD' ;
                   qryItemRecebimentocNmItem.Value        := qryNFeXMLcNmProdutoFornecedor.Value;
                   qryItemRecebimentonCdTipoItemPed.Value := 5;
               end ;

               qryItemRecebimentocUnidadeMedida.Value           := Uppercase( qryNFeXMLcUnMedidaFornecedor.Value );
               qryItemRecebimentonQtde.Value                    := qryNFeXMLnQtdeFornecedor.Value;
               qryItemRecebimentonValUnitario.Value             := qryNFeXMLnValUnitario.Value;
               qryItemRecebimentonValTotal.Value                := qryNFeXMLnValTotal.Value;
               qryItemRecebimentonValCustoFinal.Value           := ((qryNFeXMLnValTotal.Value + qryNFeXMLnValICMSSt.Value + qryNFeXMLnValIPI.Value) / qryNFeXMLnQtdeFornecedor.Value ) ;

               qryItemRecebimentonValDesconto.Value             := qryNFeXMLnValDesconto.Value;

               qryItemRecebimentonValFrete.Value                := qryNFeXMLnValFrete.Value;
               qryItemRecebimentonValSeguro.Value               := qryNFeXMLnValSeguro.Value;
               qryItemRecebimentonValAcessorias.Value           := qryNFeXMLnValDespAcessorias.Value;

               if (qryNFeXMLnValTotal.Value > 0) then
                   qryItemRecebimentonPercRedBaseCalcICMS.Value := frmMenu.TBRound( ((qryNFeXMLnValBaseICMS.Value / qryNFeXMLnValTotal.Value ) * 100), 2) ;

               qryItemRecebimentonBaseCalcICMS.Value            := qryNFeXMLnValBaseICMS.Value;
               qryItemRecebimentonPercAliqICMS.Value            := qryNFeXMLnPercAliqICMS.Value;
               qryItemRecebimentonValICMS.Value                 := qryNFeXMLnValICMS.Value;

               qryItemRecebimentonPercIVA.Value                 := qryNFeXMLnPercIVA.Value ;

               if (qryNFeXMLnPercIVA.Value = 0) and (qryNFeXMLnValBaseICMSSt.Value > 0) then
                   qryItemRecebimentonPercIVA.Value := frmMenu.TBRound( ( ( ( qryNFeXMLnValBaseICMSSt.Value / (qryNFeXMLnValTotal.Value + qryNFeXMLnValIPI.Value) ) * 100) - 100) ,0) ;

               qryItemRecebimentonPercRedBaseCalcSubTrib.Value  := qryNFeXMLnPercRedBaseICMSSt.Value;
               qryItemRecebimentonValBaseCalcSubTrib.Value      := qryNFeXMLnValBaseICMSSt.Value ;
               qryItemRecebimentonPercICMSSub.Value             := qryNFeXMLnpercAliqICMSSt.Value;
               qryItemRecebimentonValICMSSub.Value              := qryNFeXMLnValICMSSt.Value;

               qryItemRecebimentocCdSt.Value                    := qryNFeXMLcOrigemMercadoria.Value + qryNFeXMLcCdSt.Value;

               qryItemRecebimentocNCM.Value                     := qryNFeXMLcNCM.Value;
               qryItemRecebimentonValBaseIPI.Value              := qryNFeXMLnValBaseIPI.Value;

               if (qryNFeXMLnValTotal.Value > 0) then
                   qryItemRecebimentonPercBaseCalcIPI.Value     := frmMenu.TBRound(((qryNFeXMLnValBaseIPI.Value / qryNFeXMLnValTotal.Value ) * 100),2) ;

               qryItemRecebimentonPercIPI.Value                 := qryNFeXMLnPercAliqIPI.Value;
               qryItemRecebimentonValIPI.Value                  := qryNFeXMLnValIPI.Value;
               qryItemRecebimentocCdSTIPI.Value                 := qryNFeXMLcCSTIPI.Value;
               qryItemRecebimentocCFOPNF.Value                  := qryNFeXMLcCFOP.Value;

               qryNaturezaOperacao.Close;
               qryNaturezaOperacao.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento;
               qryNaturezaOperacao.Parameters.ParamByName('cCFOPPedido').Value    := qryItemRecebimentocCFOPNF.Value;
               qryNaturezaOperacao.Parameters.ParamByName('cFlgEntSai').Value     := 'S';
               qryNaturezaOperacao.Open;

               if (not qryNaturezaOperacao.IsEmpty) then
                   qryItemRecebimentocCFOP.Value := qryNaturezaOperacaocCFOP.Value;

               if (qryItemRecebimentonPercRedBaseCalcICMS.Value = 100) then
                   qryItemRecebimentonPercRedBaseCalcICMS.Value := 0 ;

               if (qryItemRecebimentonPercBaseCalcIPI.Value = 100) then
                   qryItemRecebimentonPercBaseCalcIPI.Value     := 0 ;

               qryItemRecebimentocCSTPIS.Value                  := qryNFeXMLcCSTPIS.Value;
               qryItemRecebimentonValBasePIS.Value              := qryNFeXMLnValBasePIS.Value;
               qryItemRecebimentonAliqPIS.Value                 := qryNFeXMLnAliqPIS.Value;
               qryItemRecebimentonValPIS.Value                  := qryNFeXMLnValPIS.Value;
               qryItemRecebimentocCSTCOFINS.Value               := qryNFeXMLcCSTCOFINS.Value;
               qryItemRecebimentonValBaseCOFINS.Value           := qryNFeXMLnValBaseCOFINS.Value;
               qryItemRecebimentonAliqCOFINS.Value              := qryNFeXMLnAliqCOFINS.Value;
               qryItemRecebimentonValCOFINS.Value               := qryNFeXMLnValCOFINS.Value;
               qryItemRecebimentonValBaseII.Value               := qryNFeXMLnValBaseII.Value;
               qryItemRecebimentonValDespAdu.Value              := qryNFeXMLnValDespAdu.Value;
               qryItemRecebimentonValII.Value                   := qryNFeXMLnValII.Value;
               qryItemRecebimentonValIOFImp.Value               := qryNFeXMLnValIOFImp.Value;

               qryItemRecebimento.Post;

               qryNFeXML.Next;
       end

  end;

  //Executa query para inserir produto pai no recebimento caso for varejo
  qryInsereItemPai.Close;
  qryInsereItemPai.Parameters.ParamByName('nPK').Value := qryNFeXMLnCdRecebimento.Value;
  qryInsereItemPai.ExecSQL;

  Close;

end;

procedure TfrmRecebimentoLeituraXML.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 6) then
        begin

            if (qryNFeXML.State = dsBrowse) then
                 qryNFeXML.Edit ;

            if ((qryNFeXML.State = dsInsert) or (qryNFeXML.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(68);

                If (nPK > 0) then
                begin
                    qryNFeXMLnCdProduto.Value := nPK ;

                    PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);
                    qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
                    qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
                end;
            end ;

        end ;

    end ;

   end;

end ;


procedure TfrmRecebimentoLeituraXML.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRecebimentoLeituraXML.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}
end;

procedure TfrmRecebimentoLeituraXML.qryNFeXMLBeforeDelete(
  DataSet: TDataSet);
begin
    MensagemAlerta('Item n�o pode ser Excluido.');
    Abort;
    inherited;
end;

procedure TfrmRecebimentoLeituraXML.qryNFeXMLnCdProdutoChange(
  Sender: TField);
begin
  inherited;

  qryProduto.Close;
  PosicionaQuery(qryProduto,qryNFeXMLnCdProduto.AsString);

  qryNFeXMLcNmProduto.Value := '';
  qryNFeXMLcUnMedida.Value  := '';

  if (not qryProduto.Eof) then
  begin
      qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
      qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;
  end;

end;

procedure TfrmRecebimentoLeituraXML.FormShow(Sender: TObject);
begin
  inherited;

  btCadProduto.Enabled      := ( frmMenu.fnValidaUsuarioAPL('FRMPRODUTOERP') ) ;
  btCadProdutoGrade.Enabled := ( frmMenu.fnValidaUsuarioAPL('FRMPRODUTO') ) ;

  DBGridEh1.SetFocus;
  
end;

procedure TfrmRecebimentoLeituraXML.ToolButton6Click(Sender: TObject);
begin
  inherited;

  qryNFeXML.First;

  while not qryNFeXML.Eof do
  begin

      {-- s� vincula os produtos que n�o tem v�nculo --}
      if (qryNFeXMLnCdProduto.Value = 0) then
      begin

          qryNFeXML.Edit;
          
          {-- primeiro procura pelo c�digo EAN do produto para ver se encontra no cadastro de produtos --}
          if (qryNFeXMLcEAN.Value <> '') then
          begin

              qryProdutoEAN.Close;
              PosicionaQuery( qryProdutoEAN , qryNFeXMLcEAN.Value);

              if (not qryProdutoEAN.eof) then
              begin
                  qryNFeXMLnCdProduto.Value := qryProdutoEANnCdProduto.Value;
                  qryNFeXMLcNmProduto.Value := qryProdutoEANcNmProduto.Value;
                  qryNFeXMLcUnMedida.Value  := qryProdutoEANcUnidadeMedida.Value;
              end ;

          end ;

          {-- se n�o achou pelo c�digo de barras, tenta vinculando por c�digo fabricante --}
          if (qryNFeXMLnCdProduto.Value = 0) then
          begin

            qryProdutoFornecedor.Close;
            qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := qryNFeXMLcCdProdutoFornecedor.Value;
            qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
            qryProdutoFornecedor.Open;

            if not qryProdutoFornecedor.eof then
            begin

                qryProduto.Close;
                qryProduto.Parameters.ParamByName('nPk').Value := qryProdutoFornecedornCdProduto.Value;
                qryProduto.Open;

                if not qryProduto.eof then
                begin

                    qryNFeXMLnCdProduto.Value := qryProdutonCdProduto.Value;
                    qryNFeXMLcNmProduto.Value := qryProdutocNmProduto.Value;
                    qryNFeXMLcUnMedida.Value  := qryProdutocUnidadeMedida.Value;

                end ;

            end ;

          end ;

          qryNFeXML.Post;

      end ;

      qryNFeXML.Next ;
      
  end ;

  qryNFeXML.First ;

end;

procedure TfrmRecebimentoLeituraXML.btCadProdutoClick(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimentoLeituraXML.btCadProdutoGradeClick(
  Sender: TObject);
var
  objForm : TfrmProduto;
begin
  inherited;

  objForm := TfrmProduto.Create( Self );
  showForm( objForm , TRUE ) ;

end;

end.
