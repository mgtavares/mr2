inherited frmAutoriza_devolucao: TfrmAutoriza_devolucao
  Left = 301
  Top = 182
  Width = 811
  Height = 450
  Caption = 'Autoriza'#231#227'o Devolu'#231#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 795
    Height = 383
  end
  inherited ToolBar1: TToolBar
    Width = 795
    ButtonWidth = 115
    inherited ToolButton1: TToolButton
      Caption = 'Aprovar Devolu'#231#227'o'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 795
    Height = 383
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'dDtPedido'
        Footers = <>
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoVencido'
        Footers = <>
        Width = 116
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoaVencer'
        Footers = <>
        Width = 112
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 488
    Top = 120
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPedido'
      '      ,dDtPedido'
      '      ,cNmTerceiro'
      '      ,nValPedido'
      '      ,IsNull((SELECT Sum(nSaldoTit)'
      '                 FROM Titulo'
      '                WHERE nCdTerceiro = Pedido.nCdTerceiro'
      '                  AND nCdEmpresa  = Pedido.nCdEmpresa'
      '                  AND cSenso      = '#39'C'#39
      '                  AND dDtBloqTit IS NULL'
      '                  AND dDtCancel IS NULL'
      
        '                  AND dDtVenc < dbo.fn_OnlyDate(GetDate())),0) n' +
        'SaldoVencido'
      '      ,IsNull((SELECT Sum(nSaldoTit)'
      '                 FROM Titulo'
      '                WHERE nCdTerceiro = Pedido.nCdTerceiro'
      '                  AND nCdEmpresa  = Pedido.nCdEmpresa'
      '                  AND cSenso      = '#39'C'#39
      '                  AND dDtBloqTit IS NULL'
      '                  AND dDtCancel IS NULL'
      
        '                  AND dDtVenc >= dbo.fn_OnlyDate(GetDate())),0) ' +
        'nSaldoaVencer'
      '  FROM Pedido'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      ' WHERE cFlgDevolucVenda = 1'
      '   AND cFlgDevPag       = 0'
      '   AND nCdEmpresa       = :nPK'
      '   AND nCdTabStatusPed  = 2 --Analise Financeiro')
    Left = 456
    Top = 120
    object qryMasternCdPedido: TIntegerField
      DisplayLabel = 'Devolu'#231#245'es Aguardando Aprova'#231#227'o Financeira|N'#250'mero'
      FieldName = 'nCdPedido'
    end
    object qryMasterdDtPedido: TDateTimeField
      DisplayLabel = 'Devolu'#231#245'es Aguardando Aprova'#231#227'o Financeira|Data Devoluc.'
      FieldName = 'dDtPedido'
    end
    object qryMastercNmTerceiro: TStringField
      DisplayLabel = 'Devolu'#231#245'es Aguardando Aprova'#231#227'o Financeira|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternValPedido: TBCDField
      DisplayLabel = 'Devolu'#231#245'es Aguardando Aprova'#231#227'o Financeira|Val. Devoluc.'
      FieldName = 'nValPedido'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoVencido: TBCDField
      DisplayLabel = 'Posi'#231#227'o Financeira|Saldo Vencido'
      FieldName = 'nSaldoVencido'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryMasternSaldoaVencer: TBCDField
      DisplayLabel = 'Posi'#231#227'o Financeira|Saldo a Vencer'
      FieldName = 'nSaldoaVencer'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 456
    Top = 152
  end
  object SP_AUTORIZA_DEVOLUCAO_VENDA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AUTORIZA_DEVOLUCAO_VENDA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 488
    Top = 152
  end
end
