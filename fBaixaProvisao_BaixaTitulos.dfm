inherited frmBaixaProvisao_BaixaTitulos: TfrmBaixaProvisao_BaixaTitulos
  Left = 271
  Top = 152
  Width = 901
  Height = 526
  BorderIcons = [biSystemMenu]
  Caption = 'Baixa de Provis'#227'o'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 105
    Width = 885
    Height = 383
  end
  inherited ToolBar1: TToolBar
    Width = 885
    ButtonWidth = 116
    inherited ToolButton1: TToolButton
      Caption = 'Efetivar Pagamento'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 116
    end
    inherited ToolButton2: TToolButton
      Left = 124
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 105
    Width = 885
    Height = 383
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulos
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 38
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'nCdSP'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 275
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 133
      end
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
        Width = 26
      end
      item
        EditButtons = <>
        FieldName = 'cAgencia'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'cNumConta'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'cDigitoConta'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
        Width = 19
      end
      item
        EditButtons = <>
        FieldName = 'cNmTitularConta1'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cCdBarraTP'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
        Width = 170
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 885
    Height = 76
    Align = alTop
    Caption = ' Dados Adicionais da Baixa '
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 27
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero do Cheque'
      Color = clWhite
      ParentColor = False
    end
    object Label2: TLabel
      Tag = 1
      Left = 208
      Top = 27
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Favorecido'
    end
    object Label3: TLabel
      Tag = 1
      Left = 18
      Top = 51
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Documento'
    end
    object Label4: TLabel
      Tag = 1
      Left = 220
      Top = 51
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hist'#243'rico'
    end
    object edtNrCheque: TMaskEdit
      Left = 104
      Top = 19
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtNrChequeExit
    end
    object edtNmFavorecido: TEdit
      Left = 264
      Top = 19
      Width = 417
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 50
      TabOrder = 1
    end
    object edtDocto: TEdit
      Left = 104
      Top = 43
      Width = 89
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 15
      TabOrder = 2
    end
    object edtHistorico: TEdit
      Left = 264
      Top = 43
      Width = 417
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 50
      TabOrder = 3
    end
  end
  inherited ImageList1: TImageList
    Left = 304
    Top = 256
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Titulo'#39') IS NULL) '
      'BEGIN'
      ''
      #9'SELECT TOP 0 nCdTitulo'
      #9#9'  ,cNrTit'
      #9#9'  ,nCdSP'
      #9#9'  ,dDtVenc'
      #9#9'  ,cNmTerceiro'
      #9#9'  ,nSaldoTit'
      #9#9'  ,cNrNF'
      #9#9'  ,cNmEspTit'
      #9#9'  ,cNmEspTit cCdBarraTP'
      '          ,Terceiro.nCdBanco'
      '          ,Terceiro.cAgencia'
      '          ,Terceiro.cNumConta'
      '          ,Terceiro.cDigitoConta'
      '          ,Terceiro.cNmTitularConta1'
      #9'  INTO #Titulo'
      #9'  FROM Titulo'
      
        '           INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.' +
        'nCdTerceiro'
      
        '           INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEsp' +
        'Tit'
      ''
      ''
      'END'
      ''
      'TRUNCATE TABLE #Titulo'
      ''
      'INSERT INTO #Titulo'
      '    SELECT nCdTitulo'
      '          ,cNrTit'
      '          ,nCdSP'
      '          ,dDtVenc'
      '          ,cNmTerceiro'
      '          ,nSaldoTit'
      '          ,cNrNF'
      '          ,cNmEspTit'
      '          ,'#39#39
      '          ,Terceiro.nCdBanco'
      '          ,Terceiro.cAgencia'
      '          ,Terceiro.cNumConta'
      '          ,Terceiro.cDigitoConta'
      '          ,Terceiro.cNmTitularConta1'
      '      FROM Titulo'
      
        '           INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.' +
        'nCdTerceiro'
      
        '           INNER JOIN EspTit   ON EspTit.nCdEspTit     = Titulo.' +
        'nCdEspTit'
      '     WHERE nCdProvisaoTit = :nPK'
      ''
      'SELECT *'
      'FROM #Titulo')
    Left = 272
    Top = 256
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Provisionados|ID'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosnCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. SP'
      FieldName = 'nCdSP'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Provisionados|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTituloscNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos Provisionados|Saldo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloscNrNF: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Provisionados|Esp'#233'cie de T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTituloscCdBarraTP: TStringField
      DisplayLabel = 'C'#243'digo Barra|Boleto'
      FieldName = 'cCdBarraTP'
      Size = 50
    end
    object qryTitulosnCdBanco: TIntegerField
      DisplayLabel = 'Dados para Dep'#243'sito|Bco'
      FieldName = 'nCdBanco'
    end
    object qryTituloscAgencia: TStringField
      DisplayLabel = 'Dados para Dep'#243'sito|Ag.'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryTituloscNumConta: TStringField
      DisplayLabel = 'Dados para Dep'#243'sito|Conta'
      FieldName = 'cNumConta'
      Size = 11
    end
    object qryTituloscDigitoConta: TStringField
      DisplayLabel = 'Dados para Dep'#243'sito|DV'
      FieldName = 'cDigitoConta'
      FixedChar = True
      Size = 1
    end
    object qryTituloscNmTitularConta1: TStringField
      DisplayLabel = 'Dados para Dep'#243'sito|Titular'
      FieldName = 'cNmTitularConta1'
      Size = 50
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 272
    Top = 288
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoFormaPagto'
      'FROM FormaPagto'
      'WHERE nCdFormaPagto = :nPK')
    Left = 208
    Top = 256
    object qryFormaPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Titulo'#39') IS NULL) '#13#10'BEGIN'#13#10#13#10#9'SELECT TOP' +
      ' 0 nCdTitulo'#13#10#9#9'  ,cNrTit'#13#10#9#9'  ,nCdSP'#13#10#9#9'  ,dDtVenc'#13#10#9#9'  ,cNmTer' +
      'ceiro'#13#10#9#9'  ,nSaldoTit'#13#10#9#9'  ,cNrNF'#13#10#9#9'  ,cNmEspTit'#13#10#9#9'  ,cNmEspTi' +
      't cCdBarraTP'#13#10'          ,Terceiro.nCdBanco'#13#10'          ,Terceiro.' +
      'cAgencia'#13#10'          ,Terceiro.cNumConta'#13#10'          ,Terceiro.cDi' +
      'gitoConta'#13#10'          ,Terceiro.cNmTitularConta1'#13#10#9'  INTO #Titulo' +
      #13#10#9'  FROM Titulo'#13#10'           INNER JOIN Terceiro ON Terceiro.nCd' +
      'Terceiro = Titulo.nCdTerceiro'#13#10'           INNER JOIN EspTit ON E' +
      'spTit.nCdEspTit = Titulo.nCdEspTit'#13#10#13#10#13#10'END'#13#10#13#10'TRUNCATE TABLE #T' +
      'itulo'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 240
    Top = 256
  end
  object SP_BAIXA_PROVISAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_BAIXA_PROVISAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProvisaoTit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgTipo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = '@cNmFavorecido'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cNrDocto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@cNmHistorico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 240
    Top = 288
  end
  object SP_CRIA_LOTELIQ_PROVISAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CRIA_LOTELIQ_PROVISAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProvisaoTit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 288
  end
end
