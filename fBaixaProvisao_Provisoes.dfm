inherited frmBaixaProvisao_Provisoes: TfrmBaixaProvisao_Provisoes
  Left = 78
  Top = 168
  Width = 777
  BorderIcons = [biSystemMenu]
  Caption = 'Provis'#245'es'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 761
  end
  inherited ToolBar1: TToolBar
    Width = 761
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 761
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsProvisoes
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProvisaoTit'
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 263
      end
      item
        EditButtons = <>
        FieldName = 'dDtPrev'
        Footers = <>
        Width = 166
      end
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footers = <>
        Width = 133
      end
      item
        EditButtons = <>
        FieldName = 'nValProvisao'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 115
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryProvisoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtPagto'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdFormapagto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa       int'
      '       ,@dDtPagto         VARCHAR(10)'
      '       ,@nCdUsuario       int'
      '       ,@nCdContaBancaria int'
      '       ,@nCdFormaPagto    int'
      ''
      'Set @nCdEmpresa       = :nCdEmpresa'
      'Set @dDtPagto         = :dDtPagto'
      'Set @nCdUsuario       = :nCdUsuario'
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @nCdFormaPagto    = :nCdFormapagto'
      ''
      'SELECT nCdProvisaoTit'
      '      ,cNmUsuario'
      '      ,dDtPrev'
      '      ,cNmFormaPagto'
      '      ,nValProvisao'
      '      ,ProvisaoTit.nCdFormaPagto'
      '  FROM ProvisaoTit'
      
        '       INNER JOIN Usuario       ON Usuario.nCdUsuario       = Pr' +
        'ovisaoTit.nCdUsuarioPrev'
      
        '       INNER JOIN FormaPagto    ON FormaPagto.nCdFormaPagto = Pr' +
        'ovisaoTit.nCdFormaPagto'
      
        '       LEFT  JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = ProvisaoTit.nCdContaBancaria'
      ' WHERE ProvisaoTit.dDtCancel        IS NULL'
      '   AND ProvisaoTit.cFlgBaixado       = 0'
      
        '   AND ProvisaoTit.dDtPagto          = Convert(DATETIME,@dDtPagt' +
        'o,103)'
      '   AND ProvisaoTit.nCdEmpresa        = @nCdEmpresa'
      '   AND ProvisaoTit.nCdContaBancaria  = @nCdContaBancaria'
      '   AND ProvisaoTit.nCdFormaPagto     = @nCdFormaPagto'
      '   AND (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))')
    Left = 312
    Top = 184
    object qryProvisoesnCdProvisaoTit: TAutoIncField
      DisplayLabel = 'Provis'#245'es em aberto|C'#243'd'
      FieldName = 'nCdProvisaoTit'
      ReadOnly = True
    end
    object qryProvisoescNmUsuario: TStringField
      DisplayLabel = 'Provis'#245'es em aberto|Respons'#225'vel Provis'#227'o'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryProvisoesdDtPrev: TDateTimeField
      DisplayLabel = 'Provis'#245'es em aberto|Data Reg. Provis'#227'o'
      FieldName = 'dDtPrev'
    end
    object qryProvisoescNmFormaPagto: TStringField
      DisplayLabel = 'Provis'#245'es em aberto|Forma de Pagamento'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryProvisoesnValProvisao: TBCDField
      DisplayLabel = 'Provis'#245'es em aberto|Valor'
      FieldName = 'nValProvisao'
      Precision = 12
      Size = 2
    end
    object qryProvisoesnCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
  end
  object dsProvisoes: TDataSource
    DataSet = qryProvisoes
    Left = 368
    Top = 176
  end
end
