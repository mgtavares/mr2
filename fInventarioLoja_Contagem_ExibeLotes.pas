unit fInventarioLoja_Contagem_ExibeLotes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh, Menus;

type
  TfrmInventarioLoja_Contagem_ExibeLotes = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    DBGridEh1: TDBGridEh;
    ToolButton8: TToolButton;
    qryLoteInventario: TADOQuery;
    qryLoteInventarionCdLoteContagemInventario: TAutoIncField;
    qryLoteInventarionCdInventario: TIntegerField;
    qryLoteInventariocNrLote: TStringField;
    qryLoteInventarioiQtdeProdutos: TIntegerField;
    qryLoteInventarioiQtdeProdutosInv: TIntegerField;
    dsLoteInventario: TDataSource;
    qryAux: TADOQuery;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    qryConfereDataContagem: TADOQuery;
    qryConfereDataContagemdDtContagemEncerrada: TDateTimeField;
    PopupMenu1: TPopupMenu;
    Atualizar1: TMenuItem;
    btnLanctoLote: TToolButton;
    ToolButton9: TToolButton;
    btnFiltro: TToolButton;
    ToolButton11: TToolButton;
    qryLoteInventariocLocalLote: TStringField;
    ToolButton7: TToolButton;
    ToolButton10: TToolButton;
    procedure edicaoGrid(bAtivo:boolean) ;
    procedure FormShow(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure qryLoteInventarioBeforePost(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryLoteInventarioBeforeDelete(DataSet: TDataSet);
    procedure ToolButton6Click(Sender: TObject);
    procedure Atualizar1Click(Sender: TObject);
    procedure btnLanctoLoteClick(Sender: TObject);
    procedure btnFiltroClick(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cFlgContagemEncerrada : integer ;
  end;

var
  frmInventarioLoja_Contagem_ExibeLotes: TfrmInventarioLoja_Contagem_ExibeLotes;

implementation

uses fMenu, fPdv, fInventarioLoja_Contagem_ContagemItemLote, fCaixa_Recebimento,
     fInventarioLoja_Contagem_ExibeLotes_LocalLote;

{$R *.dfm}

{ TfrmInventarioLoja_Contagem_ExibeLotes }

procedure TfrmInventarioLoja_Contagem_ExibeLotes.edicaoGrid(
  bAtivo: boolean);
begin

    if not bAtivo then
    begin
        DBGridEh1.Columns[2].ReadOnly := True ;
        DBGridEh1.Columns[3].ReadOnly := True ;

        DBGridEh1.Columns[2].Title.Font.Color := clRed ;
        DBGridEh1.Columns[3].Title.Font.Color := clRed ;

        DBGridEh1.Columns[2].Font.Color := clTeal ;
        DBGridEh1.Columns[3].Font.Color := clTeal ;

        DBGridEh1.ReadOnly := True ;
    end
    else
    begin
        DBGridEh1.Columns[2].ReadOnly := False ;
        DBGridEh1.Columns[3].ReadOnly := False ;

        DBGridEh1.Columns[2].Title.Font.Color := clBlack;
        DBGridEh1.Columns[3].Title.Font.Color := clBlack ;

        DBGridEh1.Columns[2].Font.Color := clBlack ;
        DBGridEh1.Columns[3].Font.Color := clBlack ;

        DBGridEh1.ReadOnly := False ;
    end ;

    DBGridEh1.Repaint;
    DBGridEh1.SetFocus;

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.FormShow(Sender: TObject);
begin
  inherited;

  edicaoGrid(false) ;

  ToolButton8.Enabled   := (cFlgContagemEncerrada = 0) ;
  ToolButton6.Enabled   := (cFlgContagemEncerrada = 0) ;
  btnLanctoLote.Enabled := (cFlgContagemEncerrada = 0) ;

  if (frmMenu.LeParametro('PERMDIFCONTLOTE') = 'N') then
      ToolButton7.Caption := 'Perm. Diverg�ncia'
  else ToolButton7.Caption := 'Bloq. Diverg�ncia';

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.ToolButton8Click(
  Sender: TObject);
var
  objCaixa : TfrmCaixa_Recebimento;
begin
  inherited;

  qryConfereDataContagem.Close;
  PosicionaQuery(qryConfereDataContagem,qryLoteInventario.Parameters.ParamByName('nPK').Value) ;

  if (qryConfereDataContagemdDtContagemEncerrada.AsString <> '') then
  begin
      MensagemErro('A contagem deste invent�rio foi encerrada.') ;
      close;
      abort ;
  end ;

  objCaixa := TfrmCaixa_Recebimento.Create(nil);

  if (objCaixa.AutorizacaoGerente(60)) then
      edicaoGrid(true) ;

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.qryLoteInventarioBeforePost(
  DataSet: TDataSet);
begin

  if (trim(qryLoteInventariocNrLote.Value) = '') then
  begin
      MensagemAlerta('Informe o n�mero do lote.') ;
      abort ;
  end ;

  if (qryLoteInventarioiQtdeProdutos.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade de pe�as do lote.') ;
      abort ;
  end ;

  qryLoteInventarionCdInventario.Value := qryLoteInventario.Parameters.ParamByName('nPK').Value ;

  qryLoteInventariocNrLote.Value := Uppercase(qryLoteInventariocNrLote.Value) ;
  
  inherited;

  if (qryLoteInventario.State = dsInsert) then
      qryLoteInventarionCdLoteContagemInventario.Value := frmMenu.fnProximoCodigo('LOTECONTAGEMINVENTARIO'); 

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.DBGridEh1DblClick(
  Sender: TObject);
var
  objForm : TfrmInventarioLoja_Contagem_ContagemItemLote;
begin
  inherited;

  if (cFlgContagemEncerrada = 0) then
  begin

      qryConfereDataContagem.Close;
      PosicionaQuery(qryConfereDataContagem,qryLoteInventario.Parameters.ParamByName('nPK').Value) ;

      if (qryConfereDataContagemdDtContagemEncerrada.AsString <> '') then
      begin
          MensagemErro('A contagem deste invent�rio foi encerrada.') ;
          close;
          abort ;
      end ;

  end;

  if (qryLoteInventarionCdLoteContagemInventario.Value > 0) then
  begin
      objForm := TfrmInventarioLoja_Contagem_ContagemItemLote.Create(nil);

      if (cFlgContagemEncerrada = 0) then
          objForm.lblNumeroLote.Caption    := 'Invent�rio: ' + frmMenu.ZeroEsquerda(qryLoteInventario.Parameters.ParamByName('nPK').Value,10) + ' - N�mero Lote: ' + qryLoteInventariocNrLote.Value
      else objForm.lblNumeroLote.Caption    := 'Invent�rio: ' + frmMenu.ZeroEsquerda(qryLoteInventario.Parameters.ParamByName('nPK').Value,10) + ' - N�mero Lote: ' + qryLoteInventariocNrLote.Value + '   -   CONTAGEM ENCERRADA.';

      objForm.lblNumeroLote.Font.Size  := 12 ;
      objForm.lblNumeroLote.Font.Color := clBlue ;

      if (StrToInt(frmMenu.LeParametro('TMMXBRPRPDV')) > 0) then
          objForm.qryTempContagemItemLotecCdProduto.Size := StrToint(frmMenu.LeParametro('TMMXBRPRPDV'));

      objForm.nCdLoteContagemInv     := qryLoteInventarionCdLoteContagemInventario.Value;
      objForm.btValidarLista.Visible := True;

      if (frmMenu.LeParametro('USACOLETOR') = 'S') then
      begin
          objForm.btLerArquivo.Visible := True;
      end ;

      objForm.DBGridEh1.ReadOnly                    := False ;
      objForm.DBGridEh1.Columns[1].Title.Font.Color := clBlack;
      objForm.DBGridEh1.Columns[1].Font.Color       := clBlack ;

      {-- se a contagem estiver encerrada, desativa alguns recursos --}
      if (cFlgContagemEncerrada = 1) then
      begin

          objForm.ToolButton2.Visible    := True ;
          objForm.btValidarLista.Visible := False ;
          objForm.btLimparLista.Visible  := False ;

          if (frmMenu.LeParametro('USACOLETOR') = 'S') then
          begin
              objForm.btLerArquivo.Visible := False;
          end ;

          objForm.DBGridEh1.ReadOnly                    := True;
          objForm.DBGridEh1.Columns[1].Title.Font.Color := clRed;
          objForm.DBGridEh1.Columns[1].Font.Color       := clTeal;

      end ;

      showForm(objForm,true);

      qryLoteInventario.Requery();

  end ;

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.qryLoteInventarioBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM ItemLoteContagemInventario WHERE nCdLoteContagemInventario = ' + qryLoteInventarionCdLoteContagemInventario.asString) ;
  qryAux.ExecSQL;
  
end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.ToolButton6Click(
  Sender: TObject);
var
    cFlgDiferenca : integer ;
begin
  inherited;

  qryConfereDataContagem.Close;
  PosicionaQuery(qryConfereDataContagem,qryLoteInventario.Parameters.ParamByName('nPK').Value) ;

  if (qryConfereDataContagemdDtContagemEncerrada.AsString <> '') then
  begin
      MensagemErro('A contagem deste invent�rio foi encerrada.') ;
      close;
      abort ;
  end ;
  
  cFlgDiferenca := 0 ;

  qryLoteInventario.First;

  while not qryLoteInventario.Eof do
  begin

      if (qryLoteInventarioiQtdeProdutos.Value <> qryLoteInventarioiQtdeProdutosInv.Value) then
      begin
          MensagemErro('Diferen�a de contagem no lote: ' + qryLoteInventariocNrLote.Value) ;
          cFlgDiferenca := 1 ;
      end ;

      { -- verifica se itens do lote possuem refer�ncia ao produto -- }
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1                                                                            ');
      qryAux.SQL.Add('  FROM ItemLoteContagemInventario                                                         ');
      qryAux.SQL.Add(' WHERE nCdLoteContagemInventario = ' + qryLoteInventarionCdLoteContagemInventario.AsString );
      qryAux.SQL.Add('   AND nCdProduto                IS NULL                                                  ');
      qryAux.Open;

      if (not qryAux.IsEmpty) then
      begin
          MensagemErro('Erro ao validar os itens do lote ' + qryLoteInventariocNrLote.Value + ', efetue a valida��o do lote novamente.');
          Abort;
      end;

      qryLoteInventario.Next;
  end ;

  qryLoteInventario.First;

  if (cFlgDiferenca = 1) then
      exit ;

  if (MessageDlg('Confirma o encerramento da contagem dos lotes ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (MessageDlg('Tem certeza que deseja encerrar ? Ap�s este processo a contagem n�o poder� ser alterada.',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE Inventario SET dDtContagemEncerrada = GetDate() WHERE nCdInventario = ' + intToStr(qryLoteInventario.Parameters.ParamByName('nPK').Value)) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Contagem encerrada com Sucesso!') ;

  close ;

end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.Atualizar1Click(
  Sender: TObject);
begin
  inherited;

  qryLoteInventario.Close;
  qryLoteInventario.Open;
  
end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.btnLanctoLoteClick(
  Sender: TObject);
var
  objLocalLote : TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote;
  objCaixa     : TfrmCaixa_Recebimento;
begin
    objLocalLote := TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote.Create(nil);
    objCaixa     := TfrmCaixa_Recebimento.Create(nil);

    if (objCaixa.AutorizacaoGerente(60)) then
    begin
        objLocalLote.qryTempLote.Close;

        objLocalLote.cmdPreparaTemp.Execute;
        objLocalLote.qryTempLote.Open;

        objLocalLote.nCdInventario := qryLoteInventarionCdInventario.Value;

        showForm(objLocalLote,true);

        Atualizar1.Click;
    end;
end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.btnFiltroClick(
  Sender: TObject);
var
    iFlgInvalido : Integer;
begin
  inherited;

  if (btnFiltro.Tag = 0) then
  begin
      btnFiltro.Tag        := 1;
      btnFiltro.Caption    := 'Exibir Todos';
      btnFiltro.ImageIndex := 8;

      { -- filtra itens inv�lidos -- }
      qryLoteInventario.Close;
      qryLoteInventario.Parameters.ParamByName('cFlgExibeTodos').Value := 0;
      qryLoteInventario.Open;
  end

  else
  begin
      btnFiltro.Tag        := 0;
      btnFiltro.Caption    := 'Exibir Diferen�a';
      btnFiltro.ImageIndex := 7;

      { -- filtra todos os itens -- }
      qryLoteInventario.Close;
      qryLoteInventario.Parameters.ParamByName('cFlgExibeTodos').Value := 1;
      qryLoteInventario.Open;
  end;

  iFlgInvalido := 0; { -- 0: V�lido / 1: Inv�lido -- }

  qryLoteInventario.First;

  while (not qryLoteInventario.Eof) do
  begin
      if (qryLoteInventarioiQtdeProdutos.Value <> qryLoteInventarioiQtdeProdutosInv.Value) then
          iFlgInvalido := 1;

      qryLoteInventario.Next;
  end;
end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes.ToolButton7Click(
  Sender: TObject);
var
  objCaixa     : TfrmCaixa_Recebimento;
begin

    objCaixa := TfrmCaixa_Recebimento.Create(nil);

    if (objCaixa.AutorizacaoGerente(60)) then
    begin

        if (frmMenu.LeParametro('PERMDIFCONTLOTE') = 'S') then
        begin

            qryAux.Close;
            qryAux.SQL.Clear;
            qryAux.SQL.Append('UPDATE Parametro SET cValor = ''N'' WHERE cParametro LIKE ''PERMDIFCONTLOTE''');
            qryAux.ExecSQL;

            ToolButton7.Caption := 'Perm. Diverg�ncia';
        end else
        begin

            qryAux.Close;
            qryAux.SQL.Clear;
            qryAux.SQL.Append('UPDATE Parametro SET cValor = ''S'' WHERE cParametro LIKE ''PERMDIFCONTLOTE''');
            qryAux.ExecSQL;

            ToolButton7.Caption := 'Bloq. Diverg�ncia';
        end;

    end;

end;

end.
