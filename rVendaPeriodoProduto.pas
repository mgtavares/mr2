unit rVendaPeriodoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, comObj, ExcelXP;

type
  TrptVendaPeriodoProduto = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaPeriodoProduto: TrptVendaPeriodoProduto;

implementation

uses fMenu, fLookup_Padrao, rVendaPeriodoProduto_view;

{$R *.dfm}

procedure TrptVendaPeriodoProduto.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;


procedure TrptVendaPeriodoProduto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptVendaPeriodoProduto.ToolButton1Click(Sender: TObject);
var
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        : TrptVendaPeriodoProduto_view;

begin

  objRel := TrptVendaPeriodoProduto_view.Create(nil);

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdClasse').Value         := frmMenu.ConvInteiro(MaskEdit9.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Open  ;

  if (RadioGroup1.ItemIndex = 0) then
  begin
      Try
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa : '        + Trim(MaskEdit3.Text) + ' ' + DbEdit3.Text ;
          objRel.lblFiltro2.Caption := 'Terceiro: '        + String(MaskEdit6.Text) + ' ' + DbEdit10.Text ;
          objRel.lblFiltro3.Caption := 'Grupo Economico: ' + String(MaskEdit4.Text) + ' ' + DbEdit1.Text ;
          objRel.lblFiltro4.Caption := 'Per�odo        : ' + String(MaskEdit1.Text) + ' ' + String(MaskEdit2.Text) ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  end
  else
  begin

      try
          planilha:= CreateoleObject('Excel.Application');
      except
          MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
          exit ;
      end ;

      LCID := GetUserDefaultLCID;
      planilha.DisplayAlerts  := False;
      planilha.WorkBooks.add(1);
      planilha.ScreenUpdating := true;

      planilha.caption := 'Vendas Per�odo - Produto';

      for linha := 0 to objRel.usp_Relatorio.RecordCount - 1 do
      begin

          for coluna := 1 to objRel.usp_Relatorio.FieldCount do
          begin

              valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].AsString;

              planilha.cells[linha + 2,coluna] := valorCampo;

              if (objRel.usp_Relatorio.Fields[coluna - 1].DataType = ftBCD) then
              begin
                  planilha.Cells[Linha+2,coluna].NumberFormat        := '0,00' ;
                  planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlRight ;
              end ;

              if (objRel.usp_Relatorio.Fields[coluna - 1].DataType = ftDateTime) then
              begin
                  planilha.Cells[Linha+2,coluna].NumberFormat        := 'dd/mm/yyyy' ;
                  planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlLeft ;
              end ;

          end;

          objRel.usp_Relatorio.Next;

      end;

      for coluna := 1 to objRel.usp_Relatorio.FieldCount do
      begin

          valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].DisplayLabel;
          planilha.cells[1,coluna] := valorcampo;

      end;

      planilha.columns.Autofit;
      planilha.Visible := True;

  end;

  FreeAndNil(objRel);

end;


procedure TrptVendaPeriodoProduto.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;
end;

procedure TrptVendaPeriodoProduto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;


procedure TrptVendaPeriodoProduto.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TrptVendaPeriodoProduto.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

initialization
     RegisterClass(TrptVendaPeriodoProduto) ;

end.
