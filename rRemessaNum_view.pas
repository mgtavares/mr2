unit rRemessaNum_view;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, jpeg;

type
  TrptRemessaNum_view = class(TQuickRep)
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    lblEmpresa: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel3: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    qryCheques: TADOQuery;
    QRSubDetail1: TQRSubDetail;
    QRGroup1: TQRGroup;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRBand2: TQRBand;
    QRLabel8: TQRLabel;
    qryCartao: TADOQuery;
    QRSubDetail2: TQRSubDetail;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand3: TQRBand;
    QRLabel10: TQRLabel;
    lblTotCartao: TQRLabel;
    lblTotCheque: TQRLabel;
    QRGroup2: TQRGroup;
    QRLabel9: TQRLabel;
    qryCrediario: TADOQuery;
    QRSubDetail3: TQRSubDetail;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRGroup3: TQRGroup;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRBand4: TQRBand;
    lblTotCrediario: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel25: TQRLabel;
    QRBand6: TQRBand;
    qryRemessa: TADOQuery;
    qryRemessanCdRemessaNum: TIntegerField;
    qryRemessacNmEmpresa: TStringField;
    qryRemessacNmLoja: TStringField;
    qryRemessanCdContaOrigem: TStringField;
    qryRemessanCdContaDestino: TStringField;
    qryRemessanValDinheiro: TBCDField;
    qryRemessacNmUsuarioCad: TStringField;
    qryRemessadDtRemessa: TDateTimeField;
    qryRemessadDtFech: TDateTimeField;
    qryRemessadDtCancel: TDateTimeField;
    qryRemessacOBS: TStringField;
    qryRemessadDtConfirma: TDateTimeField;
    qryRemessacNmUsuarioConfirma: TStringField;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel27: TQRLabel;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel29: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    qryChequesnCdBanco: TIntegerField;
    qryChequescAgencia: TStringField;
    qryChequescConta: TStringField;
    qryChequescDigito: TStringField;
    qryChequescCNPJCPF: TStringField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequescNmTerceiro: TStringField;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    qryCartaodDtTransacao: TDateTimeField;
    qryCartaocNmOperadoraCartao: TStringField;
    qryCartaoiNrDocto: TIntegerField;
    qryCartaoiNrAutorizacao: TIntegerField;
    qryCartaonValTransacao: TBCDField;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText11: TQRDBText;
    qryCrediarionCdCrediario: TAutoIncField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediariocNmTerceiro: TStringField;
    qryCrediarionValCrediario: TBCDField;
    QRLabel16: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel17: TQRLabel;
    qryChequescStatus: TStringField;
    qryCartaocStatus: TStringField;
    qryCrediariocStatus: TStringField;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel20: TQRLabel;
    QRImage1: TQRImage;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRBand3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText9Print(sender: TObject; var Value: String);
  private

      nValTotalCheque, nValTotalCartao, nValCrediario : double ;

  public

  end;

var
  rptRemessaNum_view: TrptRemessaNum_view;

implementation

{$R *.DFM}

procedure TrptRemessaNum_view.QRDBText3Print(sender: TObject;
  var Value: String);
begin

    nValTotalCartao := nValTotalCartao + qryCartaonValTransacao.Value ;

    lblTotCartao.Caption := FormatCurr('#,##0.00',nValTotalCartao) ;

end;

procedure TrptRemessaNum_view.QRDBText2Print(sender: TObject;
  var Value: String);
begin
    nValTotalCheque := nValTotalCheque + qryChequesnValCheque.Value ;

    lblTotCheque.Caption := FormatCurr('#,##0.00',nValTotalCheque) ;

end;

procedure TrptRemessaNum_view.QRBand3AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
    nValTotalCartao := 0 ;
    nValTotalCheque := 0 ;
    nValCrediario:= 0 ;
end;

procedure TrptRemessaNum_view.QRDBText9Print(sender: TObject;
  var Value: String);
begin

    nValCrediario := nValCrediario + qryCrediarionValCrediario.Value ;

    lblTotCrediario.Caption := formatCurr('#,##0.00',nValCrediario) ;

end;

end.


