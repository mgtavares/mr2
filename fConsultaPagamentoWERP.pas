unit fConsultaPagamentoWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, Mask, DBCtrls;

type
  TfrmConsultaPagamentoWERP = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryMovimentacao: TADOQuery;
    dsDoctoFiscal: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryMovimentacaodDtMov: TDateTimeField;
    qryMovimentacaonCdTitulo: TIntegerField;
    qryMovimentacaocNrTit: TStringField;
    qryMovimentacaoiParcela: TIntegerField;
    qryMovimentacaocSigla: TStringField;
    qryMovimentacaocNmCategFinanc: TStringField;
    qryMovimentacaocNmOperacao: TStringField;
    qryMovimentacaonValMov: TBCDField;
    qryMovimentacaocSenso: TStringField;
    qryMovimentacaocNmEspTit: TStringField;
    qryMovimentacaocNrNF: TStringField;
    cxGrid1DBTableView1dDtMov: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1cSigla: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperacao: TcxGridDBColumn;
    cxGrid1DBTableView1nValMov: TcxGridDBColumn;
    cxGrid1DBTableView1cSenso: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    qryMovimentacaonCdOperacao: TIntegerField;
    qryMovimentacaocNrDoc: TStringField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPagamentoWERP: TfrmConsultaPagamentoWERP;
  nCdOperacaoLote : integer ;
implementation

uses fMenu, rRomaneio, fBaixaColetiva, rBaixaColetiva_view;

{$R *.dfm}

procedure TfrmConsultaPagamentoWERP.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(MaskEdit1.Text) = '/  /') then
  begin
      MaskEdit1.Text := DateToStr(Date-7) ;
      MaskEdit2.Text := DateToStr(Date) ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
      MaskEdit2.Text := DateToStr(Date) ;

  qryMovimentacao.Close ;
  qryMovimentacao.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.nCdTerceiroUsuario;
  qryMovimentacao.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryMovimentacao.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(Maskedit1.Text);
  qryMovimentacao.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text);
  qryMovimentacao.Open  ;

  if qryMovimentacao.eof then
      ShowMessage('Nenhum pagamento encontrado para o crit�rio utilizado.') ;

end;

procedure TfrmConsultaPagamentoWERP.FormShow(Sender: TObject);
begin
  inherited;

  nCdOperacaoLote := strToint(frmMenu.LeParametro('CDOPERLOTE')) ;
  MaskEdit3.Text  := intToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, MaskEdit3.Text) ;
  
end;

procedure TfrmConsultaPagamentoWERP.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if (qryMovimentacaonCdOperacao.Value = nCdOperacaoLote) then
  begin

      rptBaixaColetiva_view.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      rptBaixaColetiva_view.QRLabel47.Caption := frmMenu.cNmEmpresaAtiva;
      PosicionaQuery(rptBaixaColetiva_view.qryLoteLiq, qryMovimentacaocNrDoc.Value) ;
      PosicionaQuery(rptBaixaColetiva_view.qryChequeLoteLiq, qryMovimentacaocNrDoc.Value) ;
      rptBaixaColetiva_view.QRCompositeReport1.Preview;

  end ;

end;

initialization
    RegisterClass(TfrmConsultaPagamentoWERP) ;
    
end.
