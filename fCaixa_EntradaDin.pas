unit fCaixa_EntradaDin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls;

type
  TfrmCaixa_EntradaDin = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    procedure MaskEdit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_EntradaDin: TfrmCaixa_EntradaDin;

implementation

{$R *.dfm}

procedure TfrmCaixa_EntradaDin.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  if (trim(MaskEdit1.Text) = '') then
      MaskEdit1.text := '0' ;
      
  If (trim(MaskEdit1.Text) <> '') then
      try
          MaskEdit1.text := formatcurr('0.00',StrToFloat(StringReplace(MaskEdit1.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;
      except
          MensagemAlerta('Valor inv�lido') ;
          exit ;
      end ;

    close ;
       
end;

end.
