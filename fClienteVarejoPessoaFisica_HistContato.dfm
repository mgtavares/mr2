inherited frmClienteVarejoPessoaFisica_HistContato: TfrmClienteVarejoPessoaFisica_HistContato
  Left = 44
  Top = 127
  Width = 1072
  Height = 594
  Caption = 'Hist'#243'rico de Contatos'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1056
    Height = 529
  end
  inherited ToolBar1: TToolBar
    Width = 1056
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1056
    Height = 529
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 525
    ClientRectLeft = 4
    ClientRectRight = 1052
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Contatos'
      ImageIndex = 0
      object Label7: TLabel
        Tag = 1
        Left = 177
        Top = 16
        Width = 16
        Height = 13
        Caption = 'at'#233
      end
      object Label5: TLabel
        Tag = 1
        Left = 2
        Top = 16
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Per'#237'odo Contato'
      end
      object DBGridEh4: TDBGridEh
        Left = 2
        Top = 38
        Width = 1135
        Height = 420
        DataGrouping.GroupLevels = <>
        DataSource = dsContatos
        DrawMemoText = True
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh4DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdContatoCobranca'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtContato'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cHoraContato'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'dDtReagendado'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmPessoaContato'
            Footers = <>
            Width = 109
          end
          item
            EditButtons = <>
            FieldName = 'cTelefoneContato'
            Footers = <>
            Width = 109
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoResultadoContato'
            Footers = <>
            Width = 128
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 99
          end
          item
            EditButtons = <>
            FieldName = 'cTextoContato'
            Footers = <>
            Width = 222
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object edtDtIniContato: TMaskEdit
        Left = 93
        Top = 8
        Width = 73
        Height = 21
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 1
        Text = '  /  /    '
      end
      object edtDtFimContato: TMaskEdit
        Left = 205
        Top = 8
        Width = 73
        Height = 21
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 2
        Text = '  /  /    '
      end
      object cxButton5: TcxButton
        Left = 285
        Top = 3
        Width = 115
        Height = 30
        Caption = 'Exibir Consulta'
        TabOrder = 3
        OnClick = cxButton5Click
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000C6D6DEC6948C
          C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
          8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
          D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
          C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
          00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
          0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
          C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
          FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
          948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
          C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
          FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
          0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
          FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
          948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
          C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
          8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
          D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
          EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
          63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
          D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
          EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
          63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
          D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
        LookAndFeel.NativeStyle = True
      end
    end
  end
  object qryContatos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cDataInicial VARCHAR(10)'
      '       ,@cDataFinal   VARCHAR(10)'
      ''
      'Set @cDataInicial = :dDtInicial'
      'Set @cDataFinal   = :dDtFinal'
      ''
      'SELECT nCdContatoCobranca'
      
        '      ,Convert(DATETIME,Convert(VARCHAR,dDtContato,103),103) as ' +
        'dDtContato'
      '      ,Convert(VARCHAR,dDtContato,108) as cHoraContato'
      '      ,cNmPessoaContato'
      '      ,cTelefoneContato'
      '      ,cNmTipoResultadoContato'
      '      ,dbo.fn_ZeroEsquerda(nCdLoja,3) as nCdLoja'
      '      ,cNmUsuario'
      '      ,cTextoContato'
      '      ,dDtReagendado'
      '      ,cFlgCarta'
      '      ,nCdItemListaCobranca'
      '  FROM ContatoCobranca'
      
        '       LEFT JOIN TipoResultadoContato ON TipoResultadoContato.nC' +
        'dTipoResultadoContato = ContatoCobranca.nCdTipoResultadoContato'
      
        '       LEFT JOIN Usuario              ON Usuario.nCdUsuario     ' +
        '                      = ContatoCobranca.nCdUsuario'
      ' WHERE dDtContato >= Convert(DATETIME,@cDataInicial,103)'
      '   AND dDtContato  < Convert(DATETIME,@cDataFinal,103)+1'
      '   AND nCdTerceiro = :nPK'
      ' ORDER BY dDtContato DESC'
      '')
    Left = 60
    Top = 320
    object qryContatosnCdContatoCobranca: TAutoIncField
      DisplayLabel = 'Hist'#243'rico de Contatos|'
      FieldName = 'nCdContatoCobranca'
      ReadOnly = True
    end
    object qryContatosdDtContato: TDateTimeField
      DisplayLabel = 'Hist'#243'rico de Contatos|Data'
      FieldName = 'dDtContato'
    end
    object qryContatoscHoraContato: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Hora'
      FieldName = 'cHoraContato'
      ReadOnly = True
      Size = 30
    end
    object qryContatoscNmPessoaContato: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Pessoa Contato'
      FieldName = 'cNmPessoaContato'
      Size = 50
    end
    object qryContatoscTelefoneContato: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Telefone Contato'
      FieldName = 'cTelefoneContato'
      EditMask = '!\(99\) 9999-9999;0;_'
      Size = 50
    end
    object qryContatoscNmTipoResultadoContato: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Tipo Resultado'
      FieldName = 'cNmTipoResultadoContato'
      Size = 50
    end
    object qryContatosnCdLoja: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Loja'
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryContatoscNmUsuario: TStringField
      DisplayLabel = 'Hist'#243'rico de Contatos|Analista Cobran'#231'a'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryContatoscTextoContato: TMemoField
      DisplayLabel = 'Hist'#243'rico de Contatos|Anota'#231#227'o'
      FieldName = 'cTextoContato'
      BlobType = ftMemo
    end
    object qryContatosdDtReagendado: TDateTimeField
      DisplayLabel = 'Hist'#243'rico de Contatos|Reagendado para'
      FieldName = 'dDtReagendado'
    end
    object qryContatoscFlgCarta: TIntegerField
      FieldName = 'cFlgCarta'
    end
    object qryContatosnCdItemListaCobranca: TIntegerField
      FieldName = 'nCdItemListaCobranca'
    end
  end
  object dsContatos: TDataSource
    DataSet = qryContatos
    Left = 104
    Top = 320
  end
end
