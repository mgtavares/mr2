unit fConsultaMovtoCaixa_Parcelas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls;

type
  TfrmConsultaMovtoCaixa_Parcelas = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryParcelas: TADOQuery;
    qryParcelascNrTit: TStringField;
    qryParcelasiParcela: TIntegerField;
    qryParcelasnCdTerceiro: TIntegerField;
    qryParcelascNmTerceiro: TStringField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelasnValPago: TBCDField;
    dsParcelas: TDataSource;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValPago: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaMovtoCaixa_Parcelas: TfrmConsultaMovtoCaixa_Parcelas;

implementation

{$R *.dfm}

end.
