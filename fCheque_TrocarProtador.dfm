inherited frmCheque_TrocarProtador: TfrmCheque_TrocarProtador
  Left = 150
  Top = 220
  Width = 938
  Height = 166
  BorderIcons = [biSystemMenu]
  Caption = 'Troca de Portador'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 922
    Height = 101
  end
  inherited ToolBar1: TToolBar
    Width = 922
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [2]
    Left = 8
    Top = 40
    Width = 185
    Height = 81
    Caption = 'Tipo de Portador'
    ItemIndex = 0
    Items.Strings = (
      'Conta Banc'#225'ria/Caixa/Cofre'
      'Terceiro')
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object GroupBox1: TGroupBox [3]
    Left = 200
    Top = 40
    Width = 713
    Height = 81
    Caption = 'Novo Portador'
    TabOrder = 2
    object Label5: TLabel
      Left = 66
      Top = 28
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta'
    end
    object Label1: TLabel
      Left = 11
      Top = 52
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Portador'
    end
    object MaskEdit6: TMaskEdit
      Left = 104
      Top = 44
      Width = 62
      Height = 21
      Enabled = False
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit6Exit
      OnKeyDown = MaskEdit6KeyDown
    end
    object MaskEdit1: TMaskEdit
      Left = 104
      Top = 20
      Width = 62
      Height = 21
      Enabled = False
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = MaskEdit1Exit
      OnKeyDown = MaskEdit1KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 168
      Top = 44
      Width = 537
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 168
      Top = 20
      Width = 41
      Height = 21
      DataField = 'nCdBanco'
      DataSource = DataSource2
      TabOrder = 3
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 216
      Top = 20
      Width = 41
      Height = 21
      DataField = 'cAgencia'
      DataSource = DataSource2
      TabOrder = 4
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 264
      Top = 20
      Width = 193
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource2
      TabOrder = 5
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 464
      Top = 20
      Width = 241
      Height = 21
      DataField = 'cNmTitular'
      DataSource = DataSource2
      TabOrder = 6
    end
  end
  inherited ImageList1: TImageList
    Left = 160
    Top = 40
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 112
    Top = 88
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 192
    Top = 80
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja    int'
      '       ,@nCdEmpresa int'
      ''
      'Set @nCdLoja    = :nCdLoja'
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'Set @nCdLoja = IsNull(@nCdLoja,0)'
      ''
      'SELECT nCdContaBancaria'
      '      ,nCdBanco'
      '      ,nCdConta'
      '      ,cAgencia'
      '      ,nCdConta'
      '      ,cNmTitular'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND nCdEmpresa       = @nCdEmpresa'
      '   AND ((@nCdLoja = 0) OR (nCdLoja = @nCdLoja))')
    Left = 112
    Top = 40
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta_1: TStringField
      FieldName = 'nCdConta_1'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancaria
    Left = 152
    Top = 96
  end
  object cmdAtualizaCheque: TADOCommand
    CommandText = 
      'DECLARE @nCdCheque int'#13#10'       ,@nCdTerceiroPort int'#13#10'       ,@n' +
      'CdContaBancariaDep int'#13#10#13#10'Set @nCdCheque           = :nCdCheque'#13 +
      #10'Set @nCdTerceiroPort     = :nCdTerceiroPort'#13#10'Set @nCdContaBanca' +
      'riaDep = :nCdContaBancariaDep'#13#10#13#10#13#10'UPDATE Cheque'#13#10'   SET nCdTerc' +
      'eiroPort = NULL'#13#10'      ,nCdContaBancariaDep = NULL'#13#10' WHERE nCdCh' +
      'eque = @nCdCheque'#13#10#13#10'UPDATE Cheque'#13#10'   SET nCdTerceiroPort = CAS' +
      'E WHEN IsNull(@nCdTerceiroPort,0) > 0 THEN @nCdTerceiroPort'#13#10'   ' +
      '                           ELSE nCdTerceiroPort'#13#10'               ' +
      '          END'#13#10'      ,nCdContaBancariaDep = CASE WHEN IsNull(@nC' +
      'dContaBancariaDep,0) > 0 THEN @nCdContaBancariaDep'#13#10'            ' +
      '                      ELSE nCdContaBancariaDep'#13#10'                ' +
      '             END'#13#10' WHERE nCdCheque = @nCdCheque'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCheque'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiroPort'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdContaBancariaDep'
        Size = -1
        Value = Null
      end>
    Left = 232
    Top = 40
  end
end
