inherited frmDepartamento_Marca: TfrmDepartamento_Marca
  Left = 147
  Top = 142
  Width = 500
  Height = 448
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Marca'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 484
    Height = 383
  end
  inherited ToolBar1: TToolBar
    Width = 484
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 484
    Height = 383
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsMarcaSub
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdMarcaSubCategoria'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdMarca'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmMarca'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 240
  end
  object dsMarcaSub: TDataSource
    DataSet = qryMarcaSub
    Left = 200
    Top = 184
  end
  object qryMarcaSub: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMarcaSubBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarcaSubCategoria'
      '      ,MarcaSubCategoria.nCdMarca'
      '      ,MarcaSubCategoria.nCdSubCategoria'
      '  FROM MarcaSubCategoria'
      ' WHERE nCdSubCategoria = :nPK')
    Left = 168
    Top = 184
    object qryMarcaSubnCdMarcaSubCategoria: TAutoIncField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdMarcaSubCategoria'
    end
    object qryMarcaSubnCdMarca: TIntegerField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdMarca'
    end
    object qryMarcaSubnCdSubCategoria: TIntegerField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdSubCategoria'
    end
    object qryMarcaSubcNmMarca: TStringField
      DisplayLabel = 'Marca|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmMarca'
      LookupDataSet = qryMarca
      LookupKeyFields = 'nCdMarca'
      LookupResultField = 'cNmMarca'
      KeyFields = 'nCdMarca'
      LookupCache = True
      ReadOnly = True
      Size = 50
      Lookup = True
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdMarca'
      ',cNmMarca'
      'FROM Marca')
    Left = 272
    Top = 176
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMarca'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      '  FROM MarcaSubCategoria'
      ' WHERE nCdMarca        = :nCdMarca'
      '   AND nCdSubCategoria = :nCdSubCategoria')
    Left = 200
    Top = 112
  end
end
