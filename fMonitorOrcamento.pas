unit fMonitorOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  GridsEh, DBGridEh, DB, ADODB;

type
  TfrmMonitorOrcamento = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    DBGridEh1: TDBGridEh;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonValPedido: TBCDField;
    qryPedidodDtAutor: TDateTimeField;
    qryPedidocNmUsuario: TStringField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidodDtRejeicao: TDateTimeField;
    qryPedidocMotivoRejeicao: TMemoField;
    dsPedido: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryPedidonCdUsuarioRejeicao: TIntegerField;
    qryPedidonCdTabStatusPed: TIntegerField;
    procedure RadioGroup1Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorOrcamento: TfrmMonitorOrcamento;

implementation

uses fMonitorOrcamento_Itens, fMonitorOrcamento_GerarPedido, fMenu;

{$R *.dfm}

procedure TfrmMonitorOrcamento.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  qryPedido.Close ;
  qryPedido.Parameters.ParamByName('iTipoFiltro').Value := RadioGroup1.ItemIndex;
  qryPedido.Open ;

end;

procedure TfrmMonitorOrcamento.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
var 
  R     : TRect;
begin
  inherited;

  R := Rect;
  Dec(R.Bottom, 2);

  if Column.Field = qryPedido.FieldByName('cMotivoRejeicao') then
  begin
    DBGridEh1.Canvas.FillRect(Rect);
    DBGridEh1.Canvas.TextRect(R,R.Left,R.Top,qryPedido.FieldByName('cMotivoRejeicao').Value) ;
  end ;

end;

procedure TfrmMonitorOrcamento.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if (qryPedido.Active) and (qryPedidonCdPedido.Value > 0) then
  begin
      frmMonitorOrcamento_Itens.qryItemPedido.Close ;
      PosicionaQuery(frmMonitorOrcamento_Itens.qryItemPedido, qryPedidonCdPedido.AsString) ;
      frmMonitorOrcamento_Itens.ShowModal;
  end ;

end;

procedure TfrmMonitorOrcamento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryPedido.Active and (RadioGroup1.ItemIndex = 0)) then
  begin
      if (qryPedidonCdTabStatusPed.Value <> 2) then
      begin
          MensagemAlerta('O Or�amento n�o foi finalizado, finalize antes de aprovar.') ;
          exit ;
      end ;

      frmMonitorOrcamento_GerarPedido.cmdTemp.Execute ;
      frmMonitorOrcamento_GerarPedido.qryPreparaTemp.Parameters.ParamByName('nPK').Value := qryPedidonCdPedido.Value ;
      frmMonitorOrcamento_GerarPedido.qryPreparaTemp.ExecSQL;

      frmMonitorOrcamento_GerarPedido.qryTemp.Close ;
      frmMonitorOrcamento_GerarPedido.qryTemp.Open ;

      frmMonitorOrcamento_GerarPedido.qryTemp.First;

      frmMonitorOrcamento_GerarPedido.qryTemp.Edit ;

      frmMonitorOrcamento_GerarPedido.nCdPedido := qryPedidonCdPedido.Value ;

      frmMonitorOrcamento_GerarPedido.ShowModal ;

      qryPedido.Close ;
      qryPedido.Open ;
  end ;

end;

procedure TfrmMonitorOrcamento.ToolButton7Click(Sender: TObject);
var
    cMotivo : string ;
begin
  inherited;

  if not qryPedido.Active then
  begin
      MensagemAlerta('Nenhum or�amento selecionado.') ;
      exit ;
  end ;

  if (RadioGroup1.ItemIndex <> 0) then
  begin
      MensagemAlerta('Somente um or�amento pendente pode ser cancelado.') ;
      exit ;
  end ;

  if (qryPedido.Active and (RadioGroup1.ItemIndex = 0)) then
  begin

      case MessageDlg('Confirma o cancelamento deste or�amento ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      InputQuery('ER2Soft','Motivo Resumido do Cancelamento do Or�amento',cMotivo) ;

      frmMenu.Connection.BeginTrans ;

      try
          qryPedido.Edit ;
          qryPedidonCdUsuarioRejeicao.Value := frmMenu.nCdUsuarioLogado ;
          qryPedidodDtRejeicao.Value        := Now() ;
          qryPedidocMotivoRejeicao.Value    := cMotivo ;
          qryPedidonCdTabStatusPed.Value    := 10 ;
          qryPedido.Post ;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans ;

      ShowMessage('Or�amento Cancelado.') ;

      qryPedido.Close ;
      qryPedido.Parameters.ParamByName('iTipoFiltro').Value := RadioGroup1.ItemIndex;
      qryPedido.Open ;

  end ;

end;

initialization
    RegisterClass(TfrmMonitorOrcamento) ;
    
end.
