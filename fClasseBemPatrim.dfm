inherited frmClasseBemPatrim: TfrmClasseBemPatrim
  Left = 193
  Top = 162
  Caption = 'Classe Bem Patarimonial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 52
    Top = 39
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 6
    Top = 64
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Classe'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 95
    Top = 35
    Width = 65
    Height = 19
    DataField = 'nCdClasseBemPatrim'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 95
    Top = 59
    Width = 650
    Height = 19
    DataField = 'cNmClasseBemPatrim'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from ClasseBemPatrim'
      'WHERE nCdClasseBemPatrim = :nPK'
      '')
    object qryMasternCdClasseBemPatrim: TIntegerField
      FieldName = 'nCdClasseBemPatrim'
    end
    object qryMastercNmClasseBemPatrim: TStringField
      FieldName = 'cNmClasseBemPatrim'
      Size = 50
    end
  end
end
