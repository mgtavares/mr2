inherited rptTituloReabilitado: TrptTituloReabilitado
  Left = 369
  Top = 331
  Width = 900
  Height = 319
  Caption = 'Rel. Titulo Reabilitado'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 884
    Height = 252
  end
  object Label1: TLabel [1]
    Left = 71
    Top = 50
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 92
    Top = 75
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label3: TLabel [3]
    Left = 39
    Top = 100
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Cobradora'
  end
  object Label4: TLabel [4]
    Left = 80
    Top = 122
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label15: TLabel [5]
    Left = 16
    Top = 146
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Reabilita'#231#227'o'
  end
  object Label5: TLabel [6]
    Left = 204
    Top = 146
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 884
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object ER2LkpEmp: TER2LookupMaskEdit [8]
    Left = 120
    Top = 42
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object ER2LkpLoja: TER2LookupMaskEdit [9]
    Left = 120
    Top = 66
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object ER2LkpLojaNeg: TER2LookupMaskEdit [10]
    Left = 120
    Top = 90
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 59
    WhereAdicional.Strings = (
      'cFlgLojaCobradora = 1')
    QueryLookup = qryLojaCobradora
  end
  object ER2LpkTerceiro: TER2LookupMaskEdit [11]
    Left = 120
    Top = 114
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 180
    QueryLookup = qryTerceiro
  end
  object DBEdit1: TDBEdit [12]
    Tag = 1
    Left = 260
    Top = 42
    Width = 583
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 188
    Top = 66
    Width = 655
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 6
  end
  object MaskEdit1: TMaskEdit [14]
    Left = 120
    Top = 138
    Width = 72
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [15]
    Left = 231
    Top = 138
    Width = 73
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object DBEdit3: TDBEdit [16]
    Tag = 1
    Left = 188
    Top = 90
    Width = 655
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLojaCobradora
    TabOrder = 9
  end
  object DBEdit4: TDBEdit [17]
    Tag = 1
    Left = 188
    Top = 114
    Width = 655
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 10
  end
  object DBEdit5: TDBEdit [18]
    Tag = 1
    Left = 188
    Top = 42
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 11
  end
  inherited ImageList1: TImageList
    Left = 464
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 336
    Top = 152
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 336
    Top = 184
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM Loja'
      
        '       INNER JOIN Empresa     ON Empresa.nCdEmpresa  = Loja.nCdE' +
        'mpresa'
      
        '       INNER JOIN UsuarioLoja ON UsuarioLoja.nCdLoja = Loja.nCdL' +
        'oja'
      '   AND Loja.nCdLoja           = :nPK')
    Left = 368
    Top = 152
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 368
    Top = 184
  end
  object qryLojaCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Loja'
      
        '       INNER JOIN Empresa     ON Empresa.nCdEmpresa  = Loja.nCdE' +
        'mpresa'
      
        '       INNER JOIN UsuarioLoja ON UsuarioLoja.nCdLoja = Loja.nCdL' +
        'oja'
      '   AND Loja.cFlgLojaCobradora = 1'
      '   AND Loja.nCdLoja           = :nPK'
      '')
    Left = 400
    Top = 152
    object qryLojaCobradoracNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLojaCobradora: TDataSource
    DataSet = qryLojaCobradora
    Left = 400
    Top = 184
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM vwClienteVarejo'
      ' WHERE nCdTerceiro = :nPK')
    Left = 432
    Top = 152
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 432
    Top = 184
  end
end
