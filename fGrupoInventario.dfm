inherited frmGrupoInventario: TfrmGrupoInventario
  Left = 17
  Top = 129
  Caption = 'Grupo de Invent'#225'rio'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 19
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoInventario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoInventario'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoInventario'
      'WHERE nCdGrupoInventario = :nPK')
    object qryMasternCdGrupoInventario: TIntegerField
      FieldName = 'nCdGrupoInventario'
    end
    object qryMastercNmGrupoInventario: TStringField
      FieldName = 'cNmGrupoInventario'
      Size = 50
    end
  end
end
