unit fClienteVarejoPessoaFisica_Simples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls,
  ACBrBase, ACBrValidador;

type
  TfrmClienteVarejoPessoaFisica_Simples = class(TfrmCadastro_Padrao)
    qryEndereco: TADOQuery;
    dsEndereco: TDataSource;
    qryInformacao: TADOQuery;
    qryInformacaonCdInformacaoTerceiro: TAutoIncField;
    qryInformacaonCdTerceiro: TIntegerField;
    qryInformacaonCdLoja: TIntegerField;
    qryInformacaonCdUsuario: TIntegerField;
    qryInformacaodDtCad: TDateTimeField;
    qryInformacaocInformacao: TStringField;
    dsInformacao: TDataSource;
    dsUsuarioCad: TDataSource;
    qryTabTipoSituacao: TADOQuery;
    qryTabTipoSituacaocNmTabTipoSituacao: TStringField;
    dsTabTipoSituacao: TDataSource;
    qryUsuarioAprova: TADOQuery;
    qryUsuarioAprovacNmUsuario: TStringField;
    dsUsuarioAprova: TDataSource;
    qryUsuarioUltAlt: TADOQuery;
    qryUsuarioUltAltcNmUsuario: TStringField;
    dsUsuarioUltAlt: TDataSource;
    dsTabTipoOrigem: TDataSource;
    qryTabTipoOrigem: TADOQuery;
    qryTabTipoOrigemcNmTabTipoOrigem: TStringField;
    dsStatus: TDataSource;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label29: TLabel;
    Label30: TLabel;
    dsSexo: TDataSource;
    qrySexo: TADOQuery;
    qrySexonCdTabTipoSexo: TIntegerField;
    qrySexocNmTabTipoSexo: TStringField;
    qryUsuarioCad: TADOQuery;
    qryUsuarioCadcNmUsuario: TStringField;
    qryBuscaCEP: TADOQuery;
    qryBuscaCEPcUF: TStringField;
    qryBuscaCEPcEndereco: TStringField;
    qryBuscaCEPcBairro: TStringField;
    qryBuscaCEPcCidade: TStringField;
    qryUsuarioInfo: TADOQuery;
    qryInformacaocNmUsuario: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasternCdStatus: TIntegerField;
    qryMasteriQtdeChequeDev: TIntegerField;
    qryMasterdDtUltChequeDev: TDateTimeField;
    qryMasternValMaiorChequeDev: TBCDField;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasterdMaiorRecAtraso: TDateTimeField;
    qryMastercFlgBloqPedVenda: TIntegerField;
    qryMastercRG: TStringField;
    qryMastercTelefone1: TStringField;
    qryMastercTelefone2: TStringField;
    qryMastercTelefoneMovel: TStringField;
    qryMastercFax: TStringField;
    qryMastercEmail: TStringField;
    qryMasternCdLojaTerceiro: TIntegerField;
    qryMasterdDtUltNegocio: TDateTimeField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternCdPessoaFisica: TIntegerField;
    qryMasternCdTerceiroPessoaFisica: TIntegerField;
    qryMasterdDtNasc: TDateTimeField;
    qryMastercUFRG: TStringField;
    qryMasternCdTabTipoComprovRG: TIntegerField;
    qryMastercOBSRG: TStringField;
    qryMasterdDtEmissaoRG: TDateTimeField;
    qryMasternCdTabTipoSexo: TIntegerField;
    qryMasternCdTabTipoEstadoCivil: TIntegerField;
    qryMasteriNrDependentes: TIntegerField;
    qryMastercNaturalidade: TStringField;
    qryMasternCdTabTipoGrauEscola: TIntegerField;
    qryMasteriDiaVenctoPref: TIntegerField;
    qryMastercNmEmpTrab: TStringField;
    qryMastercTelefoneEmpTrab: TStringField;
    qryMastercRamalEmpTrab: TStringField;
    qryMastercEnderecoEmpTrab: TStringField;
    qryMasteriNrEnderecoEmpTrab: TIntegerField;
    qryMastercBairroEmpTrab: TStringField;
    qryMastercCidadeEmpTrab: TStringField;
    qryMastercUFEmpTrab: TStringField;
    qryMastercCEPEmpTrab: TStringField;
    qryMasternCdTabTipoClasseProf: TIntegerField;
    qryMasternCdTabTipoProfissao: TIntegerField;
    qryMastercCargoTrab: TStringField;
    qryMasterdDtAdmissao: TDateTimeField;
    qryMasternCdTabTipoComprovAdm: TIntegerField;
    qryMastercOBSAdmissao: TStringField;
    qryMasternValRendaBruta: TBCDField;
    qryMasternCdTabTipoComprovRenda: TIntegerField;
    qryMastercOBSRenda: TStringField;
    qryMastercNmCjg: TStringField;
    qryMasterdDtNascCjg: TDateTimeField;
    qryMastercRGCjg: TStringField;
    qryMastercUFRGCjg: TStringField;
    qryMasternCdTabTipoComprovRGCjg: TIntegerField;
    qryMastercOBSRGCjg: TStringField;
    qryMasterdDtEmissaoRGCjg: TDateTimeField;
    qryMastercCPFCjg: TStringField;
    qryMastercTelefoneCelCjg: TStringField;
    qryMastercNmEmpresaTrabCjg: TStringField;
    qryMastercTelefoneEmpTrabCjg: TStringField;
    qryMastercRamalEmpTrabCjg: TStringField;
    qryMasternCdTabTipoClasseProfCjg: TIntegerField;
    qryMasternCdTabTipoProfissaoCjg: TIntegerField;
    qryMastercCargoTrabCjg: TStringField;
    qryMasterdDtAdmissaoCjg: TDateTimeField;
    qryMasternCdTabTipoComprovCjg: TIntegerField;
    qryMastercOBSAdmissaoCjg: TStringField;
    qryMasternValRendaBrutaCjg: TBCDField;
    qryMasternCdTabTipoComprovRendaCjg: TIntegerField;
    qryMastercOBSRendaCjg: TStringField;
    qryMastercNmRendaAdd1: TStringField;
    qryMasternValRendaAdd1: TBCDField;
    qryMasternCdTabTipoRendaAdd1: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd1: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd1: TIntegerField;
    qryMastercOBSRendaAdd1: TStringField;
    qryMastercNmRendaAdd2: TStringField;
    qryMasternValRendaAdd2: TBCDField;
    qryMasternCdTabTipoRendaAdd2: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd2: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd2: TIntegerField;
    qryMastercOBSRendaAdd2: TStringField;
    qryMastercNmRendaAdd3: TStringField;
    qryMasternValRendaAdd3: TBCDField;
    qryMasternCdTabTipoRendaAdd3: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd3: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd3: TIntegerField;
    qryMastercOBSRendaAdd3: TStringField;
    qryMastercNmRefPessoal1: TStringField;
    qryMastercTelefoneRefPessoal1: TStringField;
    qryMastercNmRefPessoal2: TStringField;
    qryMastercTelefoneRefPessoal2: TStringField;
    qryMastercNmRefPessoal3: TStringField;
    qryMastercTelefoneRefPessoal3: TStringField;
    qryMastercNmRefComercial1: TStringField;
    qryMastercRefComercial1: TStringField;
    qryMastercNmRefComercial2: TStringField;
    qryMastercRefComercial2: TStringField;
    qryMastercNmRefComercial3: TStringField;
    qryMastercRefComercial3: TStringField;
    qryMasternCdUsuarioCadastro: TIntegerField;
    qryMasternCdLojaCadastro: TIntegerField;
    qryMasternCdUsuarioUltAlt: TIntegerField;
    qryMasternCdLojaUltAlt: TIntegerField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryMasternCdTabTipoOrigem: TIntegerField;
    qryMasternCdTabTipoSituacao: TIntegerField;
    qryMastercNmMae: TStringField;
    qryMastercNmPai: TStringField;
    qryMasternCdUsuarioAprova: TIntegerField;
    qryMasterdDtUsuarioAprova: TDateTimeField;
    qryMastercCaixaPostal: TStringField;
    qryMastercTelefoneRec1: TStringField;
    qryMastercTelefoneRec2: TStringField;
    qryMastercNmContatoRec1: TStringField;
    qryMastercNmContatoRec2: TStringField;
    qryMastercCNPJEmpTrab: TStringField;
    qryMastercComplementocEndEmpTrab: TStringField;
    qryMastercCNPJEmpTrabCjg: TStringField;
    qryMastercFlgRendaCjgLimite: TIntegerField;
    qryMastercRefComercialGeral: TStringField;
    qryMasternCdBancoRef: TIntegerField;
    qryMastercAgenciaRef: TStringField;
    qryMastercContaBancariaRef: TStringField;
    qryMasternCdTipoContaBancariaRef: TIntegerField;
    qryMastercFlgCartaoCredito: TIntegerField;
    qryMastercFlgTalaoCheque: TIntegerField;
    qryMastercNrDocOutro: TStringField;
    qryMasternCdTabTipoComprovDocOutro: TIntegerField;
    qryMastercCepCxPostal: TStringField;
    qryMasterdDtProxSPC: TDateTimeField;
    qryMasterdDtNegativacao: TDateTimeField;
    qryMastercOBSSituacaoCadastro: TStringField;
    qryMasternPercEntrada: TBCDField;
    qryMastercFlgBloqAtuLimAutom: TIntegerField;
    qryBuscaClienteCPF: TADOQuery;
    qryBuscaClienteCPFnCdTerceiro: TIntegerField;
    ValidaDoc: TACBrValidador;
    ToolButton3: TToolButton;
    btConvCadastro: TToolButton;
    qryConverteCadastro: TADOQuery;
    qryMastercFlgCadSimples: TIntegerField;
    ComboEstadoCivil: TDBLookupComboBox;
    Label33: TLabel;
    qryTabTipoEstadoCivil: TADOQuery;
    qryTabTipoEstadoCivilnCdTabTipoEstadoCivil: TIntegerField;
    qryTabTipoEstadoCivilcNmTabTipoEstadoCivil: TStringField;
    dsTabTipoEstadoCivil: TDataSource;
    ComboSexo: TDBLookupComboBox;
    qryEndereconCdEndereco: TIntegerField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecoiNumero: TIntegerField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEndereconCdTipoEnd: TIntegerField;
    qryEndereconCdRegiao: TIntegerField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEndereconCdPais: TIntegerField;
    qryEndereconCdStatus: TIntegerField;
    qryEnderecocCep: TStringField;
    qryEnderecocComplemento: TStringField;
    qryEndereconCdTabTipoResidencia: TIntegerField;
    qryEnderecoiTempoResidAno: TIntegerField;
    qryEnderecoiTempoResidMes: TIntegerField;
    qryEndereconValAluguel: TBCDField;
    qryEndereconCdTabTipoComprovEnd: TIntegerField;
    qryEnderecocOBSComprov: TStringField;
    qryEndereconCdUsuarioCadastro: TIntegerField;
    qryEnderecodDtCadastro: TDateTimeField;
    qryEndereconCdLojaCadastro: TIntegerField;
    qryTabTipoLogradouro: TADOQuery;
    qryTabTipoLogradouronCdTabTipoLogradouro: TIntegerField;
    qryTabTipoLogradourocNmTabTipoLogradouro: TStringField;
    dsTabTipoLogradouro: TDataSource;
    qryEndereconCdTabTipoLogradouro: TIntegerField;
    pagCliente: TcxPageControl;
    tabEndereco: TcxTabSheet;
    Image2: TImage;
    Label3: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label13: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    btConsultaEndereco: TcxButton;
    DBEdit4: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    btHistAltEndereco: TcxButton;
    btAltEndereco: TcxButton;
    btAltTelefone: TcxButton;
    ComboTabTipoLogradouro: TDBLookupComboBox;
    tabInfo: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    tabOutros: TcxTabSheet;
    Image4: TImage;
    GroupBox11: TGroupBox;
    Image3: TImage;
    Label124: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    cxButton5: TcxButton;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    cxButton1: TcxButton;
    qryGrupoCliente: TADOQuery;
    qryGrupoClientenCdGrupoClienteDesc: TIntegerField;
    qryGrupoClientecNmGrupoClienteDesc: TStringField;
    dsGrupoCliente: TDataSource;
    Label34: TLabel;
    qryMasternCdGrupoClienteDesc: TIntegerField;
    DBEdit7: TDBEdit;
    DBEdit38: TDBEdit;
    procedure btConsultaEnderecoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure pagClienteEnter(Sender: TObject);
    procedure tabOutrosShow(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryBuscaCEPAfterOpen(DataSet: TDataSet);
    procedure qryInformacaoBeforePost(DataSet: TDataSet);
    procedure qryEnderecoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure cxButton5Click(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btConvCadastroClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure btAltEnderecoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DesativaEndereco();
    procedure AtivaEndereco();
    procedure btHistAltEnderecoClick(Sender: TObject);
    procedure btAltTelefoneClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure qryEnderecoAfterOpen(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryEnderecoBeforeInsert(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterInsert(DataSet: TDataSet);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
  private
    { Private declarations }
    cEnderecoManual : String;
  
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_Simples: TfrmClienteVarejoPessoaFisica_Simples;

implementation

uses
  fMenu, fConsultaEndereco, DateUtils, Math, rFichaCadastralCliente_view,
  fLookup_Padrao, fClienteVarejoPessoaFisica_HistAltEnd, fClienteVarejoPessoaFisica_HistMov;

{$R *.dfm}

procedure TfrmClienteVarejoPessoaFisica_Simples.FormCreate(
  Sender: TObject);
begin
    inherited;

    cNmTabelaMaster   := 'CLIENTECREDIARIO';
    nCdTabelaSistema  := 13;
    nCdConsultaPadrao := 180;
    bLimpaAposSalvar  := False;
    bCodigoAutomatico := True;

    btConsultaEndereco.Visible := False;
    pagCliente.ActivePage      := tabEndereco;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.DesativaEndereco;
begin
  DBEDit3.ReadOnly  := True;
  DBEdit4.ReadOnly  := true;
  DBEdit13.ReadOnly := True;
  DBEdit14.ReadOnly := True;
  DBEdit16.ReadOnly := True;
  DBEdit15.ReadOnly := True;
  DBEdit17.ReadOnly := True;
  DBEdit36.ReadOnly := True;
  DBEdit37.ReadOnly := True;

  DBEDit3.Color  := $00E9E4E4;
  DBEdit4.Color  := $00E9E4E4;
  DBEdit13.Color := $00E9E4E4;
  DBEdit14.Color := $00E9E4E4;
  DBEdit16.Color := $00E9E4E4;
  DBEdit15.Color := $00E9E4E4;
  DBEdit17.Color := $00E9E4E4;
  DBEdit36.Color := $00E9E4E4;
  DBEdit37.Color := $00E9E4E4;

  btConsultaEndereco.Visible := False;
  btAltEndereco.Visible      := False;
  btAltTelefone.Visible      := False;
  btHistAltEndereco.Visible  := False;

  ComboTabTipoLogradouro.ReadOnly := True;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.AtivaEndereco;
begin
  DBEDit3.ReadOnly  := False;
  DBEdit13.ReadOnly := False;
  DBEdit14.ReadOnly := False;

  DBEDit3.Color  := clWhite;
  DBEdit13.Color := clWhite;
  DBEdit14.Color := clWhite;

  if (cEnderecoManual = 'S') then
  begin
      DBEdit4.ReadOnly  := False;
      DBEdit16.ReadOnly := False;
      DBEdit15.ReadOnly := False;
      DBEdit17.ReadOnly := False;

      DBEdit4.Color  := clWhite;
      DBEdit16.Color := clWhite;
      DBEdit15.Color := clWhite;
      DBEdit17.Color := clWhite;
  end ;

  btConsultaEndereco.Visible := True;

  ComboTabTipoLogradouro.ReadOnly := False;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btConsultaEnderecoClick(
  Sender: TObject);
var
    objConsultaEndereco : TfrmConsultaEndereco ;
begin
    inherited;

    if (qryEndereco.IsEmpty) then
        Exit;

    objConsultaEndereco := TfrmConsultaEndereco.Create( Self ) ;
    objConsultaEndereco.ShowModal() ;

    if not objConsultaEndereco.qryConsultaEndereco.Eof then
    begin
        if (qryEndereco.State = dsBrowse) then
            qryEndereco.Edit ;

        qryEnderecocEndereco.Value := objConsultaEndereco.qryConsultaEnderecocEndereco.Value;
        qryEnderecocCEP.Value      := objConsultaEndereco.qryConsultaEnderecocCEP.Value     ;
        qryEnderecocBairro.Value   := objConsultaEndereco.qryConsultaEnderecocBairro.Value  ;
        qryEnderecocCidade.Value   := objConsultaEndereco.qryConsultaEnderecocCidade.Value  ;
        qryEnderecocUF.Value       := objConsultaEndereco.edtUF.Text                        ;

        DBEdit3.SetFocus;
    end ;

    FreeAndNil( objConsultaEndereco );
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterBeforePost(
  DataSet: TDataSet);
begin
    { -- valida cadastro -- }
    if (Trim(qryMastercNmTerceiro.Value) = '') then
    begin
        MensagemAlerta('Informe o nome completo do cliente.') ;
        DBEdit2.SetFocus ;
        abort ;
    end ;

    if (Trim(qryMasterdDtNasc.AsString) = '') then
    begin
        MensagemAlerta('Informe a data de nascimento.') ;
        DBEdit12.SetFocus ;
        abort ;
    end ;

    if (frmMenu.LeParametro('PERMCLIMENOR') = 'N') and (YearsBetween(qryMasterdDtNasc.Value, Now) < 18) then
    begin
        MensagemAlerta('N�o permite cadastro de clientes menores de 18 anos.') ;
        DBEdit5.SetFocus ;
        abort ;
    end ;

    if ((Trim(qryMastercCNPJCPF.Value) = '') and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Informe o CPF do cliente.') ;
        DBEdit5.SetFocus ;
        abort ;
    end ;

    if ((Trim(qryMastercRG.Value) = '') and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Informe o documento de identidade.') ;
        DBEdit6.SetFocus ;
        abort ;
    end ;

    if ((Trim(qryMastercUFRG.Value) = '') and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Informe o estado de emiss�o do documento de identidade.') ;
        DBEdit10.SetFocus ;
        abort ;
    end ;

    if ((Trim(qryMastercUFRG.Value) <> '') and (not fnValidaEstado(qryMastercUFRG.Value))) then
    begin
        DBEdit10.SetFocus;
        Abort;
    end;

    if ((Trim(qryMasterdDtEmissaoRG.asString) = '') and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Informe a data de emiss�o do documento de identidade.') ;
        DBEdit11.SetFocus ;
        abort ;
    end ;

    if ((qryMasternCdTabTipoSexo.Value = 0) and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Selecione o sexo do cliente.') ;
        ComboSexo.SetFocus ;
        abort ;
    end ;

    if ((qryMasternCdTabTipoEstadoCivil.Value = 0) and (frmMenu.LeParametro('VALIDCLISIMPL') = 'S')) then
    begin
        MensagemAlerta('Selecione o estado civil.') ;
        ComboEstadoCivil.SetFocus;
        abort ;
    end ;

    if (Length(trim(qryMastercTelefoneMovel.Value)) > 0) and (Length(trim(qryMastercTelefoneMovel.Value)) < 10) then
    begin
        MensagemAlerta('Telefone Celular inv�lido. Informe o DDD e no m�nimo 8 d�gitos') ;
        DBedit8.SetFocus;
        abort ;
    end;

    if (frmMenu.LeParametro('VALIDCLISIMPL') = 'S') then
    begin
        // Valida o CPF
        qryBuscaClienteCPF.Close;
        PosicionaQuery(qryBuscaClienteCPF, qryMastercCNPJCPF.Value);

        if (not (qryBuscaClienteCPF.eof) and (qryBuscaClienteCPFnCdTerceiro.Value <> qryMasternCdTerceiro.Value)) then
        begin
            if (MessageDlg('CPF j� cadastrado. Deseja visualizar o cadastro ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
            begin
                PosicionaQuery(qryMaster, qryBuscaClienteCPFnCdTerceiro.AsString);
                Abort;
            end

            else
            begin
                qryMastercCNPJCPF.Value := '';
                DBEdit5.SetFocus;
            end;

            Abort;
        end;

        ValidaDoc.Documento := DBEdit5.Text;
        ValidaDoc.TipoDocto := docCPF;

        ValidaDoc.IgnorarChar := '';

        if (DBedit10.Text <> '') and (not ValidaDoc.Validar) then
        begin
            MensagemErro('CPF Inv�lido. N�o informe pontos ou hifens, apenas os n�meros.');

            qryMastercCNPJCPF.Value := '';

            DBEdit5.SetFocus;

            Abort;
        end;

        { -- valida endere�o -- }
        if (Trim(qryEnderecocCep.Value) = '') then
        begin
            MensagemAlerta('Informe o CEP.');
            DBEdit3.SetFocus;
            Abort;
        end;

        if (qryEndereconCdTabTipoLogradouro.Value = 0) then
        begin
            MensagemAlerta('Selecione o Tipo de Endere�o');
            ComboTabTipoLogradouro.SetFocus;
            Abort;
        end;

        if (Trim(qryEnderecocEndereco.Value) = '') then
        begin
            if (cEnderecoManual <> 'S') then
            begin
                MensagemAlerta('CEP inv�lido.');
                DBEdit3.SetFocus;
            end
            else
            begin
                MensagemAlerta('Informe o endere�o.');
                DBEdit4.SetFocus;
            end;

            Abort;
        end;

        if (frmMenu.ConvInteiro(qryEnderecoiNumero.AsString) = 0) then
        begin
            MensagemAlerta('Informe o n�mero residencial.');
            DBEdit13.SetFocus;
            Abort;
        end;

        if (cEnderecoManual = 'S') then
        begin

            if (Trim(qryEnderecocBairro.Value) = '') then
            begin
                MensagemAlerta('Informe o bairro.');
                DBEdit16.SetFocus;
                Abort;
            end;

            if (Trim(qryEnderecocCidade.Value) = '') then
            begin
                MensagemAlerta('Informe a cidade.');
                DBEdit15.SetFocus;
                Abort;
            end;

            if (Trim(qryEnderecocUF.Value) = '') then
            begin
                MensagemAlerta('Informe o estado.');
                DBEdit17.SetFocus;
                Abort;
            end;

            if ((Trim(qryEnderecocUF.Value) <> '') and (not fnValidaEstado(qryEnderecocUF.Value))) then
            begin
                DBEdit17.SetFocus;
                Abort;
            end;
        end;
    end;

    if (Length(trim(qryEnderecocTelefone.Value)) > 0) and (Length(trim(qryEnderecocTelefone.Value)) < 10) then
    begin
        MensagemAlerta('Telefone Residencial 1 inv�lido. Informe o DDD e no m�nimo 8 d�gitos') ;
        DBedit36.SetFocus;
        abort ;
    end;

    if (Length(trim(qryEnderecocFax.Value)) > 0) and (Length(trim(qryEnderecocFax.Value)) < 10) then
    begin
        MensagemAlerta('Telefone Residencial 2 inv�lido. Informe o DDD e no m�nimo 8 d�gitos') ;
        DBedit37.SetFocus;
        abort ;
    end;

    inherited;

    if (qryMaster.State = dsInsert) then
    begin
        qryMasterdDtCadastro.Value              := Now() ;
        qryMasternCdStatus.Value                := 1 ;
        qryMasternCdUsuarioCadastro.Value       := frmMenu.nCdUsuarioLogado;

        if (frmMenu.nCdLojaAtiva > 0) then
            qryMasternCdLojaCadastro.Value      := frmMenu.nCdLojaAtiva;

        qryMasternCdTabTipoOrigem.Value         := 1 ;
        qryMasternCdTabTipoSituacao.Value       := 1 ;
        qryMasternCdTerceiroPessoaFisica.Value  := qryMasternCdTerceiro.Value ;

        qryMastercFlgRendaCjgLimite.Value := 0;
        qryMastercFlgCartaoCredito.Value  := 0;
        qryMastercFlgTalaoCheque.Value    := 0;
        qryMastercFlgCadSimples.Value     := 1;
    end ;

    if (qryMaster.State = dsEdit) then
    begin
        qryMasternCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;
        qryMasterdDtUltAlt.Value        := Now() ;

        if (frmMenu.nCdLojaAtiva > 0) then
            qryMasternCdLojaUltAlt.Value := frmMenu.nCdLojaAtiva;

        if (qryMasternCdTabTipoOrigem.Value = 0) then
            qryMasternCdTabTipoOrigem.Value := 1 ;

        if (qryMasternCdTabTipoSituacao.Value = 0) then
            qryMasternCdTabTipoSituacao.Value := 1 ;

        if (qryEndereco.State = dsBrowse) then
            qryEndereco.Edit;

        if (Trim(DBEdit3.Text) <> '') then
            qryEndereco.Post;
    end ;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterAfterOpen(DataSet: TDataSet);
begin
    inherited;

    //DBEdit1.SetFocus(); desativado devido erro de focus ao criar tela

    pagCliente.ActivePage := tabEndereco;

    DesativaEndereco();
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.pagClienteEnter(
  Sender: TObject);
begin
    inherited;

    DBEdit3.SetFocus;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.tabOutrosShow(
  Sender: TObject);
begin
    inherited;

    if (qryMaster.IsEmpty) then
        Exit;

    qryUsuarioCad.Close ;
    qryUsuarioUltAlt.Close ;
    qryTabTipoSituacao.Close ;
    qryTabTipoOrigem.Close ;
    qryStatus.Close ;
    qryUsuarioAprova.Close ;

    PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCadastro.AsString) ;
    PosicionaQuery(qryUsuarioUltAlt, qryMasternCdUsuarioUltAlt.AsString) ;
    PosicionaQuery(qryTabTipoSituacao, qryMasternCdTabTipoSituacao.AsString) ;
    PosicionaQuery(qryTabTipoOrigem, qryMasternCdTabTipoOrigem.AsString) ;
    PosicionaQuery(qryStatus, qryMasternCdStatus.AsString) ;
    PosicionaQuery(qryUsuarioAprova, qryMasternCdUsuarioAprova.AsString) ;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.DBEdit3Exit(
  Sender: TObject);
begin
  inherited;

  if ((qryMaster.IsEmpty) or (DBEdit3.ReadOnly)) then
      Exit;

  if (Trim(DBEdit3.Text) <> '') then
  begin
      qryBuscaCEP.Close ;
      PosicionaQuery(qryBuscaCEP, DBEdit3.Text) ;
  end
  else
  begin
      qryEnderecocEndereco.Value := '';
      qryEnderecocBairro.Value   := '';
      qryEnderecocCidade.Value   := '';
      qryEnderecocUF.Value       := '';
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.DBEdit3KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    inherited;

    if (Key = VK_F4) and (not qryMaster.IsEmpty) then
        btConsultaEndereco.Click;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryBuscaCEPAfterOpen(
  DataSet: TDataSet);
begin
    inherited;

    if not (qryBuscaCEP.IsEmpty) then
    begin
        if (qryEndereco.State = dsBrowse) then
            qryEndereco.Edit ;

        qryEnderecocEndereco.Value := qryBuscaCEPcEndereco.Value;
        qryEnderecocBairro.Value   := qryBuscaCEPcBairro.Value;
        qryEnderecocCidade.Value   := qryBuscaCEPcCidade.Value;
        qryEnderecocUF.Value       := qryBuscaCEPcUF.Text;
    end

    else
    begin
        qryEnderecocEndereco.Value := '';
        qryEnderecocBairro.Value   := '';
        qryEnderecocCidade.Value   := '';
        qryEnderecocUF.Value       := '';
    end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryInformacaoBeforePost(
  DataSet: TDataSet);
begin
    inherited;

    qryInformacaonCdInformacaoTerceiro.Value := frmMenu.fnProximoCodigo('INFORMACAOTERCEIRO');
    qryInformacaonCdTerceiro.Value           := qryMasternCdTerceiro.Value;
    qryInformacaonCdLoja.Value               := frmMenu.nCdLojaAtiva;
    qryInformacaonCdUsuario.Value            := frmMenu.nCdUsuarioLogado;
    qryInformacaodDtCad.Value                := Now;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryEnderecoBeforePost(
  DataSet: TDataSet);
begin
    inherited;

    if (qryEndereco.State = dsInsert) then
    begin
        qryEndereconCdTerceiro.Value := qryMasternCdTerceiro.Value;
        qryEndereconCdEndereco.Value := frmMenu.fnProximoCodigo('ENDERECO');
        qryEndereconCdStatus.Value   := 1;
        qryEndereconCdTipoEnd.Value  := 1;
        qryEndereconCdRegiao.Value   := 1;
        qryEndereconCdPais.Value     := StrToInt(frmMenu.LeParametro('CDPAISBR'));
        qryEnderecodDtCadastro.Value := Now;
    end;

end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterAfterClose(
  DataSet: TDataSet);
begin
    inherited;

    qryEndereco.Close;
    qryInformacao.Close;
    qryUsuarioCad.Close;
    qryUsuarioUltAlt.Close;
    qryTabTipoSituacao.Close;
    qryTabTipoOrigem.Close;
    qryStatus.Close;
    qryUsuarioAprova.Close;
    qryGrupoCliente.Close;

    DesativaEndereco();
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.cxButton5Click(
  Sender: TObject);
var
  objForm : TrptFichaCadastralCliente_view;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  objForm := TrptFichaCadastralCliente_view.Create(nil);

  objForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objForm.qryTerceiro.Close;
  PosicionaQuery(objForm.qryTerceiro, qryMasternCdTerceiro.AsString);

  objForm.qrySPCPessoaFisica.Close;
  PosicionaQuery(objForm.qrySPCPessoaFisica, qryMasternCdTerceiro.AsString);

  objForm.QuickRep1.PreviewModal;

  FreeAndNil(objForm);
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btIncluirClick(
  Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btConvCadastroClick(
  Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (MessageDLG('O processo de convers�o de cadastro n�o poder� ser desfeito.' + #13 + 'Deseja converter o Cadastro Simples para Cadastro Completo ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      { -- atualiza flag do cliente para que seja utilizado cadastro completo -- }
      qryConverteCadastro.Close;
      qryConverteCadastro.Parameters.ParamByName('nPK').Value := qryMasternCdTerceiro.Value;
      qryConverteCadastro.ExecSQL;

      ShowMessage('Convers�o gerada com sucesso.');
      qryMaster.Close;
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btConsultarClick(
  Sender: TObject);
var
  nPK : Integer;
begin
    //inherited;

    nPK := frmLookup_Padrao.ExecutaConsulta2(180,'cFlgCadSimples = 1');

    if (nPK <> 0) then
        PosicionaQuery(qryMaster, IntToStr(nPK));

end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btAltEnderecoClick(
  Sender: TObject);
begin
  inherited;

  if ((qryMaster.Active) and (qryMaster.RecordCount > 0)) then
  begin
      if (qryEndereco.IsEmpty) then
          qryEndereco.Insert
      else
          qryEndereco.Edit;

      AtivaEndereco();

      DBEdit3.SetFocus;
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.FormShow(Sender: TObject);
begin
  inherited;

  qrySexo.Close;
  qrySexo.Open;

  qryTabTipoEstadoCivil.Close;
  qryTabTipoEstadoCivil.Open;
  qryTabTipoLogradouro.Open;

  cEnderecoManual := frmMenu.LeParametro('CLIPERMENDMAN') ;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btHistAltEnderecoClick(
  Sender: TObject);
var
  objClienteVarejoPessoaFisica_HistAltEnd : TfrmClienteVarejoPessoaFisica_HistAltEnd;
begin
  inherited;

  if (not qryMaster.Active) then
      Exit;

  objClienteVarejoPessoaFisica_HistAltEnd := TfrmClienteVarejoPessoaFisica_HistAltEnd.Create( Self ) ;

  PosicionaQuery(objClienteVarejoPessoaFisica_HistAltEnd.qryEndereco, qryMasternCdTerceiro.asString) ;
  objClienteVarejoPessoaFisica_HistAltEnd.ShowModal ;

  freeAndNil( objClienteVarejoPessoaFisica_HistAltEnd ) ;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.btAltTelefoneClick(
  Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin
    if (qryEndereco.RecordCount = 0) then
    begin
        MensagemAlerta('Cliente ainda n�o possui endere�o cadastrado.');
        DBEdit3.SetFocus;
        Abort;
    end;

    DBEdit36.ReadOnly := False;
    DBEdit37.ReadOnly := False;

    DBEdit36.Color := clWhite;
    DBEdit37.Color := clWhite;

    DBEdit36.SetFocus;
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.cxButton1Click(
  Sender: TObject);
var
    objClienteVarejoPessoaFisica_HistMov : TfrmClienteVarejoPessoaFisica_HistMov ;
begin

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryMaster.Active) and (qryMasternCdTerceiro.Value > 0) then
  begin
      objClienteVarejoPessoaFisica_HistMov := TfrmClienteVarejoPessoaFisica_HistMov.Create( Self ) ;

      objClienteVarejoPessoaFisica_HistMov.exibeMovimentos( qryMasternCdTerceiro.Value ) ;

      freeAndNil( objClienteVarejoPessoaFisica_HistMov );
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryEnderecoAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State <> dsInsert) then
  begin
      btAltEndereco.Visible     := True;
      btAltTelefone.Visible     := True;
      btHistAltEndereco.Visible := True;
  end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterAfterPost(
  DataSet: TDataSet);
begin
    inherited;

    if (qryEndereco.State = dsInsert) then
        qryEndereco.Post;

    qryMaster.Close;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryEnderecoBeforeInsert(
  DataSet: TDataSet);
begin
    inherited;

    AtivaEndereco;

    DBEdit36.ReadOnly := False;
    DBEdit37.ReadOnly := False;

    DBEdit36.Color := clWhite;
    DBEdit37.Color := clWhite;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
    inherited;

    qryGrupoCliente.Close;
    PosicionaQuery(qryGrupoCliente, qryMasternCdGrupoClienteDesc.AsString);
    
    if (qryMaster.State <> dsInsert) then
    begin
        qryEndereco.Close;
        PosicionaQuery(qryEndereco, qryMasternCdTerceiro.AsString);
        
        qryInformacao.Close;
        PosicionaQuery(qryInformacao, qryMasternCdTerceiro.AsString);
    end;

    if (qryEndereco.IsEmpty) then
    begin
       btAltEndereco.Click;

       DBEdit36.ReadOnly := False;
       DBEdit37.ReadOnly := False;

       DBEdit36.Color := clWhite;
       DBEdit37.Color := clWhite;
    end;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.qryMasterAfterInsert(
  DataSet: TDataSet);
begin
  inherited;

  qryEndereco.Close;
  qryEndereco.Open;
  qryEndereco.Insert;

  AtivaEndereco;
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.DBEdit7KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  {case Key of
      vk_f4 : begin

          if(qryMaster.State = dsBrowse) then
              qryMaster.Edit;

          if(qryMaster.State in[dsInsert, dsEdit]) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta(787);

              if(nPK > 0) then
                  qryMasternCdGrupoClienteDesc.Value := nPK;

          end;
      end;
  end;}
end;

procedure TfrmClienteVarejoPessoaFisica_Simples.DBEdit7Exit(
  Sender: TObject);
begin
  inherited;

  {qryGrupoCliente.Close;

  if(qryMaster.IsEmpty) then
      Exit;

  PosicionaQuery(qryGrupoCliente, qryMasternCdGrupoClienteDesc.AsString);}
end;

initialization
    RegisterClass(TfrmClienteVarejoPessoaFisica_Simples);

end.
