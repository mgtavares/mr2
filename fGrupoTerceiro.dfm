inherited frmGrupoTerceiro: TfrmGrupoTerceiro
  Left = 64
  Top = 141
  Height = 664
  Caption = 'Grupo Terceiro'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 608
  end
  object Label1: TLabel [1]
    Left = 48
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 37
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 91
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoTerceiro'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 91
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoTerceiro'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoTerceiro'
      'WHERE nCdGrupoTerceiro = :nPK')
    object qryMasternCdGrupoTerceiro: TIntegerField
      FieldName = 'nCdGrupoTerceiro'
    end
    object qryMastercNmGrupoTerceiro: TStringField
      FieldName = 'cNmGrupoTerceiro'
      Size = 50
    end
  end
end
