unit fTipoImposto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup, DBGridEhGrouping, GridsEh,
  DBGridEh, cxPC, cxControls;

type
  TfrmTipoImposto = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasternCdTipoImposto: TIntegerField;
    qryMastercNmTipoImposto: TStringField;
    qryMasternCdTabTipoCompetTributaria: TIntegerField;
    qryMasternCdTabTipoGuiaRecImposto: TIntegerField;
    qryMasternCdTerceiroCredor: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdEspTitPagto: TIntegerField;
    qryMasternCdEspTitProv: TIntegerField;
    qryMasternCdTabTipoRetencaoImposto: TIntegerField;
    qryMasternCdTabTipoPessoa: TIntegerField;
    qryMastercFlgAgrupaPagamento: TIntegerField;
    qryMastercFlgAbatimento: TIntegerField;
    qryMasteriNumeroDias: TIntegerField;
    qryMasteriDiaVencto: TIntegerField;
    qryMasternValMinRetencao: TBCDField;
    qryMasternValMaxRetencao: TBCDField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryTipoCompetTributaria: TADOQuery;
    Label3: TLabel;
    dsTipoCompetTributaria: TDataSource;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    qryTipoGuiaRecolhimento: TADOQuery;
    dsTipoGuiaRecolhimento: TDataSource;
    DBEdit6: TDBEdit;
    DataSource1: TDataSource;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    Label12: TLabel;
    DBEdit14: TDBEdit;
    Label13: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    DBEdit16: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    qryTerceironCdMoeda: TIntegerField;
    dsTerceiro: TDataSource;
    DBEdit19: TDBEdit;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryCategFinancnCdPlanoConta: TIntegerField;
    qryCategFinancnCdServidorOrigem: TIntegerField;
    qryCategFinancdDtReplicacao: TDateTimeField;
    dsCategFinanc: TDataSource;
    DBEdit20: TDBEdit;
    qryEspTitPagto: TADOQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    IntegerField6: TIntegerField;
    dsEspTitPagto: TDataSource;
    DBEdit21: TDBEdit;
    qryEspTitProv: TADOQuery;
    qryEspTitProvnCdEspTit: TIntegerField;
    qryEspTitProvnCdGrupoEspTit: TIntegerField;
    qryEspTitProvcNmEspTit: TStringField;
    qryEspTitProvcTipoMov: TStringField;
    qryEspTitProvcFlgPrev: TIntegerField;
    dsEspTitProv: TDataSource;
    DBEdit22: TDBEdit;
    qryTipoRetencao: TADOQuery;
    DBEdit23: TDBEdit;
    dsTipoRetencao: TDataSource;
    qryTipoPessoa: TADOQuery;
    DBEdit24: TDBEdit;
    dsTipoPessoa: TDataSource;
    edtCompetTributaria: TER2LookupDBEdit;
    edtGuiaRecolhimento: TER2LookupDBEdit;
    edtTerceiroCredor: TER2LookupDBEdit;
    edtCategFinanc: TER2LookupDBEdit;
    edtEspTitPagto: TER2LookupDBEdit;
    edtEspTitPrev: TER2LookupDBEdit;
    edtTipoRetencao: TER2LookupDBEdit;
    edtTipoPessoa: TER2LookupDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox3: TDBLookupComboBox;
    dsAbatimento: TDataSource;
    dsAgruparPagamento: TDataSource;
    DataSource2: TDataSource;
    qryTipoCompetTributarianCdTabTipoCompetTributaria: TIntegerField;
    qryTipoCompetTributariacNmTabTipoCompetTributaria: TStringField;
    qryTipoGuiaRecolhimentonCdTabTipoGuiaRecImposto: TIntegerField;
    qryTipoGuiaRecolhimentocNmTabTipoGuiaRecImposto: TStringField;
    qryTipoPessoanCdTabTipoPessoa: TIntegerField;
    qryTipoPessoacNmTabTipoPessoa: TStringField;
    qryTipoRetencaonCdTabTipoRetencaoImposto: TIntegerField;
    qryTipoRetencaocNmTabTipoRetencaoImposto: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryMunicipio: TADOQuery;
    qryMunicipionCdMunicipio: TIntegerField;
    qryMunicipionCdEstado: TIntegerField;
    qryMunicipiocNmMunicipio: TStringField;
    qryMunicipionCdMunicipioIBGE: TIntegerField;
    dsMunicipio: TDataSource;
    qryTipoImpostoMunicipio: TADOQuery;
    dsTipoImpostoMunicipio: TDataSource;
    qryTipoImpostoMunicipionCdTipoImpostoMunicipio: TIntegerField;
    qryTipoImpostoMunicipionCdTipoImposto: TIntegerField;
    qryTipoImpostoMunicipionCdMunicipio: TIntegerField;
    qryTipoImpostoMunicipionCdTerceiroCredor: TIntegerField;
    qryTerceiroMunicipio: TADOQuery;
    DataSource3: TDataSource;
    qryTipoImpostoMunicipiocNmMunicipio: TStringField;
    qryTipoImpostoMunicipiocNmTerceiroCredor: TStringField;
    qryTerceiroMunicipionCdTerceiro: TIntegerField;
    qryTerceiroMunicipiocCNPJCPF: TStringField;
    qryTerceiroMunicipiocNmTerceiro: TStringField;
    qryTerceiroMunicipionCdStatus: TIntegerField;
    qryTerceiroMunicipionCdMoeda: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryTipoImpostoMunicipioBeforePost(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure qryTipoImpostoMunicipioAfterScroll(DataSet: TDataSet);
    procedure qryTipoImpostoMunicipioCalcFields(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure edtTerceiroCredorExit(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure edtEspTitPrevBeforeLookup(Sender: TObject);
    procedure edtCategFinancBeforeLookup(Sender: TObject);
    procedure edtEspTitPagtoBeforeLookup(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoImposto: TfrmTipoImposto;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoImposto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOIMPOSTO' ;
  nCdTabelaSistema  := 505 ;
  nCdConsultaPadrao := 1012 ;

  btIncluir.Enabled  := False ;
  btExcluir.Enabled  := False ;

end;
procedure TfrmTipoImposto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.setFocus;
end;

procedure TfrmTipoImposto.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh2.Col = 1) then
        begin

            if (qryTipoImpostoMunicipio.State = dsBrowse) then
            qryTipoImpostoMunicipio.Edit ;

            if (qryTipoImpostoMunicipio.State = dsInsert) or (qryTipoImpostoMunicipio.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(205);

                If (nPK > 0) then
                begin
                    qryTipoImpostoMunicipionCdMunicipio.Value := nPK ;
                end ;

            end ;
        end;

        if (DBGridEh2.Col = 3) then
        begin

            if (qryTipoImpostoMunicipio.State = dsBrowse) then
            qryTipoImpostoMunicipio.Edit ;

            if (qryTipoImpostoMunicipio.State = dsInsert) or (qryTipoImpostoMunicipio.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(17);

                If (nPK > 0) then
                begin
                    qryTipoImpostoMunicipionCdTerceiroCredor.Value := nPK ;
                end ;

            end ;
        end;

    end ;

  end ;

end;

procedure TfrmTipoImposto.FormShow(Sender: TObject);
begin
  inherited;

  btIncluir.Enabled  := False ;
  btExcluir.Enabled  := False ;

  qryMaster.open;
  PosicionaQuery(qryTipoImpostoMunicipio,qryMasternCdTipoImposto.AsString);

 
end;

procedure TfrmTipoImposto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTipoCompetTributaria.Close;
  qryTipoGuiaRecolhimento.Close;
  qryTerceiro.Close;
  qryCategFinanc.Close;
  qryEspTitPagto.Close;
  qryEspTitProv.Close;
  qryTipoRetencao.Close;
  qryTipoPessoa.Close;
  qryTipoImpostoMunicipio.Close;

  PosicionaQuery(qryTipoCompetTributaria,qryMasternCdTabTipoCompetTributaria.AsString);
  PosicionaQuery(qryTipoGuiaRecolhimento,qryMasternCdTabTipoGuiaRecImposto.AsString);
  PosicionaQuery(qryTerceiro,qryMasternCdTerceiroCredor.AsString);
  PosicionaQuery(qryCategFinanc,qryMasternCdCategFinanc.AsString);
  PosicionaQuery(qryEspTitPagto,qryMasternCdEspTitPagto.AsString);
  PosicionaQuery(qryEspTitProv,qryMasternCdEspTitProv.AsString);
  PosicionaQuery(qryTipoRetencao,qryMasternCdTabTipoRetencaoImposto.AsString);
  PosicionaQuery(qryTipoPessoa,qryMasternCdTabTipoPessoa.AsString);

  PosicionaQuery(qryTipoImpostoMunicipio,qryMasternCdTipoImposto.AsString);

end;

procedure TfrmTipoImposto.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh2.Col = 1) then
      PosicionaQuery(qryMunicipio,qryTipoImpostoMunicipionCdMunicipio.AsString);


  if (DBGridEh2.Col = 3) then
      PosicionaQuery(qryTerceiroMunicipio,qryTipoImpostoMunicipionCdTerceiroCredor.AsString);

end;

procedure TfrmTipoImposto.qryTipoImpostoMunicipioBeforePost(
  DataSet: TDataSet);
begin

  if (qryTipoImpostoMunicipionCdMunicipio.Value = 0) then
  begin
      MensagemAlerta('Selecione o Munic�pio.') ;
      abort ;
  end;

  if (qryTipoImpostoMunicipionCdTerceiroCredor.Value = 0) then
  begin
      MensagemAlerta('Selecione o Terceiro Credor.') ;
      abort ;
  end;

  qryTipoImpostoMunicipionCdTipoImposto.Value := qryMasternCdTipoImposto.Value;

  if (qryTipoImpostoMunicipio.State = dsInsert) then
      qryTipoImpostoMunicipionCdTipoImpostoMunicipio.Value := frmMenu.fnProximoCodigo('TIPOIMPOSTOMUNICIPIO') ;

  inherited;

end;

procedure TfrmTipoImposto.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryTipoCompetTributaria.Close;
  qryTipoGuiaRecolhimento.Close;
  qryTerceiro.Close;
  qryCategFinanc.Close;
  qryEspTitPagto.Close;
  qryEspTitProv.Close;
  qryTipoRetencao.Close;
  qryTipoPessoa.Close;
  qryTipoImpostoMunicipio.Close;

  btExcluir.Enabled  := False ;

end;

procedure TfrmTipoImposto.qryTipoImpostoMunicipioAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryMunicipio,qryTipoImpostoMunicipionCdMunicipio.AsString);
  PosicionaQuery(qryTerceiroMunicipio,qryTipoImpostoMunicipionCdTerceiroCredor.AsString);
end;

procedure TfrmTipoImposto.qryTipoImpostoMunicipioCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryMunicipio,qryTipoImpostoMunicipionCdMunicipio.AsString);
  PosicionaQuery(qryTerceiroMunicipio,qryTipoImpostoMunicipionCdTerceiroCredor.AsString);

  if qryTipoImpostoMunicipionCdMunicipio.value > 0
      then  qryTipoImpostoMunicipiocNmMunicipio.Value := qryMunicipiocNmMunicipio.Value;

  if qryTipoImpostoMunicipionCdTerceiroCredor.value > 0
      then qryTipoImpostoMunicipiocNmTerceiroCredor.Value := qryTerceiroMunicipiocNmTerceiro.Value;

end;

procedure TfrmTipoImposto.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit13.Text) = '') then qryMasternValMinRetencao.Value := 0;
  if (Trim(DBEdit14.Text) = '') then qryMasternValMaxRetencao.Value := 0;

  if (Trim(DBEdit16.Text) = '') then qryMasteriDiaVencto.Value  := 0;
  if (Trim(DBEdit15.Text) = '') then qryMasteriNumeroDias.Value := 0;

  if ((qryMasternCdTabTipoCompetTributaria.Value = 0) or (DBEdit4.Text = '' )) then
  begin
      MensagemAlerta('Informe a Compet�ncia Tribut�ria.') ;
      abort ;
      DBEdit4.SetFocus;
  end;

  if ((qryMasternCdTabTipoGuiaRecImposto.Value = 0) or (DBEdit6.Text = '' )) then
  begin
      MensagemAlerta('Informe a guia de recolhimento.') ;
      abort ;
      DBEdit6.SetFocus;
  end;

  if ((qryMasternCdTerceiroCredor.Value = 0) and ((qryMasternCdTabTipoCompetTributaria.Value > 0) and (qryMasternCdTabTipoCompetTributaria.Value <=2))) then
  begin
      MensagemAlerta('Informe o Terceiro Credor.') ;
      abort ;
  end;

  if ((qryMasternCdCategFinanc.Value = 0) or (DBEdit20.Text = '' )) then
  begin
      MensagemAlerta('Informe a Catetgoria Financeira.') ;
      abort ;
      DBEdit20.SetFocus;
  end;

  if ((qryMasternCdEspTitPagto.Value = 0) or (DBEdit21.Text = '' )) then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo .') ;
      abort ;
      DBEdit21.SetFocus;
  end;

  if ((qryMasternCdEspTitProv.Value = 0) or (DBEdit22.Text = '' )) then
  begin
      MensagemAlerta('Informe a esp�cie de previs�o.') ;
      abort ;
      DBEDit22.SetFocus;
  end;

  if ((qryMasternCdTabTipoRetencaoImposto.Value = 0) or (DBEdit23.Text = '' )) then
  begin
      MensagemAlerta('Informe o tipo de reten��o.') ;
      abort ;
      DBEDit23.SetFocus;
  end;

  if ((qryMasternCdTabTipoPessoa.Value = 0) or (DBEdit24.Text = '' )) then
  begin
      MensagemAlerta('Informe o tipo de Pessoa.') ;
      abort ;
      DBEDit24.SetFocus;
  end;

  if (qryMasteriDiaVencto.Value < 0) or (qryMasteriDiaVencto.Value > 31) then
  begin
      MensagemAlerta('Informe um dia de Vencimento entre 0 e 31.') ;
      abort ;
  end;

  if (qryMasteriNumeroDias.Value < 0) then
  begin
      MensagemAlerta('Informe numero de dias maior que 0.') ;
      abort ;
  end;

  if (qryMasteriDiaVencto.Value > 0) and (qryMasteriNumeroDias.Value > 0) then
  begin
      MensagemAlerta('Informe apenas Dia de Vencimento ou N�mero de Dias.') ;
      DBEdit16.SetFocus;
      abort ;
  end;

  if (qryMasteriDiaVencto.Value = 0) and  (qryMasteriNumeroDias.Value =0) then
  begin
      MensagemAlerta('Informe o Dia de Vencimento ou N�mero de Dias.') ;
      DBEdit16.SetFocus;
      abort ;
  end;

  inherited;

end;

procedure TfrmTipoImposto.edtTerceiroCredorExit(Sender: TObject);
begin
  inherited;
  if ((qryMasternCdTerceiroCredor.Value = 0) and ((qryMasternCdTabTipoCompetTributaria.Value > 0) and (qryMasternCdTabTipoCompetTributaria.Value <=2))) then
  begin
      MensagemAlerta('Informe o Terceiro Credor.') ;
      abort ;
  end;
end;

procedure TfrmTipoImposto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
   qryTipoImpostoMunicipio.Close;
end;

procedure TfrmTipoImposto.edtEspTitPrevBeforeLookup(Sender: TObject);
begin
  inherited;
  edtEspTitPrev.WhereAdicional.Text := 'cTipoMov = ' +  Chr(39) + 'P'  + Chr(39) + 'AND cFlgPrev = ' + '1';
end;

procedure TfrmTipoImposto.edtCategFinancBeforeLookup(Sender: TObject);
begin
  inherited;
  edtCategFinanc.WhereAdicional.Text := 'cFlgRecDes = ' + Chr(39) + 'D' + Chr(39) ;
end;

procedure TfrmTipoImposto.edtEspTitPagtoBeforeLookup(Sender: TObject);
begin
  inherited;
  edtEspTitPagto.WhereAdicional.Text := 'cTipoMov = ' +  Chr(39) + 'P'  + Chr(39) + 'AND cFlgPrev <> ' + '1';
end;

procedure TfrmTipoImposto.btConsultarClick(Sender: TObject);
begin

  inherited;
  btExcluir.Enabled  := False ;
  edtCompetTributaria.SetFocus;
end;

procedure TfrmTipoImposto.btSalvarClick(Sender: TObject);
begin
  inherited;
  btExcluir.Enabled  := False ;
  qryTipoCompetTributaria.Close;
  qryTipoGuiaRecolhimento.Close;
  qryTerceiro.Close;
  qryCategFinanc.Close;
  qryEspTitPagto.Close;
  qryEspTitProv.Close;
  qryTipoRetencao.Close;
  qryTipoPessoa.Close;
  qryTipoImpostoMunicipio.Close;

end;

procedure TfrmTipoImposto.ToolButton1Click(Sender: TObject);
begin
  inherited;
  btExcluir.Enabled  := False ;
end;

initialization
    RegisterClass(tFrmTipoImposto) ;

end.
