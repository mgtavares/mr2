unit fPedidoDevVenda_ConsultaItens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DBCtrls,
  StdCtrls, Mask, ER2Lookup, cxPC, cxControls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxGridCustomPopupMenu,
  cxGridPopupMenu, Menus, cxLookAndFeelPainters, cxButtons, ADODB;

type
  TfrmPedidoDevVenda_ConsultaItens = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label2: TLabel;
    mskDtInicial: TMaskEdit;
    mskDtFinal: TMaskEdit;
    Label1: TLabel;
    edtNumNF: TMaskEdit;
    Label3: TLabel;
    cxPageControl1: TcxPageControl;
    tabItemDoctoFiscal: TcxTabSheet;
    cxGridPedidoDBTableView1: TcxGridDBTableView;
    cxGridPedidoLevel1: TcxGridLevel;
    cxGridPedido: TcxGrid;
    popupMenu: TPopupMenu;
    btVisualizarDoctoFiscal: TMenuItem;
    btConsultarDoctoFiscal: TcxButton;
    qryConsultaPedido: TADOQuery;
    qryConsultaPedidonCdPedido: TIntegerField;
    qryConsultaPedidodDtPedido: TDateTimeField;
    qryConsultaPedidocNmTerceiro: TStringField;
    qryConsultaPedidonCdItemPedido: TIntegerField;
    qryConsultaPedidocCdProduto: TStringField;
    qryConsultaPedidocNmItem: TStringField;
    qryConsultaPedidonCdTipoItemPed: TIntegerField;
    qryConsultaPedidonValUnitario: TBCDField;
    qryConsultaPedidonQtdeAtendida: TBCDField;
    dsConsultaPedido: TDataSource;
    cxGridPedidoDBTableView1DBColumn_nCdPedido: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_dDtPedido: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_iNrDocto: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_cCdProduto: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_cNmItem: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_nQtdeAtendida: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_nValUnitario: TcxGridDBColumn;
    cxGridPedidoDBTableView1DBColumn_nCdItemPedido: TcxGridDBColumn;
    qryConsultaPedidoiNrDocto: TIntegerField;
    SP_INCLUI_ITEM_DEVOLUCAO: TADOStoredProc;
    qryConsultaPedidocUFEmitente: TStringField;
    qryConsultaPedidonCdDoctoFiscal: TIntegerField;
    procedure btConsultarDoctoFiscalClick(Sender: TObject);
    procedure cxGridPedidoDBTableView1DblClick(Sender: TObject);
    procedure btVisualizarDoctoFiscalClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTipoPedido : Integer;
    nCdPedido     : Integer;
    nCdTerceiro   : Integer;
  end;

var
  frmPedidoDevVenda_ConsultaItens: TfrmPedidoDevVenda_ConsultaItens;

implementation

uses
  fMenu, fDoctoFiscal;

{$R *.dfm}

procedure TfrmPedidoDevVenda_ConsultaItens.btConsultarDoctoFiscalClick(
  Sender: TObject);
begin
  inherited;

  if (Trim(mskDtInicial.Text) = '/  /') then
      mskDtInicial.Text := DateToStr(Now());

  if (Trim(mskDtFinal.Text) = '/  /') then
      mskDtFinal.Text := DateToStr(Now());


  { -- valida per�odo -- }
  if (Trim(mskDtInicial.Text) <> '/  /') then
  begin
      if (frmMenu.ConvData(mskDtInicial.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtInicial.SetFocus;
          Abort;
      end;
  end;

  if (Trim(mskDtFinal.Text) <> '/  /') then
  begin
      if (frmMenu.ConvData(mskDtFinal.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtFinal.SetFocus;
          Abort;
      end;
  end;

  if (not frmMenu.fnValidaPeriodo(frmMenu.ConvData(mskDtInicial.Text),frmMenu.ConvData(mskDtFinal.Text))) then
  begin
      mskDtInicial.SetFocus;
      Abort;
  end;
  
  qryConsultaPedido.Close;
  qryConsultaPedido.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryConsultaPedido.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
  qryConsultaPedido.Parameters.ParamByName('nCdPedido').Value   := nCdPedido;
  qryConsultaPedido.Parameters.ParamByName('iNrDocto').Value    := frmMenu.ConvInteiro(edtNumNF.Text);
  qryConsultaPedido.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(mskDtInicial.Text);
  qryConsultaPedido.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(mskDtFinal.Text);
  qryConsultaPedido.Open;

  if (qryConsultaPedido.IsEmpty) then
      cxGridPedidoDBTableView1.ViewData.Expand(False)
  else
      cxGridPedidoDBTableView1.ViewData.Expand(True);
end;

procedure TfrmPedidoDevVenda_ConsultaItens.cxGridPedidoDBTableView1DblClick(
  Sender: TObject);
var
  cQtdeDevolv : string ;
begin
  inherited;

  if (qryConsultaPedido.RecordCount > 0) then
  begin
      case MessageDlg('Confirma a inclus�o do produto na devolu��o ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit;
      end;

      cQtdeDevolv := '1';
      
      InputQuery('Devolu��o de Venda','Quantidade Devolvida:',cQtdeDevolv);

      try
          StrToFloat(cQtdeDevolv);
      except
          MensagemAlerta('A quantidade informada n�o � um n�mero v�lido.');
          Abort;
      end;

      if (StrToFloat(cQtdeDevolv) > qryConsultaPedidonQtdeAtendida.Value) then
      begin
          MensagemAlerta('A quantidade informada � maior que a quantidade vendida.') ;
          Abort;
      end;

      try
          frmMenu.Connection.BeginTrans;

          SP_INCLUI_ITEM_DEVOLUCAO.Close;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdPedido').Value           := nCdPedido;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdProduto').Value          := 0;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cNmItem').Value             := qryConsultaPedidocNmItem.Value;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cSiglaUnidadeMedida').Value := '';
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nQtdePed').Value            := StrToFloat(cQtdeDevolv);
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitario').Value        := qryConsultaPedidonValUnitario.Value;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitarioEsp').Value     := 0;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nPercIPI').Value            := 0;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdItem').Value             := qryConsultaPedidonCdItemPedido.Value;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdTipoItemPed').Value      := qryConsultaPedidonCdTipoItemPed.Value;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cUFOrigem').Value           := qryConsultaPedidocUFEmitente.Value;
          SP_INCLUI_ITEM_DEVOLUCAO.ExecProc;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Produto incluso com sucesso.');
      except
          MensagemErro('Erro no processamento.');
          frmMenu.Connection.RollbackTrans;
          Raise;
      end;

      qryConsultaPedido.Requery();
  end;
end;

procedure TfrmPedidoDevVenda_ConsultaItens.btVisualizarDoctoFiscalClick(
  Sender: TObject);
var
  objDoctoFiscal : TfrmDoctoFiscal;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMDOCTOFISCAL')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objDoctoFiscal := TfrmDoctoFiscal.Create(nil);

  PosicionaQuery(objDoctoFiscal.qryMaster, qryConsultaPedidonCdDoctoFiscal.AsString);

  showForm(objDoctoFiscal,True);
end;

procedure TfrmPedidoDevVenda_ConsultaItens.FormCreate(Sender: TObject);
begin
  inherited;

  btVisualizarDoctoFiscal.Enabled := frmMenu.fnValidaUsuarioAPL('FRMDOCTOFISCAL');
end;

end.
