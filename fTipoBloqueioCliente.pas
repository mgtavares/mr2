unit fTipoBloqueioCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh;

type
  TfrmTipoBloqueioCliente = class(TfrmCadastro_Padrao)
    qryMasternCdTabTipoRestricaoVenda: TIntegerField;
    qryMastercNmTabTipoRestricaoVenda: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoRestricao: TADOQuery;
    dsUsuarioTipoRestricao: TDataSource;
    qryUsuarioTipoRestricaonCdUsuarioTabTipoRestricaoVenda: TAutoIncField;
    qryUsuarioTipoRestricaonCdTabTipoRestricaoVenda: TIntegerField;
    qryUsuarioTipoRestricaonCdUsuario: TIntegerField;
    qryUsuarioTipoRestricaocNmUsuario: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryMastercFlgGerenteLibera: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioTipoRestricaoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryUsuarioTipoRestricaoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoBloqueioCliente: TfrmTipoBloqueioCliente;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoBloqueioCliente.FormCreate(Sender: TObject);
begin
  inherited;
  
  cNmTabelaMaster   := 'TABTIPORESTRICAOVENDA' ;
  nCdTabelaSistema  := 82 ;
  nCdConsultaPadrao := 189 ;

end;

procedure TfrmTipoBloqueioCliente.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTipoRestricao.State = dsBrowse) then
             qryUsuarioTipoRestricao.Edit ;
        

        if (qryUsuarioTipoRestricao.State = dsInsert) or (qryUsuarioTipoRestricao.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTipoRestricaonCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoBloqueioCliente.qryUsuarioTipoRestricaoBeforePost(DataSet: TDataSet);
begin
  if (qryUsuarioTipoRestricaocNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um usu�rio.') ;
      abort ;
  end ;

  qryUsuarioTipoRestricaonCdTabTipoRestricaoVenda.Value := qryMasternCdTabTipoRestricaoVenda.Value ;

  if (qryUsuarioTipoRestricao.State = dsInsert) then
      qryUsuarioTipoRestricaonCdUsuarioTabTipoRestricaoVenda.Value := frmMenu.fnProximoCodigo('USUARIOTABTIPORESTRICAOVENDA') ;

  inherited;

end;

procedure TfrmTipoBloqueioCliente.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioTipoRestricao.Close ;
  
end;

procedure TfrmTipoBloqueioCliente.qryUsuarioTipoRestricaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioTipoRestricaocNmUsuario.Value = '') and (qryUsuarioTipoRestricaonCdUsuario.Value > 0) then
  begin

      qryUsuario.Close ;

      PosicionaQuery(qryUsuario, qryUsuarioTipoRestricaonCdUsuario.asString) ;

      if not qryUsuario.eof then
          qryUsuarioTipoRestricaocNmUsuario.Value := qryUsuariocNmUsuario.Value ;

  end ;
  
end;

procedure TfrmTipoBloqueioCliente.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioTipoRestricao.Close ;
  PosicionaQuery(qryUsuarioTipoRestricao, qryMasternCdTabTipoRestricaoVenda.asString) ;

end;

procedure TfrmTipoBloqueioCliente.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  if (bInclusao) then
    PosicionaQuery(qryUsuarioTipoRestricao, qryMasternCdTabTipoRestricaoVenda.AsString) ;

end;

procedure TfrmTipoBloqueioCliente.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus ;
  
end;

procedure TfrmTipoBloqueioCliente.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do bloqueio.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  inherited;

end;

procedure TfrmTipoBloqueioCliente.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post ;
      
end;

initialization
    RegisterClass(TfrmTipoBloqueioCliente) ;
    

end.
