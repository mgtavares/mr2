inherited frmGerenciaPreco: TfrmGerenciaPreco
  Left = 181
  Top = 199
  Width = 945
  Caption = 'Gerenciamento de Pre'#231'os'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 929
  end
  inherited ToolBar1: TToolBar
    Width = 929
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 929
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTabPrecoAux
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTabPrecoAux'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 287
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 339
      end
      item
        EditButtons = <>
        FieldName = 'nValor'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nPercent'
        Footers = <>
        Width = 69
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryTabPrecoAux: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTabPrecoAuxBeforePost
    OnCalcFields = qryTabPrecoAuxCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabPrecoAux'
      'WHERE nCdEmpresa = :nPK'
      'ORDER BY nCdProduto')
    Left = 368
    Top = 152
    object qryTabPrecoAuxnCdTabPrecoAux: TAutoIncField
      FieldName = 'nCdTabPrecoAux'
      ReadOnly = True
    end
    object qryTabPrecoAuxnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTabPrecoAuxnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryTabPrecoAuxnCdTerceiro: TIntegerField
      DisplayLabel = 'Cliente|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryTabPrecoAuxnValor: TBCDField
      DisplayLabel = 'Valor Unit'#225'rio'
      FieldName = 'nValor'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTabPrecoAuxnPercent: TBCDField
      DisplayLabel = '% Valor'
      FieldName = 'nPercent'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTabPrecoAuxcNmTerceiro: TStringField
      DisplayLabel = 'Cliente|Nome Cliente'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryTabPrecoAuxcNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o Produto'
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      Size = 150
      Calculated = True
    end
  end
  object dsTabPrecoAux: TDataSource
    DataSet = qryTabPrecoAux
    Left = 448
    Top = 160
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 272
    Top = 256
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 336
    Top = 248
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
end
