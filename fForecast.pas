unit fForecast;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, GridsEh, DBGridEh, cxPC,
  cxControls, ER2Lookup, cxLookAndFeelPainters, cxButtons, ER2Excel,
  ToolCtrlsEh;

type
  TfrmForecast = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    qryProduto: TADOQuery;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    DBEdit10: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DataSource1: TDataSource;
    dsPlanDiario: TDataSource;
    qryForecast: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    qryInsereItem: TADOQuery;
    ER2Excel1: TER2Excel;
    qryExcluiItem: TADOQuery;
    qryMasternCdForecast: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmForecast: TStringField;
    qryMasterdDtInicial: TDateTimeField;
    qryMasterdDtFinal: TDateTimeField;
    qryMastercDocumento: TStringField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryMasternCdUsuarioUltAlt: TIntegerField;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure qryForecastBeforeDelete(DataSet: TDataSet);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
    nColumn : integer;
    iCursor : Integer;
    ProdutoImportado : boolean;
    procedure GeraGrid_ItemForecast(DataInicial,DataFinal : String ; nCdForecast : Integer; Metodo : String);
  public
    { Public declarations }
  end;

var
  frmForecast: TfrmForecast;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmForecast.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'FORECAST' ;
  nCdTabelaSistema  := 317 ;
  nCdConsultaPadrao := 746 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmForecast.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if ((DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProduto') and (qryForecast.State <> dsBrowse)) then
  begin

      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nPK').Value := qryForecast.FieldByName('nCdProduto').Value;
      qryProduto.Open;

      qryForecast.FieldByName('cDescricao').Value      := qryProdutocNmProduto.Value;
      qryForecast.FieldByName('cUM').Value             := qryProdutocUnidadeMedida.Value;
      qryForecast.FieldByName('nQtdeLoteMinimo').Value := qryProdutonQtdeMinimaCompra.Value;

  end;

end;

procedure TfrmForecast.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdProduto') then
        begin

            if (qryForecast.State = dsBrowse) then
                qryForecast.Edit;

            if (qryForecast.State = dsInsert) or (qryForecast.State = dsEdit) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(176); 

                If (nPK > 0) then
                begin
                    qryForecast.FieldByName('nCdProduto').Value := nPK ;
                end ;

            end ;

        end ;

      end ;
  end;

end;

procedure TfrmForecast.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryMaster.Post;

end;

procedure TfrmForecast.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit4.Text) = '/  /') then
  begin
      MensagemAlerta('Insira a data inicial do Per�odo de Produ��o.');
      DBEdit4.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit5.Text) = '/  /') then
  begin
      MensagemAlerta('Insira a data final do Per�odo de Produ��o.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (frmMenu.ConvData(DBEdit4.Text) > frmMenu.ConvData(DBEdit5.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final');
      DBEdit4.SetFocus;
      Abort;
  end;

  if ((qryMaster.Active) and (qryMaster.State = dsInsert)) then
  begin
      if (MessageDlg('Ap�s salvar o per�odo n�o poder� ser alterado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      begin
          DBEdit4.SetFocus;
          abort;
      end;
  end ;

  qryMasterdDtUltAlt.Value        := Now();
  qryMasternCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioUltAlt.AsString);

  if (qryMaster.State = dsEdit) then
  begin
      cxButton3.Click;
  end;

  inherited;
end;

procedure TfrmForecast.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);

  qryUsuario.Close;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioUltAlt.AsString);

  if (qryMaster.State = dsBrowse) then
      GeraGrid_ItemForecast (qryMasterdDtInicial.AsString,qryMasterdDtFinal.AsString,qryMasternCdForecast.Value,'INSERT');

  if (qryMasterdDtUltAlt.AsString <> '') then
  begin
      DBEdit4.ReadOnly   := True;
      DBEdit4.Color      := $00E9E4E4;
      DBEdit4.Font.Color := clBlack ;
      DBEdit4.TabStop    := False ;
      DBEdit4.Update;

      DBEdit5.ReadOnly   := True;
      DBEdit5.Color      := $00E9E4E4;
      DBEdit5.Font.Color := clBlack ;
      DBEdit5.TabStop    := False ;
      DBEdit5.Update;
  end
  else
  begin

      DBEdit4.ReadOnly   := False;
      DBEdit4.Color      := clWhite;
      DBEdit4.TabStop    := True ;
      DBEdit4.Update;

      DBEdit5.ReadOnly   := False;
      DBEdit5.Color      := clWhite;
      DBEdit5.TabStop    := True ;
      DBEdit5.Update;

  end;

end;

procedure TfrmForecast.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  GeraGrid_ItemForecast (DBEdit4.Text,DBEdit5.Text,qryMasternCdForecast.Value,'INSERT') ;

end;

procedure TfrmForecast.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  PosicionaQuery(qryEmpresa,DBEdit3.Text);

  DBEdit4.SetFocus;
end;

procedure TfrmForecast.qryMasterBeforeClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuario.Close;
  qryEmpresa.Close;
  qryForecast.Close;
  DBGridEH1.Columns.Clear;
end;

procedure TfrmForecast.GeraGrid_ItemForecast(DataInicial, DataFinal: String; nCdForecast : Integer ; Metodo : String);
var
  i             : integer;
  cHead,cSemana : String;
begin
  inherited;

  cmdPreparaTemp.Execute;

  {-- aqui gera as colunas que ser�o apresentadas no grid --}

  try
      qryForecast.Close;
      qryForecast.Parameters.ParamByName('dDtInicial').Value  := DataInicial;
      qryForecast.Parameters.ParamByName('dDtFinal').Value    := DataFinal;
      qryForecast.Parameters.ParamByName('nCdForecast').Value := nCdForecast;
      qryForecast.Parameters.ParamByName('cMetodo').Value     := UpperCase(Metodo);
      qryForecast.Open;
  except
      MensagemErro('Erro no Processamento');
      raise;
  end;

  qryForecast.DisableControls;
  qryForecast.FieldList.Update;

  {-- adiciona as colunas no grid --}
  DBGridEh1.Columns.AddAllColumns(true);

  {-- tratamento das colunas --}
  DBGridEh1.Columns[0].Visible          := False;

  DBGridEh1.Columns[1].Title.Caption    := 'Produto|C�d.';
  DBGridEh1.Columns[1].Width            := 74;
  DBGridEh1.FrozenCols                  := 5;

  DBGridEh1.Columns[2].Title.Caption    := 'Produto|Descri��o';
  DBGridEh1.Columns[2].Width            := 338;
  DBGridEh1.Columns[2].ReadOnly         := True;
  DBGridEh1.Columns[2].Title.Font.Color := clRed;
  DBGridEh1.Columns[2].Font.Color       := clTeal;

  DBGridEh1.Columns[3].Title.Caption    := 'Produto|U.M';
  DBGridEh1.Columns[3].Width            := 48;
  DBGridEh1.Columns[3].ReadOnly         := True;
  DBGridEh1.Columns[3].Title.Font.Color := clRed;
  DBGridEh1.Columns[3].Font.Color       := clTeal;

  DBGridEh1.Columns[4].Title.Caption    := 'Produto|Lote M�nimo';
  DBGridEh1.Columns[4].Width            := 73;
  DBGridEh1.Columns[4].ReadOnly         := True;
  DBGridEh1.Columns[4].Title.Font.Color := clRed;
  DBGridEh1.Columns[4].Font.Color       := clTeal;

  nColumn := DBGridEh1.Columns.Count - 1;

  for i := 5 to nColumn do
  begin

      cHead := DBGridEh1.Columns[i].Title.Caption;
      cHead := Copy(cHead,15,Length(cHead)-14);

      cHead   := StringReplace(cHead,'_','/',[rfReplaceAll]);
      cSemana := Copy(FormatDateTime('DDDD',StrToDate(cHead)),1,3);
      cHead   := cSemana + ' ' + FormatDateTime('dd/mm/yyyy',StrToDate(cHead));

      DBGridEh1.Columns[i].Title.Caption := cHead;
      DBGridEh1.Columns[i].Width         := 73;

      if ((cSemana = 's�b') or (cSemana = 'dom')) then
          DBGridEh1.Columns[i].Color := clSilver;


  end;

  qryForecast.EnableControls;

  DBGridEh1.SetFocus;
end;

procedure TfrmForecast.qryForecastBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  qryExcluiItem.Close;
  qryExcluiItem.Parameters.ParamByName('nCdForecast').Value     := qryMasternCdForecast.Value;
  qryExcluiItem.Parameters.ParamByName('nCdProduto').Value := qryForecast.FieldValues['nCdProduto'];
  qryExcluiItem.ExecSQL;

end;

procedure TfrmForecast.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  iCursor       := Screen.Cursor ;
  Screen.Cursor := crSQLWait ;

  frmMenu.StatusBar1.Panels.Items[6].Text := 'Exportando planilha...';
end;

procedure TfrmForecast.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  frmMenu.StatusBar1.Panels.Items[6].Text := '';
  
  Screen.Cursor := iCursor ;

  ShowMessage('Forecast Exportado com sucesso.');
end;

procedure TfrmForecast.cxButton1Click(Sender: TObject);
var
  i, n : integer;
  head : string;
begin
  inherited;

  if not (qryMaster.Active) then
      abort;

  n := 10;

  {-- mescla as celulas --}
  ER2Excel1.Celula['B2'].Mesclar('C2');
  ER2Excel1.Celula['B3'].Mesclar('C3');
  ER2Excel1.Celula['B4'].Mesclar('C4');
  ER2Excel1.Celula['B5'].Mesclar('C5');
  ER2Excel1.Celula['B6'].Mesclar('C6');
  ER2Excel1.Celula['D3'].Mesclar('L3');
  ER2Excel1.Celula['D4'].Mesclar('L4');
  ER2Excel1.Celula['D5'].Mesclar('L5');
  ER2Excel1.Celula['D6'].Mesclar('L6');

  {-- defino os textos do cabe�alho da planilha --}
  ER2Excel1.Celula['B2'].Text := 'C�digo:';
  ER2Excel1.Celula['B3'].Text := 'Empresa:';
  ER2Excel1.Celula['B4'].Text := 'Per�odo de Produ��o:';
  ER2Excel1.Celula['B5'].Text := 'Descri��o Forecast:';
  ER2Excel1.Celula['B6'].Text := 'Documento:';

  ER2Excel1.Celula['B9'].Text := 'C�d.';
  ER2Excel1.Celula['C9'].Text := 'Descri��o';
  ER2Excel1.Celula['K9'].Text := 'U.M';
  ER2Excel1.Celula['L9'].Text := 'Lote M�nimo';


  ER2Excel1.Celula['D2'].Text := qryMasternCdForecast.Value;
  ER2Excel1.Celula['D3'].Text := qryMasternCdEmpresa.AsString + ' - ' + DBEdit10.Text;
  ER2Excel1.Celula['D4'].Text := qryMasterdDtInicial.AsString + ' � ' + qryMasterdDtFinal.AsString;
  ER2Excel1.Celula['D5'].Text := qryMastercNmForecast.Value;
  ER2Excel1.Celula['D6'].Text := qryMastercDocumento.Value;


  {-- formata as celulas --}
  ER2Excel1.Celula['B2'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B3'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B4'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B5'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B6'].HorizontalAlign := haRight;
  ER2Excel1.Celula['B9'].HorizontalAlign := haRight;
  ER2Excel1.Celula['K9'].HorizontalAlign := haCenter;
  
  ER2Excel1.Celula['B2'].Negrito;
  ER2Excel1.Celula['B3'].Negrito;
  ER2Excel1.Celula['B4'].Negrito;
  ER2Excel1.Celula['B5'].Negrito;
  ER2Excel1.Celula['B6'].Negrito;
  ER2Excel1.Celula['B9'].Negrito;
  ER2Excel1.Celula['C9'].Negrito;
  ER2Excel1.Celula['K9'].Negrito;
  ER2Excel1.Celula['L9'].Negrito;

  ER2Excel1.Celula['B9'].Border       := [EdgeBottom];
  ER2Excel1.Celula['B9'].BorderWeight := xlMedium;
  ER2Excel1.Celula['B9'].Range('L9');

  {-- aqui insere os registros --}

  qryForecast.First;

  while not qryForecast.Eof do
  begin

      for i := 1 to nColumn do
      begin

          if (i > 4) then
          begin
              Head := Copy(DBGridEh1.Columns[i].Title.Caption,1,9);

              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Text            := Head;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Border          := [EdgeBottom];
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].BorderWeight    := xlMedium;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '1'].Width           := 21;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + '9'].Negrito;

              if ((Copy(Head,1,3) = 's�b') or (Copy(Head,1,3) = 'dom')) then
                  ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Background := clSilver;

          end;

          if (i = 1) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(2) + intToStr(n)].HorizontalAlign := haRight;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(2) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
          end;

          if (i = 2) then
          begin
              ER2Excel1.Celula['C' + intToStr(n)].Mesclar('J' + intToStr(n));
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(3) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
          end;

          if (i > 3) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].HorizontalAlign := haRight;
          end;

          if (i = 3) then
          begin
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].Text := DBGridEh1.Columns[i].Field.Value;
              ER2Excel1.Celula[ER2Excel1.RetornaEndereco(i+8) + intToStr(n)].HorizontalAlign := haCenter;
          end;

      end;

      n := n + 1; 
      qryForecast.Next;

  end;

  ER2Excel1.Celula['B1'].Width := 10;
  ER2Excel1.Celula['K1'].Width := 5;
  ER2Excel1.Celula['A1'].Width := 2;
  ER2Excel1.Celula['L1'].Width := 12;

  {-- gera planilha --}
  ER2Excel1.ExportXLS;

end;

procedure TfrmForecast.cxButton2Click(Sender: TObject);
var
  nPK : integer;
begin
  inherited;

  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta(746);

      If (nPK > 0) then
      begin

          if (MessageDlg('Ao realizar a importa��o os produtos digitados ser�o removidos. Confirma a importa��o ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
              abort;

          ProdutoImportado := True;

          GeraGrid_ItemForecast (qryMasterdDtInicial.AsString,qryMasterdDtFinal.AsString,nPK,'IMPORT');

          ShowMessage('Produtos Importados com sucesso');

      end ;

  end ;

end;

procedure TfrmForecast.cxButton3Click(Sender: TObject);
var
  i : integer;
  cData : String;
begin
  inherited;

  if (MessageDlg('Confirma o Processamento dos Itens ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  qryForecast.First;

  frmMenu.Connection.BeginTrans;

  if(ProdutoImportado) then
  begin
      qryExcluiItem.Close;
      qryExcluiItem.Parameters.ParamByName('nCdForecast').Value := qryMasternCdForecast.Value;
      qryExcluiItem.Parameters.ParamByName('nCdProduto').Value  := 0;
      qryExcluiItem.ExecSQL;
  end;

  while not qryForecast.Eof do
  begin

      for i := 5 to nColumn do
      begin

          cData := Copy(DBGridEh1.Columns[i].Title.Caption,5,10);

          try
              qryInsereItem.Close;
              qryInsereItem.Parameters.ParamByName('nCdForecast').Value    := qryMasternCdForecast.Value;
              qryInsereItem.Parameters.ParamByName('nCdProduto').Value     := qryForecast.FieldValues['nCdProduto'];
              qryInsereItem.Parameters.ParamByName('dDtForecast').Value    := cData;
              qryInsereItem.Parameters.ParamByName('nQtdePrevista').Value  := DBGridEH1.Columns[i].Field.Value;
              qryInsereItem.ExecSQL;
          except
              MensagemErro('Erro no processamento.');
              frmMenu.Connection.RollbackTrans;
              raise
          end;
      end;

      qryForecast.Next;
  end;

  frmMenu.Connection.CommitTrans;

  ProdutoImportado := False;

  ShowMessage('Itens Processados com Sucesso.');
end;

initialization
    RegisterClass(TfrmForecast);
    
end.
