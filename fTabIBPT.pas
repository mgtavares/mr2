unit fTabIBPT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmTabIBPT = class(TfrmCadastro_Padrao)
    qryMasternCdTabIBPT: TIntegerField;
    qryMastercNCM: TStringField;
    qryMasteriTabela: TIntegerField;
    qryMastercDescricao: TStringField;
    qryMasternAliqNacional: TBCDField;
    qryMasternAliqImportado: TBCDField;
    qryMastercFlgManual: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    qryMasternAliqEstadual: TBCDField;
    qryMasternAliqMunicipal: TBCDField;
    qryMastercVersao: TStringField;
    qryMasterdDtValidInicial: TDateTimeField;
    qryMasterdDtValidFinal: TDateTimeField;
    qryMastercChave: TStringField;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    Label10: TLabel;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    Label12: TLabel;
    qryEstado: TADOQuery;
    qryEstadocUF: TStringField;
    qryEstadocNmEstado: TStringField;
    qryEstadonCdEstado: TIntegerField;
    dsEstado: TDataSource;
    qryMasternCdEstado: TIntegerField;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit13: TDBEdit;
    qryAux: TADOQuery;
    DBRadioGroup2: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit13Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabIBPT: TfrmTabIBPT;

implementation

{$R *.dfm}
uses
  fMenu, fLookup_Padrao;

procedure TfrmTabIBPT.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TABIBPT' ;
  nCdTabelaSistema  := 516 ;
  nCdConsultaPadrao := 767 ;
  bLimpaAposSalvar  := false;
end;

procedure TfrmTabIBPT.qryMasterBeforePost(DataSet: TDataSet);
begin
  if ((qryMaster.State = dsEdit) and (qryMastercFlgManual.Value = 0)) then
  begin
      MensagemAlerta('Al�quotas importadas n�o podem ser alteradas.');
      Abort;
  end;

  if (Trim(qryMastercNCM.Value) = '') then
  begin
      MensagemAlerta('Informe o NCM.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (qryMasternCdEstado.Value = 0) then
  begin
      MensagemAlerta('Informe o Estado.');
      DBEdit13.SetFocus;
      Abort;
  end;

  if (Trim(qryMastercDescricao.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o da al�quota.');
      DBEdit3.SetFocus;
      Abort;
  end;

  inherited;

  { -- inclus�o manual -- }
  if (qryMaster.State = dsInsert) then
      qryMastercFlgManual.Value := 1;
end;

procedure TfrmTabIBPT.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit3.SetFocus;

  qryMasternAliqNacional.Value  := 0;
  qryMasternAliqImportado.Value := 0;
  qryMasternAliqEstadual.Value  := 0;
  qryMasternAliqMunicipal.Value := 0;
  DBRadioGroup1.ItemIndex       := 0;
  
end;

procedure TfrmTabIBPT.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryEstado, qryMasternCdEstado.AsString);

end;

procedure TfrmTabIBPT.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(204);

            If (nPK > 0) then
            begin
                qryMasternCdEstado.Value := nPK ;
                DBEdit3.SetFocus;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabIBPT.DBEdit13Exit(Sender: TObject);
begin
  inherited;
  if not qryMaster.Active then
      Exit;

  PosicionaQuery(qryEstado, qryMasternCdEstado.AsString);

end;

procedure TfrmTabIBPT.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryEstado.Close;
end;

procedure TfrmTabIBPT.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryAux.Close;
  qryAux.Parameters.ParamByName('nPK').Value := qryMasternCdTabIBPT.Value;
  qryAux.ExecSQL;

  PosicionaPK(qryMasternCdTabIBPT.Value);
end;

initialization
    RegisterClass(TfrmTabIBPT);
end.
