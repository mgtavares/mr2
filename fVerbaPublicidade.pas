unit fVerbaPublicidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmVerbaPublicidade = class(TfrmCadastro_Padrao)
    qryMasternCdTitulo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMasternValLiq: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMastercObsTit: TMemoField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit11: TDBEdit;
    DataSource1: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit12: TDBEdit;
    DataSource2: TDataSource;
    qryMTitulo: TADOQuery;
    qryMTituloMovimentoID: TAutoIncField;
    qryMTituloOperaoCdigo: TIntegerField;
    qryMTituloOperaoDescrio: TStringField;
    qryMTituloFormaMovimentoCdigo: TIntegerField;
    qryMTituloFormaMovimentoDescrio: TStringField;
    qryMTituloMovimentoNrCheque: TIntegerField;
    qryMTituloMovimentoValor: TBCDField;
    qryMTituloMovimentoData: TDateTimeField;
    qryMTituloMovimentoCadastro: TDateTimeField;
    qryMTituloMovimentoCancel: TDateTimeField;
    qryMTituloMovimentoNrDoc: TStringField;
    qryMTituloMovimentoContab: TDateTimeField;
    qryMTituloUsurioMovimento: TStringField;
    qryMTituloUsurioCancelamento: TStringField;
    qryMTituloOperaoConta: TStringField;
    dsMovimento: TDataSource;
    qryMTitulocOBSMov: TMemoField;
    qryMastercNrTit: TStringField;
    Label12: TLabel;
    DBEdit13: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBMemo2: TDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdEspTit : integer ;
  end;

var
  frmVerbaPublicidade: TfrmVerbaPublicidade;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmVerbaPublicidade.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TITULO' ;
  nCdTabelaSistema  := 21 ;
  nCdConsultaPadrao := 162 ;

end;

procedure TfrmVerbaPublicidade.FormActivate(Sender: TObject);
begin
  inherited;

  try
      nCdEspTit := StrToInt(frmMenu.LeParametro('CDESPTIT-VP')) ;
  except
      MensagemErro('Parametro CDESPTIT-VP n�o definido ou incorreto.') ;
      close ;
  end ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryMaster.Parameters.ParamByName('nCdEspTit').Value  := nCdEspTit ;

  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmVerbaPublicidade.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit2.Text) ;
  
end;

procedure TfrmVerbaPublicidade.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, DBEdit6.Text) ;
  
end;

procedure TfrmVerbaPublicidade.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTerceiro.Close ;
  qryUnidadeNegocio.Close ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.AsString) ;
  PosicionaQuery(qryMTitulo, qryMasternCdTitulo.AsString) ;
  
end;

procedure TfrmVerbaPublicidade.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DBEdit11.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (Trim(qryMasterdDtEmissao.AsString) = '/  /') then
  begin
      MensagemAlerta('Informe a data de emiss�o.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (trim(qryMasterdDtVenc.AsString) = '/  /') then
  begin
      MensagemAlerta('Informe a data de vencimento.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (dbEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio.') ;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternValTit.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da verba.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  qryMasternCdEspTit.Value  := nCdEspTit ;
  qryMasteriParcela.Value   := 1 ;
  qryMasternCdMoeda.Value   := 1 ;
  qryMastercSenso.Value     := 'C' ;

  if (qryMaster.State = dsInsert) then
      qryMasterdDtCad.Value := Now() ;

  qryMasternSaldoTit.Value  := qryMasternValTit.Value - qryMasternValLiq.Value - qryMasternValDesconto.Value ;

  if (qryMasternSaldoTit.Value < 0) then
      qryMasternSaldoTit.Value := 0 ;



end;

procedure TfrmVerbaPublicidade.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEDit2.SetFocus ;
  
end;

procedure TfrmVerbaPublicidade.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryTerceiro.Close ;
  qryUnidadeNegocio.Close ;
  qryMTitulo.Close ;
  
end;

procedure TfrmVerbaPublicidade.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17) ;

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmVerbaPublicidade.DBEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva) + ')');

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmVerbaPublicidade) ;
    
end.
