inherited frmConsultaLanctoConta: TfrmConsultaLanctoConta
  Left = -8
  Top = -8
  Width = 1152
  Height = 864
  Caption = 'Consulta Lan'#231'amento Conta'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 153
    Width = 1136
    Height = 675
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 115
    inherited ToolButton1: TToolButton
      Caption = 'Executar Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
    object ToolButton4: TToolButton
      Left = 238
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 246
      Top = 0
      Caption = 'Imprimir'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 124
    Align = alTop
    Caption = 'Filtros'
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 1
    object Label3: TLabel
      Tag = 1
      Left = 40
      Top = 44
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Lan'#231'amentos'
    end
    object Label1: TLabel
      Tag = 1
      Left = 7
      Top = 20
      Width = 135
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Banc'#225'ria/Caixa/Cofre'
    end
    object Label2: TLabel
      Tag = 1
      Left = 232
      Top = 44
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 61
      Top = 68
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo Lan'#231'amento'
    end
    object Label5: TLabel
      Tag = 1
      Left = 65
      Top = 92
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero Cheque'
    end
    object Label6: TLabel
      Tag = 1
      Left = 220
      Top = 92
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = '(Cheque emitido)'
    end
    object MaskEdit2: TMaskEdit
      Left = 256
      Top = 36
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object MaskEdit1: TMaskEdit
      Left = 152
      Top = 36
      Width = 71
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Left = 152
      Top = 12
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object MaskEdit4: TMaskEdit
      Left = 152
      Top = 60
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 3
      Text = '      '
      OnExit = MaskEdit4Exit
      OnKeyDown = MaskEdit4KeyDown
    end
    object MaskEdit5: TMaskEdit
      Left = 152
      Top = 84
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 4
      Text = '      '
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 224
      Top = 60
      Width = 321
      Height = 21
      DataField = 'cNmTipoLancto'
      DataSource = DataSource2
      TabOrder = 5
    end
    object RadioGroup1: TRadioGroup
      Left = 568
      Top = 16
      Width = 249
      Height = 41
      Caption = 'Situa'#231#227'o Lan'#231'amento'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o Conciliados'
        'Conciliados')
      TabOrder = 6
    end
    object RadioGroup2: TRadioGroup
      Left = 568
      Top = 64
      Width = 249
      Height = 41
      Caption = 'Somente Lan'#231'amento Manual'
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Sim'
        'N'#227'o')
      TabOrder = 7
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 224
      Top = 12
      Width = 321
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource1
      TabOrder = 8
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 153
    Width = 1136
    Height = 675
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsResultado
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    ShowHint = True
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdLanctoFin'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 98
      end
      item
        EditButtons = <>
        FieldName = 'dDtLancto'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoLancto'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 312
      end
      item
        EditButtons = <>
        FieldName = 'nValCredito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clBlack
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 119
      end
      item
        EditButtons = <>
        FieldName = 'nValDebito'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clBlack
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 119
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'cDocumento'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'cFlgManual'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 48
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 696
    Top = 56
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC00515757006564640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C003E003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0070000
      00008001C007000000008001C003000000018001C003000000038001C0030000
      0077C003E0030000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryContaBancariaDeb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END nCdConta'
      '      ,cFlgCaixa'
      '      ,cFlgCofre'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND ((ContaBancaria.nCdLoja IS NULL) OR EXISTS(SELECT 1'
      
        '                                                    FROM Usuario' +
        'Loja UL'
      
        '                                                   WHERE UL.nCdL' +
        'oja    = ContaBancaria.nCdLoja'
      
        '                                                     AND UL.nCdU' +
        'suario = :nCdUsuario))'
      '   AND nCdEmpresa       = :nCdEmpresa')
    Left = 324
    Top = 200
    object qryContaBancariaDebnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDebnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 50
    end
    object qryContaBancariaDebcFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryContaBancariaDebcFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'cDataInicial'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 10
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cDataFinal'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 10
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdTipoLancto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'iNrCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'cFlgConciliado'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 1
        Size = 1
        Value = 'N'
      end
      item
        Name = 'cFlgManual'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 1
        Size = 1
        Value = 'N'
      end>
    SQL.Strings = (
      'EXEC SP_CONSULTA_LANCTOFIN :nCdContaBancaria'
      '                          ,:cDataInicial'
      '                          ,:cDataFinal'
      '                          ,:nCdTipoLancto'
      '                          ,:iNrCheque'
      '                          ,:cFlgConciliado'
      '                          ,:cFlgManual')
    Left = 464
    Top = 328
    object qryResultadonCdLanctoFin: TAutoIncField
      DisplayLabel = 'Lancto'
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryResultadodDtLancto: TDateTimeField
      DisplayLabel = 'Data Lancto'
      FieldName = 'dDtLancto'
      ReadOnly = True
    end
    object qryResultadonCdTipoLancto: TIntegerField
      DisplayLabel = 'Tipo Lan'#231'amento|C'#243'd'
      FieldName = 'nCdTipoLancto'
    end
    object qryResultadocNmTipoLancto: TStringField
      DisplayLabel = 'Hist'#243'rico'
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryResultadonValCredito: TBCDField
      DisplayLabel = 'Valor|Cr'#233'dito'
      FieldName = 'nValCredito'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object qryResultadonValDebito: TBCDField
      DisplayLabel = 'Valor|D'#233'bito'
      FieldName = 'nValDebito'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object qryResultadonCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryResultadocNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio|Nome'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryResultadocFlgManual: TStringField
      DisplayLabel = 'Manual'
      FieldName = 'cFlgManual'
      ReadOnly = True
      Size = 3
    end
    object qryResultadodDtEstorno: TDateTimeField
      DisplayLabel = 'Dados do Estorno|Data'
      FieldName = 'dDtEstorno'
    end
    object qryResultadocMotivoEstorno: TStringField
      DisplayLabel = 'Dados do Estorno|Motivo'
      FieldName = 'cMotivoEstorno'
      Size = 30
    end
    object qryResultadocHistorico: TStringField
      DisplayLabel = 'Hist'#243'rico Lan'#231'amento'
      FieldName = 'cHistorico'
      Size = 50
    end
    object qryResultadocDocumento: TStringField
      DisplayLabel = 'Documento'
      FieldName = 'cDocumento'
      Size = 15
    end
    object qryResultadonCdProvisaoTit: TIntegerField
      DisplayLabel = 'N'#250'm. Provis'#227'o'
      FieldName = 'nCdProvisaoTit'
    end
    object qryResultadoiNrCheque: TIntegerField
      DisplayLabel = 'N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 496
    Top = 336
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancariaDeb
    Left = 632
    Top = 376
  end
  object qryTipoLancto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoLancto, cNmTipoLancto'
      'FROM TipoLancto'
      'WHERE nCdTipoLancto = :nPK')
    Left = 560
    Top = 360
    object qryTipoLanctonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryTipoLanctocNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTipoLancto
    Left = 640
    Top = 384
  end
end
