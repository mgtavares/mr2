unit fLimiteCredCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmLimiteCredCliente = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMastercNmFantasia: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasterdMaiorRecAtraso: TDateTimeField;
    qryMasternValLimiteCred: TBCDField;
    qryMastercFlgBloqPedVenda: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLimiteCredCliente: TfrmLimiteCredCliente;

implementation

{$R *.dfm}

procedure TfrmLimiteCredCliente.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TERCEIRO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 80 ;

end;

initialization
    RegisterClass(TfrmLimiteCredCliente) ;
    
end.
