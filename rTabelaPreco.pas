unit rTabelaPreco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QuickRpt, QRCtrls, ExtCtrls;

type
  TrelTabelaPreco = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryTabPreco: TADOQuery;
    qryProdutoTabPreco: TADOQuery;
    qryTabPrecocNmTabela: TStringField;
    qryTabPrecocNmEmpresa: TStringField;
    qryTabPrecocNmLoja: TStringField;
    qryTabPrecocNmTerceiro: TStringField;
    qryTabPrecocDtValidadeIni: TStringField;
    qryTabPrecocDtValidadeFim: TStringField;
    QRBand2: TQRBand;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    qryProdutoTabPreconCdProduto: TIntegerField;
    qryProdutoTabPrecocNmProduto: TStringField;
    qryProdutoTabPreconValor: TBCDField;
    QRLabel5: TQRLabel;
    qryTabPrecocNmTipoTabPreco: TStringField;
    QRDBText4: TQRDBText;
    QRLabel7: TQRLabel;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape3: TQRShape;
    procedure exibeRel(nPK : integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  relTabelaPreco: TrelTabelaPreco;

implementation

uses
    fMenu;
{$R *.dfm}

{ TrelTabelaPreco }

procedure TrelTabelaPreco.exibeRel(nPK: integer);
begin
    qryTabPreco.Close;
    qryTabPreco.Parameters.ParamByName('nPK').Value := nPK;
    qryTabPreco.Open;

    qryProdutoTabPreco.Close;
    qryProdutoTabPreco.Parameters.ParamByName('nPK').Value := nPK;
    qryProdutoTabPreco.Open;

    lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

    QuickRep1.PreviewModal;

    Close;
end;

end.
