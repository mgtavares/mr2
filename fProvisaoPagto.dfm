inherited frmProvisaoPagto: TfrmProvisaoPagto
  Left = 0
  Top = 0
  Width = 1152
  Height = 780
  Caption = 'Provis'#227'o de Pagamento'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1144
    Height = 720
  end
  object Label1: TLabel [1]
    Left = 23
    Top = 168
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data do Pagamento'
  end
  object Label2: TLabel [2]
    Left = 9
    Top = 72
    Width = 109
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Vencimento'
  end
  object Label3: TLabel [3]
    Left = 209
    Top = 72
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [4]
    Left = 66
    Top = 120
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero SP'
  end
  object Label5: TLabel [5]
    Left = 37
    Top = 96
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero do T'#237'tulo'
  end
  object Label6: TLabel [6]
    Left = 43
    Top = 144
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Credor'
  end
  object Label7: TLabel [7]
    Left = 77
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  inherited ToolBar1: TToolBar
    Width = 1144
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object edtDtPagto: TDateTimePicker [9]
    Left = 121
    Top = 160
    Width = 97
    Height = 21
    Date = 39868.000000000000000000
    Time = 39868.000000000000000000
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object edtDtVencIni: TMaskEdit [10]
    Left = 121
    Top = 64
    Width = 73
    Height = 21
    EditMask = '##/##/####;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object edtDtVencFim: TMaskEdit [11]
    Left = 233
    Top = 64
    Width = 81
    Height = 21
    EditMask = '##/##/####;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtNrSP: TMaskEdit [12]
    Left = 121
    Top = 112
    Width = 73
    Height = 21
    EditMask = '##########;0; '
    MaxLength = 10
    TabOrder = 4
  end
  object edtNrTit: TMaskEdit [13]
    Left = 121
    Top = 88
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
  end
  object edtnCdTerceiro: TMaskEdit [14]
    Left = 121
    Top = 136
    Width = 73
    Height = 21
    EditMask = '##########;0; '
    MaxLength = 10
    TabOrder = 5
    OnExit = edtnCdTerceiroExit
    OnKeyDown = edtnCdTerceiroKeyDown
  end
  object DBEdit1: TDBEdit [15]
    Tag = 1
    Left = 201
    Top = 136
    Width = 393
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 6
  end
  object cxButton1: TcxButton [16]
    Left = 8
    Top = 192
    Width = 105
    Height = 33
    Caption = 'Exibir T'#237'tulos'
    TabOrder = 8
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit3: TDBEdit [17]
    Tag = 1
    Left = 193
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource2
    TabOrder = 9
  end
  object edtEmpresa: TER2LookupMaskEdit [18]
    Left = 121
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  inherited ImageList1: TImageList
    Top = 40
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 352
    Top = 160
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 416
    Top = 152
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 544
    Top = 216
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryEmpresa
    Left = 560
    Top = 408
  end
end
