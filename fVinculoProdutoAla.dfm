inherited frmVinculoProdutoAla: TfrmVinculoProdutoAla
  Left = -8
  Top = -8
  Width = 1296
  Height = 786
  Caption = 'frmVinculoProdutoAla'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1280
    Height = 721
  end
  inherited ToolBar1: TToolBar
    Width = 1280
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 889
    Height = 721
    AllowedOperations = [alopUpdateEh]
    DataSource = dsProdutos
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 467
      end
      item
        EditButtons = <>
        FieldName = 'nCdAlaProducao'
        Footers = <>
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'cNmAlaProducao'
        Footers = <>
        ReadOnly = True
        Width = 275
      end>
  end
  object qryProdutos: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryProdutosCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,nCdAlaProducao'
      '  FROM Produto'
      ' WHERE nCdProdutoPai IS NULL'
      '   AND cFlgProdVenda = 1'
      ' ORDER BY cNmProduto')
    Left = 184
    Top = 88
    object qryProdutosnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryProdutoscNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutosnCdAlaProducao: TIntegerField
      DisplayLabel = 'Ala Produ'#231#227'o|C'#243'd'
      FieldName = 'nCdAlaProducao'
    end
    object qryProdutoscNmAlaProducao: TStringField
      DisplayLabel = 'Ala Produ'#231#227'o|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmAlaProducao'
      Size = 50
      Calculated = True
    end
  end
  object dsProdutos: TDataSource
    DataSet = qryProdutos
    Left = 224
    Top = 80
  end
  object qryAlaProducao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmAlaProducao'
      ' FROM AlaProducao'
      'WHERE nCdAlaProducao = :nPK')
    Left = 416
    Top = 200
    object qryAlaProducaocNmAlaProducao: TStringField
      FieldName = 'cNmAlaProducao'
      Size = 50
    end
  end
end
