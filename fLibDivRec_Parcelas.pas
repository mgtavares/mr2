unit fLibDivRec_Parcelas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmLibDivRec_Parcelas = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    Panel1: TPanel;
    DBGridEh2: TDBGridEh;
    qryParcelasPrevistas: TADOQuery;
    qryParcelasPrevistasiParcela: TIntegerField;
    qryParcelasPrevistasdDtVenc: TDateTimeField;
    qryParcelasPrevistasiDias: TIntegerField;
    dsParcelasPrevistas: TDataSource;
    qryParcelasRealizadas: TADOQuery;
    qryParcelasRealizadasdDtVenc: TDateTimeField;
    qryParcelasRealizadasiQtdeDias: TIntegerField;
    dsParcelasRealizadas: TDataSource;
    DBGridEh3: TDBGridEh;
    qryPedidos: TADOQuery;
    dsPedidos: TDataSource;
    qryPedidosnCdPedido: TIntegerField;
    qryPedidoscNmCondPagto: TStringField;
    qryParcelasRealizadasnValParcela: TBCDField;
    procedure ExibeParcelas(nCdRecebimento : integer) ;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLibDivRec_Parcelas: TfrmLibDivRec_Parcelas;

implementation

{$R *.dfm}

procedure TfrmLibDivRec_Parcelas.ExibeParcelas(nCdRecebimento : integer) ;
begin

    qryParcelasPrevistas.Close ;
    qryParcelasPrevistas.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
    qryParcelasPrevistas.Open ;

    qryParcelasRealizadas.Close ;
    qryParcelasRealizadas.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
    qryParcelasRealizadas.Open ;

    Panel1.Caption := ' ' ;

    if (qryParcelasPrevistas.Parameters.ParamByName('iQtdePedidos').Value = 0) then
        Panel1.Caption := 'RECEBIMENTO SEM PEDIDO' ;

    if (qryParcelasPrevistas.Parameters.ParamByName('iQtdePedidos').Value > 1) then
        Panel1.Caption := 'RECEBIMENTO COM MAIS DE 1 PEDIDO COM CONDI��ES COMERCIAIS DIFERENTES' ;

    qryPedidos.Close ;
    qryPedidos.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
    qryPedidos.Open ;

    Self.ShowModal ;

end ;


end.
