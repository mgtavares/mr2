unit fGeraListaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, Mask, DBCtrls, ADODB;

type
  TfrmGeraListaCobranca = class(TfrmProcesso_Padrao)
    edtNmLista: TEdit;
    Label1: TLabel;
    qryLojaCobradora: TADOQuery;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    dsLojaCobradora: TDataSource;
    DBEdit2: TDBEdit;
    edtDtFinal: TMaskEdit;
    Label6: TLabel;
    edtDtInicial: TMaskEdit;
    Label4: TLabel;
    edtCliente: TMaskEdit;
    Label5: TLabel;
    edtDtCobranca: TMaskEdit;
    Label7: TLabel;
    GroupBox1: TGroupBox;
    chkCobrancaTelefonica: TCheckBox;
    chkEnvioCarta: TCheckBox;
    chkNegativar: TCheckBox;
    chkCobradora: TCheckBox;
    chkAcordo: TCheckBox;
    chkIncluirNegativado: TCheckBox;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit3: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit4: TDBEdit;
    chkIncTitCobradora: TCheckBox;
    chkIncReagendamento: TCheckBox;
    SP_GERA_LISTA_COBRANCA: TADOStoredProc;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    Label3: TLabel;
    dsEspTit: TDataSource;
    DBEdit6: TDBEdit;
    edtEspTit: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure chkNegativarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEspTitKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEspTitExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraListaCobranca: TfrmGeraListaCobranca;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmGeraListaCobranca.FormShow(Sender: TObject);
begin
  inherited;

  qryLojaCobradora.Close ;

  if (frmMenu.nCdLojaAtiva > 0) then
      PosicionaQuery(qryLojaCobradora, intToStr(frmMenu.nCdLojaAtiva)) ;

  edtCliente.SetFocus;
  
end;

procedure TfrmGeraListaCobranca.edtClienteExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtCliente.Text) ;

  
end;

procedure TfrmGeraListaCobranca.chkNegativarClick(Sender: TObject);
begin
  inherited;

  chkIncluirNegativado.Enabled := True ;

  if (chkNegativar.Checked) then
  begin

      chkNegativar.Checked := True ;

      chkIncluirNegativado.Checked := false ;
      chkIncluirNegativado.Enabled := false ;

  end ;

end;

procedure TfrmGeraListaCobranca.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtEspTit.Text) = '') then
  begin
      MensagemErro('Informe a esp�cie de titulo.') ;
      edtEspTit.SetFocus;
      abort ;
  end ;

  if (trim(edtDtInicial.Text) = '/  /') or (trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemErro('Informe o intervalo de vencimento dos t�tulos.') ;
      edtDtInicial.SetFocus;
      abort ;
  end ;

  if (frmMenu.ConvData(edtDtInicial.Text) > frmMenu.ConvData(edtDtFinal.Text)) then
  begin
      MensagemErro('Intervalo de vencimento dos t�tulos inv�lido.') ;
      edtDtInicial.SetFocus;
      abort ;
  end ;

  if (frmMenu.ConvData(edtDtCobranca.Text) < Date) then
  begin
      MensagemErro('A data da cobran�a n�o pode ser menor que hoje.') ;
      edtDtCobranca.SetFocus;
      abort ;
  end ;

  if (trim(edtNmLista.Text) = '') then
  begin
      MensagemErro('Informe o nome da lista de cobran�a.') ;
      edtNmLista.SetFocus;
      abort ;
  end ;

  if (MessageDlg('Confirma a gera��o desta lista de cobran�a ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdEmpresa').Value          := frmMenu.nCdEmpresaAtiva;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdLojaCobradora').Value    := frmMenu.ConvInteiro(DBEdit1.Text);
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdTerceiro').Value         := frmMenu.ConvInteiro(edtCliente.Text);
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdEtapaCobranca').Value    := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdEspTit').Value           := edtEspTit.Text;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@nCdCronogramaCobDia').Value := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@dDtVenctoIni').Value        := frmMenu.ConvData(edtDtInicial.Text);
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@dDtVenctoFim').Value        := frmMenu.ConvData(edtDtFinal.Text);
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@dDtCobranca').Value         := frmMenu.ConvData(edtDtCobranca.Text);
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cNmListaCobranca').Value    := edtNmLista.Text;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncNegativados').Value  := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncTitCobradora').Value := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncReagend').Value      := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgTelefone').Value        := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgCarta').Value           := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgNegativar').Value       := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgCobradora').Value       := 0;
      SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgAcordo').Value          := 0;

      if (chkIncluirNegativado.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncNegativados').Value  := 1;

      if (chkIncTitCobradora.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncTitCobradora').Value := 1;

      if (chkIncReagendamento.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgIncReagend').Value      := 1;


      if (chkCobrancaTelefonica.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgTelefone').Value        := 1;

      if (chkEnvioCarta.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgCarta').Value           := 1;

      if (chkNegativar.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgNegativar').Value       := 1;

      if (chkCobradora.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgCobradora').Value       := 1;

      if (chkAcordo.Checked) then
          SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@cFlgAcordo').Value          := 1;

      SP_GERA_LISTA_COBRANCA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  if (SP_GERA_LISTA_COBRANCA.Parameters.ParamByName('@iQtdeListaGerada').Value > 0) then
      ShowMessage('Lista Gerada com sucesso.')
  else MensagemErro('Nenhum t�tulo encontrado para o crit�rio utilizado. Nenhuma lista de cobran�a foi gerada.') ;

  qryTerceiro.Close;
  qryEspTit.Close;
  edtCliente.Text    := '' ;
  edtEspTit.Text     := '' ;
  edtDtInicial.Text  := '' ;
  edtDtFinal.Text    := '' ;
  edtDtCobranca.Text := '' ;
  edtNmLista.Text    := '' ;

  chkIncluirNegativado.Checked  := False ;
  chkIncTitCobradora.Checked    := False ;
  chkIncReagendamento.Checked   := False ;
  chkCobrancaTelefonica.Checked := False ;
  chkEnvioCarta.Checked         := False ;
  chkNegativar.Checked          := False ;
  chkCobradora.Checked          := False ;
  chkAcordo.Checked             := False ;

end;

procedure TfrmGeraListaCobranca.edtClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        If (nPK > 0) then
            edtCliente.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmGeraListaCobranca.edtEspTitKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cFlgCobranca = 1');

        If (nPK > 0) then
            edtEspTit.Text := intToStr(nPK) ;
            PosicionaQuery(qryEspTit, edtEspTit.Text) ;
    end ;

  end ;

end;

procedure TfrmGeraListaCobranca.edtEspTitExit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close;
  PosicionaQuery(qryEspTit, edtEspTit.Text) ;
end;

initialization
    RegisterClass(TfrmGeraListaCobranca) ;

end.
