unit fAtualizacaoIndice;

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB;

type
  TfrmAtualizacaoMonetaria = class(TForm)
    qryTituloFinanceiro: TADOQuery;
    qryCotacao: TADOQuery;
    qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField;
    qryCotacaonCdIndiceReajuste: TIntegerField;
    qryCotacaoiAnoReferencia: TIntegerField;
    qryCotacaoiMesReferencia: TIntegerField;
    qryCotacaonIndice: TBCDField;
    qryTituloFinanceironCdTitulo: TIntegerField;
    qryTituloFinanceironCdEspTit: TIntegerField;
    qryTituloFinanceirocNmEspTit: TStringField;
    qryTituloFinanceironCdSP: TIntegerField;
    qryTituloFinanceirocNrTit: TStringField;
    qryTituloFinanceiroiParcela: TIntegerField;
    qryTituloFinanceironCdTerceiro: TIntegerField;
    qryTituloFinanceirocNmTerceiro: TStringField;
    qryTituloFinanceirocCNPJCPF: TStringField;
    qryTituloFinanceirocRG: TStringField;
    qryTituloFinanceironCdMoeda: TIntegerField;
    qryTituloFinanceirocSenso: TStringField;
    qryTituloFinanceirodDtEmissao: TDateTimeField;
    qryTituloFinanceirodDtLiq: TDateTimeField;
    qryTituloFinanceirodDtVenc: TDateTimeField;
    qryTituloFinanceirocDtCotacao: TStringField;
    qryTituloFinanceironValTit: TBCDField;
    qryTituloFinanceironSaldoTit: TBCDField;
    qryTituloFinanceironValIndiceCorrecao: TBCDField;
    qryTituloFinanceironValCorrecao: TBCDField;
    qryTituloFinanceironValJuro: TBCDField;
    qryTituloFinanceironValMulta: TBCDField;
    qryTituloFinanceironvalDesconto: TBCDField;
    qryTituloFinanceironValAbatimento: TBCDField;
    qryTituloFinanceirodDtCancel: TDateTimeField;
    qryTituloFinanceironValLiq: TBCDField;
    qryTituloFinanceiroiParcelas: TIntegerField;
    qryTituloFinanceirocNrUnidade: TStringField;
    qryTituloFinanceirodDtContrato: TDateTimeField;
    qryTituloFinanceironCdIndiceReajuste: TIntegerField;
    qryTituloFinanceirocNmBlocoEmpImobiliario: TStringField;
    qryTituloFinanceirocNmEmpImobiliario: TStringField;
    qryTituloFinanceirocEndereco: TStringField;
    qryTituloFinanceirocNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryTituloFinanceirocNmIndiceReajuste: TStringField;
    qryJuros: TADOQuery;
    qryJurosnCdTabelaJuros: TAutoIncField;
    qryJurosnCdEmpresa: TIntegerField;
    qryJurosnCdEspTit: TIntegerField;
    qryJurosdDtValidade: TDateTimeField;
    qryJurosiDiasCarencia: TIntegerField;
    qryJurosnPercMulta: TBCDField;
    qryJurosnPercJuroDia: TBCDField;
    qryJurosnPercDescAntecipDia: TBCDField;
    qryJurosiDiasMinDescAntecip: TIntegerField;
    qryJurosnPercMaxDescAntecip: TBCDField;
    qryUltimaCotacao: TADOQuery;
    qryUltimaCotacaonCdCotacaoIndiceReajuste: TAutoIncField;
    qryUltimaCotacaonCdIndiceReajuste: TIntegerField;
    qryUltimaCotacaoiAnoReferencia: TIntegerField;
    qryUltimaCotacaoiMesReferencia: TIntegerField;
    qryUltimaCotacaonIndice: TBCDField;
    qryTituloParcelaAnterior: TADOQuery;
    qryTituloParcelaAnteriornCdTitulo: TIntegerField;
    qryTituloParcelaAnterioriparcela: TIntegerField;
    qryTituloParcelaAnteriornValCorrecao: TBCDField;
    qryTituloParcelaAnteriornCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryTituloParcelaAnteriorcNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryTituloFinanceironCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryParcelasPagas: TADOQuery;
    qryParcelasPagasnCdTitulo: TIntegerField;
    qryParcelasPagasiparcela: TIntegerField;
    qryParcelasPagasnValIndiceCorrecao: TBCDField;
    qryParcelasPagasnValCorrecao: TBCDField;
    qryParcelasPagasnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryParcelasPagascNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryPrimeiraParcela: TADOQuery;
    qryPrimeiraParcelanCdTitulo: TIntegerField;
    qryPrimeiraParcelaiparcela: TIntegerField;
    qryPrimeiraParcelanValCorrecao: TBCDField;
    qryPrimeiraParcelanCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryPrimeiraParcelacNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryUltimaParcelaPaga: TADOQuery;
    qryUltimaParcelaPaganCdTitulo: TIntegerField;
    qryUltimaParcelaPagaiparcela: TIntegerField;
    qryUltimaParcelaPaganValIndiceCorrecao: TBCDField;
    qryUltimaParcelaPaganValCorrecao: TBCDField;
    qryUltimaParcelaPaganCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryUltimaParcelaPagacNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryUltimaParcelaPaganSaldoTit: TBCDField;
    qryUltimaParcelaPaganValTit: TBCDField;
    qryCalculaJuros: TADOQuery;
    qryCalculaJurosnValJuro: TBCDField;
    qryCalculaMulta: TADOQuery;
    qryCalculaMultanValMulta: TBCDField;
    qryUltimaParcelaPagadDtLiq: TDateTimeField;
    function calculaMulta (nValorBaseCalc : Double): Double;
    function calculaDiasDecorrido (dDataInicial: TDateTime;dDataFinal : TDateTime): Integer;
    function atualizaIndiceParcela (dDtLiquidacao: TDateTime; nCdTitulo: Integer ; nValBaseCorrecao : Double): Double;
    function atualizaJurosParcelaAtraso (dDtLiquidacao: TDateTime; nCdTitulo: Integer; nValorCorrigido : Double): Double;

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAtualizacaoMonetaria: TfrmAtualizacaoMonetaria;

implementation

uses fMenu;

{$R *.dfm}

function TfrmAtualizacaoMonetaria.calculaMulta (nValorBaseCalc : Double): Double;
var
  cPercMulta : String;
begin

    cPercMulta := frmMenu.LeParametro('MULTAFINIMOB');
    if ((strToFloat(cPercMulta)) = 0) then
    begin
        frmMenu.MensagemAlerta('Percentual de Multa n�o parametrizado,(MULTAFINIMOB)') ;
        exit;
    end;

    nValorBaseCalc := (nValorBaseCalc * (1 + (strToFloat(cPercMulta)/100)) );

    Result := nValorBaseCalc ;

end;

function TfrmAtualizacaoMonetaria.calculaDiasDecorrido (dDataInicial : TDateTime; dDataFinal : TDateTime): Integer;
var
   idias : Integer;
begin

   idias := Trunc(dDataInicial-dDataFinal);

   if idias < 0 then
       idias :=0;

   Result := idias;

end;

function TfrmAtualizacaoMonetaria.atualizaIndiceParcela(dDtLiquidacao: TDateTime ; nCdTitulo : integer; nValBaseCorrecao : Double): Double;
var
    sValor_corrigido  : string;
    nValor_corrigido  : Double;
    nBase_Atualizacao : Double;

    iAnoVencto : integer;
    iMesVencto : integer;
    iDiaVencto : integer;

    iAnoLiquid : integer;
    iMesLiquid : integer;
    iDiaLiquid : integer;

    iAno : integer;
    iMes : integer;

begin

    qryTituloFinanceiro.Close;
    qryTituloFinanceiro.Parameters.ParamByName('nCdTitulo').Value  := nCdTitulo;
    qryTituloFinanceiro.Open;

    // titulo nao encontrado volta
    if qryTituloFinanceiro.eof then
    begin
       exit;
    end;

    // parcela sinal - nao tem correcao
    if (qryTituloFinanceironCdTabTipoParcContratoEmpImobiliario.Value =  strToint(frmMenu.LeParametro('TPPARCIMOBSINAL'))) then
    begin

        Result := qryTituloFinanceironSaldoTit.Value;
        exit;

    end;

    // parcela intermediarias
    if (qryTituloFinanceironCdTabTipoParcContratoEmpImobiliario.Value = strToint(frmMenu.LeParametro('TPPARCIMOBINTER'))) then
    begin

        // calcula correcao desde o inicio do contrato at� a data de liquidacao
        nValor_corrigido := qryTituloFinanceironSaldoTit.Value;
        iDiaVencto       := StrToInt(FormatDateTime('dd'  ,qryTituloFinanceirodDtContrato.Value));
        iMesVencto       := StrToInt(FormatDateTime('mm'  ,qryTituloFinanceirodDtContrato.Value));
        iAnoVencto       := StrToInt(FormatDateTime('yyyy',qryTituloFinanceirodDtContrato.Value));

        // liquidacao
        iDiaLiquid := StrToInt(FormatDateTime('dd'  ,dDtLiquidacao));
        iMesLiquid := StrToInt(FormatDateTime('mm'  ,dDtLiquidacao));
        iAnoLiquid := StrToInt(FormatDateTime('yyyy',dDtLiquidacao));

        iano := iAnoVencto;
        imes := iMesVencto;

        while ((iAno < iAnoLiquid) or ((iAno = iAnoLiquid)  and (iMes <= iMesLiquid))) do
        begin
            if ((iAnoVencto < iAnoLiquid) or ((iAnoVencto = iAnoLiquid)  and (iMesVencto <= iMesLiquid))) then
            begin
                qryCotacao.Close;
                qryCotacao.Parameters.ParamByName('nCdIndice').Value := qryTituloFinanceironCdIndiceReajuste.Value;
                qryCotacao.Parameters.ParamByName('iAno').Value      := iano;
                qryCotacao.Parameters.ParamByName('iMes').Value      := imes;
                qryCotacao.Open;

                if qryCotacao.eof then
                begin
                    frmMenu.MensagemAlerta('�ndice do m�s: ' + intToStr(imes) + '/' + intTostr(iano) + ' n�o encontrado.' ) ;
                    nValor_corrigido := qryTituloFinanceironSaldoTit.Value;
                    exit;
                end;

               if not qryCotacao.eof then
               begin
                   nValor_corrigido := (nValor_corrigido * (1+(qryCotacaonIndice.Value/100)));
                   nValor_corrigido := (frmMenu.TBRound(nValor_corrigido,2));
               end;
            end;

            imes := (imes +1);
            if (imes = 13) then
            begin
                imes := 1;
                iano := (iano + 1);
            end;
        end;

        Result := nValor_corrigido;

        exit;

    end;

    // parcela de financiamento

    qryTituloFinanceiro.Close;
    qryTituloFinanceiro.Parameters.ParamByName('nCdTitulo').Value  := nCdTitulo;
    qryTituloFinanceiro.Open;

    // vencimento
    //iDiaVencto := StrToInt(FormatDateTime('dd'  ,qryTituloFinanceirodDtVenc.Value));
    //iMesVencto := StrToInt(FormatDateTime('mm'  ,qryTituloFinanceirodDtVenc.Value));
    //iAnoVencto := StrToInt(FormatDateTime('yyyy',qryTituloFinanceirodDtVenc.Value));

    nValor_corrigido := qryTituloFinanceironSaldoTit.Value;
    iDiaVencto       := StrToInt(FormatDateTime('dd'  ,qryTituloFinanceirodDtContrato.Value));
    iMesVencto       := StrToInt(FormatDateTime('mm'  ,qryTituloFinanceirodDtContrato.Value));
    iAnoVencto       := StrToInt(FormatDateTime('yyyy',qryTituloFinanceirodDtContrato.Value));

    // liquidacao
    iDiaLiquid := StrToInt(FormatDateTime('dd'  ,dDtLiquidacao));
    iMesLiquid := StrToInt(FormatDateTime('mm'  ,dDtLiquidacao));
    iAnoLiquid := StrToInt(FormatDateTime('yyyy',dDtLiquidacao));

    iano := iAnoVencto;
    imes := iMesVencto;

    {
    //busca o valor corrigido da ultima parcela paga.
    //pesquisa ultima parcela quitada do contrato tipo financiamento
    qryUltimaParcelaPaga.Close;
    qryUltimaParcelaPaga.Parameters.ParamByName('nCdTerceiro').Value    := qryTituloFinanceironCdTerceiro.Value;
    qryUltimaParcelaPaga.Parameters.ParamByName('cNrTit').Value         := qryTituloFinanceirocNrTit.Value;
    qryUltimaParcelaPaga.Parameters.ParamByName('nCdTipoParcela').Value := qryTituloFinanceironCdTabTipoParcContratoEmpImobiliario.Value;
    qryUltimaParcelaPaga.Open;

    if (nValBaseCorrecao > 0) then
       nValor_corrigido := nValBaseCorrecao
    else
    begin
        if ((not qryUltimaParcelaPaga.eof) and (nValBAseCorrecao = 0)) then
             nValor_corrigido := qryUltimaParcelaPaganValCorrecao.Value
        else
        begin
            // calcula correcao desde o inicio do contrato at� a data de liquidacao
            nValor_corrigido := qryTituloFinanceironSaldoTit.Value;
            iDiaVencto       := StrToInt(FormatDateTime('dd'  ,qryTituloFinanceirodDtContrato.Value));
            iMesVencto       := StrToInt(FormatDateTime('mm'  ,qryTituloFinanceirodDtContrato.Value));
            iAnoVencto       := StrToInt(FormatDateTime('yyyy',qryTituloFinanceirodDtContrato.Value));
            // frmMenu.MensagemAlerta('A o valor corrigido da parcela anterior n�o foi encontrado');
        end;
    end;
    }

    while ((iAno < iAnoLiquid) or ((iAno = iAnoLiquid)  and (iMes <= iMesLiquid))) do
    begin
        if ((iAnoVencto < iAnoLiquid) or ((iAnoVencto = iAnoLiquid)  and (iMesVencto <= iMesLiquid))) then
        begin
            qryCotacao.Close;
            qryCotacao.Parameters.ParamByName('nCdIndice').Value := qryTituloFinanceironCdIndiceReajuste.Value;
            qryCotacao.Parameters.ParamByName('iAno').Value      := iano;
            qryCotacao.Parameters.ParamByName('iMes').Value      := imes;
            qryCotacao.Open;

            if qryCotacao.eof then
            begin
                frmMenu.MensagemAlerta('�ndice de corre��o do m�s: ' + intToStr(imes) + '/' + intTostr(iano) + ' (C�digo do �ndice: ' + qryTituloFinanceironCdIndiceReajuste.asString + ') n�o encontrado.') ;
                if ((iAno < iAnoLiquid) or ((iAno = iAnoLiquid)  and (iMes < iMesLiquid))) then
                begin
                    nValor_corrigido := qryTituloFinanceironSaldoTit.Value;
                    abort;
                end;
            end;

            if not qryCotacao.eof then
            begin
                nValor_corrigido := (nValor_corrigido * (1+(qryCotacaonIndice.Value/100)));
                nValor_corrigido := (frmMenu.TBRound(nValor_corrigido,2));
            end;

        end;

        imes := (imes +1);
        if (imes = 13) then
        begin
            imes := 1;
            iano := (iano + 1);
        end;

    end;

    Result := nValor_corrigido;

end;


function TfrmAtualizacaoMonetaria.atualizaJurosParcelaAtraso(dDtLiquidacao: TDateTime; nCdTitulo: Integer; nValorCorrigido: Double): Double;
var
    cTaxaJuros : String;

    iAnoVencto : integer;
    iMesVencto : integer;
    iDiaVencto : integer;

    iAnoLiquid : integer;
    iMesLiquid : integer;
    iDiaLiquid : integer;

    iAno : integer;
    iMes : integer;

begin

    qryTituloFinanceiro.Close;
    qryTituloFinanceiro.Parameters.ParamByName('nCdTitulo').Value  := nCdTitulo;
    qryTituloFinanceiro.Open;

    // vencimento
    iDiaVencto := StrToInt(FormatDateTime('dd'  ,qryTituloFinanceirodDtVenc.Value));
    iMesVencto := StrToInt(FormatDateTime('mm'  ,qryTituloFinanceirodDtVenc.Value));
    iAnoVencto := StrToInt(FormatDateTime('yyyy',qryTituloFinanceirodDtVenc.Value));

    // liquidacao
    iDiaLiquid := StrToInt(FormatDateTime('dd'  ,dDtLiquidacao));
    iMesLiquid := StrToInt(FormatDateTime('mm'  ,dDtLiquidacao));
    iAnoLiquid := StrToInt(FormatDateTime('yyyy',dDtLiquidacao));

    iano := iAnoVencto;
    imes := iMesVencto;

    cTaxaJuros := frmMenu.LeParametro('JUROSMESFINIMOB');

    if (strToFloat(cTaxaJuros) = 0) then
    begin
        frmMenu.MensagemAlerta('Taxa de Juros n�o parametrizado(JUROSMESFINIMOB).') ;
        abort;
    end;

    while ((iAno < iAnoLiquid) or ((iAno = iAnoLiquid)  and (iMes <= iMesLiquid))) do
    begin
        if ((iAnoVencto < iAnoLiquid) or ((iAnoVencto = iAnoLiquid)  and (iMesVencto <= iMesLiquid))) then
        begin

                nValorcorrigido := (nValorcorrigido * (1 + (strToFloat(cTaxaJuros)) / 100));
                nValorcorrigido := (frmMenu.TBRound(nValorcorrigido,2));

        end;

        imes := (imes +1);
        if (imes = 13) then
        begin
            imes := 1;
            iano := (iano + 1);
        end;

    end;

    Result := nValorcorrigido;

end;

end.
