unit fExploraCaixaCofre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmExploraCaixaCofre = class(TfrmProcesso_Padrao)
    qryConta: TADOQuery;
    qryContanCdContaBancaria: TIntegerField;
    qryContanCdConta: TStringField;
    qryContacTipo: TStringField;
    qryContacNmUsuario: TStringField;
    DBGridEh1: TDBGridEh;
    dsConta: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryContanSaldoConta: TBCDField;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryLocalizaResumoCaixa: TADOQuery;
    qryLocalizaResumoCaixanCdResumoCaixa: TAutoIncField;
    procedure ToolButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryContaAfterScroll(DataSet: TDataSet);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmExploraCaixaCofre: TfrmExploraCaixaCofre;

implementation

uses fCaixa_StatusDocto, fMenu, rBorderoCaixaNovo;

{$R *.dfm}

procedure TfrmExploraCaixaCofre.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmCaixa_StatusDocto;
begin
  inherited;

  if (not qryConta.Active) or (qryContanCdContaBancaria.Value = 0) then
  begin

      MensagemAlerta('Nenhuma conta selecionada.') ;
      exit ;

  end ;

  objForm := TfrmCaixa_StatusDocto.Create(nil);

  objForm.cxTabSheet4.Enabled := (qryContacTipo.Value = 'Caixa') ;

  PosicionaQuery(objForm.qryCheque, qryContanCdContaBancaria.asString) ;
  PosicionaQuery(objForm.qryCartao, qryContanCdContaBancaria.asString) ;
  PosicionaQuery(objForm.qryCrediario, qryContanCdContaBancaria.asString) ;

  if (qryContacTipo.Value = 'Caixa') then
      PosicionaQuery(objForm.qryLanctoMan, qryContanCdContaBancaria.asString) ;

  objForm.DBGridEh2.PopupMenu := nil ;

  showForm(objForm,true);

  //objForm.DBGridEh2.PopupMenu := objForm.PopupMenu2;

end;

procedure TfrmExploraCaixaCofre.FormShow(Sender: TObject);
begin
  inherited;

  qryConta.Close ;
  PosicionaQuery(qryConta, IntToStr(frmMenu.nCdLojaAtiva)) ;

  if qryConta.eof then
      MensagemAlerta('Nenhuma conta caixa/cofre encontrada para esta loja.') ;
  
end;

procedure TfrmExploraCaixaCofre.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  ToolButton5.Click;
end;

procedure TfrmExploraCaixaCofre.qryContaAfterScroll(DataSet: TDataSet);
begin
  inherited;

  ToolButton7.Enabled := (qryContacTipo.Value = 'Caixa') ;
  
end;

procedure TfrmExploraCaixaCofre.ToolButton7Click(Sender: TObject);
var
  objForm : TrptBorderoCaixaNovo;
begin
  inherited;

  qryLocalizaResumoCaixa.Close;
  PosicionaQuery(qryLocalizaResumoCaixa, qryContanCdContaBancaria.AsString) ;

  objForm := TrptBorderoCaixaNovo.Create(nil);

  if not qryLocalizaResumoCaixa.Eof then
      objForm.BorderoIndividual(qryLocalizaResumoCaixanCdResumoCaixa.Value)
  else MensagemErro('Este caixa n�o est� aberto.') ;

end;

initialization
    RegisterClass(TfrmExploraCaixaCofre) ;
    
end.
