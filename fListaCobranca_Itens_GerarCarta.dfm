inherited frmListaCobranca_Itens_GerarCarta: TfrmListaCobranca_Itens_GerarCarta
  Left = 384
  Top = 303
  Width = 730
  Height = 168
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  Caption = 'Gerar Carta Cobran'#231'a'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 714
    Height = 101
  end
  object Label1: TLabel [1]
    Left = 15
    Top = 51
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo Carta'
  end
  inherited ToolBar1: TToolBar
    Width = 714
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object edtModeloCarta: TMaskEdit [3]
    Left = 83
    Top = 43
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtModeloCartaExit
    OnKeyDown = edtModeloCartaKeyDown
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 152
    Top = 43
    Width = 550
    Height = 21
    DataField = 'cNmModeloCarta'
    DataSource = DataSource1
    TabOrder = 2
  end
  object cxButton1: TcxButton [5]
    Left = 8
    Top = 96
    Width = 137
    Height = 28
    Caption = 'Processar Cartas'
    TabOrder = 4
    OnClick = cxButton1Click
    Glyph.Data = {
      0A020000424D0A0200000000000036000000280000000C0000000D0000000100
      180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
      656464656464656464656464656464656464656464656464898A89B9BABAD5D5
      D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
      C6C68C908F191D23313332313332313332313332313332313332313332191D23
      040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
      BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
      85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
      A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
      FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
      FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
      C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
      CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
      C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
      939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
      23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
      A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
    LookAndFeel.NativeStyle = True
  end
  object DBMemo1: TDBMemo [6]
    Left = 8
    Top = 120
    Width = 953
    Height = 242
    DataField = 'cTextoCorpo'
    DataSource = DataSource1
    TabOrder = 5
    Visible = False
    WantTabs = True
  end
  object DBMemo2: TDBMemo [7]
    Left = 8
    Top = 360
    Width = 953
    Height = 241
    DataField = 'cTextoPosTitulo'
    DataSource = DataSource1
    TabOrder = 6
    Visible = False
    WantTabs = True
  end
  object CheckBox1: TCheckBox [8]
    Left = 83
    Top = 72
    Width = 358
    Height = 17
    Caption = 'Gerar Registro de Contato no Hist'#243'rico de Contatos do Cliente'
    TabOrder = 3
  end
  object qryModeloCarta: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM ModeloCarta'
      ' WHERE nCdTabTipoModeloCarta = 1'
      '   AND cFlgAtivo             = 1'
      '   AND nCdModeloCarta        = :nPK'
      'AND nCdEmpresa = :nCdEmpresa'
      '')
    Left = 400
    Top = 72
    object qryModeloCartanCdModeloCarta: TIntegerField
      FieldName = 'nCdModeloCarta'
    end
    object qryModeloCartacNmModeloCarta: TStringField
      FieldName = 'cNmModeloCarta'
      Size = 50
    end
    object qryModeloCartanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryModeloCartanCdTabTipoModeloCarta: TIntegerField
      FieldName = 'nCdTabTipoModeloCarta'
    end
    object qryModeloCartaiLinhaDestinatario: TIntegerField
      FieldName = 'iLinhaDestinatario'
    end
    object qryModeloCartaiColunaDestinatario: TIntegerField
      FieldName = 'iColunaDestinatario'
    end
    object qryModeloCartacFlgNaoExibeDest: TIntegerField
      FieldName = 'cFlgNaoExibeDest'
    end
    object qryModeloCartaiLinhaTextoCorpo: TIntegerField
      FieldName = 'iLinhaTextoCorpo'
    end
    object qryModeloCartacTextoCorpo: TMemoField
      FieldName = 'cTextoCorpo'
      BlobType = ftMemo
    end
    object qryModeloCartaiLinhaTitulo: TIntegerField
      FieldName = 'iLinhaTitulo'
    end
    object qryModeloCartacTextoPosTitulo: TMemoField
      FieldName = 'cTextoPosTitulo'
      BlobType = ftMemo
    end
    object qryModeloCartacFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryModeloCartacTamanhoFonteDest: TStringField
      FieldName = 'cTamanhoFonteDest'
      Size = 10
    end
    object qryModeloCartacTamanhoFonteCorpo: TStringField
      FieldName = 'cTamanhoFonteCorpo'
      Size = 10
    end
    object qryModeloCartacTamanhoFonteRodape: TStringField
      FieldName = 'cTamanhoFonteRodape'
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = qryModeloCarta
    Left = 432
    Top = 72
  end
  object qryItemLista: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '   FROM ItemListaCobranca'
      'WHERE nCdItemListaCobranca = :nPK')
    Left = 200
    Top = 64
    object qryItemListanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      
        '      ,cNmTerceiro + '#39'  -  ('#39' + dbo.fn_zeroEsquerda(Terceiro.nCd' +
        'Terceiro,10) + '#39')'#39' as cNmTerceiro'
      '      ,cCNPJCPF'
      '      ,cRG'
      '      ,cNmFantasia'
      '      ,cCepCxPostal'
      '      ,cCaixaPostal'
      '  FROM Terceiro'
      
        '       LEFT  JOIN PessoaFisica ON PessoaFisica.nCdTerceiro = Ter' +
        'ceiro.nCdTerceiro'
      ' WHERE Terceiro.nCdTerceiro = :nPK')
    Left = 232
    Top = 64
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      DisplayWidth = 150
      FieldName = 'cNmTerceiro'
      Size = 150
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryTerceirocNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryTerceirocCepCxPostal: TStringField
      FieldName = 'cCepCxPostal'
      FixedChar = True
      Size = 8
    end
    object qryTerceirocCaixaPostal: TStringField
      FieldName = 'cCaixaPostal'
      Size = 15
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.cNrTit'
      '  FROM Titulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE Titulo.nCdEmpresa   = :nCdEmpresa'
      '   AND Titulo.nCdTerceiro  = :nCdTerceiro'
      '   AND EspTit.cFlgCobranca = 1'
      '   AND Titulo.cSenso       = '#39'C'#39
      '   AND Titulo.nSaldoTit    > 0'
      '   AND Titulo.dDtCancel   IS NULL'
      '   AND dDtVenc             < dbo.fn_OnlyDate(GetDate()) ')
    Left = 272
    Top = 64
    object qryTitulosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulosiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulosnValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTituloscNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
  end
  object RDprint1: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Carta Cobran'#231'a'
    OpcoesPreview.PreviewZoom = -1
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = True
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Invisivel
    OpcoesPreview.BotaoLer = Invisivel
    OpcoesPreview.BotaoProcurar = Invisivel
    Margens.Left = 2
    Margens.Right = 2
    Margens.Top = 2
    Margens.Bottom = 2
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'EDGAR DE SOUZA'
    RegistroUsuario.SerieProduto = 'SINGLE-0708/01624'
    RegistroUsuario.AutorizacaoKey = 'BWVI-4846-SAHU-8864-PAPQ'
    About = 'RDprint 4.0e - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Carta Cobran'#231'a'
    UsaGerenciadorImpr = True
    CorForm = clWhite
    CorFonte = clBlack
    Impressora = HP
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    OnPreview = RDprint1Preview
    Left = 472
    Top = 72
  end
  object qryItensLista: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemListaCobranca'
      'FROM ItemListaCobranca'
      'WHERE nCdListaCobranca = :nPK')
    Left = 512
    Top = 72
    object qryItensListanCdItemListaCobranca: TAutoIncField
      FieldName = 'nCdItemListaCobranca'
      ReadOnly = True
    end
  end
  object qryEndereco: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT --RTRIM(IsNull(cEndereco,'#39' '#39'))  +'
      '       CASE WHEN Endereco.nCdTabTipoLogradouro IS NULL'
      '            THEN Endereco.cEndereco'
      #9#9'    ELSE UPPER(TipoLog.cSigla) + '#39' '#39' + Endereco.cEndereco'
      #9'   END +'
      
        '       CASE WHEN iNumero > 0 THEN + '#39',  '#39' + dbo.fn_ZeroEsquerda(' +
        'IsNull(iNumero,0),6) '
      '            ELSE '#39#39
      '       END + '
      '       '#39'  '#39' + IsNull(cComplemento,'#39' '#39') as cEndereco'
      '      ,cBairro'
      
        '      ,IsNull(cCidade,'#39' '#39') + '#39' / '#39' + IsNull(cUF,'#39' '#39') + '#39'   CEP: ' +
        #39' + IsNull(cCep,'#39'00000000'#39') as cCidade'
      '  FROM Endereco'
      
        '       LEFT JOIN TabTipoLogradouro TipoLog ON TipoLog.nCdTabTipo' +
        'Logradouro = Endereco.nCdTabTipoLogradouro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND nCdStatus   = 1')
    Left = 552
    Top = 72
    object qryEnderecocEndereco: TStringField
      FieldName = 'cEndereco'
      ReadOnly = True
      Size = 66
    end
    object qryEnderecocBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryEnderecocCidade: TStringField
      FieldName = 'cCidade'
      ReadOnly = True
      Size = 56
    end
  end
  object qryCidadeEmpresa: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 cCidade + '#39'/'#39' + cUF as cCidade'
      '  FROM Endereco'
      ' WHERE nCdTerceiro = (SELECT nCdTerceiroEmp'
      '                        FROM Empresa'
      '                       WHERE nCdEmpresa = :nPK)'
      '   AND nCdStatus   = 1')
    Left = 648
    Top = 72
    object qryCidadeEmpresacCidade: TStringField
      FieldName = 'cCidade'
      ReadOnly = True
      Size = 56
    end
  end
  object qryGeraContatoCarta: TADOQuery
    Connection = frmMenu.Connection
    CursorLocation = clUseServer
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdListaCobranca'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja              int'
      '       ,@dDtContato           datetime'
      '       ,@nCdListaCobranca     int'
      '       ,@nCdUsuario           int'
      '       ,@nCdterceiro          int'
      '       ,@nCdItemListaCobranca int'
      '       ,@nCdContatoCobranca   int'
      ''
      'Set @dDtContato       = GetDate()'
      'Set @nCdLoja          = :nCdLoja'
      'Set @nCdListaCobranca = :nCdListaCobranca'
      'Set @nCdUsuario       = :nCdUsuario'
      ''
      'IF (@nCdLoja = 0) Set @nCdLoja = NULL'
      ''
      'DECLARE curItens CURSOR '
      '    FOR SELECT nCdTerceiro'
      '              ,nCdItemListaCobranca'
      '          FROM ItemListaCobranca'
      '         WHERE nCdListaCobranca = @nCdListaCobranca'
      '           AND NOT EXISTS(SELECT 1'
      '                            FROM ContatoCobranca'
      
        '                           WHERE ContatoCobranca.nCdTerceiro = I' +
        'temListaCobranca.nCdTerceiro'
      
        '                             AND dbo.fn_OnlyDate(dDtContato) = d' +
        'bo.fn_OnlyDate(@dDtContato)'
      
        '                             AND cNmPessoaContato            = '#39 +
        'CARTA COBRAN'#199'A'#39')'
      ''
      'OPEN curItens'
      ''
      'FETCH NEXT'
      ' FROM curItens'
      ' INTO @nCdTerceiro'
      '     ,@nCdItemListaCobranca'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '  Set @nCdContatoCobranca = NULL'
      ''
      '  EXEC usp_ProximoID  '#39'CONTATOCOBRANCA'#39
      '                     ,@nCdContatoCobranca OUTPUT'
      ''
      #9'INSERT INTO ContatoCobranca (nCdContatoCobranca'
      '                ,nCdTerceiro'
      #9#9#9#9#9#9#9#9',nCdLoja'
      #9#9#9#9#9#9#9#9',dDtContato'
      #9#9#9#9#9#9#9#9',cNmPessoaContato'
      #9#9#9#9#9#9#9#9',cTextoContato'
      #9#9#9#9#9#9#9#9',nCdItemListaCobranca'
      #9#9#9#9#9#9#9#9',cFlgPendReagendamento'
      #9#9#9#9#9#9#9#9',nCdUsuario'
      #9#9#9#9#9#9#9#9',cFlgCarta'
      '                ,cFlgInclusoAutomatico)'
      #9#9#9#9#9#9'  VALUES(@nCdContatoCobranca'
      '                ,@nCdTerceiro'
      #9#9#9#9#9#9#9#9',@nCdLoja'
      #9#9#9#9#9#9#9#9',@dDtContato'
      #9#9#9#9#9#9#9#9','#39'CARTA COBRAN'#199'A'#39
      #9#9#9#9#9#9#9#9','#39'GERADA CARTA DE COBRAN'#199'A'#39
      #9#9#9#9#9#9#9#9',@nCdItemListaCobranca'
      #9#9#9#9#9#9#9#9',0'
      #9#9#9#9#9#9#9#9',@nCdUsuario'
      #9#9#9#9#9#9#9#9',1'
      '                ,1)'
      ''
      #9'FETCH NEXT'
      #9' FROM curItens'
      #9' INTO @nCdTerceiro'
      #9#9' ,@nCdItemListaCobranca'
      ''
      'END'
      ''
      'CLOSE curItens'
      'DEALLOCATE curItens')
    Left = 584
    Top = 72
  end
end
