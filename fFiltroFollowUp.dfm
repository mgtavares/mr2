inherited frmFiltroFollowUp: TfrmFiltroFollowUp
  Left = 335
  Top = 259
  Width = 693
  Height = 289
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Filtro FollowUp'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 677
    Height = 222
  end
  object Label1: TLabel [1]
    Left = 49
    Top = 40
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 51
    Top = 88
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 208
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Entrega'
  end
  object Label6: TLabel [4]
    Left = 172
    Top = 208
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 8
    Top = 64
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Econ'#244'mico'
  end
  object Label7: TLabel [6]
    Left = 61
    Top = 136
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label4: TLabel [7]
    Left = 172
    Top = 232
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label8: TLabel [8]
    Left = 11
    Top = 232
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo Pedido'
  end
  object Label9: TLabel [9]
    Left = 35
    Top = 112
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido'
  end
  object Label10: TLabel [10]
    Left = 11
    Top = 160
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
  end
  object Label11: TLabel [11]
    Left = 52
    Top = 184
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  inherited ToolBar1: TToolBar
    Width = 677
    TabOrder = 18
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    object ToolButton5: TToolButton
      Left = 166
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 174
      Top = 0
      Hint = 'Limpar Filtros'
      Caption = 'Limpar'
      ImageIndex = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton4Click
    end
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 164
    Top = 32
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [14]
    Tag = 1
    Left = 227
    Top = 32
    Width = 446
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 13
  end
  object DBEdit9: TDBEdit [15]
    Tag = 1
    Left = 164
    Top = 80
    Width = 120
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 14
  end
  object DBEdit10: TDBEdit [16]
    Tag = 1
    Left = 287
    Top = 80
    Width = 386
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 12
  end
  object MaskEdit6: TMaskEdit [17]
    Left = 96
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit7: TMaskEdit [18]
    Left = 192
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEdit1: TMaskEdit [19]
    Left = 96
    Top = 32
    Width = 65
    Height = 21
    Enabled = False
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
  end
  object MaskEdit3: TMaskEdit [20]
    Left = 96
    Top = 80
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [21]
    Tag = 1
    Left = 164
    Top = 56
    Width = 509
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 15
  end
  object MaskEdit2: TMaskEdit [22]
    Left = 96
    Top = 56
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit2Exit
    OnKeyDown = MaskEdit2KeyDown
  end
  object MaskEdit5: TMaskEdit [23]
    Left = 96
    Top = 128
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit5: TDBEdit [24]
    Tag = 1
    Left = 164
    Top = 128
    Width = 509
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 16
  end
  object MaskEdit9: TMaskEdit [25]
    Left = 192
    Top = 224
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object MaskEdit8: TMaskEdit [26]
    Left = 96
    Top = 224
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object DBEdit4: TDBEdit [27]
    Tag = 1
    Left = 164
    Top = 104
    Width = 509
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 17
  end
  object MaskEdit4: TMaskEdit [28]
    Left = 96
    Top = 104
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit10: TMaskEdit [29]
    Left = 96
    Top = 152
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit10Exit
    OnKeyDown = MaskEdit10KeyDown
  end
  object DBEdit6: TDBEdit [30]
    Tag = 1
    Left = 164
    Top = 152
    Width = 509
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCC
    TabOrder = 19
  end
  object edtCdProduto: TER2LookupMaskEdit [31]
    Left = 96
    Top = 176
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 6
    Text = '         '
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object DBEdit7: TDBEdit [32]
    Tag = 1
    Left = 164
    Top = 176
    Width = 509
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 20
  end
  inherited ImageList1: TImageList
    Left = 600
    Top = 152
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C
      6F009C8C6F009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED9000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F009C8C
      6F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000009C8C6F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFC030000
      FE00C003C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C007000000018001C00F000000038001C01F0000
      0077C003C03F0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 408
    Top = 184
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nPK')
    Left = 472
    Top = 184
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 408
    Top = 216
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 504
    Top = 216
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 472
    Top = 216
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 440
    Top = 184
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 440
    Top = 216
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 536
    Top = 184
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 536
    Top = 216
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdusuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido, cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM UsuarioTipoPedido UTP'
      
        '               WHERE UTP.nCdTipoPedido = TipoPedido.nCdTipoPedid' +
        'o'
      '                 AND UTP.nCdUsuario = :nCdusuario)')
    Left = 504
    Top = 184
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      'FROM CentroCusto'
      'WHERE cFlgLanc = 1'
      'AND nCdCC = :nPK')
    Left = 568
    Top = 184
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCC: TDataSource
    DataSet = qryCC
    Left = 568
    Top = 216
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 600
    Top = 184
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 600
    Top = 216
  end
end
