unit rCartaCorrecaoNFe_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls;

type
  TrptCartaCorrecaoNFe_View = class(TForm)
    QuickRep1: TQuickRep;
    qrTitle: TQRBand;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    lblMsgCabecalho: TQRLabel;
    qrDetail: TQRBand;
    QRLabel4: TQRLabel;
    qrFooter: TQRBand;
    QRShape1: TQRShape;
    QRShape6: TQRShape;
    QRShape10: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape20: TQRShape;
    QRLabel5: TQRLabel;
    lblModelo: TQRLabel;
    lblNumero: TQRLabel;
    lblMesAnoEmissao: TQRLabel;
    lblOrgao: TQRLabel;
    lblAmbiente: TQRLabel;
    lblSeqEvento: TQRLabel;
    lblStatus: TQRLabel;
    lblProtocolo: TQRLabel;
    lblDtRegistro: TQRLabel;
    QRLabel20: TQRLabel;
    QRShape3: TQRShape;
    lblCondUso: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape4: TQRShape;
    lblCorrecao: TQRLabel;
    lblDtEvento: TQRLabel;
    QRShape24: TQRShape;
    lblDescEvento: TQRLabel;
    lblChave: TQRLabel;
    lblSerie: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptCartaCorrecaoNFe_View: TrptCartaCorrecaoNFe_View;

implementation

{$R *.dfm}

end.
