unit fMonitorTransferencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, PrnDbgeh, Menus, DBGridEhGrouping, ToolCtrlsEh,
  DBCtrls, StdCtrls, Mask, ER2Lookup, ER2Excel;

type
  TfrmMonitorTransferencia = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTransfEst: TADOQuery;
    dsTransfEst: TDataSource;
    qryTransfEstnCdTransfEst: TIntegerField;
    qryTransfEstnCdLojaOrigem: TStringField;
    qryTransfEstnCdLojaDestino: TStringField;
    qryTransfEstcNmTabTipoTransf: TStringField;
    qryTransfEstdDtCad: TDateTimeField;
    qryTransfEstiDias: TIntegerField;
    qryTransfEstcNmUsuario: TStringField;
    qryTransfEstnValTransferencia: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopupMenu1: TPopupMenu;
    CancelarTransferncia1: TMenuItem;
    SP_CANCELA_TRANSFERENCIA_ESTOQUE: TADOStoredProc;
    qryTransfEstdDtFinalizacao: TDateTimeField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label9: TLabel;
    er2LkpLojaOrigem: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    er2LkpLojaDestino: TER2LookupMaskEdit;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    mskDtInicial: TMaskEdit;
    Label6: TLabel;
    mskDtFinal: TMaskEdit;
    qryEmpresa: TADOQuery;
    dsEmpresa: TDataSource;
    dsLojaOrigem: TDataSource;
    qryLojaOrigem: TADOQuery;
    qryLojaOrigemnCdLoja: TIntegerField;
    qryLojaOrigemcNmLoja: TStringField;
    qryLojaDestino: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    dsLojaDestino: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    ToolButton6: TToolButton;
    btPlanilha: TToolButton;
    ER2Excel: TER2Excel;
    qryCancelaPedidoTransf: TADOQuery;
    qryTransfEstcFlgAutomaticaCD: TIntegerField;
    qryTransfEstnCdTabTipoTransf: TIntegerField;
    qryTransfEstcDadosFat: TStringField;
    DBEdit1: TDBEdit;
    qryTransfEstiQtdTotalItem: TBCDField;
    ImprimirProtocolo1: TMenuItem;
    N1: TMenuItem;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure CancelarTransferncia1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure btPlanilhaClick(Sender: TObject);
    procedure ImprimirProtocolo1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorTransferencia: TfrmMonitorTransferencia;

implementation

uses
  fMenu, rTransferencia_view, fMonitorTransferencia_Item;

{$R *.dfm}

procedure TfrmMonitorTransferencia.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryTransfEst.Close;
  qryTransfEst.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
  qryTransfEst.Parameters.ParamByName('nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
  qryTransfEst.Parameters.ParamByName('nCdLojaOrigem').Value  := frmMenu.ConvInteiro(er2LkpLojaOrigem.Text);
  qryTransfEst.Parameters.ParamByName('nCdLojaDestino').Value := frmMenu.ConvInteiro(er2LkpLojaDestino.Text);
  qryTransfEst.Parameters.ParamByName('dDtCadastroIni').Value := frmMenu.ConvData(mskDtInicial.Text);
  qryTransfEst.Parameters.ParamByName('dDtCadastroFin').Value := frmMenu.ConvData(mskDtFinal.Text);
  qryTransfEst.Open;

  if (qryTransfEst.Eof) then
      ShowMessage('Nenhuma transfer�ncia pendente de recebimento.') ;

end;

procedure TfrmMonitorTransferencia.FormShow(Sender: TObject);
begin
  inherited;

  GroupBox1.Align := alTop;
  DBGridEh1.Align := alClient;

  DBGridEh1.Columns[1].Visible := (frmMenu.nCdLojaAtiva > 0) ;
  DBGridEh1.Columns[2].Visible := (frmMenu.nCdLojaAtiva > 0) ;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      desativaER2LkpMaskEdit(er2LkpLojaOrigem);
      desativaER2LkpMaskEdit(er2LkpLojaDestino);
  end;

  ToolButton1Click(nil);
end;

procedure TfrmMonitorTransferencia.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBgridEh1,'Monitor de Transfer�ncias');
  
end;

procedure TfrmMonitorTransferencia.CancelarTransferncia1Click(
  Sender: TObject);
begin
  inherited;

  if (qryTransfEst.Active) and (qryTransfEst.RecordCount > 0) then
  begin

      if (MessageDlg('Confirma o cancelamento desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          SP_CANCELA_TRANSFERENCIA_ESTOQUE.Close;
          SP_CANCELA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdTransfEst').Value := qryTransfEstnCdTransfEst.Value ;
          SP_CANCELA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
          SP_CANCELA_TRANSFERENCIA_ESTOQUE.ExecProc;

          { -- se transfer�ncia foi gerada pelo centro de distribui��o, cancela pedido -- }
          if (qryTransfEstnCdTabTipoTransf.Value = 4) then
          begin
              qryCancelaPedidoTransf.Close;
              qryCancelaPedidoTransf.Parameters.ParamByName('nCdTransfEst').Value := qryTransfEstnCdTransfEst.Value;
              qryCancelaPedidoTransf.Parameters.ParamByName('nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
              qryCancelaPedidoTransf.ExecSQL;
          end;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Transfer�ncia cancelada com sucesso!') ;

      ToolButton1Click(nil) ;

  end ;

end;

procedure TfrmMonitorTransferencia.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmMonitorTransferencia_Item;
begin
  inherited;

  if (qryTransfEst.IsEmpty) then
      Exit;

  objForm := TfrmMonitorTransferencia_Item.Create(nil);
  objForm.nCdTransfEst := qryTransfEstnCdTransfEst.Value;
  showForm(objForm, True);
end;

procedure TfrmMonitorTransferencia.btPlanilhaClick(Sender: TObject);
var
  iLinha : Integer;
begin
  inherited;

  if (qryTransfEst.IsEmpty) then
      Exit;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  { -- formata c�lulas -- }
  ER2Excel.Celula['A1'].Background := RGB(221,221,221);
  ER2Excel.Celula['A1'].Range('Q1');
  ER2Excel.Celula['A2'].Background := RGB(221,221,221);
  ER2Excel.Celula['A2'].Range('Q2');
  ER2Excel.Celula['A3'].Background := RGB(221,221,221);
  ER2Excel.Celula['A3'].Range('Q3');
  ER2Excel.Celula['A4'].Background := RGB(221,221,221);
  ER2Excel.Celula['A4'].Range('Q4');

  { -- inseri informa��es do cabe�alho -- }
  ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel.Celula['A2'].Text := 'Monitor de Transfer�ncias';
  ER2Excel.Celula['A4'].Text := 'C�d.';
  ER2Excel.Celula['B4'].Text := 'Loja Origem';
  ER2Excel.Celula['C4'].Text := 'Loja Destino';
  ER2Excel.Celula['D4'].Text := 'Tipo Transf.';
  ER2Excel.Celula['F4'].Text := 'Dt. Trans.';
  ER2Excel.Celula['H4'].Text := 'Dias';
  ER2Excel.Celula['I4'].Text := 'Dt. Finaliza��o';
  ER2Excel.Celula['K4'].Text := 'Usu�rio Cadastro';
  ER2Excel.Celula['L4'].Text := 'Total Itens';
  ER2Excel.Celula['M4'].Text := 'Val. Transf.';
  ER2Excel.Celula['N4'].Text := 'Dados Transf. Faturada';

  ER2Excel.Celula['A1'].Congelar('A5');

  iLinha := 5;

  qryTransfEst.First;
  qryTransfEst.DisableControls;

  while (not qryTransfEst.Eof) do
  begin
      ER2Excel.Celula['A' + IntToStr(iLinha)].Text := qryTransfEstnCdTransfEst.Value;
      ER2Excel.Celula['B' + IntToStr(iLinha)].Text := qryTransfEstnCdLojaOrigem.Value;
      ER2Excel.Celula['C' + IntToStr(iLinha)].Text := qryTransfEstnCdLojaDestino.Value;
      ER2Excel.Celula['D' + IntToStr(iLinha)].Text := qryTransfEstcNmTabTipoTransf.Value;
      ER2Excel.Celula['F' + IntToStr(iLinha)].Text := qryTransfEstdDtCad.Value;
      ER2Excel.Celula['H' + IntToStr(iLinha)].Text := qryTransfEstiDias.Value;
      ER2Excel.Celula['I' + IntToStr(iLinha)].Text := qryTransfEstdDtFinalizacao.Value;
      ER2Excel.Celula['K' + IntToStr(iLinha)].Text := qryTransfEstcNmUsuario.Value;
      ER2Excel.Celula['L' + IntToStr(iLinha)].Text := qryTransfEstiQtdTotalItem.AsInteger;
      ER2Excel.Celula['M' + IntToStr(iLinha)].Text := qryTransfEstnValTransferencia.Value;
      ER2Excel.Celula['N' + IntToStr(iLinha)].Text := qryTransfEstcDadosFat.Value;

      ER2Excel.Celula['M' + IntToStr(iLinha)].Mascara := '#.##0,00';

      qryTransfEst.Next;

      Inc(iLinha);
  end;

  qryTransfEst.First;
  qryTransfEst.EnableControls;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel.ExportXLS;
  ER2Excel.CleanupInstance;

  frmMenu.mensagemUsuario('');
end;

procedure TfrmMonitorTransferencia.ImprimirProtocolo1Click(
  Sender: TObject);
var
  objRel : TrptTransferencia_view;
begin
  inherited;

  if (qryTransfEst.Active) and (qryTransfEst.RecordCount > 0) then
  begin
      objRel := TrptTransferencia_view.Create(nil);

      try
          try
              PosicionaQuery(objRel.qryMaster, qryTransfEstnCdTransfEst.AsString) ;
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
              objRel.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
              raise ;
          end ;

      finally
          FreeAndNil(objRel);
      end;
  end ;
end;

initialization
    RegisterClass(TfrmMonitorTransferencia) ;

end.
