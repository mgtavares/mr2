unit fRegistroEntradaFuncionario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB, ExtCtrls,
  jpeg;

type
  TfrmRegistroEntradaFuncionario = class(TForm)
    edtSenha: TEdit;
    edtLogin: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    SP_INSERE_REG_ENTRADA_FUNC: TADOStoredProc;
    qryValidaUsuario: TADOQuery;
    btnRegistrar: TcxButton;
    btnFechar: TcxButton;
    lblData: TLabel;
    lblHora: TLabel;
    Timer1: TTimer;
    Shape1: TShape;
    Label3: TLabel;
    qryValidaUsuarionCdUsuario: TIntegerField;
    procedure btnRegistrarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegistroEntradaFuncionario: TfrmRegistroEntradaFuncionario;

implementation

uses fMenu, fMensagem;

{$R *.dfm}

function TfrmRegistroEntradaFuncionario.MessageDLG(Msg: string;
  AType: TMsgDlgType; AButtons: TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmRegistroEntradaFuncionario.Timer1Timer(Sender: TObject);
begin
    lblHora.Caption := TimeToStr(Time);
end;

procedure TfrmRegistroEntradaFuncionario.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0);
end;

procedure TfrmRegistroEntradaFuncionario.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0);
end;

procedure TfrmRegistroEntradaFuncionario.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0);
end;


procedure TfrmRegistroEntradaFuncionario.btnRegistrarClick(Sender: TObject);
begin
    if (MessageDLG('Confirma o registro de entrada/sa�da?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
        Abort;

    qryValidaUsuario.Close;
    qryValidaUsuario.Parameters.ParamByName('cNmLogin').Value := edtLogin.Text;
    qryValidaUsuario.Parameters.ParamByName('cSenha').Value   := frmMenu.Cripto(UpperCase(edtSenha.Text));
    qryValidaUsuario.Open;

    if (qryValidaUsuario.IsEmpty) then
        MensagemAlerta('Login ou senha inv�lidos!')
    else
    begin
        frmMenu.Connection.BeginTrans;

        try
            SP_INSERE_REG_ENTRADA_FUNC.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
            SP_INSERE_REG_ENTRADA_FUNC.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.nCdLojaAtiva;
            SP_INSERE_REG_ENTRADA_FUNC.Parameters.ParamByName('@nCdUsuario').Value := qryValidaUsuarionCdUsuario.Value;
            SP_INSERE_REG_ENTRADA_FUNC.ExecProc;

            frmMenu.Connection.CommitTrans;
        except
            MensagemErro('Erro no processamento.');

            frmMenu.Connection.RollbackTrans;

            raise;
        end;

        ShowMessage('Registro inserido com sucesso!');

        edtLogin.Clear;
        edtSenha.Clear;

        qryValidaUsuario.Close;
    end;
end;

procedure TfrmRegistroEntradaFuncionario.btnFecharClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmRegistroEntradaFuncionario.FormShow(Sender: TObject);
begin
    lblData.Caption := DateToStr(Date);

    edtLogin.SetFocus;
end;

procedure TfrmRegistroEntradaFuncionario.FormCreate(Sender: TObject);
begin
    lblHora.Caption := TimeToStr(Time);
end;

procedure TfrmRegistroEntradaFuncionario.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_RETURN) then
    begin
        Key := 0;
        Perform(WM_NextDlgCtl, 0, 0);
    end;
end;

initialization
    RegisterClass(TfrmRegistroEntradaFuncionario);

end.
