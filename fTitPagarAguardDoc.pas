unit fTitPagarAguardDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DBCtrls, Mask,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTitPagarAguardDoc = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryResultado: TADOQuery;
    qryResultadonCdTitulo: TIntegerField;
    qryResultadocNrNF: TStringField;
    qryResultadodDtEmissao: TDateTimeField;
    qryResultadodDtReceb: TDateTimeField;
    qryResultadodDtVenc: TDateTimeField;
    qryResultadonValTit: TBCDField;
    qryResultadonCdSP: TIntegerField;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadocNmContato: TStringField;
    qryResultadocTelefone: TStringField;
    qryResultadocFax: TStringField;
    qryResultadocEmail: TStringField;
    qryResultadocNmFormaPagto: TStringField;
    dsResultado: TDataSource;
    ToolButton4: TToolButton;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    qryResultadocNrTit: TStringField;
    qryResultadocFlgDocCobranca: TIntegerField;
    GroupBox1: TGroupBox;
    edtDtInicial: TMaskEdit;
    edtDtFinal: TMaskEdit;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    edtnCdTerceiro: TMaskEdit;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    edtNrNF: TEdit;
    Label2: TLabel;
    edtNrTit: TEdit;
    qryResultadonCdLojaTit: TStringField;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    cxTextEdit7: TcxTextEdit;
    cxTextEdit8: TcxTextEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure edtnCdTerceiroExit(Sender: TObject);
    procedure edtnCdTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTitPagarAguardDoc: TfrmTitPagarAguardDoc;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmTitPagarAguardDoc.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('cNrNF').Value       := Trim(edtNrNF.Text)  ;
  qryResultado.Parameters.ParamByName('cNrTit').Value      := Trim(edtNrTit.Text) ;
  qryResultado.Parameters.ParamByName('dDtInicial').Value  := DateToStr(frmMenu.ConvData(edtDtInicial.Text)) ;
  qryResultado.Parameters.ParamByName('dDtFinal').Value    := DateToStr(frmMenu.ConvData(edtDtFinal.Text))   ;
  qryResultado.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtnCdTerceiro.Text) ;
  qryResultado.Open ;

end;

procedure TfrmTitPagarAguardDoc.cxButton1Click(Sender: TObject);
begin
  inherited;
  ToolButton1.Click;
end;

procedure TfrmTitPagarAguardDoc.edtnCdTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtnCdTerceiro.Text) ;

end;

procedure TfrmTitPagarAguardDoc.edtnCdTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(90);

        If (nPK > 0) then
        begin
            edtnCdTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtnCdTerceiro.Text);
        end ;

    end ;

  end ;

end;

procedure TfrmTitPagarAguardDoc.DBGridEh1DblClick(Sender: TObject);
var
    cNrTit : String ;
begin
  inherited;

  if not qryResultado.Active then
      exit ;

  if not InputQuery('ER2Soft','Informe o n�mero do t�tulo',cNrTit) then exit ;

  if (Trim(cNrTit) = '') then
  begin
      case MessageDlg('O n�mero do t�tulo n�o foi informado, Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
  end ;

  case MessageDlg('Confirma o recebimento deste documento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryResultado.Edit ;
      qryResultadocNrTit.Value          := cNrTit ;
      qryResultadocFlgDocCobranca.Value := 1 ;
      qryResultado.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ToolButton1.Click;


end;

procedure TfrmTitPagarAguardDoc.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (DataCol > 0) then
  begin

      // t�tulos vencidos
      if (qryResultadodDtVenc.Value < StrToDate(DateToStr(Now()))) then
      begin

          DBGridEh1.Canvas.Brush.Color := clRed;
          DBGridEh1.Canvas.Font.Color  := clWhite;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

      end

      // t�tulos vencendo hoje
      else if (qryResultadodDtVenc.Value = StrToDate(DateToStr(Now()))) then
      begin

          DBGridEh1.Canvas.Brush.Color := clYellow;
          DBGridEh1.Canvas.Font.Color  := clBlack;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

      end

      // t�tulos vencendo nos pr�ximos 15 dias
      else if ((qryResultadodDtVenc.Value - StrToDate(DateToStr(Now()))) <= 15) then
      begin

          DBGridEh1.Canvas.Brush.Color := clBlue;
          DBGridEh1.Canvas.Font.Color  := clWhite;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

      end

      // t�tulos vencendo ap�s 15 dias
      else
      begin

          DBGridEh1.Canvas.Brush.Color := clGradientInactiveCaption;
          DBGridEh1.Canvas.Font.Color  := clBlack;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

      end;
  end;
end;

initialization
    RegisterClass(TfrmTitPagarAguardDoc) ;

end.
