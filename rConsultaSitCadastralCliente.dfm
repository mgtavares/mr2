inherited rptConsultaSitCadastralCliente: TrptConsultaSitCadastralCliente
  Left = 306
  Top = 267
  Width = 770
  Height = 248
  Caption = 'Rel. Consulta Situa'#231#227'o Cadastral do Cliente'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 754
    Height = 181
  end
  object Label3: TLabel [1]
    Left = 29
    Top = 118
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Cadastro'
  end
  object Label6: TLabel [2]
    Left = 201
    Top = 120
    Width = 16
    Height = 13
    Alignment = taCenter
    Caption = 'at'#233
  end
  object Label1: TLabel [3]
    Left = 70
    Top = 93
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Situa'#231#227'o'
  end
  object Label5: TLabel [4]
    Left = 30
    Top = 48
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja de Cadastro'
    FocusControl = DBEdit1
  end
  object Label7: TLabel [5]
    Left = 78
    Top = 69
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  inherited ToolBar1: TToolBar
    Width = 754
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object mskdDtInicial: TMaskEdit [7]
    Left = 115
    Top = 112
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object mskdDtFinal: TMaskEdit [8]
    Left = 226
    Top = 112
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object er2LkpTabTipoSituacao: TER2LookupMaskEdit [9]
    Left = 116
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;0; '
    MaxLength = 9
    TabOrder = 2
    CodigoLookup = 764
    QueryLookup = qryTabTipoSituacao
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 184
    Top = 88
    Width = 555
    Height = 21
    DataField = 'cNmTabTipoSituacao'
    DataSource = dsSitucao
    TabOrder = 7
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 40
    Width = 555
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 8
  end
  object er2LkpLoja: TER2LookupMaskEdit [12]
    Left = 116
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object rgModeloImp: TRadioGroup [13]
    Left = 115
    Top = 145
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 5
  end
  object mskTerceiro: TER2LookupMaskEdit [14]
    Left = 116
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 180
    QueryLookup = qryTerceiro
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 184
    Top = 64
    Width = 555
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 9
  end
  inherited ImageList1: TImageList
    Left = 360
    Top = 128
  end
  object qryTabTipoSituacao: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoSituacao'
      '      ,cNmTabTipoSituacao'
      '  FROM TabTipoSituacao'
      ' WHERE nCdTabTipoSituacao = :nPK')
    Left = 392
    Top = 128
    object qryTabTipoSituacaonCdTabTipoSituacao: TIntegerField
      FieldName = 'nCdTabTipoSituacao'
    end
    object qryTabTipoSituacaocNmTabTipoSituacao: TStringField
      FieldName = 'cNmTabTipoSituacao'
      Size = 50
    end
  end
  object dsSitucao: TDataSource
    DataSet = qryTabTipoSituacao
    Left = 392
    Top = 160
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryLojaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 424
    Top = 128
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 424
    Top = 160
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Consulta Situa'#231#227'o Cadastral do Cliente'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 328
    Top = 128
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM  vwClienteVarejo'
      ' WHERE nCdTerceiro = :nPK')
    Left = 456
    Top = 128
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 456
    Top = 160
  end
end
