inherited frmConferenciaProdutoPadrao: TfrmConferenciaProdutoPadrao
  Left = 395
  Top = 191
  Width = 544
  Height = 461
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Digita'#231#227'o/Confer'#234'ncia de Produtos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 101
    Width = 528
    Height = 322
  end
  inherited ToolBar1: TToolBar
    Width = 528
    ButtonWidth = 82
    inherited ToolButton1: TToolButton
      Caption = 'Gravar Itens'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 82
    end
    inherited ToolButton2: TToolButton
      Left = 90
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 101
    Width = 528
    Height = 322
    Align = alClient
    AllowedOperations = [alopDeleteEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsClientProduto
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cCodigo'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Itens|C'#243'd. Sistema'
        Width = 113
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Itens|Descri'#231#227'o'
        Width = 334
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footer.FieldName = 'nQtde'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Itens|Qtde.'
        Width = 42
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 29
    Width = 528
    Height = 72
    Align = alTop
    BevelOuter = bvLowered
    Color = clWhite
    TabOrder = 2
    object gbProduto: TGroupBox
      Left = 8
      Top = 5
      Width = 249
      Height = 60
      Caption = ' Produto '
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 30
        Width = 57
        Height = 13
        Caption = 'C'#243'digo/EAN'
      end
      object edtEAN: TEdit
        Left = 72
        Top = 24
        Width = 161
        Height = 21
        TabOrder = 0
        OnKeyDown = edtEANKeyDown
      end
    end
    object gbModo: TGroupBox
      Left = 264
      Top = 5
      Width = 257
      Height = 60
      Caption = ' Modo '
      Enabled = False
      TabOrder = 1
      object Image2: TImage
        Left = 13
        Top = 27
        Width = 16
        Height = 16
        Center = True
        Picture.Data = {
          07544269746D617046020000424D460200000000000036000000280000000F00
          00000B0000000100180000000000100200000000000000000000000000000000
          0000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF
          000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF0000000000
          00FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF00
          0000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF
          000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF0000000000
          00FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF00
          0000FFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF
          7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF0000000D06FF0D06FF0D06FF0D06FF0D06
          FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF00
          0000FFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF
          7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF000000FFFFFF000000FFFFFF0000000000
          00FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF00
          0000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF
          000000000000FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF0000000000
          00FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF00
          0000FFFFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF
          000000000000FFFFFF000000FFFFFF000000}
      end
      object Image3: TImage
        Left = 150
        Top = 26
        Width = 16
        Height = 16
        Center = True
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          000010000000010018000000000000030000C40E0000C40E0000000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFF7FAF7F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E
          4754A35C4F9F57337D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFF8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE
          897BCC8776CA8176C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF9FCFA59B0636BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C
          36803CF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A7
          5CB2D6B59CC9A05CAD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFF
          FFFFFFFFFFD9EFDC6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD89
          7CCD8756A55F38823EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFF
          FFFFFFFFFFFFFFFFFFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E
          60B06A81CF8D7FCF8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609F
          C4A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF9ACA9F63B36D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      object rbDigitacaoLeitura: TRadioButton
        Left = 36
        Top = 26
        Width = 104
        Height = 17
        Caption = 'Digita'#231#227'o/Leitura'
        TabOrder = 0
      end
      object rbConferencia: TRadioButton
        Left = 171
        Top = 26
        Width = 79
        Height = 17
        Caption = 'Confer'#234'ncia'
        TabOrder = 1
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 248
    Top = 240
  end
  object cdsProduto: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'nQtde'
        DataType = ftFloat
      end
      item
        Name = 'cCodigo'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'iPosicao'
        DataType = ftInteger
      end
      item
        Name = 'cNmProduto'
        DataType = ftString
        Size = 150
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 280
    Top = 208
    Data = {
      740000009619E0BD0100000018000000040000000000030000007400056E5174
      646508000400000000000763436F6469676F0100490000000100055749445448
      0200020014000869506F736963616F04000100000000000A634E6D50726F6475
      746F01004900000001000557494454480200020096000000}
    object cdsProdutonQtde: TFloatField
      DisplayLabel = 'Itens|Quantidade'
      FieldName = 'nQtde'
    end
    object cdsProdutocCodigo: TStringField
      DisplayLabel = 'Itens|C'#243'digo/EAN'
      FieldName = 'cCodigo'
    end
    object cdsProdutoiPosicao: TIntegerField
      FieldName = 'iPosicao'
    end
    object cdsProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsClientProduto: TDataSource
    DataSet = cdsProduto
    Left = 280
    Top = 240
  end
  object cdsProdutosConfere: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'cCdProduto'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cNmProduto'
        DataType = ftString
        Size = 150
      end
      item
        Name = 'cEAN'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cEANFornec'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 248
    Top = 208
    Data = {
      900000009619E0BD01000000180000000400000000000300000090000A634364
      50726F6475746F01004900000001000557494454480200020014000A634E6D50
      726F6475746F0100490000000100055749445448020002009600046345414E01
      004900000001000557494454480200020014000A6345414E466F726E65630100
      4900000001000557494454480200020014000000}
    object cdsProdutosConferecCdProduto: TStringField
      FieldName = 'cCdProduto'
    end
    object cdsProdutosConferecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object cdsProdutosConferecEAN: TStringField
      FieldName = 'cEAN'
    end
    object cdsProdutosConferecEANFornec: TStringField
      FieldName = 'cEANFornec'
    end
  end
  object SP_CONSULTA_PRODUTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONSULTA_PRODUTO'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@cCdProduto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@nCdTabTipoProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmProduto'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 150
        Value = Null
      end
      item
        Name = '@cFlgTipoCod'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = Null
      end>
    Left = 312
    Top = 208
  end
  object qryTempProdutosBip: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosBip'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosBip (cCdProduto  varchar(20)'
      '                                  ,nQtdeBipada int)'
      '                         '
      'END'
      ''
      'SELECT *'
      '  FROM #TempProdutosBip')
    Left = 344
    Top = 208
    object qryTempProdutosBipcCdProduto: TStringField
      FieldName = 'cCdProduto'
    end
    object qryTempProdutosBipnQtdeBipada: TIntegerField
      FieldName = 'nQtdeBipada'
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosBip'#39') IS NULL) '#13#10'BEGIN'#13#10#13#10'  ' +
      '  CREATE TABLE #TempProdutosBip (cCdProduto  varchar(20)'#13#10'      ' +
      '                            ,nQtdeBipada int)'#13#10'                 ' +
      '        '#13#10'END'#13#10#13#10'TRUNCATE TABLE #TempProdutosBip'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 312
    Top = 240
  end
end
