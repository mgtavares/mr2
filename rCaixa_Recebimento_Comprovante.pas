unit rCaixa_Recebimento_Comprovante;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, DB, ADODB, jpeg, QRCtrls;

type
  TrptCaixa_Recebimento_Comprovante = class(TForm)
    QuickRep1: TQuickRep;
    QRBand3: TQRBand;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText1: TQRDBText;
    QRGroup1: TQRGroup;
    qryRecibo: TADOQuery;
    QRGroup2: TQRGroup;
    qryRecibonCdTitulo: TIntegerField;
    qryRecibocNrTit: TStringField;
    qryReciboiParcela: TIntegerField;
    qryRecibonValMov: TBCDField;
    qryRecibocNmTerceiro: TStringField;
    qryRecibocCNPJCPF: TStringField;
    qryRecibocRG: TStringField;
    qryRecibodDtMov: TDateTimeField;
    qryRecibonCdLoja: TIntegerField;
    qryRecibocNmTerceiroLoja: TStringField;
    qryRecibocMunicipioLoja: TStringField;
    qryRecibonValTotalPago: TBCDField;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText3: TQRDBText;
    qryRecibodDtVenc: TDateTimeField;
    qryRecibonCdConta: TStringField;
    qryRecibonCdLanctoFin: TStringField;
    qryRecibocExtenso: TStringField;
    QRDBText11: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand1: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel4: TQRLabel;
    procedure imprimeRecibo(nCdLanctoFin:integer);
    procedure QRBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptCaixa_Recebimento_Comprovante: TrptCaixa_Recebimento_Comprovante;

implementation

uses fMenu;

{$R *.dfm}

{ TrptCaixa_Recebimento_Comprovante }

procedure TrptCaixa_Recebimento_Comprovante.imprimeRecibo(
  nCdLanctoFin: integer);
begin

    qryRecibo.Close;
    qryRecibo.Parameters.ParamByName('nPK').Value := nCdLanctoFin ;
    qryRecibo.Open ;

    lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

    QuickRep1.PreviewModal;

    qryRecibo.Close ;

end;

procedure TrptCaixa_Recebimento_Comprovante.QRBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin

    if not (qryRecibo.Eof) then
        QuickRep1.NewPage;

end;

end.
