unit fModeloCartaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, cxLookAndFeelPainters, cxButtons,
  RDprint;

type
  TfrmModeloCartaCobranca = class(TfrmCadastro_Padrao)
    qryMasternCdModeloCarta: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmModeloCarta: TStringField;
    qryMasternCdTabTipoModeloCarta: TIntegerField;
    qryMasteriLinhaDestinatario: TIntegerField;
    qryMasteriColunaDestinatario: TIntegerField;
    qryMastercFlgNaoExibeDest: TIntegerField;
    qryMasteriLinhaTextoCorpo: TIntegerField;
    qryMastercTextoCorpo: TMemoField;
    qryMasteriLinhaTitulo: TIntegerField;
    qryMastercTextoPosTitulo: TMemoField;
    qryMastercFlgAtivo: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBMemo2: TDBMemo;
    DBCheckBox2: TDBCheckBox;
    GroupBox1: TGroupBox;
    Memo1: TMemo;
    Label9: TLabel;
    GroupBox2: TGroupBox;
    Memo2: TMemo;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    RDprint1: TRDprint;
    qryMastercTamanhoFonteDest: TStringField;
    qryMastercTamanhoFonteCorpo: TStringField;
    qryMastercTamanhoFonteRodape: TStringField;
    DBComboBox1: TDBComboBox;
    Label10: TLabel;
    Label11: TLabel;
    DBComboBox2: TDBComboBox;
    DBComboBox3: TDBComboBox;
    Label12: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBMemo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure DBEdit3Change(Sender: TObject);
    procedure DBEdit4Change(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModeloCartaCobranca: TfrmModeloCartaCobranca;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmModeloCartaCobranca.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MODELOCARTA' ;
  nCdTabelaSistema  := 91 ;
  nCdConsultaPadrao := 198 ;

end;

procedure TfrmModeloCartaCobranca.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmModeloCartaCobranca.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
end;

procedure TfrmModeloCartaCobranca.DBMemo1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmModeloCartaCobranca.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do modelo da carta.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  if (qryMastercFlgNaoExibeDest.Value = 0) then
  begin
      if (qryMasteriLinhaDestinatario.Value <= 0) then
      begin
          MensagemErro('A linha inicial dos dados do destinat�rio deve ser maior que zero.') ;
          DBEdit3.SetFocus;
          abort ;
      end;

      if (qryMasteriColunaDestinatario.Value <= 0) then
      begin
          MensagemErro('A coluna inicial dos dados do destinat�rio deve ser maior que zero.') ;
          DBEdit4.SetFocus;
          abort ;
      end;

      if (qryMastercTamanhoFonteDest.Value = '') then
      begin
          MensagemErro('Selecione o tamanho da fonte para identifica��o do destinat�rio.') ;
          DBComboBox2.SetFocus;
          abort;
      end ;

  end ;

  if (qryMastercTextoCorpo.Value <> '') and (qryMasteriLinhaTextoCorpo.Value <= 0) then
  begin
      MensagemErro('A linha inicial do texto do corpo da carta deve ser maior que zero.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (qryMasteriLinhaTextoCorpo.Value > 0) and (qryMasteriLinhaTextoCorpo.Value > qryMasteriLinhaDestinatario.Value) and (qryMasteriLinhaTextoCorpo.Value <= (qryMasteriLinhaDestinatario.Value + 3)) then
  begin
      MensagemErro('A linha inicial de impress�o do corpo da carta est� conflitando com os dados do destinat�rio.'+#13#13+'Linha M�nima Sugerida: ' + intToStr((qryMasteriLinhaDestinatario.Value + 4))) ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (qryMasteriLinhaTitulo.Value <= 0) then
  begin
      MensagemErro('A linha inicial de impress�o dos t�tulos vencidos deve ser maior que zero.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (qryMasteriLinhaTitulo.Value <= (qryMasteriLinhaTextoCorpo.Value + DBMemo1.Lines.Count)) then
  begin
      MensagemErro('A linha inicial de impress�o dos t�tulos vencidos est� conflitando com o corpo da carta.'+#13#13+'Linha M�nima Sugerida: ' + intToStr((qryMasteriLinhaTextoCorpo.Value + DBMemo1.Lines.Count)+1)) ;
      DBEdit6.SetFocus;
      abort ;
  end ;


  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdEmpresa.Value            := frmMenu.nCdEmpresaAtiva;
      qryMasternCdTabTipoModeloCarta.Value := 1 ;
      qryMastercFlgAtivo.Value             := 1 ;
  end ;

  inherited;


end;

procedure TfrmModeloCartaCobranca.DBCheckBox1Click(Sender: TObject);
begin
  inherited;

  if (DBCheckBox1.Checked) and (qryMaster.State <> dsBrowse) then
  begin
      qryMasteriLinhaDestinatario.Value  := 0;
      qryMasteriColunaDestinatario.Value := 0;
      qryMastercTamanhoFonteDest.Value   := '';
  end ;

end;

procedure TfrmModeloCartaCobranca.DBEdit3Change(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsEdit) then
      qryMastercFlgNaoExibeDest.Value := 0;

end;

procedure TfrmModeloCartaCobranca.DBEdit4Change(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsEdit) then
      qryMastercFlgNaoExibeDest.Value := 0 ;
  
end;

procedure TfrmModeloCartaCobranca.ToolButton10Click(Sender: TObject);
var
    iLinha, I : integer ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  rdPrint1.Abrir;

  {-- Imprime Identifica��o Cliente --}
  if (qryMastercFlgNaoExibeDest.Value = 0) then
  begin
      iLinha := qryMasteriLinhaDestinatario.Value ;

      if (qryMastercTamanhoFonteDest.Value = 'NORMAL') then
      begin
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'NOME COMPLETO',[Normal]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'ENDERECO, N�mero',[Normal]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'BAIRRO',[Normal]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'MUNICIPIO - ESTADO - CEP',[Normal]) ;
          iLinha := iLinha + 1 ;
      end ;

      if (qryMastercTamanhoFonteDest.Value = 'EXPANDIDO') then
      begin
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'NOME COMPLETO',[EXPANDIDO]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'ENDERECO, N�mero',[EXPANDIDO]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'BAIRRO',[EXPANDIDO]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'MUNICIPIO - ESTADO - CEP',[EXPANDIDO]) ;
          iLinha := iLinha + 1 ;
      end ;

      if (qryMastercTamanhoFonteDest.Value = 'COMP12') then
      begin
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'NOME COMPLETO',[COMP12]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'ENDERECO, N�mero',[COMP12]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'BAIRRO',[COMP12]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'MUNICIPIO - ESTADO - CEP',[COMP12]) ;
          iLinha := iLinha + 1 ;
      end ;

      if (qryMastercTamanhoFonteDest.Value = 'COMP17') then
      begin
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'NOME COMPLETO',[COMP17]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'ENDERECO, N�mero',[COMP17]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'BAIRRO',[COMP17]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'MUNICIPIO - ESTADO - CEP',[COMP17]) ;
          iLinha := iLinha + 1 ;
      end ;

      if (qryMastercTamanhoFonteDest.Value = 'COMP20') then
      begin
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'NOME COMPLETO',[COMP20]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'ENDERECO, N�mero',[COMP20]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'BAIRRO',[COMP20]) ;
          iLinha := iLinha + 1 ;
          rdPrint1.ImpF(iLinha,qryMasteriColunaDestinatario.Value,'MUNICIPIO - ESTADO - CEP',[COMP20]) ;
          iLinha := iLinha + 1 ;
      end ;

  end ;

  {-- Imprime corpo carta --}
  iLinha := qryMasteriLinhaTextoCorpo.Value ;

  for i := 1 to DBMemo1.Lines.Count do
  begin

      if (qryMastercTamanhoFonteCorpo.Value = 'NORMAL') then
          rdPrint1.ImpF(iLinha,1,DBMemo1.Lines.Strings[i-1],[NORMAL]) ;

      if (qryMastercTamanhoFonteCorpo.Value = 'EXPANDIDO') then
          rdPrint1.ImpF(iLinha,1,DBMemo1.Lines.Strings[i-1],[EXPANDIDO]) ;

      if (qryMastercTamanhoFonteCorpo.Value = 'COMP12') then
          rdPrint1.ImpF(iLinha,1,DBMemo1.Lines.Strings[i-1],[COMP12]) ;

      if (qryMastercTamanhoFonteCorpo.Value = 'COMP17') then
          rdPrint1.ImpF(iLinha,1,DBMemo1.Lines.Strings[i-1],[COMP17]) ;

      if (qryMastercTamanhoFonteCorpo.Value = 'COMP20') then
          rdPrint1.ImpF(iLinha,1,DBMemo1.Lines.Strings[i-1],[COMP20]) ;

      iLinha := iLinha + 1;
  end ;

  {-- Imprime os t�tulos --}
  iLinha := qryMasteriLinhaTitulo.Value ;

  rdPrint1.ImpD(iLinha,10,'T�tulo',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,12,'Parc.',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,16,'Dt. Compra',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,23,'Dt. Vencimento',[COMP17,negrito]) ;
  rdPrint1.ImpD(iLinha,36,'Valor',[COMP17,negrito]) ;

  rdPrint1.ImpD(iLinha,45,'T�tulo',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,47,'Parc.',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,51,'Dt. Compra',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,58,'Dt. Vencimento',[COMP17,negrito]) ;
  rdPrint1.ImpD(iLinha,71,'Valor',[COMP17,negrito]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  iLinha := iLinha + 1 ;

  rdPrint1.ImpD(iLinha,10,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,14,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,16,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,24,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,31,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.ImpD(iLinha,45,'0000012345',[COMP17]) ;
  rdPrint1.ImpD(iLinha,49,'01',[COMP17]) ;
  rdPrint1.ImpF(iLinha,51,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpF(iLinha,59,'01/01/1901',[COMP17]) ;
  rdPrint1.ImpVal(iLinha,66,'###,##0.00',0.01,[COMP17]) ;

  rdPrint1.Fechar;


end;

initialization
    RegisterClass(TfrmModeloCartaCobranca) ;

end.
