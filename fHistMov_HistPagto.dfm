inherited frmHistMov_HistPagto: TfrmHistMov_HistPagto
  Left = 252
  Top = 200
  Width = 729
  Height = 386
  Caption = 'Historico de Liquida'#231#227'o Parcial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 713
    Height = 319
  end
  inherited ToolBar1: TToolBar
    Width = 713
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object ListaInfPagto: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 713
    Height = 319
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsInfPagto
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 71
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'nValMov'
        Footers = <>
        Width = 69
      end
      item
        EditButtons = <>
        FieldName = 'dDtMov'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'nValJuro'
        Footers = <>
        Title.Caption = 'Valor Juros'
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 252
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 800
    Top = 8
  end
  object dsInfPagto: TDataSource
    DataSet = qryInfPag
    Left = 680
    Top = 32
  end
  object qryInfPag: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,mTitulo.nValMov'
      '      ,mTitulo.dDtMov '
      '      ,Titulo.nValJuro'
      ' --   ,Titulo.nValLiq'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM mTitulo'
      
        '       INNER JOIN Titulo        ON Titulo.nCdTitulo = mTitulo.nC' +
        'dTitulo'
      
        '       INNER JOIN ContaBancaria ON mTitulo.nCdContaBancaria = Co' +
        'ntaBancaria.nCdContaBancaria'
      
        '       INNER JOIN Usuario       ON Usuario.nCdUsuario = ContaBan' +
        'caria.nCdUsuarioOperador'
      'WHERE mTitulo.nCdTitulo = :nPk')
    Left = 648
    Top = 32
    object qryInfPagcNrTit: TStringField
      DisplayLabel = 'Carnet'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryInfPagiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryInfPagnValMov: TBCDField
      DisplayLabel = 'Valor Pagto'
      FieldName = 'nValMov'
      Precision = 12
      Size = 2
    end
    object qryInfPagdDtMov: TDateTimeField
      DisplayLabel = 'Data Pagto.'
      FieldName = 'dDtMov'
    end
    object qryInfPagnValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryInfPagnCdUsuario: TIntegerField
      DisplayLabel = 'Usuario|Codigo'
      FieldName = 'nCdUsuario'
    end
    object qryInfPagcNmUsuario: TStringField
      DisplayLabel = 'Usuario|Nome'
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
