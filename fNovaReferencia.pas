unit fNovaReferencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DB, ADODB;

type
  TfrmNovaReferencia = class(TfrmProcesso_Padrao)
    Edit1: TEdit;
    Label1: TLabel;
    qryAux: TADOQuery;
    usp_processo: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmNovaReferencia: TfrmNovaReferencia;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmNovaReferencia.ToolButton1Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a gera��o deste novo produto ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM Produto WHERE nCdProdutoPai = ' + IntToStr(nCdProduto) + ' AND cReferencia = ' + Chr(39) + UpperCase(Edit1.Text) + Chr(39)) ;
  qryAux.Open ;

  if not qryAux.Eof then
  begin
      ShowMessage('J� existe essa refer�ncia para este produto.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans ;
  
  try
      usp_processo.Close ;
      usp_processo.Parameters.ParamByName('@nCdProduto').Value := nCdProduto ;
      usp_processo.Parameters.ParamByName('@cReferencia').Value := Edit1.Text ;
      usp_Processo.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      usp_processo.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processo.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  Edit1.Text := '' ;

  Close ;

end;

end.
