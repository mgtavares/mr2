unit rVendaXVendedorAnalitico_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, QRPrev, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaXVendedorAnalitico_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    DetailBand1: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText11: TQRDBText;
    SPREL_VENDA_VENDEDOR_ANALITICO: TADOStoredProc;
    QRDBText12: TQRDBText;
    qryDescricaoPedido: TADOQuery;
    cmd_PreparaTemp: TADOCommand;
    dsDescricaoPedido: TDataSource;
    QRGroup1: TQRGroup;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRBand2: TQRBand;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape10: TQRShape;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRShape11: TQRShape;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    qryDescricaoPedidonCdPedido: TIntegerField;
    qryDescricaoPedidonCdLoja: TStringField;
    qryDescricaoPedidonCdTerceiroColab: TIntegerField;
    qryDescricaoPedidocNmVendedor: TStringField;
    qryDescricaoPedidodDtVenda: TDateTimeField;
    qryDescricaoPedidonValVenda: TBCDField;
    qryDescricaoPedidonValTroca: TBCDField;
    qryDescricaoPedidonValValeUsado: TBCDField;
    qryDescricaoPedidonValPedido: TBCDField;
    qryDescricaoPedidonValValeGerado: TBCDField;
    qryDescricaoPedidonValVendaFinal: TBCDField;
    qryDescricaoPedidonValTotalVenda: TBCDField;
    qryDescricaoPedidonValTotalTroca: TBCDField;
    qryDescricaoPedidonValSubtotal: TBCDField;
    qryDescricaoPedidonValTotalValeUsado: TBCDField;
    qryDescricaoPedidonValTotalValeGerado: TBCDField;
    qryDescricaoPedidonValTotalVendaFinal: TBCDField;
    qryDescricaoPedidonCdProduto: TIntegerField;
    qryDescricaoPedidonCdTipoItemPed: TIntegerField;
    qryDescricaoPedidocNmItem: TStringField;
    qryDescricaoPedidonQtdePed: TIntegerField;
    qryDescricaoPedidonValUnitario: TBCDField;
    qryDescricaoPedidonValDescAcresc: TBCDField;
    qryDescricaoPedidonValUnitFinal: TBCDField;
    qryDescricaoPedidonValTotal: TBCDField;
    QRShape2: TQRShape;
    qryDescricaoPedidonValAcrescimo: TBCDField;
    qryDescricaoPedidonValTotalAcrescimo: TBCDField;
    QRLabel13: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel14: TQRLabel;
    QRShape9: TQRShape;
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaXVendedorAnalitico_view: TrptVendaXVendedorAnalitico_view;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TrptVendaXVendedorAnalitico_view.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  QRLabel13.Enabled := False;
  QRDBText19.Enabled := False;
  QRLabel14.Enabled  := False;

  if (frmMenu.LeParametro('CONTACRESCMETA') = 'N') then
  begin
      QRLabel13.Enabled  := True;
      QRDBText19.Enabled := True;
      QRLabel14.Enabled  := True;
  end;
end;

end.
