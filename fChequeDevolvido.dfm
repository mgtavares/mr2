inherited frmChequeDevolvido: TfrmChequeDevolvido
  Left = 428
  Top = 278
  Width = 355
  Height = 196
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Cheque Devolvido'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 339
    Height = 131
  end
  object Label1: TLabel [1]
    Left = 46
    Top = 80
    Width = 78
    Height = 13
    Caption = 'Data Devolu'#231#227'o'
  end
  object Label2: TLabel [2]
    Left = 42
    Top = 104
    Width = 82
    Height = 13
    Caption = 'Data Vencimento'
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 128
    Width = 116
    Height = 13
    Caption = 'Valor Tarifas e Encargos'
  end
  inherited ToolBar1: TToolBar
    Width = 339
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object CheckBox1: TCheckBox [5]
    Left = 24
    Top = 48
    Width = 297
    Height = 17
    Caption = 'Cheque voltou para o terceiro respons'#225'vel '
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object MaskEdit1: TMaskEdit [6]
    Left = 128
    Top = 72
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
    OnKeyDown = MaskEdit1KeyDown
  end
  object MaskEdit2: TMaskEdit [7]
    Left = 128
    Top = 96
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
    OnKeyDown = MaskEdit2KeyDown
  end
  object MaskEdit3: TMaskEdit [8]
    Left = 128
    Top = 120
    Width = 73
    Height = 21
    TabOrder = 4
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object usp_Processo: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_CHEQUE_DEVOLVIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cFlgVoltaTerceiro'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end
      item
        Name = '@dDtDevol'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@dDtVencto'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@nValTarifa'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 224
    Top = 80
  end
end
