unit fCancelaBorderoRecebimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, StdCtrls, DB, ADODB;

type
  TfrmCancelaBorderoRecebimento = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    DBGridEh1: TDBGridEh;
    qryRemessas: TADOQuery;
    DataSource1: TDataSource;
    qryRemessasnCdBorderoFinanceiro: TIntegerField;
    qryRemessasdDtGeracao: TDateTimeField;
    qryRemessasnCdUsuarioGeracao: TIntegerField;
    qryRemessasnCdBanco: TIntegerField;
    qryRemessascNmBanco: TStringField;
    qryRemessascAgencia: TIntegerField;
    qryRemessasnCdConta: TStringField;
    qryRemessascNmTitular: TStringField;
    qryRemessasnValTotalTitulo: TBCDField;
    qryRemessascNmArquivoRemessa: TStringField;
    qryRemessascNmComputadorGeracao: TStringField;
    qryRemessasnQtdeTitulo: TIntegerField;
    qryVerificaBordero: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCancelaBorderoRecebimento: TfrmCancelaBorderoRecebimento;

implementation

uses fCancelaBorderoRecebimento_titulo;

{$R *.dfm}

procedure TfrmCancelaBorderoRecebimento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryRemessas.Close;
  qryRemessas.Parameters.ParamByName('cFlgListarRemessa').Value := RadioGroup1.ItemIndex;
  qryRemessas.Open;
  
end;

procedure TfrmCancelaBorderoRecebimento.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
end;

procedure TfrmCancelaBorderoRecebimento.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmCancelaBorderoRecebimento_titulo;
begin
  inherited;

  if (qryRemessasnCdBorderoFinanceiro.Value = 0) then
      exit;

  qryVerificaBordero.Close;
  PosicionaQuery(qryVerificaBordero,qryRemessasnCdBorderoFinanceiro.AsString);

  objForm := TfrmCancelaBorderoRecebimento_titulo.Create(nil);

  objForm.nCdBorderoFinanceiro := qryRemessasnCdBorderoFinanceiro.Value;
  
  objForm.qryTitulosRemessa.Close;
  objForm.PosicionaQuery(objForm.qryTitulosRemessa,qryRemessasnCdBorderoFinanceiro.AsString);

  {-- quando o border� j� foi cancelado permite apenas visualizar os titulos
   que o comp�em --}
  if (qryVerificaBordero.RecordCount > 0) then
      objForm.ToolButton1.Visible := False;

  showForm(objForm, True);

  ToolButton1.Click;
end;

procedure TfrmCancelaBorderoRecebimento.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  qryRemessas.Close;
  qryRemessas.Parameters.ParamByName('cFlgListarRemessa').Value := RadioGroup1.ItemIndex;
  qryRemessas.Open;
end;

initialization
  RegisterClass(TfrmCancelaBorderoRecebimento);

end.
