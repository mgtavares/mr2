object frmPedidoVendaSimplesDesconto: TfrmPedidoVendaSimplesDesconto
  Left = 337
  Top = 255
  Width = 319
  Height = 265
  BorderIcons = [biSystemMenu]
  Caption = 'Desconto'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 28
    Top = 108
    Width = 106
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 52
    Top = 68
    Width = 82
    Height = 21
    Alignment = taRightJustify
    Caption = '% Desconto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 28
    Top = 28
    Width = 106
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor da Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 41
    Top = 188
    Width = 93
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor L'#237'quido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 21
    Top = 148
    Width = 113
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor Acr'#233'scimo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object edtValDesconto: TcxCurrencyEdit
    Left = 144
    Top = 96
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 2
    OnKeyPress = edtValDescontoKeyPress
  end
  object edtPercent: TcxCurrencyEdit
    Left = 144
    Top = 56
    Width = 73
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00%;-,0.00%'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 1
    OnExit = edtPercentExit
    OnKeyPress = edtPercentKeyPress
  end
  object edtValVenda: TcxCurrencyEdit
    Left = 144
    Top = 16
    Width = 145
    Height = 33
    BeepOnEnter = False
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 0
  end
  object edtValliquido: TcxCurrencyEdit
    Left = 144
    Top = 176
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 3
    OnKeyPress = edtValliquidoKeyPress
  end
  object edtValAcrescimo: TcxCurrencyEdit
    Left = 144
    Top = 136
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 4
    OnExit = edtValAcrescimoExit
    OnKeyPress = edtValAcrescimoKeyPress
  end
end
