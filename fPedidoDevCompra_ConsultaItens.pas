unit fPedidoDevCompra_ConsultaItens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, ADODB, DBCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxLookAndFeelPainters, cxButtons, cxPC;

type
  TfrmPedidoDevCompra_ConsultaItens = class(TfrmProcesso_Padrao)
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    dsProduto: TDataSource;
    qryConsultaReceb: TADOQuery;
    qryConsultaRecebnCdRecebimento: TIntegerField;
    qryConsultaRecebdDtReceb: TDateTimeField;
    qryConsultaRecebcNrDocto: TStringField;
    qryConsultaRecebcNmTerceiro: TStringField;
    qryConsultaRecebnQtde: TBCDField;
    qryConsultaRecebnPercIPI: TBCDField;
    qryConsultaRecebnCdPedido: TIntegerField;
    dsConsultaReceb: TDataSource;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutocUnidadeMedidaCompra: TStringField;
    SP_INCLUI_ITEM_DEVOLUCAO: TADOStoredProc;
    qryConsultaRecebnCdTipoItemPed: TIntegerField;
    qryConsultaRecebnCdItemRecebimento: TAutoIncField;
    qryConsultaRecebnValUnitario: TFloatField;
    qryConsultaRecebnValUnitarioEsp: TFloatField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    edtDtFinal: TMaskEdit;
    edtDtInicial: TMaskEdit;
    edtProduto: TMaskEdit;
    edtQtde: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    cxButton1: TcxButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGrid1DBTableView1dDtReceb: TcxGridDBColumn;
    cxGrid1DBTableView1cNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nPercIPI: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    edtDtFinalNF: TMaskEdit;
    edtDtInicialNF: TMaskEdit;
    edtNumNF: TMaskEdit;
    cxButton2: TcxButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryConsultaRecebNF: TADOQuery;
    dsConsultaRecebNF: TDataSource;
    cxGridDBTableView1cCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmItem: TcxGridDBColumn;
    cxGridDBTableView1cUnidadeMedida: TcxGridDBColumn;
    qryConsultaRecebNFnCdRecebimento: TIntegerField;
    qryConsultaRecebNFdDtReceb: TDateTimeField;
    qryConsultaRecebNFcNrDocto: TStringField;
    qryConsultaRecebNFcNmTerceiro: TStringField;
    qryConsultaRecebNFcCdProduto: TStringField;
    qryConsultaRecebNFcNmItem: TStringField;
    qryConsultaRecebNFcUnidadeMedida: TStringField;
    qryConsultaRecebNFnQtde: TFloatField;
    qryConsultaRecebNFnValUnitario: TFloatField;
    qryConsultaRecebNFnValUnitarioEsp: TFloatField;
    qryConsultaRecebNFnPercIPI: TFloatField;
    qryConsultaRecebNFnCdPedido: TIntegerField;
    qryConsultaRecebNFnCdTipoItemPed: TIntegerField;
    qryConsultaRecebNFnCdItemRecebimento: TIntegerField;
    qryConsultaRecebNFcUFOrigemNF: TStringField;
    qryConsultaRecebNFcChaveNfe: TStringField;
    qryConsultaRecebcChaveNfe: TStringField;
    qrySaldoItemDevoluc: TADOQuery;
    qrySaldoItemDevolucnQtdePed: TBCDField;
    qryConsultaRecebnCdProduto: TIntegerField;
    qryConsultaRecebNFnCdProduto: TIntegerField;
    procedure edtProdutoChange(Sender: TObject);
    procedure edtQtdeChange(Sender: TObject);
    procedure edtDtInicialChange(Sender: TObject);
    procedure edtDtFinalChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure edtProdutoExit(Sender: TObject);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtQtdeEnter(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure VerificaSaldoProduto(nCdProduto : Integer; cChaveNfe : String);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTipoPedido : integer ;
    nCdPedido     : integer ;
    nCdTerceiroAux: integer ;
    nCdLoja       : Integer;
  end;

var
  frmPedidoDevCompra_ConsultaItens: TfrmPedidoDevCompra_ConsultaItens;

implementation

uses fMenu, fLookup_Padrao, fPedidoDevCompra;

{$R *.dfm}

procedure TfrmPedidoDevCompra_ConsultaItens.VerificaSaldoProduto(nCdProduto : Integer; cChaveNfe : String);
begin

  if Trim(cChaveNfe) <> '' then
  begin

      qrySaldoItemDevoluc.Close;
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdTipoPedido').Value := nCdTipoPedido;
      qrySaldoItemDevoluc.Parameters.ParamByName('cDoctoCompra').Value  := cChaveNfe;
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdPedido').Value     := nCdPedido;
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdProduto').Value    := nCdProduto;
      qrySaldoItemDevoluc.Open;

      if qrySaldoItemDevolucnQtdePed.Value > 0 then
      begin
          case MessageDlg('Item selecionado j� foi devolvido anteriormente, deseja continuar ?' + #13#13 + 'Total Dev.: ' + qrySaldoItemDevolucnQtdePed.AsString, mtConfirmation,[mbYes,mbNo],0) of
              mrNo:Abort ;
          end ;
      end;
  end;
  
end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtProdutoChange(
  Sender: TObject);
begin
  inherited;

  qryConsultaReceb.Close ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtQtdeChange(Sender: TObject);
begin
  inherited;
  qryConsultaReceb.Close ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtDtInicialChange(
  Sender: TObject);
begin
  inherited;
  qryConsultaReceb.Close ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtDtFinalChange(
  Sender: TObject);
begin
  inherited;
  qryConsultaReceb.Close ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.FormShow(Sender: TObject);
begin
  inherited;

  qryConsultaReceb.Close ;
  qryConsultaRecebNF.Close;
  qryProduto.Close ;

  cxPageControl1.ActivePageIndex := 0 ;
  edtProduto.SetFocus;
  
end;

procedure TfrmPedidoDevCompra_ConsultaItens.cxButton1Click(
  Sender: TObject);
begin
  inherited;

  if (dbEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o produto.') ;
      edtProduto.SetFocus ;
      abort ;
  end ;

  if (frmMenu.ConvInteiro(edtQtde.Text) <= 0) then
  begin
      MensagemAlerta('Informe a quantidade a devolver.') ;
      edtQtde.SetFocus ;
      abort ;
  end ;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date - 30) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  qryConsultaReceb.Close ;
  qryConsultaReceb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryConsultaReceb.Parameters.ParamByName('nCdLoja').Value    := nCdLoja;
  qryConsultaReceb.Parameters.ParamByName('nCdProduto').Value := frmMenu.ConvInteiro(edtProduto.Text) ;
  qryConsultaReceb.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtInicial.Text) ;
  qryConsultaReceb.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFinal.Text) ;
  qryConsultaReceb.Open ;

  if qryConsultaReceb.Eof then
  begin
      MensagemAlerta('Nenhum recebimento para o crit�rio informado.') ;
      edtProduto.SetFocus;
      abort ;
  end ;

  cxGrid1.SetFocus ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtProdutoExit(
  Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := nCdTipoPedido ;
  PosicionaQuery(qryProduto, edtProduto.Text) ;
  
end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtProdutoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(76,'((Produto.nCdTabTipoProduto = 3) OR (Produto.nCdGrade IS NULL)) AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido   = ' + IntToStr(nCdTipoPedido) + ')');

        If (nPK > 0) then
            edtProduto.Text := IntToStr(nPK) ;
    
    end ;

  end ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.edtQtdeEnter(Sender: TObject);
begin
  inherited;

  if (frmMenu.ConvInteiro(edtQtde.Text) <= 0) then
      edtQtde.Text := '1' ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  VerificaSaldoProduto(qryConsultaRecebnCdProduto.Value,qryConsultaRecebcChaveNfe.Value);

  if not qryConsultaReceb.Eof then
  begin

      case MessageDlg('Confirma a inclus�o do produto na devolu��o ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          SP_INCLUI_ITEM_DEVOLUCAO.Close ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdPedido').Value           := nCdPedido ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdProduto').Value          := qryProdutonCdProduto.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cNmItem').Value             := qryProdutocNmProduto.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cSiglaUnidadeMedida').Value := qryProdutocUnidadeMedida.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nQtdePed').Value            := frmMenu.ConvInteiro(edtQtde.Text) ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitario').Value        := qryConsultaRecebnValUnitario.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitarioEsp').Value     := qryConsultaRecebnValUnitarioEsp.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nPercIPI').Value            := qryConsultaRecebnPercIPI.Value  ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdItem').Value             := qryConsultaRecebnCdItemRecebimento.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdTipoItemPed').Value      := qryConsultaRecebnCdTipoItemPed.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cUFOrigem').Value           := '';
          SP_INCLUI_ITEM_DEVOLUCAO.ExecProc;
      except
          MensagemErro('Erro no processamento.') ;
          frmMenu.Connection.RollbackTrans;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryConsultaReceb.Close ;
      qryProduto.Close ;
      edtProduto.Text := '' ;
      edtQtde.Text    := '' ;
      edtProduto.SetFocus ;

  end ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.ToolButton2Click(
  Sender: TObject);
begin
  qryConsultaReceb.Close ;
  inherited;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.cxButton2Click(
  Sender: TObject);
begin
  inherited;

  try
      strToint( trim( edtNumNF.Text ) ) ;
  except
      MensagemAlerta('N�mero de NF de entrada inv�lido.') ;
      edtNumNF.Setfocus;
      abort;
  end ;

  if (trim(edtDtInicialNF.Text) = '/  /') then
      edtDtInicialNF.Text := DateToStr(Date - 30) ;

  if (trim(edtDtFinalNF.Text) = '/  /') then
      edtDtFinalNF.Text := DateToStr(Date) ;

  qryConsultaRecebNF.Close ;
  qryConsultaRecebNF.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryConsultaRecebNF.Parameters.ParamByName('nCdLoja').Value     := nCdLoja;
  qryConsultaRecebNF.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiroAux ;
  qryConsultaRecebNF.Parameters.ParamByName('cNrDocto').Value    := frmMenu.ConvInteiro( trim( edtNumNF.Text ) ) ;
  qryConsultaRecebNF.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(edtDtInicialNF.Text) ;
  qryConsultaRecebNF.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(edtDtFinalNF.Text) ;
  qryConsultaRecebNF.Open ;

  if qryConsultaRecebNF.Eof then
  begin
      MensagemAlerta('Nenhum recebimento para o crit�rio informado.') ;
      edtNumNF.SetFocus;
      abort ;
  end ;

  cxGrid2.SetFocus ;

end;

procedure TfrmPedidoDevCompra_ConsultaItens.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  cQtdeDevolv : string ;
begin
  inherited;

  VerificaSaldoProduto(qryConsultaRecebNFnCdProduto.Value,qryConsultaRecebNFcChaveNfe.Value);

  if (qryConsultaRecebNF.RecordCount > 0) then
  begin

      case MessageDlg('Confirma a inclus�o do produto na devolu��o ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      cQtdeDevolv := '1' ;

      InputQuery('Devolu��o de Compra','Quantidade Devolvida:',cQtdeDevolv) ;

      try
          strTofloat( cQtdeDevolv ) ;
      except
          MensagemAlerta('A quantidade informada n�o � n�mero v�lido.') ;
          abort ;
      end ;

      if (strTofloat( cQtdeDevolv ) > qryConsultaRecebNFnQtde.Value ) then
      begin
          MensagemAlerta('A quantidade informada � maior que a quantidade recebida.') ;
          abort ;
      end;

      try
          frmMenu.Connection.BeginTrans;

          SP_INCLUI_ITEM_DEVOLUCAO.Close ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdPedido').Value           := nCdPedido ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdProduto').Value          := 0 ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cNmItem').Value             := qryConsultaRecebNFcNmItem.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cSiglaUnidadeMedida').Value := qryConsultaRecebNFcUnidadeMedida.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nQtdePed').Value            := strTofloat( cQtdeDevolv ) ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitario').Value        := qryConsultaRecebNFnValUnitario.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nValUnitarioEsp').Value     := qryConsultaRecebNFnValUnitarioEsp.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nPercIPI').Value            := qryConsultaRecebNFnPercIPI.Value  ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdItem').Value             := qryConsultaRecebNFnCdItemRecebimento.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@nCdTipoItemPed').Value      := qryConsultaRecebNFnCdTipoItemPed.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.Parameters.ParamByName('@cUFOrigem').Value           := qryConsultaRecebNFcUFOrigemNF.Value ;
          SP_INCLUI_ITEM_DEVOLUCAO.ExecProc;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Produto incluso com sucesso.');
      except
          MensagemErro('Erro no processamento.') ;
          frmMenu.Connection.RollbackTrans;
          raise ;
      end ;

      qryConsultaRecebNF.Close ;
      qryProduto.Close ;
      edtNumNF.SetFocus ;
      
  end ;

end;

end.
