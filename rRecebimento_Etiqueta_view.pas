unit rRecebimento_Etiqueta_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, DB, DBClient, QRCtrls, SvQrBarcode;

type
  TrptRecebimento_Etiqueta_view = class(TForm)
    QuickRep1: TQuickRep;
    cdsEtiquetas: TClientDataSet;
    DetailBand1: TQRBand;
    cdsEtiquetascCdBarra1: TStringField;
    cdsEtiquetascNmProduto1: TStringField;
    cdsEtiquetascReferencia1: TStringField;
    cdsEtiquetascMsg1: TStringField;
    cdsEtiquetascNmLoja1: TStringField;
    cdsEtiquetascCdbarra2: TStringField;
    cdsEtiquetascNmProduto2: TStringField;
    cdsEtiquetascReferencia2: TStringField;
    cdsEtiquetascMsg2: TStringField;
    cdsEtiquetascNmLoja2: TStringField;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    SQBarra1: TSvQRBarcode;
    SQBarra2: TSvQRBarcode;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    SQBarra3: TSvQRBarcode;
    SQBarra4: TSvQRBarcode;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    SQBarra5: TSvQRBarcode;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    cdsEtiquetascCdBarra3: TStringField;
    cdsEtiquetascCdBarra4: TStringField;
    cdsEtiquetascCdBarra5: TStringField;
    cdsEtiquetascReferencia3: TStringField;
    cdsEtiquetascReferencia4: TStringField;
    cdsEtiquetascReferencia5: TStringField;
    cdsEtiquetascNmProduto3: TStringField;
    cdsEtiquetascNmProduto4: TStringField;
    cdsEtiquetascNmProduto5: TStringField;
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRDBText1Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure SQBarra3BeforePrint(sender: TObject);
    procedure SQBarra4BeforePrint(sender: TObject);
    procedure SQBarra5BeforePrint(sender: TObject);
    procedure SQBarra2BeforePrint(sender: TObject);
    procedure SQBarra1BeforePrint(sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRecebimento_Etiqueta_view: TrptRecebimento_Etiqueta_view;

implementation

{$R *.dfm}

procedure TrptRecebimento_Etiqueta_view.QRDBText3Print(sender: TObject;
  var Value: String);
begin
{    SQBarra1.Text := cdsEtiquetascCdBarra1.Value ;

    SQBarra1.Enabled := True ;

    if (SQBarra1.Text = '') then
        SQBarra1.Enabled := False ;}
end;

procedure TrptRecebimento_Etiqueta_view.QRDBText1Print(sender: TObject;
  var Value: String);
begin

{    SQBarra2.Text := cdsEtiquetascCdBarra2.Value ;

    SQBarra2.Enabled := True ;

    if (SQBarra2.Text = '') then
        SQBarra2.Enabled := False ;}
end;

procedure TrptRecebimento_Etiqueta_view.QRDBText8Print(sender: TObject;
  var Value: String);
begin
{    SQBarra3.Text := cdsEtiquetascCdBarra3.Value ;

    SQBarra3.Enabled := True ;

    if (SQBarra3.Text = '') then
        SQBarra3.Enabled := False ;}

end;

procedure TrptRecebimento_Etiqueta_view.QRDBText5Print(sender: TObject;
  var Value: String);
begin
{    SQBarra4.Text := cdsEtiquetascCdBarra4.Value ;

    SQBarra4.Enabled := True ;

    if (SQBarra4.Text = '') then
        SQBarra4.Enabled := False ;}

end;

procedure TrptRecebimento_Etiqueta_view.QRDBText9Print(sender: TObject;
  var Value: String);
begin
{    SQBarra5.Text := cdsEtiquetascCdBarra5.Value ;

    SQBarra5.Enabled := True ;

    if (SQBarra5.Text = '') then
        SQBarra5.Enabled := False ;}

end;

procedure TrptRecebimento_Etiqueta_view.SQBarra3BeforePrint(
  sender: TObject);
begin

    SQBarra3.Text := cdsEtiquetascCdBarra3.Value ;

end;

procedure TrptRecebimento_Etiqueta_view.SQBarra4BeforePrint(
  sender: TObject);
begin

    SQBarra4.Text := cdsEtiquetascCdBarra4.Value ;

end;

procedure TrptRecebimento_Etiqueta_view.SQBarra5BeforePrint(
  sender: TObject);
begin

    SQBarra5.Text := cdsEtiquetascCdBarra5.Value ;

end;

procedure TrptRecebimento_Etiqueta_view.SQBarra2BeforePrint(
  sender: TObject);
begin
    SQBarra2.Text := cdsEtiquetascCdBarra2.Value ;

end;

procedure TrptRecebimento_Etiqueta_view.SQBarra1BeforePrint(
  sender: TObject);
begin
    SQBarra1.Text := cdsEtiquetascCdBarra1.Value ;

end;

end.
