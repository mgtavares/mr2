inherited rptRelMovimentoCaixaAnalitico: TrptRelMovimentoCaixaAnalitico
  Left = 226
  Top = 200
  Caption = 'rptRelMovimentoCaixaAnalitico'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 80
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 41
    Top = 72
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Caixa'
  end
  object Label3: TLabel [3]
    Left = 9
    Top = 120
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Movimento'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 120
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [5]
    Left = 4
    Top = 96
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Lan'#231'amento'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 176
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 7
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 176
    Top = 64
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = dsContaBancaria
    TabOrder = 8
  end
  object MaskEdit1: TMaskEdit [9]
    Left = 104
    Top = 112
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 216
    Top = 112
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object Edit1: TEdit [11]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 1
    OnExit = Edit1Exit
    OnKeyDown = Edit1KeyDown
  end
  object Edit2: TEdit [12]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    TabOrder = 2
    OnExit = Edit2Exit
    OnKeyDown = Edit2KeyDown
  end
  object Edit3: TEdit [13]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    TabOrder = 3
    OnExit = Edit3Exit
    OnKeyDown = Edit3KeyDown
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 176
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmTipoLancto'
    DataSource = dsTipoLancto
    TabOrder = 9
  end
  object RadioGroup1: TRadioGroup [15]
    Left = 104
    Top = 144
    Width = 153
    Height = 33
    Caption = 'S'#243' Lan'#231'amento Estornado ?'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 6
  end
  inherited ImageList1: TImageList
    Left = 464
    Top = 368
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgSomenteEstorno'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTipoLancto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial         varchar(10)'
      '       ,@dDtFinal           varchar(10)'
      '       ,@nCdLoja            integer'
      '       ,@nCdContaBancaria   integer'
      '       ,@cFlgSomenteEstorno integer'
      '       ,@nCdTipoLancto      integer'
      ''
      'SET @dDtInicial         =:dDtInicial'
      'SET @dDtFinal           =:dDtFinal'
      'SET @nCdLoja            =:nCdLoja'
      'SET @nCdContaBancaria   =:nCdContaBancaria'
      'SET @cFlgSomenteEstorno =:cFlgSomenteEstorno'
      'SET @nCdTipoLancto      =:nCdTipoLancto'
      ''
      '    SELECT nCdLanctoFin'
      '          ,dDtLancto'
      '          ,nCdConta'
      
        '          ,(Convert(varchar,LanctoFin.nCdTipoLancto) + '#39' - '#39' + T' +
        'ipoLancto.cNmTipoLancto) AS TipoLanctoFin'
      '          ,Usuario.cNmUsuario'
      '          ,CASE'
      '             WHEN dDtEstorno is null     THEN 0'
      '             WHEN dDtEstorno is not null THEN 1'
      '           END AS cFlgEstorno'
      '      FROM LanctoFin'
      
        '           INNER JOIN ContaBancaria ON ContaBancaria.ncdContaBan' +
        'caria = LanctoFin.ncdContaBancaria'
      
        '           INNER JOIN TipoLancto    ON TipoLancto.nCdTipoLancto ' +
        '      = LanctoFin.nCdTipoLancto'
      
        '           INNER JOIN Usuario       ON Usuario.nCdUsuario       ' +
        '      = LanctoFin.nCdUsuario'
      
        '     WHERE (nCdLoja                    = @nCdLoja          OR @n' +
        'CdLoja          = 0)'
      
        '       AND (LanctoFin.nCdContaBancaria = @nCdContaBancaria OR @n' +
        'CdContaBancaria = 0)'
      
        '       AND LanctoFin.dDtLancto        >= Convert(DATETIME,@dDtIn' +
        'icial,103)'
      
        '       AND LanctoFin.dDtLancto         < (Convert(DATETIME,@dDtF' +
        'inal,103)+1)'
      
        '       AND (not isNull(dDtEstorno,1)    = @cFlgSomenteEstorno OR' +
        ' @cFlgSomenteEstorno = 0)'
      
        '       AND (LanctoFin.nCdTipoLancto     = @nCdTipoLancto      OR' +
        ' @nCdTipoLancto      = 0)')
    Left = 464
    Top = 152
    object qryLanctoFinnCdLanctoFin: TAutoIncField
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryLanctoFindDtLancto: TDateTimeField
      FieldName = 'dDtLancto'
    end
    object qryLanctoFinnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryLanctoFinTipoLanctoFin: TStringField
      FieldName = 'TipoLanctoFin'
      ReadOnly = True
      Size = 83
    end
    object qryLanctoFincNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLanctoFincFlgEstorno: TIntegerField
      FieldName = 'cFlgEstorno'
      ReadOnly = True
    end
  end
  object qryPedidos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Pedido.nCdPedido'
      '      ,nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nValUnitario'
      '      ,nValTotalItem'
      '  FROM ItemPedido '
      
        '       INNER JOIN Pedido ON Pedido.nCdPedido = ItemPedido.nCdPed' +
        'ido '
      ' WHERE Pedido.nCdLanctoFin =:nCdLanctoFin')
    Left = 504
    Top = 152
    object qryPedidosnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryPedidoscNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryPedidosnQtdePed: TBCDField
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryPedidosnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryPedidosnValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryPedidosnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmFormaPagto'
      '      ,cNmCondPagto'
      '      ,nValPagto'
      '      ,nValDescontoCondPagto'
      '      ,nValDescontoManual'
      '      ,nValJurosCondPagto'
      '      ,cFlgEntrada'
      '  FROM formaPagtoLanctoFin'
      
        '       INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = Forma' +
        'PagtoLanctoFin.nCdFormaPagto'
      
        '       INNER JOIN CondPagto  ON CondPagto.nCdCondPagto   = Forma' +
        'PagtoLanctoFin.nCdCondPagto'
      ' WHERE FormaPagtoLanctoFin.nCdLanctoFin =:nCdLanctoFin')
    Left = 544
    Top = 152
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryFormaPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryFormaPagtonValPagto: TBCDField
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
    object qryFormaPagtonValDescontoCondPagto: TBCDField
      FieldName = 'nValDescontoCondPagto'
      Precision = 12
      Size = 2
    end
    object qryFormaPagtonValDescontoManual: TBCDField
      FieldName = 'nValDescontoManual'
      Precision = 12
      Size = 2
    end
    object qryFormaPagtonValJurosCondPagto: TBCDField
      FieldName = 'nValJurosCondPagto'
      Precision = 12
      Size = 2
    end
    object qryFormaPagtocFlgEntrada: TIntegerField
      FieldName = 'cFlgEntrada'
    end
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNrTit'
      '      ,nCdTitulo'
      '      ,iParcela'
      '      ,dDtVenc'
      '      ,nValTit'
      
        '      ,Convert(varchar,Terceiro.nCdTerceiro) + '#39' - '#39' + cNmTercei' +
        'ro AS cTerceiro'
      'FROM Titulo'
      
        '     INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdTer' +
        'ceiro'
      'WHERE nCdLanctoFin =:nCdLanctoFin'
      '  AND Titulo.nCdCrediario IS NOT NULL')
    Left = 584
    Top = 152
    object qryParcelascNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelasdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryParcelasnValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryParcelascTerceiro: TStringField
      FieldName = 'cTerceiro'
      ReadOnly = True
      Size = 83
    end
  end
  object RDprint1: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = True
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'EDGAR DE SOUZA'
    RegistroUsuario.SerieProduto = 'SINGLE-0708/01624'
    RegistroUsuario.AutorizacaoKey = 'BWVI-4846-SAHU-8864-PAPQ'
    About = 'RDprint 4.0e - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = True
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    OnNewPage = RDprint1NewPage
    Left = 344
    Top = 296
  end
  object qryRecebParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT MTitulo.nCdTitulo'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValLiq'
      '      ,Titulo.nValJuro'
      
        '      ,Convert(varchar,Terceiro.nCdTerceiro) + '#39' - '#39' + Terceiro.' +
        'cNmTerceiro AS cTerceiro'
      '  FROM MTitulo'
      
        '       INNER JOIN Titulo   ON Titulo.nCdTitulo     = MTitulo.nCd' +
        'Titulo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE MTitulo.ncdLanctofin =:nCdLanctoFin'
      '   AND MTitulo.nCdOperacao != dbo.fn_leparametro('#39'OPERJUROCRED'#39')'
      '   AND Titulo.nCdCrediario IS NOT NULL')
    Left = 624
    Top = 152
    object qryRecebParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryRecebParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryRecebParcelasdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryRecebParcelasnValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryRecebParcelasnValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryRecebParcelascTerceiro: TStringField
      FieldName = 'cTerceiro'
      ReadOnly = True
      Size = 83
    end
  end
  object qryValeGerado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdVale'
      '      ,nValVale'
      'FROM Vale'
      'WHERE nCdLanctoFin =:nCdLanctoFin')
    Left = 664
    Top = 152
    object qryValeGeradonCdVale: TAutoIncField
      FieldName = 'nCdVale'
      ReadOnly = True
    end
    object qryValeGeradonValVale: TBCDField
      FieldName = 'nValVale'
      Precision = 12
      Size = 2
    end
  end
  object qryValeBaixado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdVale'
      '      ,nValVale'
      'FROM Vale'
      'WHERE nCdLanctoFinBaixa =:nCdLanctoFin')
    Left = 704
    Top = 152
    object qryValeBaixadonCdVale: TAutoIncField
      FieldName = 'nCdVale'
      ReadOnly = True
    end
    object qryValeBaixadonValVale: TBCDField
      FieldName = 'nValVale'
      Precision = 12
      Size = 2
    end
  end
  object qryLanctoDiversos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nValLancto'
      '      ,cHistorico '
      '  FROM LanctoFin'
      ' WHERE nCdLanctoFin =:nCdLanctoFin'
      '   AND cHistorico is not Null')
    Left = 744
    Top = 152
    object qryLanctoDiversosnValLancto: TBCDField
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
    object qryLanctoDiversoscHistorico: TStringField
      FieldName = 'cHistorico'
      Size = 50
    end
  end
  object qryPagtoCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdBanco'
      '      ,Cheque.cAgencia'
      '      ,Cheque.cConta'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      
        '      ,Convert(varchar,Terceiro.nCdTerceiro) + '#39' - '#39' + cNmTercei' +
        'ro AS cTerceiro '
      '  FROM Cheque '
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdT' +
        'erceiroResp'
      ' WHERE ncdLanctoFin =:nCdLanctoFin')
    Left = 784
    Top = 152
    object qryPagtoChequenCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryPagtoChequecAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryPagtoChequecConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryPagtoChequeiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryPagtoChequenValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryPagtoChequecTerceiro: TStringField
      FieldName = 'cTerceiro'
      ReadOnly = True
      Size = 83
    end
  end
  object qryChequeResgate: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdBanco'
      '      ,Cheque.cAgencia'
      '      ,Cheque.cConta'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      
        '      ,Convert(varchar,Terceiro.nCdTerceiro) + '#39' - '#39' + cNmTercei' +
        'ro AS cTerceiro '
      '  FROM Cheque '
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdT' +
        'erceiroResp'
      ' WHERE ncdLanctoFinResgate =:nCdLanctoFin')
    Left = 824
    Top = 152
    object qryChequeResgatenCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequeResgatecAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequeResgatecConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequeResgateiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryChequeResgatenValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequeResgatecTerceiro: TStringField
      FieldName = 'cTerceiro'
      ReadOnly = True
      Size = 83
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja =:nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja'
      '               WHERE UsuarioLoja.nCdLoja = Loja.nCdLoja'
      '                 AND UsuarioLoja.nCdUsuario =:nCdUsuario)')
    Left = 344
    Top = 152
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja   =:nCdLoja'
      '   AND cFlgCaixa = 1'
      '   AND nCdContaBancaria =:nPK')
    Left = 384
    Top = 152
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 360
    Top = 184
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 400
    Top = 184
  end
  object qryTipoLancto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoLancto'
      '      ,cNmTipoLancto'
      'FROM TipoLancto'
      'WHERE nCdTipoLancto  =:nPK'
      '  AND cFlgContaCaixa = 1')
    Left = 424
    Top = 152
    object qryTipoLanctonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryTipoLanctocNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
  end
  object dsTipoLancto: TDataSource
    DataSet = qryTipoLancto
    Left = 440
    Top = 184
  end
end
