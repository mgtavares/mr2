unit dcCompraFornecPeriodo_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, PivotMap_SRC, PivotCube_SRC, PivotGrid_SRC, PivotToolBar_SRC,
  Menus,comObj, ExcelXP, Jpeg;

type
  TdcmCompraFornecPeriodo_view = class(TfrmProcesso_Padrao)
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    PVColToolBar1: TPVColToolBar;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1cNmTerceiroRepres: TStringField;
    ADODataSet1cNmDepartamento: TStringField;
    ADODataSet1cNmCategoria: TStringField;
    ADODataSet1cNmSubCategoria: TStringField;
    ADODataSet1cNmSegmento: TStringField;
    ADODataSet1cNmMarca: TStringField;
    ADODataSet1cNmLinha: TStringField;
    ADODataSet1cNmClasseProduto: TStringField;
    ADODataSet1nValor: TBCDField;
    ADODataSet1cNmProduto: TStringField;
    PivotGrid1: TPivotGrid;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    ADODataSet1nQtde: TBCDField;
    procedure ToolButton5Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmCompraFornecPeriodo_view: TdcmCompraFornecPeriodo_view;

implementation

{$R *.dfm}

procedure TdcmCompraFornecPeriodo_view.ToolButton5Click(Sender: TObject);
begin
  inherited;
  PivotGrid1.Print(Rect(20,5,5,5),True,pqFlat,False);

end;

procedure TdcmCompraFornecPeriodo_view.MenuItem1Click(Sender: TObject);
var
  Planilha  : Olevariant;

begin
  inherited;

    try
        planilha:= CreateoleObject('Excel.Application');
    except
        MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
        exit ;
    end ;

    PivotGrid1.ExportToExcel(Planilha,True,True,True,'ER2Soft - An�lise de Compras por Fornecedor');

    Planilha.columns.Autofit;
    Planilha.Visible := True;

end;

procedure TdcmCompraFornecPeriodo_view.MenuItem2Click(Sender: TObject);
var
   image : TJPEGImage;
   jpg_array : array of TJpegImage;
begin
    inherited;

   if PivotMap1.Active then
   begin
       image := TJPegImage.Create;
       Setlength(jpg_array,2);
       {PivotChart1.SaveToJpegImage(image);}
       jpg_array[0] := image;
       PivotGrid1.ExportToHTML('',True,True,jpg_array,'ER2Soft - An�lise de Compras por Fornecedor');
       image.Free;
       setLength(jpg_array,0);
   end;

end;

end.
