unit fLoteLiqCobradora_Cliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, StdCtrls,
  Mask, DB, DBCtrls, ADODB;

type
  TfrmLoteLiqCobradora_Cliente = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtCliente: TMaskEdit;
    Label5: TLabel;
    Label1: TLabel;
    cmbTipoBaixa: TComboBox;
    Label2: TLabel;
    qryCliente: TADOQuery;
    qryClientenCdTerceiro: TIntegerField;
    qryClientecNmTerceiro: TStringField;
    qryClientecCNPJCPF: TStringField;
    DBEdit1: TDBEdit;
    dsCliente: TDataSource;
    DBEdit2: TDBEdit;
    qrySomaTit: TADOQuery;
    qrySomaTitnSaldoTit: TBCDField;
    qryValidaCliente: TADOQuery;
    qryValidaClienteCOLUMN1: TIntegerField;
    edtValorBaixa: TcxCurrencyEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cmbTipoBaixaChange(Sender: TObject);
    procedure qryClienteAfterOpen(DataSet: TDataSet);
    procedure edtClienteExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bValidado: boolean ;
    nCdCobradora: integer;
    nCdLoteLiqCobradora: integer;
  end;

var
  frmLoteLiqCobradora_Cliente: TfrmLoteLiqCobradora_Cliente;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmLoteLiqCobradora_Cliente.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione o cliente.') ;
      edtCliente.SetFocus;
      abort ;
  end ;

  if (cmbTipoBaixa.ItemIndex = -1) then
  begin
      MensagemAlerta('Selecione o tipo de baixa.') ;
      cmbTipoBaixa.SetFocus;
      abort ;
  end ;

  if (StrToCurr(edtValorBaixa.Text) <= 0) then
  begin
      MensagemAlerta('Informe o valor da baixa.') ;
      edtValorBaixa.SetFocus;
      abort ;
  end ;

  if ((cmbTipoBaixa.ItemIndex = 0) and (StrToCurr(edtValorBaixa.Text) < qrySomaTitnSaldoTit.Value)) then
  begin
      MensagemAlerta('O valor da baixa total n�o pode ser inferior ao valor do saldo do t�tulo.');
      edtValorBaixa.SetFocus;
      abort;
  end;

  if ((cmbTipoBaixa.ItemIndex = 1) and (StrToCurr(edtValorBaixa.Text) >= qrySomaTitnSaldoTit.Value)) then
  begin
      MensagemAlerta('O valor da baixa parcial deve ser menor do que o saldo total do t�tulo.');
      edtValorBaixa.SetFocus;
      abort;
  end;

  bValidado := True ;
  Close ;

end;

procedure TfrmLoteLiqCobradora_Cliente.FormShow(Sender: TObject);
begin
  inherited;

  bValidado := false ;

  qryCliente.Close;
  edtCliente.Text     := '' ;
  edtValorBaixa.Value := 0 ;

  edtCliente.SetFocus ;

end;

procedure TfrmLoteLiqCobradora_Cliente.ToolButton2Click(Sender: TObject);
begin
  bValidado := False ;
  inherited;

end;

procedure TfrmLoteLiqCobradora_Cliente.edtClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        If (nPK > 0) then
            edtCliente.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmLoteLiqCobradora_Cliente.cmbTipoBaixaChange(Sender: TObject);
begin
  inherited;

  {if (cmbTipoBaixa.ItemIndex = 1) then
  begin
      edtValorBaixa.Properties.ReadOnly := False;

      edtValorBaixa.Clear;
      edtValorBaixa.SetFocus;
  end

  else
  begin
      edtValorBaixa.Properties.ReadOnly := True;

      if not (qrySomaTit.IsEmpty) then
          edtValorBaixa.Value := qrySomaTitnSaldoTit.Value;
  end;}
end;

procedure TfrmLoteLiqCobradora_Cliente.qryClienteAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  if not (qryCliente.IsEmpty) then
  begin
      qryValidaCliente.Close;
      qryValidaCliente.Parameters.ParamByName('nCdLoteLiqCobradora').Value := nCdLoteLiqCobradora;
      qryValidaCliente.Parameters.ParamByName('nCdTerceiro').Value         := qryClientenCdTerceiro.Value;
      qryValidaCliente.Open;

      if not (qryValidaCliente.IsEmpty) then
      begin
          MensagemAlerta('Cliente selecionado j� existe neste lote: ' + qryClientecNmTerceiro.Value);

          qryCliente.Close;
          qryValidaCliente.Close;

          edtCliente.Clear;
          edtValorBaixa.Clear;

          Abort;
      end;

      qrySomaTit.Close;
      qrySomaTit.Parameters.ParamByName('nCdTerceiro').Value  := qryClientenCdTerceiro.Value;
      qrySomaTit.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
      qrySomaTit.Parameters.ParamByName('nCdCobradora').Value := nCdCobradora;
      qrySomaTit.Open;

      if (qrySomaTitnSaldoTit.Value <= 0) then
      begin
          MensagemAlerta('Nenhum t�tulo na cobradora para o Cliente: ' + qryClientecNmTerceiro.Value);

          qryCliente.Close;
          qrySomaTit.Close;

          edtCliente.Clear;
          edtValorBaixa.Clear;

          Abort;
      end;
  end

  else
      edtCliente.Clear;

  cmbTipoBaixa.ItemIndex := -1;
end;

procedure TfrmLoteLiqCobradora_Cliente.edtClienteExit(Sender: TObject);
begin
  inherited;

  qryCliente.Close;
  PosicionaQuery(qryCliente, edtCliente.Text);
end;

end.
