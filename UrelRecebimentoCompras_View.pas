unit UrelRecebimentoCompras_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB, StdCtrls, Mask,
  DBCtrls;

type
  TrptRecebimentoCompras_View = class(TForm)
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRShape2: TQRShape;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    lblProduto: TQRLabel;
    lblQtd: TQRLabel;
    lblValUnit: TQRLabel;
    lblValTotal: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro2: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro4: TQRLabel;
    usp_relatorio: TADOStoredProc;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel1: TQRLabel;
    lblPedido: TQRLabel;
    QRDBText16: TQRDBText;
    lblFiltro5: TQRLabel;
    lblFiltro6: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText21: TQRDBText;
    QuickRep1: TQuickRep;
    lblValDesc: TQRLabel;
    QRDBText22: TQRDBText;
    lblPercDesc: TQRLabel;
    QRDBText23: TQRDBText;
    lblPercIPI: TQRLabel;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    lblValIPI: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBText26: TQRDBText;
    lblCustoFinal: TQRLabel;
    QRDBText9: TQRDBText;
    lblRef: TQRLabel;
    QRDBText27: TQRDBText;
    QRDBText3: TQRDBText;
    QRExpr1: TQRExpr;
    lblTotalSImp: TQRLabel;
    lblPercST: TQRLabel;
    QRDBText28: TQRDBText;
    lblValST: TQRLabel;
    QRDBText29: TQRDBText;
    lblFiltro7: TQRLabel;
    QRLabel33: TQRLabel;
    QRDBText30: TQRDBText;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRShape6: TQRShape;
    QRDBText31: TQRDBText;
    lblValVenda: TQRLabel;
    QRLabel39: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel40: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel41: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel42: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel43: TQRLabel;
    QRDBText34: TQRDBText;
    usp_relatorionCdRecebimento: TIntegerField;
    usp_relatorionCdEmpresa: TIntegerField;
    usp_relatorionCdLoja: TStringField;
    usp_relatoriocNmTerceiro: TStringField;
    usp_relatoriocNmTipoReceb: TStringField;
    usp_relatoriodDtReceb: TDateTimeField;
    usp_relatoriodDtCad: TDateTimeField;
    usp_relatoriocNmUsuarioCad: TStringField;
    usp_relatoriodDtFech: TDateTimeField;
    usp_relatoriocNmUsuarioFech: TStringField;
    usp_relatoriodDtAutorFinanc: TDateTimeField;
    usp_relatoriocNmUsuarioAutorFin: TStringField;
    usp_relatoriocNmTabStatusReceb: TStringField;
    usp_relatoriocNrDocto: TStringField;
    usp_relatoriocChaveNFe: TStringField;
    usp_relatoriocUFOrigemNF: TStringField;
    usp_relatoriodDtDocto: TDateTimeField;
    usp_relatorionValProdutos: TBCDField;
    usp_relatorionValFrete: TBCDField;
    usp_relatorionValDescontoNF: TBCDField;
    usp_relatorionValICMS: TBCDField;
    usp_relatorionValICMSSub: TBCDField;
    usp_relatorionValSeguroNF: TBCDField;
    usp_relatorionValDespesas: TBCDField;
    usp_relatorionValIPI: TBCDField;
    usp_relatorionValTotalNF: TBCDField;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    usp_relatoriocNmDepartamento: TStringField;
    usp_relatorionCdItemRecebimento: TIntegerField;
    usp_relatorionCdPedido: TIntegerField;
    usp_relatorionCdItemPedido: TIntegerField;
    usp_relatoriocNmCategoria: TStringField;
    usp_relatoriocNmMarca: TStringField;
    usp_relatorionCdProduto: TIntegerField;
    usp_relatoriocNmProduto: TStringField;
    usp_relatoriocReferencia: TStringField;
    usp_relatorionQtde: TBCDField;
    usp_relatorionValUnitario: TBCDField;
    usp_relatorionValVenda: TBCDField;
    usp_relatorionValTotal: TBCDField;
    usp_relatorionPercIPI: TBCDField;
    usp_relatorionValIPIItem: TBCDField;
    usp_relatorionPercICMSSub: TBCDField;
    usp_relatorionValICMSSubItem: TBCDField;
    usp_relatorionPercDesconto: TBCDField;
    usp_relatorionValDesconto: TBCDField;
    usp_relatorionValCustoFinal: TBCDField;
    usp_relatorionValUnitarioEsp: TBCDField;
    usp_relatorionValUnitarioPed: TBCDField;
    usp_relatorionValTotalItemFinal: TBCDField;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bPrintItem : Boolean;
  end;

var
  rptRecebimentoCompras_View: TrptRecebimentoCompras_View;

implementation

uses fMenu;

{$R *.dfm}

procedure TrptRecebimentoCompras_View.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
    QRDBText12.Mask := frmMenu.cMascaraCompras;
    QRDBText9.Mask := frmMenu.cMascaraCompras;
end;

procedure TrptRecebimentoCompras_View.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand := bPrintItem;
end;

procedure TrptRecebimentoCompras_View.QRBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand := bPrintItem;
end;

end.
