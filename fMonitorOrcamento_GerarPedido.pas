unit fMonitorOrcamento_GerarPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBCtrls;

type
  TfrmMonitorOrcamento_GerarPedido = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryPreparaTemp: TADOQuery;
    qryTemp: TADOQuery;
    qryTempnCdTipoPedido: TIntegerField;
    qryTempdDtPrevEntregaIni: TDateTimeField;
    qryTempdDtPrevEntregaFim: TDateTimeField;
    qryTempcNrPedTerceiro: TStringField;
    qryTempcOBS: TMemoField;
    qryTempcAnotacao: TMemoField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBMemo1: TDBMemo;
    Label7: TLabel;
    DBMemo2: TDBMemo;
    DBEdit5: TDBEdit;
    DataSource2: TDataSource;
    qryTempnCdIncoterms: TIntegerField;
    Label2: TLabel;
    DBEdit6: TDBEdit;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    DBEdit7: TDBEdit;
    DataSource3: TDataSource;
    DBEdit8: TDBEdit;
    SP_APROVA_ORCAMENTO: TADOStoredProc;
    qryTempnValAdiantamento: TBCDField;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    cmdTemp: TADOCommand;
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido : integer ;
  end;

var
  frmMonitorOrcamento_GerarPedido: TfrmMonitorOrcamento_GerarPedido;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmMonitorOrcamento_GerarPedido.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      nPK := frmLookup_Padrao.ExecutaConsulta2(71,'cFlgVenda = 1');

      If (nPK > 0) then
      begin
          qryTempnCdTipoPedido.Value := nPK ;
          PosicionaQuery(qryTipoPedido, IntToStr(nPK)) ;
      end ;

    end ;

  end ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.DBEdit1Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, DbEdit1.Text) ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryIncoterms.Close;
  PosicionaQuery(qryIncoterms, DbEdit6.Text) ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.DBEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      nPK := frmLookup_Padrao.ExecutaConsulta(24);

      If (nPK > 0) then
      begin
          qryTempnCdIncoterms.Value := nPK ;
          PosicionaQuery(qryIncoterms, IntToStr(nPK)) ;
      end ;

    end ;

  end ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryTempnCdTipoPedido.Value = 0) then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      Dbedit1.SetFocus;
      exit ;
  end ;

  if (qryTempdDtPrevEntregaIni.AsString = '') or (qryTempdDtPrevEntregaFim.AsString = '') then
  begin
      MensagemAlerta('Informe a previs�o de entrega.') ;
      Dbedit2.SetFocus;
      exit ;
  end ;

  if (qryTempdDtPrevEntregaIni.Value > qryTempdDtPrevEntregaFim.Value) then
  begin
      MensagemAlerta('Previs�o de entrega inv�lida.') ;
      Dbedit2.SetFocus;
      exit ;
  end ;

  if (qryTempnCdIncoterms.Value = 0) then
  begin
      MensagemAlerta('Informe o acordo do frete.') ;
      Dbedit6.SetFocus;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o do pedido e o encerramento deste or�amento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if (qryTemp.State <> dsBrowse) then
      qryTemp.Post ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_APROVA_ORCAMENTO.Close ;
      SP_APROVA_ORCAMENTO.Parameters.ParamByName('@nCdPedido').Value  := nCdPedido ;
      SP_APROVA_ORCAMENTO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_APROVA_ORCAMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryTemp.Close ;
  qryTipoPedido.Close ;
  qryIncoterms.Close ;

  Close ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryIncoterms,Dbedit6.Text) ;

end;

procedure TfrmMonitorOrcamento_GerarPedido.ToolButton2Click(
  Sender: TObject);
begin
  qryTemp.Close ;
  qryTipoPedido.Close ;
  qryIncoterms.Close ;
  inherited;

end;

end.
