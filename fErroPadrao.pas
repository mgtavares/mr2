unit fErroPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls, ShellApi;

type
  TfrmErroPadrao = class(TfrmProcesso_Padrao)
    Memo1: TMemo;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    procedure ToolButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cInterface : String ;
  end;

var
  frmErroPadrao: TfrmErroPadrao;

implementation

uses
  fMenu, fFeedback;

{$R *.dfm}

procedure TfrmErroPadrao.ToolButton5Click(Sender: TObject);
var 

  StringShellExecute: String;
  LF :String ;

begin

  LF := '%0D%0A';
  
  StringShellExecute := StringShellExecute +
   'mailto:suporte@er2soft.com.br' +
   '&subject=Suporte - Erro de processamento' +
   '&body=' + 'Usuario   :' + frmMenu.cNmUsuarioLogado + LF
            + 'Empresa   : ' + frmMenu.cNmEmpresaAtiva + LF
            + 'Loja      : ' + frmMenu.cNmLojaAtiva + LF
            + 'Computador: ' + frmMenu.cNomeComputador + LF
            + 'Versao    : ' + frmMenu.StatusBar1.Panels[0].Text + LF
            + 'Interface : ' + cInterface + LF + LF + LF
            + 'Mensagem  : ' + Memo1.Lines.Text ;

  ShellExecute(Self.Handle, 'open',PChar(StringShellExecute),'','',SW_SHOWNORMAL);

end;

procedure TfrmErroPadrao.FormShow(Sender: TObject);
begin
  inherited;

  Memo1.Font.Name  := 'Courier New' ;
  Memo1.Font.Size  := 10 ;

end;

procedure TfrmErroPadrao.ToolButton6Click(Sender: TObject);
var
  objFeedback : TfrmFeedback;
begin
  objFeedback := TfrmFeedback.Create(Nil);

  frmMenu.showForm(objFeedback, True);
end;

end.
