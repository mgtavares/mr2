inherited rptPosicaoPedidoVendaWeb: TrptPosicaoPedidoVendaWeb
  Left = 307
  Top = 249
  Width = 839
  Height = 273
  Caption = 'Rel. Posi'#231#227'o Pedido Venda Web'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 823
    Height = 211
  end
  object Label9: TLabel [1]
    Left = 35
    Top = 96
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dt. Pedido'
  end
  object Label8: TLabel [2]
    Left = 172
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [3]
    Left = 14
    Top = 120
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dt. Integra'#231#227'o'
  end
  object Label2: TLabel [4]
    Left = 172
    Top = 120
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label1: TLabel [5]
    Left = 19
    Top = 72
    Width = 66
    Height = 13
    Caption = 'Status Pedido'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [6]
    Left = 52
    Top = 48
    Width = 33
    Height = 13
    Caption = 'Cliente'
    FocusControl = DBEdit2
  end
  inherited ToolBar1: TToolBar
    Width = 823
    TabOrder = 7
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtTerceiro: TER2LookupMaskEdit [8]
    Left = 88
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 17
    QueryLookup = qryTerceiro
  end
  object edtStatus: TER2LookupMaskEdit [9]
    Left = 88
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 773
    QueryLookup = qryTabStatusPed
  end
  object edtDtPedIni: TMaskEdit [10]
    Left = 88
    Top = 88
    Width = 70
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtDtPedFin: TMaskEdit [11]
    Left = 200
    Top = 88
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtDtIntegraIni: TMaskEdit [12]
    Left = 88
    Top = 112
    Width = 70
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object edtDtIntegraFin: TMaskEdit [13]
    Left = 200
    Top = 112
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 156
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 8
  end
  object DBEdit2: TDBEdit [15]
    Tag = 1
    Left = 156
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmTabStatusPed'
    DataSource = dsTabStatusPed
    TabOrder = 9
  end
  object rgModoExibicao: TRadioGroup [16]
    Left = 88
    Top = 144
    Width = 185
    Height = 49
    Caption = ' Modo de Exibi'#231#227'o '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 6
  end
  inherited ImageList1: TImageList
    Left = 424
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro     = :nPK')
    Left = 360
    Top = 120
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 360
    Top = 152
  end
  object qryTabStatusPed: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabStatusPed'
      '      ,cNmTabStatusPed'
      '  FROM TabStatusPed'
      ' WHERE nCdTabStatusPed = :nPK')
    Left = 392
    Top = 120
    object qryTabStatusPednCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryTabStatusPedcNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
  end
  object dsTabStatusPed: TDataSource
    DataSet = qryTabStatusPed
    Left = 392
    Top = 152
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Posi'#231#227'o Pedido Venda Web'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 424
    Top = 120
  end
end
