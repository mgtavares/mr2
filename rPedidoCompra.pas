unit rPedidoCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, ER2Lookup, ER2Excel;

type
  TrptPedidoCompra = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit9: TMaskEdit;
    MaskEdit8: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit4: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsEmpresa: TDataSource;
    dsTipoPedido: TDataSource;
    dsTerceiro: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    dsMarca: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    DBEdit2: TDBEdit;
    dsGrupoProduto: TDataSource;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit3: TDBEdit;
    dsLoja: TDataSource;
    Label11: TLabel;
    edtCdProduto: TER2LookupMaskEdit;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit6: TDBEdit;
    dsProduto: TDataSource;
    RadioGroup5: TRadioGroup;
    ER2Excel1: TER2Excel;
    RadioGroup7: TRadioGroup;
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure exportaRelatorio;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedidoCompra: TrptPedidoCompra;

implementation

uses fLookup_Padrao, rPedidoCompra_view, fMenu;

var
    objRel : TrptPedidoCompra_view;

{$R *.dfm}

procedure TrptPedidoCompra.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close;

  PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;

end;

procedure TrptPedidoCompra.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;

end;

procedure TrptPedidoCompra.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;

end;

procedure TrptPedidoCompra.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, MaskEdit5.Text) ;

end;

procedure TrptPedidoCompra.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(71,'cFlgCompra = 1');

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            MaskEdit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
  
end;

procedure TrptPedidoCompra.MaskEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.ToolButton1Click(Sender: TObject);
var
    cFiltro : String ;
begin
  inherited;

  objRel := TrptPedidoCompra_view.Create(nil);

  try
      try
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value :=  frmMenu.ConvInteiro(MaskEdit2.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       :=  frmMenu.ConvInteiro(MaskEdit3.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTipoPedido').Value     :=  frmMenu.ConvInteiro(MaskEdit4.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cFlgCompraVenda').Value   :=  'C';
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMarca').Value          :=  frmMenu.ConvInteiro(MaskEdit5.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoProduto').Value   :=  frmMenu.ConvInteiro(MaskEdit1.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cPrevEntregaIni').Value   :=  frmMenu.ConvData(MaskEdit6.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cPrevEntregaFim').Value   :=  frmMenu.ConvData(MaskEdit7.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cDtPedidoIni').Value      :=  frmMenu.ConvData(MaskEdit8.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cDtPedidoFim').Value      :=  frmMenu.ConvData(MaskEdit9.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cSituacaoItem').Value     :=  RadioGroup2.ItemIndex ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoDivergencia').Value    :=  RadioGroup4.ItemIndex ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cExibirValor').Value      :=  RadioGroup3.ItemIndex ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value           :=  frmMenu.ConvInteiro(MaskEdit10.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        :=  frmMenu.nCdEmpresaAtiva;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoTroca').Value          :=  1 ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cExibeBonif').Value       :=  0 ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoBonif').Value          :=  1 ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdProduto').Value        :=  frmMenu.ConvInteiro(edtCdProduto.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nSituacaoPedido').Value   :=  RadioGroup7.ItemIndex;
          objRel.usp_Relatorio.Open ;

          if (RadioGroup5.ItemIndex = 0) then {-- relat�rio --}
          begin

              // oculta os valores
              objRel.QRBand4.Enabled := True ;

              if (RadioGroup3.ItemIndex = 1) then
                  objRel.QRBand4.Enabled := False ;

              // oculta os itens
              objRel.QRBand3.Enabled := True ;

              if (RadioGroup1.ItemIndex = 1) then
                  objRel.QRBand3.Enabled := False ;

              // Monta os filtros para exibir

              cFiltro := 'Empresa : ' + IntToStr(frmMenu.nCdEmpresaAtiva) + '-' + frmMenu.cNmEmpresaAtiva ;

              IF (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + ' / Loja: ' + MaskEdit10.Text + '-' + Trim(DbEdit3.Text) ;

              IF (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + ' / Grupo Economico: ' + MaskEdit2.Text + '-' + Trim(DbEdit1.Text) ;

              IF (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + ' / Terceiro: ' + MaskEdit3.Text + '-' + Trim(DbEdit10.Text) ;

              IF (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + ' / Tipo Pedido: ' + MaskEdit4.Text + '-' + Trim(DbEdit4.Text) ;

              IF (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + ' / Marca: ' + MaskEdit5.Text + '-' + Trim(DbEdit5.Text) ;

              IF (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + ' / Grupo Produto: ' + MaskEdit1.Text + '-' + Trim(DbEdit2.Text) ;

              IF (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + ' / Produto: ' + edtCdProduto.Text + '-' + Trim(DbEdit6.Text) ;

              IF (Trim(MaskEdit6.Text) <> '/  /') or (Trim(MaskEdit7.Text) <> '/  /') then
                  cFiltro := cFiltro + ' / Prev. Entrega: ' ;

              IF (Trim(MaskEdit6.Text) <> '/  /') then
                  cFiltro := cFiltro + ' de : ' + MaskEdit6.Text ;

              IF (Trim(MaskEdit7.Text) <> '/  /') then
                  cFiltro := cFiltro + ' at� : ' + MaskEdit7.Text ;

              IF (Trim(MaskEdit8.Text) <> '/  /') or (Trim(MaskEdit9.Text) <> '/  /') then
                  cFiltro := cFiltro + ' / Data Pedido: ' ;

              IF (Trim(MaskEdit8.Text) <> '/  /') then
                  cFiltro := cFiltro + ' de : ' + MaskEdit8.Text ;

              IF (Trim(MaskEdit9.Text) <> '/  /') then
                  cFiltro := cFiltro + ' at� : ' + MaskEdit9.Text ;

              cFiltro := cFiltro + ' / Situa��o : ' ;

              IF (RadioGroup2.ItemIndex = 0) then
                  cFiltro := cFiltro + 'TODOS' ;

              IF (RadioGroup2.ItemIndex = 1) then
                  cFiltro := cFiltro + 'S� em aberto' ;

              IF (RadioGroup2.ItemIndex = 2) then
                  cFiltro := cFiltro + 'S� em atraso' ;

              IF (RadioGroup2.ItemIndex = 3) then
                  cFiltro := cFiltro + 'S� recebidos' ;

              IF (RadioGroup2.ItemIndex = 4) then
                  cFiltro := cFiltro + 'S� previs�es' ;

              IF (RadioGroup4.ItemIndex = 0) then
                  cFiltro := cFiltro + ' / Somente os Recebidos com Diverg�ncias' ;

              objRel.lblFiltro1.Caption := cFiltro ;

              objRel.QuickRep1.PreviewModal;

          end
          else {-- planilha --}
          begin

              exportaRelatorio;

          end ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptPedidoCompra.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(86);

        If (nPK > 0) then
        begin
            MaskEdit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, MaskEdit10.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptPedidoCompra.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit10.Text) ;

end;

procedure TrptPedidoCompra.exportaRelatorio;
var
  cNmFonte : string ;
  iLinha   : integer ;
begin
  inherited;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  cNmFonte := 'Calibri' ;

  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := 'Relat�rio de Posi��o de Pedido de Compra' ;
  
  ER2Excel1.Celula['A1'].FontName   := cNmFonte;
  ER2Excel1.Celula['A1'].FontSize   := 10 ;

  ER2Excel1.Celula['A2'].FontName   := cNmFonte;
  ER2Excel1.Celula['A2'].FontSize   := 10 ;


  ER2Excel1.Celula['A6'].Text := 'Pedido' ;
  ER2Excel1.Celula['B6'].Text := 'Data Ped.' ;
  ER2Excel1.Celula['C6'].Text := 'Terceiro Fornecedor' ;


  ER2Excel1.Celula['D5'].Text := 'Produto/Item' ;
  ER2Excel1.Celula['D6'].Text := 'C�d. Prod.' ;
  ER2Excel1.Celula['E6'].Text := 'Descri��o' ;
  ER2Excel1.Celula['F6'].Text := 'U.M' ;

  ER2Excel1.Celula['D5'].Mesclar('F5');
  ER2Excel1.Celula['D5'].HorizontalAlign := haCenter ;


  ER2Excel1.Celula['G5'].Text := 'Quantidade Pedido' ;
  ER2Excel1.Celula['G6'].Text := 'Quant. Firme' ;
  ER2Excel1.Celula['H6'].Text := 'Saldo Firme' ;
  ER2Excel1.Celula['I6'].Text := 'Quant. Prev.' ;

  ER2Excel1.Celula['G5'].Mesclar('F5');
  ER2Excel1.Celula['I5'].HorizontalAlign := haCenter ;

  ER2Excel1.Celula['J5'].Text := 'Previs�o Entrega' ;
  ER2Excel1.Celula['J6'].Text := 'Inicial' ;
  ER2Excel1.Celula['K6'].Text := 'Final' ;

  ER2Excel1.Celula['J5'].Mesclar('F5');
  ER2Excel1.Celula['K5'].HorizontalAlign := haCenter ;

  ER2Excel1.Celula['A4'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A4'].FontName   := cNmFonte;
  ER2Excel1.Celula['A4'].FontSize   := 9 ;

  ER2Excel1.Celula['A5'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A5'].FontName   := cNmFonte;
  ER2Excel1.Celula['A5'].FontSize   := 9 ;

  ER2Excel1.Celula['A6'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A6'].FontName   := cNmFonte;
  ER2Excel1.Celula['A6'].FontSize   := 9 ;
  ER2Excel1.Celula['A6'].Border     := [EdgeBottom];

  ER2Excel1.Celula['A4'].Range('K4');

  ER2Excel1.Celula['A5'].Range('K5');

  ER2Excel1.Celula['A6'].Range('K6');

  iLinha := 7 ; {-- Linha Inicial --}

  objRel.usp_Relatorio.First;

  while not objRel.usp_Relatorio.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdPedido.Value ;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtPedido.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNmTerceiro.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdProduto.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNmItem.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocSiglaUnidadeMedida.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := objRel.usp_RelatorionQtdePed.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := objRel.usp_RelatorionQtdeSaldo.Value;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Text := objRel.usp_RelatorionQtdePrev.Value;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtEntregaIni.Value;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtEntregaFim.Value;

      ER2Excel1.Celula['G' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0' ;

      ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight ;

      if (objRel.usp_RelatoriodDtEntregaFim.Value < Date) then
      begin
          ER2Excel1.Celula['A' + IntToStr(iLinha)].Background := clRed;
          ER2Excel1.Celula['A' + IntToStr(iLinha)].FontColor  := clWhite;

          ER2Excel1.Celula['J' + IntToStr(iLinha)].Background := clRed;
          ER2Excel1.Celula['J' + IntToStr(iLinha)].FontColor  := clWhite;

          ER2Excel1.Celula['K' + IntToStr(iLinha)].Background := clRed;
          ER2Excel1.Celula['K' + IntToStr(iLinha)].FontColor  := clWhite;
      end ;

      inc( iLinha ) ;
      
      objRel.usp_Relatorio.Next;
  end ;

  objRel.usp_Relatorio.First;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;

  frmMenu.mensagemUsuario('');
end;

initialization
    RegisterClass(TrptPedidoCompra) ;

end.
