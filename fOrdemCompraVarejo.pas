unit fOrdemCompraVarejo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBCtrlsEh, DBLookupEh, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmOrdemCompraVarejo = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    DBEdit12: TDBEdit;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    dsTerceiro: TDataSource;
    DBEdit17: TDBEdit;
    dsStatusPed: TDataSource;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit18: TDBEdit;
    qryMasternCdEstoqueMov: TIntegerField;
    dsEstoque: TDataSource;
    Label17: TLabel;
    DBEdit24: TDBEdit;
    DBEdit27: TDBEdit;
    dsCondPagto: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    Label27: TLabel;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    dsTerceiroColab: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryItemEstoquecCalc: TStringField;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    usp_Finaliza: TADOStoredProc;
    ToolButton12: TToolButton;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    dsDadoAutorz: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    dsItemFormula: TDataSource;
    usp_gera_item_embalagem: TADOStoredProc;
    qryMastercOBSFinanc: TMemoField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label37: TLabel;
    DBEdit40: TDBEdit;
    qryTerceiroRepres: TADOQuery;
    dsTerceiroRepres: TDataSource;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    Label15: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    ToolButton13: TToolButton;
    PopupMenu1: TPopupMenu;
    ExibirRecebimentosdoItem1: TMenuItem;
    qryItemEstoquenPercICMSSub: TBCDField;
    qryItemEstoquenValICMSSub: TBCDField;
    qryMasternCdGrupoEconomico: TIntegerField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    DBEdit3: TDBEdit;
    dsGrupoEconomico: TDataSource;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    qryItemEstoquenPercentCompra: TBCDField;
    qryItemEstoquenCdTerceiroColab: TIntegerField;
    qryItemEstoquecNmTerceiro: TStringField;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironPercDesconto: TBCDField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdGrade: TIntegerField;
    cxTabSheet5: TcxTabSheet;
    Image2: TImage;
    cxButton2: TcxButton;
    DBGridEh2: TDBGridEh;
    StaticText1: TStaticText;
    DBGridEh4: TDBGridEh;
    StaticText2: TStaticText;
    DBGridEh5: TDBGridEh;
    StaticText3: TStaticText;
    qryTotalMarca: TADOQuery;
    qryTotalMarcanCdMarca: TIntegerField;
    qryTotalMarcacNmMarca: TStringField;
    qryTotalMarcaCOLUMN1: TBCDField;
    dsTotalMarca: TDataSource;
    qryTotalDepartamento: TADOQuery;
    qryTotalDepartamentonCdDepartamento: TIntegerField;
    qryTotalDepartamentocNmDepartamento: TStringField;
    qryTotalDepartamentonValTotal: TBCDField;
    qryTotalDepartamentonPercentPart: TBCDField;
    dsTotalDepartamento: TDataSource;
    DBGridEh6: TDBGridEh;
    StaticText4: TStaticText;
    qryTotalCategoria: TADOQuery;
    qryTotalCategorianCdCategoria: TIntegerField;
    qryTotalCategoriacNmCategoria: TStringField;
    qryTotalCategorianValTotal: TBCDField;
    qryTotalCategorianPercentPart: TBCDField;
    dsTotalCategoria: TDataSource;
    DBGridEh7: TDBGridEh;
    StaticText5: TStaticText;
    dsTotalSubCategoria: TDataSource;
    qryTotalSubCategoria: TADOQuery;
    qryTotalSubCategorianCdSubCategoria: TIntegerField;
    qryTotalSubCategoriacNmSubCategoria: TStringField;
    qryTotalSubCategorianValTotal: TBCDField;
    qryTotalSubCategorianPercentPart: TBCDField;
    dsTotalSegmento: TDataSource;
    qryTotalSegmento: TADOQuery;
    DBGridEh8: TDBGridEh;
    StaticText6: TStaticText;
    qryTotalSegmentonCdSegmento: TIntegerField;
    qryTotalSegmentocNmSegmento: TStringField;
    qryTotalSegmentonValTotal: TBCDField;
    qryTotalSegmentonPercentPart: TBCDField;
    qryTotalMarcanPercentPart: TBCDField;
    dsTotalLoja: TDataSource;
    qryTotalLoja: TADOQuery;
    qryTotalLojanCdLoja: TIntegerField;
    qryTotalLojacNmLoja: TStringField;
    qryTotalLojanValTotal: TBCDField;
    qryTotalLojanPercentPart: TBCDField;
    cxTabSheet1: TcxTabSheet;
    DBGridEh9: TDBGridEh;
    qryPedidoGerado: TADOQuery;
    dsPedidoGerado: TDataSource;
    qryPedidoGeradonCdPedido: TIntegerField;
    qryPedidoGeradonCdLoja: TIntegerField;
    qryPedidoGeradocNmLoja: TStringField;
    qryPedidoGeradonCdTerceiro: TIntegerField;
    qryPedidoGeradocNmTerceiro: TStringField;
    qryPedidoGeradonValPedido: TBCDField;
    PopupMenu2: TPopupMenu;
    ImprimirPedido1: TMenuItem;
    qryItemEstoquenValProdutos: TFloatField;
    cxTabSheet2: TcxTabSheet;
    qryTituloAbatPedido: TADOQuery;
    qryTituloAbatPedidonCdTituloAbatPedido: TAutoIncField;
    qryTituloAbatPedidonCdPedido: TIntegerField;
    qryTituloAbatPedidonCdTitulo: TIntegerField;
    qryTituloAbatPedidonPercAbat: TBCDField;
    qryTituloAbatPedidocNmEspTit: TStringField;
    qryTituloAbatPedidocObsTit: TStringField;
    qryTituloAbatPedidodDtVenc: TDateField;
    dsTituloAbatPedido: TDataSource;
    DBGridEh10: TDBGridEh;
    qryTitulo: TADOQuery;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulocObsTit: TMemoField;
    qryTitulocNmEspTit: TStringField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonSaldoTit: TBCDField;
    cxButton3: TcxButton;
    qryProdutonValVenda: TBCDField;
    qryInseriItemGrade: TADOQuery;
    qryInseriGradeLoja: TADOQuery;
    ImprimirTodosPedidos1: TMenuItem;
    qryPreparaTempLoja: TADOQuery;
    ToolButton15: TToolButton;
    panelItem: TPanel;
    btItemGrade: TcxButton;
    btSugParcela: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure DBEdit24Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure btItemGradeClick(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure DBEdit19Exit(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ExibirRecebimentosdoItem1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure ImprimirPedido1Click(Sender: TObject);
    procedure qryTituloAbatPedidoBeforePost(DataSet: TDataSet);
    procedure qryTituloAbatPedidoCalcFields(DataSet: TDataSet);
    procedure DBGridEh10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton3Click(Sender: TObject);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure ImprimirTodosPedidos1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);

  private
    { Private declarations }
    bInsert     : boolean ;
    bGrade      : boolean ;
    nCdTerceiro : integer ;
  public
    { Public declarations }
  end;

var
  frmOrdemCompraVarejo: TfrmOrdemCompraVarejo;

implementation

uses Math,fMenu, fLookup_Padrao, rPedidoCom_Simples, fEmbFormula,
  fPrecoEspPedCom, fItemPedidoCompraAtendido, fOrdemCompraVarejo_Grade,
  fMontaGrade,fOrdemCompraVarejo_GradeAberta;

{$R *.dfm}

var
   objOrdemCompraVarejo_Grade       : TfrmOrdemCompraVarejo_Grade ;
   objOrdemCompraVarejo_GradeAberta : TfrmOrdemCompraVarejo_GradeAberta ;
   objGrade                         : TfrmMontaGrade ;

procedure TfrmOrdemCompraVarejo.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmOrdemCompraVarejo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 145 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmOrdemCompraVarejo.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  DBGridEh10.ReadOnly  := False ;
  btSugParcela.Enabled := True ;

  nCdTerceiro := 0 ;

  DBEdit4.SetFocus;
end;

procedure TfrmOrdemCompraVarejo.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  nCdTerceiro := 0 ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(94);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEconomico.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                ShowMessage('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(152);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmOrdemCompraVarejo.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  PosicionaQuery(qryGrupoEconomico, DBEdit4.Text) ;


end;

procedure TfrmOrdemCompraVarejo.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;

  if not qryLoja.eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit38Exit(Sender: TObject);
begin
  inherited;
  qryTerceiroColab.Close ;
  PosicionaQuery(qryTerceiroColab, DBEdit38.Text) ;
end;

procedure TfrmOrdemCompraVarejo.DBEdit24Exit(Sender: TObject);
begin
  inherited;
  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit24.Text) ;
end;

procedure TfrmOrdemCompraVarejo.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      ShowMessage('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  PosicionaQuery(qryTerceiroColab, qryMasternCdTerceiroColab.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.AsString) ;

  PosicionaQuery(qryGrupoEconomico, qryMasternCdGrupoEconomico.AsString) ;

  PosicionaQuery(qryTituloAbatPedido, qryMasternCdPedido.AsString) ;


  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  DBGridEh10.ReadOnly  := False ;
  btSugParcela.Enabled := True ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly   := True ;
      DBGridEh3.ReadOnly   := True ;
      DBGridEh10.ReadOnly  := True ;
      btSugParcela.Enabled := False ;
  end ;

  TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;

end;

procedure TfrmOrdemCompraVarejo.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiroColab.Close ;
  qryCondPagto.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryTerceiroRepres.Close ;
  qryTotalLoja.Close ;
  qryTotalMarca.Close ;
  qryTotalDepartamento.Close ;
  qryTotalCategoria.Close ;
  qryTotalSubCategoria.Close ;
  qryTotalSegmento.Close ;
  qryGrupoEconomico.Close ;
  qryPedidoGerado.Close ;
  qryTituloAbatPedido.Close ;


  cxPageControl1.ActivePageIndex := 0 ;

  TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;
end;


procedure TfrmOrdemCompraVarejo.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      abort ;
  end ;

  if (qryItemEstoquecNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe o fornecedor.') ;
      abort ;
  end ;

  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;
  qryItemEstoquenCdTipoItemPed.Value := 1 ;

  inherited;

  qryItemEstoquenValIPI.Value     := 0 ;
  qryItemEstoquenValICMSSub.Value := 0 ;

  qryItemEstoquenValDesconto.Value := frmMenu.TBRound(qryItemEstoquenValDesconto.Value,2) ;

  if (qryItemEstoquenPercIPI.Value > 0) then
      qryItemEstoquenValIPI.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercIPI.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

  if (qryItemEstoquenPercICMSSub.Value > 0) then
      qryItemEstoquenValICMSSub.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercICMSSub.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

  qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

  // calcula o custo final
  qryItemEstoquenValCustoUnit.Value := (( (frmMenu.TBRound((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value),2)*qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) ;
  qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,2) ;

  qryItemEstoquenValTotalItem.Value := (((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) ;

  bInsert := False ;

  if (qryItemEstoque.State = dsInsert) then
  begin
      bInsert := True ;
      qryItemEstoquenCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;
  end ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - ((StrToFloat(qryItemEstoquenValUnitario.OldValue) - StrToFloat(qryItemEstoquenValDesconto.OldValue)) * StrToFloat(qryItemEstoquenQtdePed.OldValue)) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - StrToFloat(qryItemEstoquenValIPI.OldValue) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - StrToFloat(qryItemEstoquenValICMSSub.OldValue) ;
  end ;

end;

procedure TfrmOrdemCompraVarejo.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryItemEstoquenValProdutos.Value := ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) ;

  qryProduto.Close ;
  qryTerceiro.Close ;

  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;
  PosicionaQuery(qryTerceiro, qryItemEstoquenCdTerceiroColab.AsString) ;

  if not qryProduto.eof then
      qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;

  if not qryTerceiro.eof then
      qryItemEstoquecNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;

  If (DBGridEh1.Col = 1) then
  begin

      DBGridEh1.Columns[2].ReadOnly   := True ;

      if (qryItemEstoque.State <> dsBrowse) then
      begin

        PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

        if not qryProduto.eof then
        begin
          qryItemEstoquecNmItem.Value      := qryProdutocNmProduto.Value ;
          qryItemEstoquenValSugVenda.Value := qryProdutonValVenda.Value ;

          if (frmMenu.LeParametro('USAGRADEABERTA') = 'N') then
          begin

              bGrade := False ;

              if (qryProdutonCdGrade.Value > 0) then
                  bGrade := True ;

              if bGrade then
              begin

                  objGrade.qryValorAnterior.SQL.Text := '' ;

                  if (qryItemEstoquenCdItemPedido.Value > 0) then
                      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WHERE nCdTipoItemPed = 4 AND nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' ORDER BY ncdProduto') ;

                  objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);
                  objGrade.Renderiza;

                  qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;

                  DBGridEh1.SetFocus;
                  DBGridEh1.Col := 5 ;

                  end ;

                  qryPreparaTempLoja.ExecSQL;

                  //monta a grade pedindo a quantidade por loja
                  objOrdemCompraVarejo_Grade.nCdItemPedido := qryItemEstoquenCdItemPedido.Value ;
                  objOrdemCompraVarejo_Grade.bGrade        := bGrade ;
                  objOrdemCompraVarejo_Grade.ShowModal;

                  if (objOrdemCompraVarejo_Grade.nQtdeTotalPares <= 0) then
                  begin
                      MensagemAlerta('� obrigat�rio selecionar a quantidade de fichas por loja.') ;
                      DBGridEh1.Col := 1 ;
                      abort ;
                  end ;

                  qryItemEstoquenQtdePed.Value := qryItemEstoquenQtdePed.Value * objOrdemCompraVarejo_Grade.nQtdeTotalPares ;

                  DBGridEh1.Col := 3 ;
              end else
              begin

              //    if (qryItemEstoquenCdItemPedido.Value > 0) then
              //    begin
                  objOrdemCompraVarejo_GradeAberta := TfrmOrdemCompraVarejo_GradeAberta.Create(nil) ;
                  objOrdemCompraVarejo_GradeAberta.nCdItemPedido := qryItemEstoquenCdItemPedido.Value;
                  objOrdemCompraVarejo_GradeAberta.nCdProduto := qryItemEstoquenCdProduto.Value;
                  objOrdemCompraVarejo_GradeAberta.ShowModal;

                  qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;
              //  end;
              end;

              if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
              begin
                  DBGridEh1.Columns[2].ReadOnly   := False ;
                  DBGridEh1.Col := 2 ;

              end ;

          end;

      end ;
  end;

  {ipi}
  if (dbGridEh1.Col = 12) and (qryItemEstoque.State <> dsBrowse) then
  begin
      qryItemEstoquenValIPI.Value     := 0 ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercIPI.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

  end;

  {st}
  if (dbGridEh1.Col = 16) and (qryItemEstoque.State <> dsBrowse) then
  begin
      qryItemEstoquenValICMSSub.Value := 0 ;

      if (qryItemEstoquenPercICMSSub.Value > 0) then
          qryItemEstoquenValICMSSub.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercICMSSub.Value / 100)) * qryItemEstoquenQtdePed.Value ,2) ;

  end;

  {custo final}
  if (dbGridEh1.Col = 16) and (qryItemEstoque.State <> dsBrowse) then
  begin

{      qryItemEstoquenValIPI.Value     := 0 ;
      qryItemEstoquenValICMSSub.Value := 0 ;

      // calcula o custo final
      qryItemEstoquenValTotalItem.Value := ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * qryItemEstoquenQtdePed.Value) ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercIPI.Value / 100)) ;

      qryItemEstoquenValCustoUnit.Value := ((qryItemEstoquenValTotalItem.Value + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) + 0.005;}

      qryItemEstoquenValIPI.Value     := 0 ;
      qryItemEstoquenValICMSSub.Value := 0 ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercIPI.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

      if (qryItemEstoquenPercICMSSub.Value > 0) then
          qryItemEstoquenValICMSSub.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercICMSSub.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

      qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

      // calcula o custo final
      if (qryItemEstoquenQtdePed.Value > 0) then
      begin
          qryItemEstoquenValCustoUnit.Value := (( ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value)*qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) ;
          qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,2) ;

          qryItemEstoquenValTotalItem.Value := (qryItemEstoquenValCustoUnit.Value * qryItemEstoquenQtdePed.Value) ;
      end;
  end ;


end;

procedure TfrmOrdemCompraVarejo.FormShow(Sender: TObject);
begin
  inherited;

  objOrdemCompraVarejo_Grade := TfrmOrdemCompraVarejo_Grade.Create( Self ) ;
  objGrade                   := TfrmMontaGrade.Create( Self ) ;

  cxPageControl1.ActivePageIndex := 0 ;

  TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroRepres.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryTituloAbatPedido.Active then
      PosicionaQuery(qryTituloAbatPedido, qryMasternCdPedido.asString) ;

end;

procedure TfrmOrdemCompraVarejo.qryItemEstoqueAfterPost(DataSet: TDataSet);
var
    i : integer ;
begin

  if (objGrade.bMontada) and (bGrade) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString) ;
      qryAux.ExecSQL;

      objGrade.cdsQuantidade.First;
      objGrade.cdsCodigo.First ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value    := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value         := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      objGrade.LiberaGrade;

      qryInseriGradeLoja.Close ;
      qryInseriGradeLoja.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
      qryInseriGradeLoja.ExecSQL;

  end ;

  bInsert := False ;

  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value + ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + qryItemEstoquenValIPI.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + qryItemEstoquenValICMSSub.Value ;
  qryMaster.Post ;

  nCdTerceiro := qryItemEstoquenCdTerceiroColab.Value ;

end;

procedure TfrmOrdemCompraVarejo.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusPed.Value  := 1 ;
      qryMasternCdTabTipoPedido.Value := 4 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiroColab.Value = 0) or (DbEdit39.Text = '') then
  begin
      MensagemAlerta('Informe o Comprador.') ;
      DbEdit38.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDesconto.Value < 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      DbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDescontoVencto.Value < 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      DbEdit35.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercAcrescimoVendor.Value < 0) then
  begin
      MensagemAlerta('Percentual de acr�scimo inv�lido.') ;
      DbEdit40.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntIni.asString = '') then
  begin
      MensagemAlerta('Informe a previs�o inicial de entrega.') ;
      DbEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntFim.asString = '') then
  begin
      MensagemAlerta('Informe a previs�o final de entrega.') ;
      DbEdit18.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntFim.Value < qryMasterdDtPrevEntIni.Value) then
  begin
      MensagemAlerta('Previs�o de entrega inv�lida.') ;
      DbEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntIni.Value < qryMasterdDtPedido.Value) then
  begin
      MensagemAlerta('A data de previs�o inicial n�o pode ser menor que a data do pedido.') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabStatusPed.Value := 1;

  qryMasternValPedido.Value := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value ;
end;

procedure TfrmOrdemCompraVarejo.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  - qryItemEstoquenValIPI.Value       - qryItemEstoquenValICMSSub.Value ;

end;

procedure TfrmOrdemCompraVarejo.btItemGradeClick(Sender: TObject);
begin
  if not qryItemEstoque.Active then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      MensagemAlerta('Selecione um item.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value = 20) then
  begin
      MensagemAlerta('Est� ordem est� processada e somente a consulta � permitida.') ;
  end ;

  if (frmMenu.LeParametro('USAGRADEABERTA') = 'N') then
  begin

      bGrade := False ;

  if (qryProdutonCdGrade.Value > 0) then
      bGrade := True ;

  if bGrade then
  begin

      objGrade.qryValorAnterior.SQL.Text := '' ;

      if (qryItemEstoquenCdItemPedido.Value > 0) then
          objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

      objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

      if (qryMasternCdTabStatusPed.Value = 20) then
          objGrade.DBGridEh1.ReadOnly := True ;

      objGrade.Renderiza;
      objGrade.DBGridEh1.ReadOnly := False ;

      if (qryMasternCdTabStatusPed.Value < 20) then
      begin
          qryItemEstoque.Edit ;
          qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;
      end ;

      DBGridEh1.SetFocus;
      DBGridEh1.Col := 5 ;

  end ;

  //monta a grade pedindo a quantidade por loja
  objOrdemCompraVarejo_Grade.nCdItemPedido := qryItemEstoquenCdItemPedido.Value ;
  objOrdemCompraVarejo_Grade.bGrade        := bGrade ;

  if (qryMasternCdTabStatusPed.Value = 20) then
      objOrdemCompraVarejo_Grade.ToolButton1.Enabled := False ;

  objOrdemCompraVarejo_Grade.ShowModal;

  objOrdemCompraVarejo_Grade.ToolButton1.Enabled := True ;

  if (qryMasternCdTabStatusPed.Value < 20) then
  begin

    if (objOrdemCompraVarejo_Grade.nQtdeTotalPares <= 0) then
    begin
        MensagemAlerta('� obrigat�rio selecionar a quantidade de fichas por loja.') ;
        DBGridEh1.Col := 1 ;
        abort ;
    end ;

    if (qryItemEstoque.State = dsBrowse) then
        qryItemEstoque.Edit ;

    qryItemEstoquenQtdePed.Value := qryItemEstoquenQtdePed.Value * objOrdemCompraVarejo_Grade.nQtdeTotalPares ;

      end ;

  end;
  
end;

procedure TfrmOrdemCompraVarejo.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
  begin
      qryItemEstoquenValDesconto.Value   := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;
      qryItemEstoquenPercentCompra.Value := qryMasternPercDesconto.Value ;
  end ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmOrdemCompraVarejo.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;

  if (qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) > 0 then
      qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value + 0.005 ;
  
  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;

end;

procedure TfrmOrdemCompraVarejo.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmOrdemCompraVarejo.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  if (qryPrazoPedido.State = dsInsert) then
      qryPrazoPedidonCdPrazoPedido.Value := frmMenu.fnProximoCodigo('PRAZOPEDIDO') ;

end;

procedure TfrmOrdemCompraVarejo.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhuma ordem de compra ativa.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      MensagemAlerta('Ordem de Compra sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve a ordem de compra antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmOrdemCompraVarejo.ToolButton10Click(Sender: TObject);
var
  nValProdutos : double ;
begin

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhuma ordem de compra ativa.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve a ordem de compra antes de utilizar esta fun��o.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value = 20) then
  begin
      MensagemAlerta('Ordem de Compra j� Processada.') ;
      exit ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  nValProdutos := 0 ;

  // calcula o total do pedido
  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos := nValProdutos + ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) ;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value := nValProdutos ;
  qryMaster.Post ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Ordem de compra sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  case MessageDlg('O ordem ser� finalizada, os pedidos ser�o gerados e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;

      qryMasternCdTabStatusPed.Value := 20 ; // Processado
      qryMasterdDtAutor.Value        := Now() ;
      qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;

      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
      usp_Finaliza.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      usp_Finaliza.ExecProc;

      PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem finalizada com sucesso.') ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 1) then
        begin

            if (qryItemEstoque.State = dsBrowse) then
                qryItemEstoque.Edit ;

            if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(146);

                If (nPK > 0) then
                    qryItemEstoquenCdProduto.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Col = 4) then
        begin

            if (qryItemEstoque.State = dsBrowse) then
                qryItemEstoque.Edit ;

            if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
            begin

                if (DBEdit4.Text = '') then
                    nPK := frmLookup_Padrao.ExecutaConsulta(90)
                else begin
                    nPK := frmLookup_Padrao.ExecutaConsulta2(90,'nCdGrupoEconomico = ' + DbEdit4.Text) 
                end ;


                If (nPK > 0) then
                    qryItemEstoquenCdTerceiroColab.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmOrdemCompraVarejo.ToolButton12Click(Sender: TObject);
var
  objRel : TrptPedidoCom_Simples ;
begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  objRel := TrptPedidoCom_Simples.Create( Self ) ;

  PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

  objRel.QRSubDetail1.Enabled := True ;
  objRel.QRSubDetail2.Enabled := True ;
  objRel.QRSubDetail3.Enabled := True ;

  if (objRel.qryItemEstoque_Grade.eof) then
      objRel.QRSubDetail1.Enabled := False ;

  if (objRel.qryItemAD.eof) then
      objRel.QRSubDetail2.Enabled := False ;

  if (objRel.qryItemFormula.eof) then
      objRel.QRSubDetail3.Enabled := False ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  qryTerceiroRepres.Close ;
  PosicionaQuery(qryTerceiroRepres, DBEdit19.Text) ;

end;

procedure TfrmOrdemCompraVarejo.ToolButton13Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Ordem de Compra Cancelada') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  
  ShowMessage('Ordem de Compra Cancelada com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmOrdemCompraVarejo.ExibirRecebimentosdoItem1Click(
  Sender: TObject);
var
  objForm : TfrmItemPedidoCompraAtendido ;
begin
  inherited;
  
  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin

      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin

          objForm := TfrmItemPedidoCompraAtendido.Create( Self ) ;

          PosicionaQuery(objForm.qryItem, qryItemEstoquenCdItemPedido.AsString) ;

          showForm( objForm , TRUE ) ;

      end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.cxButton2Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryTotalLoja, qryMasternCdPedido.asString) ;
  
  qryTotalMarca.Close ;
  qryTotalMarca.Parameters.ParamByName('nPK').Value  := qryMasternCdPedido.Value ;
  qryTotalMarca.Parameters.ParamByName('nPK2').Value := qryMasternCdPedido.Value ;
  qryTotalMarca.Open ;

  qryTotalDepartamento.Close ;
  qryTotalDepartamento.Parameters.ParamByName('nPK').Value  := qryMasternCdPedido.Value ;
  qryTotalDepartamento.Parameters.ParamByName('nPK2').Value := qryMasternCdPedido.Value ;
  qryTotalDepartamento.Open ;

  qryTotalCategoria.Close ;
  qryTotalCategoria.Parameters.ParamByName('nPK').Value  := qryMasternCdPedido.Value ;
  qryTotalCategoria.Parameters.ParamByName('nPK2').Value := qryMasternCdPedido.Value ;
  qryTotalCategoria.Open ;

  qryTotalSubCategoria.Close ;
  qryTotalSubCategoria.Parameters.ParamByName('nPK').Value  := qryMasternCdPedido.Value ;
  qryTotalSubCategoria.Parameters.ParamByName('nPK2').Value := qryMasternCdPedido.Value ;
  qryTotalSubCategoria.Open ;

  qryTotalSegmento.Close ;
  qryTotalSegmento.Parameters.ParamByName('nPK').Value  := qryMasternCdPedido.Value ;
  qryTotalSegmento.Parameters.ParamByName('nPK2').Value := qryMasternCdPedido.Value ;
  qryTotalSegmento.Open ;

end;

procedure TfrmOrdemCompraVarejo.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = cxTabSheet1) then
  begin
      if (qryMaster.Active) then
          PosicionaQuery(qryPedidoGerado, qryMasternCdPedido.AsString) ;
  end ;
end;

procedure TfrmOrdemCompraVarejo.ImprimirPedido1Click(Sender: TObject);
var
  objRel : TrptPedidoCom_Simples ;
begin
  inherited;

  if (qryPedidoGerado.Active) then
  begin

      objRel := TrptPedidoCom_Simples.Create( Self ) ;

      PosicionaQuery(objRel.qryPedido,qryPedidoGeradonCdPedido.asString) ;
      PosicionaQuery(objRel.qryItemEstoque_Grade,qryPedidoGeradonCdPedido.asString) ;
      PosicionaQuery(objRel.qryItemAD,qryPedidoGeradonCdPedido.asString) ;
      PosicionaQuery(objRel.qryItemFormula,qryPedidoGeradonCdPedido.asString) ;

      objRel.QRSubDetail1.Enabled := True ;
      objRel.QRSubDetail2.Enabled := True ;
      objRel.QRSubDetail3.Enabled := True ;

      if (objRel.qryItemEstoque_Grade.eof) then
          objRel.QRSubDetail1.Enabled := False ;

      if (objRel.qryItemAD.eof) then
          objRel.QRSubDetail2.Enabled := False ;

      if (objRel.qryItemFormula.eof) then
          objRel.QRSubDetail3.Enabled := False ;

      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

      try
          try
              {--visualiza o relat�rio--}

              objRel.QuickRep1.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRel);
      end;

  end ;

end;

procedure TfrmOrdemCompraVarejo.qryTituloAbatPedidoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTituloAbatPedidocNmEspTit.Value = '') then
  begin
      MensagemAlerta('Selecione o titulo a ser abatido.') ;
      abort ;
  end ;

  if (qryTituloAbatPedidonPercAbat.Value <= 0) then
  begin
      MensagemAlerta('Informe o percentual de desconto no pedido para o abatimento neste t�tulo.') ;
      abort ;
  end ;

  qryTitulo.Close ;
  qryTitulo.Parameters.ParamByName('nPK').Value         := qryTituloAbatPedidonCdTitulo.Value ;
  qryTitulo.Open ;

  if qryTitulo.eof then
  begin
      MensagemAlerta('T�tulo inv�lido ou n�o pertence a este fornecedor.') ;
      abort ;
  end ;

  if (qryTitulodDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este t�tulo est� cancelado e n�o pode ser movimentado.') ;
      abort ;
  end ;

  if (qryTitulodDtBloqTit.AsString <> '') then
  begin
      case MessageDlg('Este t�tulo est� bloqueado para movimenta��es. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;
  end ;

  if (qryTitulonSaldoTit.Value <= 0) then
  begin
      case MessageDlg('Este t�tulo n�o tem saldo pendente. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;
  end ;

  qryTituloAbatPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryTituloAbatPedido.State = dsInsert) then
      qryTituloAbatPedidonCdTituloAbatPedido.Value := frmMenu.fnProximoCodigo('TITULOABATPEDIDO') ;

end;

procedure TfrmOrdemCompraVarejo.qryTituloAbatPedidoCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if (qryTituloAbatPedidodDtVenc.asString = '') and (qryTituloAbatPedidonCdTitulo.Value > 0) then
  begin
      qryTitulo.Close ;
      PosicionaQuery(qryTitulo, qryTituloAbatPedidonCdTitulo.AsString) ;

      qryTituloAbatPedidodDtVenc.Value   := qryTitulodDtVenc.Value ;
      qryTituloAbatPedidocNmEspTit.Value := qryTitulocNmEspTit.Value ;
      qryTituloAbatPedidocObsTit.Value   := qryTitulocObsTit.Value ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTituloAbatPedido.State = dsBrowse) then
             qryTituloAbatPedido.Edit ;

        if (qryTituloAbatPedido.State = dsInsert) or (qryTituloAbatPedido.State = dsEdit) then
        begin

            if (DBEdit4.Text = '') then
                nPK := frmLookup_Padrao.ExecutaConsulta(90)
            else begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(90,'nCdGrupoEconomico = ' + DbEdit4.Text)
            end ;

            If (nPK > 0) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(161,'Titulo.nCdTerceiro = ' + IntToStr(nPK)) ; // + ' ORDER BY dDtVenc');

                If (nPK > 0) then
                begin
                    qryTituloAbatPedidonCdTitulo.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOrdemCompraVarejo.cxButton3Click(Sender: TObject);
var
    objForm : TfrmPrecoEspPedCom ;
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhuma ordem de compra ativa.') ;
        exit ;
    end ;

    if (qryMasternCdTabStatusPed.Value = 20) then
    begin
      MensagemAlerta('Ordem de Compra j� Processada.') ;
      exit ;
    end ;

    objForm := TfrmPrecoEspPedCom.Create( Self ) ;

    objForm.nCdPedido := qryMasternCdPedido.Value ;

    showForm( objForm , TRUE ) ;

end;

procedure TfrmOrdemCompraVarejo.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Col = 4) and (qryItemEstoque.State = dsInsert) then
      qryItemEstoquenCdTerceiroColab.Value := nCdTerceiro ;

  if (DBGridEh1.Col = 8) and (qryItemEstoque.State <> dsBrowse) then
  begin
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercentCompra.Value  / 100) ;
      qryItemEstoquenValDesconto.Value := frmMenu.TBRound(qryItemEstoquenValDesconto.Value,2) ;
  end ;

  {ipi}
  if (dbGridEh1.Col = 12) and (qryItemEstoque.State <> dsBrowse) then
  begin
      qryItemEstoquenValIPI.Value := 0 ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercIPI.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

  end;

  {st}
  if (dbGridEh1.Col = 16) and (qryItemEstoque.State <> dsBrowse) then
  begin
      qryItemEstoquenValICMSSub.Value := 0 ;

      if (qryItemEstoquenPercICMSSub.Value > 0) then
          qryItemEstoquenValICMSSub.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercICMSSub.Value / 100)) * qryItemEstoquenQtdePed.Value ,2) ;

  end;


  if (DBGridEh1.Col = 16) and (qryItemEstoque.State <> dsBrowse) then  
  begin

      qryItemEstoquenValIPI.Value     := 0 ;
      qryItemEstoquenValICMSSub.Value := 0 ;

      qryItemEstoquenValDesconto.Value := frmMenu.TBRound(qryItemEstoquenValDesconto.Value,2) ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercIPI.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

      if (qryItemEstoquenPercICMSSub.Value > 0) then
          qryItemEstoquenValICMSSub.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * (qryItemEstoquenPercICMSSub.Value / 100)) * qryItemEstoquenQtdePed.Value ,2);

      qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

      // calcula o custo final
      if (qryItemEstoquenQtdePed.Value > 0) then
      begin
          qryItemEstoquenValCustoUnit.Value := (( (frmMenu.TBRound((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value),2)*qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) ;
          qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,2) ;

          qryItemEstoquenValTotalItem.Value := (((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value) * qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) ;
      end;

  end ;

end;

procedure TfrmOrdemCompraVarejo.ImprimirTodosPedidos1Click(
  Sender: TObject);
var
  objRel : TrptPedidoCom_Simples ;
begin
  inherited;

  if (qryPedidoGerado.Active) and not (qryPedidoGerado.eof) then
  begin

      case MessageDlg('Confirma a impress�o de todos os pedidos gerados ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      qryPedidoGerado.First ;

      objRel := TrptPedidoCom_Simples.Create( Self ) ;

      while not qryPedidoGerado.eof do
      begin
          PosicionaQuery(objRel.qryPedido,qryPedidoGeradonCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryPedidoGeradonCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD,qryPedidoGeradonCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemFormula,qryPedidoGeradonCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;
          objRel.QRSubDetail3.Enabled := True ;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False ;

          if (objRel.qryItemAD.eof) then
              objRel.QRSubDetail2.Enabled := False ;

          if (objRel.qryItemFormula.eof) then
              objRel.QRSubDetail3.Enabled := False ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

          objRel.QuickRep1.PreviewModal;

          qryPedidoGerado.Next ;

      end ;

      freeAndNil( objRel ) ;

      qryPedidoGerado.First ;
      
  end ;

end;

procedure TfrmOrdemCompraVarejo.ToolButton1Click(Sender: TObject);
begin
  inherited;

  nCdTerceiro := 0 ;
end;

initialization
    RegisterClass(TfrmOrdemCompraVarejo) ;

end.
