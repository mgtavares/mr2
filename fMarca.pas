unit fMarca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping,
  cxLookAndFeelPainters, cxButtons, ToolCtrlsEh;

type
  TfrmMarca = class(TfrmCadastro_Padrao)
    qryMasternCdMarca: TIntegerField;
    qryMastercNmMarca: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmStatus: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    qryMaterial: TADOQuery;
    dsMaterial: TDataSource;
    qryLinha: TADOQuery;
    dsLinha: TDataSource;
    qryMaterialnCdMaterial: TAutoIncField;
    qryMaterialnCdMarca: TIntegerField;
    qryMaterialcNmMaterial: TStringField;
    qryMaterialnCdStatus: TIntegerField;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhanCdMarca: TIntegerField;
    qryLinhacNmLinha: TStringField;
    qryLinhanCdStatus: TIntegerField;
    StaticText2: TStaticText;
    DBGridEh1: TDBGridEh;
    StaticText1: TStaticText;
    DBGridEh2: TDBGridEh;
    qryMaterialiNivel: TIntegerField;
    btnAtribuirMarca: TcxButton;
    Label7: TLabel;
    qryMasternCdIdExternoWeb: TIntegerField;
    DBEdit5: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMaterialBeforePost(DataSet: TDataSet);
    procedure qryLinhaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure btnAtribuirMarcaClick(Sender: TObject);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMarca: TfrmMarca;

implementation

uses fLookup_Padrao, fMenu, fAtribuiMarca;

{$R *.dfm}

procedure TfrmMarca.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MARCA' ;
  nCdTabelaSistema  := 28 ;
  nCdConsultaPadrao := 49 ;
end;

procedure TfrmMarca.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmMarca.qryMaterialBeforePost(DataSet: TDataSet);
begin

  if (qryMaterialcNmMaterial.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do material.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryMaterialnCdMarca.Value := qryMasternCdMarca.Value ;

  if qryMaterialnCdStatus.Value = 0 then
      qryMaterialnCdStatus.Value := 1 ;

  qryMaterialiNivel.Value      := 1 ;
  qryMaterialcNmMaterial.Value := Uppercase(qryMaterialcNmMaterial.Value) ;

  if (qryMaterial.State = dsInsert) then
      qryMaterialnCdMaterial.Value := frmMenu.fnProximoCodigo('MATERIAL') ;

  inherited;

end;

procedure TfrmMarca.qryLinhaBeforePost(DataSet: TDataSet);
begin

  if (qryLinhacNmLinha.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da linha.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryLinhanCdMarca.Value := qryMasternCdMarca.Value ;

  if qryLinhanCdStatus.Value = 0 then
      qryLinhanCdStatus.Value := 1 ;

  qryLinhacNmLinha.Value := Uppercase(qryLinhacNmLinha.Value) ;

  if (qryLinha.State = dsInsert) then
      qryLinhanCdLinha.Value := frmMenu.fnProximoCodigo('LINHA') ;

  inherited;

end;

procedure TfrmMarca.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryMaterial.Close ;
  qryMaterial.Parameters.ParamByName('nPK').Value := qryMasternCdMarca.Value ;
  qryMaterial.Open ;

  qryLinha.Close ;
  qryLinha.Parameters.ParamByName('nPK').Value := qryMasternCdMarca.Value ;
  qryLinha.Open ;
end;

procedure TfrmMarca.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryMaterial.Close ;
  qryLinha.Close ;

  btnAtribuirMarca.Enabled := False;
end;

procedure TfrmMarca.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMarca.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryMaterial.Close ;
  qryMaterial.Parameters.ParamByName('nPK').Value := qryMasternCdMarca.Value ;
  qryMaterial.Open ;

  qryLinha.Close ;
  qryLinha.Parameters.ParamByName('nPK').Value := qryMasternCdMarca.Value ;
  qryLinha.Open ;

end;

procedure TfrmMarca.DBGridEh1Enter(Sender: TObject);
begin
    if (qryMaster.State = dsInsert) then
        if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
            btSalvarClick(Self);

  inherited;
end;

procedure TfrmMarca.DBGridEh2Enter(Sender: TObject);
begin
    if (qryMaster.State = dsInsert) then
        if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
            btSalvarClick(Self);
end;

procedure TfrmMarca.btnAtribuirMarcaClick(Sender: TObject);
var
    objForm : TfrmAtribuiMarca;
begin
    if (qryMaster.State = dsInsert) then
        btSalvarClick(Self);

    objForm := TfrmAtribuiMarca.Create(nil);

    objForm.qryPopulaTemp.Parameters.ParamByName('nCdMarca').Value := qryMasternCdMarca.Value;
    objForm.qryMarca.Parameters.ParamByName('nPK').Value  := qryMasternCdMarca.Value;

    objForm.cmdPreparaTemp.Execute;
    
    objForm.qryPreparaTemp.ExecSQL;
    objForm.qryPopulaTemp.ExecSQL;

    objForm.qryTempAtribuiMarca.Open;
    objForm.qryMarca.Open;
    objForm.qrySubcategoria.Open;

    if (objForm.qryTempAtribuiMarca.IsEmpty) then
        objForm.qryTempAtribuiMarca.Insert;

    objForm.ShowModal;

    FreeAndNil(objForm);
end;

procedure TfrmMarca.qryMasterAfterOpen(DataSet: TDataSet);
begin
    btnAtribuirMarca.Enabled := True;
end;

procedure TfrmMarca.btSalvarClick(Sender: TObject);
begin
    if (Trim(DBEdit2.Text) = '') then
    begin
        MensagemAlerta('Informe o nome da marca!');

        DBEdit2.SetFocus;

        Abort;
    end;

    if (Trim(DBEdit3.Text) = '') then
    begin
        MensagemAlerta('Informe o status da marca!');

        DBEdit3.SetFocus;

        Abort;
    end;

    qryMaster.Post;
end;

procedure TfrmMarca.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  if (Key = VK_TAB) then
      DBGridEh2.SetFocus;

end;

procedure TfrmMarca.DBGridEh2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  if (Key = VK_TAB) then
      btnAtribuirMarca.SetFocus;
end;

initialization
    RegisterClass(TfrmMarca) ;

end.
