unit fSegmentoCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmSegmentoCobranca = class(TfrmCadastro_Padrao)
    qryMasternCdSegmentoCobranca: TIntegerField;
    qryMastercNmSegmentoCobranca: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSegmentoCobranca: TfrmSegmentoCobranca;

implementation

{$R *.dfm}

procedure TfrmSegmentoCobranca.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'SEGMENTOCOBRANCA' ;
  nCdTabelaSistema  := 237 ;
  nCdConsultaPadrao := 235 ;
end;

procedure TfrmSegmentoCobranca.qryMasterBeforePost(DataSet: TDataSet);
begin
  
  if ( trim( DBEdit2.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus;
      abort;
  end ;
  
  inherited;

end;

procedure TfrmSegmentoCobranca.btIncluirClick(Sender: TObject);
begin
  inherited;
  
  DBEdit2.SetFocus;
end;

initialization
  RegisterClass( TfrmSegmentoCobranca ) ;
end.
