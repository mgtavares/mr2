unit fTerceiro_ChequePendente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxPC;

type
  TfrmTerceiro_ChequePendente = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxTabSheet2: TcxTabSheet;
    qryChequePendente: TADOQuery;
    DataSource1: TDataSource;
    qryChequePendentenCdCheque: TIntegerField;
    qryChequePendentenCdEmpresa: TIntegerField;
    qryChequePendentenCdBanco: TIntegerField;
    qryChequePendentecAgencia: TStringField;
    qryChequePendentecConta: TStringField;
    qryChequePendentecDigito: TStringField;
    qryChequePendentecCNPJCPF: TStringField;
    qryChequePendenteiNrCheque: TIntegerField;
    qryChequePendentenValCheque: TBCDField;
    qryChequePendentedDtDeposito: TDateTimeField;
    cxGrid1DBTableView1nCdCheque: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cAgencia: TcxGridDBColumn;
    cxGrid1DBTableView1cConta: TcxGridDBColumn;
    cxGrid1DBTableView1cDigito: TcxGridDBColumn;
    cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGrid1DBTableView1iNrCheque: TcxGridDBColumn;
    cxGrid1DBTableView1nValCheque: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryChequeConcentracaoRisco: TADOQuery;
    DataSource2: TDataSource;
    cxGridDBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView1cNmEmissor: TcxGridDBColumn;
    cxGridDBTableView1cOBS: TcxGridDBColumn;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    qryChequeConcentracaoRiscoEmissor: TADOQuery;
    DataSource3: TDataSource;
    qryChequeConcentracaoRisconCdCheque: TIntegerField;
    qryChequeConcentracaoRisconCdEmpresa: TIntegerField;
    qryChequeConcentracaoRisconCdBanco: TIntegerField;
    qryChequeConcentracaoRiscocAgencia: TStringField;
    qryChequeConcentracaoRiscocConta: TStringField;
    qryChequeConcentracaoRiscocDigito: TStringField;
    qryChequeConcentracaoRiscocCNPJCPF: TStringField;
    qryChequeConcentracaoRiscoiNrCheque: TIntegerField;
    qryChequeConcentracaoRisconValCheque: TBCDField;
    qryChequeConcentracaoRiscodDtDeposito: TDateTimeField;
    cxGridDBTableView3nCdCheque: TcxGridDBColumn;
    cxGridDBTableView3nCdEmpresa: TcxGridDBColumn;
    cxGridDBTableView3nCdBanco: TcxGridDBColumn;
    cxGridDBTableView3cAgencia: TcxGridDBColumn;
    cxGridDBTableView3cConta: TcxGridDBColumn;
    cxGridDBTableView3cDigito: TcxGridDBColumn;
    cxGridDBTableView3cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView3iNrCheque: TcxGridDBColumn;
    cxGridDBTableView3nValCheque: TcxGridDBColumn;
    cxGridDBTableView3dDtDeposito: TcxGridDBColumn;
    qryChequeConcentracaoRiscoEmissorcCNPJCPF: TStringField;
    qryChequeConcentracaoRiscoEmissorEMISSOR: TStringField;
    qryChequeConcentracaoRiscoEmissorTOTAL: TBCDField;
    cxGridDBTableView2cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView2EMISSOR: TcxGridDBColumn;
    cxGridDBTableView2TOTAL: TcxGridDBColumn;
    qryChequePendentecNmTabStatusCheque: TStringField;
    qryChequeConcentracaoRiscocNmTabStatusCheque: TStringField;
    cxGridDBTableView3cNmTabStatusCheque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabStatusCheque: TcxGridDBColumn;
    procedure exibeValores(nCdTerceiro : integer; nCdPaginaAtiva : integer);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTerceiro_ChequePendente: TfrmTerceiro_ChequePendente;

implementation

{$R *.dfm}

{ TfrmTerceiro_ChequePendente }

procedure TfrmTerceiro_ChequePendente.exibeValores(nCdTerceiro: integer;nCdPaginaAtiva: integer);
begin

    cxPageControl1.ActivePageIndex := nCdPaginaAtiva;
    qryChequePendente.Close;
    qryChequePendente.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryChequePendente.Open;

    qryChequeConcentracaoRisco.Close;
    qryChequeConcentracaoRisco.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryChequeConcentracaoRisco.Open;

    qryChequeConcentracaoRiscoEmissor.Close;
    qryChequeConcentracaoRiscoEmissor.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryChequeConcentracaoRiscoEmissor.Open;

    if (not Self.Showing) then
        Self.ShowModal;

end;

end.
