inherited frmConsultaRequisAberta_Itens: TfrmConsultaRequisAberta_Itens
  Left = 245
  Top = 156
  Width = 773
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Itens Requisicao'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 757
  end
  inherited ToolBar1: TToolBar
    Width = 757
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 757
    Height = 435
    Align = alClient
    DataSource = dsItens
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemRequisicao'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdRequisicao'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        Width = 340
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeReq'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeAtend'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeCanc'
        Footers = <>
        Width = 63
      end
      item
        EditButtons = <>
        FieldName = 'nCdMapaCompra'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
      end>
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ItemRequisicao '
      ' WHERE nCdRequisicao = :nPK'
      '   AND (nQtdeAtend+nQtdeCanc < nQtdeReq)')
    Left = 280
    Top = 144
    object qryItensnCdItemRequisicao: TAutoIncField
      FieldName = 'nCdItemRequisicao'
      ReadOnly = True
    end
    object qryItensnCdRequisicao: TIntegerField
      FieldName = 'nCdRequisicao'
    end
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItenscNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 100
    end
    object qryItensnQtdeReq: TBCDField
      DisplayLabel = 'Quantidades|Requisitada'
      FieldName = 'nQtdeReq'
      Precision = 12
    end
    object qryItensnQtdeAtend: TBCDField
      DisplayLabel = 'Quantidades|Atendida'
      FieldName = 'nQtdeAtend'
      Precision = 12
    end
    object qryItensnQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancelada'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItensnCdMapaCompra: TIntegerField
      DisplayLabel = 'Processo Compra|Mapa'
      FieldName = 'nCdMapaCompra'
    end
    object qryItensnCdPedido: TIntegerField
      DisplayLabel = 'Processo Compra|Pedido'
      FieldName = 'nCdPedido'
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 328
    Top = 144
  end
end
