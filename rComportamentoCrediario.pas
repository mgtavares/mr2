unit rComportamentoCrediario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP;

type
  TrptComportamentoCrediario = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptComportamentoCrediario: TrptComportamentoCrediario;

implementation

uses fMenu, fLookup_Padrao, rVendaCrediario_view,
  rPosicaoInadimplencia_view, rComportamentoCrediario_view;

{$R *.dfm}

procedure TrptComportamentoCrediario.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptComportamentoCrediario.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptComportamentoCrediario.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptComportamentoCrediario.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;

  objRel        : TrptComportamentoCrediario_view ;

begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial de an�lise.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final de an�lise.') ;
      edtDtFinal.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtFinal.Text) < strToDate(edtDtInicial.Text)) then
  begin
      MensagemAlerta('Per�odo de an�lise inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (strToDate(edtDtFinal.Text) > (Date)) then
  begin
      MensagemAlerta('A data final de an�lise n�o pode ser maior que hoje');
      edtDtFinal.SetFocus;
      exit ;
  end ;

  objRel := TrptComportamentoCrediario_view.Create(Self) ;

  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Close;
  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;
  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(edtLoja.Text) ;
  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@dDtInicial').Value   := frmMenu.ConvData(edtDtInicial.Text) ;
  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@dDtFinal').Value     := frmMenu.ConvData(edtDtFinal.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@cFlgTipoData').Value := 'E'
  else objRel.SPREL_COMPORTAMENTO_CREDIARIO.Parameters.ParamByName('@cFlgTipoData').Value := 'V' ;

  objRel.SPREL_COMPORTAMENTO_CREDIARIO.Open;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  cFiltro := '' ;

  if (DBEdit1.Text <> '') then
      cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

  cFiltro := cFiltro + ' / Per�odo An�lise: ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text;

  if (RadioGroup1.ItemIndex = 0) then
      cFiltro := cFiltro + ' / Tipo Data: EMISS�O'
  else cFiltro := cFiltro + ' / Tipo Data: VENCIMENTO' ;

  objRel.lblFiltro1.Caption := cFiltro ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TrptComportamentoCrediario) ;

end.
