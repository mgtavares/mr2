unit fProdutoERP_UltimasCompras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmProdutoERP_UltimasCompras = class(TfrmProcesso_Padrao)
    qryUltimasCompras: TADOQuery;
    dsUltimasCompras: TDataSource;
    qryUltimasComprasdDtReceb: TDateTimeField;
    qryUltimasComprasnCdRecebimento: TIntegerField;
    qryUltimasComprascNrDocto: TStringField;
    qryUltimasComprascNmTerceiro: TStringField;
    qryUltimasComprasnValUnitario: TBCDField;
    qryUltimasComprasnPercIPI: TBCDField;
    qryUltimasComprasnPercICMSSub: TBCDField;
    qryUltimasComprasnValCustoFinal: TBCDField;
    qryUltimasComprasnCdPedido: TIntegerField;
    qryUltimasComprasnQtde: TBCDField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1dDtReceb: TcxGridDBColumn;
    cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGrid1DBTableView1cNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nPercIPI: TcxGridDBColumn;
    cxGrid1DBTableView1nPercICMSSub: TcxGridDBColumn;
    cxGrid1DBTableView1nValCustoFinal: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoERP_UltimasCompras: TfrmProdutoERP_UltimasCompras;

implementation

uses fRecebimento;

{$R *.dfm}

procedure TfrmProdutoERP_UltimasCompras.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmRecebimento;
begin
  inherited;

  objForm := TfrmRecebimento.Create(nil);

  objForm.PosicionaPK(qryUltimasComprasnCdRecebimento.Value);

  showForm(objForm,True);

end;

end.
