inherited frmPickPack: TfrmPickPack
  Caption = 'Separa'#231#227'o e Embalagem'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 161
    Height = 303
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 132
    Align = alTop
    TabOrder = 1
    object RadioGroup1: TRadioGroup
      Left = 8
      Top = 16
      Width = 185
      Height = 105
      Caption = 'Visualiza'#231#227'o'
      Items.Strings = (
        'Pedidos Aguardando Picking'
        'Pedidos em Picking'
        'Pedidos Autorizados para Sa'#237'da')
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 312
    Top = 48
  end
  object qryPedidoAguardandoPickingResumido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,Pedido.dDtAutor'
      '      ,Pedido.dDtPrevEntIni'
      '      ,Pedido.dDtPrevEntFim'
      
        '      ,(Convert(int,(dDtPrevEntIni-dbo.fn_OnlyDate(GetDate()))))' +
        ' iDiasEntrega'
      '      ,SUBSTRING(cNmTipoPedido,1,15) cNmTipoPedido'
      '      ,Entrega.cNmTerceiro cNmTerceiroEntrega'
      '      ,Pagador.cNmTerceiro cNmTerceiroPagador'
      
        '      ,(SELECT Sum(ItemPedido.nValCustoUnit * (ItemPedido.nQtdeP' +
        'ed - (ItemPedido.nQtdeCanc + ItemPedido.nQtdeExpRec + ItemPedido' +
        '.nQtdeLibFat)))'
      '          FROM ItemPedido'
      '         WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '           AND ItemPedido.nCdTipoItemPed IN (1,2,5,6,9)'
      
        '           AND nQtdePed > (nQtdeCanc + nQtdeExpRec + nQtdeLibFat' +
        ')) as nValSaldoItem'
      '      ,TabStatusPed.cNmTabStatusPed'
      '  FROM Pedido'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido  ' +
        '   = Pedido.nCdTipoPedido'
      
        '       INNER JOIN Terceiro Entrega ON Entrega.nCdTerceiro       ' +
        '   = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro Pagador ON Pagador.nCdTerceiro       ' +
        '   = Pedido.nCdTerceiroPagador'
      
        '       INNER JOIN TabStatusPed     ON TabStatusPed.nCdTabStatusP' +
        'ed = Pedido.nCdTabStatusPed'
      ' WHERE cFlgAguardarMontagem = 1'
      
        '   AND ((Pedido.nCdTabStatusPed IN (3,4)) OR (Pedido.nCdTabStatu' +
        'sPed = 2 AND Pedido.cFlgLibParcial = 1))'
      '   AND nCdEmpresa = :nPK'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND ItemPedido.nCdTipoItemPed IN (1,2,5,6,9)'
      
        '                 AND nQtdePed > (nQtdeCanc + nQtdeExpRec + nQtde' +
        'LibFat))'
      ' ORDER BY dDtPrevEntFim')
    Left = 104
    Top = 216
    object qryPedidoAguardandoPickingResumidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPedidoAguardandoPickingResumidodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryPedidoAguardandoPickingResumidodDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryPedidoAguardandoPickingResumidodDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
    end
    object qryPedidoAguardandoPickingResumidodDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
    end
    object qryPedidoAguardandoPickingResumidoiDiasEntrega: TIntegerField
      FieldName = 'iDiasEntrega'
      ReadOnly = True
    end
    object qryPedidoAguardandoPickingResumidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      ReadOnly = True
      Size = 15
    end
    object qryPedidoAguardandoPickingResumidocNmTerceiroEntrega: TStringField
      FieldName = 'cNmTerceiroEntrega'
      Size = 50
    end
    object qryPedidoAguardandoPickingResumidocNmTerceiroPagador: TStringField
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryPedidoAguardandoPickingResumidonValSaldoItem: TBCDField
      FieldName = 'nValSaldoItem'
      ReadOnly = True
      Precision = 38
      Size = 10
    end
    object qryPedidoAguardandoPickingResumidocNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
  end
  object qryPedidoAguardandoPickingDetalhado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,Pedido.dDtAutor'
      '      ,Pedido.dDtPrevEntIni'
      '      ,Pedido.dDtPrevEntFim'
      
        '      ,(Convert(int,(dDtPrevEntIni-dbo.fn_OnlyDate(GetDate()))))' +
        ' iDiasEntrega'
      '      ,SUBSTRING(cNmTipoPedido,1,15) cNmTipoPedido'
      '      ,Entrega.cNmTerceiro cNmTerceiroEntrega'
      '      ,Pagador.cNmTerceiro cNmTerceiroPagador'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.cDescricaoTecnica'
      
        '      ,(ItemPedido.nQtdePed - (ItemPedido.nQtdeCanc + ItemPedido' +
        '.nQtdeExpRec + ItemPedido.nQtdeLibFat)) as nSaldoQtde'
      
        '      ,ItemPedido.nValCustoUnit * (ItemPedido.nQtdePed - (ItemPe' +
        'dido.nQtdeCanc + ItemPedido.nQtdeExpRec + ItemPedido.nQtdeLibFat' +
        ')) as nValSaldoItem'
      '      ,TabStatusPed.cNmTabStatusPed'
      '  FROM Pedido'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido  ' +
        '   = Pedido.nCdTipoPedido'
      
        '       INNER JOIN Terceiro Entrega ON Entrega.nCdTerceiro       ' +
        '   = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro Pagador ON Pagador.nCdTerceiro       ' +
        '   = Pedido.nCdTerceiroPagador'
      
        '       INNER JOIN ItemPedido       ON ItemPedido.nCdPedido      ' +
        '   = Pedido.nCdPedido'
      
        '       INNER JOIN TabStatusPed     ON TabStatusPed.nCdTabStatusP' +
        'ed = Pedido.nCdTabStatusPed'
      ' WHERE cFlgAguardarMontagem = 1'
      
        '   AND ((Pedido.nCdTabStatusPed IN (3,4)) OR (Pedido.nCdTabStatu' +
        'sPed = 2 AND Pedido.cFlgLibParcial = 1))'
      '   AND nCdEmpresa = :nPK'
      '   AND ItemPedido.nCdTipoItemPed IN (1,2,5,6,9)'
      '   AND nQtdePed > (nQtdeCanc + nQtdeExpRec + nQtdeLibFat)'
      ' ORDER BY dDtPrevEntFim')
    Left = 152
    Top = 216
    object qryPedidoAguardandoPickingDetalhadonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPedidoAguardandoPickingDetalhadodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryPedidoAguardandoPickingDetalhadodDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryPedidoAguardandoPickingDetalhadodDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
    end
    object qryPedidoAguardandoPickingDetalhadodDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
    end
    object qryPedidoAguardandoPickingDetalhadoiDiasEntrega: TIntegerField
      FieldName = 'iDiasEntrega'
      ReadOnly = True
    end
    object qryPedidoAguardandoPickingDetalhadocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      ReadOnly = True
      Size = 15
    end
    object qryPedidoAguardandoPickingDetalhadocNmTerceiroEntrega: TStringField
      FieldName = 'cNmTerceiroEntrega'
      Size = 50
    end
    object qryPedidoAguardandoPickingDetalhadocNmTerceiroPagador: TStringField
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryPedidoAguardandoPickingDetalhadonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryPedidoAguardandoPickingDetalhadocNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryPedidoAguardandoPickingDetalhadocDescricaoTecnica: TMemoField
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
    object qryPedidoAguardandoPickingDetalhadonSaldoQtde: TBCDField
      FieldName = 'nSaldoQtde'
      ReadOnly = True
      Precision = 16
    end
    object qryPedidoAguardandoPickingDetalhadonValSaldoItem: TBCDField
      FieldName = 'nValSaldoItem'
      ReadOnly = True
      Precision = 31
      Size = 10
    end
    object qryPedidoAguardandoPickingDetalhadocNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
  end
end
