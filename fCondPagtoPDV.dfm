inherited frmCondPagtoPDV: TfrmCondPagtoPDV
  Left = 264
  Top = 31
  Width = 876
  Height = 601
  Caption = 'Condi'#231#245'es de Pagamento para PDV'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 860
    Height = 538
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 52
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label5: TLabel [3]
    Left = 8
    Top = 94
    Width = 93
    Height = 13
    Caption = 'Forma Pagamento'
    FocusControl = DBEdit6
  end
  inherited ToolBar2: TToolBar
    Width = 860
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCondPagto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 104
    Top = 64
    Width = 657
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit6: TDBEdit [7]
    Left = 104
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdFormaPagto'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [8]
    Tag = 1
    Left = 172
    Top = 88
    Width = 589
    Height = 19
    DataField = 'cNmFormaPagto'
    DataSource = dsFormaPagto
    TabOrder = 4
  end
  object cxPageControl1: TcxPageControl [9]
    Left = 16
    Top = 128
    Width = 745
    Height = 417
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    ClientRectBottom = 413
    ClientRectLeft = 4
    ClientRectRight = 741
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Instru'#231#245'es'
      ImageIndex = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 737
        Height = 241
        Align = alTop
        BiDiMode = bdLeftToRight
        Caption = ' Instru'#231#245'es para PDV '
        Color = clWhite
        Ctl3D = False
        ParentBackground = False
        ParentBiDiMode = False
        ParentColor = False
        ParentCtl3D = False
        TabOrder = 0
        object Label3: TLabel
          Tag = 1
          Left = 9
          Top = 30
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo de Validade'
          FocusControl = DBEdit3
          WordWrap = True
        end
        object Label7: TLabel
          Tag = 1
          Left = 296
          Top = 28
          Width = 129
          Height = 15
          Alignment = taRightJustify
          AutoSize = False
          Caption = '% Acr'#233'scimo / Desconto'
          FocusControl = DBEdit8
          WordWrap = True
        end
        object Label10: TLabel
          Tag = 1
          Left = 12
          Top = 62
          Width = 101
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de Parcelas'
          FocusControl = DBEdit11
        end
        object Label11: TLabel
          Tag = 1
          Left = 17
          Top = 102
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dias entre Parcelas'
          FocusControl = DBEdit13
        end
        object Label13: TLabel
          Tag = 1
          Left = 507
          Top = 14
          Width = 122
          Height = 26
          Caption = '(Para desconto informe o percentual negativo)'
          FocusControl = DBEdit13
          WordWrap = True
        end
        object Label14: TLabel
          Tag = 1
          Left = 195
          Top = 54
          Width = 91
          Height = 39
          Caption = '(A entrada '#233' considerada uma parcela)'
          FocusControl = DBEdit13
          WordWrap = True
        end
        object Label6: TLabel
          Tag = 1
          Left = 18
          Top = 134
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dias M'#225'ximo Pagto'
          FocusControl = DBEdit9
        end
        object Label15: TLabel
          Tag = 1
          Left = 296
          Top = 60
          Width = 129
          Height = 15
          Alignment = taRightJustify
          AutoSize = False
          Caption = '% Vale Desconto'
          FocusControl = DBEdit14
          WordWrap = True
        end
        object DBEdit3: TDBEdit
          Left = 120
          Top = 24
          Width = 73
          Height = 19
          DataField = 'dDtValidadeIni'
          DataSource = dsMaster
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 200
          Top = 24
          Width = 73
          Height = 19
          DataField = 'dDtValidadeFim'
          DataSource = dsMaster
          TabOrder = 1
        end
        object DBCheckBox1: TDBCheckBox
          Left = 8
          Top = 166
          Width = 177
          Height = 17
          Caption = 'Condi'#231#227'o Ativa'
          DataField = 'cFlgAtivo'
          DataSource = dsMaster
          TabOrder = 8
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 432
          Top = 190
          Width = 161
          Height = 17
          Caption = 'Permitir Desconto Manual'
          DataField = 'cFlgPermDesconto'
          DataSource = dsMaster
          TabOrder = 9
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 8
          Top = 215
          Width = 297
          Height = 17
          Caption = 'Permitir Liquidar Presta'#231#227'o/Resgatar Cheques'
          DataField = 'cFlgLiqCrediario'
          DataSource = dsMaster
          TabOrder = 10
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit8: TDBEdit
          Left = 432
          Top = 24
          Width = 65
          Height = 19
          DataField = 'nPercAcrescimo'
          DataSource = dsMaster
          TabOrder = 2
        end
        object DBCheckBox7: TDBCheckBox
          Left = 432
          Top = 215
          Width = 209
          Height = 17
          Caption = 'Permite Produtos em Promo'#231#227'o'
          DataField = 'cFlgPermPromocao'
          DataSource = dsMaster
          TabOrder = 11
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit11: TDBEdit
          Left = 120
          Top = 56
          Width = 65
          Height = 19
          DataField = 'iQtdeParcelas'
          DataSource = dsMaster
          TabOrder = 3
        end
        object DBEdit13: TDBEdit
          Left = 120
          Top = 96
          Width = 65
          Height = 19
          DataField = 'iDiasProxParc'
          DataSource = dsMaster
          TabOrder = 6
          OnExit = DBEdit13Exit
        end
        object DBEdit9: TDBEdit
          Left = 120
          Top = 128
          Width = 65
          Height = 19
          DataField = 'iDiasMaxPrimParc'
          DataSource = dsMaster
          TabOrder = 7
        end
        object DBEdit14: TDBEdit
          Left = 432
          Top = 56
          Width = 65
          Height = 19
          DataField = 'nPercValeDesconto'
          DataSource = dsMaster
          TabOrder = 4
        end
        object DBCheckBox5: TDBCheckBox
          Left = 432
          Top = 80
          Width = 273
          Height = 25
          Caption = 'Gerar Vale Desconto para Produto em Promo'#231#227'o'
          DataField = 'cFlgGeraValeDescontoPromocao'
          DataSource = dsMaster
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox6: TDBCheckBox
          Left = 432
          Top = 166
          Width = 161
          Height = 17
          Caption = 'Arredondar Pre'#231'o do PDV'
          DataField = 'cFlgArredondaPrecoPDV'
          DataSource = dsMaster
          TabOrder = 12
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox8: TDBCheckBox
          Left = 8
          Top = 190
          Width = 177
          Height = 17
          Caption = 'Condi'#231#227'o Ativa na Web'
          DataField = 'cFlgCondPagtoWeb'
          DataSource = dsMaster
          TabOrder = 13
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 241
        Width = 737
        Height = 148
        Align = alClient
        Caption = ' Instru'#231#245'es para para a Primeira Parcela '
        TabOrder = 1
        object Label9: TLabel
          Tag = 1
          Left = 336
          Top = 110
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = '% Fixo de Entrada'
          FocusControl = DBEdit12
        end
        object Label4: TLabel
          Tag = 1
          Left = 25
          Top = 78
          Width = 131
          Height = 13
          Alignment = taRightJustify
          Caption = 'Dias para Primeira Parcela'
          FocusControl = DBEdit5
        end
        object Label8: TLabel
          Tag = 1
          Left = 272
          Top = 78
          Width = 156
          Height = 13
          Alignment = taRightJustify
          Caption = 'Data Sugerida Primeira Parcela'
          FocusControl = DBEdit10
        end
        object Label12: TLabel
          Tag = 1
          Left = 524
          Top = 110
          Width = 181
          Height = 13
          Caption = '(Deixar zerado para parcelas iguais)'
          FocusControl = DBEdit12
        end
        object DBCheckBox3: TDBCheckBox
          Left = 8
          Top = 32
          Width = 241
          Height = 17
          Caption = 'Considerar a Primeira Parcela como Entrada'
          DataField = 'cFlgPrimParcEnt'
          DataSource = dsMaster
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
          OnClick = DBCheckBox3Click
        end
        object DBEdit12: TDBEdit
          Left = 432
          Top = 104
          Width = 89
          Height = 19
          DataField = 'nPercEntrada'
          DataSource = dsMaster
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 160
          Top = 72
          Width = 65
          Height = 19
          DataField = 'iDiasPrimParc'
          DataSource = dsMaster
          TabOrder = 1
        end
        object DBEdit10: TDBEdit
          Left = 432
          Top = 72
          Width = 89
          Height = 19
          DataField = 'dDtPrimParcela'
          DataSource = dsMaster
          TabOrder = 2
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Lojas Autorizadas'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 737
        Height = 389
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLojaCondPagto
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdCondPagto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Width = 38
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Width = 659
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    LockType = ltBatchOptimistic
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CondPagto'
      'WHERE nCdCondPagto = :nPK'
      'AND cFlgPDV = 1')
    Left = 336
    Top = 272
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMastercNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryMasternCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMasterdDtValidadeIni: TDateTimeField
      FieldName = 'dDtValidadeIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtValidadeFim: TDateTimeField
      FieldName = 'dDtValidadeFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMastercFlgPermDesconto: TIntegerField
      FieldName = 'cFlgPermDesconto'
    end
    object qryMasternFatorArred: TBCDField
      FieldName = 'nFatorArred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriParcArred: TIntegerField
      FieldName = 'iParcArred'
    end
    object qryMastercFlgPrimParcEnt: TIntegerField
      FieldName = 'cFlgPrimParcEnt'
    end
    object qryMastercFlgPDV: TIntegerField
      FieldName = 'cFlgPDV'
    end
    object qryMastercFlgLiqCrediario: TIntegerField
      FieldName = 'cFlgLiqCrediario'
    end
    object qryMasternPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercEntrada: TBCDField
      FieldName = 'nPercEntrada'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriDiasPrimParc: TIntegerField
      FieldName = 'iDiasPrimParc'
    end
    object qryMasteriDiasMaxPrimParc: TIntegerField
      FieldName = 'iDiasMaxPrimParc'
    end
    object qryMasteriDiasProxParc: TIntegerField
      FieldName = 'iDiasProxParc'
    end
    object qryMastercFlgPermPromocao: TIntegerField
      FieldName = 'cFlgPermPromocao'
    end
    object qryMasterdDtPrimParcela: TDateTimeField
      FieldName = 'dDtPrimParcela'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasteriQtdeParcelas: TIntegerField
      FieldName = 'iQtdeParcelas'
    end
    object qryMasternPercValeDesconto: TBCDField
      FieldName = 'nPercValeDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgGeraValeDescontoPromocao: TIntegerField
      FieldName = 'cFlgGeraValeDescontoPromocao'
    end
    object qryMastercFlgArredondaPrecoPDV: TIntegerField
      FieldName = 'cFlgArredondaPrecoPDV'
    end
    object qryMastercFlgCondPagtoWeb: TIntegerField
      FieldName = 'cFlgCondPagtoWeb'
    end
  end
  inherited dsMaster: TDataSource
    Left = 336
    Top = 304
  end
  inherited qryID: TADOQuery
    Left = 336
    Top = 336
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 336
    Top = 368
  end
  inherited qryStat: TADOQuery
    Left = 400
    Top = 368
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 368
    Top = 368
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 400
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdFormaPagto, cNmFormaPagto'
      'FROM FormaPagto'
      'WHERE nCdFormaPagto = :nPK')
    Left = 368
    Top = 272
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagto
    Left = 368
    Top = 304
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 368
    Top = 336
  end
  object qryLojaCondPagto: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    BeforePost = qryLojaCondPagtoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM LojaCondPagto'
      'WHERE nCdCondPagto = :nPK')
    Left = 400
    Top = 272
    object qryLojaCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryLojaCondPagtonCdLoja: TIntegerField
      DisplayLabel = 'Lojas Autorizadas|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryLojaCondPagtocNmLoja: TStringField
      DisplayLabel = 'Lojas Autorizadas|Nome'
      FieldKind = fkLookup
      FieldName = 'cNmLoja'
      LookupDataSet = qryLoja
      LookupKeyFields = 'nCdLoja'
      LookupResultField = 'cNmLoja'
      KeyFields = 'nCdLoja'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryLojaCondPagtonCdLojaCondPagto: TIntegerField
      FieldName = 'nCdLojaCondPagto'
    end
  end
  object dsLojaCondPagto: TDataSource
    DataSet = qryLojaCondPagto
    Left = 400
    Top = 304
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja')
    Left = 400
    Top = 336
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
end
