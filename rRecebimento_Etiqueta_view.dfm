object rptRecebimento_Etiqueta_view: TrptRecebimento_Etiqueta_view
  Left = -8
  Top = -8
  Width = 1280
  Height = 786
  Caption = 'Etiqueta Recebimento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object QuickRep1: TQuickRep
    Left = 8
    Top = 8
    Width = 945
    Height = 1338
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    DataSet = cdsEtiquetas
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = B4
    Page.Values = (
      0.100000000000000000
      3540.000000000000000000
      130.000000000000000000
      2500.000000000000000000
      50.000000000000000000
      50.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    object DetailBand1: TQRBand
      Left = 19
      Top = 49
      Width = 907
      Height = 80
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        211.666666666666700000
        2399.770833333333000000)
      BandType = rbDetail
      object QRDBText2: TQRDBText
        Left = 8
        Top = 56
        Width = 137
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          21.166666666666670000
          148.166666666666700000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cNmProduto1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 5
      end
      object QRDBText3: TQRDBText
        Left = 8
        Top = 48
        Width = 137
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          21.166666666666670000
          127.000000000000000000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cReferencia1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        OnPrint = QRDBText3Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 6
      end
      object SQBarra1: TSvQRBarcode
        Left = 8
        Top = 8
        Width = 130
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.958333333333330000
          21.166666666666670000
          21.166666666666670000
          343.958333333333300000)
        BarcodeHeight = 34
        BarcodeWidth = 129
        Modul = 1
        Ratio = 2.000000000000000000
        Typ = bcCode39
        ShowText = bcoCode
        BeforePrint = SQBarra1BeforePrint
        Text = '00000000'
      end
      object SQBarra2: TSvQRBarcode
        Left = 160
        Top = 8
        Width = 130
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.958333333333330000
          423.333333333333300000
          21.166666666666670000
          343.958333333333300000)
        BarcodeHeight = 34
        BarcodeWidth = 129
        Modul = 1
        Ratio = 2.000000000000000000
        Typ = bcCode39
        ShowText = bcoCode
        BeforePrint = SQBarra2BeforePrint
        Text = '00000000'
      end
      object QRDBText1: TQRDBText
        Left = 160
        Top = 48
        Width = 137
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          423.333333333333300000
          127.000000000000000000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cReferencia2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        OnPrint = QRDBText1Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 6
      end
      object QRDBText4: TQRDBText
        Left = 160
        Top = 56
        Width = 137
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          423.333333333333300000
          148.166666666666700000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cNmProduto2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 5
      end
      object SQBarra3: TSvQRBarcode
        Left = 312
        Top = 8
        Width = 130
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.958333333333330000
          825.500000000000000000
          21.166666666666670000
          343.958333333333300000)
        BarcodeHeight = 34
        BarcodeWidth = 129
        Modul = 1
        Ratio = 2.000000000000000000
        Typ = bcCode39
        ShowText = bcoCode
        BeforePrint = SQBarra3BeforePrint
        Text = '00000000'
      end
      object SQBarra4: TSvQRBarcode
        Left = 464
        Top = 8
        Width = 130
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.958333333333330000
          1227.666666666667000000
          21.166666666666670000
          343.958333333333300000)
        BarcodeHeight = 34
        BarcodeWidth = 129
        Modul = 1
        Ratio = 2.000000000000000000
        Typ = bcCode39
        ShowText = bcoCode
        BeforePrint = SQBarra4BeforePrint
        Text = '00000000'
      end
      object QRDBText5: TQRDBText
        Left = 464
        Top = 48
        Width = 137
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          1227.666666666667000000
          127.000000000000000000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cReferencia4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        OnPrint = QRDBText5Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 6
      end
      object QRDBText6: TQRDBText
        Left = 464
        Top = 56
        Width = 137
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1227.666666666667000000
          148.166666666666700000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cNmProduto4'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 5
      end
      object QRDBText7: TQRDBText
        Left = 312
        Top = 56
        Width = 137
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          825.500000000000000000
          148.166666666666700000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cNmProduto3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 5
      end
      object QRDBText8: TQRDBText
        Left = 312
        Top = 48
        Width = 137
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          825.500000000000000000
          127.000000000000000000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cReferencia3'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        OnPrint = QRDBText8Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 6
      end
      object SQBarra5: TSvQRBarcode
        Left = 616
        Top = 8
        Width = 130
        Height = 34
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          89.958333333333330000
          1629.833333333333000000
          21.166666666666670000
          343.958333333333300000)
        BarcodeHeight = 34
        BarcodeWidth = 129
        Modul = 1
        Ratio = 2.000000000000000000
        Typ = bcCode39
        ShowText = bcoCode
        BeforePrint = SQBarra5BeforePrint
        Text = '00000000'
      end
      object QRDBText9: TQRDBText
        Left = 616
        Top = 48
        Width = 137
        Height = 9
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          23.812500000000000000
          1629.833333333333000000
          127.000000000000000000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cReferencia5'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Courier New'
        Font.Style = []
        OnPrint = QRDBText9Print
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 6
      end
      object QRDBText10: TQRDBText
        Left = 616
        Top = 56
        Width = 137
        Height = 17
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          44.979166666666670000
          1629.833333333333000000
          148.166666666666700000
          362.479166666666700000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        DataSet = cdsEtiquetas
        DataField = 'cNmProduto5'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 5
      end
    end
  end
  object cdsEtiquetas: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'cCdBarra1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cNmProduto1'
        DataType = ftString
        Size = 150
      end
      item
        Name = 'cReferencia1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cMsg1'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'cNmLoja1'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'cCdbarra2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cNmProduto2'
        DataType = ftString
        Size = 150
      end
      item
        Name = 'cReferencia2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cMsg2'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'cNmLoja2'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'cCdBarra3'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cCdBarra4'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cCdBarra5'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cReferencia3'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cReferencia4'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'cReferencia5'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'cNmProduto3'
        DataType = ftString
        Size = 150
      end
      item
        Name = 'cNmProduto4'
        DataType = ftString
        Size = 150
      end
      item
        Name = 'cNmProduto5'
        DataType = ftString
        Size = 150
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 232
    Top = 304
    Data = {
      630200009619E0BD010000001800000013000000000003000000630209634364
      42617272613101004900000001000557494454480200020014000B634E6D5072
      6F6475746F3101004900000001000557494454480200020096000C6352656665
      72656E63696131010049000000010005574944544802000200140005634D7367
      31010049000000010005574944544802000200280008634E6D4C6F6A61310100
      4900000001000557494454480200020028000963436462617272613201004900
      000001000557494454480200020014000B634E6D50726F6475746F3201004900
      000001000557494454480200020096000C635265666572656E63696132010049
      000000010005574944544802000200140005634D736732010049000000010005
      574944544802000200280008634E6D4C6F6A6132010049000000010005574944
      5448020002002800096343644261727261330100490000000100055749445448
      0200020014000963436442617272613401004900000001000557494454480200
      0200140009634364426172726135010049000000010005574944544802000200
      14000C635265666572656E636961330100490000000100055749445448020002
      0014000C635265666572656E6369613401004900000001000557494454480200
      020014000C635265666572656E63696135010049000000010005574944544802
      0002001E000B634E6D50726F6475746F33010049000000010005574944544802
      00020096000B634E6D50726F6475746F34010049000000010005574944544802
      00020096000B634E6D50726F6475746F35010049000000010005574944544802
      00020096000000}
    object cdsEtiquetascCdBarra1: TStringField
      FieldName = 'cCdBarra1'
    end
    object cdsEtiquetascNmProduto1: TStringField
      FieldName = 'cNmProduto1'
      Size = 150
    end
    object cdsEtiquetascReferencia1: TStringField
      FieldName = 'cReferencia1'
      Size = 30
    end
    object cdsEtiquetascMsg1: TStringField
      DisplayWidth = 40
      FieldName = 'cMsg1'
      Size = 40
    end
    object cdsEtiquetascNmLoja1: TStringField
      FieldName = 'cNmLoja1'
      Size = 40
    end
    object cdsEtiquetascCdbarra2: TStringField
      FieldName = 'cCdbarra2'
    end
    object cdsEtiquetascNmProduto2: TStringField
      FieldName = 'cNmProduto2'
      Size = 150
    end
    object cdsEtiquetascReferencia2: TStringField
      FieldName = 'cReferencia2'
      Size = 30
    end
    object cdsEtiquetascMsg2: TStringField
      FieldName = 'cMsg2'
      Size = 40
    end
    object cdsEtiquetascNmLoja2: TStringField
      FieldName = 'cNmLoja2'
      Size = 40
    end
    object cdsEtiquetascCdBarra3: TStringField
      FieldName = 'cCdBarra3'
    end
    object cdsEtiquetascCdBarra4: TStringField
      FieldName = 'cCdBarra4'
    end
    object cdsEtiquetascCdBarra5: TStringField
      FieldName = 'cCdBarra5'
    end
    object cdsEtiquetascReferencia3: TStringField
      FieldName = 'cReferencia3'
      Size = 30
    end
    object cdsEtiquetascReferencia4: TStringField
      FieldName = 'cReferencia4'
      Size = 30
    end
    object cdsEtiquetascReferencia5: TStringField
      FieldName = 'cReferencia5'
      Size = 30
    end
    object cdsEtiquetascNmProduto3: TStringField
      FieldName = 'cNmProduto3'
      Size = 150
    end
    object cdsEtiquetascNmProduto4: TStringField
      FieldName = 'cNmProduto4'
      Size = 150
    end
    object cdsEtiquetascNmProduto5: TStringField
      FieldName = 'cNmProduto5'
      Size = 150
    end
  end
end
