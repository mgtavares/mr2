inherited rptVendaXVendedorAnalitico: TrptVendaXVendedorAnalitico
  Left = 183
  Top = 100
  Caption = 'Rel. Venda X Vendedor - Anal'#237'tico'
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [1]
    Left = 78
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label1: TLabel [2]
    Left = 52
    Top = 72
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vendedor'
  end
  object Label3: TLabel [3]
    Left = 7
    Top = 96
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Movimento'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    TabOrder = 4
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 176
    Top = 40
    Width = 455
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 176
    Top = 64
    Width = 455
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = dsVendedor
    TabOrder = 6
  end
  object edtDtInicial: TMaskEdit [8]
    Left = 104
    Top = 88
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtDtFinal: TMaskEdit [9]
    Left = 216
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtLoja: TMaskEdit [10]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtVendedor: TMaskEdit [11]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtVendedorExit
    OnKeyDown = edtVendedorKeyDown
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja =:nPK')
    Left = 96
    Top = 280
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 104
    Top = 312
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario               =:nPK'
      '   AND Usuario.nCdStatus        = 1'
      '   AND (Usuario.cFlgVendedor    = 1'
      '        OR Usuario.cFlgGerente  = 1)'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdUsuario = Usuario.nCdUsuario'
      '                 AND UL.nCdLoja =:nCdLoja)'
      'ORDER BY Usuario.cNmUsuario')
    Left = 144
    Top = 280
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 152
    Top = 312
  end
end
