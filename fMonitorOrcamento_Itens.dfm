inherited frmMonitorOrcamento_Itens: TfrmMonitorOrcamento_Itens
  Left = 155
  Top = 201
  Height = 368
  Caption = 'Itens'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 303
  end
  object Label1: TLabel [1]
    Left = 472
    Top = 32
    Width = 85
    Height = 13
    Caption = 'Descri'#231#227'o T'#233'cnica'
    FocusControl = DBMemo1
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 32
    Width = 465
    Height = 297
    DataSource = DataSource1
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemPedido'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePed'
        Footers = <>
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        Width = 382
      end>
  end
  object DBMemo1: TDBMemo [4]
    Left = 472
    Top = 48
    Width = 305
    Height = 281
    DataField = 'cDescricaoTecnica'
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 2
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdItemPedido, nCdProduto, cNmItem, nQtdePed, cDescricaoT' +
        'ecnica'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK'
      'AND nCdTipoItemPed IN (1,2,3,5,6,9,10,11)'
      '')
    Left = 512
    Top = 176
    object qryItemPedidonCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      DisplayLabel = 'Itens do Pedido|Descri'#231#227'o Resumida'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtdePed: TBCDField
      DisplayLabel = 'Itens do Pedido|Qtde'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidocDescricaoTecnica: TMemoField
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
  end
  object DataSource1: TDataSource
    DataSet = qryItemPedido
    Left = 384
    Top = 240
  end
end
