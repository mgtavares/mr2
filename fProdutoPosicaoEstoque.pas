unit fProdutoPosicaoEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, StdCtrls, Mask, DBCtrls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmProdutoPosicaoEstoque = class(TfrmProcesso_Padrao)
    qryPosicaoEstoque: TADOQuery;
    dsPosicaoEstoque: TDataSource;
    qryPosicaoEstoquenCdEmpresa: TIntegerField;
    qryPosicaoEstoquecNmEmpresa: TStringField;
    qryPosicaoEstoquenCdLoja: TIntegerField;
    qryPosicaoEstoquecNmLoja: TStringField;
    qryPosicaoEstoquenCdLocalEstoque: TIntegerField;
    qryPosicaoEstoquecNmLocalEstoque: TStringField;
    qryPosicaoEstoquenQtdeDisponivel: TBCDField;
    qryPosicaoEstoquenQtdeTransito: TBCDField;
    qryPosicaoEstoquedDtUltInventario: TDateTimeField;
    qryPosicaoEstoquenCdProduto: TIntegerField;
    qryPosicaoEstoquecNmProduto: TStringField;
    GroupBox1: TGroupBox;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    qryPosicaoEstoquecDisponivel: TStringField;
    qryPosicaoEstoquecTerceiro: TStringField;
    qryPosicaoEstoquedDtUltRecebEstoque: TDateTimeField;
    qryPosicaoEstoquedDtUltVendaEstoque: TDateTimeField;
    qryPosicaoEstoquenQtdeDisponivelReal: TBCDField;
    qryPosicaoEstoquenQtdeEmpenhado: TBCDField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView1nQtdeDisponivelReal: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeTransito: TcxGridDBColumn;
    cxGridDBTableView1nQtdeEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltRecebEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltVendaEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltInventario: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryPosicaoEstoquedDtUltRecebEstoqueCD: TDateTimeField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoPosicaoEstoque: TfrmProdutoPosicaoEstoque;

implementation

{$R *.dfm}

uses
  fMenu;

end.
