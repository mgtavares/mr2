unit fTipoOrdemProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmTipoOrdemProducao = class(TfrmCadastro_Padrao)
    qryMasternCdTipoOP: TIntegerField;
    qryMastercNmTipoOP: TStringField;
    qryMasternCdTipoOPRetrabalho: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    edtOpRetrabalho: TER2LookupDBEdit;
    qryTipoOPRet: TADOQuery;
    qryOpEstoqueCP: TADOQuery;
    qryOpEstoqueSLP: TADOQuery;
    Label3: TLabel;
    qryTipoOPRetnCdTipoOP: TIntegerField;
    qryTipoOPRetcNmTipoOP: TStringField;
    DBEdit3: TDBEdit;
    dsTipoOPRet: TDataSource;
    qryOpEstoqueCPnCdOperacaoEstoque: TIntegerField;
    qryOpEstoqueCPcNmOperacaoEstoque: TStringField;
    dsOPEstoqueCP: TDataSource;
    qryOpEstoqueSLPnCdOperacaoEstoque: TIntegerField;
    qryOpEstoqueSLPcNmOperacaoEstoque: TStringField;
    dsOPEstoqueRE: TDataSource;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    edtEstoqueCP: TER2LookupDBEdit;
    edtEstoqueRE: TER2LookupDBEdit;
    DBEdit5: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    edtTransfEntrada: TER2LookupDBEdit;
    edtTransfSaida: TER2LookupDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    edtDevEntrada: TER2LookupDBEdit;
    edtDevSaida: TER2LookupDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    qryOpEstoqueELP: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    dsOpEstoqueELP: TDataSource;
    qryOpEstoqueBLP: TADOQuery;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    dsOpEstoqueBLP: TDataSource;
    qryOpEstoqueDLP: TADOQuery;
    IntegerField3: TIntegerField;
    StringField3: TStringField;
    dsOpEstoqueDLP: TDataSource;
    qryOpEstoqueDPA: TADOQuery;
    IntegerField4: TIntegerField;
    StringField4: TStringField;
    dsOpEstoqueDPA: TDataSource;
    qryMasternCdOperacaoEstoqueCP: TIntegerField;
    qryMasternCdOperEstoqueBaixaExplosao: TIntegerField;
    qryMasternCdOperTransfEstoqueSaida: TIntegerField;
    qryMasternCdOperTransfEstoqueEntrada: TIntegerField;
    qryMasternCdOperDevEstoqueSaida: TIntegerField;
    qryMasternCdOperDevEstoqueEntrada: TIntegerField;
    qryMasternCdTipoRequisicao: TIntegerField;
    edtTipoRequisicao: TER2LookupDBEdit;
    Label10: TLabel;
    qryTipoRequisicao: TADOQuery;
    qryTipoRequisicaocNmTipoRequisicao: TStringField;
    DBEdit10: TDBEdit;
    DataSource1: TDataSource;
    qryMastercFlgAtualizaPrecoCusto: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure edtOpRetrabalhoBeforeLookup(Sender: TObject);
    procedure edtEstoqueCPBeforeLookup(Sender: TObject);
    procedure edtEstoqueREBeforeLookup(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure edtTransfSaidaBeforeLookup(Sender: TObject);
    procedure edtTransfEntradaBeforeLookup(Sender: TObject);
    procedure edtDevSaidaBeforeLookup(Sender: TObject);
    procedure edtDevEntradaBeforeLookup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoOrdemProducao: TfrmTipoOrdemProducao;

implementation

{$R *.dfm}

procedure TfrmTipoOrdemProducao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOORDEMPRODUCAO' ;
  nCdTabelaSistema  := 313 ;
  nCdConsultaPadrao := 739 ;
  bCodigoAutomatico := True ;
end;

procedure TfrmTipoOrdemProducao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit2.Text) = '') then
  begin
     MensagemAlerta('Antes de Salvar preencha a descri��o do Tipo de Ordem de Produ��o');
     DBEdit2.SetFocus;
     Abort;
  end;

  if (qryMasternCdOperacaoEstoqueCP.Value = 0) then
  begin
     MensagemAlerta('Encerramento Produ��o/Opera��o Conclus�o em branco');
     edtEstoqueCP.SetFocus;
     Abort;
  end;

  if (qryMasternCdOperEstoqueBaixaExplosao.Value = 0) then
  begin
     MensagemAlerta('Encerramento Produ��o/Opera��o Baixa Explos�o em branco');
     edtEstoqueRE.SetFocus;
     Abort;
  end;

    if (qryMasternCdOperTransfEstoqueEntrada.Value = 0) then
  begin
     MensagemAlerta('Transfer�ncia Estoque/ Opera��o de Entrada em branco');
     edtTransfEntrada.SetFocus;
     Abort;
  end;

  if (qryMasternCdOperTransfEstoqueSaida.Value = 0) then
  begin
     MensagemAlerta('Transfer�ncia Estoque/ Opera��o de Sa�da em branco');
     edtTransfSaida.SetFocus;
     Abort;
  end;

  if (qryMasternCdOperDevEstoqueEntrada.Value = 0) then
  begin
     MensagemAlerta('Devolu��o Processo/ Opera��o de Entrada em branco');
     edtDevEntrada.SetFocus;
     Abort;
  end;

  if (qryMasternCdOperDevEstoqueSaida.Value = 0) then
  begin
     MensagemAlerta('Devolu��o Processo/ Opera��o de Sa�da em branco');
     edtDevSaida.SetFocus;
     Abort;
  end;

  inherited;
  
end;

procedure TfrmTipoOrdemProducao.edtOpRetrabalhoBeforeLookup(
  Sender: TObject);
begin
  inherited;
  
  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.edtEstoqueCPBeforeLookup(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.edtEstoqueREBeforeLookup(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTipoOPRet.Close;
  PosicionaQuery(qryTipoOPRet,qryMasternCdTipoOPRetrabalho.AsString);

  qryOpEstoqueCP.Close;
  PosicionaQuery(qryOpEstoqueCP,qryMasternCdOperacaoEstoqueCP.AsString);

  qryOpEstoqueSLP.Close;
  PosicionaQuery(qryOpEstoqueSLP,qryMasternCdOperEstoqueBaixaExplosao.AsString);

  qryOpEstoqueBLP.Close;
  PosicionaQuery(qryOpEstoqueBLP,qryMasternCdOperTransfEstoqueSaida.AsString);

  qryOpEstoqueELP.Close;
  PosicionaQuery(qryOpEstoqueELP,qryMasternCdOperTransfEstoqueEntrada.AsString);

  qryOpEstoqueDLP.Close;
  PosicionaQuery(qryOpEstoqueDLP,qryMasternCdOperDevEstoqueSaida.AsString);

  qryOpEstoqueDPA.Close;
  PosicionaQuery(qryOpEstoqueDPA,qryMasternCdOperDevEstoqueEntrada.AsString);

  qryTipoRequisicao.Close;
  PosicionaQuery( qryTipoRequisicao, qryMasternCdTipoRequisicao.asString ) ;

end;

procedure TfrmTipoOrdemProducao.qryMasterBeforeClose(DataSet: TDataSet);
begin
  inherited;

  qryTipoOPRet.Close;
  qryOpEstoqueCP.Close;
  qryOpEstoqueSLP.Close;
  qryOpEstoqueBLP.Close;
  qryOpEstoqueELP.Close;
  qryOpEstoqueDLP.Close;
  qryOpEstoqueDPA.Close;
  qryTipoRequisicao.Close;
  
end;

procedure TfrmTipoOrdemProducao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmTipoOrdemProducao.edtTransfSaidaBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.edtTransfEntradaBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.edtDevSaidaBeforeLookup(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

procedure TfrmTipoOrdemProducao.edtDevEntradaBeforeLookup(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      abort;
end;

initialization
    RegisterClass(TfrmTipoOrdemProducao);

end.
