unit fConsTituloAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, fRelatorio_Padrao,
  cxLookAndFeelPainters, cxButtons, ComObj, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk;

type
  TfrmConsTituloAberto = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    usp_Consulta: TADOStoredProc;
    DataSource6: TDataSource;
    usp_ConsultanCdTitulo: TIntegerField;
    usp_ConsultanCdEspTit: TIntegerField;
    usp_ConsultacNmEspTit: TStringField;
    usp_ConsultanCdSP: TIntegerField;
    usp_ConsultacNrTit: TStringField;
    usp_ConsultaiParcela: TIntegerField;
    usp_ConsultanCdTerceiro: TIntegerField;
    usp_ConsultacNmTerceiro: TStringField;
    usp_ConsultacSenso: TStringField;
    usp_ConsultadDtVenc: TDateTimeField;
    usp_ConsultanValTit: TBCDField;
    usp_ConsultanSaldoTit: TBCDField;
    usp_ConsultacSiglaEmp: TStringField;
    cxButton1: TcxButton;
    usp_ConsultacNrNF: TStringField;
    usp_ConsultanCdMoeda: TIntegerField;
    usp_ConsultacSigla: TStringField;
    usp_ConsultadDtCancel: TDateTimeField;
    usp_ConsultanCdProvisaoTit: TIntegerField;
    usp_ConsultadDtPagto: TDateTimeField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    usp_ConsultanCdLojaTit: TIntegerField;
    usp_ConsultanValJuro: TBCDField;
    usp_ConsultanValAbatimento: TBCDField;
    usp_ConsultanValDesconto: TBCDField;
    usp_ConsultanValLiq: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    usp_ConsultanCdEmpresa: TIntegerField;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdSP: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cSenso: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValAbatimento: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nValLiq: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nCdMoeda: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProvisaoTit: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPagto: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    RadioGroup1: TRadioGroup;
    Edit1: TEdit;
    Edit2: TEdit;
    RadioGroup2: TRadioGroup;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    MaskEdit3: TMaskEdit;
    Label7: TLabel;
    MaskEdit7: TMaskEdit;
    DBEdit4: TDBEdit;
    qryGrupoEspecieTitulo: TADOQuery;
    qryGrupoEspecieTitulonCdGrupoEspTit: TIntegerField;
    qryGrupoEspecieTitulocNmGrupoEspTit: TStringField;
    dsGrupoEspecieTitulo: TDataSource;
    usp_ConsultacNmGrupoEconomico: TStringField;
    cxGrid1DBTableView1cNmGrupoEconomico: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsTituloAberto: TfrmConsTituloAberto;

implementation

uses fMenu, fLookup_Padrao, fTitulo;

{$R *.dfm}

procedure TfrmConsTituloAberto.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  qryMoeda.Close ;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit4.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit4.text) ;
  end
  else
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;
  
  if (frmMenu.nCdLojaAtiva = 0) then
      cxGrid1DBTableView1nCdLojaTit.Visible := false ;

end;


procedure TfrmConsTituloAberto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin
    qryEspTit.Close ;
    qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEspTit.Parameters.ParamByName('nPK').Value := Trim(MaskEdit5.Text) ;
    qryEspTit.Open;
  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TfrmConsTituloAberto.ToolButton1Click(Sender: TObject);
begin

  usp_Consulta.Close ;

  usp_Consulta.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdEspTit').Value        := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdGrupoEspTit').Value   := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdTerceiro').Value      := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  usp_Consulta.Parameters.ParamByName('@cNrTit').Value           := Trim(Edit1.Text) ;
  usp_Consulta.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text) ;
  usp_Consulta.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text) ;
  usp_Consulta.Parameters.ParamByName('@cNrNF').Value            := Trim(Edit2.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      usp_Consulta.Parameters.ParamByName('@nCdLoja').Value       := 0
  else usp_Consulta.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(MaskEdit4.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Consulta.Parameters.ParamByName('@cSenso').Value := 'D' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Consulta.Parameters.ParamByName('@cSenso').Value := 'C' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Consulta.Parameters.ParamByName('@cSenso').Value := 'A' ;

  if (RadioGroup2.ItemIndex = 0) then
      usp_Consulta.Parameters.ParamByName('@cProvisao').Value := 'T' ;

  if (RadioGroup2.ItemIndex = 1) then
      usp_Consulta.Parameters.ParamByName('@cProvisao').Value := 'S' ;

  if (RadioGroup2.ItemIndex = 2) then
      usp_Consulta.Parameters.ParamByName('@cProvisao').Value := 'N' ;

  usp_Consulta.Open  ;

end;


procedure TfrmConsTituloAberto.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK         : Integer ;
    TipoAcesso  : String;
begin
  inherited;

  case key of
    vk_F4 : begin
        TipoAcesso := #39 + 'L' + #39;
        if Trim(MaskEdit7.Text) <> '' then nPK := frmLookup_Padrao.ExecutaConsulta2(10, ' (EspTit.nCdGrupoEspTit = ' + MaskEdit7.Text + ') AND ' + 'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')' )
        else nPK := frmLookup_Padrao.ExecutaConsulta2(10, 'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')' );

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAberto.cxButton1Click(Sender: TObject);
var linha, coluna : integer;
    var planilha : variant;
    var valorcampo : string;
begin
    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);
    planilha.caption := 'Posi��o de t�tulos em aberto';
    planilha.visible := true;

    for linha := 0 to usp_consulta.RecordCount - 1 do
    begin
       for coluna := 1 to usp_consulta.FieldCount do
       begin
         valorcampo := usp_consulta.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       usp_consulta.Next;
    end;
    for coluna := 1 to usp_consulta.FieldCount do
    begin
       valorcampo := usp_consulta.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;
    end;
    planilha.columns.Autofit;


end;

procedure TfrmConsTituloAberto.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmTitulo ;
begin
  inherited;

  if (usp_Consulta.Active) then
  begin
      objForm := TfrmTitulo.Create(nil) ;

      objForm.PosicionaQuery(objForm.qryMaster,usp_ConsultanCdTitulo.AsString);

      objForm.btIncluir.Visible   := False ;
      objForm.btSalvar.Visible    := False ;
      objForm.btConsultar.Visible := False ;
      objForm.btExcluir.Visible   := False ;
      objForm.ToolButton1.Visible := False ;
      objForm.btCancelar.Visible  := False ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmConsTituloAberto.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  usp_Consulta.Close ;
  cxGrid1DBTableView1nCdSP.Visible          := (RadioGroup1.ItemIndex = 0) ;
  cxGrid1DBTableView1nCdProvisaoTit.Visible := (RadioGroup1.ItemIndex = 0) ;
  cxGrid1DBTableView1dDtPagto.Visible       := (RadioGroup1.ItemIndex = 0) ;
  RadioGroup2.Visible                       := (RadioGroup1.ItemIndex = 0) ;
  
end;

procedure TfrmConsTituloAberto.RadioGroup2Click(Sender: TObject);
begin
  inherited;
  usp_Consulta.Close ;

end;

procedure TfrmConsTituloAberto.ToolButton5Click(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1.Preview(True,nil);

end;

procedure TfrmConsTituloAberto.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TfrmConsTituloAberto.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit4.Enabled := False ;
end;

procedure TfrmConsTituloAberto.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        nPK := frmLookup_Padrao.ExecutaConsulta(9);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAberto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEspecieTitulo.Close;
  PosicionaQuery(qryGrupoEspecieTitulo,Maskedit7.Text);
end;

initialization
     RegisterClass(TfrmConsTituloAberto) ;

end.
