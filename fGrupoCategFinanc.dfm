inherited frmGrupoCategFinanc: TfrmGrupoCategFinanc
  Caption = 'Grupo Categoria Financeira'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 55
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Grupo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoCategFinanc'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoCategFinanc'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOCATEGFINANC'
      'WHERE nCdGrupoCategFinanc = :nPK')
    object qryMasternCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryMastercNmGrupoCategFinanc: TStringField
      FieldName = 'cNmGrupoCategFinanc'
      Size = 50
    end
  end
end
