unit fProduto_Duplicar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DB, DBCtrls, ADODB, Mask;

type
  TfrmProduto_Duplicar = class(TfrmProcesso_Padrao)
    edtReferencia: TEdit;
    edtDescricao: TEdit;
    edtGrade: TMaskEdit;
    qryGrade: TADOQuery;
    qryGradenCdGrade: TIntegerField;
    qryGradecNmGrade: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SP_DUPLICAR_PRODUTO: TADOStoredProc;
    procedure edtGradeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGradeExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto         : integer ;
    nCdProdutoNovo     : Integer ;
    cDescricaoAnterior : String ;
  end;

var
  frmProduto_Duplicar: TfrmProduto_Duplicar;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmProduto_Duplicar.edtGradeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(51);

        If (nPK > 0) then
        begin
            edtGrade.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrade, edtGrade.Text)
        end ;

    end ;

  end ;

end;

procedure TfrmProduto_Duplicar.edtGradeExit(Sender: TObject);
begin
  inherited;

  qryGrade.Close ;
  PosicionaQuery(qryGrade, edtGrade.Text)

end;

procedure TfrmProduto_Duplicar.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(edtDescricao.Text) = Trim(cDescricaoAnterior)) then
  begin
      MensagemAlerta('A descri��o do novo produto deve ser diferente do produto anterior.') ;
      edtDescricao.SetFocus;
      exit ;
  end ;

  if (Trim(edtDescricao.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do novo produto.') ;
      edtDescricao.SetFocus;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o do novo produto ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_DUPLICAR_PRODUTO.Close ;
      SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@nCdProduto').Value  := nCdProduto ;
      SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@cReferencia').Value := Trim(edtReferencia.Text) ;
      SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@cDescricao').Value  := Trim(edtDescricao.Text) ;
      SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@nCdGrade').Value    := frmMenu.ConvInteiro(edtGrade.Text) ;
      SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;
      SP_DUPLICAR_PRODUTO.ExecProc;
      nCdProdutoNovo := SP_DUPLICAR_PRODUTO.Parameters.ParamByName('@nCdProdutoNovo').Value  ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  close ;

end;

procedure TfrmProduto_Duplicar.FormShow(Sender: TObject);
begin
  inherited;

  nCdProdutoNovo     := 0 ;
  edtReferencia.Text := '' ;
  edtGrade.Text      := '' ;
  edtDescricao.Text  := cDescricaoAnterior ;

  qryGrade.Close ;

  edtReferencia.SetFocus;

end;

end.
