unit fCaixa_CPFNotaFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmCaixa_CPFNotaFiscal = class(TForm)
    frmCaixa_CPFNotaFiscal: TLabel;
    edtCPF: TEdit;
    procedure FormShow(Sender: TObject);
    procedure edtCPFKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_CPFNotaFiscal: TfrmCaixa_CPFNotaFiscal;

implementation

{$R *.dfm}

procedure TfrmCaixa_CPFNotaFiscal.FormShow(Sender: TObject);
begin

    edtCPF.Text := '' ;
    edtCPF.SetFocus;

end;

procedure TfrmCaixa_CPFNotaFiscal.edtCPFKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        Close;

end;

end.
