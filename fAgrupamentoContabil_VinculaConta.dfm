inherited frmAgrupamentoContabil_VinculaConta: TfrmAgrupamentoContabil_VinculaConta
  Left = 234
  Top = 289
  Width = 659
  Height = 184
  Caption = 'Vincular Conta Cont'#225'bil'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 148
    Width = 643
    Height = 0
  end
  inherited ToolBar1: TToolBar
    Width = 643
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 643
    Height = 119
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 40
      Top = 24
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'SubConta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 16
      Top = 48
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Cont'#225'bil'
    end
    object Label3: TLabel
      Tag = 1
      Left = 38
      Top = 72
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sequ'#234'ncia'
    end
    object Label4: TLabel
      Tag = 1
      Left = 47
      Top = 96
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'scara'
    end
    object edtMascaraSuperior: TEdit
      Tag = 1
      Left = 91
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edtPlanoConta: TER2LookupMaskEdit
      Left = 91
      Top = 40
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      CodigoLookup = 218
      QueryLookup = qryPlanoConta
    end
    object edtCodigo: TMaskEdit
      Left = 91
      Top = 64
      Width = 65
      Height = 21
      TabOrder = 2
      OnChange = edtCodigoChange
    end
    object edtMascara: TEdit
      Tag = 1
      Left = 91
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 216
      Top = 16
      Width = 417
      Height = 21
      DataField = 'cNmContaAgrupamentoContabil'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 160
      Top = 40
      Width = 473
      Height = 21
      DataField = 'cNmPlanoConta'
      DataSource = DataSource2
      TabOrder = 5
    end
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 104
  end
  object qrySubConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoContabil'
      'WHERE nCdContaAgrupamentoContabil = :nPK')
    Left = 392
    Top = 61
    object qrySubContanCdContaAgrupamentoContabil: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabil'
    end
    object qrySubContanCdContaAgrupamentoContabilPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabilPai'
    end
    object qrySubContanCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qrySubContacMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qrySubContacNmContaAgrupamentoContabil: TStringField
      FieldName = 'cNmContaAgrupamentoContabil'
      Size = 100
    end
    object qrySubContacNmContaSegundoIdioma: TStringField
      FieldName = 'cNmContaSegundoIdioma'
      Size = 100
    end
    object qrySubContaiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qrySubContaiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qrySubContanCdTabTipoNaturezaContaSPED: TIntegerField
      FieldName = 'nCdTabTipoNaturezaContaSPED'
    end
    object qrySubContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qrySubContacFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
    object qrySubContacFlgNaturezaCredDev: TStringField
      FieldName = 'cFlgNaturezaCredDev'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource1: TDataSource
    DataSet = qrySubConta
    Left = 312
    Top = 80
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdPlanoConta, cNmPlanoConta, cNmPlanoContaSegundoIdioma,' +
        ' cFlgNaturezaCredDev'
      'FROM PlanoConta'
      'WHERE nCdPlanoConta = :nPK')
    Left = 480
    Top = 93
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 100
    end
    object qryPlanoContacNmPlanoContaSegundoIdioma: TStringField
      FieldName = 'cNmPlanoContaSegundoIdioma'
      Size = 100
    end
    object qryPlanoContacFlgNaturezaCredDev: TStringField
      FieldName = 'cFlgNaturezaCredDev'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = qryPlanoConta
    Left = 320
    Top = 88
  end
  object qryProximaSequencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT IsNull(MAX(iSequencia),0) + 1 as iProximaSequencia'
      '  FROM ContaAgrupamentoContabil'
      ' WHERE nCdContaAgrupamentoContabilPai = :nPK')
    Left = 384
    Top = 109
    object qryProximaSequenciaiProximaSequencia: TIntegerField
      FieldName = 'iProximaSequencia'
      ReadOnly = True
    end
  end
  object qryAgrupamentoContabil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM AgrupamentoContabil'
      'WHERE nCdAgrupamentoContabil = :nPK')
    Left = 528
    Top = 61
    object qryAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryAgrupamentoContabilnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryAgrupamentoContabilcNmAgrupamentoContabil: TStringField
      FieldName = 'cNmAgrupamentoContabil'
      Size = 50
    end
    object qryAgrupamentoContabilcMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField
      FieldName = 'nCdTabTipoAgrupamentoContabil'
    end
    object qryAgrupamentoContabilnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAgrupamentoContabilcFlgAnaliseVertical: TIntegerField
      FieldName = 'cFlgAnaliseVertical'
    end
    object qryAgrupamentoContabiliMaxNiveis: TIntegerField
      FieldName = 'iMaxNiveis'
    end
  end
  object qryContaAgrupamentoContabil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoContabil'
      'WHERE nCdContaAgrupamentoContabil = :nPK')
    Left = 536
    Top = 101
    object qryContaAgrupamentoContabilnCdContaAgrupamentoContabil: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabil'
    end
    object qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabilPai'
    end
    object qryContaAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryContaAgrupamentoContabilcMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaAgrupamentoContabilcNmContaAgrupamentoContabil: TStringField
      FieldName = 'cNmContaAgrupamentoContabil'
      Size = 100
    end
    object qryContaAgrupamentoContabilcNmContaSegundoIdioma: TStringField
      FieldName = 'cNmContaSegundoIdioma'
      Size = 100
    end
    object qryContaAgrupamentoContabiliNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaAgrupamentoContabiliSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED: TIntegerField
      FieldName = 'nCdTabTipoNaturezaContaSPED'
    end
    object qryContaAgrupamentoContabilnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryContaAgrupamentoContabilcFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
    object qryContaAgrupamentoContabilcFlgNaturezaCredDev: TStringField
      FieldName = 'cFlgNaturezaCredDev'
      FixedChar = True
      Size = 1
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 584
    Top = 85
  end
end
