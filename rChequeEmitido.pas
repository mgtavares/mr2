unit rChequeEmitido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptChequeEmitido = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    edtDtEmissaoIni: TMaskEdit;
    Label6: TLabel;
    edtDtEmissaoFim: TMaskEdit;
    edtEmpresa: TMaskEdit;
    edtContaBancaria: TMaskEdit;
    Label2: TLabel;
    edtDtVenctoIni: TMaskEdit;
    Label4: TLabel;
    edtDtVenctoFim: TMaskEdit;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdEmpresa: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DBEdit1: TDBEdit;
    DataSource6: TDataSource;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptChequeEmitido: TrptChequeEmitido;

implementation

uses fMenu, fLookup_Padrao, rChequeEmitido_view;

{$R *.dfm}

procedure TrptChequeEmitido.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  edtEmpresa.SetFocus ;

end;


procedure TrptChequeEmitido.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;

  if (Trim(edtEmpresa.Text) <> '') then
  begin

      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := edtEmpresa.Text ;
      qryEmpresa.Open ;
    
  end ;

end;

procedure TrptChequeEmitido.edtContaBancariaExit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close;

  If (Trim(edtContaBancaria.Text) <> '') then
  begin

    if (DBEdit3.Text = '') then
    begin
        MensagemAlerta('Selecione uma empresa.') ;
        edtContaBancaria.Text := '' ;
        edtEmpresa.SetFocus;
        abort ;
    end ;

    qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text) ;

    PosicionaQuery(qryContaBancaria, edtContaBancaria.Text) ;

  end ;

end;

procedure TrptChequeEmitido.ToolButton1Click(Sender: TObject);
var
    cFiltro : String ;
    objRel  : TrptChequeEmitido_view ;
begin

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Selecione uma empresa.') ;
      edtEmpresa.SetFocus;
      abort ;
  end ;

  objRel := TrptChequeEmitido_view.Create( Self ) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(edtEmpresa.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdContaBancaria').Value := frmMenu.ConvInteiro(edtContaBancaria.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtEmissaoIni').Value    := frmMenu.ConvData(edtDtEmissaoIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtEmissaoFim').Value    := frmMenu.ConvData(edtDtEmissaoFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtVenctoIni').Value     := frmMenu.ConvData(edtDtVenctoIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cDtVenctoFim').Value     := frmMenu.ConvData(edtDtVenctoFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSituacao').Value     := RadioGroup1.ItemIndex;
  objRel.usp_Relatorio.Parameters.ParamByName('@cFlgOrdenacao').Value    := RadioGroup2.ItemIndex;
  objRel.usp_Relatorio.Open  ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  cFiltro := 'Filtros: Empresa: ' + Trim(edtEmpresa.Text) + ' - ' + Trim(DBEdit3.Text) ;

  if (DBEdit1.Text <> '') then
      cFiltro := cFiltro + '/ Conta Banc�ria: ' + DBEdit1.Text + ' - ' + DBEdit4.Text + ' - ' + Trim(DBEdit5.Text) + ' - ' + DBEdit6.Text ;

  cFiltro := cFiltro + '/ Per�odo Emiss�o: '    + Trim(edtDtEmissaoIni.Text)  + ' - ' + Trim(edtDtEmissaoFim.Text) ;
  cFiltro := cFiltro + '/ Per�odo Vencimento: ' + Trim(edtDtVenctoIni.Text)   + ' - ' + Trim(edtDtVenctoFim.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      cFiltro := cFiltro + '/ Situa��o: CHEQUE ABERTO' ;

  if (RadioGroup1.ItemIndex = 1) then
      cFiltro := cFiltro + '/ Situa��o: CHEQUE COMPENSADO' ;

  if (RadioGroup1.ItemIndex = 2) then
      cFiltro := cFiltro + '/ Situa��o: TODOS' ;

  if (RadioGroup2.ItemIndex = 0) then
      cFiltro := cFiltro + '/ Ordena��o: N�mero Cheque' ;

  if (RadioGroup2.ItemIndex = 1) then
      cFiltro := cFiltro + '/ Ordena��o: Data Emiss�o' ;

  if (RadioGroup2.ItemIndex = 2) then
      cFiltro := cFiltro + '/ Ordena��o: Data Vencimento' ;

  objRel.lblFiltro.Caption := cFiltro ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptChequeEmitido.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            edtEmpresa.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptChequeEmitido.edtContaBancariaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma empresa.') ;
            edtContaBancaria.Text := '' ;
            edtEmpresa.SetFocus;
            abort ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + edtEmpresa.Text + ' AND ContaBancaria.cFlgEmiteCheque = 1');

        If (nPK > 0) then
        begin
            edtContaBancaria.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptChequeEmitido) ;

end.
