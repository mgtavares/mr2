inherited frmListaCobranca: TfrmListaCobranca
  Left = -8
  Top = -8
  Width = 1382
  Height = 784
  BorderIcons = [biSystemMenu]
  Caption = 'Lista de Cobran'#231'a'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 52
    Width = 1366
    Height = 694
  end
  object lblAnalista: TLabel [1]
    Left = 0
    Top = 29
    Width = 1366
    Height = 23
    Align = alTop
    Caption = 'Analista'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -20
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
  end
  inherited ToolBar1: TToolBar
    Width = 1366
    ButtonWidth = 97
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 97
    end
    inherited ToolButton2: TToolButton
      Left = 105
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = -8
    Top = 93
    Width = 1017
    Height = 435
    DataGrouping.GroupLevels = <>
    DataSource = dsLista
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdListaCobranca'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtCobranca'
        Footers = <>
        Width = 109
      end
      item
        EditButtons = <>
        FieldName = 'cNmEtapacobranca'
        Footers = <>
        Width = 298
      end
      item
        EditButtons = <>
        FieldName = 'iSeq'
        Footers = <>
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'dDtVencto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteTotal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteRestante'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtAtribuicao'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtAbertura'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryLista: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdListaCobranca'
      '      ,dDtCobranca'
      '      ,dDtVencto'
      '      ,iSeq'
      '      ,cNmListaCobranca  cNmEtapaCobranca'
      '      ,iQtdeClienteTotal'
      
        '      ,(iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContat' +
        'o) iQtdeClienteRestante'
      '      ,dDtAtribuicao'
      '      ,dDtAbertura'
      '      ,ListaCobranca.cFlgTelefone'
      '      ,ListaCobranca.cFlgNegativar'
      '      ,ListaCobranca.dDtEncerramento'
      '  FROM ListaCobranca'
      ' WHERE ListaCobranca.nCdUsuarioResp  = :nPK'
      '   AND ListaCobranca.nCdEmpresa      = :nCdEmpresa'
      '   AND dDtEncerramento              IS NULL'
      '   AND dDtCancel                    IS NULL'
      '   AND ((@nCdLoja = 0) OR (ListaCobranca.nCdLoja = @nCdLoja))'
      ' ORDER BY dDtAbertura, dDtAtribuicao')
    Left = 304
    Top = 168
    object qryListanCdListaCobranca: TIntegerField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|C'#243'd'
      FieldName = 'nCdListaCobranca'
    end
    object qryListadDtCobranca: TDateTimeField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Data Cobran'#231'a'
      FieldName = 'dDtCobranca'
    end
    object qryListadDtVencto: TDateTimeField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Data Vencto'
      FieldName = 'dDtVencto'
    end
    object qryListaiSeq: TIntegerField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Seq.'
      FieldName = 'iSeq'
    end
    object qryListacNmEtapacobranca: TStringField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Nome Lista'
      FieldName = 'cNmEtapacobranca'
      Size = 50
    end
    object qryListaiQtdeClienteTotal: TIntegerField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Qtde Clientes'
      FieldName = 'iQtdeClienteTotal'
    end
    object qryListaiQtdeClienteRestante: TIntegerField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Qtde Restantes'
      FieldName = 'iQtdeClienteRestante'
      ReadOnly = True
    end
    object qryListadDtAtribuicao: TDateTimeField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Data/Hora Atribui'#231#227'o'
      FieldName = 'dDtAtribuicao'
    end
    object qryListadDtAbertura: TDateTimeField
      DisplayLabel = 'Suas Listas de Cobran'#231'a|Data Abertura'
      FieldName = 'dDtAbertura'
    end
    object qryListacFlgTelefone: TIntegerField
      FieldName = 'cFlgTelefone'
    end
    object qryListadDtEncerramento: TDateTimeField
      FieldName = 'dDtEncerramento'
    end
    object qryListacFlgNegativar: TIntegerField
      FieldName = 'cFlgNegativar'
    end
  end
  object dsLista: TDataSource
    DataSet = qryLista
    Left = 352
    Top = 168
  end
end
