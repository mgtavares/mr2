unit rRelPedidoVenda_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRelPedidoVenda_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText8: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    usp_RelatorionCdPedido: TIntegerField;
    usp_RelatoriodDtPedido: TDateTimeField;
    usp_RelatoriodDtAutor: TDateTimeField;
    usp_RelatorionValPedido: TFloatField;
    usp_RelatorionSaldoPedido: TFloatField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionCdItemPedido: TIntegerField;
    usp_RelatorionCdProduto: TIntegerField;
    usp_RelatoriocNmItem: TStringField;
    usp_RelatoriocSiglaUnidadeMedida: TStringField;
    usp_RelatorionQtdePed: TFloatField;
    usp_RelatorionQtdePrev: TFloatField;
    usp_RelatorionQtdeSaldo: TFloatField;
    usp_RelatorionValUnitario: TFloatField;
    usp_RelatorionValTotalItem: TFloatField;
    usp_RelatorionValorSaldo: TFloatField;
    usp_RelatoriodDtEntregaIni: TDateTimeField;
    usp_RelatoriodDtEntregaFim: TDateTimeField;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape6: TQRShape;
    QRLabel29: TQRLabel;
    QRLabel4: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    usp_RelatoriocFlgAtraso: TIntegerField;
    QRLabel12: TQRLabel;
    QRExpr3: TQRExpr;
    usp_RelatorionCdTipoPedido: TIntegerField;
    usp_RelatoriocNmTipoPedido: TStringField;
    QRLabel13: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel30: TQRLabel;
    QRDBText21: TQRDBText;
    usp_RelatorionCdLoja: TIntegerField;
    usp_RelatoriocNmLoja: TStringField;
    QRDBText22: TQRDBText;
    QRLabel31: TQRLabel;
    QRDBText23: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText7Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelPedidoVenda_view: TrptRelPedidoVenda_view;

implementation

{$R *.dfm}

procedure TrptRelPedidoVenda_view.QRDBText6Print(sender: TObject;
  var Value: String);
begin

    QRDBText6.Color := clWhite ;

    if (usp_RelatoriocFlgAtraso.Value = 1) then
        QRDBText6.Color := clSilver ;

end;

procedure TrptRelPedidoVenda_view.QRDBText7Print(sender: TObject;
  var Value: String);
begin

    QRDBText7.Color := clWhite ;

    if (usp_RelatoriocFlgAtraso.Value = 1) then
        QRDBText7.Color := clSilver ;

end;

end.
