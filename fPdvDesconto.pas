unit fPdvDesconto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, DB, ADODB, cxLookAndFeelPainters, cxButtons;

type
  TfrmPdvDesconto = class(TForm)
    edtValDesconto: TcxCurrencyEdit;
    Label3: TLabel;
    Label2: TLabel;
    edtPercent: TcxCurrencyEdit;
    edtValVenda: TcxCurrencyEdit;
    Label1: TLabel;
    edtValliquido: TcxCurrencyEdit;
    Label4: TLabel;
    edtValAcrescimo: TcxCurrencyEdit;
    Label5: TLabel;
    qryMetaDesconto: TADOQuery;
    qryMetaDescontonValDescontoUtil: TBCDField;
    qryMetaDescontonValCotaDesconto: TBCDField;
    btSair: TcxButton;
    procedure edtPercentExit(Sender: TObject);
    procedure edtValDescontoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtPercentKeyPress(Sender: TObject; var Key: Char);
    procedure edtValDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValliquidoKeyPress(Sender: TObject; var Key: Char);
    procedure ValidaDesconto;
    procedure edtValAcrescimoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValAcrescimoExit(Sender: TObject);
    function ArredondaMatematico(Value: extended; Decimals: integer): extended;
    procedure btSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nPercMaxDesc          : double ;
    cFlgArredondaPrecoPDV : integer;
    nCdUsuarioVendedor    : integer;
  end;

var
  frmPdvDesconto: TfrmPdvDesconto;

implementation

uses fPdv, fMenu, Math;

{$R *.dfm}

procedure TfrmPdvDesconto.edtPercentExit(Sender: TObject);
begin

//  edtValDesconto.Value := frmMenu.TBRound((edtvalvenda.value * (edtPercent.Value/100)),2) ;
  edtValDesconto.Value := (edtvalvenda.value * (edtPercent.Value/100)) ;
  edtValLiquido.Value := edtValVenda.Value - edtValDesconto.Value + edtValAcrescimo.Value ;

end;

procedure TfrmPdvDesconto.edtValDescontoExit(Sender: TObject);
begin

  edtPercent.Value    := ((edtValDesconto.Value / edtvalvenda.value)*100);
  edtValLiquido.Value := edtValVenda.Value - edtValDesconto.Value + edtValAcrescimo.Value

end;

procedure TfrmPdvDesconto.FormShow(Sender: TObject);
begin
    edtValDesconto.Value   := 0 ;
    edtValAcrescimo.Value  := 0 ;
    edtvalLiquido.Value    := edtValVenda.Value;
    edtPercent.Enabled     := True ;
    edtValDesconto.Enabled := True ;

    if (edtValVenda.Value <= 0) then
    begin

        edtPercent.Enabled     := False ;
        edtValDesconto.Enabled := False ;

        edtValAcrescimo.SelectAll;
        edtValAcrescimo.SetFocus;

    end
    else
    begin
        edtPercent.SelectAll;
        edtPercent.SetFocus;
    end ;

end;

procedure TfrmPdvDesconto.edtPercentKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtValDesconto.SetFocus ;

end;

procedure TfrmPdvDesconto.edtValDescontoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
    begin
        if (edtValAcrescimo.Enabled = True) then edtValAcrescimo.SetFocus
        else edtValliquido.SetFocus;
    end;
end;

procedure TfrmPdvDesconto.edtValliquidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        ValidaDesconto;
end;

procedure TfrmPdvDesconto.ValidaDesconto;
begin
  if (frmMenu.LeParametro('VAREJO') = 'S') then
      if (cFlgArredondaPrecoPDV = 1) then
          edtValLiquido.Value := frmMenu.arredondaVarejo(edtValLiquido.Value)
      else
          edtValliquido.Value := edtValliquido.Value
  else edtValliquido.Value  := ArredondaMatematico(edtValliquido.Value,2);

  edtValDesconto.Value  := 0 ;
  edtValAcrescimo.Value := 0 ;

  if (edtValVenda.Value < 0) and (edtValLiquido.Value < edtValVenda.Value) then
  begin
      frmMenu.MensagemErro('N�o � permitido desconto em opera��o de troca.') ;
      edtValLiquido.Value := edtValVenda.Value ;
      edtValAcrescimo.SelectAll;
      edtValAcrescimo.SetFocus;
      abort ;
  end ;

  if (edtValLiquido.Value < edtValVenda.Value) then
      edtValDesconto.Value := edtValVenda.Value - edtValLiquido.Value ;

  if (edtValLiquido.Value > edtValVenda.Value) then
      edtValAcrescimo.Value := edtValLiquido.Value - edtValVenda.Value ;

  if (edtValDesconto.Value > 0) and (edtValVenda.Value <> 0) then
      edtPercent.Value     := (edtValDesconto.Value / edtValVenda.value) * 100 ;

  { -- se usa meta de desconto, faz consist�ncias -- }
  if (frmMenu.LeParametro('USARMETADESC') = 'S') then
  begin
      qryMetaDesconto.Close;
      qryMetaDesconto.Parameters.ParamByName('nPK').Value     := IntToStr(nCdUsuarioVendedor);
      qryMetaDesconto.Parameters.ParamByName('nCdLoja').Value := IntToStr(frmMenu.nCdLojaAtiva);
      qryMetaDesconto.Open;

      if ((qryMetaDescontonValDescontoUtil.Value + edtValDesconto.Value) > qryMetaDescontonValCotaDesconto.Value) then
      begin
          if ((qryMetaDescontonValCotaDesconto.Value - qryMetaDescontonValDescontoUtil.Value) = 0) then
              frmMenu.MensagemAlerta('N�o h� saldo dispon�vel para concess�o de desconto para o vendedor do pedido.')
          else
              frmMenu.MensagemAlerta('Desconto informado � superior ao saldo dispon�vel para o m�s vigente.' + #13#13 + 'Saldo dispon�vel: R$ ' + FormatFloat('#,##0.00', qryMetaDescontonValCotaDesconto.Value - qryMetaDescontonValDescontoUtil.Value) + '.');

          edtPercent.SetFocus;
          Abort;
      end;
  end;

  if (frmPDV.MessageDlg('Confirma o valor do desconto/acr�scimo ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
  begin

      if (edtPercent.Value > nPercMaxDesc) then
      begin
          frmPDV.MensagemErro('Desconto n�o autorizado.') ;
          edtPercent.Value     := 0 ;
          edtValDesconto.Value := 0 ;
          edtValLiquido.Value  := edtValVenda.Value ;
          edtPercent.SetFocus ;
          abort ;

      end ;

      close ;

  end
  else
  begin
      if (edtPercent.Enabled) then
          edtPercent.SetFocus
      else edtValLiquido.SetFocus;

  end ;

end;

procedure TfrmPdvDesconto.edtValAcrescimoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtValLiquido.SetFocus;

end;

procedure TfrmPdvDesconto.edtValAcrescimoExit(Sender: TObject);
begin

  edtValLiquido.Value := edtValVenda.Value - edtValDesconto.Value + edtValAcrescimo.Value ;

end;

function TfrmPdvDesconto.ArredondaMatematico(Value: extended;
  Decimals: integer): extended;
var
    Factor   : extended;
    Fraction : extended;
    i        : integer;
begin
    Factor   := IntPower(10,Decimals);
    Value    := StrToFloat(FloatToStr(Value * Factor));
    Result   := Int(Value);
    Fraction := Frac(Value);

    for i := 3 downto 1 do
    Begin
        Fraction := Fraction * intPower(10,i);

        if (frac(Fraction) > 0.5) then
            Fraction :=Fraction + 1 - Frac(Fraction)
        else if (Frac(Fraction) < 0.5) then
            Fraction := Fraction - Frac(Fraction);

        Fraction := Fraction / intPower(10,i);
    end;

    if ((((Result/2) - Int(Result/2) > 0) OR ( Fraction > 0.5))) then
    BEGIN
        IF (Fraction >= 0.5) then
            Result := Result +1
        Else
            if (Fraction <= -0.5) then
                Result :=Result - 1;
    END;

    Result := Result/Factor;

end;

procedure TfrmPdvDesconto.btSairClick(Sender: TObject);
begin
  edtValDesconto.Value  := 0;
  edtValAcrescimo.Value := 0;
  Close;
end;

procedure TfrmPdvDesconto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F12) then
      btSair.Click;
end;

end.
