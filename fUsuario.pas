unit fUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, ImgList, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmUsuario = class(TfrmCadastro_Padrao)
    qryMasternCdUsuario: TIntegerField;
    qryMastercNmLogin: TStringField;
    qryMastercSenha: TStringField;
    qryMastercFlgAtivo: TSmallintField;
    qryMasterdDtUltAcesso: TDateTimeField;
    qryMasterdDtUltAltSenha: TDateTimeField;
    qryMasternCdEmpPadrao: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmUsuario: TStringField;
    qryMastercCPF: TStringField;
    qryMasternCdTerceiroResponsavel: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    qryMastercNmStatus: TStringField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMastercFlagMaster: TSmallintField;
    qryMastercAcessoWERP: TSmallintField;
    DBCheckBox3: TDBCheckBox;
    qryMastercTrocaSenhaProxLogin: TSmallintField;
    DBCheckBox4: TDBCheckBox;
    qryUsuarioEmpresa: TADOQuery;
    qryUsuarioEmpresanCdUsuario: TIntegerField;
    qryUsuarioEmpresanCdEmpresa: TIntegerField;
    dsUsuarioEmpresa: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEmpresanCdTerceiroMatriz: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresanCdStatus: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryUsuarioEmpresacNmEmpresa: TStringField;
    qryUsuarioEmpresacSiglaEmpresa: TStringField;
    qryGrupoUsuario: TADOQuery;
    dsGrupoUsuario: TDataSource;
    qryGrupoUsuarionCdGrupoUsuario: TIntegerField;
    qryGrupoUsuariocNmGrupoUsuario: TStringField;
    qryGrupoUsuarionCdStatus: TIntegerField;
    qryMastercFlgVendedor: TIntegerField;
    qryMastercFlgCaixa: TIntegerField;
    qryMastercFlgGerente: TIntegerField;
    qryMasternPercMaxDesc: TBCDField;
    qryMasternValAlcadaCompra: TBCDField;
    qryMastercFlgComprador: TIntegerField;
    Label10: TLabel;
    DBEdit11: TDBEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    qryMasternCdLojaPadrao: TIntegerField;
    Label3: TLabel;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    qryEmpresaPadrao: TADOQuery;
    qryEmpresaPadraonCdEmpresa: TIntegerField;
    qryEmpresaPadraocNmEmpresa: TStringField;
    DBEdit14: TDBEdit;
    DataSource2: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit15: TDBEdit;
    DataSource3: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    Image2: TImage;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    cxTabSheet4: TcxTabSheet;
    Image3: TImage;
    DBCheckBox8: TDBCheckBox;
    DBEdit10: TDBEdit;
    Label8: TLabel;
    qryMastercFlgAnalistaCobranca: TIntegerField;
    cxTabSheet5: TcxTabSheet;
    Image4: TImage;
    DBCheckBox2: TDBCheckBox;
    cxTabSheet6: TcxTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    qryDireitosDisponiveis: TADOQuery;
    qryDireitosConcedidos: TADOQuery;
    qryDireitosDisponiveisnCdTipoDireitoGerente: TIntegerField;
    qryDireitosDisponiveiscNmTipoDireitoGerente: TStringField;
    qryDireitosConcedidosnCdTipoDireitoGerente: TIntegerField;
    qryDireitosConcedidoscNmTipoDireitoGerente: TStringField;
    DBGridEh3: TDBGridEh;
    dsDireitosConcedidos: TDataSource;
    dsDireitosDisponiveis: TDataSource;
    DBGridEh4: TDBGridEh;
    qryConcedeDireito: TADOQuery;
    qryRevogaDireito: TADOQuery;
    qryUsuarioEmpresanCdUsuarioEmpresa: TAutoIncField;
    cxTabSheet7: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdUsuarioLoja: TIntegerField;
    qryUsuarioLojanCdUsuario: TIntegerField;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryUsuarioLojanCdServidorOrigem: TIntegerField;
    qryUsuarioLojadDtReplicacao: TDateTimeField;
    dsUsuarioLoja: TDataSource;
    qryUsuarioLojacNmLoja: TStringField;
    qryLoja2: TADOQuery;
    qryLoja2nCdLoja: TIntegerField;
    qryLoja2cNmLoja: TStringField;
    DBCheckBox18: TDBCheckBox;
    qryMastercFlgRepresentante: TIntegerField;
    qryTerceiroTipoTerceiro: TADOQuery;
    qryTerceiroTipoTerceironCdTipoTerceiro: TIntegerField;
    qryVerificaLogin: TADOQuery;
    qryMastercEmail: TStringField;
    DBEdit16: TDBEdit;
    Label12: TLabel;
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure qryUsuarioEmpresaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxTabSheet6Show(Sender: TObject);
    procedure DBGridEh3DblClick(Sender: TObject);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure qryUsuarioLojaCalcFields(DataSet: TDataSet);
    procedure qryUsuarioLojaBeforePost(DataSet: TDataSet);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUsuario: TfrmUsuario;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmUsuario.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmUsuario.qryMasterBeforePost(DataSet: TDataSet);
var
    cDado : String ;
begin

  if (qryMastercNmLogin.Value = '') then
  begin
      MensagemAlerta('Informe o login do usu�rio.');
      DBEdit2.SetFocus;
      Abort;
  end;

  { -- verifica se login j� est� em uso -- }
  qryVerificaLogin.Close;
  qryVerificaLogin.Parameters.ParamByName('Login').Value := DBEdit2.Text;
  qryVerificaLogin.Parameters.ParamByName('nPK').Value   := qryMasternCdUsuario.Value;
  qryVerificaLogin.Open;

  if (not qryVerificaLogin.IsEmpty) then
  begin
      MensagemAlerta('O login ' + Trim(DBEdit2.Text) + ' j� est� em uso. Informe outro login.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (Trim(qryMastercEmail.Value) <> '') and (frmMenu.fnValidarEMail(qryMastercEmail.Value) = False) then
  begin
      DBEdit16.SetFocus;
      Abort;
  end;

  if ((qryMasternCdStatus.Value = null) or (qryMasternCdStatus.Value = 0)) then
  begin
      qryMasternCdStatus.Value := 1 ;
  end ;

  if (qryMastercCPF.value <> '') then
  begin
      cDado := frmMenu.TestaCpfCgc(qryMastercCPF.Value) ;

      if (cDado = qryMastercCPF.Value) then
          abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMastercSenha.Value               := frmMenu.Cripto('1111') ;
      qryMastercTrocaSenhaProxLogin.Value := 1 ;
      qryMasterdDtUltAltSenha.Value       := Now() ;
  end ;

  if (qryMastercAcessoWERP.Value = 1) and (Dbedit12.Text = '') then
  begin
      MensagemAlerta('Usu�rio com acesso via WERP precisa ter o terceiro respons�vel informado.') ;
      DBEdit11.setFocus ;
      abort ;
  end ;

  if (qryMastercAcessoWERP.Value = 1) and (Dbedit12.Text = '') then
  begin
      MensagemAlerta('Usu�rio com acesso via WERP precisa ter o terceiro respons�vel informado.') ;
      DBEdit11.setFocus ;
      abort ;
  end ;

  if (((qryMastercFlgVendedor.Value = 1) or (qryMastercFlgCaixa.Value = 1) or (qryMastercFlgGerente.Value = 1)) and (Dbedit12.Text = '')) then
  begin
      MensagemAlerta('Usu�rio (Vendedor / Caixa / Gerente) precisa ter o terceiro respons�vel informado.') ;
      DBEdit11.setFocus ;
      abort ;
  end ;

  if ((qryMastercFlgRepresentante.Value = 1)  and (Dbedit12.Text = '')) then
  begin
      MensagemAlerta('Usu�rio (Representante Comercial / Vendedor) precisa ter o terceiro respons�vel informado.') ;
      DBEdit11.SetFocus ;
      abort ;
  end ;

  if ((qryMastercFlgRepresentante.Value = 1)  and (Dbedit12.Text <> '')) then
  begin
      posicionaQuery(qryTerceiroTipoTerceiro,Dbedit11.Text);
      if qryTerceiroTipoTerceironCdTipoTerceiro.Value <> 4 then
      begin
          MensagemAlerta('Terceiro informado n�o � (Representante Comercial / Vendedor).') ;
          DBEdit11.SetFocus ;
          abort ;
      end;
  end ;

  inherited;


end;

procedure TfrmUsuario.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryDireitosDisponiveis.Close;
  qryDireitosConcedidos.Close;

  DBEdit2.SetFocus;
end;

procedure TfrmUsuario.ToolButton3Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMasternCdUsuario.Value > 0) then
  begin

    case MessageDlg('Confirma o rein�cio da senha deste usu�rio ?',mtConfirmation,[mbYes,mbNo],0) of

      mrNo: begin
          Exit;
      end;

      mrYes: begin
          qryMaster.Edit ;
          qryMastercSenha.Value               := frmMenu.Cripto('1111') ;
          qryMastercTrocaSenhaProxLogin.Value := 1 ;

          ShowMessage('A senha foi alterada para 1111.' +#13+ 'Clique em Salvar para efetivar a altera��o.') ;
      end;

    end ;
    
  end ;
end;

procedure TfrmUsuario.qryUsuarioEmpresaBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryUsuarioEmpresanCdUsuario.Value := qryMaster.FieldList[0].Value ;

  if (qryUsuarioEmpresa.State = dsInsert) then
      qryUsuarioEmpresanCdUsuarioEmpresa.Value := frmMenu.fnProximoCodigo('USUARIOEMPRESA') ;

  inherited;


end;

procedure TfrmUsuario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioEmpresa.Close ;
  qryUsuarioEmpresa.Parameters.ParamByName('nCdUsuario').Value := qryMasternCdUsuario.Value ;
  qryUsuarioEmpresa.Open ;

  qryUsuarioLoja.Close ;
  qryUsuarioLoja.Parameters.ParamByName('nPK').Value := qryMasternCdUsuario.Value ;
  qryUsuarioLoja.Open ;

  qryGrupoUsuario.Close ;
  qryGrupoUsuario.Parameters.ParamByName('nCdUsuario').Value := qryMasternCdUsuario.Value ;
  qryGrupoUsuario.Open ;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiroResponsavel.AsString) ;

  qryEmpresaPadrao.Close ;
  qryLoja.Close ;

  PosicionaQuery(qryEmpresaPadrao, qryMasternCdEmpPadrao.AsString) ;
  PosicionaQuery(qryLoja, qryMasternCdLojaPadrao.AsString) ;


end;

procedure TfrmUsuario.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryGrupoUsuario.Close ;
  qryUsuarioEmpresa.Close ;
  qryTerceiro.Close ;
  qryEmpresaPadrao.Close ;
  qryLoja.Close ;
  qryUsuarioLoja.Close;

  qryDireitosDisponiveis.Close;
  qryDireitosConcedidos.Close;

end;

procedure TfrmUsuario.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (qryUsuarioEmpresa.State = dsBrowse) then
            qryUsuarioEmpresa.Edit ;


            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryUsuarioEmpresanCdEmpresa.Value := nPK ;
            end ;

    end ;

  end ;

end;

procedure TfrmUsuario.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Open;

  DBEdit13.Enabled  := True ;

  cxTabSheet3.Enabled := (frmMenu.LeParametro('VAREJO') = 'S') ;
  cxTabSheet6.Enabled := (frmMenu.LeParametro('VAREJO') = 'S') ;
  cxTabSheet7.Enabled := (frmMenu.LeParametro('VAREJO') = 'S') ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmUsuario.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit11.Text) ;
  
end;

procedure TfrmUsuario.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;
        
        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroResponsavel.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
  

end;

procedure TfrmUsuario.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryEmpresaPadrao.Close ;
  PosicionaQuery(qryEmpresaPadrao, DBEdit7.Text) ;
  
end;

procedure TfrmUsuario.DBEdit13Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit13.Text) ;
  
end;

procedure TfrmUsuario.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpPadrao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmUsuario.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (DBEdit14.Text = '') then
            begin
                MensagemAlerta('Selecione a empresa.') ;
                DBEdit7.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(147,'Loja.nCdEmpresa = ' + DBEdit7.Text);

            If (nPK > 0) then
            begin
                qryMasternCdLojaPadrao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmUsuario.btSalvarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmUsuario.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmUsuario.ToolButton1Click(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmUsuario.cxTabSheet6Show(Sender: TObject);
begin
  inherited;

  qryDireitosDisponiveis.Close;
  qryDireitosConcedidos.Close;

  if (qryMaster.Active) and (qryMasternCdUsuario.Value > 0) then
  begin

      PosicionaQuery(qryDireitosDisponiveis,qryMasternCdUsuario.AsString) ;
      PosicionaQuery(qryDireitosConcedidos,qryMasternCdUsuario.AsString) ;

  end ;

end;

procedure TfrmUsuario.DBGridEh3DblClick(Sender: TObject);
begin
  inherited;

  if (qryDireitosDisponiveis.RecordCount > 0) then
  begin

      qryConcedeDireito.Parameters.ParamByName('nCdUsuario').Value            := qryMasternCdUsuario.Value;
      qryConcedeDireito.Parameters.ParamByName('nCdTipoDireitoGerente').Value := qryDireitosDisponiveisnCdTipoDireitoGerente.Value;
      qryConcedeDireito.ExecSQL;

      PosicionaQuery(qryDireitosDisponiveis,qryMasternCdUsuario.AsString) ;
      PosicionaQuery(qryDireitosConcedidos,qryMasternCdUsuario.AsString) ;

  end ;

end;

procedure TfrmUsuario.DBGridEh4DblClick(Sender: TObject);
begin
  inherited;

  if (qryDireitosConcedidos.RecordCount > 0) then
  begin

      qryRevogaDireito.Parameters.ParamByName('nCdUsuario').Value            := qryMasternCdUsuario.Value;
      qryRevogaDireito.Parameters.ParamByName('nCdTipoDireitoGerente').Value := qryDireitosConcedidosnCdTipoDireitoGerente.Value;
      qryRevogaDireito.ExecSQL;

      PosicionaQuery(qryDireitosDisponiveis,qryMasternCdUsuario.AsString) ;
      PosicionaQuery(qryDireitosConcedidos,qryMasternCdUsuario.AsString) ;

  end ;

end;

procedure TfrmUsuario.qryUsuarioLojaCalcFields(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryLoja2.Close;
  PosicionaQuery(qryLoja2, qryUsuarioLojanCdLoja.AsString) ;

  qryUsuarioLojacNmLoja.Value := qryLoja2cNmLoja.Value;

end;

procedure TfrmUsuario.qryUsuarioLojaBeforePost(DataSet: TDataSet);
begin

  if (qryUsuarioLojacNmLoja.Text = '') then
  begin
      MensagemAlerta('Selecione a loja.') ;
      abort ;
  end ;

  inherited;

  qryUsuarioLojanCdUsuario.Value := qryMasternCdUsuario.Value;

  if (qryUsuarioLoja.State = dsInsert) then
      qryUsuarioLojanCdUsuarioLoja.Value := frmMenu.fnProximoCodigo('USUARIOLOJA') ;


end;

procedure TfrmUsuario.DBGridEh5KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioLoja.State = dsBrowse) then
            qryUsuarioLoja.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
                qryUsuarioLojanCdLoja.Value := nPK ;

    end ;

  end ;

end;

procedure TfrmUsuario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'USUARIO' ;
  nCdTabelaSistema  := 3 ;
  nCdConsultaPadrao := 5 ;

end;

initialization
  RegisterClass(TfrmUsuario) ;


end.
