unit fRegAtendOcorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, DBGridEhGrouping,
  ToolCtrlsEh, GridsEh, DBGridEh, cxLookAndFeelPainters, cxButtons,
  cxContainer, cxEdit, cxTextEdit, Menus;

type
  TfrmRegAtendOcorrencia = class(TfrmCadastro_Padrao)
    qryMasternCdOcorrencia: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTipoOcorrencia: TIntegerField;
    qryMastercResumoOcorrencia: TStringField;
    qryMastercDescOcorrencia: TMemoField;
    qryMasterdDtOcorrencia: TDateTimeField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdTabStatusOcorrencia: TIntegerField;
    qryMasternCdUsuarioAtend: TIntegerField;
    qryMasterdDtPrazoAtendIni: TDateTimeField;
    qryMasterdDtPrazoAtendFinal: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryMasterdDtUsuarioAtend: TDateTimeField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBMemo1: TDBMemo;
    qryEmpresa: TADOQuery;
    dsEmpresa: TDataSource;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit7: TDBEdit;
    dsLoja: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit8: TDBEdit;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label8: TLabel;
    dsTipoOcorrencia: TDataSource;
    qryTipoOcorrencia: TADOQuery;
    qryTipoOcorrenciacNmTipoOcorrencia: TStringField;
    qryTipoOcorrenciaiDiasAtendimento: TIntegerField;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    cxPageControl1: TcxPageControl;
    tabAtendimento: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    qryContatoOcorrencia: TADOQuery;
    dsContatoOcorrencia: TDataSource;
    qryContatoTerceiro: TADOQuery;
    qryContatoTerceironCdTerceiro: TIntegerField;
    qryContatoTerceirocNmTipoTelefone: TStringField;
    qryContatoTerceirocTelefone: TStringField;
    qryContatoTerceirocRamal: TStringField;
    qryContatoTerceirocContato: TStringField;
    qryContatoTerceirocDepartamento: TStringField;
    dsContatoTerceiro: TDataSource;
    tabFollowUp: TcxTabSheet;
    DBEdit13: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit17: TDBEdit;
    Label15: TLabel;
    DBEdit18: TDBEdit;
    Label16: TLabel;
    DBEdit19: TDBEdit;
    Label17: TLabel;
    DBMemo2: TDBMemo;
    qryMastercNmUsuarioCad: TStringField;
    qryUsuarioCad: TADOQuery;
    dsUsuarioCad: TDataSource;
    qryUsuarioCadcNmUsuario: TStringField;
    DBEdit20: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox1: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    cxButton2: TcxButton;
    btLeitura: TcxButton;
    cxButton3: TcxButton;
    GroupBox2: TGroupBox;
    btRegAtendimento: TcxButton;
    qryTabStatusOcorrencia: TADOQuery;
    qryTabStatusOcorrenciacNmTabStatusOcorrencia: TStringField;
    DBEdit16: TDBEdit;
    dsTabStatusOcorrencia: TDataSource;
    btGerarOcorrencia: TToolButton;
    ToolButton12: TToolButton;
    qryVerifTipoOcorrenciaUsuarioReg: TADOQuery;
    qryVerifTipoOcorrenciaUsuarioRegcNmTipoOcorrencia: TStringField;
    qryVerifTipoOcorrenciaUsuarioRegiDiasAtendimento: TIntegerField;
    dsVerifTipoOcorrenciaUsuarioReg: TDataSource;
    qryAux: TADOQuery;
    DBEdit14: TDBEdit;
    qryMasternCdUsuarioResp: TIntegerField;
    Label12: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    qryContatoOcorrencianCdContatoOcorrencia: TIntegerField;
    qryContatoOcorrencianCdUsuarioContato: TIntegerField;
    qryContatoOcorrencianCdOcorrencia: TIntegerField;
    qryContatoOcorrenciacDescAtendimento: TMemoField;
    qryContatoOcorrenciacNmContatoOcorrencia: TStringField;
    qryContatoOcorrenciacTelefoneContato: TStringField;
    qryContatoOcorrencianCdTabStatusOcorrencia: TIntegerField;
    Label14: TLabel;
    DBEdit23: TDBEdit;
    cxPageControl2: TcxPageControl;
    tabContatos: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    qryContatoOcorrenciadDtAtendimento: TDateTimeField;
    qryContatoOcorrenciacHoraAtendimento: TStringField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryGeraPrazoAtend: TADOQuery;
    qryGeraPrazoAtenddDtPrazoAtendIni: TDateTimeField;
    qryGeraPrazoAtenddDtPrazoAtendFinal: TDateTimeField;
    qryUsuarioResp: TADOQuery;
    StringField1: TStringField;
    dsUsuarioResp: TDataSource;
    qryUsuarioRespnCdUsuario: TIntegerField;
    qryUsuarioCadnCdUsuario: TIntegerField;
    dsUsuarioAtend: TDataSource;
    qryUsuarioAtend: TADOQuery;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    qryTabStatusOcorrencianCdTabStatusOcorrencia: TIntegerField;
    qryContatoOcorrenciacNmTabStatusOcorrencia: TStringField;
    qryContatoOcorrenciacNmUsuario: TStringField;
    DBMemo3: TDBMemo;
    DBGridEh1: TDBGridEh;
    dsFollow: TDataSource;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btRegAtendimentoClick(Sender: TObject);
    procedure qryContatoOcorrenciaCalcFields(DataSet: TDataSet);
    procedure btGerarOcorrenciaClick(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegAtendOcorrencia: TfrmRegAtendOcorrencia;

implementation

{$R *.dfm}

uses
  fLookup_Padrao, fMenu, fRegAtendOcorrencia_Atendimento;

procedure TfrmRegAtendOcorrencia.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'OCORRENCIA' ;
  nCdTabelaSistema  := 529 ;
  nCdConsultaPadrao := 778 ;
  bLimpaAposSalvar  := False;
end;

procedure TfrmRegAtendOcorrencia.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
                  if (DBEdit3.ReadOnly) then
                      Exit;

                  if (qryMaster.State in [dsInsert,dsEdit]) then
                  begin
                      nPK := frmLookup_Padrao.ExecutaConsulta(59);

                      if (nPK > 0) then
                          qryMasternCdLoja.Value := nPK;
                  end;
              end;
  end;
end;

procedure TfrmRegAtendOcorrencia.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin
      qryLoja.Close;
      qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
      qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryLoja.Open;
  end;
end;

procedure TfrmRegAtendOcorrencia.btIncluirClick(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));
  
  qryMasternCdEmpresa.Value    := frmMenu.nCdEmpresaAtiva;
  qryMasterdDtOcorrencia.Value := Now();

  if (frmMenu.LeParametro('VAREJO') = 'S') then
      DBEdit3.SetFocus
  else
      DBEdit4.SetFocus;
end;

procedure TfrmRegAtendOcorrencia.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
                  if (qryMaster.State in [dsInsert,dsEdit]) then
                  begin
                      nPK := frmLookup_Padrao.ExecutaConsulta(17);

                      if (nPK > 0) then
                          qryMasternCdTerceiro.Value := nPK;
                  end;
              end;
  end;
end;

procedure TfrmRegAtendOcorrencia.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin
      PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString);
      PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.AsString);
  end;
end;

procedure TfrmRegAtendOcorrencia.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
    VK_F4 : begin
                if (qryMaster.State in [dsInsert,dsEdit]) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta2(777,' EXISTS (SELECT 1 FROM UsuarioRegTipoOcorrencia WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND  nCdTipoOcorrencia = TipoOcorrencia.nCdTipoOcorrencia)');

                    if (nPK > 0) then
                        qryMasternCdTipoOcorrencia.Value := nPK;
                end;
            end;
  end;
end;

procedure TfrmRegAtendOcorrencia.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin
      {if (qryMasternCdTabStatusOcorrencia.Value > 1) then
      begin
          qryMasternCdTabStatusOcorrencia.Value := qryMasternCdTabStatusOcorrencia.OldValue;
          Abort;
      end;}

      qryTipoOcorrencia.Close;
      qryTipoOcorrencia.Parameters.ParamByName('nPK').Value        := qryMasternCdTipoOcorrencia.Value;
      qryTipoOcorrencia.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryTipoOcorrencia.Open;

      if (not qryTipoOcorrencia.IsEmpty) then
      begin
          qryGeraPrazoAtend.Close;
          qryGeraPrazoAtend.Parameters.ParamByName('nCdTipoOcorrencia').Value := qryMasternCdTipoOcorrencia.Value;
          qryGeraPrazoAtend.Parameters.ParamByName('cDtOcorrencia').Value     := DateToStr(qryMasterdDtOcorrencia.Value);
          qryGeraPrazoAtend.Open;

          qryMasterdDtPrazoAtendIni.Value   := qryGeraPrazoAtenddDtPrazoAtendIni.Value;
          qryMasterdDtPrazoAtendFinal.Value := qryGeraPrazoAtenddDtPrazoAtendFinal.Value;
      end;
  end;
end;

procedure TfrmRegAtendOcorrencia.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdTabStatusOcorrencia.Value > 1) then
  begin
      MensagemAlerta('Status da ocorr�ncia n�o permite altera��es.');
      PosicionaPK(qryMasternCdOcorrencia.Value);
      Abort;
  end;

  if ((qryLoja.IsEmpty) and (not DBEdit3.ReadOnly)) then
  begin
       MensagemAlerta('Informe a loja correspondente a esta ocorr�ncia.');
       DBEdit3.SetFocus;
       Abort;
  end;

  if (qryTerceiro.IsEmpty) then
  begin
       MensagemAlerta('Informe o cliente correspondente a esta ocorr�ncia.');
       DBEdit4.SetFocus;
       Abort;
  end;

  if (qryTipoOcorrencia.IsEmpty) then
  begin
       MensagemAlerta('Informe o tipo de ocorr�ncia.');
       DBEdit5.SetFocus;
       Abort;
  end;

  if (Trim(qryMastercResumoOcorrencia.Value) = '') then
  begin
      MensagemAlerta('Informe o resumo da ocorr�ncia.');
      DBEdit6.SetFocus;
      Abort;
  end;

  if (Trim(qryMastercDescOcorrencia.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o da ocorr�ncia.');
      DBMemo1.SetFocus;
      Abort;
  end;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdUsuarioCad.Value          := frmMenu.nCdUsuarioLogado;
      qryMasternCdUsuarioResp.Value         := frmMenu.nCdUsuarioLogado;
      qryMasternCdTabStatusOcorrencia.Value := 1; //status digitado
      qryMastercDescOcorrencia.Value        := AnsiUpperCase(qryMastercDescOcorrencia.Value);

      PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCad.AsString);
      PosicionaQuery(qryUsuarioResp, qryMasternCdUsuarioResp.AsString);
  end;
end;

procedure TfrmRegAtendOcorrencia.qryMasterCalcFields(DataSet: TDataSet);
begin
  inherited;

  {if (qryMaster.State <> dsInsert) then
  begin
      qryUsuario.Close;
      PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString);
      qryMastercNmUsuarioCad.Value := qryUsuariocNmUsuario.Value;

      qryUsuario.Close;
      PosicionaQuery(qryUsuario, qryMasternCdUsuarioAtend.AsString);
      qryMastercNmUsuarioAtend.Value := qryUsuariocNmUsuario.Value;

      qryUsuario.Close;
      PosicionaQuery(qryUsuario, qryMasternCdUsuarioResp.AsString);
      qryMastercNmUsuarioResp.Value := qryUsuariocNmUsuario.Value;

  end;}
end;

procedure TfrmRegAtendOcorrencia.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      Exit;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString);

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Open;

  qryTipoOcorrencia.Close;
  qryTipoOcorrencia.Parameters.ParamByName('nPK').Value        := qryMasternCdTipoOcorrencia.Value;
  qryTipoOcorrencia.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryTipoOcorrencia.Open;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString);
  PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.AsString);
  PosicionaQuery(qryTabStatusOcorrencia, qryMasternCdTabStatusOcorrencia.AsString);
  PosicionaQuery(qryContatoOcorrencia, qryMasternCdOcorrencia.AsString);
  PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCad.AsString);
  PosicionaQuery(qryUsuarioResp, qryMasternCdUsuarioResp.AsString);
  PosicionaQuery(qryUsuarioAtend, qryMasternCdUsuarioAtend.AsString);
  PosicionaQuery(qryFollow,qryMasternCdOcorrencia.AsString);
  
  if (qryMasternCdTabStatusOcorrencia.Value <> 1) then
  begin
      btGerarOcorrencia.Enabled := False;
  end;
end;

procedure TfrmRegAtendOcorrencia.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  
  qryEmpresa.Close;
  qryLoja.Close;
  qryTerceiro.Close;
  qryTipoOcorrencia.Close;
  qryContatoOcorrencia.Close;
  qryContatoTerceiro.Close;
  qryTabStatusOcorrencia.Close;
  qryUsuarioCad.Close;
  qryUsuarioResp.Close;
  qryUsuarioAtend.Close;

  btGerarOcorrencia.Enabled := True;
end;

procedure TfrmRegAtendOcorrencia.btRegAtendimentoClick(Sender: TObject);
var
  objAtendimento : TfrmRegAtendOcorrencia_Atendimento;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryMasternCdTabStatusOcorrencia.Value = 7) then
  begin
      MensagemAlerta('Ocorr�ncia finalizada n�o permite novas inclus�es de atendimento.');
      Exit;
  end;

  if (qryMasternCdTabStatusOcorrencia.Value = 1) then
  begin
      MensagemAlerta('Ocorr�ncia ainda n�o foi gerada.');
      btGerarOcorrencia.Click;
  end
  else
  begin
      { -- 2 : pendente           -- }
      { -- 3 : an�lise            -- }
      { -- 4 : retorno an�lise    -- }
      { -- 5 : aguardando contato -- }
      { -- 6 : aguardando retorno -- }
      if (qryMasternCdTabStatusOcorrencia.Value in [2,3,4,5,6]) then
      begin
          { -- verifica se usu�rio possui permiss�o para atender esta requisi��o -- }
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT TOP 1 1                                                    ');
          qryAux.SQL.Add('  FROM UsuarioAtendTipoOcorrencia                                 ');
          qryAux.SQL.Add(' WHERE nCdTipoOcorrencia = ' + qryMasternCdTipoOcorrencia.AsString );
          qryAux.SQL.Add('   AND nCdUsuario        = ' + IntToStr(frmMenu.nCdUsuarioLogado)  );
          qryAux.Open;

          if (qryAux.IsEmpty) then
          begin
              MensagemAlerta('Usu�rio ativo n�o possui permiss�o para atender esta ocorr�ncia.');
              Exit;
          end;

          objAtendimento := TfrmRegAtendOcorrencia_Atendimento.Create(nil);

          objAtendimento.nCdOcorrencia          := qryMasternCdOcorrencia.Value;
          objAtendimento.cNmTabSatusAtual       := UpperCase(qryTabStatusOcorrenciacNmTabStatusOcorrencia.Value);
          objAtendimento.nCdTabStatusOcorrencia := qryMasternCdTabStatusOcorrencia.Value;
          objAtendimento.nCdTerceiro            := qryMasternCdTerceiro.Value;
          
          showForm(objAtendimento, True);

          PosicionaPK(qryMasternCdOcorrencia.Value);
      end;
  end;
end;

procedure TfrmRegAtendOcorrencia.qryContatoOcorrenciaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  {PosicionaQuery(qryUsuario, qryContatoOcorrencianCdUsuarioContato.AsString);
  qryContatoOcorrenciacNmUsuario.Value := qryUsuariocNmUsuario.Value;

  PosicionaQuery(qryTabStatusOcorrencia, qryContatoOcorrencianCdTabStatusOcorrencia.AsString);
  qryContatoOcorrenciacNmTabStatusOcorrencia.Value := qryTabStatusOcorrenciacNmTabStatusOcorrencia.Value;}
end;

procedure TfrmRegAtendOcorrencia.btGerarOcorrenciaClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryMasternCdTabStatusOcorrencia.Value = 1) then
  begin
      case MessageDlg('Confirma a gera��o desta ocorr�ncia?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo : Exit;
      end;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE Ocorrencia SET nCdTabStatusOcorrencia = 2 WHERE nCdOcorrencia = ' + qryMasternCdOcorrencia.AsString);
      qryAux.ExecSQL;

      qryMaster.Requery();
      ShowMessage('Ocorr�ncia gerada com sucesso.');
  end;
end;

procedure TfrmRegAtendOcorrencia.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  
  PosicionaPK(qryMasternCdOcorrencia.Value);
end;

procedure TfrmRegAtendOcorrencia.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryUsuarioLoja.Open;

      if (qryUsuarioLoja.eof) then
      begin
          MensagemAlerta('Nenhuma loja vinculada para o usu�rio ativo.');
          btCancelar.Click;
          Abort;
      end;

      qryUsuarioLoja.Close;
  end
  else
  begin
      desativaDBEdit(DBEdit3);
  end;
end;

procedure TfrmRegAtendOcorrencia.btExcluirClick(Sender: TObject);
begin
  if (qryMasternCdTabStatusOcorrencia.Value > 1) then
  begin
      MensagemAlerta('Status da ocorr�ncia n�o permite exclus�o.');
      Abort;
  end;

  inherited;
end;

initialization
    RegisterClass(TfrmRegAtendOcorrencia) ;

end.
