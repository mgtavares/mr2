inherited frmPlantaMRP: TfrmPlantaMRP
  Left = 180
  Top = 119
  Caption = 'Planta MRP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 75
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEditCdPlantaMRP
  end
  object Label2: TLabel [2]
    Left = 64
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEditNmPlantaMRP
  end
  object Label3: TLabel [3]
    Left = 70
    Top = 94
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object DBEditCdPlantaMRP: TDBEdit [5]
    Tag = 1
    Left = 120
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdPlantaMRP'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEditNmPlantaMRP: TDBEdit [6]
    Left = 120
    Top = 64
    Width = 401
    Height = 19
    DataField = 'cNmPlantaMRP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object cxPageControl1: TcxPageControl [7]
    Left = 16
    Top = 120
    Width = 881
    Height = 425
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    ClientRectBottom = 421
    ClientRectLeft = 4
    ClientRectRight = 877
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios Permitidos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 873
        Height = 397
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioPlantaMRP
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnEnter = DBGridEh1Enter
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Locais de Estoque'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 873
        Height = 397
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dslocalEstoqueMRP
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnKeyDown = DBGridEh3KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmLocalEstoque'
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object ER2LookupDBEdit1: TER2LookupDBEdit [8]
    Left = 120
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdEmpresaPlanoMRP'
    DataSource = dsMaster
    TabOrder = 2
    CodigoLookup = 25
    QueryLookup = qryEmpresa
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 192
    Top = 88
    Width = 329
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 5
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM PlantaMRP'
      ' WHERE nCdPlantaMRP = :nPK')
    Left = 928
    Top = 32
    object qryMasternCdPlantaMRP: TIntegerField
      FieldName = 'nCdPlantaMRP'
    end
    object qryMastercNmPlantaMRP: TStringField
      FieldName = 'cNmPlantaMRP'
      Size = 50
    end
    object qryMasternCdEmpresaPlanoMRP: TIntegerField
      FieldName = 'nCdEmpresaPlanoMRP'
    end
  end
  inherited dsMaster: TDataSource
    Left = 960
    Top = 32
  end
  object qryUsuarioPlantaMRP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioPlantaMRPBeforePost
    OnCalcFields = qryUsuarioPlantaMRPCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuarioPlantaMRP'
      '      ,nCdPlantaMRP'
      '      ,nCdUsuario'
      '  FROM UsuarioPlantaMRP'
      ' WHERE nCdPlantaMRP = :nPK')
    Left = 720
    Top = 32
    object qryUsuarioPlantaMRPnCdUsuario: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioPlantaMRPcNmUsuario: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioPlantaMRPnCdUsuarioPlantaMRP: TIntegerField
      FieldName = 'nCdUsuarioPlantaMRP'
    end
    object qryUsuarioPlantaMRPnCdPlantaMRP: TIntegerField
      FieldName = 'nCdPlantaMRP'
    end
  end
  object dsUsuarioPlantaMRP: TDataSource
    DataSet = qryUsuarioPlantaMRP
    Left = 752
    Top = 32
  end
  object dslocalEstoqueMRP: TDataSource
    DataSet = qrylocalEstoqueMRP
    Left = 752
    Top = 64
  end
  object qrylocalEstoqueMRP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrylocalEstoqueMRPBeforePost
    OnCalcFields = qrylocalEstoqueMRPCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LocalEstoquePlantaMRP'
      ' WHERE nCdPlantaMRP = :nPK')
    Left = 720
    Top = 64
    object qrylocalEstoqueMRPnCdLocalEstoquePlantaMRP: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdLocalEstoquePlantaMRP'
    end
    object qrylocalEstoqueMRPcNmLocalEstoque: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
    object qrylocalEstoqueMRPnCdPlantaMRP: TIntegerField
      FieldName = 'nCdPlantaMRP'
    end
    object qrylocalEstoqueMRPnCdLocalEstoque: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdLocalEstoque'
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 800
    Top = 32
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdlocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK'
      ''
      '')
    Left = 800
    Top = 64
    object qryEstoquenCdlocalEstoque: TIntegerField
      FieldName = 'nCdlocalEstoque'
    end
    object qryEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK'
      '')
    Left = 800
    Top = 96
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 496
    Top = 352
  end
end
