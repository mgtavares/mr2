unit fProdutoERP_Identado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxControls, cxContainer, cxTreeView, StdCtrls;

type
  TfrmProdutoERP_Identado = class(TfrmProcesso_Padrao)
    SP_MONTA_IDENTACAO_ESTRUTURA_PRODUTO: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    qryProdutoIdentado: TADOQuery;
    qryProdutoIdentadonCdTempProdutoIdent: TAutoIncField;
    qryProdutoIdentadonCdProduto: TIntegerField;
    qryProdutoIdentadocNmDescricaoLinha: TStringField;
    qryProdutoIdentadoiNivel: TIntegerField;
    qryProdutoIdentadocFlgTipo: TStringField;
    tvProdutoIdentado: TcxTreeView;
    GroupBox1: TGroupBox;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
  private
    { Private declarations }
    rootno : TTreeNode;
  public
    { Public declarations }
    procedure EstruturaIdentada(nCdProduto : integer);
  end;

var
  frmProdutoERP_Identado: TfrmProdutoERP_Identado;

implementation

{$R *.dfm}

procedure TfrmProdutoERP_Identado.EstruturaIdentada(nCdProduto: integer);
var
  iNivel, iNo : integer;
begin
  inherited;

  iNivel := 0;

  cmdPreparaTemp.Execute;
  SP_MONTA_IDENTACAO_ESTRUTURA_PRODUTO.Parameters.ParamByName('@nCdProduto').Value := nCdProduto;
  SP_MONTA_IDENTACAO_ESTRUTURA_PRODUTO.Parameters.ParamByName('@iNivel').Value     := 0;
  SP_MONTA_IDENTACAO_ESTRUTURA_PRODUTO.ExecProc;

  qryProdutoIdentado.Close;
  qryProdutoIdentado.Open;

  qryProdutoIdentado.First;

  while not qryProdutoIdentado.Eof do
  begin

      if (qryProdutoIdentadoiNivel.Value = 0) then
      begin
          rootno := tvProdutoIdentado.Items.AddObject(nil, Trim(qryProdutoIdentadocNmDescricaoLinha.AsString), Pointer(qryProdutoIdentadonCdTempProdutoIdent.Value)) ;
      end;

      if (qryProdutoIdentadoiNivel.Value > iNivel) then
      begin
          rootno := tvProdutoIdentado.Items.AddChild(rootno, Trim(qryProdutoIdentadocNmDescricaoLinha.AsString)) ;
      end;

      if ((qryProdutoIdentadoiNivel.Value = iNivel) and (iNivel > 0)) then
      begin
          rootno := tvProdutoIdentado.Items.AddChild(rootno.Parent, Trim(qryProdutoIdentadocNmDescricaoLinha.AsString)) ;
      end;

      if ((iNivel > 1) and (qryProdutoIdentadoiNivel.Value < iNivel)) then
      begin

          for iNo := qryProdutoIdentadoiNivel.Value to iNivel do
          begin
              rootno := rootno.Parent;
          end;

          rootno := tvProdutoIdentado.Items.AddChild(rootno, Trim(qryProdutoIdentadocNmDescricaoLinha.AsString)) ;
      end;

      iNivel := qryProdutoIdentadoiNivel.Value;

      if (qryProdutoIdentadocFlgTipo.Value = 'P') then
      begin
          rootno.ImageIndex    := 3;
          rootno.SelectedIndex := 3;
      end
      else if (qryProdutoIdentadocFlgTipo.Value = 'R') then
      begin
          rootno.ImageIndex    := 4;
          rootno.SelectedIndex := 4;
      end
      else if (qryProdutoIdentadocFlgTipo.Value = 'E') then
      begin
          rootno.ImageIndex    := 5;
          rootno.SelectedIndex := 5;
      end;

      qryProdutoIdentado.Next;

  end;

  Self.ShowModal;
end;

end.
