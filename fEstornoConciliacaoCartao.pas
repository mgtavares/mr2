unit fEstornoConciliacaoCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmEstornoConciliacaoCartao = class(TfrmProcesso_Padrao)
    qryLoteConciliacao: TADOQuery;
    qryLoteConciliacaonCdLoteConciliacaoCartao: TAutoIncField;
    qryLoteConciliacaonCdOperadoraCartao: TIntegerField;
    qryLoteConciliacaocNmOperadoraCartao: TStringField;
    qryLoteConciliacaodDtConciliacaoCartao: TDateTimeField;
    qryLoteConciliacaonCdUsuario: TIntegerField;
    qryLoteConciliacaocNmUsuario: TStringField;
    qryLoteConciliacaonValLote: TBCDField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsLoteConciliacao: TDataSource;
    cxGrid1DBTableView1nCdLoteConciliacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1nCdOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtConciliacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1nValLote: TcxGridDBColumn;
    SP_ESTORNA_CONCILIACAO_CARTAO: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstornoConciliacaoCartao: TfrmEstornoConciliacaoCartao;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmEstornoConciliacaoCartao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryLoteConciliacao.Close ;
  qryLoteConciliacao.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
  qryLoteConciliacao.Parameters.ParamByName('dDtConciliacao').Value := DateToStr(Date) ;
  qryLoteConciliacao.Open ;

end;

procedure TfrmEstornoConciliacaoCartao.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
  
end;

procedure TfrmEstornoConciliacaoCartao.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryLoteConciliacao.Eof then
  begin

      if (qryLoteConciliacaonCdUsuario.Value <> frmMenu.nCdUsuarioLogado) then
      begin
          MensagemAlerta('Somente o usu�rio ' + trim(qryLoteConciliacaocNmUsuario.Value) + ' pode estornar este lote.') ;
          exit ;
      end ;

      case MessageDlg('Confirma o estorno deste lote ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          SP_ESTORNA_CONCILIACAO_CARTAO.Close ;
          SP_ESTORNA_CONCILIACAO_CARTAO.Parameters.ParamByName('@nCdLoteConciliacaoCartao').Value := qryLoteConciliacaonCdLoteConciliacaoCartao.Value ;
          SP_ESTORNA_CONCILIACAO_CARTAO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ToolButton1.Click;

  end ;

end;

initialization
    RegisterClass(TfrmEstornoConciliacaoCartao) ;
    
end.
