unit fGrupoUsuario_Permissoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Menus,
  cxControls, cxContainer, cxTreeView, DB, ADODB;

type
  TfrmGrupoUsuario_Permissoes = class(TfrmProcesso_Padrao)
    tvMenus: TcxTreeView;
    qryMenus: TADOQuery;
    qryAplicacaoGrupoUsuario: TADOQuery;
    qryAplicacaoGrupoUsuarionCdGrupoUsuario: TIntegerField;
    qryAplicacaoGrupoUsuarionCdAplicacao: TIntegerField;
    qryAplicacaoGrupoUsuariodDataAcesso: TDateTimeField;
    qryAplicacaoGrupoUsuarionCdUsuarioAcesso: TIntegerField;
    qryAplicacaoGrupoUsuariocFlgAlterar: TIntegerField;
    qryAplicacaoGrupoUsuarionCdAplicacaoGrupoUsuario: TIntegerField;
    qryMenusnCdAplicacaoGrupoUsuario: TIntegerField;
    qryMenusnCdMenuPrincipal: TIntegerField;
    qryMenuscNmMenuPrincipal: TStringField;
    qryMenusnCdSubMenu: TIntegerField;
    qryMenuscNmSubMenu: TStringField;
    qryMenusnCdAplicacao: TIntegerField;
    qryMenuscNmAplicacao: TStringField;
    qryMenuscFlgAlterar: TIntegerField;
    qryMenusnCdGrupoUsuario: TIntegerField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure tvMenusCollapsed(Sender: TObject; Node: TTreeNode);
    procedure tvMenusExpanded(Sender: TObject; Node: TTreeNode);
    procedure qryAplicacaoGrupoUsuarioBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure tvMenusDblClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
    nCdAplicacaoGrupoUsuario : integer;
    procedure ProcessaMenu;
  public
    { Public declarations }
    nCdGrupoUsuario : integer;
  end;

var
  frmGrupoUsuario_Permissoes: TfrmGrupoUsuario_Permissoes;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmGrupoUsuario_Permissoes.FormShow(Sender: TObject);
begin
  inherited;

  ProcessaMenu;
end;

procedure TfrmGrupoUsuario_Permissoes.tvMenusCollapsed(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  if (tvMenus.Selected.HasChildren) then
  begin
      tvMenus.Selected.ImageIndex    := 5;
      tvMenus.Selected.SelectedIndex := 5;
  end;
end;

procedure TfrmGrupoUsuario_Permissoes.tvMenusExpanded(Sender: TObject;
  Node: TTreeNode);
begin
  inherited;

  if (tvMenus.Selected.HasChildren) then
  begin
      tvMenus.Selected.ImageIndex    := 4;
      tvMenus.Selected.SelectedIndex := 4;
  end;

end;

procedure TfrmGrupoUsuario_Permissoes.ProcessaMenu;
var
  nCdMenu,nCdSubMenu : integer;
  raiz, rootno, no : TTreeNode;
begin

  tvMenus.Items.Clear;
  tvMenus.Items.BeginUpdate;

  qryMenus.Close;
  qryMenus.Parameters.ParamByName('nCdGrupoUsuario').Value := nCdGrupoUsuario;
  qryMenus.Open;

  qryAplicacaoGrupoUsuario.Close;
  qryAplicacaoGrupoUsuario.Parameters.ParamByName('nCdGrupoUsuario').Value := nCdGrupoUsuario;
  qryAplicacaoGrupoUsuario.Open;

  while not qryMenus.Eof do
  begin

      if (nCdMenu <> qryMenusnCdMenuPrincipal.Value) then
      begin
          nCdMenu := qryMenusnCdMenuPrincipal.Value;
          raiz    := tvMenus.Items.AddObject(nil, qryMenuscNmMenuPrincipal.Value, Pointer(qryMenusnCdMenuPrincipal.Value)) ;
          raiz.ImageIndex    := 5;
          raiz.SelectedIndex := 5;
      end;

      if (nCdSubMenu <> qryMenusnCdSubMenu.Value) then
      begin
          nCdSubMenu := qryMenusnCdSubMenu.Value;
          rootno     := tvMenus.Items.AddChildObject(raiz, qryMenuscNmSubMenu.Value, Pointer(qryMenusnCdSubMenu.Value)) ;
          rootno.ImageIndex    := 5;
          rootno.SelectedIndex := 5;
      end;

      no := tvMenus.Items.AddChildObject(rootno, qryMenuscNmAplicacao.Value, Pointer(qryMenusnCdAplicacaoGrupoUsuario.Value)) ;
      no.ImageIndex    := 12;
      no.SelectedIndex := 12;

      if (qryMenusnCdGrupoUsuario.Value = 0) then
      begin
          no.ImageIndex    := 11;
          no.SelectedIndex := 11;
      end;

      qryMenus.Next;
  end;

  tvMenus.Items.EndUpdate;

  tvMenus.OnExpanded := tvMenusExpanded;
  tvMenus.Selected   := tvMenus.Items[0];

end;

procedure TfrmGrupoUsuario_Permissoes.qryAplicacaoGrupoUsuarioBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryAplicacaoGrupoUsuario.State = dsInsert) then
      qryAplicacaoGrupoUsuarionCdAplicacaoGrupoUsuario.Value := frmMenu.fnProximoCodigo('APLICACAOGRUPOUSUARIO') ;

  qryAplicacaoGrupoUsuarionCdGrupoUsuario.Value  := nCdGrupoUsuario;
  qryAplicacaoGrupoUsuariocFlgAlterar.Value      := 1;
  qryAplicacaoGrupoUsuariodDataAcesso.Value      := Now() ;
  qryAplicacaoGrupoUsuarionCdUsuarioAcesso.Value := frmMenu.nCdUsuarioLogado ;
end;

procedure TfrmGrupoUsuario_Permissoes.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryAplicacaoGrupoUsuario.UpdateBatch;
  qryAplicacaoGrupoUsuario.Close;
  Close;
end;

procedure TfrmGrupoUsuario_Permissoes.tvMenusDblClick(Sender: TObject);
begin
  inherited;

  if not(tvMenus.Selected.HasChildren) then
  begin
      qryMenus.Locate('cNmAplicacao',tvMenus.Selected.Text,[]);

      if not (qryAplicacaoGrupoUsuario.Locate('nCdAplicacao',qryMenusnCdAplicacao.Value,[])) then
      begin
          tvMenus.Selected.ImageIndex    := 12;
          tvMenus.Selected.SelectedIndex := 12;

          qryAplicacaoGrupoUsuario.Insert;
          qryAplicacaoGrupoUsuarionCdAplicacao.Value := qryMenusnCdAplicacao.Value;
          qryAplicacaoGrupoUsuario.Post;

      end else
      begin
          tvMenus.Selected.ImageIndex    := 11;
          tvMenus.Selected.SelectedIndex := 11;

          qryAplicacaoGrupoUsuario.Delete;
      end;
  end;

end;

procedure TfrmGrupoUsuario_Permissoes.ToolButton5Click(Sender: TObject);
begin
  inherited;
  
  tvMenus.Items.BeginUpdate;
  tvMenus.FullExpand;
  tvMenus.Selected   := tvMenus.Items[0];
  tvMenus.Items.EndUpdate;

end;

procedure TfrmGrupoUsuario_Permissoes.ToolButton7Click(Sender: TObject);
begin
  inherited;

  tvMenus.Items.BeginUpdate;
  tvMenus.FullCollapse;
  tvMenus.Selected   := tvMenus.Items[0];
  tvMenus.Items.EndUpdate;

end;

end.
