inherited frmTipoServicoImposto: TfrmTipoServicoImposto
  Left = 51
  Top = 134
  Height = 600
  Caption = 'Cadastro Tipo de Servi'#231'os Impostos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 548
  end
  object Label1: TLabel [1]
    Left = 36
    Top = 43
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 25
    Top = 88
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 33
    Top = 65
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 79
    Top = 37
    Width = 65
    Height = 19
    DataField = 'nCdTipoServicoImposto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 79
    Top = 82
    Width = 650
    Height = 19
    DataField = 'cNmTipoServicoImposto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 79
    Top = 59
    Width = 65
    Height = 19
    DataField = 'iNroTipoServicoImposto'
    DataSource = dsMaster
    TabOrder = 3
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoServicoImposto'
      'WHERE nCdTipoServicoImposto = :nPk')
    object qryMasternCdTipoServicoImposto: TIntegerField
      FieldName = 'nCdTipoServicoImposto'
    end
    object qryMasteriNroTipoServicoImposto: TIntegerField
      FieldName = 'iNroTipoServicoImposto'
    end
    object qryMastercNmTipoServicoImposto: TStringField
      FieldName = 'cNmTipoServicoImposto'
      Size = 150
    end
  end
end
