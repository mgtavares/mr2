inherited frmGrupoEstoque: TfrmGrupoEstoque
  Caption = 'Grupo de Estoque'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 64
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Estoque'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 106
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoEstoque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 106
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoEstoque'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEstoque'
      'WHERE nCdGrupoEstoque = :nPK')
    object qryMasternCdGrupoEstoque: TIntegerField
      FieldName = 'nCdGrupoEstoque'
    end
    object qryMastercNmGrupoEstoque: TStringField
      FieldName = 'cNmGrupoEstoque'
      Size = 50
    end
  end
end
