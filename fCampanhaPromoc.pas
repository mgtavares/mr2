unit fCampanhaPromoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, cxLookAndFeelPainters, cxButtons,
  cxPC, cxControls, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh,
  ER2Excel, Menus;

type
  TfrmCampanhaPromoc = class(TfrmCadastro_Padrao)
    qryMasternCdCampanhaPromoc: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercChaveCampanha: TStringField;
    qryMastercNmCampanhaPromoc: TStringField;
    qryMastercDescricaoCampanha: TMemoField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasterdDtValidadeFim: TDateTimeField;
    qryMastercFlgAtivada: TIntegerField;
    qryMasterdDtAtivacao: TDateTimeField;
    qryMasternCdUsuarioAtivacao: TIntegerField;
    qryMasternValVenda: TBCDField;
    qryMasternValCMV: TBCDField;
    qryMasternMargemLucro: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBMemo1: TDBMemo;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    cxButton1: TcxButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    Image2: TImage;
    cxPageControl2: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    qryProduto: TADOQuery;
    qryProdutonCdItemCampanhaPromoc: TAutoIncField;
    qryProdutonCdCampanhaPromoc: TIntegerField;
    qryProdutocFlgTipoProduto: TStringField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocFlgSubProduto: TIntegerField;
    qryProdutonQtde: TBCDField;
    qryProdutocFlgTipoValor: TStringField;
    qryProdutonValor: TBCDField;
    dsProduto: TDataSource;
    qryProdutocNmProduto: TStringField;
    qryNomeProduto: TADOQuery;
    qryNomeProdutocNmProduto: TStringField;
    qryItemDepartamento: TADOQuery;
    qryItemDepartamentonCdItemCampanhaPromoc: TAutoIncField;
    qryItemDepartamentonCdCampanhaPromoc: TIntegerField;
    qryItemDepartamentonCdDepartamento: TIntegerField;
    qryItemDepartamentonCdCategoria: TIntegerField;
    qryItemDepartamentonCdSubCategoria: TIntegerField;
    qryItemDepartamentonCdSegmento: TIntegerField;
    qryItemDepartamentonQtde: TBCDField;
    qryItemDepartamentocFlgTipoValor: TStringField;
    qryItemDepartamentonValor: TBCDField;
    qryItemDepartamentocNmDepartamento: TStringField;
    qryItemDepartamentocNmCategoria: TStringField;
    qryItemDepartamentocNmSubCategoria: TStringField;
    qryItemDepartamentocNmSegmento: TStringField;
    DBGridEh2: TDBGridEh;
    dsItemDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryCategoria: TADOQuery;
    qrySubCategoria: TADOQuery;
    qrySegmento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryDepartamentonCdGrupoProduto: TIntegerField;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBGridEh3: TDBGridEh;
    qryItemLinha: TADOQuery;
    qryItemLinhanCdItemCampanhaPromoc: TAutoIncField;
    qryItemLinhanCdCampanhaPromoc: TIntegerField;
    qryItemLinhanCdLinha: TIntegerField;
    qryItemLinhacFlgTipoValor: TStringField;
    qryItemLinhanValor: TBCDField;
    dsItemLinha: TDataSource;
    qryItemLinhacNmLinha: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryItemMarca: TADOQuery;
    dsItemMarca: TDataSource;
    qryItemMarcanCdItemCampanhaPromoc: TAutoIncField;
    qryItemMarcanCdCampanhaPromoc: TIntegerField;
    qryItemMarcanCdMarca: TIntegerField;
    qryItemMarcacFlgTipoValor: TStringField;
    qryItemMarcanValor: TBCDField;
    qryItemMarcacNmMarca: TStringField;
    DBGridEh4: TDBGridEh;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryItemGrupoProduto: TADOQuery;
    dsItemGrupoProduto: TDataSource;
    qryItemGrupoProdutonCdItemCampanhaPromoc: TAutoIncField;
    qryItemGrupoProdutonCdCampanhaPromoc: TIntegerField;
    qryItemGrupoProdutonCdGrupoProduto: TIntegerField;
    qryItemGrupoProdutocFlgTipoValor: TStringField;
    qryItemGrupoProdutonValor: TBCDField;
    qryItemGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBGridEh5: TDBGridEh;
    qryLojaCampanha: TADOQuery;
    qryLojaCampanhanCdLojaCampanhaPromoc: TAutoIncField;
    qryLojaCampanhanCdCampanhaPromoc: TIntegerField;
    qryLojaCampanhanCdLoja: TIntegerField;
    qryLojaCampanhacNmLoja: TStringField;
    dsLojaCampanha: TDataSource;
    DBGridEh6: TDBGridEh;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryTipoTabPreco: TADOQuery;
    qryTipoTabPreconCdTipoTabPreco: TIntegerField;
    qryTipoTabPrecocNmTipoTabPreco: TStringField;
    Image3: TImage;
    cxButton2: TcxButton;
    qryNomeProdutonValVenda: TBCDField;
    qryValidaProdutoCampanha: TADOQuery;
    qryValidaProdutoCampanhanCdCampanhaPromoc: TIntegerField;
    qryMasternCdTipoTabPreco: TIntegerField;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DataSource2: TDataSource;
    qryMastercFlgSuspensa: TIntegerField;
    qryValidaCampanha: TADOQuery;
    qryValidaCampanhanCdCampanhaPromoc: TIntegerField;
    qryValidaProdutoLojaCampanha: TADOQuery;
    qryValidaProdutoLojaCampanhanCdCampanhaPromoc: TIntegerField;
    qryProdutoSel: TADOQuery;
    qryProdutoSelcFlgAux: TIntegerField;
    qryProdutoSelnCdProduto: TIntegerField;
    qryProdutoSelcReferencia: TStringField;
    qryProdutoSelcNmProduto: TStringField;
    qryProdutoSelnValCusto: TBCDField;
    qryProdutoSelnValVenda: TBCDField;
    qryProdutoSelnMarkUp: TBCDField;
    qryProdutoSeldDtUltVenda: TDateTimeField;
    qryProdutoSeldDtUltReceb: TDateTimeField;
    qryProdutoSeliDiasEstoque: TIntegerField;
    qryProdutoSelnQtdeEstoque: TBCDField;
    qryProdutoSelcNmDepartamento: TStringField;
    qryProdutoSelcNmCategoria: TStringField;
    qryProdutoSelcNmSubCategoria: TStringField;
    qryProdutoSelcNmSegmento: TStringField;
    qryProdutoSelcNmLinha: TStringField;
    qryProdutoSelcNmMarca: TStringField;
    qryProdutoSelcNmGrupoProduto: TStringField;
    qryProdutoSelcNmClasseProduto: TStringField;
    qryProdutoSelcNmStatus: TStringField;
    qryAux: TADOQuery;
    qryProdutonValVendaAtual: TBCDField;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    qryAuxProduto: TADOQuery;
    qryAuxProdutonCdItemCampanhaPromoc: TIntegerField;
    qryAuxProdutonCdCampanhaPromoc: TIntegerField;
    qryAuxProdutocFlgTipoProduto: TStringField;
    qryAuxProdutonCdProduto: TIntegerField;
    qryAuxProdutocNmProduto: TStringField;
    qryAuxProdutocFlgSubProduto: TIntegerField;
    qryAuxProdutonQtde: TBCDField;
    qryAuxProdutocFlgTipoValor: TStringField;
    qryAuxProdutonValor: TBCDField;
    qryAuxProdutonValVendaAtual: TBCDField;
    qryIncluirProdutos: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryEfetivaAlt: TADOQuery;
    qryValidaItensCampanha: TADOQuery;
    qryValidaItensCampanhanCdProduto: TIntegerField;
    qryValidaItensCampanhacNmProduto: TStringField;
    qryValidaItensCampanhanValor: TBCDField;
    qryValidaItensCampanhanCdCampanhaPromoc: TIntegerField;
    cxPageControl3: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    Image4: TImage;
    Label14: TLabel;
    DBGridEh1: TDBGridEh;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    edtFiltro: TEdit;
    cxTabSheet10: TcxTabSheet;
    DBGridEh8: TDBGridEh;
    dsHistInclusaoProduto: TDataSource;
    qryHistInclusaoProduto: TADOQuery;
    qryHistInclusaoProdutonCdProduto: TIntegerField;
    Image5: TImage;
    Label13: TLabel;
    edtFiltroHis: TEdit;
    qryHistInclusaoProdutocNmProduto: TStringField;
    qryMastercFlgAtivadaWeb: TIntegerField;
    ToolButton12: TToolButton;
    GroupBox3: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    checkCampanhaAtivaWeb: TDBCheckBox;
    ER2Excel: TER2Excel;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMasterdDtValidadeIniWeb: TDateTimeField;
    qryMasterdDtValidadeFimWeb: TDateTimeField;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    btOpcao: TToolButton;
    menuOpcao: TPopupMenu;
    btImpCampanha: TMenuItem;
    N1: TMenuItem;
    btExportaCampanha: TMenuItem;
    N2: TMenuItem;
    btAtivaInativaCampanhaWeb: TMenuItem;
    SP_ATUALIZA_PRECO_PROMOCIONAL_WEB: TADOStoredProc;
    cxButton8: TcxButton;
    OpenDialog1: TOpenDialog;
    Label17: TLabel;
    Edit2: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryProdutoAfterInsert(DataSet: TDataSet);
    procedure qryProdutoBeforePost(DataSet: TDataSet);
    procedure qryProdutoCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemDepartamentoCalcFields(DataSet: TDataSet);
    procedure qryItemDepartamentoBeforePost(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemLinhaCalcFields(DataSet: TDataSet);
    procedure qryItemLinhaBeforePost(DataSet: TDataSet);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemMarcaCalcFields(DataSet: TDataSet);
    procedure qryItemMarcaBeforePost(DataSet: TDataSet);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemGrupoProdutoCalcFields(DataSet: TDataSet);
    procedure qryItemGrupoProdutoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryLojaCampanhaBeforePost(DataSet: TDataSet);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryLojaCampanhaCalcFields(DataSet: TDataSet);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure edtFiltroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFiltroHisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure checkCampanhaAtivaWebClick(Sender: TObject);
    procedure btImpCampanhaClick(Sender: TObject);
    procedure btExportaCampanhaClick(Sender: TObject);
    procedure btAtivaInativaCampanhaWebClick(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure dsProdutoDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCampanhaPromoc: TfrmCampanhaPromoc;
  cVarejo          : string ;

implementation

uses fMenu, fLookup_Padrao, fCampanhaPromoc_IncluiProduto,
  fCampanhaPromoc_AnaliseVendas, fCampanhaPromoc_AnaliseVendasERP,
  rCampanhaPromoc_view, Math, DateUtils;

{$R *.dfm}

procedure TfrmCampanhaPromoc.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CAMPANHAPROMOC' ;
  nCdTabelaSistema  := 85 ;
  nCdConsultaPadrao := 194 ;
  bLimpaAposSalvar  := false ;

end;

procedure TfrmCampanhaPromoc.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBedit4.Text = '') then
  begin
      MensagemAlerta('Informe o nome da campanha promocional.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtValidadeIni.AsString = '') or (qryMasterdDtValidadeFim.AsString = '') then
  begin
      MensagemAlerta('Informe o per�odo de validade desta campanha.') ;
      DBedit5.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtValidadeIni.Value > qryMasterdDtValidadeFim.Value) then
  begin
      MensagemAlerta('Per�odo de validade inv�lido.') ;
      DBedit5.SetFocus;
      abort ;
  end ;

  if (checkCampanhaAtivaWeb.Checked) then
  begin
      if (qryMasterdDtValidadeIniWeb.AsString = '') or (qryMasterdDtValidadeFimWeb.AsString = '') then
      begin
          MensagemAlerta('Informe o per�odo de validade desta campanha na web.') ;
          DBedit13.SetFocus;
          Abort;
      end;

      if (qryMasterdDtValidadeIniWeb.Value > qryMasterdDtValidadeFimWeb.Value) then
      begin
          MensagemAlerta('Per�odo de validade web inv�lido.') ;
          DBedit13.SetFocus;
          abort ;
      end ;
  end;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  end ;

  qryMastercDescricaoCampanha.Value    := Uppercase(qryMastercDescricaoCampanha.Value) ;
  qryMastercFlgAtivada.Value           := 0 ;
  qryMasterdDtAtivacao.AsString        := '' ;
  qryMasternCdUsuarioAtivacao.AsString := '' ;

  inherited;

end;

procedure TfrmCampanhaPromoc.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  cxPageControl2.ActivePageIndex := 0 ;
  cxPageControl3.ActivePageIndex := 0 ;

  if (qryMaster.State = dsInsert) then
      exit ;

  PosicionaQuery(qryUsuario, qryMasternCdUsuarioAtivacao.AsString) ;

  {-- adiciona todos os produtos da campanha na tabela tempor�ria --}
  qryPreparaTemp.Close;
  qryPreparaTemp.Open;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMasternCdCampanhaPromoc.Value ;
  qryPopulaTemp.ExecSQL;

  qryProduto.Close;
  qryProduto.Open;

  qryHistInclusaoProduto.Close;
  qryHistInclusaoProduto.Open;
  {--}

  PosicionaQuery(qryItemDepartamento, qryMasternCdCampanhaPromoc.AsString) ;
  PosicionaQuery(qryItemLinha, qryMasternCdCampanhaPromoc.AsString) ;
  PosicionaQuery(qryItemMarca, qryMasternCdCampanhaPromoc.AsString) ;
  PosicionaQuery(qryItemGrupoProduto, qryMasternCdCampanhaPromoc.AsString) ;
  PosicionaQuery(qryLojaCampanha, qryMasternCdCampanhaPromoc.AsString) ;
  PosicionaQuery(qryTipoTabPreco, qryMasternCdTipoTabPreco.AsString) ;
  PosicionaQuery(qryAuxProduto, qryMasternCdCampanhaPromoc.AsString) ;


end;

procedure TfrmCampanhaPromoc.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  cxPageControl2.ActivePageIndex := 0 ;

  if (bInclusao) then
      DBGridEh1.SetFocus;

end;

procedure TfrmCampanhaPromoc.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  cxPageControl1.ActivePageIndex := 0 ;
  cxPageControl2.ActivePageIndex := 0 ;
  cxPageControl3.ActivePageIndex := 0 ;

  {tab das lojas}
  cVarejo := frmMenu.LeParametro('VAREJO') ;

  cxTabSheet2.Enabled := (cVarejo = 'S') ;
  
end;

procedure TfrmCampanhaPromoc.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.state = dsInsert) then
      qryMaster.Post ;

end;

procedure TfrmCampanhaPromoc.qryProdutoAfterInsert(DataSet: TDataSet);
begin
  inherited;

  qryProdutocFlgTipoValor.Value := 'V' ;

end;

procedure TfrmCampanhaPromoc.qryProdutoBeforePost(DataSet: TDataSet);
begin

  if (qryProdutocNmProduto.Value = '') then
  begin
      MensagemAlerta('Selecione um produto.');
      abort ;
  end ;

  if (qryProdutonQtde.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.');
      qryProdutonQtde.Value := 0 ;
      abort ;
  end ;

  inherited;

  if (qryProduto.State = dsInsert) then
      qryProdutonCdItemCampanhaPromoc.Value := frmMenu.fnProximoCodigo('ITEMCAMPANHAPROMOC') ;

  qryProdutonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
  qryProdutocFlgTipoProduto.Value   := 'I' ;
  qryProdutocFlgSubProduto.Value    := 0 ;


end;

procedure TfrmCampanhaPromoc.qryProdutoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryProdutocNmProduto.Value = '') and (qryProdutonCdProduto.Value > 0) then
  begin
      qryNomeProduto.Close ;
      PosicionaQuery(qryNomeProduto, qryProdutonCdProduto.AsString) ;

      if not qryNomeProduto.Eof then
          qryProdutocNmProduto.Value := qryNomeProdutocNmProduto.Value ;

  end ;

end;

procedure TfrmCampanhaPromoc.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryProduto.State = dsBrowse) then
             qryProduto.Edit ;

        if (qryProduto.State = dsInsert) or (qryProduto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(193);

            If (nPK > 0) then
            begin
                qryProdutonCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.Active) then
      if (qryMastercFlgAtivada.Value = 1) then
          if (MessageDlg('Esta campanha promocional est� ativada. Se voc� alter�-la ela dever� ser novamente ativada.' +#13#13+ 'Continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
              exit ;

  qryMaster.Post ;

  if (qryProduto.State = dsInactive) or (qryProduto.RecordCount = 0) then
  begin
      if (qryMastercFlgSuspensa.Value = 0) then
      begin
          MensagemAlerta('Selecione ao menos um produto para a campanha ou marque a campanha como suspensa.') ;
          exit ;
      end ;
  end ;
       if (cVarejo = 'S') then
  begin
      if (qryLojaCampanha.State = dsInactive) or (qryLojaCampanha.RecordCount = 0) then
      begin
          MensagemAlerta('Selecione ao menos uma loja para a campanha.') ;
          exit ;
      end ;
  end ;

  if (MessageDlg('Confirma a grava��o desta campanha promocional ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  {-- se a campanha n�o estiver suspensa, valida se est� conflitando com outra campanha --}

  if (qryMastercFlgSuspensa.Value = 0) then
  begin
      if (frmMenu.LeParametro('VALIDACAMPANHA') = 'S') then
      begin
          {-- Valida se a campanha n�o ir� conflitar com outra campanha --}
          qryValidaCampanha.Close;
          qryValidaCampanha.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
          qryValidaCampanha.Parameters.ParamByName('nCdTipoTabPreco').Value   := qryMasternCdTipoTabPreco.Value ;
          qryValidaCampanha.Parameters.ParamByName('dDtValidadeIni').Value    := qryMasterdDtValidadeIni.AsString;
          qryValidaCampanha.Parameters.ParamByName('dDtValidadeFim').Value    := qryMasterdDtValidadeFim.AsString;
          qryValidaCampanha.Parameters.ParamByName('nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.AsString;
          qryValidaCampanha.Open;

          if not qryValidaCampanha.eof then
          begin
              MensagemAlerta('Esta campanha est� conflitando com outra campanha promocional, verifique.' + #13#13+ 'C�digo da campanha conflitante: ' + qryValidaCampanhanCdCampanhaPromoc.asString) ;
              abort ;
          end ;
      end;

      qryProduto.First;

      {-- Valida se os produtos da campanha n�o ir�o conflitar com outra campanha --}
      {-- se n�o for modo de varejo, procura somente produto x campanha --}
      if (cVarejo = 'N') then
      begin
          qryValidaItensCampanha.Close;
          qryValidaItensCampanha.Parameters.ParamByName('nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.AsString;
          qryValidaItensCampanha.Parameters.ParamByName('dDtValidadeIni').Value    := qryMasterdDtValidadeIni.AsString;
          qryValidaItensCampanha.Parameters.ParamByName('dDtValidadeFim').Value    := qryMasterdDtValidadeFim.AsString;
          qryValidaItensCampanha.Parameters.ParamByName('nCdTipoTabPreco').Value   := qryMasternCdTipoTabPreco.Value ;
          qryValidaItensCampanha.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
          qryValidaItensCampanha.Parameters.ParamByName('nCdLoja').Value           := 0;
          qryValidaItensCampanha.Open;

          if not (qryValidaItensCampanha.IsEmpty) then
          begin
              MensagemAlerta('O produto ' + qryValidaItensCampanhanCdProduto.AsString + ' - ' + qryValidaItensCampanhacNmProduto.Value + ' est� em uma outra campanha promocional com validade conflitante com a campanha atual.' + #13#13+ 'C�digo da campanha conflitante: ' + qryValidaItensCampanhanCdCampanhaPromoc.asString) ;
              abort ;
          end;

      end else
      begin
          qryLojaCampanha.First;

          {-- verifica se os itens podem conflitar com outras campanhas de cada loja participante --}
          while not qryLojaCampanha.Eof do
          begin

              qryValidaItensCampanha.Close;
              qryValidaItensCampanha.Parameters.ParamByName('nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.AsString;
              qryValidaItensCampanha.Parameters.ParamByName('dDtValidadeIni').Value    := qryMasterdDtValidadeIni.AsString;
              qryValidaItensCampanha.Parameters.ParamByName('dDtValidadeFim').Value    := qryMasterdDtValidadeFim.AsString;
              qryValidaItensCampanha.Parameters.ParamByName('nCdTipoTabPreco').Value   := qryMasternCdTipoTabPreco.Value ;
              qryValidaItensCampanha.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
              qryValidaItensCampanha.Parameters.ParamByName('nCdLoja').Value           := qryLojaCampanhanCdLoja.Value;
              qryValidaItensCampanha.Open;

              if not (qryValidaItensCampanha.IsEmpty) then
              begin
                  MensagemAlerta('O produto ' + qryValidaItensCampanhanCdProduto.AsString + ' - ' + qryValidaItensCampanhacNmProduto.Value + ' est� em uma outra campanha promocional com validade conflitante com a campanha atual.' + #13#13+ 'C�digo da campanha conflitante: ' + qryValidaItensCampanhanCdCampanhaPromoc.asString) ;
                  abort ;
              end;

              qryLojaCampanha.Next;
          end;

          qryLojaCampanha.First;
      end;

      {-- valida os pre�os de promo��o --}
      qryAux.Close;
      qryAux.sql.Clear;
      qryAux.SQL.Add('SELECT TOP 1 * FROM #Temp_ItemCampanhaPromoc WHERE nValor <= 0');
      qryAux.Open;

      if (qryAux.RecordCount > 0) then
      begin
          qryProduto.Locate('nCdProduto',qryAux.FieldValues['nCdProduto'],[loCaseInsensitive]);
          MensagemAlerta('O produto ' + qryProdutonCdProduto.AsString + ' - ' + qryProdutocNmProduto.Value + ' est� sem valor promocional.') ;
          abort ;
      end;

  end ;

  try
      if (qryMaster.State <> dsInactive) then
          qryMaster.UpdateBatch();

      if (qryProduto.State <> dsInactive) then
      begin

          frmMenu.Connection.BeginTrans;

          try
              qryEfetivaAlt.Close;
              qryEfetivaAlt.Parameters.ParamByName('nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.Value;
              qryEfetivaAlt.ExecSQL;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.');
              raise;
          end;

          frmMenu.Connection.CommitTrans;

      end;

      if (qryItemDepartamento.State <> dsInactive) then
          qryItemDepartamento.UpdateBatch();

      if (qryItemLinha.State <> dsInactive) then
          qryItemLinha.UpdateBatch();

      if (qryItemMarca.State <> dsInactive) then
          qryItemMarca.UpdateBatch();

      if (qryItemGrupoProduto.State <> dsInactive) then
          qryItemGrupoProduto.UpdateBatch();

      if (qryLojaCampanha.State <> dsInactive) then
          qryLojaCampanha.UpdateBatch();

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  ShowMessage('Campanha gravada com sucesso!' +#13#13+ 'Esta campanha s� entrar� em vigor ap�s a ativa��o.') ;

  btCancelar.Click;

end;

procedure TfrmCampanhaPromoc.btIncluirClick(Sender: TObject);
begin
  inherited;

  {-- adiciona todos os produtos da campanha na tabela tempor�ria --}
  qryPreparaTemp.Close;
  qryPreparaTemp.Open;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMasternCdCampanhaPromoc.Value ;
  qryPopulaTemp.ExecSQL;

  qryProduto.Close;
  qryProduto.Open;

  {--}

  PosicionaQuery(qryItemDepartamento, '0') ;
  PosicionaQuery(qryItemLinha, '0') ;
  PosicionaQuery(qryItemMarca, '0') ;
  PosicionaQuery(qryItemGrupoProduto, '0') ;
  PosicionaQuery(qryLojaCampanha, '0') ;

  DBEdit3.SetFocus ;
  
end;

procedure TfrmCampanhaPromoc.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if (Key <> VK_F4) then
      exit ;

  if (qryItemDepartamento.State = dsBrowse) then
       qryItemDepartamento.Edit ;

  if (qryItemDepartamento.State = dsInsert) or (qryItemDepartamento.State = dsEdit) then
  begin

      nPK := frmLookup_Padrao.ExecutaConsulta(45);

      If (nPK > 0) then
      begin
          qryItemDepartamentonCdDepartamento.Value := nPK ;
          PosicionaQuery(qryDepartamento, qryItemDepartamentonCdDepartamento.asString) ;
          qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

          // consulta de categoria
          nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
          If (nPK > 0) then
          begin
              qryItemDepartamentonCdCategoria.Value := nPK ;
              PosicionaQuery(qryCategoria, qryItemDepartamentonCdCategoria.asString) ;
              qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

              // consulta de subcategoria
              nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
              If (nPK > 0) then
              begin
                  qryItemDepartamentonCdSubCategoria.Value := nPK ;
                  PosicionaQuery(qrySubCategoria,qryItemDepartamentonCdSubCategoria.asString) ;

                  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

                  // Consulta de segmento
                  nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                  If (nPK > 0) then
                  begin
                      qryItemDepartamentonCdSegmento.Value := nPK ;
                      PosicionaQuery(qrySegmento,qryItemDepartamentonCdSegmento.AsString) ;
                  end ;

              end ;

          end ;

      end ;
      
  end ;

end;

procedure TfrmCampanhaPromoc.qryItemDepartamentoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItemDepartamentonCdDepartamento.Value > 0) and (qryItemDepartamentocNmDepartamento.Value = '') then
  begin
      PosicionaQuery(qryDepartamento, qryItemDepartamentonCdDepartamento.AsString) ;

      if not qryDepartamento.eof then
          qryItemDepartamentocNmDepartamento.Value := qryDepartamentocNmDepartamento.Value ;
  end ;

  if (qryItemDepartamentonCdCategoria.Value > 0) and (qryItemDepartamentocNmCategoria.Value = '') then
  begin
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryItemDepartamentonCdDepartamento.Value ;
      PosicionaQuery(qryCategoria, qryItemDepartamentonCdCategoria.AsString) ;

      if not qryCategoria.eof then
          qryItemDepartamentocNmCategoria.Value := qryCategoriacNmCategoria.Value ;
  end ;

  if (qryItemDepartamentonCdSubCategoria.Value > 0) and (qryItemDepartamentocNmSubCategoria.Value = '') then
  begin
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryItemDepartamentonCdCategoria.Value ;
      PosicionaQuery(qrySubCategoria, qryItemDepartamentonCdSubCategoria.AsString) ;

      if not qrySubCategoria.eof then
          qryItemDepartamentocNmSubCategoria.Value := qrySubCategoriacNmSubCategoria.Value ;
  end ;

  if (qryItemDepartamentonCdSegmento.Value > 0) and (qryItemDepartamentocNmSegmento.Value = '') then
  begin
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qryItemDepartamentonCdSubCategoria.Value ;
      PosicionaQuery(qrySegmento, qryItemDepartamentonCdSegmento.AsString) ;

      if not qrySegmento.eof then
          qryItemDepartamentocNmSegmento.Value := qrySegmentocNmSegmento.Value ;
  end ;

end;

procedure TfrmCampanhaPromoc.qryItemDepartamentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryItemDepartamentocNmDepartamento.Value = '') then
  begin
      MensagemAlerta('Selecione o departamento.') ;
      abort ;
  end ;

  if (qryItemDepartamentocNmCategoria.Value = '') and (qryItemDepartamentonCdCategoria.Value > 0) then
  begin
      MensagemAlerta('Selecione a categoria.') ;
      abort ;
  end ;

  if (qryItemDepartamentocNmSubCategoria.Value = '') and (qryItemDepartamentonCdSubCategoria.Value > 0) then
  begin
      MensagemAlerta('Selecione a Sub categoria.') ;
      abort ;
  end ;

  if (qryItemDepartamentocNmSegmento.Value = '') and (qryItemDepartamentonCdSegmento.Value > 0) then
  begin
      MensagemAlerta('Selecione o segmento.') ;
      abort ;
  end ;

  if (qryItemDepartamentonValor.Value <= 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      abort ;
  end ;

  inherited;

  qryItemDepartamentonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
  qryItemDepartamentocFlgTipoValor.Value     := 'D' ;

end;

procedure TfrmCampanhaPromoc.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemLinha.State = dsBrowse) then
             qryItemLinha.Edit ;

        if (qryItemLinha.State = dsInsert) or (qryItemLinha.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(50);

            If (nPK > 0) then
            begin
                qryItemLinhanCdLinha.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc.qryItemLinhaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemLinhanCdLinha.Value > 0) and (qryItemLinhacNmLinha.Value = '') then
  begin
      PosicionaQuery(qryLinha, qryItemLinhanCdLinha.AsString) ;

      if not qryLinha.eof then
          qryItemLinhacNmLinha.Value := qryLinhacNmLinha.Value ;
  end ;

end;

procedure TfrmCampanhaPromoc.qryItemLinhaBeforePost(DataSet: TDataSet);
begin

  if (qryItemLinhacNmLinha.Value = '') then
  begin
      MensagemAlerta('Selecione a linha.') ;
      abort ;
  end ;

  if (qryItemLinhanValor.Value <= 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      abort ;
  end ;

  inherited;

  qryItemLinhanCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
  qryItemlinhacFlgTipoValor.Value     := 'D' ;

end;

procedure TfrmCampanhaPromoc.DBGridEh4KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemMarca.State = dsBrowse) then
             qryItemMarca.Edit ;

        if (qryItemMarca.State = dsInsert) or (qryItemMarca.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(49);

            If (nPK > 0) then
            begin
                qryItemMarcanCdMarca.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmCampanhaPromoc.qryItemMarcaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemMarcanCdMarca.Value > 0) and (qryItemMarcacNmMarca.Value = '') then
  begin
      PosicionaQuery(qryMarca, qryItemMarcanCdMarca.AsString) ;

      if not qryMarca.eof then
          qryItemMarcacNmMarca.Value := qryMarcacNmMarca.Value ;
  end ;

end;

procedure TfrmCampanhaPromoc.qryItemMarcaBeforePost(DataSet: TDataSet);
begin

  if (qryItemMarcacNmMarca.Value = '') then
  begin
      MensagemAlerta('Selecione a marca.') ;
      abort ;
  end ;

  if (qryItemMarcanValor.Value <= 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      abort ;
  end ;

  inherited;

  qryItemMarcanCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
  qryItemMarcacFlgTipoValor.Value     := 'D' ;

end;

procedure TfrmCampanhaPromoc.DBGridEh5KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemGrupoProduto.State = dsBrowse) then
             qryItemGrupoProduto.Edit ;

        if (qryItemGrupoProduto.State = dsInsert) or (qryItemGrupoProduto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryItemGrupoProdutonCdGrupoProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc.qryItemGrupoProdutoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItemGrupoProdutonCdGrupoProduto.Value > 0) and (qryItemGrupoProdutocNmGrupoProduto.Value = '') then
  begin
      PosicionaQuery(qryGrupoProduto, qryItemGrupoProdutonCdGrupoProduto.AsString) ;

      if not qryGrupoProduto.eof then
          qryItemGrupoProdutocNmGrupoProduto.Value := qryGrupoProdutocNmGrupoProduto.Value ;
  end ;

end;

procedure TfrmCampanhaPromoc.qryItemGrupoProdutoBeforePost(
  DataSet: TDataSet);
begin
  if (qryItemGrupoProdutocNmGrupoProduto.Value = '') then
  begin
      MensagemAlerta('Selecione o grupo de produtos.') ;
      abort ;
  end ;

  if (qryItemGrupoProdutonValor.Value <= 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      abort ;
  end ;

  inherited;

  qryItemGrupoProdutonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
  qryItemGrupoProdutocFlgTipoValor.Value     := 'D' ;

end;

procedure TfrmCampanhaPromoc.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  
  qryProduto.Close ;
  qryItemDepartamento.Close ;
  qryItemLinha.Close ;
  qryItemMarca.Close ;
  qryItemGrupoProduto.Close ;
  qryLojaCampanha.Close ;
  qryUsuario.Close ;
  qryTipoTabPreco.Close ;
  qryAuxProduto.Close ;
  qryHistInclusaoProduto.Close;

end;

procedure TfrmCampanhaPromoc.qryLojaCampanhaBeforePost(DataSet: TDataSet);
begin

  if (qryLojaCampanhacNmLoja.Value = '') then
  begin
      MensagemAlerta('Selecione a loja.') ;
      abort ;
  end ;

  inherited;

  qryLojaCampanhanCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;

  if (qryLojaCampanha.State = dsInsert) then
      qryLojaCampanhanCdLojaCampanhaPromoc.Value := frmMenu.fnProximoCodigo('LOJACAMPANHAPROMOC');

end;

procedure TfrmCampanhaPromoc.DBGridEh6KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryLojaCampanha.State = dsBrowse) then
             qryLojaCampanha.Edit ;

        if (qryLojaCampanha.State = dsInsert) or (qryLojaCampanha.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = @@Empresa');

            If (nPK > 0) then
            begin
                qryLojaCampanhanCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc.qryLojaCampanhaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryLojaCampanhanCdLoja.Value > 0) and (qryLojaCampanhacNmLoja.Value = '') then
  begin
      PosicionaQuery(qryLoja, qryLojaCampanhanCdLoja.AsString) ;

      if not qryLoja.eof then
          qryLojaCampanhacNmLoja.Value := qryLojacNmLoja.Value ;
  end ;

end;

procedure TfrmCampanhaPromoc.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryTipoTabPreco.Close ;
  PosicionaQuery(qryTipoTabPreco, DBEdit11.Text) ;

end;

procedure TfrmCampanhaPromoc.DBEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(192);

            If (nPK > 0) then
            begin
                qryMasternCdTipoTabPreco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc.cxButton2Click(Sender: TObject);
var
  objForm : TfrmCampanhaPromoc_IncluiProduto;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma campanha promocional ativa.') ;
      exit ;
  end ;

  objForm := TfrmCampanhaPromoc_IncluiProduto.Create(nil);
  try
      try
          {--visualiza o relat�rio--}

          objForm.ShowModal ;

      except
          MensagemErro('Erro na cria��o do formulario');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

  qryProdutoSel.Close ;
  qryProdutoSel.Open ;

  (*while not qryProdutoSel.eof do
  begin

      if not qryProduto.Locate('nCdProduto',qryProdutoSelnCdProduto.Value,[loCaseInsensitive,loPartialKey]) then
      begin
          qryProduto.Insert;
          qryProdutonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value ;
          qryProdutocFlgTipoProduto.Value   := 'I' ;
          qryProdutonCdProduto.Value        := qryProdutoSelnCdProduto.Value ;
          qryProdutonQtde.Value             := 0 ;
          qryProdutocFlgTipoValor.Value     := 'V' ;
          qryProdutonValVendaAtual.Value    := qryProdutoSelnValVenda.Value ;
          qryProdutonValor.Value            := qryProdutoSelnValVenda.Value ;
          qryProduto.Post;
      end ;

      qryProdutoSel.Next;
  end ;  *)

  frmMenu.Connection.BeginTrans ;

  try
      qryIncluirProdutos.Close;
      qryIncluirProdutos.Parameters.ParamByName('nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.Value;
      qryIncluirProdutos.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryProduto.Close ;
  qryProduto.Open ;
  qryProduto.Last ;

  qryHistInclusaoProduto.Close;
  qryHistInclusaoProduto.Open;
  qryHistInclusaoProduto.Last;



end;

procedure TfrmCampanhaPromoc.cxButton1Click(Sender: TObject);
var
  objCons    : TfrmCampanhaPromoc_AnaliseVendas;
  objConsERP : TfrmCampanhaPromoc_AnaliseVendasERP;

begin

  if qryMaster.Active and (not qryMaster.eof) then
  begin

      if (qryMastercFlgAtivada.Value = 0) then
      begin
          MensagemErro('Esta campanha n�o foi ativada.') ;
          abort ;
      end ;

      objCons    := TfrmCampanhaPromoc_AnaliseVendas.Create(nil) ;
      objConsERP := TfrmCampanhaPromoc_AnaliseVendasERP.Create(nil) ;

      try
          try
              if (frmMenu.LeParametro('VAREJO') = 'S') then
              begin
                  objCons.qryVendas.Close;
                  objCons.qryVendas.Parameters.ParamByName('nPk').Value := qryMasternCdCampanhaPromoc.Value ;
                  objCons.qryVendas.Open;

                  objCons.cxGridDBTableView1.ViewData.Expand(True) ;
                  objCons.ShowModal ;
              end
              else
              begin
                  objConsERP.qryVendas.Close;
                  objConsERP.qryVendas.Parameters.ParamByName('nPk').Value := qryMasternCdCampanhaPromoc.Value ;
                  objConsERP.qryVendas.Open;

                  objConsERP.cxGridDBTableView1.ViewData.Expand(True) ;
                  objConsERP.ShowModal ;
              end ;

          except
              MensagemErro('Erro na cria��o da Consulta');
              raise;
          end;
      finally
          FreeAndNil(objCons);
          FreeAndNil(objConsERP);
      end;

  end ;

end;

procedure TfrmCampanhaPromoc.ToolButton5Click(Sender: TObject);
var
    i : integer ;
begin
  inherited;


end;

procedure TfrmCampanhaPromoc.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      abort ;

  if (qryMasternCdCampanhaPromoc.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

end;

procedure TfrmCampanhaPromoc.cxButton3Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma campanha promocional ativa.') ;
      abort ;
  end ;

  if (qryProduto.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto para exclus�o.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a exclus�o dos produtos da campanha ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      abort ;

  qryProduto.First;

  (*while not qryProduto.eof do
  begin
       qryProduto.Delete;
       qryProduto.First;
  end ; *)

  qryAux.Close;
  qryAux.SQL.Clear;

  qryAux.SQL.Add('DELETE FROM #Temp_ItemCampanhaPromoc WHERE nCdCampanhaPromoc = ' + qryMasternCdCampanhaPromoc.AsString);
  qryAux.ExecSQL;

  qryProduto.Close;
  qryProduto.Open;

  ShowMessage('Produtos exclu�dos com sucesso.') ;

end;

procedure TfrmCampanhaPromoc.cxButton4Click(Sender: TObject);
var
    cPercDesconto : string ;
    cValVenda     : string;
begin
  inherited;
  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma campanha promocional ativa.') ;
      abort ;
  end ;

  if (qryProduto.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto incluso na promo��o.') ;
      abort ;
  end ;

  cPercDesconto := '' ;
  
  try
      if not InputQuery('Campanha Promocional','Percentual Desconto',cPercDesconto) then
          abort;

      cPercDesconto := StringReplace(cPercDesconto, '.', ',',[rfReplaceAll, rfIgnoreCase]);
      StrToFloat(cPercDesconto) ;
  except
      MensagemErro('Percentual de Desconto Inv�lido.') ;
      abort ;
  end ;

  if (strToFloat(cPercDesconto) < 0) then
  begin
      MensagemErro('Percentual de Desconto Inv�lido.') ;
      abort ;
  end ;

  if (strToFloat(cPercDesconto) = 0) then
      abort ;

  if (MessageDlg('Confirma a aplica��o do desconto nos produtos da campanha ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      abort ;

  qryProduto.First;

  SetRoundMode(rmDown) ;

 (* while not qryProduto.eof do
  begin
      qryProduto.Edit;
      qryProdutonValor.Value := 0 ;
      qryProdutonValor.Value := frmMenu.arredondaVarejo(qryProdutonValVendaAtual.Value * ((100-StrToFloat(cPercDesconto))/100)) ;
      qryProduto.Post;

      qryProduto.Next;
  end ;  *)

  qryAux.Close;
  qryAux.SQL.Clear;

  cValVenda := FloatToStr(((100-StrToFloat(cPercDesconto))/100) );
  cValVenda := StringReplace(cValVenda,',','.',[rfReplaceAll]);

  qryAux.SQL.Add('UPDATE #Temp_ItemCampanhaPromoc');
  qryAux.SQL.Add('   SET nValor = dbo.fn_ArredondaVarejo(nValVendaAtual) * ' + cValVenda);
  qryAux.SQL.Add('   WHERE nCdCampanhaPromoc = ' + qryMasternCdCampanhaPromoc.AsString);
  qryAux.ExecSQL;

  qryProduto.Close;
  qryProduto.Open;

  qryProduto.First ;

  ShowMessage('Produtos atualizados com sucesso.') ;

end;

procedure TfrmCampanhaPromoc.edtFiltroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

var
  Filtro : String;
begin
  inherited;

  if (Key = VK_Return) then
  begin

      if not (qryProduto.Active) then
          exit;

      if (Trim(edtFiltro.Text) <> '') then
          qryAuxProduto.Filter := 'cNmProduto LIKE ' + #39 + '%' + edtFiltro.Text + '%' + #39
      else qryAuxProduto.Filter := '';

      {-- gera o filtro --}

      qryAuxProduto.First;

      Filtro := '';

      while not qryAuxProduto.Eof do
      begin

          Filtro := Filtro + 'nCdProduto = ' + qryAuxProdutonCdProduto.AsString + ' OR ';

          qryAuxProduto.Next;
      end;

      if (qryAuxProduto.RecordCount > 0) then
          Filtro := Copy(Filtro,0,Length(Filtro) - 4)
      else Filtro := 'nCdProduto = 0'; {-- simula nenhum registro encontrado--}

      {-- passa o filtro gerado para a qryProduto --}

      qryProduto.Filtered := False;

      if (Trim(edtFiltro.Text) <> '') then
          qryProduto.Filter := Filtro
      else qryProduto.Filter := '';

      qryProduto.Filtered := True;

  end;
end;

procedure TfrmCampanhaPromoc.edtFiltroHisKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Filtro : String;
begin
  inherited;

  if (Key = VK_Return) then
  begin

      if not (qryProduto.Active) then
          exit;

      if (Trim(edtFiltroHis.Text) <> '') then
          qryHistInclusaoProduto.Filter := 'cNmProduto LIKE ' + #39 + '%' + edtFiltroHis.Text + '%' + #39
      else qryHistInclusaoProduto.Filter := '';

      {-- gera o filtro --}

      qryHistInclusaoProduto.First;

      Filtro := '';

      while not qryHistInclusaoProduto.Eof do
      begin

          Filtro := Filtro + 'nCdProduto = ' + qryHistInclusaoProdutonCdProduto.AsString + ' OR ';

          qryHistInclusaoProduto.Next;
      end;

      if (qryHistInclusaoProduto.RecordCount > 0) then
          Filtro := Copy(Filtro,0,Length(Filtro) - 4)
      else Filtro := 'nCdProduto = 0'; {-- simula nenhum registro encontrado--}

      {-- passa o filtro gerado para a qryProduto --}

      qryHistInclusaoProduto.Filtered := False;

      if (Trim(edtFiltroHis.Text) <> '') then
          qryHistInclusaoProduto.Filter := Filtro
      else qryHistInclusaoProduto.Filter := '';

      qryHistInclusaoProduto.Filtered := True;

    end;

end;

procedure TfrmCampanhaPromoc.checkCampanhaAtivaWebClick(Sender: TObject);
begin
  inherited;

  if (checkCampanhaAtivaWeb.Checked) then
  begin
      ativaDBEdit(DBEdit13);
      ativaDBEdit(DBEdit14);
  end
  else
  begin
      desativaDBEdit(DBEdit13);
      desativaDBEdit(DBEdit14);
  end;
end;

procedure TfrmCampanhaPromoc.btImpCampanhaClick(Sender: TObject);
var
  objRel : TrptCampanhaPromoc_view;
begin
  inherited;

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Selecione uma campanha promocional.') ;
      exit ;
  end ;

  if (qryProduto.State = dsInactive) or (qryProduto.RecordCount = 0) then
  begin
      MensagemAlerta('Selecione ao menos um produto para a campanha.') ;
      exit ;
  end ;

  if (cVarejo = 'S') then
  begin
      if (qryLojaCampanha.State = dsInactive) or (qryLojaCampanha.RecordCount = 0) then
      begin
          MensagemAlerta('Selecione ao menos uma loja para a campanha.') ;
          exit ;
      end ;
  end ;

  objRel := TrptCampanhaPromoc_view.Create(nil) ;
  try
      try
          PosicionaQuery(objRel.qryCampanhaPromoc, qryMasternCdCampanhaPromoc.asString) ;

          {--envia filtros utilizados--}

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmCampanhaPromoc.btExportaCampanhaClick(Sender: TObject);
var
 objRel : TrptCampanhaPromoc_view;
 iLinha : integer;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma campanha promocional ativa.');
      Abort;
  end ;

  if ((qryProduto.State = dsInactive) or (qryProduto.RecordCount = 0)) then
  begin
      MensagemAlerta('Selecione ao menos um produto para a campanha.');
      Exit;
  end;

  try
      try
          objRel := TrptCampanhaPromoc_view.Create(nil);

          frmMenu.mensagemUsuario('Consultando Dados...');

          PosicionaQuery(objRel.qryCampanhaPromoc, qryMasternCdCampanhaPromoc.asString) ;

          {-- exibe relat�rio --}
          frmMenu.mensagemUsuario('Exportando Planilha...');

          ER2Excel.Celula['A1'].Mesclar('E1');
          ER2Excel.Celula['A2'].Mesclar('E2');
          ER2Excel.Celula['A3'].Mesclar('E3');
          ER2Excel.Celula['A4'].Mesclar('E4');
          ER2Excel.Celula['A5'].Mesclar('E5');
          ER2Excel.Celula['B6'].Mesclar('I6');
          ER2Excel.Celula['J6'].Mesclar('K6');

          ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A1'].Range('M1');
          ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A2'].Range('M2');
          ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A3'].Range('M3');
          ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A4'].Range('M4');
          ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A5'].Range('M5');
          ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
          ER2Excel.Celula['A6'].Range('M6');

          { -- inseri informa��es do cabe�alho -- }
          ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
          ER2Excel.Celula['A2'].Text := 'Campanha Promocional.';
          ER2Excel.Celula['A3'].Text := 'C�digo Campanha: '   + DBEdit1.Text;
          ER2Excel.Celula['G3'].Text := 'Validade Campanha: ' + DBEdit5.Text + ' a ' + DBEdit6.Text;
          ER2Excel.Celula['G4'].Text := 'Validade Campanha na WEB: ' + DBEdit13.Text + ' a ' + DBEdit14.Text;
          ER2Excel.Celula['A4'].Text := 'Nome da Campanha: '  + DBEdit4.Text;
          ER2Excel.Celula['K3'].Text := 'Situa��o Campanha: ' + objRel.qryCampanhaPromoccFlgSituacao.Value;
          ER2Excel.Celula['A5'].Text := 'Descri��o: ' + objRel.qryCampanhaPromoccDescricaoCampanha.Value;
          ER2Excel.Celula['A6'].Text := 'C�d ';
          ER2Excel.Celula['B6'].Text := 'Descri��o Produto ';
          ER2Excel.Celula['J6'].Text := 'Quantidade Minima ';
          ER2Excel.Celula['L6'].Text := 'Valor';
          ER2Excel.Celula['M6'].Text := 'Tipo Valor';

          iLinha := 7;

          ER2Excel.Celula['A7'].Congelar('A7');

          objRel.qryCampanhaPromoc.First;

          while (not objRel.qryCampanhaPromoc.Eof) do
          begin
              ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryCampanhaPromocnCdProduto.Value;
              ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryCampanhaPromoccNmProduto.Value;
              ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.qryCampanhaPromocnQtde.Value;
              ER2Excel.Celula['J' + IntToStr(iLinha)].Mascara := '#.##0,00' ;
              ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.qryCampanhaPromocnValor.Value;
              ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.qryCampanhaPromoccFlgTipoValor.Value;

              objRel.qryCampanhaPromoc.Next;

              Inc(iLinha);
          end;

          { -- exporta planilha e limpa result do componente -- }
          ER2Excel.ExportXLS;
          ER2Excel.CleanupInstance;

          frmMenu.mensagemUsuario('');
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmCampanhaPromoc.btAtivaInativaCampanhaWebClick(
  Sender: TObject);
var
  cFlgStatusWeb    : Integer;
  cDecLogAuditoria : String;
begin
  inherited;

  case (qryMastercFlgAtivadaWeb.Value) of
      0 : begin
          case MessageDlg('Confirma a ativa��o manual da campanha promocional na web ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo  : Exit;
          end;

          cFlgStatusWeb    := 1;
          cDecLogAuditoria := 'Campanha promocional ativada na web manualmente pelo usu�rio ' + frmMenu.cNmUsuarioLogado;
      end;
      1 : begin
          case MessageDlg('Confirma a desativa��o manual da campanha promocional na web ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo  : Exit;
          end;

          cFlgStatusWeb    := 0;
          cDecLogAuditoria := 'Campanha promocional desativada na web manualmente pelo usu�rio ' + frmMenu.cNmUsuarioLogado;
      end;
  end;

  { -- se campanha esta sendo ativada valida vig�ncia -- }
  if (cFlgStatusWeb = 1) then
  begin
      ativaDBEdit(DBedit13);
      ativaDBEdit(DBedit14);

      if (qryMasterdDtValidadeIniWeb.AsString = '') or (qryMasterdDtValidadeFimWeb.AsString = '') then
      begin
          MensagemAlerta('Informe o per�odo de validade desta campanha na web.') ;
          DBedit13.SetFocus;
          Abort;
      end;

      if (qryMasterdDtValidadeIniWeb.Value > qryMasterdDtValidadeFimWeb.Value) then
      begin
          MensagemAlerta('Per�odo de validade web inv�lido.') ;
          DBedit13.SetFocus;
          abort ;
      end ;
  end
  else
  begin
      desativaDBEdit(DBedit13);
      desativaDBEdit(DBedit14);
  end;

  try
      frmMenu.Connection.BeginTrans;

      frmMenu.LogAuditoria(nCdTabelaSistema, 2, qryMasternCdCampanhaPromoc.Value, cDecLogAuditoria);

      qryMaster.Edit;
      qryMastercFlgAtivadaWeb.Value := cFlgStatusWeb;
      qryMaster.Post;

      SP_ATUALIZA_PRECO_PROMOCIONAL_WEB.Close;
      SP_ATUALIZA_PRECO_PROMOCIONAL_WEB.Parameters.ParamByName('@nCdCampanhaPromoc').Value := qryMasternCdCampanhaPromoc.Value;
      SP_ATUALIZA_PRECO_PROMOCIONAL_WEB.Parameters.ParamByName('@cFlgCampanhaAuto').Value  := 0;
      SP_ATUALIZA_PRECO_PROMOCIONAL_WEB.ExecProc;

      frmMenu.Connection.CommitTrans;
      
      if (checkCampanhaAtivaWeb.Checked) then
          ShowMessage('Campanha promocional ativada na web.')
      else
          ShowMessage('Campanha promocional desativada na web.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmCampanhaPromoc.cxButton8Click(Sender: TObject);
var
  Lista : TStringList;
  Texto : String;
  I,C : Integer;
function MV : String;
var
  Valor : String;
begin
  Valor := '';
  inc(I);
  While Texto[I] > '*' do
    begin
      If Texto[I]= ';' then
        break;
      Valor := Valor + Texto[I];
      inc(I);
    end;
    result := Valor;
end;

begin
  inherited;
  try
    if OpenDialog1.Execute then
      begin
        Screen.Cursor := crHourGlass;
        qryProduto.Open;
        Lista := TStringList.Create;
        Lista.LoadFromFile(OpenDialog1.FileName);
        for C := 1 to Lista.Count-1 do
          begin

            Texto := Lista[C];

            With qryProduto do begin
              Insert;
              I := 0;
              qryProdutonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value;
              qryProdutonCdCampanhaPromoc.Value := qryMasternCdCampanhaPromoc.Value;
              qryProdutocFlgTipoProduto.Value   := 'I' ;
              qryProdutonCdProduto.Value        := StrToInt(MV);
              qryProdutonQtde.Value             := 0 ;
              qryProdutocFlgTipoValor.Value     := 'V' ;
              qryProdutonValVendaAtual.Value    := StrToFloat(MV);
              qryProdutonValor.Value            := StrToFloat(MV);
              Post;
            end;

          end;
        Lista.Free;
        Screen.Cursor := crDefault;
        ShowMessage('importa��o conclu�da.');
    end;
except
  ShowMessage('N�o foi poss�vel concluir a importa��o.');
end;
end;

procedure TfrmCampanhaPromoc.dsProdutoDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  Edit2.Text := IntToStr(qryProduto.RecordCount);
end;

initialization
    RegisterClass(TfrmCampanhaPromoc) ;

end.
