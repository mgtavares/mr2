unit rClientesVarejoSemCompraPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, DBCtrls, ER2Excel;

type
  TrptClientesVarejoSemCompraPeriodo = class(TfrmRelatorio_Padrao)
    Label3: TLabel;
    mskdDtInicial: TMaskEdit;
    Label6: TLabel;
    mskdDtFinal: TMaskEdit;
    mskQtdDias: TMaskEdit;
    Label2: TLabel;
    Label4: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    er2LkpLoja: TER2LookupMaskEdit;
    rgModeloImp: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
    procedure mskQtdDiasExit(Sender: TObject);
    procedure qryLojaBeforeOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptClientesVarejoSemCompraPeriodo: TrptClientesVarejoSemCompraPeriodo;

implementation

uses
  fMenu, rClientesVarejoSemCompraPeriodo_View;

{$R *.dfm}

procedure TrptClientesVarejoSemCompraPeriodo.ToolButton1Click(
  Sender: TObject);
var
  objRel      : TrptClientesVarejoSemCompraPeriodo_View;
  //dDtCalc     : String;
  //cNmSituacao : String;
  cNmLoja     : String;
//  nCdSituacao : Integer;
  iLinha      : Integer;
begin
  inherited;

  objRel := TrptClientesVarejoSemCompraPeriodo_View.Create(nil);

  { -- valida consist�ncias -- }
  if (Trim(er2LkpLoja.Text) = '') then
      cNmLoja := 'Todas'
  else
      cNmLoja := DBEdit1.Text;

  if (frmMenu.ConvInteiro(mskQtdDias.Text) < 0) then
  begin
      MensagemAlerta('Quantidade de dias n�o pode ser negativo.');
      mskQtdDias.SetFocus;
      Abort;
  end;
 
  if (frmMenu.ConvData(mskdDtInicial.Text)) > (frmMenu.ConvData(mskdDtFinal.Text)) then
  begin
      MensagemAlerta('Data inicial maior que data final.');
      mskdDtInicial.SetFocus;
      Abort;
  end;

  try
      try
          objRel.qryGeraRelatorio.Close;
          objRel.qryGeraRelatorio.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(mskdDtInicial.Text);
          objRel.qryGeraRelatorio.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(mskdDtFinal.Text);
          objRel.qryGeraRelatorio.Parameters.ParamByName('iQtdDias').Value   := frmMenu.ConvInteiro(mskQtdDias.Text);
          objRel.qryGeraRelatorio.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          objRel.qryGeraRelatorio.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(er2LkpLoja.Text);

          objRel.qryGeraRelatorio.Open;

          case (rgModeloImp.ItemIndex) of
              0 : begin
                      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
                      objRel.lblFiltro1.Caption := '';
                      objRel.lblFiltro1.Caption := 'Loja de Cadastro: ' + cNmLoja + ' Per�odo: ' + mskdDtInicial.Text + ' at� ' + mskdDtFinal.Text + #13 + 'Qtd. Dias: ' + mskQtdDias.Text;

                      objRel.QuickRep1.PreviewModal;
                  end;
                  
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      { -- formata c�lulas -- }
                      ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A1'].Range('Q1');
                      ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A2'].Range('Q2');
                      ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A3'].Range('Q3');
                      ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A4'].Range('Q4');
                      ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A5'].Range('Q5');
                      ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A6'].Range('Q6');

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel.Celula['A2'].Text := 'Rel. Clientes Sem Compra Per�odo';
                      ER2Excel.Celula['A4'].Text := 'Per�odo de Cadastro: ' + mskdDtInicial.Text + ' at� ' + mskdDtFinal.Text;
                      ER2Excel.Celula['E4'].Text := 'Qtd. Dias: ' + mskQtdDias.Text;
                      ER2Excel.Celula['G4'].Text := 'Loja de Cadastro: ' + cNmLoja;
                      ER2Excel.Celula['A6'].Text := 'Loja de Cadastro';
                      ER2Excel.Celula['E6'].Text := 'Cliente';
                      ER2Excel.Celula['L6'].Text := 'CPF';
                      ER2Excel.Celula['N6'].Text := 'Dt. Cadastro';
                      ER2Excel.Celula['P6'].Text := 'Dt. �lt Neg�cio';

                      ER2Excel.Celula['A1'].Congelar('A7');

                      iLinha := 7;

                      objRel.qryGeraRelatorio.First;

                      while (not objRel.qryGeraRelatorio.Eof) do
                      begin
                          ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryGeraRelatorionCdLoja.Value;
                          ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryGeraRelatoriocNmLoja.Value;
                          ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.qryGeraRelatorionCdTerceiro.Value;
                          ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.qryGeraRelatoriocNmTerceiro.Value;
                          ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.qryGeraRelatoriocCNPJCPF.Value;
                          ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.qryGeraRelatoriodDtCadastro.Value;
                          ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.qryGeraRelatoriodDtUltNegocio.Value;

                          Inc(iLinha);

                          objRel.qryGeraRelatorio.Next;
                      end;

                      { -- exporta planilha e limpa result do componente -- }
                      ER2Excel.ExportXLS;
                      ER2Excel.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;
      except
          MensagemErro('Erro na cria��o do relat�rio.');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptClientesVarejoSemCompraPeriodo.mskQtdDiasExit(Sender: TObject);
begin
  inherited;

  if (Trim(mskQtdDias.Text) = '') then
      mskQtdDias.Text := '0';
end;

procedure TrptClientesVarejoSemCompraPeriodo.qryLojaBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TrptClientesVarejoSemCompraPeriodo.FormActivate(Sender: TObject);
begin
  inherited;

  er2LkpLoja.SetFocus;
end;

initialization
  RegisterClass(TrptClientesVarejoSemCompraPeriodo);

end.
