inherited frmConciliaBancoManual_TituloMovimentado: TfrmConciliaBancoManual_TituloMovimentado
  Left = 50
  Width = 1012
  Caption = 'T'#237'tulos Movimentados pelo Lan'#231'amento'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 996
  end
  inherited ToolBar1: TToolBar
    Width = 996
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 996
    Height = 435
    Align = alClient
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsTitulos
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'nCdTitulo'
        Width = 46
      end
      object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'nCdEmpresa'
        Width = 63
      end
      object cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'nCdLojaTit'
        Width = 43
      end
      object cxGrid1DBTableView1nCdSP: TcxGridDBColumn
        Caption = 'N'#250'm. SP'
        DataBinding.FieldName = 'nCdSP'
      end
      object cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn
        Caption = 'N'#250'm. Receb.'
        DataBinding.FieldName = 'nCdRecebimento'
        Width = 80
      end
      object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
        Caption = 'N'#250'm. T'#237'tulo'
        DataBinding.FieldName = 'cNrTit'
        Width = 77
      end
      object cxGrid1DBTableView1iParcela: TcxGridDBColumn
        Caption = 'Parc.'
        DataBinding.FieldName = 'iParcela'
        Width = 40
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        Caption = 'N'#250'm. NF'
        DataBinding.FieldName = 'cNrNF'
        Width = 63
      end
      object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
        Caption = 'C'#243'd. Terceiro'
        DataBinding.FieldName = 'nCdTerceiro'
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 116
      end
      object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
        Caption = 'Dt. Emiss'#227'o'
        DataBinding.FieldName = 'dDtEmissao'
        Width = 81
      end
      object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
        Caption = 'Dt. Vencto'
        DataBinding.FieldName = 'dDtVenc'
        Width = 77
      end
      object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
        Caption = 'Esp'#233'cie'
        DataBinding.FieldName = 'cNmEspTit'
        Width = 127
      end
      object cxGrid1DBTableView1nValMov: TcxGridDBColumn
        Caption = 'Valor do Lan'#231'amento'
        DataBinding.FieldName = 'nValMov'
      end
      object cxGrid1DBTableView1nValTit: TcxGridDBColumn
        Caption = 'Valor T'#237'tulo'
        DataBinding.FieldName = 'nValTit'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      ''
      'Set @nCdLanctoFin = :nPK'
      ''
      'SELECT Titulo.nCdTitulo'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdEmpresa,3) as nCdEmpresa'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) as nCdLojaTit'
      '      ,Titulo.nCdSP'
      '      ,Titulo.nCdRecebimento'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Titulo.cNrNF'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,MTitulo.nValMov'
      '      ,EspTit.cNmEspTit'
      '  FROM MTitulo '
      
        '       INNER JOIN Titulo   ON Titulo.nCdTitulo     = MTitulo.nCd' +
        'Titulo'
      
        '       INNER JOIN EspTit   ON EspTit.nCdEspTit     = Titulo.nCdE' +
        'spTit'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE MTitulo.nCdLanctoFin = @nCdLanctoFin'
      'UNION ALL'
      'SELECT Titulo.nCdTitulo'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdEmpresa,3) as nCdEmpresa'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) as nCdLojaTit'
      '      ,Titulo.nCdSP'
      '      ,Titulo.nCdRecebimento'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Titulo.cNrNF'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,ABS(LanctoFin.nValLancto) as nValMov'
      '      ,EspTit.cNmEspTit'
      '  FROM LanctoFin'
      
        '       INNER JOIN Titulo   ON Titulo.nCdTitulo     = LanctoFin.n' +
        'CdTituloDA'
      
        '       INNER JOIN EspTit   ON EspTit.nCdEspTit     = Titulo.nCdE' +
        'spTit'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE LanctoFin.nCdLanctoFin = @nCdLanctoFin'
      ''
      ''
      '')
    Left = 232
    Top = 104
    object qryTitulosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulosnCdEmpresa: TStringField
      FieldName = 'nCdEmpresa'
      ReadOnly = True
      Size = 15
    end
    object qryTitulosnCdLojaTit: TStringField
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
    object qryTitulosnCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTitulosnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTituloscNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTituloscNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTituloscNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulosnValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValMov: TBCDField
      FieldName = 'nValMov'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloscNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 272
    Top = 104
  end
end
