�
 TRPTBORDEROCAIXA_VIEW 0�  TPF0TrptBorderoCaixa_viewrptBorderoCaixa_viewLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetqryTemp_Resumo_VendasFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1Left&Top&Width�Height[Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesTUUUUU��@������v�	@ BandTyperbTitle TQRLabelQRLabel1LeftTopWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU��@      P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Borderô de CaixaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblEmpresaLeftTopWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�TopWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@TUUUUU��	@UUUUUU��@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel3LeftpTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      `�	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Pág.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData2Left�TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel2LeftTop0WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@       �@     @�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionLojaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4Left@Top0Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@       �@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionConta CaixaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel5LeftTop@WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������@UUUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Período MovimentaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellblCaixaLeft�Top0Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������J�	@       �@UUUUUU)�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblCaixaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblLojaLeft8Top0Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@       �@UUUUUUi�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblLojaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblInicialLeft�Top@WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������J�	@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lblInicialColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblFinalLeft�Top@WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������	@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblFinalColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel25LeftTop@Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   UsuárioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblUsuarioLeft8Top@Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lblUsuarioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape1Left Top Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                    ������t�	@ 	Pen.WidthShape
qrsHorLine  TQRShapeQRShape2Left Top(Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          ��������@������t�	@ 	Pen.WidthShape
qrsHorLine   TQRSubDetailQRSubDetail1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ MasterOwnerDataSetqryTemp_Resumo_Vendas
FooterBandQRBand2PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText1Left(Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@                ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo_Vendas	DataFieldcNmTabTipoFormaPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2LeftTop Width@HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@          UUUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo_Vendas	DataField	nValPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText2Print
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@������v�	@ MasterQRSubDetail1ReprintOnNewPage TQRLabelQRLabel7LeftTopWidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionRESUMO VENDASColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2Left&Top� Width�Height0Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageLinkBandQRGroup1Size.Values       �@������v�	@ BandTyperbGroupFooter TQRLabelQRLabel8Left(TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblTotVendaLeft� TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������
�@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel9LeftTop WidthtHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUUU�@UUUUUUu�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionRECEBIMENTO DE PARCELASColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRSubDetailQRSubDetail2Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ MasterOwnerDataSetqryTemp_Resumo_Recebimento
FooterBandQRBand3PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText3LeftTop Width@HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@          UUUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo_Recebimento	DataField	nValPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText3Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText4Left(Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@                ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo_Recebimento	DataFieldcNmTabTipoFormaPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand3Left&TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRight
AfterPrintQRBand3AfterPrintAlignToBottomColorclWhiteForceNewColumnForceNewPageLinkBandQRGroup2Size.Values       �@������v�	@ BandTyperbGroupFooter TQRLabel	QRLabel10Left(TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblTotRecebLeft� TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������
�@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup2Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@������v�	@ MasterQRSubDetail2ReprintOnNewPage  TQRSubDetailQRSubDetail3Left&TopIWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ MasterOwnerDataSetqryTemp_Resumo_Lanctos
FooterBandQRBand4PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText5Left� Top WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�@          UUUUUU%�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataField	dDtLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText6Left� Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@                ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldcNmTipoLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText7LeftpTop WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������j�@          UUUUUU%�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldcNmTabTipoFormaPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8Left�Top WidthiHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@                �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldnCdContaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText9Left�Top Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataField
nValLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText9Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText15LefthTop WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@          ������R�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldcMotivoEstornoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText10LeftTop WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@          ��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldnCdLanctoFinFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText16LeftPTop WidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@          ��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Lanctos	DataFieldnCdLanctoFinPaiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup3Left&TopWidth�Height0Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@������v�	@ MasterQRSubDetail3ReprintOnNewPage TQRLabel	QRLabel11LeftTopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUUU�@������J�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption:   LANÇAMENTOS MANUAL                             ** EstornoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel12Left� Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU�@UUUUUUU�@     @�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionDATAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13Left� Top WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUu�@UUUUUUU�@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   TIPO LANÇAMENTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14LeftpTop Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@UUUUUUU�@������*�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionFORMA PAGTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel15Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionVALORColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel16Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCAIXAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel27LefthTop WidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������	@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionMOTIVO DO ESTORNOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel17LeftTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@������
�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionLanctoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel28LeftPTop Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Lancto PaiColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand4Left&TopYWidth�Height Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteEnabledForceNewColumnForceNewPageLinkBandQRGroup3Size.ValuesUUUUUUU�@������v�	@ BandTyperbGroupFooter TQRLabellblTotLanctoLeft TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18Left(TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRSubDetailQRSubDetail4Left&Top�Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ MasterOwnerDataSetqryTemp_Resumo_Caixa
FooterBandQRBand5PrintBeforePrintIfEmpty	 	TQRDBText
QRDBText11Left� Top WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@          ������R�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Caixa	DataField	nValTotalFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText11Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText12Left(Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@                ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Caixa	DataFieldcNmTabTipoFormaPagtoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText13LeftuTop Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@          ������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Caixa	DataFieldnValInformadoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText14Left�Top Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUW�	@          ������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo_Caixa	DataFieldnValDiferencaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup4Left&TopyWidth�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@������v�	@ MasterQRSubDetail4ReprintOnNewPage TQRLabel	QRLabel19LeftTopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUUU�@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionRESUMO DO CAIXAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel20Left(TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionFORMAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel21LeftTopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@       �@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionVALOR CALCULADOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel22Left�TopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��	@       �@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionVALOR INFORMADOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel23LeftTopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@       �@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   VALOR DIFERENÇAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand5Left&Top�Width�Height(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteEnabledForceNewColumnForceNewPageSize.Values��������@������v�	@ BandTyperbGroupFooter TQRLabel	QRLabel24Left(TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTOTALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblTotalMovLeftTopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand6Left&Top�Width�Height@Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ BandTyperbPageFooter TQRShapeQRShape3Left Top0Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@                 �@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel36Left8TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionER2SoftColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel6Left9Top Width[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ж@UUUUUUU�@TUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionwww.er2soft.com.brColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRImageQRImage1LeftTop
Width)Height)Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@��������@UUUUUU��@ Picture.Data
�  
TJPEGImage�  ���� JFIF  ` `  �� C 		
 $.' ",#(7),01444'9=82<.342�� C			2!!22222222222222222222222222222222222222222222222222��  #  " ��           	
�� �   } !1AQa"q2���#B��R��$3br�	
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������        	
�� �  w !1AQaq"2�B����	#3R�br�
$4�%�&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������   ? ��&H�',z(䚭q8D�s �>˟��ϵWO5�����͸�3������o2��{��h��|��\��� �����4���hX���g2G	K@3��������r����n8�PkS#+j3y�y[x�����MMq���%E%z���(�����߈IE� _����eC#��[�		*�g����肗K��N�s��~�C�-���guo(��'>��iwז�%ջ�|�c�d��P��YUt�8N6V�����Щ)�R���v5��h���nG�<��� 3P��Ƴ�--�츖\��?��
�%��[[�gt�`�o>��*�[��D��m����u	˯��z�Ly⿯Էګ^E��YA;HϵWT���x�ĳ�V�v�&�QE�PKv��  TQRShapeQRShape5Left Top Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                    ������t�	@ 	Pen.WidthShape
qrsHorLine   TADOStoredProcSPREL_BORDERO_CAIXA
ConnectionfrmMenu.ConnectionProcedureNameSPREL_BORDERO_CAIXA;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@dDtInicial
Attributes
paNullable DataTypeftStringSize
Value  Name	@dDtFinal
Attributes
paNullable DataTypeftStringSize
Value  Name@nCdLoja
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@nCdContaBancaria
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@nSaldoAnterior
Attributes
paNullable DataTypeftBCD	DirectionpdOutputNumericScale	PrecisionValue          Left�Top   	TADOQueryqryTemp_Resumo_Vendas
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *FROM #Temp_Resumo_Vendas Left�Top  TStringField)qryTemp_Resumo_VendascNmTabTipoFormaPagto	FieldNamecNmTabTipoFormaPagtoSize2  	TBCDFieldqryTemp_Resumo_VendasnValPagto	FieldName	nValPagto	PrecisionSize   	TADOQueryqryTemp_Resumo_Recebimento
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *FROM #Temp_Resumo_Recebimento LefthTop� TStringField.qryTemp_Resumo_RecebimentocNmTabTipoFormaPagto	FieldNamecNmTabTipoFormaPagtoSize2  	TBCDField#qryTemp_Resumo_RecebimentonValPagto	FieldName	nValPagto	PrecisionSize   	TADOQueryqryTemp_Resumo_Lanctos
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *FROM #Temp_Resumo_Lanctos LeftTop� TDateTimeFieldqryTemp_Resumo_LanctosdDtLancto	FieldName	dDtLancto  TStringField#qryTemp_Resumo_LanctoscNmTipoLancto	FieldNamecNmTipoLanctoSize2  TStringField*qryTemp_Resumo_LanctoscNmTabTipoFormaPagto	FieldNamecNmTabTipoFormaPagtoSize2  	TBCDField qryTemp_Resumo_LanctosnValLancto	FieldName
nValLancto	PrecisionSize  TStringFieldqryTemp_Resumo_LanctosnCdConta	FieldNamenCdConta	FixedChar	Size  TStringFieldqryTemp_Resumo_LanctoscNmLoja	FieldNamecNmLojaSize2  TStringField$qryTemp_Resumo_LanctoscMotivoEstorno	FieldNamecMotivoEstornoSize�   TIntegerField"qryTemp_Resumo_LanctosnCdLanctoFin	FieldNamenCdLanctoFin  TIntegerField%qryTemp_Resumo_LanctosnCdLanctoFinPai	FieldNamenCdLanctoFinPai   	TADOQueryqryTemp_Resumo_Caixa
ConnectionfrmMenu.Connection
Parameters SQL.Strings	SELECT cNmTabTipoFormaPagto+		  ,Sum(nValTotal)               nValTotal/		  ,Sum(nValInformado)           nValInformado/		  ,Sum(nValTotal-nValInformado) nValDiferenca	 FROM #Temp_Resumo_Caixa	GROUP BY cNmTabTipoFormaPagto  Left@Top� TStringField(qryTemp_Resumo_CaixacNmTabTipoFormaPagto	FieldNamecNmTabTipoFormaPagtoSize2  	TBCDFieldqryTemp_Resumo_CaixanValTotal	FieldName	nValTotal	PrecisionSize  	TBCDField!qryTemp_Resumo_CaixanValInformado	FieldNamenValInformado	PrecisionSize  	TBCDField!qryTemp_Resumo_CaixanValDiferenca	FieldNamenValDiferenca	PrecisionSize   	TADOQueryqryPreparaTemp
ConnectionfrmMenu.Connection
Parameters SQL.Strings5IF (OBJECT_ID('tempdb..#Temp_Resumo_Vendas') IS NULL)BEGIN F    CREATE TABLE #Temp_Resumo_Vendas (cNmTabTipoFormaPagto VARCHAR(50)Z		                                  ,nValPagto           DECIMAL(12,2) DEFAULT 0 NOT NULL) END  6IF (OBJECT_ID('tempdb..#Temp_Resumo_Lanctos') IS NULL)BEGIN D    CREATE TABLE #Temp_Resumo_Lanctos (dDtLancto            datetimeE		                                  ,cNmTipoLancto        varchar(50)E		                                  ,cNmTabTipoFormaPagto varchar(50)Z		                                  ,nValLancto           decimal(12,2) default 0 not nullE    		                              ,nCdConta             varchar(50)E		                                  ,cNmLoja              varchar(50)H                                      ,cMotivoEstorno       varchar(150)?                                      ,nCdLanctoFin         int@                                      ,nCdLanctoFinPai      int) END 4IF (OBJECT_ID('tempdb..#Temp_Resumo_Caixa') IS NULL)BEGIN E    CREATE TABLE #Temp_Resumo_Caixa (cNmTabTipoFormaPagto varchar(50)X		                                ,nValTotal            decimal(12,2) default 0 not nullY	                                  ,nValInformado        DECIMAL(12,2) DEFAULT 0 NOT NULLS								                   	,nValDiferenca        DECIMAL(12,2) DEFAULT 0 NOT NULL) END :IF (OBJECT_ID('tempdb..#Temp_Resumo_Recebimento') IS NULL)BEGIN K    CREATE TABLE #Temp_Resumo_Recebimento (cNmTabTipoFormaPagto varchar(50)a                                          ,nValPagto            decimal(12,2) default 0 not null) END  LeftTop   TADOCommandcmdPreparaTempCommandText  	IF (OBJECT_ID('tempdb..#Temp_Resumo_Vendas') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Resumo_Vendas (cNmTabTipoFormaPagto VARCHAR(50)
										 ,nValPagto            DECIMAL(12,2) DEFAULT 0 NOT NULL)

	END


	IF (OBJECT_ID('tempdb..#Temp_Resumo_Lanctos') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Resumo_Lanctos (dDtLancto            datetime
										  ,cNmTipoLancto        varchar(50)
										  ,cNmTabTipoFormaPagto varchar(50)
										  ,nValLancto           decimal(12,2) default 0 not null
										  ,nCdConta             varchar(50)
										  ,cNmLoja              varchar(50)
										  ,cMotivoEstorno       varchar(150)
										  ,nCdLanctoFin         int
										  ,nCdLanctoFinPai      int)

	END

	IF (OBJECT_ID('tempdb..#Temp_Resumo_Caixa') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Resumo_Caixa (cNmTabTipoFormaPagto varchar(50)
										,nValTotal            decimal(12,2) default 0 not null
										,nValInformado        DECIMAL(12,2) DEFAULT 0 NOT NULL
										,nValDiferenca        DECIMAL(12,2) DEFAULT 0 NOT NULL)

	END

	IF (OBJECT_ID('tempdb..#Temp_Resumo_Recebimento') IS NULL)
	BEGIN

		CREATE TABLE #Temp_Resumo_Recebimento (cNmTabTipoFormaPagto varchar(50)
											  ,nValPagto            decimal(12,2) default 0 not null)

	END

ConnectionfrmMenu.Connection
Parameters LeftTop    