inherited frmCopiaAgrupamentoContabil: TfrmCopiaAgrupamentoContabil
  Left = 236
  Top = 199
  Width = 540
  Height = 183
  Caption = 'Duplicar Agrupamento'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 524
    Height = 116
  end
  object Label1: TLabel [1]
    Left = 16
    Top = 48
    Width = 114
    Height = 13
    Caption = 'Descri'#231#227'o Agrupamento'
  end
  object Label2: TLabel [2]
    Left = 42
    Top = 72
    Width = 88
    Height = 13
    Caption = 'Tipo Agrupamento'
  end
  object Label3: TLabel [3]
    Left = 20
    Top = 96
    Width = 110
    Height = 13
    Caption = 'Exerc'#237'cio Agrupamento'
  end
  inherited ToolBar1: TToolBar
    Width = 524
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtDescricao: TEdit [5]
    Left = 136
    Top = 40
    Width = 313
    Height = 21
    TabOrder = 1
  end
  object edtExercicio: TEdit [6]
    Left = 136
    Top = 88
    Width = 65
    Height = 21
    MaxLength = 4
    TabOrder = 3
  end
  object ER2LookupMaskEdit1: TER2LookupMaskEdit [7]
    Left = 136
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 220
    QueryLookup = qryConsultaTipo
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 208
    Top = 64
    Width = 241
    Height = 21
    DataField = 'cNmTabTipoAgrupamentoContabil'
    DataSource = dsConsultaTipo
    TabOrder = 4
  end
  inherited ImageList1: TImageList
    Left = 448
    Top = 32
  end
  object qryConsultaTipo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoAgrupamentoContabil'
      'WHERE nCdTabTipoAgrupamentoContabil = :nPK'
      ''
      '')
    Left = 448
    Top = 72
    object qryConsultaTiponCdTabTipoAgrupamentoContabil: TIntegerField
      FieldName = 'nCdTabTipoAgrupamentoContabil'
    end
    object qryConsultaTipocNmTabTipoAgrupamentoContabil: TStringField
      FieldName = 'cNmTabTipoAgrupamentoContabil'
      Size = 50
    end
  end
  object dsConsultaTipo: TDataSource
    DataSet = qryConsultaTipo
    Left = 488
    Top = 72
  end
  object SP_DUPLICA_AGRUPAMENTO_CONTABIL: TADOStoredProc
    Connection = frmMenu.Connection
    CursorType = ctStatic
    ProcedureName = 'SP_DUPLICA_AGRUPAMENTO_CONTABIL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdAgrupamentoContabil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@cNmAgrupamentoContabil'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@nCdTabTipoAgrupamentoContabil'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iAno'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 488
    Top = 32
  end
  object qryVerificaTipo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPkTipo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPKAno'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ncdAgrupamentoContabil'
      '      ,iAno'
      '  FROM AgrupamentoContabil'
      
        ' WHERE (nCdtabtipoagrupamentocontabil = :nPkTipo) and (iAno = :n' +
        'PKAno)')
    Left = 448
    Top = 112
    object qryVerificaTiponcdAgrupamentoContabil: TIntegerField
      FieldName = 'ncdAgrupamentoContabil'
    end
    object qryVerificaTipoiAno: TIntegerField
      FieldName = 'iAno'
    end
  end
  object dsVerificaTipo: TDataSource
    DataSet = qryVerificaTipo
    Left = 488
    Top = 112
  end
end
