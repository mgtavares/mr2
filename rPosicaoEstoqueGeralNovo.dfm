inherited rptPosicaoEstoqueGeralNovo: TrptPosicaoEstoqueGeralNovo
  Left = 128
  Top = 123
  Caption = 'Rel. Posi'#231#227'o Estoque - Tamanho x Cole'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 46
    Top = 44
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 21
    Top = 68
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local Estoque'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 92
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 5
    Top = 116
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Invent'#225'rio'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 18
    Top = 140
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 40
    Top = 164
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 22
    Top = 188
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'SubCategoria'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 39
    Top = 212
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 49
    Top = 236
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 58
    Top = 260
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grade'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 355
    Top = 260
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tamanho'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 49
    Top = 284
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cole'#231#227'o'
    FocusControl = DBEdit12
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [14]
    Left = 16
    Top = 312
    Width = 161
    Height = 49
    Caption = 'Exibir Valores'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 19
  end
  object RadioGroup2: TRadioGroup [15]
    Left = 184
    Top = 312
    Width = 345
    Height = 49
    Caption = 'M'#233'todo de Valoriza'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Ultimo Pre'#231'o de Custo'
      'Pre'#231'o M'#233'dio')
    TabOrder = 20
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 161
    Top = 36
    Width = 534
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [17]
    Tag = 1
    Left = 161
    Top = 60
    Width = 534
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstoque
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 161
    Top = 84
    Width = 534
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = dsGrupoProduto
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 161
    Top = 108
    Width = 534
    Height = 21
    DataField = 'cNmGrupoInventario'
    DataSource = dsGrupoInventario
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [20]
    Tag = 1
    Left = 161
    Top = 132
    Width = 534
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [21]
    Tag = 1
    Left = 161
    Top = 156
    Width = 534
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = dsCategoria
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [22]
    Tag = 1
    Left = 161
    Top = 180
    Width = 534
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = dsSubCategoria
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [23]
    Tag = 1
    Left = 161
    Top = 204
    Width = 534
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = dsSegmento
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [24]
    Tag = 1
    Left = 161
    Top = 228
    Width = 534
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 9
  end
  object edtEmpresa: TER2LookupMaskEdit [25]
    Left = 93
    Top = 36
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtLocalEstoque: TER2LookupMaskEdit [26]
    Left = 93
    Top = 60
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 11
    Text = '         '
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object edtGrupoProduto: TER2LookupMaskEdit [27]
    Left = 93
    Top = 84
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 12
    Text = '         '
    CodigoLookup = 69
    QueryLookup = qryGrupoProdutos
  end
  object edtGrupoInventario: TER2LookupMaskEdit [28]
    Left = 93
    Top = 108
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 13
    Text = '         '
    CodigoLookup = 153
    QueryLookup = qryGrupoInventario
  end
  object edtDepartamento: TER2LookupMaskEdit [29]
    Left = 93
    Top = 132
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 14
    Text = '         '
    CodigoLookup = 129
    QueryLookup = qryDepartamento
  end
  object edtCategoria: TER2LookupMaskEdit [30]
    Left = 93
    Top = 156
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 15
    Text = '         '
    OnBeforeLookup = edtCategoriaBeforeLookup
    CodigoLookup = 46
    QueryLookup = qryCategoria
  end
  object edtSubCategoria: TER2LookupMaskEdit [31]
    Left = 93
    Top = 180
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 16
    Text = '         '
    OnBeforeLookup = edtSubCategoriaBeforeLookup
    CodigoLookup = 47
    QueryLookup = qrySubCategoria
  end
  object edtSegmento: TER2LookupMaskEdit [32]
    Left = 93
    Top = 204
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 17
    Text = '         '
    OnBeforeLookup = edtSegmentoBeforeLookup
    CodigoLookup = 48
    QueryLookup = qrySegmento
  end
  object edtProduto: TER2LookupMaskEdit [33]
    Left = 93
    Top = 228
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 18
    Text = '         '
    CodigoLookup = 76
    WhereAdicional.Strings = (
      
        'NOT EXISTS(SELECT  1 FROM Produto Filho WHERE Filho.nCdProdutoPa' +
        'i = Produto.nCdProduto)')
    QueryLookup = qryProduto
  end
  object RadioGroup3: TRadioGroup [34]
    Left = 16
    Top = 368
    Width = 161
    Height = 49
    Caption = 'Somente Estoque Negativo'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 21
  end
  object RadioGroup4: TRadioGroup [35]
    Left = 184
    Top = 368
    Width = 185
    Height = 49
    Caption = 'Status do produto'
    Columns = 3
    ItemIndex = 1
    Items.Strings = (
      'Ambos'
      'Ativo'
      'Inativo')
    TabOrder = 22
  end
  object RadioGroup5: TRadioGroup [36]
    Left = 536
    Top = 368
    Width = 153
    Height = 49
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 23
  end
  object ER2LookupMaskEdit1: TER2LookupMaskEdit [37]
    Left = 93
    Top = 252
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 24
    Text = '         '
    CodigoLookup = 51
    QueryLookup = qryGrade
  end
  object DBEdit10: TDBEdit [38]
    Tag = 1
    Left = 161
    Top = 252
    Width = 176
    Height = 21
    DataField = 'cNmGrade'
    DataSource = dsGrade
    TabOrder = 25
  end
  object ER2LookupMaskEdit2: TER2LookupMaskEdit [39]
    Left = 405
    Top = 252
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 26
    Text = '         '
    OnBeforeLookup = ER2LookupMaskEdit2BeforeLookup
    CodigoLookup = 371
    QueryLookup = qryTamanho
  end
  object DBEdit11: TDBEdit [40]
    Tag = 1
    Left = 473
    Top = 252
    Width = 222
    Height = 21
    DataField = 'cNmTamanho'
    DataSource = dsTamanho
    TabOrder = 27
  end
  object ER2LookupMaskEdit3: TER2LookupMaskEdit [41]
    Left = 93
    Top = 276
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 28
    Text = '         '
    CodigoLookup = 52
    QueryLookup = qryColecao
  end
  object DBEdit12: TDBEdit [42]
    Tag = 1
    Left = 161
    Top = 276
    Width = 534
    Height = 21
    DataField = 'cNmColecao'
    DataSource = dsColecao
    TabOrder = 29
  end
  object RadioGroup6: TRadioGroup [43]
    Left = 536
    Top = 312
    Width = 281
    Height = 49
    Caption = 'Filtro Cole'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Igual '#224
      'Menor que'
      'Maior que')
    TabOrder = 30
  end
  object RadioGroup7: TRadioGroup [44]
    Left = 376
    Top = 368
    Width = 153
    Height = 49
    Caption = 'Nivel Produto'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      '1'
      '2'
      '3')
    TabOrder = 31
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 200
  end
  object qryGrupoProdutos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto'
      '      ,cNmGrupoProduto '
      '  FROM GrupoProduto'
      ' WHERE nCdGrupoProduto = :nPK')
    Left = 104
    Top = 448
    object qryGrupoProdutosnCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutoscNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object qryGrupoInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdGrupoInventario'
      '      ,cNmGrupoInventario '
      '  FROM GrupoInventario'
      ' WHERE nCdGrupoInventario = :nPK')
    Left = 144
    Top = 448
    object qryGrupoInventarionCdGrupoInventario: TIntegerField
      FieldName = 'nCdGrupoInventario'
    end
    object qryGrupoInventariocNmGrupoInventario: TStringField
      FieldName = 'cNmGrupoInventario'
      Size = 50
    end
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento '
      '  FROM Departamento'
      ' WHERE nCdDepartamento = :nPK')
    Left = 184
    Top = 448
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 64
    Top = 448
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      '      ,cNmCategoria '
      '  FROM Categoria'
      ' WHERE nCdCategoria    = :nPK'
      '   AND nCdDepartamento = :nCdDepartamento')
    Left = 224
    Top = 448
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      '      ,cNmSubCategoria '
      '  FROM SubCategoria'
      ' WHERE nCdSubCategoria = :nPK'
      '   AND nCdCategoria    = :nCdCategoria')
    Left = 264
    Top = 448
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdSegmento'
      '      ,cNmSegmento '
      '  FROM Segmento'
      ' WHERE nCdSegmento     = :nPK'
      '   AND nCdSubCategoria = :nCdSubCategoria')
    Left = 304
    Top = 448
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa)')
    Left = 24
    Top = 448
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto '
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 344
    Top = 448
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 40
    Top = 480
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 80
    Top = 480
  end
  object dsGrupoProduto: TDataSource
    DataSet = qryGrupoProdutos
    Left = 120
    Top = 480
  end
  object dsGrupoInventario: TDataSource
    DataSet = qryGrupoInventario
    Left = 160
    Top = 480
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 200
    Top = 480
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 240
    Top = 480
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 280
    Top = 480
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 320
    Top = 480
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 360
    Top = 480
  end
  object ER2Excel1: TER2Excel
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 552
    Top = 464
  end
  object qryGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrade'
      '            ,cNmGrade'
      '  FROM Grade'
      ' WHERE nCdGrade = :nPK')
    Left = 384
    Top = 448
    object qryGradenCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryGradecNmGrade: TStringField
      FieldName = 'cNmGrade'
      Size = 50
    end
  end
  object qryTamanho: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select nCdItemGrade'
      '      ,cNmTamanho'
      '  from ItemGrade'
      ' where nCdItemGrade = :nPK')
    Left = 424
    Top = 448
    object qryTamanhonCdItemGrade: TIntegerField
      FieldName = 'nCdItemGrade'
    end
    object qryTamanhocNmTamanho: TStringField
      FieldName = 'cNmTamanho'
      FixedChar = True
      Size = 5
    end
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao'
      '             ,cNmColecao'
      '  FROM Colecao'
      ' WHERE nCdColecao = :nPK')
    Left = 464
    Top = 448
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object dsGrade: TDataSource
    DataSet = qryGrade
    Left = 400
    Top = 480
  end
  object dsTamanho: TDataSource
    DataSet = qryTamanho
    Left = 440
    Top = 480
  end
  object dsColecao: TDataSource
    DataSet = qryColecao
    Left = 480
    Top = 480
  end
end
