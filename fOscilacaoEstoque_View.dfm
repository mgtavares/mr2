inherited frmOscilacaoEstoque_View: TfrmOscilacaoEstoque_View
  Left = 112
  Top = 124
  Width = 1194
  Height = 708
  Caption = 'Oscila'#231#227'o de Estoque'
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1186
    Height = 652
  end
  inherited ToolBar1: TToolBar
    Width = 1186
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1186
    Height = 652
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Position = spFooter
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoAnt'
          Column = cxGrid1DBTableView1nSaldoAnt
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorAnt'
          Column = cxGrid1DBTableView1nValorAnt
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nQtdeEnt'
          Column = cxGrid1DBTableView1nQtdeEnt
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorEnt'
          Column = cxGrid1DBTableView1nValorEnt
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nQtdeSai'
          Column = cxGrid1DBTableView1nQtdeSai
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorSai'
          Column = cxGrid1DBTableView1nValorSai
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorSai'
          Column = cxGrid1DBTableView1nValorSai
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoPos'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorPos'
          Column = cxGrid1DBTableView1nValorPos
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nQtdeOsc'
          Column = cxGrid1DBTableView1nQtdeOsc
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValorOsc'
          Column = cxGrid1DBTableView1nValorOsc
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoPos'
          Column = cxGrid1DBTableView1nSaldoPos
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn
        Caption = 'Local de Estoque'
        DataBinding.FieldName = 'cNmLocalEstoque'
        Width = 164
      end
      object cxGrid1DBTableView1cNmGrupoProduto: TcxGridDBColumn
        Caption = 'Grupo Produto'
        DataBinding.FieldName = 'cNmGrupoProduto'
        Width = 155
      end
      object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'cNmProduto'
        Width = 186
      end
      object cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn
        Caption = 'U.M.'
        DataBinding.FieldName = 'cUnidadeMedida'
        Width = 48
      end
      object cxGrid1DBTableView1nSaldoAnt: TcxGridDBColumn
        Caption = 'Qtde Anterior'
        DataBinding.FieldName = 'nSaldoAnt'
        Width = 101
      end
      object cxGrid1DBTableView1nValorAnt: TcxGridDBColumn
        Caption = 'Valor Anterior'
        DataBinding.FieldName = 'nValorAnt'
        Width = 103
      end
      object cxGrid1DBTableView1nQtdeEnt: TcxGridDBColumn
        Caption = 'Qtde Entrada'
        DataBinding.FieldName = 'nQtdeEnt'
        Width = 99
      end
      object cxGrid1DBTableView1nValorEnt: TcxGridDBColumn
        Caption = 'Valor Entrada'
        DataBinding.FieldName = 'nValorEnt'
        Width = 102
      end
      object cxGrid1DBTableView1nQtdeSai: TcxGridDBColumn
        Caption = 'Qtde Sa'#237'da'
        DataBinding.FieldName = 'nQtdeSai'
        Width = 88
      end
      object cxGrid1DBTableView1nValorSai: TcxGridDBColumn
        Caption = 'Valor Sa'#237'da'
        DataBinding.FieldName = 'nValorSai'
        Width = 90
      end
      object cxGrid1DBTableView1nSaldoPos: TcxGridDBColumn
        Caption = 'Qtde Posterior'
        DataBinding.FieldName = 'nSaldoPos'
        Width = 109
      end
      object cxGrid1DBTableView1nValorPos: TcxGridDBColumn
        Caption = 'Valor Posterior'
        DataBinding.FieldName = 'nValorPos'
        Width = 117
      end
      object cxGrid1DBTableView1nQtdeOsc: TcxGridDBColumn
        Caption = 'Qtde Oscila'#231#227'o'
        DataBinding.FieldName = 'nQtdeOsc'
        Width = 112
      end
      object cxGrid1DBTableView1nValorOsc: TcxGridDBColumn
        Caption = 'Valor Oscila'#231#227'o'
        DataBinding.FieldName = 'nValorOsc'
      end
      object cxGrid1DBTableView1cNmDepartamento: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'cNmDepartamento'
        Width = 154
      end
      object cxGrid1DBTableView1cNmCategoria: TcxGridDBColumn
        Caption = 'Categoria'
        DataBinding.FieldName = 'cNmCategoria'
        Width = 170
      end
      object cxGrid1DBTableView1cNmSubCategoria: TcxGridDBColumn
        Caption = 'SubCategoria'
        DataBinding.FieldName = 'cNmSubCategoria'
        Width = 170
      end
      object cxGrid1DBTableView1nValPrecoUltRec: TcxGridDBColumn
        DataBinding.FieldName = 'nValPrecoUltRec'
        Width = 114
      end
      object cxGrid1DBTableView1dDtUltReceb: TcxGridDBColumn
        DataBinding.FieldName = 'dDtUltReceb'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 1152
    Top = 232
  end
  object uspRelatorio: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_OSCILACAO_ESTOQUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdLocalEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdGrupoProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdGrupoInventario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdDepartamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdSubCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@ExibirValores'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@Valorizacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@SomenteProdutoAtivo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtRecebInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtRecebFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    Left = 752
    Top = 288
    object uspRelatorionCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object uspRelatoriocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object uspRelatorionCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object uspRelatoriocNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object uspRelatorionCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object uspRelatoriocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object uspRelatoriocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      Size = 50
    end
    object uspRelatorionSaldoAnt: TBCDField
      FieldName = 'nSaldoAnt'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionQtdeEnt: TBCDField
      FieldName = 'nQtdeEnt'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionQtdeSai: TBCDField
      FieldName = 'nQtdeSai'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionSaldoPos: TBCDField
      FieldName = 'nSaldoPos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionQtdeOsc: TBCDField
      FieldName = 'nQtdeOsc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValorAnt: TBCDField
      FieldName = 'nValorAnt'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValorEnt: TBCDField
      FieldName = 'nValorEnt'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValorSai: TBCDField
      FieldName = 'nValorSai'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValorPos: TBCDField
      FieldName = 'nValorPos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValorOsc: TBCDField
      FieldName = 'nValorOsc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
    object uspRelatorionValMedio: TBCDField
      FieldName = 'nValMedio'
      Precision = 12
      Size = 2
    end
    object uspRelatoriocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object uspRelatoriocNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object uspRelatoriocNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object uspRelatorionValPrecoUltRec: TBCDField
      DisplayLabel = 'Pre'#231'o '#218'lt. Receb.'
      FieldName = 'nValPrecoUltRec'
      Precision = 12
      Size = 2
    end
    object uspRelatoriodDtUltReceb: TDateTimeField
      DisplayLabel = 'Dt. '#218'lt. Receb.'
      FieldName = 'dDtUltReceb'
    end
  end
  object DataSource1: TDataSource
    DataSet = uspRelatorio
    Left = 792
    Top = 288
  end
end
