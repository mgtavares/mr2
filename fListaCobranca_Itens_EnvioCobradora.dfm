inherited frmListaCobranca_Itens_EnvioCobradora: TfrmListaCobranca_Itens_EnvioCobradora
  Left = 335
  Top = 363
  Height = 140
  BorderIcons = [biSystemMenu]
  Caption = 'Envio T'#237'tulo Cobradora'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 75
  end
  object Label1: TLabel [1]
    Left = 27
    Top = 48
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cobradora'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object edtCobradora: TMaskEdit [3]
    Left = 83
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtCobradoraExit
    OnKeyDown = edtCobradoraKeyDown
  end
  object cxButton1: TcxButton [4]
    Left = 8
    Top = 72
    Width = 137
    Height = 28
    Caption = 'Processar Envio'
    TabOrder = 2
    OnClick = cxButton1Click
    Glyph.Data = {
      72020000424D720200000000000036000000280000000E0000000D0000000100
      1800000000003C020000000000000000000000000000000000008C9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      008C9C9C00000000000000840000840000840000840000840000840000840000
      8400008400008400008400008400000000000000009C9CFF0000FF0000FF0000
      FFEFFFFF0000840000840000840000840000FF0000FF00008400000000000000
      009C9CFF0000FF0000FFEFFFFFEFFFFFEFFFFFEFFFFF00008400008400008400
      008400008400000000000000009C9CFF0000FFEFFFFFEFFFFFEFFFFFEFFFFFEF
      FFFFEFFFFFEFFFFF0000840000FF00008400000000000000009C9CFF0000FF00
      00FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF000084000000
      00000000009C9CFF0000FF0000FF0000FF0000FF0000FFEFFFFFEFFFFF000084
      0000840000FF00008400000000000000009C9CFF0000FF0000FFEFFFFFEFFFFF
      EFFFFFEFFFFFEFFFFFEFFFFF0000FF0000FF00008400000000000000009C9CFF
      0000FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF0000FF0000
      8400000000000000009C9CFF0000FF0000FF0000FF0000FFEFFFFFEFFFFF0000
      840000840000FF0000FF00008400000000000000009C9CFF0000FF0000FF0000
      FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF00008400000000000000
      009C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C
      9CFF9C9CFF00000000008C9C9C00000000000000000000000000000000000000
      00000000000000000000000000000000008C9C9C0000}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 152
    Top = 40
    Width = 614
    Height = 21
    DataField = 'cNmCobradora'
    DataSource = DataSource1
    TabOrder = 3
  end
  object qryCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCobradora, cNmCobradora'
      'FROM Cobradora'
      'WHERE nCdCobradora = :nPK'
      'AND cFlgAtivo = 1')
    Left = 296
    Top = 64
    object qryCobradoranCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryCobradoracNmCobradora: TStringField
      FieldName = 'cNmCobradora'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryCobradora
    Left = 336
    Top = 64
  end
  object SP_REGISTRA_TITULO_COBRADORA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRA_TITULO_COBRADORA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdListaCobranca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 440
    Top = 64
  end
end
