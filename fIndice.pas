unit fIndice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, GridsEh, DBGridEh, Mask, DBCtrls, cxPC, cxControls;

type
  TFrmIndice = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryMasternCdIndiceReajuste: TIntegerField;
    qryMastercNmIndiceReajuste: TStringField;
    qryCotacao: TADOQuery;
    qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField;
    qryCotacaonCdIndiceReajuste: TIntegerField;
    qryCotacaoiAnoReferencia: TIntegerField;
    qryCotacaoiMesReferencia: TIntegerField;
    qryCotacaonIndice: TBCDField;
    DataSource1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryCotacaoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmIndice: TFrmIndice;

implementation

{$R *.dfm}

procedure TFrmIndice.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'INDICEREAJUSTE' ;
  nCdTabelaSistema  := 94 ;
  nCdConsultaPadrao := 1001 ;

  inherited;

end;

procedure TFrmIndice.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit3.SetFocus;
end;

procedure TFrmIndice.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryCotacao.Close ;
  qryCotacao.Parameters.ParamByName('nCdIndiceReajuste').Value := qryMasternCdIndiceReajuste.Value ;
  qryCotacao.Open ;

end;

procedure TFrmIndice.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCotacao.Close ;
end;

procedure TFrmIndice.qryCotacaoBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryCotacaonCdIndiceReajuste.Value := qryMaster.FieldList[0].Value ;

end;

initialization
    RegisterClass(tFrmIndice) ;

end.
