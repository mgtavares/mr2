inherited frmEnvioClienteCobradora: TfrmEnvioClienteCobradora
  Caption = 'Envio Cliente Cobradora'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 794
    Height = 428
  end
  object Label5: TLabel [1]
    Left = 34
    Top = 48
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label1: TLabel [2]
    Left = 16
    Top = 72
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cobradora'
  end
  object Label3: TLabel [3]
    Left = 15
    Top = 96
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Envio'
  end
  inherited ToolBar1: TToolBar
    Width = 794
    TabOrder = 4
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object edtCliente: TMaskEdit [5]
    Left = 72
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    OnExit = edtClienteExit
    OnKeyDown = edtClienteKeyDown
  end
  object btExibeTitulo: TcxButton [6]
    Left = 72
    Top = 116
    Width = 113
    Height = 29
    Caption = 'Exibir T'#237'tulos'
    TabOrder = 3
    OnClick = btExibeTituloClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 140
    Top = 40
    Width = 149
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 293
    Top = 40
    Width = 501
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 6
  end
  object edtCobradora: TMaskEdit [9]
    Left = 72
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnExit = edtCobradoraExit
    OnKeyDown = edtCobradoraKeyDown
  end
  object DBEdit3: TDBEdit [10]
    Tag = 1
    Left = 140
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmCobradora'
    DataSource = DataSource2
    TabOrder = 7
  end
  object edtDtEnvio: TMaskEdit [11]
    Left = 72
    Top = 88
    Width = 81
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCNPJCPF'
      '  FROM Terceiro         '
      ' WHERE EXISTS(SELECT 1                   '
      
        '                FROM TerceiroTipoTerceiro TTT                   ' +
        '               '
      
        '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro     ' +
        '                                 '
      '                 AND TTT.nCdTipoTerceiro = 2)  '
      '   AND Terceiro.nCdTerceiro = :nPK')
    Left = 280
    Top = 216
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 384
    Top = 240
  end
  object qryCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCobradora, cNmCobradora'
      'FROM Cobradora'
      'WHERE nCdCobradora = :nPK'
      'AND cFlgAtivo  = 1')
    Left = 328
    Top = 200
    object qryCobradoranCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryCobradoracNmCobradora: TStringField
      FieldName = 'cNmCobradora'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCobradora
    Left = 392
    Top = 248
  end
end
