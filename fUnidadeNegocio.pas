unit fUnidadeNegocio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, ImgList;

type
  TfrmUnidadeNegocio = class(TfrmCadastro_Padrao)
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMastercNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    dsEmpresaUnidadeNegocio: TDataSource;
    qryEmpresaUnidadeNegocio: TADOQuery;
    qryEmpresaUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryEmpresaUnidadeNegocionCdEmpresa: TIntegerField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEmpresanCdTerceiroMatriz: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresanCdStatus: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryEmpresaUnidadeNegociocNmEmpresa: TStringField;
    DBGridEh1: TDBGridEh;
    qryEmpresaUnidadeNegociocSigla: TStringField;
    StaticText1: TStaticText;
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryEmpresaUnidadeNegocioBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUnidadeNegocio: TfrmUnidadeNegocio;

implementation

{$R *.dfm}

procedure TfrmUnidadeNegocio.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmUnidadeNegocio.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryEmpresaUnidadeNegocio.Close ;
  qryEmpresaUnidadeNegocio.Parameters.ParamByName('nCdUnidadeNegocio').Value := qryMasternCdUnidadeNegocio.Value ;
  qryEmpresaUnidadeNegocio.Open ;
end;

procedure TfrmUnidadeNegocio.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryEmpresaUnidadeNegocio.Close ;
end;

procedure TfrmUnidadeNegocio.qryEmpresaUnidadeNegocioBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryEmpresaUnidadeNegocionCdUnidadeNegocio.Value := qryMaster.FieldList[0].Value ;

  inherited;

end;

procedure TfrmUnidadeNegocio.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'UNIDADENEGOCIO' ;
  nCdTabelaSistema  := 10 ;
  nCdConsultaPadrao := 14 ;
  inherited;

end;

initialization
    RegisterClass(tfrmUnidadeNegocio) ;

end.
