unit rEmitePisCofinsVendasSint_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, jpeg, DB, ADODB;

type
  TrEmitePisCofinsSint_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRBand5: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRBand2: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    uspRelatorio: TADOStoredProc;
    uspRelatorioiNrDocto: TIntegerField;
    uspRelatoriovrProdVenda: TBCDField;
    uspRelatoriovrProdDev: TBCDField;
    uspRelatoriovrPis: TBCDField;
    uspRelatoriovrCofins: TBCDField;
    uspRelatoriovrPisDev: TBCDField;
    uspRelatoriovrCofinsDev: TBCDField;
    uspRelatoriocEmpresa: TStringField;
    uspRelatoriocCdLoja: TStringField;
    uspRelatoriodDtEmissao: TDateTimeField;
    uspRelatoriocNmTerceiro: TStringField;
    QRShape8: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rEmitePisCofinsSint_View: TrEmitePisCofinsSint_View;

implementation

{$R *.dfm}

end.
