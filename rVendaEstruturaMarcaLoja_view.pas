unit rVendaEstruturaMarcaLoja_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaEstruturaMarcaLoja_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape6: TQRShape;
    QRExpr3: TQRExpr;
    QRLabel20: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRExpr6: TQRExpr;
    qryResultado: TADOQuery;
    QRLabel4: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText30: TQRDBText;
    QRBand2: TQRBand;
    QRShape7: TQRShape;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    qryResultadocNmDepartamento: TStringField;
    qryResultadocNmCategoria: TStringField;
    qryResultadocNmSubCategoria: TStringField;
    qryResultadocNmSegmento: TStringField;
    qryResultadocNmMarca: TStringField;
    qryResultadonValor: TBCDField;
    qryResultadonQtde: TBCDField;
    qryResultadonValCMV: TBCDField;
    qryResultadonMarkup: TBCDField;
    qryResultadonQtdeEstoque: TBCDField;
    qryResultadonValCustoEstoque: TBCDField;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr4: TQRExpr;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaEstruturaMarcaLoja_view: TrptVendaEstruturaMarcaLoja_view;

implementation

{$R *.dfm}

end.
