unit fPlantaMRP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, StdCtrls,
  Mask, DBCtrls, cxPC, cxControls, ER2Lookup;

type
  TfrmPlantaMRP = class(TfrmCadastro_Padrao)
    qryUsuarioPlantaMRP: TADOQuery;
    dsUsuarioPlantaMRP: TDataSource;
    qryUsuarioPlantaMRPnCdUsuario: TIntegerField;
    dslocalEstoqueMRP: TDataSource;
    qrylocalEstoqueMRP: TADOQuery;
    qryMasternCdPlantaMRP: TIntegerField;
    qryMastercNmPlantaMRP: TStringField;
    Label1: TLabel;
    DBEditCdPlantaMRP: TDBEdit;
    Label2: TLabel;
    DBEditNmPlantaMRP: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    qryUsuario: TADOQuery;
    qryUsuarioPlantaMRPcNmUsuario: TStringField;
    DBGridEh1: TDBGridEh;
    DBGridEh3: TDBGridEh;
    qryUsuarioPlantaMRPnCdUsuarioPlantaMRP: TIntegerField;
    qryUsuarioPlantaMRPnCdPlantaMRP: TIntegerField;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryEstoque: TADOQuery;
    qrylocalEstoqueMRPnCdLocalEstoquePlantaMRP: TIntegerField;
    qrylocalEstoqueMRPcNmLocalEstoque: TStringField;
    qryEstoquenCdlocalEstoque: TIntegerField;
    qryEstoquecNmLocalEstoque: TStringField;
    qrylocalEstoqueMRPnCdPlantaMRP: TIntegerField;
    qrylocalEstoqueMRPnCdLocalEstoque: TIntegerField;
    qryMasternCdEmpresaPlanoMRP: TIntegerField;
    Label3: TLabel;
    ER2LookupDBEdit1: TER2LookupDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryUsuarioPlantaMRPBeforePost(DataSet: TDataSet);
    procedure qryUsuarioPlantaMRPCalcFields(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrylocalEstoqueMRPBeforePost(DataSet: TDataSet);
    procedure qrylocalEstoqueMRPCalcFields(DataSet: TDataSet);
    procedure DBGridEh3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlantaMRP: TfrmPlantaMRP;

implementation
Uses
  fmenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmPlantaMRP.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'PLANTAMRP' ;
  nCdTabelaSistema  := 236 ;
  nCdConsultaPadrao := 237 ;
  bLimpaAposSalvar  := false;

end;

procedure TfrmPlantaMRP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if(Trim(qryMastercNmPlantaMRP.Value) = '') then
  begin
      MensagemAlerta('Preencha a descri��o para prosseguir.');
      DBEditNmPlantaMRP.SetFocus;
      abort;
  end;

  if (qryMasternCdEmpresaPlanoMRP.Value) <= 0 then
  begin
      MensagemAlerta('Preencha o C�digo da Empresa para prosseguir.');
      ER2LookupDBEdit1.SetFocus;
      abort;
  end;


  inherited;
end;

procedure TfrmPlantaMRP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioPlantaMRP.Close;
  qryUsuarioPlantaMRP.Parameters.ParamByName('nPK').Value := qryMasternCdPlantaMRP.Value;
  qryUsuarioPlantaMRP.Open;

  qryLocalEstoqueMRP.Close;
  qryLocalEstoqueMRP.Parameters.ParamByName('nPK').Value := qryMasternCdPlantaMRP.Value;
  qryLocalEstoqueMRP.Open;

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nPK').Value := qryMasternCdEmpresaPlanoMRP.Value;
  qryEmpresa.Open;

end;

procedure TfrmPlantaMRP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioPlantaMRP.Close;
  qryEmpresa.Close;
  qrylocalEstoqueMRP.Close;

end;

procedure TfrmPlantaMRP.qryUsuarioPlantaMRPBeforePost(DataSet: TDataSet);
begin
  inherited;

  if(qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryUsuarioPlantaMRP.State = dsInsert) then
  begin

      qryUsuarioPlantaMRPnCdUsuarioPlantaMRP.Value := frmMenu.fnProximoCodigo('USUARIOPLANTAMRP');
      qryUsuarioPlantaMRPnCdPlantaMRP.Value := qryMasternCdPlantaMRP.Value;
  end;

end;

procedure TfrmPlantaMRP.qryUsuarioPlantaMRPCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close;
  qryUsuario.Parameters.ParamByName('nPK').Value := qryUsuarioPlantaMRPnCdUsuario.Value;
  qryUsuario.Open;

  qryUsuarioPlantaMRPcNmUsuario.Value := qryUsuariocNmUsuario.Value;

end;

procedure TfrmPlantaMRP.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEditNmPlantaMRP.SetFocus;
end;

procedure TfrmPlantaMRP.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK: integer;
begin

    if ((Key = VK_F4) and (qryMaster.State <> dsBrowse)) then
    begin

        if (qryUsuarioPlantaMRP.State = dsBrowse) then
            qryUsuarioPlantaMRP.Edit;


        nPK := frmLookup_Padrao.ExecutaConsulta(5);

        if (nPk > 0) then
            qryUsuarioPlantaMRPnCdUsuario.Value := nPK;

    end;

end;

procedure TfrmPlantaMRP.qrylocalEstoqueMRPBeforePost(DataSet: TDataSet);
begin
  inherited;

  if(qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryLocalEstoqueMRP.State = dsInsert) then
  begin
      qrylocalEstoqueMRPnCdLocalEstoquePlantaMRP.Value := frmMenu.fnProximoCodigo('LOCALESTOQUEPLANTAMRP');
      qryLocalEstoqueMRPnCdPlantaMRP.Value := qryMasternCdPlantaMRP.Value;
  end;

end;

procedure TfrmPlantaMRP.qrylocalEstoqueMRPCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryEstoque.Close;
  qryEstoque.Parameters.ParamByName('nPK').Value := qryLocalEstoqueMRPnCdLocalEstoque.Value;
  qryEstoque.Open;

  qryLocalEstoqueMRPcNmLocalEstoque.Value := qryEstoquecNmLocalEstoque.Value;

end;

procedure TfrmPlantaMRP.DBGridEh3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK: integer;
begin

    if ((Key = VK_F4) and (qryMaster.State <> dsBrowse)) then
    begin

        if (qryLocalEstoqueMRP.State = dsBrowse) then
            qryLocalEstoqueMRP.Edit;


        nPK := frmLookup_Padrao.ExecutaConsulta(87);

        if (nPk > 0) then
            qryLocalEstoqueMRPnCdLocalEstoque.Value := nPK;

    end;
end;
procedure TfrmPlantaMRP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if qryMaster.Active and (qryMasternCdPlantaMRP.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

initialization
    RegisterClass(TfrmPlantaMRP);

end.
