unit fDistribuicaoOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, cxDBLookupComboBox;

type
  TfrmDistribuicaoOrcamento = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    chkContaSemOrcamento: TCheckBox;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryTemp: TADOQuery;
    qryTempnCdPlanoConta: TIntegerField;
    qryTempnCdGrupoPlanoConta: TIntegerField;
    qryTempnCdPlanoContaBase: TIntegerField;
    qryTempnPercentBase: TBCDField;
    qryTempnCdMetodoDistribOrcamento: TIntegerField;
    qryTempnValorOrcamentoConta: TBCDField;
    qryTempcOBS: TStringField;
    qryTempcOBSGeral: TMemoField;
    dsTemp: TDataSource;
    cxGrid1DBTableView1nCdPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPlanoContaBase: TcxGridDBColumn;
    cxGrid1DBTableView1nPercentBase: TcxGridDBColumn;
    cxGrid1DBTableView1nValorOrcamentoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cOBS: TcxGridDBColumn;
    cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmPlanoContaBase: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMetodoDistribOrcamento: TcxGridDBColumn;
    SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA: TADOStoredProc;
    qryTempnCdItemOrcamento: TIntegerField;
    qryPopulaTemp: TADOQuery;
    qryTempcNmPlanoConta: TStringField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    qryMetodoDistribOrcamento: TADOQuery;
    qryMetodoDistribOrcamentonCdMetodoDistribOrcamento: TIntegerField;
    qryMetodoDistribOrcamentocNmMetodoDistribOrcamento: TStringField;
    qryMetodoDistribOrcamentocFlgLinear: TIntegerField;
    qryMetodoDistribOrcamentocFlgPadrao: TIntegerField;
    dsMetodoDistribOrcamento: TDataSource;
    qryTempcNmMetodoDistribOrcamento: TStringField;
    qryTempcNmGrupoPlanoConta: TStringField;
    qryTempcNmPlanoContaBase: TStringField;
    qryAux: TADOQuery;
    SP_EXCLUI_CONTA_SEM_ORCAMENTO: TADOStoredProc;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure qryTempBeforeDelete(DataSet: TDataSet);
    procedure qryTempAfterDelete(DataSet: TDataSet);
    procedure chkContaSemOrcamentoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdOrcamento : integer ;
  end;

var
  frmDistribuicaoOrcamento: TfrmDistribuicaoOrcamento;

implementation

{$R *.dfm}

uses fDistribuicaoOrcamento_Valores, fMenu;

procedure TfrmDistribuicaoOrcamento.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDistribuicaoOrcamento.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}
end;

procedure TfrmDistribuicaoOrcamento.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmDistribuicaoOrcamento_Valores ;
begin
  inherited;

  objForm := TfrmDistribuicaoOrcamento_Valores.Create( Self ) ;

  try
      objForm.exibeValores( qryTempnCdItemOrcamento.Value ) ;
  finally
      freeAndNil( objForm ) ;
  end ;

  qryPopulaTemp.ExecSQL ;

  qryTemp.Close;
  qryTemp.Open;

end;

procedure TfrmDistribuicaoOrcamento.qryTempBeforeDelete(DataSet: TDataSet);
var
  nCdOrcamento : integer ;
begin
  inherited;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdOrcamento FROM ItemOrcamento WHERE nCdItemOrcamento = ' + qryTempnCdItemOrcamento.AsString) ;
  qryAux.Open;

  qryAux.FieldList.Update;

  if not qryAux.eof then
      nCdOrcamento := qryAux.FieldList[0].Value;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE ItemOrcamento Set nCdPlanoContaBase = NULL, nPercentBase = 0 WHERE nCdPlanoContaBase = ' + qryTempnCdPlanoConta.AsString + ' AND  nCdOrcamento = ' + intToStr( nCdOrcamento )) ;
  qryAux.ExecSQL;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM ItemOrcamentoMes WHERE nCdItemOrcamento = ' + qryTempnCdItemOrcamento.AsString) ;
  qryAux.ExecSQL;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM ItemOrcamento WHERE nCdItemOrcamento = ' + qryTempnCdItemOrcamento.AsString) ;
  qryAux.ExecSQL;


end;

procedure TfrmDistribuicaoOrcamento.qryTempAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryPopulaTemp.ExecSQL;

  qryTemp.Close;
  qryTemp.Open;

end;

procedure TfrmDistribuicaoOrcamento.chkContaSemOrcamentoClick(Sender: TObject);
begin
  inherited;

  if (not chkContaSemOrcamento.Checked) then
  begin

      frmMenu.Connection.BeginTrans;

      try
          SP_EXCLUI_CONTA_SEM_ORCAMENTO.Close;
          SP_EXCLUI_CONTA_SEM_ORCAMENTO.Parameters.ParamByName('@nCdOrcamento').Value := nCdOrcamento ;
          SP_EXCLUI_CONTA_SEM_ORCAMENTO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          abort ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryPopulaTemp.ExecSQL ;

      qryTemp.Close;
      qryTemp.Open;

  end
  else
  begin

      if (MessageDlg('Deseja incluir todas as contas do plano de contas que n�o constam neste or�amento ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Close;
          SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Parameters.ParamByName('@nCdOrcamento').Value     := nCdOrcamento ;
          SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Parameters.ParamByName('@cFlgComplementar').Value := 'S' ;
          SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      qryPopulaTemp.Parameters.ParamByName('nPK').Value := nCdOrcamento ;
      qryPopulaTemp.ExecSQL;

      qryTemp.Close;
      qryTemp.Open;

  end ;

  
end;

end.
