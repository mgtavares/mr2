unit rAcompanhaGiroProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, dateUtils;

type
  TrptAcompanhaGiroProduto = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    RadioGroup2: TRadioGroup;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    RadioGroup4: TRadioGroup;
    Label8: TLabel;
    edtDiasEstoqueIni: TMaskEdit;
    Label9: TLabel;
    edtDiasEstoqueFim: TMaskEdit;
    edtReferencia: TEdit;
    Label10: TLabel;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    RadioGroup7: TRadioGroup;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DataSource11: TDataSource;
    DataSource12: TDataSource;
    RadioGroup5: TRadioGroup;
    edtDiasEstoqueZerado: TMaskEdit;
    Label15: TLabel;
    edtGiroIdeal: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    edtDtFinal: TMaskEdit;
    Label14: TLabel;
    edtDtInicial: TMaskEdit;
    Label18: TLabel;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAcompanhaGiroProduto: TrptAcompanhaGiroProduto;

implementation

uses fMenu, fLookup_Padrao, rAcompanhaGiroProduto_view,
  fAcompanhaGiroProduto_view;

{$R *.dfm}

procedure TrptAcompanhaGiroProduto.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, edtLoja.text) ;
  end
  else
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

  edtDiasEstoqueIni.Text    := '0' ;
  edtDiasEstoqueFim.Text    := '999999999' ;

  edtGiroIdeal.Text         := '3' ;
  edtDtInicial.Text         := DateToStr(Date-31) ;
  edtDtFinal.Text           := DateToStr(Date-1) ;

  edtDiasEstoqueZerado.Text := '720' ;

end;

procedure TrptAcompanhaGiroProduto.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptAcompanhaGiroProduto.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptAcompanhaGiroProduto.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptAcompanhaGiroProduto.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptAcompanhaGiroProduto.ToolButton1Click(Sender: TObject);
var
  cFiltro : string ;
  objRel  : TrptAcompanhaGiroProduto_view;
  objForm : TfrmAcompanhaGiroProduto_view;
begin
  inherited;

  if (trim(edtDiasEstoqueIni.Text) = '') then
      edtDiasEstoqueIni.Text := '0' ;

  if (trim(edtDiasEstoqueFim.Text) = '') then
      edtDiasEstoqueFim.Text := '999999999' ;

  try
      strToInt(trim(edtDiasEstoqueIni.Text)) ;
  except
      MensagemErro('Dias de estoque Inicial inv�lido.') ;
      edtDiasEstoqueIni.Text := '0' ;
      edtDiasEstoqueIni.SetFocus;
      exit ;
  end ;

  try
      strToInt(trim(edtDiasEstoqueFim.Text)) ;
  except
      MensagemErro('Dias de estoque Final inv�lido.') ;
      edtDiasEstoqueFim.Text := '999999999' ;
      edtDiasEstoqueFim.SetFocus;
      exit ;
  end ;


  {-- tela --}
  try
      try
          if (RadioGroup1.ItemIndex = 0) then
          begin

              objForm := TfrmAcompanhaGiroProduto_view.Create(nil);

              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Close ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdClasse').Value        := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iNivelAgrupamento').Value:= RadioGroup2.ItemIndex;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasEstoqueIni').Value  := frmMenu.ConvInteiro(edtDiasEstoqueIni.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasEstoqueFim').Value  := frmMenu.ConvInteiro(edtDiasEstoqueFim.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@cReferencia').Value      := edtReferencia.Text ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iGiroIdeal').Value       := frmMenu.ConvInteiro(edtGiroIdeal.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasSemEstoque').Value  := frmMenu.ConvInteiro(edtDiasEstoqueZerado.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iOrdenacao').Value       := RadioGroup4.ItemIndex ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iTipoOrdenacao').Value   := RadioGroup7.ItemIndex ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
              objForm.SPREL_ACOMPANHA_GIRO_PRODUTO.Open ;
              objForm.cxGrid1.Align := alClient ;
              showForm(objForm,true) ;
          end ;

          {-- relat�rio --}
          if (RadioGroup1.ItemIndex = 1) then
          begin
              objRel := TrptAcompanhaGiroProduto_view.Create(nil);

              objRel.QRLabel11.Caption := 'Dias de Vendas: ' + intToStr(DaysBetween(strToDate(edtDtFinal.Text), strToDate(edtDtInicial.Text))) + ' dia(s)' ;

              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Close ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdClasse').Value        := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iNivelAgrupamento').Value:= RadioGroup2.ItemIndex;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasEstoqueIni').Value  := frmMenu.ConvInteiro(edtDiasEstoqueIni.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasEstoqueFim').Value  := frmMenu.ConvInteiro(edtDiasEstoqueFim.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@cReferencia').Value      := edtReferencia.Text ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iGiroIdeal').Value       := frmMenu.ConvInteiro(edtGiroIdeal.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iDiasSemEstoque').Value  := frmMenu.ConvInteiro(edtDiasEstoqueZerado.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iOrdenacao').Value       := RadioGroup4.ItemIndex ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@iTipoOrdenacao').Value   := RadioGroup7.ItemIndex ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
              objRel.SPREL_ACOMPANHA_GIRO_PRODUTO.Open ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '' ;

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ Departamento: ' + trim(edtDepartamento.Text) + '-' + DbEdit2.Text ;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ Categoria: ' + trim(edtCategoria.Text) + '-' + DbEdit3.Text ;

              if (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + '/ SubCategoria: ' + trim(edtSubCategoria.Text) + '-' + DbEdit4.Text ;

              if (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + '/ Segmento: ' + trim(edtSegmento.Text) + '-' + DbEdit5.Text ;

              if (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + '/ Marca: ' + trim(edtMarca.Text) + '-' + DbEdit6.Text ;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ Linha: ' + trim(edtLinha.Text) + '-' + DbEdit8.Text ;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ Cole��o: ' + trim(edtColecao.Text) + '-' + DbEdit9.Text ;

              if (DBEdit7.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Produto: ' + trim(edtGrupoProduto.Text) + '-' + DbEdit7.Text ;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ Classe Produto: ' + trim(edtClasseProduto.Text) + '-' + DbEdit10.Text ;

              if (RadioGroup2.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Agrupamento : Produto Estruturado '
              else cFiltro := cFiltro + '/ Agrupamento : Produto Final ' ;

              cFiltro := cFiltro + '/ Dias de Estoque: ' + trim(edtDiasEstoqueIni.Text) + ' a ' + trim(edtDiasEstoqueFim.Text) ;

              cFiltro := cFiltro + '/ Giro Ideal: ' + trim(edtGiroIdeal.Text) + ' x 1' ;

              cFiltro := cFiltro + '/ Dia Estoque Zerado: ' + trim(edtDiasEstoqueZerado.Text) ;

              cFiltro := cFiltro + '/ Per�odo Venda: ' + edtDtInicial.Text + ' at� ' + edtDtFinal.Text ;

              if (trim(edtReferencia.Text) <> '') then
                  cFiltro := cFiltro + '/ Refer�ncia: ' + trim(edtReferencia.Text);

              cFiltro := cFiltro + '/ Ordena��o: ' ;

              if (RadioGroup4.ItemIndex = 0) then cFiltro := cFiltro + ' Dias de Estoque' ;
              if (RadioGroup4.ItemIndex = 1) then cFiltro := cFiltro + ' Venda Total' ;
              if (RadioGroup4.ItemIndex = 2) then cFiltro := cFiltro + ' Venda Per�odo' ;
              if (RadioGroup4.ItemIndex = 3) then cFiltro := cFiltro + ' Saldo Atual Estoque' ;
              if (RadioGroup4.ItemIndex = 4) then cFiltro := cFiltro + ' Giro Total' ;
              if (RadioGroup4.ItemIndex = 5) then cFiltro := cFiltro + ' Giro Per�odo' ;
              if (RadioGroup4.ItemIndex = 6) then cFiltro := cFiltro + ' % Venda' ;

              if (RadioGroup7.ItemIndex = 1) then cFiltro := cFiltro + ' DEC ' ;

              objRel.lblFiltro1.Caption := cFiltro ;

              objRel.bZebrado := (RadioGroup5.ItemIndex = 0) ;

              objRel.QuickRep1.PreviewModal;

          end ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptAcompanhaGiroProduto.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TrptAcompanhaGiroProduto.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptAcompanhaGiroProduto.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(TrptAcompanhaGiroProduto) ;

end.
