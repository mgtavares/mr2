inherited frmDistribuicaoOrcamento_Valores: TfrmDistribuicaoOrcamento_Valores
  Left = 243
  Top = 234
  Width = 653
  Height = 277
  Caption = 'Valores'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 637
    Height = 212
  end
  object Label1: TLabel [1]
    Left = 73
    Top = 48
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 47
    Top = 72
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Base'
  end
  object Label3: TLabel [3]
    Left = 65
    Top = 96
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = '% Base'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 120
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#233'todo Distribui'#231#227'o'
  end
  object Label5: TLabel [5]
    Left = 22
    Top = 144
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Or'#231'amento'
  end
  object Label6: TLabel [6]
    Left = 44
    Top = 168
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBEdit6
  end
  inherited ToolBar1: TToolBar
    Width = 637
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 106
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdPlanoConta'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [9]
    Left = 106
    Top = 88
    Width = 65
    Height = 21
    DataField = 'nPercentBase'
    DataSource = DataSource1
    TabOrder = 3
    OnExit = DBEdit3Exit
  end
  object DBEdit6: TDBEdit [10]
    Left = 106
    Top = 160
    Width = 521
    Height = 21
    DataField = 'cOBS'
    DataSource = DataSource1
    TabOrder = 6
  end
  object comboMetodoDistrib: TDBLookupComboBox [11]
    Left = 106
    Top = 112
    Width = 173
    Height = 21
    DataField = 'nCdMetodoDistribOrcamento'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    KeyField = 'nCdMetodoDistribOrcamento'
    ListField = 'cNmMetodoDistribOrcamento'
    ListSource = dsTipoMetodoDistrib
    ParentFont = False
    TabOrder = 4
  end
  object DBEdit4: TDBEdit [12]
    Left = 106
    Top = 136
    Width = 173
    Height = 21
    DataField = 'nValorOrcamentoConta'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit5: TDBEdit [13]
    Tag = 1
    Left = 178
    Top = 40
    Width = 449
    Height = 21
    DataField = 'cNmPlanoConta'
    DataSource = DataSource2
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [14]
    Tag = 1
    Left = 178
    Top = 64
    Width = 449
    Height = 21
    DataField = 'cNmPlanoConta'
    DataSource = DataSource3
    TabOrder = 8
  end
  object cxButton1: TcxButton [15]
    Left = 8
    Top = 192
    Width = 177
    Height = 41
    Caption = 'Distribui'#231#227'o Mensal'
    TabOrder = 9
    OnClick = cxButton1Click
    Glyph.Data = {
      AA040000424DAA04000000000000360000002800000014000000130000000100
      1800000000007404000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF800000800000800000800000FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF800000FFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999999999999999999999999999FFFF
      FF800000FFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000999999FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFF000000009DAA009DAA009DAA009DAA000000999999FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      00000000FFFF9EF0FF9EF0FF009DAA000000999999FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000
      FFFF9EF0FF9EF0FF009DAA000000999999800000800000800000800000800000
      800000800000800000800000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FF
      FF00FFFF00FFFF000000999999FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000
      000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF800000FFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      800000FFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
      0000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000008000008000008000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit2: TER2LookupDBEdit [16]
    Left = 106
    Top = 64
    Width = 65
    Height = 21
    DataField = 'nCdPlanoContaBase'
    DataSource = DataSource1
    TabOrder = 2
    OnExit = DBEdit2Exit
    CodigoLookup = 41
    QueryLookup = qryPlanoContaBase
  end
  object qryItemOrcamento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemOrcamentoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemOrcamento'
      'WHERE nCdItemOrcamento = :nPK')
    Left = 376
    Top = 112
    object qryItemOrcamentonCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryItemOrcamentonCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryItemOrcamentonCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryItemOrcamentonCdPlanoContaBase: TIntegerField
      FieldName = 'nCdPlanoContaBase'
    end
    object qryItemOrcamentonPercentBase: TBCDField
      FieldName = 'nPercentBase'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentonCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryItemOrcamentonValorOrcamentoConta: TBCDField
      FieldName = 'nValorOrcamentoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentocOBS: TStringField
      FieldName = 'cOBS'
      Size = 100
    end
    object qryItemOrcamentocOBSGeral: TMemoField
      FieldName = 'cOBSGeral'
      BlobType = ftMemo
    end
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT RTRIM(LTRIM(IsNull(LTRIM(RTRIM(GrupoPlanoConta.cMascaraGr' +
        'upo)),'#39#39') + '#39'.'#39' + IsNull(PlanoConta.cMascara,'#39#39'))) + '#39' - '#39' + Pla' +
        'noConta.cNmPlanoConta as cNmPlanoConta'
      '  FROM PlanoConta'
      
        '       INNER JOIN GrupoPlanoConta ON GrupoPlanoConta.nCdGrupoPla' +
        'noConta = PlanoConta.nCdGrupoPlanoConta'
      ' WHERE PlanoConta.nCdPlanoConta = :nPK')
    Left = 344
    Top = 112
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      ReadOnly = True
      Size = 78
    end
  end
  object qryPlanoContaBase: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPlanoConta'
      
        '      ,RTRIM(LTRIM(IsNull(LTRIM(RTRIM(GrupoPlanoConta.cMascaraGr' +
        'upo)),'#39#39') + '#39'.'#39' + IsNull(PlanoConta.cMascara,'#39#39'))) + '#39' - '#39' + Pla' +
        'noConta.cNmPlanoConta as cNmPlanoConta'
      '  FROM PlanoConta'
      
        '       INNER JOIN GrupoPlanoConta ON GrupoPlanoConta.nCdGrupoPla' +
        'noConta = PlanoConta.nCdGrupoPlanoConta'
      ' WHERE PlanoConta.nCdPlanoConta = :nPK')
    Left = 440
    Top = 112
    object qryPlanoContaBasecNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      ReadOnly = True
      Size = 78
    end
    object qryPlanoContaBasenCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
  end
  object qryTipoMetodoDistrib: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM MetodoDistribOrcamento')
    Left = 312
    Top = 112
    object qryTipoMetodoDistribnCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryTipoMetodoDistribcNmMetodoDistribOrcamento: TStringField
      FieldName = 'cNmMetodoDistribOrcamento'
      Size = 50
    end
    object qryTipoMetodoDistribcFlgLinear: TIntegerField
      FieldName = 'cFlgLinear'
    end
    object qryTipoMetodoDistribcFlgPadrao: TIntegerField
      FieldName = 'cFlgPadrao'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryItemOrcamento
    Left = 536
    Top = 112
  end
  object DataSource2: TDataSource
    DataSet = qryPlanoConta
    Left = 504
    Top = 112
  end
  object DataSource3: TDataSource
    DataSet = qryPlanoContaBase
    Left = 472
    Top = 112
  end
  object dsTipoMetodoDistrib: TDataSource
    DataSet = qryTipoMetodoDistrib
    Left = 408
    Top = 112
  end
  object qryConfereContaBase: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdOrcamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdPlanoConta'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdItemOrcamento'
      '      ,nCdPlanoContaBase'
      '      ,nValorOrcamentoConta'
      '  FROM ItemOrcamento'
      ' WHERE nCdOrcamento  = :nCdOrcamento'
      '   AND nCdPlanoConta = :nCdPlanoConta')
    Left = 576
    Top = 112
    object qryConfereContaBasenCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryConfereContaBasenCdPlanoContaBase: TIntegerField
      FieldName = 'nCdPlanoContaBase'
    end
    object qryConfereContaBasenValorOrcamentoConta: TBCDField
      FieldName = 'nValorOrcamentoConta'
      Precision = 12
      Size = 2
    end
  end
end
