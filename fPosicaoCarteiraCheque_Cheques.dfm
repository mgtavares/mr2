inherited frmPosicaoCarteiraCheque_Cheques: TfrmPosicaoCarteiraCheque_Cheques
  Top = 187
  BorderIcons = [biSystemMenu]
  Caption = 'Cheques'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsCheques
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCheque'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 82
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 116
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 340
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryCheques: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDataInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTabStatusCheque'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria   int'
      '       ,@cDataInicial       varchar(10)'
      '       ,@cDataFinal         varchar(10)'
      '       ,@nCdEmpresa         int'
      '       ,@nCdTabStatusCheque int'
      ''
      'Set @nCdContaBancaria   = :nCdContaBancaria'
      'Set @cDataInicial       = :cDataInicial'
      'Set @nCdEmpresa         = :nCdEmpresa'
      'Set @nCdTabStatusCheque = :nCdTabStatusCheque'
      ''
      'IF (@cDataInicial = '#39#39') Set @cDataInicial = '#39'01/01/1900'#39
      ''
      'SELECT Cheque.nCdCheque'
      '      ,Cheque.nCdBanco'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      '      ,Cheque.cCNPJCPF'
      '      ,cNmTerceiro'
      '  FROM Cheque'
      
        '       LEFT JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdTe' +
        'rceiroResp'
      ' WHERE nCdTabTipoCheque    = 2'
      '   AND nCdTabStatusCheque  = @nCdTabStatusCheque'
      '   AND dDtDeposito         = Convert(DATETIME,@cDataInicial,103)'
      '   AND nCdContaBancariaDep = @nCdContaBancaria'
      '   AND Cheque.nCdEmpresa   = @nCdEmpresa'
      ' ORDER BY iNrCheque')
    Left = 208
    Top = 120
    object qryChequesnCdCheque: TAutoIncField
      DisplayLabel = 'Rela'#231#227'o de Cheques|C'#243'd'
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequesnCdBanco: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequesiNrCheque: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequesnValCheque: TBCDField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Valor'
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequescCNPJCPF: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequescNmTerceiro: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Cliente Respons'#225'vel'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsCheques: TDataSource
    DataSet = qryCheques
    Left = 248
    Top = 120
  end
end
