�
 TRPTINVENTARIO_RESUMO 0! TPF0TrptInventario_ResumorptInventario_ResumoLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetSP_PREPARA_RESUMO_INVENTARIOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1Left&Top&Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������	@������v�	@ BandTyperbPageHeader TQRLabelQRLabel1LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU��@UUUUUUM�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Inventário Físico - ResumoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblEmpresaLeftTopWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�TopWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      H�	@UUUUUU��@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel3Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Pág.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData2Left�TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@TUUUUU��	@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel2Left Top8Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@������*�@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Inventário:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4Left/TopHWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@TUUUUU��@      ��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionEmpresa:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellblEmpresa2Left\TopHWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@      ��@UUUUUU)�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblInventarioLeft\Top8Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@������*�@      Ж@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblInventarioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel6LeftTopXWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������@VUUUUU��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionLocal Estoque:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellblLocalEstoqueLeft\TopXWidth!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@VUUUUU��@UUUUUU)�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblLocalEstoqueColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel5LeftTop� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@UUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   RESUMO DO INVENTÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText1Left� Top� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnValPrecoCustoLogicoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel7LeftTop� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Total Preço de CustoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel8Left� Top� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@VUUUUU��@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Antes InventárioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel9Left0Top� WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU�@VUUUUU��@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Após inventárioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel10Left�Top� Width.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ޏ	@VUUUUU��@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption
   DiferençaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel11LeftTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������	@VUUUUU��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionPerc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2LeftTop� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnValPrecoCustoFisicoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13Left/Top� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@TUUUUU��@      ��@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# itens contadosColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText5LeftTop� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@      ��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdeItensContadosFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText6LeftTop WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@UUUUUUU�@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdePecasOKFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14Left%Top Width[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������@UUUUUUU�@TUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# Igual ao SistemaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel15Left TopWidth`HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@�������@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# Menor que SistemaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText7LeftTopWidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@�������@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdePecasLogicoMaiorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8LeftTop WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@      ��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdePecasLogicoMenorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel16Left Top Width`HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@      ��@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# Maior que SistemaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblDifCustoLeft�Top� Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������:�	@       �@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlblDifCustoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	lblPercOKLeft�Top Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@UUUUUUU�@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption	lblPercOKColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblPercLogicoMaiorLeft�TopWidth9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@�������@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlblPercLogicoMaiorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblPercLogicoMenorLeft�Top Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@      ��@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlblPercLogicoMenorColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel17LeftTop@Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@��������@      8�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption$   ANÁLISE DIFERENÇAS - EM PERCENTUALColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18Left8TopxWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@TUUUUU��@������
�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   até 1%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText9LeftfTopxWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@TUUUUU��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif1Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText10Left� TopxWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ؒ@TUUUUU��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif1Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText11LeftVTopxWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      8�@TUUUUU��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif1Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel19Left� TophWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU=�@�������@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# ItensColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel20LeftTophWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������ֽ@�������@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel21LefteTophWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      $�@�������@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption% AcumuladoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText12LeftVTop�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      8�@UUUUUU��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif3Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText13Left� Top�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ؒ@UUUUUU��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif3Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText14LeftfTop�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUU��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif3Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel22Left3Top�Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@UUUUUU��	@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption1% a 3%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText15LeftVTop�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      8�@      ��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif5Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText16Left� Top�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ؒ@      ��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif5Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText17LeftfTop�WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@      ��	@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif5Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel23Left3Top�Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��@      ��	@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption3% a 5%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText18LeftQTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@������:�	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif10Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText19Left� Top�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@������:�	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif10Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText20LeftaTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������R�@������:�	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif10Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel24Left.Top�Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@������:�	@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption5% a 10%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText21LeftQTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@UUUUUU��	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif15Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText22Left� Top�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@UUUUUU��	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif15Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText23LeftaTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������R�@UUUUUU��	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif15Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel25Left)Top�Width.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@UUUUUU��	@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption	10% a 15%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText24LeftQTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@      Ж	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif16Perc_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText25Left� Top�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@      Ж	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif16Perc_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText26LeftaTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������R�@      Ж	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo2	DataFieldnDif16Perc_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel27LeftTop�Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU��@      Ж	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionmais que 15%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel28LeftTop�Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@UUUUUUe�	@      8�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption$   ANÁLISE DIFERENÇAS - EM QUANTIDADEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel29LeftTop WidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@     @�@�������	@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   até 50 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText27LeftuTop Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ț@�������	@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif50_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText28Left� Top Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ĝ@�������	@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif50_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText29LefteTop Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      $�@�������	@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif50_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel30Left� TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU=�@      ��	@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption# ItensColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel31LeftTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������ֽ@      ��	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel32LefteTopWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      $�@      ��	@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption% AcumuladoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText30Left`Top0Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@UUUUUU5�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif100_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText31Left� Top0Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@UUUUUU5�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif100_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText32LeftpTop0Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@UUUUUU5�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif100_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel33LeftTop0WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@UUUUUU5�	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption51 a 100 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText33Left`Top@Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@      ��	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif150_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText34Left� Top@Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@      ��	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif150_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText35LeftpTop@Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@      ��	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif150_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel34LeftTop@WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@      ��	@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption101 a 150 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText36Left`TopPWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif200_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText37Left� TopPWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif200_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText38LeftpTopPWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif200_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel35LeftTopPWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@��������	@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption151 a 200 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText39Left`Top`Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@UUUUUU�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif250_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText40Left� Top`Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@UUUUUU�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif250_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText41LeftpTop`Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@UUUUUU�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif250_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel36LeftTop`WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@UUUUUU�	@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption201 a 250 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText42Left`ToppWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@      `�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif300_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText43Left� ToppWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@      `�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif300_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText44LeftpToppWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@      `�	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif300_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel37LeftToppWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@       �@      `�	@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption251 a 300 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel38LeftTop�WidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@VUUUUU��@��������	@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionmais 300 unids.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText45LeftpTop�Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif301_QtdeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText46Left� Top�Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUu�@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif301_PercFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText47Left`Top�Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@VUUUUU��@��������	@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryTemp_Resumo3	DataFieldnDif301_AcumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel39LeftTophWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������@UUUUUU��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionData Abertura:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellblAberturaLeft\TophWidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@UUUUUU��@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblAberturaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel40LeftTopxWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������*�@      ��@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionData Fechamento:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabellblFechamentoLeft\TopxWidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������j�@      ��@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionlblFechamentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel41LeftTop� WidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@�������@UUUUUU��@      ؒ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionQuant. Estoque (Total)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText48Left� Top� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������@UUUUUU��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdePecasLogicoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText49LeftTop� WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU5�@UUUUUU��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryTemp_Resumo1	DataFieldnQtdePecasFisicoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask,0
ParentFontTransparentWordWrap	FontSize  TQRLabellblDifPecasLeft�Top� Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������:�	@UUUUUU��@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionlblDifPecasColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape2Left Top�Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          UUUUUUU� �������t�	@ 	Pen.WidthShape
qrsHorLine  TQRShapeQRShape1Left Top'Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                `�@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabellblPercDifCustoLeft�Top� Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@       �@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0,00ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblPercDifQuantidadeLeft�Top� Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      ��	@UUUUUU��@      Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0,00ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel12Left'Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU;�	@       �@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel43Left'Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU;�	@UUUUUU��@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel44Left'Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU;�	@UUUUUUU�@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel45Left'TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU;�	@�������@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel46Left'Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUU;�	@      ��@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel47Left�TopxWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@TUUUUU��@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel48Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@UUUUUU��	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel49Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@      ��	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel50Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@������:�	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel51Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@UUUUUU��	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel52Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@      Ж	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel53Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@�������	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel54Left�Top0WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@UUUUUU5�	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel55Left�Top@WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@      ��	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel56Left�TopPWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@��������	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel57Left�Top`WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@UUUUUU�	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel58Left�ToppWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@      `�	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel59Left�Top�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������@�	@��������	@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2Left&Top)Width�Height'Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      `�@������v�	@ BandTyperbDetail  TQRBandQRBand3Left&TopPWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values������*�@������v�	@ BandTyperbPageFooter TQRShapeQRShape5Left Top�Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          UUUUUUU� �UUUUUU��	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel26LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUUU�@������Z�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption6   ER2SOFT - Soluções inteligentes para o seu negócio.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel42Left|TopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      X�	@UUUUUUU�@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionwww.er2soft.com.brColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize   TADOStoredProcSP_PREPARA_RESUMO_INVENTARIO
ConnectionfrmMenu.ConnectionProcedureNameSP_PREPARA_RESUMO_INVENTARIO;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@nCdInventario
Attributes
paNullable DataType	ftInteger	Precision
Value   Left� Top@  	TADOQueryqryTemp_Resumo1
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *  FROM #Temp_Resumo1 Left Top�  	TBCDField!qryTemp_Resumo1nQtdeItensContados	FieldNamenQtdeItensContados	PrecisionSize  	TBCDField#qryTemp_Resumo1nValPrecoCustoLogico	FieldNamenValPrecoCustoLogico	PrecisionSize  	TBCDField#qryTemp_Resumo1nValPrecoCustoFisico	FieldNamenValPrecoCustoFisico	PrecisionSize  	TBCDField#qryTemp_Resumo1nValPrecoVendaLogico	FieldNamenValPrecoVendaLogico	PrecisionSize  	TBCDField#qryTemp_Resumo1nValPrecoVendaFisico	FieldNamenValPrecoVendaFisico	PrecisionSize  	TBCDFieldqryTemp_Resumo1nQtdePecasLogico	FieldNamenQtdePecasLogico	Precision  	TBCDFieldqryTemp_Resumo1nQtdePecasFisico	FieldNamenQtdePecasFisico	Precision  	TBCDFieldqryTemp_Resumo1nQtdePecasOK	FieldNamenQtdePecasOK	Precision  	TBCDField$qryTemp_Resumo1nQtdePecasLogicoMenor	FieldNamenQtdePecasLogicoMenor	Precision  	TBCDField$qryTemp_Resumo1nQtdePecasLogicoMaior	FieldNamenQtdePecasLogicoMaior	Precision   	TADOQueryqryTemp_Resumo2
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *  FROM #Temp_Resumo2 LeftXTop�  	TBCDField"qryTemp_Resumo2nQtdeItensDiferenca	FieldNamenQtdeItensDiferenca	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif1Perc_Qtde	FieldNamenDif1Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif1Perc_Perc	FieldNamenDif1Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif1Perc_Acum	FieldNamenDif1Perc_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif3Perc_Qtde	FieldNamenDif3Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif3Perc_Perc	FieldNamenDif3Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif3Perc_Acum	FieldNamenDif3Perc_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif5Perc_Qtde	FieldNamenDif5Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif5Perc_Perc	FieldNamenDif5Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif5Perc_Acum	FieldNamenDif5Perc_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif10Perc_Qtde	FieldNamenDif10Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif10Perc_Perc	FieldNamenDif10Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif10Perc_Acum	FieldNamenDif10Perc_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif15Perc_Qtde	FieldNamenDif15Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif15Perc_Perc	FieldNamenDif15Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif15Perc_Acum	FieldNamenDif15Perc_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif16Perc_Qtde	FieldNamenDif16Perc_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo2nDif16Perc_Perc	FieldNamenDif16Perc_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo2nDif16Perc_Acum	FieldNamenDif16Perc_Acum	PrecisionSize   	TADOQueryqryTemp_Resumo3
ConnectionfrmMenu.Connection
Parameters SQL.StringsSELECT *  FROM #Temp_Resumo3 LeftxTop�  	TBCDField"qryTemp_Resumo3nQtdeItensDiferenca	FieldNamenQtdeItensDiferenca	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif50_Qtde	FieldNamenDif50_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif50_Perc	FieldNamenDif50_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif50_Acum	FieldNamenDif50_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif100_Qtde	FieldNamenDif100_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif100_Perc	FieldNamenDif100_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif100_Acum	FieldNamenDif100_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif150_Qtde	FieldNamenDif150_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif150_Perc	FieldNamenDif150_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif150_Acum	FieldNamenDif150_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif200_Qtde	FieldNamenDif200_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif200_Perc	FieldNamenDif200_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif200_Acum	FieldNamenDif200_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif250_Qtde	FieldNamenDif250_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif250_Perc	FieldNamenDif250_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif250_Acum	FieldNamenDif250_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif300_Qtde	FieldNamenDif300_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif300_Perc	FieldNamenDif300_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif300_Acum	FieldNamenDif300_Acum	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif301_Qtde	FieldNamenDif301_Qtde	PrecisionSize   	TBCDFieldqryTemp_Resumo3nDif301_Perc	FieldNamenDif301_Perc	PrecisionSize  	TBCDFieldqryTemp_Resumo3nDif301_Acum	FieldNamenDif301_Acum	PrecisionSize   TADOCommandqryPreparaTempCommandTextE  IF (OBJECT_ID('tempdb..#Temp_Resumo1') IS NULL)
BEGIN

    CREATE TABLE #Temp_Resumo1 (nQtdeItensContados    decimal(12,2) default 0 not null
                                ,nValPrecoCustoLogico  decimal(12,2) default 0 not null
                                ,nValPrecoCustoFisico  decimal(12,2) default 0 not null
                                ,nValPrecoVendaLogico  decimal(12,2) default 0 not null
                                ,nValPrecoVendaFisico  decimal(12,2) default 0 not null
                                ,nQtdePecasLogico      decimal(12,4) default 0 not null
                                ,nQtdePecasFisico      decimal(12,4) default 0 not null
                                ,nQtdePecasOK          decimal(12,4) default 0 not null
                                ,nQtdePecasLogicoMenor decimal(12,4) default 0 not null
                                ,nQtdePecasLogicoMaior decimal(12,4) default 0 not null)

END

IF (OBJECT_ID('tempdb..#Temp_Resumo2') IS NULL)
BEGIN

    CREATE TABLE #Temp_Resumo2 (nQtdeItensDiferenca  decimal(12,2) default 0 not null
                                ,nDif1Perc_Qtde       decimal(12,0) default 0 not null
                                ,nDif1Perc_Perc       decimal(12,2) default 0 not null
                                ,nDif1Perc_Acum       decimal(12,2) default 0 not null
                                ,nDif3Perc_Qtde       decimal(12,0) default 0 not null
                                ,nDif3Perc_Perc       decimal(12,2) default 0 not null
                                ,nDif3Perc_Acum       decimal(12,2) default 0 not null
                                ,nDif5Perc_Qtde       decimal(12,0) default 0 not null
                                ,nDif5Perc_Perc       decimal(12,2) default 0 not null
                                ,nDif5Perc_Acum       decimal(12,2) default 0 not null
                                ,nDif10Perc_Qtde      decimal(12,0) default 0 not null
                                ,nDif10Perc_Perc      decimal(12,2) default 0 not null
                                ,nDif10Perc_Acum      decimal(12,2) default 0 not null
                                ,nDif15Perc_Qtde      decimal(12,0) default 0 not null
                                ,nDif15Perc_Perc      decimal(12,2) default 0 not null
                                ,nDif15Perc_Acum      decimal(12,2) default 0 not null
                                ,nDif16Perc_Qtde      decimal(12,0) default 0 not null
                                ,nDif16Perc_Perc      decimal(12,2) default 0 not null
                                ,nDif16Perc_Acum      decimal(12,2) default 0 not null)

END

IF (OBJECT_ID('tempdb..#Temp_Resumo3') IS NULL)
BEGIN

    CREATE TABLE #Temp_Resumo3 (nQtdeItensDiferenca decimal(12,2) default 0 not null
                                ,nDif50_Qtde         decimal(12,0) default 0 not null
                                ,nDif50_Perc         decimal(12,2) default 0 not null
                                ,nDif50_Acum         decimal(12,2) default 0 not null
                                ,nDif100_Qtde        decimal(12,0) default 0 not null
                                ,nDif100_Perc        decimal(12,2) default 0 not null
                                ,nDif100_Acum        decimal(12,2) default 0 not null
                                ,nDif150_Qtde        decimal(12,0) default 0 not null
                                ,nDif150_Perc        decimal(12,2) default 0 not null
                                ,nDif150_Acum        decimal(12,2) default 0 not null
                                ,nDif200_Qtde        decimal(12,0) default 0 not null
                                ,nDif200_Perc        decimal(12,2) default 0 not null
                                ,nDif200_Acum        decimal(12,2) default 0 not null
                                ,nDif250_Qtde        decimal(12,0) default 0 not null
                                ,nDif250_Perc        decimal(12,2) default 0 not null
                                ,nDif250_Acum        decimal(12,2) default 0 not null
                                ,nDif300_Qtde        decimal(12,0) default 0 not null
                                ,nDif300_Perc        decimal(12,2) default 0 not null
                                ,nDif300_Acum        decimal(12,2) default 0 not null
                                ,nDif301_Qtde        decimal(12,0) default 0 not null
                                ,nDif301_Perc        decimal(12,2) default 0 not null
                                ,nDif301_Acum        decimal(12,2) default 0 not null)

END


ConnectionfrmMenu.Connection
Parameters Left�Topp   