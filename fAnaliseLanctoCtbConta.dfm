inherited frmAnaliseLanctoCtbConta: TfrmAnaliseLanctoCtbConta
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'An'#225'lise de Lan'#231'amentos Cont'#225'beis'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 153
    Width = 1152
    Height = 661
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    ButtonWidth = 115
    inherited ToolButton1: TToolButton
      Caption = 'Executar Consulta'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 124
    Align = alTop
    Caption = 'Filtro'
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 82
      Top = 48
      Width = 29
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta'
    end
    object Label3: TLabel
      Tag = 1
      Left = 9
      Top = 96
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Lan'#231'amentos'
    end
    object Label6: TLabel
      Tag = 1
      Left = 190
      Top = 96
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label1: TLabel
      Tag = 1
      Left = 35
      Top = 24
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo de Conta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 16
      Top = 72
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Unidade de Neg'#243'cio'
    end
    object edtConta: TMaskEdit
      Left = 114
      Top = 40
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnChange = edtContaChange
      OnExit = edtContaExit
      OnKeyDown = edtContaKeyDown
    end
    object edtDtInicial: TMaskEdit
      Left = 114
      Top = 88
      Width = 72
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
      OnChange = edtDtInicialChange
    end
    object edtDtFinal: TMaskEdit
      Left = 210
      Top = 88
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
      OnChange = edtDtFinalChange
    end
    object edtGrupo: TMaskEdit
      Left = 114
      Top = 16
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnChange = edtGrupoChange
      OnExit = edtGrupoExit
      OnKeyDown = edtGrupoKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 184
      Top = 16
      Width = 537
      Height = 21
      DataField = 'cNmGrupoPlanoConta'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 184
      Top = 40
      Width = 537
      Height = 21
      DataField = 'cNmPlanoConta'
      DataSource = DataSource2
      TabOrder = 6
    end
    object edtUnidadeNegocio: TMaskEdit
      Left = 114
      Top = 64
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnChange = edtUnidadeNegocioChange
      OnExit = edtUnidadeNegocioExit
      OnKeyDown = edtUnidadeNegocioKeyDown
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 184
      Top = 64
      Width = 537
      Height = 21
      DataField = 'cNmUnidadeNegocio'
      DataSource = DataSource3
      TabOrder = 7
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 153
    Width = 1152
    Height = 661
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsConsulta
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nSaldoTit'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupFooters = gfAlwaysVisible
      object cxGridDBTableView1cNmUnidadeNegocio: TcxGridDBColumn
        Caption = 'Unidade Neg'#243'cio'
        DataBinding.FieldName = 'cNmUnidadeNegocio'
        Visible = False
        GroupIndex = 0
      end
      object cxGridDBTableView1cNmGrupoPlanoConta: TcxGridDBColumn
        Caption = 'Grupo Conta'
        DataBinding.FieldName = 'cNmGrupoPlanoConta'
      end
      object cxGridDBTableView1cNmPlanoConta: TcxGridDBColumn
        Caption = 'Conta'
        DataBinding.FieldName = 'cNmPlanoConta'
      end
      object cxGridDBTableView1nValor: TcxGridDBColumn
        Caption = 'Valor Total'
        DataBinding.FieldName = 'nValor'
        HeaderAlignmentHorz = taRightJustify
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryGrupoPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoPlanoConta'
      '      ,cNmGrupoPlanoConta'
      '  FROM GrupoPlanoConta'
      '  WHERE nCdGrupoPlanoConta = :nPK')
    Left = 328
    Top = 176
    object qryGrupoPlanoContanCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryGrupoPlanoContacNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryGrupoPlanoConta
    Left = 384
    Top = 240
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdGrupoPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdGrupoPlanoConta int'
      ''
      'Set @nCdGrupoPlanoConta = :nCdGrupoPlanoConta'
      ''
      
        'SELECT nCdPlanoConta, cNmPlanoConta FROM PlanoConta WHERE cFlgLa' +
        'nc = 1'
      'AND nCdPlanoConta = :nPK'
      
        'AND ((@nCdGrupoPlanoConta = 0) OR (nCdGrupoPlanoConta = @nCdGrup' +
        'oPlanoConta))')
    Left = 304
    Top = 248
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryPlanoConta
    Left = 392
    Top = 248
  end
  object qryConsulta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdGrupoPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa         int'
      '       ,@nCdUnidadeNegocio  int'
      '       ,@nCdGrupoPlanoConta int'
      '       ,@nCdPlanoConta      int'
      '       ,@dDtInicial         varchar(10)'
      '       ,@dDtFinal           varchar(10)'
      ''
      'Set @nCdEmpresa         = :nCdEmpresa'
      'Set @nCdUnidadeNegocio  = :nCdUnidadeNegocio'
      'Set @nCdGrupoPlanoConta = :nCdGrupoPlanoConta'
      'Set @nCdPlanoConta      = :nCdPlanoConta'
      'Set @dDtInicial         = :dDtInicial'
      'Set @dDtFinal           = :dDtFinal'
      ''
      'SELECT UnidadeNegocio.cNmUnidadeNegocio'
      '      ,GrupoPlanoConta.cNmGrupoPlanoConta'
      '      ,PlanoConta.cNmPlanoConta'
      '      ,PlanoConta.nCdPlanoConta'
      '      ,UnidadeNegocio.nCdUnidadeNegocio'
      '      ,Sum(LanctoPlanoConta.nValLancto) nValor'
      '  FROM LanctoPlanoConta'
      
        '       LEFT JOIN GrupoPlanoConta        ON GrupoPlanoConta.nCdGr' +
        'upoPlanoConta = LanctoPlanoConta.nCdGrupoPlanoConta'
      
        '       LEFT JOIN PlanoConta             ON PlanoConta.nCdPlanoCo' +
        'nta           = LanctoPlanoConta.nCdPlanoConta'
      
        '       LEFT JOIN UnidadeNegocio         ON UnidadeNegocio.nCdUni' +
        'dadeNegocio   = LanctoPlanoConta.nCdUnidadeNegocio'
      
        '       LEFT JOIN Usuario                ON Usuario.nCdUsuario   ' +
        '              = LanctoPlanoConta.nCdUsuarioCad'
      
        '       LEFT JOIN SP                     ON SP.nCdSP             ' +
        '              = LanctoPlanoConta.nCdSP'
      
        '       LEFT JOIN Recebimento            ON Recebimento.nCdRecebi' +
        'mento         = LanctoPlanoConta.nCdRecebimento'
      
        '       LEFT JOIN Pedido                 ON Pedido.nCdPedido     ' +
        '              = LanctoPlanoConta.nCdPedido'
      
        '       LEFT JOIN Terceiro TerceiroSP    ON TerceiroSP.nCdTerceir' +
        'o             = SP.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroReceb ON TerceiroReceb.nCdTerc' +
        'eiro          = Recebimento.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroPed   ON TerceiroPed.nCdTercei' +
        'ro            = Pedido.nCdTerceiro'
      ' WHERE LanctoPlanoConta.nCdEmpresa = @nCdEmpresa'
      
        '   AND ((@nCdUnidadeNegocio = 0)   OR (LanctoPlanoConta.nCdUnida' +
        'deNegocio  = @nCdUnidadeNegocio))'
      
        '   AND ((@nCdGrupoPlanoConta = 0)  OR (LanctoPlanoConta.nCdGrupo' +
        'PlanoConta = @nCdGrupoPlanoConta))'
      
        '   AND ((@nCdPlanoConta = 0)       OR (LanctoPlanoConta.nCdPlano' +
        'Conta      = @nCdPlanoConta))'
      
        '   AND LanctoPlanoConta.dDtCompetencia >= Convert(DATETIME,@dDtI' +
        'nicial,103)'
      
        '   AND LanctoPlanoConta.dDtCompetencia  < Convert(DATETIME,@dDtF' +
        'inal,103) + 1'
      ' GROUP BY UnidadeNegocio.cNmUnidadeNegocio'
      '         ,GrupoPlanoConta.cNmGrupoPlanoConta'
      '         ,PlanoConta.cNmPlanoConta'
      '         ,PlanoConta.nCdPlanoConta'
      '         ,UnidadeNegocio.nCdUnidadeNegocio'
      '')
    Left = 328
    Top = 336
    object qryConsultacNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
    object qryConsultacNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryConsultacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryConsultanValor: TBCDField
      FieldName = 'nValor'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryConsultanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryConsultanCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
  end
  object dsConsulta: TDataSource
    DataSet = qryConsulta
    Left = 264
    Top = 336
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio, cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 184
    Top = 240
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 400
    Top = 256
  end
end
