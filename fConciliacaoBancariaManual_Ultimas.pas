unit fConciliacaoBancariaManual_Ultimas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmConciliacaoBancariaManual_Ultimas = class(TfrmProcesso_Padrao)
    qryConciliacao: TADOQuery;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryConciliacaonCdConciliacaoBancaria: TAutoIncField;
    qryConciliacaodDtExtrato: TDateTimeField;
    qryConciliacaonSaldoContaBancaria: TBCDField;
    qryConciliacaonValCredito: TBCDField;
    qryConciliacaonValDebito: TBCDField;
    qryConciliacaonCdUsuario: TIntegerField;
    qryConciliacaocNmUsuario: TStringField;
    cxGrid1DBTableView1nCdConciliacaoBancaria: TcxGridDBColumn;
    cxGrid1DBTableView1dDtExtrato: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoContaBancaria: TcxGridDBColumn;
    cxGrid1DBTableView1nValCredito: TcxGridDBColumn;
    cxGrid1DBTableView1nValDebito: TcxGridDBColumn;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    qryConciliacaonCdContaBancaria: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliacaoBancariaManual_Ultimas: TfrmConciliacaoBancariaManual_Ultimas;

implementation

uses fMenu, fConciliaBancoManual, rExtratoContaBancaria_view;

{$R *.dfm}

procedure TfrmConciliacaoBancariaManual_Ultimas.FormShow(Sender: TObject);
begin
  inherited;

  cxGrid1.Align := alClient ;

  if (qryConciliacao.Eof) then
  begin
      ToolButton1.Click;
  end ;

end;

procedure TfrmConciliacaoBancariaManual_Ultimas.ToolButton1Click(
  Sender: TObject);
var
  objForm : TfrmConciliaBancoManual ;
begin
  {inherited;}

  objForm := TfrmConciliaBancoManual.Create( Self ) ;

  PosicionaQuery(objForm.qryContaBancaria, qryConciliacao.Parameters.ParamByName('nPK').Value) ;

  objForm.qryNaoConciliados.Close ;
  objForm.qryConciliados.Close ;
  objForm.qrySaldoConta.Close ;

  objForm.DBEdit5.Enabled := True ;

  showForm( objForm , TRUE ) ;

  qryConciliacao.Close ;
  qryConciliacao.Open  ;

end;

procedure TfrmConciliacaoBancariaManual_Ultimas.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objRel : TrptExtratoContaBancaria_view ;
begin
  inherited;

  if not qryConciliacao.Eof then
  begin

      objRel := TrptExtratoContaBancaria_view.Create( Self ) ;

      objRel.SPREL_EXTRATO_BANCARIO.Close ;
      objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@nCdContaBancaria').Value := qryConciliacaonCdContaBancaria.Value ;
      objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtInicial').Value       := qryConciliacaodDtExtrato.AsString ;
      objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtFinal').Value         := qryConciliacaodDtExtrato.AsString ;
      objRel.SPREL_EXTRATO_BANCARIO.Open ;

      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRel.lblPeriodo.Caption := qryConciliacaodDtExtrato.asString + ' at� ' + qryConciliacaodDtExtrato.asString ;

      try
          try
              {--visualiza o relat�rio--}

              objRel.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRel);
      end;

  end ;

end;

end.
