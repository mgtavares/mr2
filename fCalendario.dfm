inherited frmCalendario: TfrmCalendario
  Left = 273
  Top = 137
  Width = 878
  Height = 521
  Caption = 'Calend'#225'rio'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 862
    Height = 458
  end
  object Label1: TLabel [1]
    Left = 20
    Top = 51
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 123
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 14
    Top = 76
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label4: TLabel [4]
    Left = 36
    Top = 100
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label5: TLabel [5]
    Left = 36
    Top = 147
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ano'
    FocusControl = DBEdit5
  end
  inherited ToolBar2: TToolBar
    Width = 862
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 65
    Top = 46
    Width = 65
    Height = 19
    DataField = 'nCdCalendario'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [8]
    Left = 65
    Top = 118
    Width = 718
    Height = 19
    DataField = 'cNmCalendario'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit5: TDBEdit [9]
    Left = 65
    Top = 142
    Width = 65
    Height = 19
    DataField = 'iAno'
    DataSource = dsMaster
    MaxLength = 4
    TabOrder = 4
  end
  object DBEdit3: TDBEdit [10]
    Tag = 1
    Left = 133
    Top = 70
    Width = 650
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 6
  end
  object DBEdit4: TDBEdit [11]
    Tag = 1
    Left = 133
    Top = 94
    Width = 650
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 7
  end
  object cxPageControl1: TcxPageControl [12]
    Left = 65
    Top = 181
    Width = 720
    Height = 289
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 8
    ClientRectBottom = 285
    ClientRectLeft = 4
    ClientRectRight = 716
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Feriados'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 712
        Height = 261
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemCalendario
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFeriado'
            Footers = <>
            Title.Caption = 'Feriados|Data'
          end
          item
            EditButtons = <>
            FieldName = 'cNmItemCalendario'
            Footers = <>
            Title.Caption = 'Feriados|Descri'#231#227'o'
            Width = 560
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object ER2LkEmpresa: TER2LookupDBEdit [13]
    Left = 65
    Top = 70
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 1
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object ER2LkLoja: TER2LookupDBEdit [14]
    Left = 65
    Top = 94
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 2
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT * '
      '   FROM Calendario'
      'WHERE nCdCalendario = :nPK')
    Left = 648
    Top = 288
    object qryMasternCdCalendario: TIntegerField
      FieldName = 'nCdCalendario'
    end
    object qryMastercNmCalendario: TStringField
      FieldName = 'cNmCalendario'
      Size = 50
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasteriAno: TIntegerField
      FieldName = 'iAno'
    end
  end
  inherited dsMaster: TDataSource
    Left = 680
    Top = 288
  end
  inherited qryID: TADOQuery
    Left = 648
    Top = 320
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 680
    Top = 352
  end
  inherited qryStat: TADOQuery
    Left = 680
    Top = 320
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 648
    Top = 352
  end
  inherited ImageList1: TImageList
    Left = 680
    Top = 384
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 648
    Top = 224
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '  FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 648
    Top = 256
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 680
    Top = 224
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 680
    Top = 256
  end
  object qryItemCalendario: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemCalendarioBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM ItemCalendario'
      'WHERE nCdCalendario = :nPK'
      'ORDER BY dDtFeriado')
    Left = 648
    Top = 192
    object qryItemCalendarionCdItemCalendario: TIntegerField
      FieldName = 'nCdItemCalendario'
    end
    object qryItemCalendariocNmItemCalendario: TStringField
      FieldName = 'cNmItemCalendario'
      Size = 100
    end
    object qryItemCalendarionCdCalendario: TIntegerField
      FieldName = 'nCdCalendario'
    end
    object qryItemCalendariodDtFeriado: TDateTimeField
      FieldName = 'dDtFeriado'
    end
  end
  object dsItemCalendario: TDataSource
    DataSet = qryItemCalendario
    Left = 680
    Top = 192
  end
  object qryVerificaCalendario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iAno'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCalendario'
      '  FROM Calendario'
      ' WHERE nCdCalendario <> :nPK'
      '   AND nCdEmpresa     = :nCdEmpresa'
      '   AND ((nCdLoja      = :nCdLoja) OR (ISNULL(nCdLoja,0) = 0))'
      '   AND iAno           = :iAno'
      '')
    Left = 648
    Top = 160
    object qryVerificaCalendarionCdCalendario: TIntegerField
      FieldName = 'nCdCalendario'
    end
  end
end
