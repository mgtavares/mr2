unit fConsultaPosicaoEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid,  comObj, ExcelXP, Menus,
  ER2Excel;

type
  TfrmConsultaPosicaoEstoque = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryGrupoEstoque: TADOQuery;
    dsEmpresa: TDataSource;
    dsGrupoEstoque: TDataSource;
    dsResultado: TDataSource;
    qryGrupoProduto: TADOQuery;
    dsGrupoProduto: TDataSource;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    qryResultado: TADOQuery;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmProduto: TStringField;
    qryResultadonQtdeEstoque: TBCDField;
    qryResultadocNmEmpresa: TStringField;
    qryResultadocNmLocalEstoque: TStringField;
    qryResultadonQtdeDisponivel: TBCDField;
    qryResultadonValCusto: TBCDField;
    qryResultadodDtUltReceb: TDateTimeField;
    qryResultadodDtUltVenda: TDateTimeField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    dsLocalEstoque: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    dsProduto: TDataSource;
    qryResultadocUnidadeMedida: TStringField;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit10: TDBEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    MaskEdit1: TMaskEdit;
    DBEdit2: TDBEdit;
    MaskEdit2: TMaskEdit;
    DBEdit4: TDBEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryResultadocNmGrupoProduto: TStringField;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1nValCusto: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltReceb: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltVenda: TcxGridDBColumn;
    cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoProduto: TcxGridDBColumn;
    edtCdLoja: TMaskEdit;
    Label9: TLabel;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit3: TDBEdit;
    dsLoja: TDataSource;
    cxGrid1DBTableView1SaldoEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1SaldoDisponivel: TcxGridDBColumn;
    qryResultadonValSaldoDisponivel: TBCDField;
    qryResultadonValSaldoEmpenhado: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryResultadonQtdeRecebida: TBCDField;
    cxGrid1DBTableView1nQtdeRecebida: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    PedidodeCompra1: TMenuItem;
    DetalharEmpenho1: TMenuItem;
    ltimosRecebimentos1: TMenuItem;
    qryResultadonCdLoja: TIntegerField;
    RadioGroup1: TRadioGroup;
    ER2Excel: TER2Excel;
    rgProdutoAtivoWeb: TRadioGroup;
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdLojaExit(Sender: TObject);
    procedure edtCdLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton4Click(Sender: TObject);
    procedure PedidodeCompra1Click(Sender: TObject);
    procedure DetalharEmpenho1Click(Sender: TObject);
    procedure ltimosRecebimentos1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPosicaoEstoque: TfrmConsultaPosicaoEstoque;

implementation

uses fLookup_Padrao, fMenu, fPedidoAbertoProduto, fMRP_DetalhaEmpenho, fProdutoERP_UltimasCompras;

{$R *.dfm}

procedure TfrmConsultaPosicaoEstoque.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEstoque.Close ;
  PosicionaQuery(qryGrupoEstoque, MaskEdit4.Text) ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, MaskEdit6.Text) ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(85);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEstoque, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPosicaoEstoque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  {qryResultadoPlanilha.SQL := qryResultado.SQL ;
  qryResultadoPlanilha.Open;}

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('nCdGrupoEstoque').Value  := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value  := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  qryResultado.Parameters.ParamByName('nCdLocalEstoque').Value  := frmMenu.ConvInteiro(MaskEdit1.Text) ;
  qryResultado.Parameters.ParamByName('nCdProduto').Value       := frmMenu.ConvInteiro(MaskEdit2.Text) ;
  qryResultado.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtCdLoja.Text) ;
  qryResultado.Parameters.ParamByName('cSomenteAtivo').Value    := 0 ;
  qryResultado.Parameters.ParamByName('cSomenteAtivoWeb').Value := 0 ;

  if (RadioGroup1.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cSomenteAtivo').Value := 1 ;

  if (rgProdutoAtivoWeb.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cSomenteAtivoWeb').Value := 1 ;

  qryResultado.Open ;

  if qryResultado.eof then
      MensagemAlerta('Nenhum resultado para o crit�rio utilizado.') ;

end;

procedure TfrmConsultaPosicaoEstoque.FormShow(Sender: TObject);
begin
  inherited;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtCdLoja.ReadOnly := True ;
      edtCdLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  PosicionaQuery(qryLocalEstoque, MaskEdit1.Text) ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdLoja = ' + edtCdLoja.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(87);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLocalEstoque, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, MaskEdit2.Text) ;

end;

procedure TfrmConsultaPosicaoEstoque.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(76);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, MaskEdit2.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPosicaoEstoque.edtCdLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtCdLoja.Text) ;

end;

procedure TfrmConsultaPosicaoEstoque.edtCdLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            edtCdLoja.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmConsultaPosicaoEstoque.ToolButton4Click(Sender: TObject);
var
    iLinha  : integer;
begin
    if (qryResultado.IsEmpty) then
    begin
        MensagemAlerta('N�o h� dados para exportar.');
        Exit;
    end;

    frmMenu.mensagemUsuario('Exportando Planilha...');

    // Informa��es do Cabe�alho
    ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
    ER2Excel.Celula['A2'].Text := 'Consulta de Posi��o de Estoque';

    // Produto
    if not (qryProduto.IsEmpty) then
        ER2Excel.Celula['A4'].Text := 'Produto: ' + qryResultadonCdProduto.AsString + ' - ' + qryResultadocNmProduto.Value
    else
        ER2Excel.Celula['A4'].Text := 'Produto: -';

    // Loja
    if not (qryLoja.IsEmpty) then
        ER2Excel.Celula['A5'].Text := 'Loja: ' + qryLojanCdLoja.AsString + ' - ' + qryLojacNmLoja.Value
    else
        ER2Excel.Celula['A5'].Text := 'Loja: -';

    // Grupo Estoque
    if not (qryGrupoEstoque.IsEmpty) then
        ER2Excel.Celula['A6'].Text := 'Grupo Estoque: ' + qryGrupoEstoquenCdGrupoEstoque.AsString + ' - ' + qryGrupoEstoquecNmGrupoEstoque.AsString
    else
        ER2Excel.Celula['A6'].Text := 'Grupo Estoque: -';

    // Local Estoque
    if not (qryLocalEstoque.IsEmpty) then
        ER2Excel.Celula['E5'].Text := 'Local Estoque: ' + qryLocalEstoquenCdLocalEstoque.AsString + ' - ' + qryLocalEstoquecNmLocalEstoque.AsString
    else
        ER2Excel.Celula['E5'].Text := 'Local Estoque: -';

    // Grupo Produto
    if not (qryGrupoProduto.IsEmpty) then
        ER2Excel.Celula['E6'].Text := 'Grupo Produto: ' + qryGrupoProdutonCdGrupoProduto.AsString + ' - ' + qryGrupoProdutocNmGrupoProduto.AsString
    else
        ER2Excel.Celula['E6'].Text := 'Grupo Produto: -';

    ER2Excel.Celula['A1'].Negrito;
    ER2Excel.Celula['A2'].Negrito;
    ER2Excel.Celula['A4'].Negrito;
    ER2Excel.Celula['A5'].Negrito;
    ER2Excel.Celula['A6'].Negrito;
    ER2Excel.Celula['E5'].Negrito;
    ER2Excel.Celula['E6'].Negrito;
    
    // Colunas
    ER2Excel.Celula['A8'].Text := 'C�d.';
    ER2Excel.Celula['B8'].Text := 'Descri��o';
    ER2Excel.Celula['C8'].Text := 'Local Estoque';
    ER2Excel.Celula['D8'].Text := 'Grupo Produto';
    ER2Excel.Celula['E8'].Text := 'UM';
    ER2Excel.Celula['F8'].Text := 'F�sico';
    ER2Excel.Celula['G8'].Text := 'Empenhado';
    ER2Excel.Celula['H8'].Text := 'Dispon�vel';
    ER2Excel.Celula['I8'].Text := '�lt. Receb.';
    ER2Excel.Celula['J8'].Text := 'Qt. Receb.';
    ER2Excel.Celula['K8'].Text := '�lt. Venda';

    ER2Excel.Celula['A8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['B8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['C8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['D8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['E8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['F8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['G8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['H8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['I8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['J8'].HorizontalAlign := haCenter;
    ER2Excel.Celula['K8'].HorizontalAlign := haCenter;

    ER2Excel.Celula['A8'].Negrito;
    ER2Excel.Celula['B8'].Negrito;
    ER2Excel.Celula['C8'].Negrito;
    ER2Excel.Celula['D8'].Negrito;
    ER2Excel.Celula['E8'].Negrito;
    ER2Excel.Celula['F8'].Negrito;
    ER2Excel.Celula['G8'].Negrito;
    ER2Excel.Celula['H8'].Negrito;
    ER2Excel.Celula['I8'].Negrito;
    ER2Excel.Celula['J8'].Negrito;
    ER2Excel.Celula['K8'].Negrito;

    ER2Excel.Celula['A1'].Background := RGB(221,221,221);
    ER2Excel.Celula['A1'].Range('K8');

    ER2Excel.Celula['A1'].Congelar('A9');

    iLinha := 9;

    qryResultado.DisableControls;

    qryResultado.First;

    while not (qryResultado.Eof) do
    begin
        ER2Excel.Celula['A' + IntToStr(iLinha)].Text := qryResultadonCdProduto.Value;
        ER2Excel.Celula['B' + IntToStr(iLinha)].Text := qryResultadocNmProduto.Value;
        ER2Excel.Celula['C' + IntToStr(iLinha)].Text := qryResultadocNmLocalEstoque.Value;
        ER2Excel.Celula['D' + IntToStr(iLinha)].Text := qryResultadocNmGrupoProduto.Value;

        ER2Excel.Celula['E' + IntToStr(iLinha)].Text            := qryResultadocUnidadeMedida.Value;
        ER2Excel.Celula['E' + intToStr(iLinha)].HorizontalAlign := haCenter;

        ER2Excel.Celula['F' + IntToStr(iLinha)].Text            := qryResultadonQtdeEstoque.Value;
        ER2Excel.Celula['F' + intToStr(iLinha)].Mascara         := '0';
        ER2Excel.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;

        ER2Excel.Celula['G' + IntToStr(iLinha)].Text            := qryResultadonValSaldoEmpenhado.Value;
        ER2Excel.Celula['G' + intToStr(iLinha)].Mascara         := '0';
        ER2Excel.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;

        ER2Excel.Celula['H' + IntToStr(iLinha)].Text            := qryResultadonValSaldoDisponivel.Value;
        ER2Excel.Celula['H' + intToStr(iLinha)].Mascara         := '0';
        ER2Excel.Celula['H' + intToStr(iLinha)].HorizontalAlign := haRight;

        ER2Excel.Celula['I' + IntToStr(iLinha)].Text    := qryResultadodDtUltReceb.AsString;
        ER2Excel.Celula['I' + IntToStr(iLinha)].Mascara := 'dd/MM/aaaa';
        ER2Excel.Celula['I' + intToStr(iLinha)].HorizontalAlign := haRight;

        ER2Excel.Celula['J' + IntToStr(iLinha)].Text            := qryResultadonQtdeRecebida.Value;
        ER2Excel.Celula['J' + intToStr(iLinha)].Mascara         := '0';
        ER2Excel.Celula['J' + intToStr(iLinha)].HorizontalAlign := haRight;

        ER2Excel.Celula['K' + IntToStr(iLinha)].Text    := qryResultadodDtUltVenda.AsString;
        ER2Excel.Celula['K' + IntToStr(iLinha)].Mascara := 'dd/MM/aaaa';
        ER2Excel.Celula['K' + intToStr(iLinha)].HorizontalAlign := haRight;

        Inc(iLinha);

        qryResultado.Next;
    end;

    qryResultado.EnableControls;

    { -- exporta planilha e limpa result do componente -- }
    ER2Excel.ExportXLS;
    ER2Excel.CleanupInstance;

    frmMenu.mensagemUsuario('');
end;

procedure TfrmConsultaPosicaoEstoque.PedidodeCompra1Click(Sender: TObject);
var
  objForm : TfrmPedidoAbertoProduto;
begin
  inherited;

  objForm := TfrmPedidoAbertoProduto.Create(nil);

  objForm.qryMaster.Close ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  objForm.qryMaster.Parameters.ParamByName('nCdProduto').Value := qryResultadonCdProduto.Value ;
  objForm.qryMaster.Parameters.ParamByName('nCdLoja').Value    := qryResultadonCdLoja.Value    ;
  objForm.qryMaster.Open ;

  if not objForm.qryMaster.Eof then
      showForm(objForm,true)
  else MensagemAlerta('Nenhum pedido de compra em aberto para o produto.') ;

end;

procedure TfrmConsultaPosicaoEstoque.DetalharEmpenho1Click(
  Sender: TObject);
var
  objForm : TfrmMRP_DetalhaEmpenho ;
begin
  inherited;

  objForm := TfrmMRP_DetalhaEmpenho.Create( Self ) ;

  objForm.exibeEmpenhos( qryResultadonCdProduto.Value );

  freeAndNil( objForm ) ;

end;

procedure TfrmConsultaPosicaoEstoque.ltimosRecebimentos1Click(
  Sender: TObject);
var
  objForm : TfrmProdutoERP_UltimasCompras;
begin
  inherited;

  objForm := TfrmProdutoERP_UltimasCompras.Create(nil);

  objForm.qryUltimasCompras.Close;
  objForm.qryUltimasCompras.Parameters.ParamByName('nCdProduto').Value := qryResultadonCdProduto.Value;
  objForm.qryUltimasCompras.Open;

  showForm(objForm, True)

end;

initialization
    RegisterClass(TfrmConsultaPosicaoEstoque) ;

end.
