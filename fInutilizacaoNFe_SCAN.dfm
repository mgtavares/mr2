inherited frmInutilizacaoNFe_SCAN: TfrmInutilizacaoNFe_SCAN
  Caption = 'Inutiliza'#231#227'o NFe - SCAN'
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 45
    Top = 72
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 125
    Top = 72
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 19
    Top = 48
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo S'#233'rie'
    FocusControl = DBEdit1
  end
  object Label4: TLabel [4]
    Left = 158
    Top = 120
    Width = 6
    Height = 13
    Alignment = taRightJustify
    Caption = #224
    FocusControl = DBEdit1
  end
  object Label5: TLabel [5]
    Left = 22
    Top = 96
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Justificativa'
    FocusControl = DBEdit1
  end
  object Label6: TLabel [6]
    Left = 13
    Top = 120
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo NFe'
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtNumInicial: TMaskEdit [8]
    Left = 84
    Top = 112
    Width = 65
    Height = 21
    EditMask = '########;1; '
    MaxLength = 8
    TabOrder = 4
    Text = '        '
  end
  object edtNumFinal: TMaskEdit [9]
    Left = 171
    Top = 112
    Width = 65
    Height = 21
    EditMask = '########;1; '
    MaxLength = 8
    TabOrder = 5
    Text = '        '
  end
  object cJustificativa: TEdit [10]
    Left = 84
    Top = 88
    Width = 686
    Height = 21
    TabOrder = 3
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 84
    Top = 64
    Width = 30
    Height = 21
    DataField = 'cModelo'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 152
    Top = 64
    Width = 43
    Height = 21
    DataField = 'cSerie'
    DataSource = DataSource1
    TabOrder = 2
  end
  object edtCodSerie: TER2LookupMaskEdit [13]
    Left = 84
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 82
    WhereAdicional.Strings = (
      '900 <= CONVERT(int, cSerie)')
    QueryLookup = qrySerieFiscal
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 256
  end
  object qrySerieFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSerieFiscal'
      '      ,cModelo'
      '      ,cSerie'
      '  FROM SerieFiscal'
      ' WHERE nCdSerieFiscal = :nPK'
      '   AND 900 <= CONVERT(int, cSerie)')
    Left = 248
    Top = 264
    object qrySerieFiscalnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qrySerieFiscalcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qrySerieFiscalcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
  end
  object qryNFeInutilizada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '   FROM NFeInutilizada'
      ' WHERE nCdNFeInutilizada = :nPK')
    Left = 296
    Top = 344
    object qryNFeInutilizadanCdNFeInutilizada: TIntegerField
      FieldName = 'nCdNFeInutilizada'
    end
    object qryNFeInutilizadanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryNFeInutilizadacCNPJEmitente: TStringField
      FieldName = 'cCNPJEmitente'
      Size = 14
    end
    object qryNFeInutilizadacJustificativa: TStringField
      FieldName = 'cJustificativa'
      Size = 150
    end
    object qryNFeInutilizadaiAno: TIntegerField
      FieldName = 'iAno'
    end
    object qryNFeInutilizadanCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryNFeInutilizadacSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryNFeInutilizadacModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryNFeInutilizadaiNrInicial: TIntegerField
      FieldName = 'iNrInicial'
    end
    object qryNFeInutilizadaiNrFinal: TIntegerField
      FieldName = 'iNrFinal'
    end
    object qryNFeInutilizadadDtInutilizacao: TDateTimeField
      FieldName = 'dDtInutilizacao'
    end
    object qryNFeInutilizadanCdUsuarioInutilizacao: TIntegerField
      FieldName = 'nCdUsuarioInutilizacao'
    end
    object qryNFeInutilizadacNrProtocoloInutilizacao: TStringField
      FieldName = 'cNrProtocoloInutilizacao'
      Size = 100
    end
  end
  object DataSource1: TDataSource
    DataSet = qrySerieFiscal
    Left = 264
    Top = 296
  end
  object SP_INUTILIZA_NFE: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INUTILIZA_NFE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdNFeInutilizada'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cJustificativa'
        Attributes = [paNullable]
        DataType = ftString
        Size = 150
        Value = Null
      end
      item
        Name = '@nCdSerieFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrInicial'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrFinal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuarioInutilizacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 264
    Top = 344
  end
end
