inherited frmPosicaoCarteiraCheque: TfrmPosicaoCarteiraCheque
  Left = -8
  Top = -8
  Width = 1040
  Height = 754
  Caption = 'Posi'#231#227'o Carteira Cheque'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 489
    Width = 552
    Height = 229
  end
  inherited ToolBar1: TToolBar
    Width = 1024
    ButtonWidth = 115
    TabOrder = 2
    inherited ToolButton1: TToolButton
      Caption = 'Executar Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 97
    Width = 1024
    Height = 392
    Hint = 
      'Clique duas vezes na conta para exibir um resumo por situa'#231#227'o de' +
      ' cheque'
    Align = alTop
    DataGrouping.GroupLevels = <>
    DataSource = dsResultado
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    ShowHint = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdContaBancariaDep'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'cNmConta'
        Footers = <>
        Width = 333
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        Width = 258
      end
      item
        EditButtons = <>
        FieldName = 'nTotalCheque'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 174
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1024
    Height = 68
    Align = alTop
    Caption = 'Filtros'
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 0
    object Label3: TLabel
      Tag = 1
      Left = 7
      Top = 44
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data de Dep'#243'sito At'#233
    end
    object Label1: TLabel
      Tag = 1
      Left = 30
      Top = 20
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Portadora'
    end
    object Label2: TLabel
      Tag = 1
      Left = 216
      Top = 44
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 320
      Top = 44
      Width = 199
      Height = 13
      Caption = '(deixe em branco para todos os cheques)'
    end
    object MaskEdit2: TMaskEdit
      Left = 240
      Top = 36
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object MaskEdit1: TMaskEdit
      Left = 120
      Top = 36
      Width = 71
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Left = 120
      Top = 12
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 192
      Top = 12
      Width = 199
      Height = 21
      DataField = 'nCdConta'
      DataSource = DataSource1
      TabOrder = 3
    end
  end
  object DBGridEh2: TDBGridEh [4]
    Left = 0
    Top = 489
    Width = 552
    Height = 229
    Hint = 'Clique duas vezes para exibir a posi'#231#227'o por data de vencimento'
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsPosicaoStatus
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    ShowHint = True
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh2DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTabStatusCheque'
        Footers = <>
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'cNmTabStatusCheque'
        Footers = <>
        Width = 296
      end
      item
        EditButtons = <>
        FieldName = 'nTotalCheque'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 164
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBGridEh3: TDBGridEh [5]
    Left = 552
    Top = 489
    Width = 472
    Height = 229
    Align = alRight
    DataGrouping.GroupLevels = <>
    DataSource = dsPosicaoVencto
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh3DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDtDeposito'
        Footers = <>
        Width = 138
      end
      item
        EditButtons = <>
        FieldName = 'nTotalCheque'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 155
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 640
    Top = 88
  end
  object qryContaBancariaDeb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND ((@nCdLoja = 0) OR (@nCdLoja = ContaBancaria.nCdLoja))'
      '   AND nCdEmpresa       = :nCdEmpresa')
    Left = 324
    Top = 200
    object qryContaBancariaDebnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDebnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancariaDeb
    Left = 632
    Top = 376
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryResultadoAfterScroll
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDataInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cDataFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria int'
      '       ,@cDataInicial     varchar(10)'
      '       ,@cDataFinal       varchar(10)'
      '       ,@nCdEmpresa       int'
      ''
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @cDataInicial     = :cDataInicial'
      'Set @cDataFinal       = :cDataFinal'
      'Set @nCdEmpresa       = :nCdEmpresa'
      ''
      'IF (@cDataInicial = '#39#39') Set @cDataInicial = '#39'01/01/1900'#39
      'IF (@cDataFinal   = '#39#39') Set @cDataFinal   = '#39'01/01/1900'#39
      ''
      'SELECT nCdContaBancariaDep'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END cNmConta'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,nTotalCheque'
      '  FROM (SELECT nCdContaBancariaDep'
      #9#9#9'  ,Sum(nValCheque) nTotalCheque'
      #9#9'  FROM Cheque'
      #9#9' WHERE nCdTabTipoCheque    = 2'
      #9#9'   AND nCdTabStatusCheque IN (1,5,6,8,9)'
      
        #9#9'   AND ((@cDataInicial     = '#39'01/01/1900'#39') OR (dDtDeposito >= ' +
        'Convert(DATETIME,@cDataInicial,103)))'
      
        #9#9'   AND ((@cDataFinal       = '#39'01/01/1900'#39') OR (dDtDeposito <= ' +
        'Convert(DATETIME,@cDataFinal,103)))'
      
        #9#9'   AND ((@nCdContaBancaria = 0)            OR (nCdContaBancari' +
        'aDep = @nCdContaBancaria))'
      '       AND Cheque.nCdEmpresa   = @nCdEmpresa'
      #9#9' GROUP BY nCdContaBancariaDep) Cheques'
      
        '       LEFT JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria' +
        ' = Cheques.nCdContaBancariaDep'
      
        '       LEFT JOIN Loja          ON Loja.nCdLoja                  ' +
        ' = ContaBancaria.nCdLoja'
      ' ORDER BY nCdContaBancariaDep')
    Left = 464
    Top = 328
    object qryResultadonCdContaBancariaDep: TIntegerField
      DisplayLabel = 'Conta Portadora|C'#243'd'
      FieldName = 'nCdContaBancariaDep'
    end
    object qryResultadocNmConta: TStringField
      DisplayLabel = 'Conta Portadora|Conta'
      FieldName = 'cNmConta'
      ReadOnly = True
      Size = 84
    end
    object qryResultadonCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryResultadocNmLoja: TStringField
      DisplayLabel = 'Loja|Loja'
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryResultadonTotalCheque: TBCDField
      DisplayLabel = 'Total de Cheques'
      FieldName = 'nTotalCheque'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 496
    Top = 336
  end
  object qryPosicaoStatus: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryPosicaoStatusAfterScroll
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDataInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cDataFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria int'
      '       ,@cDataInicial     varchar(10)'
      '       ,@cDataFinal       varchar(10)'
      '       ,@nCdEmpresa       int'
      ''
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @cDataInicial     = :cDataInicial'
      'Set @cDataFinal       = :cDataFinal'
      'Set @nCdEmpresa       = :nCdEmpresa'
      ''
      'IF (@cDataInicial = '#39#39') Set @cDataInicial = '#39'01/01/1900'#39
      'IF (@cDataFinal   = '#39#39') Set @cDataFinal   = '#39'01/01/1900'#39
      ''
      'SELECT TabStatusCheque.nCdTabStatusCheque'
      '      ,TabStatusCheque.cNmTabStatusCheque'
      '      ,nTotalCheque'
      '  FROM (SELECT nCdTabStatusCheque'
      #9#9#9'  ,Sum(nValCheque) nTotalCheque'
      #9#9'  FROM Cheque'
      #9#9' WHERE nCdTabTipoCheque    = 2'
      #9#9'   AND nCdTabStatusCheque IN (1,5,6,8,9)'
      
        #9#9'   AND ((@cDataInicial     = '#39'01/01/1900'#39') OR (dDtDeposito >= ' +
        'Convert(DATETIME,@cDataInicial,103)))'
      
        #9#9'   AND ((@cDataFinal       = '#39'01/01/1900'#39') OR (dDtDeposito <= ' +
        'Convert(DATETIME,@cDataFinal,103)))'
      
        #9#9'   AND ((@nCdContaBancaria = 0)            OR (nCdContaBancari' +
        'aDep = @nCdContaBancaria))'
      '           AND Cheque.nCdEmpresa   = @nCdEmpresa'
      #9#9' GROUP BY nCdTabStatusCheque) Cheques'
      
        '       LEFT JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatus' +
        'Cheque = Cheques.nCdTabStatusCheque'
      ' ORDER BY nCdTabStatusCheque')
    Left = 520
    Top = 592
    object qryPosicaoStatusnCdTabStatusCheque: TIntegerField
      DisplayLabel = 'Posi'#231#227'o de Cheques por Status|C'#243'd'
      FieldName = 'nCdTabStatusCheque'
    end
    object qryPosicaoStatuscNmTabStatusCheque: TStringField
      DisplayLabel = 'Posi'#231#227'o de Cheques por Status|Status'
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
    object qryPosicaoStatusnTotalCheque: TBCDField
      DisplayLabel = 'Posi'#231#227'o de Cheques por Status|Total de Cheques'
      FieldName = 'nTotalCheque'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsPosicaoStatus: TDataSource
    DataSet = qryPosicaoStatus
    Left = 560
    Top = 592
  end
  object qryPosicaoVencto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDataInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cDataFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTabStatusCheque'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria   int'
      '       ,@cDataInicial       varchar(10)'
      '       ,@cDataFinal         varchar(10)'
      '       ,@nCdEmpresa         int'
      '       ,@nCdTabStatusCheque int'
      ''
      'Set @nCdContaBancaria   = :nCdContaBancaria'
      'Set @cDataInicial       = :cDataInicial'
      'Set @cDataFinal         = :cDataFinal'
      'Set @nCdEmpresa         = :nCdEmpresa'
      'Set @nCdTabStatusCheque = :nCdTabStatusCheque'
      ''
      'IF (@cDataInicial = '#39#39') Set @cDataInicial = '#39'01/01/1900'#39
      'IF (@cDataFinal   = '#39#39') Set @cDataFinal   = '#39'01/01/1900'#39
      ''
      'SELECT dDtDeposito'
      '      ,Sum(nValCheque) nTotalCheque'
      '  FROM Cheque'
      ' WHERE nCdTabTipoCheque    = 2'
      '   AND nCdTabStatusCheque  = @nCdTabStatusCheque'
      
        '   AND ((@cDataInicial     = '#39'01/01/1900'#39') OR (dDtDeposito >= Co' +
        'nvert(DATETIME,@cDataInicial,103)))'
      
        '   AND ((@cDataFinal       = '#39'01/01/1900'#39') OR (dDtDeposito <= Co' +
        'nvert(DATETIME,@cDataFinal,103)))'
      
        '   AND ((@nCdContaBancaria = 0)            OR (nCdContaBancariaD' +
        'ep = @nCdContaBancaria))'
      '   AND Cheque.nCdEmpresa   = @nCdEmpresa'
      ' GROUP BY dDtDeposito')
    Left = 824
    Top = 600
    object qryPosicaoVenctodDtDeposito: TDateTimeField
      DisplayLabel = 'Posi'#231#227'o de Cheques por Data de Vencimento|Data'
      FieldName = 'dDtDeposito'
    end
    object qryPosicaoVenctonTotalCheque: TBCDField
      DisplayLabel = 'Posi'#231#227'o de Cheques por Data de Vencimento|Total de Cheques'
      FieldName = 'nTotalCheque'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsPosicaoVencto: TDataSource
    DataSet = qryPosicaoVencto
    Left = 864
    Top = 608
  end
end
