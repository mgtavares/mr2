unit fConciliaBancoManual_TituloMovimentado_Cartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmConciliaBancoManual_TituloMovimentado_Cartao = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryTitulos: TADOQuery;
    DataSource1: TDataSource;
    qryTitulosiNrDocto: TIntegerField;
    qryTituloscNrLotePOS: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosiParcelas: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosnCdLoteConciliacaoCartao: TIntegerField;
    qryTitulosdDtConciliacaoCartao: TDateTimeField;
    qryTituloscNmUsuario: TStringField;
    cxGrid1DBTableView1iNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cNrLotePOS: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1iParcelas: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoteConciliacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtConciliacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure exibeTitulos(nCdLanctoFin : integer);
  end;

var
  frmConciliaBancoManual_TituloMovimentado_Cartao: TfrmConciliaBancoManual_TituloMovimentado_Cartao;

implementation

{$R *.dfm}

{ TfrmConciliaBancoManual_TituloMovimentado_Cartao }

procedure TfrmConciliaBancoManual_TituloMovimentado_Cartao.exibeTitulos(
  nCdLanctoFin: integer);
begin
    qryTitulos.Close;
    PosicionaQuery(qryTitulos, intTostr(nCdLanctoFin)) ;

    Self.ShowModal ;

    qryTitulos.Close;
end;

end.
