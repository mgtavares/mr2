unit fCaixa_ParcelaCred;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, StdCtrls, Mask, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCurrencyEdit, cxLookAndFeelPainters, cxButtons,
  DBCtrls, jpeg, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCaixa_ParcelaCred = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTemp_Crediario: TADOQuery;
    qryTemp_CrediariodDtVencto: TDateTimeField;
    qryTemp_CrediariodDtSugerida: TDateTimeField;
    qryTemp_CrediariodDtLimite: TDateTimeField;
    qryTemp_CrediarionValParcela: TBCDField;
    dsTemp_Crediario: TDataSource;
    GroupBox1: TGroupBox;
    edtValEntradaMinima: TcxCurrencyEdit;
    edtValorFinanciar: TcxCurrencyEdit;
    Label2: TLabel;
    edtPercEntrCliente: TcxCurrencyEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtParcelasAberto: TcxCurrencyEdit;
    Label6: TLabel;
    edtCreditoDisponivel: TcxCurrencyEdit;
    Label7: TLabel;
    edtSaldoLimite: TcxCurrencyEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    GroupBox3: TGroupBox;
    listRestricoes: TListBox;
    edtValEntrada: TcxCurrencyEdit;
    Label10: TLabel;
    Label1: TLabel;
    edtValCompra: TcxCurrencyEdit;
    Label11: TLabel;
    qryTemp_CrediarionCdTemp_Pagto: TIntegerField;
    qryTemp_CrediariocFlgEntrada: TIntegerField;
    qryTemp_CrediarionPercent: TBCDField;
    qryTemp_CrediarioiDiasPrazo: TIntegerField;
    qryRecalculaParcelas: TADOQuery;
    btConsParcAberto: TcxButton;
    Timer1: TTimer;
    Image2: TImage;
    qryTemp_RestricaoVenda: TADOQuery;
    qryTemp_RestricaoVendanCdTemp_Pagto: TIntegerField;
    qryTemp_RestricaoVendanCdTabTipoRestricaoVenda: TIntegerField;
    qryTemp_RestricaoVendanCdUsuarioAutor: TIntegerField;
    qryTemp_RestricaoVendadDtAutor: TDateTimeField;
    qryTemp_RestricaoVendacJustificativa: TMemoField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironValLimiteCred: TBCDField;
    qryTerceironValChequePendente: TBCDField;
    qryTerceironValChequeDevolvidoAberto: TBCDField;
    qryTerceironValParcelaAberto: TBCDField;
    qryTerceironValParcelaAtraso: TBCDField;
    qryTerceironValLimiteDisp: TBCDField;
    qryTerceironCdStatus: TIntegerField;
    qryTerceironCdTabTipoSituacao: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    cmd: TADOCommand;
    qryPrepara_Temp: TADOQuery;
    imgAlerta: TImage;
    qryTemp_RestricaoVendacFlgLiberado: TIntegerField;
    qryAux: TADOQuery;
    qryDataCrediario: TADOQuery;
    qryDataCrediariodDtVencto: TDateTimeField;
    qryDataCrediariodDtSugerida: TDateTimeField;
    qryDataCrediariodDtLimite: TDateTimeField;
    qryDataCrediarionValParcela: TBCDField;
    qryDataCrediarionCdTemp_Pagto: TIntegerField;
    qryDataCrediariocFlgEntrada: TIntegerField;
    qryDataCrediarionPercent: TBCDField;
    qryDataCrediarioiDiasPrazo: TIntegerField;
    qryTerceirodDtProxSPC: TDateTimeField;
    qryTerceirodDtNegativacao: TDateTimeField;
    qryTerceironPercEntrada: TBCDField;
    qryTerceirocFlgRestricao: TIntegerField;
    edtLimiteCreditoTotal: TcxCurrencyEdit;
    Label13: TLabel;
    qryBloqueioCadastro: TADOQuery;
    qryBloqueioCadastronCdTabTipoRestricaoVenda: TIntegerField;
    qryBloqueioCadastrocNmTabTipoRestricaoVenda: TStringField;
    edtPercEntrCondicao: TcxCurrencyEdit;
    Label14: TLabel;
    edtCondPagto: TEdit;
    Label9: TLabel;
    ToolButton4: TToolButton;
    btCliente: TToolButton;
    qryTerceirocFlgPessoaAutVencida: TIntegerField;
    qryCalendarioFeriado: TADOQuery;
    dsCalendarioFeriado: TDataSource;
    qryCalendarioFeriadodDtFeriado: TStringField;
    qryTemp_CrediariocFeriado: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure qryTemp_CrediarioBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure VerificaRestricoes;
    procedure edtValEntradaExit(Sender: TObject);
    procedure edtValEntradaEnter(Sender: TObject);
    procedure RecalculaParcelas;
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btConsParcAbertoClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure InseriRestricao(nCdTabTipoRestricaoVenda: integer);
    procedure FormCreate(Sender: TObject);
    procedure edtValEntradaKeyPress(Sender: TObject; var Key: Char);
    procedure edtValEntradaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTemp_CrediarioAfterPost(DataSet: TDataSet);
    procedure btClienteClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryTemp_CrediarioCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nValorEntradaMinima : double ;
    nCdTerceiro         : integer ;
    nCdTemp_Pagto       : integer ;
    iDiasProxParc       : integer ;
    bAlterouData        : boolean ;
    bProcessado         : boolean ;
    procedure VerificaCalendario();
  end;

var
  frmCaixa_ParcelaCred: TfrmCaixa_ParcelaCred;
  nValEntradaOld : double ;

implementation

uses fMenu, fCaixa_Justificativa, fPdvSupervisor, fCaixa_ParcelaAberto,
  fCaixa_Recebimento, fCaixa_LiberaRestricao, fClienteVarejoPessoaFisica,
  DateUtils;

{$R *.dfm}

procedure TfrmCaixa_ParcelaCred.ToolButton1Click(Sender: TObject);
var
  nValTotalParcelas        : double ;
  nValorFinanciar          : double ;
  objCaixa_LiberaRestricao : TfrmCaixa_LiberaRestricao ;
  objCaixa                 : TfrmCaixa_Recebimento ;
begin
  inherited;

  nValTotalParcelas := 0 ;

  qryTemp_Crediario.DisableControls ;

  qryTemp_Crediario.First ;

  while not qryTemp_Crediario.Eof do
  begin
      nValTotalParcelas := nValTotalParcelas + qryTemp_CrediarionValParcela.Value ;

      if (qryTemp_CrediariodDtVencto.Value < Date) then
      begin
          qryTemp_Crediario.EnableControls;
          MensagemErro('A data de vencimento da parcela n�o pode ser menor que hoje.') ;
          abort ;
      end ;

      qryTemp_Crediario.Next ;
  end ;

  qryTemp_Crediario.First ;
  qryTemp_Crediario.EnableControls ;

  nValorFinanciar := frmMenu.TBRound(edtValorFinanciar.Value,2) ;

  if ((frmMenu.TBRound(nValTotalParcelas,2) - frmMenu.TBRound(nValorFinanciar,2)) <> 0) then
  begin
      MensagemAlerta('O Valor da soma das parcelas � diferente do valor a financiar.') ;
      abort ;
  end ;

  if (qryTemp_CrediariodDtVencto.Value <= Date) and (edtValEntrada.Value > 0) then
  begin
      MensagemAlerta('Para credi�rio com entrada o primeiro vencimento deve ser maior que hoje.') ;
      abort ;
  end ;

  if (listRestricoes.Items.Count > 1) then
  begin

      if (MessageDlg('O cliente tem restri��es para esta venda. Deseja continuar mesmo assim ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
      begin

            if (frmMenu.LeParametro('JUSTIFVDAREST') = 'S') then
            begin

                objCaixa_LiberaRestricao := TfrmCaixa_LiberaRestricao.Create( Self ) ;
                objCaixa_LiberaRestricao.nCdTemp_Pagto   := nCdTemp_Pagto ;

                {-- solicita autoriza��o para liberar as diverg�ncias --}
                objCaixa := TfrmCaixa_Recebimento.Create(Self) ;

                try

                    if not objCaixa.AutorizacaoGerente(33) then
                    begin
                        MensagemAlerta('Autentica��o falhou.') ;
                        abort ;
                    end ;

                    {-- armazena o c�digo do usu�rio que autorizou a libera��o das diverg�ncias --}
                    objCaixa_LiberaRestricao.nCdUsuarioAutor := objCaixa.nCdUsuarioSupervisor;
                    
                finally

                    freeAndNil( objCaixa ) ;

                end ;


                // Atualiza lista de permiss�es
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('UPDATE #Temp_RestricaoVenda                                                                                      ');
                qryAux.SQL.Add('   SET cFlgGerenteLibera = (SELECT cFlgGerenteLibera                                                             ');
                qryAux.SQL.Add('                              FROM TabTipoRestricaoVenda Tipo                                                    ');
                qryAux.SQL.Add('                             WHERE Tipo.nCdTabTipoRestricaoVenda = #Temp_RestricaoVenda.nCdTabTipoRestricaoVenda)');
                qryAux.ExecSQL;

                if not (objCaixa_LiberaRestricao.LiberaRestricoes()) then
                begin
                    MensagemErro('As restri��es n�o foram liberadas.') ;
                    abort ;
                end
                else ShowMessage('Restri��es liberadas com sucesso.') ;

            end ;

      end
      else abort ;

  end ;

  bProcessado := True ;
  
  close ;
end;

procedure TfrmCaixa_ParcelaCred.qryTemp_CrediarioBeforePost(
  DataSet: TDataSet);
begin

  bAlterouData := false ;

  if ((qryTemp_CrediariodDtVencto.AsString) = '') then
  begin
      MensagemAlerta('Informe a Data de vencimento da parcela.') ;
      abort ;
  end ;

  qryTemp_CrediariodDtVencto.Value := StrToDate(DateToStr(qryTemp_CrediariodDtVencto.Value)) ;

  if (qryTemp_CrediariodDtVencto.Value > qryTemp_CrediariodDtLimite.Value) then
  begin
      MensagemAlerta('Data de vencimento maior que a data limite.') ;
      abort ;
  end ;

  {if (qryTemp_CrediariodDtVencto.Value < Date) then
  begin
      MensagemAlerta('Data de vencimento menor que hoje.') ;
      abort ;
  end ;}

  bAlterouData := ((qryTemp_CrediariodDtVencto.NewValue <> qryTemp_CrediariodDtvencto.OldValue) and (qryTemp_Crediario.RecNo = 1)) ;

  inherited;

end;

procedure TfrmCaixa_ParcelaCred.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;

end;

procedure TfrmCaixa_ParcelaCred.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;

end;

procedure TfrmCaixa_ParcelaCred.FormShow(Sender: TObject);
begin
  inherited;

  //VerificaCalendario;

  imgAlerta.Visible := False ;

  qryTemp_Crediario.EnableControls;

  nValorEntradaMinima := edtValEntrada.Value ;

  edtLimiteCreditoTotal.Value := qryTerceironValLimiteCred.Value ;
  edtPercEntrCliente.Value    := qryTerceironPercEntrada.Value ;
  edtParcelasAberto.Value     := qryTerceironValParcelaAberto.Value + qryTerceironValChequePendente.Value + qryTerceironValChequeDevolvidoAberto.Value;
  edtCreditoDisponivel.Value  := qryTerceironValLimiteDisp.Value ;

  RecalculaParcelas;

  VerificaRestricoes;

  Label3.Visible              := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCliente.Visible  := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  Label14.Visible             := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCondicao.Visible := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  edtPercEntrCliente.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCliente.StyleDisabled.TextColor := clTeal ;

  edtPercEntrCondicao.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCondicao.StyleDisabled.TextColor := clTeal ;

  if (edtPercEntrCliente.Value >  edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCliente.StyleDisabled.Color     := clYellow ;
      edtPercentrCliente.StyleDisabled.TextColor := clBlack ;
  end ;

  if (edtPercEntrCliente.Value <= edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCondicao.StyleDisabled.Color     := clYellow ;
      edtPercEntrCondicao.StyleDisabled.TextColor := clBlack ;
  end ;

  edtValEntrada.SetFocus;

end;

procedure TfrmCaixa_ParcelaCred.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmCaixa_ParcelaCred.VerificaRestricoes;
var
    iRestricoes : integer ;
begin

  {-- Exclui as restri��es n�o liberadas --}
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE #Temp_RestricaoVenda WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 0') ;
  qryAux.ExecSQL;

  listRestricoes.Clear;
  iRestricoes := 0 ;

  edtSaldoLimite.Value := edtCreditoDisponivel.Value - edtValorFinanciar.Value ;

  if (edtValEntrada.Value < edtValEntradaMinima.Value) then
  begin
      listRestricoes.Items.Add('-> ENTRARA MENOR EXIGIDO') ;
      InseriRestricao(9);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (edtSaldoLimite.Value < 0) then
  begin
      listRestricoes.Items.Add('-> LIMITE CREDITO EXCEDIDO') ;
      InseriRestricao(1);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironValParcelaAtraso.Value > 0) then
  begin
      listRestricoes.Items.Add('-> PARCELA EM ATRASO') ;
      InseriRestricao(2);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironValChequeDevolvidoAberto.Value > 0) then
  begin
      listRestricoes.Items.Add('-> CHEQUE DEVOLVIDO PENDENTE') ;
      InseriRestricao(7);
      iRestricoes := iRestricoes +1 ;
  end ;

  Application.ProcessMessages;

  if (qryTerceironCdTabTipoSituacao.Value <> 2) and (qryTerceironCdTabTipoSituacao.Value < 4) then
  begin
      listRestricoes.Items.Add('-> CADASTRO PENDENTE DE APROVA��O') ;
      InseriRestricao(3);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironCdTabTipoSituacao.Value = 4) then
  begin
      listRestricoes.Items.Add('-> CADASTRO BLOQUEADO') ;
      InseriRestricao(4);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirocFlgRestricao.Value = 1) then
  begin
      listRestricoes.Items.Add('-> RESTRI��O SPC') ;
      InseriRestricao(5);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirodDtNegativacao.AsString <> '') then
  begin
      listRestricoes.Items.Add('-> CLIENTE NEGATIVADO PELA EMPRESA') ;
      InseriRestricao(6);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirodDtProxSPC.Value < Date) then
  begin
      listRestricoes.Items.Add('-> CONSULTA SPC VENCIDA') ;
      InseriRestricao(8);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirocFlgPessoaAutVencida.Value = 1) then
  begin
      listRestricoes.Items.Add('-> VALIDADE DE PESSOA(S) AUTORIZADA VENCIDA') ;
      InseriRestricao(13);
      iRestricoes := iRestricoes +1 ;
  end;

  qryBloqueioCadastro.Close;
  PosicionaQuery(qryBloqueioCadastro, qryTerceironCdTerceiro.AsString) ;

  if not qryBloqueioCadastro.eof then
  begin
      qryBloqueioCadastro.First ;

      while not qryBloqueioCadastro.eof do
      begin
          listRestricoes.Items.Add('-> ' + Trim(qryBloqueioCadastrocNmTabTipoRestricaoVenda.Value)) ;
          InseriRestricao(qryBloqueioCadastronCdTabTipoRestricaoVenda.Value);
          iRestricoes := iRestricoes +1 ;

          qryBloqueioCadastro.Next ;
      end ;

  end ;

  qryBloqueioCadastro.Close ;

  Timer1.Enabled       := False ;
  listRestricoes.Color := clWhite ;

  if (iRestricoes = 0) then
      listRestricoes.Items.Add('Nenhuma restri��o para esta venda.')
  else begin
      listRestricoes.Items.Add('Total de Restri��es : ' + intToStr(iRestricoes)) ;
      Timer1.Enabled := True ;
  end ;

end;

procedure TfrmCaixa_ParcelaCred.edtValEntradaExit(Sender: TObject);
begin
  inherited;

  if (edtValEntrada.Value < 0) then
  begin
      MensagemErro('Valor de entrada inv�lido.') ;
      edtValEntrada.SetFocus;
      abort ;
  end ;

  if (edtValEntrada.Value <> nValEntradaOld) then
  begin

      if (edtValEntrada.Value >= edtValCompra.Value) then
      begin
          MensagemAlerta('Valor da entrada deve ser menor que o valor da compra') ;
          edtValEntrada.Value := edtValEntradaMinima.Value ;
          abort ;
      end ;

      RecalculaParcelas;
      VerificaRestricoes;

  end ;

  DBGridEh1.SetFocus;

end;

procedure TfrmCaixa_ParcelaCred.edtValEntradaEnter(Sender: TObject);
begin
  inherited;

  nValEntradaOld := edtValEntrada.Value ;

end;

procedure TfrmCaixa_ParcelaCred.RecalculaParcelas;
begin

    edtValorFinanciar.Value := edtValCompra.Value - edtValEntrada.Value ;
    edtSaldoLimite.Value    := edtCreditoDisponivel.Value - edtValorFinanciar.Value ;

    qryRecalculaParcelas.Close ;
    qryRecalculaParcelas.Parameters.ParamByName('nValTotalVenda').Value := edtValCompra.Value  ;
    qryRecalculaParcelas.Parameters.ParamByName('nValEntrada').Value    := edtValEntrada.Value ;
    qryRecalculaParcelas.Parameters.ParamByName('nCdTemp_Pagto').Value  := nCdTemp_Pagto       ;
    qryRecalculaParcelas.ExecSQL;

    qryTemp_Crediario.Close ;
    qryTemp_Crediario.Open ;

end;

procedure TfrmCaixa_ParcelaCred.Timer1Timer(Sender: TObject);
begin
  inherited;

  if (listRestricoes.Count > 0) then
  begin
      if (imgAlerta.Visible) then
          imgAlerta.Visible := false
      else imgAlerta.Visible := true ;
  end ;
  
end;

procedure TfrmCaixa_ParcelaCred.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  Timer1.Enabled := false ;
end;

procedure TfrmCaixa_ParcelaCred.btConsParcAbertoClick(Sender: TObject);
var
  objCaixa_ParcelaAberto : TfrmCaixa_ParcelaAberto ;
begin
  inherited;

  if (edtParcelasAberto.Value > 0) then
  begin

      objCaixa_ParcelaAberto := TfrmCaixa_ParcelaAberto.Create( Self ) ;

      PosicionaQuery(objCaixa_ParcelaAberto.qryTituloAberto,intToStr(nCdTerceiro)) ;
      objCaixa_ParcelaAberto.ShowModal ;

      freeAndNil( objCaixa_ParcelaAberto ) ;

  end ;

  edtValEntrada.SetFocus;

end;

procedure TfrmCaixa_ParcelaCred.ToolButton2Click(Sender: TObject);
begin

  if (MessageDlg('O Credi�rio n�o ser� gerado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  qryTemp_Crediario.Cancel;
  
  qryTemp_Crediario.DisableControls;

  qryTemp_Crediario.First ;

  while not qryTemp_Crediario.Eof do
  begin
      qryTemp_Crediario.Delete ;
      qryTemp_Crediario.First ;
  end ;

  qryTemp_Crediario.EnableControls;

  Close ;

end;

procedure TfrmCaixa_ParcelaCred.InseriRestricao(
  nCdTabTipoRestricaoVenda: integer);
begin

    if not qryTemp_RestricaoVenda.Active then
        qryTemp_RestricaoVenda.Open;

    cmd.CommandText := 'INSERT INTO #Temp_RestricaoVenda (nCdTemp_Pagto, nCdTabTipoRestricaoVenda, cTemporario) VALUES(' + intToStr(nCdTemp_Pagto) + ',' + intToStr(nCdTabTipoRestricaoVenda) + ',' + Chr(39) + 'Liberar' + Chr(39) + ')' ;
    cmd.Execute;

    imgAlerta.Visible := True ;

end;

procedure TfrmCaixa_ParcelaCred.FormCreate(Sender: TObject);
begin
{  inherited;}

end;

procedure TfrmCaixa_ParcelaCred.edtValEntradaKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if (Key = #13) then
      edtValEntradaExit(nil) ;

end;

procedure TfrmCaixa_ParcelaCred.edtValEntradaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F4) then
      Toolbutton1.Click;

  if (Key = VK_F9) then
      btCliente.Click;

end;

procedure TfrmCaixa_ParcelaCred.qryTemp_CrediarioAfterPost(
  DataSet: TDataSet);
var
    dDataInicial :TdateTime ;
    iParcela : integer ;
begin
  inherited;

  if (iDiasProxParc = 30) and (bAlterouData) then
  begin

      dDataInicial := qryTemp_crediariodDtVencto.Value ;

      qryDataCrediario.Close;
      qryDataCrediario.Parameters.ParamByName('nCdTemp_Pagto').Value := nCdTemp_Pagto ;
      qryDataCrediario.Open;

      qryDataCrediario.DisableControls;

      qryDataCrediario.First;

      iParcela := 0 ;

      while not qryDataCrediario.Eof do
      begin
          qryDataCrediario.Edit;
          qryDataCrediariodDtVencto.Value := incMonth(dDataInicial,iParcela) ;
          qryDataCrediario.Post;
          qryDataCrediario.Next;
          iParcela := iParcela + 1 ;
      end ;

      qryDataCrediario.EnableControls;

      qryTemp_crediario.Close;
      qryTemp_crediario.Open;
      qryTemp_crediario.First;

      bAlterouData := false ;

  end ;

end;

procedure TfrmCaixa_ParcelaCred.btClienteClick(Sender: TObject);
var
  objClientePessoaFisica : TfrmClienteVarejoPessoaFisica ;
begin
  inherited;

  objClientePessoaFisica := TfrmClienteVarejoPessoaFisica.Create( Self ) ;

  objClientePessoaFisica.PosicionaQuery(objClientePessoaFisica.qryMaster, intToStr(nCdTerceiro));
  objClientePessoaFisica.btIncluir.Visible   := False ;
  objClientePessoaFisica.btExcluir.Visible   := False ;
  objClientePessoaFisica.ToolButton1.Visible := False ;
  objClientePessoaFisica.btCancelar.Visible  := False ;
  objClientePessoaFisica.btConsultar.Visible := False ;
  objClientePessoaFisica.bChamadaExterna     := True ;
  objClientePessoaFisica.ShowModal;

  freeAndNil( objClientePessoaFisica ) ;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, intToStr(ncdTerceiro)) ;

  {-- na volta do cadastro do cliente revisa se ficaram restri��es --}
  
  edtLimiteCreditoTotal.Value := qryTerceironValLimiteCred.Value ;
  edtPercEntrCliente.Value    := qryTerceironPercEntrada.Value ;
  edtParcelasAberto.Value     := qryTerceironValParcelaAberto.Value + qryTerceironValChequePendente.Value + qryTerceironValChequeDevolvidoAberto.Value;
  edtCreditoDisponivel.Value  := qryTerceironValLimiteDisp.Value ;

  Label3.Visible              := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCliente.Visible  := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  Label14.Visible             := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCondicao.Visible := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  edtPercEntrCliente.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCliente.StyleDisabled.TextColor := clTeal ;

  edtPercEntrCondicao.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCondicao.StyleDisabled.TextColor := clTeal ;

  if (edtPercEntrCliente.Value >  edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCliente.StyleDisabled.Color     := clYellow ;
      edtPercentrCliente.StyleDisabled.TextColor := clBlack ;
  end ;

  if (edtPercEntrCliente.Value <= edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCondicao.StyleDisabled.Color     := clYellow ;
      edtPercEntrCondicao.StyleDisabled.TextColor := clBlack ;
  end ;

  RecalculaParcelas;

  VerificaRestricoes;


end;

procedure TfrmCaixa_ParcelaCred.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if(qryTemp_Crediario.State = dsBrowse) then
      qryTemp_Crediario.Edit;

end;

procedure TfrmCaixa_ParcelaCred.VerificaCalendario();
var
  dDtAtualizada : TDateTime;
label
  VerificaData;
begin

  if (frmMenu.LeParametro('USACALENDCRED') = 'N') then
      Exit;

  {-- buscar as datas dentro do periodo das parcelas --}
  qryTemp_Crediario.First;

  while (not qryTemp_Crediario.Eof) do
  begin
      VerificaData:

      qryCalendarioFeriado.Close;
      qryCalendarioFeriado.Parameters.ParamByName('dDtVenc').Value    := DateToStr(qryTemp_CrediariodDtVencto.Value);
      qryCalendarioFeriado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryCalendarioFeriado.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      qryCalendarioFeriado.Open;

      { -- se vencimento da parcela cair na data cadastrada no calend�rio, verifica se adianta ou atrasa o vencimento -- }
      if (DateToStr(qryTemp_CrediariodDtVencto.Value) = qryCalendarioFeriadodDtFeriado.Value) then
      begin
          { -- AT = Atrasar Vencimento -- }
          if (frmMenu.LeParametro('USACALENDCRED') = 'AT') then
              dDtAtualizada := IncDay(qryTemp_CrediariodDtVencto.Value, 1);

          { -- AD = Adiantar Vencimento -- }
          if (frmMenu.LeParametro('USACALENDCRED') = 'AD') then
              dDtAtualizada := IncDay(qryTemp_CrediariodDtVencto.Value, -1);

          { --  Atualiza Vencimento -- }
          qryTemp_Crediario.Edit;
          qryTemp_CrediariodDtVencto.Value   := dDtAtualizada;
          qryTemp_CrediariodDtSugerida.Value := dDtAtualizada;
          qryTemp_Crediario.Post;

          Goto VerificaData;
      end;

      qryTemp_Crediario.Next;
  end;

  qryTemp_Crediario.Close;
  qryTemp_Crediario.Open;
end;

procedure TfrmCaixa_ParcelaCred.qryTemp_CrediarioCalcFields(
  DataSet: TDataSet);
begin
  qryCalendarioFeriado.Close;
  qryCalendarioFeriado.Parameters.ParamByName('dDtVenc').Value    := DateToStr(qryTemp_CrediariodDtVencto.Value);
  qryCalendarioFeriado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryCalendarioFeriado.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryCalendarioFeriado.Open;

  if DateToStr(qryTemp_CrediariodDtVencto.Value) = qryCalendarioFeriadodDtFeriado.Value then
      qryTemp_CrediariocFeriado.Value := 'SIM'
  else
      qryTemp_CrediariocFeriado.Value := 'N�O';
end;

end.
