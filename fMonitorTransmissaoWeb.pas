unit fMonitorTransmissaoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxPC, cxControls, DB, ADODB, Menus, StdCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, CheckLst;

type
  TfrmMonitorTransmissaoWeb = class(TfrmProcesso_Padrao)
    pagMonitor: TcxPageControl;
    tabProdutos: TcxTabSheet;
    qryProdutos: TADOQuery;
    dsProdutos: TDataSource;
    qryProdutosnCdTempRegistroWeb: TIntegerField;
    qryProdutosiIDRegistro: TIntegerField;
    qryProdutoscFlgAcao: TStringField;
    PopUp: TPopupMenu;
    btnAtualizar: TMenuItem;
    btnGerarXML: TMenuItem;
    qryProdutoscMsg: TStringField;
    N1: TMenuItem;
    btnTransReg: TMenuItem;
    qryProdutosdDtInclusao: TDateTimeField;
    qryProdutosnCdTabelaSistema: TIntegerField;
    SaveDialog: TSaveDialog;
    qryProdutoscFlgErro: TIntegerField;
    qryProdutoscArquivoXML: TMemoField;
    tabPosEst: TcxTabSheet;
    qryVerificaRegProd: TADOQuery;
    qryVerificaRegProddDtIntegracao: TDateTimeField;
    qryPosEstoque: TADOQuery;
    dsPosEstoque: TDataSource;
    qryPosEstoquenCdTempRegistroWeb: TIntegerField;
    qryPosEstoqueiIDRegistro: TIntegerField;
    qryPosEstoquecFlgAcao: TStringField;
    qryPosEstoquecMsg: TStringField;
    qryPosEstoquedDtInclusao: TDateTimeField;
    qryPosEstoquenCdTabelaSistema: TIntegerField;
    qryPosEstoquecArquivoXML: TMemoField;
    qryPosEstoquecFlgErro: TIntegerField;
    qryPosEstoquecNmProduto: TStringField;
    qryProdutoscNmProduto: TStringField;
    SP_ENVIA_POSICAO_ESTOQUE_TEMPREGISTROWEB: TADOStoredProc;
    qryProdFinal: TADOQuery;
    qryProdFinalnCdProduto: TIntegerField;
    btnTransTodos: TMenuItem;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn7: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    N2: TMenuItem;
    tabPedidoFaturado: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    qryPedidoWeb: TADOQuery;
    dsPedidoWeb: TDataSource;
    ToolButton4: TToolButton;
    btTransmitirReg: TToolButton;
    ToolButton6: TToolButton;
    btTransmitirTodosReg: TToolButton;
    qryPedidoWebnCdTempRegistroWeb: TIntegerField;
    qryPedidoWebiIDRegistro: TIntegerField;
    qryPedidoWebcNmCliente: TStringField;
    qryPedidoWebcFlgAcao: TStringField;
    qryPedidoWebcMsg: TStringField;
    qryPedidoWebdDtInclusao: TDateTimeField;
    qryPedidoWebnCdTabelaSistema: TIntegerField;
    qryPedidoWebcArquivoXML: TMemoField;
    qryPedidoWebcFlgErro: TIntegerField;
    ToolButton5: TToolButton;
    btCancelarTrans: TToolButton;
    qryProdutosdDtCancel: TDateTimeField;
    qryCancel: TADOQuery;
    btCancelarTransReg: TMenuItem;
    N3: TMenuItem;
    qryProdutoSimilar: TADOQuery;
    dsProdutoSimilar: TDataSource;
    tabProdutoSimilar: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    qryProdutoSimilarnCdTempRegistroWeb: TIntegerField;
    qryProdutoSimilariIDRegistro: TIntegerField;
    qryProdutoSimilarcNmProduto: TStringField;
    qryProdutoSimilarcFlgAcao: TStringField;
    qryProdutoSimilarcMsg: TStringField;
    qryProdutoSimilardDtInclusao: TDateTimeField;
    qryProdutoSimilarnCdTabelaSistema: TIntegerField;
    qryProdutoSimilarcArquivoXML: TMemoField;
    qryProdutoSimilarcFlgErro: TIntegerField;
    qryProdutoSimilardDtCancel: TDateTimeField;
    tabPrecoWeb: TcxTabSheet;
    qryPrecoWeb: TADOQuery;
    dsPrecoWeb: TDataSource;
    qryPrecoWebnCdTempRegistroWeb: TIntegerField;
    qryPrecoWebiIDRegistro: TIntegerField;
    qryPrecoWebcNmProduto: TStringField;
    qryPrecoWebcFlgAcao: TStringField;
    qryPrecoWebcMsg: TStringField;
    qryPrecoWebdDtInclusao: TDateTimeField;
    qryPrecoWebnCdTabelaSistema: TIntegerField;
    qryPrecoWebcArquivoXML: TMemoField;
    qryPrecoWebcFlgErro: TIntegerField;
    qryPrecoWebdDtCancel: TDateTimeField;
    cxGrid5: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridDBColumn22: TcxGridDBColumn;
    cxGridDBColumn23: TcxGridDBColumn;
    cxGridDBColumn24: TcxGridDBColumn;
    cxGridDBColumn25: TcxGridDBColumn;
    cxGridDBColumn26: TcxGridDBColumn;
    cxGridDBColumn27: TcxGridDBColumn;
    cxGridDBColumn28: TcxGridDBColumn;
    cxGridLevel4: TcxGridLevel;
    qryPrecoWebnValVendaWebDe: TBCDField;
    qryPrecoWebnValVendaWebPor: TBCDField;
    procedure btnAtualizarClick(Sender: TObject);
    procedure ExecutaAplicativo(cCaminho: string);
    procedure btnTransRegClick(Sender: TObject);
    procedure TransmitirRegistro(bTodos : boolean);
    procedure btnGerarXMLClick(Sender: TObject);
    procedure PopUpPopup(Sender: TObject);
    procedure btnTransTodosClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure btTransmitirRegClick(Sender: TObject);
    procedure btTransmitirTodosRegClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btCancelarTransClick(Sender: TObject);
    procedure btCancelarTransRegClick(Sender: TObject);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure cxGridDBTableView4DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorTransmissaoWeb: TfrmMonitorTransmissaoWeb;

implementation

uses
    fMenu;

{$R *.dfm}

procedure TfrmMonitorTransmissaoWeb.btnAtualizarClick(Sender: TObject);
begin
  inherited;

  { -- atualizar grid -- }
  ToolButton1.Click;
end;

procedure TfrmMonitorTransmissaoWeb.ExecutaAplicativo(cCaminho: String);
var
  StartupInfo        : TStartupInfo;
  ProcessInformation : TProcessInformation;
  bValida            : Boolean;
begin
    try
        FillChar(StartupInfo, SizeOf(StartupInfo), #0);

        with StartupInfo do
            cb := SizeOf(StartupInfo);

        // Cria e Executa o aplicativo
        bValida := CreateProcess(nil, PChar(cCaminho), nil, nil, false, NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInformation);

        // Se o aplicativo foi iniciado corretamente
        if (bValida) then
            WaitForSingleObject(ProcessInformation.hProcess, INFINITE)

        else
            Raise Exception.Create('O execut�vel ' + cCaminho + ' n�o foi localizado.');
    except
        MensagemErro('Erro no processamento.');
        Raise;
    end;
end;

procedure TfrmMonitorTransmissaoWeb.btnTransRegClick(Sender: TObject);
begin
    inherited;

    btTransmitirReg.Click;
end;

procedure TfrmMonitorTransmissaoWeb.TransmitirRegistro(bTodos : boolean);
var
  cCaminho, cIdRegistro, cAcao, cMsg: String;
begin
  inherited;

  { -- valida a a��o e o processo de acordo com a aba ativa -- }
  case (pagMonitor.ActivePageIndex) of
      { -- produtos -- }
      0 : begin
          if (qryProdutos.IsEmpty) then
              Exit;

          cAcao := 'Produto_InsUp';

          if (bTodos) then
              cIdRegistro := '0'
          else
              cIdRegistro := qryProdutosiIDRegistro.AsString;
      end;
      { -- produtos similares -- }
      1 : begin
          if (qryProdutoSimilar.IsEmpty) then
              Exit;

          cAcao := 'ProdutoSimilar_InsUp';

          if (bTodos) then
              cIdRegistro := '0'
          else
              cIdRegistro := qryProdutoSimilariIDRegistro.AsString;
      end;
      { -- posi��o estoque -- }
      2 : begin
          if (qryPosEstoque.IsEmpty) then
              Exit;

          { -- valida par�metro web -- }
          if (frmMenu.LeParametro('LOCESTECOMMERCE') = '') then
          begin
              MensagemErro('Par�metro LOCESTECOMMERCE n�o configurado ou inv�lido');
              Abort;
          end;

          cAcao := 'Estoque_Up';

          if (bTodos) then
              cIdRegistro := '0'
          else
              cIdRegistro := qryPosEstoqueiIDRegistro.AsString;
      end;
      { -- pedido web faturado -- }
      3 : begin
          if (qryPedidoWeb.IsEmpty) then
              Exit;

          cAcao := 'PedidoFat_Up';

          if (bTodos) then
              cIdRegistro := '0'
          else
              cIdRegistro := qryPedidoWebiIDRegistro.AsString;
      end;
      { -- pre�os web -- }
      4 : begin
      if (qryPrecoWeb.IsEmpty) then
              Exit;

          cAcao := 'PrecoWeb_Up';

          if (bTodos) then
              cIdRegistro := '0'
          else
              cIdRegistro := qryPrecoWebiIDRegistro.AsString;
      end;
  end;

  if (bTodos) then
      cMsg := 'Confirma a transmiss�o de todos os registros?'
  else
      cMsg := 'Confirma a transmiss�o do registro?';

  if (MessageDLG(cMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
      { -- prepara caminho para execu��o do aplicativo -- }
      cCaminho := '.\ECommerce\TakeWS.exe ' + cAcao + ' ' + cIdRegistro + ' ' + IntToStr(frmMenu.nCdUsuarioLogado);

      { -- executa aplicativo *.exe -- }
      ExecutaAplicativo(cCaminho);

      { -- se foi solicitado a transmiss�o de todos os produtos, trata tamb�m a transmiss�o dos registros de altera��o de produto -- }
      //if ((pagMonitor.ActivePageIndex = 0) and (bTodos = True)) then
      //    ExecutaAplicativo('.\ECommerce\Plataforma.exe AtualizarProduto 0 ' + IntToStr(frmMenu.nCdUsuarioLogado));

      { -- se inclus�o de produto exibe mensagem abaixo sobre posi��o de estoque gerado atrav�s da trigger -- }
      {if ((pagMonitor.ActivePageIndex = 0) and (cAcao = 'CriarProduto')) then
          ShowMessage('O registro de posi��o de estoque do(s) produto(s) foi gerado com sucesso.');}

      btnAtualizar.Click;
  end;
end;

procedure TfrmMonitorTransmissaoWeb.btnGerarXMLClick(Sender: TObject);
var
  ArquivoXML : TextFile;
  cConteudo  : String;
begin
  inherited;

  SaveDialog.Execute;

  if (DirectoryExists(ExtractFilePath(SaveDialog.FileName))) then
  begin
      AssignFile(ArquivoXML, SaveDialog.FileName + '.xml');

      Rewrite(ArquivoXML);

      case (pagMonitor.ActivePageIndex) of
          0: cConteudo := qryProdutoscArquivoXML.AsString;
          1: cConteudo := qryProdutoSimilarcArquivoXML.AsString;
          2: cConteudo := qryPosEstoquecArquivoXML.AsString;
          3: cConteudo := qryPedidoWebcArquivoXML.AsString;
          4: cConteudo := qryPrecoWebcArquivoXML.AsString;
      end;

      Write(ArquivoXML, UTF8Encode(cConteudo));

      CloseFile(ArquivoXML);
  end;
end;

procedure TfrmMonitorTransmissaoWeb.PopUpPopup(Sender: TObject);
begin
  inherited;

  case (pagMonitor.ActivePageIndex) of
      { -- produtos -- }
      0: begin
             if (qryProdutos.IsEmpty) then
             begin
                 btnGerarXML.Enabled   := False;
                 btnTransReg.Enabled   := False;
                 btnTransTodos.Enabled := False;
             end
             else
             begin
                 if (qryProdutoscFlgErro.Value = 1) then
                     btnGerarXML.Enabled := True
                 else
                     btnGerarXML.Enabled := False;

                 btnTransReg.Enabled   := True;
                 btnTransTodos.Enabled := True;
             end;
         end;
      { -- produto similar -- }
      1: begin
             if (qryProdutoSimilar.IsEmpty) then
             begin
                 btnGerarXML.Enabled   := False;
                 btnTransReg.Enabled   := False;
                 btnTransTodos.Enabled := False;
             end
             else
             begin
                 if (qryProdutoSimilarcFlgErro.Value = 1) then
                     btnGerarXML.Enabled := True
                 else
                     btnGerarXML.Enabled := False;

                 btnTransReg.Enabled   := True;
                 btnTransTodos.Enabled := True;
             end;
         end;
         { -- posi��o estoque -- }
      2: begin
             if (qryPosEstoque.IsEmpty) then
             begin
                 btnGerarXML.Enabled   := False;
                 btnTransReg.Enabled   := False;
                 btnTransTodos.Enabled := False;
             end
             else
             begin
                 if (qryPosEstoquecFlgErro.Value = 1) then
                     btnGerarXML.Enabled := True
                 else
                     btnGerarXML.Enabled := False;

                 btnTransReg.Enabled   := True;
                 btnTransTodos.Enabled := True;
             end;
         end;
         { -- pedido web faturado -- }
      3: begin
             if (qryPedidoWeb.IsEmpty) then
             begin
                 btnGerarXML.Enabled   := False;
                 btnTransReg.Enabled   := False;
                 btnTransTodos.Enabled := False;
             end
             else
             begin
                 if (qryPedidoWebcFlgErro.Value = 1) then
                     btnGerarXML.Enabled := True
                 else
                     btnGerarXML.Enabled := False;

                 btnTransReg.Enabled   := True;
                 btnTransTodos.Enabled := True;
             end;
         end;
         { -- pre�o web -- }
      4: begin
             if (qryPrecoWeb.IsEmpty) then
             begin
                 btnGerarXML.Enabled   := False;
                 btnTransReg.Enabled   := False;
                 btnTransTodos.Enabled := False;
             end
             else
             begin
                 if (qryPrecoWebcFlgErro.Value = 1) then
                     btnGerarXML.Enabled := True
                 else
                     btnGerarXML.Enabled := False;

                 btnTransReg.Enabled   := True;
                 btnTransTodos.Enabled := True;
             end;
         end;
  end;
end;

procedure TfrmMonitorTransmissaoWeb.btnTransTodosClick(
  Sender: TObject);
begin
  inherited;

  btTransmitirTodosReg.Click;
end;

procedure TfrmMonitorTransmissaoWeb.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  if (qryProdutos.IsEmpty) then
      Exit;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.cxGridDBTableView3DblClick(
  Sender: TObject);
begin
  inherited;
  if (qryProdutoSimilar.IsEmpty) then
      Exit;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  if (qryPosEstoque.IsEmpty) then
      Exit;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.cxGridDBTableView2DblClick(
  Sender: TObject);
begin
  inherited;
  if (qryPedidoWeb.IsEmpty) then
      Exit;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.cxGridDBTableView4DblClick(
  Sender: TObject);
begin
  inherited;
  if (qryPrecoWeb.IsEmpty) then
      Exit;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.ToolButton1Click(Sender: TObject);
begin
  inherited;

  case (pagMonitor.ActivePageIndex) of
  { -- Produtos -- }
  0: begin
         qryProdutos.Close;
         qryProdutos.Open;
     end;

  { -- Produto Similar -- }
  1: begin
         qryProdutoSimilar.Close;
         qryProdutoSimilar.Open;
     end;

  { -- Posi��o Estoque -- }
  2: begin
         qryPosEstoque.Close;
         qryPosEstoque.Open;
     end;

  { -- Pedido Web Faturado -- }
  3: begin
         qryPedidoWeb.Close;
         qryPedidoWeb.Open;
     end;
  { -- Pre�o Web -- }
  4: begin
         qryPrecoWeb.Close;
         qryPrecoWeb.Open;
     end;
  end;
end;

procedure TfrmMonitorTransmissaoWeb.btTransmitirRegClick(Sender: TObject);
begin
  inherited;

  TransmitirRegistro(False);
end;

procedure TfrmMonitorTransmissaoWeb.btTransmitirTodosRegClick(
  Sender: TObject);
begin
  inherited;

  TransmitirRegistro(True);
end;

procedure TfrmMonitorTransmissaoWeb.FormCreate(Sender: TObject);
begin
  inherited;

  { -- ativa p�gina produtos -- }
  pagMonitor.ActivePageIndex := 0;
end;

procedure TfrmMonitorTransmissaoWeb.btCancelarTransClick(Sender: TObject);
var
  nCdTempRegistroWeb : Integer;
begin
  inherited;

  case (pagMonitor.ActivePageIndex) of
      { -- produtos -- }
      0: begin
             if (qryProdutos.IsEmpty) then
                 Exit;

             nCdTempRegistroWeb := qryProdutosnCdTempRegistroWeb.Value;
         end;

      { -- produto similar -- }
      1: begin
             if (qryProdutoSimilar.IsEmpty) then
                 Exit;

             nCdTempRegistroWeb := qryProdutoSimilarnCdTempRegistroWeb.Value;
         end;

      { -- posi��o estoque -- }
      2: begin
             if (qryPosEstoque.IsEmpty) then
                 Exit;

             nCdTempRegistroWeb := qryPosEstoquenCdTempRegistroWeb.Value;
         end;

      { -- pedido web faturado -- }
      3: begin
             if (qryPedidoWeb.IsEmpty) then
                 Exit;

             nCdTempRegistroWeb := qryPedidoWebnCdTempRegistroWeb.Value;
         end;
      { -- pre�o web -- }
      4: begin
             if (qryPrecoWeb.IsEmpty) then
                 Exit;

             nCdTempRegistroWeb := qryPrecoWebnCdTempRegistroWeb.Value;
         end;
  end;

  if (MessageDLG('Deseja realmente cancelar a transmiss�o do registro No ' + IntToStr(nCdTempRegistroWeb) + ' ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
      try
          frmMenu.Connection.BeginTrans;

          qryCancel.Close;
          qryCancel.Parameters.ParamByName('nCdUsuarioCancel').Value   := frmMenu.nCdUsuarioLogado;
          qryCancel.Parameters.ParamByName('nCdTempRegistroWeb').Value := nCdTempRegistroWeb;
          qryCancel.ExecSQL;

          frmMenu.Connection.CommitTrans;

          ToolButton1.Click;

          ShowMessage('Registro de transmiss�o cancelado com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
          Abort;
      end;
  end;
end;

procedure TfrmMonitorTransmissaoWeb.btCancelarTransRegClick(Sender: TObject);
begin
  inherited;

  btCancelarTrans.Click;
end;

initialization
    RegisterClass(TfrmMonitorTransmissaoWeb);

end.
