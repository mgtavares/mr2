inherited frmOperacaoLocalEstoque: TfrmOperacaoLocalEstoque
  Left = 99
  Top = 168
  Width = 1228
  Height = 437
  Caption = 'V'#237'nculo Opera'#231#227'o x Local de Estoque'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1212
    Height = 370
  end
  inherited ToolBar1: TToolBar
    Width = 1212
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1212
    Height = 370
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsOperacaoLocalEstoque
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdOperacaoLocalEstoque'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdOperacaoEstoque'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmOperacaoEstoque'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrupoProduto'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmGrupoProduto'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdLocalEstoque'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        ReadOnly = True
        Width = 200
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 456
    Top = 232
  end
  object qryOperacaoLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryOperacaoLocalEstoqueBeforePost
    OnCalcFields = qryOperacaoLocalEstoqueCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM OperacaoLocalEstoque'
      ' ORDER BY nCdOperacaoEstoque'
      '         ,nCdEmpresa'
      '         ,nCdLoja'
      '         ,nCdGrupoProduto'
      '         ,nCdLocalEstoque')
    Left = 360
    Top = 200
    object qryOperacaoLocalEstoquenCdOperacaoLocalEstoque: TAutoIncField
      FieldName = 'nCdOperacaoLocalEstoque'
    end
    object qryOperacaoLocalEstoquenCdOperacaoEstoque: TIntegerField
      DisplayLabel = 'Opera'#231#227'o de Estoque|C'#243'd'
      FieldName = 'nCdOperacaoEstoque'
    end
    object qryOperacaoLocalEstoquecNmOperacaoEstoque: TStringField
      DisplayLabel = 'Opera'#231#227'o de Estoque|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
      Calculated = True
    end
    object qryOperacaoLocalEstoquenCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa|C'#243'd'
      FieldName = 'nCdEmpresa'
    end
    object qryOperacaoLocalEstoquecNmEmpresa: TStringField
      DisplayLabel = 'Empresa|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 50
      Calculated = True
    end
    object qryOperacaoLocalEstoquenCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryOperacaoLocalEstoquecNmLoja: TStringField
      DisplayLabel = 'Loja|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
    object qryOperacaoLocalEstoquenCdGrupoProduto: TIntegerField
      DisplayLabel = 'Grupo Produto|C'#243'd'
      FieldName = 'nCdGrupoProduto'
    end
    object qryOperacaoLocalEstoquecNmGrupoProduto: TStringField
      DisplayLabel = 'Grupo Produto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmGrupoProduto'
      Size = 50
      Calculated = True
    end
    object qryOperacaoLocalEstoquenCdLocalEstoque: TIntegerField
      DisplayLabel = 'Local de Estoque|C'#243'd'
      FieldName = 'nCdLocalEstoque'
    end
    object qryOperacaoLocalEstoquecNmLocalEstoque: TStringField
      DisplayLabel = 'Local de Estoque|Nome Local'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryEmpresaAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 392
    Top = 232
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryLojaAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 424
    Top = 200
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryGrupoProdutoAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto'
      ',cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 424
    Top = 232
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryLocalEstoqueAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdGrupoProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '      ,nCdLoja'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoLocalEstoque GPLE '
      
        '               WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLoca' +
        'lEstoque'
      '                 AND GPLE.nCdGrupoProduto = :nCdGrupoProduto)'
      
        '   --AND ((nCdLoja = @nCdLoja) OR (nCdLoja IS NULL AND @nCdLoja2' +
        ' = 0))'
      '   AND nCdEmpresa = :nCdEmpresa')
    Left = 456
    Top = 200
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryLocalEstoquenCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsOperacaoLocalEstoque: TDataSource
    DataSet = qryOperacaoLocalEstoque
    Left = 360
    Top = 232
  end
  object qryOperacaoEstoque: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryOperacaoEstoqueAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      ',cNmOperacaoEstoque'
      'FROM OperacaoEstoque'
      'WHERE nCdOperacaoEstoque = :nPK')
    Left = 392
    Top = 200
    object qryOperacaoEstoquenCdOperacaoEstoque: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object qryOperacaoEstoquecNmOperacaoEstoque: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
end
