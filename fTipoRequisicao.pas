unit fTipoRequisicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTipoRequisicao = class(TfrmCadastro_Padrao)
    qryMasternCdTipoRequisicao: TIntegerField;
    qryMastercNmTipoRequisicao: TStringField;
    qryMasternCdTabTipoRequis: TIntegerField;
    qryTabTipoRequis: TADOQuery;
    qryTabTipoRequisnCdTabTipoRequis: TIntegerField;
    qryTabTipoRequiscNmTabTipoRequis: TStringField;
    qryMastercNmTabTipoRequis: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    qryGrupoProdutoTipoRequisicao: TADOQuery;
    qryGrupoProdutoTipoRequisicaonCdTipoRequisicao: TIntegerField;
    qryGrupoProdutoTipoRequisicaonCdGrupoProduto: TIntegerField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    dsGrupoProdutoTipoRequisicao: TDataSource;
    qryGrupoProdutoTipoRequisicaocNmGrupoProduto: TStringField;
    qryMastercFlgExigeCC: TIntegerField;
    qryMastercExigeAutor: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryCentroCustoTipoRequisicao: TADOQuery;
    qryCentroCustoTipoRequisicaonCdTipoRequisicao: TIntegerField;
    qryCentroCustoTipoRequisicaonCdCC: TIntegerField;
    qryCentroCustoTipoRequisicaocNmCC: TStringField;
    DBGridEh2: TDBGridEh;
    dsCentroCustoTipoRequisicao: TDataSource;
    qryCentroCusto: TADOQuery;
    qryCentroCustocNmCC: TStringField;
    cxTabSheet3: TcxTabSheet;
    qryAutorizaTipoReq: TADOQuery;
    dsAutorizaTipoRequisicao: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBGridEh3: TDBGridEh;
    qryAutorizaTipoReqnCdUsuarioTipoRequisicaoAutor: TAutoIncField;
    qryAutorizaTipoReqnCdTipoRequisicao: TIntegerField;
    qryAutorizaTipoReqnCdUsuario: TIntegerField;
    qryAutorizaTipoReqcNmUsuario: TStringField;
    procedure btIncluirClick(Sender: TObject);
    procedure qryGrupoProdutoTipoRequisicaoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryGrupoProdutoTipoRequisicaoCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryCentroCustoTipoRequisicaoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCentroCustoTipoRequisicaoCalcFields(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryAutorizaTipoReqBeforePost(DataSet: TDataSet);
    procedure qryAutorizaTipoReqCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoRequisicao: TfrmTipoRequisicao;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoRequisicao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
end;

procedure TfrmTipoRequisicao.qryGrupoProdutoTipoRequisicaoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  qryGrupoProdutoTipoRequisicaonCdTipoRequisicao.Value := qryMasternCdTipoRequisicao.Value ;

end;

procedure TfrmTipoRequisicao.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryGrupoProdutoTipoRequisicao.State = dsBrowse) then
             qryGrupoProdutoTipoRequisicao.Edit ;

        if (qryGrupoProdutoTipoRequisicao.State = dsInsert) or (qryGrupoProdutoTipoRequisicao.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryGrupoProdutoTipoRequisicaonCdGrupoProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoRequisicao.qryGrupoProdutoTipoRequisicaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, qryGrupoProdutoTipoRequisicaonCdGrupoProduto.AsString) ;

  if not qryGrupoProduto.Eof then
      qryGrupoProdutoTipoRequisicaocNmGrupoProduto.Value := qryGrupoProdutocNmGrupoProduto.Value ;

end;

procedure TfrmTipoRequisicao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOREQUISICAO' ;
  nCdTabelaSistema  := 55 ;
  nCdConsultaPadrao := 121 ;
end;

procedure TfrmTipoRequisicao.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(124);

            If (nPK > 0) then
            begin
                qryMasternCdtabTipoRequis.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoRequisicao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryGrupoProdutoTipoRequisicao.Close ;
  PosicionaQuery(qryGrupoProdutoTipoRequisicao, qryMasternCdTipoRequisicao.AsString) ;

  qryCentroCustoTipoRequisicao.Close ;
  PosicionaQuery(qryCentroCustoTipoRequisicao, qryMasternCdTipoRequisicao.AsString) ;

  qryAutorizaTipoReq.Close ;
  PosicionaQuery(qryAutorizaTipoReq, qryMasternCdTipoRequisicao.AsString) ;

  cxPageControl1.ActivePageIndex := 0;
end;

procedure TfrmTipoRequisicao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryGrupoProdutoTipoRequisicao.Close ;
  qryCentroCustoTipoRequisicao.Close ;
  qryAutorizaTipoReq.Close ;

end;

procedure TfrmTipoRequisicao.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if ((qryMaster.State <> dsBrowse) and (not qryMaster.IsEmpty)) then
      qryMaster.Post;
end;

procedure TfrmTipoRequisicao.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryGrupoProdutoTipoRequisicao.Close ;
  PosicionaQuery(qryGrupoProdutoTipoRequisicao, qryMasternCdTipoRequisicao.AsString) ;

end;

procedure TfrmTipoRequisicao.qryCentroCustoTipoRequisicaoBeforePost(
  DataSet: TDataSet);
begin

  if (qryCentroCustoTipoRequisicaocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo.') ;
      abort ;
  end ;

  qryCentroCustoTipoRequisicaonCdTipoRequisicao.Value := qryMasternCdTipoRequisicao.Value ;

  inherited;

end;

procedure TfrmTipoRequisicao.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCentroCustoTipoRequisicao.State = dsBrowse) then
             qryCentroCustoTipoRequisicao.Edit ;

        if (qryCentroCustoTipoRequisicao.State = dsInsert) or (qryCentroCustoTipoRequisicao.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryCentroCustoTipoRequisicaonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoRequisicao.qryCentroCustoTipoRequisicaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  If (qryCentroCustoTipoRequisicaonCdCC.Value > 0) and (qryCentroCustoTipoRequisicaocNmCC.Value = '') then
  begin
      PosicionaQuery(qryCentroCusto, qryCentroCustoTipoRequisicaonCdCC.asString) ;

      if not qryCentroCusto.eof then
          qryCentroCustoTipoRequisicaocNmCC.Value := qryCentroCustocNmCC.Value ;

  end ;

end;

procedure TfrmTipoRequisicao.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        if (qryAutorizaTipoReq.State = dsBrowse) then
             qryAutorizaTipoReq.Edit ;

        if (qryAutorizaTipoReq.State = dsInsert) or (qryAutorizaTipoReq.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryAutorizaTipoReqnCdUsuario.Value := nPK ;
            end ;

        end ;

     end;

     end;
end;


procedure TfrmTipoRequisicao.qryAutorizaTipoReqBeforePost(
  DataSet: TDataSet);
begin
  if (qryAutorizaTipoReqcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Informe o Usu�rio.') ;
      abort ;
  end ;

  qryAutorizaTipoReqnCdTipoRequisicao.Value := qryMasternCdTipoRequisicao.Value ;

  inherited;

end;

procedure TfrmTipoRequisicao.qryAutorizaTipoReqCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  If (qryAutorizaTipoReqnCdUsuario.Value > 0) then
  begin
      PosicionaQuery(qryUsuario, qryAutorizaTipoReqnCdUsuario.asString) ;

      if not qryUsuario.eof then
          qryAutorizaTipoReqcNmUsuario.Value := qryUsuariocNmUsuario.Value ;
  end ;

end;

procedure TfrmTipoRequisicao.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0;
end;

procedure TfrmTipoRequisicao.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin
      if (Trim(qryMastercNmTipoRequisicao.Value) = '') then
      begin
          MensagemAlerta('Informe a descri��o da requisi��o.');
          DBEdit2.SetFocus;
          Abort;
      end;

      if (DBEdit4.Text = '') then
      begin
          MensagemAlerta('Infome o tipo da requisi��o.');
          DBEdit3.SetFocus;
          Abort;
      end;
  end;

  inherited;
end;

initialization
    RegisterClass(TfrmTipoRequisicao) ;

end.
