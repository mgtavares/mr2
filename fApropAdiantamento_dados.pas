unit fApropAdiantamento_dados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit,
  DB, DBCtrls, Mask, ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmApropAdiantamento_dados = class(TfrmProcesso_Padrao)
    edtDinheiro: TcxCurrencyEdit;
    Label1: TLabel;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    StaticText2: TStaticText;
    DBGridEh3: TDBGridEh;
    qryPrepara_Temp_Cheque: TADOQuery;
    qryPrepara_Temp_ChequenCdBanco: TIntegerField;
    qryPrepara_Temp_ChequenCdAgencia: TIntegerField;
    qryPrepara_Temp_ChequenCdConta: TIntegerField;
    qryPrepara_Temp_ChequecDigito: TStringField;
    qryPrepara_Temp_ChequeiNrCheque: TIntegerField;
    qryPrepara_Temp_ChequecCNPJCPF: TStringField;
    qryPrepara_Temp_ChequenValCheque: TBCDField;
    qryPrepara_Temp_ChequedDtDeposito: TDateTimeField;
    dsTemp_Cheque: TDataSource;
    SP_LIQUIDA_ADIANTAMENTO: TADOStoredProc;
    procedure MaskEdit6Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryPrepara_Temp_ChequeBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido        : integer ;
    nValAdiantamento : double ;
  end;

var
  frmApropAdiantamento_dados: TfrmApropAdiantamento_dados;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmApropAdiantamento_dados.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, MaskEdit6.Text) ;

end;

procedure TfrmApropAdiantamento_dados.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  edtDinheiro.Value := 0 ;

  qryContaBancaria.Close ;
  MaskEdit6.Text := '' ;

  qryPrepara_Temp_Cheque.Close ;
  qryPrepara_Temp_Cheque.Open ;

  edtDinheiro.SetFocus ;

end;

procedure TfrmApropAdiantamento_dados.ToolButton1Click(Sender: TObject);
var
    nAux : double ;
begin
  inherited;

  if (edtDinheiro.Value < 0) then
  begin
      MensagemAlerta('Valor em esp�cie n�o pode ser negativo.') ;
      edtDinheiro.SetFocus ;
      exit ;
  end ;

  if (edtDInheiro.Value > 0) and (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Para recebimento em dinheiro a conta banc�ria de cr�dito � obrigat�ria.') ;
      MaskEdit6.SetFocus;
      exit ;
  end ;

  nAux := 0 ;
  nAux := edtDinheiro.Value ;

  qryPrepara_Temp_Cheque.First ;

  while not qryPrepara_Temp_Cheque.Eof do
  begin
      nAux := nAux + qryPrepara_Temp_ChequenValCheque.Value ;
      qryPrepara_Temp_Cheque.Next ;
  end ;

  qryPrepara_Temp_Cheque.First ;

  if (nAux <> nValAdiantamento) then
  begin
      MensagemAlerta('Soma dos valores diverge do valor do adiantamento do pedido. Verifique.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a liquida��o do adiantamento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_LIQUIDA_ADIANTAMENTO.Close ;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nCdEmpresa').Value       :=  frmMenu.nCdEmpresaAtiva ;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nCdPedido').Value        :=  nCdpedido ;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nCdUsuario').Value       :=  frmMenu.nCdUsuarioLogado;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nValAdiantamento').Value :=  nValAdiantamento ;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nValDinheiro').Value     :=  edtDinheiro.Value ;
      SP_LIQUIDA_ADIANTAMENTO.Parameters.ParamByName('@nCdContaBancaria').Value :=  frmMenu.ConvInteiro(MaskEdit6.Text) ;
      SP_LIQUIDA_ADIANTAMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;
  

end;

procedure TfrmApropAdiantamento_dados.qryPrepara_Temp_ChequeBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPrepara_Temp_ChequedDtDeposito.Value < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('A data de dep�sito n�o pode ser inferior a hoje.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequenCdBanco.Value = 0) then
  begin
      MensagemAlerta('Informe o banco.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequenCdAgencia.Value = 0) then
  begin
      MensagemAlerta('Informe a ag�ncia.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequenCdConta.Value = 0) then
  begin
      MensagemAlerta('Informe o n�mero da conta.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequecDigito.Value = '') then
  begin
      MensagemAlerta('Informe o d�gito da conta.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequeiNrCheque.Value = 0) then
  begin
      MensagemAlerta('Informe o n�mero do cheque.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequecCNPJCPF.Value = '') then
  begin
      MensagemAlerta('Informe o CNPJ/CPF.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequenValCheque.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do cheque.') ;
      abort ;
  end ;

end;

end.
