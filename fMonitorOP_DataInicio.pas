unit fMonitorOP_DataInicio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxDropDownEdit, cxCalendar, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxTimeEdit, Menus, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData, ADODB,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxPC;

type
  TfrmMonitorOP_DataInicio = class(TfrmProcesso_Padrao)
    MonthCalendar1: TMonthCalendar;
    PopupMenu1: TPopupMenu;
    ExibirOrdensPrevistasparaIncio1: TMenuItem;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBTableView2dDtInicioPrevisto: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    qryOPAutorizada: TADOQuery;
    qryOPAutorizadanCdOrdemProducao: TIntegerField;
    qryOPAutorizadacNumeroOP: TStringField;
    qryOPAutorizadacNmTabTipoOrigemOP: TStringField;
    qryOPAutorizadadDtAbertura: TDateTimeField;
    qryOPAutorizadadDtInicioPrevisto: TDateTimeField;
    qryOPAutorizadadDtPrevConclusao: TDateTimeField;
    qryOPAutorizadacNmTipoOP: TStringField;
    qryOPAutorizadanCdProduto: TIntegerField;
    qryOPAutorizadacReferencia: TStringField;
    qryOPAutorizadacNmProduto: TStringField;
    qryOPAutorizadanQtdePlanejada: TBCDField;
    qryOPAutorizadanCdPedido: TIntegerField;
    qryOPAutorizadacNmTerceiro: TStringField;
    qryOPAutorizadacFlgAtraso: TIntegerField;
    qryOPAutorizadacFlgInicioAtrasado: TIntegerField;
    dsOPAutorizada: TDataSource;
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MonthCalendar1DblClick(Sender: TObject);
    procedure MonthCalendar1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bCancelado       : boolean ;
    dDtPrevConclusao : TDateTime ;
  end;

var
  frmMonitorOP_DataInicio: TfrmMonitorOP_DataInicio;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmMonitorOP_DataInicio.ToolButton2Click(Sender: TObject);
begin

  bCancelado := True ;

  inherited;

end;

procedure TfrmMonitorOP_DataInicio.FormShow(Sender: TObject);
begin
  inherited;

  bCancelado := False ;
  
end;

procedure TfrmMonitorOP_DataInicio.MonthCalendar1DblClick(Sender: TObject);
begin
  inherited;

  if (monthCalendar1.Date < Date) then
  begin

      MensagemErro('N�o � poss�vel agendar uma produ��o para uma data inferior a data de hoje.') ;
      abort ;

  end ;

  if (monthCalendar1.Date > dDtPrevConclusao) then
  begin

      if (MessageDlg('A previs�o de in�cio da OP � maior que a data prevista de conclus�o, Deseja Continuar ?' +#13#13+ 'Previs�o de Conclus�o : ' + DateToStr( dDtPrevConclusao ),mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  end ;

  Close ;

end;

procedure TfrmMonitorOP_DataInicio.MonthCalendar1Click(Sender: TObject);
begin
  inherited;

  qryOPAutorizada.Close;
  qryOPAutorizada.Parameters.ParamByName('nPK').Value               := frmMenu.nCdEmpresaAtiva ;
  qryOPAutorizada.Parameters.ParamByName('dDtInicioPrevisto').Value := DateToStr(monthCalendar1.Date) ;
  qryOPAutorizada.Open;
  
end;

end.
