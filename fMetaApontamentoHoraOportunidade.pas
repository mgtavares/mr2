unit fMetaApontamentoHoraOportunidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DB, ADODB, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, cxButtons, DBCtrls, Mask, DBGridEhGrouping;

type
  TfrmMetaApontamentoHoraOportunidade = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    edtMesAno: TMaskEdit;
    DBEdit2: TDBEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    edtEmpresa: TMaskEdit;
    edtLoja: TMaskEdit;
    DBEdit1: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    qryApontamentoHora: TADOQuery;
    qryApontamentoHoradDtSemana: TDateTimeField;
    qryApontamentoHoracNmSemana: TStringField;
    qryApontamentoHoranQtdeOportunidades: TIntegerField;
    dsApontamentoHora: TDataSource;
    cmdPreparaTemp: TADOCommand;
    qryApontamentoHoraiSemana: TIntegerField;
    qryApontamentoHoraiTotalHorasVendaReal: TBCDField;
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtEmpresaChange(Sender: TObject);
    procedure edtLojaChange(Sender: TObject);
    procedure edtMesAnoChange(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMetaApontamentoHoraOportunidade: TfrmMetaApontamentoHoraOportunidade;
  iMes, iAno : integer;

implementation

uses fLookup_Padrao, fMenu, fMetaApontamentoHora_Vendedor;

{$R *.dfm}

procedure TfrmMetaApontamentoHoraOportunidade.edtEmpresaExit(
  Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtEmpresaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            qryEmpresa.Close;

            edtEmpresa.Text := IntToStr(nPK);
            PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtLojaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            qryLoja.Close;

            edtLoja.Text := IntToStr(nPK);
            PosicionaQuery(qryLoja, edtLoja.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMetaApontamentoHoraOportunidade.cxButton1Click(
  Sender: TObject);
begin
  inherited;

  {-- verifica se a data digitada � valida --}
  if (trim(edtMesAno.Text) = '/') then
  begin
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
  end ;

  edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end ;

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  {-- se os campos de empresa ou loja estiverem em branco usa a loja e/ou empresa ativa --}

  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  if (Trim(edtLoja.Text) = '') then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva);

      qryLoja.Close;
      PosicionaQuery(qryLoja, edtLoja.Text) ;
  end;

  {-- exibe os valores da busca --}

  qryApontamentoHora.Close;
  qryApontamentoHora.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text);
  qryApontamentoHora.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(edtLoja.Text);
  qryApontamentoHora.Parameters.ParamByName('iMes').Value       := iMes;
  qryApontamentoHora.Parameters.ParamByName('iAno').Value       := iAno;
  qryApontamentoHora.Open;

  
end;

procedure TfrmMetaApontamentoHoraOportunidade.cxButton2Click(
  Sender: TObject);
begin
  inherited;
  qryApontamentoHora.Close;
  qryLoja.Close;
  qryEmpresa.Close;
  
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtLoja.Text    := IntToStr(frmMenu.nCdLojaAtiva);
  edtMesAno.Text  := '';

  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

  edtMesAno.SetFocus;
end;

procedure TfrmMetaApontamentoHoraOportunidade.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtLoja.Text    := IntToStr(frmMenu.nCdLojaAtiva);

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtEmpresaChange(
  Sender: TObject);
begin
  inherited;
  qryApontamentoHora.Close;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtLojaChange(
  Sender: TObject);
begin
  inherited;
  qryApontamentoHora.Close;
end;

procedure TfrmMetaApontamentoHoraOportunidade.edtMesAnoChange(
  Sender: TObject);
begin
  inherited;
  qryApontamentoHora.Close;
end;

procedure TfrmMetaApontamentoHoraOportunidade.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  if (qryApontamentoHoracNmSemana.Value = 'DOMINGO') then
  begin
      DBGridEh1.Canvas.Brush.Color := clSilver;
  end;

  DBGridEh1.Canvas.FillRect(Rect);
  DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
end;

procedure TfrmMetaApontamentoHoraOportunidade.DBGridEh1DblClick(
  Sender: TObject);
var
  objForm : TfrmMetaApontamentoHora_Vendedor;
begin
  inherited;

  if (qryApontamentoHora.Active) then
  begin
      if(qryApontamentoHora.RecordCount > 0) then
      begin

          {-- cria a tabela tempor�ria --}
          cmdPreparaTemp.Execute;

          {-- se o data for maior que hoje, permite apontar apenas as horas para
           -- executar uma previs�o de metas --}

          objForm := TfrmMetaApontamentoHora_Vendedor.Create(nil);

          if (qryApontamentoHoradDtSemana.Value > Now()) then
              objForm.cFlgPrevisaoMeta := 1
          else objForm.cFlgPrevisaoMeta := 0;

          objForm.qryApontamentos.Close;
          objForm.qryApontamentos.Parameters.ParamByName('dDtRef').Value     := qryApontamentoHoradDtSemana.AsString;
          objForm.qryApontamentos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text);
          objForm.qryApontamentos.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro(edtLoja.Text);
          objForm.qryApontamentos.Parameters.ParamByName('iMes').Value       := iMes;
          objForm.qryApontamentos.Parameters.ParamByName('iAno').Value       := iAno;
          objForm.qryApontamentos.Open;

          if (objForm.qryApontamentos.Eof) then
          begin
              ShowMessage('N�o existem metas para a semana. Estabele�a uma meta para a semana.');
              abort;
          end;

          showForm(objForm,true);

          qryApontamentoHora.Close;
          qryApontamentoHora.Open;

      end;
  end;

end;

initialization
    RegisterClass(TfrmMetaApontamentoHoraOportunidade);

end.
