unit rAniversariantesMes_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, QRPrev, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptAniversariantesMes_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    lblTitle: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    SummaryBand1: TQRBand;
    DetailBand1: TQRBand;
    QRDBText7: TQRDBText;
    QRDBText30: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText3: TQRDBText;
    cmdPreparaTemp: TADOCommand;
    SPREL_ANIVERSARIANTES_MES: TADOStoredProc;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel2: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText4: TQRDBText;
    qryResultado: TADOQuery;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadodDtNasc: TDateTimeField;
    qryResultadonIdade: TIntegerField;
    qryResultadodDtCadastro: TDateTimeField;
    qryResultadonCdLojaCadastro: TIntegerField;
    qryResultadocTelefone1: TStringField;
    qryResultadocTelefone2: TStringField;
    qryResultadocTelefoneMovel: TStringField;
    qryResultadocEmail: TStringField;
    qryResultadocTelefoneEmpTrab: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAniversariantesMes_view: TrptAniversariantesMes_view;

implementation

uses
  fMenu;

{$R *.dfm}

end.
