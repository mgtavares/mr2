inherited rptItensPorVendaPar: TrptItensPorVendaPar
  Width = 754
  Height = 543
  Caption = 'Itens por Venda - Venda Par'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 738
    Height = 481
  end
  object Label3: TLabel [1]
    Left = 9
    Top = 96
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Movimento'
  end
  object Label6: TLabel [2]
    Left = 188
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label1: TLabel [3]
    Left = 54
    Top = 71
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vendedor'
  end
  object Label5: TLabel [4]
    Left = 80
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    Width = 738
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [6]
    Left = 104
    Top = 120
    Width = 199
    Height = 41
    Caption = ' Exibir Somente Vendedores Ativos '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    ParentColor = False
    TabOrder = 5
  end
  object MaskEdit1: TMaskEdit [7]
    Left = 104
    Top = 88
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [8]
    Left = 216
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object DBEdit2: TDBEdit [9]
    Tag = 1
    Left = 172
    Top = 64
    Width = 541
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = dsVendedor
    TabOrder = 6
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 172
    Top = 40
    Width = 541
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 7
  end
  object er2LkpLoja: TER2LookupMaskEdit [11]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object er2LkpVendedor: TER2LookupMaskEdit [12]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnBeforeLookup = er2LkpVendedorBeforeLookup
    CodigoLookup = 703
    QueryLookup = qryVendedor
  end
  object pagDeptos: TcxPageControl [13]
    Left = 104
    Top = 184
    Width = 609
    Height = 249
    ActivePage = tabDeptos
    LookAndFeel.NativeStyle = True
    TabOrder = 8
    ClientRectBottom = 245
    ClientRectLeft = 4
    ClientRectRight = 605
    ClientRectTop = 24
    object tabDeptos: TcxTabSheet
      Caption = 'Ignorar os Departamentos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 601
        Height = 221
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTempDepto
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTempDepto'
            Footers = <>
            Title.Caption = 'C'#243'digo'
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'cNmDepartamento'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Descri'#231#227'o'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clRed
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = []
            Width = 499
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object rgModeloImp: TRadioGroup [14]
    Left = 306
    Top = 120
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 9
  end
  inherited ImageList1: TImageList
    Left = 128
    Top = 304
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryLojaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja'
      '               WHERE nCdLoja    = Loja.nCdLoja'
      '                 AND nCdUsuario = :nCdUsuario)'
      '')
    Left = 160
    Top = 272
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 160
    Top = 304
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 192
    Top = 304
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryVendedorBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario               = :nPK'
      '   AND Usuario.nCdStatus        = 1'
      '   AND (Usuario.cFlgVendedor    = 1 OR Usuario.cFlgGerente  = 1)'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdUsuario = Usuario.nCdUsuario'
      '                 AND UL.nCdLoja    = :nCdLoja)'
      ' ORDER BY Usuario.cNmUsuario')
    Left = 192
    Top = 272
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object cmdTempDepto: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#tempDepto'#39') IS NULL)'#13#10'BEGIN'#13#10'    CREATE ' +
      'TABLE #tempDepto (nCdTempDepto int PRIMARY KEY)'#13#10'END'#13#10#13#10'TRUNCATE' +
      ' TABLE #tempDepto'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 128
    Top = 272
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#tempDepto'#39') IS NULL)'
      'BEGIN'
      '    CREATE TABLE #tempDepto (nCdTempDepto int PRIMARY KEY)'
      'END'
      ''
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento'
      '  FROM Departamento'
      ' WHERE NOT EXISTS (SELECT 1'
      '                     FROM #tempDepto'
      '                    WHERE nCdTempDepto = nCdDepartamento)'
      '     ')
    Left = 256
    Top = 272
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryTempDepto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempDeptoBeforePost
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#tempDepto'#39') IS NULL)'
      'BEGIN'
      '    CREATE TABLE #tempDepto (nCdTempDepto int PRIMARY KEY)'
      'END'
      ''
      'TRUNCATE TABLE #tempDepto'
      ''
      'SELECT nCdTempDepto'
      '  FROM #tempDepto')
    Left = 224
    Top = 272
    object qryTempDeptonCdTempDepto: TIntegerField
      FieldName = 'nCdTempDepto'
    end
    object qryTempDeptocNmDepartamento: TStringField
      Tag = 1
      FieldKind = fkLookup
      FieldName = 'cNmDepartamento'
      LookupDataSet = qryDepartamento
      LookupKeyFields = 'nCdDepartamento'
      LookupResultField = 'cNmDepartamento'
      KeyFields = 'nCdTempDepto'
      Lookup = True
    end
  end
  object dsTempDepto: TDataSource
    DataSet = qryTempDepto
    Left = 224
    Top = 304
  end
  object ER2Excel: TER2Excel
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 264
    Top = 304
  end
end
