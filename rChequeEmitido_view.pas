unit rChequeEmitido_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptChequeEmitido_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    usp_RelatorionCdCheque: TAutoIncField;
    usp_RelatorionCdEmpresa: TStringField;
    usp_RelatoriocNmContaBancaria: TStringField;
    usp_RelatorioiNrCheque: TIntegerField;
    usp_RelatorionValCheque: TFloatField;
    usp_RelatoriodDtDeposito: TDateTimeField;
    usp_RelatoriodDtEmissao: TDateTimeField;
    usp_RelatoriocNmFavorecido: TStringField;
    usp_RelatorionCdProvisaoTit: TIntegerField;
    usp_RelatoriocFlgCompensado: TStringField;
    usp_RelatoriodDtExtrato: TDateTimeField;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptChequeEmitido_view: TrptChequeEmitido_view;

implementation

{$R *.dfm}

end.
