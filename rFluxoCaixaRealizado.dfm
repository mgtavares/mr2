inherited rptFluxoCaixaRealizado: TrptFluxoCaixaRealizado
  Left = 399
  Top = 178
  Width = 682
  Height = 433
  Caption = 'Fluxo de Caixa Realizado'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 666
    Height = 371
  end
  object Label6: TLabel [1]
    Left = 212
    Top = 192
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 192
    Width = 108
    Height = 13
    Caption = 'Per'#237'odo Movimenta'#231#227'o'
  end
  object Label1: TLabel [3]
    Tag = 1
    Left = 12
    Top = 144
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria/Caixa'
  end
  object Label2: TLabel [4]
    Tag = 1
    Left = 75
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label4: TLabel [5]
    Tag = 1
    Left = 12
    Top = 72
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja (Conta Banc'#225'ria)'
  end
  object Label5: TLabel [6]
    Tag = 1
    Left = 43
    Top = 96
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja (Emissora)'
  end
  object Label7: TLabel [7]
    Tag = 1
    Left = 37
    Top = 120
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
  end
  object Label8: TLabel [8]
    Tag = 1
    Left = 31
    Top = 168
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Totalizador'
  end
  inherited ToolBar1: TToolBar
    Width = 666
    TabOrder = 8
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 240
    Top = 184
    Width = 81
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit1: TMaskEdit [11]
    Left = 120
    Top = 184
    Width = 81
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [12]
    Left = 120
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 188
    Top = 136
    Width = 457
    Height = 21
    DataField = 'nCdConta'
    DataSource = dsContaBancariaDeb
    TabOrder = 9
  end
  object MaskEdit4: TMaskEdit [14]
    Left = 120
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit2: TDBEdit [15]
    Tag = 1
    Left = 188
    Top = 40
    Width = 457
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 10
  end
  object MaskEdit5: TMaskEdit [16]
    Left = 120
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit3: TDBEdit [17]
    Tag = 1
    Left = 188
    Top = 64
    Width = 457
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 11
  end
  object RadioGroup1: TRadioGroup [18]
    Left = 120
    Top = 214
    Width = 345
    Height = 49
    Caption = ' Tipo de Conta '
    Columns = 4
    ItemIndex = 0
    Items.Strings = (
      'Todas'
      'Banc'#225'ria'
      'Cofre'
      'Caixa')
    TabOrder = 12
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 188
    Top = 88
    Width = 457
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLojaEmissora
    TabOrder = 13
  end
  object edtLojaEmissora: TER2LookupMaskEdit [20]
    Left = 120
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnBeforePosicionaQry = edtLojaEmissoraBeforePosicionaQry
    CodigoLookup = 59
    QueryLookup = qryLojaEmissora
  end
  object CheckBox1: TCheckBox [21]
    Left = 120
    Top = 272
    Width = 193
    Height = 17
    Caption = 'Imprimir Composi'#231#227'o do Saldo final.'
    Checked = True
    State = cbChecked
    TabOrder = 14
  end
  object edtCdCC: TER2LookupMaskEdit [22]
    Left = 120
    Top = 112
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    OnBeforePosicionaQry = edtLojaEmissoraBeforePosicionaQry
    CodigoLookup = 28
    QueryLookup = qryCentroCusto
  end
  object DBEdit5: TDBEdit [23]
    Tag = 1
    Left = 188
    Top = 112
    Width = 457
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCusto
    TabOrder = 15
  end
  object DBEdit6: TDBEdit [24]
    Tag = 1
    Left = 188
    Top = 160
    Width = 457
    Height = 21
    DataField = 'cNmGrupoTotalCC'
    DataSource = DataSource3
    TabOrder = 16
  end
  object edtGrupoTotalizador: TER2LookupMaskEdit [25]
    Left = 120
    Top = 160
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    CodigoLookup = 212
    QueryLookup = qryGrupoTotalCC
  end
  object chkListaContaTransf: TCheckBox [26]
    Left = 120
    Top = 296
    Width = 249
    Height = 17
    Caption = 'Listar Contas de Transfer'#234'ncia de Recursos'
    Checked = True
    State = cbChecked
    TabOrder = 17
  end
  inherited ImageList1: TImageList
    Left = 352
    Top = 320
  end
  object qryContaBancariaDeb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@nCdLoja    int'
      '       ,@nCdUsuario int'
      ''
      'SET @nCdEmpresa =:nCdEmpresa'
      'SET @nCdLoja    =:nCdLoja'
      'SET @nCdUsuario =:nCdUsuario'
      ''
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END nCdConta'
      '      ,cFlgCaixa'
      '      ,cFlgCofre'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      
        '   AND ((nCdEmpresa = @nCdEmpresa) OR (@nCdEmpresa = 0 AND EXIST' +
        'S(SELECT 1'
      
        '                                                                ' +
        '    FROM UsuarioEmpresa UE'
      
        '                                                                ' +
        '   WHERE UE.nCdEmpresa = ContaBancaria.nCdEmpresa'
      
        '                                                                ' +
        '     AND UE.nCdUsuario = @nCdUsuario)))'
      
        '   AND ((nCdLoja   = @nCdLoja) OR (dbo.fn_LeParametro('#39'VAREJO'#39')=' +
        #39'N'#39') OR (@nCdLoja = 0 AND EXISTS(SELECT 1'
      
        '                                                                ' +
        '                                   FROM UsuarioLoja UL'
      
        '                                                                ' +
        '                                  WHERE UL.nCdLoja    = ContaBan' +
        'caria.nCdLoja'
      
        '                                                                ' +
        '                                    AND UL.nCdUsuario = @nCdUsua' +
        'rio)))'
      '   AND cFlgFluxo        = 1')
    Left = 216
    Top = 328
    object qryContaBancariaDebnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDebnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 50
    end
    object qryContaBancariaDebcFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryContaBancariaDebcFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
  end
  object dsContaBancariaDeb: TDataSource
    DataSet = qryContaBancariaDeb
    Left = 216
    Top = 360
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa    int'
      '       ,@nCdUsuario int'
      ''
      'SET @nCdEmpresa =:nPK'
      'SET @nCdUsuario =:nCdUsuario'
      ''
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa '
      '  FROM Empresa'
      
        ' WHERE ((nCdEmpresa = @nCdEmpresa) OR (@nCdEmpresa = 0 AND EXIST' +
        'S(SELECT 1'
      
        '                                                                ' +
        '    FROM UsuarioEmpresa UE'
      
        '                                                                ' +
        '   WHERE Empresa.nCdEmpresa = UE.nCdEmpresa'
      
        '                                                                ' +
        '     AND nCdUsuario = @nCdUsuario)))')
    Left = 120
    Top = 328
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja    int'
      '       ,@nCdUsuario int'
      ''
      'SET @nCdLoja =:nPK'
      'SET @nCdUsuario =:nCdUsuario'
      ''
      'SELECT nCdLoja'
      '      ,cNmLoja '
      '  FROM Loja'
      
        ' WHERE ((nCdLoja = @nCdLoja) OR (@nCdLoja = 0 AND EXISTS(SELECT ' +
        '1'
      
        '                                                           FROM ' +
        'UsuarioLoja UL'
      
        '                                                          WHERE ' +
        'Loja.nCdLoja = UL.nCdLoja'
      
        '                                                            AND ' +
        'nCdUsuario = @nCdUsuario)))'
      '   AND nCdEmpresa =:nCdEmpresa')
    Left = 152
    Top = 328
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 120
    Top = 360
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 152
    Top = 360
  end
  object qryLojaEmissora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja    int'
      '       ,@nCdUsuario int'
      ''
      'SET @nCdLoja =:nPK'
      'SET @nCdUsuario =:nCdUsuario'
      ''
      'SELECT nCdLoja'
      '      ,cNmLoja '
      '  FROM Loja'
      
        ' WHERE ((nCdLoja = @nCdLoja) OR (@nCdLoja = 0 AND EXISTS(SELECT ' +
        '1'
      
        '                                                           FROM ' +
        'UsuarioLoja UL'
      
        '                                                          WHERE ' +
        'Loja.nCdLoja = UL.nCdLoja'
      
        '                                                            AND ' +
        'nCdUsuario = @nCdUsuario)))'
      '   AND nCdEmpresa =:nCdEmpresa')
    Left = 184
    Top = 328
    object IntegerField1: TIntegerField
      FieldName = 'nCdLoja'
    end
    object StringField1: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLojaEmissora: TDataSource
    DataSet = qryLojaEmissora
    Left = 184
    Top = 360
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC, iNivel'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK')
    Left = 248
    Top = 328
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryCentroCustoiNivel: TSmallintField
      FieldName = 'iNivel'
    end
  end
  object dsCentroCusto: TDataSource
    DataSet = qryCentroCusto
    Left = 248
    Top = 360
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempResumoCaixa'#39') IS NULL)'
      'BEGIN'
      ''
      
        #9#9'CREATE TABLE #TempResumoCaixa (nCdTempResumoCaixa    int ident' +
        'ity(1,1)'
      
        '                                  ,nCdPlanoConta         int nul' +
        'l'
      #9#9#9#9#9#9#9#9#9'                ,nCdLoja               int null'
      
        #9#9#9#9#9#9#9#9#9'                ,nValor                decimal(12,2) de' +
        'fault 0 not null'
      #9#9#9#9#9#9#9#9#9'                ,nCdLanctoFin          int'
      #9#9#9#9#9#9#9#9#9'                ,nCdLojaEmissora       int'
      #9#9#9#9#9#9#9#9#9'                ,nCdTitulo             int'
      #9#9#9#9#9#9#9#9#9'                ,nCdContaBancaria      int'
      #9#9#9#9#9#9#9#9#9'                ,nCdCC1                int'
      #9#9#9#9#9#9#9#9#9'                ,nCdCC2                int'
      #9#9#9#9#9#9#9#9#9'                ,nCdCC3                int'
      #9#9#9#9#9#9#9#9#9'                ,cHistorico            varchar(100)'
      #9#9#9#9#9#9#9#9#9'                ,dDtLancto             datetime'
      #9#9#9#9#9#9#9#9#9'                ,cNrDocto              varchar(20)'
      #9#9#9#9#9#9#9#9#9'                ,nCdUsuario            int'
      
        '                                  ,cFlgApuradoCC         int def' +
        'ault 0 not null'
      
        '                                  ,cNmColunaGrupoTotalCC varchar' +
        '(50))'
      ''
      
        '        CREATE INDEX IDX ON #TempResumoCaixa (nCdTitulo,nCdPlano' +
        'Conta,nCdTempResumoCaixa)'
      ''
      'END'
      ''
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempResumoLancto'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempResumoLancto (cTipo              VARCHAR(2' +
        '0)'
      '                                   ,iSeqDRE            int'
      
        '                                   ,cNmGrupoPlanoConta VARCHAR(1' +
        '00)'
      '                                   ,nCdPlanoConta      int'
      
        '                                   ,cNmPlanoConta      VARCHAR(1' +
        '00)'
      '                                   ,cFlgRecDes         CHAR(1)'
      '                                   ,nCdCC              int'
      
        '                                   ,cNmCC1             VARCHAR(1' +
        '00)'
      
        '                                   ,cNmCC2             VARCHAR(1' +
        '00)'
      
        '                                   ,cNmCC3             VARCHAR(1' +
        '00)'
      
        '                                   ,nValor             DECIMAL(1' +
        '2,2) DEFAULT 0 NOT NULL)'
      ''
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempResumoContasFCR'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempResumoContasFCR (cFlgOK             int de' +
        'fault 0 not null'
      
        '                                      ,cTipo              VARCHA' +
        'R(50)'
      
        '                                      ,cNmGrupoPlanoConta VARCHA' +
        'R(100)'
      '                                      ,nCdPlanoConta      int'
      
        '                                      ,cNmPlanoConta      VARCHA' +
        'R(100))'
      ''
      'END'
      ''
      '')
    Left = 316
    Top = 357
  end
  object qryGrupoTotalCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoTotalCC'
      'WHERE nCdGrupoTotalCC = :nPK')
    Left = 280
    Top = 328
    object qryGrupoTotalCCnCdGrupoTotalCC: TIntegerField
      FieldName = 'nCdGrupoTotalCC'
    end
    object qryGrupoTotalCCcNmGrupoTotalCC: TStringField
      FieldName = 'cNmGrupoTotalCC'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryGrupoTotalCC
    Left = 280
    Top = 360
  end
  object qryTempResumoCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    IF (OBJECT_ID('#39'tempdb..#TempResumoLancto'#39') IS NULL)'
      '    BEGIN'
      ''
      
        '        CREATE TABLE #TempResumoLancto (cTipo              VARCH' +
        'AR(10)'
      '                                       ,iSeqDRE            int'
      
        '                                       ,cNmGrupoPlanoConta VARCH' +
        'AR(100)'
      '                                       ,nCdPlanoConta      int'
      
        '                                       ,cNmPlanoConta      VARCH' +
        'AR(100)'
      
        '                                       ,cFlgRecDes         CHAR(' +
        '1)'
      '                                       ,nCdCC              int'
      
        '                                       ,cNmCC1             VARCH' +
        'AR(100)'
      
        '                                       ,cNmCC2             VARCH' +
        'AR(100)'
      
        '                                       ,cNmCC3             VARCH' +
        'AR(100)'
      
        '                                       ,nValor             DECIM' +
        'AL(12,2) DEFAULT 0 NOT NULL)'
      ''
      '    END'
      ''
      '    SELECT * FROM #TempResumoLancto')
    Left = 316
    Top = 325
  end
  object qryColunaGrupoTotalCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdGrupoTotalCC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdColunaGrupoTotalCC'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmColunaGrupoTotalCC'
      '  FROM ColunaGrupoTotalCC'
      ' WHERE nCdGrupoTotalCC       = :nCdGrupoTotalCC'
      '   AND nCdColunaGrupoTotalCC = :nCdColunaGrupoTotalCC')
    Left = 352
    Top = 352
    object qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField
      FieldName = 'cNmColunaGrupoTotalCC'
      Size = 50
    end
  end
end
