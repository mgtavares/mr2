unit fServidorSAT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DB, ADODB;

type
  TfrmServidorSAT = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryServidorSAT: TADOQuery;
    dsServidorSAT: TDataSource;
    qryServidorSATnCdServidorSAT: TIntegerField;
    qryServidorSATcNmServidorSAT: TStringField;
    qryServidorSATnCdStatus: TIntegerField;
    qryServidorSATdDtCadastro: TDateTimeField;
    qryServidorSATnCdEmpresa: TIntegerField;
    qryServidorSATnCdLoja: TIntegerField;
    qryServidorSATcIPPrimario: TStringField;
    qryServidorSATcNmDatabase: TStringField;
    qryServidorSATcNmEmpresa: TStringField;
    qryServidorSATcNmLoja: TStringField;
    qryServidorSATcNmStatus: TStringField;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojanCdEmpresa: TIntegerField;
    qryLojacNmLoja: TStringField;
    procedure FormShow(Sender: TObject);
    procedure qryServidorSATCalcFields(DataSet: TDataSet);
    procedure qryServidorSATBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmServidorSAT: TfrmServidorSAT;

implementation

uses Math, fMenu;

{$R *.dfm}

procedure TfrmServidorSAT.FormShow(Sender: TObject);
begin
  inherited;

  qryServidorSAT.Close;
  qryServidorSAT.Open;

  DBGridEh1.Align := alClient;
  DBGridEh1.SetFocus;
end;

procedure TfrmServidorSAT.qryServidorSATCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, qryServidorSATnCdEmpresa.AsString);

  if (qryEmpresa.IsEmpty) then
      Exit;

  qryServidorSATcNmEmpresa.Value := qryEmpresacNmEmpresa.Value;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
  PosicionaQuery(qryLoja, qryServidorSATnCdLoja.AsString);

  if (not qryLoja.IsEmpty) then
      qryServidorSATcNmLoja.Value := qryLojacNmLoja.Value;
end;

procedure TfrmServidorSAT.qryServidorSATBeforePost(DataSet: TDataSet);
begin
  if (qryServidorSATnCdServidorSAT.Value <= 0) then
  begin
      MensagemAlerta('Informe o c�digo do servidor SAT v�lido.');
      DBGridEh1.SelectedField := qryServidorSATnCdServidorSAT;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (qryServidorSATcNmEmpresa.Value = '') then
  begin
      MensagemAlerta('Informe o c�digo da empresa vinculada ao servidor SAT.');
      DBGridEh1.SelectedField := qryServidorSATnCdEmpresa;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (qryServidorSATcNmLoja.Value = '') then
  begin
      MensagemAlerta('Informe o c�digo da loja vinculada ao servidor SAT.');
      DBGridEh1.SelectedField := qryServidorSATnCdLoja;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (Trim(qryServidorSATcNmServidorSAT.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do servidor SAT.');
      DBGridEh1.SelectedField := qryServidorSATcNmServidorSAT;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (Trim(qryServidorSATcIPPrimario.Value) = '') then
  begin
      MensagemAlerta('Informe o IP de conex�o da base de dados.');
      DBGridEh1.SelectedField := qryServidorSATcIPPrimario;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (Trim(qryServidorSATcNmDatabase.Value) = '') then
  begin
      MensagemAlerta('Informe a inst�ncia da base de dados.');
      DBGridEh1.SelectedField := qryServidorSATcNmDatabase;
      DBGridEh1.SetFocus;
      Abort;
  end;

  inherited;

  if (qryServidorSAT.State = dsInsert) then
      qryServidorSATdDtCadastro.Value := Now();
end;

initialization
  RegisterClass(TfrmServidorSAT);

end.
