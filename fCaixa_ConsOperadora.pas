unit fCaixa_ConsOperadora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ADODB,
  ImgList, StdCtrls, cxButtons, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, jpeg, ExtCtrls;

type
  TfrmCaixa_ConsOperadora = class(TForm)
    Image1: TImage;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    btSeleciona: TcxButton;
    btFechar: TcxButton;
    ImageList1: TImageList;
    qryOperadora: TADOQuery;
    dsOperadora: TDataSource;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    qryOperadoracNmOperadoraCartao: TStringField;
    cxGrid1DBTableView1nCdOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperadoraCartao: TcxGridDBColumn;
    procedure btSelecionaClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure ConsultaOperadora;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    cTipoOperacao : string ;
    nCdLoja       : integer ;
  end;

var
  frmCaixa_ConsOperadora: TfrmCaixa_ConsOperadora;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_ConsOperadora.btSelecionaClick(Sender: TObject);
begin
    close;
end;

procedure TfrmCaixa_ConsOperadora.btFecharClick(Sender: TObject);
begin
    qryOperadora.Close ;
    close ;
end;

procedure TfrmCaixa_ConsOperadora.ConsultaOperadora;
begin
    qryOperadora.Close ;

    if (nCdLoja = 0) then
        qryOperadora.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva
    else qryOperadora.Parameters.ParamByName('nCdLoja').Value := nCdLoja;

    qryOperadora.Parameters.ParamByName('cTipoOperacao').Value := cTipoOperacao ;
    qryOperadora.Open;

    nCdLoja := 0 ;
    
    Self.ShowModal;
end;

procedure TfrmCaixa_ConsOperadora.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin

    if (Key = #13) then
        btSeleciona.Click;

end;

procedure TfrmCaixa_ConsOperadora.FormShow(Sender: TObject);
begin

    cxGrid1.SetFocus;

end;

procedure TfrmCaixa_ConsOperadora.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  case key of

      vk_return : btSeleciona.Click;
      VK_ESCAPE : btFechar.Click;

  end

end;

end.
