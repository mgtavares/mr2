unit fPdvReabrePedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, jpeg, ExtCtrls;

type
  TfrmPdvReabrePedido = class(TForm)
    edtPedido: TcxCurrencyEdit;
    Label2: TLabel;
    Image1: TImage;
    procedure edtPedidoKeyPress(Sender: TObject; var Key: Char);
    function ReabrePedido():integer ;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPdvReabrePedido: TfrmPdvReabrePedido;

implementation

{$R *.dfm}

procedure TfrmPdvReabrePedido.edtPedidoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (key = #13) then
        close();

end;

function TfrmPdvReabrePedido.ReabrePedido: integer;
begin

    edtPedido.Value := 0 ;

    Self.ShowModal;

    Result := strToInt(floatToStr(edtPedido.Value)) ;

end;

procedure TfrmPdvReabrePedido.FormShow(Sender: TObject);
begin
    edtPedido.SelectAll;
end;

end.
