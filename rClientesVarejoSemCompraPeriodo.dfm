inherited rptClientesVarejoSemCompraPeriodo: TrptClientesVarejoSemCompraPeriodo
  Left = 344
  Top = 236
  Width = 820
  Height = 281
  Caption = 'Rel. Clientes Sem Compra Per'#237'odo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 804
    Height = 214
  end
  object Label3: TLabel [1]
    Left = 30
    Top = 94
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Cadastro'
  end
  object Label6: TLabel [2]
    Left = 208
    Top = 96
    Width = 16
    Height = 13
    Alignment = taCenter
    Caption = 'at'#233
  end
  object Label2: TLabel [3]
    Left = 12
    Top = 72
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtd. Dias s/ Comprar'
  end
  object Label4: TLabel [4]
    Left = 189
    Top = 72
    Width = 161
    Height = 13
    Caption = '0 = Cliente nunca relizou compra.'
  end
  object Label5: TLabel [5]
    Left = 30
    Top = 48
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja de Cadastro'
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    Width = 804
    TabOrder = 5
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object mskdDtInicial: TMaskEdit [7]
    Left = 116
    Top = 88
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object mskdDtFinal: TMaskEdit [8]
    Left = 234
    Top = 88
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object mskQtdDias: TMaskEdit [9]
    Left = 116
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#####;1; '
    MaxLength = 5
    TabOrder = 1
    Text = '     '
    OnExit = mskQtdDiasExit
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 184
    Top = 40
    Width = 585
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 6
  end
  object er2LkpLoja: TER2LookupMaskEdit [11]
    Left = 116
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object rgModeloImp: TRadioGroup [12]
    Left = 115
    Top = 118
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 4
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 96
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryLojaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 424
    Top = 96
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 424
    Top = 128
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Clientes Sem Compra Per'#237'odo'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 360
    Top = 96
  end
end
