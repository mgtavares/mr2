unit fMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ActnMan, ActnCtrls, ActnMenus, ExtCtrls,
  ActnList, XPStyleActnCtrls, ImgList, Menus, StdStyleActnCtrls, StdCtrls,
  DB, ADODB, DBCtrls, Grids, DBGrids, Mask, cxControls, cxContainer,
  cxTreeView, cxLookAndFeelPainters, cxButtons, TypInfo, WinSock, fMensagemPadrao,
  cxEdit, cxProgressBar, OleServer, ExcelXP, jpeg, ACBrBase, ACBrECF,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit, GIFImage, cxStyles, DBGridEh,
  PrnDbgeh, ACBrGAV, MMSystem, Registry, ACBrSATExtratoClass,
  ACBrSATExtratoESCPOS, ACBrSAT, ACBrSATExtratoFortesFr, ACBrNFe, ShellAPI, IniFiles,
  IdFTP, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  ACBrPosPrinter,  comObj, ACBrSATExtratoReportClass;

type
  TThread1 = class(TThread)
    protected
      constructor Create;
      destructor Destroy(); override;
      procedure Execute; override;
    public
      myForm : TForm ;
  end;

  TfrmMenu = class(TForm)
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    ImageList1: TImageList;
    Connection: TADOConnection;
    PageControlPageDock: TPageControl;
    qryLogAuditoria: TADOQuery;
    qryLogAuditorianCdLogAuditoria: TAutoIncField;
    qryLogAuditorianCdTabelaSistema: TIntegerField;
    qryLogAuditorianCdUsuario: TIntegerField;
    qryLogAuditorianCdTipoAcao: TIntegerField;
    qryLogAuditoriadDtLog: TDateTimeField;
    qryLogAuditoriaID: TIntegerField;
    qryLogAuditoriacObserv: TStringField;
    qryValidaLogin: TADOQuery;
    qryValidaLoginnCdUsuario: TIntegerField;
    qryValidaLogincNmLogin: TStringField;
    qryValidaLogincSenha: TStringField;
    qryValidaLogincFlgAtivo: TSmallintField;
    qryValidaLogindDtUltAcesso: TDateTimeField;
    qryValidaLogindDtUltAltSenha: TDateTimeField;
    qryValidaLoginnCdEmpPadrao: TIntegerField;
    qryValidaLoginnCdStatus: TIntegerField;
    qryValidaLogincNmUsuario: TStringField;
    qryValidaLogincCPF: TStringField;
    qryValidaLoginnCdTerceiroResponsavel: TIntegerField;
    qryValidaLogincFlagMaster: TSmallintField;
    qryValidaLogincAcessoWERP: TSmallintField;
    qryValidaLogincTrocaSenhaProxLogin: TSmallintField;
    usp_PreparaMenu: TADOStoredProc;
    usp_PreparaMenunCdMenu: TIntegerField;
    usp_PreparaMenunCdMenuPai: TIntegerField;
    usp_PreparaMenucNmMenu: TStringField;
    usp_PreparaMenunCdAplicacao: TIntegerField;
    usp_PreparaMenucNmAplicacao: TStringField;
    usp_PreparaMenucNmObjeto: TStringField;
    usp_PreparaMenunCdMenuRaiz: TIntegerField;
    TabSheet1: TTabSheet;
    MainMenu1: TMainMenu;
    Opes1: TMenuItem;
    AlterarEmpresa1: TMenuItem;
    rocarLogon1: TMenuItem;
    N1: TMenuItem;
    SAir1: TMenuItem;
    qryAtuUltAcesso: TADOQuery;
    qryUsuarioEmpresa: TADOQuery;
    qryUsuarioEmpresanCdEmpresa: TIntegerField;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    ConfiguraoAmbiente1: TMenuItem;
    qryParametroEmpresa: TADOQuery;
    Timer2: TTimer;
    qryTestaConn: TADOQuery;
    Image3: TImage;
    Timer3: TTimer;
    btAlerta: TcxButton;
    btTrocarLogin: TcxButton;
    qryValidaLoginnCdLojaPadrao: TIntegerField;
    cxStylesER2: TcxStyleRepository;
    Header: TcxStyle;
    FonteSomenteLeitura: TcxStyle;
    SP_GERA_ALERTA: TADOStoredProc;
    qryUnidadeMedida: TADOQuery;
    qryUnidadeMedidanCdUnidadeMedida: TAutoIncField;
    LinhaVermelha: TcxStyle;
    LinhaAmarela: TcxStyle;
    LinhaCinza: TcxStyle;
    LinhaAzul: TcxStyle;
    HeaderPDV: TcxStyle;
    HeaderCaixa: TcxStyle;
    PopupMenu1: TPopupMenu;
    AdicionaremFavoritos1: TMenuItem;
    Footer: TcxStyle;
    DataSource1: TDataSource;
    qryValidaLogincFlgVendedor: TIntegerField;
    qryValidaLogincFlgCaixa: TIntegerField;
    qryValidaLogincFlgGerente: TIntegerField;
    cmdZeraTemp: TADOCommand;
    qryParametroEmpresacValor: TStringField;
    SP_LOG_ACESSO: TADOStoredProc;
    qryTabSimNao: TADOQuery;
    qryTabSimNaonCdTabSimNao: TIntegerField;
    qryTabSimNaocNmTabSimNao: TStringField;
    printDBGridEh: TPrintDBGridEh;
    qryUltimoAcesso: TADOQuery;
    qryUltimoAcessonCdAplicacao: TIntegerField;
    qryUltimoAcessocNmAplicacao: TStringField;
    qryUltimoAcessocNmObjeto: TStringField;
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecNmComputador: TStringField;
    qryConfigAmbientecPortaMatricial: TStringField;
    qryConfigAmbientecFlgUsaECF: TIntegerField;
    qryConfigAmbientecModeloECF: TStringField;
    qryConfigAmbientecPortaECF: TStringField;
    qryConfigAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryConfigAmbienteiViasECF: TIntegerField;
    qryConfigAmbientecPortaEtiqueta: TStringField;
    qryConfigAmbientecFlgEmiteNFe: TIntegerField;
    qryConfigAmbientecNrSerieCertificadoNFe: TStringField;
    qryConfigAmbientedDtUltAcesso: TDateTimeField;
    qryConfigAmbientecNmUsuarioUltAcesso: TStringField;
    ConteudoCaixa: TcxStyle;
    ConteudoCaixaGrande: TcxStyle;
    LinhaPreta: TcxStyle;
    qryDataServidor: TADOQuery;
    qryDataServidordDataAtual: TDateTimeField;
    SP_PROXIMO_CODIGO: TADOStoredProc;
    qryConvUM: TADOQuery;
    qryConvUMnFatorConversaoUM: TBCDField;
    tMensagens: TTimer;
    SP_SEND_MAIL: TADOStoredProc;
    qryUsuarioAplicacao: TADOQuery;
    qryUsuarioAplicacaoCOLUMN1: TIntegerField;
    qryVersaoSistema: TADOQuery;
    qryConfigAmbientecNmImpCompartilhadaPDV: TStringField;
    qryConfigAmbientecNmImpCompartilhadaEtiqueta: TStringField;
    qryConfigAmbientecFlgUsaMapeamentoAutPDV: TIntegerField;
    qryConfigAmbientecFlgUsaMapeamentoAutEtiqueta: TIntegerField;
    ACBrGAV1: TACBrGAV;
    qryConfigAmbientecFlgUsaGaveta: TIntegerField;
    qryConfigAmbientecNmPortaGaveta: TStringField;
    qryConfigAmbientenCdTipoGavetaPDV: TIntegerField;
    Panel1: TPanel;
    Panel2: TPanel;
    edtBuscaApp: TcxTextEdit;
    tvMenus: TcxTreeView;
    ComboLoja: TComboBox;
    btBuscaApp: TcxButton;
    qryValidaLogincFlgLoginDuplicado: TIntegerField;
    btSobre: TMenuItem;
    N2: TMenuItem;
    ACBrECF1: TACBrECF;
    qryConfigAmbientecCdAtivacaoSAT: TStringField;
    qryConfigAmbientecAssinaturaACSAT: TStringField;
    qryVersaoSistemanCdVersaoSistema: TAutoIncField;
    qryVersaoSistemadDtInicioVersao: TDateTimeField;
    qryVersaoSistemadDtFimVersao: TDateTimeField;
    qryVersaoSistemacNmVersaoSistema: TStringField;
    qryValidaLogincEmail: TStringField;
    btFeedback: TMenuItem;
    btHistVersao: TMenuItem;
    Ajuda1: TMenuItem;
    btGuiaOperacional: TMenuItem;
    N3: TMenuItem;
    btSuporteInt: TMenuItem;
    N4: TMenuItem;
    IdHTTP1: TIdHTTP;
    IdFTP1: TIdFTP;
    btER2Update: TMenuItem;
    qryConfigAmbientecFlgUpdateER2: TIntegerField;
    ACBrSAT1: TACBrSAT;
    ACBrSATExtratoFortes1: TACBrSATExtratoFortes;
    ACBrSATExtratoESCPOS1: TACBrSATExtratoESCPOS;
    ACBrPosPrinter1: TACBrPosPrinter;
    qryAux: TADOQuery;
    qryVerificaUpdate: TADOQuery;
    qryVerificaUpdatecFlgUpdateER2: TIntegerField;
    pnNotificacao: TPanel;
    Label1: TLabel;
    Image1: TImage;
    qryConfigAmbientecCNPJTEF: TStringField;
    qryConfigAmbientenPDVTEF: TIntegerField;
    qryConfigAmbientecChaveAutTEF: TStringField;
    qryConfigAmbientecFlgTEFAtivo: TIntegerField;
    procedure MenuClick(Sender: TObject);
    function OpenModule(cModule: string; cCaptionForm: string = ''): TForm;           
    procedure Timer1Timer(Sender: TObject);
    procedure emFoco(Sender: TObject) ;
    procedure semFoco(Sender: TObject) ;
    function OpenForm(cForm : TForm; cCaptionForm: string = ''): TForm;
    function BuscaPK() : Integer ;
    function LogAuditoria (nCdTabelaSistema: integer ; nCdTipoAcao: integer; nID : integer; cObserv : string) : boolean ;
    procedure ExibeAuditoria(nCdTabelaSistema:Integer; nCdID:Integer) ;
    function Cripto(Texto: String): String;
    function DesCripto(Texto: String): String;
    procedure FormShow(Sender: TObject);
    function ValidaLogin(cIPServidor:String; cLogin:String; cSenha:String): boolean ;
    procedure ConectaDB(cIPServidor:String) ;
    function TestaCpfCgc(Dado: string): String;
    procedure SelecionaEmpresa;
    procedure ToolButton1Click(Sender: TObject);
    procedure PreparaMenu;
    procedure tvMenusDblClick(Sender: TObject);
    procedure tvMenusClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure NavegaEnter(Sender: TObject; var Key: Char);
    procedure SAir1Click(Sender: TObject);
    procedure rocarLogon1Click(Sender: TObject);
    procedure AlterarEmpresa1Click(Sender: TObject);
    function ConvInteiro(cValor:String) : integer ;
    function ConvData(cValor:String) : TDateTime ;
    function UsuarioEmpresaAutorizada : integer ;
    function LeParametro(cParametro:String): String;
    procedure EnterColor(Sender: TWinControl) ;
    procedure ExitColor(Sender: TWinControl) ;
    procedure ScreenActiveControlChange(Sender: TObject) ;
    procedure ConfiguraoAmbiente1Click(Sender: TObject);
    function LeParametroEmpresa(cParametro:String): String;
    procedure ConnectionWillExecute(Connection: TADOConnection;
      var CommandText: WideString; var CursorType: TCursorType;
      var LockType: TADOLockType; var CommandType: TCommandType;
      var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
      const Command: _Command; const Recordset: _Recordset);
    procedure Timer2Timer(Sender: TObject);
    function checkdigit11(codigo: variant; tamanho: integer): integer;
    procedure FormActivate(Sender: TObject);
    procedure ACBrECF1MsgAguarde(const Mensagem: String);
    procedure FormCreate(Sender: TObject);
    procedure TrataErros(Sender: TObject; E: Exception);
    function TBRound(Value: Extended; Decimals: integer): Extended;
    function RightStr(Const Str: String; Size: Word): String;
    procedure ComboLojaSelect(Sender: TObject);
    procedure ConnectionExecuteComplete(Connection: TADOConnection;
      RecordsAffected: Integer; const Error: Error;
      var EventStatus: TEventStatus; const Command: _Command;
      const Recordset: _Recordset);
    procedure Timer3Timer(Sender: TObject);
    procedure btAlertaClick(Sender: TObject);
    procedure btTrocarLoginClick(Sender: TObject);
    procedure StatusBar1Click(Sender: TObject);
    procedure ComboLojaExit(Sender: TObject);
    procedure ConnectionAfterConnect(Sender: TObject);
    procedure tvMenusKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function MascaraDecimal(): String ;
    procedure MensagemPadrao (cMensagem : String ; iTipo : integer ; iSleep : integer) ;
    procedure MensagemAlerta(cTexto: string);
    procedure ShowMessage(cTexto: string);
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure MensagemErro(cTexto: string);
    function ZeroEsquerda(Texto: String; Qtde: Integer): String;
    procedure AdicionaremFavoritos1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ZeraTemp(cNmTabela : string);
    function GetAveCharSize(Canvas: TCanvas): TPoint;
    function InputQuery(const ACaption, APrompt: string; var Value: string): Boolean;
    procedure GeraLog(nCdAplicacao:integer) ;
    procedure ImprimeDBGrid(DBGridEh1 : TDBGridEh; cTitulo : string);
    function validaUnidadeMedida(cSigla:string): Boolean;
    procedure configuraAmbiente(cNmComputador:string);
    procedure mensagemUsuario(cMsg:string);
    procedure limpaBufferTeclado;
    procedure limpaBufferMouse;
    function arredondaVarejo(nValor:Extended): Extended;
    procedure validaDataServidor();
    function fnProximoCodigo(cNmTabela:string): integer;
    procedure ACBrECF1MsgPoucoPapel(Sender: TObject);
    function StringToCaseSelect (Selector : string; CaseList: array of string): Integer;
    procedure showForm(objForm : TForm; bDestroirObjeto : boolean) ;
    function fnFatorConversaoUM(cUnidadeMedidaDe, cUnidadeMedidaPara: string) : double;
    function fnConverteQtdeUnidadeMedida(cUnidadeMedidaDe, cUnidadeMedidaPara: string; nQtdeAConverter: double) : double;
    function fnValidaHoraDecimal(nValorHora : double) : boolean ;
    procedure nShowMessage (Mensagem : String);
    procedure nMensagemAlerta (Mensagem : String);
    procedure nMensagemErro (Mensagem : String);
    procedure tMensagensTimer(Sender: TObject);
    procedure SendMail (cPara,cAssunto,cMensagem : String; cNmRemetente : String = ''; cCaminhoAnexo : String = '');
    function fnValidaUsuarioAPL(cNmObjeto:string) : boolean;
    function fnValidaMascaraContabil(cMascara:string) : boolean;
    function fnAplicaMascaraContabil( cMascara , cFormatacao : string ) : string ;
    function fnMapeamentoImpressora(Porta, Workstation, Compartilhamento:string):string;
    procedure edtBuscaAppPropertiesChange(Sender: TObject);
    procedure btBuscaAppClick(Sender: TObject);
    procedure edtBuscaAppKeyPress(Sender: TObject; var Key: Char);
    procedure prAbreGaveta();
    function fnValidaPeriodo(dDtInicial, dDtFinal : TDate) : Boolean;
    function ArredondaDecimal(Value: extended; Decimals: integer): extended;
    procedure btSobreClick(Sender: TObject);
    function fnValidarEMail(strEmail: String): Boolean;
    procedure prPreparaAmbiente();
    procedure btFeedbackClick(Sender: TObject);
    procedure btHistVersaoClick(Sender: TObject);
    procedure btGuiaOperacionalClick(Sender: TObject);
    procedure btSuporteIntClick(Sender: TObject);
    procedure IdFTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure IdFTP1WorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure prGeraArqConfigIni;
    procedure fnDownloadFTP(cCaminhoApp, cNomeApp : String; bIniciarApp : Boolean);
    procedure btER2UpdateClick(Sender: TObject);
    procedure ExportExcelDinamic(cTitulo, cFiltro, cFiltroAux: String; qryAux: TADOQuery);
    procedure prExecutaAplicativo(cCaminho : String);
    procedure IdFTP1Work(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
  private
    { Private declarations }
    arrObjetos : Array of Array of String;
    bTransacao : Boolean;
    myThread   : Tthread1;
    nTotal     : Integer;
    waitTime   : Integer;
    DelayMsg   : Integer;
    frmMsg     : TForm;
    procedure ER2Mensagem(Mensagem, Tipo : String; Delay: integer = 3); // TIPO (ERRO,INFORMACAO,ALERTA)

  public
    { Public declarations }

    nCdUsuarioLogado    : Integer ;
    cNmUsuarioLogado    : String  ;
    cNmCompletoUsuario  : String  ;
    cEmailUsuario       : String  ;
    cFlgUsuarioMaster   : String  ;
    nCdEmpresaAtiva     : Integer ;
    cNmEmpresaAtiva     : String  ;
    cNmFantasiaEmpresa  : String  ;
    cIPServidor         : String  ;
    cInstancia          : String  ;
    cNomeComputador     : String  ;
    nCdLojaAtiva        : integer ;
    cNmLojaAtiva        : String  ;
    cConnectionString   : String  ;
    cModeloECF          : String  ;
    cFlgUsaPDV          : Integer ;
    nCdLojaPadrao       : integer ;
    cNmEmpresaCupom     : string  ;
    cNmEmpresaCupom2    : string  ;
    iCasaDecimal        : integer ;
    cMascaraCompras     : string  ;
    cCartaoNSU          : string  ;
    cCartaoTransacao    : string  ;
    cCartaoOperadora    : string  ;
    cFlgUsuarioGerente  : integer ;
    cFlgUsuarioVendedor : integer ;
    cFlgLogAcesso       : string  ;
    cFlgWERP            : integer ;
    nCdTerceiroUsuario  : integer ;
    cMsgCupomFiscal     : string  ;
    estadosBrasileiros  : string  ;
    cNmVersaoSistema    : String;
    cPathSistema        : String;
    cPathEmpAtiva       : String;
    procedure ACBRPosPrinter1AguardarPronta(confTEF:boolean);
    procedure ACBRPosPrinter1FechaImpTermica;
    procedure CapptaLog(cStr:String);
    procedure CapptaCreateLogfile;
    function RetNumeros(s:String):String;
  end;

const
  focusColor = $00CCF8FF ; //$00AAFFFF ;

var
  lastFocused : TWinControl;
  originalColor : TColor;
  frmMenu: TfrmMenu;
  PageControlPageDock: TPageControl;

implementation

uses Math,fBuscaPK, fAuditoria, fLogin, fSelecEmpresa, fAltSenha, fSelecServer,
  fConfigAmbiente, fSelecLoja, fErroPadrao,commctrl, frmSQLWait , fAlertas, fUtils, fMensagem, fTimedMessage,
  fLookup_Padrao, fSobre, fEmiteNFe2, fGuiaOperacional, fFeedback, fHistoricoVersoes;

{$R *.dfm}

function TfrmMenu.fnMapeamentoImpressora(Porta, Workstation, Compartilhamento: String): String;
var
  cComando : String;
begin
  { -- exclui log tempor�rio -- }
  if (FileExists('C:\Temp\ER2Temp.txt')) then
      WinExec(Pchar('cmd /c del C:\Temp\ER2Temp.txt'),SW_HIDE);

  { -- libera porta e efetua o mapeamento da impressora -- }
  cComando :=  'NET USE ' + Porta + ' /delete';
  WinExec(Pchar('cmd /c '+ cComando ),SW_HIDE);

  cComando :=  'NET USE ' + Porta + ': ' + '\\' + Workstation + '\' + Compartilhamento + '>> C:\Temp\ER2Temp.txt date% 2>&1';
  WinExec(Pchar('cmd /c date /t >> C:\Temp\ER2Temp.txt'+ cComando ),SW_HIDE);
  WinExec(Pchar('cmd /c '+ cComando ),SW_HIDE);
end;

function TfrmMenu.GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

function TfrmMenu.InputQuery(const ACaption, APrompt: string; var Value: string): Boolean;
var
  Form: TForm;
  Prompt: TLabel;
  Edit: TEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := False;
  Form := TForm.Create(Application);
  with Form do
    try
      Canvas.Font := Font;
      DialogUnits := GetAveCharSize(Canvas);
      BorderStyle := bsDialog;
      Caption     := ACaption;
      Color       := clWhite ;
      ClientWidth := MulDiv(180, DialogUnits.X, 4);
      ClientHeight := MulDiv(63, DialogUnits.Y, 8);
      Position := poScreenCenter;
      Prompt := TLabel.Create(Form);
      Font.Name   := 'Consolas' ;
      Font.Size   := 10 ;
      with Prompt do
      begin
        Parent := Form;
        AutoSize := True;
        Left := MulDiv(8, DialogUnits.X, 4);
        Top := MulDiv(8, DialogUnits.Y, 8);
        Caption := APrompt;
      end;
      Edit := TEdit.Create(Form);
      with Edit do
      begin
        Parent := Form;
        Left := Prompt.Left;
        Top := MulDiv(19, DialogUnits.Y, 8);
        Width := MulDiv(164, DialogUnits.X, 4);
        MaxLength := 255;
        Text := Value;
        SelectAll;
      end;
      ButtonTop := MulDiv(41, DialogUnits.Y, 8);
      ButtonWidth := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := 'Ok';
        ModalResult := mrOk;  // Unit Controls
        Default := True;
        SetBounds(MulDiv(38, DialogUnits.X, 4), ButtonTop, ButtonWidth,
          ButtonHeight);
      end;
      with TButton.Create(Form) do
      begin
        Parent := Form;
        Caption := 'Cancelar';
        ModalResult := mrCancel;
        Cancel := True;
        SetBounds(MulDiv(92, DialogUnits.X, 4), ButtonTop, ButtonWidth,
          ButtonHeight);
      end;
      if ShowModal = mrOk then
      begin
        Value := Edit.Text;
        Result := True;
      end;
    finally
      Form.Free;
    end;
end;

function TfrmMenu.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;
    
    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmMenu.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
end;

procedure TfrmMenu.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
end;

procedure TfrmMenu.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
end;

constructor TThread1.Create;
begin
    inherited Create(true);

end;

destructor TThread1.Destroy;
begin
    inherited;
end ;

procedure TThread1.Execute;
begin
    inherited ;

    myForm := TfrmWait.Create(Application) ;
    myForm.Show;

    {while frmMenu.bTransacao do
    begin
        Synchronize((myForm as TfrmWait).Repaint) ;
    end ;}

end;

function TfrmMenu.TBRound(Value: Extended; Decimals: integer): Extended;
var
 Factor, Fraction: Extended;
begin
 Factor := IntPower(10, Decimals);
 { A convers�o para string e depois para float evita
   erros de arredondamentos indesej�veis. }
 Value := StrToFloat(FloatToStr(Value * Factor));
 Result := Int(Value);
 Fraction := Frac(Value);
 if Fraction >= 0.5 then
   Result := Result + 1
 else if Fraction <= -0.5 then
   Result := Result - 1;
 Result := Result / Factor;
end;


Procedure TfrmMenu.TrataErros(Sender: TObject; E: Exception);
var
    i, iUltimaPosicao : integer ;
begin
    frmErroPadrao.Memo1.Lines.Clear;

    for i := 0 to length(E.Message) do
    begin
        if (Copy(E.Message,i+1,1) = ']') then
            iUltimaPosicao := i
    end ;

    if iUltimaPosicao > 0 then
    begin
        frmErroPadrao.Memo1.Lines.Add('Mensagem Erro: ' + Copy(E.Message,iUltimaPosicao+2,length(E.Message)-(iUltimaPosicao-1))) ;
        frmErroPadrao.Memo1.Lines.Add('') ;
        frmErroPadrao.Memo1.Lines.Add('Fonte        : ' + Copy(E.Message,1,iUltimaPosicao+1)) ;
    end
    else frmErroPadrao.Memo1.Lines.Add(E.Message) ;

    MessageBeep(84) ;
    frmErroPadrao.cInterface := StatusBar1.Panels[5].Text ;
    frmErroPadrao.ShowModal;      
end ;

function TfrmMenu.checkdigit11(codigo: variant; tamanho: integer): integer;
//
// Retornar o digito verificador pelo algoritmo de cheque
// digito modulo 11
// Sintaxe: chk11(codigo,tamanho): digito
// Exemplo: chk11(3378,5) -> 2
//          chk11('4.268',5) -> 4
//
var
    i:      integer;
    digito: integer;
    indice: integer;
    soma:   integer;
    cod:    string; 
Begin

    cod := codigo;

    if length(cod) < tamanho Then
    begin

        for i := length(cod) to (tamanho-1) do
        begin
            cod := '0' + cod;
        end;

    end;

    soma := 0;
    indice := (tamanho mod 8) + 2;

    for i := 1 to tamanho do
    Begin

        digito := strtoint(copy(cod,i,1));
        indice := indice - 1;

        if indice < 2 Then
        Begin
            indice := 9;
        end;

        soma := soma + (digito * indice);

    end;

    digito := 11 - (soma mod 11);

    if (digito > 9) Then
    Begin
        digito := 0;
    end;

    Result := digito;

end;

procedure TfrmMenu.ScreenActiveControlChange(Sender: TObject) ;
var
   doEnter, doExit : boolean;
   previousActiveControl : TWinControl;
begin

   if Screen.ActiveControl = nil then
   begin
     lastFocused := nil;
     Exit;
   end;

   doEnter := true;
   doExit := true;

   //CheckBox
   if Screen.ActiveControl is TButtonControl then doEnter := false;
   if Screen.ActiveControl is TDbGridEh then doEnter := false;

   previousActiveControl := lastFocused;

   if previousActiveControl <> nil then
   begin
     //CheckBox
     if previousActiveControl is TButtonControl then doExit := false;
     if previousActiveControl is TDbGridEh then doExit := false;
     if previousActiveControl is TForm then doExit := false ;
     if previousActiveControl is TToolBar then doExit := false ;
   end;

   lastFocused := Screen.ActiveControl;

   if doExit then ExitColor(previousActiveControl) ;

   if Uppercase(Screen.ActiveControl.ClassName) = 'TTOOLBAR' then
      exit ; //doEnter := False ;

   if Uppercase(Screen.ActiveControl.ClassName) = 'TPANEL' then
      exit ;

   if Copy(Uppercase(Screen.ActiveControl.Name),1,3) = 'FRM' then
      exit ;

   if doEnter then EnterColor(lastFocused) ;
end;

procedure TfrmMenu.EnterColor(Sender: TWinControl) ;
var
    cxBtConsulta : TcxButton;
begin
   if Sender <> nil then
   begin
     if IsPublishedProp(Sender,'Color') then
     begin
       originalColor := GetOrdProp(Sender,'Color') ;
       SetOrdProp(Sender,'Color', focusColor) ;
     end;

{     if (Uppercase(Sender.ClassName) = 'TDBEDIT') then
     begin
         cxBtConsulta := TcxButton.Create(Self) ;
         cxBtConsulta.Top := Sender.Top;
         cxBtConsulta.Width := 50 ;
         cxBtConsulta.Left := Sender.Left+Sender.Width+10 ;
         cxBtConsulta.Caption := 'teste';
         cxBtConsulta.BringToFront;
         cxBtConsulta.Update;


     end ;}

   end;
end;

procedure TfrmMenu.ExitColor(Sender: TWinControl) ;
begin
   if Sender <> nil then
   begin
     if IsPublishedProp(Sender,'Color') then
     begin
       SetOrdProp(Sender,'Color',originalColor) ;
     end;
   end;
end;

procedure TfrmMenu.MenuClick(Sender: TObject);
var
    cAplicacao : String;
begin

  OpenModule(TMenuItem(Sender).Name, TMenuItem(Sender).Caption);

  if PageControlPageDock.ActivePage <> nil then
  begin
      PageControlPageDock.ActivePage.Highlighted := True;
  end;

end;

function TfrmMenu.OpenModule(cModule: string; cCaptionForm: string = ''): TForm;
var
  bCriar    : Boolean;
  Inst      : TPersistentClass;
  Instance  : TComponent;
  bModal    : Boolean;
  cAplicacao: string ;
begin

  cAplicacao := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex+500] ;
  GeraLog(strToint(cAplicacao)) ;

  if PageControlPageDock.ActivePage <> nil then
  begin
    PageControlPageDock.ActivePage.Highlighted := False;
  end;

  bModal := False ;

  if (Copy(cModule,1,5) = 'MODAL') then
  begin
      bModal := True ;
      cModule := Copy(cModule,7,(Length(cModule))) ;
  end ;

  Inst   := FindClass('t' + cModule);
  bCriar := True;
  Result := nil;

  if bCriar then
  begin

    Instance := Inst.NewInstance as TComponent;
    Result   := Instance.Create(Self) as TForm;

    if (bModal) then
    begin
        Result.ShowModal;
        exit ;
    end ;
    
    if cCaptionForm <> '' then
    begin
      Result.Caption := StringReplace(cCaptionForm,'&','',[rfReplaceAll]) ; //('&', cCaptionForm, '');
    end;

    Result.ManualDock(frmMenu.PageControlPageDock, nil, alClient);
    PageControlPageDock.ActivePage            := PageControlPageDock.Pages[PageControlPageDock.PageCount-1];
    PageControlPageDock.ActivePage.ImageIndex := -1;
    PageControlPageDock.ActivePage.Caption    := StringReplace(cCaptionForm,'&','',[rfReplaceAll]);
    Result.ClientHeight                       := PageControlPageDock.ActivePage.Height;
    Result.Tag                                := PageControlPageDock.ActivePageIndex;

  end;

  PageControlPageDock.ActivePage            := PageControlPageDock.Pages[Result.Tag];
  PageControlPageDock.Pages[Result.Tag].Tag := 100 ;
  Result.Visible                            := True;
  Result.Font.Name := 'Tahoma' ;
  Result.Font.Size := 8 ;

  try

      Result.SetFocus;
  except
  end ;

end;


function TfrmMenu.OpenForm(cForm : TForm; cCaptionForm: string = ''): TForm;
var
  bCriar  : Boolean;
begin

  if PageControlPageDock.ActivePage <> nil then
  begin
    PageControlPageDock.ActivePage.Highlighted := False;
  end;

  Result := nil;

  if bCriar then
  begin

    if cCaptionForm <> '' then
    begin
      cForm.Caption := StringReplace(cCaptionForm,'&','',[rfReplaceAll]) ; //('&', cCaptionForm, '');
    end;

    cForm.ManualDock(frmMenu.PageControlPageDock, nil, alClient);
    PageControlPageDock.ActivePage            := PageControlPageDock.Pages[PageControlPageDock.PageCount-1];
    PageControlPageDock.ActivePage.ImageIndex := -1;
    PageControlPageDock.ActivePage.Caption    := StringReplace(cCaptionForm,'&','',[rfReplaceAll]);
    cForm.ClientHeight                       := PageControlPageDock.ActivePage.Height;
    cForm.Tag                                := PageControlPageDock.ActivePageIndex;

  end;

  PageControlPageDock.ActivePage := PageControlPageDock.Pages[cForm.Tag];
  cForm.Visible                 := True;
  cForm.Font.Name := 'Tahoma' ;
  cForm.Font.Size := 8 ;
  cForm.SetFocus;

end;


function ValorAsc(Letra: String): Byte;
begin
    if Length(letra) > 0 then ValorAsc := Ord(Letra[1]) else ValorAsc := 0;
end;

function TfrmMenu.LeParametro(cParametro:String): String;
begin
    qryParametro.Close ;
    qryParametro.Parameters.ParamByName('cParametro').Value := cParametro ;
    qryParametro.Open ;

    if (qryParametro.eof) then
        MensagemErro('Parametro ' + cParametro + ' n�o encontrado.') ;

    Result := qryParametrocValor.Value ;

end ;

function TfrmMenu.LeParametroEmpresa(cParametro:String): String;
begin
    qryParametroEmpresa.Close ;
    qryParametroEmpresa.Parameters.ParamByName('cParametro').Value := cParametro ;
    qryParametroEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nCdEmpresaAtiva ;
    qryParametroEmpresa.Open ;

    if (qryParametroEmpresa.eof) then
        MensagemErro('Parametro Empresa ' + cParametro + ' n�o encontrado.') ;

    Result := qryParametroEmpresacValor.Value ;

end ;

function TfrmMenu.Cripto(Texto: String): String;
var
  Cont, Cod: Integer;
  Retorna: String;
begin

  for Cont := 1 to Length(Texto) do
  begin
    Cod := ValorAsc(Copy(Texto, Cont, 1));
    Retorna := Retorna + Chr(Cod + 57(*valor a ser adicionado - pode ser modificado*));
  end;

  Cripto := Retorna;

end;

function TfrmMenu.DesCripto(Texto: String): String;
var
  Cont, Cod: integer;
  Retorna: String;
begin
  for Cont := 1 to Length(Texto) do
  begin
    Cod := ValorAsc(Copy(Texto, Cont, 1));
    Retorna := Retorna + Chr(Cod - 57);
  end;
  DesCripto := Retorna;
end;

function TfrmMenu.BuscaPK() : Integer ;
begin
    Result := frmBuscaPK.AbreFormPK() ;
end ;

procedure TfrmMenu.ExibeAuditoria(nCdTabelaSistema:Integer; nCdID:Integer) ;
begin
    if (nCdID <= 0) then
        exit ;

    frmAuditoria.ExibeAuditoria(nCdTabelaSistema, nCdID) ;
end;

function TfrmMenu.LogAuditoria (nCdTabelaSistema: integer ; nCdTipoAcao: integer; nID : integer; cObserv : string) : boolean ;
begin

  try
    qryLogAuditoria.Close ;
    qryLogAuditoria.Open ;

    qryLogAuditoria.Append ;
    qryLogAuditoria.FieldByName('nCdTabelaSistema').Value := nCdTabelaSistema ;
    qryLogAuditoria.FieldByName('nCdUsuario').Value       := nCdUsuarioLogado ;
    qryLogAuditoria.FieldByName('nCdTipoAcao').Value      := nCdTipoAcao ;
    qryLogAuditoria.FieldByName('dDtLog').Value           := Now() ;
    qryLogAuditoria.FieldByName('ID').Value               := nID ;
    qryLogAuditoria.FieldByName('cObserv').Value          := cObserv ;
    qryLogAuditoria.FieldByName('nCdLogAuditoria').Value  := frmMenu.fnProximoCodigo('LOGAUDITORIA') ;
    qryLogAuditoria.Post ;

    qryLogAuditoria.Close ;

  except
    raise ;
  end ;

  Result := True ;
end ;

procedure TfrmMenu.Timer1Timer(Sender: TObject);
begin
    StatusBar1.Panels[3].Text := DateTimeToStr(Now) ;
end;

procedure TfrmMenu.emFoco(Sender: TObject) ;
begin

   if (Sender is TEdit) then
   begin
       (sender as TEdit).Color     := $00AAFFFF;
       (sender as TEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBMemo) then
   begin
       (sender as TDBMemo).Color     := $00AAFFFF;
       (sender as TDBMemo).Font.Color:= clBlack;
   end ;

   if (Sender is TMaskEdit) then
   begin
       (sender as TMaskEdit).Color     := $00AAFFFF;
       (sender as TMaskEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBEdit) then
   begin
       (sender as TDBEdit).Color     := $00AAFFFF;
       (sender as TDBEdit).Font.Color:= clBlack;
   end;

   if (Sender is TDBLookupComboBox) then
   begin
       (sender as TDBLookupComboBox).Color     := $00AAFFFF;
       (sender as TDBLookupComboBox).Font.Color:= clBlack;
   end ;

   if (Sender is TComboBox) then
   begin
       (sender as TComboBox).Color     := $00AAFFFF;
       (sender as TComboBox).Font.Color:= clBlack;
   end ;

end;

procedure TfrmMenu.semFoco(Sender: TObject) ;
begin

   if (Sender is TEdit) then
   begin
       (sender as TEdit).Color     := clWhite;
       (sender as TEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBMemo) then
   begin
       (sender as TDBMemo).Color     := clWhite;
       (sender as TDBMemo).Font.Color:= clBlack;
   end ;

   if (Sender is TMaskEdit) then
   begin
       (sender as TMaskEdit).Color     := clWhite;
       (sender as TMaskEdit).Font.Color:= clBlack;
   end ;

   if (Sender is TDBEdit) then
   begin
       (sender as TDBEdit).Color     := clWhite ; //clWhite;
       (sender as TDBEdit).Font.Color:= clBlack;
   end;

   if (Sender is TDBLookupComboBox) then
   begin
       (sender as TDBLookupComboBox).Color     := clWhite;
       (sender as TDBLookupComboBox).Font.Color:= clBlack;
   end ;

   if (Sender is TComboBox) then
   begin
       (sender as TComboBox).Color     := clWhite;
       (sender as TComboBox).Font.Color:= clBlack;
   end ;


end;

function TfrmMenu.RightStr(Const Str: String; Size: Word): String;
begin
  if Size > Length(Str) then Size := Length(Str) ;
  RightStr := Copy(Str, Length(Str)-Size+1, Size)
end;


procedure TfrmMenu.FormShow(Sender: TObject);
var r : TRect;
begin

    if (nCdUsuarioLogado = 0) then
    begin
        frmSelecServer.ShowModal; //Modal ;

        frmLogin.ShowModal ;
        (Sender as TForm).Repaint;

        configuraAmbiente(cNomeComputador);
        SelecionaEmpresa() ;
        PreparaMenu() ;

        Timer2.Enabled := True ;

    end ;

    cNmEmpresaCupom := LeParametro('NMEMCUPOMPDV') ;
    cNmEmpresaCupom2:= LeParametro('NMEMCUPOMPDV1') ;
    iCasaDecimal    := StrToint(LeParametro('CASADECCOMPRA')) ;
    cFlgLogAcesso   := LeParametro('GERALOGACESSO') ;
    cMascaraCompras := MascaraDecimal() ;

    Timer3.Enabled := True ;

    Statusbar1.perform(SB_GETRECT,0,integer( @R ));

    btAlerta.Parent := StatusBar1 ;
    btAlerta.Top    := r.top ;
    btAlerta.Left   := 1100 ;

    btTrocarLogin.Parent := StatusBar1 ;
    btTrocarLogin.Top    := r.top ;
    btTrocarLogin.Left   := 1068 ;

    ComboLoja.Parent := StatusBar1 ;
    ComboLoja.top    := r.top;            //set size of
    ComboLoja.left   := r.left;           //Progressbar to
    ComboLoja.width  := r.right-r.left;   //fit with panel
    ComboLoja.height := r.bottom-r.top;

    ComboLoja.Items.Clear;

    ComboLoja.Items.Add(RightStr('000' + IntToStr(frmMenu.nCdLojaAtiva),3) + ' - ' + cNmLojaAtiva);

    if (frmSelecLoja.qryLojas.Active) then
    begin
        frmSelecLoja.qryLojas.First ;

        while not frmSelecLoja.qryLojas.Eof do
        begin
            if (frmSelecLoja.qryLojasnCdLoja.Value <> nCdLojaAtiva) then
                ComboLoja.Items.Add(RightStr('000' + frmSelecLoja.qryLojasnCdLoja.AsString,3) + ' - ' + frmSelecLoja.qryLojascNmLoja.Value);

            frmSelecLoja.qryLojas.Next ;
        end ;

        ComboLoja.Enabled   := True ;

        ComboLoja.ItemIndex := 0 ;
    end ;

    if (ComboLoja.Items.Count <= 1) then
        ComboLoja.Enabled := False ;

    ComboLoja.Visible := False ;

    inherited ;
end;

procedure TfrmMenu.PreparaMenu;
var
    NovoMenu : TMenuItem;
    NovoItem : TMenuItem;
    NovoItem2 : TMenuItem;
    i, i2: integer ;
    raiz, rootno, no : TTreeNode;
begin

    SetLength(arrObjetos,0,0) ;
    SetLength(arrObjetos,5000,5000) ;

    { -- verifica as permiss�es de acesso do usu�rio -- }
    usp_PreparaMenu.Close ;
    usp_PreparaMenu.Parameters.ParamByName('@nCdUsuario').Value   := nCdUsuarioLogado ;
    usp_PreparaMenu.Parameters.ParamByName('@cNmAplicacao').Value := edtBuscaApp.Text ;
    usp_PreparaMenu.Open ;

    usp_PreparaMenu.First;

    if ((usp_PreparaMenu.Eof) and (Trim(edtBuscaApp.Text) = '')) then
    begin
        MensagemErro('Nenhum grupo de usu�rio ou previl�gio de acesso concedido.') ;
        Application.Terminate;
    end ;

    qryUltimoAcesso.Close ;
    qryUltimoAcesso.Parameters.ParamByName('nCdUsuario').Value := nCdUsuarioLogado ;
    qryUltimoAcesso.Open ;

    i := 0 ;

    tvMenus.Items.Clear;

    while not qryUltimoAcesso.eof do
    begin

        if (i = 0) then
        begin

          rootno := tvMenus.Items.AddObject(nil, 'Favoritos', Pointer(999999999)) ;
          tvMenus.Items.Item[i].ImageIndex    := 3 ;
          tvMenus.Items.Item[i].SelectedIndex := 3 ;

          i := i + 1 ;

          no := tvMenus.Items.AddChildObject(rootno, '�ltimos Acessos', Pointer(888888888)) ;
          tvMenus.Items.Item[i].ImageIndex    := 4 ;
          tvMenus.Items.Item[i].SelectedIndex := 4 ;

          i := i + 1 ;

        end ;

        tvMenus.Items.AddChildObject(no, qryUltimoAcessocNmAplicacao.Value , Pointer(qryUltimoAcessonCdAplicacao.Value));
        tvMenus.Items.Item[i].ImageIndex    := 1 ;
        tvMenus.Items.Item[i].SelectedIndex := 2 ;

        arrObjetos[i,i]     := qryUltimoAcessocNmObjeto.Value ;

        arrObjetos[i,i+100] := qryUltimoAcessocNmAplicacao.Value ;
        arrObjetos[i,i+500] := qryUltimoAcessonCdAplicacao.AsString ;

        qryUltimoAcesso.Next;

        i := i + 1 ;

    end ;

    if (i > 0) then
        tvMenus.Items.Item[0].Expand(True);

    qryUltimoAcesso.Close;
    usp_PreparaMenu.First;

    while not usp_PreparaMenu.Eof do
    begin

      if usp_PreparaMenu.FieldByName('nCdMenuPai').AsString = '' then
      begin
          rootno := tvMenus.Items.AddObject(nil, usp_PreparaMenu.FieldByName('cNmMenu').AsString, Pointer(usp_PreparaMenu.FieldByName('nCdMenu').AsInteger)) ;
          tvMenus.Items.Item[i].ImageIndex := 0 ;
      end ;

      if ((usp_PreparaMenu.FieldByName('nCdMenuPai').AsString <> '') And (usp_PreparaMenu.FieldByName('cNmAplicacao').AsString = '')) then
      begin
          no := tvMenus.Items.AddChildObject(rootno, usp_PreparaMenu.FieldByName('cNmMenu').AsString, Pointer(usp_PreparaMenu.FieldByName('nCdMenu').AsInteger)) ;
          tvMenus.Items.Item[i].ImageIndex := 0 ;
      end ;

      if (usp_PreparaMenu.FieldByName('cNmAplicacao').AsString <> '') then
          if Assigned(no) then Begin
              tvMenus.Items.AddChildObject(no, usp_PreparaMenu.FieldByName('cNmAplicacao').AsString, Pointer(usp_PreparaMenu.FieldByName('nCdAplicacao').AsInteger));
              tvMenus.Items.Item[i].ImageIndex    := 1 ;
              tvMenus.Items.Item[i].SelectedIndex := 2 ;
          end;

      if usp_PreparaMenu.FieldByName('cNmObjeto').Value <> null then
          arrObjetos[i,i] := usp_PreparaMenu.FieldByName('cNmObjeto').Value ;

      if usp_PreparaMenu.FieldByName('cNmAplicacao').Value <> null then
      begin
          arrObjetos[i,i+100] := usp_PreparaMenu.FieldByName('cNmAplicacao').Value ;
          arrObjetos[i,i+500] := usp_PreparaMenu.FieldByName('nCdAplicacao').asstring ;
      end ;

      usp_PreparaMenu.Next;

      i := i + 1 ;

    end;

    usp_PreparaMenu.Close ;

end ;

procedure TfrmMenu.SelecionaEmpresa;
var
   i : Integer;
   iEmpresaSelecionada : Integer ;
begin

    iEmpresaSelecionada := frmSelecEmpresa.SelecionaEmpresa(nCdUsuarioLogado) ;
    nCdEmpresaAtiva     := iEmpresaSelecionada ;

    frmLookup_Padrao.nCdEmpresaAtiva  := nCdEmpresaAtiva;

    if (LeParametro('VAREJO') = 'S') then
    begin
        nCdLojaAtiva := frmSelecLoja.SelecionaLoja(nCdUsuarioLogado) ;
        frmLookup_Padrao.nCdLojaAtiva := nCdLojaAtiva;
    end ;

    prPreparaAmbiente;
end ;

procedure TFrmMenu.ConectaDB(cIPServidor:String) ;
var
  i            : Integer;
  iCursor      : Integer;
  ArqConfigIni : TIniFile;
begin
    If (cIPServidor = '') then
        cIPServidor := '127.0.0.1' ;

    Connection.Close;
    //Connection.ConnectionString := 'Provider=MSDASQL.1;Password=ADMSoft101580;Persist Security Info=True;User ID=ADMSoft;Data Source=ADMSoft;Initial Catalog=ADMSoft' ;

    if (cInstancia = '') then
        cInstancia := 'ADMSOFT' ;

    Connection.ConnectionString := 'provider=MSDASQL.1;Network Library=DBMSSOCN;driver={SQL Server};Server=' + cIpServidor + ';uid=ADMSoft;pwd=admsoft101580;database=' + cInstancia ;
    cConnectionString           := Connection.ConnectionString ;
    Connection.LoginPrompt      := false ;

    //Connection.ConnectionString := 'Diver={Microsoft SQL Server};Provider=MSDASQL.1;Password=ADMSoft101580;Persist Security Info=True;User ID=ADMSoft;Host=' + cIpServidor + ';Initial Catalog=ADMSoft' ;

    try
        iCursor := Screen.Cursor ;
        Screen.Cursor := crSQLWait ;
        Connection.Open();
    except
        Screen.Cursor := iCursor ;
        MensagemErro('N�o foi poss�vel estabelecer uma conex�o com o servidor de banco de dados') ;
        Raise ;
        abort ;
    end ;

    Screen.Cursor := iCursor ;

    StatusBar1.Panels[4].Text := ('Servidor : ' + frmLogin.EdtServidor.Text);

    //for i:= 0 to COnnection.Properties.Count do
    //    ShowMessage(IntToStr(i) + ' - ' + Connection.Properties.Item[i].Name + ' - ' + String(Connection.Properties.Item[i].Value)) ;

    { -- verifrica vers�o do sistema -- }
    qryVersaoSistema.Close;
    qryVersaoSistema.Open;

    qryConfigAmbiente.Close;
    qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := Uppercase(GetHostByName( nil )^.h_name);
    qryConfigAmbiente.Open;

    pnNotificacao.Visible := (qryConfigAmbientecFlgUpdateER2.Value = 1);

    if (qryVersaoSistemacNmVersaoSistema.Value <> cNmVersaoSistema) then
    begin
        ShowMessage('Existe uma nova vers�o do ER2Soft (' + qryVersaoSistemacNmVersaoSistema.Value + ') dispon�vel para download, para realizar a atualiza��o o sistema ser� encerrado.');

        fnDownloadFTP('.\ER2Instal.exe', 'ER2Instal.exe', False);

        if (not FileExists('.\ER2Instal.exe')) then
        begin
            MensagemErro('Execut�vel de atualiza��o n�o encontrado, contate o administrador do sistema.');
            Application.Terminate;
        end;

        prGeraArqConfigIni;

        PostMessage(FindWindow('TfrmEr2Instal', nil), WM_CLOSE, 0, 0);

        WinExec(PAnsiChar('ER2Instal.exe ' + cIPServidor + ' ' + cInstancia + ' ' + cNmVersaoSistema + ' 0 ' + qryConfigAmbientecNmComputador.Value),SHOW_OPENWINDOW);

        Application.Terminate;
    end;

    if (LeParametro('VAREJO') = 'S') then
        StatusBar1.Panels[0].Text := cNmVersaoSistema + '-Varejo'
    else
        StatusBar1.Panels[0].Text := cNmVersaoSistema + '-ERP';

    qryTabSimNao.Open;
end ;

function TfrmMenu.UsuarioEmpresaAutorizada : integer ;
begin

  qryUsuarioEmpresa.Close ;
  qryUsuarioEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryUsuarioEmpresa.Open ;

  if (qryUsuarioEmpresa.eof) then
  begin
      Result := -1 ;
      exit
  end ;

  if (qryUsuarioEmpresa.RecordCount = 1) then
  begin

    Result := qryUsuarioEmpresanCdEmpresa.value ;
    exit ;

  end ;

  Result := 0 ;

end ;

function TfrmMenu.ValidaLogin(cIPServidor:String; cLogin:String; cSenha:String): boolean ;
var
    wsaData: TWSAData ;
begin

    ConectaDB(cIPServidor) ;
    validaDataServidor();

    qryValidaLogin.Close ;
    qryValidaLogin.Parameters.ParamByName('cNmLogin').Value := cLogin ;
    qryValidaLogin.Parameters.ParamByName('cSenha').Value   := frmMenu.Cripto(UpperCase(cSenha)) ;
    qryValidaLogin.Open ;

    If (qryValidaLogin.Eof) then
    begin
        Result := False ;
        MensagemErro('Usu�rio ou Senha inv�lido.') ;
        exit ;
    end ;

    if (qryValidaLogincFlgAtivo.Value <> 1) then
    begin
        MensagemErro('Usu�rio Inativo ou Bloqueado.') ;
        Result := False ;
        exit ;
    end ;

    if (qryValidaLogincFlgLoginDuplicado.Value = 1) then
    begin
        MensagemErro('Usu�rio com login duplicado. Atualize seu cadastro e efetue o login novamente.');
        Result := False;
        Exit;
    end;

    if (((qryValidaLogindDtUltAltSenha.Value + 30) < Now()) Or (qryValidaLogincTrocaSenhaProxLogin.Value=1))then
    begin
        ShowMessage('Sua senha expirou. Clique em OK para troc�-la agora.') ;

        frmAltSenha.ShowModal ;
    end ;

    frmMenu.Connection.BeginTrans ;

    try
        qryAtuUltAcesso.Close ;
        qryAtuUltAcesso.Parameters.ParamByName('nCdUsuario').Value := qryValidaLoginnCdUsuario.Value ;
        qryAtuUltAcesso.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans ;
        Raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    nCdUsuarioLogado    := qryValidaLoginnCdUsuario.Value ;
    cNmUsuarioLogado    := qryValidaLogincNmLogin.Value ;
    cFlgUsuarioMaster   := intToStr(qryValidaLogincFlagMaster.Value) ;
    nCdLojaPadrao       := qryValidaLoginnCdLojaPadrao.Value ;
    cFlgUsuarioGerente  := qryValidaLogincFlgGerente.Value ;
    cFlgUsuarioVendedor := qryValidaLogincFlgVendedor.Value ;
    cNmCompletoUsuario  := qryValidaLogincNmUsuario.Value ;
    cEmailUsuario       := qryValidaLogincEmail.Value ;
    cFlgWERP            := qryValidaLogincAcessoWERP.Value ;
    nCdTerceiroUsuario  := qryValidaLoginnCdTerceiroResponsavel.Value ;

    frmLookup_Padrao.cNmUsuarioLogado   := cNmUsuarioLogado;
    frmLookup_Padrao.nCdEmpresaAtiva    := nCdEmpresaAtiva;
    frmLookup_Padrao.nCdUsuarioLogado   := nCdUsuarioLogado;
    frmLookup_Padrao.nCdLojaAtiva       := nCdLojaAtiva;
    frmLookup_Padrao.nCdTerceiroUsuario := nCdTerceiroUsuario;
    
    qryValidaLogin.Close ;

    StatusBar1.Panels[2].Text := cNmUsuarioLogado ;
    //StatusBar1.Panels[5].Text := 'Cliente : ' + Trim(iNet_ntoa( PInAddr( GetHostByName( nil )^.h_addr_list^ )^ )) + ' - ' + GetHostByName( nil )^.h_name;

    cNomeComputador := Uppercase(GetHostByName( nil )^.h_name);

    result := true ;

end ;


function TestaCGC(Dado : string) : boolean;
var  D1            : array[1..12] of byte;
     I,
     DF1,
     DF2,
     DF3,
     DF4,
     DF5,
     DF6,
     Resto1,
     Resto2,
     PrimeiroDigito,
     SegundoDigito : integer;
begin
     Result := true;
     if Length(Dado) = 14 then
     begin
          for I := 1 to 12 do
               if Dado[I] in ['0'..'9'] then
                    D1[I] := StrToInt(Dado[I])
               else
                    Result := false;
          if Result then
          begin
               DF1 := 0;
               DF2 := 0;
               DF3 := 0;
               DF4 := 0;
               DF5 := 0;
               DF6 := 0;
               Resto1 := 0;
               Resto2 := 0;
               PrimeiroDigito := 0;
               SegundoDigito := 0;
               DF1 := 5*D1[1] + 4*D1[2] + 3*D1[3] + 2*D1[4] + 9*D1[5] + 8*D1[6] +
                      7*D1[7] + 6*D1[8] + 5*D1[9] + 4*D1[10] + 3*D1[11] + 2*D1[12];
               DF2 := DF1 div 11;
               DF3 := DF2 * 11;
               Resto1 := DF1 - DF3;
               if (Resto1 = 0) or (Resto1 = 1) then
                    PrimeiroDigito := 0
               else
                    PrimeiroDigito := 11 - Resto1;
               DF4 := 6*D1[1] + 5*D1[2] + 4*D1[3] + 3*D1[4] + 2*D1[5] + 9*D1[6] +
                      8*D1[7] + 7*D1[8] + 6*D1[9] + 5*D1[10] + 4*D1[11] + 3*D1[12] +
                      2*PrimeiroDigito;
               DF5 := DF4 div 11;
               DF6 := DF5 * 11;
               Resto2 := DF4 - DF6;
               if (Resto2 = 0) or (Resto2 = 1) then
                    SegundoDigito := 0
               else
                    SegundoDigito := 11 - Resto2;
               if (PrimeiroDigito <> StrToInt(Dado[13])) or
                  (SegundoDigito <> StrToInt(Dado[14])) then
                    Result := false;
          end;
     end
     else
          if Length(Dado) <> 0 then
               Result := false;
end;

{Valida d�gito verificador de CPF}
function TestaCPF(Dado : string) : boolean;
var  D1            : array[1..9] of byte;
     I,
     DF1,
     DF2,
     DF3,
     DF4,
     DF5,
     DF6,
     Resto1,
     Resto2,
     PrimeiroDigito,
     SegundoDigito : integer;
begin
     Result := true;

     if Length(Dado) = 11 then
     begin

          for I := 1 to 9 do
               if Dado[I] in ['0'..'9'] then
                    D1[I] := StrToInt(Dado[I])
               else
                    Result := false;

          if Result then
          begin
               DF1 := 0;
               DF2 := 0;
               DF3 := 0;
               DF4 := 0;
               DF5 := 0;
               DF6 := 0;
               Resto1 := 0;
               Resto2 := 0;
               PrimeiroDigito := 0;
               SegundoDigito := 0;
               DF1 := 10*D1[1] + 9*D1[2] + 8*D1[3] + 7*D1[4] + 6*D1[5] + 5*D1[6] +
                      4*D1[7] + 3*D1[8] + 2*D1[9];
               DF2 := DF1 div 11;
               DF3 := DF2 * 11;
               Resto1 := DF1 - DF3;

               if (Resto1 = 0) or (Resto1 = 1) then
                    PrimeiroDigito := 0
               else
                    PrimeiroDigito := 11 - Resto1;

               DF4 := 11*D1[1] + 10*D1[2] + 9*D1[3] + 8*D1[4] + 7*D1[5] + 6*D1[6] +
                      5*D1[7] + 4*D1[8] + 3*D1[9] + 2*PrimeiroDigito;
               DF5 := DF4 div 11;
               DF6 := DF5 * 11;
               Resto2 := DF4 - DF6;

               if (Resto2 = 0) or (Resto2 = 1) then
                    SegundoDigito := 0
               else
                    SegundoDigito := 11 - Resto2;

               if (PrimeiroDigito <> StrToInt(Dado[10])) or
                  (SegundoDigito <> StrToInt(Dado[11])) then
                    Result := false;
          end;

     end
     else
          if Length(Dado) <> 0 then
               Result := false;
end;

function TfrmMenu.TestaCpfCgc(Dado: string): String;
var
  i: integer;
begin

  for i:= 1 to length(Dado) do begin
    if not (Dado[i] in ['0'..'9']) then delete(Dado,i,1);
  end;

  if ((length(Dado) <> 11) and (length(Dado) <> 14))then
  begin
     MessageDlg('N�mero do CPF ou CNPJ Inv�lido.', mtWarning, [mbOK], 0);
     exit ;
  end ;

  if length(Dado) = 14 then begin

     if TestaCGC(Dado) then begin
        insert('-',Dado,13);
        insert('/',Dado,9);
        insert('.',Dado,6);
        insert('.',Dado,3);
     end
     else begin
         MessageDlg('N�mero do CNPJ Inv�lido.', mtWarning, [mbOK], 0);
         exit;
     end ;

  end;

  if length(Dado) = 11 then begin

     if TestaCPF(Dado) then begin
        insert('-',Dado,10);
        insert('.',Dado,7);
        insert('.',Dado,4);
     end
     else begin
         MessageDlg('N�mero do CPF Inv�lido.', mtWarning, [mbOK], 0);
         exit ;
     end;
  end;

  Result := Dado;

end;

procedure TfrmMenu.ToolButton1Click(Sender: TObject);
begin
    SelecionaEmpresa();
end;

function TfrmMenu.ConvInteiro(cValor:String) : integer ;
begin

    if (Trim(cValor) = '') then
    begin
        Result := 0 ;
        exit;
    end ;

    try
        Result := StrToInt(Trim(cValor)) ;
        exit ;
    except
        if (Trim(cValor) <> '') then
        begin
            MensagemErro('O valor ' + cValor + ' n�o pode ser convertido para inteiro') ;
            Result := 0 ;
            exit ;
        end ;
    end ;

    Result := 0 ;

end ;

function TfrmMenu.ConvData(cValor:String) : TDateTime ;
begin

    if (Trim(cValor) = '/  /') then
    begin
        Result := StrToDate('01/01/1900') ;
        exit ;
    end ;

    try
        Result := StrToDate(Trim(cValor)) ;
        exit ;
    except
        if (Trim(cValor) <> '/  /') then
        begin
            MensagemErro('O valor ' + cValor + ' n�o pode ser convertido para data') ;
            Result := StrToDate('01/01/1900') ;
            exit ;
        end ;
    end ;

    Result := StrToDate('01/01/1900') ;

end ;

procedure TfrmMenu.tvMenusDblClick(Sender: TObject);
var
    cNmObjeto, cTitulo : String;
begin

    cNmObjeto := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex] ;
    cTitulo   := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex+100] ;

    if (cNmObjeto <> '') then
    begin
        OpenModule(cNmObjeto, cTitulo);

        if PageControlPageDock.ActivePage <> nil then
        begin
            PageControlPageDock.ActivePage.Highlighted := True;
        end;
    end;

end;

procedure TfrmMenu.tvMenusClick(Sender: TObject);
var
    cNmObjeto, cTitulo : String;
begin
end;

procedure TfrmMenu.cxButton2Click(Sender: TObject);
begin

    case MessageDlg('Deseja realmente sair do ER2Soft ?',mtCustom,mbYesNoCancel,0) of
        mrNo: Exit;
        mrCancel: Exit;
    end;
    Application.Terminate ;
    
end;

procedure TfrmMenu.cxButton1Click(Sender: TObject);
begin
    frmMenu.SelecionaEmpresa;
end;

procedure TfrmMenu.NavegaEnter(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin

    key := #0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmMenu.SAir1Click(Sender: TObject);
begin
    case MessageDlg('Deseja realmente sair do ER2Soft ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: Exit;
        mrCancel: Exit;
    end;
    Application.Terminate ;

end;

procedure TfrmMenu.rocarLogon1Click(Sender: TObject);
var
    i : integer ;
begin

        for i := 0 to PageControlPageDock.PageCount - 1 do
        begin
            try
                if (PageControlPageDock.Pages[i].Tag > 0) then
                begin
                    PageControlPageDock.ActivePageIndex := 0 ;
                    PageControlPageDock.Pages[i].TabVisible := False ;
                end ;
            except
            end ;
        end ;

        frmLogin.EdtLogin.Text := '' ;
        frmLogin.edtSenha.Text := '' ;

        frmSelecServer.ShowModal; //Modal ;

        frmLogin.ShowModal ;

        SelecionaEmpresa() ;

        { -- limpa filtro de busca de aplica��es -- }
        edtBuscaApp.Clear;

        PreparaMenu() ;
end;

procedure TfrmMenu.AlterarEmpresa1Click(Sender: TObject);
var
  i : integer ;
begin
        for i := 0 to 100 do
        begin
            try
                if (PageControlPageDock.Pages[i].Tag > 0) then
                begin
                    PageControlPageDock.ActivePageIndex := 0 ;
                    PageControlPageDock.Pages[i].TabVisible := False ;
                end ;
            except
            end ;
        end ;

        SelecionaEmpresa() ;
        PreparaMenu() ;
end;

procedure TfrmMenu.ConfiguraoAmbiente1Click(Sender: TObject);
var
    objForm : TfrmConfigAmbiente;
begin

    objForm := TfrmConfigAmbiente.Create(nil) ;

    showForm( objForm, TRUE ) ;

end;

procedure TfrmMenu.Timer2Timer(Sender: TObject);
begin

    try
        qryTestaConn.Close ;
        qryTestaConn.Open ;
    except

        Connection.Close ;
        Connection.ConnectionString := cConnectionString ;

        try
            Connection.Open ;
        except
            raise ;


        end ;

    end ;

end;

procedure TfrmMenu.FormActivate(Sender: TObject);
begin
  frmMenu.StatusBar1.Panels[5].Text := 'MENU' ;
end;

procedure TfrmMenu.ACBrECF1MsgAguarde(const Mensagem: String);
begin

    StatusBar1.Panels[6].Text := ACBRECF1.MsgAguarde ;

end;


procedure TfrmMenu.ACBRPosPrinter1AguardarPronta( confTEF:boolean );
var
  Status: TACBrPosPrinterStatus;
  ii: TACBrPosTipoStatus;
  AStr: String;
  nTimeOut:integer;

begin
         nTimeOut := 5; // Cinco Tentativas de 1 segundo se erro ou nao pronta
         repeat
         try
          Status := frmMenu.ACBrPosPrinter1.LerStatusImpressora;   // frmMenu.
          if Status = [] then
            break
          else
          begin
          if nTimeOut > 0 then
          begin
            sleep(1000);
            nTimeOut := nTimeOut -1;
          end else begin
           AStr := '';
           For ii := Low(TACBrPosTipoStatus) to High(TACBrPosTipoStatus) do
           begin
            if ii in Status then
             AStr := AStr + GetEnumName(TypeInfo(TACBrPosTipoStatus), integer(ii) )+ ', ';
           end;
            if (confTEF) then
             CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta ERRO LerStatus' );

            case MessageDlg(AStr+' Tentar Novamente?', mtConfirmation,[mbYes,mbNo],0) of
            mrYes : begin
                    if (confTEF) then CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta Impressao: ERRO, Respondeu SIM' );
                    sleep( 500 );
                    end;
            mrNo  : begin
                    if (confTEF) then CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta Impressao: ERRO, Respondeu NAO' );
                    raise Exception.Create('Erro Confirmado pelo Operador');
                    break;
                    end;
            end;
          end;
          end;
         except
          if (confTEF) then
            CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta ERRO' );

          if nTimeOut >0 then
          begin
           sleep( 1000 );
           nTimeOut := nTimeOut -1;
          end else
          begin
           case MessageDlg('Impressora Nao Disponivel, Tentar Novamente?', mtConfirmation,[mbYes,mbNo],0) of
           mrYes : begin
                   if (confTEF) then CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta Impressao: ERRO, Respondeu SIM' );
                   sleep( 500 );
                   end;
           mrNo  : begin
                   if (confTEF) then CapptaLog('fCaixaRecebimento/ACBRPosPrinter1AguardarPronta Impressao: ERRO, Respondeu NAO' );
                   break;
                   end;
           end;
          end;
          Continue;
         end;
         until False;

end;

procedure TfrmMenu.ACBRPosPrinter1FechaImpTermica;
begin
  with (frmMenu) do
  begin
         ACBrPosPrinter1.Desativar;
         ACBrPosPrinter1.Free;
         ACBrPosPrinter1 := TACBrPosPrinter.Create(Application);
  end;
end;


//** This procedure just creates a new Logfile an appends when it was created **
procedure TfrmMenu.CapptaCreateLogfile;
var
  F:TextFile;
FN:String;
begin
  // Getting the filename for the logfile (In this case the Filename is 'application-exename.log'
  FN := ChangeFileExt('CapptaER2', '.log');
  // Assigns Filename to variable F
  AssignFile(F, FN);
  // Rewrites the file F
  Rewrite(F);
  // Open file for appending
  Append(F);
  // Write text to Textfile F
//  WriteLn(F, BreakingLine);
  WriteLn(F, 'Arquivo Log Criado em ' + DateTimeToStr(Now) );
//  WriteLn(F, BreakingLine);
  WriteLn(F, '');
  // finally close the file
  CloseFile(F);
end;

// Procedure for appending a Message to an existing logfile with current Date and Time **
procedure TfrmMenu.CapptaLog(cStr:String);
var
  F:TextFile;
FN:String;
begin
  // Getting the filename for the logfile (In this case the Filename is 'application-exename.log'
  FN := ChangeFileExt('CapptaER2', '.log');

  //Checking for file
  if (not FileExists(FN)) then
  begin
    // if file is not available then create a new file
    CapptaCreateLogFile;
  end;

  // Assigns Filename to variable F
  AssignFile(F, FN);
  // start appending text
  Append(F);
  //Write a new line with current date and message to the file
  WriteLn(F, DateTimeToStr(Now) + ': ' + cStr );
  // Close file
  CloseFile(F)
end;

function TFrmMenu.RetNumeros( s: String ): String ;
var
    i: Integer ;
begin
    Result := '' ;
    for i := 1 to length( s ) do
    begin
        if s[ i ] in ['0'..'9'] then
        Result := Result + s[ i ] ;
    end ;
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
var
  Reg : TRegistry;
begin
  CurrencyString     := 'R$';
  CurrencyFormat     := 0;
  NegCurrFormat      := 14;
  ThousandSeparator  := '.';
  DecimalSeparator   := ',';
  CurrencyDecimals   := 2;
  DateSeparator      := '/';
  ShortDateFormat    := 'dd/MM/yyyy';
  TimeSeparator      := ':';
  TimeAMString       := 'AM';
  TimePMString       := 'PM';
  ShortTimeFormat    := 'hh:mm';
  estadosBrasileiros := '#SP#MG#RJ#RS#SC#PR#ES#DF#MT#MS#GO#TO#BA#SE#AL#PB#PE#MA#RN#CE#PI#PA#AM#AP#AC#RR#RO#EX#';

  { -- **** VERS�O DO SISTEMA **** -- }
  cNmVersaoSistema := 'v3.41';

  Application.OnException := TrataErros ;

  SystemParametersInfo(SPI_SETBEEP, 0, nil, SPIF_SENDWININICHANGE);

  { -- verifica formato data do computador e altera para padr�o brasileiro se necess�rio -- }
  Reg         := TRegistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;
  Reg.OpenKey('Control Panel\International', True) ;

  if Reg.ReadString('sShortDate') <> 'dd/MM/yyyy' then
      Reg.WriteString('sShortDate','dd/MM/yyyy');

  Reg.CloseKey;
  Reg.Free;
end;

procedure TfrmMenu.ComboLojaSelect(Sender: TObject);
begin

    frmMenu.nCdLojaAtiva := StrToInt(Copy(ComboLoja.Text,0,3)) ;
    frmMenu.cNmLojaAtiva := Copy(ComboLoja.Text,7,50) ;
    StatusBar1.Panels[1].Text := frmMenu.cNmLojaAtiva ;

end;

procedure TfrmMenu.ConnectionWillExecute(Connection: TADOConnection;
  var CommandText: WideString; var CursorType: TCursorType;
  var LockType: TADOLockType; var CommandType: TCommandType;
  var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
  const Command: _Command; const Recordset: _Recordset);
begin
  bTransacao    := True;
  Screen.Cursor := crSQLWait;

  StatusBar1.Panels[6].Text := 'Aguarde...';
  StatusBar1.Repaint;
end;

procedure TfrmMenu.ConnectionExecuteComplete(Connection: TADOConnection;
  RecordsAffected: Integer; const Error: Error;
  var EventStatus: TEventStatus; const Command: _Command;
  const Recordset: _Recordset);
begin
  bTransacao    := False;
  Screen.Cursor := crDefault;

  StatusBar1.Panels[6].Text := ' ';

  {myThread.myForm.Free;
  //myThread.Suspend;
  myThread.Terminate;}
end;

procedure TfrmMenu.Timer3Timer(Sender: TObject);
begin

    if not frmAlertas.qryAlertas.Active then
    begin

        frmAlertas.qryAlertas.Close ;
        frmAlertas.qryAlertas.Parameters.ParamByName('nPK').Value := frmMenu.nCdUsuarioLogado ;
        frmAlertas.qryAlertas.Open ;

        if not frmAlertas.qryAlertas.Eof then
        begin
            Timer3.Enabled := False ;

            frmAlertas.ShowModal;

        end ;

        frmAlertas.qryAlertas.Close ;

        Timer3.Enabled := True ;
    end ;

end;

procedure TfrmMenu.btAlertaClick(Sender: TObject);
begin

    if not frmAlertas.qryAlertas.Active then
    begin

        frmAlertas.qryAlertas.Close ;
        frmAlertas.qryAlertas.Parameters.ParamByName('nPK').Value := frmMenu.nCdUsuarioLogado ;
        frmAlertas.qryAlertas.Open ;

        if not frmAlertas.qryAlertas.Eof then
        begin
            Timer3.Enabled := False ;

            frmAlertas.ShowModal;

        end ;

        frmAlertas.qryAlertas.Close ;

        Timer3.Enabled := True ;
    end ;

end;

procedure TfrmMenu.btTrocarLoginClick(Sender: TObject);
var
   i : integer ;
begin

        for i := 0 to 100 do
        begin
            try
                if (PageControlPageDock.Pages[i].Tag > 0) then
                begin
                    PageControlPageDock.ActivePageIndex := 0 ;
                    PageControlPageDock.Pages[i].TabVisible := False ;
                end ;
            except
            end ;
        end ;

        frmSelecServer.ShowModal; //Modal ;

        frmLogin.EdtLogin.Text := '' ;
        frmLogin.edtSenha.Text := '' ;
        frmLogin.ShowModal ;

        SelecionaEmpresa() ;

        { -- limpa filtro de busca de aplica��es -- }
        edtBuscaApp.Clear;

        PreparaMenu() ;

end;

procedure TfrmMenu.StatusBar1Click(Sender: TObject);
var
    i : integer ;
begin

    if (cNmLojaAtiva <> '') then
    begin

        if (ComboLoja.Items.Count > 1) then
        begin

            for i := 0 to 100 do
            begin
                try
                    if (PageControlPageDock.Pages[i].Tag > 0) then
                    begin
                        PageControlPageDock.ActivePageIndex := 0 ;
                        PageControlPageDock.Pages[i].TabVisible := False ;
                    end ;
                except
                end ;
            end ;

            ComboLoja.Visible := True ;
            ComboLoja.SetFocus;
            
        end ;

    end ;

end;

procedure TfrmMenu.ComboLojaExit(Sender: TObject);
begin

  ComboLoja.Visible := False ;

  { -- limpa filtro de busca de aplica��es -- }
  edtBuscaApp.Clear;

end;

procedure TfrmMenu.ConnectionAfterConnect(Sender: TObject);
var
    i : integer ;
begin

{    for i := 87 to Connection.Properties.Count do
    begin
        ShowMessage(IntToStr(i) + ' ' + Connection.Properties.Item[i].Name + ' ' + String(Connection.Properties.Item[i].Value)) ;
    end ;}

end;

procedure TfrmMenu.tvMenusKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    cNmObjeto, cTitulo : String;
begin

    if (Key = 13) then
    begin
    
        cNmObjeto := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex] ;
        cTitulo   := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex+100] ;

        if (cNmObjeto <> '') then
        begin
            OpenModule(cNmObjeto, cTitulo);

            if PageControlPageDock.ActivePage <> nil then
            begin
                PageControlPageDock.ActivePage.Highlighted := True;
            end;
        end;

    end ;

end;

function TfrmMenu.MascaraDecimal: String;
var
    cMascara : string ;
    i : integer ;
begin

    cMascara := '#,##0.' ;

    if iCasaDecimal < 2 then
        iCasaDecimal := 2 ;

    for i := 1 to iCasaDecimal do
    begin
        cMascara := cMascara + '0' ;
    end ;

    Result := cMascara ;
end;

procedure TFrmMenu.MensagemPadrao(cMensagem : String ; iTipo : integer ; iSleep : integer) ;
begin

  if (iSleep >0) then
      iSleep := 5 ;
      
  TfrmTimedMessage.ShowMessage(cMensagem, iSleep,'ER2Soft') ;

  {with TfrmMensagem.Create(Application) do
  begin
    Memo1.Lines.Clear ;
    Memo1.Lines.Add(UpperCase(cMensagem)) ;

    btnSim.Visible    := False ;
    btnNao.Visible    := False ;
    btnOK.Visible     := False ;
    pnlBotoes.Visible := False ;

    if (iTipo = 1) then
    begin
        pnlBotoes.Visible := True;
        btnOK.Visible     := True ;
    end ;

    if (iTipo = 2) then
    begin
        pnlBotoes.Visible := True;
        btnSim.Visible    := True ;
        btnNao.Visible    := True ;
    end ;

    _iTipo  := iTipo ;
    _iSleep := iSleep ;

    Refresh ;
    ShowModal ;

    Free;
  end;     }

end ;

function TfrmMenu.ZeroEsquerda(Texto: String; Qtde: Integer): String;
var i, Tam: integer;
        Aux: String;
begin

   Aux := '';
   Tam := Length(Texto);

   for i := Tam to (Qtde - 1) do
         Aux := Aux + '0';

   ZeroEsquerda := Aux + Texto;

end;

procedure TfrmMenu.AdicionaremFavoritos1Click(Sender: TObject);
var
    cNmObjeto, cTitulo, cAplicacao : String;
begin

    cNmObjeto := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex] ;
    cTitulo   := arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex+100] ;
    cAplicacao:= arrObjetos[tvMenus.Selected.TreeView.Selected.AbsoluteIndex,tvMenus.Selected.TreeView.Selected.AbsoluteIndex+500] ;

    if (cAplicacao <> '') then
    begin
        ShowMessage(cAplicacao) ;
    end;

end;

procedure TfrmMenu.PopupMenu1Popup(Sender: TObject);
begin

    if tvMenus.Selected.Level <> 999 then
        abort ;

end;

procedure TfrmMenu.ZeraTemp(cNmTabela: string);
begin

    try
        cmdZeraTemp.CommandText := 'IF OBJECT_ID(' + Chr(39) + 'tempdb..' + cNmTabela + Chr(39) + ') IS NOT NULL TRUNCATE TABLE ' + cNmTabela ;
        cmdZeraTemp.Execute;
    except;
    end ;

end;

procedure TfrmMenu.GeraLog(nCdAplicacao: integer);
begin

   if (cFlgLogAcesso <> 'S') then
       exit ;

   SP_LOG_ACESSO.Close ;
   SP_LOG_ACESSO.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
   SP_LOG_ACESSO.Parameters.ParamByName('@nCdAplicacao').Value  := nCdAplicacao;
   SP_LOG_ACESSO.Parameters.ParamByName('@cNmComputador').Value := cNomeComputador;
   SP_LOG_ACESSO.ExecProc;

end;

procedure TfrmMenu.ImprimeDBGrid(DBGridEh1: TDBGridEh; cTitulo: string);
begin

    PrintDBGridEh.PageFooter.LeftText.Text  := 'Emitido por ' + frmMenu.cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + ' ' + cNomeComputador;
    PrintDBGridEh.PageFooter.RightText.Text := 'www.er2soft.com.br' ;
    PrintDBGridEh.PageFooter.Font.Name      := 'Consolas' ;
    PrintDBGridEh.DBGridEh                  := DBGridEh1 ;
    PrintDBGridEh.Title.Text                := cTitulo ;
    PrintDBGridEh.PrintFontName             := 'Consolas' ;
    PrintDBGridEh.Options                   := [pghFitGridToPageWidth];
    PrintDBGridEh.Preview;

end;

function TfrmMenu.validaUnidadeMedida(cSigla: string): Boolean;
begin

    qryUnidadeMedida.Close;
    qryUnidadeMedida.Parameters.ParamByName('nPK').Value := cSigla;
    qryUnidadeMedida.Open ;
    
    result := (not qryUnidadeMedida.Eof) ;

end;

procedure TfrmMenu.configuraAmbiente(cNmComputador: string);
begin

    qryConfigAmbiente.Close;
    qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := cNmComputador ;
    qryConfigAmbiente.Open;

    if qryConfigAmbiente.eof then
    begin

        qryConfigAmbiente.Insert;
        qryConfigAmbientecNmComputador.Value   := cNmComputador ;
        qryConfigAmbientecPortaMatricial.Value := 'LPT1' ;
        qryConfigAmbientecPortaEtiqueta.Value  := 'LPT1' ;
        qryConfigAmbiente.Post;

    end ;

    qryConfigAmbiente.Edit;
    qryConfigAmbientedDtUltAcesso.Value        := Now() ;
    qryConfigAmbientecNmUsuarioUltAcesso.Value := cNmUsuarioLogado ;
    qryConfigAmbiente.Post;

    if (qryConfigAmbientecFlgUsaMapeamentoAutPDV.Value = 1) then
        fnMapeamentoImpressora(qryConfigAmbientecPortaMatricial.Value,qryConfigAmbientecNmComputador.Value,qryConfigAmbientecNmImpCompartilhadaPDV.Value);

    if (qryConfigAmbientecFlgUsaMapeamentoAutEtiqueta .Value = 1) then
        fnMapeamentoImpressora(qryConfigAmbientecPortaEtiqueta.Value,qryConfigAmbientecNmComputador.Value,qryConfigAmbientecNmImpCompartilhadaEtiqueta.Value);
        
end;

procedure TfrmMenu.mensagemUsuario(cMsg: string);
begin
    StatusBar1.Panels[6].Text := cMsg ;
    StatusBar1.Repaint;
end;

procedure TfrmMenu.limpaBufferTeclado;
var
   Msg: TMsg;
begin
   while PeekMessage(Msg, 0, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE or PM_NOYIELD) do;
end;

procedure TfrmMenu.limpaBufferMouse;
var
   Msg: TMsg;
begin
   while PeekMessage(Msg, 0, WM_MOUSEFIRST, WM_MOUSELAST, PM_REMOVE or PM_NOYIELD) do;
end;

function TfrmMenu.arredondaVarejo(nValor: Extended): Extended;
var
    iUltimaCasa : Double ;
begin

    nValor := tbRound(nvalor,2) ;

    iUltimaCasa := (Frac(nValor) * 100) ; // pega das casas decimais do valor
    iUltimaCasa := iUltimaCasa / 10 ; // divide o valor decimal por dez
    iUltimaCasa := Frac(iUltimaCasa)*10 ; // pega as casas decimais da divis�o, para pegar a ultima casa decimal do valor

    if (iUltimaCasa < 3) then
    begin
        nValor := nValor - (iUltimaCasa/100) ;
    end ;

    if (iUltimaCasa >= 3) and (iUltimaCasa < 5) then
    begin
        nValor := nValor - (iUltimaCasa/100) ;
        nValor := nValor + 0.05 ;
    end ;

    if (iUltimaCasa > 5) then
    begin
        nValor := nValor - (iUltimaCasa/100) ;
        nValor := nValor + 0.1 ;
    end ;

    Result := nValor ;

end;

procedure TfrmMenu.validaDataServidor;
begin

    qryDataServidor.Close;
    qryDataServidor.Open;

    if not qryDataServidor.eof then
        if (qryDataServidordDataAtual.Value <> Date) then
        begin
            MensagemErro('A data do sistema operacional desta esta��o de trabalho � diferente da data do servidor de banco de dados do ER2Soft.'+#13#13+'Data do Servidor: ' + qryDataServidordDataAtual.AsString) ;
            Application.Terminate;
        end ;

end;

function TfrmMenu.fnProximoCodigo(cNmTabela: string): integer;
begin

    try
        SP_PROXIMO_CODIGO.Close;
        SP_PROXIMO_CODIGO.Parameters.ParamByName('@cNmTabela').Value := cNmTabela ;
        SP_PROXIMO_CODIGO.ExecProc;
    except
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    Result := SP_PROXIMO_CODIGO.Parameters.ParamByName('@iUltimoCodigo').Value ;

end;

procedure TfrmMenu.ACBrECF1MsgPoucoPapel(Sender: TObject);
var
  iid : integer ;
begin

    iid := 0;

end;

function TfrmMenu.StringToCaseSelect(Selector: string;
  CaseList: array of string): Integer;
var
    cnt: integer;
begin

    Result:=-1;

    for cnt:=0 to Length(CaseList)-1 do
    begin

        if CompareText(Selector, CaseList[cnt]) = 0 then
        begin
            Result := cnt;
            Break;
        end;

    end;

end;

procedure TfrmMenu.showForm(objForm: TForm; bDestroirObjeto : boolean);
begin


    try
        try
            objForm.ShowModal ;
        except
            MensagemErro('Erro na cria��o da tela. Classe:  ' + objForm.ClassName) ;
            raise ;
        end ;

    finally

        if (bDestroirObjeto) then
            freeAndNil(objForm) ;
            
    end;

end;

function TfrmMenu.fnFatorConversaoUM(cUnidadeMedidaDe,
  cUnidadeMedidaPara: string): double;
begin

    if ( Uppercase( Trim( cUnidadeMedidaDe ) ) = Uppercase( Trim( cUnidadeMedidaPara ) ) ) then
    begin
        Result := 1 ;
        exit ;
    end ;

    qryConvUM.Close;
    qryConvUM.Parameters.ParamByName('cUMDe').Value   := Uppercase( Trim( cUnidadeMedidaDe ) ) ;
    qryConvUM.Parameters.ParamByName('cUMPara').Value := Uppercase( Trim( cUnidadeMedidaPara ) ) ;
    qryConvUM.Open ;

    if qryConvUM.eof then
    begin
        Result := 1 ;
        MensagemErro('Fator de Convers�o da Unidade de Medida ' + Uppercase(cUnidadeMedidaDe) + ' para ' + Uppercase(cUnidadeMedidaPara) + ' n�o encontrado.') ;
        abort ;
    end
    else Result := qryConvUMnFatorConversaoUM.Value ;

end;

function TfrmMenu.fnConverteQtdeUnidadeMedida(cUnidadeMedidaDe,
  cUnidadeMedidaPara: string; nQtdeAConverter: double): double;
begin

    Result := fnFatorConversaoUM( cUnidadeMedidaDe , cUnidadeMedidaPara ) * nQtdeAConverter ;

end;

function TfrmMenu.fnValidaHoraDecimal(nValorHora: double): boolean;
begin

    Result := (Round((Frac( nValorHora ) * 100)) <= 59)

end;

procedure TfrmMenu.ER2Mensagem(Mensagem, Tipo: String; Delay: integer);
var
    lblMsg  : TLabel;
    nLinhas : integer;
    memMsg  : TMemo;
begin

    FreeAndNil(frmMsg);
    frmMsg := TForm.Create(nil);
    lblMsg := TLabel.Create(nil);

    if (UpperCase(Tipo) = 'ERRO') then
    begin
        frmMsg.Color       := RGB(246,56,56);
        frmMsg.Caption     := 'ER2Soft - Erro';
        lblMsg.Font.Color  := clWhite;
    end
    else if (UpperCase(Tipo) = 'ALERTA') then
    begin
        frmMsg.Color       := RGB(230,255,144);
        frmMsg.Caption     := 'ER2Soft - Alerta';
        lblMsg.Font.Color  := clBlack;
    end
    else
    if (UpperCase(Tipo) = 'INFORMACAO') then
    begin
        frmMsg.Color       := clWhite;
        frmMsg.Caption     := 'ER2Soft - Informa��o';
        lblMsg.Font.Color  := clBlue;
    end;

    waitTime           := 0;
    DelayMsg           := Delay;

    frmMsg.BorderStyle := bsToolWindow;
    frmMsg.Parent      := PageControlPageDock.ActivePage;
    frmMsg.BorderWidth := 0;
    frmMsg.Width       := 750;

    {-- Cria componente TMemo temporario para controlar a quantidade de linhas necessarias para a mensagem. --}
    memMsg := TMemo.Create(nil);
    memMsg.Font.Name  := 'Verdana';
    memMsg.Font.Size  := 8;
    memMsg.Font.Style := [fsBold];
    memMsg.Parent  := frmMsg;
    memMsg.Align   := alClient;
    memMsg.Visible := True;
    memMsg.Lines.Append(Mensagem);
    nLinhas := memMsg.Lines.Count;

    FreeAndNil(memMsg);

    if (nLinhas > 1) then
        frmMsg.Height := (nLinhas * 11) + 57
    else frmMsg.Height := 57 ;

    frmMsg.Top         := PageControlPageDock.ActivePage.Height;
    frmMsg.Left        := 0;

    //Desabilita a fun��o de mover a janela
    DeleteMenu(GetSystemMenu(frmMsg.Handle, false), SC_MOVE, MF_BYCOMMAND);

    lblMsg.Parent     := frmMsg;
    lblMsg.Align      := alClient;
    lblMsg.WordWrap   := True;
    lblMsg.Font.Name  := 'Verdana';
    lblMsg.Font.Size  := 8;
    lblMsg.Font.Style := [fsBold];
    lblMsg.Caption    := Mensagem;

    frmMsg.Show;

    tMensagens.Enabled := true;
end;

procedure TfrmMenu.nMensagemAlerta(Mensagem: String);
begin
    ER2Mensagem(Mensagem,'ALERTA');
end;

procedure TfrmMenu.nMensagemErro(Mensagem: String);
begin
    ER2Mensagem(Mensagem,'ERRO');
end;

procedure TfrmMenu.nShowMessage(Mensagem: String);
begin
    ER2Mensagem(Mensagem,'INFORMACAO');
end;

procedure TfrmMenu.tMensagensTimer(Sender: TObject);
begin
    if ((frmMsg.Top > (frmMsg.Parent.Height - frmMsg.Height)) and (waitTime = 0)) then
    begin
        tMensagens.Interval := 5;
        frmMsg.Top := frmMsg.Top - 5;
    end
    else if ((waitTime < DelayMsg) and (frmMsg.Top <= (frmMsg.Parent.Height - frmMsg.Height))) then
    begin
        tMensagens.Interval := 1000;
        waitTime := waitTime + 1;
    end
    else if (waitTime = DelayMsg) then
    begin
        tMensagens.Interval := 5;
        frmMsg.Top := frmMsg.Top + 5;
    end else
    begin
        tMensagens.Enabled  := False;
        FreeAndNil(frmMsg);
    end;
end;

procedure TfrmMenu.SendMail(cPara, cAssunto, cMensagem, cNmRemetente,
  cCaminhoAnexo: String);
begin
    Connection.BeginTrans;

    try
        SP_SEND_MAIL.Close;
        SP_SEND_MAIL.Parameters.ParamByName('@cFrom').Value         := cNmRemetente;
        SP_SEND_MAIL.Parameters.ParamByName('@cTo').Value           := cPara;
        SP_SEND_MAIL.Parameters.ParamByName('@cAssunto').Value      := cAssunto;
        SP_SEND_MAIL.Parameters.ParamByName('@cMensagem').Value     := cMensagem;
        SP_SEND_MAIL.Parameters.ParamByName('@cCaminhoAnexo').Value := cCaminhoAnexo;
        SP_SEND_MAIL.ExecProc;
    except
        Connection.RollbackTrans;
        MensagemErro('Erro no Processamento');
        raise;
    end;

    Connection.CommitTrans;
end;

function TfrmMenu.fnValidaUsuarioAPL(cNmObjeto: string): boolean;
begin

    qryUsuarioAplicacao.Close;
    qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := self.nCdUsuarioLogado ;
    qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := Uppercase( cNmObjeto ) ;
    qryUsuarioAplicacao.Open;

    result := (not qryUsuarioAplicacao.eof) ;
    
end;

function TfrmMenu.fnValidaMascaraContabil(cMascara: string): boolean;
var
    i    : integer ;
    bAux : boolean ;
begin

    if ( Length( Trim(cMascara) ) = 0) then
    begin
        result := false ;
        exit ;
    end ;

    {-- se o primeiro caracter da mascara n�o for #, devolve como false --}
    if (Copy(cMascara,1,1) <> '#') then
    begin
        result := false ;
        exit ;
    end ;

    {-- se o ultimo caracter da mascara n�o for #, devolve como false --}
    if (Copy(cMascara,Length(cMascara),1) <> '#') then
    begin
        result := false;
        exit ;
    end ;

    bAux := false ;

    for i := 0 to Length(cMascara) do
    begin

        if (Copy(cMascara,i,1) = '.') and (bAux) then
        begin
            result := false ;
            exit ;
        end ;

        bAux := (Copy(cMascara,i,1) = '.') ;

        if ( (Copy(cMascara,i,1) <> '.') and (Copy(cMascara,i,1) <> '#') ) then
        begin
            result := false ;
            exit ;
        end ;

    end ;

    result := true ;

end;

function TfrmMenu.fnAplicaMascaraContabil(cMascara,
  cFormatacao: string): string;
var
  tMaskEditTemp     : TMaskEdit ;
  cFormatacaoReal   : string ;
  iTamanhoFormatado : integer ;
  i                 : integer ;
begin

  iTamanhoFormatado := 0 ;

  for i := 1 to length( cFormatacao ) do
  begin

      if ( Copy( cFormatacao , i , 1) <> '.' ) then
          inc( iTamanhoFormatado ) ;

      if ( iTamanhoFormatado > length( trim ( cMascara ) ) ) then
          break ;

      cFormatacaoReal := cFormatacaoReal + Copy( cFormatacao , i , 1) ;

  end ;

  {-- se o ultimo caracter da mascara for ponto, remove --}
  if ( Copy( cFormatacaoReal , length( cFormatacaoReal ) , 1 ) = '.' ) then
      cFormatacaoReal := Copy( cFormatacaoReal , 1 , ( length( cFormatacaoReal ) - 1 ) ) ;
      
  tMaskEditTemp          := TMaskEdit.Create( Self );
  tMaskEditTemp.EditMask := cFormatacaoReal + ';0; ' ;
  tMaskEditTemp.Text     := cMascara ;
  tMaskEditTemp.Update;

  Result := tMaskEditTemp.EditText ;

  freeAndNil( tMaskEditTemp ) ;

end;

procedure TfrmMenu.prAbreGaveta;
begin
  qryConfigAmbiente.Close;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := cNomeComputador;
  qryConfigAmbiente.Open;

  if (qryConfigAmbientecFlgUsaGaveta.Value = 1) then
      ACBrGAV1.AbreGaveta;
end;

procedure TfrmMenu.edtBuscaAppPropertiesChange(Sender: TObject);
begin
  { --  gera menu -- }
  if (edtBuscaApp.Text = '') then
      PreparaMenu;
end;

procedure TfrmMenu.btBuscaAppClick(Sender: TObject);
begin
  { -- filtra menu de acordo com o filtro digitado pelo usu�rio -- }
  if (Trim(edtBuscaApp.Text) <> '') then
      PreparaMenu;
end;

procedure TfrmMenu.edtBuscaAppKeyPress(Sender: TObject; var Key: Char);
begin
  { -- dispara evento do bot�o de busca -- }
  if (Key = #13) then
      btBuscaApp.Click;
end;

function TfrmMenu.fnValidaPeriodo(dDtInicial, dDtFinal : TDate) : Boolean;
begin
  if (dDtInicial > dDtFinal) then
  begin
      MensagemAlerta('Per�odo inicial maior do que o per�odo final.');
      Result := False;
  end
  else
      Result := True;
end;

function TfrmMenu.ArredondaDecimal(Value: extended;
  Decimals: integer): extended;
var
    Factor,Fraction : extended ;
    i, Rest         : integer;
begin
    Factor   := IntPower(10,Decimals);
    Value    := StrToFloat(FloatToStr(Value * Factor));
    Result   := Int(Value);
    Fraction := Frac(Value);

    for i := 3 downto 1 do
    Begin
        Fraction := Fraction * intPower(10,i);

        if (frac(Fraction) > 0.5) then
            Fraction :=Fraction + 1 - Frac(Fraction)
        else if (Frac(Fraction) < 0.5) then
            Fraction := Fraction - Frac(Fraction);

        Fraction := Fraction / intPower(10,i);
    end;

    if ((((Result/2) - Int(Result/2) > 0) OR ( Fraction > 0.5))) then
    BEGIN
        IF (Fraction >= 0.5) then
            Result := Result +1
        Else
            if (Fraction <= -0.5) then
                Result :=Result - 1;
    END;

    Result := Result/Factor;
end;

procedure TfrmMenu.btFeedbackClick(Sender: TObject);
var
  objFeedback : TfrmFeedback;
begin
  objFeedback := TfrmFeedback.Create(Nil);

  showForm(objFeedback, True);
end;

procedure TfrmMenu.btHistVersaoClick(Sender: TObject);
var
  objHistoricoVersoes : TfrmHistoricoVersoes;
begin
  objHistoricoVersoes := TfrmHistoricoVersoes.Create(Nil);

  showForm(objHistoricoVersoes, True);
end;

procedure TfrmMenu.btGuiaOperacionalClick(Sender: TObject);
var
  objGuiaOper : TfrmGuiaOperacional;
begin
  objGuiaOper := TfrmGuiaOperacional.Create(Nil);

  showForm(objGuiaOper, True);
end;

procedure TfrmMenu.btSuporteIntClick(Sender: TObject);
begin
  if (FileExists('.\ER2TVCli.exe')) then
  begin
      mensagemUsuario('Iniciando aplicativo...');
      ShellExecute(handle, 'open', PChar('ER2TVCli.exe'), '', '', SW_SHOWNORMAL);

      Sleep(3000);
      mensagemUsuario('');
  end
  else
  begin
      fnDownloadFTP('.\ER2TVCli.exe', 'ER2TVCli.exe', True);
  end;
end;

procedure TfrmMenu.btSobreClick(Sender: TObject);
var
  objSobre : TfrmSobre;
begin
  objSobre := TfrmSobre.Create(Nil);

  showForm(objSobre, True);
end;

function TfrmMenu.fnValidarEMail(strEmail: String): Boolean;
begin
  strEmail := Trim(UpperCase(strEmail));

  if (Pos('@', strEmail) > 1) then
  begin
      Delete(strEmail, 1, Pos('@', strEmail));

      if (Length(strEmail) > 0) and (Pos('.', strEmail) > 1) then
          Result := True
      else
      begin
          MensagemAlerta('E-mail informado � inv�lido.' + #13#13 + 'Ex.: exemplo@er2soft.com.br');
          Result := False;
      end;
  end
  else
  begin
      MensagemAlerta('E-mail informado � inv�lido.' + #13#13 + 'Ex.: exemplo@er2soft.com.br');
      Result := False;
  end;
end;

procedure TfrmMenu.ExportExcelDinamic(cTitulo, cFiltro,cFiltroAux: String; qryAux: TADOQuery);
var
  linha, coluna,LCID : integer;
  planilha           : variant;
  valorcampo         : string;
  letra,LetraAux     : string;
begin
    {--Passar os par�metros conforme mencionado a cima, Titulo, Filtros e a Query.
       Na Query renomear os nomes das colunas e colocar em ordem conforme deseja que saia.
       N�meros e datas ser�o tratados. --}

    try
        mensagemUsuario('Exportando planilha...');

        try
            planilha:= CreateoleObject('Excel.Application');
        except
            MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
            exit ;
        end ;

        try

            LCID := GetUserDefaultLCID;
            planilha.DisplayAlerts  := False;
            planilha.WorkBooks.add(1);
            planilha.ScreenUpdating := true;

            planilha.caption := cTitulo;
                         //L C
            planilha.cells[1,1] := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
            planilha.cells[2,1] := cTitulo;
            planilha.cells[4,1] := cFiltro;
            planilha.cells[5,1] := cFiltroAux;

            //Descobre a ultima coluna
            letra := chr(64+qryAux.FieldCount) + IntToStr(6);

            //Arruma as colunas
            planilha.Range['A1',letra].font.italic := false;
            planilha.Range['A1',letra].font.bold := True;

            //Pinta as colunas
            planilha.Range['A1',letra].Interior.Color :=  $A9A9A9;

            for coluna := 1 to qryAux.FieldCount do
            begin
                LetraAux := '';
                LetraAux := chr(64+coluna) + IntToStr(6);
                planilha.cells[6,coluna] := qryAux.Fields[coluna - 1].DisplayName;

                if (qryAux.Fields[coluna -1].DisplayWidth < Length(qryAux.Fields[coluna - 1].DisplayName)) then
                begin
                    planilha.Range[LetraAux].ColumnWidth := Length(qryAux.Fields[coluna -1].DisplayName);
                end
                else
                begin
                    if qryAux.Fields[coluna -1].DisplayWidth > 50 then
                        planilha.Range[LetraAux].ColumnWidth := 50
                    else
                        planilha.Range[LetraAux].ColumnWidth := qryAux.Fields[coluna -1].DisplayWidth;
                end;
            end;

            for linha := 0 to qryAux.RecordCount - 1 do
            begin

               for coluna := 1 to qryAux.FieldCount do
               begin

                   planilha.cells[linha + 7,coluna] := qryAux.Fields[coluna - 1].AsString;

                   if qryAux.Fields[coluna - 1].DataType = ftBCD then
                   begin
                       planilha.cells[linha + 7,coluna].NumberFormat := '0,00' ;
                   end;

                   if qryAux.Fields[coluna - 1].DataType = ftDateTime then
                   begin
                        planilha.Cells[Linha+7,coluna].NumberFormat        := 'dd/MM/yyyy' ;
                        planilha.Cells[Linha+7,Coluna].HorizontalAlignment := xlLeft ;
                   end;

               end;

               qryAux.Next;

            end;

            LetraAux := chr(64+coluna);
            planilha.Range['A1',LetraAux + IntToStr(qryAux.RecordCount + 6)].HorizontalAlignment := 2;
            planilha.Range['A1',LetraAux + IntToStr(qryAux.RecordCount + 6)].Font.Name := 'Calibri';
            planilha.Range['A1',LetraAux + IntToStr(qryAux.RecordCount + 6)].Font.Size := 9;
            planilha.Visible := True;

      except
          MensagemErro('Erro no processamento.');
          raise;
      end;
  finally
      mensagemUsuario('');
  end;
end;

procedure TfrmMenu.IdFTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  Screen.Cursor := crHourGlass;
end;

procedure TfrmMenu.IdFTP1WorkEnd(Sender: TObject; AWorkMode: TWorkMode);
begin
  Screen.Cursor := crDefault;
end;

procedure TfrmMenu.IdFTP1Work(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  mensagemUsuario('Efetuando download ' + IntToStr(AWorkCount) + ' bytes...');
  Application.ProcessMessages;
end;

procedure TfrmMenu.btER2UpdateClick(Sender: TObject);
var
  ER2VersaoIni        : TIniFile;
  cNmUltVersaoSistema : String;
  dDtUltVersaoSistema : TDate;
begin
  qryVerificaUpdate.Close;
  qryVerificaUpdate.Parameters.ParamByName('nPK').Value := cNomeComputador;
  qryVerificaUpdate.Open;

  if (not qryVerificaUpdate.IsEmpty) then
  begin
      if (qryVerificaUpdatecFlgUpdateER2.Value = 1) then
      begin
          case MessageDlg('Existe uma nova vers�o do ER2Soft dispon�vel para download, para realizar a atualiza��o o sistema ser� encerrado.' + #13#13 + 'Deseja atualizar agora ?', mtConfirmation,[mbYes,mbNo],0) of
              mrNo : Exit;
          end;

          fnDownloadFTP('.\ER2Instal.exe', 'ER2Instal.exe', False);

          if (not FileExists('.\ER2Instal.exe')) then
          begin
              MensagemErro('Execut�vel de atualiza��o n�o encontrado, contate o administrador do sistema.');
              Application.Terminate;
          end;

          prGeraArqConfigIni;

          PostMessage(FindWindow('TfrmEr2Instal', nil), WM_CLOSE, 0, 0);

          WinExec(PAnsiChar('ER2Instal.exe ' + cIPServidor + ' ' + cInstancia + ' ' + qryVersaoSistemacNmVersaoSistema.Value + ' 1 ' + cNomeComputador),SHOW_OPENWINDOW);

          Application.Terminate;

          Exit;
      end;
  end;

  try
      fnDownloadFTP('.\ER2Versao.ini', 'ER2Versao.ini', False);
  except
      MensagemErro('Falha ao verificar novas atualiza��es, tente novamente mais tarde.');
      Exit;
  end;

  ER2VersaoIni := TIniFile.Create('.\ER2Versao.ini');

  if (not FileExists('.\ER2Versao.ini')) then
  begin
      MensagemErro('Falha ao verificar novas atualiza��es, tente novamente mais tarde.');
      Exit;
  end;

  cNmUltVersaoSistema := ER2VersaoIni.ReadString('UPDATE', 'Versao', '');
  dDtUltVersaoSistema := StrToDate(ER2VersaoIni.ReadString('UPDATE', 'DataUpdate', ''));

  if (cNmUltVersaoSistema <> cNmVersaoSistema) then
      ShowMessage('Existe uma nova vers�o dispon�vel para atualiza��o.' + #13 + 'Vers�o: ' + cNmUltVersaoSistema + #13 + 'Disp. em: ' + DateTimeToStr(dDtUltVersaoSistema) + #13#13 + 'Contate o administrador do sistema e agende sua atualiza��o.')
  else
      ShowMessage('Voc� j� possui a �ltima vers�o do ER2Soft instalada.');
end;

procedure TfrmMenu.fnDownloadFTP(cCaminhoApp, cNomeApp : String; bIniciarApp : Boolean);
begin
  try
      mensagemUsuario('Testando conex�o com a internet...');

      IdHTTP1.Get('http://er2soft.com.br');
  except on E : Exception do
  begin
      MensagemErro('Erro ao estabelecer conex�o com a internet: ' + E.Message);
      Abort;
  end;end;

  try
      mensagemUsuario('Testando comunica��o com o FTP...');

      IdFTP1.Disconnect();

      IdFTP1.Host           := 'ftp.er2soft.com.br';
      IdFTP1.Port           := 21;
      IdFTP1.Username       := 'suporte@er2soft.com.br';;
      IdFTP1.Password       := 'admsoft101580';
      IdFTP1.Passive        := false;
      IdFTP1.RecvBufferSize := 8192;

      IdFTP1.Connect();
  except on E : Exception do
  begin
      MensagemErro('Erro ao estabelecer comunica��o com o FTP: ' + E.Message);
      Abort;
  end;end;

  try
      mensagemUsuario('Efetuando download...');

      IdFTP1.Get(cNomeApp, cCaminhoApp, True, False);

      if (bIniciarApp) then
      begin
          mensagemUsuario('Iniciando aplicativo...');
          ShellExecute(handle, 'open', PChar(cNomeApp), '', '', SW_SHOWNORMAL);
      end;
  finally
      IdFTP1.Disconnect;
      IdHTTP1.Disconnect;

      Sleep(3000);
      mensagemUsuario('');
  end;
end;

procedure TfrmMenu.prGeraArqConfigIni();
var
  ArqConfigIni : TIniFile;
begin
  try
      ArqConfigIni := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Config.ini');

      ArqConfigIni.WriteString('SERVIDOR','Servername',cIPServidor);
      ArqConfigIni.WriteString('SERVIDOR','Database',cInstancia);
      ArqConfigIni.WriteString('UPDATE','VersaoUpdate',qryVersaoSistemacNmVersaoSistema.Value);
      ArqConfigIni.WriteString('UPDATE','DataUpdate',DateTimeToStr(qryVersaoSistemadDtInicioVersao.Value));
      ArqConfigIni.WriteString('UPDATE','UpdateER2',qryConfigAmbientecFlgUpdateER2.AsString);
  finally
      ArqConfigIni.Free;
  end;
end;

procedure TfrmMenu.prExecutaAplicativo(cCaminho: String);
var
  StartupInfo        : TStartupInfo;
  ProcessInformation : TProcessInformation;
  bValida            : Boolean;
begin
  try
      mensagemUsuario('Iniciando app externo...');

      try
          FillChar(StartupInfo, SizeOf(StartupInfo), #0);

          with StartupInfo do
              cb := SizeOf(StartupInfo);

          // Cria e Executa o aplicativo
          bValida := CreateProcess(nil, PChar(cCaminho), nil, nil, false, NORMAL_PRIORITY_CLASS, nil, nil, StartupInfo, ProcessInformation);

          // Se o aplicativo foi iniciado corretamente
          if (bValida) then
              WaitForSingleObject(ProcessInformation.hProcess, INFINITE)
          else
              Raise Exception.Create('O aplicativo ' + cCaminho + ' n�o foi localizado.');
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      mensagemUsuario('');
  end;
end;

procedure TfrmMenu.prPreparaAmbiente();
begin
  cPathSistema  := ExtractFilePath(Application.ExeName);
  cPathEmpAtiva := 'EMP' + ZeroEsquerda(IntToStr(nCdEmpresaAtiva),3) + '\';

  { -- diret�rios diversos -- }
  ForceDirectories('C:\Temp\');
  ForceDirectories(cPathSistema + 'Versao\');

  { --     diret�rios Arquivos    -- }
  { -- fontes com caminho padr�o: -- }
  { -- fEmiteNFE2                 -- }
  { -- fImportaALiquotaIBPT       -- }
  { -- fImportaMunicipioIBGE      -- }
  { -- fModeloDocumento           -- }
  ForceDirectories(cPathSistema + 'Arquivos\Reports\');
  ForceDirectories(cPathSistema + 'Arquivos\Schemas\');
  ForceDirectories(cPathSistema + 'Arquivos\Municipios\');
  ForceDirectories(cPathSistema + 'Arquivos\IBPT\');
  ForceDirectories(cPathSistema + 'Arquivos\ModeloDocumento\');
  ForceDirectories(cPathSistema + 'Arquivos\ModeloDocumento\Imagens\');

  { --     diret�rios Imagens     -- }
  { -- fontes com caminho padr�o: -- }
  { -- fPdv                       -- }
  { -- fCaixa_Recebimento         -- }
  ForceDirectories(cPathSistema + 'Imagens\');

  { --     diret�rios NFe/NFCe    -- }
  { -- fontes com caminho padr�o: -- }
  { -- fEmiteNFE2                 -- }

  { -- remove pastas, remover esta fun��o na pr�xima vers�o -- }
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\NFeCanc');
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\NFeDPEC');
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\CCe');
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Producao\NFe\NFeCanc');
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Producao\NFe\NFeDPEC');
  RemoveDir(cPathSistema + cPathEmpAtiva + 'Producao\NFe\CCe');

  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\NFeAtivo\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\NFeInu\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Evento\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Evento\Canc');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Evento\CCe');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Evento\EPEC');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\PDF\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFe\Lote\');

  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\NFeAtivo\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\NFeInu\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Evento\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Evento\Canc');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Evento\CCe');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Evento\EPEC');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\PDF\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFe\Lote\');

  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\NFCeAtivo');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\NFCeCanc');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\PDF');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\NFCe\Lote');

  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\NFCeAtivo');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\NFCeCanc');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\PDF');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\NFCe\Lote');

  { --        diret�rios CFe      -- }
  { -- fontes com caminho padr�o: -- }
  { -- fFuncoesSAT                -- }
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\CFe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\CFe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\CFe\CFeVenda\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\CFe\CFeCanc\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Homologacao\CFe\Lote\');

  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\CFe\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\CFe\Envio e Resposta\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\CFe\CFeVenda\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\CFe\CFeCanc\');
  ForceDirectories(cPathSistema + cPathEmpAtiva + 'Producao\CFe\Lote\');
end;

end.
