unit fFormaPagtoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmFormaPagtoWeb = class(TfrmCadastro_Padrao)
    qryTabTipoFormaPagto: TADOQuery;
    qryTabTipoFormaPagtonCdTabTipoFormaPagto: TIntegerField;
    qryTabTipoFormaPagtocNmTabTipoFormaPagto: TStringField;
    qryTabTipoFormaPagtocFlgExigeCliente: TIntegerField;
    dsTabTipoFormaPagto: TDataSource;
    qryMasternCdFormaPagtoWeb: TIntegerField;
    qryMastercNmFormaPagtoWeb: TStringField;
    qryMasternCdTabTipoFormaPagto: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFormaPagtoWeb: TfrmFormaPagtoWeb;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmFormaPagtoWeb.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'FORMAPAGTOWEB';
  nCdTabelaSistema  := 557;
  nCdConsultaPadrao := 789;
end;

procedure TfrmFormaPagtoWeb.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTabTipoFormaPagto.Close;
  PosicionaQuery(qryTabTipoFormaPagto, DBEdit3.Text);
end;

procedure TfrmFormaPagtoWeb.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(113);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoFormaPagto.Value := nPK ;
                PosicionaQuery(qryTabTipoFormaPagto, qryMasternCdTabTipoFormaPagto.asString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFormaPagtoWeb.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoFormaPagto.Close;
end;

procedure TfrmFormaPagtoWeb.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoFormaPagto.Close;
  PosicionaQuery(qryTabTipoFormaPagto, qryMasternCdTabTipoFormaPagto.asString);
end;

procedure TfrmFormaPagtoWeb.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (Trim(DbEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DbEdit2.SetFocus ;
  end ;

  if (qryMasternCdTabTipoFormaPagto.Value = 0) or (DbEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pagamento.');
      DbEdit3.SetFocus ;
  end;
end;

initialization
  RegisterClass(TfrmFormaPagtoWeb);

end.
