unit fProdutoWeb_AplicaPrecoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, dxCntner, dxEditor, dxExEdtr, dxEdLib,
  StdCtrls, DB, ADODB, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmProdutoWeb_AplicaPrecoProduto = class(TfrmProcesso_Padrao)
    qryAplicaPreco: TADOQuery;
    Label2: TLabel;
    dxCurValVendaPor: TdxCurrencyEdit;
    dxCurValVendaDe: TdxCurrencyEdit;
    Label1: TLabel;
    qryValidaPreco: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     nCdProdutoPai : integer;
  end;

var
  frmProdutoWeb_AplicaPrecoProduto: TfrmProdutoWeb_AplicaPrecoProduto;

implementation

{$R *.dfm}

procedure TfrmProdutoWeb_AplicaPrecoProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  { -- n�o permite informar pre�os menor que 0 -- }
  if (dxCurValVendaDe.Value < 0) then
  begin
      MensagemAlerta('Pre�o de venda (De) informado deve ser maior ou igual a zero.');
      dxCurValVendaDe.SetFocus;
      Abort;
  end;

  if (dxCurValVendaPor.Value < 0) then
  begin
      MensagemAlerta('Pre�o de venda (Por) informado deve ser maior ou igual a zero.');
      dxCurValVendaPor.SetFocus;
      Abort;
  end;

  { -- valida se o produto n�vel 2 est� com o pre�o zerado, e caso esteja, obriga que o usu�rio informe os pre�os dos sub itens SKU's -- }
  qryValidaPreco.Close;
  qryValidaPreco.Parameters.ParamByName('nPK').Value := nCdProdutoPai;
  qryValidaPreco.Open;

  if ((not qryValidaPreco.IsEmpty) and (dxCurValVendaDe.Value = 0)) then
  begin
      MensagemAlerta('Os pre�os n�o podem ser igual a zero.');
      Abort;
  end;

  { -- o pre�o de venda (de) n�o pode ser menor do que o pre�o de venda (por) -- }
  if (dxCurValVendaDe.Value < dxCurValVendaPor.Value) then
  begin
      MensagemAlerta('Pre�o de Venda (De) informado deve ser maior ou igual ao Pre�o de Venda (Por).');
      dxCurValVendaDe.SetFocus;
      Abort;
  end;

  if (MessageDLG('Os valores informados ser�o aplicados em todos os sub itens do produto.' + chr(13)
      + 'Confirma atualiza��o de pre�o de venda ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      qryAplicaPreco.Close;
      qryAplicaPreco.Parameters.ParamByName('nPK').Value          := nCdProdutoPai;
      qryAplicaPreco.Parameters.ParamByName('nValVendaDe').Value  := dxCurValVendaDe.Value;
      qryAplicaPreco.Parameters.ParamByName('nValVendaPor').Value := dxCurValVendaPor.Value;
      qryAplicaPreco.ExecSQL;
      
      Close;
  end;
end;

end.
