unit rItensPorVenda_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, QRPrev, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptItensPorVenda_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    SummaryBand1: TQRBand;
    qryResultado: TADOQuery;
    DetailBand1: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    cmdPreparaTemp: TADOCommand;
    SPREL_ITENS_POR_VENDA: TADOStoredProc;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText8: TQRDBText;
    QRGroup1: TQRGroup;
    qryResultadonCdLoja: TStringField;
    qryResultadonCdVendedor: TIntegerField;
    qryResultadocNmVendedor: TStringField;
    qryResultadonValPedidosGeral: TBCDField;
    qryResultadonPercentParticip: TBCDField;
    qryResultadoiTotalPedidosGeral: TIntegerField;
    qryResultadoiQtdItensGeral: TIntegerField;
    qryResultadonValPedidosVenda: TBCDField;
    qryResultadoiQtdVenda: TIntegerField;
    qryResultadoiQtdItensVenda: TIntegerField;
    qryResultadonValMedio: TBCDField;
    qryResultadonItemPorVenda: TBCDField;
    qryResultadonValItemMedio: TBCDField;
    qryResultadonValPedidosTroca: TBCDField;
    qryResultadonValVale: TBCDField;
    qryResultadoiTotalTrocas: TIntegerField;
    qryResultadoiQtdItensTrocas: TIntegerField;
    qryResultadonSumValPedidos: TBCDField;
    qryResultadonSumTotalPedidos: TIntegerField;
    qryResultadonSumQtdItens: TIntegerField;
    qryResultadonValMedioTotal: TBCDField;
    qryResultadonItemPorVendaTotal: TBCDField;
    qryResultadonValItemMedioTotal: TBCDField;
    QRLabel1: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRDBText30: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRExpr1: TQRExpr;
    QRLabel29: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRDBText9: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr9: TQRExpr;
    qryResultadonSumValPedidosGeral: TBCDField;
    QRBand2: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRExpr10: TQRExpr;
    QRLabel39: TQRLabel;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    QRShape9: TQRShape;
    QRExpr19: TQRExpr;
    QRExpr20: TQRExpr;
    QRExpr21: TQRExpr;
    QRExpr22: TQRExpr;
    QRLabel43: TQRLabel;
    QRExpr24: TQRExpr;
    QRDBText10: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryResultadonValJurosCondPagto: TBCDField;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel46: TQRLabel;
    QRExpr23: TQRExpr;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRExpr25: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptItensPorVenda_view: TrptItensPorVenda_view;

implementation

uses
  fMenu;

{$R *.dfm}

end.
