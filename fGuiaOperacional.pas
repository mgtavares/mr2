unit fGuiaOperacional;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxContainer, cxTreeView, OleCtrls, IdBaseComponent, IdComponent,
  StdCtrls, IdTCPConnection, ShellAPI, IdTCPClient, IdFTP, SHDocVw;

type
  TfrmGuiaOperacional = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    Label1: TLabel;
    cxTreeView1: TcxTreeView;
    Panel2: TPanel;
    lblGuia: TLabel;
    WebBrowser1: TWebBrowser;
    procedure cxTreeView1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGuiaOperacional : TfrmGuiaOperacional;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmGuiaOperacional.cxTreeView1Click(Sender: TObject);
begin
  inherited;

  if (cxTreeView1.Selected.Text <> 'Guia Operacional') then
      WebBrowser1.Navigate(frmMenu.cPathSistema + 'Ajuda\' + cxTreeView1.Selected.Text + '.pdf');

  lblGuia.Caption := cxTreeView1.Selected.Text;
end;

procedure TfrmGuiaOperacional.FormShow(Sender: TObject);
var
  i           : Integer;
  Arquivo     : TextFile;
  buscaPdf    : TSearchRec;
  arquivosPdf : TStrings;
  ArquivoNome : String;
begin
  inherited;

  lblGuia.Font.Color   := clBlack;
  lblGuia.Font.Size    := 12;
  
  {-- Varre a pasta de Ajuda e seleciona os arquivos .pdf --}
  arquivosPdf := TStringList.Create;
  if FindFirst('.\Ajuda\*.*', faArchive, buscaPdf) = 0 then
  begin
      repeat
          {-- adiciona o arquivo enontrado na pasta, dentro do ArquivosPdf --}
          arquivosPdf.Add(buscaPdf.Name);
      until FindNext(buscaPdf) <> 0;
      FindClose(buscaPdf);
  end;

  {-- Faz a contagem e lista os arquivos --}
  for i := 0 to arquivosPdf.Count - 1 do
  begin
      {-- Tira o .pdf do nome do arquivo --}
      ArquivoNome := StringReplace(arquivosPdf[i], '.pdf', '', [rfReplaceAll, rfIgnoreCase]);
      cxTreeView1.Items.AddChild(cxTreeView1.Items.Item[0], ArquivoNome);
  end;
end;

initialization
  RegisterClass (TfrmGuiaOperacional);
end.
