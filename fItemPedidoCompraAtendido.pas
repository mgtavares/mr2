unit fItemPedidoCompraAtendido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmItemPedidoCompraAtendido = class(TfrmProcesso_Padrao)
    qryItem: TADOQuery;
    DBGridEh1: TDBGridEh;
    dsItem: TDataSource;
    qryItemnCdRecebimento: TIntegerField;
    qryItemcNrDocto: TStringField;
    qryItemcSerieDocto: TStringField;
    qryItemdDtDocto: TDateTimeField;
    qryItemdDtReceb: TDateTimeField;
    qryItemdDtAtendimento: TDateTimeField;
    qryItemnQtdeAtendida: TBCDField;
    qryItemiDias: TIntegerField;
    qryItemcNmTerceiro: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmItemPedidoCompraAtendido: TfrmItemPedidoCompraAtendido;

implementation

{$R *.dfm}

end.
