unit fConciliaCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, cxLookAndFeelPainters, cxButtons, cxPC,
  cxControls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, ER2Excel, FileCtrl;

type
  TfrmConciliaCartao = class(TfrmProcesso_Padrao)
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    qryGrupoOperadora: TADOQuery;
    qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField;
    qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField;
    dsGrupoOperadora: TDataSource;
    qryOperadora: TADOQuery;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    qryOperadoracNmOperadoraCartao: TStringField;
    dsOperadora: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edtLoja: TMaskEdit;
    edtDtVencIni: TMaskEdit;
    edtGrupoOperadora: TMaskEdit;
    edtOperadora: TMaskEdit;
    edtDtVencFim: TMaskEdit;
    edtDtTransacaoFim: TMaskEdit;
    edtDtTransacaoIni: TMaskEdit;
    edtNrParcela: TMaskEdit;
    edtNrLote: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    cxButton1: TcxButton;
    StaticText1: TStaticText;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cmdPreparaTemp: TADOCommand;
    qryTransacaoPendente: TADOQuery;
    qryTransacaoPendentenCdTitulo: TIntegerField;
    qryTransacaoPendentenCdTransacaoCartao: TIntegerField;
    qryTransacaoPendentedDtVenc: TDateTimeField;
    qryTransacaoPendentedDtTransacao: TDateTimeField;
    qryTransacaoPendentecNrLote: TStringField;
    qryTransacaoPendentecParcela: TStringField;
    qryTransacaoPendentenCdOperadora: TIntegerField;
    qryTransacaoPendentecNmOperadora: TStringField;
    qryTransacaoPendentenCdGrupoOperadora: TIntegerField;
    qryTransacaoPendentecNmGrupoOperadora: TStringField;
    qryTransacaoPendentenValTit: TBCDField;
    qryTransacaoPendentenSaldoTit: TBCDField;
    qryTransacaoPendentedDtVencOriginal: TDateTimeField;
    qryTransacaoPendentenSaldoTitOriginal: TBCDField;
    qryTransacaoPendentecStatus: TStringField;
    qryTransacaoSelecionada: TADOQuery;
    qryTransacaoSelecionadanCdTitulo: TIntegerField;
    qryTransacaoSelecionadanCdTransacaoCartao: TIntegerField;
    qryTransacaoSelecionadadDtVenc: TDateTimeField;
    qryTransacaoSelecionadadDtTransacao: TDateTimeField;
    qryTransacaoSelecionadacNrLote: TStringField;
    qryTransacaoSelecionadacParcela: TStringField;
    qryTransacaoSelecionadanCdOperadora: TIntegerField;
    qryTransacaoSelecionadacNmOperadora: TStringField;
    qryTransacaoSelecionadanCdGrupoOperadora: TIntegerField;
    qryTransacaoSelecionadacNmGrupoOperadora: TStringField;
    qryTransacaoSelecionadanValTit: TBCDField;
    qryTransacaoSelecionadanSaldoTit: TBCDField;
    qryTransacaoSelecionadadDtVencOriginal: TDateTimeField;
    qryTransacaoSelecionadanSaldoTitOriginal: TBCDField;
    qryTransacaoSelecionadacStatus: TStringField;
    dsTransacaoPendente: TDataSource;
    dsTransacaoSelecionada: TDataSource;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1dDtTransacao: TcxGridDBColumn;
    cxGridDBTableView1cNrLote: TcxGridDBColumn;
    cxGridDBTableView1cParcela: TcxGridDBColumn;
    cxGridDBTableView1nCdOperadora: TcxGridDBColumn;
    cxGridDBTableView1cNmOperadora: TcxGridDBColumn;
    cxGridDBTableView1nCdGrupoOperadora: TcxGridDBColumn;
    cxGridDBTableView1cNmGrupoOperadora: TcxGridDBColumn;
    cxGridDBTableView1nValTit: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1dDtVencOriginal: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTitOriginal: TcxGridDBColumn;
    cxGridDBTableView1cStatus: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1dDtTransacao: TcxGridDBColumn;
    cxGrid1DBTableView1cNrLote: TcxGridDBColumn;
    cxGrid1DBTableView1cParcela: TcxGridDBColumn;
    cxGrid1DBTableView1nCdOperadora: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperadora: TcxGridDBColumn;
    cxGrid1DBTableView1nCdGrupoOperadora: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoOperadora: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVencOriginal: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTitOriginal: TcxGridDBColumn;
    cxGrid1DBTableView1cStatus: TcxGridDBColumn;
    qryTransacoes: TADOQuery;
    qryAux: TADOQuery;
    qryTransacaoConciliada: TADOQuery;
    qryTransacaoConciliadanCdTitulo: TIntegerField;
    qryTransacaoConciliadanCdTransacaoCartao: TIntegerField;
    qryTransacaoConciliadadDtVenc: TDateTimeField;
    qryTransacaoConciliadadDtTransacao: TDateTimeField;
    qryTransacaoConciliadacNrLote: TStringField;
    qryTransacaoConciliadacParcela: TStringField;
    qryTransacaoConciliadanCdOperadora: TIntegerField;
    qryTransacaoConciliadacNmOperadora: TStringField;
    qryTransacaoConciliadanCdGrupoOperadora: TIntegerField;
    qryTransacaoConciliadacNmGrupoOperadora: TStringField;
    qryTransacaoConciliadanValTit: TBCDField;
    qryTransacaoConciliadanSaldoTit: TBCDField;
    qryTransacaoConciliadadDtVencOriginal: TDateTimeField;
    qryTransacaoConciliadanSaldoTitOriginal: TBCDField;
    qryTransacaoConciliadacStatus: TStringField;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    dsTransacaoConciliada: TDataSource;
    cxGrid3DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid3DBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGrid3DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid3DBTableView1dDtTransacao: TcxGridDBColumn;
    cxGrid3DBTableView1cNrLote: TcxGridDBColumn;
    cxGrid3DBTableView1cParcela: TcxGridDBColumn;
    cxGrid3DBTableView1nCdOperadora: TcxGridDBColumn;
    cxGrid3DBTableView1cNmOperadora: TcxGridDBColumn;
    cxGrid3DBTableView1nCdGrupoOperadora: TcxGridDBColumn;
    cxGrid3DBTableView1cNmGrupoOperadora: TcxGridDBColumn;
    cxGrid3DBTableView1nValTit: TcxGridDBColumn;
    cxGrid3DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid3DBTableView1dDtVencOriginal: TcxGridDBColumn;
    cxGrid3DBTableView1nSaldoTitOriginal: TcxGridDBColumn;
    cxGrid3DBTableView1cStatus: TcxGridDBColumn;
    qryTransacaoPendenteiParcela: TIntegerField;
    qryTransacaoSelecionadaiParcela: TIntegerField;
    qryTransacaoConciliadaiParcela: TIntegerField;
    SP_CONCILIA_CARTAO: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxButton2: TcxButton;
    PopupMenu1: TPopupMenu;
    AlterarDadosdoPagamento1: TMenuItem;
    qryTransacaoCartao: TADOQuery;
    qryTransacaoCartaonCdTransacaoCartao: TIntegerField;
    qryTransacaoCartaodDtTransacao: TDateTimeField;
    qryTransacaoCartaodDtCredito: TDateTimeField;
    qryTransacaoCartaoiNrCartao: TLargeintField;
    qryTransacaoCartaoiNrDocto: TIntegerField;
    qryTransacaoCartaoiNrAutorizacao: TIntegerField;
    qryTransacaoCartaonCdOperadoraCartao: TIntegerField;
    qryTransacaoCartaonCdLanctoFin: TIntegerField;
    qryTransacaoCartaonValTransacao: TBCDField;
    qryTransacaoCartaonCdContaBancaria: TIntegerField;
    qryTransacaoCartaocFlgConferencia: TIntegerField;
    qryTransacaoCartaonCdUsuarioConf: TIntegerField;
    qryTransacaoCartaodDtConferencia: TDateTimeField;
    qryTransacaoCartaonCdStatusDocto: TIntegerField;
    qryTransacaoCartaoiParcelas: TIntegerField;
    qryTransacaoCartaocNrLotePOS: TStringField;
    qryTransacaoCartaonCdGrupoOperadoraCartao: TIntegerField;
    qryTransacaoCartaocFlgPOSEncerrado: TIntegerField;
    qryTransacaoCartaonCdLojaCartao: TIntegerField;
    qryTransacaoCartaonValTaxaOperadora: TBCDField;
    qryTransacaoCartaonCdCondPagtoCartao: TIntegerField;
    qryTransacaoCartaonCdServidorOrigem: TIntegerField;
    qryTransacaoCartaodDtReplicacao: TDateTimeField;
    qryTransacaoCartaocIDExterno: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonPercAcrescimo: TBCDField;
    qryCondPagtonCdTabTipoFormaPagto: TIntegerField;
    qryCartaoSituacaoAtual: TADOQuery;
    qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField;
    qryCartaoSituacaoAtualcNmCondPagto: TStringField;
    qryCartaoSituacaoAtualcNmFormaPagto: TStringField;
    SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc;
    qryDadosTitulo: TADOQuery;
    qryDadosTitulonCdTitulo: TIntegerField;
    qryDadosTitulonCdLojaTit: TIntegerField;
    Label8: TLabel;
    edtNrDocto: TMaskEdit;
    qryTransacaoPendentecNmOperadoraCartao: TStringField;
    qryTransacaoPendenteiNrDocto: TIntegerField;
    qryTransacaoPendenteiNrAutorizacao: TIntegerField;
    cxGridDBTableView1DBcNmOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView1DBiNrDocto: TcxGridDBColumn;
    cxGridDBTableView1DBiNrAutorizacao: TcxGridDBColumn;
    qryTransacaoSelecionadacNmOperadoraCartao: TStringField;
    qryTransacaoSelecionadaiNrDocto: TIntegerField;
    qryTransacaoSelecionadaiNrAutorizacao: TIntegerField;
    cxGrid1DBTableView1DBcNmOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1DBiNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1DBiNrAutorizacao: TcxGridDBColumn;
    GroupBox2: TGroupBox;
    cxButton3: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    rgOrigemTransacao: TRadioGroup;
    rgConciliacaoAut: TRadioGroup;
    btGerarArquivo: TcxButton;
    btLerArquivo: TcxButton;
    qryArquivo: TADOQuery;
    qryArquivocColuna: TStringField;
    qryArquivocNmGrupoOperadoraCartao: TStringField;
    qryArquivocNmLoja: TStringField;
    qryArquivocNmOperadoraCartao: TStringField;
    qryArquivocTipoOperacao: TStringField;
    qryArquivoiParcelas: TIntegerField;
    qryArquivoiNrDocto: TIntegerField;
    qryArquivoiNrAutorizacao: TIntegerField;
    qryArquivonValTransacao: TStringField;
    qryArquivocFlgTipoOperacao: TStringField;
    qryArquivocNmTerceiro: TStringField;
    qryArquivocDtTransacao: TStringField;
    qryArquivocOrigem: TStringField;
    qryArquivocReferencia1: TStringField;
    qryTransacaoCartaocNrDoctoNSU: TSmallintField;
    qryTransacaoCartaocNrAutorizacao: TStringField;
    procedure cxButton1Click(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtGrupoOperadoraExit(Sender: TObject);
    procedure edtOperadoraExit(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure qryTransacaoPendenteAfterPost(DataSet: TDataSet);
    procedure cxButton4Click(Sender: TObject);
    procedure cxGridDBTableView1DragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure edtGrupoOperadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtOperadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure qryTransacaoPendenteBeforePost(DataSet: TDataSet);
    procedure AlterarDadosdoPagamento1Click(Sender: TObject);
    procedure btGerarArquivoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaCartao: TfrmConciliaCartao;

implementation

uses fMenu, fLookup_Padrao, fPdvFormaPagto, fPdvCondPagto,
  fCaixa_DadosCartao, fCaixa_AlteraCartao, StrUtils;

{$R *.dfm}

procedure TfrmConciliaCartao.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (DBedit1.Text = '') then
  begin
      MensagemAlerta('Seleciona a loja.') ;
      edtLoja.SetFocus;
      exit ;
  end ;

  cmdPreparaTemp.Execute;
  
  qryTransacoes.Close ;
  qryTransacoes.Parameters.ParamByName('nCdEmpresa').Value              := frmMenu.nCdEmpresaAtiva;
  qryTransacoes.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(edtLoja.Text) ;
  qryTransacoes.Parameters.ParamByName('nCdGrupoOperadoraCartao').Value := frmMenu.ConvInteiro(edtGrupoOperadora.Text) ;
  qryTransacoes.Parameters.ParamByName('nCdOperadoraCartao').Value      := frmMenu.ConvInteiro(edtOperadora.Text) ;
  qryTransacoes.Parameters.ParamByName('dDtVencIni').Value              := DateToStr(frmMenu.ConvData(edtDtVencIni.Text)) ;
  qryTransacoes.Parameters.ParamByName('dDtVencFim').Value              := DateToStr(frmMenu.ConvData(edtDtVencFim.Text)) ;
  qryTransacoes.Parameters.ParamByName('dDtTransacaoIni').Value         := DateToStr(frmMenu.ConvData(edtDtTransacaoIni.Text)) ;
  qryTransacoes.Parameters.ParamByName('dDtTransacaoFim').Value         := DateToStr(frmMenu.ConvData(edtDtTransacaoFim.Text)) ;
  qryTransacoes.Parameters.ParamByName('cNrLotePOS').Value              := edtNrLote.Text ;
  qryTransacoes.Parameters.ParamByName('iParcela').Value                := frmMenu.ConvInteiro(edtNrParcela.Text) ;
  qryTransacoes.Parameters.ParamByName('iNrDocto').Value                := frmMenu.ConvInteiro(edtNrDocto.Text) ;
  qryTransacoes.Parameters.ParamByName('cFlgOrigemTransacao').Value     := rgOrigemTransacao.ItemIndex;
  qryTransacoes.ExecSQL;

  qryTransacaoPendente.Close ;
  qryTransacaoPendente.Open ;

  qryTransacaoSelecionada.Close ;
  qryTransacaoSelecionada.Open  ;

end;

procedure TfrmConciliaCartao.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TfrmConciliaCartao.edtGrupoOperadoraExit(Sender: TObject);
begin
  inherited;

  qryGrupoOperadora.Close ;
  PosicionaQuery(qryGrupoOperadora, edtGrupoOperadora.Text) ;
  
end;

procedure TfrmConciliaCartao.edtOperadoraExit(Sender: TObject);
begin
  inherited;

  qryOperadora.Close ;
  PosicionaQuery(qryOperadora, edtOperadora.Text) ;

end;

procedure TfrmConciliaCartao.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;

  if not (qryTransacaoPendente.eof) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #TempTransacoes Set cStatus = ' + Chr(39) + 'PRE' + Chr(39) + ' WHERE nCdTitulo = ' + qryTransacaoPendentenCdTitulo.AsString) ;
      qryAux.ExecSQL ;

      qryTransacaoPendente.Requery() ;
      qryTransacaoSelecionada.Requery();
  end ;

end;

procedure TfrmConciliaCartao.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;

  if not (qryTransacaoSelecionada.eof) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #TempTransacoes Set cStatus = ' + Chr(39) + 'PEN' + Chr(39) + ' WHERE nCdTitulo = ' + qryTransacaoSelecionadanCdTitulo.AsString) ;
      qryAux.ExecSQL ;

      qryTransacaoPendente.Requery() ;
      qryTransacaoSelecionada.Requery();

  end ;

end;

procedure TfrmConciliaCartao.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
  edtLoja.SetFocus ;
  
end;

procedure TfrmConciliaCartao.cxButton3Click(Sender: TObject);
begin
  inherited;

  cmdPreparaTemp.Execute;

  qryTransacaoConciliada.Close ;
  qryTransacaoConciliada.Open ;
  
end;

procedure TfrmConciliaCartao.cxButton2Click(Sender: TObject);
begin
  inherited;

  if (qryTransacaoSelecionada.eof) then
  begin
      MensagemAlerta('Nenhuma transa��o selecionada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a pr�-concilia��o destas transa��es ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #TempTransacoes Set cStatus = ' + Chr(39) + 'CON' + Chr(39) + ' WHERE cStatus = ' + Chr(39) + 'PRE' + Chr(39)) ;
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryTransacaoSelecionada.Requery() ;
  cxButton3.Click ;


end;

procedure TfrmConciliaCartao.qryTransacaoPendenteAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTransacoes Set dDtVenc = Convert(DATETIME,' + Chr(39) + qryTransacaoPendentedDtVenc.AsString + Chr(39) + ',103)') ;
  qryAux.SQL.Add(' WHERE nCdOperadora = ' + qryTransacaoPendentenCdOperadora.AsString) ;
  qryAux.SQL.Add('   AND cStatus      = ' + Chr(39) + 'PEN' + Chr(39)) ;
  qryAux.SQL.Add('   AND cNrLote      = ' + Chr(39) + qryTransacaoPendentecNrLote.Value + Chr(39)) ;
  qryAux.SQL.Add('   AND iParcela     = ' + qryTransacaoPendenteiParcela.AsString) ;
  qryAux.SQL.Add('   AND nCdTitulo   <> ' + qryTransacaoPendentenCdTitulo.AsString) ;
  qryAux.ExecSQL ;

  qryTransacaoPendente.Requery();

end;

procedure TfrmConciliaCartao.cxButton4Click(Sender: TObject);
begin
  inherited;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTransacoes Set cStatus = ' + Chr(39) + 'PEN' + Chr(39) + ' WHERE cStatus = ' + Chr(39) + 'PRE' + Chr(39)) ;
  qryAux.ExecSQL ;

  qryTransacaoSelecionada.Requery();
  qryTransacaoPendente.Requery() ;

end;

procedure TfrmConciliaCartao.cxGridDBTableView1DragOver(Sender,
  Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;

  Accept := Source is TcxDragControlObject ;
  
end;

procedure TfrmConciliaCartao.edtGrupoOperadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(172);

        If (nPK > 0) then
        begin
            edtGrupoOperadora.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConciliaCartao.edtLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            edtLoja.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConciliaCartao.edtOperadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(149);

        If (nPK > 0) then
        begin
            edtOperadora.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConciliaCartao.cxButton5Click(Sender: TObject);
begin

  cmdPreparaTemp.Execute;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTransacoes Set cStatus = ' + Chr(39) + 'PEN' + Chr(39) + ' WHERE cStatus = ' + Chr(39) + 'CON' + Chr(39)) ;
  qryAux.ExecSQL ;

  if (qryTransacaoSelecionada.State = dsBrowse) then
      qryTransacaoSelecionada.Requery();

  if (qryTransacaoPendente.State = dsBrowse) then
      qryTransacaoPendente.Requery() ;

  cxButton3.Click;

end;

procedure TfrmConciliaCartao.cxButton6Click(Sender: TObject);
begin
  inherited;

  if qryTransacaoConciliada.Eof then
  begin
      MensagemAlerta('Nenhuma transa��o conciliada para efetiva��o.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a concilia��o destas parcelas ?' +#13#13 + 'Este processo n�o poder� ser estornado.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_CONCILIA_CARTAO.Close ;
      SP_CONCILIA_CARTAO.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      SP_CONCILIA_CARTAO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      SP_CONCILIA_CARTAO.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE FROM #TempTransacoes WHERE cStatus = ' + Chr(39) + 'CON' + Chr(39)) ;
  qryAux.ExecSQL;

  qryTransacaoConciliada.Close ;
  qryTransacaoConciliada.Open ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmConciliaCartao.qryTransacaoPendenteBeforePost(
  DataSet: TDataSet);
begin

  if (qryTransacaoPendentedDtVenc.Value < Date) then
  begin

      case MessageDlg('A data de vencimento � menor que a data de hoje. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;

  end ;

  inherited;

end;

procedure TfrmConciliaCartao.AlterarDadosdoPagamento1Click(
  Sender: TObject);
var
    nPK : Integer ;
    nCdFormapagto, nCdCondPagto : integer ;
    objDadosCartao  : TfrmCaixa_dadosCartao ;
    objAlteraCartao : TfrmCaixa_AlteraCartao ;
    objSelFormaPagto: TfrmPdvFormaPagto ;
    objSelCondPagto : TfrmPdvCondPagto ;
begin
  inherited;

  if not qryTransacaoPendente.Eof then
  begin

    qryTransacaoCartao.Close;
    PosicionaQuery(qryTransacaoCartao, qryTransacaoPendentenCdTransacaoCartao.AsString) ;

    if qryTransacaoCartao.eof then
    begin
        MensagemErro('A transa��o do cart�o n�o foi encontrada na tabela TRANSACAOCARTAO. ID: ' + qryTransacaoPendentenCdTransacaoCartao.asString) ;
        abort ;
    end ;

    if (qryTransacaoPendenteiParcela.Value > 1) then
    begin
        MensagemAlerta('A altera��o s� pode ser feita na parcela n�mero 1 da transa��o do cart�o.') ;
        abort ;
    end ;

    {-- inst�ncia os objetos das telas --}
    objDadosCartao  := TfrmCaixa_dadosCartao.Create(Self) ;
    objAlteraCartao := TfrmCaixa_AlteraCartao.Create(Self) ;
    objSelCondPagto := TfrmPdvCondPagto.Create(Self) ;
    objSelFormaPagto:= TfrmPdvFormaPagto.Create(Self) ;

    objSelFormaPagto.cFlgLiqCrediario := 0 ;
    objSelFormaPagto.bSomenteCartao   := True ;
    nCdFormaPagto                     := objSelFormaPagto.SelecionaFormaPagto() ;
    objSelFormaPagto.bSomenteCartao   := False ;

    if (nCdFormaPagto <= 0) then
        abort ;

    {-- passa os parametros para a tela de consulta de condi��es --}

    qryDadosTitulo.Close;
    PosicionaQuery(qryDadosTitulo, qryTransacaoPendentenCdTitulo.AsString) ;

    objSelCondPagto.nCdLoja               := qryDadosTitulonCdLojaTit.Value ;
    objSelCondPagto.nCdFormaPagto         := nCdFormaPagto                  ;
    objSelCondPagto.cFlgLiqCrediario      := 0                              ;

    objSelCondPagto.edtValorPagar.Value   := qryTransacaoCartaonValTransacao.Value ;
    objSelCondPagto.edtSaldo.Value        := qryTransacaoCartaonValTransacao.Value ;
    objSelCondPagto.cPermDesconto         := 'N' ;
    objSelCondPagto.edtValorPagar.Enabled := False ;

    {-- Processa a consulta --}
    nCdCondPagto  := objSelCondPagto.SelecionaCondPagto() ;

    {-- se o operador desistiu da consulta, retorna por aqui --}
    if (nCdCondPagto <= 0) then
        abort ;

    qryCondPagto.Close ;
    PosicionaQuery(qryCondPagto, IntToStr(nCdCondPagto)) ;

    // debito

    objDadosCartao.nCdLoja := qryDadosTitulonCdLojaTit.Value ;

    if (qryCondPagtonCdTabTipoFormaPagto.Value = 3) then
        objDadosCartao.cTipoOperacao       := 'D'
    else if (qryCondPagtonCdTabTipoFormaPagto.Value = 4) then
        objDadosCartao.cTipoOperacao       := 'C' ;

    objDadosCartao.edtValorPagar.Value := qryTransacaoCartaonValTransacao.Value ;

    showForm( objDadosCartao , FALSE ) ;

    objDadosCartao.nCdLoja := 0 ;

    if (Trim(objDadosCartao.edtNrDocto.Text) = '') then
    begin
        MensagemErro('Processo cancelado.') ;
        exit ;
    end ;

    qryCartaoSituacaoAtual.Close ;
    PosicionaQuery(qryCartaoSituacaoAtual, qryTransacaoPendentenCdTransacaoCartao.asString) ;

    objAlteraCartao.edtFormaPagto_Atual.Text  := qryCartaoSituacaoAtualcNmFormaPagto.Value;
    objAlteraCartao.edtCondPagto_Atual.Text   := qryCartaoSituacaoAtualcNmCondPagto.Value;
    objAlteraCartao.edtOperadora_Atual.Text   := qryCartaoSituacaoAtualcNmOperadoraCartao.Value ;
    objAlteraCartao.edtDocto_Atual.Text       := qryTransacaoCartaoiNrDocto.Text;
    objAlteraCartao.edtAutorizacao_Atual.Text := qryTransacaoCartaoiNrAutorizacao.Text ;

    objAlteraCartao.edtFormaPagto_Nova.Text  := objSelFormaPagto.qryFormaPagtocNmFormaPagto.Value;
    objAlteraCartao.edtCondPagto_Nova.Text   := objSelCondPagto.qryCondPagtocNmCondPagto.Value;
    objAlteraCartao.edtOperadora_Nova.Text   := objDadosCartao.edtNmOperadora.Text ;
    objAlteraCartao.edtDocto_Novo.Text       := objDadosCartao.edtNrDocto.Text;
    objAlteraCartao.edtAutorizacao_Nova.Text := objDadosCartao.edtNrAutorizacao.Text;

    if (objAlteraCartao.ShowModal = MrNo) then
        exit ;

    frmMenu.Connection.BeginTrans;

    try
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Close ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdTransacaoCartao').Value := qryTransacaoPendentenCdTransacaoCartao.Value ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdCondPagto').Value       := nCdCondPagto ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdOperadoraCartao').Value := strToint(objDadosCartao.edtnCdOperadoraCartao.Text) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrDocto').Value           := strToint(frmMenu.RetNumeros(objDadosCartao.edtNrDocto.Text)) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrAutorizacao').Value     := strToint(frmMenu.RetNumeros(objDadosCartao.edtNrAutorizacao.Text)) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrDoctoNSU').Value        := objDadosCartao.edtNrDocto.Text ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrAutorizacao').Value     := objDadosCartao.edtNrAutorizacao.Text ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    freeAndNil( objSelFormaPagto ) ;
    freeAndNil( objSelCondPagto ) ;
    freeAndNil( objDadosCartao ) ;
    freeAndNil( objAlteraCartao ) ;

    ShowMessage('Processamento realizado com sucesso.') ;

    cxButton1.Click;

    ShowMessage('As parcelas exibidas na concilia��o foram atualizadas.') ;

  end ;

end;

procedure TfrmConciliaCartao.btGerarArquivoClick(Sender: TObject);
var
  cDirArquivo  : String;
  cResultado   : String;
  ArquivoCSV   : TextFile;
  i            : Integer;
begin
  inherited;

  if (qryTransacaoSelecionada.Eof) then
  begin
      MensagemAlerta('Nenhuma transa��o selecionada.');
      Exit;
  end;

  case MessageDlg('Confirma gera��o do arquivo das transa��es selecionadas ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  SelectDirectory('Selecione o diret�rio do arquivo de remessa','',cDirArquivo);

  if (cDirArquivo = '') then
  begin
      MensagemAlerta('Diret�rio inv�lido');
      Abort;
  end;

  try
      frmMenu.mensagemUsuario('Gerando arquivo de remessa...');

      { -- formata nome do arquivo padr�o: REM_DATAHORA -- }
      cDirArquivo := cDirArquivo + '\REM_' + StringReplace(DateToStr(Now()),'/','',[rfReplaceAll, rfIgnoreCase]) + StringReplace(TimeToStr(Now()),':','',[rfReplaceAll, rfIgnoreCase]) + '.csv';

      AssignFile(ArquivoCSV, cDirArquivo);
      ReWrite(ArquivoCSV);

      { -- inseri cabe�alho do arquivo -- }
      Writeln(ArquivoCSV, 'A1;ADQUIRENTE;LOJA_FILIAL;BANDEIRA;TIPO_TRANSACAO;QTD_PARCELAS;NR_DOCTO;NR_AUT;VALOR;CREDITO_DEBITO;CLIENTE;DATA_VENDA;ORIGEM;REFERENCIA1');

      qryArquivo.Close;
      qryArquivo.Open;

      while (not qryArquivo.Eof) do
      begin
          cResultado := '';

          { -- percorre colunas -- }
          for i := 0 to qryArquivo.FieldCount - 1 do
          begin
            cResultado := cResultado + qryArquivo.Fields[i].AsString + ';'
          end;

          { -- remove (;) do final da linha e adiciona resultado na lista -- }
          cResultado := LeftStr(cResultado,Length(cResultado) - 1);
          Writeln(ArquivoCSV, cResultado);

          qryArquivo.Next;
      end;

      { -- for�a escrita e fecha arquivo -- }
      Flush(ArquivoCSV);
      CloseFile(ArquivoCSV);
      frmMenu.mensagemUsuario('');

      ShowMessage('Arquivo de remessa gerado com sucesso.');
  except
      frmMenu.mensagemUsuario('');
      CloseFile(ArquivoCSV);
      DeleteFile(cDirArquivo);
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

initialization
    RegisterClass(TfrmConciliaCartao) ;

end.
