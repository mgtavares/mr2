object frmEmiteNFe2: TfrmEmiteNFe2
  Left = 580
  Top = 288
  Width = 271
  Height = 216
  Caption = 'Emite NFe2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object qryConfigAmbiente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cUFEmissaoNfe'
      
        '      ,cPathLogoNFe                                             ' +
        '                                            '
      '      ,nCdTipoAmbienteNFe'
      '      ,cFlgEmiteNFe '
      '      ,cNrSerieCertificadoNFe'
      '      ,nCdUFEmissaoNFe        '
      '      ,nCdMunicipioEmissaoNFe'
      '      ,CASE WHEN nCdTipoAmbienteNFe = 0 THEN cPathArqNFeProd'
      '            ELSE cPathArqNFeHom'
      '       END cPathArqNFe'
      '      ,cServidorSMTP'
      '      ,nPortaSMTP'
      '      ,cEmail'
      '      ,cSenhaEmail'
      '      ,cFlgUsaSSL'
      '  FROM ConfigAmbiente'
      ' WHERE cNmComputador = :cNmComputador')
    Left = 48
    Top = 41
    object qryConfigAmbientecUFEmissaoNfe: TStringField
      FieldName = 'cUFEmissaoNfe'
      Size = 2
    end
    object qryConfigAmbientecPathLogoNFe: TStringField
      FieldName = 'cPathLogoNFe'
      Size = 100
    end
    object qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField
      FieldName = 'nCdTipoAmbienteNFe'
    end
    object qryConfigAmbientecFlgEmiteNFe: TIntegerField
      FieldName = 'cFlgEmiteNFe'
    end
    object qryConfigAmbientecNrSerieCertificadoNFe: TStringField
      FieldName = 'cNrSerieCertificadoNFe'
      Size = 50
    end
    object qryConfigAmbientenCdUFEmissaoNFe: TIntegerField
      FieldName = 'nCdUFEmissaoNFe'
    end
    object qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField
      FieldName = 'nCdMunicipioEmissaoNFe'
    end
    object qryConfigAmbientecPathArqNFe: TStringField
      FieldName = 'cPathArqNFe'
      ReadOnly = True
      Size = 100
    end
    object qryConfigAmbientecServidorSMTP: TStringField
      FieldName = 'cServidorSMTP'
      Size = 50
    end
    object qryConfigAmbientenPortaSMTP: TIntegerField
      FieldName = 'nPortaSMTP'
    end
    object qryConfigAmbientecEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryConfigAmbientecSenhaEmail: TStringField
      FieldName = 'cSenhaEmail'
      Size = 50
    end
    object qryConfigAmbientecFlgUsaSSL: TIntegerField
      FieldName = 'cFlgUsaSSL'
    end
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '      ,(SELECT TOP 1 1'
      '          FROM PrazoDoctoFiscal PDF'
      '         WHERE PDF.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFiscal'
      
        '           AND PDF.dDtVenc        > DoctoFiscal.dDtEmissao) cFlg' +
        'APrazo'
      '      ,(SELECT DFC.cChaveNFe'
      '          FROM DoctoFiscal DFC'
      
        '         WHERE DFC.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFiscalOr' +
        'igemCompl) cNFeRef'
      '      ,ISNULL((SELECT iUltimoNumero'
      '                 FROM NumDoctoFiscal'
      
        '                WHERE NumDoctoFiscal.nCdSerieFiscal = DoctoFisca' +
        'l.nCdSerieFiscal'
      
        '                  AND NumDoctoFiscal.nCdDoctoFiscal = DoctoFisca' +
        'l.nCdDoctoFiscal),0) as iNrDoctoPend'
      
        '      ,CASE WHEN DoctoFiscal.cUF in ('#39'AM'#39','#39'BA'#39','#39'CE'#39','#39'GO'#39','#39'MG'#39','#39'M' +
        'S'#39','#39'MT'#39','#39'PE'#39','#39'RN'#39','#39'SE'#39','#39'SP'#39')'
      '            THEN 1'
      '            ELSE 0'
      '       END cFlgUFSefaz'
      '  FROM DoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal')
    Left = 80
    Top = 40
    object qryDoctoFiscalnCdDoctoFiscal: TAutoIncField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryDoctoFiscalnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryDoctoFiscalnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryDoctoFiscalnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryDoctoFiscalcFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
    object qryDoctoFiscalcModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoFiscalcTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoFiscalnCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscaldDtSaida: TDateTimeField
      FieldName = 'dDtSaida'
    end
    object qryDoctoFiscaldDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
    object qryDoctoFiscalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoFiscalcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryDoctoFiscalcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryDoctoFiscalcIE: TStringField
      FieldName = 'cIE'
      Size = 14
    end
    object qryDoctoFiscalcTelefone: TStringField
      FieldName = 'cTelefone'
    end
    object qryDoctoFiscalnCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
    end
    object qryDoctoFiscalcEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 50
    end
    object qryDoctoFiscaliNumero: TIntegerField
      FieldName = 'iNumero'
    end
    object qryDoctoFiscalcBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryDoctoFiscalcCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryDoctoFiscalcCep: TStringField
      FieldName = 'cCep'
      FixedChar = True
      Size = 8
    end
    object qryDoctoFiscalcUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValProduto: TBCDField
      FieldName = 'nValProduto'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValICMS: TBCDField
      FieldName = 'nValICMS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValIsenta: TBCDField
      FieldName = 'nValIsenta'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValOutras: TBCDField
      FieldName = 'nValOutras'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValDespesa: TBCDField
      FieldName = 'nValDespesa'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValFrete: TBCDField
      FieldName = 'nValFrete'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscaldDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryDoctoFiscalnCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryDoctoFiscalnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryDoctoFiscalcFlgFaturado: TIntegerField
      FieldName = 'cFlgFaturado'
    end
    object qryDoctoFiscalnCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryDoctoFiscalcNmTransp: TStringField
      FieldName = 'cNmTransp'
      Size = 50
    end
    object qryDoctoFiscalcCNPJTransp: TStringField
      FieldName = 'cCNPJTransp'
      Size = 14
    end
    object qryDoctoFiscalcIETransp: TStringField
      FieldName = 'cIETransp'
      Size = 14
    end
    object qryDoctoFiscalcEnderecoTransp: TStringField
      FieldName = 'cEnderecoTransp'
      Size = 50
    end
    object qryDoctoFiscalcBairroTransp: TStringField
      FieldName = 'cBairroTransp'
      Size = 35
    end
    object qryDoctoFiscalcCidadeTransp: TStringField
      FieldName = 'cCidadeTransp'
      Size = 35
    end
    object qryDoctoFiscalcUFTransp: TStringField
      FieldName = 'cUFTransp'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscaliQtdeVolume: TIntegerField
      FieldName = 'iQtdeVolume'
    end
    object qryDoctoFiscalnPesoBruto: TBCDField
      FieldName = 'nPesoBruto'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnPesoLiq: TBCDField
      FieldName = 'nPesoLiq'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcPlaca: TStringField
      FieldName = 'cPlaca'
      Size = 10
    end
    object qryDoctoFiscalcUFPlaca: TStringField
      FieldName = 'cUFPlaca'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalnValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValFatura: TBCDField
      FieldName = 'nValFatura'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryDoctoFiscalnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryDoctoFiscalnCdUsuarioImpr: TIntegerField
      FieldName = 'nCdUsuarioImpr'
    end
    object qryDoctoFiscalnCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryDoctoFiscaldDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryDoctoFiscalnCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryDoctoFiscalnCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryDoctoFiscalcFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryDoctoFiscalnValCredAdiant: TBCDField
      FieldName = 'nValCredAdiant'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValIcentivoFiscal: TBCDField
      FieldName = 'nValIcentivoFiscal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnCdLanctoFinDoctoFiscal: TIntegerField
      FieldName = 'nCdLanctoFinDoctoFiscal'
    end
    object qryDoctoFiscaldDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryDoctoFiscaldDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryDoctoFiscalnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryDoctoFiscaldDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryDoctoFiscalcFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
    object qryDoctoFiscalcNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryDoctoFiscalcNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
    object qryDoctoFiscalcFlgAPrazo: TIntegerField
      FieldName = 'cFlgAPrazo'
      ReadOnly = True
    end
    object qryDoctoFiscalcComplEnderDestino: TStringField
      FieldName = 'cComplEnderDestino'
      Size = 35
    end
    object qryDoctoFiscalnCdMunicipioDestinoIBGE: TIntegerField
      FieldName = 'nCdMunicipioDestinoIBGE'
    end
    object qryDoctoFiscalnCdPaisDestino: TIntegerField
      FieldName = 'nCdPaisDestino'
    end
    object qryDoctoFiscalcNmPaisDestino: TStringField
      FieldName = 'cNmPaisDestino'
      Size = 50
    end
    object qryDoctoFiscalcCNPJCPFEmitente: TStringField
      FieldName = 'cCNPJCPFEmitente'
      Size = 14
    end
    object qryDoctoFiscalcIEEmitente: TStringField
      FieldName = 'cIEEmitente'
      Size = 14
    end
    object qryDoctoFiscalcNmRazaoSocialEmitente: TStringField
      FieldName = 'cNmRazaoSocialEmitente'
      Size = 100
    end
    object qryDoctoFiscalcNmFantasiaEmitente: TStringField
      FieldName = 'cNmFantasiaEmitente'
      Size = 50
    end
    object qryDoctoFiscalcTelefoneEmitente: TStringField
      FieldName = 'cTelefoneEmitente'
    end
    object qryDoctoFiscalnCdEnderecoEmitente: TIntegerField
      FieldName = 'nCdEnderecoEmitente'
    end
    object qryDoctoFiscalcEnderecoEmitente: TStringField
      FieldName = 'cEnderecoEmitente'
      Size = 50
    end
    object qryDoctoFiscaliNumeroEmitente: TIntegerField
      FieldName = 'iNumeroEmitente'
    end
    object qryDoctoFiscalcComplEnderEmitente: TStringField
      FieldName = 'cComplEnderEmitente'
      Size = 35
    end
    object qryDoctoFiscalcBairroEmitente: TStringField
      FieldName = 'cBairroEmitente'
      Size = 35
    end
    object qryDoctoFiscalnCdMunicipioEmitenteIBGE: TIntegerField
      FieldName = 'nCdMunicipioEmitenteIBGE'
    end
    object qryDoctoFiscalcCidadeEmitente: TStringField
      FieldName = 'cCidadeEmitente'
      Size = 35
    end
    object qryDoctoFiscalcUFEmitente: TStringField
      FieldName = 'cUFEmitente'
      FixedChar = True
      Size = 2
    end
    object qryDoctoFiscalcCepEmitente: TStringField
      FieldName = 'cCepEmitente'
      FixedChar = True
      Size = 8
    end
    object qryDoctoFiscalnCdPaisEmitente: TIntegerField
      FieldName = 'nCdPaisEmitente'
    end
    object qryDoctoFiscalcNmPaisEmitente: TStringField
      FieldName = 'cNmPaisEmitente'
      Size = 50
    end
    object qryDoctoFiscalcSuframa: TStringField
      FieldName = 'cSuframa'
    end
    object qryDoctoFiscalcXMLNFe: TMemoField
      FieldName = 'cXMLNFe'
      BlobType = ftMemo
    end
    object qryDoctoFiscalcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryDoctoFiscalcNrProtocoloCancNFe: TStringField
      FieldName = 'cNrProtocoloCancNFe'
      Size = 100
    end
    object qryDoctoFiscalcCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryDoctoFiscalcArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryDoctoFiscalnValSeguro: TBCDField
      FieldName = 'nValSeguro'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcFlgComplementar: TIntegerField
      FieldName = 'cFlgComplementar'
    end
    object qryDoctoFiscalnCdDoctoFiscalOrigemCompl: TIntegerField
      FieldName = 'nCdDoctoFiscalOrigemCompl'
    end
    object qryDoctoFiscalcNmMotivoComplemento: TStringField
      FieldName = 'cNmMotivoComplemento'
      Size = 150
    end
    object qryDoctoFiscalcNFeRef: TStringField
      FieldName = 'cNFeRef'
      ReadOnly = True
      Size = 100
    end
    object qryDoctoFiscalcNrProtocoloDPEC: TStringField
      FieldName = 'cNrProtocoloDPEC'
      Size = 100
    end
    object qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal: TIntegerField
      FieldName = 'nCdTabTipoEmissaoDoctoFiscal'
    end
    object qryDoctoFiscalcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryDoctoFiscalcFlgFormularioProprio: TIntegerField
      FieldName = 'cFlgFormularioProprio'
    end
    object qryDoctoFiscalnCdPedidoGerador: TIntegerField
      FieldName = 'nCdPedidoGerador'
    end
    object qryDoctoFiscalcNmMotivoComplemento2: TStringField
      FieldName = 'cNmMotivoComplemento2'
      Size = 150
    end
    object qryDoctoFiscalcFlgDPECTransmitida: TIntegerField
      FieldName = 'cFlgDPECTransmitida'
    end
    object qryDoctoFiscalnValBaseIPI: TBCDField
      FieldName = 'nValBaseIPI'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValPIS: TBCDField
      FieldName = 'nValPIS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValCOFINS: TBCDField
      FieldName = 'nValCOFINS'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcCdNumSerieECF: TStringField
      FieldName = 'cCdNumSerieECF'
    end
    object qryDoctoFiscalcJustCancelamento: TStringField
      FieldName = 'cJustCancelamento'
      Size = 255
    end
    object qryDoctoFiscalcFlgCancForaPrazo: TIntegerField
      FieldName = 'cFlgCancForaPrazo'
    end
    object qryDoctoFiscaliStatusRetorno: TIntegerField
      FieldName = 'iStatusRetorno'
    end
    object qryDoctoFiscalcNmStatusRetorno: TStringField
      FieldName = 'cNmStatusRetorno'
      Size = 150
    end
    object qryDoctoFiscalnCdTabTipoModFrete: TIntegerField
      FieldName = 'nCdTabTipoModFrete'
    end
    object qryDoctoFiscalnValTotalImpostoAproxFed: TBCDField
      FieldName = 'nValTotalImpostoAproxFed'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValTotalImpostoAproxEst: TBCDField
      FieldName = 'nValTotalImpostoAproxEst'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValTotalImpostoAproxMun: TBCDField
      FieldName = 'nValTotalImpostoAproxMun'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcFlgDevolucao: TIntegerField
      FieldName = 'cFlgDevolucao'
    end
    object qryDoctoFiscalcEspecieTransp: TStringField
      FieldName = 'cEspecieTransp'
      Size = 50
    end
    object qryDoctoFiscalcFlgAmbienteHom: TIntegerField
      FieldName = 'cFlgAmbienteHom'
    end
    object qryDoctoFiscaliNrDoctoPend: TIntegerField
      FieldName = 'iNrDoctoPend'
      ReadOnly = True
    end
    object qryDoctoFiscalcChaveCanc: TStringField
      FieldName = 'cChaveCanc'
      Size = 100
    end
    object qryDoctoFiscalcArquivoXMLCanc: TStringField
      FieldName = 'cArquivoXMLCanc'
      Size = 200
    end
    object qryDoctoFiscalcXMLCanc: TMemoField
      FieldName = 'cXMLCanc'
      BlobType = ftMemo
    end
    object qryDoctoFiscalcCaminhoXMLCanc: TStringField
      FieldName = 'cCaminhoXMLCanc'
      Size = 200
    end
    object qryDoctoFiscalnValICMSPartDest: TBCDField
      FieldName = 'nValICMSPartDest'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnValICMSPartOrig: TBCDField
      FieldName = 'nValICMSPartOrig'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcFlgConsumFinal: TIntegerField
      FieldName = 'cFlgConsumFinal'
    end
    object qryDoctoFiscalcFlgUFSefaz: TIntegerField
      FieldName = 'cFlgUFSefaz'
      ReadOnly = True
    end
  end
  object qryItemDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT CASE WHEN ((nCdTipoItemPed <> 5) OR (nCdTipoItemPed IS NU' +
        'LL)) THEN Convert(VARCHAR,ItemDoctoFiscal.nCdProduto)'
      
        '            WHEN nCdTipoItemped = 5 THEN '#39'AD'#39' + Convert(VARCHAR(' +
        '10),ItemDoctoFiscal.nCdItemDoctoFiscal)'
      '            ELSE '#39'CFOP'#39' + cCFOP'
      '       END nCdProduto'
      '      ,ItemDoctoFiscal.cNmItem'
      '      ,cDescricaoAdicional'
      '      ,cCFOP'
      '      ,CASE WHEN ItemDoctoFiscal.cNCMSH IS NULL THEN GRI.cNCM'
      '            ELSE ItemDoctoFiscal.cNCMSH'
      '       END as cNCM'
      '      ,ItemDoctoFiscal.cCdST'
      '      ,ItemDoctoFiscal.cCdSTIPI'
      '      ,ItemDoctoFiscal.cUnidadeMedida'
      
        '      ,CASE WHEN GRI.cUnidadeMedida IS NULL THEN ItemDoctoFiscal' +
        '.cUnidadeMedida'
      '            ELSE GRI.cUnidadeMedida'
      '       END as cUnidadeMedidaTribuvel'
      
        '      ,CASE WHEN ItemDoctoFiscal.nCdTabTipoOrigemMercadoria IS N' +
        'ULL THEN GRI.nCdTabTipoOrigemMercadoria'
      '            ELSE ItemDoctoFiscal.nCdTabTipoOrigemMercadoria'
      '       END as nCdTabTipoOrigemMercadoria'
      '      ,ItemDoctoFiscal.nValUnitario'
      '      ,nAliqICMS'
      '      ,ItemDoctoFiscal.nAliqIPI'
      '      ,ItemDoctoFiscal.nPercIVA'
      '      ,ItemDoctoFiscal.nPercBCICMS'
      '      ,ItemDoctoFiscal.nPercBCIVA'
      '      ,ItemDoctoFiscal.nPercBCICMSST'
      '      ,ItemDoctoFiscal.cCSTPIS'
      '      ,ItemDoctoFiscal.nAliqPIS'
      '      ,ItemDoctoFiscal.cCSTCOFINS'
      '      ,ItemDoctoFiscal.nAliqCOFINS'
      '      ,ItemDoctoFiscal.cEnqLegalIPI'
      '      ,ItemDoctoFiscal.cEXTIPI'
      '      ,ItemDoctoFiscal.nAliqICMSInternaOrigem'
      '      ,Sum(ItemDoctoFiscal.nQtde)                    nQtde'
      '      ,Sum(ItemDoctoFiscal.nValTotal)                nValTotal'
      
        '      ,Sum(ItemDoctoFiscal.nValBaseICMS)             nValBaseICM' +
        'S'
      '      ,Sum(ItemDoctoFiscal.nValICMS)                 nValICMS'
      '      ,Sum(ItemDoctoFiscal.nValIPI)                  nValIPI'
      '      ,Sum(ItemDoctoFiscal.nValBaseIPI)              nValBaseIPI'
      
        '      ,Sum(ItemDoctoFiscal.nValBASEICMSSub)          nValBASEICM' +
        'SSub'
      '      ,Sum(ItemDoctoFiscal.nValICMSSub)              nValICMSSub'
      '      ,Sum(ItemDoctoFiscal.nValIsenta)               nValIsenta'
      '      ,Sum(ItemDoctoFiscal.nValOutras)               nValOutras'
      '      ,Sum(ItemDoctoFiscal.nValDespesa)              nValDespesa'
      '      ,Sum(ItemDoctoFiscal.nValFrete)                nValFrete'
      '      ,Sum(ItemDoctoFiscal.nValBasePIS)              nValBasePIS'
      '      ,Sum(ItemDoctoFiscal.nValPIS)                  nValPIS'
      
        '      ,Sum(ItemDoctoFiscal.nValBaseCOFINS)           nValBaseCOF' +
        'INS'
      '      ,Sum(ItemDoctoFiscal.nValCOFINS)               nValCOFINS'
      
        '      ,Sum(ItemDoctoFiscal.nValIcentivoFiscal)       nValIcentiv' +
        'oFiscal'
      
        '      ,itemDoctoFiscal.nValDesconto                  nValDescont' +
        'o'
      '      ,Produto.nCdTabTipoCodProdFat'
      '      ,Produto.cCdFabricante'
      '      ,Produto.cEAN'
      '      ,Produto.cFCI'
      '      ,ItemDoctoFiscal.cCEST'
      
        '      ,Sum(ItemDoctoFiscal.nValImpostoAproxFed) nValImpostoAprox' +
        'Fed'
      
        '      ,Sum(ItemDoctoFiscal.nValImpostoAproxEst) nValImpostoAprox' +
        'Est'
      
        '      ,Sum(ItemDoctoFiscal.nValImpostoAproxMun) nValImpostoAprox' +
        'Mun'
      
        '      ,SUM(ItemDoctoFiscal.nValBaseICMSSubRet)  nValBaseICMSSubR' +
        'et'
      '      ,SUM(ItemDoctoFiscal.nValICMSSubRet)      nValICMSSubRet'
      '      ,SUM(ItemDoctoFiscal.nValBaseICMSDest)    nValBaseICMSDest'
      '      ,SUM(ItemDoctoFiscal.nAliqICMSDest)       nAliqICMSDest'
      '      ,SUM(ItemDoctoFiscal.nAliqICMSInter)      nAliqICMSInter'
      
        '      ,SUM(ItemDoctoFiscal.nAliqICMSPartDest)   nAliqICMSPartDes' +
        't'
      '      ,SUM(ItemDoctoFiscal.nValICMSPartDest)    nValICMSPartDest'
      '      ,SUM(ItemDoctoFiscal.nValICMSPartOrig)    nValICMSPartOrig'
      '  FROM ItemDoctoFiscal'
      
        '       LEFT JOIN GrupoImposto GRI ON GRI.nCdGrupoImposto      = ' +
        'ItemDoctoFiscal.nCdGrupoImposto'
      
        '       LEFT JOIN ItemPedido       ON ItemPedido.nCdItemPedido = ' +
        'ItemDoctoFiscal.nCdItemPedido'
      
        '       LEFT JOIN Produto          ON Produto.nCdProduto       = ' +
        'ItemPedido.nCdProduto'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      
        ' GROUP BY CASE WHEN ((nCdTipoItemPed <> 5) OR (nCdTipoItemPed IS' +
        ' NULL)) THEN Convert(VARCHAR,ItemDoctoFiscal.nCdProduto)'
      
        '               WHEN nCdTipoItemped = 5 THEN '#39'AD'#39' + Convert(VARCH' +
        'AR(10),ItemDoctoFiscal.nCdItemDoctoFiscal)'
      '               ELSE '#39'CFOP'#39' + cCFOP'
      '       END'
      '      ,ItemDoctoFiscal.cNmItem'
      '      ,cDescricaoAdicional'
      '      ,cCFOP'
      '      ,CASE WHEN ItemDoctoFiscal.cNCMSH IS NULL THEN GRI.cNCM'
      '            ELSE ItemDoctoFiscal.cNCMSH'
      '       END'
      '      ,ItemDoctoFiscal.cCdST'
      '      ,ItemDoctoFiscal.cCdSTIPI'
      '      ,ItemDoctoFiscal.cUnidadeMedida'
      
        '      ,CASE WHEN GRI.cUnidadeMedida IS NULL THEN ItemDoctoFiscal' +
        '.cUnidadeMedida'
      '            ELSE GRI.cUnidadeMedida'
      '       END'
      
        '      ,CASE WHEN ItemDoctoFiscal.nCdTabTipoOrigemMercadoria IS N' +
        'ULL THEN GRI.nCdTabTipoOrigemMercadoria'
      '            ELSE ItemDoctoFiscal.nCdTabTipoOrigemMercadoria'
      '       END'
      '      ,ItemDoctoFiscal.nValUnitario'
      '      ,nAliqICMS'
      '      ,ItemDoctoFiscal.nAliqIPI'
      '      ,ItemDoctoFiscal.nPercIVA'
      '      ,ItemDoctoFiscal.nPercBCICMS'
      '      ,ItemDoctoFiscal.nPercBCIVA'
      '      ,ItemDoctoFiscal.nPercBCICMSST'
      '      ,ItemDoctoFiscal.cCSTPIS'
      '      ,ItemDoctoFiscal.nAliqPIS'
      '      ,ItemDoctoFiscal.cCSTCOFINS'
      '      ,ItemDoctoFiscal.nAliqCOFINS'
      '      ,ItemDoctoFiscal.cEnqLegalIPI'
      '      ,ItemDoctoFiscal.cEXTIPI'
      '      ,ItemDoctoFiscal.nAliqICMSInternaOrigem'
      '      ,ItemDoctoFiscal.nValDesconto'
      '      ,Produto.nCdTabTipoCodProdFat'
      '      ,Produto.cCdFabricante'
      '      ,Produto.cEAN'
      '      ,Produto.cFCI'
      '      ,ItemDoctoFiscal.cCEST'
      '      ,ItemDoctoFiscal.nValImpostoAproxFed'
      '      ,ItemDoctoFiscal.nValImpostoAproxEst'
      '      ,ItemDoctoFiscal.nValImpostoAproxMun'
      '      ,ItemDoctoFiscal.nValBaseICMSSubRet'
      '      ,ItemDoctoFiscal.nValICMSSubRet'
      '      ,ItemDoctoFiscal.nValBaseICMSDest'
      '      ,ItemDoctoFiscal.nAliqICMSDest'
      '      ,ItemDoctoFiscal.nAliqICMSInter'
      '      ,ItemDoctoFiscal.nAliqICMSPartDest'
      '      ,ItemDoctoFiscal.nValICMSPartDest'
      '      ,ItemDoctoFiscal.nValICMSPartOrig'
      ' ORDER BY ItemDoctoFiscal.cNmItem'
      '         ,cCFOP')
    Left = 48
    Top = 72
    object qryItemDoctoFiscalnCdProduto: TStringField
      FieldName = 'nCdProduto'
      ReadOnly = True
      Size = 30
    end
    object qryItemDoctoFiscalcNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemDoctoFiscalcDescricaoAdicional: TStringField
      FieldName = 'cDescricaoAdicional'
      Size = 100
    end
    object qryItemDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryItemDoctoFiscalcNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryItemDoctoFiscalcCdSTIPI: TStringField
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalcUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField
      FieldName = 'nCdTabTipoOrigemMercadoria'
    end
    object qryItemDoctoFiscalnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryItemDoctoFiscalnAliqICMS: TBCDField
      FieldName = 'nAliqICMS'
      Precision = 4
      Size = 2
    end
    object qryItemDoctoFiscalnAliqIPI: TBCDField
      FieldName = 'nAliqIPI'
      Precision = 4
      Size = 2
    end
    object qryItemDoctoFiscalnPercIVA: TBCDField
      FieldName = 'nPercIVA'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCICMS: TBCDField
      FieldName = 'nPercBCICMS'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCIVA: TBCDField
      FieldName = 'nPercBCIVA'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnPercBCICMSST: TBCDField
      FieldName = 'nPercBCICMSST'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnQtde: TBCDField
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMS: TBCDField
      FieldName = 'nValICMS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValIPI: TBCDField
      FieldName = 'nValIPI'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseIPI: TBCDField
      FieldName = 'nValBaseIPI'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBASEICMSSub: TBCDField
      FieldName = 'nValBASEICMSSub'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValIsenta: TBCDField
      FieldName = 'nValIsenta'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValOutras: TBCDField
      FieldName = 'nValOutras'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValDespesa: TBCDField
      FieldName = 'nValDespesa'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValFrete: TBCDField
      FieldName = 'nValFrete'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      ReadOnly = True
      Precision = 32
      Size = 6
    end
    object qryItemDoctoFiscalnCdTabTipoCodProdFat: TIntegerField
      FieldName = 'nCdTabTipoCodProdFat'
    end
    object qryItemDoctoFiscalcCdFabricante: TStringField
      FieldName = 'cCdFabricante'
      Size = 60
    end
    object qryItemDoctoFiscalcEAN: TStringField
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryItemDoctoFiscalcUnidadeMedidaTribuvel: TStringField
      FieldName = 'cUnidadeMedidaTribuvel'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalcCSTPIS: TStringField
      FieldName = 'cCSTPIS'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnAliqPIS: TBCDField
      FieldName = 'nAliqPIS'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalcCSTCOFINS: TStringField
      FieldName = 'cCSTCOFINS'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnAliqCOFINS: TBCDField
      FieldName = 'nAliqCOFINS'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalnValBasePIS: TBCDField
      FieldName = 'nValBasePIS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValPIS: TBCDField
      FieldName = 'nValPIS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseCOFINS: TBCDField
      FieldName = 'nValBaseCOFINS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValCOFINS: TBCDField
      FieldName = 'nValCOFINS'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalcEnqLegalIPI: TStringField
      FieldName = 'cEnqLegalIPI'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalcEXTIPI: TStringField
      FieldName = 'cEXTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnValIcentivoFiscal: TBCDField
      FieldName = 'nValIcentivoFiscal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnAliqICMSInternaOrigem: TBCDField
      FieldName = 'nAliqICMSInternaOrigem'
      Precision = 12
      Size = 2
    end
    object qryItemDoctoFiscalcCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 4
    end
    object qryItemDoctoFiscalnValImpostoAproxFed: TBCDField
      FieldName = 'nValImpostoAproxFed'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValImpostoAproxEst: TBCDField
      FieldName = 'nValImpostoAproxEst'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValImpostoAproxMun: TBCDField
      FieldName = 'nValImpostoAproxMun'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseICMSSubRet: TBCDField
      FieldName = 'nValBaseICMSSubRet'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMSSubRet: TBCDField
      FieldName = 'nValICMSSubRet'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseICMSDest: TBCDField
      FieldName = 'nValBaseICMSDest'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnAliqICMSDest: TBCDField
      FieldName = 'nAliqICMSDest'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnAliqICMSInter: TBCDField
      FieldName = 'nAliqICMSInter'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnAliqICMSPartDest: TBCDField
      FieldName = 'nAliqICMSPartDest'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMSPartDest: TBCDField
      FieldName = 'nValICMSPartDest'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalnValICMSPartOrig: TBCDField
      FieldName = 'nValICMSPartOrig'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryItemDoctoFiscalcFCI: TStringField
      FieldName = 'cFCI'
      Size = 50
    end
    object qryItemDoctoFiscalcCEST: TStringField
      FieldName = 'cCEST'
      Size = 7
    end
  end
  object qryPrazoDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT iParcela'
      '      ,dDtVenc'
      '      ,nValPagto'
      '  FROM PrazoDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ORDER BY iParcela')
    Left = 80
    Top = 72
    object qryPrazoDoctoFiscaliParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryPrazoDoctoFiscaldDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryPrazoDoctoFiscalnValPagto: TBCDField
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
  end
  object qryMsgDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cMsg'
      '  FROM MsgDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ORDER BY iOrdem')
    Left = 48
    Top = 104
    object qryMsgDoctoFiscalcMsg: TStringField
      FieldName = 'cMsg'
      Size = 300
    end
  end
  object qryDescontoTotal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      'SUM(nQtde * nValDesconto) nValDescontoItens'
      ' FROM ItemDoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ' ')
    Left = 144
    Top = 40
    object qryDescontoTotalnValDescontoItens: TBCDField
      FieldName = 'nValDescontoItens'
      ReadOnly = True
      Precision = 32
      Size = 8
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgTipoDescontoDanfe'
      '      ,nCdTabTipoEnquadTributario'
      '      ,cFlgIsentoCNPJCPF'
      '      ,CASE WHEN LEN(cCNPJCPF) = 11 '
      '            THEN 1 -- pessoa f'#237'sica'
      '            ELSE 2 -- pessoa jur'#237'dica'
      '       END  AS iTipoPessoa'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nCdTerceiro')
    Left = 112
    Top = 72
    object qryTerceirocFlgTipoDescontoDanfe: TIntegerField
      FieldName = 'cFlgTipoDescontoDanfe'
    end
    object qryTerceironCdTabTipoEnquadTributario: TIntegerField
      FieldName = 'nCdTabTipoEnquadTributario'
    end
    object qryTerceirocFlgIsentoCNPJCPF: TIntegerField
      FieldName = 'cFlgIsentoCNPJCPF'
    end
    object qryTerceiroiTipoPessoa: TIntegerField
      FieldName = 'iTipoPessoa'
      ReadOnly = True
    end
  end
  object qryStatusDPEC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,nCdStatusDPEC'
      '      ,CASE WHEN nCdStatusDPEC = 1 '
      '            THEN '#39'INICIADO'#39
      '            ELSE '#39'PARADO'#39
      '       END cNmStatusDPEC'
      '      ,cMotivoDPEC'
      '      ,dDtInicioDPEC'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 80
    Top = 104
    object qryStatusDPECnCdStatusDPEC: TIntegerField
      FieldName = 'nCdStatusDPEC'
    end
    object qryStatusDPECcMotivoDPEC: TStringField
      FieldName = 'cMotivoDPEC'
      Size = 50
    end
    object qryStatusDPECdDtInicioDPEC: TDateTimeField
      FieldName = 'dDtInicioDPEC'
    end
    object qryStatusDPECcNmStatusDPEC: TStringField
      FieldName = 'cNmStatusDPEC'
      ReadOnly = True
      Size = 8
    end
  end
  object qryCartaCorrecaoNFe: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM CartaCorrecaoNFe'
      ' WHERE nCdCartaCorrecaoNFe = :nPK')
    Left = 48
    Top = 8
    object qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField
      FieldName = 'nCdCartaCorrecaoNFe'
    end
    object qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryCartaCorrecaoNFedDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryCartaCorrecaoNFecCorrecao: TStringField
      FieldName = 'cCorrecao'
      Size = 1000
    end
    object qryCartaCorrecaoNFeiSeq: TIntegerField
      FieldName = 'iSeq'
    end
    object qryCartaCorrecaoNFeiLote: TIntegerField
      FieldName = 'iLote'
    end
    object qryCartaCorrecaoNFecCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 200
    end
    object qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCartaCorrecaoNFedDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryCartaCorrecaoNFecNrProtocoloCCe: TStringField
      FieldName = 'cNrProtocoloCCe'
      Size = 100
    end
    object qryCartaCorrecaoNFecXMLCCe: TMemoField
      FieldName = 'cXMLCCe'
      BlobType = ftMemo
    end
    object qryCartaCorrecaoNFecFlgAmbienteHom: TIntegerField
      FieldName = 'cFlgAmbienteHom'
    end
  end
  object qryTerceiroEmitente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      ' '
      'DECLARE @nCdEmpresa int'
      '       ,@nCdLoja    int'
      '       '
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @nCdLoja    = ISNULL(:nCdLoja,0)'
      ''
      'IF (@nCdLoja > 0)'
      'BEGIN'
      ''
      '   SELECT Terceiro.nCdTerceiro'
      #9'       ,Terceiro.nCdTabTipoEnquadTributario'
      '         ,ISNULL((SELECT TOP 1'
      '                         nCdEstado'
      '                    FROM Endereco'
      '                   WHERE Endereco.nCdTerceiro = Loja.nCdTerceiro'
      '                     AND nCdStatus            = 1'
      '                   ORDER BY nCdTipoEnd),0) as nCdEstado'
      #9'   FROM Loja'
      
        #9#9'      INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Loja.nCdTe' +
        'rceiro'
      #9'  WHERE nCdLoja = @nCdLoja'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '   SELECT Terceiro.nCdTerceiro'
      '         ,Terceiro.nCdTabTipoEnquadTributario'
      '         ,ISNULL((SELECT TOP 1'
      '                         nCdEstado'
      '                    FROM Endereco'
      
        '                   WHERE Endereco.nCdTerceiro = Empresa.nCdTerce' +
        'iroEmp'
      '                     AND nCdStatus            = 1'
      '                   ORDER BY nCdTipoEnd),0) as nCdEstado'
      '     FROM Empresa'
      
        '          INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Empresa.' +
        'nCdTerceiroEmp'
      '    WHERE nCdEmpresa = @nCdEmpresa'
      ''
      'END')
    Left = 112
    Top = 104
    object qryTerceiroEmitentenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroEmitentenCdTabTipoEnquadTributario: TIntegerField
      FieldName = 'nCdTabTipoEnquadTributario'
    end
    object qryTerceiroEmitentenCdEstado: TIntegerField
      FieldName = 'nCdEstado'
      ReadOnly = True
    end
  end
  object qryTabIBPT: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEstadoEmitente'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT TOP 1 '
      '       cChave'
      
        '      ,DATEDIFF(D,dbo.fn_OnlyDate(GETDATE()),dDtValidFinal) as i' +
        'DiasValidade'
      '  FROM TabIBPT'
      
        ' WHERE dbo.fn_OnlyDate(GETDATE()) BETWEEN dDtValidInicial AND dD' +
        'tValidFinal'
      '   AND nCdEstado = :nCdEstadoEmitente')
    Left = 80
    Top = 8
    object qryTabIBPTcChave: TStringField
      FieldName = 'cChave'
    end
    object qryTabIBPTiDiasValidade: TIntegerField
      FieldName = 'iDiasValidade'
      ReadOnly = True
    end
  end
  object qryDoctoFiscalRef: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = 'cFlgEntSai'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdDoctoFiscal int'
      '       ,@cFlgEntSai     char(1)'
      ''
      'SET @nCdDoctoFiscal = :nCdDoctoFiscal'
      'SET @cFlgEntSai     = :cFlgEntSai'
      ''
      
        'SELECT DISTINCT REPLACE(cDescricaoAdicional,'#39'REF NF: '#39','#39#39') as cC' +
        'haveNFe'
      '  FROM DoctoFiscal'
      
        '       INNER JOIN ItemDoctoFiscal ON ItemDoctoFiscal.nCdDoctoFis' +
        'cal = DoctoFiscal.nCdDoctoFiscal'
      ' WHERE DoctoFiscal.nCdDoctoFiscal = @nCdDoctoFiscal'
      ''
      '--'
      
        '-- se nota fiscal for de entrada, consulta documentos fiscais em' +
        'itidos anteriormente'
      '--'
      '--IF (@cFlgEntSai = '#39'E'#39')'
      '--BEGIN'
      ''
      '--    SELECT DISTINCT cChaveNFe'
      '--      FROM DoctoFiscal DoctoVenda'
      
        '--           INNER JOIN ItemDoctoFiscal ON ItemDoctoFiscal.nCdDo' +
        'ctoFiscal = DoctoVenda.nCdDoctoFiscal'
      '--     WHERE nCdTipoDoctoFiscal = 1   -- nota fiscal'
      '--       AND cFlgEntSai         = '#39'S'#39' -- sa'#237'da'
      
        '--       AND iNrDocto           IN(SELECT DISTINCT CAST(REPLACE(' +
        'cDescricaoAdicional,'#39'REF NF: '#39','#39#39') as int)'
      '--                                   FROM DoctoFiscal'
      
        '--                                        INNER JOIN ItemDoctoFi' +
        'scal ON ItemDoctoFiscal.nCdDoctoFiscal = DoctoFiscal.nCdDoctoFis' +
        'cal'
      
        '--                                  WHERE DoctoFiscal.nCdDoctoFi' +
        'scal = @nCdDoctoFiscal)'
      ''
      '--END'
      '--'
      
        '-- se nota fiscal for de sa'#237'da, consulta chave informada nos ite' +
        'ns do pedido'
      '--'
      '--ELSE'
      '--BEGIN'
      ''
      
        '--    SELECT DISTINCT REPLACE(cDescricaoAdicional,'#39'REF NF: '#39','#39#39')' +
        ' as cChaveNFe'
      '--      FROM DoctoFiscal'
      
        '--           INNER JOIN ItemDoctoFiscal ON ItemDoctoFiscal.nCdDo' +
        'ctoFiscal = DoctoFiscal.nCdDoctoFiscal'
      '--     WHERE DoctoFiscal.nCdDoctoFiscal = @nCdDoctoFiscal'
      ''
      '--END')
    Left = 112
    Top = 40
    object qryDoctoFiscalRefcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
  end
  object qryConsultaDadosCert: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmMunicipio'
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = 'cUF'
        DataType = ftString
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMunicipio int'
      '       ,@nCdEstado    int'
      '       ,@cUF          char(2)'
      ''
      'SELECT @nCdMunicipio = nCdMunicipio'
      '  FROM Municipio'
      
        ' WHERE dbo.fn_RemoveCaracterEspecial(cNmMunicipio) = dbo.fn_Remo' +
        'veCaracterEspecial(:cNmMunicipio)'
      ''
      'SELECT @nCdEstado = nCdEstado '
      '      ,@cUF       = cUF'
      '  FROM Estado'
      ' WHERE cUF = :cUF'
      ' '
      'SELECT @nCdMunicipio as nCdMunicipio'
      '      ,@nCdEstado    as nCdEstado'
      '      ,@cUF          as cUF')
    Left = 144
    Top = 72
    object qryConsultaDadosCertnCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
      ReadOnly = True
    end
    object qryConsultaDadosCertnCdEstado: TIntegerField
      FieldName = 'nCdEstado'
      ReadOnly = True
    end
    object qryConsultaDadosCertcUF: TStringField
      FieldName = 'cUF'
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 144
    Top = 104
  end
  object qryModeloDocumento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cDescricaoModeloDoc'
      '      ,nCdTabTipoModeloDoc'
      '  FROM ModeloDocumento'
      ' WHERE nCdModeloDocumento = :nPK')
    Left = 112
    Top = 8
    object qryModeloDocumentocDescricaoModeloDoc: TMemoField
      FieldName = 'cDescricaoModeloDoc'
      BlobType = ftMemo
    end
    object qryModeloDocumentonCdTabTipoModeloDoc: TIntegerField
      FieldName = 'nCdTabTipoModeloDoc'
    end
  end
  object ACBrNFe1: TACBrNFe
    MAIL = ACBrMail1
    Configuracoes.Geral.SSLLib = libCapicomDelphiSoap
    Configuracoes.Geral.SSLCryptLib = cryCapicom
    Configuracoes.Geral.SSLHttpLib = httpIndy
    Configuracoes.Geral.SSLXmlSignLib = xsMsXmlCapicom
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Geral.VersaoQRCode = veqr000
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Configuracoes.WebServices.SSLType = LT_TLSv1_2
    DANFE = ACBrNFeDANFeRL1
    Left = 16
    Top = 8
  end
  object ACBrMail1: TACBrMail
    Host = '127.0.0.1'
    Port = '25'
    SetSSL = False
    SetTLS = False
    Attempts = 3
    DefaultCharset = UTF_8
    IDECharset = CP1252
    Left = 16
    Top = 72
  end
  object ACBrNFeDANFeRL1: TACBrNFeDANFeRL
    ACBrNFe = ACBrNFe1
    MostrarPreview = True
    MostrarStatus = True
    TipoDANFE = tiRetrato
    NumCopias = 1
    ImprimeNomeFantasia = False
    ImprimirDescPorc = False
    ImprimirTotalLiquido = True
    MargemInferior = 0.700000000000000000
    MargemSuperior = 0.700000000000000000
    MargemEsquerda = 0.700000000000000000
    MargemDireita = 0.700000000000000000
    CasasDecimais.Formato = tdetInteger
    CasasDecimais._qCom = 4
    CasasDecimais._vUnCom = 4
    CasasDecimais._Mask_qCom = '###,###,###,##0.00'
    CasasDecimais._Mask_vUnCom = '###,###,###,##0.00'
    ExibirResumoCanhoto = False
    FormularioContinuo = False
    TamanhoFonte_DemaisCampos = 10
    ProdutosPorPagina = 0
    ImprimirDetalhamentoEspecifico = True
    NFeCancelada = False
    ImprimirItens = True
    ViaConsumidor = True
    TamanhoLogoHeight = 0
    TamanhoLogoWidth = 0
    RecuoEndereco = 0
    RecuoEmpresa = 0
    LogoemCima = False
    TamanhoFonteEndereco = 0
    RecuoLogo = 0
    LarguraCodProd = 54
    ExibirEAN = False
    QuebraLinhaEmDetalhamentoEspecifico = True
    ExibeCampoFatura = False
    ImprimirUnQtVlComercial = iuComercial
    ImprimirDadosDocReferenciados = True
    Left = 16
    Top = 40
  end
  object IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket
    SSLOptions.Method = sslvSSLv2
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 16
    Top = 104
  end
  object SP_GRAVA_ARQUIVO_XML_NFE: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GRAVA_ARQUIVO_XML_NFE'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cCaminho'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2147483647
        Value = Null
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 144
    Top = 8
  end
end
