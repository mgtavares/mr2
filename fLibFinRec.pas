unit fLibFinRec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, cxContainer, cxTextEdit;

type
  TfrmLibFinRec = class(TfrmProcesso_Padrao)
    qryRecebimento: TADOQuery;
    qryRecebimentonCdRecebimento: TIntegerField;
    qryRecebimentocNmEmpresa: TStringField;
    qryRecebimentocNmLoja: TStringField;
    qryRecebimentocNmTipoReceb: TStringField;
    qryRecebimentodDtreceb: TDateTimeField;
    qryRecebimentocNmTerceiro: TStringField;
    qryRecebimentocNrDocto: TStringField;
    qryRecebimentodDtDocto: TDateTimeField;
    qryRecebimentonValDocto: TBCDField;
    qryRecebimentodDtFech: TDateTimeField;
    qryRecebimentocOBS: TStringField;
    dsRecebimento: TDataSource;
    qryRecebimentocNmUsuario: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1cNmEmpresa: TcxGridDBColumn;
    cxGridDBTableView1cNmLoja: TcxGridDBColumn;
    cxGridDBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoReceb: TcxGridDBColumn;
    cxGridDBTableView1dDtreceb: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNrDocto: TcxGridDBColumn;
    cxGridDBTableView1dDtDocto: TcxGridDBColumn;
    cxGridDBTableView1nValDocto: TcxGridDBColumn;
    cxGridDBTableView1dDtFech: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cOBS: TcxGridDBColumn;
    qryRecebimentodDtPrimVencto: TDateTimeField;
    cxGridDBTableView1dDtPrimVencto: TcxGridDBColumn;
    qryRecebimentocNmFantasia: TStringField;
    cxGridDBTableView1cNmFantasia: TcxGridDBColumn;
    qryRecebimentonCdTabStatusReceb: TIntegerField;
    cxGridDBTableView1nCdTabStatusReceb: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    ImageList2: TImageList;
    CheckMostrarPendentes: TCheckBox;
    GroupBox2: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    Label37: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label1: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label2: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLibFinRec: TfrmLibFinRec;

implementation

uses fMenu, fLibFinRec_Compl;

{$R *.dfm}

procedure TfrmLibFinRec.FormShow(Sender: TObject);
begin
  inherited;

  qryRecebimento.Close ;
  qryRecebimento.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryRecebimento.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryRecebimento.Parameters.ParamByName('MostrarPendentes').Value := 0;
  qryRecebimento.Open ;

  if (qryRecebimento.eof) then
  begin
      ShowMessage('Nenhum recebimento pendente de autoriza��o.') ;
  end ;

end;

procedure TfrmLibFinRec.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryRecebimento.Close ;
  if (CheckMostrarPendentes.Checked) Then  qryRecebimento.Parameters.ParamByName('MostrarPendentes').Value := 1
  else                                     qryRecebimento.Parameters.ParamByName('MostrarPendentes').Value := 0;

  qryRecebimento.Open ;
end;

procedure TfrmLibFinRec.cxGridDBTableView1DblClick(Sender: TObject);
var
  objForm : TfrmLibFinRec_Compl ;
begin
  if (qryRecebimento.FieldByName('nCdTabStatusReceb').Value <> 4) then
  Begin
      ShowMessage('Recebimento ainda n�o est� dispon�vel para Aprova��o do Financeiro, por favor verifique.') ;
      abort;
  End;

  inherited;

  objForm := TfrmLibFinRec_Compl.Create(nil) ;
  objForm.nCdRecebimento := qryRecebimentonCdRecebimento.Value ;

  showForm( objForm , TRUE ) ;

  qryRecebimento.Close ;
  qryRecebimento.Open ;

end;

procedure TfrmLibFinRec.cxGridDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (ARecord.Values[14] = '1') then
          AStyle := frmMenu.LinhaVermelha ;

  if (ARecord.Values[14] = '2') then
          AStyle := frmMenu.LinhaAmarela ;

  if (ARecord.Values[14] = '3') then
          AStyle := frmMenu.LinhaAzul ;

  if (ARecord.Values[14] = '4') then
          AStyle := frmMenu.LinhaCinza ;


end;

initialization
    RegisterClass(TfrmLibFinRec) ;

end.
