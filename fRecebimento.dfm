inherited frmRecebimento: TfrmRecebimento
  Left = 107
  Top = 28
  Width = 1154
  Height = 693
  Align = alClient
  Caption = 'Recebimento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1146
    Height = 641
  end
  object Label1: TLabel [1]
    Left = 78
    Top = 37
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 193
    Top = 37
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 95
    Top = 61
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 24
    Top = 85
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Recebimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 238
    Top = 133
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Receb.'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 76
    Top = 157
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 58
    Top = 109
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 226
    Top = 109
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 48
    Top = 133
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 927
    Top = 181
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Produtos'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 891
    Top = 38
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label12: TLabel [12]
    Left = 817
    Top = 110
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Fechamento'
  end
  object Label13: TLabel [13]
    Left = 833
    Top = 134
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Fechamento'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [14]
    Left = 56
    Top = 229
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBEdit14
  end
  object Label29: TLabel [15]
    Left = 758
    Top = 181
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor ICMS ST'
    FocusControl = DBEdit31
  end
  object Label30: TLabel [16]
    Left = 60
    Top = 205
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Frete'
    FocusControl = DBEdit32
  end
  object Label32: TLabel [17]
    Left = 786
    Top = 205
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor IPI'
    FocusControl = DBEdit34
  end
  object Label34: TLabel [18]
    Left = 932
    Top = 205
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total NF'
    FocusControl = DBEdit36
  end
  object Label25: TLabel [19]
    Left = 367
    Top = 205
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    FocusControl = DBEdit24
  end
  object Label33: TLabel [20]
    Left = 208
    Top = 205
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Seguro'
    FocusControl = DBEdit33
  end
  object Label35: TLabel [21]
    Left = 535
    Top = 205
    Width = 115
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desp. Acess'#243'rias'
    FocusControl = DBEdit35
  end
  object Label36: TLabel [22]
    Left = 390
    Top = 181
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor ICMS'
    FocusControl = DBEdit37
  end
  object Label41: TLabel [23]
    Left = 222
    Top = 181
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Base ICMS'
    FocusControl = DBEdit42
  end
  object Label42: TLabel [24]
    Left = 583
    Top = 181
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Base ICMS ST'
    FocusControl = DBEdit43
  end
  object Label19: TLabel [25]
    Left = 335
    Top = 109
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo NF'
    FocusControl = DBEdit23
  end
  object Label27: TLabel [26]
    Left = 408
    Top = 133
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Chave NFe'
  end
  object Label15: TLabel [27]
    Left = 41
    Top = 181
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'U.F Origem NF'
    FocusControl = DBEdit12
  end
  object Label21: TLabel [28]
    Left = 834
    Top = 61
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Cadastro'
  end
  object Label22: TLabel [29]
    Left = 850
    Top = 85
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Cadastro'
  end
  inherited ToolBar2: TToolBar
    Width = 1146
    inherited ToolButton9: TToolButton
      Visible = False
    end
    object ToolButton3: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 864
      Top = 0
      Caption = '&Finalizar'
      ImageIndex = 8
      OnClick = ToolButton10Click
    end
    object ToolButton14: TToolButton
      Left = 952
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 10
      Style = tbsSeparator
    end
    object btnOpcao: TToolButton
      Left = 960
      Top = 0
      Caption = '&Op'#231#245'es'
      DropdownMenu = menuOpcao
      ImageIndex = 11
    end
  end
  object DBEdit1: TDBEdit [31]
    Tag = 1
    Left = 120
    Top = 31
    Width = 65
    Height = 19
    DataField = 'nCdRecebimento'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [32]
    Tag = 1
    Left = 240
    Top = 31
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit2Exit
  end
  object DBEdit3: TDBEdit [33]
    Left = 120
    Top = 55
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [34]
    Left = 120
    Top = 79
    Width = 65
    Height = 19
    DataField = 'nCdTipoReceb'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [35]
    Tag = 1
    Left = 301
    Top = 127
    Width = 73
    Height = 19
    DataField = 'dDtReceb'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit6: TDBEdit [36]
    Left = 120
    Top = 151
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 11
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [37]
    Left = 120
    Top = 103
    Width = 65
    Height = 19
    DataField = 'cNrDocto'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit7Exit
  end
  object DBEdit8: TDBEdit [38]
    Left = 253
    Top = 103
    Width = 36
    Height = 19
    DataField = 'cSerieDocto'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit9: TDBEdit [39]
    Left = 120
    Top = 127
    Width = 81
    Height = 19
    DataField = 'dDtDocto'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit10: TDBEdit [40]
    Left = 1008
    Top = 175
    Width = 81
    Height = 19
    DataField = 'nValDocto'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit13: TDBEdit [41]
    Tag = 1
    Left = 926
    Top = 128
    Width = 163
    Height = 19
    DataField = 'dDtFech'
    DataSource = dsMaster
    TabOrder = 26
  end
  object DBEdit14: TDBEdit [42]
    Left = 120
    Top = 223
    Width = 473
    Height = 19
    DataField = 'cOBS'
    DataSource = dsMaster
    TabOrder = 24
  end
  object DBEdit15: TDBEdit [43]
    Tag = 1
    Left = 308
    Top = 31
    Width = 465
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 27
  end
  object DBEdit16: TDBEdit [44]
    Tag = 1
    Left = 188
    Top = 55
    Width = 585
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource2
    TabOrder = 28
  end
  object DBEdit17: TDBEdit [45]
    Tag = 1
    Left = 188
    Top = 79
    Width = 585
    Height = 19
    DataField = 'cNmTipoReceb'
    DataSource = DataSource3
    TabOrder = 29
  end
  object DBEdit18: TDBEdit [46]
    Tag = 1
    Left = 188
    Top = 151
    Width = 585
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 25
  end
  object DBEdit19: TDBEdit [47]
    Tag = 1
    Left = 926
    Top = 32
    Width = 163
    Height = 19
    DataField = 'cNmTabStatusReceb'
    DataSource = DataSource5
    TabOrder = 30
  end
  object DBEdit11: TDBEdit [48]
    Tag = 1
    Left = 926
    Top = 104
    Width = 163
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = DataSource6
    TabOrder = 31
  end
  object cxPageControl1: TcxPageControl [49]
    Left = 8
    Top = 283
    Width = 1121
    Height = 358
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 32
    OnChange = cxPageControl1Change
    ClientRectBottom = 358
    ClientRectRight = 1121
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens Rece&bidos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 41
        Width = 1121
        Height = 294
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemRecebimento
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Consolas'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentFont = False
        PopupMenu = PopupMenu1
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Consolas'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh1ColExit
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        OnEnter = DBGridEh1Enter
        OnKeyPress = DBGridEh1KeyPress
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            Title.Caption = 'Produto'
            Width = 54
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 39
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 145
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <
              item
                FieldName = 'nQtde'
                ValueType = fvtSum
              end>
            Title.Caption = 'Item|Qtde'
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'cNCM'
            Footers = <>
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'cCdST'
            Footers = <>
            Title.Caption = 'CST CSOSN'
            Width = 45
          end
          item
            EditButtons = <>
            FieldName = 'cCFOPNF'
            Footers = <>
            Width = 37
          end
          item
            EditButtons = <>
            FieldName = 'cCFOP'
            Footers = <>
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitarioEsp'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Tag = 1
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footers = <>
            ReadOnly = True
            Width = 56
          end
          item
            EditButtons = <>
            FieldName = 'nPercDesconto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footer.Value = ' '
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 38
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalLiq'
            Footers = <>
            ReadOnly = True
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'nPercRedBaseCalcICMS'
            Footers = <>
            Title.Caption = 'ICMS|% Red. Base'
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nBaseCalcICMS'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'ICMS|Base C'#225'lculo'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'nPercAliqICMS'
            Footers = <>
            Title.Caption = 'ICMS|% Al'#237'q.'
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValICMS'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'ICMS|Valor'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoICMS'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'ICMS|Tipo'
            Visible = False
            Width = 56
          end
          item
            EditButtons = <>
            FieldName = 'nPercBaseCalcIPI'
            Footers = <>
            Title.Caption = 'IPI|% Red. Base'
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseIPI'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'IPI|Base C'#225'lculo'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'nPercIPI'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Title.Caption = 'IPI|% Al'#237'q.'
            Width = 38
          end
          item
            EditButtons = <>
            FieldName = 'nValIPI'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'IPI|Valor'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'cCdSTIPI'
            Footers = <>
            Title.Caption = 'IPI|CST'
            Width = 26
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoIPI'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'IPI|Tipo'
            Visible = False
            Width = 54
          end
          item
            EditButtons = <>
            FieldName = 'nPercIVA'
            Footers = <>
            Title.Caption = 'Substitui'#231#227'o Tribut'#225'ria|% IVA'
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'nAliqICMSInterna'
            Footers = <>
            Title.Caption = 'Substitui'#231#227'o Tribut'#225'ria|Aliq. ICMS Interna'
          end
          item
            EditButtons = <>
            FieldName = 'nPercRedBaseCalcSubTrib'
            Footers = <>
            Title.Caption = 'Substitui'#231#227'o Tribut'#225'ria|% Red. B.C.'
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseCalcSubTrib'
            Footers = <>
            Title.Caption = 'Substitui'#231#227'o Tribut'#225'ria|Base C'#225'lculo'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'nValICMSSub'
            Footers = <>
            Title.Caption = 'Substitui'#231#227'o Tribut'#225'ria|Valor'
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'nValAcessorias'
            Footers = <>
            Title.Caption = 'Despesas Adcionais|Desp. Acess'#243'rias'
          end
          item
            EditButtons = <>
            FieldName = 'nValFrete'
            Footers = <>
            Title.Caption = 'Despesas Adcionais|Valor Frete'
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nValSeguro'
            Footers = <>
            Title.Caption = 'Despesas Adcionais|Valor Seguro'
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'cFlgImportacao'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'N'#195'O'
              'SIM')
            Title.Caption = 'Outras Informa'#231#245'es|Importa'#231#227'o?'
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'nValAliquotaII'
            Footers = <>
            Title.Caption = 'Outras Informa'#231#245'es|Aliquota II'
          end
          item
            EditButtons = <>
            FieldName = 'cFlgGeraLivroFiscal'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'N'#195'O'
              'SIM')
            Title.Caption = 'Outras Informa'#231#245'es|Gera Livro Fiscal?'
          end
          item
            EditButtons = <>
            FieldName = 'cFlgGeraCreditoICMS'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'N'#195'O'
              'SIM')
            Title.Caption = 'Outras Informa'#231#245'es|Credita ICMS?'
          end
          item
            EditButtons = <>
            FieldName = 'cFlgGeraCreditoIPI'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'N'#195'O'
              'SIM')
            Title.Caption = 'Outras Informa'#231#245'es|Credita IPI?'
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabStatusItemPed'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 30
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusItemPed'
            Footers = <>
            ReadOnly = True
            Width = 68
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1121
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object Label16: TLabel
          Tag = 2
          Left = 960
          Top = 20
          Width = 146
          Height = 13
          Caption = 'Abatimento Desconsiderado'
        end
        object Label17: TLabel
          Tag = 2
          Left = 808
          Top = 20
          Width = 109
          Height = 13
          Caption = 'Item com Diverg'#234'ncia'
        end
        object cxTextEdit3: TcxTextEdit
          Left = 928
          Top = 12
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clRed
          TabOrder = 0
        end
        object cxTextEdit4: TcxTextEdit
          Left = 776
          Top = 12
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = 8454143
          TabOrder = 1
        end
        object cxButton8: TcxButton
          Left = 2
          Top = 4
          Width = 175
          Height = 33
          Caption = 'Visualizar / Alterar Grade'
          TabOrder = 2
          OnClick = cxButton8Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F2E2E2E2E2E2
            E2E2E2E2E2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFF2F2F2E2E2E27F7F7F4444445555559B9B9BFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E27D7D7D4E4E4E9393
            938E8E8E636363FFFFFFFFFFFFFFFFFFF2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2
            E2E2E2E2E27B7B7B4F4F4F9191918F8F8FACACAC565656FFFFFFFFFFFFF2F2F2
            E2E2E27171714141415C5C5C5C5C5C3E3E3E3B3B3B3D3D3D949494969696ACAC
            AC636363949494FFFFFFF2F2F2E2E2E2494949A0A0A0E3E3E3EEEEEEEEEEEEE2
            E2E2ABABAB3A3A3A6A6A6AAFAFAF646464949494FFFFFFFFFFFFE2E2E24C4C4C
            CECECEECEAE6E3D9BCDCCC9BDCCC9BE3D9BCECEAE6CBCBCB4141415252529494
            94FFFFFFFFFFFFFFFFFF8787879A9A9AEDEBE7DBCA95DAC586DBC78ADCC88BDB
            C78ADDCC99ECEAE6ABABAB505050FFFFFFFFFFFFFFFFFFFFFFFF4F4F4FE8E8E8
            E4DBBEDBC688DECC91E1D099E2D29CE1D099DECC91E4D9BAF2F2F2474747FFFF
            FFFFFFFFFFFFFFFFFFFF5F5F5FF2F2F2DFCFA0DECC91E2D4A0E6DAACE7DDB1E6
            DAACE2D4A0E2D4A8F1F1F1737373FFFFFFFFFFFFFFFFFFFFFFFF606060F4F4F4
            E0D1A3E1D099E6DAACEBE5C0EEEAC9EBE5C0E6DAACE4D8AEF2F2F2777777FFFF
            FFFFFFFFFFFFFFFFFFFF555555EDEDEDE8DFC4E2D29CE7DDB1EEEAC9F3F3DBEE
            EAC9E7DDB1E9E1C6F9F9F94D4D4DFFFFFFFFFFFFFFFFFFFFFFFF8D8D8DB0B0B0
            F6F6F6E5D8AFE6DAACEBE5C0EEEAC9EBE5C0E8DEB9F4F3EFBABABA838383FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFF5A5A5ADDDDDDF9F9F9EDE8D4ECE5CBECE7CEEE
            EAD8F8F8F8DBDBDB5A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            5B5B5BBCBCBCF4F4F4FDFDFDFDFDFDFFFFFFBCBCBC5B5B5BFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686865A5A5A7C7C7C7C7C7C51
            5151868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.Kind = lfOffice11
        end
        object btnConferencia: TcxButton
          Left = 177
          Top = 4
          Width = 175
          Height = 33
          Caption = 'Confer'#234'ncia'
          TabOrder = 3
          TabStop = False
          OnClick = btnConferenciaClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
            7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
            C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
            6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
            FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
            AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
            6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
            3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
            FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
            8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
            6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btnVisualizaConfe: TcxButton
          Left = 352
          Top = 4
          Width = 175
          Height = 33
          Caption = 'Visualizar Itens Confer'#234'ncia'
          TabOrder = 4
          TabStop = False
          OnClick = btnVisualizaConfeClick
          Glyph.Data = {
            06030000424D060300000000000036000000280000000F0000000F0000000100
            180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFF00000000000000000000000000000000000000
            0000000000000000000000000000000000000000FFFFFF000000FFFFFF000000
            F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
            F4000000FFFFFF000000FFFFFF000000F4F4F4CCCBCAD5D4D4DCDBDB936700EB
            EBEAEBEBEAECECEBECEBEBEAE9E9F4F4F4000000FFFFFF000000FFFFFF000000
            F4F4F4C6C4C2E9E9E9936700936700936700F6F6F6F6F6F6F6F6F6E6E6E6F4F4
            F4000000FFFFFF000000FFFFFF000000F4F4F4C2BFBC93670093670093670093
            6700936700F5F5F5F4F4F4E2E2E1F4F4F4000000FFFFFF000000FFFFFF000000
            F4F4F4936700936700936700EAEAEA936700936700936700F2F2F2DEDDDCF4F4
            F4000000FFFFFF000000FFFFFF000000F4F4F4BCB7B2936700DFDCDAE3E1E0E8
            E8E8936700936700936700D6D5D4F4F4F4000000FFFFFF000000FFFFFF000000
            F4F4F4B9B3AED7D1CDD9D4D0DBD7D4DFDDDBE3E2E1936700936700936700F4F4
            F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AED5CFCBD5CFCBD6D1CDDA
            D5D2DEDBD8E1DFDD936700936700936700000000FFFFFF000000FFFFFF000000
            F4F4F4B9B3AED5CFCBD5CFCBD5CFCBD5CFCBD8D3D0DCD8D5DFDDDB936700F4F4
            F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AEB9B3AEB9B3AEB9B3AEB9
            B3AEB9B3AEBAB4AFBDB9B4C1BEBBF4F4F4000000FFFFFF000000FFFFFF000000
            F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
            F4000000FFFFFF000000FFFFFF00000000000000000000000000000000000000
            0000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Digita'#231#227'o por c'#243'digo de barra'
      ImageIndex = 2
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1113
        Height = 330
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemBarra
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh2DrawColumnCell
        OnEnter = DBGridEh2Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemRecebimento'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdRecebimento'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemRecebimentoPai'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoItemPed'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cEANFornec'
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 242
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            ReadOnly = True
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            ReadOnly = True
            Width = 56
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitarioEsp'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitarioPed'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabStatusItemPed'
            Footers = <>
            ReadOnly = True
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusItemPed'
            Footers = <>
            ReadOnly = True
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'cFlgDiverg'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoDiverg'
            Footers = <>
            ReadOnly = True
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoDiverg'
            Footers = <>
            ReadOnly = True
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuarioAutorDiverg'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtAutorDiverg'
            Footers = <>
            ReadOnly = True
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Pa&rcelas para Pagamento'
      ImageIndex = 3
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 1113
        Height = 330
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsPrazoRecebimento
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh3DrawColumnCell
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRecebimento'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            Width = 95
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeDias'
            Footers = <>
            ReadOnly = True
            Width = 33
          end
          item
            EditButtons = <>
            FieldName = 'nValParcela'
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'nValOperacaoFin'
            Footers = <>
            ReadOnly = True
            Width = 121
          end
          item
            EditButtons = <>
            FieldName = 'nValPagto'
            Footers = <>
            ReadOnly = True
            Width = 99
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
            Width = 125
          end
          item
            EditButtons = <>
            FieldName = 'cFlgDocCobranca'
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            PickList.Strings = (
              'Sim'
              'N'#227'o')
            Width = 183
          end
          item
            EditButtons = <>
            FieldName = 'cCodBarra'
            Footers = <>
            Width = 202
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxButton2: TcxButton
        Left = 8
        Top = 88
        Width = 113
        Height = 33
        Caption = 'Gerar Parcelas'
        TabOrder = 1
        Visible = False
        Glyph.Data = {
          72020000424D720200000000000036000000280000000E0000000D0000000100
          1800000000003C020000000000000000000000000000000000008C9C9C000000
          0000000000000000000000000000000000000000000000000000000000000000
          008C9C9C00000000000000840000840000840000840000840000840000840000
          8400008400008400008400008400000000000000009C9CFF0000FF0000FF0000
          FFEFFFFF0000840000840000840000840000FF0000FF00008400000000000000
          009C9CFF0000FF0000FFEFFFFFEFFFFFEFFFFFEFFFFF00008400008400008400
          008400008400000000000000009C9CFF0000FFEFFFFFEFFFFFEFFFFFEFFFFFEF
          FFFFEFFFFFEFFFFF0000840000FF00008400000000000000009C9CFF0000FF00
          00FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF000084000000
          00000000009C9CFF0000FF0000FF0000FF0000FF0000FFEFFFFFEFFFFF000084
          0000840000FF00008400000000000000009C9CFF0000FF0000FFEFFFFFEFFFFF
          EFFFFFEFFFFFEFFFFFEFFFFF0000FF0000FF00008400000000000000009C9CFF
          0000FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF0000FF0000
          8400000000000000009C9CFF0000FF0000FF0000FF0000FFEFFFFFEFFFFF0000
          840000840000FF0000FF00008400000000000000009C9CFF0000FF0000FF0000
          FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF00008400000000000000
          009C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C
          9CFF9C9CFF00000000008C9C9C00000000000000000000000000000000000000
          00000000000000000000000000000000008C9C9C0000}
        LookAndFeel.NativeStyle = True
      end
      object cxButton3: TcxButton
        Left = 120
        Top = 88
        Width = 113
        Height = 33
        Caption = 'Excluir Parcelas'
        TabOrder = 2
        Visible = False
        OnClick = cxButton3Click
        Glyph.Data = {
          06030000424D060300000000000036000000280000000F0000000F0000000100
          180000000000D002000000000000000000000000000000000000BDBDBDBDBDBD
          BDBDBDBDBDBDC6948C000000000000000000000000000000C6948CC6948C0000
          00000000BDBDBD000000BDBDBDBDBDBDBDBDBD000000000000000000EFFFFF00
          0000000084000084000000000000EFFFFF000000C6948C000000BDBDBDBDBDBD
          000000000084000084000000EFFFFFEFFFFF000000000084000000EFFFFFEFFF
          FF000000C6948C000000BDBDBD0000000000842100C62100C6000000000000EF
          FFFFEFFFFF000000EFFFFFEFFFFF000000000000C6948C000000BDBDBD000000
          2100C60000842100C62100C62100C6000000EFFFFFEFFFFFEFFFFF0000000000
          00000000C6948C0000000000000000842100C62100C62100C60000FF2100C600
          0000EFFFFFEFFFFFEFFFFF0000000000840000840000000000000000002100C6
          0000FF2100C60000FF0000FF000000EFFFFFEFFFFF000000EFFFFFEFFFFF0000
          000000000000000000000000000000FF2100C60000FF0000FF000000EFFFFFEF
          FFFF2100C60000FF000000EFFFFFEFFFFF0000000000000000000000002100C6
          0000FF0000FF0000FF000000EFFFFF0000000000FF2100C60000FF000000EFFF
          FF0000000000000000000000002100C60000FF0000FF0000FF00000000000000
          00FF0000FF0000FF2100C60000FF000000000000000000000000BDBDBD000000
          0000FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF0000FF2100C62100
          C6000000BDBDBD000000BDBDBD0000000000FF0000FF0000FFEFFFFFEFFFFF00
          00FF0000FF0000FF2100C60000FF2100C6000000BDBDBD000000BDBDBDBDBDBD
          0000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF2100C60000
          00BDBDBDBDBDBD000000BDBDBDBDBDBDBDBDBD0000000000000000FF0000FF00
          00FF0000FF0000FF000000000000BDBDBDBDBDBDBDBDBD000000BDBDBDBDBDBD
          BDBDBDBDBDBDBDBDBD000000000000000000000000000000BDBDBDBDBDBDBDBD
          BDBDBDBDBDBDBD000000}
        LookAndFeel.NativeStyle = True
      end
    end
    object TabCentroCusto: TcxTabSheet
      Caption = 'Centro Custo'
      ImageIndex = 4
      object Image3: TImage
        Left = 0
        Top = 0
        Width = 1113
        Height = 330
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label26: TLabel
        Left = 16
        Top = 14
        Width = 84
        Height = 13
        Caption = 'Item sem Pedido'
      end
      object DBComboItens: TDBLookupComboboxEh
        Left = 104
        Top = 8
        Width = 625
        Height = 19
        AutoSelect = False
        EditButtons = <>
        KeyField = 'nCdItemRecebimento'
        ListField = 'cNmItem'
        ListSource = dsItemCC
        TabOrder = 0
        Visible = True
        OnChange = DBComboItensChange
      end
      object DBGridEh8: TDBGridEh
        Left = 8
        Top = 36
        Width = 721
        Height = 197
        DataGrouping.GroupLevels = <>
        DataSource = dsCCItemRecebimento
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh8KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemRecebimento'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            Width = 494
          end
          item
            EditButtons = <>
            FieldName = 'nPercent'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxButton4: TcxButton
        Left = 736
        Top = 2
        Width = 33
        Height = 25
        Hint = 'Copiar o centro de custo deste item para todos os itens'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = cxButton4Click
        Glyph.Data = {
          66060000424D6606000000000000360000002800000017000000160000000100
          1800000000003006000000000000000000000000000000000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0CB8C44A654006336006336
          00A65400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A37600
          D9A77DCB8C44CB8C44CB8C44633600F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0A37600D9A77DD9A77DCB8C44CB8C44A37600A65400F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0A37600D9A77DD9A77DD9A77DCB8C44A37600A6
          5400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A37600D9A77DD9A77DD9A7
          7DD9A77DA37600A65400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          A37600FFFFCCFFFFCCFFFFCCFFFFCCA37600F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0A37600A37600A37600A37600CB8C44F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFFF0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFF001FFF001FFFF0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0001FFFF0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000}
        LookAndFeel.NativeStyle = True
      end
    end
    object TabLote: TcxTabSheet
      Caption = 'Lotes/ Seriais'
      ImageIndex = 5
      object Image4: TImage
        Left = 0
        Top = 0
        Width = 1113
        Height = 330
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 35
        Width = 1083
        Height = 272
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsProdLote
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh4DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Title.Caption = 'Produto|C'#243'd'
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            Title.Caption = 'Produto|Descri'#231#227'o'
            Width = 558
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
            Title.Caption = 'Quantidade|Digitada'
            Width = 96
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btRegistroLoteSerial: TcxButton
        Left = 0
        Top = 1
        Width = 153
        Height = 33
        Caption = 'Registro Lote/Serial'
        TabOrder = 1
        OnClick = btRegistroLoteSerialClick
        Glyph.Data = {
          36030000424D360300000000000036000000280000000F000000100000000100
          18000000000000030000120B0000120B00000000000000000000CFCFCFCFCFCF
          CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
          CFDADADADADADA00000090490A90490A90490A90490A90490A90490A90490A90
          490A90490A90490A90490A90490A90490ADADADADADADA000000944D0EDAA525
          CC8D21CC8D21CC8D21DAA52598551ADAA525CC8D21CC8D21CC8D21DAA525944D
          0EDADADADADADA000000975212DBA82FCE912ACE912ACE912ADBA82F9D5920DB
          A82FCE912ACE912ACE912ADBA82F975212DADADADADADA0000009B5618DCAB39
          D09533D09533D09533DCAB39A05E26DCAB39D09533D09533D09533DCAB399B56
          18DADADADADADA000000A05C1DDEAE43D2993CD2993CD2993CDEAE43A6642CDE
          AE43D2993CD2993CD2993CDEAE43A05C1DDADADADADADA000000A46223FAE062
          FAE062FAE062FAE062FAE062A96A33FAE062FAE062FAE062FAE062FAE062A462
          23DADADADADADA000000AA672AAF7039AF7039AF7039AF7039AF7039A36021AA
          672AAA672AAA672AAA672AAA672AAA672ADADADADADADA000000AE6D31E3B865
          D9A55AD9A55AD9A55AE3B865AE6D31EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
          EBEBEBEBEBEBEB000000B37338E4BB6FDBA963DBA963DBA963E4BB6FB37338EB
          EBEBEBEBEBCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF000000B7793FE6BE79
          DDAD6CDDAD6CDDAD6CE6BE79B7793FEBEBEBEBEBEB0043A40043A40043A40043
          A40043A40043A4000000BB7F46E7C182DFB174DFB174DFB174E7C182BB7F46EB
          EBEBEBEBEB004CBA2980FF0B6FFF0B6FFF2980FF004CBA000000BF844CFBE7AC
          FBE7ACFBE7ACFBE7ACFBE7ACBF844CEBEBEBEBEBEB0056D24390FF2B81FF2B81
          FF4390FF0056D2000000C28852C28852C28852C28852C28852C28852C28852EB
          EBEBEBEBEB0060EC599DFF4792FF4792FF599DFF0060EC000000EBEBEBEBEBEB
          EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB056BFFA1C7FFA1C7FFA1C7
          FFA1C7FF056BFF000000EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
          EBEBEBEBEB1977FF1977FF1977FF1977FF1977FF1977FF000000}
        LookAndFeel.NativeStyle = True
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = 'Insp. Qualidade'
      ImageIndex = 6
      object Image5: TImage
        Left = 0
        Top = 0
        Width = 1091
        Height = 313
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label38: TLabel
        Left = 16
        Top = 46
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Data inspe'#231#227'o'
        FocusControl = DBEdit38
      end
      object Label39: TLabel
        Left = 49
        Top = 70
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usu'#225'rio'
        FocusControl = DBEdit39
      end
      object Label40: TLabel
        Left = 29
        Top = 94
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observa'#231#227'o'
        FocusControl = DBEdit40
      end
      object DBCheckBox1: TDBCheckBox
        Tag = 1
        Left = 16
        Top = 16
        Width = 193
        Height = 17
        Caption = 'Requer Inspe'#231#227'o de Qualidade'
        Enabled = False
        TabOrder = 0
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox2: TDBCheckBox
        Tag = 1
        Left = 216
        Top = 16
        Width = 153
        Height = 17
        Caption = 'Liberado pela inspe'#231#227'o'
        Enabled = False
        TabOrder = 1
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit38: TDBEdit
        Tag = 1
        Left = 96
        Top = 40
        Width = 153
        Height = 19
        DataField = 'dDtAnaliseQualidade'
        DataSource = dsMaster
        TabOrder = 2
      end
      object DBEdit39: TDBEdit
        Tag = 1
        Left = 96
        Top = 64
        Width = 65
        Height = 19
        DataField = 'nCdUsuarioAnaliseQualidade'
        DataSource = dsMaster
        TabOrder = 3
      end
      object DBEdit40: TDBEdit
        Tag = 1
        Left = 96
        Top = 88
        Width = 650
        Height = 19
        DataField = 'cOBSQualidade'
        DataSource = dsMaster
        TabOrder = 4
      end
      object DBEdit41: TDBEdit
        Tag = 1
        Left = 168
        Top = 64
        Width = 201
        Height = 19
        DataField = 'cNmUsuario'
        DataSource = DataSource7
        TabOrder = 5
      end
    end
    object tabItemSemPedido: TcxTabSheet
      Caption = 'Itens Sem Pedido'
      ImageIndex = 6
      OnShow = tabItemSemPedidoShow
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 1113
        Height = 41
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label18: TLabel
        Left = 10
        Top = 14
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Pedido'
      end
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 41
        Width = 1113
        Height = 289
        Align = alBottom
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItemRecebidoSemPedido
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Consolas'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 5
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentFont = False
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Consolas'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh5KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemRecebimento'
            Footers = <>
            ReadOnly = True
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            ReadOnly = True
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 421
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Itens Recebidos Sem Pedido|Qtde. Rec.'
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoPedidoRecebido'
            Footers = <>
            Width = 95
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoPedido'
            Footers = <>
            ReadOnly = True
            Width = 300
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object Label20: TLabel
            Left = 552
            Top = 24
            Width = 188
            Height = 13
            Caption = 'Tipo de Pedido de Entrada|Descri'#231#227'o'
            FocusControl = DBEdit20
          end
          object DBEdit20: TDBEdit
            Left = 552
            Top = 40
            Width = 650
            Height = 19
            DataField = 'cNmTipoPedido'
            DataSource = dsItemRecebidoSemPedido
            TabOrder = 0
          end
        end
      end
      object ER2LookupMaskEdit1: TER2LookupMaskEdit
        Left = 77
        Top = 10
        Width = 65
        Height = 19
        EditMask = '#########;1; '
        MaxLength = 9
        TabOrder = 1
        Text = '         '
        CodigoLookup = 102
        WhereAdicional.Strings = (
          'TipoPedido.nCdTabTipoPedido = 2')
        QueryLookup = qryTipoPedidoAux
      end
      object DBEdit21: TDBEdit
        Tag = 1
        Left = 145
        Top = 10
        Width = 269
        Height = 19
        DataField = 'cNmTipoPedido'
        DataSource = dsTipoPedidoAux
        ReadOnly = True
        TabOrder = 2
      end
      object btAplicaTipoPedido: TcxButton
        Left = 416
        Top = 8
        Width = 25
        Height = 25
        Hint = 'Aplicar tipo de pedido para todos os itens sem pedido'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btAplicaTipoPedidoClick
        Glyph.Data = {
          72020000424D720200000000000036000000280000000E0000000D0000000100
          1800000000003C020000000000000000000000000000000000008C9C9C000000
          0000000000000000000000000000000000000000000000000000000000000000
          008C9C9C00000000000000840000840000840000840000840000840000840000
          8400008400008400008400008400000000000000009C9CFF0000FF0000FF0000
          FFEFFFFF0000840000840000840000840000FF0000FF00008400000000000000
          009C9CFF0000FF0000FFEFFFFFEFFFFFEFFFFFEFFFFF00008400008400008400
          008400008400000000000000009C9CFF0000FFEFFFFFEFFFFFEFFFFFEFFFFFEF
          FFFFEFFFFFEFFFFF0000840000FF00008400000000000000009C9CFF0000FF00
          00FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF000084000000
          00000000009C9CFF0000FF0000FF0000FF0000FF0000FFEFFFFFEFFFFF000084
          0000840000FF00008400000000000000009C9CFF0000FF0000FFEFFFFFEFFFFF
          EFFFFFEFFFFFEFFFFFEFFFFF0000FF0000FF00008400000000000000009C9CFF
          0000FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF0000FF0000
          8400000000000000009C9CFF0000FF0000FF0000FF0000FFEFFFFFEFFFFF0000
          840000840000FF0000FF00008400000000000000009C9CFF0000FF0000FF0000
          FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF00008400000000000000
          009C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C
          9CFF9C9CFF00000000008C9C9C00000000000000000000000000000000000000
          00000000000000000000000000000000008C9C9C0000}
        LookAndFeel.NativeStyle = True
      end
    end
  end
  object cxButton1: TcxButton [50]
    Left = 8
    Top = 247
    Width = 139
    Height = 33
    Caption = 'Consultar Pe&didos'
    TabOrder = 33
    TabStop = False
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D360300000000000036000000280000000F000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
      393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
      00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
      F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
      83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
      68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
      FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
      9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
      8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
      D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
      B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
      5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
      A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
      9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
      488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
      FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
      FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
      FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit31: TDBEdit [51]
    Left = 832
    Top = 175
    Width = 81
    Height = 19
    DataField = 'nValICMSSub'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit32: TDBEdit [52]
    Left = 120
    Top = 199
    Width = 81
    Height = 19
    DataField = 'nValFrete'
    DataSource = dsMaster
    TabOrder = 18
  end
  object DBEdit34: TDBEdit [53]
    Left = 832
    Top = 199
    Width = 81
    Height = 19
    DataField = 'nValIPI'
    DataSource = dsMaster
    TabOrder = 22
  end
  object DBEdit36: TDBEdit [54]
    Left = 1008
    Top = 199
    Width = 81
    Height = 19
    Color = clWhite
    DataField = 'nValTotalNF'
    DataSource = dsMaster
    TabOrder = 23
  end
  object DBEdit24: TDBEdit [55]
    Left = 450
    Top = 199
    Width = 81
    Height = 19
    DataField = 'nValDescontoNF'
    DataSource = dsMaster
    TabOrder = 20
  end
  object DBEdit33: TDBEdit [56]
    Left = 277
    Top = 199
    Width = 81
    Height = 19
    DataField = 'nValSeguroNF'
    DataSource = dsMaster
    TabOrder = 19
  end
  object DBEdit35: TDBEdit [57]
    Left = 654
    Top = 199
    Width = 81
    Height = 19
    Color = clWhite
    DataField = 'nValDespesas'
    DataSource = dsMaster
    TabOrder = 21
  end
  object DBEdit37: TDBEdit [58]
    Left = 450
    Top = 175
    Width = 81
    Height = 19
    DataField = 'nValICMS'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit42: TDBEdit [59]
    Left = 277
    Top = 175
    Width = 81
    Height = 19
    DataField = 'nValBaseICMS'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit43: TDBEdit [60]
    Left = 654
    Top = 175
    Width = 81
    Height = 19
    DataField = 'nValBaseCalcICMSSub'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit23: TDBEdit [61]
    Left = 396
    Top = 103
    Width = 26
    Height = 19
    DataField = 'cModeloNF'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit29: TDBEdit [62]
    Left = 466
    Top = 127
    Width = 307
    Height = 19
    DataField = 'cChaveNFe'
    DataSource = dsMaster
    TabOrder = 10
  end
  object cxButton7: TcxButton [63]
    Left = 146
    Top = 247
    Width = 155
    Height = 33
    Caption = 'Ler Arquivo NFe'
    TabOrder = 34
    TabStop = False
    OnClick = cxButton7Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      E2E2E2CBCBCBC9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9C9
      C9C9C9C9CCCCCCE2E2E2FFFFFFFFFFFFCBCBCBF9F9F9FCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF9F9F9CCCCCCFFFFFFFEFEFE
      C9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCFCFCC9C9C9FFFFFFFEFEFE
      C9C2BFE19565E09055FCFCFCE39A59E39967F8F1EEFAFAFAFAFAFAFAFAFAFAFA
      FAFAFAFAFCFCFCC9C9C9FFFFFFFBF3EED7905CE5A365E1945DFCFCFCE5A061E7
      A868E39B67F8F0EBFAFAFAFAFAFAF8F8F8F8F8F8FCFCFCC9C9C9F2CFB4E6A25A
      E8AA6AE49C5FF8EBE5FCFCFCF8EDE5E5A262E8AA6AE39C57EDC5AFF9F9F9F9F9
      F9F8F8F8FCFCFCC9C9C9E8AC60ECB879E5A55AF4DFD1FCFCFCFCFCFCFCFCFCF5
      E1D2E6A65BE9B275E49E5AF8F5F4F6F6F6F6F6F6FCFCFCC9C9C9F4D5B7EAAE60
      EAB571E8A765F8EDE5FCFCFCF9EFE6E9AE66EAB571E8A85DEDC9B0F6F6F6F3F3
      F3F2F2F2FCFCFCC9C9C9FFFFFFFCF5EFDFA565EBB672E8A965FCFCFCEBB56AEC
      BB76EAB070F6EFEAF5F5F5F2F2F2EFEFEFEDEDEDFCFCFCC9C9C9FFFFFFFEFEFE
      CAC5C0EAB371E8AF62FCFCFCECB767EBB772F7F1ECF5F5F5F1F1F1ECECECEAEA
      EAE6E6E6FCFCFCC9C9C9FFFFFFFEFEFEC9C9C9FAF5F0F9F9F9F9F9F9F9F9F9F7
      F7F7F6F6F6F2F2F2EBEBEBFCFCFCFCFCFCFCFCFCFCFCFCC9C9C9FFFFFFFEFEFE
      C9C9C9FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0F0F0EAEAEAFCFCFCF6F6
      F6F4F4F4C5C5C5DFDFDFFFFFFFFFFFFFC9C9C9FBFBFBF4F4F4F5F5F5F5F5F5F5
      F5F5F1F1F1EFEFEFE9E9E9FCFCFCE7E7E7C3C3C3DFDFDFFDFDFDFFFFFFFFFFFF
      CCCCCCF8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8C2C2
      C2DFDFDFFDFDFDFFFFFFFFFFFFFFFFFFE3E3E3CCCCCCC9C9C9C9C9C9C9C9C9C9
      C9C9C9C9C9C9C9C9C9C9C9C9C9C9DFDFDFFDFDFDFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit12: TDBEdit [64]
    Left = 120
    Top = 175
    Width = 26
    Height = 19
    DataField = 'cUFOrigemNF'
    DataSource = dsMaster
    TabOrder = 12
    OnExit = DBEdit12Exit
  end
  object btCadProduto: TcxButton [65]
    Left = 454
    Top = 247
    Width = 153
    Height = 33
    Caption = 'Produto - ERP'
    TabOrder = 35
    TabStop = False
    OnClick = btCadProdutoClick
    Glyph.Data = {
      06030000424D060300000000000036000000280000000F0000000F0000000100
      180000000000D002000000000000000000000000000000000000CCCCCCCCCCCC
      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CCCCCCCCCCCCCC0000001B566D1B566D1B566D1B566D1B566D1B566D1B566D47
      94AF1B566D1B566D1B566D1B566D1B566D1B566D1B566D0000001D58708DD2EF
      8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF4794AF8DD2EF8DD2EF8DD2EF8DD2EF8DD2
      EF8DD2EF1D58700000001E5B738DD2EF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF47
      94AF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF1E5B73000000205E7782CEEE
      82CEEE82CEEE82CEEE82CEEE82CEEE4794AF82CEEE82CEEE82CEEE82CEEE82CE
      EE82CEEE205E7700000023627B76C9ED76C9ED76C9ED76C9ED76C9ED76C9ED47
      94AF76C9ED76C9ED76C9ED76C9ED76C9ED76C9ED23627B0000004794AF4794AF
      4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794
      AF4794AF4794AF0000002768826BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC47
      94AF6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC276882000000296C876BC6EC
      6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC4794AF6BC6EC6BC6ECDCEEF5DCEEF5DCEE
      F56BC6EC296C870000002B6F8B6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC47
      94AF6BC6EC6BC6ECEDF6FCEDF6FCEDF6FC6BC6EC2B6F8B0000002F758F6BC6EC
      6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC4794AF6BC6EC6BC6EC6BC6EC6BC6EC6BC6
      EC6BC6EC2F758F00000043829B69AEC769AEC769AEC769AEC769AEC769AEC769
      AEC769AEC769AEC769AEC769AEC769AEC769AEC743829B00000075A0B1AACFDD
      DBF3FCDBF3FDDBF3FDDBF3FDBEE3F169AEC7BEE3F1DBF3FDDBF3FDDBF3FDDBF3
      FCAACFDD75A0B1000000ADC7D26AA1B9CADEE6CADEE6CADEE6CADEE6B1D4E069
      AEC7B1D4E0CADEE6CADEE6CADEE6CADEE66AA1B9ADC7D2000000D7E6EB5593AA
      347D9A347D9A347D9A347D9A448DA969AEC7448DA9347D9A347D9A347D9A347D
      9A5593AAD7E6EB000000}
    LookAndFeel.NativeStyle = True
  end
  object btCadProdutoGrade: TcxButton [66]
    Left = 606
    Top = 247
    Width = 153
    Height = 33
    Caption = 'Produto - Grade'
    TabOrder = 36
    TabStop = False
    OnClick = btCadProdutoGradeClick
    Glyph.Data = {
      06030000424D060300000000000036000000280000000F0000000F0000000100
      180000000000D002000000000000000000000000000000000000CCCCCCCCCCCC
      CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      CCCCCCCCCCCCCC0000001B566D1B566D1B566D1B566D1B566D1B566D1B566D47
      94AF1B566D1B566D1B566D1B566D1B566D1B566D1B566D0000001D58708DD2EF
      8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF4794AF8DD2EF8DD2EF8DD2EF8DD2EF8DD2
      EF8DD2EF1D58700000001E5B738DD2EF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF47
      94AF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF8DD2EF1E5B73000000205E7782CEEE
      82CEEE82CEEE82CEEE82CEEE82CEEE4794AF82CEEE82CEEE82CEEE82CEEE82CE
      EE82CEEE205E7700000023627B76C9ED76C9ED76C9ED76C9ED76C9ED76C9ED47
      94AF76C9ED76C9ED76C9ED76C9ED76C9ED76C9ED23627B0000004794AF4794AF
      4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794AF4794
      AF4794AF4794AF0000002768826BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC47
      94AF6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC276882000000296C876BC6EC
      6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC4794AF6BC6EC6BC6ECDCEEF5DCEEF5DCEE
      F56BC6EC296C870000002B6F8B6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC47
      94AF6BC6EC6BC6ECEDF6FCEDF6FCEDF6FC6BC6EC2B6F8B0000002F758F6BC6EC
      6BC6EC6BC6EC6BC6EC6BC6EC6BC6EC4794AF6BC6EC6BC6EC6BC6EC6BC6EC6BC6
      EC6BC6EC2F758F00000043829B69AEC769AEC769AEC769AEC769AEC769AEC769
      AEC769AEC769AEC769AEC769AEC769AEC769AEC743829B00000075A0B1AACFDD
      DBF3FCDBF3FDDBF3FDDBF3FDBEE3F169AEC7BEE3F1DBF3FDDBF3FDDBF3FDDBF3
      FCAACFDD75A0B1000000ADC7D26AA1B9CADEE6CADEE6CADEE6CADEE6B1D4E069
      AEC7B1D4E0CADEE6CADEE6CADEE6CADEE66AA1B9ADC7D2000000D7E6EB5593AA
      347D9A347D9A347D9A347D9A448DA969AEC7448DA9347D9A347D9A347D9A347D
      9A5593AAD7E6EB000000}
    LookAndFeel.NativeStyle = True
  end
  object CheckcFlgDocEntregue: TDBCheckBox [67]
    Left = 465
    Top = 106
    Width = 104
    Height = 17
    Caption = 'Doc. Entregue'
    Color = 13086366
    DataField = 'cFlgDocEntregue'
    DataSource = dsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 37
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxButton9: TcxButton [68]
    Left = 300
    Top = 247
    Width = 155
    Height = 33
    Hint = 'Gerar Recebimento Transfer'#234'ncia Faturada'
    Caption = 'Gera Receb. Transf. Fat.'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 38
    TabStop = False
    OnClick = cxButton9Click
    Glyph.Data = {
      9E020000424D9E0200000000000036000000280000000E0000000E0000000100
      1800000000006802000000000000000000000000000000000000E8E8E8E5E5E5
      E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5E5
      E5E8E8E80000617F9F3A6DA13A6DA13A6DA13A6DA13A6DA13A6DA13A6DA13A6D
      A13A6DA13A6DA13A6DA13A6DA1617F9F0000477BB073A6DA6FA2D66FA2D66FA2
      D66FA2D66FA2D66FA2D66FA2D66FA2D66FA2D66FA2D673A6DA477BB000005489
      BE71A4D8689BCF689BCF689BCF689BCF689BCF689BCF689BCF689BCF689BCF68
      9BCF71A4D85489BE0000598EC274A7DB6B9ED26B9ED26B9ED26B9ED26B9ED26B
      9ED26B9ED26B9ED26B9ED26B9ED274A7DB598EC200005D91C677A9DD6DA0D46D
      A0D46DA0D46DA0D46DA0D46DA0D46DA0D46DA0D46DA0D46DA0D477A9DD5D91C6
      00006195CA79ACE070A3D770A3D770A3D732659870A3D732659870A3D770A3D7
      70A3D770A3D779ACE06195CA00006599CD7CAFE372A5D972A5D972A5D9326598
      32659832659832659872A5D972A5D972A5D97CAFE36599CD0000689CD07EB1E5
      75A8DC75A8DC75A8DC32659832659832659832659875A8DC75A8DC75A8DC7EB1
      E5689CD000006B9FD373A7DB6DA0D46396CA598CBF598CBF699CD0699CD0598C
      BF5689BC5E91C4689BCF70A3D76B9FD3000093B9E08EC3EF95C9F595C9F595C9
      F55B8EC16396CA6396CA5B8EC195C9F595C9F595C9F58EC3EF93B9E00000B8D1
      EB78AFE291C4F38EC1F18EC1F16598CC6DA0D46DA0D46598CC8EC1F18EC1F191
      C4F378AFE2B8D1EB0000DCE8F576ABDE94C8F491C4F391C4F36DA0D475A8DC75
      A8DC6DA0D491C4F391C4F394C8F476ABDEDCE8F50000FBFCFD98BEE475A8DB75
      A8DB75A8DB5486B95486B95486B95486B975A8DB75A8DB75A8DB98BEE4FBFCFD
      0000}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit22: TDBEdit [69]
    Tag = 1
    Left = 926
    Top = 56
    Width = 163
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuarioCad
    TabOrder = 39
  end
  object DBEdit25: TDBEdit [70]
    Tag = 1
    Left = 926
    Top = 80
    Width = 163
    Height = 19
    DataField = 'dDtCad'
    DataSource = dsMaster
    TabOrder = 40
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Recebimento'
      'WHERE nCdRecebimento = :nPK')
    Left = 208
    Top = 400
    object qryMasternCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryMasterdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNrDocto: TStringField
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryMastercSerieDocto: TStringField
      DisplayWidth = 3
      FieldName = 'cSerieDocto'
      FixedChar = True
      Size = 3
    end
    object qryMasterdDtDocto: TDateTimeField
      FieldName = 'dDtDocto'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternValDocto: TBCDField
      FieldName = 'nValDocto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdTabStatusReceb: TIntegerField
      FieldName = 'nCdTabStatusReceb'
    end
    object qryMasternCdUsuarioFech: TIntegerField
      FieldName = 'nCdUsuarioFech'
    end
    object qryMasterdDtFech: TDateTimeField
      FieldName = 'dDtFech'
    end
    object qryMastercOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryMasternValTotalNF: TBCDField
      FieldName = 'nValTotalNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValTitulo: TBCDField
      FieldName = 'nValTitulo'
      Precision = 12
      Size = 2
    end
    object qryMasternPercDescontoVencto: TBCDField
      FieldName = 'nPercDescontoVencto'
      Precision = 12
      Size = 2
    end
    object qryMasternPercAcrescimoVendor: TBCDField
      FieldName = 'nPercAcrescimoVendor'
      Precision = 12
      Size = 2
    end
    object qryMasternCdUsuarioAutorFinanc: TIntegerField
      FieldName = 'nCdUsuarioAutorFinanc'
    end
    object qryMasterdDtAutorFinanc: TDateTimeField
      FieldName = 'dDtAutorFinanc'
    end
    object qryMastercCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryMastercCNPJEmissor: TStringField
      FieldName = 'cCNPJEmissor'
      Size = 14
    end
    object qryMastercIEEmissor: TStringField
      FieldName = 'cIEEmissor'
      Size = 14
    end
    object qryMastercUFEmissor: TStringField
      FieldName = 'cUFEmissor'
      Size = 2
    end
    object qryMastercModeloNF: TStringField
      FieldName = 'cModeloNF'
      Size = 2
    end
    object qryMasternValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValICMS: TBCDField
      FieldName = 'nValICMS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValIsenta: TBCDField
      FieldName = 'nValIsenta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValOutras: TBCDField
      FieldName = 'nValOutras'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValIPI: TBCDField
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValTotalItensPag: TBCDField
      FieldName = 'nValTotalItensPag'
      Precision = 12
      Size = 2
    end
    object qryMasternValDescontoNF: TBCDField
      FieldName = 'nValDescontoNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMastercFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryMastercFlgParcelaAutom: TIntegerField
      FieldName = 'cFlgParcelaAutom'
    end
    object qryMasternValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDespesas: TBCDField
      FieldName = 'nValDespesas'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercDescProduto: TBCDField
      FieldName = 'nPercDescProduto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgAnaliseQualidade: TIntegerField
      FieldName = 'cFlgAnaliseQualidade'
    end
    object qryMastercFlgAnaliseQualidadeOK: TIntegerField
      FieldName = 'cFlgAnaliseQualidadeOK'
    end
    object qryMasterdDtAnaliseQualidade: TDateTimeField
      FieldName = 'dDtAnaliseQualidade'
    end
    object qryMasternCdUsuarioAnaliseQualidade: TIntegerField
      FieldName = 'nCdUsuarioAnaliseQualidade'
    end
    object qryMastercOBSQualidade: TStringField
      FieldName = 'cOBSQualidade'
      Size = 50
    end
    object qryMastercChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryMasternValSeguroNF: TBCDField
      FieldName = 'nValSeguroNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercFreteNF: TBCDField
      FieldName = 'nPercFreteNF'
      Precision = 12
      Size = 2
    end
    object qryMasternPercSeguroNF: TBCDField
      FieldName = 'nPercSeguroNF'
      Precision = 12
      Size = 2
    end
    object qryMasternPercDespesaNF: TBCDField
      FieldName = 'nPercDespesaNF'
      Precision = 12
      Size = 2
    end
    object qryMasternValBaseCalcICMSSub: TBCDField
      FieldName = 'nValBaseCalcICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercUFOrigemNF: TStringField
      FieldName = 'cUFOrigemNF'
      FixedChar = True
      Size = 2
    end
    object qryMastercFlgDocEntregue: TIntegerField
      FieldName = 'cFlgDocEntregue'
    end
    object qryMastercFlgRecebTransfEst: TIntegerField
      FieldName = 'cFlgRecebTransfEst'
    end
    object qryMasternCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMasterdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
  end
  inherited dsMaster: TDataSource
    Left = 208
    Top = 432
  end
  inherited qryID: TADOQuery
    Left = 272
    Top = 512
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 128
    Top = 464
  end
  inherited qryStat: TADOQuery
    Left = 400
    Top = 480
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 96
    Top = 400
  end
  inherited ImageList1: TImageList
    Left = 64
    Top = 496
    Bitmap = {
      494C01010D000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400CCCB
      CA00D5D4D400DCDBDB0093670000EBEBEA00EBEBEA00ECECEB00ECEBEB00EAE9
      E900F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400C6C4
      C200E9E9E900936700009367000093670000F6F6F600F6F6F600F6F6F600E6E6
      E600F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400C2BF
      BC009367000093670000936700009367000093670000F5F5F500F4F4F400E2E2
      E100F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F4009367
      00009367000093670000EAEAEA00936700009367000093670000F2F2F200DEDD
      DC00F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400BCB7
      B20093670000DFDCDA00E3E1E000E8E8E800936700009367000093670000D6D5
      D400F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400B9B3
      AE00D7D1CD00D9D4D000DBD7D400DFDDDB00E3E2E10093670000936700009367
      0000F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400B9B3
      AE00D5CFCB00D5CFCB00D6D1CD00DAD5D200DEDBD800E1DFDD00936700009367
      0000936700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400B9B3
      AE00D5CFCB00D5CFCB00D5CFCB00D5CFCB00D8D3D000DCD8D500DFDDDB009367
      0000F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400B9B3
      AE00B9B3AE00B9B3AE00B9B3AE00B9B3AE00B9B3AE00BAB4AF00BDB9B400C1BE
      BB00F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000000000009797
      97006461600063605F00625F5E00615E5E00615E5D00615E5D00615E5D00615E
      5D00615E5D00BAB1A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F2F2F200E2E2
      E200E2E2E200E2E2E200E2E2E200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000000000009999
      990000000000FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE000000
      0000615E5D00BAB1A50000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F2F2F200E2E2E2007F7F
      7F0044444400555555009B9B9B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D00000000000D5CABC00B9B0A400B9B0A4009B9B
      9B0000000000A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A5000000
      0000615E5D00B8B0A400B8B0A400D5CABB000000000000000000000000000000
      0000F2F2F200F2F2F200F2F2F200F2F2F200F2F2F200E2E2E2007D7D7D004E4E
      4E00939393008E8E8E00636363000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D10000000000ABA59C0066666600646464009D9D
      9D0000000000FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD000000
      0000615E5D0048484800454545009D968E000000000000000000F2F2F200F2F2
      F200E2E2E200E2E2E200E2E2E200E2E2E200E2E2E2007B7B7B004F4F4F009191
      91008F8F8F00ACACAC005656560000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF00000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED20000000000696969009B9B9B00B8B8B8008F8F
      8F00E4E4E400949494009494940094949400949494009494940094949400E4E4
      E4005B585700A5A5A5008E918E004343430000000000F2F2F200E2E2E2007171
      7100414141005C5C5C005C5C5C003E3E3E003B3B3B003D3D3D00949494009696
      9600ACACAC00636363009494940000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000006A6A6A00B8B8B800B6B6B6007A7A
      7A00C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1
      C10053504F00A3A3A30035FE350044444400F2F2F200E2E2E20049494900A0A0
      A000E3E3E300EEEEEE00EEEEEE00E2E2E200ABABAB003A3A3A006A6A6A00AFAF
      AF0064646400949494000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D400000000006C6C6C00B8B8B800474241000101
      0100020202000202020001010100010101000101010001010100010101000101
      01000101010046424100A5A5A50046464600E2E2E2004C4C4C00CECECE00ECEA
      E600E3D9BC00DCCC9B00DCCC9B00E3D9BC00ECEAE600CBCBCB00414141005252
      520094949400000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF00000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD500000000006D6D6D00B8B8B800B8B8B8005252
      52006969690065656500606060005B5B5B0056565600525252004D4D4D004848
      480033333300A5A5A500A5A5A50047474700878787009A9A9A00EDEBE700DBCA
      9500DAC58600DBC78A00DCC88B00DBC78A00DDCC9900ECEAE600ABABAB005050
      500000000000000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D600000000006E6E6E00B8B8B800B8B8B8005454
      54006B6B6B0066666600626262005D5D5D0058585800535353004F4F4F004A4A
      4A0034343400A5A5A500A5A5A500484848004F4F4F00E8E8E800E4DBBE00DBC6
      8800DECC9100E1D09900E2D29C00E1D09900DECC9100E4D9BA00F2F2F2004747
      470000000000000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D8000000000070707000E7E7E70000000000B7B7
      B700C5C5C500C0C0C000B8B8B800B2B2B200ABABAB00A3A3A3009B9B9B009393
      93006F6F6F0000000000CCCCCC004A4A4A005F5F5F00F2F2F200DFCFA000DECC
      9100E2D4A000E6DAAC00E7DDB100E6DAAC00E2D4A000E2D4A800F1F1F1007373
      730000000000000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB00969696000000000000000000ACA69D006F6F6F006C6C6C006969
      69006E6E6E006C6C6C006A6A6A00686868006666660066666600666666006666
      660053535300505050004E4E4E009E988F0060606000F4F4F400E0D1A300E1D0
      9900E6DAAC00EBE5C000EEEAC900EBE5C000E6DAAC00E4D8AE00F2F2F2007777
      770000000000000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A40000000000000000000000000000000000000000009A9A
      9A00F3F3F300EFEFEF00EAEAEA00E6E6E600E1E1E100DFDFDF00DFDFDF00DFDF
      DF00615E5D0000000000000000000000000055555500EDEDED00E8DFC400E2D2
      9C00E7DDB100EEEAC900F3F3DB00EEEAC900E7DDB100E9E1C600F9F9F9004D4D
      4D0000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C30000000000000000000000000000000000000000009B9B
      9B00F7F7F700F3F3F300EEEEEE00EAEAEA00E5E5E500E1E1E100DFDFDF00DFDF
      DF00615E5D000000000000000000000000008D8D8D00B0B0B000F6F6F600E5D8
      AF00E6DAAC00EBE5C000EEEAC900EBE5C000E8DEB900F4F3EF00BABABA008383
      830000000000000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB0000000000000000000000000000000000000000009D9D
      9D00FBFBFB00F7F7F700F2F2F200EEEEEE00EAEAEA00E5E5E500E1E1E100DFDF
      DF00615E5D00000000000000000000000000000000005A5A5A00DDDDDD00F9F9
      F900EDE8D400ECE5CB00ECE7CE00EEEAD800F8F8F800DBDBDB005A5A5A000000
      000000000000000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF00000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E9000000000000000000000000000000000000000000000000009F9F
      9F00000000000000000000000000000000000000000000000000000000000000
      0000615E5D0000000000000000000000000000000000000000005B5B5B00BCBC
      BC00F4F4F400FDFDFD00FDFDFD0000000000BCBCBC005B5B5B00000000000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA0000000000000000000000000000000000000000000000000000000000A1A1
      A1009F9F9F009C9C9C009A9A9A009696960094949400919191008F8F8F008C8C
      8C008C8C8C000000000000000000000000000000000000000000000000008686
      86005A5A5A007C7C7C007C7C7C00515151008686860000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C9C9C0000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00BDBDBD00C6948C00C6948C00636363006363
      6300000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      0000BDBDBD00EFFFFF00FFEFDE00BDBDBD00BDBDBD00C6948C00C6948C006363
      6300000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00FFEFDE00C6948C00BDBDBD0063636300D6A5
      8C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C00000000000000000000000000636363000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00FFEFDE00C6948C00BDBDBD0063636300D6A5
      8C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00FFEFDE00C6948C00BDBDBD0063636300D6A5
      8C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C0000000000636363000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00FFEFDE00C6948C00BDBDBD0063636300D6A5
      8C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C0000000000636363000000000000000000000000000000
      0000BDBDBD00EFFFFF00BDBDBD00FFEFDE00C6948C00BDBDBD0063636300D6A5
      8C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C0000000000636363000000000000000000000000000000
      0000BDBDBD00EFFFFF00C6948C00FFEFDE008C9C9C00C6948C00424242008C9C
      9C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      0000000000000000000000000000636363000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C008C9C
      9C00000000008C9C9C00000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A563000000000000636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A56300000000000063636300000000000000000000000000BDBD
      BD00EFFFFF00EFFFFF00BDBDBD00BDBDBD00C6948C00C6948C008C9C9C008C9C
      9C008C9C9C000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      00000000000000000000BDBDBD00EFFFFF00BDBDBD00BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000C6DEC6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      8003000000000000800300000000000080030000000000008003000000000000
      8003000000000000800300000000000080030000000000008003000000000000
      8003000000000000800300000000000080030000000000008003000000000000
      8003000000000000FFFF0000000000008001E003FFC1FFFF8001E813FF81FFFF
      80010810F001800080010810C001000080010000800100008001000000030000
      800100000007000080010000000F000080010000000F000080012004000F0000
      C3C30000000F0000C3C3E007000F0000C003E007000F8000C003E007801F0000
      E007EFF7C13F0100F00FE007E07F0000FFFFFFFF81C0FFFFC000E0030180FFFF
      DFE0E0030000E0035000E0030000C003D002E0030000C003CF06E0030000C003
      B9CEE0030000C003A002E0030000C0033F62E0030000C0032002E0030000C003
      200EE0030000C0032002C0030181C0038002C0038181C0038FC2C0038181C003
      C03ED81F8181FFFFC000FFFF8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '      ,nCdTerceiroEmp'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 240
    Top = 400
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryEmpresanCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      
        'AND Exists(SELECT 1 FROM UsuarioLoja WHERE UsuarioLoja.nCdLoja =' +
        ' Loja.nCdLoja AND UsuarioLoja.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 608
    Top = 400
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 240
    Top = 432
  end
  object DataSource2: TDataSource
    DataSet = qryLoja
    Left = 608
    Top = 432
  end
  object qryTipoReceb: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTipoRecebAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoReceb'
      'WHERE nCdTipoReceb = :nPK')
    Left = 472
    Top = 400
    object qryTipoRecebnCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryTipoRecebcNmTipoReceb: TStringField
      FieldName = 'cNmTipoReceb'
      Size = 50
    end
    object qryTipoRecebcFlgExigeNF: TIntegerField
      FieldName = 'cFlgExigeNF'
    end
    object qryTipoRecebcFlgPrecoMedio: TIntegerField
      FieldName = 'cFlgPrecoMedio'
    end
    object qryTipoRecebcFlgRecebCego: TIntegerField
      FieldName = 'cFlgRecebCego'
    end
    object qryTipoRecebcFlgAguardConfirm: TIntegerField
      FieldName = 'cFlgAguardConfirm'
    end
    object qryTipoRecebnCdQuestionarioCQM: TIntegerField
      FieldName = 'nCdQuestionarioCQM'
    end
    object qryTipoRecebcFlgExigeCQM: TIntegerField
      FieldName = 'cFlgExigeCQM'
    end
    object qryTipoRecebcFlgDivValor: TIntegerField
      FieldName = 'cFlgDivValor'
    end
    object qryTipoRecebcFlgDivIPI: TIntegerField
      FieldName = 'cFlgDivIPI'
    end
    object qryTipoRecebcFlgDivICMSSub: TIntegerField
      FieldName = 'cFlgDivICMSSub'
    end
    object qryTipoRecebcFlgDivPrazoEntrega: TIntegerField
      FieldName = 'cFlgDivPrazoEntrega'
    end
    object qryTipoRecebcFlgDivPrazoPagto: TIntegerField
      FieldName = 'cFlgDivPrazoPagto'
    end
    object qryTipoRecebcFlgDivGrade: TIntegerField
      FieldName = 'cFlgDivGrade'
    end
    object qryTipoRecebcFlgExigeNFe: TIntegerField
      FieldName = 'cFlgExigeNFe'
    end
    object qryTipoRecebcFlgAtuPrecoVenda: TIntegerField
      FieldName = 'cFlgAtuPrecoVenda'
    end
    object qryTipoRecebnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTipoRecebdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTipoRecebcFlgPermiteItemAD: TIntegerField
      FieldName = 'cFlgPermiteItemAD'
    end
    object qryTipoRecebcFlgExigeInformacoesFiscais: TIntegerField
      FieldName = 'cFlgExigeInformacoesFiscais'
    end
    object qryTipoRecebcFlgExigeImportacaoXMLNFe: TIntegerField
      FieldName = 'cFlgExigeImportacaoXMLNFe'
    end
    object qryTipoRecebcFlgEscriturarEntrada: TIntegerField
      FieldName = 'cFlgEscriturarEntrada'
    end
    object qryTipoRecebcFlgDivSemPedido: TIntegerField
      FieldName = 'cFlgDivSemPedido'
    end
    object qryTipoRecebcFlgAtuPrecoWeb: TIntegerField
      FieldName = 'cFlgAtuPrecoWeb'
    end
    object qryTipoRecebcFlgConfereItem: TIntegerField
      FieldName = 'cFlgConfereItem'
    end
  end
  object DataSource3: TDataSource
    DataSet = qryTipoReceb
    Left = 472
    Top = 432
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoReceb'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '       Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCnpjCpf'
      '      ,Terceiro.cFlgOptSimples'
      '  FROM Terceiro'
      
        '       INNER JOIN TerceiroTipoTerceiro TTT ON TTT.nCdTerceiro = ' +
        'Terceiro.nCdTerceiro'
      ' WHERE nCdStatus = 1'
      '   AND Terceiro.nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM TipoPedido'
      
        '                     INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR' +
        '.nCdTipoPedido = TipoPedido.nCdTipoPedido'
      '               WHERE TPTR.nCdTipoReceb = :nCdTipoReceb)')
    Left = 408
    Top = 400
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceirocCnpjCpf: TStringField
      FieldName = 'cCnpjCpf'
      Size = 14
    end
    object qryTerceirocFlgOptSimples: TIntegerField
      FieldName = 'cFlgOptSimples'
    end
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 408
    Top = 432
  end
  object qryTabStatusReceb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabStatusReceb'
      'WHERE nCdTabStatusReceb = :nPK')
    Left = 440
    Top = 400
    object qryTabStatusRecebnCdTabStatusReceb: TIntegerField
      FieldName = 'nCdTabStatusReceb'
    end
    object qryTabStatusRecebcNmTabStatusReceb: TStringField
      FieldName = 'cNmTabStatusReceb'
      Size = 50
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      ',cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 672
    Top = 400
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource5: TDataSource
    DataSet = qryTabStatusReceb
    Left = 440
    Top = 432
  end
  object DataSource6: TDataSource
    DataSet = qryUsuario
    Left = 672
    Top = 432
  end
  object qryItemRecebimento: TADOQuery
    AutoCalcFields = False
    Connection = frmMenu.Connection
    BeforePost = qryItemRecebimentoBeforePost
    AfterPost = qryItemRecebimentoAfterPost
    BeforeDelete = qryItemRecebimentoBeforeDelete
    AfterScroll = qryItemRecebimentoAfterScroll
    OnCalcFields = qryItemRecebimentoCalcFields
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemRecebimento with(INDEX = IN03_ItemRecebimento)'
      'WHERE nCdRecebimento = :nPK'
      'AND nCdTipoItemPed IN (1,2,5)')
    Left = 640
    Top = 400
    object qryItemRecebimentonCdItemRecebimento: TIntegerField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemRecebimento'
    end
    object qryItemRecebimentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryItemRecebimentonCdItemRecebimentoPai: TIntegerField
      FieldName = 'nCdItemRecebimentoPai'
    end
    object qryItemRecebimentonCdPedido: TIntegerField
      DisplayLabel = 'Pedido|N'#250'mero'
      FieldKind = fkCalculated
      FieldName = 'nCdPedido'
      Calculated = True
    end
    object qryItemRecebimentonCdTipoItemPed: TIntegerField
      DisplayLabel = 'Pedido|Item'
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemRecebimentonCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemRecebimentocNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemRecebimentonCdItemPedido: TIntegerField
      DisplayLabel = 'Pedido|Item'
      FieldName = 'nCdItemPedido'
    end
    object qryItemRecebimentonQtde: TBCDField
      DisplayLabel = 'Item|Quant.'
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryItemRecebimentonValTotal: TBCDField
      DisplayLabel = 'Valores|Total'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentocEANFornec: TStringField
      DisplayLabel = 'Produto|EAN'
      FieldName = 'cEANFornec'
      FixedChar = True
    end
    object qryItemRecebimentonCdTabStatusItemPed: TIntegerField
      DisplayLabel = 'Status do Item|C'#243'd'
      FieldName = 'nCdTabStatusItemPed'
    end
    object qryItemRecebimentocNmTabStatusItemPed: TStringField
      DisplayLabel = 'Status do Item|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTabStatusItemPed'
      LookupDataSet = qryTabStatusItemPed
      LookupKeyFields = 'nCdTabStatusItemPed'
      LookupResultField = 'cNmTabStatusItemPed'
      KeyFields = 'nCdTabStatusItemPed'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryItemRecebimentocFlgDiverg: TIntegerField
      FieldName = 'cFlgDiverg'
    end
    object qryItemRecebimentonCdTabTipoDiverg: TIntegerField
      DisplayLabel = 'Tipo Diverg'#234'ncia|C'#243'd'
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryItemRecebimentocNmTabTipoDiverg: TStringField
      DisplayLabel = 'Tipo Diverg'#234'ncia|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTabTipoDiverg'
      LookupDataSet = qryTabTipoDiverg
      LookupKeyFields = 'nCdTabTipoDiverg'
      LookupResultField = 'cNmTabTipoDiverg'
      KeyFields = 'nCdTabTipoDiverg'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryItemRecebimentonCdUsuarioAutorDiverg: TIntegerField
      FieldName = 'nCdUsuarioAutorDiverg'
    end
    object qryItemRecebimentodDtAutorDiverg: TDateTimeField
      FieldName = 'dDtAutorDiverg'
    end
    object qryItemRecebimentonPercIPI: TBCDField
      DisplayLabel = 'Impostos do Item|% IPI'
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonValIPI: TBCDField
      DisplayLabel = 'Impostos do Item|Val. IPI'
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonPercICMSSub: TBCDField
      DisplayLabel = 'Impostos do Item|% ST'
      FieldName = 'nPercICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonValICMSSub: TBCDField
      DisplayLabel = 'Impostos do Item|Val. ST'
      FieldName = 'nValICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonPercDesconto: TBCDField
      DisplayLabel = 'Desconto Item|% Desc.'
      FieldName = 'nPercDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonValDesconto: TBCDField
      DisplayLabel = 'Desconto Item|Valor'
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonPercIPIPed: TBCDField
      FieldName = 'nPercIPIPed'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonPercICMSSubPed: TBCDField
      FieldName = 'nPercICMSSubPed'
      Precision = 12
      Size = 2
    end
    object qryItemRecebimentonValTotalLiq: TFloatField
      DisplayLabel = 'Total L'#237'q|s/ imp.'
      FieldKind = fkCalculated
      FieldName = 'nValTotalLiq'
      Calculated = True
    end
    object qryItemRecebimentocFlgDescAbatUnitario: TIntegerField
      FieldName = 'cFlgDescAbatUnitario'
    end
    object qryItemRecebimentonValDescAbatUnit: TBCDField
      FieldName = 'nValDescAbatUnit'
      Precision = 12
    end
    object qryItemRecebimentonValUnitario: TFloatField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValUnitario'
    end
    object qryItemRecebimentonValUnitarioEsp: TFloatField
      FieldName = 'nValUnitarioEsp'
    end
    object qryItemRecebimentonValUnitarioPed: TFloatField
      DisplayLabel = 'Valores|Unit'#225'rio Ped'
      FieldName = 'nValUnitarioPed'
    end
    object qryItemRecebimentonValCustoFinal: TFloatField
      FieldName = 'nValCustoFinal'
    end
    object qryItemRecebimentonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryItemRecebimentodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryItemRecebimentonPercRedBaseCalcICMS: TFloatField
      FieldName = 'nPercRedBaseCalcICMS'
    end
    object qryItemRecebimentonBaseCalcICMS: TFloatField
      FieldName = 'nBaseCalcICMS'
    end
    object qryItemRecebimentonPercRedBaseCalcSubTrib: TFloatField
      FieldName = 'nPercRedBaseCalcSubTrib'
    end
    object qryItemRecebimentonPercAliqICMS: TFloatField
      FieldName = 'nPercAliqICMS'
    end
    object qryItemRecebimentonValSeguro: TFloatField
      FieldName = 'nValSeguro'
    end
    object qryItemRecebimentonValICMS: TFloatField
      FieldName = 'nValICMS'
    end
    object qryItemRecebimentonValDifAliqICMS: TFloatField
      FieldName = 'nValDifAliqICMS'
    end
    object qryItemRecebimentonValFrete: TFloatField
      FieldName = 'nValFrete'
    end
    object qryItemRecebimentonValAcessorias: TFloatField
      FieldName = 'nValAcessorias'
    end
    object qryItemRecebimentocCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemRecebimentocCdST: TStringField
      DisplayWidth = 4
      FieldName = 'cCdST'
      OnChange = qryItemRecebimentocCdSTChange
      FixedChar = True
      Size = 4
    end
    object qryItemRecebimentonCdTipoICMS: TIntegerField
      FieldName = 'nCdTipoICMS'
    end
    object qryItemRecebimentonCdTipoIPI: TIntegerField
      FieldName = 'nCdTipoIPI'
    end
    object qryItemRecebimentocFlgImportacao: TIntegerField
      FieldName = 'cFlgImportacao'
    end
    object qryItemRecebimentocFlgGeraLivroFiscal: TIntegerField
      FieldName = 'cFlgGeraLivroFiscal'
    end
    object qryItemRecebimentocCFOP: TStringField
      DisplayLabel = 'CFOP Entrada'
      FieldName = 'cCFOP'
      OnChange = qryItemRecebimentocCFOPChange
      FixedChar = True
      Size = 5
    end
    object qryItemRecebimentonValBaseCalcSubTrib: TFloatField
      FieldName = 'nValBaseCalcSubTrib'
    end
    object qryItemRecebimentocFlgGeraCreditoICMS: TIntegerField
      FieldName = 'cFlgGeraCreditoICMS'
    end
    object qryItemRecebimentocFlgGeraCreditoIPI: TIntegerField
      FieldName = 'cFlgGeraCreditoIPI'
    end
    object qryItemRecebimentonValAliquotaII: TFloatField
      FieldName = 'nValAliquotaII'
    end
    object qryItemRecebimentocNmTipoICMS: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoICMS'
      Calculated = True
    end
    object qryItemRecebimentocNmTipoIPI: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoIPI'
      Calculated = True
    end
    object qryItemRecebimentonAliqICMSInterna: TFloatField
      FieldName = 'nAliqICMSInterna'
    end
    object qryItemRecebimentonPercIVA: TFloatField
      FieldName = 'nPercIVA'
    end
    object qryItemRecebimentonPercBaseCalcIPI: TFloatField
      FieldName = 'nPercBaseCalcIPI'
    end
    object qryItemRecebimentonValBaseIPI: TFloatField
      FieldName = 'nValBaseIPI'
    end
    object qryItemRecebimentocNCM: TStringField
      DisplayLabel = 'NCM/SH'
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryItemRecebimentocCFOPNF: TStringField
      DisplayLabel = 'CFOP NF'
      FieldName = 'cCFOPNF'
      OnChange = qryItemRecebimentocCFOPNFChange
      Size = 5
    end
    object qryItemRecebimentocCdSTIPI: TStringField
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemRecebimentonQtdeConf: TFloatField
      FieldName = 'nQtdeConf'
    end
  end
  object dsItemRecebimento: TDataSource
    DataSet = qryItemRecebimento
    Left = 640
    Top = 432
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    EnableBCD = False
    Parameters = <>
    Left = 304
    Top = 512
  end
  object qryConsultaPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoReceb'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdRecebimento'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @iDias             int'
      '       ,@cRecebeAtraso     char(1)'
      '       ,@nCdItemPedido     int'
      '       ,@nCdGrupoEconomico int'
      '       ,@nCdTerceiro       int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      ''
      'SELECT @nCdGrupoEconomico = nCdGrupoEconomico'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'SELECT @iDias = Convert(int,IsNull(cValor,'#39'0'#39'))'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'DIAATRASOREC'#39
      ''
      'SELECT @cRecebeAtraso = IsNull(cValor,'#39'S'#39')'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'RECFORAPAZO'#39
      ''
      'SELECT @nCdItemPedido = Min(nCdItemPedido)'
      '  FROM Pedido'
      
        '       LEFT  JOIN Terceiro    ON Terceiro.nCdTerceiro = Pedido.n' +
        'CdTerceiro'
      
        '       INNER JOIN ItemPedido  ON ItemPedido.nCdPedido = Pedido.n' +
        'CdPedido'
      
        '                             AND (nQtdePed - nQtdeExpRec - nQtde' +
        'Canc) > 0'
      '                             AND nCdItemPedidoPai IS NULL'
      '                             AND nCdProduto = :nCdProduto'
      ' WHERE nSaldoFat > 0'
      '   AND Pedido.nCdTabStatusPed IN (3,4)'
      '   AND (   (Pedido.nCdTerceiro = @nCdTerceiro)'
      '        OR (    @nCdGrupoEconomico IS NOT NULL'
      
        '            AND Terceiro.nCdGrupoEconomico = @nCdGrupoEconomico)' +
        ')'
      '   AND nCdEmpresa = :nCdEmpresa'
      
        '   AND (   (@cRecebeAtraso = '#39'N'#39' AND dbo.fn_OnlyDate(GetDate()) ' +
        'BETWEEN (dDtPrevEntIni-@iDias) AND (dDtPrevEntFim+@iDias))'
      '        OR (@cRecebeAtraso = '#39'S'#39'))'
      '   AND (Pedido.nCdLoja = :nCdLoja OR Pedido.nCdLoja IS NULL)'
      '   AND EXISTS(SELECT 1'
      '                FROM TipoPedidoTipoReceb TPTR'
      '               WHERE TPTR.nCdTipoPedido = Pedido.nCdTipoPedido'
      '                 AND TPTR.nCdTipoReceb  = :nCdTipoReceb)'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM ItemRecebimento ItemRec'
      
        '                   WHERE ItemRec.nCdRecebimento = :nCdRecebiment' +
        'o'
      
        '                     AND ItemRec.nCdItemPedido  = ItemPedido.nCd' +
        'ItemPedido)'
      ''
      ''
      'SELECT ItemPedido.nCdItemPedido'
      '      ,ItemPedido.nValCustoUnit'
      '      ,ItemPedido.nValUnitarioEsp'
      '      ,ItemPedido.nPercIPI'
      '      ,ItemPedido.nPercICMSSub'
      
        '      ,CASE WHEN ItemPedido.nCdTipoItemPed = 4 THEN (ItemPedido.' +
        'nValUnitario - Pai.nValDesconto + Pai.nValAcrescimo)'
      
        '            ELSE (ItemPedido.nValUnitario - ItemPedido.nValDesco' +
        'nto + ItemPedido.nValAcrescimo)'
      '       END nValUnitario'
      '  FROM ItemPedido'
      
        '       LEFT JOIN ItemPedido Pai ON Pai.nCdItemPedido = ItemPedid' +
        'o.nCdItemPedidoPai'
      ' WHERE ItemPedido.nCdItemPedido = @nCdItemPedido')
    Left = 272
    Top = 480
    object qryConsultaPedidonCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryConsultaPedidonValCustoUnit: TBCDField
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryConsultaPedidonValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 12
      Size = 2
    end
    object qryConsultaPedidonPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 5
      Size = 2
    end
    object qryConsultaPedidonPercICMSSub: TBCDField
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryConsultaPedidonValUnitario: TBCDField
      FieldName = 'nValUnitario'
      ReadOnly = True
      Precision = 14
    end
  end
  object qryTabTipoDiverg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoDiverg')
    Left = 304
    Top = 480
    object qryTabTipoDivergnCdTabTipoDiverg: TIntegerField
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryTabTipoDivergcNmTabTipoDiverg: TStringField
      FieldName = 'cNmTabTipoDiverg'
      Size = 50
    end
  end
  object qryTabStatusItemPed: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabStatusItemPed')
    Left = 400
    Top = 512
    object qryTabStatusItemPednCdTabStatusItemPed: TIntegerField
      FieldName = 'nCdTabStatusItemPed'
    end
    object qryTabStatusItemPedcNmTabStatusItemPed: TStringField
      FieldName = 'cNmTabStatusItemPed'
      Size = 50
    end
    object qryTabStatusItemPedcFlgFaturar: TIntegerField
      FieldName = 'cFlgFaturar'
    end
  end
  object qryItemBarra: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemBarraBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemRecebimento'
      'WHERE nCdRecebimento = :nPK'
      'AND cEANFornec IS NOT NULL')
    Left = 576
    Top = 400
    object qryItemBarranCdItemRecebimento: TAutoIncField
      FieldName = 'nCdItemRecebimento'
      ReadOnly = True
    end
    object qryItemBarranCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryItemBarranCdItemRecebimentoPai: TIntegerField
      FieldName = 'nCdItemRecebimentoPai'
    end
    object qryItemBarranCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemBarranCdPedido: TIntegerField
      DisplayLabel = 'Pedido|N'#250'mero'
      FieldKind = fkCalculated
      FieldName = 'nCdPedido'
      Calculated = True
    end
    object qryItemBarranCdItemPedido: TIntegerField
      DisplayLabel = 'Pedido|Item'
      FieldName = 'nCdItemPedido'
    end
    object qryItemBarranCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemBarracNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemBarranQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryItemBarranValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemBarranValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryItemBarranValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 12
    end
    object qryItemBarranValUnitarioPed: TBCDField
      FieldName = 'nValUnitarioPed'
      Precision = 12
    end
    object qryItemBarracEANFornec: TStringField
      DisplayLabel = 'Produto|EAN'
      FieldName = 'cEANFornec'
      FixedChar = True
    end
    object qryItemBarranCdTabStatusItemPed: TIntegerField
      DisplayLabel = 'Status do Item|C'#243'd'
      FieldName = 'nCdTabStatusItemPed'
    end
    object qryItemBarracNmTabStatusItemPed: TStringField
      DisplayLabel = 'Status do Item|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTabStatusItemPed'
      LookupDataSet = qryTabStatusItemPed
      LookupKeyFields = 'nCdTabStatusItemPed'
      LookupResultField = 'cNmTabStatusItemPed'
      KeyFields = 'nCdTabStatusItemPed'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryItemBarracFlgDiverg: TIntegerField
      FieldName = 'cFlgDiverg'
    end
    object qryItemBarranCdTabTipoDiverg: TIntegerField
      DisplayLabel = 'Tipo Diverg'#234'ncia|C'#243'd'
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryItemBarracNmTabTipoDiverg: TStringField
      DisplayLabel = 'Tipo Diverg'#234'ncia|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTabTipoDiverg'
      LookupDataSet = qryTabTipoDiverg
      LookupKeyFields = 'nCdTabTipoDiverg'
      LookupResultField = 'cNmTabTipoDiverg'
      KeyFields = 'nCdTabTipoDiverg'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryItemBarranCdUsuarioAutorDiverg: TIntegerField
      FieldName = 'nCdUsuarioAutorDiverg'
    end
    object qryItemBarradDtAutorDiverg: TDateTimeField
      FieldName = 'dDtAutorDiverg'
    end
    object qryItemBarranPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 12
      Size = 2
    end
    object qryItemBarranValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
      Size = 2
    end
    object qryItemBarranPercICMSSub: TBCDField
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItemBarranValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItemBarranPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      Precision = 12
      Size = 2
    end
    object qryItemBarranValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
  end
  object dsItemBarra: TDataSource
    DataSet = qryItemBarra
    Left = 576
    Top = 432
  end
  object qryPrazoRecebimento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPrazoRecebimentoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM PrazoRecebimento'
      'Where nCdRecebimento = :nPK')
    Left = 504
    Top = 400
    object qryPrazoRecebimentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryPrazoRecebimentodDtVenc: TDateTimeField
      DisplayLabel = 'Vencimento Real|Data'
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryPrazoRecebimentonValPagto: TBCDField
      DisplayLabel = 'Parcelas|Valor Pagamento'
      FieldName = 'nValPagto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrazoRecebimentonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryPrazoRecebimentocNrTit: TStringField
      DisplayLabel = 'Parcelas|N'#250'mero T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryPrazoRecebimentoiParcela: TIntegerField
      DisplayLabel = 'Parcelas|Parcela'
      FieldName = 'iParcela'
    end
    object qryPrazoRecebimentocFlgDocCobranca: TIntegerField
      DisplayLabel = 'Parcelas|Enviado Documento de Cobran'#231'a'
      FieldName = 'cFlgDocCobranca'
    end
    object qryPrazoRecebimentonValParcela: TBCDField
      DisplayLabel = 'Parcelas|Valor Parcela'
      FieldName = 'nValParcela'
      Precision = 12
      Size = 2
    end
    object qryPrazoRecebimentonValOperacaoFin: TBCDField
      DisplayLabel = 'Parcelas|Opera'#231#227'o Financeira'
      FieldName = 'nValOperacaoFin'
      Precision = 12
      Size = 2
    end
    object qryPrazoRecebimentoiQtdeDias: TIntegerField
      DisplayLabel = 'Vencimento Real|Dias'
      FieldName = 'iQtdeDias'
    end
    object qryPrazoRecebimentocFlgDiverg: TIntegerField
      FieldName = 'cFlgDiverg'
    end
    object qryPrazoRecebimentocFlgAutomatica: TIntegerField
      FieldName = 'cFlgAutomatica'
    end
    object qryPrazoRecebimentocCodBarra: TStringField
      DisplayLabel = 'Parcelas|C'#243'd. Barras'
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryPrazoRecebimentonCdPrazoRecebimento: TIntegerField
      FieldName = 'nCdPrazoRecebimento'
    end
  end
  object dsPrazoRecebimento: TDataSource
    DataSet = qryPrazoRecebimento
    Left = 504
    Top = 432
  end
  object usp_Gera_SubItem: TADOStoredProc
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    CommandTimeout = 0
    ProcedureName = 'SP_GERA_SUBITEM_GRADE_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 528
  end
  object usp_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    AfterPost = usp_GradeAfterPost
    AfterCancel = usp_GradeAfterCancel
    CommandTimeout = 0
    ProcedureName = 'SP_PREPARA_GRADE_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cSQLRetorno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1000
        Value = Null
      end>
    Left = 176
    Top = 400
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    AfterPost = qryTempAfterPost
    AfterCancel = qryTempAfterCancel
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM ##Temp_Grade_Qtde')
    Left = 208
    Top = 512
  end
  object dsTemp: TDataSource
    DataSet = usp_Grade
    Left = 176
    Top = 432
  end
  object cmdExcluiSubItem: TADOCommand
    CommandText = 
      'DECLARE @nCdItemRecebimento int'#13#10'       ,@nCdItemRecebimentoPai ' +
      'int'#13#10#13#10'Set @nCdItemRecebimentoPai = :nPK'#13#10#13#10'DECLARE curItens CUR' +
      'SOR FOR'#13#10'    SELECT nCdItemRecebimento'#13#10'       FROM ItemRecebime' +
      'nto'#13#10'    WHERE nCdItemRecebimentoPai = @nCdItemRecebimentoPai'#13#10' ' +
      '        AND nCdTipoItemPed IN (4,7,8)'#13#10#13#10'DELETE FROM CentroCusto' +
      'ItemRecebimento'#13#10'  WHERE nCdItemRecebimento = @nCdItemRecebiment' +
      'oPai'#13#10#13#10'OPEN curItens'#13#10#13#10'FETCH NEXT'#13#10'  FROM curItens'#13#10'  INTO @nC' +
      'dItemRecebimento'#13#10#13#10'WHILE (@@FETCH_STATUS = 0)'#13#10'BEGIN'#13#10#13#10'    DEL' +
      'ETE FROM HistAutorDiverg'#13#10'       WHERE nCdItemRecebimento = @nCd' +
      'ItemRecebimento'#13#10#13#10'    DELETE FROM CentroCustoItemRecebimento'#13#10' ' +
      '     WHERE nCdItemRecebimento = @nCdItemRecebimento'#13#10#13#10'    DELET' +
      'E FROM ItemRecebimento'#13#10'      WHERE nCdItemRecebimento = @nCdIte' +
      'mRecebimento'#13#10#13#10'    FETCH NEXT'#13#10'      FROM curItens'#13#10'      INTO ' +
      '@nCdItemRecebimento'#13#10#13#10'END'#13#10#13#10'CLOSE curItens'#13#10'DEALLOCATE curIten' +
      's'
    CommandTimeout = 70
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 64
    Top = 400
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cCdProduto VARCHAR(20)'
      '       ,@nCdProduto int'
      ''
      'Set @cCdProduto = RTRIM(LTRIM(:nPK))'
      ''
      'BEGIN TRY'
      '    SELECT @nCdProduto = nCdProduto'
      '      FROM Produto'
      '     WHERE nCdProduto = Convert(int,@cCdProduto)'
      'END TRY'
      'BEGIN CATCH'
      'END CATCH'
      ''
      'IF (@nCdProduto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdProduto = nCdProduto'
      '      FROM Produto'
      '     WHERE cEAN = @cCdProduto'
      ''
      'END'
      ''
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,nCdGrade'
      '      ,nCdTabTipoProduto'
      '      ,nCdProdutoPai'
      '      ,cReferencia'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      '')
    Left = 500
    Top = 480
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutonCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryProdutonCdTabTipoProduto: TIntegerField
      FieldName = 'nCdTabTipoProduto'
    end
    object qryProdutonCdProdutoPai: TIntegerField
      FieldName = 'nCdProdutoPai'
    end
    object qryProdutocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
  end
  object SP_FINALIZA_RECEBIMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_FINALIZA_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 432
  end
  object usp_copia_cc: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_COPIA_CC_ITEMRECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 132
    Top = 400
  end
  object qryItemCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemRecebimento'
      
        '      ,Convert(VARCHAR,nCdItemRecebimento) + '#39'          '#39' + IsNu' +
        'll(RIGHT(REPLICATE('#39'0'#39',10) + Convert(VARCHAR,nCdProduto),10) + '#39 +
        ' - '#39','#39#39') + cNmItem as cNmItem'
      '  FROM ItemRecebimento with (INDEX = IN03_ItemRecebimento)'
      ' WHERE nCdItemPedido IS NULL'
      '   AND nCdRecebimento = :nPK'
      '   AND nValTotal > 0'
      '  AND nCdTipoItemPed NOT IN (4,7,8)'
      ' ORDER BY nCdItemRecebimento')
    Left = 272
    Top = 400
    object qryItemCCnCdItemRecebimento: TAutoIncField
      FieldName = 'nCdItemRecebimento'
      ReadOnly = True
    end
    object qryItemCCcNmItem: TStringField
      FieldName = 'cNmItem'
      ReadOnly = True
      Size = 203
    end
  end
  object dsItemCC: TDataSource
    DataSet = qryItemCC
    Left = 272
    Top = 432
  end
  object qryCCItemRecebimento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCCItemRecebimentoBeforePost
    OnCalcFields = qryCCItemRecebimentoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      
        '  FROM CentroCustoItemRecebimento with (INDEX = IN01_CentroCusto' +
        'ItemRecebimento)'
      ' WHERE nCdItemRecebimento = :nPK')
    Left = 336
    Top = 400
    object qryCCItemRecebimentonCdItemRecebimento: TIntegerField
      FieldName = 'nCdItemRecebimento'
    end
    object qryCCItemRecebimentonCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryCCItemRecebimentocNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
    object qryCCItemRecebimentonPercent: TBCDField
      DisplayLabel = '% Particip.'
      FieldName = 'nPercent'
      Precision = 12
      Size = 2
    end
    object qryCCItemRecebimentonCdCentroCustoItemRecebimento: TAutoIncField
      FieldName = 'nCdCentroCustoItemRecebimento'
    end
  end
  object dsCCItemRecebimento: TDataSource
    DataSet = qryCCItemRecebimento
    Left = 336
    Top = 432
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM CentroCusto'
      ' WHERE iNivel    = 3'
      '   AND cFlgLanc  = 1'
      '   AND nCdStatus = 1'
      '   AND nCdCC     = :nPK'
      '')
    Left = 176
    Top = 480
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryCentroCustonCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object qryCentroCustonCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object qryCentroCustocCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustocCdHie: TStringField
      FieldName = 'cCdHie'
      FixedChar = True
      Size = 4
    end
    object qryCentroCustoiNivel: TSmallintField
      FieldName = 'iNivel'
    end
    object qryCentroCustocFlgLanc: TIntegerField
      FieldName = 'cFlgLanc'
    end
    object qryCentroCustonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryLoteItemRecebimento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLoteItemRecebimentoBeforePost
    Parameters = <
      item
        Name = 'nCdRecebimento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM LoteItemRecebimento'
      'WHERE nCdRecebimento = :nCdRecebimento'
      'AND nCdProduto = :nPK')
    Left = 540
    Top = 400
    object qryLoteItemRecebimentonCdLoteItemRecebimento: TAutoIncField
      FieldName = 'nCdLoteItemRecebimento'
      ReadOnly = True
    end
    object qryLoteItemRecebimentocNrLote: TStringField
      DisplayLabel = 'Dados do Lote|N'#250'mero'
      FieldName = 'cNrLote'
    end
    object qryLoteItemRecebimentodDtFabricacao: TDateTimeField
      DisplayLabel = 'Dados do Lote|Data Fabrica'#231#227'o'
      FieldName = 'dDtFabricacao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryLoteItemRecebimentodDtValidade: TDateTimeField
      DisplayLabel = 'Dados do Lote|Data Validade'
      FieldName = 'dDtValidade'
      EditMask = '!99/99/9999;1;_'
    end
    object qryLoteItemRecebimentonQtde: TBCDField
      DisplayLabel = 'Dados do Lote|Quantidade'
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.0000'
      Precision = 12
      Size = 2
    end
    object qryLoteItemRecebimentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryLoteItemRecebimentonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object dsLoteItemRecebimento: TDataSource
    DataSet = qryLoteItemRecebimento
    Left = 540
    Top = 432
  end
  object qryProdLote: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemRecebimento.nCdProduto'
      '     ,ItemRecebimento.nCdItemRecebimento'
      '      ,cNmProduto'
      '      ,Sum(nQtde) as nQtde'
      '  FROM ItemRecebimento'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = ItemRecebiment' +
        'o.nCdProduto'
      ' WHERE Produto.nCdTabTipoModoGestaoProduto <> 1'
      '   AND nCdRecebimento   = :nPK'
      ' GROUP BY ItemRecebimento.nCdProduto'
      '         ,cNmProduto'
      '         ,ItemRecebimento.nCdItemRecebimento')
    Left = 372
    Top = 400
    object qryProdLotenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdLotecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdLotenQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryProdLotenCdItemRecebimento: TIntegerField
      FieldName = 'nCdItemRecebimento'
    end
  end
  object dsProdLote: TDataSource
    DataSet = qryProdLote
    Left = 372
    Top = 432
  end
  object qryItemPagavel: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Sum(nValTotal-nValDesconto) as nValTotal'
      '  FROM ItemRecebimento'
      ' WHERE nCdRecebimento = :nPK'
      '   AND ((   nCdItemPedido IS NULL  --Item sem pedido'
      '         AND nCdItemRecebimentoPai IS NULL)'
      '        OR EXISTS(SELECT 1'
      '                    FROM ItemPedido'
      
        '                         INNER JOIN Pedido     ON Pedido.nCdPedi' +
        'do         = ItemPedido.nCdPedido'
      
        '                         INNER JOIN TipoPedido ON TipoPedido.nCd' +
        'TipoPedido = Pedido.nCdTipoPedido'
      
        '                   WHERE nCdItemPedido = ItemRecebimento.nCdItem' +
        'Pedido'
      '                     AND nCdTipoItemPed IN (1,2,3,5,6,8)'
      '                     AND cGerarFinanc  = 1))')
    Left = 432
    Top = 512
    object qryItemPagavelnValTotal: TFloatField
      FieldName = 'nValTotal'
      ReadOnly = True
    end
  end
  object SP_GERA_PARCELA_RECEBIMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_PARCELA_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 464
  end
  object SP_INSERI_DIVERGENCIA_RECEB: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_INSERI_DIVERGENCIA_RECEB;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTabTipoDiverg'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 432
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu1Popup
    Left = 60
    Top = 528
    object ExibirDivergncias1: TMenuItem
      Caption = 'Exibir Diverg'#234'ncias'
      ImageIndex = 10
      OnClick = ExibirDivergncias1Click
    end
    object ExcluirItem1: TMenuItem
      Caption = 'Excluir Item'
      ImageIndex = 5
      OnClick = ExcluirItem1Click
    end
    object VincularPedido1: TMenuItem
      Caption = 'Vincular Pedido'
      ImageIndex = 3
      OnClick = VincularPedido1Click
    end
  end
  object qryUsuarioQualidade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      ',cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 304
    Top = 404
    object qryUsuarioQualidadenCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioQualidadecNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryUsuarioQualidade
    Left = 304
    Top = 432
  end
  object qryGradeRecebimento: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    AfterPost = qryGradeRecebimentoAfterPost
    AfterCancel = qryGradeRecebimentoAfterCancel
    Parameters = <
      item
        Name = 'nCdRecebimento'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdItemRecebimento'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdRecebimento     int'
      '       ,@nCdItemRecebimento int'
      '       ,@nCdProduto         int'
      '       ,@cSQLRetorno        varchar(1000)'
      ''
      '    DECLARE @nCdProdutoDetalhe int'
      '           ,@iTamanho          int'
      '           ,@cSQL              varchar(100)'
      '           ,@cSQLQuery         varchar(1000)'
      '           ,@cTamanho          varchar(5)'
      '           ,@cNmProduto        varchar(150)'
      '           ,@iQtde             int'
      '           ,@iQtdeTamanhos     int'
      ''
      'Set @nCdRecebimento     = :nCdRecebimento'
      'Set @nCdItemRecebimento = :nCdItemRecebimento'
      'Set @nCdProduto         = :nCdProduto'
      ''
      '    SELECT @cNmProduto = cNmProduto'
      '      FROM Produto'
      '     WHERE nCdProduto = @nCdProduto'
      ''
      '    INSERT INTO #Temp_Grade_Qtde (nCdProdutoPai'
      '                                 ,cNmProduto'
      '                                 ,iQtdeTamanhos)'
      '                          VALUES (@nCdProduto'
      '                                 ,@cNmProduto'
      '                                 ,0)'
      ''
      '    DECLARE curTamanho CURSOR FOR'
      '      SELECT ItemGrade.iTamanho'
      '            ,ItemGrade.cNmTamanho'
      '        FROM ItemGrade'
      
        '             INNER JOIN Produto ON Produto.nCdGrade   = ItemGrad' +
        'e.nCdGrade'
      
        '                               AND Produto.nCdProduto = @nCdProd' +
        'uto'
      ''
      '    OPEN curTamanho'
      ''
      '    FETCH NEXT'
      '     FROM curTamanho'
      '     INTO @iTamanho'
      '         ,@cTamanho'
      ''
      ''
      
        '    Set @cSQLQuery = '#39'SELECT cNmProduto as '#39' + Char(39) + '#39'Produ' +
        'to|Descri'#231#227'o'#39' + Char(39)'
      ''
      '    WHILE (@@FETCH_STATUS = 0)'
      '    BEGIN'
      ''
      '        Set @nCdProdutoDetalhe = NULL'
      ''
      '        SELECT @nCdProdutoDetalhe = nCdProduto'
      '          FROM Produto'
      '         WHERE nCdProdutoPai = @nCdProduto'
      '           AND iTamanho      = @iTamanho'
      ''
      '        Set @iQtde = 0'
      ''
      '        --'
      '        -- Localiza o item da grade no recebimento'
      '        --'
      '        SELECT @iQtde = nQtde'
      '          FROM ItemRecebimento'
      '         WHERE nCdItemRecebimentoPai = @nCdItemRecebimento'
      '           AND nCdTipoItemPed        = 4'
      '           AND nCdProduto            = @nCdProdutoDetalhe'
      ''
      '        IF (@nCdProdutoDetalhe IS NOT NULL)'
      '        BEGIN'
      ''
      
        '  '#9#9#9'    Set @cSQL = '#39'UPDATE #Temp_Grade_Qtde SET c'#39' + @cTamanho' +
        ' + '#39' = '#39' + Convert(VARCHAR,@iQtde) + '#39', cCodigo'#39' + Convert(VARCH' +
        'AR,@nCdProdutoDetalhe) + '#39' = '#39' + Convert(VARCHAR,@nCdProdutoDeta' +
        'lhe)'
      ''
      #9'       '#9#9'EXECUTE(@cSQL)'
      '            '
      '        END'
      ''
      '        UPDATE #Temp_Grade_Qtde'
      '           SET iQtdeTamanhos = iQtdeTamanhos + 1'
      '          '
      '        FETCH NEXT'
      '         FROM curTamanho'
      '         INTO @iTamanho'
      '             ,@cTamanho'
      '    END'
      ''
      '    Set @cSQLQuery = @cSQLQuery + '#39' FROM #Temp_Grade_Qtde'#39
      ''
      '    CLOSE curTamanho'
      '    DEALLOCATE curTamanho'
      ''
      '    Set @cSQLRetorno = @cSQLQuery'
      ''
      '    EXECUTE(@cSQLQuery)')
    Left = 176
    Top = 512
  end
  object qryInseriItemGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'nCdItemRecebimento'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iQtde'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      ''
      'DECLARE @nCdRecebimento     int'
      '       ,@nCdItemRecebimento int'
      '       ,@nCdProdutoDetalhe  int'
      '       ,@iQtde              int'
      '       ,@iID                int'
      '       ,@nValCustoFinal     decimal(12,4)'
      ''
      'Set @nCdRecebimento     = :nCdRecebimento'
      'Set @nCdItemRecebimento = :nCdItemRecebimento'
      'Set @nCdProdutoDetalhe  = :nCdProduto'
      'Set @iQtde              = :iQtde'
      'Set @nValCustoFinal     = 0'
      ''
      'DECLARE @cNmProduto        varchar(150)'
      '       ,@nCdItemPedido     int'
      '       ,@nCdItemRecebimentoAux   int'
      '       ,@nCdItemPedido_Detalhe   int'
      '       ,@nQtde_Detalhe           decimal(12,4)'
      '       ,@nValUnitarioPed_Detalhe decimal(12,4)'
      '       ,@nValUnitarioEsp_Detalhe decimal(12,4)'
      '       ,@nValUnitarioPai         decimal(12,4)'
      ''
      ''
      '--'
      '-- Descobre o item pai da grade'
      '--'
      'SELECT @nCdItemPedido   = nCdItemPedido'
      '      ,@nValUnitarioPai = nValUnitario'
      '      ,@nValCustoFinal  = nValCustoFinal'
      '  FROM ItemRecebimento'
      ' WHERE nCdItemRecebimento = @nCdItemRecebimento'
      ''
      '--'
      '-- Descobre se o item da grade esta em pedido'
      '--'
      'Set @nQtde_Detalhe           = NULL'
      'Set @nCdItemPedido_Detalhe   = NULL'
      'Set @nValUnitarioPed_Detalhe = 0'
      'Set @nValUnitarioEsp_Detalhe = 0'
      ''
      'SELECT @nCdItemPedido_Detalhe   = ItemPedido.nCdItemPedido'
      
        '      ,@nQtde_Detalhe           = (ItemPedido.nQtdePed - ItemPed' +
        'ido.nQtdeExpRec - ItemPedido.nQtdeCanc)'
      
        '      ,@nValUnitarioPed_Detalhe = (ItemPedido.nValUnitario - Pai' +
        '.nValDesconto + Pai.nValAcrescimo) --(nValUnitario - nValDescont' +
        'o + nValAcrescimo)'
      '      ,@nValUnitarioEsp_Detalhe = ItemPedido.nValUnitarioEsp'
      '  FROM ItemPedido'
      
        '       LEFT JOIN ItemPedido Pai ON Pai.nCdItemPedido = ItemPedid' +
        'o.nCdItemPedidoPai'
      ' WHERE ItemPedido.nCdItemPedidoPai = @nCdItemPedido'
      '   AND ItemPedido.nCdProduto       = @nCdProdutoDetalhe'
      '   AND ItemPedido.nCdTipoItemPed   = 4'
      ''
      'IF EXISTS(SELECT 1'
      #9#9#9'FROM ItemRecebimento'
      #9#9'   WHERE nCdItemRecebimentoPai = @nCdItemRecebimento'
      #9#9#9' AND nCdTipoItemPed        = 4 '
      #9#9#9' AND nCdProduto            = @nCdProdutoDetalhe)'
      'BEGIN'
      ''
      #9'UPDATE ItemRecebimento'
      #9'   SET nQtde           = @iQtde'
      '          ,nCdItemPedido   = @nCdItemPedido_Detalhe'
      '          ,nValUnitarioPed = @nValUnitarioPed_Detalhe'
      '          ,nValUnitarioEsp = @nValUnitarioEsp_Detalhe'
      '          ,nValUnitario    = @nValUnitarioPai'
      '          ,nValTotal       = @nValUnitarioPai * @iQtde'
      #9' WHERE nCdItemRecebimentoPai = @nCdItemRecebimento'
      #9'   AND nCdTipoItemPed        = 4 '
      #9'   AND nCdProduto            = @nCdProdutoDetalhe'
      ''
      'END'
      'ELSE BEGIN'
      ''
      '    SELECT @cNmProduto = cNmProduto'
      '      FROM Produto'
      '     WHERE nCdProduto = @nCdProdutoDetalhe'
      ''
      '  EXEC usp_ProximoID  '#39'ITEMRECEBIMENTO'#39
      '                      ,@iID OUTPUT'
      ''
      #9'INSERT INTO ItemRecebimento   (nCdItemRecebimento'
      '                                ,nCdRecebimento'
      #9#9#9#9#9#9'                    ,nCdItemRecebimentoPai'
      #9#9#9#9#9#9'                    ,nCdProduto'
      '                                ,cNmItem'
      #9#9#9#9#9#9'                    ,nCdTipoItemPed'
      #9#9#9#9#9#9'                    ,nQtde'
      '                                ,nCdItemPedido'
      '                                ,nValUnitario'
      '                                ,nValTotal'
      '                                ,nValUnitarioPed'
      '                                ,nValUnitarioEsp'
      '                                ,nValCustoFinal)'
      #9#9#9#9#9#9'              VALUES(@iID'
      '                                ,@nCdRecebimento'
      #9#9#9#9#9#9' '#9'                  ,@nCdItemRecebimento'
      #9#9#9#9#9#9' '#9'                  ,@nCdProdutoDetalhe'
      #9#9#9#9#9#9' '#9'                  ,@cNmProduto'
      #9#9#9#9#9#9' '#9'                  ,4   --SubItem'
      #9#9#9#9#9#9#9'                  ,@iQtde'
      '                                ,@nCdItemPedido_Detalhe'
      '                                ,@nValUnitarioPai'
      '                                ,@nValUnitarioPai * @iQtde'
      '                                ,@nValUnitarioPed_Detalhe'
      '                                ,@nValUnitarioEsp_Detalhe'
      '                                ,@nValCustoFinal)'
      ''
      'END'
      ''
      '--'
      '-- Se a quantidade for superior ao saldo do pedido, gera diverg.'
      '--'
      'IF (@iQtde > IsNull(@nQtde_Detalhe,0))'
      'BEGIN'
      ''
      #9'UPDATE ItemRecebimento'
      #9'   SET cFlgDiverg       = 1'
      '          ,nCdTabTipoDiverg = 4'
      #9' WHERE nCdItemRecebimentoPai = @nCdItemRecebimento'
      #9'   AND nCdTipoItemPed        = 4 '
      #9'   AND nCdProduto            = @nCdProdutoDetalhe'
      ''
      #9'UPDATE ItemRecebimento'
      #9'   SET cFlgDiverg       = 1'
      '          ,nCdTabTipoDiverg = 4'
      #9' WHERE nCdItemRecebimento = @nCdItemRecebimento'
      ''
      'END'
      ''
      '--'
      '-- Se n'#227'o tiver na grade de pedido gera diverg.'
      '--'
      'IF (@nQtde_Detalhe IS NULL)'
      'BEGIN'
      ''
      #9'UPDATE ItemRecebimento'
      #9'   SET cFlgDiverg       = 1'
      '          ,nCdTabTipoDiverg = 1'
      #9' WHERE nCdItemRecebimentoPai = @nCdItemRecebimento'
      #9'   AND nCdTipoItemPed        = 4 '
      #9'   AND nCdProduto            = @nCdProdutoDetalhe'
      ''
      #9'UPDATE ItemRecebimento'
      #9'   SET cFlgDiverg       = 1'
      '          ,nCdTabTipoDiverg = 1'
      #9' WHERE nCdItemRecebimento = @nCdItemRecebimento'
      ''
      'END'
      '')
    Left = 336
    Top = 480
  end
  object SP_INSERI_DIVERGENCIA_PARCELA_RECEB: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_INSERI_DIVERGENCIA_PARCELA_RECEB;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 128
    Top = 528
  end
  object qryTesteDupNF: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNrDocto'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = '0'
      end
      item
        Name = 'cSerieDocto'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 2
        Size = 2
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdRecebimento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 nCdRecebimento'
      '  FROM Recebimento'
      ' WHERE cNrDocto    = :cNrDocto'
      '   AND cSerieDocto = :cSerieDocto'
      '   AND nCdTerceiro = :nCdTerceiro'
      '   AND nCdRecebimento <> :nCdRecebimento')
    Left = 336
    Top = 512
    object qryTesteDupNFnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
  end
  object qryCalculaST: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nValTotal'
        DataType = ftFloat
        Size = 1
        Value = 0.000000000000000000
      end
      item
        Name = 'nValIPI'
        DataType = ftFloat
        Size = 1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto      int'
      '       ,@nCdTerceiro     int'
      '       ,@nValTotal       decimal(12,2)'
      '       ,@nValBaseICMS    decimal(12,2)'
      '       ,@nValIPI         decimal(12,2)'
      '       ,@nValCredito     decimal(12,2)'
      '       ,@nValICMSSub     decimal(12,2)'
      '       ,@nPercICMSSub    decimal(12,2)'
      '       ,@nPercBCICMSSub  decimal(12,2)'
      '       ,@nPercIVA        decimal(12,2)'
      '       ,@nPercCredICMS   decimal(12,2)'
      '       ,@nValCreditoICMS decimal(12,2)'
      ''
      'Set @nCdProduto   = :nCdProduto'
      'Set @nCdTerceiro  = :nCdTerceiro'
      'Set @nValTotal    = :nValTotal'
      'Set @nValIPI      = :nValIPI'
      ''
      'Set @nValBaseICMS = 0'
      'Set @nValCredito  = 0'
      'Set @nValICMSSub  = 0'
      ''
      'IF EXISTS(SELECT 1'
      '            FROM ProdutoFornecedor'
      '           WHERE nCdProduto  = @nCdProduto'
      '             AND nCdTerceiro = @nCdTerceiro)'
      'BEGIN'
      ''
      #9'SELECT @nPercICMSSub   = nPercICMSSub'
      #9#9'  ,@nPercBCICMSSub = nPercBCICMSSub'
      '          ,@nPercIVA       = nPercIVA'
      '          ,@nPercCredICMS  = nPercCredICMS'
      #9'  FROM ProdutoFornecedor'
      #9' WHERE nCdProduto      = @nCdProduto'
      #9'   AND nCdTerceiro     = @nCdTerceiro'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      #9'SELECT @nPercICMSSub   = nPercICMSSub'
      #9#9'  ,@nPercBCICMSSub = nPercBCICMSSub'
      '          ,@nPercIVA       = nPercIVA'
      '          ,@nPercCredICMS  = nPercCredICMS'
      #9'  FROM ProdutoFornecedor'
      
        #9' WHERE nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      ''
      'IF (@nPercBCICMSSub <= 0) Set @nPercBCICMSSub = 100'
      ''
      'IF (@nPercICMSSub > 0) '
      'BEGIN'
      ''
      
        '    Set @nValBaseICMS    = ((@nValTotal  + @nValIPI)* (@nPercBCI' +
        'CMSSub / 100)) * (1+(@nPercIVA/100))'
      '    Set @nValCreditoICMS = (@nValTotal   * (@nPercCredICMS/100))'
      '    Set @nValICMSSub     = @nValBaseICMS * (@nPercICMSSub / 100)'
      '    Set @nValICMSSub     = @nValICMSSub  - @nValCreditoICMS'
      ''
      'END'
      ''
      'SELECT IsNull(@nValICMSSub,0) nValICMSSub'
      '      ,IsNull(@nPercICMSSub,0) nPercICMSSub')
    Left = 432
    Top = 480
    object qryCalculaSTnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
    object qryCalculaSTnPercICMSSub: TBCDField
      FieldName = 'nPercICMSSub'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object qryItemEtiqueta: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, nQtde'
      '  FROM ItemRecebimento'
      ' WHERE nCdRecebimento = :nPK'
      '   AND nCdTipoItemPed IN (2,4)'
      'ORDER BY cNmItem')
    Left = 368
    Top = 512
    object qryItemEtiquetanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItemEtiquetanQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 12
    end
  end
  object SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 96
    Top = 496
  end
  object qryCFOP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Precision = 5
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM tabCFOP'
      'WHERE cCFOP = :nPK')
    Left = 464
    Top = 480
    object qryCFOPnCdCFOP: TAutoIncField
      FieldName = 'nCdCFOP'
      ReadOnly = True
    end
    object qryCFOPcCFOP: TStringField
      FieldName = 'cCFOP'
      Size = 5
    end
    object qryCFOPcCFOPPai: TStringField
      FieldName = 'cCFOPPai'
      Size = 5
    end
    object qryCFOPcNmCFOP: TStringField
      FieldName = 'cNmCFOP'
      Size = 150
    end
    object qryCFOPcFlgGeraLivroFiscal: TIntegerField
      FieldName = 'cFlgGeraLivroFiscal'
    end
    object qryCFOPcFlgGeraCreditoICMS: TIntegerField
      FieldName = 'cFlgGeraCreditoICMS'
    end
    object qryCFOPcFlgGeraCreditoIPI: TIntegerField
      FieldName = 'cFlgGeraCreditoIPI'
    end
    object qryCFOPcFlgGeraCreditoPIS: TIntegerField
      FieldName = 'cFlgGeraCreditoPIS'
    end
    object qryCFOPcFlgGeraCreditoCOFINS: TIntegerField
      FieldName = 'cFlgGeraCreditoCOFINS'
    end
    object qryCFOPcFlgImportacao: TIntegerField
      FieldName = 'cFlgImportacao'
    end
    object qryCFOPnCdTipoICMS: TIntegerField
      FieldName = 'nCdTipoICMS'
    end
    object qryCFOPnCdTipoIPI: TIntegerField
      FieldName = 'nCdTipoIPI'
    end
  end
  object qryEnderecoEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cUF'
      'FROM endereco'
      'WHERE nCdTerceiro = :nPK'
      'AND nCdStatus=1'
      'AND nCdTipoEnd = 1'
      ''
      ''
      ''
      '')
    Left = 240
    Top = 512
    object qryEnderecoEmpresanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryEnderecoEmpresacUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
  end
  object qryTipoTributacaoICMS: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from tipoTributacaoICMS'
      'where nCdTipoTributacaoICMS = :nPK')
    Left = 240
    Top = 480
    object qryTipoTributacaoICMSnCdTipoTributacaoICMS: TAutoIncField
      FieldName = 'nCdTipoTributacaoICMS'
      ReadOnly = True
    end
    object qryTipoTributacaoICMScCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 2
    end
    object qryTipoTributacaoICMScNmTipoTributacaoICMS: TStringField
      FieldName = 'cNmTipoTributacaoICMS'
      Size = 100
    end
  end
  object qryTipoTributacaoICMS_Aux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 2
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      ' FROM TipoTributacaoICMS'
      'WHERE cCDST = :nPK')
    Left = 208
    Top = 482
    object qryTipoTributacaoICMS_AuxnCdTipoTributacaoICMS: TAutoIncField
      FieldName = 'nCdTipoTributacaoICMS'
      ReadOnly = True
    end
    object qryTipoTributacaoICMS_AuxcCdST: TStringField
      FieldName = 'cCdST'
      FixedChar = True
      Size = 2
    end
    object qryTipoTributacaoICMS_AuxcNmTipoTributacaoICMS: TStringField
      FieldName = 'cNmTipoTributacaoICMS'
      Size = 100
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 64
    Top = 464
  end
  object qryProdutoFornecedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto    int'
      '       ,@nCdProdutoPai int'
      '       ,@nCdTerceiro   int'
      ''
      'Set @nCdProduto  = :nPK'
      'Set @nCdTerceiro = :nCdTerceiro'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'SELECT nPercIPI'
      '      ,nPercIVA'
      '      ,nPercBCICMSSub'
      '      ,nPercICMSSub'
      '  FROM ProdutoFornecedor'
      ' WHERE (   nCdProduto = @nCdProduto'
      '        OR nCdProduto = @nCdProdutoPai)'
      '   AND nCdTerceiro = @nCdTerceiro')
    Left = 460
    Top = 515
    object qryProdutoFornecedornPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercIVA: TBCDField
      FieldName = 'nPercIVA'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercBCICMSSub: TBCDField
      FieldName = 'nPercBCICMSSub'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercICMSSub: TBCDField
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
  end
  object qryChecaEnderecoUFTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cUF'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 2
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 nCdEndereco'
      '  FROM Endereco'
      ' WHERE nCdTerceiro = :nCdTerceiro'
      '   AND cUF         = :cUF'
      '   AND nCdStatus   = 1'
      '')
    Left = 500
    Top = 515
  end
  object qryGrupoImposto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      ''
      'DECLARE @nCdProduto      int'
      '       ,@nCdDepartamento int'
      '       ,@nCdCategoria    int'
      '       ,@nCdSubCategoria int'
      '       ,@nCdSegmento     int'
      '       ,@nCdGrupoImposto int'
      '       ,@cUnidadeMedida  char(3)'
      '       ,@nCdTipoICMS     int'
      '  '#9'   ,@nAliqIPI        int'
      '       ,@cCdSTIPI        char(2)'
      ''
      'SET @nCdProduto = :nPK'
      ''
      'SELECT @nCdDepartamento = nCdDepartamento'
      '      ,@nCdCategoria    = nCdCategoria'
      ' '#9'  ,@nCdSubCategoria = nCdSubCategoria'
      #9'  ,@nCdSegmento     = nCdSegmento'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'SET @nCdGrupoImposto = NULL'
      ''
      'SELECT @nCdGrupoImposto = nCdGrupoImposto'
      '  FROM GrupoImpostoProduto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'IF (@nCdGrupoImposto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdGrupoImposto = nCdGrupoImposto'
      '      FROM GrupoImpostoProduto'
      '     WHERE nCdDepartamento = @nCdDepartamento'
      '       AND nCdCategoria    = @nCdCategoria'
      '       AND nCdSubCategoria = @nCdSubCategoria'
      '       AND nCdSegmento     = @nCdSegmento'
      'END'
      ''
      'IF (@nCdGrupoImposto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdGrupoImposto = nCdGrupoImposto'
      '      FROM GrupoImpostoProduto'
      '     WHERE nCdDepartamento = @nCdDepartamento'
      '       AND nCdCategoria    = @nCdCategoria'
      '       AND nCdSubCategoria = @nCdSubCategoria'
      '       AND nCdSegmento     IS NULL'
      'END'
      ''
      'IF (@nCdGrupoImposto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdGrupoImposto = nCdGrupoImposto'
      '      FROM GrupoImpostoProduto'
      '     WHERE nCdDepartamento = @nCdDepartamento'
      '       AND nCdCategoria    = @nCdCategoria'
      '       AND nCdSubCategoria IS NULL'
      '       AND nCdSegmento     IS NULL'
      'END'
      ''
      'IF (@nCdGrupoImposto IS NULL)'
      'BEGIN'
      ''
      '    SELECT @nCdGrupoImposto = nCdGrupoImposto'
      '      FROM GrupoImpostoProduto'
      '     WHERE nCdDepartamento = @nCdDepartamento'
      '       AND nCdCategoria    IS NULL'
      '       AND nCdSubCategoria IS NULL'
      '       AND nCdSegmento     IS NULL'
      'END'
      ''
      ''
      'SELECT cNCM'
      '  FROM GrupoImposto'
      ' WHERE nCdGrupoImposto = @nCdGrupoImposto'
      '')
    Left = 368
    Top = 479
    object qryGrupoImpostocNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
  end
  object qryVerificaMovEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT DISTINCT 1'
      '  FROM MovEstoque'
      ' WHERE cOBS       = '#39'RECEB No '#39' + CAST(:nPK as varchar)'
      '   AND nCdProduto IN (SELECT nCdProduto '
      '                        FROM Produto'
      '                       WHERE nCdProdutoPai = :nCdProduto)')
    Left = 536
    Top = 480
  end
  object qryNaturezaOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        Size = -1
        Value = Null
      end
      item
        Name = 'cCFOPPedido'
        DataType = ftString
        Size = 2
        Value = Null
      end
      item
        Name = 'cFlgEntSai'
        DataType = ftString
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdRecebimento int'
      '       ,@cCFOP          char(4)'
      '       ,@cUFOrigem      char(2)'
      '       ,@cUFDestino     char(2)'
      ''
      'SET @nCdRecebimento = :nCdRecebimento'
      'SET @cCFOP          = :cCFOPPedido'
      ''
      'SELECT @cUFOrigem  = cUFOrigemNF'
      '      ,@cUFDestino = (CASE WHEN Recebimento.nCdLoja IS NULL'
      
        '                           THEN (SELECT cUF -- utiliza endere'#231'o ' +
        'do terceiro da empresa'
      '                                   FROM Empresa'
      
        '                                        INNER JOIN Endereco ON E' +
        'ndereco.nCdTerceiro = Empresa.nCdTerceiroEmp'
      
        '                                  WHERE Empresa.nCdEmpresa  = Re' +
        'cebimento.nCdEmpresa'
      
        '                                    AND Endereco.nCdTipoEnd = 1 ' +
        '-- endere'#231'o administrativo'
      '                                    AND Endereco.nCdStatus  = 1)'
      
        '                           ELSE (SELECT cUF -- utiliza endere'#231'o ' +
        'do terceiro da loja'
      '                                   FROM Loja'
      
        '                                        INNER JOIN Endereco ON E' +
        'ndereco.nCdTerceiro = Loja.nCdTerceiro'
      
        '                                  WHERE Loja.nCdLoja        = Re' +
        'cebimento.nCdLoja'
      
        '                                    AND Endereco.nCdTipoEnd = 1 ' +
        '-- endere'#231'o administrativo'
      '                                    AND Endereco.nCdStatus  = 1)'
      '                      END)'
      '  FROM Recebimento'
      ' WHERE nCdRecebimento = @nCdRecebimento'
      ''
      'SELECT TOP 1'
      '       CASE WHEN @cUFOrigem = @cUFDestino'
      
        '            THEN NatDestino.cCFOPInterno -- utiliza CFOP dentro ' +
        'do estado'
      
        '            ELSE NatDestino.cCFOPExterno -- utiliza CFOP fora do' +
        ' estado'
      '       END cCFOP'
      '  FROM NaturezaOperacao NatOrigem'
      
        '       LEFT JOIN NaturezaOperacao NatDestino ON NatDestino.nCdNa' +
        'turezaOperacao = NatOrigem.nCdNaturezaOperacaoEntSai'
      ' WHERE NatOrigem.cFlgEntSai     = :cFlgEntSai'
      
        '   AND ((NatOrigem.cCFOPInterno = @cCFOP) OR (NatOrigem.cCFOPExt' +
        'erno = @cCFOP))'
      '')
    Left = 536
    Top = 515
    object qryNaturezaOperacaocCFOP: TStringField
      FieldName = 'cCFOP'
      ReadOnly = True
      FixedChar = True
      Size = 4
    end
  end
  object qryDeletaReferencias: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdRecebimento int'
      ''
      'SET @nCdRecebimento = :nCdRecebimento'
      ''
      '--## Deleta referencia de Historico de Divergencia ##--  '
      'DELETE'
      '  FROM HistAutorDiverg'
      ' WHERE nCdRecebimento = @nCdRecebimento'
      '  '
      '--## Deleta centro de custo do recebimetno ##--  '
      'DELETE'
      '  FROM CentroCustoItemRecebimento'
      ' WHERE nCdItemRecebimento IN (SELECT nCdItemRecebimento'
      '                                FROM ItemRecebimento'
      
        '                               WHERE ItemRecebimento.nCdRecebime' +
        'nto = @nCdRecebimento)                                '
      ''
      
        '--## Deleta os Itens do recebimento ##--                        ' +
        '       '
      'DELETE '
      '  FROM ItemRecebimento '
      ' WHERE nCdRecebimento = @nCdRecebimento')
    Left = 704
    Top = 400
  end
  object SP_GERA_RECEBIMENTO_CD: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_RECEBIMENTO_CD;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cChaveNFeReceb'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdRecebimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 129
    Top = 497
  end
  object dsDoctoRecebido: TDataSource
    DataSet = qryDoctoRecebido
    Left = 740
    Top = 435
  end
  object qryDoctoRecebido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cChaveNFe'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRecebimento'
      '  FROM Recebimento'
      ' WHERE cChaveNFe = :cChaveNFe'
      '     '
      '')
    Left = 740
    Top = 403
    object qryDoctoRecebidonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
  end
  object menuOpcao: TPopupMenu
    Left = 60
    Top = 563
    object btnImpProtocoloRecebimento: TMenuItem
      Caption = 'Imprimir Protocolo de Recebimento'
      OnClick = btnImpProtocoloRecebimentoClick
    end
    object btnImpListaPreSelecao: TMenuItem
      Caption = 'Imprimir Listagem de Pr'#233'-Sele'#231#227'o'
      OnClick = btnImpListaPreSelecaoClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object btnEmitirEtqTermica: TMenuItem
      Caption = 'Emitir Etiqueta T'#233'rmica'
      OnClick = btnEmitirEtqTermicaClick
    end
    object btnEmitirEtqMatricial: TMenuItem
      Caption = 'Emitir Etiqueta Matricial'
      OnClick = btnEmitirEtqMatricialClick
    end
  end
  object qryVerificaItemRecebDivergencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM ItemRecebimento'
      ' WHERE nCdRecebimento = :nPK'
      '   AND ABS(nQtde - nQtdeConf) > 0')
    Left = 700
    Top = 555
  end
  object qryItemRecebidoSemPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemRecebidoSemPedidoBeforePost
    OnCalcFields = qryItemRecebidoSemPedidoCalcFields
    DataSource = DataSource1
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT ItemRecebimento.nCdItemRecebimento'
      '      ,ItemRecebimento.cCdProduto'
      '      ,ItemRecebimento.cNmItem'
      '      ,ItemRecebimento.nQtde'
      '      ,ItemRecebimento.nCdTipoPedidoRecebido'
      '  FROM ItemRecebimento'
      ' WHERE ItemRecebimento.nCdRecebimento         = :nPK'
      '   AND ItemRecebimento.nCdItemPedido         IS NULL'
      '   AND ItemRecebimento.nCdItemRecebimentoPai IS NULL')
    Left = 836
    Top = 499
    object qryItemRecebidoSemPedidonCdItemRecebimento: TIntegerField
      DisplayLabel = 'Itens Recebidos Sem Pedido|ID Item'
      FieldName = 'nCdItemRecebimento'
    end
    object qryItemRecebidoSemPedidocCdProduto: TStringField
      DisplayLabel = 'Itens Recebidos Sem Pedido|C'#243'd. Produto'
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemRecebidoSemPedidocNmItem: TStringField
      DisplayLabel = 'Itens Recebidos Sem Pedido|Descri'#231#227'o Produto'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemRecebidoSemPedidonQtde: TBCDField
      DisplayLabel = 'Itens Recebidos Sem Pedido|Qtde Rec.'
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryItemRecebidoSemPedidonCdTipoPedidoRecebido: TIntegerField
      DisplayLabel = 'Tipo de Pedido de Entrada|C'#243'd.'
      FieldName = 'nCdTipoPedidoRecebido'
      OnChange = qryItemRecebidoSemPedidonCdTipoPedidoRecebidoChange
    end
    object qryItemRecebidoSemPedidocNmTipoPedido: TStringField
      DisplayLabel = 'Tipo de Pedido de Entrada|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmTipoPedido'
      Size = 50
      Calculated = True
    end
  end
  object dsItemRecebidoSemPedido: TDataSource
    DataSet = qryItemRecebidoSemPedido
    Left = 836
    Top = 530
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '  FROM TipoPedido'
      
        ' WHERE nCdTabTipoPedido = 2    --Somente Tipo de Pedido de Entra' +
        'da'
      '   AND nCdTipoPedido    = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioTipoPedido UTP'
      
        '               WHERE UTP.nCdTipoPedido = TipoPedido.nCdTipoPedid' +
        'o'
      '                 AND UTP.nCdUsuario    = :nCdUsuario)')
    Left = 868
    Top = 499
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 869
    Top = 530
  end
  object qryTipoPedidoAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '  FROM TipoPedido'
      
        ' WHERE nCdTabTipoPedido = 2    --Somente Tipo de Pedido de Entra' +
        'da'
      '   AND nCdTipoPedido    = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioTipoPedido UTP'
      
        '               WHERE UTP.nCdTipoPedido = TipoPedido.nCdTipoPedid' +
        'o'
      '                 AND UTP.nCdUsuario    = :nCdUsuario)')
    Left = 908
    Top = 499
    object IntegerField1: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object StringField1: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object dsTipoPedidoAux: TDataSource
    DataSet = qryTipoPedidoAux
    Left = 909
    Top = 530
  end
  object ACBrNFe1: TACBrNFe
    Configuracoes.Geral.SSLLib = libCapicomDelphiSoap
    Configuracoes.Geral.SSLCryptLib = cryCapicom
    Configuracoes.Geral.SSLHttpLib = httpIndy
    Configuracoes.Geral.SSLXmlSignLib = xsMsXmlCapicom
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Geral.VersaoQRCode = veqr000
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Left = 60
    Top = 435
  end
  object qryUsuarioCad: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 568
    Top = 480
    object qryUsuarioCadnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioCadcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsUsuarioCad: TDataSource
    DataSet = qryUsuarioCad
    Left = 568
    Top = 512
  end
  object qryPopulaConfPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '       Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Produto.cEAN'
      '      ,Produto.cEANFornec'
      '  FROM ItemRecebimento'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = ItemRecebiment' +
        'o.nCdProduto'
      ' WHERE nCdRecebimento = :nPK'
      '   AND nCdTipoItemPed IN (2,4)')
    Left = 632
    Top = 552
    object qryPopulaConfPadraonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryPopulaConfPadraocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryPopulaConfPadraocEAN: TStringField
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryPopulaConfPadraocEANFornec: TStringField
      FieldName = 'cEANFornec'
      FixedChar = True
    end
  end
  object qryGravaConfPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRecebimento'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdRecebimento        int'
      '       ,@nCdItemRecebimento    int'
      '       ,@nCdItemRecebimentoPai int'
      '       ,@nCdProduto            int'
      '       ,@nQtdeCont             int'
      '       ,@nQtdeAtual            int'
      '       ,@nQtdeCalc             int'
      ''
      'SET @nCdRecebimento = :nCdRecebimento'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosBip'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosBip (cCdProduto  varchar(20)'
      '                                  ,nQtdeBipada int)'
      '                         '
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosCont'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosCont (cCdProduto varchar(20)'
      '                                   ,nQtdeCont  int)'
      '                         '
      'END'
      ''
      'TRUNCATE TABLE #TempProdutosCont'
      ''
      '--'
      '-- contabiliza produtos bipados'
      '--'
      'INSERT INTO #TempProdutosCont (cCdProduto'
      '                              ,nQtdeCont)'
      '                       SELECT cCdProduto'
      '                             ,SUM(nQtdeBipada)'
      '                         FROM #TempProdutosBip'
      '                        GROUP BY cCdProduto'
      '                        ORDER BY cCdProduto'
      '                        '
      '--'
      '-- reseta saldo conferido'
      '--'
      'UPDATE ItemRecebimento '
      '   SET nQtdeConf = 0 '
      ' WHERE nCdRecebimento = @nCdRecebimento'
      ''
      '--'
      '-- registra confer'#234'ncia de produtos'
      '--'
      'DECLARE curConfereRec CURSOR'
      #9'FOR SELECT nCdItemRecebimento'
      '              ,nCdItemRecebimentoPai'
      '              ,nCdProduto'
      '              ,nQtde'
      '          FROM ItemRecebimento'
      '         WHERE nCdRecebimento = @nCdRecebimento'
      '           AND nCdTipoItemPed IN (2,4)'
      '         ORDER BY nCdProduto'
      ''
      'OPEN curConfereRec'
      ''
      'FETCH NEXT'
      ' FROM curConfereRec'
      ' INTO @nCdItemRecebimento'
      '     ,@nCdItemRecebimentoPai'
      #9' ,@nCdProduto'
      #9' ,@nQtdeAtual'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      #9'--'
      #9'-- consulta saldo contabilizado'
      #9'--'
      #9'SET @nQtdeCont    = 0'
      #9'SELECT @nQtdeCont = nQtdeCont'
      #9'  FROM #TempProdutosCont'
      #9' WHERE CAST(cCdProduto as int) = @nCdProduto'
      '     '
      #9'--'
      #9'-- se produto possuir saldo, atualiza confer'#234'ncia'
      #9'--'
      #9'IF (ISNULL(@nQtdeCont,0) > 0)'
      #9'BEGIN'
      ''
      
        #9#9'SET @nQtdeCalc = (CASE WHEN @nQtdeCont > @nQtdeAtual AND (SELE' +
        'CT COUNT(1)'
      
        '                                                                ' +
        '    FROM ItemRecebimento'
      
        '                                                                ' +
        '   WHERE nCdRecebimento = @nCdRecebimento'
      
        '                                                                ' +
        '     AND nCdProduto     = @nCdProduto'
      
        '                                                                ' +
        '     AND nQtdeConf      = 0) > 1'
      #9#9#9#9#9#9' '#9'   THEN @nQtdeAtual'
      #9#9#9#9#9#9#9'   ELSE @nQtdeCont'
      #9#9#9#9#9#9'  END)'
      '    '
      #9#9'--'
      #9#9'-- atualiza saldo conferido do item'
      #9#9'--'
      #9#9'UPDATE ItemRecebimento'
      #9#9'   SET nQtdeConf = @nQtdeCalc'
      #9#9' WHERE nCdItemRecebimento = @nCdItemRecebimento'
      '         '
      #9#9'--'
      #9#9'-- atualiza saldo utilizado na confer'#234'ncia acima'
      #9#9'--'
      #9#9'UPDATE #TempProdutosCont'
      #9#9'   SET nQtdeCont = nQtdeCont - @nQtdeCalc'
      #9#9' WHERE cCdProduto = CAST(@nCdProduto as varchar)'
      #9#9' '
      '        --'
      '        -- Atualiza conferencia item pai.'
      '        --'
      '        UPDATE ItemRecebimento'
      '           SET nQtdeConf = (SELECT SUM(nQtdeConf)'
      '                              FROM ItemRecebimento'
      
        '                             WHERE nCdItemRecebimentoPai = @nCdI' +
        'temRecebimentoPai)'
      '         WHERE nCdItemRecebimento = @nCdItemRecebimentoPai'
      '    '
      #9'END'
      '    '
      #9'FETCH NEXT'
      '     FROM curConfereRec'
      '     INTO @nCdItemRecebimento'
      '         ,@nCdItemRecebimentoPai'
      #9'     ,@nCdProduto'
      #9'     ,@nQtdeAtual'
      '         '
      'END'
      ''
      'CLOSE curConfereRec'
      'DEALLOCATE curConfereRec')
    Left = 664
    Top = 552
  end
end
