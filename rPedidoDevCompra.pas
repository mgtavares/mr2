unit rPedidoDevCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPedidoDevCompra = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidocNmEmpresa: TStringField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidocNmTabStatusPed: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    qryItemEstoque_Grade: TADOQuery;
    qryItemEstoque_GradenCdProduto: TIntegerField;
    qryItemEstoque_GradecNmItem: TStringField;
    qryItemEstoque_GradenQtdePed: TBCDField;
    qryItemEstoque_GradenValCustoUnit: TBCDField;
    qryItemEstoque_GradenValTotalItem: TBCDField;
    qryItemEstoque_GradenCdGrade: TIntegerField;
    qryItemEstoque_GradecTituloGrade: TStringField;
    qryItemEstoque_GradecQtdeGrade: TStringField;
    QRSubDetail1: TQRSubDetail;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText32: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText15: TQRDBText;
    QRLabel22: TQRLabel;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    QRSubDetail2: TQRSubDetail;
    QRDBText16: TQRDBText;
    QRGroup3: TQRGroup;
    qryItemADnCdPedido: TIntegerField;
    QRShape3: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    qryPedidocNmContato: TStringField;
    QRSubDetail3: TQRSubDetail;
    QRGroup4: TQRGroup;
    qryItemFormula: TADOQuery;
    qryItemFormulanCdProduto: TIntegerField;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulacComposicao: TStringField;
    QRShape4: TQRShape;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape2: TQRShape;
    qryPedidocNmTerceiroRepres: TStringField;
    qryPedidonValImposto: TBCDField;
    qryPedidonValProdutos: TBCDField;
    qryPedidonValFrete: TBCDField;
    SummaryBand1: TQRBand;
    QRDBText11: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText34: TQRDBText;
    QRLabel37: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel15: TQRLabel;
    qryPedidonValOutros: TBCDField;
    QRLabel38: TQRLabel;
    QRDBText35: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText12: TQRDBText;
    QRShape5: TQRShape;
    qryItemEstoque_GradedDtEntregaIni: TDateTimeField;
    qryItemEstoque_GradedDtEntregaFim: TDateTimeField;
    qryItemEstoque_GradenQtdePrev: TBCDField;
    qryItemEstoque_GradenPercIPI: TBCDField;
    qryItemEstoque_GradenPercICMSSub: TBCDField;
    qryPedidocNmUsuarioComprador: TStringField;
    QRShape1: TQRShape;
    QRShape8: TQRShape;
    qryItemEstoque_GradecSiglaUnidadeMedida: TStringField;
    QRDBText41: TQRDBText;
    QRLabel46: TQRLabel;
    qryItemEstoque_GradenValUnitario: TBCDField;
    qryItemEstoque_GradenValDesconto: TBCDField;
    qryItemEstoque_GradenValAcrescimo: TBCDField;
    QRDBText42: TQRDBText;
    qryItemEstoque_GradenValIPIUnitario: TBCDField;
    qryPedidonQtdeItens: TBCDField;
    QRLabel44: TQRLabel;
    QRDBText36: TQRDBText;
    qryPedidocEnderecoLocalEntrega: TStringField;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRShape6: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel16: TQRLabel;
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedidoDevCompra: TrptPedidoDevCompra;

implementation

{$R *.dfm}

procedure TrptPedidoDevCompra.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

  QRLabel22.Caption   := 'Grade' ;
  QRSubDetail1.Height := 32 ;

  if (qryItemEstoque_GradecTituloGrade.Value = '') then
  begin
      QRLabel22.Caption := 'Item Estoque' ;
      QRSubDetail1.Height := 16 ;
  end ;


end;

end.
