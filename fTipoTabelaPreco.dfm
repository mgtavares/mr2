inherited frmTipoTabelaPreco: TfrmTipoTabelaPreco
  Caption = 'Tipo de Tabela de Pre'#231'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 92
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoTabPreco'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 92
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmTipoTabPreco'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM TIPOTABPRECO'
      'WHERE nCdTipoTabPreco = :nPK')
    object qryMasternCdTipoTabPreco: TIntegerField
      FieldName = 'nCdTipoTabPreco'
    end
    object qryMastercNmTipoTabPreco: TStringField
      FieldName = 'cNmTipoTabPreco'
      Size = 50
    end
  end
end
