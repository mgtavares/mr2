inherited frmMRP_DetalhaEmpenho: TfrmMRP_DetalhaEmpenho
  Left = 240
  Top = 151
  Width = 1030
  Caption = 'Detalhamento Estoque Empenhado'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 89
    Width = 1014
    Height = 373
  end
  inherited ToolBar1: TToolBar
    Width = 1014
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1014
    Height = 60
    Align = alTop
    Caption = ' Produto '
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 16
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 111
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
      FocusControl = DBEdit2
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 8
      Top = 32
      Width = 100
      Height = 21
      DataField = 'nCdProduto'
      DataSource = dsProduto
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 111
      Top = 32
      Width = 658
      Height = 21
      DataField = 'cNmProduto'
      DataSource = dsProduto
      TabOrder = 1
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 89
    Width = 1014
    Height = 373
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 369
    ClientRectLeft = 4
    ClientRectRight = 1010
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Detalhamento'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1006
        Height = 345
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsEstoqueEmpenhado
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = ',0'
              Kind = skSum
              Column = cxGrid1DBTableView1nQtdeEstoqueEmpenhado
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          Styles.Header = frmMenu.ConteudoCaixa
          object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
            DataBinding.FieldName = 'nCdEmpresa'
          end
          object cxGrid1DBTableView1cNmLocalEstoqueEmp: TcxGridDBColumn
            Caption = 'Estoque Empenh.'
            DataBinding.FieldName = 'cNmLocalEstoqueEmp'
            Width = 115
          end
          object cxGrid1DBTableView1cNmLocalEstoqueReq: TcxGridDBColumn
            Caption = 'Estoque Requisit.'
            DataBinding.FieldName = 'cNmLocalEstoqueReq'
            Width = 115
          end
          object cxGrid1DBTableView1cOrigemEmpenhao: TcxGridDBColumn
            DataBinding.FieldName = 'cOrigemEmpenhao'
            Width = 61
          end
          object cxGrid1DBTableView1NmeroOP: TcxGridDBColumn
            DataBinding.FieldName = 'N'#250'mero OP'
            Width = 97
          end
          object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
            DataBinding.FieldName = 'nCdPedido'
            Width = 60
          end
          object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
            DataBinding.FieldName = 'dDtPedido'
            Width = 109
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 235
          end
          object cxGrid1DBTableView1nQtdeEstoqueEmpenhado: TcxGridDBColumn
            DataBinding.FieldName = 'nQtdeEstoqueEmpenhado'
            Width = 109
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 184
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 320
    Top = 182
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 320
    Top = 216
  end
  object qryEstoqueEmpenhado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto int'
      ''
      'SET @nCdProduto = :nPK'
      ''
      'SELECT EstoqueEmpenhado.nCdEmpresa'
      '      ,UPPER(cNmTabTipoOrigemEmpenho) as cOrigemEmpenhao'
      '      ,EstoqueEmpenhado.dDtEmpenho'
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN Pedido.nCdPedido'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN PedidoOP.nCdPedido'
      '            ELSE NULL'
      '       END as nCdPedido'
      '      ,OrdemProducao.cNumeroOP as '#39'N'#250'mero OP'#39
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN TerceiroPedido.cNmTerceiro'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN TerceiroOP.cNmTerceiro'
      '            ELSE NULL'
      '       END as cNmTerceiro'
      '      ,nQtdeEstoqueEmpenhado'
      '      ,LocalEstEmp.cNmLocalEstoque as cNmLocalEstoqueEmp'
      '      ,LocalEstReq.cNmLocalEstoque as cNmLocalEstoqueReq'
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN Pedido.dDtPedido'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN PedidoOP.dDtPedido'
      '            ELSE NULL'
      '       END as dDtPedido'
      '  FROM EstoqueEmpenhado'
      
        '       LEFT JOIN ItemPedido               ON ItemPedido.nCdItemP' +
        'edido                     = EstoqueEmpenhado.nCdItemPedido'
      
        '       LEFT JOIN Pedido                   ON Pedido.nCdPedido   ' +
        '                          = ItemPedido.nCdPedido'
      
        '       LEFT JOIN OrdemProducao            ON OrdemProducao.nCdOr' +
        'demProducao               = EstoqueEmpenhado.nCdOrdemProducao'
      
        '       LEFT JOIN Pedido PedidoOP          ON PedidoOP.nCdPedido ' +
        '                          = OrdemProducao.nCdPedido'
      
        '       LEFT JOIN Terceiro TerceiroPedido  ON TerceiroPedido.nCdT' +
        'erceiro                   = Pedido.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroOP      ON TerceiroOP.nCdTerce' +
        'iro                       = PedidoOP.ncdTerceiro'
      
        '       LEFT JOIN TabTipoOrigemEmpenho     ON TabTipoOrigemEmpenh' +
        'o.nCdTabTipoOrigemEmpenho = EstoqueEmpenhado.nCdTabTipoOrigemEmp' +
        'enho'
      
        '       LEFT JOIN LocalEstoque LocalEstEmp ON LocalEstEmp.nCdLoca' +
        'lEstoque                  = EstoqueEmpenhado.nCdLocalEstoque'
      
        '       LEFT JOIN LocalEstoque LocalEstReq ON LocalEstReq.nCdLoca' +
        'lEstoque                  = EstoqueEmpenhado.nCdLocalEstoqueOrig' +
        'em'
      
        ' WHERE EstoqueEmpenhado.nCdProduto                        = @nCd' +
        'Produto'
      
        '   AND ISNULL(EstoqueEmpenhado.nCdStatus,1)               = 1   ' +
        '       -- ativo'
      
        '   AND ISNULL(EstoqueEmpenhado.nCdTabTipoOrigemEmpenho,1) IN (1,' +
        '2,3,4) -- venda / compra / manual / web'
      
        '   AND EstoqueEmpenhado.nCdEstoqueEmpenhoPai              IS NUL' +
        'L      -- sem empenho totalizador'
      
        '   AND EstoqueEmpenhado.nCdItemTransfEst                  IS NUL' +
        'L      -- sem refer'#234'ncia a transfer'#234'ncia'
      ' UNION'
      ' SELECT EstoqueEmpenhado.nCdEmpresa'
      '      ,UPPER((SELECT TOP 1 cNmTabTipoOrigemEmpenho'
      '                FROM EstoqueEmpenhado EstoqueEmp'
      
        '                     LEFT JOIN TabTipoOrigemEmpenho ON TabTipoOr' +
        'igemEmpenho.nCdTabTipoOrigemEmpenho = EstoqueEmp.nCdTabTipoOrige' +
        'mEmpenho'
      
        '         WHERE EstoqueEmp.nCdEstoqueEmpenhoPai = EstoqueEmpenhad' +
        'o.nCdEstoqueEmpenhado)) as cOrigemEmpenhao'
      '      ,EstoqueEmpenhado.dDtEmpenho'
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN Pedido.nCdPedido'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN PedidoOP.nCdPedido'
      '            ELSE NULL'
      '       END as nCdPedido'
      '      ,OrdemProducao.cNumeroOP as '#39'N'#250'mero OP'#39
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN TerceiroPedido.cNmTerceiro'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN TerceiroOP.cNmTerceiro'
      '            ELSE NULL'
      '       END as cNmTerceiro'
      '      ,nQtdeEstoqueEmpenhado'
      '      ,LocalEstEmp.cNmLocalEstoque as cNmLocalEstoqueEmp'
      '      ,LocalEstReq.cNmLocalEstoque as cNmLocalEstoqueReq'
      
        '      ,CASE WHEN EstoqueEmpenhado.nCdItemPedido    IS NOT NULL T' +
        'HEN Pedido.dDtPedido'
      
        '            WHEN EstoqueEmpenhado.nCdOrdemProducao IS NOT NULL T' +
        'HEN PedidoOP.dDtPedido'
      '            ELSE NULL'
      '       END as dDtPedido'
      '  FROM EstoqueEmpenhado'
      
        '       LEFT JOIN ItemPedido               ON ItemPedido.nCdItemP' +
        'edido                     = EstoqueEmpenhado.nCdItemPedido'
      
        '       LEFT JOIN Pedido                   ON Pedido.nCdPedido   ' +
        '                          = ItemPedido.nCdPedido'
      
        '       LEFT JOIN OrdemProducao            ON OrdemProducao.nCdOr' +
        'demProducao               = EstoqueEmpenhado.nCdOrdemProducao'
      
        '       LEFT JOIN Pedido PedidoOP          ON PedidoOP.nCdPedido ' +
        '                          = OrdemProducao.nCdPedido'
      
        '       LEFT JOIN Terceiro TerceiroPedido  ON TerceiroPedido.nCdT' +
        'erceiro                   = Pedido.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroOP      ON TerceiroOP.nCdTerce' +
        'iro                       = PedidoOP.ncdTerceiro'
      
        '       LEFT JOIN TabTipoOrigemEmpenho     ON TabTipoOrigemEmpenh' +
        'o.nCdTabTipoOrigemEmpenho = EstoqueEmpenhado.nCdTabTipoOrigemEmp' +
        'enho'
      
        '       LEFT JOIN LocalEstoque LocalEstEmp ON LocalEstEmp.nCdLoca' +
        'lEstoque                  = EstoqueEmpenhado.nCdLocalEstoque'
      
        '       LEFT JOIN LocalEstoque LocalEstReq ON LocalEstReq.nCdLoca' +
        'lEstoque                  = EstoqueEmpenhado.nCdLocalEstoqueOrig' +
        'em'
      ' WHERE EstoqueEmpenhado.nCdProduto              = @nCdProduto'
      '   AND ISNULL(EstoqueEmpenhado.nCdStatus,1)     = 1     -- ativo'
      
        '   AND EstoqueEmpenhado.nCdTabTipoOrigemEmpenho IS NULL -- apena' +
        's totalizadores n'#227'o possuem origem'
      '   AND nQtdeEstoqueEmpenhado   > 0'
      ' ORDER BY dDtEmpenho'
      '')
    Left = 356
    Top = 185
    object qryEstoqueEmpenhadonCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryEstoqueEmpenhadocOrigemEmpenhao: TStringField
      DisplayLabel = 'Origem'
      FieldName = 'cOrigemEmpenhao'
      ReadOnly = True
      Size = 6
    end
    object qryEstoqueEmpenhadodDtEmpenho: TDateTimeField
      DisplayLabel = 'Data Empenho'
      FieldName = 'dDtEmpenho'
    end
    object qryEstoqueEmpenhadonCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
      ReadOnly = True
    end
    object qryEstoqueEmpenhadoNmeroOP: TStringField
      FieldName = 'N'#250'mero OP'
    end
    object qryEstoqueEmpenhadocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 100
    end
    object qryEstoqueEmpenhadonQtdeEstoqueEmpenhado: TBCDField
      DisplayLabel = 'Qt. Empenhada'
      FieldName = 'nQtdeEstoqueEmpenhado'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryEstoqueEmpenhadodDtPedido: TDateTimeField
      DisplayLabel = 'Data Pedido'
      FieldName = 'dDtPedido'
      ReadOnly = True
    end
    object qryEstoqueEmpenhadocNmLocalEstoqueEmp: TStringField
      FieldName = 'cNmLocalEstoqueEmp'
      ReadOnly = True
      Size = 50
    end
    object qryEstoqueEmpenhadocNmLocalEstoqueReq: TStringField
      FieldName = 'cNmLocalEstoqueReq'
      ReadOnly = True
      Size = 50
    end
  end
  object dsEstoqueEmpenhado: TDataSource
    DataSet = qryEstoqueEmpenhado
    Left = 356
    Top = 217
  end
end
