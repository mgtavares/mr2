inherited frmTipoImposto: TfrmTipoImposto
  Left = 100
  Top = 116
  Width = 1158
  Height = 613
  Caption = 'frmTipoImposto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1150
    Height = 561
  end
  object Label1: TLabel [1]
    Left = 134
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 123
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 53
    Top = 93
    Width = 119
    Height = 13
    Alignment = taRightJustify
    Caption = 'Compet'#234'ncia Tribut'#225'ria'
  end
  object Label4: TLabel [4]
    Left = 74
    Top = 118
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Guia Recolhimento'
  end
  object Label5: TLabel [5]
    Left = 94
    Top = 142
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Credor'
  end
  object Label6: TLabel [6]
    Left = 66
    Top = 165
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria Financeira'
  end
  object Label7: TLabel [7]
    Left = 102
    Top = 189
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie T'#237'tulo'
  end
  object Label8: TLabel [8]
    Left = 89
    Top = 214
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie Previs'#226'o'
  end
  object Label9: TLabel [9]
    Left = 83
    Top = 240
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Reten'#231#227'o'
  end
  object Label10: TLabel [10]
    Left = 112
    Top = 262
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pessoa'
  end
  object Label11: TLabel [11]
    Left = 52
    Top = 286
    Width = 120
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor M'#237'nimo Reten'#231#227'o'
    FocusControl = DBEdit13
  end
  object Label12: TLabel [12]
    Left = 328
    Top = 286
    Width = 121
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor M'#225'ximo Reten'#231#227'o'
    FocusControl = DBEdit14
  end
  object Label13: TLabel [13]
    Left = 364
    Top = 310
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero  de Dias'
    FocusControl = DBEdit15
  end
  object Label14: TLabel [14]
    Left = 91
    Top = 309
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dia Vencimento'
    FocusControl = DBEdit16
  end
  object Label15: TLabel [15]
    Left = 63
    Top = 333
    Width = 109
    Height = 13
    Alignment = taRightJustify
    Caption = 'Abatimento(Sim/N'#227'o)'
  end
  object Label16: TLabel [16]
    Left = 18
    Top = 357
    Width = 155
    Height = 13
    Alignment = taRightJustify
    Caption = 'Agrupar Pagamento (Sim/N'#227'o)'
  end
  inherited ToolBar2: TToolBar
    Width = 1150
  end
  object DBEdit1: TDBEdit [18]
    Tag = 1
    Left = 177
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoImposto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [19]
    Tag = 1
    Left = 177
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmTipoImposto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 244
    Top = 87
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoCompetTributaria'
    DataSource = dsTipoCompetTributaria
    TabOrder = 17
  end
  object DBEdit6: TDBEdit [21]
    Tag = 1
    Left = 244
    Top = 111
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoGuiaRecImposto'
    DataSource = DataSource1
    TabOrder = 18
  end
  object DBEdit13: TDBEdit [22]
    Left = 177
    Top = 280
    Width = 108
    Height = 19
    DataField = 'nValMinRetencao'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit14: TDBEdit [23]
    Left = 456
    Top = 280
    Width = 129
    Height = 19
    DataField = 'nValMaxRetencao'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit15: TDBEdit [24]
    Left = 456
    Top = 304
    Width = 41
    Height = 19
    DataField = 'iNumeroDias'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit16: TDBEdit [25]
    Left = 177
    Top = 304
    Width = 44
    Height = 19
    DataField = 'iDiaVencto'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit19: TDBEdit [26]
    Tag = 1
    Left = 244
    Top = 136
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 19
  end
  object DBEdit20: TDBEdit [27]
    Tag = 1
    Left = 244
    Top = 160
    Width = 650
    Height = 19
    DataField = 'cNmCategFinanc'
    DataSource = dsCategFinanc
    TabOrder = 20
  end
  object DBEdit21: TDBEdit [28]
    Tag = 1
    Left = 244
    Top = 184
    Width = 650
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTitPagto
    TabOrder = 21
  end
  object DBEdit22: TDBEdit [29]
    Tag = 1
    Left = 244
    Top = 208
    Width = 650
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTitProv
    TabOrder = 22
  end
  object DBEdit23: TDBEdit [30]
    Tag = 1
    Left = 244
    Top = 232
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoRetencaoImposto'
    DataSource = dsTipoRetencao
    TabOrder = 23
  end
  object DBEdit24: TDBEdit [31]
    Tag = 1
    Left = 244
    Top = 256
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoPessoa'
    DataSource = dsTipoPessoa
    TabOrder = 24
  end
  object edtCompetTributaria: TER2LookupDBEdit [32]
    Left = 177
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoCompetTributaria'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 1016
    QueryLookup = qryTipoCompetTributaria
  end
  object edtGuiaRecolhimento: TER2LookupDBEdit [33]
    Left = 177
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoGuiaRecImposto'
    DataSource = dsMaster
    TabOrder = 4
    CodigoLookup = 1017
    QueryLookup = qryTipoGuiaRecolhimento
  end
  object edtTerceiroCredor: TER2LookupDBEdit [34]
    Left = 177
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroCredor'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = edtTerceiroCredorExit
    CodigoLookup = 17
    QueryLookup = qryTerceiro
  end
  object edtCategFinanc: TER2LookupDBEdit [35]
    Left = 177
    Top = 160
    Width = 65
    Height = 19
    DataField = 'nCdCategFinanc'
    DataSource = dsMaster
    TabOrder = 6
    OnBeforeLookup = edtCategFinancBeforeLookup
    CodigoLookup = 32
    WhereAdicional.Strings = (
      '')
    QueryLookup = qryCategFinanc
  end
  object edtEspTitPagto: TER2LookupDBEdit [36]
    Left = 177
    Top = 184
    Width = 65
    Height = 19
    DataField = 'nCdEspTitPagto'
    DataSource = dsMaster
    TabOrder = 7
    OnBeforeLookup = edtEspTitPagtoBeforeLookup
    CodigoLookup = 10
    QueryLookup = qryEspTitPagto
  end
  object edtEspTitPrev: TER2LookupDBEdit [37]
    Left = 177
    Top = 208
    Width = 65
    Height = 19
    DataField = 'nCdEspTitProv'
    DataSource = dsMaster
    TabOrder = 8
    OnBeforeLookup = edtEspTitPrevBeforeLookup
    CodigoLookup = 10
    QueryLookup = qryEspTitProv
  end
  object edtTipoRetencao: TER2LookupDBEdit [38]
    Left = 177
    Top = 232
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoRetencaoImposto'
    DataSource = dsMaster
    TabOrder = 9
    CodigoLookup = 1018
    QueryLookup = qryTipoRetencao
  end
  object edtTipoPessoa: TER2LookupDBEdit [39]
    Left = 177
    Top = 256
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoPessoa'
    DataSource = dsMaster
    TabOrder = 10
    CodigoLookup = 1019
    QueryLookup = qryTipoPessoa
  end
  object DBLookupComboBox1: TDBLookupComboBox [40]
    Left = 177
    Top = 328
    Width = 55
    Height = 19
    Ctl3D = False
    DataField = 'cFlgAbatimento'
    DataSource = dsMaster
    DropDownRows = 2
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsAbatimento
    ParentCtl3D = False
    TabOrder = 15
  end
  object DBLookupComboBox3: TDBLookupComboBox [41]
    Left = 177
    Top = 352
    Width = 55
    Height = 19
    DataField = 'cFlgAgrupaPagamento'
    DataSource = dsMaster
    KeyField = 'nCdTabSimNao'
    ListField = 'cNmTabSimNao'
    ListSource = dsAgruparPagamento
    TabOrder = 16
  end
  object cxPageControl1: TcxPageControl [42]
    Left = 16
    Top = 380
    Width = 817
    Height = 200
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 25
    ClientRectBottom = 200
    ClientRectRight = 817
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Munic'#237'pio X Credor'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 817
        Height = 177
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTipoImpostoMunicipio
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh2ColExit
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMunicipio'
            Footers = <>
            Title.Caption = 'Munic'#237'pio|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmMunicipio'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Munic'#237'pio|Descri'#231#227'o'
            Width = 270
          end
          item
            EditButtons = <>
            FieldName = 'nCdTerceiroCredor'
            Footers = <>
            Title.Caption = 'Terceiro Credor|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiroCredor'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Terceiro Credor|Nome'
            Width = 354
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoImposto'
      'WHERE nCdTipoImposto = :nPk')
    Left = 600
    Top = 184
    object qryMasternCdTipoImposto: TIntegerField
      FieldName = 'nCdTipoImposto'
    end
    object qryMastercNmTipoImposto: TStringField
      FieldName = 'cNmTipoImposto'
      Size = 50
    end
    object qryMasternCdTabTipoCompetTributaria: TIntegerField
      FieldName = 'nCdTabTipoCompetTributaria'
    end
    object qryMasternCdTabTipoGuiaRecImposto: TIntegerField
      FieldName = 'nCdTabTipoGuiaRecImposto'
    end
    object qryMasternCdTerceiroCredor: TIntegerField
      FieldName = 'nCdTerceiroCredor'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdEspTitPagto: TIntegerField
      FieldName = 'nCdEspTitPagto'
    end
    object qryMasternCdEspTitProv: TIntegerField
      FieldName = 'nCdEspTitProv'
    end
    object qryMasternCdTabTipoRetencaoImposto: TIntegerField
      FieldName = 'nCdTabTipoRetencaoImposto'
    end
    object qryMasternCdTabTipoPessoa: TIntegerField
      FieldName = 'nCdTabTipoPessoa'
    end
    object qryMastercFlgAgrupaPagamento: TIntegerField
      FieldName = 'cFlgAgrupaPagamento'
    end
    object qryMastercFlgAbatimento: TIntegerField
      FieldName = 'cFlgAbatimento'
    end
    object qryMasteriNumeroDias: TIntegerField
      FieldName = 'iNumeroDias'
    end
    object qryMasteriDiaVencto: TIntegerField
      FieldName = 'iDiaVencto'
    end
    object qryMasternValMinRetencao: TBCDField
      FieldName = 'nValMinRetencao'
      DisplayFormat = '###,#0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValMaxRetencao: TBCDField
      FieldName = 'nValMaxRetencao'
      DisplayFormat = '###,#0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 640
    Top = 184
  end
  inherited qryID: TADOQuery
    Left = 680
    Top = 184
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 640
    Top = 216
  end
  inherited qryStat: TADOQuery
    Left = 608
    Top = 216
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 608
    Top = 256
  end
  object qryTipoCompetTributaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoCompetTributaria'
      '              ,cNmTabTipoCompetTributaria'
      'FROM TabTipoCompetTributaria'
      'WHERE nCdTabTipoCompetTributaria = :nPk')
    Left = 840
    Top = 144
    object qryTipoCompetTributarianCdTabTipoCompetTributaria: TIntegerField
      FieldName = 'nCdTabTipoCompetTributaria'
    end
    object qryTipoCompetTributariacNmTabTipoCompetTributaria: TStringField
      FieldName = 'cNmTabTipoCompetTributaria'
      Size = 50
    end
  end
  object dsTipoCompetTributaria: TDataSource
    DataSet = qryTipoCompetTributaria
    Left = 872
    Top = 144
  end
  object qryTipoGuiaRecolhimento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoGuiaRecImposto'
      '              ,cNmTabTipoGuiaRecImposto'
      'FROM TabTipoGuiaRecImposto'
      'WHERE nCdTabTipoGuiaRecImposto = :nPk')
    Left = 840
    Top = 176
    object qryTipoGuiaRecolhimentonCdTabTipoGuiaRecImposto: TIntegerField
      FieldName = 'nCdTabTipoGuiaRecImposto'
    end
    object qryTipoGuiaRecolhimentocNmTabTipoGuiaRecImposto: TStringField
      FieldName = 'cNmTabTipoGuiaRecImposto'
      Size = 50
    end
  end
  object dsTipoGuiaRecolhimento: TDataSource
    Left = 872
    Top = 176
  end
  object DataSource1: TDataSource
    DataSet = qryTipoGuiaRecolhimento
    Left = 640
    Top = 248
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdStatus'
      ',nCdMoeda'
      'FROM TERCEIRO'
      'WHERE nCdTerceiro = :nPK')
    Left = 840
    Top = 209
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 872
    Top = 209
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM CategFinanc'
      'WHERE nCdCategFinanc = :nPK'
      'AND cFlgRecDes = '#39'D'#39)
    Left = 840
    Top = 240
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryCategFinancnCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryCategFinanccFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
    object qryCategFinancnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryCategFinancnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCategFinancdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object dsCategFinanc: TDataSource
    DataSet = qryCategFinanc
    Left = 872
    Top = 240
  end
  object qryEspTitPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK'
      'AND cTipoMov = '#39'P'#39
      'AND cFlgPrev = 0'
      '')
    Left = 840
    Top = 274
    object IntegerField4: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object IntegerField5: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object StringField3: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField4: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object IntegerField6: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object dsEspTitPagto: TDataSource
    DataSet = qryEspTitPagto
    Left = 872
    Top = 275
  end
  object qryEspTitProv: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK'
      'AND cTipoMov = '#39'P'#39
      'AND cFlgPrev = 1')
    Left = 840
    Top = 304
    object qryEspTitProvnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitProvnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitProvcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitProvcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitProvcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object dsEspTitProv: TDataSource
    DataSet = qryEspTitProv
    Left = 872
    Top = 304
  end
  object qryTipoRetencao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoRetencaoImposto'
      '              ,cNmTabTipoRetencaoImposto'
      'FROM TabTipoRetencaoImposto'
      'WHERE nCdTabTipoRetencaoImposto = :nPk')
    Left = 840
    Top = 336
    object qryTipoRetencaonCdTabTipoRetencaoImposto: TIntegerField
      FieldName = 'nCdTabTipoRetencaoImposto'
    end
    object qryTipoRetencaocNmTabTipoRetencaoImposto: TStringField
      FieldName = 'cNmTabTipoRetencaoImposto'
      Size = 50
    end
  end
  object dsTipoRetencao: TDataSource
    DataSet = qryTipoRetencao
    Left = 872
    Top = 336
  end
  object qryTipoPessoa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoPessoa'
      '             ,cNmTabTipoPessoa '
      'FROM TabTipoPessoa'
      'WHERE nCdTabTipoPessoa = :nPk')
    Left = 840
    Top = 368
    object qryTipoPessoanCdTabTipoPessoa: TIntegerField
      FieldName = 'nCdTabTipoPessoa'
    end
    object qryTipoPessoacNmTabTipoPessoa: TStringField
      FieldName = 'cNmTabTipoPessoa'
      Size = 50
    end
  end
  object dsTipoPessoa: TDataSource
    DataSet = qryTipoPessoa
    Left = 872
    Top = 368
  end
  object dsAbatimento: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 264
    Top = 312
  end
  object dsAgruparPagamento: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 264
    Top = 344
  end
  object DataSource2: TDataSource
    DataSet = qryTipoRetencao
    Left = 1008
    Top = 272
  end
  object qryMunicipio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      ' FROM Municipio'
      'WHERE nCdMunicipio = :nPk')
    Left = 840
    Top = 408
    object qryMunicipionCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
    end
    object qryMunicipionCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
    object qryMunicipiocNmMunicipio: TStringField
      FieldName = 'cNmMunicipio'
      Size = 50
    end
    object qryMunicipionCdMunicipioIBGE: TIntegerField
      FieldName = 'nCdMunicipioIBGE'
    end
  end
  object dsMunicipio: TDataSource
    DataSet = qryMunicipio
    Left = 872
    Top = 408
  end
  object qryTipoImpostoMunicipio: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTipoImpostoMunicipioBeforePost
    AfterScroll = qryTipoImpostoMunicipioAfterScroll
    OnCalcFields = qryTipoImpostoMunicipioCalcFields
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TipoImpostoMunicipio '
      'WHERE nCdTipoImposto = :nPk')
    Left = 840
    Top = 440
    object qryTipoImpostoMunicipionCdTipoImpostoMunicipio: TIntegerField
      FieldName = 'nCdTipoImpostoMunicipio'
    end
    object qryTipoImpostoMunicipionCdTipoImposto: TIntegerField
      FieldName = 'nCdTipoImposto'
    end
    object qryTipoImpostoMunicipionCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
    end
    object qryTipoImpostoMunicipionCdTerceiroCredor: TIntegerField
      FieldName = 'nCdTerceiroCredor'
    end
    object qryTipoImpostoMunicipiocNmMunicipio: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmMunicipio'
      Calculated = True
    end
    object qryTipoImpostoMunicipiocNmTerceiroCredor: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiroCredor'
      Size = 50
      Calculated = True
    end
  end
  object dsTipoImpostoMunicipio: TDataSource
    DataSet = qryTipoImpostoMunicipio
    Left = 872
    Top = 440
  end
  object qryTerceiroMunicipio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdStatus'
      ',nCdMoeda'
      'FROM TERCEIRO'
      'WHERE nCdTerceiro = :nPK')
    Left = 840
    Top = 473
    object qryTerceiroMunicipionCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroMunicipiocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroMunicipiocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceiroMunicipionCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceiroMunicipionCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
  end
  object DataSource3: TDataSource
    DataSet = qryTerceiroMunicipio
    Left = 872
    Top = 472
  end
end
