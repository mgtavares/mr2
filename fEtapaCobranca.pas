unit fEtapaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmEtapaCobranca = class(TfrmCadastro_Padrao)
    qryMasternCdEtapaCobranca: TIntegerField;
    qryMastercNmEtapaCobranca: TStringField;
    qryMasteriDiasVencto: TIntegerField;
    qryMastercFlgTelefone: TIntegerField;
    qryMastercFlgCarta: TIntegerField;
    qryMastercFlgAcordo: TIntegerField;
    qryMastercFlgNegativar: TIntegerField;
    qryMastercFlgCobradora: TIntegerField;
    qryMastercFlgAtivo: TIntegerField;
    qryMasteriQtdeMaxItensLista: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    dsTelefone: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    dsCarta: TDataSource;
    DBLookupComboBox2: TDBLookupComboBox;
    DBLookupComboBox3: TDBLookupComboBox;
    dsNegativar: TDataSource;
    DBLookupComboBox4: TDBLookupComboBox;
    dsAcordo: TDataSource;
    DBLookupComboBox5: TDBLookupComboBox;
    dsCobradora: TDataSource;
    DBLookupComboBox6: TDBLookupComboBox;
    dsAtivo: TDataSource;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    qryMasternCdEspTit: TIntegerField;
    Label12: TLabel;
    DBEdit5: TDBEdit;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    DBEdit6: TDBEdit;
    dsEspTit: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtapaCobranca: TfrmEtapaCobranca;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmEtapaCobranca.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'ETAPACOBRANCA' ;
  nCdTabelaSistema  := 86 ;
  nCdConsultaPadrao := 195 ;
end;

procedure TfrmEtapaCobranca.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmEtapaCobranca.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasteriDiasVencto.Value <= 0) then
  begin
      MensagemAlerta('Informe os dias de atraso para a lista de cobran�a') ;
      DBedit3.SetFocus;
      abort ;
  end ;

  if(DBEdit6.Text = '') then
  begin
      MensagemAlerta('Vincule uma esp�cie de titulo � etapa de cobran�a');
      DBEdit5.SetFocus;
      Abort;
  end;
  
  inherited;

end;

procedure TfrmEtapaCobranca.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cFlgCobranca = 1');

            If (nPK > 0) then
            begin
                DBEdit5.Text := IntToStr(nPK);
                PosicionaQuery(qryEspTit, DBEdit5.Text);
            end ;
        end;
    end ;

  end ;

end;

procedure TfrmEtapaCobranca.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
  begin
      qryEspTit.Close;
      PosicionaQuery(qryEspTit, DBEdit5.Text);
  end;
end;

procedure TfrmEtapaCobranca.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryEspTit.Close;
end;

procedure TfrmEtapaCobranca.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryEspTit,qryMasternCdEspTit.AsString);
end;

initialization
    RegisterClass(TfrmEtapaCobranca) ;
    
end.
