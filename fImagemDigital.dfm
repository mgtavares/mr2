inherited frmImagemDigital: TfrmImagemDigital
  Left = 176
  Top = 163
  Width = 1024
  Height = 520
  Caption = 'Imagem Digital'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1008
    Height = 453
  end
  inherited ToolBar1: TToolBar
    Width = 1008
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1008
    Height = 453
    ActivePage = cxTabCapturar
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    OnChange = cxPageControl1Change
    ClientRectBottom = 449
    ClientRectLeft = 4
    ClientRectRight = 1004
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Digitalizar'
      ImageIndex = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 49
        Width = 1000
        Height = 376
        Align = alClient
        TabOrder = 0
        object DBImage1: TDBImage
          Left = 2
          Top = 15
          Width = 996
          Height = 359
          Align = alClient
          DataField = 'cImagem'
          DataSource = dsImagemDigital
          Stretch = True
          TabOrder = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 1000
        Height = 49
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Tag = 1
          Left = 8
          Top = 28
          Width = 76
          Height = 13
          Caption = 'Tipo de Imagem'
        end
        object cxButton1: TcxButton
          Left = 240
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Digitalizar'
          TabOrder = 0
          OnClick = cxButton1Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFC89662CA9865CA9765CA9765CA9765CA9764C99764C99764CA9865C895
            62FFFFFFFFFFFFFFFFFFA1A1A17A7A7A585858C79561F9F7F6F9F1ECF9F1EBF8
            F0E9F7EDE6F4EAE1F2E8DEFAF8F6C794612424244B4B4B9696966B6B6BA7A7A7
            B5B5B5818181AFACAAC5C0BDC5C0BDC5C0BDC5C0BDC5C0BDC5C0BDADAAA82C2C
            2CB5B5B59B9B9B232323707070B5B5B5B5B5B59595958181818181817979796E
            6E6E6161615252524343434242426E6E6EB5B5B5B5B5B5252525757575BBBBBB
            BBBBBB8D8D8DD4D4D4B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9B9D3D3D38383
            83BBBBBBBBBBBB2A2A2A7A7A7AD7D7D7D7D7D7979797D8D8D8BFBFBFBFBFBFBF
            BFBFBFBFBFBFBFBFBFBFBFD7D7D78E8E8ED7D7D7D7D7D73F3F3F7E7E7EF9F9F9
            F9F9F9ABABABDFDFDFCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBCBDFDFDFA3A3
            A3F9F9F9F9F9F9616161848484FCFCFCFCFCFCCBCBCBF2F2F2F2F2F2F2F2F2F2
            F2F2F2F2F2F2F2F2F2F2F2F2F2F2C6C6C6FCFCFCFCFCFC717171979797D2D2D2
            E8E8E87D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D
            7DE8E8E8C4C4C46D6D6DDDDDDD9A9A9ACCCCCCC78B4EF9F4EDFEE8D8FEE8D7FD
            E5D3FCE4D1FAE0C7F9DDC3FAF4EDC7854AC3C3C3747474CDCDCDFFFFFFCECECE
            878787C5894CF9F4EFFEE7D7FDE7D5FCE6D2FBE1CCF8DCC2F6DABDFAF4EFC483
            48616161BCBCBCFFFFFFFFFFFFFFFFFFFBFBFBC68C4FF9F4F0FCE6D3FDE7D3FB
            E3CDFAE0C8F5D6BBF3D4B5F8F4F0C4854AF9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFC88D51F9F5F1FCE3CFFCE4CFFAE1CAF9DDC4F4E9DFF7F2ECF5EFE9C380
            48FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC88D52F9F5F1FCE3CDFBE3CDF9
            E0C8F8DCC2FDFBF8FCE6CDE2B684D5A884FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFC5884DF7F2ECF8F4EEF8F3EDF8F3EDF8F2ECF2E6D7E2B27DDB9569FDFB
            FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8CEB9D7AA7CC88C50C88C4FCA
            9155CB9055C5894DDDAF8DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object cxButton2: TcxButton
          Left = 352
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Armazenar'
          Enabled = False
          TabOrder = 1
          OnClick = cxButton2Click
          Glyph.Data = {
            E6010000424DE60100000000000036000000280000000C0000000C0000000100
            180000000000B001000000000000000000000000000000000000CCCCCCAAA39F
            6D6C6B656464575D5E6564646564646564646D6C6B656464898A89C6C6C6B6B6
            B63D42420405065157577172726D6C6B575D5E515757191D23191D2331333289
            8A89484E4E0599CF092369DAD2C9FCFEFEFCFEFEEEEEEED5D5D5484E4E0599CF
            0923695157573D424231CDFD09236991846EB3AD9EAAA39F93929291846E3133
            320599CF0923695157573D424231CDFD10708209236909236909236909236909
            23691070820599CF0923695157573D424231CDFD0599CF107082092369092369
            092369092369479EC00599CF0923695157573D424231CDFD092369B7A68ACCC5
            C0C2BEB9C2BEB9DBC8C35157570599CF0923695157573D424231CDFD092369CB
            B9B1E6E6E6DEDEDEDEDEDEEEEEEA5157570599CF0923695157573D424231CDFD
            092369C2B9AEDEDEDEDADDD7DADDD7E6E6E65157570599CF0923695157573D42
            4231CDFD092369B8B1AADEDEDED5D5D5D5D5D5E6E6E6484E4E0599CF09236951
            57573D424231CDFD092369DAD2C9FCFEFEF9FAFAF9FAFAFCFEFE575D5E0599CF
            107082515757777B7B3D4242191D23484E4E575D5E515757515757575D5E191D
            23191D23575D5EA4A8A8}
          LookAndFeel.NativeStyle = True
        end
        object DBLookupComboBox1: TDBLookupComboBox
          Left = 88
          Top = 20
          Width = 145
          Height = 21
          KeyField = 'nCdTabTipoImagemDigital'
          ListField = 'cNmTabTipoImagemDigital'
          ListSource = dsTabTipoImagemDigital
          TabOrder = 2
        end
      end
    end
    object cxTabCapturar: TcxTabSheet
      Caption = 'Capturar'
      ImageIndex = 2
      object cxGroupBox1: TcxGroupBox
        Left = 8
        Top = 56
        Width = 649
        Height = 489
        Caption = ' Pr'#233'-Visualiza'#231#227'o '
        Style.LookAndFeel.NativeStyle = True
        TabOrder = 0
        object PanelWeb: TPanel
          Left = 2
          Top = 16
          Width = 645
          Height = 471
          Align = alClient
          Color = clBlack
          TabOrder = 0
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 8
        Top = 0
        Width = 707
        Height = 44
        Align = alCustom
        Style.LookAndFeel.NativeStyle = True
        TabOrder = 1
        object Label2: TLabel
          Tag = 1
          Left = 10
          Top = 18
          Width = 76
          Height = 13
          Caption = 'Tipo de Imagem'
        end
        object btnDesligar: TcxButton
          Left = 488
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Desligar'
          Enabled = False
          TabOrder = 0
          OnClick = btnDesligarClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FFDBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF6FFEAD9F2D6C0D6C5B7B7AFA89F
            9D9C9C9A98B3AAA3D0BFB1EFD3BCFFEAD9FFFAF6FFFFFFFFFFFFFFFFFFFFFAF6
            FFE6D3E4D2C5BEB8B1DCD1C8F1E8E1FBF6F2FAF3EEEDE0D6D2C3B8ADA49DDAC8
            BAFFE6D3FFFAF6FFFFFFFFFFFFFFEAD9E8D7C9C6BEB7F4EEE8FFFEFEFBF4EFF7
            EBE1F6EAE0FAF3EDFEFCFAEEE4DBACA196DBC9BBFFEAD9FFFFFFFFF4ECF7DAC4
            D2CCC8F7F1EDFFFFFFF6E8DDF6E8DDF5E7DCF5E6DBF5E5DAF4E5D8FFFEFEEFE4
            DCB1A9A2F0D4BEFFF4ECFFE9D8EBD9CCECE5E1FFFEFEF7EBE1F6EAE0F6E9DFF6
            E8DDF6E8DDF5E7DCF5E6DBF5E5DAFEFCFAD6C9BFD4C3B5FFE9D8FFE1C9DFD7D0
            F8F3EFFBF6F2F7ECE3F7ECE3D2925FD2915ED2915ED2905CF6E8DDF5E7DCFAF3
            EEEFE4DBBBB3ACFFE1C9FFDBBED7D5D3FEFDFBF9F1EBF8EEE5F7EDE4B47B4EB4
            7B4EB37A4DD2915EF6E8DDF6E8DDF7EBE1FAF5F1AAA9A7FFDBBEFFDBBED8D6D4
            FEFDFBFAF2ECF9EFE8F8EEE5B47B4EB47B4EB47B4ED2925FF6EAE0F6E9DFF7EC
            E3FDFBF9AFADABFFDBBEFFE1C9E2DAD4FAF6F3FCF8F4F9EFE8F9EFE8B67C4EB6
            7C4EB47B4ED2925FF7ECE3F7EBE1FBF5F1F6EFEAC5BDB6FFE1C9FFE9D8EEDDD0
            F1ECE8FFFFFFF9F1EBF9F0E9F9EFE8F9EFE8F8EFE6F8EEE5F7ECE3F7ECE3FFFF
            FFE7DFD8DFCEC0FFE9D8FFF4ECF9DDC7E2DDD9FBF8F7FFFFFFF9F1EBF9F0E9F9
            EFE8F9EFE8F8EFE6F8EEE5FFFFFFF9F6F3CFC9C4F5D9C3FFF4ECFFFFFFFFEAD9
            F2E1D3E0DAD6FBF8F7FFFFFFFCF9F6FAF3EEFAF3EDFCF8F4FFFFFFF9F6F3D3CC
            C7ECDBCDFFEAD9FFFFFFFFFFFFFFFAF6FFE6D3F2E1D3E2DDD9F1ECE8FAF6F3FE
            FDFBFEFDFBF9F5F2EEE9E5DAD4CFEEDCCEFFE6D3FFFAF6FFFFFFFFFFFFFFFFFF
            FFFAF6FFEAD9F9DDC7EFDED0E3DBD5DAD8D6D9D7D5E1D8D1ECDBCDF8DCC6FFEA
            D9FFFAF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FF
            DBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btnCapturar: TcxButton
          Left = 376
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Capturar'
          Enabled = False
          TabOrder = 1
          OnClick = btnCapturarClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFF7979795555554E4E4E4747473F3F3F3737372F
            2F2F2828282121211B1B1B171717151515151515151515FFFFFFFFFFFF9A9A9A
            D1D1D1CBCBCBC4C4C4BCBCBCB4B4B4ABABABA3A3A39B9B9B9494948E8E8E8989
            89878787191919FFFFFFFFFFFFA2A2A2C7C7C7AFAFAF8F8F8F8A8A8A9494947A
            7A7A7070707575755454544B4B4B6161618484841D1D1DFFFFFFFFFFFFE1E1E1
            A6A6A69F9F9FEFEFEF5959595B5B5B4848484545454B4B4B373737E9E9E94F4F
            4F626262969696FFFFFFFFFFFFFFFFFFE1E1E1AFAFAFA8A8A8B3B3B3CACACABF
            BFBFBEBEBEC8C8C88989897C7C7C7A7A7AA7A7A7FFFFFFFFFFFFFFFFFFFFFFFF
            F4F4F4B0B0B0BDBDBDB3B3B3A1A1A19C9C9C9A9A9A9C9C9CA5A5A59292927A7A
            7AEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFDBDBDBC3C3C3DADADAAFAFAFD1D1D1F0
            F0F0EFEFEFCECECEA6A6A6C7C7C77C7C7CAEAEAEFFFFFFFFFFFFFFFFFFFFFFFF
            D0D0D0DBDBDBDADADAEAEAEADAD1CCD5AC91E3AE8AE6D9D3E4E4E4CBCBCBADAD
            AD808080FFFFFFFFFFFFFFFFFFFFFFFFCDCDCDE8E8E8DDDDDDF9F9F9A49992E4
            B99CEBB899E7B190F7F7F7CBCBCBCCCCCC656565FFFFFFFFFFFFFFFFFFFFFFFF
            D6D6D6ECECECE3E3E3FAFAF9A49B94DAB9A2E5BBA1D8B096F7F7F7CECECEDADA
            DA6F6F6FFFFFFFFFFFFFFFFFFFFFFFFFE4E4E4EDEDEDEAEAEAF3F3F3DFD4CCA5
            9B94A49A93DCD1CAE9E9E9D3D3D3CFCFCF969696FFFFFFFFFFFFFFFFFFFFFFFF
            F2F2F2EAEAEAF2F2F2EFEFEFF5F5F5FBFAFAFAFAF9EDEDEDDADADADFDFDFB9B9
            B9C6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFCFCFCEEEEEEF1F1F1F5F5F5EEEEEEBE
            BEBEBABABAE0E0E0E3E3E3DADADABCBCBCF3F3F3FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFAFAFAF0F0F0EEEEEEEFEFEFF1F1F1EDEDEDE6E6E6DADADAD1D1D1EDED
            EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCF6F6F6EFEFEFE7
            E7E7E3E3E3E6E6E6EBEBEBF8F8F8FFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btnLigar: TcxButton
          Left = 264
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Ligar'
          TabOrder = 2
          OnClick = btnLigarClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FFDBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF6FFEAD9F2D6C0D6C5B7B7AFA89F
            9D9C9C9A98B3AAA3D0BFB1EFD3BCFFEAD9FFFAF6FFFFFFFFFFFFFFFFFFFFFAF6
            FFE6D3E4D2C5BEB8B1DCD1C8F1E8E1FBF6F2FAF3EEEDE0D6D2C3B8ADA49DDAC8
            BAFFE6D3FFFAF6FFFFFFFFFFFFFFEAD9E8D7C9C6BEB7F4EEE8FFFEFEFBF4EFF7
            EBE1F6EAE0FAF3EDFEFCFAEEE4DBACA196DBC9BBFFEAD9FFFFFFFFF4ECF7DAC4
            D2CCC8F7F1EDFFFFFFF6E8DDF6E8DDF9F1EBF5E6DBF5E5DAF4E5D8FFFEFEEFE4
            DCB1A9A2F0D4BEFFF4ECFFE9D8EBD9CCECE5E1FFFEFEF7EBE1F6EAE0F6E9DFBD
            8051F9F1EBF5E7DCF5E6DBF5E5DAFEFCFAD6C9BFD4C3B5FFE9D8FFE1C9DFD7D0
            F8F3EFFBF6F2F7ECE3F7ECE3F7EBE1C58655BE8051F9F1EBF6E8DDF5E7DCFAF3
            EEEFE4DBBBB3ACFFE1C9FFDBBED7D5D3FEFDFBF9F1EBF8EEE5F7EDE4F7ECE3C0
            8352C68655BE8051F9F1EBF6E8DDF7EBE1FAF5F1AAA9A7FFDBBEFFDBBED8D6D4
            FEFDFBFAF2ECF9EFE8F8EEE5F7EDE4C08352C08352C88755EDD3BEF6E9DFF7EC
            E3FDFBF9AFADABFFDBBEFFE1C9E2DAD4FAF6F3FCF8F4F9EFE8F9EFE8F8EFE6C1
            8352C18352E8C8ADF7ECE3F7EBE1FBF5F1F6EFEAC5BDB6FFE1C9FFE9D8EEDDD0
            F1ECE8FFFFFFF9F1EBF9F0E9F9EFE8C38453E9C9B0F8EEE5F7ECE3F7ECE3FFFF
            FFE7DFD8DFCEC0FFE9D8FFF4ECF9DDC7E2DDD9FBF8F7FFFFFFF9F1EBF9F0E9E9
            CAB1F9EFE8F8EFE6F8EEE5FFFFFFF9F6F3CFC9C4F5D9C3FFF4ECFFFFFFFFEAD9
            F2E1D3E0DAD6FBF8F7FFFFFFFCF9F6FAF3EEFAF3EDFCF8F4FFFFFFF9F6F3D3CC
            C7ECDBCDFFEAD9FFFFFFFFFFFFFFFAF6FFE6D3F2E1D3E2DDD9F1ECE8FAF6F3FE
            FDFBFEFDFBF9F5F2EEE9E5DAD4CFEEDCCEFFE6D3FFFAF6FFFFFFFFFFFFFFFFFF
            FFFAF6FFEAD9F9DDC7EFDED0E3DBD5DAD8D6D9D7D5E1D8D1ECDBCDF8DCC6FFEA
            D9FFFAF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4ECFFE9D8FFE1C9FF
            DBBEFFDBBEFFE1C9FFE9D8FFF4ECFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object DBLookupComboBox2: TDBLookupComboBox
          Left = 96
          Top = 15
          Width = 145
          Height = 21
          KeyField = 'nCdTabTipoImagemDigital'
          ListField = 'cNmTabTipoImagemDigital'
          ListSource = dsTabTipoImagemDigital
          TabOrder = 3
        end
        object cxButton3: TcxButton
          Left = 600
          Top = 8
          Width = 105
          Height = 33
          Caption = 'Armazenar'
          Enabled = False
          TabOrder = 4
          OnClick = cxButton3Click
          Glyph.Data = {
            E6010000424DE60100000000000036000000280000000C0000000C0000000100
            180000000000B001000000000000000000000000000000000000CCCCCCAAA39F
            6D6C6B656464575D5E6564646564646564646D6C6B656464898A89C6C6C6B6B6
            B63D42420405065157577172726D6C6B575D5E515757191D23191D2331333289
            8A89484E4E0599CF092369DAD2C9FCFEFEFCFEFEEEEEEED5D5D5484E4E0599CF
            0923695157573D424231CDFD09236991846EB3AD9EAAA39F93929291846E3133
            320599CF0923695157573D424231CDFD10708209236909236909236909236909
            23691070820599CF0923695157573D424231CDFD0599CF107082092369092369
            092369092369479EC00599CF0923695157573D424231CDFD092369B7A68ACCC5
            C0C2BEB9C2BEB9DBC8C35157570599CF0923695157573D424231CDFD092369CB
            B9B1E6E6E6DEDEDEDEDEDEEEEEEA5157570599CF0923695157573D424231CDFD
            092369C2B9AEDEDEDEDADDD7DADDD7E6E6E65157570599CF0923695157573D42
            4231CDFD092369B8B1AADEDEDED5D5D5D5D5D5E6E6E6484E4E0599CF09236951
            57573D424231CDFD092369DAD2C9FCFEFEF9FAFAF9FAFAFCFEFE575D5E0599CF
            107082515757777B7B3D4242191D23484E4E575D5E515757515757575D5E191D
            23191D23575D5EA4A8A8}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxGroupBox3: TcxGroupBox
        Left = 664
        Top = 56
        Width = 321
        Height = 257
        Align = alCustom
        Caption = ' Miniatura '
        Style.LookAndFeel.NativeStyle = True
        TabOrder = 2
        object DBImage2: TDBImage
          Left = 2
          Top = 16
          Width = 317
          Height = 239
          Align = alClient
          DataField = 'cImagem'
          DataSource = dsImagemDigital
          Stretch = True
          TabOrder = 0
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Imagens Arquivadas'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1000
        Height = 425
        Align = alClient
        Caption = 'Imagens Armazenadas'
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 2
          Top = 15
          Width = 996
          Height = 408
          Align = alClient
          PopupMenu = PopupMenu1
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            DataController.DataSource = dsImagensArmazenadas
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            NavigatorButtons.ConfirmDelete = False
            NavigatorButtons.First.Visible = True
            NavigatorButtons.PriorPage.Visible = True
            NavigatorButtons.Prior.Visible = True
            NavigatorButtons.Next.Visible = True
            NavigatorButtons.NextPage.Visible = True
            NavigatorButtons.Last.Visible = True
            NavigatorButtons.Insert.Visible = True
            NavigatorButtons.Delete.Visible = True
            NavigatorButtons.Edit.Visible = True
            NavigatorButtons.Post.Visible = True
            NavigatorButtons.Cancel.Visible = True
            NavigatorButtons.Refresh.Visible = True
            NavigatorButtons.SaveBookmark.Visible = True
            NavigatorButtons.GotoBookmark.Visible = True
            NavigatorButtons.Filter.Visible = True
            OptionsCustomize.ColumnGrouping = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GridLineColor = clSilver
            OptionsView.GridLines = glVertical
            OptionsView.GroupByBox = False
            Styles.Content = frmMenu.FonteSomenteLeitura
            Styles.Header = frmMenu.Header
            object cxGrid1DBTableView1nCdImagemDigital: TcxGridDBColumn
              DataBinding.FieldName = 'nCdImagemDigital'
              Visible = False
            end
            object cxGrid1DBTableView1nCdTabTipoImagemDigital: TcxGridDBColumn
              DataBinding.FieldName = 'nCdTabTipoImagemDigital'
              Visible = False
            end
            object cxGrid1DBTableView1cNmTabTipoImagemDigital: TcxGridDBColumn
              Caption = 'Tipo Imagem'
              DataBinding.FieldName = 'cNmTabTipoImagemDigital'
              Width = 256
            end
            object cxGrid1DBTableView1dDtDigitalizacao: TcxGridDBColumn
              Caption = 'Data Digitaliza'#231#227'o'
              DataBinding.FieldName = 'dDtDigitalizacao'
              Width = 141
            end
            object cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn
              DataBinding.FieldName = 'nCdUsuario'
              Visible = False
            end
            object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
              Caption = 'Usu'#225'rio Respons'#225'vel'
              DataBinding.FieldName = 'cNmUsuario'
            end
            object cxGrid1DBTableView1nCdLoja: TcxGridDBColumn
              DataBinding.FieldName = 'nCdLoja'
              Visible = False
            end
            object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
              Caption = 'Loja Digitaliza'#231#227'o'
              DataBinding.FieldName = 'cNmLoja'
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 232
  end
  object qryImagemDigital: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM ImagemDigital')
    Left = 348
    Top = 229
    object qryImagemDigitalnCdImagemDigital: TIntegerField
      FieldName = 'nCdImagemDigital'
    end
    object qryImagemDigitalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryImagemDigitalnCdTabTipoImagemDigital: TIntegerField
      FieldName = 'nCdTabTipoImagemDigital'
    end
    object qryImagemDigitaldDtDigitalizacao: TDateTimeField
      FieldName = 'dDtDigitalizacao'
    end
    object qryImagemDigitalnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryImagemDigitalnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryImagemDigitalcImagem: TBlobField
      FieldName = 'cImagem'
    end
  end
  object dsImagemDigital: TDataSource
    DataSet = qryImagemDigital
    Left = 352
    Top = 264
  end
  object DelphiTwain1: TDelphiTwain
    OnTwainAcquire = DelphiTwain1TwainAcquire
    TransferMode = ttmMemory
    SourceCount = 0
    Info.MajorVersion = 1
    Info.MinorVersion = 0
    Info.Language = tlUserLocale
    Info.CountryCode = 1
    Info.Groups = [tgControl, tgImage]
    Info.VersionInfo = 'Application name'
    Info.Manufacturer = 'Application manufacturer'
    Info.ProductFamily = 'App product family'
    Info.ProductName = 'App product name'
    LibraryLoaded = False
    SourceManagerLoaded = False
    Left = 388
    Top = 260
  end
  object qryImagensArmazenadas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdImagemDigital'
      '      ,TabTipoImagemDigital.nCdTabTipoImagemDigital'
      '      ,TabTipoImagemDigital.cNmTabTipoImagemDigital'
      '      ,dDtDigitalizacao'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '  FROM ImagemDigital'
      
        '       LEFT JOIN TabTipoImagemDigital ON TabTipoImagemDigital.nC' +
        'dTabTipoImagemDigital = ImagemDigital.nCdTabTipoImagemDigital'
      
        '       LEFT JOIN Usuario              ON Usuario.nCdUsuario = Im' +
        'agemDigital.nCdUsuario'
      
        '       LEFT JOIN Loja                 ON Loja.nCdLoja       = Im' +
        'agemDigital.nCdLoja'
      '')
    Left = 284
    Top = 228
    object qryImagensArmazenadasnCdImagemDigital: TIntegerField
      FieldName = 'nCdImagemDigital'
    end
    object qryImagensArmazenadasnCdTabTipoImagemDigital: TIntegerField
      FieldName = 'nCdTabTipoImagemDigital'
    end
    object qryImagensArmazenadascNmTabTipoImagemDigital: TStringField
      FieldName = 'cNmTabTipoImagemDigital'
      Size = 50
    end
    object qryImagensArmazenadasdDtDigitalizacao: TDateTimeField
      FieldName = 'dDtDigitalizacao'
    end
    object qryImagensArmazenadasnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryImagensArmazenadascNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryImagensArmazenadasnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryImagensArmazenadascNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsImagensArmazenadas: TDataSource
    DataSet = qryImagensArmazenadas
    Left = 284
    Top = 260
  end
  object PopupMenu1: TPopupMenu
    Left = 388
    Top = 229
    object VisualizarImagem1: TMenuItem
      Caption = 'Visualizar Imagem'
      OnClick = VisualizarImagem1Click
    end
    object btExcluirImagem: TMenuItem
      Caption = 'Excluir Imagem Arquivada'
      OnClick = btExcluirImagemClick
    end
  end
  object qryTabTipoImagemDigital: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTabTipoImagemDigital, cNmTabTipoImagemDigital'
      'FROM TabTipoImagemDigital'
      'WHERE nCdTabTipoImagemDigital > 0')
    Left = 316
    Top = 228
    object qryTabTipoImagemDigitalnCdTabTipoImagemDigital: TIntegerField
      FieldName = 'nCdTabTipoImagemDigital'
    end
    object qryTabTipoImagemDigitalcNmTabTipoImagemDigital: TStringField
      FieldName = 'cNmTabTipoImagemDigital'
      Size = 50
    end
  end
  object dsTabTipoImagemDigital: TDataSource
    DataSet = qryTabTipoImagemDigital
    Left = 316
    Top = 260
  end
  object qryDeletarImagem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE '
      '  FROM ImagemDigital'
      ' WHERE nCdImagemDigital = :nPK')
    Left = 252
    Top = 229
  end
end
