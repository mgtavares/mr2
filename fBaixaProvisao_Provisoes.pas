unit fBaixaProvisao_Provisoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmBaixaProvisao_Provisoes = class(TfrmProcesso_Padrao)
    qryProvisoes: TADOQuery;
    qryProvisoesnCdProvisaoTit: TAutoIncField;
    qryProvisoescNmUsuario: TStringField;
    qryProvisoesdDtPrev: TDateTimeField;
    qryProvisoescNmFormaPagto: TStringField;
    qryProvisoesnValProvisao: TBCDField;
    DBGridEh1: TDBGridEh;
    dsProvisoes: TDataSource;
    qryProvisoesnCdFormaPagto: TIntegerField;
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaProvisao_Provisoes: TfrmBaixaProvisao_Provisoes;

implementation

uses fBaixaProvisao_BaixaTitulos;

{$R *.dfm}

procedure TfrmBaixaProvisao_Provisoes.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmBaixaProvisao_BaixaTitulos ;
begin
  inherited;

  if not qryProvisoes.Eof and (qryProvisoesnCdProvisaoTit.Value > 0) then
  begin

      objForm := TfrmBaixaProvisao_BaixaTitulos.Create(nil) ;

      objForm.cmdPreparaTemp.Execute;

      objForm.qryTitulos.Close ;
      objForm.qryTitulos.Parameters.ParamByName('nPK').Value := qryProvisoesnCdProvisaoTit.Value ;
      objForm.qryTitulos.Open;

      PosicionaQuery(objForm.qryFormaPagto, qryProvisoesnCdFormaPagto.asString) ;

      showForm( objForm , TRUE ) ;
      
      qryProvisoes.Close ;
      qryProvisoes.Open  ;

      if (qryProvisoes.eof) then
          close ;
          
  end ;
end;

end.
