unit fAlertas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, GridsEh, DBGridEh, ImgList, DB, ADODB, StdCtrls,
  DBCtrls, cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmAlertas = class(TForm)
    ImageList1: TImageList;
    DBGridEh1: TDBGridEh;
    Panel1: TPanel;
    qryAlertas: TADOQuery;
    qryAlertasnCdMensagemUsuario: TAutoIncField;
    qryAlertascNmAssunto: TStringField;
    qryAlertasdDtMensagem: TDateTimeField;
    qryAlertasnCdUsuarioDestino: TIntegerField;
    qryAlertascMensagem: TMemoField;
    qryAlertascFlgLida: TIntegerField;
    qryAlertasdDtLeitura: TDateTimeField;
    qryAlertasnCdUsuarioLeitura: TIntegerField;
    dsAlertas: TDataSource;
    DBMemo1: TDBMemo;
    cxButton2: TcxButton;
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlertas: TfrmAlertas;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAlertas.cxButton2Click(Sender: TObject);
begin

    Close ;

end;

procedure TfrmAlertas.DBGridEh1DblClick(Sender: TObject);
begin

    if not qryAlertas.eof then
    begin

        case frmMenu.MessageDlg('Confirma a leitura deste alerta ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo:exit ;
        end ;

        qryAlertas.Edit ;
        qryAlertasdDtLeitura.Value        := Now() ;
        qryAlertasnCdUsuarioLeitura.Value := frmMenu.nCdUsuarioLogado ;
        qryAlertascFlgLida.Value          := 1 ;
        qryAlertas.Post ;

        qryAlertas.Close ;
        qryAlertas.Open ;

        if (qryAlertas.eof) then
            Close ;

    end ;

end;

end.
