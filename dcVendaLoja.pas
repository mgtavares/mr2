unit dcVendaLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TdcmVendaLoja = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    dsDepartamento: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    dsCategoria: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    dsSubCategoria: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    dsSegmento: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    dsMarca: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    dsGrupoProduto: TDataSource;
    edtReferencia: TEdit;
    Label10: TLabel;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    dsLinha: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    dsColecao: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    dsClasseProduto: TDataSource;
    edtCdCampanhaPromoc: TMaskEdit;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    dsCampanhaPromoc: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    edtDtFinal2: TMaskEdit;
    Label15: TLabel;
    edtDtInicial2: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edtDtInicial3: TMaskEdit;
    Label19: TLabel;
    edtDtFinal3: TMaskEdit;
    Label20: TLabel;
    GroupBox1: TGroupBox;
    chkDimAno: TCheckBox;
    chkDimMes: TCheckBox;
    chkDimLoja: TCheckBox;
    chkDimDataPedido: TCheckBox;
    chkDimVendedor: TCheckBox;
    chkDimFormaPagto: TCheckBox;
    chkDimCondPagto: TCheckBox;
    chkDimDepartamento: TCheckBox;
    chkDimCategoria: TCheckBox;
    chkDimSubCategoria: TCheckBox;
    chkDimSegmento: TCheckBox;
    chkDimMarca: TCheckBox;
    chkDimProdutoEstrut: TCheckBox;
    chkDimProdutoFinal: TCheckBox;
    GroupBox2: TGroupBox;
    chkMedQtdeVenda: TCheckBox;
    chkMedQtdeEstoque: TCheckBox;
    chkMedValVenda: TCheckBox;
    chkMedValCustoVenda: TCheckBox;
    chkMedMarkUp: TCheckBox;
    chkMedValBruto: TCheckBox;
    chkMedMC: TCheckBox;
    chkDimNumPedido: TCheckBox;
    qryTipoPedido: TADOQuery;
    dsTipoPedido: TDataSource;
    qryTipoPedidocNmTipoPedido: TStringField;
    Label21: TLabel;
    DBEdit12: TDBEdit;
    edtTipoPedido: TMaskEdit;
    chkConsidAcresc: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtCdCampanhaPromocExit(Sender: TObject);
    procedure edtCdCampanhaPromocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaLoja: TdcmVendaLoja;

implementation

uses fMenu, fLookup_Padrao, dcVendaLoja_view, Math;

{$R *.dfm}

procedure TdcmVendaLoja.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TdcmVendaLoja.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TdcmVendaLoja.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TdcmVendaLoja.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TdcmVendaLoja.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TdcmVendaLoja.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TdcmVendaLoja.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TdcmVendaLoja.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TdcmVendaLoja.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TdcmVendaLoja.ToolButton1Click(Sender: TObject);
var
  cFiltro : string ;
  i : integer ;
  objForm : TdcmVendaLoja_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') or (Trim(edtDtFinal.Text) = '/  /') then
  begin
      edtDtFinal.Text   := DateToStr(Date) ;
      edtDtInicial.Text := DateToStr(Date) ;
  end ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo de venda inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (trim(edtDtInicial2.Text) = '/  /') or (Trim(edtDtFinal2.Text) = '/  /') then
  begin
      edtDtFinal2.Text   := edtDtFinal.Text ;
      edtDtInicial2.Text := edtDtInicial.Text ;
  end ;

  if (trim(edtDtInicial3.Text) = '/  /') or (Trim(edtDtFinal3.Text) = '/  /') then
  begin
      edtDtFinal3.Text   := edtDtFinal2.Text ;
      edtDtInicial3.Text := edtDtInicial2.Text ;
  end ;

  if (StrToDate(edtDtInicial2.Text) > StrToDate(edtDtFinal2.Text)) then
  begin
      MensagemAlerta('Per�odo de venda 2 inv�lido.') ;
      edtDtInicial2.SetFocus;
      exit ;
  end ;

  if (StrToDate(edtDtInicial3.Text) > StrToDate(edtDtFinal3.Text)) then
  begin
      MensagemAlerta('Per�odo de venda 3 inv�lido.') ;
      edtDtInicial3.SetFocus;
      exit ;
  end ;

  objForm := TdcmVendaLoja_view.Create(nil);

  objForm.ADODataSet1.Close;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
  objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('cReferencia').Value      := edtReferencia.Text ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdCampanhaPromoc').Value:= frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial2').Value      := frmMenu.ConvData(edtDtInicial2.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal2').Value        := frmMenu.ConvData(edtDtFinal2.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial3').Value      := frmMenu.ConvData(edtDtInicial3.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal3').Value        := frmMenu.ConvData(edtDtFinal3.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdTipoPedido').Value    := frmMenu.ConvInteiro(edtTipoPedido.Text);
  objForm.ADODataSet1.Parameters.ParamByName('cFlgConsidAcr').Value    := IfThen(chkConsidAcresc.Checked,1,0);
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  objForm.PivotCube1.Dimensions[0].Enabled  := (chkDimAno.Checked) ;
  objForm.PivotCube1.Dimensions[1].Enabled  := (chkDimMes.Checked) ;
  objForm.PivotCube1.Dimensions[2].Enabled  := (chkDimLoja.Checked) ;
  objForm.PivotCube1.Dimensions[3].Enabled  := (chkDimDataPedido.Checked) ;
  objForm.PivotCube1.Dimensions[4].Enabled  := (chkDimNumPedido.Checked) ;
  objForm.PivotCube1.Dimensions[5].Enabled  := (chkDimVendedor.Checked) ;
  objForm.PivotCube1.Dimensions[6].Enabled  := (chkDimFormaPagto.Checked) ;
  objForm.PivotCube1.Dimensions[7].Enabled  := (chkDimCondPagto.Checked) ;
  objForm.PivotCube1.Dimensions[8].Enabled  := (chkDimDepartamento.Checked) ;
  objForm.PivotCube1.Dimensions[9].Enabled  := (chkDimCategoria.Checked) ;
  objForm.PivotCube1.Dimensions[10].Enabled := (chkDimSubCategoria.Checked) ;
  objForm.PivotCube1.Dimensions[11].Enabled := (chkDimSegmento.Checked) ;
  objForm.PivotCube1.Dimensions[12].Enabled := (chkDimMarca.Checked) ;
  objForm.PivotCube1.Dimensions[13].Enabled := (chkDimProdutoEstrut.Checked) ;
  objForm.PivotCube1.Dimensions[14].Enabled := (chkDimProdutoFinal.Checked) ;

  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  {objForm.PivotMap1.Measures[3].ColumnPercent := True;}
  {objForm.PivotMap1.Measures[3].Value := False;}

  objForm.PivotMap1.Measures[6].ColumnPercent := True;
  objForm.PivotMap1.Measures[6].Value := False;

  objForm.PivotMap1.Measures[7].Rank := True ;
  objForm.PivotMap1.Measures[7].Value := False;


  objForm.PivotGrid1.ColWidths[0] := 500 ;

  objForm.PivotMap1.SortColumn(0,7,False) ;

  objForm.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Loja' ;


  {-- exibe todas as medidas --}
  objForm.PivotMap1.ShowMeasure(0);
  objForm.PivotMap1.ShowMeasure(1);
  objForm.PivotMap1.ShowMeasure(2);
  objForm.PivotMap1.ShowMeasure(3);
  objForm.PivotMap1.ShowMeasure(4);
  objForm.PivotMap1.ShowMeasure(5);
  objForm.PivotMap1.ShowMeasure(6);
  objForm.PivotMap1.ShowMeasure(7);

  {-- oculta as medidas n�o selecionadas --}
  if (not chkMedQtdeVenda.Checked) then     objForm.PivotMap1.HideMeasure(0);
  if (not chkMedQtdeEstoque.Checked) then   objForm.PivotMap1.HideMeasure(1);
  if (not chkMedValVenda.Checked) then      objForm.PivotMap1.HideMeasure(2);
  if (not chkMedValCustoVenda.Checked) then objForm.PivotMap1.HideMeasure(3);
  if (not chkMedMarKup.Checked) then        objForm.PivotMap1.HideMeasure(4);
  if (not chkMedValBruto.Checked) then      objForm.PivotMap1.HideMeasure(5);
  if (not chkMedMC.Checked) then            objForm.PivotMap1.HideMeasure(6);

  objForm.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm,true) ;
  Self.Activate ;

end;

procedure TdcmVendaLoja.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TdcmVendaLoja.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TdcmVendaLoja.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TdcmVendaLoja.edtCdCampanhaPromocExit(Sender: TObject);
begin
  inherited;

  qryCampanhaPromoc.Close;
  PosicionaQuery(qryCampanhaPromoc, edtCdCampanhaPromoc.Text) ;
  
end;

procedure TdcmVendaLoja.edtCdCampanhaPromocKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(194);

        If (nPK > 0) then
            edtCdCampanhaPromoc.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtTipoPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(71);

        If (nPK > 0) then
            edtTipoPedido.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TdcmVendaLoja.edtTipoPedidoExit(Sender: TObject);
begin
  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido,edtTipoPedido.Text);
end;

initialization
    RegisterClass(TdcmVendaLoja) ;

end.
