unit fAdmListaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TfrmAdmListaCobranca = class(TfrmProcesso_Padrao)
    Label5: TLabel;
    edtDtInicial: TMaskEdit;
    Label7: TLabel;
    edtDtFinal: TMaskEdit;
    edtUsuario: TMaskEdit;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    qryUsuario: TADOQuery;
    edtCdLista: TMaskEdit;
    Label2: TLabel;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    CheckBox2: TCheckBox;
    procedure edtUsuarioExit(Sender: TObject);
    procedure edtUsuarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure edtUsuarioChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdmListaCobranca: TfrmAdmListaCobranca;

implementation

uses fLookup_Padrao, fAdmListaCobranca_Listas, fMenu;

{$R *.dfm}

procedure TfrmAdmListaCobranca.edtUsuarioExit(Sender: TObject);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, edtUsuario.Text) ;
  
end;

procedure TfrmAdmListaCobranca.edtUsuarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(197);

        If (nPK > 0) then
            edtUsuario.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmAdmListaCobranca.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmAdmListaCobranca_Listas;
begin
  inherited;

  if (trim(edtUsuario.Text) <> '') and (DBEdit1.Text = '') then
  begin
      MensagemErro('Selecione o analista de cobran�a ou deixa em branco para todos.') ;
      edtUsuario.SetFocus;
      exit ;
  end ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := dateToStr(Date) ;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := dateToStr(Date) ;

  objForm := TfrmAdmListaCobranca_Listas.Create(nil);

  objForm.qryLista.Close;
  objForm.qryLista.Parameters.ParamByName('nCdEmpresa').Value              := frmMenu.nCdEmpresaAtiva;
  objForm.qryLista.Parameters.ParamByName('nCdUsuarioResp').Value          := frmMenu.ConvInteiro(edtUsuario.Text);
  objForm.qryLista.Parameters.ParamByName('nCdListaCobranca').Value        := frmMenu.ConvInteiro(edtCdLista.Text);
  objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoEncerrada').Value := 0;
  objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoAtrib').Value     := 0;
  objForm.qryLista.Parameters.ParamByName('nCdCronogramaCob').Value        := 0;
  objForm.qryLista.Parameters.ParamByName('dDtCobranca').Value             := DatetoStr(frmMenu.ConvData(edtDtInicial.Text)) ;
  objForm.qryLista.Parameters.ParamByName('dDtCobrancaFim').Value          := DatetoStr(frmMenu.ConvData(edtDtFinal.Text)) ;
  objForm.qryLista.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.nCdLojaAtiva;

  if (CheckBox2.Checked) then
      objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoEncerrada').Value := 1;

  if (CheckBox1.Checked) then
      objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoAtrib').Value := 1;

  objForm.qryLista.Open;

  if objForm.qryLista.Eof then
  begin
      ShowMessage('Nenhuma lista encontrada para o crit�rio utilizado.') ;
      exit ;
  end ;

  showForm(objForm,true);

end;

procedure TfrmAdmListaCobranca.CheckBox1Click(Sender: TObject);
begin
  inherited;

  if (CheckBox1.Checked) then
  begin
      edtUsuario.Text := '' ;
      qryUsuario.Close;
      CheckBox1.Checked := True ;
  end ;

end;

procedure TfrmAdmListaCobranca.edtUsuarioChange(Sender: TObject);
begin
  inherited;

  CheckBox1.Checked := false ;
  
end;

initialization
    registerClass(TfrmAdmListaCobranca) ;
    
end.
