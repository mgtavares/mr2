unit fGeraSPComissaoRepres_Resumo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmGeraSPComissaoRepres_Resumo = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryResumoComissao: TADOQuery;
    dsResumoComissao: TDataSource;
    qryResumoComissaonCdTerceiro: TIntegerField;
    qryResumoComissaocNmTerceiro: TStringField;
    qryResumoComissaonValTotalComissao: TBCDField;
    SP_GERA_SP_COMISSAO_REPRES: TADOStoredProc;
    qrySPGerada: TADOQuery;
    qrySPGeradanCdSP: TIntegerField;
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dDtPagto : TDateTime;
  end;

var
  frmGeraSPComissaoRepres_Resumo: TfrmGeraSPComissaoRepres_Resumo;

implementation

uses fMenu, rImpSP;

{$R *.dfm}

procedure TfrmGeraSPComissaoRepres_Resumo.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Resumo Pagamento Comiss�o');
end;

procedure TfrmGeraSPComissaoRepres_Resumo.ToolButton1Click(
  Sender: TObject);
var
  objRel : TrptImpSP ;
begin
  inherited;

  if (qryResumoComissao.Eof) then
  begin
      MensagemAlerta('Nenhum registro de comiss�o encontrado.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a gera��o das SP�s ? Este processo n�o poder� ser cancelado.',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_SP_COMISSAO_REPRES.Close;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@dDtInicial').Value := qryResumoComissao.Parameters.ParamByName('dDtInicial').Value ;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@dDtFinal').Value   := qryResumoComissao.Parameters.ParamByName('dDtFinal').Value ;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@dDtPagto').Value   := DateToStr(dDtPagto) ;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_GERA_SP_COMISSAO_REPRES.Parameters.ParamByName('@cNmUsuario').Value := frmMenu.cNmCompletoUsuario;
      SP_GERA_SP_COMISSAO_REPRES.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP�s geradas aguardando autoriza��o.') ;

  objRel :=  TrptImpSP.Create(nil);

  if (MessageDlg('Deseja imprimir as SP�s ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
  begin
      qrySPGerada.Close;
      qrySPGerada.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      qrySPGerada.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qrySPGerada.Open ;

      while not qrySPGerada.Eof do
      begin
          objRel.qryImpSP.Close ;
          objRel.qryImpSP.Parameters.ParamByName('nCdSP').Value := qrySPGeradanCdSP.Value ;
          objRel.qryImpSP.Open ;

          objRel.qryPrazo.Close ;
          objRel.qryPrazo.Parameters.ParamByName('nCdSP').Value := qrySPGeradanCdSP.Value ;
          objRel.qryPrazo.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.QuickRep1.PreviewModal;

          qrySPGerada.Next;

      end ;
  end ;

  FreeAndNil(objRel);
  Close;

end;

end.
