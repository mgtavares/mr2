inherited rptDRE: TrptDRE
  Left = 209
  Top = 143
  Caption = 'Relat'#243'rio - DRE'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 43
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 4
    Top = 72
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Neg'#243'cio'
  end
  object Label3: TLabel [3]
    Left = 12
    Top = 120
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#234's/Ano Inicial'
  end
  object Label4: TLabel [4]
    Left = 156
    Top = 120
    Width = 67
    Height = 13
    Caption = 'M'#234's/Ano Final'
  end
  object Label5: TLabel [5]
    Left = 63
    Top = 96
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 152
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 224
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit7: TDBEdit [9]
    Tag = 1
    Left = 152
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource2
    TabOrder = 3
  end
  object MaskEdit1: TMaskEdit [10]
    Left = 88
    Top = 112
    Width = 63
    Height = 21
    EditMask = '99/9999'
    MaxLength = 7
    TabOrder = 7
    Text = '  /    '
  end
  object MaskEdit3: TMaskEdit [11]
    Left = 88
    Top = 40
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit5: TMaskEdit [12]
    Left = 88
    Top = 64
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object MaskEdit2: TMaskEdit [13]
    Left = 224
    Top = 112
    Width = 63
    Height = 21
    EditMask = '99/9999'
    MaxLength = 7
    TabOrder = 8
    Text = '  /    '
  end
  object RadioGroup1: TRadioGroup [14]
    Left = 88
    Top = 144
    Width = 185
    Height = 41
    Caption = 'Modelo'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Excel')
    TabOrder = 9
  end
  object CheckBox1: TCheckBox [15]
    Left = 88
    Top = 192
    Width = 129
    Height = 17
    Caption = 'Exibir contas zeradas'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  object RadioGroup2: TRadioGroup [16]
    Left = 280
    Top = 144
    Width = 185
    Height = 41
    Caption = 'Tipo'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Anal'#237'tico'
      'Sint'#233'tico')
    TabOrder = 11
  end
  object edtLoja: TMaskEdit [17]
    Left = 88
    Top = 88
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object DBEdit1: TDBEdit [18]
    Tag = 1
    Left = 152
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsQryLoja
    TabOrder = 12
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nPK')
    Left = 536
    Top = 112
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 536
    Top = 304
  end
  object DataSource3: TDataSource
    Left = 576
    Top = 352
  end
  object DataSource4: TDataSource
    Left = 648
    Top = 352
  end
  object DataSource5: TDataSource
    Left = 704
    Top = 336
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 584
    Top = 112
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsQryLoja: TDataSource
    DataSet = qryLoja
    Left = 688
    Top = 192
  end
end
