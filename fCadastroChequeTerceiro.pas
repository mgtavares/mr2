unit fCadastroChequeTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCadastroChequeTerceiro = class(TfrmCadastro_Padrao)
    qryMasternCdCheque: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TStringField;
    qryMastercConta: TStringField;
    qryMastercDigito: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasteriNrCheque: TIntegerField;
    qryMasternValCheque: TBCDField;
    qryMasterdDtDeposito: TDateTimeField;
    qryMasternCdTerceiroResp: TIntegerField;
    qryMasternCdTerceiroPort: TIntegerField;
    qryMasterdDtRemessaPort: TDateTimeField;
    qryMasternCdTabTipoCheque: TIntegerField;
    qryMastercChave: TStringField;
    qryMasternCdContaBancariaDep: TIntegerField;
    qryMasterdDtDevol: TDateTimeField;
    qryMastercFlgCompensado: TIntegerField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMastercNmFavorecido: TStringField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasteriAutorizacao: TIntegerField;
    qryMasternCdStatusDocto: TIntegerField;
    qryMasternCdProvisaoTit: TIntegerField;
    qryMasternCdTabStatusCheque: TIntegerField;
    qryMasterdDtSegDevol: TDateTimeField;
    qryMasterdDtStatus: TDateTimeField;
    qryMasternCdTituloCheque: TIntegerField;
    qryMastercOBSCheque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit14: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit15: TDBEdit;
    DataSource2: TDataSource;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit16: TDBEdit;
    DataSource3: TDataSource;
    qryMastercFlgChequeManual: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit12Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit8Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroChequeTerceiro: TfrmCadastroChequeTerceiro;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCadastroChequeTerceiro.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CHEQUE' ;
  nCdTabelaSistema  := 32 ;
  nCdConsultaPadrao := 167 ;
  bCodigoAutomatico := False ;

end;

procedure TfrmCadastroChequeTerceiro.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit11.Text) ;
  
end;

procedure TfrmCadastroChequeTerceiro.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  PosicionaQuery(qryContaBancaria, DBEdit12.Text) ;

end;

procedure TfrmCadastroChequeTerceiro.qryMasterBeforePost(
  DataSet: TDataSet);
begin

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o banco do cheque.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Informe a ag�ncia do cheque.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a conta do cheque.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Informe o d�gito verificador da conta do cheque.') ;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (DBEdit7.Text = '') then
  begin
      MensagemAlerta('Informe o n�mero do cheque.') ;
      DBEdit7.SetFocus ;
      abort ;
  end ;

  if (DBEdit8.Text = '') then
  begin
      MensagemAlerta('Informe o CNPJ ou CPF do emissor do cheque.') ;
      DBEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternValCheque.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do cheque.') ;
      DBEdit9.SetFocus ;
      abort ;
  end ;

  if (trim(DBEdit10.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de dep�sito.') ;
      DBEdit10.SetFocus ;
      abort ;
  end ;

  if (trim(DBEdit15.Text) = '') then
  begin
      MensagemAlerta('Informe o terceiro respons�vel.') ;
      DBEdit11.SetFocus ;
      abort ;
  end ;

  if (trim(DBEdit16.Text) = '') then
  begin
      MensagemAlerta('Informe a conta portadora.') ;
      DBEdit12.SetFocus ;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdEmpresa.Value         := frmMenu.nCdEmpresaAtiva;
      qryMasternCdTabTipoCheque.Value   := 2 ;
      qryMasternCdTabStatusCheque.Value := 1 ;
      qryMasternCdStatusDocto.Value     := 1 ;
      qryMasterdDtStatus.Value          := Now() ;
      qryMastercFlgChequeManual.Value   := 1 ;
      qryMastercFlgCompensado.Value     := 0 ;

      if (qryMasternCdCheque.Value = 0) then
          qryMasternCdCheque.Value := frmMenu.fnProximoCodigo('CHEQUE') ;
      
  end ;

  inherited;

end;

procedure TfrmCadastroChequeTerceiro.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  if ((qryMaster.State = dsInsert) and (DbEdit8.Text <> '')) then
  begin
      if (frmMenu.TestaCpfCgc(trim(DbEdit8.Text)) = '') then
      begin
          abort ;
      end ;
  end ;

end;

procedure TfrmCadastroChequeTerceiro.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  
  DBEdit3.SetFocus ;
end;

procedure TfrmCadastroChequeTerceiro.DBEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroResp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCadastroChequeTerceiro.DBEdit12KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND ((cFlgCaixa = 1 OR cFlgCofre = 1))');

            If (nPK > 0) then
            begin
                qryMasternCdContaBancariaDep.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmCadastroChequeTerceiro.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmCadastroChequeTerceiro.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  DBEdit12.Enabled := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  DBEdit12.Enabled := False ;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiroResp.AsString) ;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancariaDep.asString) ;

end;

procedure TfrmCadastroChequeTerceiro.qryMasterAfterCancel(
  DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryContaBancaria.Close ;
  
end;

initialization
    RegisterClass(TfrmCadastroChequeTerceiro) ;
    
end.
