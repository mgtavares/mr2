unit fPedidoComercial_PreDistribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DB, ADODB, StdCtrls,
  cxPC, cxControls, DBClient;

type
  TfrmPedidoComercial_PreDistribuicao = class(TfrmProcesso_Padrao)
    gridItemGrade: TDBGridEh;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryLojanCdLoja: TIntegerField;
    dsGrade: TDataSource;
    qryBuscaQtdeDistrib: TADOQuery;
    qryBuscaQtdeDistribnQtdeDistrib: TIntegerField;
    qryInsUpDel: TADOQuery;
    cdsGrade: TClientDataSet;
    qryGrade: TADOQuery;
    qryGradeiTamanho: TIntegerField;
    qryGradenCdItemPedido: TIntegerField;
    qryBuscaTotais: TADOQuery;
    qryBuscaTotaisnQtde: TBCDField;
    procedure ToolButton1Click(Sender: TObject);
    procedure exibeGradePreDistrib(nCdLocalEstoque, nCdItemPedidoPai : integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gridItemGradeColExit(Sender: TObject);
    procedure atualizaTotais(iTamanho : integer);
    procedure gridItemGradeColEnter(Sender: TObject);
    procedure cdsGradeBeforePost(DataSet: TDataSet);
    procedure cdsGradeBeforeCancel(DataSet: TDataSet);
    procedure gridItemGradeDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumnEh;
      State: TGridDrawState);
    procedure gridItemGradeCellClick(Column: TColumnEh);
    procedure gridItemGradeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    iRegistro : integer;
  public
    { Public declarations }
  end;

var
  frmPedidoComercial_PreDistribuicao: TfrmPedidoComercial_PreDistribuicao;

implementation

uses
    fMenu;

{$R *.dfm}

{ TfrmPedidoComercial_PreDistribuicao }

procedure TfrmPedidoComercial_PreDistribuicao.exibeGradePreDistrib(
  nCdLocalEstoque, nCdItemPedidoPai : integer);
var
    i, iOrdem : integer;
begin
    iOrdem := 0;

    qryGrade.Close;
    PosicionaQuery(qryGrade, IntToStr(nCdItemPedidoPai));

    with (cdsGrade.FieldDefs) do
    begin
        Add('iOrdem',ftInteger,0,False);
        Add('cLojaQtde',ftString,30,False);

        qryGrade.First;

        while not (qryGrade.Eof) do
        begin
            Add(qryGradeiTamanho.AsString,ftInteger,0,False);

            qryGrade.Next;
        end;
    end;

    cdsGrade.CreateDataSet;

    qryLoja.Close;
    PosicionaQuery(qryLoja, IntToStr(nCdLocalEstoque));

    with (cdsGrade.FieldList) do
    begin
        FieldByName('cLojaQtde').DisplayLabel := ' ';

        // Insere as lojas
        qryLoja.First;

        while not (qryLoja.Eof) do
        begin
            cdsGrade.Insert;

            FieldByName('iOrdem').Value    := iOrdem;
            FieldByName('cLojaQtde').Value := qryLojacNmLoja.Value;

            qryBuscaQtdeDistrib.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value;

            qryGrade.First;

            while not (qryGrade.Eof) do
            begin
                qryBuscaQtdeDistrib.Close;
                qryBuscaQtdeDistrib.Parameters.ParamByName('nCdItemPedido').Value := qryGradenCdItemPedido.Value;
                qryBuscaQtdeDistrib.Open;

                FieldByName(qryGradeiTamanho.AsString).Value := qryBuscaQtdeDistribnQtdeDistrib.Value;

                qryGrade.Next;
            end;

            cdsGrade.Post;

            qryLoja.Next;

            Inc(iOrdem);
        end;

        // Insere a linha de Qtd. Pedida
        cdsGrade.Insert;
        FieldByName('iOrdem').Value    := iOrdem + 1;
        FieldByName('cLojaQtde').Value := 'Qtd. Ped.';
        cdsGrade.Post;

        // Insere a linha de Qtd. Distribuida
        cdsGrade.Insert;
        FieldByName('iOrdem').Value    := iOrdem + 2;
        FieldByName('cLojaQtde').Value := 'Qtd. Distrib.';
        cdsGrade.Post;

        // Insere a Linha de Saldo
        cdsGrade.Insert;
        FieldByName('iOrdem').Value    := iOrdem + 3;
        FieldByName('cLojaQtde').Value := 'Saldo';
        cdsGrade.Post;

        cdsGrade.IndexFieldNames := 'iOrdem';

        for i := 1 to 3 do
        begin
            qryBuscaTotais.Parameters.ParamByName('iTipoValor').Value := i;

            cdsGrade.RecNo := (cdsGrade.RecordCount - 3 + i);

            cdsGrade.Edit;

            qryGrade.First;

            while not (qryGrade.Eof) do
            begin
                qryBuscaTotais.Close;
                qryBuscaTotais.Parameters.ParamByName('nCdItemPedido').Value := qryGradenCdItemPedido.Value;
                qryBuscaTotais.Open;

                FieldByName(qryGradeiTamanho.AsString).Value := qryBuscaTotaisnQtde.Value;

                qryGrade.Next;
            end;

            cdsGrade.Post;
        end;

        FieldByName('iOrdem').Visible     := False;
        FieldByName('cLojaQtde').ReadOnly := True;

    end;

    with gridItemGrade.Columns do

        for i := 0 to Count - 1 do

            if Items[i].ReadOnly then
            begin
                Items[i].Color            := clSilver;
                Items[i].Title.Font.Color := clRed;
                Items[i].Width            := 100;
            end
            else
                Items[i].Width := 65;

    // Ajusta a Largura da Tela
    Width := (65 * (gridItemGrade.Columns.Count)) + 80;

    // Ajusta a Altura da Tela
    Height := (23 * (cdsGrade.RecordCount)) + 75;

    cdsGrade.First;

    ShowModal;
end;

procedure TfrmPedidoComercial_PreDistribuicao.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (cdsGrade.State = dsEdit) then
      cdsGrade.Post;

  if ((MessageDLG('Confirma a pr�-distribui��o dos itens?',mtConfirmation,[mbYes,mbNo],0)) = mrYes) then
  begin
      frmMenu.Connection.BeginTrans;

      try

          qryLoja.First;
          cdsGrade.First;

          while not (qryLoja.Eof) do
          begin

              qryGrade.First;

              while not (qryGrade.Eof) do
              begin
                  qryInsUpDel.Close;
                  qryInsUpDel.Parameters.ParamByName('nCdItemPedido').Value := qryGradenCdItemPedido.Value;
                  qryInsUpDel.Parameters.ParamByName('nCdLoja').Value       := qryLojanCdLoja.Value;
                  qryInsUpDel.Parameters.ParamByName('nQtdeDistrib').Value  := cdsGrade.FieldList.FieldByName(qryGradeiTamanho.AsString).Value;
                  qryInsUpDel.ExecSQL;

                  qryGrade.Next;
              end;

              qryLoja.Next;
              cdsGrade.Next;
          end;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Pr�-distribui��o processada com sucesso.');

      Close;
  end;

end;

procedure TfrmPedidoComercial_PreDistribuicao.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited; // Comentado devido � propriedade TabAsEnter do grid
end;

procedure TfrmPedidoComercial_PreDistribuicao.gridItemGradeColExit(
  Sender: TObject);
var
  iTamanho : integer;
begin
  inherited;

  if (cdsGrade.State <> dsEdit) then
      Exit;

  if (gridItemGrade.Columns.Items[gridItemGrade.Col - 1].ReadOnly) then
      Exit;

  iTamanho := StrToInt(cdsGrade.FieldList.Fields[gridItemGrade.Col].FieldName);

  // Recupera a linha atual
  iRegistro := cdsGrade.RecNo;

  // Ao sair de uma coluna que foi editada, valida os totais
  atualizaTotais(iTamanho);

  iRegistro := 0;
end;

procedure TfrmPedidoComercial_PreDistribuicao.atualizaTotais(iTamanho : integer);
var
  i , nQtdeDistrib, nQtdePed : integer;
begin
    cdsGrade.DisableControls;

    nQtdeDistrib := 0;

    with (cdsGrade.FieldList.FieldByName(IntToStr(iTamanho))) do
    begin
        // Posiciona no registro de Qtd. Ped.
        cdsGrade.RecNo := cdsGrade.RecordCount - 2;

        // Recupera a Qtd. Ped.
        nQtdePed := Value;

        qryLoja.First;
        cdsGrade.First;

        // Verifica e valida as quantidades por loja x tamanho do produto
        while not (qryLoja.Eof) do
        begin

            if (Value < 0) then
            begin
                MensagemAlerta('A quantidade pr�-distribu�da n�o pode ser menor que zero.');

                cdsGrade.EnableControls;

                // Posiciona na linha alterada pelo usu�rio
                cdsGrade.RecNo    := iRegistro;
                gridItemGrade.Col := gridItemGrade.FieldColumns[IntToStr(iTamanho)].Index + 1;

                cdsGrade.Edit;

                Abort;
            end;

            // Recupera a Qtd. Distrib. informada no registro
            nQtdeDistrib := nQtdeDistrib + Value;

            if (nQtdeDistrib > nQtdePed) then
            begin
                MensagemAlerta('O item n�o tem saldo dispon�vel para pr�-distribui��o.');

                cdsGrade.EnableControls;

                // Posiciona na linha alterada pelo usu�rio
                cdsGrade.RecNo    := iRegistro;
                gridItemGrade.Col := gridItemGrade.FieldColumns[IntToStr(iTamanho)].Index + 1;

                cdsGrade.Edit;

                Abort;
            end;

            qryLoja.Next;
            cdsGrade.Next;
        end;

        // Atualiza o valor do registro de Qtd. Distrib.
        cdsGrade.RecNo := cdsGrade.RecordCount - 1;

        cdsGrade.Edit;
        Value := nQtdeDistrib;
        cdsGrade.Post;

        // Atualiza o valor do registro de Saldo
        cdsGrade.Last;

        cdsGrade.Edit;
        Value := nQtdePed - nQtdeDistrib;
        cdsGrade.Post;

        cdsGrade.EnableControls;

        // Posiciona na linha alterada pelo usu�rio
        cdsGrade.RecNo    := iRegistro;
        gridItemGrade.Col := gridItemGrade.FieldColumns[IntToStr(iTamanho)].Index + 1;

    end;
end;

procedure TfrmPedidoComercial_PreDistribuicao.gridItemGradeColEnter(
  Sender: TObject);
begin
  inherited;

  if (gridItemGrade.Columns.Items[gridItemGrade.Col - 1].ReadOnly) then
      Exit;

  if (iRegistro = 0) then
      Exit;

  qryGrade.Close;
  qryGrade.Open;

  qryGrade.First;

  while not (qryGrade.Eof) do
  begin
      atualizaTotais(qryGradeiTamanho.Value);

      qryGrade.Next;
  end;

  iRegistro := 0;
end;

procedure TfrmPedidoComercial_PreDistribuicao.cdsGradeBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  // Ignora os posts realizados antes de exibir a tela
  if not (Visible) then
      Exit;

  // Exceto nas 3 �ltimas linhas (Totalizadores)
  if (cdsGrade.RecNo > cdsGrade.RecordCount - 3) then
      Exit;

  // Armazena a e qtde linha em que ser� verificada ap�s o post
  iRegistro := cdsGrade.RecNo;
end;

procedure TfrmPedidoComercial_PreDistribuicao.cdsGradeBeforeCancel(
  DataSet: TDataSet);
begin
  inherited;

  // Trata o cancelamento de edi��o do registro
  cdsGrade.FieldList.Fields[gridItemGrade.Col].Value := 0;
  cdsGrade.Post;

  atualizaTotais(StrToInt(gridItemGrade.Columns.Items[gridItemGrade.Col - 1].FieldName));

  iRegistro := 0;
end;

procedure TfrmPedidoComercial_PreDistribuicao.gridItemGradeDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  // Pinta as c�lulas de totais
  if (cdsGrade.RecNo > cdsGrade.RecordCount - 3) then
  begin
      gridItemGrade.Canvas.Brush.Color := clSilver;
      gridItemGrade.Canvas.FillRect(Rect);
      gridItemGrade.DefaultDrawDataCell(Rect,Column.Field,State);
  end;
end;

procedure TfrmPedidoComercial_PreDistribuicao.gridItemGradeCellClick(
  Column: TColumnEh);
begin
  inherited;

  if (cdsGrade.RecNo > cdsGrade.RecordCount - 3) then
      gridItemGrade.ReadOnly := True
  else
      gridItemGrade.ReadOnly := False;

  if (cdsGrade.State <> dsEdit) then
      if (iRegistro <> 0) then
          atualizaTotais(StrToInt(gridItemGrade.Columns.Items[gridItemGrade.Col - 1].FieldName));


end;

procedure TfrmPedidoComercial_PreDistribuicao.gridItemGradeKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  
  if (Key = VK_UP) then
      Key := 13;

  if (Key = VK_DOWN) then
      Key := 13;
end;

end.

