unit rPosTituloLiquidado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosTituloLiquidado = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText11: TQRDBText;
    QRShape2: TQRShape;
    QRDBText10: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRExpr4: TQRExpr;
    QRExpr6: TQRExpr;
    QRLabel21: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRDBText14: TQRDBText;
    QRLabel25: TQRLabel;
    QRDBText15: TQRDBText;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatorionCdEspTit: TIntegerField;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatorionCdSP: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatoriodDtLiq: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionCdBancoPortador: TIntegerField;
    usp_RelatoriocFlgPrev: TIntegerField;
    usp_RelatorioiParcela: TIntegerField;
    usp_RelatorioiDias: TIntegerField;
    usp_RelatorionValLiqDia: TFloatField;
    usp_RelatorionValLiqVencido: TFloatField;
    usp_RelatoriocNmContaLiq: TStringField;
    usp_RelatoriocNmFormaPagto: TStringField;
    usp_RelatorioiNrCheque: TIntegerField;
    usp_RelatoriocNmOperacao: TStringField;
    usp_RelatoriocNmUsuario: TStringField;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel23: TQRLabel;
    usp_RelatorionCdLojaTit: TIntegerField;
    usp_RelatorionCdEmpresa: TIntegerField;
    QRLabel24: TQRLabel;
    QRDBText19: TQRDBText;
    usp_RelatoriocNrNf: TStringField;
    QRLabel26: TQRLabel;
    QRDBText20: TQRDBText;
    QRImage1: TQRImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosTituloLiquidado: TrptPosTituloLiquidado;

implementation

{$R *.dfm}

end.
