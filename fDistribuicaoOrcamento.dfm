inherited frmDistribuicaoOrcamento: TfrmDistribuicaoOrcamento
  Caption = 'Distribui'#231#227'o Or'#231'amento'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 57
    Height = 407
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 28
    Align = alTop
    TabOrder = 1
    object chkContaSemOrcamento: TCheckBox
      Left = 8
      Top = 8
      Width = 161
      Height = 17
      Caption = 'Exibir Contas sem Or'#231'amento'
      TabOrder = 0
      OnClick = chkContaSemOrcamentoClick
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 57
    Width = 784
    Height = 407
    Align = alClient
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsTemp
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded, dcoInsertOnNewItemRowFocusing]
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGrid1DBTableView1nValorOrcamentoConta
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValorOrcamentoConta
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValorOrcamentoConta
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.NavigatorHints = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsBehavior.PullFocusing = True
      OptionsCustomize.ColumnHiding = True
      OptionsData.Appending = True
      object cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn
        Caption = 'Grupo de Contas'
        DataBinding.FieldName = 'cNmGrupoPlanoConta'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
        Styles.Content = cxStyle1
        Styles.Header = cxStyle1
      end
      object cxGrid1DBTableView1nCdPlanoConta: TcxGridDBColumn
        Caption = 'Conta'
        DataBinding.FieldName = 'nCdPlanoConta'
        Options.Editing = False
        Styles.Content = cxStyle1
      end
      object cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn
        Caption = 'Descri'#231#227'o'
        DataBinding.FieldName = 'cNmPlanoConta'
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 150
      end
      object cxGrid1DBTableView1nCdPlanoContaBase: TcxGridDBColumn
        Caption = 'Conta Base'
        DataBinding.FieldName = 'nCdPlanoContaBase'
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 102
      end
      object cxGrid1DBTableView1cNmPlanoContaBase: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Conta Base'
        DataBinding.FieldName = 'cNmPlanoContaBase'
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 150
      end
      object cxGrid1DBTableView1nPercentBase: TcxGridDBColumn
        Caption = '% Base'
        DataBinding.FieldName = 'nPercentBase'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 77
      end
      object cxGrid1DBTableView1cNmMetodoDistribOrcamento: TcxGridDBColumn
        Caption = 'M'#233'todo Distribui'#231#227'o'
        DataBinding.FieldName = 'cNmMetodoDistribOrcamento'
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 168
      end
      object cxGrid1DBTableView1nValorOrcamentoConta: TcxGridDBColumn
        Caption = 'Or'#231'amento'
        DataBinding.FieldName = 'nValorOrcamentoConta'
        HeaderAlignmentHorz = taRightJustify
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 141
      end
      object cxGrid1DBTableView1cOBS: TcxGridDBColumn
        Caption = 'Observa'#231#227'o'
        DataBinding.FieldName = 'cOBS'
        Options.Editing = False
        Styles.Content = cxStyle1
        Width = 139
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    BeforeDelete = qryTempBeforeDelete
    AfterDelete = qryTempAfterDelete
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempItemOrcamento'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempItemOrcamento (nCdItemOrcamento          i' +
        'nt'
      
        '                                    ,nCdPlanoConta             i' +
        'nt'
      
        '                                    ,cNmPlanoConta             v' +
        'archar(50)'
      
        '                                    ,nCdGrupoPlanoConta        i' +
        'nt'
      
        '                                    ,cNmGrupoPlanoConta        v' +
        'archar(50)'
      
        '                                    ,nCdPlanoContaBase         i' +
        'nt'
      
        '                                    ,cNmPlanoContaBase         v' +
        'archar(50)'
      
        '                                    ,nPercentBase              d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,nCdMetodoDistribOrcamento i' +
        'nt                     not null'
      
        '                                    ,cNmMetodoDistribOrcamento v' +
        'archar(50)'
      
        '                                    ,nValorOrcamentoConta      d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,cOBS                      v' +
        'archar(100)'
      
        '                                    ,cOBSGeral                 t' +
        'ext)'
      ''
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempItemOrcamento'
      ' ORDER BY cNmGrupoPlanoConta, cNmPlanoConta')
    Left = 312
    Top = 160
    object qryTempnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryTempnCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryTempnCdPlanoContaBase: TIntegerField
      FieldName = 'nCdPlanoContaBase'
    end
    object qryTempnPercentBase: TBCDField
      FieldName = 'nPercentBase'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryTempnValorOrcamentoConta: TBCDField
      FieldName = 'nValorOrcamentoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempcOBS: TStringField
      FieldName = 'cOBS'
      Size = 100
    end
    object qryTempcOBSGeral: TMemoField
      FieldName = 'cOBSGeral'
      BlobType = ftMemo
    end
    object qryTempnCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryTempcNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryTempcNmMetodoDistribOrcamento: TStringField
      FieldName = 'cNmMetodoDistribOrcamento'
      Size = 50
    end
    object qryTempcNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryTempcNmPlanoContaBase: TStringField
      FieldName = 'cNmPlanoContaBase'
      Size = 50
    end
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 352
    Top = 160
  end
  object SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdOrcamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cFlgComplementar'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
      end>
    Left = 272
    Top = 240
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempItemOrcamento'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempItemOrcamento (nCdItemOrcamento          i' +
        'nt'
      
        '                                    ,nCdPlanoConta             i' +
        'nt'
      
        '                                    ,cNmPlanoConta             v' +
        'archar(50)'
      
        '                                    ,nCdGrupoPlanoConta        i' +
        'nt'
      
        '                                    ,cNmGrupoPlanoConta        v' +
        'archar(50)'
      
        '                                    ,nCdPlanoContaBase         i' +
        'nt'
      
        '                                    ,cNmPlanoContaBase         v' +
        'archar(50)'
      
        '                                    ,nPercentBase              d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,nCdMetodoDistribOrcamento i' +
        'nt                     not null'
      
        '                                    ,cNmMetodoDistribOrcamento v' +
        'archar(50)'
      
        '                                    ,nValorOrcamentoConta      d' +
        'ecimal(12,2) default 0 not null'
      
        '                                    ,cOBS                      v' +
        'archar(100)'
      
        '                                    ,cOBSGeral                 t' +
        'ext)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempItemOrcamento '
      ''
      'DECLARE @nCdOrcamento int'
      ''
      'Set @nCdOrcamento = :nPK'
      ''
      'INSERT INTO #TempItemOrcamento (nCdItemOrcamento'
      '                               ,nCdPlanoConta         '
      '                               ,cNmPlanoConta    '
      '                               ,nCdGrupoPlanoConta        '
      '                               ,cNmGrupoPlanoConta        '
      '                               ,nCdPlanoContaBase'
      '                               ,cNmPlanoContaBase'
      '                               ,nPercentBase              '
      '                               ,nCdMetodoDistribOrcamento'
      '                               ,cNmMetodoDistribOrcamento'
      '                               ,nValorOrcamentoConta'
      '                               ,cOBS                      '
      '                               ,cOBSGeral)'
      '                         SELECT Item.nCdItemOrcamento '
      '                               ,Item.nCdPlanoConta'
      
        '                               ,SUBSTRING(RTRIM(LTRIM(IsNull(LTR' +
        'IM(RTRIM(GrupoPlanoConta.cMascaraGrupo)),'#39#39') + '#39'.'#39' + IsNull(Plan' +
        'oConta.cMascara,'#39#39'))) + '#39' - '#39' + PlanoConta.cNmPlanoConta,1,50)'
      '                               ,PlanoConta.nCdGrupoPlanoConta'
      
        '                               ,SUBSTRING(IsNull(GrupoPlanoConta' +
        '.cMascaraGrupo,'#39#39') + '#39' '#39' + cNmGrupoPlanoConta,1,50)        '
      '                               ,Item.nCdPlanoContaBase'
      '                               ,PlanoContaBase.cNmPlanoConta'
      '                               ,Item.nPercentBase              '
      '                               ,Item.nCdMetodoDistribOrcamento'
      
        '                               ,MetodoDistribOrcamento.cNmMetodo' +
        'DistribOrcamento'
      '                               ,Item.nValorOrcamentoConta      '
      '                               ,Item.cOBS                      '
      '                               ,Item.cOBSGeral'
      '                           FROM ItemOrcamento Item'
      
        '                                INNER JOIN PlanoConta           ' +
        '     ON PlanoConta.nCdPlanoConta                         = Item.' +
        'nCdPlanoConta'
      
        '                                LEFT  JOIN PlanoConta PlanoConta' +
        'Base ON PlanoContaBase.nCdPlanoConta                     = Item.' +
        'nCdPlanoContaBase'
      
        '                                INNER JOIN GrupoPlanoConta      ' +
        '     ON GrupoPlanoConta.nCdGrupoPlanoConta               = Plano' +
        'Conta.nCdGrupoPlanoConta'
      
        '                                INNER JOIN MetodoDistribOrcament' +
        'o    ON MetodoDistribOrcamento.nCdMetodoDistribOrcamento = Item.' +
        'nCdMetodoDistribOrcamento'
      
        '                          WHERE Item.nCdOrcamento = @nCdOrcament' +
        'o'
      '')
    Left = 232
    Top = 168
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 152
    Top = 208
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
    end
  end
  object qryMetodoDistribOrcamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM MetodoDistribOrcamento')
    Left = 232
    Top = 320
    object qryMetodoDistribOrcamentonCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryMetodoDistribOrcamentocNmMetodoDistribOrcamento: TStringField
      FieldName = 'cNmMetodoDistribOrcamento'
      Size = 50
    end
    object qryMetodoDistribOrcamentocFlgLinear: TIntegerField
      FieldName = 'cFlgLinear'
    end
    object qryMetodoDistribOrcamentocFlgPadrao: TIntegerField
      FieldName = 'cFlgPadrao'
    end
  end
  object dsMetodoDistribOrcamento: TDataSource
    DataSet = qryMetodoDistribOrcamento
    Left = 264
    Top = 320
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 424
    Top = 240
  end
  object SP_EXCLUI_CONTA_SEM_ORCAMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_EXCLUI_CONTA_SEM_ORCAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdOrcamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 520
    Top = 176
  end
end
