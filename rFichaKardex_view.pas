unit rFichaKardex_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFichaKardex_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand5: TQRBand;
    lblFiltro3: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    lblIntervalo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape6: TQRShape;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    qryResultado: TADOQuery;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmProduto: TStringField;
    qryResultadodDtMovto: TDateTimeField;
    qryResultadocNmLocalEstoque: TStringField;
    qryResultadonCdOperacaoEstoque: TIntegerField;
    qryResultadocNmOperacaoEstoque: TStringField;
    qryResultadonSaldoAnterior: TBCDField;
    qryResultadonQtde: TBCDField;
    qryResultadonSaldoPosterior: TBCDField;
    qryResultadocOBS: TStringField;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel1: TQRLabel;
    qryResultadonCdLoja: TStringField;
    qryResultadocNmGrupoProduto: TStringField;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape2: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    qryResultadonTotalSaidas: TBCDField;
    qryResultadonTotalEntradas: TBCDField;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaKardex_view: TrptFichaKardex_view;

implementation

{$R *.dfm}

end.
