inherited frmAjusteEmpenho_IncluirEditar: TfrmAjusteEmpenho_IncluirEditar
  Width = 758
  Height = 182
  Caption = 'Incluir Empenho'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 742
    Height = 117
  end
  object Label2: TLabel [1]
    Left = 8
    Top = 120
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtde. Empenhado'
    FocusControl = DBEdit2
  end
  object Label1: TLabel [2]
    Left = 478
    Top = 48
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ult. Altera'#231#227'o'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [3]
    Left = 457
    Top = 72
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Altera'#231#227'o'
  end
  object Label4: TLabel [4]
    Left = 14
    Top = 72
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Estoque'
  end
  object Label5: TLabel [5]
    Left = 63
    Top = 96
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedido'
  end
  object Label6: TLabel [6]
    Left = 177
    Top = 96
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero OP'
  end
  object Label7: TLabel [7]
    Left = 57
    Top = 48
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  inherited ToolBar1: TToolBar
    Width = 742
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [9]
    Left = 98
    Top = 112
    Width = 65
    Height = 21
    DataField = 'nQtdeEstoqueEmpenhado'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 546
    Top = 40
    Width = 170
    Height = 21
    DataField = 'dDtAltManual'
    DataSource = DataSource1
    TabOrder = 6
  end
  object edtOP: TMaskEdit [11]
    Left = 234
    Top = 88
    Width = 65
    Height = 21
    TabOrder = 4
    OnChange = edtOPChange
  end
  object edtPedido: TMaskEdit [12]
    Left = 98
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    OnChange = edtPedidoChange
  end
  object DBEdit6: TDBEdit [13]
    Tag = 1
    Left = 168
    Top = 40
    Width = 280
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource2
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [14]
    Tag = 1
    Left = 168
    Top = 64
    Width = 280
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = DataSource3
    TabOrder = 8
  end
  object DBEdit8: TDBEdit [15]
    Tag = 1
    Left = 616
    Top = 64
    Width = 100
    Height = 21
    DataField = 'cNmLogin'
    DataSource = DataSource4
    TabOrder = 9
  end
  object edtProduto: TER2LookupDBEdit [16]
    Left = 98
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdProduto'
    DataSource = DataSource1
    TabOrder = 1
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object edtLocalEstoque: TER2LookupDBEdit [17]
    Left = 98
    Top = 64
    Width = 65
    Height = 21
    DataField = 'nCdLocalEstoque'
    DataSource = DataSource1
    TabOrder = 2
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object edtUsuarioAlt: TER2LookupDBEdit [18]
    Tag = 1
    Left = 546
    Top = 64
    Width = 65
    Height = 21
    DataField = 'nCdUsuarioAltManual'
    DataSource = DataSource1
    TabOrder = 10
    CodigoLookup = 0
    QueryLookup = qryUsuario
  end
  inherited ImageList1: TImageList
    Left = 288
    Top = 120
  end
  object qryEstoqueEmpenhado: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryEstoqueEmpenhadoBeforePost
    AfterScroll = qryEstoqueEmpenhadoAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM EstoqueEmpenhado'
      ' WHERE nCdEstoqueEmpenhado = :nPK')
    Left = 656
    Top = 88
    object qryEstoqueEmpenhadonCdEstoqueEmpenhado: TAutoIncField
      FieldName = 'nCdEstoqueEmpenhado'
      ReadOnly = True
    end
    object qryEstoqueEmpenhadonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryEstoqueEmpenhadonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEstoqueEmpenhadonQtdeEstoqueEmpenhado: TBCDField
      FieldName = 'nQtdeEstoqueEmpenhado'
      Precision = 12
      Size = 2
    end
    object qryEstoqueEmpenhadonCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryEstoqueEmpenhadodDtEmpenho: TDateTimeField
      FieldName = 'dDtEmpenho'
    end
    object qryEstoqueEmpenhadonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryEstoqueEmpenhadonCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryEstoqueEmpenhadocFlgInclusoManual: TIntegerField
      FieldName = 'cFlgInclusoManual'
    end
    object qryEstoqueEmpenhadonCdUsuarioAltManual: TIntegerField
      FieldName = 'nCdUsuarioAltManual'
    end
    object qryEstoqueEmpenhadodDtAltManual: TDateTimeField
      FieldName = 'dDtAltManual'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEstoqueEmpenhado
    Left = 672
    Top = 120
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '              ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 328
    Top = 88
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '              ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 360
    Top = 88
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryProduto
    Left = 344
    Top = 120
  end
  object DataSource3: TDataSource
    DataSet = qryLocalEstoque
    Left = 376
    Top = 120
  end
  object qryBuscaOP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNumeroOP'
        DataType = ftString
        Precision = 20
        Size = 20
        Value = Null
      end
      item
        Name = 'nCdOP'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP'
      '  FROM OrdemProducao '
      ' WHERE cNumeroOP        = :cNumeroOP'
      '    OR nCdOrdemProducao = :nCdOP')
    Left = 440
    Top = 88
    object qryBuscaOPnCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryBuscaOPcNumeroOP: TStringField
      FieldName = 'cNumeroOP'
    end
  end
  object qryBuscaPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdItemPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 nCdItemPedido'
      '            ,nCdPedido'
      '  FROM ItemPedido'
      
        ' WHERE ((nCdPedido  = :nCdPedido) OR (nCdItemPedido = :nCdItemPe' +
        'dido)) '
      '   AND nCdProduto = :nCdProduto')
    Left = 472
    Top = 88
    object qryBuscaPedidonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryBuscaPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '             ,cNmLogin'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 504
    Top = 88
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmLogin: TStringField
      FieldName = 'cNmLogin'
    end
  end
  object DataSource4: TDataSource
    DataSet = qryUsuario
    Left = 520
    Top = 120
  end
end
