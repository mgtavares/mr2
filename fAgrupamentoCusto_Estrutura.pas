unit fAgrupamentoCusto_Estrutura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxContainer, cxTreeView, DB, ADODB, Menus;

type
  TfrmAgrupamentoCusto_Estrutura = class(TfrmProcesso_Padrao)
    tvCC: TcxTreeView;
    qryContaAgrupamento: TADOQuery;
    PopupMenu1: TPopupMenu;
    AdicionarSubConta1: TMenuItem;
    ExcluirSubConta1: TMenuItem;
    N1: TMenuItem;
    VincularContaReduzida1: TMenuItem;
    RemoverVinculoContaContbil1: TMenuItem;
    AdicionarContaContbil1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryAux: TADOQuery;
    AlterarSubConta1: TMenuItem;
    qryContaAgrupamentonCdContaAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentonCdContaAgrupamentoCustoPai: TIntegerField;
    qryContaAgrupamentonCdAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentocMascara: TStringField;
    qryContaAgrupamentocNmContaAgrupamentoCusto: TStringField;
    qryContaAgrupamentoiNivel: TIntegerField;
    qryContaAgrupamentoiSequencia: TIntegerField;
    qryContaAgrupamentonCdCC: TIntegerField;
    qryContaAgrupamentocFlgAnalSintetica: TStringField;
    procedure FormShow(Sender: TObject);
    procedure adicionaFilhos( nCdContaAgrupamentoCusto : integer ; node : TTreeNode) ;
    procedure AdicionarSubConta1Click(Sender: TObject);
    procedure exibeEstrutura;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ExcluirSubConta1Click(Sender: TObject);
    procedure AlterarSubConta1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure VincularContaReduzida1Click(Sender: TObject);
    procedure atualizaExpande;
    procedure RemoverVinculoContaContbil1Click(Sender: TObject);
    procedure AdicionarContaContbil1Click(Sender: TObject);
  private
    { Private declarations }
    raiz, rootno, no : TTreeNode;
  public
    { Public declarations }
    nCdAgrupamentoCusto : integer ;
    cNmAgrupamentoCusto : string ;
    cFormatoMascara     : string ;
  end;

var
  frmAgrupamentoCusto_Estrutura: TfrmAgrupamentoCusto_Estrutura;

implementation

uses fMenu, fAgrupamentoCusto_AddSubConta, fAgrupamentoCusto_VinculaCC, fCC;

{$R *.dfm}

procedure TfrmAgrupamentoCusto_Estrutura.adicionaFilhos(
  nCdContaAgrupamentoCusto: integer ; node : TTreeNode);
var
  qryContaAgrupamentoFilho : TADOQuery;
begin

  qryContaAgrupamentoFilho := TADOQuery.Create( Self );
  qryContaAgrupamentoFilho.Connection := frmMenu.Connection;
  qryContaAgrupamentoFilho.SQL.Add('SELECT nCdContaAgrupamentoCusto, cMascara, cNmContaAgrupamentoCusto, cFlgAnalSintetica, nCdCC FROM ContaAgrupamentoCusto WHERE nCdContaAgrupamentoCustoPai = :nPK ORDER BY cMascara') ;

  qryContaAgrupamentoFilho.Close;
  PosicionaQuery( qryContaAgrupamentoFilho , intToStr( nCdContaAgrupamentoCusto ) ) ;

  qryContaAgrupamentoFilho.FieldList.Update;

    while not qryContaAgrupamentoFilho.eof do
    begin

      if ( qryContaAgrupamentoFilho.FieldList[4].asString <> '' ) then
          no := tvCC.Items.AddChildObject(node, frmMenu.fnAplicaMascaraContabil( qryContaAgrupamentoFilho.FieldList[1].Value , cFormatoMascara ) + ' - ' + qryContaAgrupamentoFilho.FieldList[2].Value + ' (' + qryContaAgrupamentoFilho.FieldList[4].asString + ')', Pointer( qryContaAgrupamentoFilho.FieldList[0].asInteger ))
      else no := tvCC.Items.AddChildObject(node, frmMenu.fnAplicaMascaraContabil( qryContaAgrupamentoFilho.FieldList[1].Value , cFormatoMascara ) + ' - ' + qryContaAgrupamentoFilho.FieldList[2].Value, Pointer( qryContaAgrupamentoFilho.FieldList[0].asInteger )) ;
      
      no.Data := Pointer( qryContaAgrupamentoFilho.FieldList[0].asInteger ) ;

      if (qryContaAgrupamentoFilho.FieldList[3].Value = 'A') then
      begin
          no.ImageIndex    := 3;
          no.SelectedIndex := 3;
      end
      else
      begin
          no.ImageIndex    := 2;
          no.SelectedIndex := 2;
      end ;

      adicionaFilhos( qryContaAgrupamentoFilho.FieldList[0].asInteger , no ) ;

      qryContaAgrupamentoFilho.Next ;

    end ;

    qryContaAgrupamentoFilho.Close;

    freeAndNil( qryContaAgrupamentoFilho ) ;

end;

procedure TfrmAgrupamentoCusto_Estrutura.FormShow(Sender: TObject);
begin
  inherited;

  AdicionarContaContbil1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMCC') ;

  ToolButton1.Click;
  ToolButton5.Click;

end;

procedure TfrmAgrupamentoCusto_Estrutura.AdicionarSubConta1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoCusto_AddSubConta;
  i : integer;
begin
  inherited;

  objForm := TfrmAgrupamentoCusto_AddSubConta.Create( Self ) ;

  try

      objForm.cFormatoMascara := cFormatoMascara ;

      objForm.adicionaSubConta( nCdAgrupamentoCusto
                               ,Integer( tvCC.Selected.Data )
                               ,TRUE );

  finally
      freeAndNil( objForm ) ;
  end ;

  atualizaExpande;
  
end;

procedure TfrmAgrupamentoCusto_Estrutura.exibeEstrutura;
begin

  tvCC.Items.Clear;
  
  raiz := tvCC.Items.AddObject(nil, cNmAgrupamentoCusto , Pointer( 0 )) ;
  raiz.ImageIndex    := 2;
  raiz.SelectedIndex := 2;

  qryContaAgrupamento.Close;
  PosicionaQuery( qryContaAgrupamento , intToStr( nCdAgrupamentoCusto ) ) ;

  while not qryContaAgrupamento.eof do
  begin

      rootno := tvCC.Items.AddChildObject(raiz, frmMenu.fnAplicaMascaraContabil( qryContaAgrupamentocMascara.Value , cFormatoMascara ) + ' - ' + qryContaAgrupamentocNmContaAgrupamentoCusto.Value, Pointer( qryContaAgrupamentonCdContaAgrupamentoCusto.Value ) ) ;
      rootno.Data := Pointer( qryContaAgrupamentonCdContaAgrupamentoCusto.Value );

      if (qryContaAgrupamentocFlgAnalSintetica.Value = 'A') then
      begin
          rootno.ImageIndex    := 3;
          rootno.SelectedIndex := 3;
      end
      else
      begin
          rootno.ImageIndex    := 2;
          rootno.SelectedIndex := 2;
      end ;

      adicionaFilhos( qryContaAgrupamentonCdContaAgrupamentoCusto.Value , rootno ) ;

      qryContaAgrupamento.Next;

  end ;

  qryContaAgrupamento.Close;

end;

procedure TfrmAgrupamentoCusto_Estrutura.ToolButton1Click(
  Sender: TObject);
var
  i : integer;
begin
  inherited;

  exibeEstrutura ;

end;

procedure TfrmAgrupamentoCusto_Estrutura.ToolButton5Click(
  Sender: TObject);
var
  x: byte;
begin

  for x := 0 to tvCC.Items.Count - 1 do
      tvCC.Items.Item[x].Expand(True);

end;

procedure TfrmAgrupamentoCusto_Estrutura.ToolButton7Click(
  Sender: TObject);
var
  x: byte;
begin

  for x := 0 to tvCC.Items.Count - 1 do
      tvCC.Items.Item[x].Collapse(True);

end;

procedure TfrmAgrupamentoCusto_Estrutura.ExcluirSubConta1Click(
  Sender: TObject);
var
  nCdContaAgrupamentoCusto, i : integer ;
begin
  inherited;

  if (tvCC.Selected.AbsoluteIndex > 0) then
  begin

      nCdContaAgrupamentoCusto := Integer( tvCC.Selected.Data ) ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM ContaAgrupamentoCusto WHERE nCdContaAgrupamentoCustoPai = ' + intToStr( nCdContaAgrupamentoCusto ) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin
          qryAux.Close;
          MensagemAlerta('Esta subconta tem contas/subcontas dependentes e n�o pode ser exclu�da.') ;
          abort;
      end ;

      qryAux.Close;

      if ( MessageDlg('Confirma a exclus�o desta subconta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
          exit;

      try
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ContaAgrupamentoCusto WHERE nCdContaAgrupamentoCusto = ' + intToStr( nCdContaAgrupamentoCusto ) ) ;
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoCusto_Estrutura.AlterarSubConta1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoCusto_AddSubConta;
  i : integer;
begin
  inherited;

  if (tvCC.Selected.AbsoluteIndex > 0) then
  begin

      objForm := TfrmAgrupamentoCusto_AddSubConta.Create( Self ) ;

      try

          objForm.cFormatoMascara := cFormatoMascara ;

          objForm.adicionaSubConta( nCdAgrupamentoCusto
                                   ,Integer( tvCC.Selected.Data )
                                   ,FALSE );

      finally
          freeAndNil( objForm ) ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoCusto_Estrutura.PopupMenu1Popup(
  Sender: TObject);
begin
  inherited;

  try
      AdicionarSubConta1.Enabled          := (tvCC.Selected.ImageIndex = 2) ;
      AlterarSubConta1.Enabled            := (tvCC.Selected.ImageIndex = 2) ;
      ExcluirSubConta1.Enabled            := (tvCC.Selected.ImageIndex = 2) ;
      VincularContaReduzida1.Enabled      := (tvCC.Selected.ImageIndex = 2) ;

      RemoverVinculoContaContbil1.Enabled := (tvCC.Selected.ImageIndex = 3) ;
  except
  end;

end;

procedure TfrmAgrupamentoCusto_Estrutura.VincularContaReduzida1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoCusto_VinculaCC ;
begin
  inherited;

  if (tvCC.Selected.AbsoluteIndex > 0) then
  begin

      objForm := TfrmAgrupamentoCusto_VinculaCC.Create( Self ) ;
      objForm.novoVinculo( Integer( tvCC.Selected.Data ) );

  end ;

  atualizaExpande ;

end;

procedure TfrmAgrupamentoCusto_Estrutura.atualizaExpande;
var
    i : integer ;
begin

  i := tvCC.Selected.AbsoluteIndex;
  exibeEstrutura ;
  ToolButton5.Click;

  try
      tvCC.Items.Item[i].Selected := True ;
  except
  end;

end;

procedure TfrmAgrupamentoCusto_Estrutura.RemoverVinculoContaContbil1Click(
  Sender: TObject);
var
  nCdContaAgrupamentoCusto, i : integer ;
begin
  inherited;

  if (tvCC.Selected.AbsoluteIndex > 0) then
  begin

      nCdContaAgrupamentoCusto := Integer( tvCC.Selected.Data ) ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM ContaAgrupamentoCusto WHERE nCdContaAgrupamentoCustoPai = ' + intToStr( nCdContaAgrupamentoCusto ) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin
          qryAux.Close;
          MensagemAlerta('Esta conta tem contas/subcontas dependentes e n�o pode ser exclu�da.') ;
          abort;
      end ;

      qryAux.Close;

      if ( MessageDlg('Confirma a exclus�o do v�nculo deste centro de custo ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
          exit;

      try
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ContaAgrupamentoCusto WHERE nCdContaAgrupamentoCusto = ' + intToStr( nCdContaAgrupamentoCusto ) ) ;
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoCusto_Estrutura.AdicionarContaContbil1Click(
  Sender: TObject);
var
  objForm : TfrmCC ;
begin
  inherited;

  objForm := TfrmCC.Create( Self );
  showForm( objForm , TRUE ) ;

end;

end.
