unit fCondPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, StdCtrls, Mask, DBCtrls, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmCondPagto = class(TfrmCadastro_Padrao)
    qryMasternCdCondPagto: TIntegerField;
    qryMastercNmCondPagto: TStringField;
    qryParc: TADOQuery;
    qryParcnCdCondPagto: TIntegerField;
    qryParciDiasPrazo: TIntegerField;
    qryParcnPercent: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    qryMasternCdFormaPagto: TIntegerField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasterdDtValidadeFim: TDateTimeField;
    qryMastercFlgAtivo: TIntegerField;
    qryMastercFlgPermDesconto: TIntegerField;
    qryMasternFatorArred: TBCDField;
    qryMasteriParcArred: TIntegerField;
    qryParcdDtSugerida: TDateTimeField;
    qryParcdDtMaxima: TDateTimeField;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DataSource2: TDataSource;
    qryAux: TADOQuery;
    qryLojaCondPagto: TADOQuery;
    dsLojaCondPagto: TDataSource;
    qryLojaCondPagtonCdLoja: TIntegerField;
    qryLojaCondPagtonCdCondPagto: TIntegerField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryLojaCondPagtocNmLoja: TStringField;
    qryMastercFlgPrimParcEnt: TIntegerField;
    qryMastercFlgPDV: TIntegerField;
    qryMastercFlgLiqCrediario: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryParcBeforePost(DataSet: TDataSet);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryLojaCondPagtoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCondPagto: TfrmCondPagto;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCondPagto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONDPAGTO' ;
  nCdTabelaSistema  := 47 ;
  nCdConsultaPadrao := 61 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmCondPagto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmCondPagto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagto.asString) ;
  PosicionaQuery(qryParc, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryLojaCondPagto, qryMasternCdCondPagto.asString) ;

end;

procedure TfrmCondPagto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  if not qryParc.Active then
  begin
      PosicionaQuery(qryParc, qryMasternCdCondPagto.asString) ;
      PosicionaQuery(qryLojaCondPagto, qryMasternCdCondPagto.asString) ;
  end ;

end;

procedure TfrmCondPagto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryParc.Close ;
  qryFormaPagto.Close ;
  qryLojaCondPagto.Close ;
end;

procedure TfrmCondPagto.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post ;
      
end;

procedure TfrmCondPagto.qryParcBeforePost(DataSet: TDataSet);
begin

  if (qryParciDiasPrazo.Value < 0) then
  begin
      MensagemAlerta('Prazo em dias inv�lido.') ;
      abort ;
  end ;

  if (qryParcnPercent.Value < 0) or (qryParcnPercent.Value > 100) then
  begin
      MensagemAlerta('Percentual inv�lido.') ;
      abort ;
  end ;

  if (qryParcdDtSugerida.Value > qryParcdDtMaxima.Value) then
  begin
      MensagemAlerta('Data sugerida n�o pode ser superior a data m�xima de pagamento.') ;
      abort ;
  end ;

  qryParcnCdCondPagto.Value := qryMasternCdCondPagto.Value ;
  
  inherited;

end;

procedure TfrmCondPagto.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(15);

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCondPagto.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, DbEdit6.Text) ;
  
end;

procedure TfrmCondPagto.qryMasterBeforePost(DataSet: TDataSet);
var
    nResto : integer ;
    nPercent : Double ;
begin

  if (Trim(DbEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o da condi��o.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      nPercent := 0 ;

      qryParc.First ;

      while not qryparc.Eof do
      begin
          nPercent := nPercent + qryParcnPercent.Value;
          qryParc.Next;
      end ;

      qryParc.First ;

      if (nPercent <> 100) then
      begin
          MensagemAlerta('A soma do percentual das parcelas � diferente de 100%, verifique.') ;
          abort ;
      end ;

  end ;

  qryMastercFlgPdv.Value := 0 ;
  
  inherited;

end;

procedure TfrmCondPagto.btSalvarClick(Sender: TObject);
begin
  inherited;

  qryMaster.UpdateBatch;
  qryParc.UpdateBatch;
  qryLojaCondPagto.UpdateBatch ;

  btCancelar.Click;

end;

procedure TfrmCondPagto.qryLojaCondPagtoBeforePost(DataSet: TDataSet);
begin

  if (qryLojaCondPagtonCdLoja.Value = 0) or (qryLojaCondPagtocNmLoja.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      abort ;
  end ;

  inherited;

  qryLojaCondPagtonCdCondPagto.Value := qryMasternCdCondPagto.Value ;

end;

procedure TfrmCondPagto.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryLojaCondPagto.State = dsBrowse) then
             qryLojaCondPagto.Edit ;
        
        if (qryLojaCondPagto.State = dsInsert) or (qryLojaCondPagto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(86);

            If (nPK > 0) then
            begin
                qryLojaCondPagtonCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmCondPagto) ;

end.
