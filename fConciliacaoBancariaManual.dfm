inherited frmConciliacaoBancariaManual: TfrmConciliacaoBancariaManual
  Left = -8
  Top = -8
  Width = 1152
  Height = 780
  Caption = 'Concilia'#231#227'o Banc'#225'ria Manual'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 713
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 713
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1nCdContaBancaria: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdContaBancaria'
        Width = 42
      end
      object cxGrid1DBTableView1nCdBanco: TcxGridDBColumn
        Caption = 'Banco'
        DataBinding.FieldName = 'nCdBanco'
        Width = 47
      end
      object cxGrid1DBTableView1cNmBanco: TcxGridDBColumn
        Caption = 'Nome Banco'
        DataBinding.FieldName = 'cNmBanco'
        Width = 214
      end
      object cxGrid1DBTableView1cAgencia: TcxGridDBColumn
        Caption = 'Ag'#234'ncia'
        DataBinding.FieldName = 'cAgencia'
      end
      object cxGrid1DBTableView1nCdConta: TcxGridDBColumn
        Caption = 'N'#250'mero Conta'
        DataBinding.FieldName = 'nCdConta'
      end
      object cxGrid1DBTableView1cNmTitular: TcxGridDBColumn
        Caption = 'Titular'
        DataBinding.FieldName = 'cNmTitular'
        Width = 273
      end
      object cxGrid1DBTableView1dDtUltConciliacao: TcxGridDBColumn
        Caption = #218'ltima Concilia'#231#227'o'
        DataBinding.FieldName = 'dDtUltConciliacao'
        Width = 124
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,Banco.nCdBanco'
      '      ,Banco.cNmBanco'
      '      ,cAgencia'
      '      ,nCdConta'
      '      ,cNmTitular'
      '      ,dDtUltConciliacao'
      '  FROM ContaBancaria'
      
        '       INNER JOIN Banco ON Banco.nCdBanco = ContaBancaria.nCdBan' +
        'co'
      ' WHERE nCdEmpresa = :nPK'
      '   AND cFlgCaixa  = 0'
      '   AND nCdStatus  = 1')
    Left = 296
    Top = 168
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancariadDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 384
    Top = 240
  end
end
