unit fConfigGrupoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, cxLookAndFeelPainters, cxButtons,
  ER2Lookup, Menus;

type
  TfrmConfigGrupoCliente = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    qryClienteVarejo: TADOQuery;
    qryClienteVarejonCdTerceiro: TIntegerField;
    qryClienteVarejocNmTerceiro: TStringField;
    qryClienteVarejocCNPJCPF: TStringField;
    qryClienteVarejodDtCadastro: TDateTimeField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsClienteVarejo: TDataSource;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGrid1DBTableView1dDtCadastro: TcxGridDBColumn;
    qryNovoCliente: TADOQuery;
    qryGrupoClienteDesc: TADOQuery;
    qryNovoClientenCdTerceiro: TIntegerField;
    qryNovoClientecNmTerceiro: TStringField;
    qryNovoClientecCNPJCPF: TStringField;
    dsNovoCliente: TDataSource;
    btnVisualizarGrade: TcxButton;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    er2LkpGrupo: TER2LookupMaskEdit;
    er2LkpCliente: TER2LookupMaskEdit;
    qryGrupoClienteDescnCdGrupoClienteDesc: TIntegerField;
    qryGrupoClienteDesccNmGrupoClienteDesc: TStringField;
    qryGrupoClienteDescnCdStatus: TIntegerField;
    qryGrupoClienteDescnPercDesconto: TBCDField;
    qryGrupoClienteDescnCdServidorOrigem: TIntegerField;
    qryGrupoClienteDescdDtReplicacao: TDateTimeField;
    dsGrupoClienteDesc: TDataSource;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    qryNovoClientenCdGrupoClienteDesc: TIntegerField;
    PopupMenu1: TPopupMenu;
    btRemoverGrupo: TMenuItem;
    qryClienteVarejonCdGrupoClienteDesc: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure btnVisualizarGradeClick(Sender: TObject);
    procedure btRemoverGrupoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfigGrupoCliente: TfrmConfigGrupoCliente;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmConfigGrupoCliente.ToolButton1Click(Sender: TObject);
begin
  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o grupo de clientes antes da consulta.');
      er2LkpGrupo.SetFocus;
      Abort;
  end;

  qryClienteVarejo.Close;
  PosicionaQuery(qryClienteVarejo, qryGrupoClienteDescnCdGrupoClienteDesc.AsString);
end;

procedure TfrmConfigGrupoCliente.btnVisualizarGradeClick(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o grupo de clientes para vincular ao novo cliente selecionado.');
      er2LkpGrupo.SetFocus;
      Abort;
  end;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe o novo cliente a ser vinculado no grupo ' + qryGrupoClienteDesccNmGrupoClienteDesc.Value + '.');
      er2LkpCliente.SetFocus;
      Abort;
  end;

  case MessageDlg('Confirma a inclus�o do cliente ' + qryNovoClientecNmTerceiro.Value + ' ao grupo ' + qryGrupoClienteDesccNmGrupoClienteDesc.Value + '  ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  inherited;

  try
      try
          frmMenu.Connection.BeginTrans;

          qryNovoCliente.Edit;
          qryNovoClientenCdGrupoClienteDesc.Value := qryGrupoClienteDescnCdGrupoClienteDesc.Value;
          qryNovoCliente.Post;
          qryNovoCliente.Close;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Cliente incluso para no grupo ' + qryGrupoClienteDesccNmGrupoClienteDesc.Value + ' com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      ToolButton1.Click;
      er2LkpCliente.SetFocus;
  end;
end;

procedure TfrmConfigGrupoCliente.btRemoverGrupoClick(Sender: TObject);
begin
  inherited;

  if (qryClienteVarejo.IsEmpty) then
      Exit;

  case MessageDlg('Confirma a exclus�o do cliente ' + qryNovoClientecNmTerceiro.Value + ' do grupo ' + qryGrupoClienteDesccNmGrupoClienteDesc.Value + '  ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      try
          frmMenu.Connection.BeginTrans;

          qryClienteVarejo.Edit;
          qryClienteVarejonCdGrupoClienteDesc.Clear;
          qryClienteVarejo.Post;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Cliente exclu�do do grupo ' + qryGrupoClienteDesccNmGrupoClienteDesc.Value + ' com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      ToolButton1.Click;
      er2LkpCliente.SetFocus;
  end;
end;

initialization
  RegisterClass(TfrmConfigGrupoCliente);

end.
