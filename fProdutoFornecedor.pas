unit fProdutoFornecedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, FMTBcd, Grids, DBGrids, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmProdutoFornecedor = class(TfrmProcesso_Padrao)
    qryProdutoFornecedor: TADOQuery;
    qryProdutoFornecedornCdProdutoFornecedor: TAutoIncField;
    qryProdutoFornecedornCdProduto: TIntegerField;
    qryProdutoFornecedornCdTerceiro: TIntegerField;
    qryProdutoFornecedorcNmTerceiro: TStringField;
    qryProdutoFornecedornPercentCompra: TBCDField;
    qryProdutoFornecedorcNrContrato: TStringField;
    qryProdutoFornecedornCdCondPagto: TIntegerField;
    qryProdutoFornecedorcNmCondPagto: TStringField;
    qryProdutoFornecedordDtValidadeIni: TDateTimeField;
    qryProdutoFornecedordDtValidadeFim: TDateTimeField;
    qryProdutoFornecedornCdMoeda: TIntegerField;
    qryProdutoFornecedorcNmMoeda: TStringField;
    qryProdutoFornecedornPercIPI: TBCDField;
    qryProdutoFornecedornPercICMSSub: TBCDField;
    dsProdutoFornecedor: TDataSource;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacNmMoeda: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    DBGridEh2: TDBGridEh;
    qryProdutoFornecedorcReferencia: TStringField;
    qryProdutoFornecedorcCatalogo: TStringField;
    qryProdutoFornecedornCdTipoPedidoCompra: TIntegerField;
    qryProdutoFornecedorcNmTipoPedido: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryProdutoFornecedornCdTipoRessuprimentoFornec: TIntegerField;
    qryProdutoFornecedorcNmTipoRessuprimento: TStringField;
    qryTipoRessuprimento: TADOQuery;
    qryTipoRessuprimentonCdTipoRessuprimento: TIntegerField;
    qryTipoRessuprimentocNmTipoRessuprimento: TStringField;
    qryProdutoFornecedornPercBCICMSSub: TBCDField;
    qryProdutoFornecedornPercIVA: TBCDField;
    qryProdutoFornecedornPercCredICMS: TBCDField;
    qryProdutoFornecedorcFlgDescST: TIntegerField;
    qryProdutoFornecedornPrecoUnit: TFloatField;
    qryProdutoFornecedornPrecoUnitEsp: TFloatField;
    procedure qryProdutoFornecedorBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryProdutoFornecedorCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh2ColEnter(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmProdutoFornecedor: TfrmProdutoFornecedor;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmProdutoFornecedor.qryProdutoFornecedorBeforePost(
  DataSet: TDataSet);
var
  nValor : Double ;
begin
  inherited;
  if (qryProdutoFornecedorcNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe o Fornecedor.') ;
      abort ;
  end ;

  if (qryProdutoFornecedorcNmTipoRessuprimento.Value = '') then
  begin
      MensagemAlerta('Informe o tipo ressuprimento.') ;
      abort ;
  end ;

  if (qryProdutoFornecedornCdTipoRessuprimentoFornec.Value = 2) then
  begin

      if (qryProdutoFornecedornPrecoUnit.Value <= 0) then
      begin
          MensagemAlerta('Informe o pre�o unit�rio.') ;
          abort ;
      end ;

      if (qryProdutoFornecedornCdCondPagto.Value = 0) or (qryProdutoFornecedorcNmCondPagto.Value = '') then
      begin
          MensagemAlerta('Informe a condi��o de pagamento.') ;
          abort ;
      end ;

      if (qryProdutoFornecedordDtValidadeFim.Value < qryProdutoFornecedordDtValidadeIni.Value) then
      begin
          MensagemAlerta('Per�odo de validade inv�lido.' +#13#13 + 'Deixe em branco para per�odo n�o definido.') ;
          abort ;
      end ;

      if (qryProdutoFornecedornPercIPI.Value < 0) then
      begin
          MensagemAlerta('Percentual de IPI inv�lido.') ;
          abort ;
      end ;

      if (qryProdutoFornecedornPercICMSSub.Value < 0) then
      begin
          MensagemAlerta('Percentual de ICMS de Substitui��o tribut�ria inv�lido.') ;
          abort ;
      end ;

      if (qryProdutoFornecedorcNmTipoPedido.Value = '') then
      begin
          MensagemAlerta('Selecione o tipo de pedido.') ;
          abort ;
      end ;

  end ;

  inherited;

  qryProdutoFornecedornCdProduto.Value := nCdProduto ;

  if (qryProdutoFornecedornCdMoeda.Value = 0) then
      qryProdutoFornecedornCdMoeda.Value := 1 ;

  {nValor := qryProdutoFornecedornPrecoUnit.Value ;

  qryProdutoFornecedornPrecoUnit.AsBCD := DoubleToBCD(nValor) ;}

end;

procedure TfrmProdutoFornecedor.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (    (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTerceiro'].Index)
             or (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTipoRessuprimentoFornec'].Index)
             or (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdMoeda'].Index)
             or (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdCondPagto'].Index)
             or (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTipoPedidoCompra'].Index)) then
        begin

            if (qryProdutoFornecedor.State = dsBrowse) then
                 qryProdutoFornecedor.Edit ;

            if (qryProdutoFornecedor.State = dsInsert) or (qryProdutoFornecedor.State = dsEdit) then
            begin

                //terceiro
                if (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTerceiro'].Index) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta(90);

                    If (nPK > 0) then
                    begin
                        qryProdutoFornecedornCdTerceiro.Value := nPK ;
                    end ;

                end ;

                //tipo ressuprimento
                if (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTipoRessuprimentoFornec'].Index) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta(139);

                    If (nPK > 0) then
                    begin
                        qryProdutoFornecedornCdTipoRessuprimentoFornec.Value := nPK ;
                    end ;

                end ;

                //moeda
                if (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdMoeda'].Index) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta(16);

                    If (nPK > 0) then
                    begin
                        qryProdutoFornecedornCdMoeda.Value := nPK ;
                    end ;

                end ;

                //cond pagto
                if (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdCondPagto'].Index) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta2(61,'cFlgPDV = 0');

                    If (nPK > 0) then
                    begin
                        qryProdutoFornecedornCdCondPagto.Value := nPK ;
                    end ;

                end ;

                //tipo de pedido
                if (dbGridEh2.SelectedIndex = dbGridEh2.FieldColumns['nCdTipoPedidoCompra'].Index) then
                begin
                    nPK := frmLookup_Padrao.ExecutaConsulta2(54,'cFlgCompra = 1');

                    If (nPK > 0) then
                    begin
                        qryProdutoFornecedornCdTipoPedidoCompra.Value := nPK ;
                    end ;

                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoFornecedor.qryProdutoFornecedorCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryTerceiro.Close ;
  qryCondPagto.Close ;
  qryMoeda.Close ;
  qryTipoPedido.Close ;
  qryTipoRessuprimento.Close;

  PosicionaQuery(qryTerceiro, qryProdutoFornecedornCdTerceiro.AsString) ;
  PosicionaQuery(qryCondPagto, qryProdutoFornecedornCdCondPagto.AsString) ;
  PosicionaQuery(qryMoeda, qryProdutoFornecedornCdMoeda.AsString) ;
  Posicionaquery(qryTipoPedido, qryProdutoFornecedornCdTipoPedidoCompra.AsString) ;
  PosicionaQuery(qryTipoRessuprimento, qryProdutoFornecedornCdTipoRessuprimentoFornec.AsString) ;

  if qryTerceiro.Active then
      qryProdutoFornecedorcNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

  if qryCondPagto.Active then
      qryProdutoFornecedorcNmCondPagto.Value := qryCondPagtocNmCondPagto.Value ;

  if qryMoeda.Active then
      qryProdutoFornecedorcNmMoeda.Value := qryMoedacNmMoeda.Value ;

  if qryTipoPedido.Active then
      qryProdutoFornecedorcNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value ;

  if qryTipoRessuprimento.Active then
      qryProdutoFornecedorcNmTipoRessuprimento.Value := qryTipoRessuprimentocNmTipoRessuprimento.Value ;

end;

procedure TfrmProdutoFornecedor.FormShow(Sender: TObject);
begin
  inherited;

  CurrencyDecimals := 6 ;
  DBGridEh2.Columns[10].DisplayFormat := '#,######0.000000' ;
  DBGridEh2.Columns[11].DisplayFormat := '#,######0.000000' ;

end;

procedure TfrmProdutoFornecedor.DBGridEh2ColEnter(Sender: TObject);
begin
  inherited;

  if (DBGridEh2.Col = 15) and (qryProdutoFornecedor.State = dsInsert) then
      qryProdutoFornecedornPercBCICMSSub.Value := 100 ;
      
end;

procedure TfrmProdutoFornecedor.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmProdutoFornecedor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

end.
