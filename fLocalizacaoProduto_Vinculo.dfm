inherited frmLocalizacaoProduto_Vinculo: TfrmLocalizacaoProduto_Vinculo
  Left = 193
  Top = 156
  Width = 873
  Height = 516
  BorderIcons = [biSystemMenu]
  Caption = 'V'#237'nculo'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 857
    Height = 451
  end
  inherited ToolBar1: TToolBar
    Width = 857
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 857
    Height = 451
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopAppendEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsPosicaoEstoque
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPosicaoEstoque'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdLocalEstoque'
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iQuadra'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iRua'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iPrateleira'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iBox'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cLocalizacao'
        Footers = <>
        Width = 127
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 416
    Top = 136
  end
  object qryPosicaoEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPosicaoEstoqueBeforePost
    OnCalcFields = qryPosicaoEstoqueCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM PosicaoEstoque'
      
        'INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque = Posica' +
        'oEstoque.nCdLocalEstoque     '
      ' WHERE nCdProduto = :nPK'
      '     AND LocalEstoque.nCdEmpresa = :nCdEmpresa')
    Left = 328
    Top = 128
    object qryPosicaoEstoquenCdPosicaoEstoque: TAutoIncField
      FieldName = 'nCdPosicaoEstoque'
    end
    object qryPosicaoEstoquenCdLocalEstoque: TIntegerField
      DisplayLabel = 'Local de Estoque|C'#243'd'
      FieldName = 'nCdLocalEstoque'
    end
    object qryPosicaoEstoquecNmLocalEstoque: TStringField
      DisplayLabel = 'Local de Estoque|Descri'#231#227'o Local'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
    object qryPosicaoEstoquenCdProduto: TIntegerField
      DisplayLabel = 'Locais de Estoque|'
      FieldName = 'nCdProduto'
    end
    object qryPosicaoEstoquecLocalizacao: TStringField
      DisplayLabel = 'Locais de Estoque|Localiza'#231#227'o'
      FieldName = 'cLocalizacao'
    end
    object qryPosicaoEstoqueiQuadra: TIntegerField
      DisplayLabel = 'Locais de Estoque|Quadra'
      FieldName = 'iQuadra'
    end
    object qryPosicaoEstoqueiRua: TIntegerField
      DisplayLabel = 'Locais de Estoque|Rua'
      FieldName = 'iRua'
    end
    object qryPosicaoEstoqueiPrateleira: TIntegerField
      DisplayLabel = 'Locais de Estoque|Prateleira'
      FieldName = 'iPrateleira'
    end
    object qryPosicaoEstoqueiBox: TIntegerField
      DisplayLabel = 'Locais de Estoque|Box'
      FieldName = 'iBox'
    end
  end
  object dsPosicaoEstoque: TDataSource
    DataSet = qryPosicaoEstoque
    Left = 376
    Top = 136
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque '
      'FROM LocalEstoque'
      'WHERE nCdLocalEstoque = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 392
    Top = 224
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
end
