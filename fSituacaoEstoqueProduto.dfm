inherited frmSituacaoEstoqueProduto: TfrmSituacaoEstoqueProduto
  Left = 166
  Top = 194
  Width = 879
  Height = 555
  Caption = 'Situa'#231#227'o Estoque Produto'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 106
    Width = 871
    Height = 422
  end
  inherited ToolBar1: TToolBar
    Width = 871
    Height = 26
    ButtonWidth = 109
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 109
    end
    inherited ToolButton2: TToolButton
      Left = 117
    end
    object ToolButton4: TToolButton
      Left = 226
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 234
      Top = 0
      Caption = 'Detalhar Estoque'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object RadioGroup1: TRadioGroup [2]
    Left = 0
    Top = 65
    Width = 871
    Height = 41
    Align = alTop
    Caption = 'Op'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Somente Pedido Sa'#237'da'
      'Somente Pedido Entrada')
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 106
    Width = 871
    Height = 422
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 422
    ClientRectRight = 871
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Situa'#231#227'o Detalhada'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 871
        Height = 399
        Align = alClient
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsSituacaoEstoqueProduto
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1iID: TcxGridDBColumn
            Caption = 'Item'
            DataBinding.FieldName = 'iID'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
            Caption = 'Pedido'
            DataBinding.FieldName = 'nCdPedido'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 145
          end
          object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
            Caption = 'Dt. Pedido'
            DataBinding.FieldName = 'dDtPedido'
          end
          object cxGrid1DBTableView1dDtEntrega: TcxGridDBColumn
            Caption = 'Prev. Entrega'
            DataBinding.FieldName = 'dDtEntrega'
          end
          object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
            Caption = 'Tipo Pedido'
            DataBinding.FieldName = 'cNmTipoPedido'
            Width = 126
          end
          object cxGrid1DBTableView1nQtde: TcxGridDBColumn
            Caption = 'Qtde.'
            DataBinding.FieldName = 'nQtde'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGrid1DBTableView1nSaldo: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldo'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGrid1DBTableView1nCdTabTipoPedido: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTabTipoPedido'
            Visible = False
          end
          object cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn
            Caption = 'UM'
            DataBinding.FieldName = 'cUnidadeMedida'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object GroupBox1: TGroupBox [4]
    Left = 0
    Top = 26
    Width = 871
    Height = 39
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Tag = 1
      Left = 10
      Top = 16
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 56
      Top = 10
      Width = 134
      Height = 21
      DataField = 'nCdProduto'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 192
      Top = 10
      Width = 657
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource1
      TabOrder = 1
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      9900999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFF30000
      FE00C003FFE9000000008001F051000000008001E023000000008001C0270000
      00008001860F0000000080019987000000008001008700000000800100470000
      0000800100070000000080010007000000018001800F000000038001801F0000
      0077C003C03F0000007FFFFFF0FF000000000000000000000000000000000000
      000000000000}
  end
  object qrySituacaoEstoqueProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cFlgTipoPedido  int'
      '       ,@nCdEmpresa      int'
      '       ,@nCdProduto      int'
      '       ,@nEstoqueAtual   decimal(12,2)'
      '       ,@iID             int'
      '       ,@iItem           int'
      '       ,@nQtde           decimal(12,2)'
      '       ,@cUnidadeMedida  varchar(10)'
      ''
      'Set @nCdEmpresa     = :nCdEmpresa'
      'Set @cFlgTipoPedido = :cFlgTipoPedido'
      'Set @nCdProduto     = :nCdProduto'
      ''
      '-- @cFlgTipoPedido'
      '-- 0 - Todos'
      '-- 1 - Somente Venda'
      '-- 2 - Somente Compra'
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempSituacaoEstoqueProduto'#39') IS NOT NULL' +
        ')'
      '    DROP TABLE #TempSituacaoEstoqueProduto'
      ''
      
        'CREATE TABLE #TempSituacaoEstoqueProduto (iID              int i' +
        'dentity(1,1)'
      '                                         ,nCdPedido        int'
      
        '                                         ,cNmTerceiro      varch' +
        'ar(50)'
      
        '                                         ,dDtPedido        datet' +
        'ime'
      
        '                                         ,dDtEntrega       datet' +
        'ime'
      
        '                                         ,cNmTipoPedido    varch' +
        'ar(50)'
      
        '                                         ,nQtde            decim' +
        'al(12,2) '
      
        '                                         ,nSaldo           decim' +
        'al(12,2) '
      '                                         ,nCdTabTipoPedido int'
      
        '                                         ,cUnidadeMedida   varch' +
        'ar(10))'
      ''
      'SELECT @cUnidadeMedida = cUnidadeMedida'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'INSERT INTO #TempSituacaoEstoqueProduto (nCdPedido'
      '                                        ,cNmTerceiro'
      '                                        ,dDtPedido'
      '                                        ,dDtEntrega'
      '                                        ,cNmTipoPedido'
      '                                        ,nQtde'
      '                                        ,nSaldo)'
      #9#9#9#9#9#9#9#9'                  SELECT NULL'
      '                                        ,'#39'Em Estoque'#39
      '                                        ,NULL'
      '                                        ,NULL'
      '                                        ,NULL'
      '                                        ,NULL'
      
        '                                        ,Sum(PosicaoEstoque.nQtd' +
        'eDisponivel)'
      '                                    FROM PosicaoEstoque'
      
        '                                         INNER JOIN LocalEstoque' +
        ' ON LocalEstoque.nCdLocalEstoque = PosicaoEstoque.nCdLocalEstoqu' +
        'e'
      
        '                                   WHERE PosicaoEstoque.nCdProdu' +
        'to    = @nCdProduto'
      
        '                                     AND LocalEstoque.cFlgEstoqu' +
        'eDisp = 1'
      
        '                                     AND LocalEstoque.nCdEmpresa' +
        '      = @nCdEmpresa'
      ''
      'INSERT INTO #TempSituacaoEstoqueProduto (nCdPedido'
      '                                        ,cNmTerceiro'
      '                                        ,dDtPedido'
      '                                        ,dDtEntrega'
      '                                        ,cNmTipoPedido'
      '                                        ,nQtde'
      '                                        ,nSaldo'
      '                                        ,nCdTabTipoPedido'
      '                                        ,cUnidadeMedida)'
      #9#9#9#9#9#9#9#9'SELECT ItemPedido.nCdPedido'
      #9#9#9#9#9#9#9#9#9'  ,Terceiro.cNmTerceiro'
      #9#9#9#9#9#9#9#9#9'  ,Pedido.dDtPedido'
      
        #9#9#9#9#9#9#9#9#9'  ,CASE WHEN ItemPedido.dDtEntregaFim IS NULL THEN Pedi' +
        'do.dDtPrevEntFim'
      #9#9#9#9#9#9#9#9#9#9#9'ELSE ItemPedido.dDtEntregaFim'
      #9#9#9#9#9#9#9#9#9'   END as dDtEntregaFinal'
      #9#9#9#9#9#9#9#9#9'  ,TipoPedido.cNmTipoPedido'
      
        #9#9#9#9#9#9#9#9#9'  ,CASE WHEN TipoPedido.cFlgCompra = 1 AND Produto.nFat' +
        'orCompra = 1 THEN (ItemPedido.nQtdePed - ItemPedido.nQtdeExpRec ' +
        '- ItemPedido.nQtdeCanc)'
      
        '                                            WHEN TipoPedido.cFlg' +
        'Compra = 1 AND Produto.nFatorCompra > 1 THEN (ItemPedido.nQtdePe' +
        'd - ItemPedido.nQtdeExpRec - ItemPedido.nQtdeCanc) * Produto.nFa' +
        'torCompra'
      
        '                                            ELSE (ItemPedido.nQt' +
        'dePed - ItemPedido.nQtdeExpRec - ItemPedido.nQtdeCanc)'
      '                                       END'
      #9#9#9#9#9#9#9#9#9'  ,0'
      
        '                                      ,TipoPedido.nCdTabTipoPedi' +
        'do'
      
        '                                      ,CASE WHEN TipoPedido.cFlg' +
        'Compra = 1 AND Produto.nFatorCompra = 1 THEN cUnidadeMedidaCompr' +
        'a'
      
        '                                            WHEN TipoPedido.cFlg' +
        'Compra = 1 AND Produto.nFatorCompra > 1 THEN cUnidadeMedidaCompr' +
        'a + '#39' c/ '#39'+ Convert(varchar,Convert(int,nFatorCompra))'
      '                                            ELSE cUnidadeMedida'
      '                                       END '
      #9#9#9#9#9#9#9#9'  FROM ItemPedido'
      
        #9#9#9#9#9#9#9#9#9'   INNER JOIN Pedido     ON ItemPedido.nCdPedido     = ' +
        'Pedido.nCdPedido'
      
        #9#9#9#9#9#9#9#9#9'   INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = ' +
        'Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9#9#9'   INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = ' +
        'Pedido.nCdTerceiro'
      
        '                                       INNER JOIN Produto    ON ' +
        'Produto.nCdProduto       = ItemPedido.nCdProduto'
      #9#9#9#9#9#9#9#9' WHERE Pedido.nCdTabStatusPed IN (3,4)'
      
        #9#9#9#9#9#9#9#9'   AND (ItemPedido.nQtdePed - ItemPedido.nQtdeExpRec - I' +
        'temPedido.nQtdeCanc) > 0'
      #9#9#9#9#9#9#9#9'   AND Pedido.nCdEmpresa      = @nCdEmpresa'
      #9#9#9#9#9#9#9#9'   AND ItemPedido.nCdProduto  = @nCdProduto'
      
        #9#9#9#9#9#9#9#9'   AND ((@cFlgTipoPedido = TipoPedido.nCdTabTipoPedido) ' +
        'OR (@cFlgTipoPedido = 0))'
      '                   AND TipoPedido.nCdOperacaoEstoque IS NOT NULL'
      ''
      ''
      '--'
      '-- Descobre o saldo inicial do produto'
      '--'
      'SELECT @nEstoqueAtual = nSaldo'
      '  FROM #TempSituacaoEstoqueProduto'
      ''
      '--'
      '-- Converte para negativo as quantidades de venda'
      '--'
      'UPDATE #TempSituacaoEstoqueProduto'
      '   Set nQtde            = nQtde * -1'
      ' WHERE nCdTabTipoPedido = 1'
      ''
      'DECLARE curItens CURSOR'
      '    FOR SELECT iID'
      '              ,nQtde'
      '          FROM #TempSituacaoEstoqueProduto'
      '         WHERE iID > 1'
      '         ORDER BY iID'
      ''
      'OPEN curItens'
      ''
      'FETCH NEXT'
      ' FROM curItens'
      ' INTO @iID'
      '     ,@nQtde'
      ''
      'Set @iItem = 1'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      '    Set @nEstoqueAtual = @nEstoqueAtual + @nQtde'
      ''
      '    UPDATE #TempSituacaoEstoqueProduto'
      '       Set nSaldo = @nEstoqueAtual'
      '     WHERE iID    = @iID'
      ''
      #9'FETCH NEXT'
      #9' FROM curItens'
      #9' INTO @iID'
      #9#9' ,@nQtde'
      'END'
      ''
      'CLOSE curItens'
      'DEALLOCATE curItens'
      ''
      'UPDATE #TempSituacaoEstoqueProduto'
      '   Set cUnidadeMedida = @cUnidadeMedida'
      ' WHERE iID = 1'
      ''
      'INSERT INTO #TempSituacaoEstoqueProduto (nCdPedido'
      '                                        ,cNmTerceiro'
      '                                        ,dDtPedido'
      '                                        ,dDtEntrega'
      '                                        ,cNmTipoPedido'
      '                                        ,nQtde'
      '                                        ,nSaldo'
      '                                        ,cUnidadeMedida)'
      #9#9#9#9#9#9#9#9'  SELECT NULL'
      '                                        ,'#39'Estoque Projetado'#39
      '                                        ,NULL'
      '                                        ,NULL'
      '                                        ,NULL'
      '                                        ,NULL'
      '                                        ,@nEstoqueAtual'
      '                                        ,@cUnidadeMedida'
      ''
      'SELECT * '
      '  FROM #TempSituacaoEstoqueProduto'
      ' ORDER BY iID'
      ''
      'DROP TABLE #TempSituacaoEstoqueProduto')
    Left = 172
    Top = 163
    object qrySituacaoEstoqueProdutoiID: TAutoIncField
      FieldName = 'iID'
      ReadOnly = True
    end
    object qrySituacaoEstoqueProdutonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
      DisplayFormat = ',0'
    end
    object qrySituacaoEstoqueProdutocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qrySituacaoEstoqueProdutodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qrySituacaoEstoqueProdutodDtEntrega: TDateTimeField
      FieldName = 'dDtEntrega'
    end
    object qrySituacaoEstoqueProdutocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qrySituacaoEstoqueProdutonQtde: TBCDField
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qrySituacaoEstoqueProdutonSaldo: TBCDField
      FieldName = 'nSaldo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qrySituacaoEstoqueProdutonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qrySituacaoEstoqueProdutocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      Size = 10
    end
  end
  object dsSituacaoEstoqueProduto: TDataSource
    DataSet = qrySituacaoEstoqueProduto
    Left = 212
    Top = 163
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 340
    Top = 242
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 424
    Top = 264
  end
end
