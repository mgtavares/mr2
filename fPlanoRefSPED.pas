unit fPlanoRefSPED;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxLookAndFeelPainters, cxButtons, cxPC, cxControls;

type
  TfrmPlanoRefSPED = class(TfrmCadastro_Padrao)
    qryMasternCdPlanoRefSPED: TIntegerField;
    qryMastercNmPlanoRefSPED: TStringField;
    qryMasteriAno: TIntegerField;
    qryMastercMascaraSPED: TStringField;
    qryMasternCodigoAuxiliar: TIntegerField;
    qryMastercFlgAnalitico: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    cmbFlagAnalitica: TDBRadioGroup;
    qryPlanoContaRefSPEDPlano: TADOQuery;
    dsPlanoContaRefSPED: TDataSource;
    qryPlanoContaRefSPEDPlanonCdPlanoRefSPEDPlanoConta: TIntegerField;
    qryPlanoContaRefSPEDPlanonCdPlanoRefSPED: TIntegerField;
    qryPlanoContaRefSPEDPlanonCdPlanoConta: TIntegerField;
    qryPlanoContaRefSPEDPlanocNmPlanoConta: TStringField;
    qryPlanoContaRefSPEDPlanocMascara: TStringField;
    qryPlanoConta: TADOQuery;
    qryPlanoContacMascara: TStringField;
    qryPlanoContacNmPlanoConta: TStringField;
    btDuplicaPlano: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryPlanoContanCdPlanoConta: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryPlanoContaRefSPEDPlanoBeforePost(DataSet: TDataSet);
    procedure qryPlanoContaRefSPEDPlanoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure btDuplicaPlanoClick(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure cmbFlagAnaliticaExit(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanoRefSPED: TfrmPlanoRefSPED;

implementation

uses
  fMenu, fPlanoRefSPED_DuplicaPlano, fLookup_Padrao;

{$R *.dfm}

procedure TfrmPlanoRefSPED.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PLANOREFSPED';
  nCdTabelaSistema  := 511;
  nCdConsultaPadrao := 761;
end;

procedure TfrmPlanoRefSPED.btIncluirClick(Sender: TObject);
begin
  inherited;
  
  cmbFlagAnalitica.ItemIndex := 0;
  DBEdit2.SetFocus;
end;

procedure TfrmPlanoRefSPED.qryPlanoContaRefSPEDPlanoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if(qryMaster.State = dsInsert) then
      qryMaster.Post;

  if (qryPlanoContaRefSPEDPlano.State = dsInsert) then
  begin
      qryPlanoContaRefSPEDPlanonCdPlanoRefSPEDPlanoConta.Value := frmMenu.fnProximoCodigo('PLANOREFSPEDPlANO');
      qryPlanoContaRefSPEDPlanonCdPlanoRefSPED.Value           := qryMasternCdPlanoRefSPED.Value;
  end;
end;

procedure TfrmPlanoRefSPED.qryPlanoContaRefSPEDPlanoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryPlanoConta.Close;
  qryPlanoConta.Parameters.ParamByName('nPK').Value := qryPlanoContaRefSPEDPlanonCdPlanoConta.Value;
  qryPlanoConta.Open;

  qryPlanoContaRefSPEDPlanocMascara.Value      := qryPlanoContacMascara.Value;
  qryPlanoContaRefSPEDPlanocNmPlanoConta.Value := qryPlanoContacNmPlanoConta.Value;
end;

procedure TfrmPlanoRefSPED.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  
  qryPlanoContaRefSPEDPlano.Close;
  qryPlanoContaRefSPEDPlano.Parameters.ParamByName('nPK').Value := qryMasternCdPlanoRefSPED.Value;
  qryPlanoContaRefSPEDPlano.Open;
end;

procedure TfrmPlanoRefSPED.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (cmbFlagAnalitica.ItemIndex = 1) then
      DBGridEh1.Columns[0].ReadOnly := False

  else
      DBGridEh1.Columns[0].ReadOnly := True;
end;

procedure TfrmPlanoRefSPED.btDuplicaPlanoClick(Sender: TObject);
var
  objCopiaPlano : TfrmPlanoRefSPED_DuplicaPlano;
begin
  inherited;

  objCopiaPlano := TfrmPlanoRefSPED_DuplicaPlano.Create(nil);

  showForm(objCopiaPlano,True);
end;

procedure TfrmPlanoRefSPED.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;

begin
  inherited;

  if ((Key = VK_F4) and (qryMaster.State <> dsInactive)) then
  begin
      if (qryPlanoContaRefSPEDPlano.State = dsBrowse) then
          qryPlanoContaRefSPEDPlano.Edit;

      nPK := frmLookup_Padrao.ExecutaConsulta(218);

      if (nPk > 0) then
          qryPlanoContaRefSPEDPlanonCdPlanoConta.Value := nPK;
  end;
end;

procedure TfrmPlanoRefSPED.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (not qryMaster.IsEmpty) then
  begin
      qryPlanoContaRefSPEDPlano.Close;
      qryPlanoContaRefSPEDPlano.Parameters.ParamByName('nPK').Value := qryMasternCdPlanoRefSPED.Value;
      qryPlanoContaRefSPEDPlano.Open;
  end;
end;

procedure TfrmPlanoRefSPED.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryPlanoContaRefSPEDPlano.Close;
end;

procedure TfrmPlanoRefSPED.btConsultarClick(Sender: TObject);
begin
  inherited;

  if (not qryMaster.IsEmpty) then
  begin
      qryPlanoContaRefSPEDPlano.Close;
      qryPlanoContaRefSPEDPlano.Parameters.ParamByName('nPK').Value := qryMasternCdPlanoRefSPED.Value;
      qryPlanoContaRefSPEDPlano.Open;
  end;
end;

procedure TfrmPlanoRefSPED.cmbFlagAnaliticaExit(Sender: TObject);
begin
  if (cmbFlagAnalitica.ItemIndex = 0) and (qryPlanoContaRefSPEDPlano.RecordCount > 0) then
  begin
      cmbFlagAnalitica.ItemIndex := 1;
      MessageDLG('Plano Referencial possui depend�ncias e n�o pode ser alterado.', mtWarning, [mbYes], 0);
      Abort;
  end;
end;

procedure TfrmPlanoRefSPED.btSalvarClick(Sender: TObject);
begin
  if (cmbFlagAnalitica.ItemIndex = 0) and (qryPlanoContaRefSPEDPlano.RecordCount > 0) then
  begin
      MessageDLG('Plano Referencial possui depend�ncias e n�o pode ser alterado.', mtWarning, [mbYes], 0);
      cmbFlagAnalitica.SetFocus;
      Abort;
  end;

  inherited;
end;

procedure TfrmPlanoRefSPED.FormActivate(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[0].Color := clWindow;
end;

initialization
  RegisterClass(TfrmPlanoRefSPED);

end.
