unit fBaixaTituloDeposito_Movtos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmBaixaTituloDeposito_Movtos = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsMovtos: TDataSource;
    qryMovtos: TADOQuery;
    qryMovtosnCdTitulo: TIntegerField;
    qryMovtoscNrTit: TStringField;
    qryMovtoscNrNF: TStringField;
    qryMovtosdDtVenc: TDateTimeField;
    qryMovtoscNmEspTit: TStringField;
    qryMovtosnValTit: TBCDField;
    qryMovtosdDtCad: TDateTimeField;
    qryMovtosnValMov: TBCDField;
    qryMovtoscNmUsuario: TStringField;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1dDtCad: TcxGridDBColumn;
    cxGrid1DBTableView1nValMov: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nSaldo       : double ;
    nCdTerceiro  : integer ;
    dDtDeposito  : Tdatetime ;
    nCdLanctoFin : integer ;
    bMovimento   : boolean ;

  end;

var
  frmBaixaTituloDeposito_Movtos: TfrmBaixaTituloDeposito_Movtos;

implementation

uses fBaixaTituloDeposito_Titulos;

{$R *.dfm}

procedure TfrmBaixaTituloDeposito_Movtos.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmBaixaTituloDeposito_Titulos ;
begin
  {inherited;}

  objForm := TfrmBaixaTituloDeposito_Titulos.Create(Self) ;

  objForm.nCdTerceiro  := nCdTerceiro ;
  objForm.nSaldo       := nSaldo ;
  objForm.dDtDeposito  := dDtDeposito ;
  objForm.nCdLanctoFin := nCdLanctoFin ;

  showForm( objForm , TRUE ) ;

  bMovimento := True ;
  
  qryMovtos.Close ;
  qryMovtos.Open ;
  
end;

end.
