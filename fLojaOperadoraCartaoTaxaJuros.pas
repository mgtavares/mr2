unit fLojaOperadoraCartaoTaxaJuros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmLojaOperadoraCartaoTaxaJuros = class(TfrmProcesso_Padrao)
    qryLojaOperadoraCartaoTaxaJuros: TADOQuery;
    dsLojaOperadoraCartaoTaxaJuros: TDataSource;
    DBGridEh1: TDBGridEh;
    qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartao: TIntegerField;
    qryLojaOperadoraCartaoTaxaJurosiParcelas: TIntegerField;
    qryLojaOperadoraCartaoTaxaJurosnTaxaJuroOperadora: TBCDField;
    qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartaoTaxaJuros: TIntegerField;
    procedure qryLojaOperadoraCartaoTaxaJurosBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLojaOperadoraCartaoTaxaJuros: TfrmLojaOperadoraCartaoTaxaJuros;
  nCdLojaOperadoraCartao : integer;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmLojaOperadoraCartaoTaxaJuros.qryLojaOperadoraCartaoTaxaJurosBeforePost(
  DataSet: TDataSet);
begin

  {-- verifica se o n�mero de parcelas � maior do que 1 --}
  if not (qryLojaOperadoraCartaoTaxaJurosiParcelas.Value > 0) then
  begin
      MensagemAlerta('O n�mero de parcelas n�o pode ser inferior a 1');
      Abort;
  end;

  {-- verifica se o valor da taxa de juros � maior ou igual a 0 --}
  if not (qryLojaOperadoraCartaoTaxaJurosnTaxaJuroOperadora.Value >= 0) then
  begin
      qryLojaOperadoraCartaoTaxaJurosnTaxaJuroOperadora.Value := 0;
  end;

  inherited;

  qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartao.Value := nCdLojaOperadoraCartao;

  if (qryLojaOperadoraCartaoTaxaJuros.State = dsInsert) then
      qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartaoTaxaJuros.Value := frmMenu.fnProximoCodigo('LOJAOPERADORACARTAOTAXAJUROS') ;

end;

procedure TfrmLojaOperadoraCartaoTaxaJuros.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

end.
