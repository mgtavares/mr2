unit fBaixaProvisao_BaixaTitulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, GridsEh, DBGridEh, DB, ADODB, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmBaixaProvisao_BaixaTitulos = class(TfrmProcesso_Padrao)
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosnCdSP: TIntegerField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosnSaldoTit: TBCDField;
    qryTituloscNrNF: TStringField;
    qryTituloscNmEspTit: TStringField;
    dsTitulos: TDataSource;
    DBGridEh1: TDBGridEh;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdTabTipoFormaPagto: TIntegerField;
    cmdPreparaTemp: TADOCommand;
    qryTituloscCdBarraTP: TStringField;
    SP_BAIXA_PROVISAO: TADOStoredProc;
    qryTitulosnCdBanco: TIntegerField;
    qryTituloscAgencia: TStringField;
    qryTituloscNumConta: TStringField;
    qryTituloscDigitoConta: TStringField;
    qryTituloscNmTitularConta1: TStringField;
    SP_CRIA_LOTELIQ_PROVISAO: TADOStoredProc;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtNrCheque: TMaskEdit;
    Label2: TLabel;
    edtNmFavorecido: TEdit;
    Label3: TLabel;
    edtDocto: TEdit;
    Label4: TLabel;
    edtHistorico: TEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtNrChequeExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cTipo : string ;
  end;

var
  frmBaixaProvisao_BaixaTitulos: TfrmBaixaProvisao_BaixaTitulos;

implementation

uses fMenu, fBaixaColetiva;

{$R *.dfm}

procedure TfrmBaixaProvisao_BaixaTitulos.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[8].Visible   := False ;
  DBGridEh1.Columns[9].Visible   := False ;
  DBGridEh1.Columns[10].Visible  := False ;
  DBGridEh1.Columns[11].Visible  := False ;
  DBGridEh1.Columns[12].Visible  := False ;
  DBGridEh1.Columns[13].Visible  := False ;
  DBGridEh1.ReadOnly             := True ;

  edtNrCheque.Text     := '' ;
  edtDocto.Text        := '' ;
  edtNmFavorecido.Text := '' ;
  edtHistorico.Text    := '' ;

  // dinheiro
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 1) then
  begin
      cTipo             := 'D' ;
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - PAGAMENTO EM DINHEIRO' ;
  end ;

  // cheque
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 2) then
  begin
      cTipo             := 'C' ;
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - PAGAMENTO EM CHEQUE' ;
  end ;

  // boleto
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 10) then
  begin
      cTipo             := 'B' ;
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - TELE PROCESSAMENTO' ;
  end ;

  // cheque terceiro
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 11) then
  begin
      cTipo             := 'T' ;
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - PAGAMENTO COM CHEQUES DIVERSOS' ;
  end ;

  // carta debito
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 12) then
  begin                                                                                          
      cTipo             := 'CD' ;
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - PAGAMENTO COM CARTA DEBITO' ;
  end ;

  // outros
  if (qryFormaPagtonCdTabTipoFormaPagto.Value = 8) then
  begin
      cTipo             := 'O';
      edtHistorico.Text := 'PROVISAO: ' + String(qryTitulos.Parameters.ParamByName('nPK').Value) + ' - PAGAMENTO COM OUTROS';
  end;

  // se n�o for pagamento em cheque, desativa os campos dos dados do cheque.
  if (cTipo = 'D') or (cTipo = 'B') or (cTipo = 'CD') or (cTipo = 'T') or (cTipo = 'O') then
  begin
      edtNrCheque.ReadOnly     := True ;
      edtNmFavorecido.ReadOnly := True ;

      edtNrCheque.Color        := $00E9E4E4 ;
      edtNmFavorecido.Color    := $00E9E4E4 ;
  end ;

  if (cTipo = 'C') then
  begin
      edtNrCheque.ReadOnly := false ;
      edtNrCheque.Color    := clWhite ;
  end ;

  DBGridEh1.Options := [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgConfirmDelete,dgCancelOnExit] ;

  if (cTipo = 'B') then
  begin
      DBGridEh1.Columns[13].Visible  := True ;
      DBGridEh1.ReadOnly            := False ;
      DBGridEh1.Columns[13].ReadOnly := False ;
      DBGridEh1.Options := [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit] ;
  end ;

  if (cTipo = 'CD') then
  begin
      DBGridEh1.ReadOnly             := True ;
      DBGridEh1.Columns[8].Visible   := True ;
      DBGridEh1.Columns[9].Visible   := True ;
      DBGridEh1.Columns[10].Visible  := True ;
      DBGridEh1.Columns[11].Visible  := True ;
      DBGridEh1.Columns[12].Visible  := True ;

      {DBGridEh1.Columns[9].ReadOnly  := False ;
      DBGridEh1.Columns[10].ReadOnly := False ;
      DBGridEh1.Columns[11].ReadOnly := False ;
      DBGridEh1.Columns[12].ReadOnly := False ;
      DBGridEh1.Columns[13].ReadOnly := False ;}

      //DBGridEh1.Options := [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgConfirmDelete,dgCancelOnExit] ;
  end ;

  DBGridEh1.Align := alClient ;

  if (cTipo = 'C') then
      edtNrCheque.SetFocus
  else edtDocto.SetFocus;

end;

procedure TfrmBaixaProvisao_BaixaTitulos.ToolButton1Click(Sender: TObject);
var
  objBaixaColetiva : TfrmBaixaColetiva ;
begin
  inherited;

  if (not edtNrCheque.ReadOnly) and (frmMenu.ConvInteiro(edtNrCheque.Text) <= 0) then
  begin
      MensagemAlerta('Informe o n�mero do cheque.') ;
      edtNrCheque.SetFocus;
      abort ;
  end ;

  // se for tipo boleto, confere o c�digo de barra dos boletos.
  // se for tipo carta debito, confere os dados das contas de dep�sito.
  if (cTipo = 'B') or (cTipo = 'CD') then
  begin

      qryTitulos.First ;

      while not qryTitulos.eof do
      begin

          if (Trim(qryTituloscCdBarraTP.Value) = '') and (cTipo = 'B') then
          begin
              MensagemAlerta('Informe o c�digo de barra de todos os boletos.') ;
              exit ;
          end ;

          if (cTipo = 'CD') then
          begin

              if (qryTitulosnCdBanco.Value = 0) then
              begin
                  MensagemAlerta('Informe o banco de dep�sito.') ;
                  exit ;
              end ;

              if (qryTituloscAgencia.Value = '') then
              begin
                  MensagemAlerta('Informe a ag�ncia de dep�sito.') ;
                  exit ;
              end ;

              if (qryTituloscNumConta.Value = '') then
              begin
                  MensagemAlerta('Informe o n�mero da conta de dep�sito.') ;
                  exit ;
              end ;

              if (qryTituloscDigitoConta.Value = '') then
              begin
                  MensagemAlerta('Informe o d�gito do n�mero da conta de dep�sito.') ;
                  exit ;
              end ;

              if (qryTituloscNmTitularConta1.Value = '') then
              begin
                  MensagemAlerta('Informe o titular da conta de dep�sito.') ;
                  exit ;
              end ;

          end ;

          qryTitulos.Next ;
          
      end ;

      qryTitulos.First ;

  end ;

  case MessageDlg('Confirma o pagamento desta provis�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  // se for cheque de terceiros, abre a janela de lote de liquida��o
  if (cTipo = 'T') then
  begin

      // cria um lote de liquida��o
      frmMenu.Connection.BeginTrans ;

      try
          SP_CRIA_LOTELIQ_PROVISAO.Close ;
          SP_CRIA_LOTELIQ_PROVISAO.Parameters.ParamByName('@nCdProvisaoTit').Value := qryTitulos.Parameters.ParamByName('nPK').Value ;
          SP_CRIA_LOTELIQ_PROVISAO.Open ;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans ;

      // carrega o lote de liquida��o
      objBaixaColetiva := TfrmBaixaColetiva.Create(nil) ;

      objBaixaColetiva.PosicionaQuery(objBaixaColetiva.qryMaster,SP_CRIA_LOTELIQ_PROVISAO.FieldList[0].asString) ;

      objBaixaColetiva.ToolButton1.Enabled := False ;
      objBaixaColetiva.btSalvar.Visible    := False ;
      objBaixaColetiva.btCancelar.Enabled  := False ;
      objBaixaColetiva.btExcluir.Visible   := False ;
      objBaixaColetiva.btConsultar.Enabled := False ;
      objBaixaColetiva.nCdProvisaoTit      := qryTitulos.Parameters.ParamByName('nPK').Value ;

      // exibe a tela de lote de liquida��o
      showForm( objBaixaColetiva , TRUE ) ;

      // quando o lote � processado, a provis�o tamb�m � baixada
      Close ;
      exit ;
      
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_BAIXA_PROVISAO.Close ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@nCdProvisaoTit').Value := qryTitulos.Parameters.ParamByName('nPK').Value ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@iNrCheque').Value      := frmMenu.ConvInteiro(edtNrCheque.Text) ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@cFlgTipo').Value       := cTipo ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@cNmFavorecido').Value  := edtNmFavorecido.Text ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@cNrDocto').Value       := edtDocto.Text ;
      SP_BAIXA_PROVISAO.Parameters.ParamByName('@cNmHistorico').Value   := edtHistorico.Text ;
      SP_BAIXA_PROVISAO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Titulos baixados com sucesso.') ;

  Close ;

end;

procedure TfrmBaixaProvisao_BaixaTitulos.edtNrChequeExit(Sender: TObject);
begin
  inherited;

  if (trim(edtNrCheque.Text) <> '') and (trim(edtDocto.Text) = '') then
      edtDocto.Text := edtNrCheque.Text ;
      
end;

end.
