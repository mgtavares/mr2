unit fCaixa_Cheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmCaixa_Cheques = class(TfrmProcesso_Padrao)
    qryPrepara_Temp_Cheque: TADOQuery;
    qryPrepara_Temp_ChequenCdBanco: TIntegerField;
    qryPrepara_Temp_ChequenCdAgencia: TIntegerField;
    qryPrepara_Temp_ChequenCdConta: TIntegerField;
    qryPrepara_Temp_ChequecDigito: TStringField;
    qryPrepara_Temp_ChequeiNrCheque: TIntegerField;
    qryPrepara_Temp_ChequecCNPJCPF: TStringField;
    qryPrepara_Temp_ChequenValCheque: TBCDField;
    qryPrepara_Temp_ChequeiAutorizacao: TIntegerField;
    qryPrepara_Temp_ChequedDtDeposito: TDateTimeField;
    qryPrepara_Temp_ChequedDtLimite: TDateTimeField;
    dsTemp_Cheque: TDataSource;
    DBGridEh3: TDBGridEh;
    qryPrepara_Temp_ChequenCdTemp_Pagto: TIntegerField;
    procedure qryPrepara_Temp_ChequeBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh3Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_Cheques: TfrmCaixa_Cheques;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_Cheques.qryPrepara_Temp_ChequeBeforePost(
  DataSet: TDataSet);
begin
  inherited;

    if (qryPrepara_Temp_ChequedDtLimite.AsString <> '') then
    begin

        if (qryPrepara_Temp_ChequedDtLimite.Value < qryPrepara_Temp_ChequedDtDeposito.Value) then
        begin
            MensagemAlerta('Data de dep�sito maior que a data m�xima permitida.') ;
            abort ;
        end ;

    end ;

end;

procedure TfrmCaixa_Cheques.ToolButton1Click(Sender: TObject);
var
    cAux : String ;
begin
  inherited;

  cAux := frmMenu.LeParametro('AUTORPAGCHEQ') ;

  qryPrepara_Temp_Cheque.First ;

  while not qryPrepara_Temp_Cheque.eof do
  begin

      if (qryPrepara_Temp_ChequenCdBanco.Value = 0) then
      begin
          MensagemAlerta('Informe o c�digo do banco.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequenCdAgencia.Value = 0) then
      begin
          MensagemAlerta('Informe a ag�ncia.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequenCdConta.Value = 0) then
      begin
          MensagemAlerta('Informe o n�mero da conta.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequecDigito.Value = '') then
      begin
          MensagemAlerta('Informe o d�gito da conta.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequeiNrCheque.Value = 0) then
      begin
          MensagemAlerta('Informe o n�mero do cheque.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequenValCheque.Value <= 0) then
      begin
          MensagemAlerta('Informe o valor do cheque.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequecCNPJCPF.Value = '') then
      begin
          MensagemAlerta('Informe o CNPJ/CPF do emissor.') ;
          exit ;
      end ;

      if (cAux = 'S') and (qryPrepara_Temp_ChequeiAutorizacao.Value <= 0) then
      begin
          MensagemAlerta('Informe o c�digo de autoriza��o da consulta do cheque.') ;
          exit ;
      end ;

      qryPrepara_Temp_Cheque.Next;

  end ;

  qryPrepara_Temp_Cheque.First ;

  close ;

end;

procedure TfrmCaixa_Cheques.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  case key of
      vk_F4 : ToolButton1.Click;
  end ;
  
end;

procedure TfrmCaixa_Cheques.DBGridEh3Enter(Sender: TObject);
begin
  inherited;
  DBGridEh3.Col := 6 ;
end;

end.

