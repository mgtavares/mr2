unit fImportaAliquotaIBPT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ImgList, ComCtrls, ToolWin, ExtCtrls, DB, ADODB, ACBrBase, ACBrSocket,
  ACBrIBPTax, Mask, ER2Lookup, DBCtrls;

type
  TfrmImportaAliquotaIBPT = class(TfrmProcesso_Padrao)
    Label2: TLabel;
    edtCaminho: TEdit;
    cxButton1: TcxButton;
    OpenDialog1: TOpenDialog;
    qryTabIBPT: TADOQuery;
    ACBrIBPTax1: TACBrIBPTax;
    qryTabIBPTnCdTabIBPT: TIntegerField;
    qryTabIBPTcNCM: TStringField;
    qryTabIBPTiTabela: TIntegerField;
    qryTabIBPTcDescricao: TStringField;
    qryTabIBPTnAliqNacional: TBCDField;
    qryTabIBPTnAliqImportado: TBCDField;
    qryTabIBPTcFlgManual: TIntegerField;
    qryIBPTEmUso: TADOQuery;
    qryTabIBPTnCdEstado: TIntegerField;
    qryTabIBPTnAliqEstadual: TBCDField;
    qryTabIBPTnAliqMunicipal: TBCDField;
    qryTabIBPTcVersao: TStringField;
    qryTabIBPTdDtValidInicial: TDateTimeField;
    qryTabIBPTdDtValidFinal: TDateTimeField;
    qryTabIBPTcChave: TStringField;
    qryIBPTEmUsocNCM: TStringField;
    qryIBPTEmUsonCdTabIBPT: TIntegerField;
    Label1: TLabel;
    qryEstado: TADOQuery;
    dsEstado: TDataSource;
    er2LkpEstado: TER2LookupMaskEdit;
    qryEstadocUF: TStringField;
    qryEstadocNmEstado: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    qryEstadonCdEstado: TIntegerField;
    qryAux: TADOQuery;
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaAliquotaIBPT: TfrmImportaAliquotaIBPT;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmImportaAliquotaIBPT.cxButton1Click(Sender: TObject);
begin
  if (not DirectoryExists('.\Arquivos\IBPT')) then
      CreateDir('.\Arquivos\IBPT');

  OpenDialog1.Title      := 'Selecione o Arquivo';
  OpenDialog1.DefaultExt := '*.csv';
  OpenDialog1.Filter     := 'Arquivos IBPT (*.csv)';
  OpenDialog1.InitialDir := '.\Arquivos\IBPT';

  if OpenDialog1.Execute then
      edtCaminho.Text := OpenDialog1.FileName;
end;

procedure TfrmImportaAliquotaIBPT.ToolButton1Click(Sender: TObject);
var
  i          : Integer;
  bAliqEmUso : Boolean;
begin
  inherited;

  if (DBEdit1.Text = '') and (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe o estado correspondente ao arquivo de importa��o.') ;
      er2LkpEstado.SetFocus;
      Abort;
  end;

  if (Trim(edtCaminho.Text) = '') then
  begin
      MensagemAlerta('Informe o caminho do arquivo.');
      edtCaminho.SetFocus;
      Abort;
  end;

  if not FileExists(Trim(edtCaminho.Text)) then
  begin
      MensagemErro('Arquivo inexistente.');
      edtCaminho.SetFocus;
      Abort;
  end;

  if (MessageDlg('Confirma a importa��o do arquivo ' + ExtractFileName(Trim(edtCaminho.Text)) + ' ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      Exit;

  try
      frmMenu.Connection.BeginTrans;

      if(not ACBrIBPTax1.AbrirTabela(edtCaminho.Text))then
      begin
          MensagemErro('Arquivo informado est� vazio ou n�o possui a estrutura necess�ria para importa��o.' + #13#13 + 'Estrutura: (NCM; AliqEx; Tabela; Descri��o; AliqNac; AliqImp; AliqEst; AliqMun; DataIni; DataFim; Chave; Vers�o;).');
          Abort;
      end;

      if (Now() < ACBrIBPTax1.VigenciaInicio) and (Now() > ACBrIBPTax1.VigenciaFim) then
      begin
          MensagemErro('Arquivo de importa��o fora do prazo de validade.' + #13#13 + 'Prazo de validade: ' + DateToStr(ACBrIBPTax1.VigenciaInicio) + ' at� ' + DateToStr(ACBrIBPTax1.VigenciaFim) + '.');
          Abort;
      end;

      { -- deleta registros importados anteriormente que n�o estejam em uso no Grupo de Imposto -- }
      qryTabIBPT.SQL.Clear;
      qryTabIBPT.SQL.Add('DELETE FROM TabIBPT WHERE cFlgManual = 0 AND nCdEstado = ' +  er2LkpEstado.Text + ' AND NOT EXISTS(SELECT 1 FROM GrupoImposto GI WHERE GI.nCdTabIBPT = TabIBPT.nCdTabIBPT)');
      qryTabIBPT.ExecSQL;

      { -- prepara query para importa��o dos registros -- }
      qryTabIBPT.SQL.Clear;
      qryTabIBPT.SQL.Add('SELECT * FROM TabIBPT');
      qryTabIBPT.ExecSQL;
      qryTabIBPT.Open;

      qryIBPTEmUso.Close;
      qryIBPTEmUso.Open;

      { -- importa todos os registros presentes no arquivo CSV -- }
      for i:=0 to ACBrIBPTax1.Itens.Count - 1 do
      begin
          bAliqEmUso := False;

          qryIBPTEmUso.First;

          while not qryIBPTEmUso.Eof do
          begin
              { -- verifica se regitro est� em uso -- }
              if (Trim(qryIBPTEmUsocNCM.Value) = Trim(ACBrIBPTax1.Itens[i].NCM)) then
              begin
                  bAliqEmUso := True;
                  Break;
              end;

              qryIBPTEmUso.Next;
          end;

          { -- se al�quota esta em uso, atualiza informa��es do registro -- }
          if (bAliqEmUso = True) then
          begin
              qryAux.Close;
              qryAux.Parameters.ParamByName('cNCM').Value            := ACBrIBPTax1.Itens[i].NCM;
              qryAux.Parameters.ParamByName('cDescricao').Value      := AnsiUpperCase(ACBrIBPTax1.Itens[i].Descricao);
              qryAux.Parameters.ParamByName('iTabela').Value         := Integer(ACBrIBPTax1.Itens[i].Tabela); // 0 : Mercadoria - 1 : Servi�o
              qryAux.Parameters.ParamByName('nAliqNacional').Value   := ACBrIBPTax1.Itens[i].FederalNacional;
              qryAux.Parameters.ParamByName('nAliqImportado').Value  := ACBrIBPTax1.Itens[i].FederalImportado;
              qryAux.Parameters.ParamByName('nAliqEstadual').Value   := ACBrIBPTax1.Itens[i].Estadual;
              qryAux.Parameters.ParamByName('nAliqMunicipal').Value  := ACBrIBPTax1.Itens[i].Municipal;
              qryAux.Parameters.ParamByName('dDtValidInicial').Value := DateTimeToStr(ACBrIBPTax1.VigenciaInicio);
              qryAux.Parameters.ParamByName('dDtValidFinal').Value   := DateTimeToStr(ACBrIBPTax1.VigenciaFim);
              qryAux.Parameters.ParamByName('cVersao').Value         := ACBrIBPTax1.VersaoArquivo ;
              qryAux.Parameters.ParamByName('cChave').Value          := ACBrIBPTax1.ChaveArquivo ;
              qryAux.Parameters.ParamByName('cFlgManual').Value      := 0;                                   //incluso via importa��o
              qryAux.Parameters.ParamByName('nCdEstado').Value       := qryEstadonCdEstado.Value ;
              qryAux.Parameters.ParamByName('nCdTabIBPT').Value      := qryIBPTEmUsonCdTabIBPT.Value;
              qryAux.ExecSQL;
          end
          { -- se n�o, inlui novo registro -- }
          else
          begin
              qryTabIBPT.Insert;
              qryTabIBPTnCdTabIBPT.Value      := frmMenu.fnProximoCodigo('TABIBPT');
              qryTabIBPTcNCM.Value            := ACBrIBPTax1.Itens[i].NCM;
              qryTabIBPTcDescricao.Value      := AnsiUpperCase(ACBrIBPTax1.Itens[i].Descricao);
              qryTabIBPTiTabela.Value         := Integer(ACBrIBPTax1.Itens[i].Tabela); // 0 : Mercadoria - 1 : Servi�o
              qryTabIBPTnAliqNacional.Value   := ACBrIBPTax1.Itens[i].FederalNacional;
              qryTabIBPTnAliqImportado.Value  := ACBrIBPTax1.Itens[i].FederalImportado;
              qryTabIBPTnAliqEstadual.Value   := ACBrIBPTax1.Itens[i].Estadual;
              qryTabIBPTnAliqMunicipal.Value  := ACBrIBPTax1.Itens[i].Municipal;
              qryTabIBPTdDtValidInicial.Value := ACBrIBPTax1.VigenciaInicio;
              qryTabIBPTdDtValidFinal.Value   := ACBrIBPTax1.VigenciaFim;
              qryTabIBPTcVersao.Value         := ACBrIBPTax1.VersaoArquivo;
              qryTabIBPTcChave.Value          := ACBrIBPTax1.ChaveArquivo;
              qryTabIBPTcFlgManual.Value      := 0;                                   //incluso via importa��o
              qryTabIBPTnCdEstado.Value       := qryEstadonCdEstado.Value;
              qryTabIBPT.Post;
          end;
      end;

      { -- atualiza informa��es complementares -- }
      qryTabIBPT.SQL.Clear;
      qryTabIBPT.SQL.Add('UPDATE TabIBPT                                                                                              ');
      qryTabIBPT.SQL.Add('   SET cVersao         = ' + #39 + ACBrIBPTax1.VersaoArquivo + #39                                           );
      qryTabIBPT.SQL.Add('      ,dDtValidInicial = CONVERT(DATETIME,' + #39 + DateTimeToStr(ACBrIBPTax1.VigenciaInicio) + #39 + ',103)');
      qryTabIBPT.SQL.Add('      ,dDtValidFinal   = CONVERT(DATETIME,' + #39 + DateTimeToStr(ACBrIBPTax1.VigenciaFim) + #39 + ',103)   ');
      qryTabIBPT.SQL.Add('      ,cChave = ' + #39 + ACBrIBPTax1.ChaveArquivo + #39                                                     );
      qryTabIBPT.SQL.Add('WHERE cVersao         IS NULL                                                                               ');
      qryTabIBPT.SQL.Add('   OR dDtValidInicial IS NULL                                                                               ');
      qryTabIBPT.SQL.Add('   OR dDtValidFinal   IS NULL                                                                               ');
      qryTabIBPT.SQL.Add('   OR cChave          IS NULL                                                                               ');
      qryTabIBPT.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryEstado.Close;
      edtCaminho.Clear;
      er2LkpEstado.Clear;
      er2LkpEstado.SetFocus;

      ShowMessage('Importa��o conclu�da com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro ao importar o arquivo.');
      Raise;
  end;
end;

initialization
  RegisterClass(TfrmImportaAliquotaIBPT);

end.
