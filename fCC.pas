unit fCC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCC = class(TfrmCadastro_Padrao)
    qryMasternCdCC: TIntegerField;
    qryMastercNmCC: TStringField;
    qryMasternCdCC1: TIntegerField;
    qryMasternCdCC2: TIntegerField;
    qryMastercCdCC: TStringField;
    qryMastercCdHie: TStringField;
    qryMasteriNivel: TSmallintField;
    qryMastercFlgLanc: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCC: TfrmCC;

implementation

{$R *.dfm}

procedure TfrmCC.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CENTROCUSTO' ;
  nCdTabelaSistema  := 18 ;
  nCdConsultaPadrao := 223 ;

end;

procedure TfrmCC.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdStatus.Value := 1 ;
  qryMasteriNivel.Value    := 3 ;
  qryMastercFlgLanc.Value  := 1 ;
  
  DBEdit2.SetFocus;
end;

procedure TfrmCC.qryMasterBeforePost(DataSet: TDataSet);
begin

  if ( trim( DBEdit2.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  inherited;

end;

initialization
    RegisterClass( TfrmCC ) ;

end.
