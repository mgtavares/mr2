inherited frmRequisicao: TfrmRequisicao
  Left = 133
  Top = 88
  Width = 1141
  Height = 616
  Caption = 'Requisi'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1125
    Height = 553
  end
  object Label1: TLabel [1]
    Left = 65
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 60
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 14
    Top = 86
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Requisitante'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 625
    Top = 62
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Requisi'#231#227'o'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 620
    Top = 109
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Autoriza'#231#227'o'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 76
    Top = 181
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Setor'
  end
  object Label7: TLabel [7]
    Left = 49
    Top = 206
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Solicitante'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 437
    Top = 206
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nro Formul'#225'rio'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [9]
    Left = 22
    Top = 110
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Requisi'#231#227'o'
    FocusControl = DBEdit9
  end
  object Label11: TLabel [10]
    Left = 676
    Top = 134
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit13
  end
  object Label12: TLabel [11]
    Left = 19
    Top = 158
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
    FocusControl = DBEdit17
  end
  object Label14: TLabel [12]
    Left = 624
    Top = 86
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Finaliza'#231#227'o'
    FocusControl = DBEdit19
  end
  object Label9: TLabel [13]
    Left = 853
    Top = 62
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Atend.'
    FocusControl = DBEdit4
  end
  object Label15: TLabel [14]
    Left = 1022
    Top = 62
    Width = 6
    Height = 13
    Alignment = taRightJustify
    Caption = #224
    FocusControl = DBEdit4
  end
  object Label16: TLabel [15]
    Left = 3
    Top = 134
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Distribui'#231#227'o'
    FocusControl = DBEdit22
  end
  inherited ToolBar2: TToolBar
    Width = 1125
    TabOrder = 22
    inherited ToolButton9: TToolButton
      Visible = False
    end
    object ToolButton10: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btnFinalizar: TToolButton
      Left = 864
      Top = 0
      Caption = '&Finalizar'
      ImageIndex = 9
      OnClick = cxButton1Click
    end
    object ToolButton13: TToolButton
      Left = 952
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object btOpcao: TToolButton
      Left = 960
      Top = 0
      Caption = '&Op'#231#245'es'
      DropdownMenu = menuOpcao
      ImageIndex = 11
    end
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 107
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdRequisicao'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [18]
    Tag = 1
    Left = 107
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [19]
    Left = 107
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 712
    Top = 56
    Width = 129
    Height = 19
    DataField = 'dDtRequisicao'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 712
    Top = 104
    Width = 129
    Height = 19
    DataField = 'dDtAutor'
    DataSource = dsMaster
    TabOrder = 18
  end
  object DBEdit7: TDBEdit [22]
    Left = 107
    Top = 200
    Width = 209
    Height = 19
    DataField = 'cNmSolicitante'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit8: TDBEdit [23]
    Left = 520
    Top = 200
    Width = 65
    Height = 19
    DataField = 'cNrFormulario'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit9: TDBEdit [24]
    Left = 107
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdTipoRequisicao'
    DataSource = dsMaster
    TabOrder = 6
    OnExit = DBEdit9Exit
    OnKeyDown = DBEdit9KeyDown
  end
  object DBEdit10: TDBEdit [25]
    Tag = 1
    Left = 175
    Top = 56
    Width = 410
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object DBEdit11: TDBEdit [26]
    Tag = 1
    Left = 175
    Top = 80
    Width = 410
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 3
  end
  object DBEdit12: TDBEdit [27]
    Tag = 1
    Left = 175
    Top = 104
    Width = 410
    Height = 19
    DataField = 'cNmTipoRequisicao'
    DataSource = dsTipoRequisicao
    TabOrder = 5
  end
  object cxPageControl1: TcxPageControl [28]
    Left = 17
    Top = 233
    Width = 1097
    Height = 320
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 21
    ClientRectBottom = 316
    ClientRectLeft = 4
    ClientRectRight = 1093
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Item Estoque'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 40
        Width = 1089
        Height = 252
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItens
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColEnter = DBGridEh1ColEnter
        OnColExit = DBGridEh1ColExit
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRequisicao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemRequisicao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'ID'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            Visible = False
            Width = 73
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Item|Descri'#231#227'o do Produto'
            Width = 420
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Item|U.M.'
            Width = 35
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeReq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footer.Color = clWhite
            Footer.DisplayFormat = '#,#0.00'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -13
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            ReadOnly = True
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeAtend'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nCdMapaCompra'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 50
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1089
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object btExcluirItens: TcxButton
          Left = 153
          Top = 2
          Width = 146
          Height = 33
          Caption = 'Excluir Itens'
          TabOrder = 0
          OnClick = btExcluirItensClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000B0000000D0000000100
            180000000000D401000000000000000000000000000000000000F0F0F0333333
            333333333333333333333333333333333333333333333333F0F0F0000000F0F0
            F0333333FFFFFFCCCCCCCCCCCC999999999999999999666666333333F0F0F000
            0000F0F0F0333333FFFFFF999999CCCCCC666666999999333333666666333333
            F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC6666669999993333336666
            66333333F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC66666699999933
            3333666666333333F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC666666
            999999333333666666333333F0F0F0000000F0F0F0333333FFFFFF999999CCCC
            CC666666999999333333666666333333F0F0F0000000F0F0F0333333FFFFFF99
            9999CCCCCC666666999999333333666666333333F0F0F0000000F0F0F0333333
            FFFFFFCCCCCCCCCCCC999999999999999999666666333333F0F0F00000003333
            3333333333333333333333333333333333333333333333333333333333333300
            0000333333CCCCCCCCCCCCCCCCCCCCCCCC999999999999999999999999666666
            3333330000003333333333333333333333333333333333333333333333333333
            33333333333333000000F0F0F0F0F0F0F0F0F0F0F0F0333333333333333333F0
            F0F0F0F0F0F0F0F0F0F0F0000000}
          LookAndFeel.NativeStyle = True
        end
        object btLeitura: TcxButton
          Left = 4
          Top = 2
          Width = 145
          Height = 33
          Caption = 'Digita'#231#227'o/Leitura'
          TabOrder = 1
          OnClick = btLeituraClick
          Glyph.Data = {
            46020000424D460200000000000036000000280000000F0000000B0000000100
            1800000000001002000000000000000000000000000000000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF7F7F7F
            FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
            FF7F7F7FFFFFFF0000000D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D
            06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF000000FFFFFF7F7F7F
            FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
            FF7F7F7FFFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Item AD'
      ImageIndex = 3
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 1089
        Height = 292
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemAD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColEnter = DBGridEh1ColEnter
        OnColExit = DBGridEh1ColExit
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRequisicao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemRequisicao'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 390
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footers = <>
            Title.Caption = 'Item|U.M.'
            Width = 35
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeReq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footer.Color = clWhite
            Footer.DisplayFormat = '#,##0.00'
            Footer.ValueType = fvtSum
            Footers = <>
            ReadOnly = True
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeAtend'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 63
          end
          item
            EditButtons = <>
            FieldName = 'nCdMapaCompra'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            ReadOnly = True
            Width = 65
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Detalhes'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1089
        Height = 292
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItemAD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemRequisicao'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 390
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeReq'
            Footers = <>
            ReadOnly = True
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'cNotaComprador'
            Footers = <>
            Width = 185
          end
          item
            AlwaysShowEditButton = True
            AutoDropDown = True
            EditButtons = <>
            FieldName = 'cFlgUrgente'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'N'#227'o'
              'Sim')
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cJustificativa'
            Footers = <>
            Width = 185
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Descri'#231#227'o T'#233'cnica'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 561
        Height = 292
        Align = alLeft
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItemAD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemRequisicao'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 390
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo2: TDBMemo
        Left = 561
        Top = 0
        Width = 528
        Height = 292
        Align = alClient
        DataField = 'cDescricaoTecnica'
        DataSource = dsItemAD
        TabOrder = 1
      end
    end
    object tabObservacao: TcxTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 4
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 1001
        Height = 276
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 1089
        Height = 292
        Align = alClient
        DataField = 'cOBS'
        DataSource = dsMaster
        TabOrder = 0
      end
      object cxTabsheet5: TcxTabSheet
        Caption = 'Observa'#231#245'es'
        ImageIndex = 4
        object Image3: TImage
          Left = 0
          Top = 0
          Width = 1001
          Height = 276
          Picture.Data = {
            07544269746D6170A6290000424DA62900000000000036000000280000005A00
            0000270000000100180000000000702900000000000000000000000000000000
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
            B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
            C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
            96C2B296C2B296C2B296C2B296C2B2960000}
          Stretch = True
        end
        object DBMemo3: TDBMemo
          Left = 0
          Top = 0
          Width = 1089
          Height = 292
          Align = alClient
          DataField = 'cOBS'
          DataSource = dsMaster
          TabOrder = 0
        end
      end
    end
    object cxTabSheet6: TcxTabSheet
      Caption = 'Follow Up'
      ImageIndex = 5
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 0
        Width = 609
        Height = 292
        Align = alLeft
        DataGrouping.GroupLevels = <>
        DataSource = dsFollowUp
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFollowUp'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncia|Data'
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncia|Usu'#225'rio'
            Width = 160
          end
          item
            EditButtons = <>
            FieldName = 'cOcorrenciaResum'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncia|Descri'#231#227'o Resumida'
            Width = 184
          end
          item
            EditButtons = <>
            FieldName = 'dDtProxAcao'
            Footers = <>
            Title.Caption = 'Ocorr'#234'ncia|Data Pr'#243'x. A'#231#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo4: TDBMemo
        Tag = 1
        Left = 616
        Top = 0
        Width = 473
        Height = 292
        Align = alCustom
        DataField = 'cOcorrencia'
        DataSource = dsFollowUp
        TabOrder = 1
      end
    end
  end
  object DBEdit13: TDBEdit [29]
    Tag = 1
    Left = 712
    Top = 128
    Width = 129
    Height = 19
    DataField = 'cNmTabStatusRequis'
    DataSource = dsTabStatusRequis
    TabOrder = 20
  end
  object DBEdit14: TDBEdit [30]
    Tag = 1
    Left = 844
    Top = 104
    Width = 270
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuario
    TabOrder = 19
  end
  object DBEdit6: TDBEdit [31]
    Left = 107
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdSetor'
    DataSource = dsMaster
    TabOrder = 8
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit15: TDBEdit [32]
    Tag = 1
    Left = 175
    Top = 176
    Width = 410
    Height = 19
    DataField = 'cNmSetor'
    DataSource = dsSetor
    TabOrder = 16
  end
  object DBEdit16: TDBEdit [33]
    Tag = 1
    Left = 175
    Top = 152
    Width = 410
    Height = 19
    DataField = 'cNmCC'
    DataSource = dsCC
    TabOrder = 15
  end
  object DBEdit17: TDBEdit [34]
    Left = 107
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 7
    OnExit = DBEdit17Exit
    OnKeyDown = DBEdit17KeyDown
  end
  object DBEdit19: TDBEdit [35]
    Tag = 1
    Left = 712
    Top = 80
    Width = 129
    Height = 19
    DataField = 'dDtFinalizacao'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit20: TDBEdit [36]
    Left = 936
    Top = 56
    Width = 76
    Height = 19
    DataField = 'dDtPrevAtendIni'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit21: TDBEdit [37]
    Left = 1038
    Top = 56
    Width = 76
    Height = 19
    DataField = 'dDtPrevAtendFim'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit22: TDBEdit [38]
    Tag = 1
    Left = 175
    Top = 128
    Width = 410
    Height = 19
    DataField = 'cNmCentroDistribuicao'
    DataSource = dsCentroDistribuicao
    TabOrder = 14
  end
  object DBEdit23: TDBEdit [39]
    Tag = 1
    Left = 107
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdCentroDistribuicao'
    DataSource = dsMaster
    TabOrder = 23
    OnExit = DBEdit23Exit
    OnKeyDown = DBEdit23KeyDown
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Requisicao'
      ' WHERE nCdRequisicao = :nPK'
      '   AND nCdEmpresa    = :nCdEmpresa')
    Left = 344
    Top = 392
    object qryMasternCdRequisicao: TIntegerField
      FieldName = 'nCdRequisicao'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryMasterdDtRequisicao: TDateTimeField
      FieldName = 'dDtRequisicao'
    end
    object qryMasterdDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryMasternCdUsuarioAutor: TIntegerField
      FieldName = 'nCdUsuarioAutor'
    end
    object qryMastercNmSolicitante: TStringField
      FieldName = 'cNmSolicitante'
      Size = 30
    end
    object qryMasternCdTabTipoRequis: TIntegerField
      FieldName = 'nCdTabTipoRequis'
    end
    object qryMastercNrFormulario: TStringField
      FieldName = 'cNrFormulario'
      Size = 10
    end
    object qryMasternCdTabStatusRequis: TIntegerField
      FieldName = 'nCdTabStatusRequis'
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryMasternCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercFlgAutomatica: TIntegerField
      FieldName = 'cFlgAutomatica'
    end
    object qryMasternValRequisicao: TBCDField
      FieldName = 'nValRequisicao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtFinalizacao: TDateTimeField
      FieldName = 'dDtFinalizacao'
    end
    object qryMasterdDtPrevAtendIni: TDateTimeField
      FieldName = 'dDtPrevAtendIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevAtendFim: TDateTimeField
      FieldName = 'dDtPrevAtendFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMasternCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryMasternCdEtapaOrdemProducao: TIntegerField
      FieldName = 'nCdEtapaOrdemProducao'
    end
    object qryMastercFlgCritico: TIntegerField
      FieldName = 'cFlgCritico'
    end
    object qryMasterdDtPrevAtendIniOriginal: TDateTimeField
      FieldName = 'dDtPrevAtendIniOriginal'
    end
    object qryMasterdDtPrevAtendFimOriginal: TDateTimeField
      FieldName = 'dDtPrevAtendFimOriginal'
    end
    object qryMasternCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 344
    Top = 424
  end
  inherited qryID: TADOQuery
    Left = 728
    Top = 392
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 344
    Top = 456
  end
  inherited qryStat: TADOQuery
    Left = 728
    Top = 424
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 376
    Top = 456
  end
  inherited ImageList1: TImageList
    Left = 408
    Top = 456
    Bitmap = {
      494C01010C000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000000000009797
      97006461600063605F00625F5E00615E5E00615E5D00615E5D00615E5D00615E
      5D00615E5D00BAB1A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E3E7F7009AA7E3004C62CC00364FC500344DC300465DC70095A1DE00E1E5
      F60000000000000000000000000000000000000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000000000009999
      990000000000FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE000000
      0000615E5D00BAB1A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BFC7
      EF004C63D1005264D4008490E70095A0EE00959FED00838EE5004C5DCE003D54
      C300B8C1E9000000000000000000000000000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D00000000000D5CABC00B9B0A400B9B0A4009B9B
      9B0000000000A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A5000000
      0000615E5D00B8B0A400B8B0A400D5CABB0000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000000000000C1CAF1004760
      D5007584E300A1ACF4007F8BEC005C67E4005B66E3007D87EA009FA8F1006F7C
      DD00324BC200B9C1EA0000000000000000000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D10000000000ABA59C0066666600646464009D9D
      9D0000000000FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD000000
      0000615E5D0048484800454545009D968E00F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF0000000000E7EAFA005970DE007888
      E600A3B0F5005767E7005665E6008992ED008892EC00535FE200525DE1009FA9
      F2006F7DDD004157C600E2E6F60000000000000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED20000000000696969009B9B9B00B8B8B8008F8F
      8F00E4E4E400949494009494940094949400949494009494940094949400E4E4
      E4005B585700A5A5A5008E918E0043434300EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF0000000000A8B4F0006073E000A4B3
      F7005A6EEB00596CEA005869E800FCFCFC00FCFCFC005562E5005461E300535F
      E2009FA9F2005061D10097A4E100000000000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000006A6A6A00B8B8B800B6B6B6007A7A
      7A00C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1C100C1C1
      C10053504F00A3A3A30035FE350044444400F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF00000000006A82E9008E9FF0008499
      F4005C73EE005B70EC005A6EEB00909DF100A6AFF3005767E7005665E6005562
      E5007D89EB008591E7004E64CE00000000000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D400000000006C6C6C00B8B8B800474241000101
      0100020202000202020001010100010101000101010001010100010101000101
      01000101010046424100A5A5A50046464600F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF00000000005D76EA00A0B3F7006580
      F2005F78F0005D76EF005C73EE00FCFCFC00FCFCFC00596CEA005869E8005767
      E7005D6CE70099A5F1003C55CC0000000000000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD500000000006D6D6D00B8B8B800B8B8B8005252
      52006969690065656500606060005B5B5B0056565600525252004D4D4D004848
      480033333300A5A5A500A5A5A50047474700F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF0000000000617BEE00A1B6F8006784
      F400607CF3005F7AF1005F78F000FCFCFC00FCFCFC005B70EC005A6EEB00596C
      EA005F6FE9009BA8F1004159D000000000000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D600000000006E6E6E00B8B8B800B8B8B8005454
      54006B6B6B0066666600626262005D5D5D0058585800535353004F4F4F004A4A
      4A0034343400A5A5A500A5A5A50048484800F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF0000000000768DF30091A6F30088A1
      F8006280F400617EF300607CF300FCFCFC00FCFCFC005D76EF005C73EE005B70
      EC008293F1008998EC005970D800000000000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D8000000000070707000E7E7E70000000000B7B7
      B700C5C5C500C0C0C000B8B8B800B2B2B200ABABAB00A3A3A3009B9B9B009393
      93006F6F6F0000000000CCCCCC004A4A4A00F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF0000000000B2BFFA006C81EC00A9BD
      FB006382F5006281F5006280F400FCFCFC00FCFCFC005F7AF1005F78F0005D76
      EF00A5B5F8005D70DD00A2AFEB00000000000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB00969696000000000000000000ACA69D006F6F6F006C6C6C006969
      69006E6E6E006C6C6C006A6A6A00686868006666660066666600666666006666
      660053535300505050004E4E4E009E988F00F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF0000000000EBEEFE00758CF7008397
      F000A9BDFB006382F5006382F500FCFCFC00FCFCFC00617EF300607CF300A6B9
      F9007B8DEA005C74E100E7EAFA00000000000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A40000000000000000000000000000000000000000009A9A
      9A00F3F3F300EFEFEF00EAEAEA00E6E6E600E1E1E100DFDFDF00DFDFDF00DFDF
      DF00615E5D00000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000CED7FD006D86
      F8008497F100A9BDFB008AA3F8006B89F6006B89F60089A2F800A8BCFA007F92
      EC005972E500C6CEF60000000000000000000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C30000000000000000000000000000000000000000009B9B
      9B00F7F7F700F3F3F300EEEEEE00EAEAEA00E5E5E500E1E1E100DFDFDF00DFDF
      DF00615E5D00000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF00000000000000000000000000CED7
      FD00778EFA006E83EE0092A6F400A0B4F800A0B4F80091A6F300687DE9006981
      ED00C8D1F8000000000000000000000000000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB0000000000000000000000000000000000000000009D9D
      9D00FBFBFB00F7F7F700F2F2F200EEEEEE00EAEAEA00E5E5E500E1E1E100DFDF
      DF00615E5D00000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000000000000000
      0000EBEEFF00B5C3FD007C94FA006C86F7006A84F500778EF500B1BEF800E9EC
      FD0000000000000000000000000000000000000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E9000000000000000000000000000000000000000000000000009F9F
      9F00000000000000000000000000000000000000000000000000000000000000
      0000615E5D00000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA0000000000000000000000000000000000000000000000000000000000A1A1
      A1009F9F9F009C9C9C009A9A9A009696960094949400919191008F8F8F008C8C
      8C008C8C8C00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF8001E003FFFFF00F8001E813FFFF
      E007800108108000C00380010810000080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800120040000
      8001C3C3000000008001C3C3E0070000C003C003E0078000E007C003E0070000
      F00FE007EFF70100FFFFF00FE0070000FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 376
    Top = 392
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 376
    Top = 424
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuarioAtivo'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Loja.nCdLoja, cNmLoja'
      '  FROM Loja'
      
        '       INNER JOIN Empresa     ON Empresa.nCdEmpresa     = Loja.n' +
        'CdEmpresa'
      
        '       INNER JOIN UsuarioLoja ON UsuarioLoja.nCdLoja    = Loja.n' +
        'CdLoja'
      
        '                             AND UsuarioLoja.nCdUsuario = :nCdUs' +
        'uarioAtivo'
      'WHERE Loja.nCdLoja = :nPK')
    Left = 408
    Top = 392
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 408
    Top = 424
  end
  object qryTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryTipoRequisicaoAfterOpen
    AfterCancel = qryTipoRequisicaoAfterCancel
    AfterScroll = qryTipoRequisicaoAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdTipoRequisicao, cNmTipoRequisicao, nCdTabTipoRequis, c' +
        'FlgExigeCC, cExigeAutor'
      'FROM TipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK')
    Left = 440
    Top = 392
    object qryTipoRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryTipoRequisicaocNmTipoRequisicao: TStringField
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
    object qryTipoRequisicaonCdTabTipoRequis: TIntegerField
      FieldName = 'nCdTabTipoRequis'
    end
    object qryTipoRequisicaocFlgExigeCC: TIntegerField
      FieldName = 'cFlgExigeCC'
    end
    object qryTipoRequisicaocExigeAutor: TIntegerField
      FieldName = 'cExigeAutor'
    end
  end
  object dsTipoRequisicao: TDataSource
    DataSet = qryTipoRequisicao
    Left = 440
    Top = 424
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemRequisicao'
      'WHERE nCdRequisicao = :nPK'
      'AND nCdTipoItemRequisicao = 1')
    Left = 632
    Top = 392
    object qryItensnCdRequisicao: TIntegerField
      FieldName = 'nCdRequisicao'
    end
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItensnQtdeReq: TBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'nQtdeReq'
      Precision = 12
    end
    object qryItensnQtdeAtend: TBCDField
      DisplayLabel = 'Quantidades|Atendida'
      FieldName = 'nQtdeAtend'
      Precision = 12
    end
    object qryItensnQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancelada'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItenscNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItensnCdMapaCompra: TIntegerField
      DisplayLabel = 'Processo Compra|Mapa'
      FieldName = 'nCdMapaCompra'
    end
    object qryItensnCdPedido: TIntegerField
      DisplayLabel = 'Processo Compra|Pedido'
      FieldName = 'nCdPedido'
    end
    object qryItenscReferencia: TStringField
      DisplayLabel = 'Item|Refer'#234'ncia'
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryItenscNotaComprador: TStringField
      DisplayLabel = 'Detalhamento|Nota p/ Comprador'
      FieldName = 'cNotaComprador'
      Size = 30
    end
    object qryItenscFlgUrgente: TIntegerField
      DisplayLabel = 'Detalhamento|Urg'#234'ncia'
      FieldName = 'cFlgUrgente'
    end
    object qryItenscJustificativa: TStringField
      DisplayLabel = 'Detalhamento|Justificativa'
      FieldName = 'cJustificativa'
      Size = 30
    end
    object qryItenscDescricaoTecnica: TMemoField
      DisplayLabel = 'Detalhamento|'
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
    object qryItenscUnidadeMedida: TStringField
      DisplayLabel = 'Item|UM'
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItensnValTotal: TBCDField
      DisplayLabel = 'Valor do Item|Total'
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItensnValUnitario: TBCDField
      DisplayLabel = 'Valor do Item|Unit'#225'rio'
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryItensnCdTipoItemRequisicao: TIntegerField
      DisplayLabel = 'Item|Tipo'
      FieldName = 'nCdTipoItemRequisicao'
    end
    object qryItensnCdItemRequisicao: TIntegerField
      FieldName = 'nCdItemRequisicao'
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 632
    Top = 424
  end
  object qryTabStatusRequis: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabStatusRequis, cNmTabStatusRequis'
      'FROM TabStatusRequis'
      'WHERE nCdTabStatusRequis = :nPK')
    Left = 568
    Top = 392
    object qryTabStatusRequisnCdTabStatusRequis: TIntegerField
      FieldName = 'nCdTabStatusRequis'
    end
    object qryTabStatusRequiscNmTabStatusRequis: TStringField
      FieldName = 'cNmTabStatusRequis'
      Size = 50
    end
  end
  object dsTabStatusRequis: TDataSource
    DataSet = qryTabStatusRequis
    Left = 568
    Top = 424
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdProduto, cNmProduto, cReferencia, nValCusto, cUnidadeM' +
        'edida'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 696
    Top = 424
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryProdutonValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 14
      Size = 6
    end
    object qryProdutocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 536
    Top = 392
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsUsuario: TDataSource
    DataSet = qryUsuario
    Left = 536
    Top = 424
  end
  object qrySetor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuarioLogado'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSetor'
      '  FROM Setor'
      ' WHERE nCdSetor = :nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioSetor US'
      
        '                WHERE nCdUsuario = :nCdUsuarioLogado  AND US.nCd' +
        'Setor = Setor.nCdSetor)')
    Left = 472
    Top = 392
    object qrySetorcNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
  end
  object dsSetor: TDataSource
    DataSet = qrySetor
    Left = 472
    Top = 424
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryCCBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTipoRequisicao'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC'
      '      ,cNmCC'
      '  FROM CentroCusto'
      ' WHERE nCdCC    = :nPK'
      '   AND cFlgLanc = 1'
      '   AND EXISTS(SELECT 1'
      '                FROM CentroCustoTipoRequisicao CCTR'
      '               WHERE CCTR.nCdCC = CentroCusto.nCdCC'
      
        '                 AND CCTR.nCdTipoRequisicao = :nCdTipoRequisicao' +
        ')')
    Left = 504
    Top = 392
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCC: TDataSource
    DataSet = qryCC
    Left = 504
    Top = 424
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 696
    Top = 392
  end
  object qryItensAD: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensADBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemRequisicao'
      'WHERE nCdRequisicao = :nPK'
      'AND nCdTipoItemRequisicao = 2')
    Left = 600
    Top = 392
    object qryItensADnCdItemRequisicao: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemRequisicao'
      ReadOnly = True
    end
    object qryItensADnCdRequisicao: TIntegerField
      FieldName = 'nCdRequisicao'
    end
    object qryItensADnCdProduto: TIntegerField
      DisplayLabel = 'Item|'
      FieldName = 'nCdProduto'
    end
    object qryItensADcNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o do Produto'
      FieldName = 'cNmItem'
      Size = 100
    end
    object qryItensADnQtdeReq: TBCDField
      DisplayLabel = 'Quantidade'
      FieldName = 'nQtdeReq'
      Precision = 12
    end
    object qryItensADnQtdeAtend: TBCDField
      FieldName = 'nQtdeAtend'
      Precision = 12
    end
    object qryItensADnQtdeCanc: TBCDField
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItensADnCdMapaCompra: TIntegerField
      DisplayLabel = 'Processo Compra|Mapa'
      FieldName = 'nCdMapaCompra'
    end
    object qryItensADnCdPedido: TIntegerField
      DisplayLabel = 'Processo Compra|Pedido'
      FieldName = 'nCdPedido'
    end
    object qryItensADcReferencia: TStringField
      DisplayLabel = 'Item|Refer'#234'ncia'
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryItensADcNotaComprador: TStringField
      DisplayLabel = 'Nota p/ Comprador'
      FieldName = 'cNotaComprador'
      Size = 30
    end
    object qryItensADcFlgUrgente: TIntegerField
      DisplayLabel = 'Urgente'
      FieldName = 'cFlgUrgente'
    end
    object qryItensADcJustificativa: TStringField
      DisplayLabel = 'Justificativa'
      FieldName = 'cJustificativa'
      Size = 30
    end
    object qryItensADcDescricaoTecnica: TMemoField
      FieldName = 'cDescricaoTecnica'
      BlobType = ftMemo
    end
    object qryItensADcUnidadeMedida: TStringField
      DisplayLabel = 'Item|U.M'
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItensADnValUnitario: TBCDField
      DisplayLabel = 'Valores do Item|Unit'#225'rio'
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryItensADnValTotal: TBCDField
      DisplayLabel = 'Valores do Item|Total'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryItensADnCdTipoItemRequisicao: TIntegerField
      FieldName = 'nCdTipoItemRequisicao'
    end
  end
  object dsItemAD: TDataSource
    DataSet = qryItensAD
    Left = 600
    Top = 424
  end
  object qryFollowUp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtFollowUp'
      '      ,cNmUsuario'
      '      ,cOcorrenciaResum'
      '      ,dDtProxAcao'
      '      ,cOcorrencia'
      '  FROM FollowUp'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = FollowUp.nCdUs' +
        'uario'
      ' WHERE nCdRequisicao = :nPK'
      'ORDER BY dDtFollowUp DESC')
    Left = 664
    Top = 395
    object qryFollowUpdDtFollowUp: TDateTimeField
      FieldName = 'dDtFollowUp'
    end
    object qryFollowUpcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryFollowUpcOcorrenciaResum: TStringField
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowUpdDtProxAcao: TDateTimeField
      FieldName = 'dDtProxAcao'
    end
    object qryFollowUpcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
  end
  object dsFollowUp: TDataSource
    DataSet = qryFollowUp
    Left = 664
    Top = 424
  end
  object menuOpcao: TPopupMenu
    Left = 792
    Top = 392
    object btCancelarRequis: TMenuItem
      Caption = 'Cancelar Requisi'#231#227'o'
      OnClick = btCancelarRequisClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object btImprimirRequis: TMenuItem
      Caption = 'Imprimir Requisi'#231#227'o'
      OnClick = btImprimirRequisClick
    end
  end
  object qryCentroDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CD.nCdCentroDistribuicao'
      '      ,cNmCentroDistribuicao'
      '  FROM CentroDistribuicao CD       '
      ' WHERE CD.nCdCentroDistribuicao = :nPK'
      '       AND EXISTS (SELECT 1 '
      '                FROM LojaCentroDistribuicao '
      '               WHERE nCdLoja = :nCdLoja '
      
        '                 AND LojaCentroDistribuicao.nCdCentroDistribuica' +
        'o = CD.nCdCentroDistribuicao)')
    Left = 759
    Top = 392
    object qryCentroDistribuicaonCdCentroDistribuicao: TIntegerField
      FieldName = 'nCdCentroDistribuicao'
    end
    object qryCentroDistribuicaocNmCentroDistribuicao: TStringField
      FieldName = 'cNmCentroDistribuicao'
      Size = 100
    end
  end
  object dsCentroDistribuicao: TDataSource
    DataSet = qryCentroDistribuicao
    Left = 759
    Top = 424
  end
  object qryGravaConfPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdRequisicao'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdRequisicao     int'
      '       ,@nCdEmpresa        int'
      '       ,@nCdItemRequisicao int'
      '       ,@nCdProduto        int'
      '       ,@nQtdeCont         int'
      '       ,@cNmProduto        varchar(150)'
      '       ,@cReferencia       varchar(15)'
      '       ,@cUnidadeMedida    varchar(3)'
      '       ,@nValCusto         decimal(12,2)'
      '       ,@nValVenda         decimal(12,2)'
      ''
      'SET @nCdRequisicao = :nCdRequisicao'
      'SET @nCdEmpresa    = :nCdEmpresa'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosBip'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosBip (cCdProduto  varchar(20)'
      '                                  ,nQtdeBipada int)'
      '                         '
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosCont'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosCont (cCdProduto varchar(20)'
      '                                   ,nQtdeCont  int)'
      '                         '
      'END'
      ''
      'TRUNCATE TABLE #TempProdutosCont'
      ''
      '--'
      '-- contabiliza produtos bipados'
      '--'
      'INSERT INTO #TempProdutosCont (cCdProduto'
      '                              ,nQtdeCont)'
      '                       SELECT cCdProduto'
      '                             ,SUM(nQtdeBipada)'
      '                         FROM #TempProdutosBip'
      '                        GROUP BY cCdProduto'
      '                        ORDER BY cCdProduto'
      '    '
      '--'
      '-- registra digita'#231#227'o de produtos'
      '--'
      'DECLARE curDigitRequis CURSOR'
      #9'FOR SELECT CAST(cCdProduto as int)'
      #9'          ,nQtdeCont'
      #9#9'  FROM #TempProdutosCont'
      #9#9' ORDER BY cCdProduto'
      ''
      'OPEN curDigitRequis'
      ''
      'FETCH NEXT'
      ' FROM curDigitRequis'
      ' INTO @nCdProduto'
      #9' ,@nQtdeCont'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      #9'--'
      #9'-- verifica se produto j'#225' existe na requisi'#231#227'o'
      #9'--'
      #9'SET @nCdItemRequisicao = NULL'
      #9
      #9'SELECT TOP 1 @nCdItemRequisicao = nCdItemRequisicao'
      '      FROM ItemRequisicao'
      '     WHERE nCdRequisicao = @nCdRequisicao'
      '       AND nCdProduto    = @nCdProduto'
      '       '
      '    IF (@nCdItemRequisicao IS NULL)'
      '    BEGIN'
      ''
      '        SELECT @cNmProduto     = cNmProduto'
      '              ,@cReferencia    = cReferencia'
      '              ,@cUnidadeMedida = cUnidadeMedida'
      '              ,@nValCusto      = nValCusto'
      '              ,@nValVenda      = nValVenda'
      '          FROM Produto'
      '         WHERE nCdProduto = @nCdProduto'
      ''
      '        --'
      
        '        -- se n'#227'o houver pre'#231'o de custo, busca pre'#231'o do '#250'ltimo r' +
        'ecebimento'
      '        --'
      '        IF (ISNULL(@nValCusto,0) = 0)'
      '        BEGIN'
      '            '
      '            SELECT TOP 1 @nValCusto = nValCustoFinal'
      '              FROM ItemRecebimento'
      
        '                   INNER JOIN Recebimento ON Recebimento.nCdRece' +
        'bimento = ItemRecebimento.nCdRecebimento'
      '             WHERE Recebimento.nCdTabStatusReceb >= 4'
      '               AND Recebimento.nCdEmpresa         = @nCdEmpresa'
      '               AND nCdProduto                     = @nCdProduto'
      '             ORDER BY dDtReceb DESC'
      '             '
      '            --'
      
        '            -- se n'#227'o localizar pre'#231'o do '#250'ltimo recebimento, bus' +
        'ca pre'#231'o da '#250'ltima compra'
      '            --'
      '            IF (ISNULL(@nValCusto,0) = 0)'
      '            BEGIN'
      '            '
      '                SELECT TOP 1 @nValCusto = nValCustoUnit'
      '                  FROM ItemPedido'
      
        '                       INNER JOIN Pedido     ON Pedido.nCdPedido' +
        ' = ItemPedido.nCdPedido'
      
        '                       INNER JOIN TipoPedido ON TipoPedido.nCdTi' +
        'poPedido = Pedido.nCdTipoPedido'
      '                 WHERE Pedido.nCdTabStatusPed IN (4,5)'
      '                   AND Pedido.nCdEmpresa       = @nCdEmpresa'
      '                   AND ItemPedido.nCdProduto   = @nCdProduto'
      '                   AND ItemPedido.nQtdeExpRec  > 0'
      '                   AND TipoPedido.cFlgCompra   = 1'
      '                 ORDER BY dDtPedido DESC'
      '            '
      '            END'
      '            '
      '            --'
      
        '            -- se n'#227'o localizar '#250'ltima compra, considera valor d' +
        'e venda do produto'
      '            --'
      '            IF (ISNULL(@nValCusto,0) = 0)'
      '            BEGIN'
      '            '
      '               SET @nValCusto = @nValVenda'
      '            '
      '            END'
      '             '
      '        END'
      '        '
      '        SET @nCdItemRequisicao = NULL'
      '        EXEC usp_ProximoID '#39'ITEMREQUISICAO'#39
      '                          ,@nCdItemRequisicao OUTPUT'
      ''
      '        INSERT INTO ItemRequisicao (nCdItemRequisicao'
      '                                   ,nCdRequisicao'
      '                                   ,nCdProduto'
      '                                   ,cNmItem'
      '                                   ,cReferencia'
      '                                   ,cUnidadeMedida'
      '                                   ,nValUnitario'
      '                                   ,nCdTipoItemRequisicao)'
      '                            VALUES (@nCdItemRequisicao'
      '                                   ,@nCdRequisicao'
      '                                   ,@nCdProduto'
      '                                   ,@cNmProduto'
      '                                   ,@cReferencia'
      '                                   ,@cUnidadeMedida'
      '                                   ,@nValCusto'
      '                                   ,1) -- item estoque'
      ''
      '    END'
      '    '
      '    --'
      '    -- contabiliza item da requisi'#231#227'o'
      '    --'
      '    UPDATE ItemRequisicao'
      '       SET nQtdeReq  = nQtdeReq + @nQtdeCont'
      '          ,nValTotal = nValUnitario * (nQtdeReq + @nQtdeCont)'
      '     WHERE nCdRequisicao     = @nCdRequisicao'
      '       AND nCdItemRequisicao = @nCdItemRequisicao'
      '       AND nCdProduto        = @nCdProduto'
      '       '
      '    FETCH NEXT'
      '     FROM curDigitRequis'
      '     INTO @nCdProduto'
      '         ,@nQtdeCont'
      '         '
      'END'
      ''
      'CLOSE curDigitRequis'
      'DEALLOCATE curDigitRequis')
    Left = 792
    Top = 424
  end
end
