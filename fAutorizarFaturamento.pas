unit fAutorizarFaturamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, ADODB, DB, cxPC, cxControls,
  ToolCtrlsEh;

type
  TfrmAutorizarFaturamento = class(TfrmProcesso_Padrao)
    qryItensFaturar: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryItensFaturarnCdItemFaturar: TAutoIncField;
    qryItensFaturarnCdProduto: TIntegerField;
    qryItensFaturarcNmProduto: TStringField;
    qryItensFaturarnQtdePick: TBCDField;
    qryItensFaturarnQtdeLibFat: TBCDField;
    qryItensFaturarnCdLocalEstoque: TIntegerField;
    dsItensFaturar: TDataSource;
    qryItensFaturarnCdItemPedido: TIntegerField;
    qryItensFaturarnCdPickList: TIntegerField;
    SP_AUTORIZAR_FATURAMENTO: TADOStoredProc;
    ToolButton4: TToolButton;
    btRegistroLoteSerial: TToolButton;
    qryItensFaturarcLocalizacao: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryItensFaturarcNmTipoSaida: TStringField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItensFaturarcCdProdutoConf: TStringField;
    qryItensFaturarnCdProdutoConf: TIntegerField;
    qryItensFaturarcNmProdutoConf: TStringField;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure qryItensFaturarBeforePost(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryItensFaturarcCdProdutoConfChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAutorizarFaturamento: TfrmAutorizarFaturamento;

implementation

uses fMenu,fRegistroMovLote;

{$R *.dfm}

procedure TfrmAutorizarFaturamento.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAutorizarFaturamento.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAutorizarFaturamento.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('USACONFAUTFAT') = 'N') then
  begin
      cxPageControl1.ActivePageIndex     := 0;
      cxPageControl1.Pages[1].TabVisible := False;
      DBGridEh1.SelectedIndex            := 3;

      DBGridEh1.SetFocus;
      DBGridEh1.Col := 4 ;
  end else
  begin
      cxPageControl1.ActivePageIndex     := 1;
      cxPageControl1.Pages[0].TabVisible := False;
      DBGridEh2.SelectedIndex            := 4;

      DBGridEh2.SetFocus;
      DBGridEh2.Col := 5 ;
  end;
  
end;

procedure TfrmAutorizarFaturamento.ToolButton1Click(Sender: TObject);
var
  nQtdeLote    : Double;
  objForm      : TfrmRegistroMovLote;
  nCdTipoSaida : integer;
begin
  inherited;

  if (qryItensFaturar.RecordCount = 0) then
  begin
      MensagemAlerta('Pedido n�o possui saldo dispon�vel para autoriza��o do faturamento.');
      Exit;
  end;

  qryItensFaturar.First;

  if (frmMenu.LeParametro('USACONFAUTFAT') = 'S') then
  begin

      while not qryItensFaturar.Eof do
      begin

          if (qryItensFaturarnQtdeLibfat.Value > 0) and (qryItensFaturarcNmProdutoConf.Value = '') then
          begin
              MensagemAlerta('Quantidade de libera��o informada sem confer�ncia do produto. Informe o produto para confer�ncia.') ;
              abort;
          end ;

          nCdTipoSaida := DBGridEh2.FieldColumns['cNmTipoSaida'].PickList.IndexOf(qryItensFaturarcNmTipoSaida.Value);

          case nCdTipoSaida of

          0:
          begin

              if (qryItensFaturarnQtdeLibFat.Value < qryItensFaturarnQtdePick.Value) then
              begin
                  MensagemAlerta('Para o tipo de sa�da "Sa�da Total" a quantidade informada n�o pode ser menor que a quantidade do pedido.');
                  abort;
              end;

              if (qryItensFaturarnCdProdutoConf.Value <> qryItensFaturarnCdProduto.Value) and (qryItensFaturarnQtdeLibFat.Value > 0) then
              begin
                  MensagemAlerta('O Produto Informado na confer�ncia difere do Produto do Pedido.');
                  abort;
              end;

          end;

          1:
          begin

              if (qryItensFaturarnQtdeLibFat.Value = 0) then
              begin
                  MensagemAlerta('Para o tipo de sa�da "Sa�da Parcial" a quantidade informada deve ser maior que 0 e menor/ igual a quantidade do pedido.');
                  abort;
              end;

              if (qryItensFaturarnCdProdutoConf.Value <> qryItensFaturarnCdProduto.Value) and (qryItensFaturarnQtdeLibFat.Value > 0) then
              begin
                  MensagemAlerta('O Produto Informado na confer�ncia difere do Produto do Pedido.');
                  abort;
              end;

          end;

          2:
              if (qryItensFaturarnQtdeLibFat.Value > 0) then
              begin
                  MensagemAlerta('Para o tipo de sa�da "N�o Enviado" nenhuma quantidade deve ser informada.');
                  abort;
              end;
              else begin
                  MensagemAlerta('Tipo de Sa�da n�o informado ou inv�lido.');
                  abort;
              end;
          end;

          qryItensFaturar.Next;
          
      end;

  end ;
  
  if (MessageDlg('Deseja autorizar o faturamento ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  try

      qryItensFaturar.First;

      while not (qryItensFaturar.Eof) do
      begin

          if (qryItensFaturarnCdProduto.AsString <> '') then  {-- quando for item AD n�o verifica lote/serial--}
          begin
              nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryItensFaturarnCdProduto.Value
                                                                        , 6
                                                                        , qryItensFaturarnCdItemPedido.Value ) ;

              {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
              if (nQtdeLote <> -1) then
              begin

                  if ( nQtdeLote <> qryItensFaturarnQtdeLibFat.Value ) then
                  begin
                      MensagemAlerta('A quantidade especificada no Registro de Lotes/Seriais para o produto: ' + qryItensFaturarnCdProduto.AsString + ' - ' + qryItensFaturarcNmProduto.Value + ' n�o confere com o total deste movimento.') ;
                      btRegistroLoteSerial.Click;
                      abort ;
                  end ;

              end ;
          end;

          qryItensFaturar.Next;
      end;

  finally
      freeAndNil( objForm ) ;
  end ;


  frmMenu.Connection.BeginTrans;

  try
      SP_AUTORIZAR_FATURAMENTO.Close;
      SP_AUTORIZAR_FATURAMENTO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_AUTORIZAR_FATURAMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Faturamento autorizado com sucesso.');

  Close;

end;

procedure TfrmAutorizarFaturamento.btRegistroLoteSerialClick(
  Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryItensFaturarnCdProduto.Value
                         , 0
                         , 6 {-- Embarque/Expedi��o Pedido --}
                         , qryItensFaturarnCdItemPedido.Value
                         , qryItensFaturarnCdLocalEstoque.Value
                         , qryItensFaturarnQtdeLibFat.Value
                         , 'PEDIDO DE VENDA - ITEM PEDIDO No. ' + Trim( qryItensFaturarnCdItemPedido.AsString )
                         , False
                         , False
                         , False) ;

  freeAndNil( objForm ) ;
end;

procedure TfrmAutorizarFaturamento.qryItensFaturarBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItensFaturarnQtdeLibFat.Value < 0) then
  begin
      MensagemAlerta('A auantidade selecionada n�o pode ser menor que 0.');
      abort;
  end;

  if (qryItensFaturarnQtdeLibFat.Value > qryItensFaturarnQtdePick.Value) then
  begin
      MensagemAlerta('A quantidade dispon�vel no Picking n�o � suficiente para atender este faturamento.');
      abort;
  end;

end;

procedure TfrmAutorizarFaturamento.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh2.SelectedIndex = DBGridEh2.FieldColumns['cCdProdutoConf'].Index) then
  begin

      if (Trim(qryItensFaturarcCdProdutoConf.Value) <> '') then
      begin
          qryProduto.Close;
          qryProduto.Parameters.ParamByName('cEAN').Value := Trim(qryItensFaturarcCdProdutoConf.Value);
          qryProduto.Open;

          if (qryProduto.RecordCount = 0) then
          begin
              MensagemAlerta('C�digo do produto n�o cadastrado.');
              abort;
          end;

          qryItensFaturar.Edit;
          qryItensFaturarcNmProdutoConf.Value := qryProdutocNmProduto.Value;
          qryItensFaturarnCdProdutoConf.Value := qryProdutonCdProduto.Value;
          qryItensFaturar.Post;
      end;

  end;
end;

procedure TfrmAutorizarFaturamento.qryItensFaturarcCdProdutoConfChange(
  Sender: TField);
begin
  inherited;

  if (qryItensFaturar.State = dsEdit) then
      qryItensFaturarcNmProdutoConf.Value := '' ;
      
end;

end.
