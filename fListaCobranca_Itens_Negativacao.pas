unit fListaCobranca_Itens_Negativacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmListaCobranca_Itens_Negativacao = class(TfrmProcesso_Padrao)
    qryPreparaTemp: TADOQuery;
    qryTempTerceiroNegativar: TADOQuery;
    qryTempTerceiroNegativarnCdTerceiro: TIntegerField;
    qryTempTerceiroNegativarcCNPJCPF: TStringField;
    qryTempTerceiroNegativarcNmTerceiro: TStringField;
    qryTempTerceiroNegativarcFlgAux: TIntegerField;
    DBGridEh1: TDBGridEh;
    dsTempTerceiroNegativar: TDataSource;
    qryProcessaNegativacao: TADOQuery;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    SP_PROCESSA_NEGATIVACAO: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobranca : integer ;
  end;

var
  frmListaCobranca_Itens_Negativacao: TfrmListaCobranca_Itens_Negativacao;

implementation

uses fMenu, rTituloNegativado_view;

{$R *.dfm}

procedure TfrmListaCobranca_Itens_Negativacao.ToolButton1Click(
  Sender: TObject);
var
    i : integer ;
begin
  inherited;

  if (qryTempTerceiroNegativar.State <> dsBrowse) then
      qryTempTerceiroNegativar.Post ;

  qryTempTerceiroNegativar.First ;

  i := 0 ;
  
  while not qryTempTerceiroNegativar.Eof do
  begin
      if (qryTempTerceiroNegativarcFlgAux.Value = 1) then
          i := i + 1 ;

      qryTempTerceiroNegativar.Next ;
  end ;

  qryTempTerceiroNegativar.First ;

  if (i=0) then
  begin
      MensagemErro('Nenhum cliente selecionado para negativa��o.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a negativa��o destes clientes ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;


  qryTempTerceiroNegativar.First ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_PROCESSA_NEGATIVACAO.Close ;
      SP_PROCESSA_NEGATIVACAO.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      SP_PROCESSA_NEGATIVACAO.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      SP_PROCESSA_NEGATIVACAO.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Clientes negativados com sucesso!') ;

  ToolButton5.Click;

  Close ;

end;

procedure TfrmListaCobranca_Itens_Negativacao.ToolButton5Click(
  Sender: TObject);
var
  objRel : TrptTituloNegativado_view;
begin
  inherited;

  objRel := TrptTituloNegativado_view.Create(nil);

  try
      try
          objRel.qryTitulos.Close;
          objRel.qryTitulos.Parameters.ParamByName('nCdListaCobranca').Value := nCdListaCobranca ;
          objRel.qryTitulos.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
          objRel.qryTitulos.Parameters.ParamByName('dDtInicial').Value       := '01/01/1900' ;
          objRel.qryTitulos.Parameters.ParamByName('dDtFinal').Value         := '01/01/1900' ;
          objRel.qryTitulos.Parameters.ParamByName('nCdLoja').Value          := 0;
          objRel.qryTitulos.Parameters.ParamByName('nCdLojaNeg').Value       := 0;
          objRel.qryTitulos.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Lista de Cobranca : ' + frmMenu.ZeroEsquerda(intToStr(nCdListaCobranca),10) ;

          if (frmMenu.nCdLojaAtiva > 0) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Loja Cobradora: ' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdLojaAtiva),3) + '-' + frmMenu.cNmLojaAtiva ;

          { -- remove dados complementares do cliente no relat�rio -- }
          objRel.QRSubDetail1.PrintIfEmpty := False;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

end.
