unit rComissaoRepresSintetico_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TrptComissaoRepresSintetico_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRShape6: TQRShape;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel26: TQRLabel;
    qryResultado: TADOQuery;
    QRLabel4: TQRLabel;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText5: TQRDBText;
    QRExpr1: TQRExpr;
    QRDBText11: TQRDBText;
    qryResultadonCdDoctoFiscal: TIntegerField;
    qryResultadoncdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadoiNrDocto: TIntegerField;
    qryResultadocSerie: TStringField;
    qryResultadocNmTerceiro_1: TStringField;
    qryResultadonCdPedido: TIntegerField;
    qryResultadodDtEmissao: TDateTimeField;
    qryResultadocFlgComissaoPaga: TIntegerField;
    qryResultadonCdSp: TIntegerField;
    qryResultadocComissaoPaga: TStringField;
    qryResultadonValAcumulado: TBCDField;
    qryResultadonValVenda: TBCDField;
    qryResultadonValBaseCalculo: TBCDField;
    qryResultadonValComissao: TBCDField;
    QRBand4: TQRBand;
    QRLabel9: TQRLabel;
    QRExpr2: TQRExpr;
    QRShape3: TQRShape;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    PageFooterBand1: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptComissaoRepresSintetico_View: TrptComissaoRepresSintetico_View;

implementation

{$R *.dfm}

end.
