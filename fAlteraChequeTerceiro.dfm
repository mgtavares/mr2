inherited frmAlteraChequeTerceiro: TfrmAlteraChequeTerceiro
  Left = 291
  Top = 199
  Width = 868
  Height = 499
  Caption = 'Altera'#231#227'o Cheque Terceiro'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 852
    Height = 438
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 69
    Top = 62
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 60
    Top = 86
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ag'#234'ncia'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 176
    Top = 86
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 408
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Digito'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 11
    Top = 110
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'CPF/CNPJ Emissor'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 480
    Top = 86
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Cheque'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 31
    Top = 134
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Cheque'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 211
    Top = 110
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Dep'#243'sito'
    FocusControl = DBEdit9
  end
  inherited ToolBar2: TToolBar
    Width = 852
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCheque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 176
    Top = 56
    Width = 369
    Height = 19
    DataField = 'cNmBanco'
    DataSource = dsBanco
    TabOrder = 10
  end
  object DBEdit3: TDBEdit [13]
    Left = 104
    Top = 80
    Width = 65
    Height = 19
    DataField = 'cAgencia'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [14]
    Left = 208
    Top = 80
    Width = 195
    Height = 19
    DataField = 'cConta'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [15]
    Left = 448
    Top = 80
    Width = 25
    Height = 19
    DataField = 'cDigito'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [16]
    Left = 104
    Top = 104
    Width = 105
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [17]
    Left = 552
    Top = 80
    Width = 130
    Height = 19
    DataField = 'iNrCheque'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit8: TDBEdit [18]
    Left = 104
    Top = 128
    Width = 105
    Height = 19
    DataField = 'nValCheque'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit9: TDBEdit [19]
    Left = 288
    Top = 104
    Width = 65
    Height = 19
    DataField = 'dDtDeposito'
    DataSource = dsMaster
    TabOrder = 8
  end
  object edtBanco: TER2LookupDBEdit [20]
    Left = 104
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 2
    CodigoLookup = 23
    QueryLookup = qryBanco
  end
  inherited qryMaster: TADOQuery
    BeforeClose = qryMasterBeforeClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCheque'
      '      ,nCdBanco'
      '      ,cAgencia'
      '      ,cConta'
      '      ,cDigito'
      '      ,cCNPJCPF'
      '      ,iNrCheque'
      '      ,nValCheque'
      '      ,dDtDeposito '
      '  FROM Cheque '
      ' WHERE nCdCheque        = :nPK'
      '   AND nCdTabTipoCheque = 2')
    object qryMasternCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryMastercConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryMastercDigito: TStringField
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMasteriNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMasternValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
      EditMask = '!99/99/9999;1;_'
    end
  end
  object qryBanco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdBanco'
      '              ,cNmBanco'
      '    FROM Banco'
      ' WHERE nCdBanco = :nPK')
    Left = 152
    Top = 272
    object qryBanconCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryBancocNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
  end
  object dsBanco: TDataSource
    DataSet = qryBanco
    Left = 168
    Top = 304
  end
end
