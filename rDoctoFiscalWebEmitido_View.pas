unit rDoctoFiscalWebEmitido_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, QRPrev, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptDoctoFiscalWebEmitido_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    SummaryBand1: TQRBand;
    DetailBand1: TQRBand;
    QRDBText7: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    SPREL_DOCTO_FISCALWEB_EMITIDO: TADOStoredProc;
    QRGroup1: TQRGroup;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRDBText11: TQRDBText;
    QRBand2: TQRBand;
    QRShape9: TQRShape;
    QRDBText13: TQRDBText;
    QRDBText24: TQRDBText;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRExpr26: TQRExpr;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel22: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel25: TQRLabel;
    QRDBText30: TQRDBText;
    SPREL_DOCTO_FISCALWEB_EMITIDOnCdDoctoFiscal: TIntegerField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcEmpresa: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcLoja: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcCNPJCPF: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcNmCliente: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcEndereco: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOiNumero: TIntegerField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcComplEnderDestino: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcBairro: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcCidade: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcUF: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcCep: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOiQtdeVolume: TIntegerField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnPesoBruto: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnPesoLiq: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcDiasFrete: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcEspecieTransp: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcTipoEntrega: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcNmTabTipoModFrete: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcTrackingCode: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcCNPJTransp: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcNmTransp: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnValProduto: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnValFrete: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnValDesconto: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOnValTotal: TBCDField;
    SPREL_DOCTO_FISCALWEB_EMITIDOdDtEmissao: TDateTimeField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcNrDoctoSerie: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcChaveNFe: TStringField;
    SPREL_DOCTO_FISCALWEB_EMITIDOcNmStatusDocto: TStringField;
    QRLabel43: TQRLabel;
    QRExpr24: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDoctoFiscalWebEmitido_View: TrptDoctoFiscalWebEmitido_View;

implementation

{$R *.dfm}

uses
  fMenu;

end.
