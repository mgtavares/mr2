inherited frmManutencaoPedidoVenda: TfrmManutencaoPedidoVenda
  Left = 321
  Top = 98
  Width = 886
  Height = 614
  Caption = 'Manuten'#231#227'o de Pedido de Venda'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 870
    Height = 551
  end
  object Label1: TLabel [1]
    Left = 43
    Top = 38
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Pedido'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 66
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 88
    Top = 86
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 73
    Top = 134
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 58
    Top = 110
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vendedor'
  end
  object Label7: TLabel [6]
    Left = 200
    Top = 38
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data do Pedido'
    FocusControl = DBEdit7
  end
  object Label9: TLabel [7]
    Left = 12
    Top = 206
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caixa Recebimento'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [8]
    Left = 17
    Top = 158
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cond. Pagamento'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [9]
    Left = 452
    Top = 38
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status do Pedido'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [10]
    Left = 188
    Top = 182
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Produtos'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [11]
    Left = 350
    Top = 182
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [12]
    Left = 43
    Top = 182
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Pedido'
    FocusControl = DBEdit14
  end
  object Label15: TLabel [13]
    Left = 368
    Top = 206
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lan'#231'amento'
    FocusControl = DBEdit15
  end
  object Label6: TLabel [14]
    Left = 511
    Top = 206
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Docto Fiscal'
    FocusControl = DBEdit17
  end
  inherited ToolBar2: TToolBar
    Width = 870
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited btAlterar: TToolButton
      Visible = True
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 112
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdPedido'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit2: TDBEdit [17]
    Tag = 1
    Left = 180
    Top = 56
    Width = 573
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 180
    Top = 80
    Width = 573
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 180
    Top = 128
    Width = 573
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [20]
    Tag = 1
    Left = 283
    Top = 32
    Width = 137
    Height = 19
    DataField = 'dDtPedido'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit9: TDBEdit [21]
    Tag = 1
    Left = 112
    Top = 200
    Width = 233
    Height = 19
    DataField = 'nCdConta'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit10: TDBEdit [22]
    Tag = 1
    Left = 112
    Top = 152
    Width = 641
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit11: TDBEdit [23]
    Tag = 1
    Left = 545
    Top = 32
    Width = 208
    Height = 19
    DataField = 'cNmTabStatusPed'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit12: TDBEdit [24]
    Tag = 1
    Left = 273
    Top = 176
    Width = 73
    Height = 19
    DataField = 'nValProdutos'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit13: TDBEdit [25]
    Tag = 1
    Left = 433
    Top = 176
    Width = 73
    Height = 19
    DataField = 'nValDesconto'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit14: TDBEdit [26]
    Tag = 1
    Left = 112
    Top = 176
    Width = 73
    Height = 19
    DataField = 'nValPedido'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit15: TDBEdit [27]
    Tag = 1
    Left = 433
    Top = 200
    Width = 73
    Height = 19
    DataField = 'nCdLanctoFin'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit6: TDBEdit [28]
    Tag = 1
    Left = 112
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit8: TDBEdit [29]
    Tag = 1
    Left = 112
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit16: TDBEdit [30]
    Tag = 1
    Left = 112
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 17
  end
  object cxPageControl1: TcxPageControl [31]
    Left = 8
    Top = 240
    Width = 793
    Height = 321
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 18
    ClientRectBottom = 317
    ClientRectLeft = 4
    ClientRectRight = 789
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens do Pedido'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 785
        Height = 293
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemPedidos
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Title.Caption = 'Itens|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Title.Caption = 'Itens|Descri'#231#227'o'
            Width = 385
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Title.Caption = 'Itens|Qtd. Pedido'
            Width = 90
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            Title.Caption = 'Itens|Valor Unit'#225'rio'
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            Title.Caption = 'Itens|Valor Total'
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit18: TDBEdit [32]
    Tag = 1
    Left = 180
    Top = 104
    Width = 573
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsVendedoresRede
    TabOrder = 19
  end
  object DBEdit17: TDBEdit [33]
    Tag = 1
    Left = 606
    Top = 200
    Width = 98
    Height = 19
    DataField = 'nCdDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 14
  end
  object edtVendedor: TER2LookupMaskEdit [34]
    Left = 112
    Top = 104
    Width = 65
    Height = 19
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnExit = edtVendedorExit
    OnKeyDown = edtVendedorKeyDown
    OnBeforeLookup = edtVendedorBeforeLookup
    CodigoLookup = 703
    QueryLookup = qryVendedoresRede
  end
  inherited qryMaster: TADOQuery
    AfterOpen = qryMasterAfterOpen
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPedido'
      '      ,Empresa.cNmEmpresa'
      '      ,Pedido.nCdEmpresa'
      '      ,Loja.cNmLoja'
      '      ,Pedido.nCdLoja'
      '      ,Terceiro.cNmTerceiro'
      '      ,Pedido.nCdTerceiro'
      '      ,Usuario.cNmUsuario cNmVendedor'
      '      ,Usuario.nCdUsuario'
      '      ,Pedido.nCdTerceiroColab'
      '      ,TP.cNmTerceiro cNmTerceiroPagador'
      '      ,Pedido.dDtPedido'
      '      ,LanctoFin.dDtLancto'
      '      ,CB.nCdConta'
      '      ,CondPagto.cNmCondPagto'
      '      ,TSP.cNmTabStatusPed'
      '      ,Pedido.nValProdutos'
      '      ,(SELECT Sum(nValDesconto)'
      '          FROM ItemPedido'
      '         WHERE nCdPedido = Pedido.nCdPedido) as nValDesconto'
      '      ,(SELECT Sum(nValTotalItem)'
      '          FROM ItemPedido'
      '         WHERE nCdPedido = Pedido.nCdPedido) as nValPedido'
      '      ,Pedido.nCdLanctoFin'
      '      ,nCdDoctoFiscal = (SELECT nCdDoctoFiscal '
      '                           FROM DoctoFiscal '
      
        '                          WHERE nCdLanctoFinDoctoFiscal = Pedido' +
        '.nCdLanctoFin)'
      '  FROM Pedido'
      
        '       INNER JOIN TabStatusPed TSP ON TSP.nCdTabStatusPed       ' +
        '     = Pedido.nCdTabStatusPed'
      
        '       INNER JOIN Empresa          ON Empresa.nCdEmpresa        ' +
        '     = Pedido.nCdEmpresa'
      
        '       INNER JOIN Loja             ON Loja.nCdLoja              ' +
        '     = Pedido.nCdLoja'
      
        '       INNER JOIN Terceiro         ON Terceiro.nCdTerceiro      ' +
        '     = Pedido.nCdTerceiro'
      
        '       INNER JOIN Usuario          ON Usuario.nCdTerceiroRespons' +
        'avel = Pedido.nCdTerceiroColab'
      
        '       LEFT  JOIN Terceiro TP      ON TP.nCdTerceiro            ' +
        '     = Pedido.nCdTerceiroPagador'
      
        '       LEFT  JOIN CondPagto        ON CondPagto.nCdCondPagto    ' +
        '     = Pedido.nCdCondPagto'
      
        '       LEFT  JOIN LanctoFin        ON LanctoFin.nCdLanctoFin    ' +
        '     = Pedido.nCdLanctoFin'
      
        '       LEFT  JOIN ContaBancaria CB ON CB.nCdContaBancaria       ' +
        '     = LanctoFin.nCdContaBancaria'
      ' WHERE nCdPedido = :nPK'
      
        '   AND nCdTipoPedido                = Convert(int,dbo.fn_LeParam' +
        'etro('#39'TIPOPEDPDV'#39'))'
      
        '   AND ((LanctoFin.nCdContaBancaria is null) OR (CB.cFlgCaixa  =' +
        ' 1))')
    Left = 320
    Top = 328
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMastercNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryMastercNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercNmVendedor: TStringField
      FieldName = 'cNmVendedor'
      Size = 50
    end
    object qryMastercNmTerceiroPagador: TStringField
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryMasterdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryMasterdDtLancto: TDateTimeField
      FieldName = 'dDtLancto'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMastercNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryMastercNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
    object qryMasternValProdutos: TBCDField
      FieldName = 'nValProdutos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 6
    end
    object qryMasternValPedido: TBCDField
      FieldName = 'nValPedido'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryMasternCdTerceiroColab: TIntegerField
      FieldName = 'nCdTerceiroColab'
    end
    object qryMasternCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
  end
  inherited dsMaster: TDataSource
    Left = 320
    Top = 360
  end
  inherited qryID: TADOQuery
    Left = 352
    Top = 328
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 416
    Top = 328
  end
  inherited qryStat: TADOQuery
    Left = 384
    Top = 328
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 416
    Top = 360
  end
  inherited ImageList1: TImageList
    Left = 288
    Top = 328
  end
  object qryItemPedidos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPedido '
      '      ,nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nValUnitario'
      '      ,nValTotalItem'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK')
    Left = 448
    Top = 328
    object qryItemPedidosnCdPedido: TIntegerField
      DisplayLabel = 'N'#250'm. Pedido'
      FieldName = 'nCdPedido'
    end
    object qryItemPedidosnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidoscNmItem: TStringField
      DisplayLabel = 'Item'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidosnQtdePed: TBCDField
      DisplayLabel = 'Qtd. Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidosnValUnitario: TBCDField
      DisplayLabel = 'Valor Unit'#225'rio'
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryItemPedidosnValTotalItem: TBCDField
      DisplayLabel = 'Valor Total'
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsItemPedidos: TDataSource
    DataSet = qryItemPedidos
    Left = 448
    Top = 360
  end
  object qryVendedoresRede: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '      ,Usuario.nCdTerceiroResponsavel'
      
        '  FROM Usuario INNER JOIN UsuarioEmpresa ON UsuarioEmpresa.nCdUs' +
        'uario = Usuario.nCdUsuario'
      ' WHERE Usuario.nCdUsuario    = :nPK'
      '   AND Usuario.nCdStatus     = 1'
      '   AND (Usuario.cFlgVendedor = 1 OR Usuario.cFlgGerente  = 1)')
    Left = 480
    Top = 328
    object qryVendedoresRedenCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedoresRedecNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryVendedoresRedenCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
  end
  object dsVendedoresRede: TDataSource
    DataSet = qryVendedoresRede
    Left = 480
    Top = 360
  end
  object SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 380
    Top = 360
  end
end
