unit fMemoText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls;

type
  TfrmMemoText = class(TfrmProcesso_Padrao)
    campoMemo: TMemo;
    procedure FormShow(Sender: TObject);
    procedure campoMemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMemoText: TfrmMemoText;

implementation

{$R *.dfm}

procedure TfrmMemoText.FormShow(Sender: TObject);
begin
  inherited;

  campoMemo.Font.Name := 'Courier New' ;
  campoMemo.Font.Size := 14 ;

  
end;

procedure TfrmMemoText.campoMemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  if (Shift = [ssCtrl]) and (key = vk_return) then
      ToolButton2.Click;

end;

end.
