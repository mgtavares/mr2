unit fDepositoBancario_ConsultaCheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmDepositoBancario_ConsultaCheques = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryCheques: TADOQuery;
    dsCheques: TDataSource;
    qryChequesnCdCheque: TAutoIncField;
    qryChequesnCdBanco: TIntegerField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequescCNPJCPF: TStringField;
    qryChequescNmTerceiro: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDepositoBancario_ConsultaCheques: TfrmDepositoBancario_ConsultaCheques;

implementation

{$R *.dfm}

end.
