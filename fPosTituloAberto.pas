unit fPosTituloAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TfrmPosTituloAberto = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DBEdit7: TDBEdit;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    RadioGroup1: TRadioGroup;
    edtCdCobradora: TMaskEdit;
    qryCobradora: TADOQuery;
    qryCobradoranCdCobradora: TIntegerField;
    qryCobradoracNmCobradora: TStringField;
    DBEdit1: TDBEdit;
    DataSource6: TDataSource;
    RadioGroup2: TRadioGroup;
    MaskEdit4: TMaskEdit;
    DBEdit4: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    Image2: TImage;
    dsLoja: TDataSource;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    Label4: TLabel;
    MaskEdit8: TMaskEdit;
    DBEdit5: TDBEdit;
    qryGrupoEspecieTitulo: TADOQuery;
    qryGrupoEspecieTitulonCdGrupoEspTit: TIntegerField;
    qryGrupoEspecieTitulocNmGrupoEspTit: TStringField;
    dsGrupoEspecieTitulo: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdCobradoraExit(Sender: TObject);
    procedure edtCdCobradoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdCobradoraChange(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPosTituloAberto: TfrmPosTituloAberto;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rPosTituloAberto;

{$R *.dfm}

procedure TfrmPosTituloAberto.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  MaskEdit7.Text := '1' ;

  qryMoeda.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    qryMoeda.Parameters.ParamByName('nCdMoeda').Value := MaskEdit7.Text ;
    qryMoeda.Open ;
  end ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit4.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit4.text) ;
  end
  else
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;

end;


procedure TfrmPosTituloAberto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  If (Trim(MaskEdit5.Text) <> '') then
  begin
    qryEspTit.Close ;
    qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEspTit.Parameters.ParamByName('nPK').Value := Trim(MaskEdit5.Text) ;
    qryEspTit.Open;
  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMoeda.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    qryMoeda.Parameters.ParamByName('nCdMoeda').Value := MaskEdit7.Text ;
    qryMoeda.Open ;
  end ;

end;

procedure TfrmPosTituloAberto.ToolButton1Click(Sender: TObject);
var
  objRel : TrptPosTituloAberto ;
begin

  if (DBEdit1.Text <> '') and (RadioGroup1.ItemIndex = 0) then
  begin
      MensagemAlerta('N�o � permitido selecionar cobradora para t�tulos a pagar.') ;
      edtCdCobradora.Text := '' ;
      qryCobradora.Close ;
  end ;

  {--posiciona qryEmpresa para verificar se a empresa digitada � valida
   --e tem acesso permitido para o usu�rio logado--}

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Open;
  if qryEmpresa.Eof then MaskEdit3.Text := '';

  {--posiciona qryLoja para verificar se a loja digitada � valida
   --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

  qryLoja.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin

     qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
     qryLoja.Parameters.ParamByName('nPK').Value        := MaskEdit4.Text ;
     qryLoja.Open ;

     if qryLoja.Eof then
         MaskEdit4.Text := '';

  end;

  objRel := TrptPosTituloAberto.Create(Self) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.ConvInteiro(MaskEdit3.Text);

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value   := 0
  else begin
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value   := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  end ;

  {--posiciona qryEspTit para verificar se a Especie de Titulo digitada
   --� valida e tem acesso permitido para o usu�rio logado--}

  qryEspTit.Close ;
  qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEspTit,Trim(MaskEdit5.Text));

  if(qryEspTit.Eof) then
        objRel.usp_Relatorio.Parameters.ParamByName('@nCdEspTit').Value   := 0
    else objRel.usp_Relatorio.Parameters.ParamByName('@nCdEspTit').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;

  {--aqui passa os valores para os parametros de entrada da procedure--}

  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value     := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdCobradora').Value    := frmMenu.ConvInteiro(edtCdCobradora.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdMoeda').Value        := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value      := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value        := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEspTit').Value  := frmMenu.ConvInteiro(MaskEdit8.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'D' ;

  if (RadioGroup1.ItemIndex = 1) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgIncTitCobradora').Value := 1
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgIncTitCobradora').Value := 0 ;

  objRel.usp_Relatorio.Open  ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (DBedit3.Text <> '') then
      objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

  if (DBedit5.Text <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Grupo Esp�cie: ' + trim(MaskEdit8.Text) + '-' + DbEdit5.Text ;

  if (DBedit7.Text <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Esp�cie: ' + trim(MaskEdit5.Text) + '-' + DbEdit7.Text ;

  if (DBedit10.Text <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Terceiro: ' + trim(MaskEdit6.Text) + '-' + DbEdit10.Text ;

  if (DBedit1.Text <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Cobradora: ' + trim(edtCdCobradora.Text) + '-' + DbEdit1.Text ;

  if (DBedit13.Text <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Moeda: ' + trim(MaskEdit7.Text) + '-' + DbEdit13.Text ;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo : ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo: TITULOS A PAGAR'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo: TITULOS A RECEBER' ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Titulos em Cobradora: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Titulos em Cobradora: N�o' ;

  if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit4.Text)) <> ''))then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja : ' + trim(MaskEdit4.Text) + '-' + DbEdit4.Text ;

  try
      try
          {--visualiza o relat�rio--}
          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TfrmPosTituloAberto.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
    TipoAcesso : String;
begin
  inherited;

  case key of
    vk_F4 : begin

        TipoAcesso := #39 + 'L' + #39;
        if Trim(MaskEdit8.Text) <> '' then nPK := frmLookup_Padrao.ExecutaConsulta2(10, ' (EspTit.nCdGrupoEspTit = ' + MaskEdit8.Text + ') AND ' + 'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')' )
        else nPK := frmLookup_Padrao.ExecutaConsulta2(10, 'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')' );

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto.edtCdCobradoraExit(Sender: TObject);
begin
  inherited;

  qryCobradora.Close;
  PosicionaQuery(qryCobradora, edtCdCobradora.Text) ;
  
end;

procedure TfrmPosTituloAberto.edtCdCobradoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(199);

        If (nPK > 0) then
            edtCdCobradora.Text := intToStr(nPK) ;
    end ;

  end ;


end;

procedure TfrmPosTituloAberto.edtCdCobradoraChange(Sender: TObject);
begin
  inherited;

  if (trim(edtCdCobradora.Text) <> '') then
      RadioGroup2.ItemIndex := 0 ;
      
end;

procedure TfrmPosTituloAberto.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TfrmPosTituloAberto.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit4.Enabled := False ;
end;

procedure TfrmPosTituloAberto.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TfrmPosTituloAberto.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   nPk : integer;

begin
  inherited;

  case key of
    vk_F4 : begin
        nPK := frmLookup_Padrao.ExecutaConsulta(9);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
        end ;

    end ;

  end;
    
end;

procedure TfrmPosTituloAberto.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEspecieTitulo.Close;
  PosicionaQuery(qryGrupoEspecieTitulo,Maskedit8.Text);

end;

initialization
     RegisterClass(TfrmPosTituloAberto) ;

end.
