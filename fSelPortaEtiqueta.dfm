inherited frmSelPortaEtiqueta: TfrmSelPortaEtiqueta
  Left = 252
  Top = 294
  Width = 666
  Height = 209
  BorderIcons = [biSystemMenu]
  Caption = 'Selecionar Porta Etiqueta'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 650
    Height = 142
  end
  inherited ToolBar1: TToolBar
    Width = 650
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 32
    Width = 473
    Height = 137
    Caption = ' Modelo Etiqueta '
    TabOrder = 1
    object DBLookupListBox1: TDBLookupListBox
      Left = 2
      Top = 15
      Width = 469
      Height = 109
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Consolas'
      Font.Style = []
      KeyField = 'nCdModImpETP'
      ListField = 'cNmModImpETP'
      ListSource = dsModImpETP
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 480
    Top = 32
    Width = 169
    Height = 137
    Caption = ' Op'#231#245'es '
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 79
      Height = 13
      Caption = 'Porta Impress'#227'o'
    end
    object Label2: TLabel
      Left = 8
      Top = 72
      Width = 87
      Height = 13
      Caption = 'N'#250'mero de C'#243'pias'
    end
    object cmbPorta: TComboBox
      Left = 8
      Top = 40
      Width = 153
      Height = 21
      CharCase = ecUpperCase
      ItemHeight = 13
      MaxLength = 10
      TabOrder = 0
    end
    object edtQtdeCopias: TMaskEdit
      Left = 8
      Top = 88
      Width = 51
      Height = 21
      EditMask = '###;1; '
      MaxLength = 3
      TabOrder = 1
      Text = '1  '
    end
    object cxButton1: TcxButton
      Left = 64
      Top = 88
      Width = 97
      Height = 33
      Caption = 'Imprimir'
      TabOrder = 2
      OnClick = cxButton1Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D0020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDB0B0B0B6B6B6F4F4F4FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB4
        B4B4E0E0E0D8D8D8B6B6B6F4F4F4FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEEEEEEB7B7B7E2E2E2FDFDFDFDFDFDD9D9D9B6B6B6F4F4
        F4FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFEFEFEFBBBBBBE4E4E4FD
        FDFDF9F7F3000000FDFBF7D9D9D9B7B7B7F4F4F4FFFFFF000000FFFFFFFFFFFF
        FFFFFFF0F0F0BEBEBEE5E5E5FDFDFDF9F7F3000000F9F7F3000000F9F7F3D9D9
        D9B7B7B7F4F4F4000000FFFFFFFFFFFFF1F1F1C2C2C2E7E7E7FFFFFFFFFFFF00
        0000F9F7F3000000F9F7F3000000FDFCFBD9D9D9B7B7B7000000FFFFFFF2F2F2
        C5C5C5E8E8E8FDFDFDFFCC83FDCD88FFFFFF000000F9F7F3000000F9F7F3F9F7
        F3D7D7D7B5B5B5000000F2F2F2C9C9C9E9E9E9FDFDFDFFCC83FFCC83FFD498FF
        D79EFFFFFF000000F9F7F3FDFDFDD9D9D9B9B9B9F9F9F9000000CDCDCDEBEBEB
        FDFDFDFAFAFAFBF3E7FECE89FFD496FFD59AFFCF8BFFFFFFFDFDFDDADADABDBD
        BDF9F9F9FFFFFF000000CDCDCDFDFDFDFDFDFDFCFCFCF7F7F7FDF5EAFECF8AFF
        CC83FFCC83FDFDFDDCDCDCC0C0C0F9F9F9FFFFFFFFFFFF000000CECECEFDFDFD
        E0E0E0CBCBCBCECECEF7F7F7FBF3E8FFCC83FDFDFDDEDEDEC3C3C3FAFAFAFFFF
        FFFFFFFFFFFFFF000000D0D0D0FDFDFDCDCDCDFFFFFFD4D4D4F3F3F3FBFBFBFD
        FDFDE0E0E0C7C7C7FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF000000D2D2D2FDFDFD
        E2E2E2CECECEE0E0E0FDFDFDFDFDFDE2E2E2CBCBCBFAFAFAFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000D3D3D3FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDE4E4E4CD
        CDCDFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000D5D5D5D4D4D4
        D2D2D2D1D1D1D0D0D0CECECECDCDCDFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object qryModImpETP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTabTipoModEtiq'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ModImpETP'
      'WHERE nCdTabTipoModEtiq = :nCdTabTipoModEtiq'
      'ORDER BY cNmModImpETP')
    Left = 200
    Top = 136
    object qryModImpETPnCdModImpETP: TIntegerField
      FieldName = 'nCdModImpETP'
    end
    object qryModImpETPcNmModImpETP: TStringField
      FieldName = 'cNmModImpETP'
      Size = 50
    end
    object qryModImpETPiQtdeEtiquetas: TIntegerField
      FieldName = 'iQtdeEtiquetas'
    end
    object qryModImpETPiAvancoEtiqueta: TIntegerField
      FieldName = 'iAvancoEtiqueta'
    end
    object qryModImpETPiModelo: TIntegerField
      FieldName = 'iModelo'
    end
    object qryModImpETPiTemperatura: TIntegerField
      FieldName = 'iTemperatura'
    end
    object qryModImpETPcFlgLimpaMemoria: TIntegerField
      FieldName = 'cFlgLimpaMemoria'
    end
    object qryModImpETPcQuery: TMemoField
      FieldName = 'cQuery'
      BlobType = ftMemo
    end
    object qryModImpETPiEspacoEntreEtiquetas: TIntegerField
      FieldName = 'iEspacoEntreEtiquetas'
    end
  end
  object dsModImpETP: TDataSource
    DataSet = qryModImpETP
    Left = 280
    Top = 104
  end
  object qryConfigAmbiente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ConfigAmbiente '
      'WHERE cNmComputador = :cNmComputador')
    Left = 400
    Top = 104
    object qryConfigAmbientecPortaEtiqueta: TStringField
      FieldName = 'cPortaEtiqueta'
      Size = 10
    end
    object qryConfigAmbientecNmComputador: TStringField
      FieldName = 'cNmComputador'
      Size = 50
    end
    object qryConfigAmbientecPortaMatricial: TStringField
      FieldName = 'cPortaMatricial'
      Size = 100
    end
    object qryConfigAmbientecFlgUsaECF: TIntegerField
      FieldName = 'cFlgUsaECF'
    end
    object qryConfigAmbientecModeloECF: TStringField
      FieldName = 'cModeloECF'
    end
    object qryConfigAmbientecPortaECF: TStringField
      FieldName = 'cPortaECF'
      Size = 10
    end
    object qryConfigAmbientenCdTipoImpressoraPDV: TIntegerField
      FieldName = 'nCdTipoImpressoraPDV'
    end
    object qryConfigAmbienteiViasECF: TIntegerField
      FieldName = 'iViasECF'
    end
  end
end
