inherited frmConsultaChequeEmitido: TfrmConsultaChequeEmitido
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Consulta de Cheques Emitidos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 129
    Width = 1152
    Height = 685
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 100
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 33
      Top = 26
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Banc'#225'ria'
      FocusControl = DBEdit1
    end
    object Label6: TLabel
      Tag = 1
      Left = 196
      Top = 50
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label3: TLabel
      Tag = 1
      Left = 21
      Top = 50
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intervalo Emiss'#227'o'
    end
    object Label2: TLabel
      Tag = 1
      Left = 196
      Top = 74
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 20
      Top = 74
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Previs'#227'o Dep'#243'sito'
    end
    object MaskEdit1: TMaskEdit
      Left = 112
      Top = 18
      Width = 54
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit1Exit
      OnKeyDown = MaskEdit1KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 176
      Top = 18
      Width = 490
      Height = 21
      DataField = 'cNmContaBancaria'
      DataSource = DataSource1
      TabOrder = 6
    end
    object MaskEdit3: TMaskEdit
      Left = 216
      Top = 42
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object MaskEdit2: TMaskEdit
      Left = 112
      Top = 42
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object RadioGroup1: TRadioGroup
      Left = 304
      Top = 42
      Width = 309
      Height = 41
      Caption = 'Situa'#231#227'o do Cheque'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Emitidos'
        'Compensados'
        'Todos')
      TabOrder = 5
    end
    object MaskEdit5: TMaskEdit
      Left = 216
      Top = 66
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object MaskEdit4: TMaskEdit
      Left = 112
      Top = 66
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 129
    Width = 1152
    Height = 685
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 681
    ClientRectLeft = 4
    ClientRectRight = 1148
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Resultado Consulta'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1144
        Height = 657
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCheque
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footers = <>
            ReadOnly = True
            Width = 116
          end
          item
            EditButtons = <>
            FieldName = 'dDtDeposito'
            Footers = <>
            ReadOnly = True
            Width = 117
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footers = <>
            ReadOnly = True
            Width = 92
          end
          item
            EditButtons = <>
            FieldName = 'nValCheque'
            Footers = <>
            ReadOnly = True
            Width = 107
          end
          item
            EditButtons = <>
            FieldName = 'Status'
            Footers = <>
            Width = 132
          end
          item
            EditButtons = <>
            FieldName = 'cNmFavorecido'
            Footers = <>
            ReadOnly = True
            Width = 396
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF00107082005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C003E003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000018001C003000000038001C0030000
      0077C003FFFF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT Convert(VARCHAR(3),dbo.fn_ZeroEsquerda(ContaBancaria.nCdB' +
        'anco,3)) + '#39'-'#39' + Convert(VARCHAR(10),ContaBancaria.cAgencia) + '#39 +
        ' - '#39' + Convert(VARCHAR(15),ContaBancaria.nCdConta) AS cNmContaBa' +
        'ncaria'
      '      ,nCdContaBancaria'
      '  FROM ContaBancaria'
      ' WHERE nCdEmpresa       = :nCdEmpresa'
      '   AND nCdContaBancaria = :nPK'
      '   AND cFlgEmiteCheque  = 1'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               WHERE UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      '                 AND UCB.nCdUsuario = :nCdUsuario)'
      '')
    Left = 640
    Top = 72
    object qryContaBancariacNmContaBancaria: TStringField
      FieldName = 'cNmContaBancaria'
      ReadOnly = True
      Size = 38
    end
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 424
    Top = 240
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 672
    Top = 72
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicialDep'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinalDep'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'cStatus'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdContaBancaria'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial    varchar(10)'
      '       ,@dDtFinal      varchar(10)'
      '       ,@cStatus       char(1)'
      '       ,@dDtInicialDep varchar(10)'
      '       ,@dDtFinalDep   varchar(10)'
      ''
      'Set @dDtInicial    = :dDtInicial'
      'Set @dDtFinal      = :dDtFinal'
      'Set @dDtInicialDep = :dDtInicialDep'
      'Set @dDtFinalDep   = :dDtFinalDep'
      'Set @cStatus       = :cStatus'
      ''
      'SELECT dDtDeposito'
      '      ,iNrCheque'
      '      ,nValCheque'
      '      ,dDtEmissao'
      '      ,cNmFavorecido'
      '      ,CASE WHEN cFlgCompensado = 0 THEN '#39'EMITIDO'#39
      '            ELSE '#39'COMPENSADO'#39
      '       END as Status'
      '  FROM Cheque'
      ' WHERE nCdTabTipoCheque = 1'
      '   AND nCdContaBancaria = :nCdContaBancaria'
      
        '   AND ((@cStatus = '#39'T'#39') OR (   (@cStatus = '#39'E'#39' AND cFlgCompensa' +
        'do = 0)'
      
        '                             OR (@cStatus = '#39'C'#39' AND cFlgCompensa' +
        'do = 1)'
      '                            ))'
      
        '   AND ((dDtEmissao >= Convert(DATETIME,@dDtInicial,103))     OR' +
        ' (@dDtInicial    = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtEmissao <  Convert(DATETIME,@dDtFinal,103)+1)     OR' +
        ' (@dDtFinal      = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtDeposito >= Convert(DATETIME,@dDtInicialDep,103)) OR' +
        ' (@dDtInicialDep = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtDeposito <  Convert(DATETIME,@dDtFinalDep,103)+1) OR' +
        ' (@dDtFinalDep   = '#39'01/01/1900'#39'))'
      ''
      ''
      ''
      '')
    Left = 704
    Top = 72
    object qryChequedDtEmissao: TDateTimeField
      DisplayLabel = 'Resultado|Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryChequedDtDeposito: TDateTimeField
      DisplayLabel = 'Resultado|Bom para'
      FieldName = 'dDtDeposito'
    end
    object qryChequeiNrCheque: TIntegerField
      DisplayLabel = 'Resultado|Nr Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequenValCheque: TBCDField
      DisplayLabel = 'Resultado|Valor'
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequeStatus: TStringField
      DisplayLabel = 'Resultado|Status'
      FieldName = 'Status'
      ReadOnly = True
      Size = 10
    end
    object qryChequecNmFavorecido: TStringField
      DisplayLabel = 'Resultado|Favorecido'
      FieldName = 'cNmFavorecido'
      Size = 50
    end
  end
  object dsCheque: TDataSource
    DataSet = qryCheque
    Left = 488
    Top = 240
  end
end
