unit fCancelaBorderoRecebimento_titulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmCancelaBorderoRecebimento_titulo = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulosRemessa: TADOQuery;
    qryTitulosRemessanCdTitulo: TIntegerField;
    qryTitulosRemessacNrTit: TStringField;
    qryTitulosRemessacNrNF: TStringField;
    qryTitulosRemessadDtVenc: TDateTimeField;
    qryTitulosRemessaiParcela: TIntegerField;
    qryTitulosRemessanCdTerceiro: TIntegerField;
    qryTitulosRemessacNmTerceiro: TStringField;
    qryTitulosRemessanSaldoTit: TBCDField;
    qryTitulosRemessacDiretorioArquivoRemessa: TStringField;
    dsTitulosRemessa: TDataSource;
    SP_CANCELA_BORDERO_RECEBIMENTO: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdBorderoFinanceiro : integer;
  end;

var
  frmCancelaBorderoRecebimento_titulo: TfrmCancelaBorderoRecebimento_titulo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCancelaBorderoRecebimento_titulo.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (MessageDLG('Esta a��o n�o poder� ser desfeita. Deseja realmente cancelar esta Remessa ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  frmMenu.Connection.BeginTrans;

  try
      SP_CANCELA_BORDERO_RECEBIMENTO.Close;
      SP_CANCELA_BORDERO_RECEBIMENTO.Parameters.ParamByName('@nCdBorderoFinanceiro').Value := nCdBorderoFinanceiro;
      SP_CANCELA_BORDERO_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value           := frmMenu.nCdUsuarioLogado;
      SP_CANCELA_BORDERO_RECEBIMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Remessa Cancelada com Sucesso.');

  close;

end;

end.
