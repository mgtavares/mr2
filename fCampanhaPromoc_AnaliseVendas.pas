unit fCampanhaPromoc_AnaliseVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmCampanhaPromoc_AnaliseVendas = class(TfrmProcesso_Padrao)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryVendas: TADOQuery;
    dsVendas: TDataSource;
    qryVendasnCdItemPedido: TAutoIncField;
    qryVendasnCdPedido: TIntegerField;
    qryVendascNmLoja: TStringField;
    qryVendasdDtPedido: TDateTimeField;
    qryVendasnCdProduto: TIntegerField;
    qryVendascNmItem: TStringField;
    qryVendasnQtdeExpRec: TBCDField;
    qryVendasnValUnitario: TBCDField;
    qryVendasnValDesconto: TBCDField;
    qryVendasnValAcrescimo: TBCDField;
    qryVendasnValCustoUnit: TBCDField;
    qryVendasnValTotalItem: TBCDField;
    qryVendasnValCMV: TBCDField;
    qryVendasnMargemBruta: TBCDField;
    qryVendascNmDepartamento: TStringField;
    qryVendascNmCategoria: TStringField;
    qryVendascNmSubCategoria: TStringField;
    qryVendascNmSegmento: TStringField;
    qryVendascNmLinha: TStringField;
    qryVendascNmMarca: TStringField;
    cxGridDBTableView1nCdItemPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmLoja: TcxGridDBColumn;
    cxGridDBTableView1dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmItem: TcxGridDBColumn;
    cxGridDBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGridDBTableView1nValUnitario: TcxGridDBColumn;
    cxGridDBTableView1nValDesconto: TcxGridDBColumn;
    cxGridDBTableView1nValAcrescimo: TcxGridDBColumn;
    cxGridDBTableView1nValCustoUnit: TcxGridDBColumn;
    cxGridDBTableView1nValTotalItem: TcxGridDBColumn;
    cxGridDBTableView1nValCMV: TcxGridDBColumn;
    cxGridDBTableView1nMargemBruta: TcxGridDBColumn;
    cxGridDBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView1cNmCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSegmento: TcxGridDBColumn;
    cxGridDBTableView1cNmLinha: TcxGridDBColumn;
    cxGridDBTableView1cNmMarca: TcxGridDBColumn;
    qryVendascNmTerceiro: TStringField;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    qryVendasnValTotalBruto: TBCDField;
    qryVendasnMargemReal: TBCDField;
    cxGridDBTableView1nValTotalBruto: TcxGridDBColumn;
    cxGridDBTableView1nMargemReal: TcxGridDBColumn;
    qryVendasnCdLanctoFin: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCampanhaPromoc_AnaliseVendas: TfrmCampanhaPromoc_AnaliseVendas;

implementation

uses fConsultaVendaProduto_Dados;

{$R *.dfm}

procedure TfrmCampanhaPromoc_AnaliseVendas.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  qryVendas.Close ;
  qryVendas.Open ;
end;

procedure TfrmCampanhaPromoc_AnaliseVendas.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryVendas.Eof then
  begin

      PosicionaQuery(frmConsultaVendaProduto_Dados.qryItemPedido, qryVendasnCdLanctoFin.AsString) ;
      PosicionaQuery(frmConsultaVendaProduto_Dados.qryCondicao, qryVendasnCdLanctoFin.AsString) ;
      PosicionaQuery(frmConsultaVendaProduto_Dados.qryPrestacoes, qryVendasnCdLanctoFin.AsString) ;
      PosicionaQuery(frmConsultaVendaProduto_Dados.qryCheques, qryVendasnCdLanctoFin.AsString) ;
      frmConsultaVendaProduto_Dados.ShowModal;

  end ;
end;

end.
