inherited rptBorderoCaixa: TrptBorderoCaixa
  Left = 417
  Top = 269
  Width = 731
  Height = 257
  Caption = 'Border'#244' Caixa'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 715
    Height = 190
  end
  object Label5: TLabel [1]
    Left = 80
    Top = 48
    Width = 20
    Height = 13
    Caption = 'Loja'
  end
  object Label6: TLabel [2]
    Left = 188
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [3]
    Left = 9
    Top = 96
    Width = 91
    Height = 13
    Caption = 'Per'#237'odo Movimento'
  end
  object Label1: TLabel [4]
    Left = 41
    Top = 72
    Width = 59
    Height = 13
    Caption = 'Conta Caixa'
  end
  inherited ToolBar1: TToolBar
    Width = 715
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit6: TMaskEdit [6]
    Left = 104
    Top = 40
    Width = 60
    Height = 21
    Enabled = False
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
  end
  object MaskEdit2: TMaskEdit [7]
    Left = 216
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit1: TMaskEdit [8]
    Left = 104
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [9]
    Left = 104
    Top = 64
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 168
    Top = 40
    Width = 529
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 168
    Top = 64
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource2
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 408
    Top = 160
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 344
    Top = 128
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdLoja   = :nCdLoja'
      '   AND cFlgCaixa = 1'
      '   AND nCdContaBancaria = :nPK')
    Left = 376
    Top = 128
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 344
    Top = 160
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancaria
    Left = 376
    Top = 160
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 408
    Top = 128
  end
end
