unit fItensPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmItensPedido = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItens: TADOQuery;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtdePed: TBCDField;
    qryItensnValUnitario: TBCDField;
    qryItensnPercDescontoItem: TBCDField;
    qryItensnPercIPI: TBCDField;
    qryItensnValCustoUnit: TBCDField;
    qryItensnValTotal: TBCDField;
    qryItensnValSugVenda: TBCDField;
    dsItens: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmItensPedido: TfrmItensPedido;

implementation

{$R *.dfm}

end.
