unit fConsultaItemPedidoAberto_SomenteItens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmConsultaItemPedidoAberto_SomenteItens = class(TfrmProcesso_Padrao)
    qryItemPedido: TADOQuery;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidocNmTipoItemPed: TStringField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtde: TBCDField;
    qryItemPedidocSiglaUnidadeMedida: TStringField;
    DataSource2: TDataSource;
    DBGridEh2: TDBGridEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaItemPedidoAberto_SomenteItens: TfrmConsultaItemPedidoAberto_SomenteItens;

implementation

{$R *.dfm}

end.
