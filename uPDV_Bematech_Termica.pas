unit uPDV_Bematech_Termica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WinSpool;


procedure Imprime_Termica_Bematech_AbrePorta(cNomeDLL: string ; cPorta : string);
procedure Imprime_Termica_Bematech_ImprimeLinha(cTexto : string);
procedure Imprime_Termica_Bematech_ImprimeBarra(cTexto : string);
procedure Imprime_Termica_Bematech_FechaPorta;
procedure Imprime_Termica_Bematech_Negrito(cTexto : string) ;

var
  DLLInstance : THandle;
  iRetorno    : integer ;

implementation

procedure Imprime_Termica_Bematech_Negrito(cTexto : string) ;
type
    TFormataTX = function(BufTras: string; TpoLtra: integer; Italic: integer; Sublin: integer; expand: integer; enfat: integer ): integer; stdcall;
var
    FormataTX : TFormataTX ;
begin

  @FormataTX := GetProcAddress(DLLInstance, 'FormataTX');

  if @FormataTX <> nil then
    iretorno := FormataTX(pchar(cTexto),3,0,0,1,0) 
  else
    MessageDlg('Incapaz de localizar o procedimento. FormataTX',
         mtError, [mbOK], 0);

end ;

procedure Imprime_Termica_Bematech_AbrePorta(cNomeDLL: string ; cPorta : string);
type
  TIniciaPorta                = function( Porta: string): integer; stdcall;
  TLe_Status                  = function: integer; stdcall;
var
  IniciaPorta                : TIniciaPorta;
  Le_Status                  : TLe_Status ;
begin
  DLLInstance := LoadLibrary(pChar(cNomeDLL));


  if DLLInstance = 0 then begin
    MessageDlg('Incapaz de carregar a DLL:'+cNomeDLL, mtError, [mbOK], 0);
    Exit;
  end;


  @IniciaPorta := GetProcAddress(DLLInstance, 'IniciaPorta');
  if @IniciaPorta <> nil then
    iretorno := IniciaPorta(pchar(cPorta))
  else
    MessageDlg('Incapaz de localizar o procedimento. IniciaPorta',
         mtError, [mbOK], 0);

  @Le_Status  := GetProcAddress(DLLInstance, 'Le_Status');
  if @Le_Status <> nil then
    iretorno := Le_Status
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  if (iRetorno = 79) then
  begin
      ShowMessage('Impressora Off-Line ou Desconectada.') ;
      exit ;
  end ;

  if (iRetorno = 40) then
  begin
      ShowMessage('Impressora sem papel') ;
      exit ;
  end ;

  if (iRetorno = 48) then
  begin
      ShowMessage('Impressora com pouco papel') ;
  end ;

  if (iRetorno = 32) then
  begin
      ShowMessage('Impressora com pouco papel e Off-Line') ;
      exit ;
  end ;

  if (iRetorno = 128) then
  begin
      ShowMessage('Impressora aberta ou n�o preparada') ;
      exit ;
  end ;

  if (iRetorno = 0) then
  begin
      ShowMessage('Impressora Desligado ou Cabo Desconectado') ;
      exit ;
  end ;

end;

procedure Imprime_Termica_Bematech_ImprimeLinha(cTexto : string);
type
  TBematechTX                 = function( BufTrans: string ): integer ; stdcall;
var
  BematechTX                 : TBematechTX ;
begin
  @BematechTX := GetProcAddress(DLLInstance, 'BematechTX');

  cTexto   := cTexto + #10;

  if @BematechTX <> nil then
    iretorno := BematechTX(pchar(cTexto))
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

end;

procedure Imprime_Termica_Bematech_ImprimeBarra(cTexto : string);
type
  TImprimeCodigoBarrasCODE39  = function ( Codigo: string ): integer; stdcall;
var
  ImprimeCodigoBarrasCODE39  : TImprimeCodigoBarrasCODE39 ;
begin
  @ImprimeCodigoBarrasCODE39 := GetProcAddress(DLLInstance, 'ImprimeCodigoBarrasCODE39') ;

  if @ImprimeCodigoBarrasCODE39 <> nil then
      iRetorno := ImprimeCodigoBarrasCODE39(pchar(Copy(cTexto,1,9))) ;

end;

procedure Imprime_Termica_Bematech_FechaPorta;
type
  TFechaPorta                 = function: integer ; stdcall;
  TAcionaGuilhotina           = function( Modo: integer ): integer; stdcall;
var
  FechaPorta                 : TFechaPorta ;
  AcionaGuilhotina           : TAcionaGuilhotina ;
begin
  @FechaPorta := GetProcAddress(DLLInstance, 'FechaPorta');
  if @FechaPorta <> nil then
    iretorno := FechaPorta
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  @AcionaGuilhotina := GetProcAddress(DLLInstance, 'AcionaGuilhotina');
  if @FechaPorta <> nil then
      iRetorno := AcionaGuilhotina(0)
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  FreeLibrary(DLLInstance);
end;


{
procedure Imprime_Termica_Bematech_ImprimeLinha(cTexto : string);
type
  TIniciaPorta                = function( Porta: string): integer; stdcall;
  TBematechTX                 = function( BufTrans: string ): integer ; stdcall;
  TFechaPorta                 = function: integer ; stdcall;
  TAcionaGuilhotina           = function( Modo: integer ): integer; stdcall;
  TConfiguraModeloImpressora  = function( ModeloImpressora: integer ): integer; stdcall;
  TImprimeCodigoBarrasCODE39  = function ( Codigo: string ): integer; stdcall;
  TLe_Status                  = function: integer; stdcall;
var
  sTexto      : string ;
  iRetorno    : integer ;

  IniciaPorta                : TIniciaPorta;
  BematechTX                 : TBematechTX ;
  FechaPorta                 : TFechaPorta ;
  AcionaGuilhotina           : TAcionaGuilhotina ;
  ConfiguraModeloImpressora  : TConfiguraModeloImpressora;
  ImprimeCodigoBarrasCODE39  : TImprimeCodigoBarrasCODE39 ;
  Le_Status                  : TLe_Status ;
begin
  DLLInstance := LoadLibrary('MP2032.DLL');

  if DLLInstance = 0 then begin
    MessageDlg('Incapaz de carregar a DLL.', mtError, [mbOK], 0);
    Exit;
  end;


  @IniciaPorta := GetProcAddress(DLLInstance, 'cPorta');
  if @IniciaPorta <> nil then
    iretorno := IniciaPorta(pchar('USB'))
  else
    MessageDlg('Incapaz de localizar o procedimento. IniciaPorta',
         mtError, [mbOK], 0);

  @Le_Status  := GetProcAddress(DLLInstance, 'Le_Status');
  if @Le_Status <> nil then
    iretorno := Le_Status
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  if (iRetorno = 79) then
  begin
      ShowMessage('Impressora Off-Line ou Desconectada.') ;
      exit ;
  end ;

  if (iRetorno = 40) then
  begin
      ShowMessage('Impressora sem papel') ;
      exit ;
  end ;

  if (iRetorno = 48) then
  begin
      ShowMessage('Impressora com pouco papel') ;
  end ;

  if (iRetorno = 32) then
  begin
      ShowMessage('Impressora com pouco papel e Off-Line') ;
      exit ;
  end ;

  if (iRetorno = 128) then
  begin
      ShowMessage('Impressora aberta ou n�o preparada') ;
      exit ;
  end ;

  if (iRetorno = 0) then
  begin
      ShowMessage('Impressora Desligado ou Cabo Desconectado') ;
      exit ;
  end ;

  @BematechTX := GetProcAddress(DLLInstance, 'BematechTX');

  sTexto   := 'Total bruto: 12.500,00' + #10;
  sTexto   := sTexto + 'Total l�quido: 9.600,00' + #10;

  if @BematechTX <> nil then
    iretorno := BematechTX(pchar(sTexto))
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  @ImprimeCodigoBarrasCODE39 := GetProcAddress(DLLInstance, 'ImprimeCodigoBarrasCODE39') ;

  if @ImprimeCodigoBarrasCODE39 <> nil then
      iRetorno := ImprimeCodigoBarrasCODE39(pchar('000001423')) ;

  @FechaPorta := GetProcAddress(DLLInstance, 'FechaPorta');
  if @FechaPorta <> nil then
    iretorno := FechaPorta
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  @AcionaGuilhotina := GetProcAddress(DLLInstance, 'AcionaGuilhotina');
  if @FechaPorta <> nil then
      iRetorno := AcionaGuilhotina(0)
  else
    MessageDlg('Incapaz de localizar o procedimento.',
         mtError, [mbOK], 0);

  FreeLibrary(DLLInstance);
end;
}

end.
