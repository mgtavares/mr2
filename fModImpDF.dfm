inherited frmModImpDF: TfrmModImpDF
  Left = 88
  Top = 32
  Width = 1242
  Height = 647
  Caption = 'Modelo Impress'#227'o Documento Fiscal'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1226
    Height = 584
  end
  object Label1: TLabel [1]
    Left = 48
    Top = 38
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 13
    Top = 62
    Width = 73
    Height = 13
    Caption = 'Nome Modelo'
    FocusControl = DBEdit2
  end
  object Label5: TLabel [3]
    Left = 278
    Top = 85
    Width = 33
    Height = 13
    Caption = 'Linhas'
    FocusControl = DBEdit4
  end
  object Label13: TLabel [4]
    Left = 332
    Top = 85
    Width = 42
    Height = 13
    Caption = 'Colunas'
    FocusControl = DBEdit4
  end
  object Label10: TLabel [5]
    Left = 386
    Top = 85
    Width = 36
    Height = 13
    Caption = 'Espa'#231'o'
    FocusControl = DBEdit4
  end
  object Label14: TLabel [6]
    Left = 440
    Top = 85
    Width = 32
    Height = 13
    Caption = 'Buffer'
    FocusControl = DBEdit4
  end
  object Label8: TLabel [7]
    Left = 495
    Top = 85
    Width = 92
    Height = 13
    Caption = 'P'#225'gina de C'#243'digo'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    Width = 1226
    TabOrder = 11
    inherited ToolButton9: TToolButton
      Visible = False
    end
    object ToolButton3: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 864
      Top = 0
      Caption = '&Op'#231#245'es'
      DropdownMenu = menuOpcao
      ImageIndex = 8
    end
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 88
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdModImpDF'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [10]
    Left = 88
    Top = 56
    Width = 657
    Height = 19
    DataField = 'cNmModImpDF'
    DataSource = dsMaster
    TabOrder = 1
  end
  object cxPageControl1: TcxPageControl [11]
    Left = 8
    Top = 144
    Width = 1209
    Height = 457
    ActivePage = tabCabecalho
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    LookAndFeel.NativeStyle = True
    ParentFont = False
    TabOrder = 9
    ClientRectBottom = 453
    ClientRectLeft = 4
    ClientRectRight = 1205
    ClientRectTop = 24
    object tabCabecalho: TcxTabSheet
      Caption = 'Ca&be'#231'alho'
      ImageIndex = 0
      OnEnter = tabCabecalhoEnter
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 20
        Height = 13
        Caption = 'SQL'
        FocusControl = DBMemo1
      end
      object Label6: TLabel
        Left = 0
        Top = 0
        Width = 20
        Height = 13
        Caption = 'SQL'
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 600
        Height = 429
        Align = alLeft
        DataField = 'cQueryHeader'
        DataSource = dsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 600
        Top = 0
        Width = 601
        Height = 429
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object DBGridEh1: TDBGridEh
          Left = 0
          Top = 0
          Width = 601
          Height = 360
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsCampoC
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'Segoe UI'
          FooterFont.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Segoe UI'
          TitleFont.Style = []
          UseMultiTitle = True
          OnEnter = DBGridEh1Enter
          Columns = <
            item
              EditButtons = <>
              FieldName = 'nCdCampoModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'nCdModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'cParte'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'iSequencia'
              Footers = <>
              Title.Caption = 'Campo|Seq.'
              Width = 43
            end
            item
              EditButtons = <>
              FieldName = 'cNmCampo'
              Footers = <>
              Width = 121
            end
            item
              EditButtons = <>
              FieldName = 'cDescricao'
              Footers = <>
              Width = 167
            end
            item
              EditButtons = <>
              FieldName = 'cTipoDado'
              Footers = <>
              KeyList.Strings = (
                'C'
                'I'
                'V')
              PickList.Strings = (
                'Caracter'
                'Inteiro'
                'Valor')
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'iLinha'
              Footers = <>
              Width = 22
            end
            item
              EditButtons = <>
              FieldName = 'iColuna'
              Footers = <>
              Width = 24
            end
            item
              EditButtons = <>
              FieldName = 'cTamanhoFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'EXPANDIDO'
                'COMP12'
                'COMP17'
                'COMP20')
              Width = 57
            end
            item
              EditButtons = <>
              FieldName = 'cEstiloFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'ITALICO'
                'NEGRITO')
            end
            item
              EditButtons = <>
              FieldName = 'cLPP'
              Footers = <>
              PickList.Strings = (
                'SEIS'
                'OITO')
            end
            item
              EditButtons = <>
              FieldName = 'cTagFormatacao'
              Footers = <>
              Title.Caption = 'Tag'#39's de Formata'#231#227'o do Campo (@@value : subs. valor campo)'
              Width = 365
            end>
          object RowDetailData: TRowDetailPanelControlEh
            object DBRadioGroup1: TDBRadioGroup
              Left = -88
              Top = -48
              Width = 185
              Height = 105
              Caption = 'DBRadioGroup1'
              TabOrder = 0
            end
          end
        end
        object cxPageControl2: TcxPageControl
          Left = 0
          Top = 360
          Width = 601
          Height = 69
          ActivePage = tabTags
          Align = alBottom
          Images = ImageList1
          LookAndFeel.NativeStyle = True
          TabOrder = 1
          DragCursor = crHelp
          OnEnter = cxPageControl2Enter
          OnExit = cxPageControl2Exit
          ClientRectBottom = 65
          ClientRectLeft = 4
          ClientRectRight = 597
          ClientRectTop = 25
          object tabTags: TcxTabSheet
            Caption = 'Tags'
            ImageIndex = 9
            object DBGridEh1_: TDBGridEh
              Tag = 2
              Left = 0
              Top = 0
              Width = 593
              Height = 40
              Align = alClient
              DataGrouping.GroupLevels = <>
              DataSource = dsTags
              Flat = True
              FooterColor = clWindow
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'Segoe UI'
              FooterFont.Style = []
              IndicatorOptions = [gioShowRowIndicatorEh]
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Segoe UI'
              TitleFont.Style = []
              UseMultiTitle = True
              OnEnter = DBGridEh1Enter
              Columns = <
                item
                  EditButtons = <>
                  FieldName = 'cTag'
                  Footers = <>
                  Title.Caption = 'Tag'
                  Width = 110
                end
                item
                  EditButtons = <>
                  FieldName = 'cDescricao'
                  Footers = <>
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 430
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object DBRadioGroup2: TDBRadioGroup
                  Left = -88
                  Top = -48
                  Width = 185
                  Height = 105
                  Caption = 'DBRadioGroup1'
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
    end
    object tabFatura: TcxTabSheet
      Caption = '&Fatura'
      ImageIndex = 1
      OnEnter = tabFaturaEnter
      object Label4: TLabel
        Left = 0
        Top = 0
        Width = 20
        Height = 13
        Caption = 'SQL'
        FocusControl = DBMemo2
      end
      object DBMemo2: TDBMemo
        Left = 0
        Top = 0
        Width = 600
        Height = 429
        Align = alLeft
        DataField = 'cQueryFatura'
        DataSource = dsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 600
        Top = 0
        Width = 601
        Height = 429
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object cxPageControl3: TcxPageControl
          Left = 0
          Top = 360
          Width = 601
          Height = 69
          ActivePage = cxTabSheet1
          Align = alBottom
          Images = ImageList1
          LookAndFeel.NativeStyle = True
          TabOrder = 0
          DragCursor = crHelp
          OnEnter = cxPageControl2Enter
          OnExit = cxPageControl2Exit
          ClientRectBottom = 65
          ClientRectLeft = 4
          ClientRectRight = 597
          ClientRectTop = 25
          object cxTabSheet1: TcxTabSheet
            Caption = 'Tags'
            ImageIndex = 9
            object DBGridEh2_: TDBGridEh
              Tag = 2
              Left = 0
              Top = 0
              Width = 593
              Height = 40
              Align = alClient
              DataGrouping.GroupLevels = <>
              DataSource = dsTags
              Flat = True
              FooterColor = clWindow
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'Segoe UI'
              FooterFont.Style = []
              IndicatorOptions = [gioShowRowIndicatorEh]
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Segoe UI'
              TitleFont.Style = []
              UseMultiTitle = True
              OnEnter = DBGridEh1Enter
              Columns = <
                item
                  EditButtons = <>
                  FieldName = 'cTag'
                  Footers = <>
                  Title.Caption = 'Tag'
                  Width = 110
                end
                item
                  EditButtons = <>
                  FieldName = 'cDescricao'
                  Footers = <>
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 430
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object DBRadioGroup4: TDBRadioGroup
                  Left = -88
                  Top = -48
                  Width = 185
                  Height = 105
                  Caption = 'DBRadioGroup1'
                  TabOrder = 0
                end
              end
            end
          end
        end
        object DBGridEh2: TDBGridEh
          Left = 0
          Top = 0
          Width = 601
          Height = 360
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsCampoC
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'Segoe UI'
          FooterFont.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Segoe UI'
          TitleFont.Style = []
          UseMultiTitle = True
          OnEnter = DBGridEh1Enter
          Columns = <
            item
              EditButtons = <>
              FieldName = 'nCdCampoModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'nCdModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'cParte'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'iSequencia'
              Footers = <>
              Title.Caption = 'Campo|Seq.'
              Width = 43
            end
            item
              EditButtons = <>
              FieldName = 'cNmCampo'
              Footers = <>
              Width = 121
            end
            item
              EditButtons = <>
              FieldName = 'cDescricao'
              Footers = <>
              Width = 167
            end
            item
              EditButtons = <>
              FieldName = 'cTipoDado'
              Footers = <>
              KeyList.Strings = (
                'C'
                'I'
                'V')
              PickList.Strings = (
                'Caracter'
                'Inteiro'
                'Valor')
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'iLinha'
              Footers = <>
              Width = 22
            end
            item
              EditButtons = <>
              FieldName = 'iColuna'
              Footers = <>
              Width = 24
            end
            item
              EditButtons = <>
              FieldName = 'cTamanhoFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'EXPANDIDO'
                'COMP12'
                'COMP17'
                'COMP20')
              Width = 57
            end
            item
              EditButtons = <>
              FieldName = 'cEstiloFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'ITALICO'
                'NEGRITO')
            end
            item
              EditButtons = <>
              FieldName = 'cLPP'
              Footers = <>
              PickList.Strings = (
                'SEIS'
                'OITO')
            end
            item
              EditButtons = <>
              FieldName = 'cTagFormatacao'
              Footers = <>
              Title.Caption = 'Tag'#39's de Formata'#231#227'o do Campo (@@value : subs. valor campo)'
              Width = 365
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
    object tabItem: TcxTabSheet
      Caption = '&Itens'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      OnEnter = tabItemEnter
      object DBMemo3: TDBMemo
        Left = 0
        Top = 0
        Width = 600
        Height = 429
        Align = alLeft
        DataField = 'cQueryDetalhe'
        DataSource = dsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Panel3: TPanel
        Left = 600
        Top = 0
        Width = 601
        Height = 429
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object cxPageControl4: TcxPageControl
          Left = 0
          Top = 360
          Width = 601
          Height = 69
          ActivePage = cxTabSheet2
          Align = alBottom
          Images = ImageList1
          LookAndFeel.NativeStyle = True
          TabOrder = 0
          DragCursor = crHelp
          OnEnter = cxPageControl2Enter
          OnExit = cxPageControl2Exit
          ClientRectBottom = 65
          ClientRectLeft = 4
          ClientRectRight = 597
          ClientRectTop = 25
          object cxTabSheet2: TcxTabSheet
            Caption = 'Tags'
            ImageIndex = 9
            object DBGridEh3_: TDBGridEh
              Tag = 2
              Left = 0
              Top = 0
              Width = 593
              Height = 40
              Align = alClient
              DataGrouping.GroupLevels = <>
              DataSource = dsTags
              Flat = True
              FooterColor = clWindow
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'Segoe UI'
              FooterFont.Style = []
              IndicatorOptions = [gioShowRowIndicatorEh]
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Segoe UI'
              TitleFont.Style = []
              UseMultiTitle = True
              OnEnter = DBGridEh1Enter
              Columns = <
                item
                  EditButtons = <>
                  FieldName = 'cTag'
                  Footers = <>
                  Title.Caption = 'Tag'
                  Width = 110
                end
                item
                  EditButtons = <>
                  FieldName = 'cDescricao'
                  Footers = <>
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 430
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object DBRadioGroup3: TDBRadioGroup
                  Left = -88
                  Top = -48
                  Width = 185
                  Height = 105
                  Caption = 'DBRadioGroup1'
                  TabOrder = 0
                end
              end
            end
          end
        end
        object DBGridEh3: TDBGridEh
          Left = 0
          Top = 0
          Width = 601
          Height = 360
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsCampoC
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'Segoe UI'
          FooterFont.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Segoe UI'
          TitleFont.Style = []
          UseMultiTitle = True
          OnEnter = DBGridEh1Enter
          Columns = <
            item
              EditButtons = <>
              FieldName = 'nCdCampoModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'nCdModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'cParte'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'iSequencia'
              Footers = <>
              Title.Caption = 'Campo|Seq.'
              Width = 43
            end
            item
              EditButtons = <>
              FieldName = 'cNmCampo'
              Footers = <>
              Width = 121
            end
            item
              EditButtons = <>
              FieldName = 'cDescricao'
              Footers = <>
              Width = 167
            end
            item
              EditButtons = <>
              FieldName = 'cTipoDado'
              Footers = <>
              KeyList.Strings = (
                'C'
                'I'
                'V')
              PickList.Strings = (
                'Caracter'
                'Inteiro'
                'Valor')
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'iLinha'
              Footers = <>
              Width = 22
            end
            item
              EditButtons = <>
              FieldName = 'iColuna'
              Footers = <>
              Width = 24
            end
            item
              EditButtons = <>
              FieldName = 'cTamanhoFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'EXPANDIDO'
                'COMP12'
                'COMP17'
                'COMP20')
              Width = 57
            end
            item
              EditButtons = <>
              FieldName = 'cEstiloFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'ITALICO'
                'NEGRITO')
            end
            item
              EditButtons = <>
              FieldName = 'cLPP'
              Footers = <>
              PickList.Strings = (
                'SEIS'
                'OITO')
            end
            item
              EditButtons = <>
              FieldName = 'cTagFormatacao'
              Footers = <>
              Title.Caption = 'Tag'#39's de Formata'#231#227'o do Campo (@@value : subs. valor campo)'
              Width = 365
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
    object tabMensagem: TcxTabSheet
      Caption = '&Mensagens'
      ImageIndex = 3
      OnEnter = tabMensagemEnter
      object Label7: TLabel
        Left = 0
        Top = 0
        Width = 20
        Height = 13
        Caption = 'SQL'
        FocusControl = DBMemo4
      end
      object DBMemo4: TDBMemo
        Left = 0
        Top = 0
        Width = 600
        Height = 429
        Align = alLeft
        DataField = 'cQueryMensagem'
        DataSource = dsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Panel4: TPanel
        Left = 600
        Top = 0
        Width = 601
        Height = 429
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object cxPageControl5: TcxPageControl
          Left = 0
          Top = 360
          Width = 601
          Height = 69
          ActivePage = cxTabSheet3
          Align = alBottom
          Images = ImageList1
          LookAndFeel.NativeStyle = True
          TabOrder = 0
          DragCursor = crHelp
          OnEnter = cxPageControl2Enter
          OnExit = cxPageControl2Exit
          ClientRectBottom = 65
          ClientRectLeft = 4
          ClientRectRight = 597
          ClientRectTop = 25
          object cxTabSheet3: TcxTabSheet
            Caption = 'Tags'
            ImageIndex = 9
            object DBGridEh4_: TDBGridEh
              Tag = 2
              Left = 0
              Top = 0
              Width = 593
              Height = 40
              Align = alClient
              DataGrouping.GroupLevels = <>
              DataSource = dsTags
              Flat = True
              FooterColor = clWindow
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'Segoe UI'
              FooterFont.Style = []
              IndicatorOptions = [gioShowRowIndicatorEh]
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Segoe UI'
              TitleFont.Style = []
              UseMultiTitle = True
              OnEnter = DBGridEh1Enter
              Columns = <
                item
                  EditButtons = <>
                  FieldName = 'cTag'
                  Footers = <>
                  Title.Caption = 'Tag'
                  Width = 110
                end
                item
                  EditButtons = <>
                  FieldName = 'cDescricao'
                  Footers = <>
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 430
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object DBRadioGroup5: TDBRadioGroup
                  Left = -88
                  Top = -48
                  Width = 185
                  Height = 105
                  Caption = 'DBRadioGroup1'
                  TabOrder = 0
                end
              end
            end
          end
        end
        object DBGridEh4: TDBGridEh
          Left = 0
          Top = 0
          Width = 601
          Height = 360
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsCampoC
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'Segoe UI'
          FooterFont.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Segoe UI'
          TitleFont.Style = []
          UseMultiTitle = True
          OnEnter = DBGridEh1Enter
          Columns = <
            item
              EditButtons = <>
              FieldName = 'nCdCampoModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'nCdModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'cParte'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'iSequencia'
              Footers = <>
              Title.Caption = 'Campo|Seq.'
              Width = 43
            end
            item
              EditButtons = <>
              FieldName = 'cNmCampo'
              Footers = <>
              Width = 121
            end
            item
              EditButtons = <>
              FieldName = 'cDescricao'
              Footers = <>
              Width = 167
            end
            item
              EditButtons = <>
              FieldName = 'cTipoDado'
              Footers = <>
              KeyList.Strings = (
                'C'
                'I'
                'V')
              PickList.Strings = (
                'Caracter'
                'Inteiro'
                'Valor')
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'iLinha'
              Footers = <>
              Width = 22
            end
            item
              EditButtons = <>
              FieldName = 'iColuna'
              Footers = <>
              Width = 24
            end
            item
              EditButtons = <>
              FieldName = 'cTamanhoFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'EXPANDIDO'
                'COMP12'
                'COMP17'
                'COMP20')
              Width = 57
            end
            item
              EditButtons = <>
              FieldName = 'cEstiloFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'ITALICO'
                'NEGRITO')
            end
            item
              EditButtons = <>
              FieldName = 'cLPP'
              Footers = <>
              PickList.Strings = (
                'SEIS'
                'OITO')
            end
            item
              EditButtons = <>
              FieldName = 'cTagFormatacao'
              Footers = <>
              Title.Caption = 'Tag'#39's de Formata'#231#227'o do Campo (@@value : subs. valor campo)'
              Width = 365
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
    object tabAutenticacao: TcxTabSheet
      Caption = 'A&utentica'#231#227'o Carnet'
      ImageIndex = 4
      OnEnter = tabAutenticacaoEnter
      object Label9: TLabel
        Left = 0
        Top = 0
        Width = 20
        Height = 13
        Caption = 'SQL'
        FocusControl = DBMemo5
      end
      object DBMemo5: TDBMemo
        Left = 0
        Top = 0
        Width = 600
        Height = 429
        Align = alLeft
        DataField = 'cQueryAutent'
        DataSource = dsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object Panel5: TPanel
        Left = 600
        Top = 0
        Width = 601
        Height = 429
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object cxPageControl6: TcxPageControl
          Left = 0
          Top = 360
          Width = 601
          Height = 69
          ActivePage = cxTabSheet4
          Align = alBottom
          Images = ImageList1
          LookAndFeel.NativeStyle = True
          TabOrder = 0
          DragCursor = crHelp
          OnEnter = cxPageControl2Enter
          OnExit = cxPageControl2Exit
          ClientRectBottom = 65
          ClientRectLeft = 4
          ClientRectRight = 597
          ClientRectTop = 25
          object cxTabSheet4: TcxTabSheet
            Caption = 'Tags'
            ImageIndex = 9
            object DBGridEh5_: TDBGridEh
              Tag = 2
              Left = 0
              Top = 0
              Width = 593
              Height = 40
              Align = alClient
              DataGrouping.GroupLevels = <>
              DataSource = dsTags
              Flat = True
              FooterColor = clWindow
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'Segoe UI'
              FooterFont.Style = []
              IndicatorOptions = [gioShowRowIndicatorEh]
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Segoe UI'
              TitleFont.Style = []
              UseMultiTitle = True
              OnEnter = DBGridEh1Enter
              Columns = <
                item
                  EditButtons = <>
                  FieldName = 'cTag'
                  Footers = <>
                  Title.Caption = 'Tag'
                  Width = 110
                end
                item
                  EditButtons = <>
                  FieldName = 'cDescricao'
                  Footers = <>
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 430
                end>
              object RowDetailData: TRowDetailPanelControlEh
                object DBRadioGroup6: TDBRadioGroup
                  Left = -88
                  Top = -48
                  Width = 185
                  Height = 105
                  Caption = 'DBRadioGroup1'
                  TabOrder = 0
                end
              end
            end
          end
        end
        object DBGridEh5: TDBGridEh
          Left = 0
          Top = 0
          Width = 601
          Height = 360
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsCampoA
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'Segoe UI'
          FooterFont.Style = []
          IndicatorOptions = [gioShowRowIndicatorEh]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Segoe UI'
          TitleFont.Style = []
          UseMultiTitle = True
          OnEnter = DBGridEh1Enter
          Columns = <
            item
              EditButtons = <>
              FieldName = 'nCdCampoModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'nCdModImpDF'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'cParte'
              Footers = <>
              Visible = False
            end
            item
              EditButtons = <>
              FieldName = 'iSequencia'
              Footers = <>
              Title.Caption = 'Campo|Seq.'
              Width = 43
            end
            item
              EditButtons = <>
              FieldName = 'cNmCampo'
              Footers = <>
              Width = 121
            end
            item
              EditButtons = <>
              FieldName = 'cDescricao'
              Footers = <>
              Width = 167
            end
            item
              EditButtons = <>
              FieldName = 'cTipoDado'
              Footers = <>
              KeyList.Strings = (
                'C'
                'I'
                'V')
              PickList.Strings = (
                'Caracter'
                'Inteiro'
                'Valor')
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'iLinha'
              Footers = <>
              Width = 22
            end
            item
              EditButtons = <>
              FieldName = 'iColuna'
              Footers = <>
              Width = 24
            end
            item
              EditButtons = <>
              FieldName = 'cTamanhoFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'EXPANDIDO'
                'COMP12'
                'COMP17'
                'COMP20')
              Width = 57
            end
            item
              EditButtons = <>
              FieldName = 'cEstiloFonte'
              Footers = <>
              PickList.Strings = (
                'NORMAL'
                'ITALICO'
                'NEGRITO')
            end
            item
              EditButtons = <>
              FieldName = 'cLPP'
              Footers = <>
              PickList.Strings = (
                'SEIS'
                'OITO')
            end
            item
              EditButtons = <>
              FieldName = 'cTagFormatacao'
              Footers = <>
              Title.Caption = 'Tag'#39's de Formata'#231#227'o do Campo (@@value : subs. valor campo)'
              Width = 365
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
  end
  object rgTipoImpDF: TDBRadioGroup [12]
    Left = 88
    Top = 80
    Width = 177
    Height = 45
    Caption = ' Tipo de Impress'#227'o '
    Columns = 2
    DataField = 'cFlgTipoImpDF'
    DataSource = dsMaster
    Items.Strings = (
      'Matricial'
      'T'#233'rmica')
    TabOrder = 2
    Values.Strings = (
      'M'
      'T')
    OnChange = rgTipoImpDFChange
  end
  object DBEdit4: TDBEdit [13]
    Left = 278
    Top = 102
    Width = 50
    Height = 19
    DataField = 'iLinhaEntreItens'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit5: TDBEdit [14]
    Left = 332
    Top = 102
    Width = 50
    Height = 19
    DataField = 'iColuna'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [15]
    Left = 386
    Top = 102
    Width = 50
    Height = 19
    DataField = 'iEspaco'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [16]
    Left = 440
    Top = 102
    Width = 50
    Height = 19
    DataField = 'iBuffer'
    DataSource = dsMaster
    TabOrder = 10
  end
  object checkTraduzirTag: TDBCheckBox [17]
    Left = 600
    Top = 86
    Width = 154
    Height = 17
    Caption = 'Traduzir Tags (imp. t'#233'mica)'
    DataField = 'cFlgTraduzirTag'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object checkIgnoraTag: TDBCheckBox [18]
    Left = 600
    Top = 109
    Width = 154
    Height = 17
    Caption = 'Ignorar Tags (imp. t'#233'mica)'
    DataField = 'cFlgIgnorarTag'
    DataSource = dsMaster
    TabOrder = 8
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBComboBox5: TDBComboBox [19]
    Left = 495
    Top = 102
    Width = 97
    Height = 22
    Style = csOwnerDrawFixed
    DataField = 'cPageCodImpDF'
    DataSource = dsMaster
    ItemHeight = 16
    Items.Strings = (
      'pcNone'
      'pc437'
      'pc850'
      'pc852'
      'pc860'
      'pcUTF8'
      'pc1252')
    TabOrder = 6
  end
  object GroupBox1: TGroupBox [20]
    Left = 760
    Top = 44
    Width = 122
    Height = 81
    Caption = ' C'#243'digo de Barras '
    TabOrder = 12
    object Label11: TLabel
      Left = 8
      Top = 14
      Width = 39
      Height = 13
      Caption = 'Largura'
      FocusControl = DBEdit3
    end
    object Label12: TLabel
      Left = 64
      Top = 14
      Width = 31
      Height = 13
      Caption = 'Altura'
      FocusControl = DBEdit8
    end
    object DBEdit3: TDBEdit
      Left = 8
      Top = 30
      Width = 50
      Height = 19
      DataField = 'iLarguraBarras'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit8: TDBEdit
      Left = 64
      Top = 30
      Width = 50
      Height = 19
      DataField = 'iAlturaBarras'
      DataSource = dsMaster
      TabOrder = 1
    end
    object checkNumBarra: TDBCheckBox
      Left = 8
      Top = 56
      Width = 89
      Height = 17
      Caption = 'Exibe N'#250'mero'
      DataField = 'cFlgNumBarras'
      DataSource = dsMaster
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  object GroupBox2: TGroupBox [21]
    Left = 888
    Top = 44
    Width = 179
    Height = 81
    Caption = ' QRCode '
    TabOrder = 13
    object Label15: TLabel
      Left = 8
      Top = 14
      Width = 22
      Height = 13
      Caption = 'Tipo'
      FocusControl = DBEdit9
    end
    object Label16: TLabel
      Left = 64
      Top = 14
      Width = 39
      Height = 13
      Caption = 'Largura'
      FocusControl = DBEdit10
    end
    object Label17: TLabel
      Left = 120
      Top = 14
      Width = 50
      Height = 13
      Caption = 'LevelError'
      FocusControl = DBEdit11
    end
    object DBEdit9: TDBEdit
      Left = 8
      Top = 30
      Width = 50
      Height = 19
      DataField = 'iTipoQRCode'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit10: TDBEdit
      Left = 64
      Top = 30
      Width = 50
      Height = 19
      DataField = 'iLarguraQRCode'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit11: TDBEdit
      Left = 120
      Top = 30
      Width = 50
      Height = 19
      DataField = 'iLevelErroQRCode'
      DataSource = dsMaster
      TabOrder = 2
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ModImpDF'
      'WHERE nCdModImpDF = :nPK')
    Left = 408
    Top = 248
    object qryMasternCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
    end
    object qryMastercNmModImpDF: TStringField
      FieldName = 'cNmModImpDF'
      Size = 50
    end
    object qryMastercQueryHeader: TMemoField
      FieldName = 'cQueryHeader'
      BlobType = ftMemo
    end
    object qryMastercQueryDetalhe: TMemoField
      FieldName = 'cQueryDetalhe'
      BlobType = ftMemo
    end
    object qryMastercQueryFatura: TMemoField
      FieldName = 'cQueryFatura'
      BlobType = ftMemo
    end
    object qryMastercQueryMensagem: TMemoField
      FieldName = 'cQueryMensagem'
      BlobType = ftMemo
    end
    object qryMastercQueryAutent: TMemoField
      FieldName = 'cQueryAutent'
      BlobType = ftMemo
    end
    object qryMasteriLinhaEntreItens: TIntegerField
      FieldName = 'iLinhaEntreItens'
    end
    object qryMastercFlgTipoImpDF: TStringField
      FieldName = 'cFlgTipoImpDF'
      FixedChar = True
      Size = 1
    end
    object qryMasteriColuna: TIntegerField
      FieldName = 'iColuna'
    end
    object qryMasteriEspaco: TIntegerField
      FieldName = 'iEspaco'
    end
    object qryMasteriBuffer: TIntegerField
      FieldName = 'iBuffer'
    end
    object qryMastercFlgTraduzirTag: TIntegerField
      FieldName = 'cFlgTraduzirTag'
    end
    object qryMastercFlgIgnorarTag: TIntegerField
      FieldName = 'cFlgIgnorarTag'
    end
    object qryMastercPageCodImpDF: TStringField
      FieldName = 'cPageCodImpDF'
      Size = 30
    end
    object qryMasteriLarguraBarras: TIntegerField
      FieldName = 'iLarguraBarras'
    end
    object qryMasteriAlturaBarras: TIntegerField
      FieldName = 'iAlturaBarras'
    end
    object qryMastercFlgNumBarras: TIntegerField
      FieldName = 'cFlgNumBarras'
    end
    object qryMasteriTipoQRCode: TIntegerField
      FieldName = 'iTipoQRCode'
    end
    object qryMasteriLarguraQRCode: TIntegerField
      FieldName = 'iLarguraQRCode'
    end
    object qryMasteriLevelErroQRCode: TIntegerField
      FieldName = 'iLevelErroQRCode'
    end
  end
  inherited dsMaster: TDataSource
    Left = 408
    Top = 280
  end
  inherited qryID: TADOQuery
    Left = 440
    Top = 248
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 472
    Top = 248
  end
  inherited qryStat: TADOQuery
    Left = 440
    Top = 280
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 472
    Top = 280
  end
  inherited ImageList1: TImageList
    Left = 504
    Top = 248
    Bitmap = {
      494C01010A000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF000000000000000000000000000000
      0000FCF5F200E1956500E09056000000000000000000E39A5900E49A6800FCF5
      F200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF00000000000000000000000000FCF4
      EF00E49C6800E5A36500E1945D000000000000000000E6A06100E7A86800E49C
      6800FCF3EF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF000000000000000000F2CFB400E6A2
      5A00E8AA6A00E49D6000FAEEE7000000000000000000FBF0E800E6A36200E8AA
      6A00E39C5700EFC7B20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF0000000000FEFCFA00E8AC6000ECB8
      7900E6A65B00F7E2D30000000000000000000000000000000000F8E4D400E6A6
      5B00E9B27500E59F5B00FEFBFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF000000000000000000F4D5B700EAAE
      6000EAB57100E8A76500FBF0E8000000000000000000FCF2E900E9AE6700EAB5
      7100E8A85D00F2CEB40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF00000000000000000000000000FDF6
      F000EBB17100EBB67200E8AA66000000000000000000ECB56A00ECBB7600EBB1
      7100FDF6F0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF000000000000000000000000000000
      0000FDF8F300EBB47200E8AF62000000000000000000ECB76700ECB87300FDF8
      F300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000000000000000
      000000000000FDF8F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF00000000FFFFFFFF00000000
      8000FFFF000000000000FFFF000000000000F18F000000000000E18700000000
      0000C18300000000000083C1000000000000C183000000000000E18700000000
      0000F18F000000000000FBFF000000008000FFFF000000000000FFFF00000000
      0100FFFF000000000000FFFF00000000FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryCampoC: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCampoCBeforePost
    Parameters = <
      item
        Name = 'cParte'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 1
        Size = 1
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CampoModImpDF'
      ' WHERE cParte      = :cParte'
      '   AND nCdModImpDF = :nPK'
      ' ORDER BY iSequencia ')
    Left = 412
    Top = 312
    object qryCampoCnCdCampoModImpDF: TIntegerField
      FieldName = 'nCdCampoModImpDF'
    end
    object qryCampoCnCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
    end
    object qryCampoCcParte: TStringField
      FieldName = 'cParte'
      FixedChar = True
      Size = 1
    end
    object qryCampoCiSequencia: TIntegerField
      DisplayLabel = 'Campo|Ordem'
      FieldName = 'iSequencia'
    end
    object qryCampoCcNmCampo: TStringField
      DisplayLabel = 'Campo|Nome Campo'
      FieldName = 'cNmCampo'
      Size = 30
    end
    object qryCampoCcDescricao: TStringField
      DisplayLabel = 'Campo|Descri'#231#227'o'
      FieldName = 'cDescricao'
      Size = 50
    end
    object qryCampoCiLinha: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Lin.'
      FieldName = 'iLinha'
    end
    object qryCampoCiColuna: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Col.'
      FieldName = 'iColuna'
    end
    object qryCampoCcTipoDado: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryCampoCcTamanhoFonte: TStringField
      DisplayLabel = 'Fonte|Tamanho'
      FieldName = 'cTamanhoFonte'
      Size = 10
    end
    object qryCampoCcEstiloFonte: TStringField
      DisplayLabel = 'Fonte|Estilo'
      FieldName = 'cEstiloFonte'
      Size = 10
    end
    object qryCampoCcLPP: TStringField
      DisplayLabel = 'Fonte|lpp'
      FieldName = 'cLPP'
      Size = 10
    end
    object qryCampoCcTagFormatacao: TStringField
      FieldName = 'cTagFormatacao'
      Size = 200
    end
  end
  object dsCampoC: TDataSource
    DataSet = qryCampoC
    Left = 412
    Top = 344
  end
  object qryCampoA: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCampoABeforePost
    Parameters = <
      item
        Name = 'cParte'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 1
        Size = 1
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CampoModImpDF'
      ' WHERE cParte = :cParte'
      '   AND nCdModImpDF = :nPK')
    Left = 444
    Top = 312
    object qryCampoAnCdCampoModImpDF: TAutoIncField
      FieldName = 'nCdCampoModImpDF'
      ReadOnly = True
    end
    object qryCampoAnCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
    end
    object qryCampoAcParte: TStringField
      FieldName = 'cParte'
      FixedChar = True
      Size = 1
    end
    object qryCampoAiSequencia: TIntegerField
      DisplayLabel = 'Campo|Ordem'
      FieldName = 'iSequencia'
    end
    object qryCampoAcNmCampo: TStringField
      DisplayLabel = 'Campo|Nome Campo'
      FieldName = 'cNmCampo'
      Size = 30
    end
    object qryCampoAcDescricao: TStringField
      DisplayLabel = 'Campo|Descri'#231#227'o'
      FieldName = 'cDescricao'
      Size = 50
    end
    object qryCampoAiLinha: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Lin.'
      FieldName = 'iLinha'
    end
    object qryCampoAiColuna: TIntegerField
      DisplayLabel = 'Posi'#231#227'o|Col.'
      FieldName = 'iColuna'
    end
    object qryCampoAcTipoDado: TStringField
      DisplayLabel = 'Tipo'
      FieldName = 'cTipoDado'
      FixedChar = True
      Size = 1
    end
    object qryCampoAcTamanhoFonte: TStringField
      DisplayLabel = 'Fonte|Tamanho'
      FieldName = 'cTamanhoFonte'
      Size = 10
    end
    object qryCampoAcEstiloFonte: TStringField
      DisplayLabel = 'Fonte|Estilo'
      FieldName = 'cEstiloFonte'
      Size = 10
    end
    object qryCampoAcLPP: TStringField
      DisplayLabel = 'Fonte|lpp'
      FieldName = 'cLPP'
      Size = 10
    end
    object qryCampoAcTagFormatacao: TStringField
      FieldName = 'cTagFormatacao'
      Size = 200
    end
  end
  object dsCampoA: TDataSource
    DataSet = qryCampoA
    Left = 444
    Top = 344
  end
  object qryTags: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTags'#39') IS NOT NULL)'
      '    DROP TABLE #TempTags'
      ''
      'CREATE TABLE #TempTags (nCdTag     int identity(1,1) primary key'
      '                       ,cTag       varchar(30) unique'
      '                       ,cDescricao varchar(200))'
      ''
      
        'INSERT INTO #TempTags VALUES ('#39'</abre_gaveta>'#39', '#39'Aciona a abertu' +
        'ra da gaveta de dinheiro'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</beep>'#39', '#39'Emite um beep na impre' +
        'ssora (n'#227'o disponivel em alguns modelos)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</corte_parcial>'#39', '#39'Efetua corte ' +
        'parcial no papel (n'#227'o disponivel em alguns modelos)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</corte_total>'#39', '#39'Efetua corte to' +
        'tal no papel'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</linha_dupla>'#39', '#39'Imprime linha d' +
        'upla'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</linha_simples>'#39', '#39'Imprime linha' +
        ' simples'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</pular_linhas>'#39', '#39'Pula N linhas ' +
        'de acordo com propriedade do componente'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</zera>'#39', '#39'Reseta as configura'#231#245'e' +
        's de fonte alinhamento'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<abre_gaveta>'#39', '#39'Bloco - abertura' +
        ' de gaveta espec'#237'fica (1 ou 2)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<barra_altura>'#39', '#39'Bloco - configu' +
        'ra a altura do cod. barras: 0 a 255. (0 : default)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<barra_largura>'#39', '#39'Bloco - config' +
        'ura a largura das barras do cod. barras: 0 a 5. (0 : default)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<barra_mostrar>'#39', '#39'Bloco - config' +
        'ura se deve exibir conte'#250'do abaixo do cod. barras: (0 : n'#227'o / 1 ' +
        ': sim)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</cr>'#39', '#39'Retorna para o in'#237'cio da' +
        ' linha'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ad>'#39', '#39'Bloco - texto alinhado a ' +
        'direita'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</ad>'#39', '#39'Liga alinhamento a direi' +
        'ta'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ae>'#39', '#39'Bloco - texto alinhado a ' +
        'esquerda'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</ae>'#39', '#39'Liga alinhamento a esque' +
        'rda'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<lf>'#39', '#39'Ajusta p'#225'gina de c'#243'digo e' +
        ' espa'#231'o entre linhas'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</lf>'#39', '#39'Pula para a pr'#243'pxima lin' +
        'ha'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</fa>'#39', '#39'Liga fonte tipo A (norma' +
        'l)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</fb>'#39', '#39'Liga fonte tipo B (conde' +
        'nsada)'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</fn>'#39', '#39'Fonte normal'#39')'
      'INSERT INTO #TempTags VALUES ('#39'<c>'#39', '#39'Liga condensado'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</c>'#39', '#39'Desliga condensado'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ce>'#39', '#39'Bloco - texto centralizad' +
        'o'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</ce>'#39', '#39'Liga alinhamento ao cent' +
        'ro'#39')'
      'INSERT INTO #TempTags VALUES ('#39'<e>'#39', '#39'Liga expandido'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</e>'#39', '#39'Desliga expandido'#39')'
      'INSERT INTO #TempTags VALUES ('#39'<i>'#39', '#39'Liga it'#225'lico'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</i>'#39', '#39'Desliga italico'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<in>'#39', '#39'Liga fonte invertida (fun' +
        'do preto)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'</in>'#39', '#39'Desliga fonte invertida'#39 +
        ')'
      'INSERT INTO #TempTags VALUES ('#39'<n>'#39', '#39'Liga negrito'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</n>'#39', '#39'Desliga negrito'#39')'
      'INSERT INTO #TempTags VALUES ('#39'<s>'#39', '#39'Liga sublinhado'#39')'
      'INSERT INTO #TempTags VALUES ('#39'</s>'#39', '#39'Desliga sublinhado'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<logo_fatorx>'#39', '#39'Bloco - configur' +
        'a o aumento horizonal do logo, de 1 a 4'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<logo_fatory>'#39', '#39'Bloco - configur' +
        'a o aumento vertical do logo, de 1 a 4'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<logo_kc1>'#39', '#39'Bloco - configura a' +
        ' posi'#231#227'o KC1 do logo a ser impresso. Ex: 0 : 48'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<logo_kc2>'#39', '#39'Bloco - configura a' +
        ' posi'#231#227'o KC2 do logo a ser impresso. Ex: 1: 49'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<logo_imprimir>'#39', '#39'Bloco - config' +
        'ura a impress'#227'o ou n'#227'o do logo tipo: (0 : n'#227'o / 1: sim default)'#39 +
        ')'
      
        'INSERT INTO #TempTags VALUES ('#39'</logo>'#39', '#39'Imprime logotipo j'#225' gr' +
        'avado na impressora (use utilit'#225'rio do fabricante)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<codabar>'#39', '#39'Bloco - cod. barra M' +
        'SI - apenas n'#250'meros, 1 d'#237'gito verificador'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code11>'#39', '#39'Bloco - cod. barras C' +
        'ode11 - apenas n'#250'meros, tamanho livre'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code128>'#39', '#39'Bloco - cod. barras ' +
        'Code128 - todos os caracteres ASCII, tamanho livre'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code128a>'#39', '#39'Bloco - cod. barras' +
        ' Code128 - subtipo B (padr'#227'o) = <code128>'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code128b>'#39', '#39'Bloco - cod. barras' +
        ' Code128 - subtipo C (informar valores em BCD)'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code128c>'#39', '#39'Bloco - cod. barras' +
        ' Code128 - tipo C'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code39>'#39', '#39'Bloco - cod. barras C' +
        'ode39 - aceita: 0..9,A..Z, ,$,%,*,+,-,.,/, tamanho livre'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<code93>'#39', '#39'Bloco - cod. barras C' +
        'ode93 - aceita: 0..9,A..Z,-,., ,$,/,+,%, tamanho livre'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ean13>'#39', '#39'Bloco - cod. barras EA' +
        'N13 - 12 n'#250'meros e 1 d'#237'gito verificador'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ean8>'#39', '#39'Bloco - cod. barras EAN' +
        '8 - 7 n'#250'meros e 1 d'#237'gito verificador'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<ignorar_tags>'#39', '#39'Bloco - ignora ' +
        'todas as tags contidas no bloco'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<inter>'#39', '#39'Bloco - cod. barras "i' +
        'nterleaved 2 of 5" - apenas n'#250'meros, tamanho PAR'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<msi>'#39', '#39'Bloco - cod. barras Code' +
        '128 - Subtipo A'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<qrcode_error>'#39', '#39'Bloco - configu' +
        'ra o error level do QRCode: 0 a 3'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<qrcode_largura>'#39', '#39'Bloco - confi' +
        'gura a largura do QRCode: 1 a 16'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<qrcode_tipo>'#39', '#39'Bloco - Configur' +
        'a o tipo de QRCode: 1 ou 2'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<qrcode>'#39', '#39'Bloco - imprime QRCod' +
        'e de acordo com "ConfigQRCode"'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<std>'#39', '#39'Bloco - cod. barras "Sta' +
        'ndard 2 of 5" - apenas n'#250'meros, tamanho livre'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<upca>'#39', '#39'Bloco - cod. barras UPC' +
        'A - 11 numeros e 1 d'#237'gito verificador'#39')'
      
        'INSERT INTO #TempTags VALUES ('#39'<upce>'#39', '#39'Bloco - cod. barras Cod' +
        'aBar - Aceita: 0..9,A..D,a..d,$,+,-,.,/,:, tamanho livre'#39')'
      ''
      'SELECT *'
      '  FROM #TempTags')
    Left = 476
    Top = 312
    object qryTagsnCdTag: TAutoIncField
      FieldName = 'nCdTag'
      ReadOnly = True
    end
    object qryTagscTag: TStringField
      FieldName = 'cTag'
      Size = 30
    end
    object qryTagscDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 200
    end
  end
  object dsTags: TDataSource
    DataSet = qryTags
    Left = 476
    Top = 344
  end
  object menuOpcao: TPopupMenu
    Left = 508
    Top = 280
    object btDuplicarModelo: TMenuItem
      Caption = 'Duplicar Modelo de Doc.'
      OnClick = btDuplicarModeloClick
    end
  end
  object qryDuplicarModelo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdModImpDF     int'
      '       ,@nCdModImpDFNovo int'
      ''
      'SET @nCdModImpDF = :nPK'
      ''
      'EXEC usp_ProximoID '#39'MODIMPDF'#39
      '    ,@nCdModImpDFNovo OUTPUT'
      ''
      'INSERT INTO ModImpDF'
      '     SELECT @nCdModImpDFNovo'
      '           ,cNmModImpDF + '#39' (NOVO MODELO)'#39
      '           ,cQueryHeader'
      '           ,cQueryDetalhe'
      '           ,cQueryFatura'
      '           ,cQueryMensagem'
      '           ,cQueryAutent'
      '           ,iLinhaEntreItens'
      '           ,iColuna'
      '           ,iEspaco'
      '           ,iBuffer'
      '           ,cFlgTraduzirTag'
      '           ,cFlgIgnorarTag'
      '           ,cFlgTipoImpDF'
      '           ,cPageCodImpDF'
      '           ,iLarguraBarras'
      '           ,iAlturaBarras'
      '           ,cFlgNumBarras'
      '           ,iTipoQRCode'
      '           ,iLarguraQRCode'
      '           ,iLevelErroQRCode'
      '       FROM ModImpDF'
      '      WHERE nCdModImpDF = @nCdModImpDF'
      ' '
      'INSERT INTO CampoModImpDF'
      '     SELECT @nCdModImpDFNovo'
      '           ,cParte'
      '           ,iSequencia'
      '           ,cNmCampo'
      '           ,cDescricao'
      '           ,iLinha'
      '           ,iColuna'
      '           ,cTipoDado'
      '           ,cTamanhoFonte'
      '           ,cEstiloFonte'
      '           ,cLPP'
      '           ,nCdServidorOrigem'
      '           ,dDtReplicacao'
      '           ,cTagFormatacao'
      '       FROM CampoModImpDF'
      '      WHERE nCdModImpDF = @nCdModImpDF'
      '      '
      'SELECT @nCdModImpDFNovo as nCdModImpDF')
    Left = 508
    Top = 312
    object qryDuplicarModelonCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
      ReadOnly = True
    end
  end
end
