inherited frmParam_TipoPedido_Ajuda: TfrmParam_TipoPedido_Ajuda
  Left = 499
  Top = 249
  Width = 277
  Height = 291
  BorderIcons = []
  Caption = 'Vari'#225'veis Dispon'#237'veis'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 261
    Height = 224
  end
  object Label3: TLabel [1]
    Left = 102
    Top = 89
    Width = 142
    Height = 13
    Caption = '- ICMS Substitui'#231#227'o Tribut'#225'ria'
  end
  object Label9: TLabel [2]
    Left = 102
    Top = 163
    Width = 45
    Height = 13
    Caption = '- COFINS'
  end
  object Label11: TLabel [3]
    Left = 102
    Top = 42
    Width = 21
    Height = 13
    Caption = '- IPI'
  end
  object Label7: TLabel [4]
    Left = 102
    Top = 137
    Width = 23
    Height = 13
    Caption = '- PIS'
  end
  object Label5: TLabel [5]
    Left = 102
    Top = 65
    Width = 32
    Height = 13
    Caption = '- ICMS'
  end
  object Label12: TLabel [6]
    Left = 102
    Top = 113
    Width = 66
    Height = 13
    Caption = '- ICMS Retido'
  end
  object Label1: TLabel [7]
    Left = 9
    Top = 195
    Width = 241
    Height = 52
    Caption = 
      'Nota: as vari'#225'veis ser'#227'o modificadas no momento da gera'#231#227'o do fa' +
      'turamento contabilizando os valores totais dos impostos presente' +
      ' no documento fiscal.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  inherited ToolBar1: TToolBar
    Width = 261
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object Edit1: TEdit [9]
    Tag = 1
    Left = 8
    Top = 40
    Width = 89
    Height = 21
    TabOrder = 1
    Text = '&ipi&'
  end
  object Edit2: TEdit [10]
    Tag = 1
    Left = 8
    Top = 64
    Width = 89
    Height = 21
    TabOrder = 2
    Text = '&icms&'
  end
  object Edit3: TEdit [11]
    Tag = 1
    Left = 8
    Top = 88
    Width = 89
    Height = 21
    TabOrder = 3
    Text = '&icmssub&'
  end
  object Edit4: TEdit [12]
    Tag = 1
    Left = 8
    Top = 112
    Width = 89
    Height = 21
    TabOrder = 4
    Text = '&icmsret&'
  end
  object Edit5: TEdit [13]
    Tag = 1
    Left = 8
    Top = 136
    Width = 89
    Height = 21
    TabOrder = 5
    Text = '&pis&'
  end
  object Edit6: TEdit [14]
    Tag = 1
    Left = 8
    Top = 160
    Width = 89
    Height = 21
    TabOrder = 6
    Text = '&cofins&'
  end
  inherited ImageList1: TImageList
    Left = 160
    Top = 40
  end
end
