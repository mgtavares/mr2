unit dcVendaLojaWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TdcmVendaLojaWeb = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    dsDepartamento: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    dsCategoria: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    dsSubCategoria: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    dsSegmento: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    dsMarca: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    dsGrupoProduto: TDataSource;
    edtReferencia: TEdit;
    Label10: TLabel;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    dsLinha: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    dsColecao: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    dsClasseProduto: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    edtDtFinal2: TMaskEdit;
    Label15: TLabel;
    edtDtInicial2: TMaskEdit;
    Label16: TLabel;
    Label17: TLabel;
    GroupBox1: TGroupBox;
    chkDimAno: TCheckBox;
    chkDimMes: TCheckBox;
    chkDimLoja: TCheckBox;
    chkDimDataPedido: TCheckBox;
    chkDimFormaPagto: TCheckBox;
    chkDimCondPagto: TCheckBox;
    chkDimDepartamento: TCheckBox;
    chkDimCategoria: TCheckBox;
    chkDimSubCategoria: TCheckBox;
    chkDimSegmento: TCheckBox;
    chkDimMarca: TCheckBox;
    chkDimProduto: TCheckBox;
    chkDimSku: TCheckBox;
    GroupBox2: TGroupBox;
    chkMedQtdeVenda: TCheckBox;
    chkMedQtdeEstoque: TCheckBox;
    chkMedValVenda: TCheckBox;
    chkMedValCustoVenda: TCheckBox;
    chkMedMarkUp: TCheckBox;
    chkMedValBruto: TCheckBox;
    chkMedMC: TCheckBox;
    chkDimNumPedido: TCheckBox;
    qryTipoPedido: TADOQuery;
    dsTipoPedido: TDataSource;
    qryTipoPedidocNmTipoPedido: TStringField;
    Label21: TLabel;
    DBEdit12: TDBEdit;
    edtTipoPedido: TMaskEdit;
    chkDimNumPedidoWeb: TCheckBox;
    chkDimStatusPedido: TCheckBox;
    GroupBox3: TGroupBox;
    chkMkpFreteDest: TCheckBox;
    chkMkpFreteEmit: TCheckBox;
    chkMkpFreteTerc: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoExit(Sender: TObject);
    procedure chkMedMarkUpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaLojaWeb: TdcmVendaLojaWeb;

implementation

uses
  fMenu, fLookup_Padrao, dcVendaLojaWeb_view, Math;

{$R *.dfm}

procedure TdcmVendaLojaWeb.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      desativaMaskEdit(edtLoja);
  end;
end;

procedure TdcmVendaLojaWeb.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text);
end;

procedure TdcmVendaLojaWeb.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value;
end;

procedure TdcmVendaLojaWeb.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value;
end;

procedure TdcmVendaLojaWeb.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
end;

procedure TdcmVendaLojaWeb.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
end;

procedure TdcmVendaLojaWeb.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
end;

procedure TdcmVendaLojaWeb.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin
            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin
            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin
            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin
            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
end;

procedure TdcmVendaLojaWeb.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
        end ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
end;

procedure TdcmVendaLojaWeb.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
end;

procedure TdcmVendaLojaWeb.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
end;

procedure TdcmVendaLojaWeb.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.edtTipoPedidoExit(Sender: TObject);
begin
  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido,edtTipoPedido.Text);
end;

procedure TdcmVendaLojaWeb.edtTipoPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(71, 'cFlgTipoPedidoWeb = 1');

        If (nPK > 0) then
            edtTipoPedido.Text := IntToStr(nPK) ;
    end ;
  end ;
end;

procedure TdcmVendaLojaWeb.ToolButton1Click(Sender: TObject);
var
  objForm : TdcmVendaLojaWeb_View;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') or (Trim(edtDtFinal.Text) = '/  /') then
  begin
      edtDtFinal.Text   := DateToStr(Date) ;
      edtDtInicial.Text := DateToStr(Date) ;
  end ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo de venda inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  if (trim(edtDtInicial2.Text) = '/  /') or (Trim(edtDtFinal2.Text) = '/  /') then
  begin
      edtDtFinal2.Text   := edtDtFinal.Text ;
      edtDtInicial2.Text := edtDtInicial.Text ;
  end ;

  if (StrToDate(edtDtInicial2.Text) > StrToDate(edtDtFinal2.Text)) then
  begin
      MensagemAlerta('Per�odo de faturamento inv�lido.') ;
      edtDtInicial2.SetFocus;
      exit ;
  end ;

  try
      objForm := TdcmVendaLojaWeb_View.Create(nil);

      objForm.ADODataSet1.Close;
      objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
      objForm.ADODataSet1.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdTipoPedido').Value    := frmMenu.ConvInteiro(edtTipoPedido.Text);
      objForm.ADODataSet1.Parameters.ParamByName('cReferencia').Value      := edtReferencia.Text ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtPedidoInicial').Value := frmMenu.ConvData(edtDtInicial.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtPedidoFinal').Value   := frmMenu.ConvData(edtDtFinal.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtFaturInicial').Value  := frmMenu.ConvData(edtDtInicial2.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtFaturFinal').Value    := frmMenu.ConvData(edtDtFinal2.Text) ;
      objForm.ADODataSet1.Parameters.ParamByName('cFlgConsFreteEmitMKP').Value := IfThen(chkMkpFreteEmit.Checked, 1, 0);
      objForm.ADODataSet1.Parameters.ParamByName('cFlgConsFreteDestMKP').Value := IfThen(chkMkpFreteDest.Checked, 1, 0);
      objForm.ADODataSet1.Parameters.ParamByName('cFlgConsFreteTercMKP').Value := IfThen(chkMkpFreteTerc.Checked, 1, 0);
      objForm.ADODataSet1.Open ;

      if (objForm.ADODataSet1.eof) then
      begin
          ShowMessage('Nenhuma informa��o encontrada.') ;
          Exit ;
      end ;

      frmMenu.mensagemUsuario('Preparando cubo...');

      if objForm.PivotCube1.Active then
      begin
          objForm.PivotCube1.Active := False;
      end;

      { -- configura dimens�es do cubo -- }
      objForm.PivotCube1.Dimensions[0].Enabled  := (chkDimAno.Checked) ;
      objForm.PivotCube1.Dimensions[1].Enabled  := (chkDimMes.Checked) ;
      objForm.PivotCube1.Dimensions[2].Enabled  := (chkDimLoja.Checked) ;
      objForm.PivotCube1.Dimensions[3].Enabled  := (chkDimDataPedido.Checked) ;
      objForm.PivotCube1.Dimensions[4].Enabled  := (chkDimNumPedido.Checked) ;
      objForm.PivotCube1.Dimensions[5].Enabled  := (chkDimNumPedidoWeb.Checked) ;
      objForm.PivotCube1.Dimensions[6].Enabled  := (chkDimStatusPedido.Checked) ;
      objForm.PivotCube1.Dimensions[7].Enabled  := (chkDimFormaPagto.Checked) ;
      objForm.PivotCube1.Dimensions[8].Enabled  := (chkDimCondPagto.Checked) ;
      objForm.PivotCube1.Dimensions[9].Enabled  := (chkDimDepartamento.Checked) ;
      objForm.PivotCube1.Dimensions[10].Enabled := (chkDimCategoria.Checked) ;
      objForm.PivotCube1.Dimensions[11].Enabled := (chkDimSubCategoria.Checked) ;
      objForm.PivotCube1.Dimensions[12].Enabled := (chkDimSegmento.Checked) ;
      objForm.PivotCube1.Dimensions[13].Enabled := (chkDimMarca.Checked) ;
      objForm.PivotCube1.Dimensions[14].Enabled := (chkDimProduto.Checked) ;
      objForm.PivotCube1.Dimensions[15].Enabled := (chkDimSku.Checked) ;

      objForm.PivotMap1.Title               := 'ER2Soft - An�lise de Vendas Web';
      objForm.PVMeasureToolBar1.HideButtons := False;
      objForm.PivotCube1.ExtendedMode       := True;
      objForm.PivotCube1.Active             := True;
      objForm.PivotGrid1.ColWidths[0]       := 500;

      objForm.PivotMap1.SortColumn(0,8,False);
      objForm.PivotMap1.Measures[6].ColumnPercent := True;
      objForm.PivotMap1.Measures[6].Value         := False;
      objForm.PivotMap1.Measures[7].Rank          := True;
      objForm.PivotMap1.Measures[7].Value         := False;

      { -- configura medidas do cubo -- }
      objForm.PivotMap1.ShowMeasure(0);
      objForm.PivotMap1.ShowMeasure(1);
      objForm.PivotMap1.ShowMeasure(2);
      objForm.PivotMap1.ShowMeasure(3);
      objForm.PivotMap1.ShowMeasure(4);
      objForm.PivotMap1.ShowMeasure(5);
      objForm.PivotMap1.ShowMeasure(6);
      objForm.PivotMap1.ShowMeasure(7); //nValMC

      { -- oculta as medidas n�o selecionadas -- }
      if (not chkMedQtdeVenda.Checked) then     objForm.PivotMap1.HideMeasure(0);
      if (not chkMedQtdeEstoque.Checked) then   objForm.PivotMap1.HideMeasure(1);
      if (not chkMedValVenda.Checked) then      objForm.PivotMap1.HideMeasure(2);
      if (not chkMedValCustoVenda.Checked) then objForm.PivotMap1.HideMeasure(3);
      if (not chkMedMarkUp.Checked) then        objForm.PivotMap1.HideMeasure(4);
      if (not chkMedValBruto.Checked) then      objForm.PivotMap1.HideMeasure(5);
      if (not chkMedMC.Checked) then            objForm.PivotMap1.HideMeasure(6);

      frmMenu.mensagemUsuario('');
      objForm.PivotGrid1.RefreshData;
      objForm.ShowModal;
  finally
      frmMenu.mensagemUsuario('');
      FreeAndNil(objForm);
  end;
end;

procedure TdcmVendaLojaWeb.chkMedMarkUpClick(Sender: TObject);
begin
  inherited;

  chkMkpFreteEmit.Enabled := chkMedMarkUp.Checked;
  chkMkpFreteEmit.Checked := chkMedMarkUp.Checked;
  chkMkpFreteDest.Enabled := chkMedMarkUp.Checked;
  chkMkpFreteDest.Checked := chkMedMarkUp.Checked;
  chkMkpFreteTerc.Enabled := chkMedMarkUp.Checked;
  chkMkpFreteTerc.Checked := chkMedMarkUp.Checked;
end;

initialization
  RegisterClass(TdcmVendaLojaWeb);

end.
