inherited frmProdutoMRP: TfrmProdutoMRP
  Left = 33
  Top = 138
  Width = 950
  Height = 446
  Caption = 'Produto - Configura'#231#227'o MRP'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 934
    Height = 381
  end
  inherited ToolBar1: TToolBar
    Width = 934
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh4: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 934
    Height = 381
    Align = alClient
    DataSource = dsProdutoEmpresa
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh4KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProdutoEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Width = 140
      end
      item
        EditButtons = <>
        FieldName = 'nEstoqueSeg'
        Footers = <>
        Width = 71
      end
      item
        EditButtons = <>
        FieldName = 'nEstoqueRessup'
        Footers = <>
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'nEstoqueMax'
        Footers = <>
        Width = 64
      end
      item
        AutoDropDown = True
        EditButtons = <>
        FieldName = 'cFlgTipoPonto'
        Footers = <>
        KeyList.Strings = (
          'D'
          'Q')
        PickList.Strings = (
          'Dias'
          'Quantidade')
        Width = 70
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgAtivoCompra'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'nConsumoMedDia'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'iDiaEntrega'
        Footers = <>
        Visible = False
        Width = 31
      end
      item
        Alignment = taLeftJustify
        EditButtons = <>
        FieldName = 'iTipoEntrega'
        Footers = <>
        KeyList.Strings = (
          '0'
          '1')
        PickList.Strings = (
          'Data Fixa'
          'Dias ap'#243's a emiss'#227'o do pedido')
        Visible = False
        Width = 174
      end
      item
        EditButtons = <>
        FieldName = 'nCdLocalEstoqueEntrega'
        Footers = <>
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        ReadOnly = True
        Width = 174
      end>
  end
  inherited ImageList1: TImageList
    Left = 400
    Top = 160
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 88
    Top = 256
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 120
    Top = 256
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryProdutoEmpresa: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryProdutoEmpresaBeforePost
    OnCalcFields = qryProdutoEmpresaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ProdutoEmpresa'
      'WHERE nCdProduto = :nPK')
    Left = 632
    Top = 248
    object qryProdutoEmpresanCdProdutoEmpresa: TAutoIncField
      FieldName = 'nCdProdutoEmpresa'
      ReadOnly = True
    end
    object qryProdutoEmpresanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoEmpresanCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa|C'#243'd'
      FieldName = 'nCdEmpresa'
    end
    object qryProdutoEmpresacNmEmpresa: TStringField
      DisplayLabel = 'Empresa|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 50
      Calculated = True
    end
    object qryProdutoEmpresanCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryProdutoEmpresacNmLoja: TStringField
      DisplayLabel = 'Loja|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
    object qryProdutoEmpresacFlgTipoPonto: TStringField
      DisplayLabel = 'Ponto de Compra|Tipo Gatilho'
      FieldName = 'cFlgTipoPonto'
      FixedChar = True
      Size = 1
    end
    object qryProdutoEmpresanEstoqueSeg: TBCDField
      DisplayLabel = 'Ponto de Compra|Seguran'#231'a'
      FieldName = 'nEstoqueSeg'
      Precision = 12
      Size = 2
    end
    object qryProdutoEmpresanEstoqueRessup: TBCDField
      DisplayLabel = 'Ponto de Compra|Ressup.'
      FieldName = 'nEstoqueRessup'
      Precision = 12
      Size = 2
    end
    object qryProdutoEmpresanEstoqueMax: TBCDField
      DisplayLabel = 'Ponto de Compra|M'#225'ximo'
      FieldName = 'nEstoqueMax'
      Precision = 12
      Size = 2
    end
    object qryProdutoEmpresanConsumoMedDia: TBCDField
      DisplayLabel = 'Consumo|M'#233'd. Dia'
      FieldName = 'nConsumoMedDia'
      Precision = 12
      Size = 2
    end
    object qryProdutoEmpresaiDiaEntrega: TIntegerField
      DisplayLabel = 'Entrega|Dias'
      FieldName = 'iDiaEntrega'
    end
    object qryProdutoEmpresaiTipoEntrega: TIntegerField
      DisplayLabel = 'Entrega|Tipo C'#225'lculo'
      FieldName = 'iTipoEntrega'
    end
    object qryProdutoEmpresanCdLocalEstoqueEntrega: TIntegerField
      DisplayLabel = 'Estoque Entrega|C'#243'd'
      FieldName = 'nCdLocalEstoqueEntrega'
    end
    object qryProdutoEmpresacNmLocalEstoque: TStringField
      DisplayLabel = 'Estoque Entrega|Local'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
    object qryProdutoEmpresacFlgAtivoCompra: TIntegerField
      DisplayLabel = 'Ativo|MRP'
      FieldName = 'cFlgAtivoCompra'
    end
  end
  object dsProdutoEmpresa: TDataSource
    DataSet = qryProdutoEmpresa
    Left = 664
    Top = 248
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      'FROM LocalEstoque'
      'WHERE nCdLocalEstoque = :nPK')
    Left = 332
    Top = 176
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
end
