unit fCaixa_ParcelaAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ADODB, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGrid, ImgList, ComCtrls, ToolWin,
  ExtCtrls, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSCore, dxPScxCommon, dxPScxGridLnk;

type
  TfrmCaixa_ParcelaAberto = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryTituloAberto: TADOQuery;
    dsTituloAberto: TDataSource;
    qryTituloAbertocEspecieTitulo: TStringField;
    qryTituloAbertodDtVencto: TDateTimeField;
    qryTituloAbertocNrTit: TStringField;
    qryTituloAbertocSituacao: TStringField;
    qryTituloAbertodDtEmissao: TDateTimeField;
    qryTituloAbertonValTit: TBCDField;
    qryTituloAbertonValCorrigido: TBCDField;
    qryTituloAbertoiDiasAtraso: TIntegerField;
    cxGrid1DBTableView1cEspecieTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVencto: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cSituacao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValCorrigido: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasAtraso: TcxGridDBColumn;
    qryTituloAbertoiParcela: TIntegerField;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    ToolButton4: TToolButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FormShow(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_ParcelaAberto: TfrmCaixa_ParcelaAberto;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_ParcelaAberto.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGrid1DBTableView1dDtVencto) or (AItem = cxGrid1DBTableView1cNrTit) or (AItem = cxGrid1DBTableView1iParcela) then
      if (strToInt(ARecord.Values[8]) > 0) then
          AStyle := frmMenu.LinhaVermelha ;

end;

procedure TfrmCaixa_ParcelaAberto.FormShow(Sender: TObject);
begin
  inherited;

  cxGrid1DBTableView1.ViewData.Expand(TRUE) ;

end;

procedure TfrmCaixa_ParcelaAberto.ToolButton4Click(Sender: TObject);
begin
  inherited;
  dxComponentPrinter1.Preview(True,nil);

end;

end.

