unit fInventario_Abertura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmInventario_Abertura = class(TfrmCadastro_Padrao)
    qryMasternCdInventario: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasterdDtAbertura: TDateTimeField;
    qryMasterdDtFech: TDateTimeField;
    qryMasternCdUsuarioAbertura: TIntegerField;
    qryMasternCdUsuarioFech: TIntegerField;
    qryMasternCdGrupoEstoque: TIntegerField;
    qryMasternCdLocalEstoque: TIntegerField;
    qryMastercResponsavel: TStringField;
    qryMastercOBS: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryGrupoEstoque: TADOQuery;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    qryUsuarioAbre: TADOQuery;
    qryUsuarioAbrenCdUsuario: TIntegerField;
    qryUsuarioAbrecNmUsuario: TStringField;
    qryUsuarioFech: TADOQuery;
    qryUsuarioFechnCdUsuario: TIntegerField;
    qryUsuarioFechcNmUsuario: TStringField;
    DBEdit11: TDBEdit;
    DataSource1: TDataSource;
    DBEdit12: TDBEdit;
    DataSource2: TDataSource;
    DBEdit13: TDBEdit;
    DataSource3: TDataSource;
    DBEdit14: TDBEdit;
    DataSource4: TDataSource;
    DBEdit15: TDBEdit;
    DataSource5: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryLocalEstoqueItem: TADOQuery;
    qryLocalEstoqueItemnCdLocalEstoque: TIntegerField;
    qryLocalEstoqueItemcNmLocalEstoque: TStringField;
    qryLocalEstoqueItemnCdGrupoEstoque: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryAux: TADOQuery;
    DBGridEh1: TDBGridEh;
    qryItemInventario: TADOQuery;
    qryItemInventarionCdInventario: TIntegerField;
    qryItemInventarioiItem: TIntegerField;
    qryItemInventarionCdProduto: TIntegerField;
    qryItemInventarionCdLocalEstoque: TIntegerField;
    qryItemInventarionQtdeFisica: TBCDField;
    qryItemInventarionQtdeLogica: TBCDField;
    qryItemInventarionDiferenca: TBCDField;
    qryItemInventarionPercDif: TBCDField;
    dsItemInventario: TDataSource;
    qryItemInventariocNmProduto: TStringField;
    qryItemInventariocNmLocalEstoque: TStringField;
    ToolButton3: TToolButton;
    SP_GERA_ITEM_INVENTARIO: TADOStoredProc;
    qryMasterdDtEstoque: TDateTimeField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryItemInventarionCdItemInventario: TLargeintField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure ToolButton3Click(Sender: TObject);
    procedure qryItemInventarioCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure qryItemInventarioBeforePost(DataSet: TDataSet);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    bNovoRegistro : boolean ;
  public
    { Public declarations }
  end;

var
  frmInventario_Abertura: TfrmInventario_Abertura;

implementation

uses fMenu, fLookup_Padrao, rInventario;

{$R *.dfm}

procedure TfrmInventario_Abertura.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'INVENTARIO' ;
  nCdTabelaSistema  := 58 ;
  nCdConsultaPadrao := 128 ;

end;

procedure TfrmInventario_Abertura.btIncluirClick(Sender: TObject);
begin
  inherited;

  bNovoRegistro := True ;
  
  qryMasternCdEmpresa.Value         := frmMenu.nCdEmpresaAtiva;
  qryMasterdDtAbertura.Value        := Now() ;
  qryMasternCdUsuarioAbertura.Value := frmMenu.nCdUsuarioLogado;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  DbEdit7.SetFocus ;

end;

procedure TfrmInventario_Abertura.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEstoque.Close ;
  PosicionaQuery(qryGrupoEstoque, DbEdit7.Text) ;
  
end;

procedure TfrmInventario_Abertura.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryLocalEstoque, DBEdit8.Text) ;
  
end;

procedure TfrmInventario_Abertura.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryUsuarioAbre.Close ;
  qryUsuarioFech.Close ;
  qryGrupoEstoque.Close ;
  qryLocalEstoque.Close ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryUsuarioAbre, qryMasternCdUsuarioAbertura.AsString) ;
  PosicionaQuery(qryUsuarioFech, qryMasternCdUsuarioFech.AsString) ;
  PosicionaQuery(qryGrupoEstoque, qryMasternCdGrupoEstoque.AsString) ;
  PosicionaQuery(qryLocalEstoque, qryMasternCdLocalEstoque.AsString) ;
  
  PosicionaQuery(qryItemInventario, qryMasternCdInventario.AsString) ;
  
end;

procedure TfrmInventario_Abertura.ToolButton3Click(Sender: TObject);
var
  objRel : TrptInventario_view;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhum invent�rio ativo.') ;
      exit ;
  end ;

  objRel := TrptInventario_view.Create(nil);

  try
      try
          objRel.SPREL_INVENTARIO.Close ;
          objRel.SPREL_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := qryMasternCdInventario.Value ;
          objRel.SPREL_INVENTARIO.Open ;

          objRel.lblEmpresa.Caption      := frmMenu.cNmEmpresaAtiva;
          objRel.lblEmpresa2.Caption     := qryEmpresacNmEmpresa.Value ;
          objRel.lblInventario.Caption   := qryMasternCdInventario.AsString ;
          objRel.Preview;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmInventario_Abertura.qryItemInventarioCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryLocalEstoqueitem.Close ;
  qryProduto.Close ;

  qryLocalEstoqueItem.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryLocalEstoqueitem, qryItemInventarionCdLocalEstoque.asString) ;
  PosicionaQuery(qryProduto, qryItemInventarionCdProduto.asString) ;

  if not qryProduto.eof then
      qryItemInventariocNmProduto.Value      := qryProdutocNmProduto.Value ;

  if not qryLocalEstoqueItem.eof then
      qryItemInventariocNmLocalEstoque.Value := qryLocalEstoqueItemcNmLocalEstoque.Value ;

end;

procedure TfrmInventario_Abertura.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bNovoRegistro) then
  begin

      frmMenu.Connection.BeginTrans;

      try
          SP_GERA_ITEM_INVENTARIO.Close ;
          SP_GERA_ITEM_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value   := qryMasternCdInventario.Value ;
          SP_GERA_ITEM_INVENTARIO.Parameters.ParamByName('@nCdEmpresa').Value      := qryMasternCdEmpresa.Value ;
          SP_GERA_ITEM_INVENTARIO.Parameters.ParamByName('@nCdGrupoEstoque').Value := qryMasternCdGrupoEstoque.Value ;
          SP_GERA_ITEM_INVENTARIO.Parameters.ParamByName('@nCdLocalEstoque').Value := qryMasternCdLocalEstoque.Value ;
          SP_GERA_ITEM_INVENTARIO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      PosicionaQuery(qryItemInventario, qryMasternCdInventario.AsString) ;

  end ;

  bNovoRegistro := False ;

end;

procedure TfrmInventario_Abertura.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsEdit) then
      qryMaster.Post ;
      
end;

procedure TfrmInventario_Abertura.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      if ((DBEdit14.Text = '') and (DBEdit15.Text = '')) then
      begin

          case MessageDlg('Ser� gerado um invent�rio para TODOS OS ESTOQUES. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

      end
      else
      begin

          case MessageDlg('Confirma a abertura do invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

      end ;

      qryMaster.Post ;
  end ;

end;

procedure TfrmInventario_Abertura.qryItemInventarioBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItemInventariocNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Informe o local de estoque.') ;
      abort ;
  end ;

  if (qryItemInventariocNmProduto.Value = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      abort ;
  end ;

  if (qryItemInventarionCdLocalEstoque.Value <> qryMasternCdlocalEstoque.Value) and (qryMasternCdLocalEstoque.Value > 0) then
  begin
      MensagemAlerta('O Local de estoque n�o pode ser diferente do local selecionado na abertura.') ;
      ShowMessage('Utilize o local C�digo: ' + qryMasternCdLocalEstoque.asString + ' - ' + qryLocalEstoquecNmLocalEstoque.AsString) ; 
      abort ;
  end ;

  if (qryItemInventarioiItem.Value <= 0) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT IsNull(MAX(iItem),0)+1 FROM ItemInventario WHERE nCdInventario = ' + qryMasternCdInventario.AsString) ;
      qryAux.Open ;

      if not qryAux.eof then
          qryItemInventarioiItem.Value := qryAux.FieldList[0].Value ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nQtdeDisponivel FROM PosicaoEstoque WHERE nCdProduto = ' + qryItemInventarionCdProduto.AsString + ' AND nCdLocalEstoque = ' + qryItemInventarionCdLocalEstoque.AsString) ;
  qryAux.Open ;

  if not qryAux.Eof then
      qryItemInventarionQtdeLogica.Value := qryAux.FieldList[0].Value ;

  qryAux.Close ;

end;

procedure TfrmInventario_Abertura.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (DbEdit11.Text = '') then
            begin
                MensagemAlerta('Informe a empresa.') ;
                Dbedit2.SetFocus ;
                exit ;
            end ;

            if (DBEdit14.Text = '') then
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + Dbedit2.Text)
            else nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + Dbedit2.Text + ' AND LocalEstoque.nCdGrupoEstoque = ' + DbEdit7.Text) ;

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmInventario_Abertura.DBEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(85) ;

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmInventario_Abertura.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryEmpresa.Close ;
  qryUsuarioAbre.Close ;
  qryUsuarioFech.Close ;
  qryGrupoEstoque.Close ;
  qryLocalEstoque.Close ;
  qryItemInventario.Close ;

end;

procedure TfrmInventario_Abertura.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasterdDtEstoque.Value := Now() ;
end;

initialization
    RegisterClass(TfrmInventario_Abertura) ;
    
end.
