unit fAprovacaoMapaCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid,
  DBGridEhGrouping;

type
  TfrmAprovacaoMapaCompra = class(TfrmCadastro_Padrao)
    qryMasternCdMapaCompra: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmMapaCompra: TStringField;
    qryMasterdDtGeracao: TDateTimeField;
    qryMasternCdUsuarioGerador: TIntegerField;
    qryMasternCdTerceiroVenc: TIntegerField;
    qryMasternCdTipoRazaoEscolha: TIntegerField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtAutor: TDateTimeField;
    qryMastercOBS: TMemoField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryUsuarioGer: TADOQuery;
    qryUsuarioGernCdUsuario: TIntegerField;
    qryUsuarioGercNmUsuario: TStringField;
    qryUsuarioAutor: TADOQuery;
    qryUsuarioAutornCdUsuario: TIntegerField;
    qryUsuarioAutorcNmUsuario: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoRazaoEscolha: TADOQuery;
    qryTipoRazaoEscolhanCdTipoRazaoEscolha: TIntegerField;
    qryTipoRazaoEscolhacNmRazaoEscolha: TStringField;
    qryUsuarioCancel: TADOQuery;
    qryUsuarioCancelnCdUsuario: TIntegerField;
    qryUsuarioCancelcNmUsuario: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    qryFornMapaCompra: TADOQuery;
    qryFornMapaCompranCdFornMapaCompra: TAutoIncField;
    qryFornMapaCompranCdMapaCompra: TIntegerField;
    qryFornMapaCompranCdTerceiro: TIntegerField;
    qryFornMapaCompracNmFornecedor: TStringField;
    qryFornMapaCompracNmContato: TStringField;
    qryFornMapaCompracTelefone: TStringField;
    qryFornMapaCompracEmail: TStringField;
    dsFornMapaCompra: TDataSource;
    dsItem: TDataSource;
    qryItens: TADOQuery;
    SP_ATUALIZA_DADOS_MAPA: TADOStoredProc;
    qryFornMapaCompranValTotalCotacao: TBCDField;
    qryFornMapaCompranCdFornVenc: TIntegerField;
    qryAux: TADOQuery;
    ToolButton10: TToolButton;
    qryUsuario: TADOQuery;
    qryUsuarionValAlcadaCompra: TBCDField;
    SP_APROVA_MAPA_COMPRA: TADOStoredProc;
    ToolButton13: TToolButton;
    qryItensiItem: TIntegerField;
    qryItenscCdProduto: TStringField;
    qryItenscNmItem: TStringField;
    qryItensnQtde: TBCDField;
    qryItensnCdFornecVenc: TIntegerField;
    qryItensnCdTipoRazaoEscolha: TIntegerField;
    qryItensnValUnitarioFinal: TBCDField;
    qryItensnValTotalFinal: TBCDField;
    qryItensnCdItemMapaCompra: TAutoIncField;
    qryItenscNmRazaoEscolha: TStringField;
    qryItensnPercIPI: TBCDField;
    qryItensnPercICMSSub: TBCDField;
    qryPopulaTemp: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    GroupBox1: TGroupBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    DBGridEh2: TDBGridEh;
    GroupBox4: TGroupBox;
    DBMemo1: TDBMemo;
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure qryFornMapaCompraAfterScroll(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItensCalcFields(DataSet: TDataSet);
    procedure qryItensBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAprovacaoMapaCompra: TfrmAprovacaoMapaCompra;

implementation

uses fMenu, fLookup_Padrao, rMapaCompra;

{$R *.dfm}

procedure TfrmAprovacaoMapaCompra.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MAPACOMPRA' ;
  nCdTabelaSistema  := 60 ;
  nCdConsultaPadrao := 132 ;

end;

procedure TfrmAprovacaoMapaCompra.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
  PosicionaQuery(qryUsuarioGer, qryMasternCdUsuarioGerador.AsString) ;
  PosicionaQuery(qryUsuarioAutor, qryMasternCdUsuarioAutor.AsString) ;
  PosicionaQuery(qryUsuarioCancel, qryMasternCdUsuarioCancel.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiroVenc.AsString) ;
  PosicionaQuery(qryTipoRazaoEscolha, qryMasternCdTipoRazaoEscolha.AsString) ;

  btSalvar.Enabled   := True ;
  DBGridEh2.ReadOnly := False ;

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      btSalvar.Enabled := False ;
      DBGridEh2.ReadOnly := True ;
  end ;

  PosicionaQuery(qryFornMapaCompra, qryMasternCdMapaCompra.asString) ;

  cmdPreparaTemp.Execute;

  qryPopulaTemp.Close ;
  qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMasternCdMapaCompra.Value ;
  qryPopulaTemp.ExecSQL ;

  qryItens.Close ;
  qryItens.Open ;

end;

procedure TfrmAprovacaoMapaCompra.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  qryFornMapaCompra.Close ;
  qryItens.Close ;

end;

procedure TfrmAprovacaoMapaCompra.qryFornMapaCompraAfterScroll(DataSet: TDataSet);
begin
  inherited;

//  PosicionaQuery(qryItens, qryFornMapaCompranCdFornMapaCompra.AsString) ;
end;

procedure TfrmAprovacaoMapaCompra.btSalvarClick(Sender: TObject);
begin

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa somente para consulta.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString = '') and (qryMasterdDtCancel.AsString = '') then
  begin

      frmMenu.Connection.Begintrans ;

      try
          SP_ATUALIZA_DADOS_MAPA.Close ;
          SP_ATUALIZA_DADOS_MAPA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  inherited;

  qryFornMapaCompra.Close ;
  qryItens.Close ;

end;

procedure TfrmAprovacaoMapaCompra.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;

  if (dsItem.State <> dsBrowse) and not DbGridEh2.ReadOnly then
  begin
      qryItensnValTotalFinal.Value := qryItensnValUnitarioFinal.Value * qryItensnQtde.Value ;
  end ;

end;

procedure TfrmAprovacaoMapaCompra.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGer.Close ;
  qryUsuarioAutor.Close ;
  qryUsuarioCancel.Close ;
  qryTerceiro.Close ;
  qryTipoRazaoEscolha.Close ;
  qryLoja.Close ;

  qryFornMapaCompra.Close ;
  qryItens.Close ;

end;

procedure TfrmAprovacaoMapaCompra.qryItensCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItensnCdTipoRazaoEscolha.Value > 0) then
  begin
      PosicionaQuery(qryTipoRazaoEscolha, qryItensnCdTipoRazaoEscolha.AsString) ;

      if not qryTipoRazaoEscolha.Eof then
          qryItenscNmRazaoEscolha.Value := qryTipoRazaoEscolhacNmRazaoEscolha.Value ;
          
  end ;
end;

procedure TfrmAprovacaoMapaCompra.qryItensBeforePost(DataSet: TDataSet);
begin
  inherited;


  if (qryItensnValUnitarioFinal.Value < 0) then
  begin
      MensagemAlerta('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  qryItensnValTotalFinal.Value := qryItensnValUnitarioFinal.Value * qryItensnQtde.Value ;
  
  if (qryItensnCdFornecVenc.Value > 0) and (qryItensnValUnitarioFinal.Value = 0) then
  begin
      MensagemAlerta('Informe o valor final da compra.') ;
      abort ;
  end ;

  if (qryItensnCdFornecVenc.Value = 0) then
  begin
      MensagemAlerta('Informe o c�digo do mapa do fornecedor vencedor.') ;
      abort ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM FornMapaCompra WHERE nCdMapaCompra = ' + qryMasternCdMapaCompra.AsString + ' AND nCdFornVenc = ' + qryItensnCdFornecVenc.AsString) ;
  qryAux.Open ;

  if (qryAux.Eof) then
  begin
      qryAux.Close ;
      MensagemAlerta('C�digo do Mapa do Fornecedor n�o encontrado.') ;
      abort ;
  end ;

  qryAux.Close ;

end;

procedure TfrmAprovacaoMapaCompra.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (DbGridEh2.Col = 6) then
      begin

        if (qryItens.State = dsBrowse) then
             qryItens.Edit ;

        if (qryItens.State = dsInsert) or (qryItens.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(133);

            If (nPK > 0) then
            begin
                qryItensnCdTipoRazaoEscolha.Value := nPK ;
            end ;

        end ;

      end ;

    end ;

  end ;

end;

procedure TfrmAprovacaoMapaCompra.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhum mapa de compra ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString <> '') or (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Mapa j� foi finalizado.') ;
      exit ;
  end ;

  qryItens.First ;

  while not qryItens.eof do
  begin

      if (qryItensnCdFornecVenc.Value = 0) then
      begin
          MensagemAlerta('Existe Item sem fornecedor vencedor selecionado. verifique.') ;
          exit ;
      end ;

      if (qryItensnValUnitarioFinal.Value <= 0) then
      begin
          MensagemAlerta('Existe Item sem o valor unit�rio final informado. verifique.') ;
          exit ;
      end ;

      qryItens.Next ;
  end ;

  qryItens.First ;

  PosicionaQuery(qryUsuario, intToStr(frmMenu.nCdUsuarioLogado)) ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT IsNull(Sum(nValTotalFinal),0) FROM #Temp_Dados_Aprovacao') ;
  qryAux.Open ;

  if (qryAux.FieldList[0].Value > qryUsuarionValAlcadaCompra.Value) then
  begin
      qryaux.Close ;
      MensagemAlerta('Valor da compra superior ao seu limite de aprova��o.') ;
      exit ;
  end ;

  qryaux.Close ;

  case MessageDlg('Confirma a aprova��o deste mapa de compras ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_APROVA_MAPA_COMPRA.Close ;
      SP_APROVA_MAPA_COMPRA.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
      SP_APROVA_MAPA_COMPRA.ExecProc;

      qryMaster.Edit ;
      qryMasterdDtAutor.Value        := Now() ;
      qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;
      qryMaster.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Mapa aprovado!') ;
  btCancelar.Click;

end;

procedure TfrmAprovacaoMapaCompra.ToolButton13Click(Sender: TObject);
var
   objRel : TrptMapaCompra;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione um mapa.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString = '') and (qryMasterdDtCancel.AsString = '') then
  begin
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;
  end ;

  objRel := TrptMapaCompra.Create(nil);

  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
          objRel.usp_Relatorio.Open ;

          objRel.SPREL_MAPA_HEADER.Close ;
          objRel.SPREL_MAPA_HEADER.Parameters.ParamByName('@nCdMapaCompra').Value := qryMasternCdMapaCompra.Value ;
          objRel.SPREL_MAPA_HEADER.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.QuickRep1.Preview;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TfrmAprovacaoMapaCompra) ;

end.
