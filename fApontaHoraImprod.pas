unit fApontaHoraImprod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, ER2Lookup;

type
  TfrmApontaHoraImprod = class(TfrmCadastro_Padrao)
    qryMasternCdHoraImprodutivaPCP: TIntegerField;
    qryMasternCdCentroProdutivo: TIntegerField;
    qryMasternCdTipoMaquinaPCP: TIntegerField;
    qryMasternCdOperadorPCP: TIntegerField;
    qryMasternCdMotivoHoraImprodutiva: TIntegerField;
    qryMasterdDtInicial: TDateTimeField;
    qryMastercHoraInicial: TStringField;
    qryMasterdDtFinal: TDateTimeField;
    qryMastercHoraFinal: TStringField;
    qryMasternTempoTotal: TBCDField;
    qryMasternCdUsuario: TIntegerField;
    qryMasterdDtApontamento: TDateTimeField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBMemo1: TDBMemo;
    qryCentroProdutivo: TADOQuery;
    dsCentroProdutivo: TDataSource;
    edtCentroProdutivo: TER2LookupDBEdit;
    edtTipoMaquina: TER2LookupDBEdit;
    edtOperadorPCP: TER2LookupDBEdit;
    qryTipoMaquina: TADOQuery;
    qryTipoMaquinacNmTipoMaquinaPCP: TStringField;
    DBEdit2: TDBEdit;
    dsTipoMaquina: TDataSource;
    qryOperadorPCP: TADOQuery;
    qryOperadorPCPcNmOperadorPCP: TStringField;
    DBEdit14: TDBEdit;
    dsOperadorPCP: TDataSource;
    qryMotivoHoraImprod: TADOQuery;
    edtMotivoHoraImprod: TER2LookupDBEdit;
    dsMotivoHoraImprod: TDataSource;
    edtUsuarioApontamento: TER2LookupDBEdit;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    DBEdit16: TDBEdit;
    dsUsuario: TDataSource;
    qryCentroProdutivocNmCentroProdutivo: TStringField;
    DBEdit3: TDBEdit;
    qryMotivoHoraImprodcNmMotivoHoraImprodutiva: TStringField;
    DBEdit4: TDBEdit;
    qryCalculaDifTempo: TADOQuery;
    qryCalculaDifTemponTempoTotal: TBCDField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  frmApontaHoraImprod: TfrmApontaHoraImprod;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmApontaHoraImprod.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'HORAIMPRODUTIVAPCP' ;
  nCdTabelaSistema  := 426 ;
  nCdConsultaPadrao := 1023 ;
end;

procedure TfrmApontaHoraImprod.btIncluirClick(Sender: TObject);
begin
  inherited;
  edtCentroProdutivo.setFocus;
end;

procedure TfrmApontaHoraImprod.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o centro produtivo.') ;
      edtCentroProdutivo.SetFocus;
      abort ;
  end ;

  
  inherited;

  qryMasternCdUsuario.Value     := frmMenu.nCdUsuarioLogado;
  qryMasterdDtApontamento.Value := Now;

end;

procedure TfrmApontaHoraImprod.DBEdit9Exit(Sender: TObject);
var
  DataIni : String;
  DataFin : String;

begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin

      if (trim(qryMastercHoraFinal.Value) = ':') then
      begin
          MensagemAlerta('Informe a hora final da parada.') ;
          qryMastercHoraFinal.Value := '00:00' ;
          DBEdit9.SetFocus;
          abort ;
      end ;

      if (length(trim(qryMastercHoraFinal.Value)) <> 5) then
      begin
          MensagemAlerta('Hora final Inv�lida.' +#13#13 + 'Exemplo: 15:45 / 16:20') ;
          qryMastercHoraFinal.Value := '00:00' ;
          DBEdit9.SetFocus;
          abort ;
      end ;

      if (strToInt(copy(qryMastercHoraFinal.Value,1,2)) < 0) or (strToInt(copy(qryMastercHoraFinal.Value,1,2)) > 23) then
      begin
          MensagemAlerta('Hora final inv�lida.');
          DBEdit9.SetFocus;
          abort ;
      end ;

      if (strToInt(copy(qryMastercHoraFinal.Value,4,2)) < 0) or (strToInt(copy(qryMastercHoraFinal.Value,4,2)) > 59) then
      begin
          MensagemAlerta('Minuto final inv�lido.');
          DBEdit9.SetFocus;
          abort ;
      end ;

      if (trim(qryMastercHoraInicial.Value) = ':') then
      begin
          MensagemAlerta('Informe a hora inicial da parada.') ;
          qryMastercHoraInicial.Value := '00:00' ;
          DBEdit7.SetFocus;
          abort ;
      end ;

      if (length(trim(qryMastercHoraInicial.Value)) <> 5) then
      begin
          MensagemAlerta('Hora Inicial Inv�lida.' +#13#13 + 'Exemplo: 15:45 / 16:20') ;
          qryMastercHoraInicial.Value := '00:00' ;
          DBEdit7.SetFocus;
          abort ;
      end ;


      if (strToInt(copy(qryMastercHoraInicial.Value,1,2)) < 0) or (strToInt(copy(qryMastercHoraInicial.Value,1,2)) > 23) then
      begin
          MensagemAlerta('Hora inicial inv�lida.');
          DBEdit7.SetFocus;
          abort ;
      end ;

      if (strToInt(copy(qryMastercHoraInicial.Value,4,2)) < 0) or (strToInt(copy(qryMastercHoraInicial.Value,4,2)) > 59) then
      begin
          MensagemAlerta('Minuto inicial inv�lido.');
          DBEdit7.SetFocus;
          abort ;
      end ;

      if (qryMasterdDtInicial.AsString = '') then
      begin
          MensagemAlerta('Informe a data inicial da parada.') ;
          DBEdit6.SetFocus;
          abort ;
      end ;

      if (qryMasterdDtFinal.AsString = '') then
      begin
          MensagemAlerta('Informe a data final da parada.') ;
          DBEdit8.SetFocus;
          abort ;
      end ;

      DataIni := DBEdit6.Text + ' ' + DBEdit7.Text ;
      DataFin := DBEdit8.Text + ' ' + DBEdit9.Text ;

      qryCalculaDifTempo.Close;
      qryCalculaDifTempo.Parameters.ParamByName('cDtInicial').Value := DataIni;
      qryCalculaDifTempo.Parameters.ParamByName('cDtFinal').Value   := DataFin;
      qryCalculaDifTempo.Open;

      if not qryCalculaDifTempo.eof then
          qryMasternTempoTotal.Value := qryCalculaDifTemponTempoTotal.Value ;

      qryCalculaDifTempo.Close;

  end ;

end;

procedure TfrmApontaHoraImprod.DBEdit8Exit(Sender: TObject);
begin
  inherited;
  if (qryMasterdDtFinal.Value < qryMasterdDtInicial.Value) then
  begin
      ShowMessage('Data final menor que data inicial') ;
      DbEdit6.SetFocus ;
      abort ;
  end;
end;

procedure TfrmApontaHoraImprod.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
  begin

      if (trim(qryMastercHoraInicial.Value) = ':') then
      begin
          MensagemAlerta('Informe a hora inicial da parada.') ;
          qryMastercHoraInicial.Value := '00:00' ;
          DBEdit7.SetFocus;
          abort ;
      end ;

      if (length(trim(qryMastercHoraInicial.Value)) <> 5) then
      begin
          MensagemAlerta('Hora Inicial Inv�lida.' +#13#13 + 'Exemplo: 15:45 / 16:20') ;
          qryMastercHoraInicial.Value := '00:00' ;
          DBEdit7.SetFocus;
          abort ;
      end ;


      if (strToInt(copy(qryMastercHoraInicial.Value,1,2)) < 0) or (strToInt(copy(qryMastercHoraInicial.Value,1,2)) > 23) then
      begin
          MensagemAlerta('Hora inicial inv�lida.');
          DBEdit7.SetFocus;
          abort ;
      end ;

      if (strToInt(copy(qryMastercHoraInicial.Value,4,2)) < 0) or (strToInt(copy(qryMastercHoraInicial.Value,4,2)) > 59) then
      begin
          MensagemAlerta('Minuto inicial inv�lido.');
          DBEdit7.SetFocus;
          abort ;
      end ;

  end ;

end;

procedure TfrmApontaHoraImprod.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryCentroProdutivo.Close;
  qryTipoMaquina.Close;
  qryOperadorPCP.Close;
  qryMotivoHoraImprod.Close;

  PosicionaQuery(qryCentroProdutivo, qryMasternCdCentroProdutivo.asString) ;
  PosicionaQuery(qryTipoMaquina, qryMasternCdTipoMaquinaPCP.AsString) ;
  PosicionaQuery(qryOperadorPCP, qryMasternCdOperadorPCP.AsString) ;
  PosicionaQuery(qryMotivoHoraImprod, qryMasternCdMotivoHoraImprodutiva.AsString) ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;


end;

procedure TfrmApontaHoraImprod.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryCentroProdutivo.Close;
  qryTipoMaquina.Close;
  qryOperadorPCP.Close;
  qryMotivoHoraImprod.Close;
  qryUsuario.Close;

end;

initialization
    RegisterClass(TfrmApontaHoraImprod) ;

end.
