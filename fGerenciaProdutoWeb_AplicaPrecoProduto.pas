unit fGerenciaProdutoWeb_AplicaPrecoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, dxCntner, dxEditor, dxExEdtr, dxEdLib,
  StdCtrls, DB, ADODB, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmGerenciaProdutoWeb_AplicaPrecoProduto = class(TfrmProcesso_Padrao)
    qryAplicaPreco: TADOQuery;
    Label2: TLabel;
    dxCurValVendaPor: TdxCurrencyEdit;
    dxCurValVendaDe: TdxCurrencyEdit;
    Label1: TLabel;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     nCdTabTipoProduto : Integer;
  end;

var
  frmGerenciaProdutoWeb_AplicaPrecoProduto: TfrmGerenciaProdutoWeb_AplicaPrecoProduto;

implementation

uses Math, StrUtils;

{$R *.dfm}

procedure TfrmGerenciaProdutoWeb_AplicaPrecoProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  { -- n�o permite informar pre�os menor que 0 -- }
  if (dxCurValVendaDe.Value < 0) then
  begin
      MensagemAlerta('Pre�o de venda (De) informado deve ser maior ou igual a zero.');
      dxCurValVendaDe.SetFocus;
      Abort;
  end;

  if (dxCurValVendaPor.Value < 0) then
  begin
      MensagemAlerta('Pre�o de venda (Por) informado deve ser maior ou igual a zero.');
      dxCurValVendaPor.SetFocus;
      Abort;
  end;

  { -- o pre�o de venda (de) n�o pode ser menor do que o pre�o de venda (por) -- }
  if (dxCurValVendaDe.Value < dxCurValVendaPor.Value) then
  begin
      MensagemAlerta('Pre�o de Venda (De) informado deve ser maior ou igual ao Pre�o de Venda (Por).');
      dxCurValVendaDe.SetFocus;
      Abort;
  end;

  if (MessageDLG('Os valores informados ser�o aplicados em todos os ' + IfThen(nCdTabTipoProduto=2,'produtos','sub itens') + ' selecionados.' + chr(13)
      + 'Confirma atualiza��o de pre�o de venda ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      qryAplicaPreco.Close;
      qryAplicaPreco.Parameters.ParamByName('nCdTipoProd').Value  := nCdTabTipoProduto;
      qryAplicaPreco.Parameters.ParamByName('nValVendaDe').Value  := dxCurValVendaDe.Value;
      qryAplicaPreco.Parameters.ParamByName('nValVendaPor').Value := dxCurValVendaPor.Value;
      qryAplicaPreco.ExecSQL;
      
      Close;
  end;
end;

end.
