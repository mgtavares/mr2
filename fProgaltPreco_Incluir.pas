unit fProgaltPreco_Incluir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, ADODB, DBCtrls;

type
  TfrmProgaltPreco_Incluir = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    edtnCdProduto: TMaskEdit;
    Label5: TLabel;
    Label3: TLabel;
    edtDtProgramada: TMaskEdit;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonValVenda: TBCDField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    edtNovoPreco: TMaskEdit;
    Label4: TLabel;
    SP_INCLUI_PROG_ALT_PRECO: TADOStoredProc;
    edtNovoCusto: TMaskEdit;
    Label1: TLabel;
    qryProdutonCdTabTipoProduto: TIntegerField;
    Label6: TLabel;
    Label7: TLabel;
    procedure edtNovoPrecoExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtnCdProdutoExit(Sender: TObject);
    procedure edtDtProgramadaEnter(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure edtnCdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtDtProgramadaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProgaltPreco_Incluir: TfrmProgaltPreco_Incluir;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmProgaltPreco_Incluir.edtNovoPrecoExit(Sender: TObject);
begin
  inherited;
  
  If (trim(edtNovoPreco.Text) <> '') then
      try
          edtNovoPreco.text := formatcurr('#0.00',StrToFloat(StringReplace(edtNovoPreco.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;
      except
          raise ;
          exit ;
      end ;

end;

procedure TfrmProgaltPreco_Incluir.FormShow(Sender: TObject);
begin
  inherited;

  qryProduto.Close;

  RadioGroup1.ItemIndex := 0 ;
  edtNovoCusto.Text     := '' ;
  edtNovoPreco.Text     := '' ;
  edtDtProgramada.Text  := '' ;
  edtnCdProduto.Text    := '' ;

  edtnCdProduto.SetFocus;

end;

procedure TfrmProgaltPreco_Incluir.edtnCdProdutoExit(Sender: TObject);
begin
  inherited;

  qryProduto.Close;
  qryProduto.Parameters.ParamByName('nCdTabTipoProduto').Value := (RadioGroup1.ItemIndex + 1) ;
  PosicionaQuery(qryProduto, edtnCdProduto.Text) ;
  
end;

procedure TfrmProgaltPreco_Incluir.edtDtProgramadaEnter(Sender: TObject);
begin
  inherited;

  if (Trim(edtDtProgramada.Text) = '/  /') then
      edtDtProgramada.Text := DateToStr(Date+1) ;
      
end;

procedure TfrmProgaltPreco_Incluir.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  qryProduto.Close;
  edtnCdProduto.Text := '' ;
  edtnCdProduto.SetFocus;
  
end;

procedure TfrmProgaltPreco_Incluir.edtnCdProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (RadioGroup1.ItemIndex = 0) then
            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdTabTipoProduto = 1 AND cFlgProdVenda = 1');

        if (RadioGroup1.ItemIndex = 1) then
            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdTabTipoProduto = 2 AND cFlgProdVenda = 1');

        if (RadioGroup1.ItemIndex = 2) then
            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdTabTipoProduto = 3 AND cFlgProdVenda = 1');

        If (nPK > 0) then
            edtnCdProduto.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmProgaltPreco_Incluir.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o produto.') ;
      edtnCdProduto.SetFocus;
      exit ;
  end ;

  if (trim(edtNovoPreco.Text) = '') then
      edtNovoPreco.Text := '0' ;

  if (trim(edtNovoCusto.Text) = '') then
      edtNovoCusto.Text := '0' ;

  try
      StrToFloat(edtNovoPreco.Text) ;
  except
      MensagemAlerta('Valor de venda inv�lido ou n�o informado.') ;
      edtNovoPreco.SetFocus;
      exit ;
  end ;

  try
      StrToFloat(edtNovoCusto.Text) ;
  except
      MensagemAlerta('Valor de custo inv�lido ou n�o informado.') ;
      edtNovoCusto.SetFocus;
      exit ;
  end ;

  if (strToFloat(edtNovoPreco.Text) < 0) then
  begin
      MensagemAlerta('Valor de venda inv�lido ou n�o informado.') ;
      edtNovoPreco.SetFocus;
      exit ;
  end ;

  if (strToFloat(edtNovoCusto.Text) < 0) then
  begin
      MensagemAlerta('Valor de custo inv�lido ou n�o informado.') ;
      edtNovoCusto.SetFocus;
      exit ;
  end ;

  if (strToFloat(edtNovoPreco.Text) = 0) and (strToFloat(edtNovoCusto.Text) = 0) then
  begin
      MensagemAlerta('Informe o pre�o de custo e/ou o pre�o de venda.') ;
      edtNovoPreco.SetFocus;
      exit ;
  end ;

  if (strToFloat(edtNovoCusto.Text) > 0) and (qryProdutonCdTabTipoProduto.Value = 3) then
  begin
      MensagemAlerta('A altera��o de pre�o de custo s� pode ser feita no produto nivel 2 ou 1.') ;
      exit ;
  end ;

  if (trim(edtDtProgramada.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de atualiza��o do pre�o.') ;
      edtDtProgramada.SetFocus;
      exit ;
  end ;

  if (StrToDate(edtDtProgramada.Text) <= Date) then
  begin

      if (MessageDlg('O valor de venda deste produto ser� alterado agora. Deseja Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      begin
          edtDtProgramada.SetFocus;
          exit ;
      end ;

  end ;


  if (MessageDlg('Confirma a inclus�o desta altera��o de pre�o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
  begin
      edtnCdProduto.SetFocus;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_INCLUI_PROG_ALT_PRECO.Close;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@nCdProduto').Value        := qryProdutonCdProduto.Value ;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@dDtProgramada').Value     := edtDtProgramada.Text;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@nValVendaAnterior').Value := qryProdutonValVenda.Value ;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@nValNovoPreco').Value     := StrToFloat(edtNovoPreco.Text) ;
      SP_INCLUI_PROG_ALT_PRECO.Parameters.ParamByName('@nValNovoCusto').Value     := StrToFloat(edtNovoCusto.Text) ;
      SP_INCLUI_PROG_ALT_PRECO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryProduto.Close;

  RadioGroup1.ItemIndex := 0 ;
  edtNovoPreco.Text     := '' ;
  edtDtProgramada.Text  := '' ;
  edtnCdProduto.Text    := '' ;
  edtNovoCusto.Text     := '' ;

  edtnCdProduto.SetFocus;

end;

procedure TfrmProgaltPreco_Incluir.edtDtProgramadaExit(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
  
end;

end.
