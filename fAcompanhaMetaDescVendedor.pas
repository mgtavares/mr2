unit fAcompanhaMetaDescVendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxPC, cxControls, DBGridEhGrouping, ToolCtrlsEh, StdCtrls, Mask,
  DBCtrls, GridsEh, DBGridEh, DBCtrlsEh, cxLookAndFeelPainters, cxButtons,
  ER2Lookup;

type
  TfrmAcompanhaMetaDescVendedor = class(TfrmProcesso_Padrao)
    qryVendedor: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsLoja: TDataSource;
    dsEmpresa: TDataSource;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    qryVendedorcFlgGerente: TIntegerField;
    dsVendedor: TDataSource;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
    Label3: TLabel;
    edtMesAno: TMaskEdit;
    qryMasternCdMetaVendedorDesconto: TIntegerField;
    qryMasternValCotaDesconto: TBCDField;
    qryMasternCdPedido: TIntegerField;
    qryMasternValPedido: TBCDField;
    qryMasternValDescontoUtil: TBCDField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasternValDescontoAnt: TBCDField;
    qryMasternValDescontoPost: TBCDField;
    er2LkpEmpresa: TER2LookupMaskEdit;
    er2LkpLoja: TER2LookupMaskEdit;
    er2LkpVendedor: TER2LookupMaskEdit;
    qryVendedorcFlgVendedor: TIntegerField;
    qryVendedornCdTerceiroResponsavel: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure prCalcSaldo();
    procedure er2LkpEmpresaExit(Sender: TObject);
    procedure er2LkpLojaExit(Sender: TObject);
    procedure er2LkpVendedorExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    cflgVendedorAtivo : Boolean;
  public
    { Public declarations }
  end;

var
  frmAcompanhaMetaDescVendedor: TfrmAcompanhaMetaDescVendedor;


implementation

uses
  fMenu, fLookup_Padrao, DateUtils;

{$R *.dfm}

procedure TfrmAcompanhaMetaDescVendedor.FormShow(Sender: TObject);
begin
  inherited;

  cflgVendedorAtivo := False;

  er2LkpEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva));

  er2LkpLoja.Text := IntToStr(frmMenu.nCdLojaAtiva);
  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nPK').Value        := frmMenu.nCdLojaAtiva;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Open;

  er2LkpVendedor.WhereAdicional.Text := 'EXISTS(SELECT 1 FROM UsuarioLoja WHERE UsuarioLoja.nCdUsuario = Usuario.nCdUsuario AND UsuarioLoja.nCdLoja = ' + Trim(er2LkpLoja.Text) + ')';
  er2LkpVendedor.SetFocus;

  { -- se usu�rio ativo for vendedor, bloqueia visualiza��o de desconto de outros vendedores -- }
  if (frmMenu.cFlgUsuarioVendedor = 1) and (frmMenu.cFlgUsuarioGerente = 0) then
  begin
      cflgVendedorAtivo   := True;
      er2LkpVendedor.Text := IntToStr(frmMenu.nCdUsuarioLogado);

      edtMesAno.SetFocus;
      desativaER2LkpMaskEdit(er2LkpEmpresa);
      desativaER2LkpMaskEdit(er2LkpLoja);
      desativaER2LkpMaskEdit(er2LkpVendedor);

      qryVendedor.Close;
      qryVendedor.Parameters.ParamByName('nPK').Value     := frmMenu.nCdUsuarioLogado;
      qryVendedor.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
      qryVendedor.Open;
  end;
end;

procedure TfrmAcompanhaMetaDescVendedor.er2LkpEmpresaExit(
  Sender: TObject);
begin
  inherited;

  if (Trim(er2LkpEmpresa.Text) = '') then
  begin
      qryEmpresa.Close;
      qryLoja.Close;
      qryVendedor.Close;
      er2LkpLoja.Clear;
      er2LkpVendedor.Clear;
      Exit;
  end;

  PosicionaQuery(qryEmpresa, er2LkpEmpresa.Text);
end;

procedure TfrmAcompanhaMetaDescVendedor.er2LkpLojaExit(
  Sender: TObject);
begin
  inherited;

  if (Trim(er2LkpLoja.Text) = '') then
  begin
      qryLoja.Close;
      qryVendedor.Close;
      er2LkpVendedor.Clear;
      Exit;
  end;

  if ((Trim(er2LkpLoja.Text) <> '') and (Trim(DBEdit4.Text) = '')) then
  begin
      MensagemAlerta('Informe a empresa.');
      er2LkpLoja.Clear;
      er2LkpEmpresa.SetFocus;
      Abort;
  end;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nPK').Value        :=  StrToInt(Trim(er2LkpLoja.Text));
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Open;

  er2LkpVendedor.WhereAdicional.Text := 'EXISTS(SELECT 1 FROM UsuarioLoja WHERE UsuarioLoja.nCdUsuario = Usuario.nCdUsuario AND UsuarioLoja.nCdLoja = ' + Trim(er2LkpLoja.Text) + ')';
end;

procedure TfrmAcompanhaMetaDescVendedor.er2LkpVendedorExit(
  Sender: TObject);
begin
  inherited;

  if (Trim(er2LkpVendedor.Text) = '') then
      Exit;

  if ((Trim(er2LkpVendedor.Text) <> '') and (Trim(DBEdit2.Text) = '')) then
  begin
      MensagemAlerta('Informe a loja.');
      er2LkpVendedor.Clear;
      er2LkpLoja.SetFocus;
      Abort;
  end;

  qryVendedor.Close;
  qryVendedor.Parameters.ParamByName('nPK').Value     := StrToInt(Trim(er2LkpVendedor.Text));
  qryVendedor.Parameters.ParamByName('nCdLoja').Value := StrToInt(Trim(er2LkpLoja.Text));
  qryVendedor.Open;
end;

procedure TfrmAcompanhaMetaDescVendedor.ToolButton1Click(Sender: TObject);
begin
  inherited;
  prCalcSaldo;
end;

procedure TfrmAcompanhaMetaDescVendedor.prCalcSaldo();
var
  nValUtilizado  : Double;
  nValDisponivel : Double;
  iMes           : Integer;
  iAno           : Integer;
begin
  if (Trim(DBEdit4.Text) = '') then
  begin
      MensagemAlerta('Informe a empresa.');
      er2LkpEmpresa.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a loja.');
      er2LkpLoja.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit6.Text) = '') then
  begin
      MensagemAlerta('Informe o vendedor.');
      er2LkpVendedor.SetFocus;
      Abort;
  end;

  if (edtMesAno.Text = '  /    ') then
  begin
      edtMesAno.Text := frmMenu.ZeroEsquerda(IntToStr(MonthOf(Date)),2) + '/' + IntToStr(YearOf(Date));
  end;

  try
      StrToDate('01/' + edtMesAno.Text);
  except
      MensagemErro('M�s/Ano informado � inv�lido, o valor deve seguir o formato MM/AAAA.');
      edtMesAno.SetFocus;
      Abort;
  end;

  DBGridEh1.Columns[5].Footers[0].Value := '0,00';
  DBGridEh1.Columns[5].Footers[1].Value := '0,00';
  DBGridEh1.Columns[5].Footers[2].Value := '0,00';

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  qryMaster.Close;
  qryMaster.Parameters.ParamByName('nCdTerceiro').Value := qryVendedornCdTerceiroResponsavel.Value;
  qryMaster.Parameters.ParamByName('iMes').Value        := iMes;
  qryMaster.Parameters.ParamByName('iAno').Value        := iAno;
  qryMaster.Open;

  if (qryMaster.IsEmpty) then
  begin
      ShowMessage('Nenhum registro encontrado.');
      edtMesAno.SetFocus;
      Abort;
  end;

  nValUtilizado  := 0;
  nValDisponivel := 0;

  {-- Faz a somat�ria do desconto utilizado --}
  while not(qryMaster.Eof) do
  begin
      nValUtilizado := nValUtilizado + qryMasternValDescontoUtil.Value;
      qryMaster.Next;
  end;

  {-- Valor dispon�vel --}
  nValDisponivel := qryMasternValCotaDesconto.Value - nValUtilizado;

  DBGridEh1.Columns[5].Footers[0].Value := FormatFloat('#,##0.00', qryMasternValCotaDesconto.Value);
  DBGridEh1.Columns[5].Footers[1].Value := FormatFloat('#,##0.00', nValUtilizado);
  DBGridEh1.Columns[5].Footers[2].Value := FormatFloat('#,##0.00', nValDisponivel);
end;

initialization
  RegisterClass(TfrmAcompanhaMetaDescVendedor) ;
  
end.

