unit fInventarioLoja_Contagem_ContagemItemLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  GridsEh, DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfrmInventarioLoja_Contagem_ContagemItemLote = class(TfrmProcesso_Padrao)
    lblNumeroLote: TLabel;
    qryAux: TADOQuery;
    ToolButton8: TToolButton;
    ToolButton4: TToolButton;
    btLerArquivo: TToolButton;
    dlgSelecionaArquivoTxt: TOpenDialog;
    btValidarLista: TToolButton;
    btLimparLista: TToolButton;
    qryValidaItemLote: TADOQuery;
    qryProcessaLote: TADOQuery;
    qryExcluiItemLote: TADOQuery;
    DBGridEh1: TDBGridEh;
    qryPopulaTemp: TADOQuery;
    qryTempContagemItemLote: TADOQuery;
    qryTempContagemItemLotenCdItem: TAutoIncField;
    qryTempContagemItemLotecCdProduto: TStringField;
    qryTempContagemItemLotenCdProduto: TIntegerField;
    qryTempContagemItemLotecNmProduto: TStringField;
    qryTempContagemItemLotecMsg: TStringField;
    qryTempContagemItemLotenCdItemLoteContagemInv: TIntegerField;
    dsTempContagemItemLote: TDataSource;
    cmdPreparaTemp: TADOCommand;
    procedure FormShow(Sender: TObject);
    procedure btLerArquivoClick(Sender: TObject);
    procedure btValidarListaClick(Sender: TObject);
    procedure btLimparListaClick(Sender: TObject);
    procedure qryTempContagemItemLoteBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLoteContagemInv : Integer;
    ArquivoTXT         : TextFile;
  end;

var
  frmInventarioLoja_Contagem_ContagemItemLote: TfrmInventarioLoja_Contagem_ContagemItemLote;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmInventarioLoja_Contagem_ContagemItemLote.FormShow(Sender: TObject);
begin
  inherited;

  Self.Caption := 'Contagem de Lote - ' + lblNumeroLote.Caption;
  DBGridEh1.SetFocus;

  if (nCdLoteContagemInv = 0) then
  begin
      MensagemErro('C�digo do lote n�o informado.');
      Close;
  end;

  cmdPreparaTemp.Execute;

  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdLoteContagemInventario').Value := nCdLoteContagemInv;
  qryPopulaTemp.ExecSQL;

  qryTempContagemItemLote.Close;
  qryTempContagemItemLote.Open;
end;

procedure TfrmInventarioLoja_Contagem_ContagemItemLote.qryTempContagemItemLoteBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  { -- exclui item do lote -- }
  qryExcluiItemLote.Close;
  qryExcluiItemLote.Parameters.ParamByName('cAcao').Value                         := 'I';
  qryExcluiItemLote.Parameters.ParamByName('nCdLoteContagemInventario').Value     := nCdLoteContagemInv;
  qryExcluiItemLote.Parameters.ParamByName('nCdItemLoteContagemInventario').Value := qryTempContagemItemLotenCdItemLoteContagemInv.Value;
  qryExcluiItemLote.ExecSQL;
end;

procedure TfrmInventarioLoja_Contagem_ContagemItemLote.btLerArquivoClick(
  Sender: TObject);
var
  Linha : string;
  Qtde  : integer;
  i     : integer;
begin
  if not(dlgSelecionaArquivoTxt.Execute) then
      Abort;

  AssignFile(ArquivoTXT, dlgSelecionaArquivoTxt.FileName);

  if (trim(dlgSelecionaArquivoTxt.FileName)<>'')then
  begin
      try
          qryTempContagemItemLote.DisableControls;
          Reset(ArquivoTXT);

          while not (Eof(ArquivoTXT)) do
          begin
              Readln(ArquivoTXT, Linha);
              frmMenu.mensagemUsuario('Lendo item ' + Linha + '...');

              try
                  if (Pos(',',Linha) > 0) then
                  begin
                      Qtde  := StrToInt(Copy(Linha, Pos(',', Linha) + 1, Length(Linha) - 1));
                      Linha := Copy(Linha, 0, Pos(',', Linha) - 1);
                  end
                  else
                      Qtde := 1;
              except
                  MensagemErro('Estrutura de arquivo inv�lido.' + #13 + 'Layout 1: [CodProduto]' + #13 + 'Layout 2: [CodProduto,QtdProduto]');
                  Exit;
              end;

              for i := 1 to Qtde do
              begin
                  qryTempContagemItemLote.Insert;
                  qryTempContagemItemLotecCdProduto.Value := Linha;
                  qryTempContagemItemLote.Post;
              end;
          end;
      finally
          qryTempContagemItemLote.EnableControls;
          qryTempContagemItemLote.Requery();
          frmMenu.mensagemUsuario('');
          CloseFile(ArquivoTXT);
      end;
  end;
end;

procedure TfrmInventarioLoja_Contagem_ContagemItemLote.btValidarListaClick(
  Sender: TObject);
begin
  inherited;

  if (qryTempContagemItemLote.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto informado no lote ativo.');
      Exit;
  end;

  if (MessageDlg('Confirma a valida��o deste lote ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      Exit;

  try
      try
          frmMenu.mensagemUsuario('Validando lote...');
          qryValidaItemLote.Close;
          qryValidaItemLote.Parameters.ParamByName('nCdLoteContagemInventario').Value := nCdLoteContagemInv;
          qryValidaItemLote.Open;

          if (qryValidaItemLote.RecordCount > 0) then
          begin
              MensagemAlerta('Existem ' + IntToStr(qryValidaItemLote.RecordCount) + ' itens com problema de valida��o, verifique a mensagem de resposta para obter mais detalhes.');
              Exit;
          end;

          try
              frmMenu.mensagemUsuario('Processando lote...');

              frmMenu.Connection.BeginTrans;

              qryProcessaLote.Close;
              qryProcessaLote.Parameters.ParamByName('nCdLoteContagemInventario').Value := nCdLoteContagemInv;
              qryProcessaLote.ExecSQL;

              frmMenu.Connection.CommitTrans;

              ShowMessage('Lote validado com sucesso.');
              Close;
          except
              frmMenu.Connection.RollbackTrans;
          end;
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      qryTempContagemItemLote.Requery();
      frmMenu.mensagemUsuario('');
  end;
end;

procedure TfrmInventarioLoja_Contagem_ContagemItemLote.btLimparListaClick(
  Sender: TObject);
begin
  inherited;

  if (qryTempContagemItemLote.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto informado no lote ativo.');
      Exit;
  end;

  if (MessageDlg('Confirma a exclus�o de todos os itens da lista ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      Exit;

  try
      qryExcluiItemLote.Close;
      qryExcluiItemLote.Parameters.ParamByName('cAcao').Value                         := 'T';
      qryExcluiItemLote.Parameters.ParamByName('nCdLoteContagemInventario').Value     := nCdLoteContagemInv;
      qryExcluiItemLote.Parameters.ParamByName('nCdItemLoteContagemInventario').Value := 0;
      qryExcluiItemLote.ExecSQL;

      qryPopulaTemp.Close;
      qryPopulaTemp.Parameters.ParamByName('nCdLoteContagemInventario').Value := nCdLoteContagemInv;
      qryPopulaTemp.ExecSQL;

      qryTempContagemItemLote.Requery();
  except
      MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

end.
