unit rPosicaoVale_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoVale_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_POSICAO_VALE: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRDBText1: TQRDBText;
    QRShape6: TQRShape;
    QRLabel2: TQRLabel;
    SPREL_POSICAO_VALEnCdVale: TAutoIncField;
    SPREL_POSICAO_VALEnCdLoja: TStringField;
    SPREL_POSICAO_VALEdDtVale: TDateTimeField;
    SPREL_POSICAO_VALEcNmTabTipoVale: TStringField;
    SPREL_POSICAO_VALEcNmTerceiro: TStringField;
    SPREL_POSICAO_VALEnValVale: TFloatField;
    SPREL_POSICAO_VALEnSaldoVale: TFloatField;
    SPREL_POSICAO_VALEnCdContaCaixaEmissao: TStringField;
    SPREL_POSICAO_VALEnCdLanctoFin: TIntegerField;
    SPREL_POSICAO_VALEdDtBaixa: TDateTimeField;
    SPREL_POSICAO_VALEnCdContaCaixaBaixa: TStringField;
    SPREL_POSICAO_VALEnCdLanctoFinBaixa: TIntegerField;
    SPREL_POSICAO_VALEcFlgRecomprado: TStringField;
    SPREL_POSICAO_VALEcFlgLiqVenda: TStringField;
    QRDBText6: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText12: TQRDBText;
    QRExpr1: TQRExpr;
    QRLabel18: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape5: TQRShape;
    QRLabel15: TQRLabel;
    SPREL_POSICAO_VALEcCodigo: TStringField;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText14: TQRDBText;
    SPREL_POSICAO_VALEcNmTerceiroColab: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoVale_view: TrptPosicaoVale_view;

implementation

{$R *.dfm}

end.
