unit fPosTituloAberto_Cliente_Simplificado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TfrmPosTituloAberto_Cliente_Simplificado = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    DataSource3: TDataSource;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label8: TLabel;
    DataSource5: TDataSource;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    RadioGroup1: TRadioGroup;
    Label9: TLabel;
    MaskEdit4: TMaskEdit;
    DBEdit1: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    MaskEdit3: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPosTituloAberto_Cliente_Simplificado: TfrmPosTituloAberto_Cliente_Simplificado;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rPosTituloAberto,
  rPosTituloAberto_Cliente_Simplificado;

{$R *.dfm}

procedure TfrmPosTituloAberto_Cliente_Simplificado.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  MaskEdit7.Text := '1' ;

  qryMoeda.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    qryMoeda.Parameters.ParamByName('nCdMoeda').Value := MaskEdit7.Text ;
    qryMoeda.Open ;
  end ;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit4.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit4.text) ;
  end
  else
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;

end;


procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin
    qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEspTit.Parameters.ParamByName('nPk').Value := MaskEdit5.Text ;
    qryEspTit.Open ;
  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMoeda.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    qryMoeda.Parameters.ParamByName('nCdMoeda').Value := MaskEdit7.Text ;
    qryMoeda.Open ;
  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.ToolButton1Click(Sender: TObject);
var
  objRel : TrptPosTituloAberto_cliente_simplificado ;
begin

  objRel := TrptPosTituloAberto_cliente_simplificado.Create(Self) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEspTit').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdMoeda').Value    := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'D' ;

  if (RadioGroup1.ItemIndex = 1) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'C' ;

  if (DBEdit1.Text = '') then MaskEdit4.Text := '';
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value       := 0
  else objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(MaskEdit4.Text) ;

  objRel.usp_Relatorio.Open  ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if ((Trim(DbEdit3.Text)) <> '')  then
      objRel.lblFiltro1.Caption := 'Empresa : ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

  if ((Trim(MaskEdit5.Text)) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Esp�cie : ' + Trim(MaskEdit5.Text) + '-' + DbEdit7.Text ;

  if ((Trim(MaskEdit6.Text)) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Terceiro: ' + Trim(MaskEdit6.Text) + '-' + DbEdit10.Text ;

  if ((Trim(MaskEdit7.Text)) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Moeda   : ' + Trim(MaskEdit7.Text) + '-' + DbEdit13.Text ;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo : ' + Trim(MaskEdit1.Text) + '-' + Trim(MaskEdit2.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo    : PAGAR' ;

  if (RadioGroup1.ItemIndex = 1) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo    : RECEBER' ;

  if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit4.Text)) <> '') ) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Loja : ' + trim(MaskEdit4.Text) + '-' + DbEdit1.Text;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
    TipoAcesso : String;
begin
  inherited;

  case key of
    vk_F4 : begin

       TipoAcesso := #39 + 'L' + #39;
       nPK := frmLookup_Padrao.ExecutaConsulta2(10,'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')');

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TfrmPosTituloAberto_Cliente_Simplificado.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit4.Enabled := False ;

end;

initialization
     RegisterClass(TfrmPosTituloAberto_Cliente_Simplificado) ;

end.
