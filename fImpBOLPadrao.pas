unit fImpBOLPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, ComObj, Classes, Graphics, Controls, Forms,
  Dialogs, RDprint, DB, ADODB, cxLookAndFeelPainters, StdCtrls, cxButtons, Printers;

type
  TfrmImpBOLPadrao = class(TForm)
    qryModImpBOL: TADOQuery;
    qryAux: TADOQuery;
    qryResultado: TADOQuery;
    qryCampos: TADOQuery;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    RDprint1: TRDprint;
    qryModImpBOLnCdModImpBOL: TIntegerField;
    qryModImpBOLcNmModImpBOL: TStringField;
    qryModImpBOLnCdBanco: TIntegerField;
    qryModImpBOLcQuery: TMemoField;
    qryCamposnCdCampoModImpBOL: TAutoIncField;
    qryCamposnCdModImpBOL: TIntegerField;
    qryCamposiSequencia: TIntegerField;
    qryCamposcNmCampo: TStringField;
    qryCamposcDescricao: TStringField;
    qryCamposiLinha: TIntegerField;
    qryCamposiColuna: TIntegerField;
    qryCamposcTipoDado: TStringField;
    qryCamposcTamanhoFonte: TStringField;
    qryCamposcEstiloFonte: TStringField;
    qryCamposcLPP: TStringField;
    qryContaBancaria: TADOQuery;
    qryEndereco: TADOQuery;
    qryTitulo: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacCedenteAgencia: TStringField;
    qryContaBancariacCedenteAgenciaDigito: TStringField;
    qryContaBancariacCedenteConta: TStringField;
    qryContaBancariacCedenteContaDigito: TStringField;
    qryContaBancariacCedenteCodCedente: TStringField;
    qryContaBancariacDiretorioArquivoLicenca: TStringField;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulodDtEmissao: TDateTimeField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryEndereconCdEndereco: TIntegerField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEnderecocCEP: TStringField;
    qryInstrucaoContaBancaria: TADOQuery;
    qryInstrucaoContaBancarianCdTituloInstrucaoBoleto: TAutoIncField;
    qryInstrucaoContaBancariacNmMensagem: TStringField;
    qryInstrucaoContaBancariacFlgMensagemPadrao: TIntegerField;
    qryContaBancariacPrimeiraInstrucaoBoleto: TStringField;
    qryContaBancariacSegundaInstrucaoBoleto: TStringField;
    qryContaBancariaiUltimoBoleto: TIntegerField;
    qryEnderecocNmTerceiro: TStringField;
    qryEnderecocCNPJCPF: TStringField;
    qryContaBancariacFlgAceite: TIntegerField;
    qryTitulocFlgBoletoImpresso: TIntegerField;
    qryTabelaJuros: TADOQuery;
    qryTabelaJurosnPercMulta: TBCDField;
    qryTabelaJurosnPercJuroDia: TBCDField;
    qryContaBancariaiDiasProtesto: TIntegerField;
    procedure ImprimeLinha(iLinha: Integer; iColuna: Integer; cTipoDado: String ;cValorCampo: String; cLPP: String; cEstiloFonte: String; cTamanhoFonte: String);
    procedure ImprimirBoleto();
    procedure cxButton1Click(Sender: TObject);
    procedure emissaoBoleto(arrID: array of integer; nCdTabTipoModBoleto : integer;  nCdBanco: integer);
    procedure emissaoBoletoA4(arrID: array of integer; nCdContaBancaria : integer);
  private
    { Private declarations }
    function TrataMascara(Mascara, Valor: String): String;
  public
    { Public declarations }
    nCdDoctoFiscal : Integer ;
    nCdBanco       : integer ;
    iSeqProximoNum : integer ;
  end;

var
  frmImpBOLPadrao: TfrmImpBOLPadrao;

implementation

uses fMenu, StrUtils;

{$R *.dfm}

procedure TfrmImpBOLPadrao.ImprimeLinha(iLinha: Integer; iColuna: Integer; cTipoDado: String ;cValorCampo: String; cLPP: String; cEstiloFonte: String; cTamanhoFonte: String);
begin

                if cLPP = 'SEIS' then
                    rdPrint1.TamanhoQteLPP := seis ;

                if cLPP = 'OITO' then
                    rdPrint1.TamanhoQteLPP := oito ;

                if cTipoDado = 'C' then
                begin

                    if cEstiloFonte = 'NORMAL' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20]) ;
                    end ;

                    if cEstiloFonte = 'ITALICO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[italico]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido,italico]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12,italico]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17,italico]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20,italico]) ;
                    end ;

                    if cEstiloFonte = 'NEGRITO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[NEGRITO]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20,NEGRITO]) ;
                    end ;

                end ;


                if cTipoDado = 'I' then
                begin


                    if cEstiloFonte = 'NORMAL' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20]) ;

                    end ;

                    if cEstiloFonte = 'ITALICO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[italico]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido,italico]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12,italico]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17,italico]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20,italico]) ;
                    end ;

                    if cEstiloFonte = 'NEGRITO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[NEGRITO]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20,NEGRITO]) ;
                    end ;



                end ;


                if cTipoDado = 'V' then
                begin

                    if cEstiloFonte = 'NORMAL' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[expandido]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP12]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP17]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP20]) ;

                    end ;

                    if cEstiloFonte = 'ITALICO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[italico]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[expandido,italico]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP12,italico]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP17,italico]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP20,italico]) ;

                    end ;

                    if cEstiloFonte = 'NEGRITO' then
                    begin

                        if cTamanhoFonte = 'NORMAL' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[NEGRITO]) ;

                        if cTamanhoFonte = 'EXPANDIDO' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[expandido,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP12' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP12,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP17' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP17,NEGRITO]) ;

                        if cTamanhoFonte = 'COMP20' then
                            rdPrint1.ImpD(iLinha, iColuna, FormatFloat('###,##0.00',StrToFloat(cValorCampo)),[COMP20,NEGRITO]) ;
                    end ;

                end ;


end ;

procedure TfrmImpBOLPadrao.ImprimirBoleto();
var
    i       : integer ;
    iLinha  : integer ;
    iMaximo : integer ;
    iItens  : integer ;
begin

    {qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT cValor FROM ParametroEmpresa WHERE cParametro = ' + Chr(39) + 'MODPADBOL' + Chr(39) + ' AND nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva)) ;
    qryAux.Fields.Clear;
    qryAux.Open ;

    if (qryAux.FieldList[0].Value = 0) then
    begin
        qryAux.Close ;
        ShowMessage('Nenhum modelo de impress�o de boleto ativo. Atualize o parametro empresa MODPADBOL.') ;
        exit ;
    end ;}

    // localiza o modelo de impress�o
    qryModImpBOL.Close ;
    qryModImpBOL.Parameters.ParamByName('nPK').Value := nCdBanco ;
    qryModImpBOL.Open ;

    if (qryModImpBol.eof) then
    begin
        ShowMessage('Modelo de impress�o de boleto n�o configurado para o banco ' + IntToStr(nCdBanco)) ;
        abort ;
    end ;

    // processa as informa��es do cabe�alho

    rdPrint1.Abrir ;
    rdPrint1.OpcoesPreview.CaptionPreview := 'Boleto Banc�rio';

    RDprint1.Orientacao := poLandscape;

    if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
    begin

        rdprint1.OpcoesPreview.Preview := true ;
        rdprint1.OpcoesPreview.Remalina:= true ;
        rdprint1.OpcoesPreview.PaginaZebrada:= true ;

    end ;


    // obtem a query do cabe�alho
    if (qryModImpBOLcQuery.Value <> '') then
    begin

        // monta a query dinamica e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpBOLcQuery.AsString) ;
        qryResultado.Parameters.ParamByName('nPK').Value := nCdDoctoFiscal ;

        qryResultado.Open ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryModImpBOLnCdModImpBOL.Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                   ImprimeLinha(qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value);

                qryCampos.Next;
            end ;

            qryResultado.Next;

            if not qryResultado.eof then
            begin
                rdPrint1.Novapagina;
            end ;

        end ;

    end ;

    qryCampos.Close ;
    qryResultado.Close ;
    qryAux.Close ;
    qryModImpBol.Close ;

    rdPrint1.Fechar ;

end ;

procedure TfrmImpBOLPadrao.cxButton1Click(Sender: TObject);
begin

    nCdDoctoFiscal := 98 ;
    ImprimirBoleto;


end;

procedure TfrmImpBOLPadrao.emissaoBoleto(arrID: array of integer;
  nCdTabTipoModBoleto: integer; nCdBanco: integer);
var
    i       : integer ;
    iLinha  : integer ;
    iMaximo : integer ;
    iItens  : integer ;
    iIDs,iCampos   : integer ;
begin

    // localiza o modelo de impress�o
    qryModImpBOL.Close ;
    qryModImpBOL.Parameters.ParamByName('nPK').Value := nCdBanco ;
    qryModImpBOL.Open ;

    if (qryModImpBol.eof) then
    begin
        ShowMessage('Modelo de impress�o de boleto n�o configurado para o banco ' + IntToStr(nCdBanco)) ;
        abort ;
    end ;

    // processa as informa��es do cabe�alho

    rdPrint1.Abrir ;
    rdPrint1.OpcoesPreview.CaptionPreview := 'Boleto Banc�rio';

    RDprint1.Orientacao := poLandscape;

    if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
    begin

        rdprint1.OpcoesPreview.Preview := true ;
        rdprint1.OpcoesPreview.Remalina:= true ;
        rdprint1.OpcoesPreview.PaginaZebrada:= true ;

    end ;


    // obtem a query do cabe�alho
    if (qryModImpBOLcQuery.Value <> '') then
    begin

        {-- fecha um loop em todas as entradas do array --}
        for iIDs := 0 to Length(arrID)-1 do
        begin

            // monta a query dinamica e recupera o resultado
            qryResultado.Close ;
            qryResultado.Fields.Clear ;
            qryResultado.SQL.Clear ;
            qryResultado.SQL.Add(qryModImpBOLcQuery.AsString) ;
            qryResultado.Parameters.ParamByName('nPK').Value := (arrID[iIDs]); //nCdDoctoFiscal ;

            qryResultado.Open ;

            while not qryResultado.eof do
            begin

                // recupera os campos que dever�o ser impressos no cabe�alho
                qryCampos.Close ;
                qryCampos.Parameters.ParamByName('nPK').Value    := qryModImpBOLnCdModImpBOL.Value ;
                qryCampos.Open ;

                while not qryCampos.eof do
                begin

                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value);

                    qryCampos.Next;
                end ;

                qryResultado.Next;

                if not qryResultado.eof then
                begin
                    rdPrint1.Novapagina;
                end ;

            end ;

            qryResultado.Close ;
            rdPrint1.Novapagina;
        end;
    end ;

    rdPrint1.Fechar ;

end;

procedure TfrmImpBOLPadrao.emissaoBoletoA4(arrID: array of integer;
  nCdContaBancaria: integer);
var
   CobreBemX      : Variant;
   Boleto         : Variant;
   iTotalTitulo   : integer;
   cDemonstrativo : TStringList;
begin

    qryContaBancaria.Close;
    qryContaBancaria.Parameters.ParamByName('nPK').Value := nCdContaBancaria;
    qryContaBancaria.Open;

    qryTabelaJuros.Close;
    qryTabelaJuros.Open;

    if (qryTabelaJuros.Eof) then
    begin
        frmMenu.MensagemAlerta('Tabela de juros n�o cadastrada ou vencida para a esp�cie de Titulo de Boleto, verifique.');
        abort;
    end;

    qryInstrucaoContaBancaria.Close;
    qryInstrucaoContaBancaria.Open;

    cDemonstrativo := TStringList.Create;

    while not qryInstrucaoContaBancaria.Eof do
    begin
        cDemonstrativo.Append(qryInstrucaoContaBancariacNmMensagem.Value + '<br>');

        qryInstrucaoContaBancaria.Next;
    end;

    if not DirectoryExists(ExtractFilePath(Application.ExeName) + 'Boleto\Imagens\') then
    begin
        frmMenu.MensagemAlerta('"' + ExtractFilePath(Application.ExeName) + 'Boleto\Imagens". Diret�rio de imagens n�o encontrado.');
        abort; 
    end;

    CobreBemX := CreateOleObject('CobreBemX.ContaCorrente');

    try
        CobreBemX.ArquivoLicenca      := qryContaBancariacDiretorioArquivoLicenca.Value;
        CobreBemX.CodigoAgencia       := TrataMascara(CobreBemX.MascaraCodigoAgencia, qryContaBancariacCedenteAgencia.Value + qryContaBancariacCedenteAgenciaDigito.Value);
        CobreBemX.NumeroContaCorrente := TrataMascara(CobreBemX.MascaraContaCorrente, qryContaBancariacCedenteConta.Value + qryContaBancariacCedenteContaDigito.Value);
        CobreBemX.CodigoCedente       := TrataMascara(CobreBemX.MascaraCodigoCedente, qryContaBancariacCedenteCodCedente.Value);
        CobreBemX.InicioNossoNumero   := '00000000001';
        CobreBemX.FimNossoNumero      := '02147483647';
        
        CobreBemX.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras := ExtractFilePath(Application.ExeName) + 'Boleto\Imagens\';
    except
        frmMenu.MensagemAlerta('Arquivo de Lincen�a inv�lido ou inexistente. Caminho: "' + qryContaBancariacDiretorioArquivoLicenca.Value + '"');
        abort;
    end;

    for iTotalTitulo := 0 to Length(arrID) - 1 do
    begin

        qryTitulo.Close;
        qryTitulo.Parameters.ParamByName('nPK').Value := arrID[iTotalTitulo];
        qryTitulo.Open;

        qryEndereco.Close;
        qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryTitulonCdTerceiro.Value;
        qryEndereco.Open;

        Boleto                               := CobreBemX.DocumentosCobranca.Add;
        Boleto.NumeroDocumento               := qryTitulocNrTit.Value;
        Boleto.NomeSacado                    := qryEnderecocNmTerceiro.Value;
        Boleto.CPFSacado                     := qryEnderecocCNPJCPF.Value;
        Boleto.EnderecoSacado                := qryEnderecocEndereco.Value;
        Boleto.BairroSacado                  := qryEnderecocBairro.Value;
        Boleto.CidadeSacado                  := qryEnderecocCidade.Value;
        Boleto.EstadoSacado                  := qryEnderecocUF.Value;
        Boleto.CepSacado                     := qryEnderecocCEP.Value;
        Boleto.DataDocumento                 := qryTitulodDtEmissao.AsString;
        Boleto.DataVencimento                := qryTitulodDtVenc.AsString;
        Boleto.ValorDocumento                := qryTitulonValTit.Value;
        Boleto.NossoNumero                   := qryTitulonCdTitulo.Value;
        Boleto.CalculaDacNossoNumero         := True;
        Boleto.PercentualJurosDiaAtraso      := qryTabelaJurosnPercJuroDia.Value;
        Boleto.PercentualMultaAtraso         := qryTabelaJurosnPercMulta.Value;
        Boleto.DiasProtesto                  := qryContaBancariaiDiasProtesto.Value;

        Boleto.PadroesBoleto.InstrucoesCaixa := '<font size="1">'
                                               + qryContaBancariacPrimeiraInstrucaoBoleto.Value + '<br>'
                                               + qryContaBancariacSegundaInstrucaoBoleto.Value + '<br>'
                                               + cDemonstrativo.Text + '</font>';

        Boleto.TipoDocumentoCobranca         := 'DM';

        if (qryContaBancariacFlgAceite.Value = 1) then
            Boleto.Aceite := 'S';
    end;

    CobreBemX.ImprimeBoletos;

    CobreBemX := Unassigned;
end;

function TfrmImpBOLPadrao.TrataMascara(Mascara, Valor: String): String;
var
    Count        : integer;
    PSeparador   : integer;
    DV           : String;
    ContaAgencia : String;
begin

    if (Trim(Mascara) <> '') then
    begin

        PSeparador := Pos('-',Mascara);

        if (PSeparador > 0) then
        begin

            Count := Length(Mascara) - PSeparador;

            DV           := '-' + Copy(ReverseString(Valor),0, Count);
            ContaAgencia := Copy(Valor,0,Length(Valor) - Count);

            Count := Length(Copy(Mascara,0,PSeparador - 1));

            if (Length(ContaAgencia) < Count) then
                ContaAgencia := frmMenu.ZeroEsquerda(ContaAgencia,Count)
            else if (Length(ContaAgencia) > Count) then
                ContaAgencia := ReverseString(Copy(ReverseString(ContaAgencia),0,Count));

            Result := ContaAgencia + DV;
        end else
        begin

            SetLength(Valor,Length(Mascara));
            
            Result := Valor;

        end;

    end else
        Result := Valor;
end;

initialization
    RegisterClass(TfrmImpBOLPadrao) ;

end.
