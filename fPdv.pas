unit fPdv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, GridsEh,
  DBGridEh, DB, Mask, DBCtrls, ADODB, ComCtrls, cxControls, cxContainer,
  cxListView, Buttons, jpeg, ImgList, ToolWin, Menus, RDprint, DBClient,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxTextEdit, cxCurrencyEdit, cxCheckBox, GIFImage, acbrUtil, cxImage,
  cxImageComboBox;

type
  TfrmPDV = class(TForm)
    qryPedido: TADOQuery;
    dsPedido: TDataSource;
    SP_PDV_NOVO_PRODUTO: TADOStoredProc;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidonValProdutos: TBCDField;
    qryPedidonValDesconto: TBCDField;
    qryPedidonValDevoluc: TBCDField;
    qryPedidonValValeMerc: TBCDField;
    qryPedidonValValePres: TBCDField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidonCdTerceiroColab: TIntegerField;
    qryPedidonValPedido: TBCDField;
    qryPedidocNmTerceiro: TStringField;
    qryVendedor: TADOQuery;
    qryVendedorcNmUsuario: TStringField;
    SP_PDV_ATU_CLI_CONDPAGTO: TADOStoredProc;
    SP_PDV_ENCERRA_PDV: TADOStoredProc;
    qryPedidonCdUsuarioVended: TIntegerField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryVendedornCdUsuario: TIntegerField;
    qryItemPedido: TADOQuery;
    qryItemPedidocCdProduto: TStringField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonCdTipoItemPed: TIntegerField;
    qryItemPedidonQtdePed: TBCDField;
    qryItemPedidonValUnitario: TBCDField;
    qryItemPedidonValTotalItem: TBCDField;
    dsVendedor: TDataSource;
    GroupBox2: TGroupBox;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    edtCondPagto: TEdit;
    Label5: TLabel;
    GroupBox4: TGroupBox;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdUsuario: TIntegerField;
    Label10: TLabel;
    edtVendedor: TEdit;
    DBEdit2: TDBEdit;
    btConsultaVend: TcxButton;
    ImageList1: TImageList;
    PopupMenu1: TPopupMenu;
    ExcluirItem1: TMenuItem;
    SP_PDV_EXCLUIR_ITEM: TADOStoredProc;
    RDprint1: TRDprint;
    qryPedidocNmUsuario: TStringField;
    qryItemPedidonCdProduto: TIntegerField;
    qryInseriTempPedido: TADOQuery;
    edtCPF: TMaskEdit;
    Label9: TLabel;
    qryCliente: TADOQuery;
    qryClientenCdTerceiro: TIntegerField;
    qryClientecNmTerceiro: TStringField;
    dsCliente: TDataSource;
    Label13: TLabel;
    btConsultaCli: TcxButton;
    Label14: TLabel;
    Label15: TLabel;
    edtCartao: TMaskEdit;
    edtNmCliente: TEdit;
    qryClientecRG: TStringField;
    qryClientecEmail: TStringField;
    qryClientecTelefone1: TStringField;
    qryClientecCNPJCPF: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    cdsItens: TClientDataSet;
    cdsItenscTipo: TStringField;
    cdsItensnCdProduto: TStringField;
    cdsItenscNmProduto: TStringField;
    cdsItensnQtde: TIntegerField;
    cdsItensnValUnitario: TFloatField;
    cdsItensnValTotal: TFloatField;
    dsItens: TDataSource;
    qryConsultaProdutoTroca: TADOQuery;
    qryConsultaProdutoTrocanQtdeExpRec: TBCDField;
    qryConsultaProdutoTrocacFlgTrocado: TIntegerField;
    qryConsultaProdutoTrocanValUnitario: TBCDField;
    qryConsultaProdutoTrocanCdItemPedido: TAutoIncField;
    qryConsultaProdutoTrocanCdProduto: TIntegerField;
    qryAmbiente: TADOQuery;
    qryAmbientecPortaMatricial: TStringField;
    qryAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryAmbientecNomeDLL: TStringField;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    lblNumPedido: TLabel;
    ComboOperacao: TComboBox;
    edtCodProduto: TEdit;
    edtQtde: TMaskEdit;
    btConsProd: TcxButton;
    edtNumPedido: TMaskEdit;
    DBEdit1: TDBEdit;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    Image1: TImage;
    imgPDV: TImage;
    btNovo: TcxButton;
    btSelCli: TcxButton;
    btSair: TcxButton;
    btReabre: TcxButton;
    btValePres: TcxButton;
    btValeMerc: TcxButton;
    btEncerra: TcxButton;
    btTroca: TcxButton;
    btCondPagto: TcxButton;
    btDesconto: TcxButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1cTipo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    qryPedidonValDescontoCondPagto: TBCDField;
    qryPedidonValJurosCondpagto: TBCDField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocFlgPermDesconto: TIntegerField;
    qryPedidonValSubTotal: TBCDField;
    cdsItenscFlgPromocional: TIntegerField;
    cxGrid1DBTableView1cFlgPromocional: TcxGridDBColumn;
    qryItemPedidocFlgPromocional: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutocEAN: TStringField;
    btConsProdDetalhe: TcxButton;
    qryPedidonValAcrescimo: TBCDField;
    qryAtualizaTerceiro: TADOQuery;
    qryPedidocFlgArredondaPrecoPDV: TIntegerField;
    qryProdutonCdTabTipoProduto: TIntegerField;
    qryItemPedidocFlgMovEstoqueConsignado: TIntegerField;
    cdsItenscFlgMovEstoqueConsignado: TIntegerField;
    qryBuscaConsignacao: TADOQuery;
    qryBuscaConsignacaonCdConsignacao: TIntegerField;
    qryAmbientecPathLogoPDV: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidocFlgBloqEstNeg: TIntegerField;
    qryPosicaoEstoque: TADOQuery;
    qryPosicaoEstoquenCdProduto: TIntegerField;
    qryPosicaoEstoquecNmProduto: TStringField;
    qryPosicaoEstoquenCdStatus: TIntegerField;
    qryPosicaoEstoquenQtdeDisp: TBCDField;
    Label2: TLabel;
    qryClientenCdGrupoClienteDesc: TIntegerField;
    qryClientecNmGrupoClienteDesc: TStringField;
    DBEdit5: TDBEdit;
    qryClientenPercDesconto: TBCDField;
    qryPedidonValDescontoGrupo: TBCDField;
    qryCondPagtoGrupoClienteDesc: TADOQuery;
    qryCondPagtoGrupoClienteDescnCdCondPagtoGrupoClienteDesc: TIntegerField;
    qryCondPagtoGrupoClienteDescnCdGrupoClienteDesc: TIntegerField;
    qryCondPagtoGrupoClienteDescnCdCondPagto: TIntegerField;
    qryCondPagtoGrupoClienteDescnPercDesconto: TBCDField;
    qryClientenCdStatus: TIntegerField;
    qryCondPagtoAcres: TADOQuery;
    qryCondPagtoAcresnPercAcrescimo: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
    procedure LocalizaProduto(cEAN: String);
    procedure IncluiLista(cAcao:String; cCod:String; cDescricao:String; iQtde:Integer; nValorUnit:double; nValorTotal:double ; nCdCampanhaPromoc:integer; cFlgMovEstoqueConsignado:integer) ;
    procedure edtVendedorExit(Sender: TObject);
    procedure btCondPagtoClick(Sender: TObject);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocaClick(Sender: TObject);
    procedure edtQtdeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btDescontoClick(Sender: TObject);
    procedure btValePresClick(Sender: TObject);
    procedure MensagemAlerta(cTexto: string);
    procedure ShowMessage(cTexto: string);
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure MensagemErro(cTexto: string);
    procedure btReabreClick(Sender: TObject);
    procedure btConsProdClick(Sender: TObject);
    procedure btValeMercClick(Sender: TObject);
    procedure btSelCliClick(Sender: TObject);
    procedure btConsultaVendClick(Sender: TObject);
    procedure btEncerraClick(Sender: TObject);
    procedure ExcluirItem1Click(Sender: TObject);
    procedure ImprimirPDV(nCdPedido : integer);
    procedure FormCreate(Sender: TObject);
    procedure edtCPFExit(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboCadastroChange(Sender: TObject);
    procedure ComboCadastroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConsultaCliClick(Sender: TObject);
    procedure edtCPFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtNmClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboOperacaoChange(Sender: TObject);
    procedure GeraValePresente(nValValePresente : double);
    procedure Mensagem(cMensagem:string);
    procedure btDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure btNovoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TrataTeclaFuncao(Key: Word);
    procedure btCondPagtoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btDescontoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btEncerraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btValePresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btValeMercKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btReabreKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSairKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TrataEnterProduto;
    procedure btConsProdDetalheClick(Sender: TObject);
    function AutorizacaoGerente(nCdTipoLiberacao:integer): boolean;
    procedure ValidaPosicaoEstoque();
  private
    { Private declarations }
    nPercMaxDescSupervisor : Double;
    cFlgBloqEstNeg         : Integer;
    cOBS                   : String;
    nCdUsuarioAutorPDV     : Integer;

  public
    { Public declarations }
    nCdVendedor      : integer ;
    nCdTerceiro      : integer ;
    nCdPedido        : integer ;
    nCdFormaPagto    : integer ;
    nCdCondPagto     : integer ;
    cOrigemCaixa     : integer ;
    cAcao            : string  ;
    cPedidoTroca     : string  ;
    cPedidoDefeito   : string  ;
    cAlteraFone      : string  ;
    cFlgPermDesconto : integer ;
    nCdCampanhaPromoc: integer ;
    bItemPromocao    : boolean ;
    iNrPedidoExterno : integer ;
end;

var
  frmPDV: TfrmPDV;

implementation

uses fMenu, fSelecServer, fLogin, fPdvFormaPagto, fPdvCondPagto,
  fPdvSupervisor, fPdvDesconto, fPdvReabrePedido, fPdvConsProduto,
  fPdvValeMerc, fCaixa_SelCliente, fLookup_Padrao, fCaixa_Recebimento,
  fCadastro_Template, fUtils, uPDV_Bematech_Termica, fPDVValePresente,
  fMensagem, fPDVConsVended, fPDVConsProdutoDetalhe;

label
    SelecionaCondPagto ;

{$R *.dfm}

function TfrmPDV.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmPDV.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
end;

procedure TfrmPDV.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
end;

procedure TfrmPDV.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
end;

procedure TfrmPDV.FormShow(Sender: TObject);
begin

    {if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.JPG') then
        imgPDV.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.JPG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.JPEG') then
        imgPDV.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.JPEG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.GIF') then
        imgPDV.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.GIF')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.BMP') then
        imgPDV.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDV.BMP')}

    qryAmbiente.Close;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open;

    if (Trim(qryAmbientecPathLogoPDV.Value) = '') then
    begin
        if (FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDVPadrao.JPG')) then
            imgPDV.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoPDVPadrao.JPG');
    end
    else
    begin
        if (FileExists(qryAmbientecPathLogoPDV.Value)) then
            imgPDV.Picture.LoadFromFile(qryAmbientecPathLogoPDV.Value);
    end;
    
    if (frmMenu.nCdUsuarioLogado = 0) then
    begin
        frmSelecServer.ShowModal ;

        frmLogin.ShowModal ;

        frmMenu.SelecionaEmpresa() ;

    end ;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    edtCondPagto.Text         := '' ;
    ComboOperacao.ItemIndex   := 0 ;

    if (frmMenu.LeParametro('VAREJO') <> 'S') then
    begin
        MensagemErro('O ER2Soft n�o est� configurado para funcionamento em modo de varejo.') ;
        exit ;
    end ;

    cPedidoTroca   := frmMenu.LeParametro('OBRPEDTROCPDV') ;
    cPedidoDefeito := frmMenu.LeParametro('OBRPEDDEFPDV')  ;
    cAlteraFone    := frmMenu.LeParametro('ALTFONEPDV') ;

    qryPedido.Close ;

    Mensagem(' ') ;

    edtCodProduto.MaxLength := StrToint(frmMenu.LeParametro('TMMXBRPRPDV')) ;

    cdsItens.CreateDataSet;
    cdsItens.EmptyDataSet;

    btCondPagto.Enabled := False ;
    btSelCli.Enabled    := False ;
    btDesconto.Enabled  := False ;
    btTroca.Enabled     := False ;
    btEncerra.Enabled   := False ;
    btValePres.Enabled  := False ;
    btValeMerc.Enabled  := False ;
    btConsProd.Enabled  := False ;

    edtVendedor.Enabled := False ;
    edtVendedor.Text    := '' ;
    edtQtde.Text        := '' ;
    edtCondPagto.Text   := '' ;

    qryVendedor.Close ;

    qryCliente.Close ;

    btNovo.Enabled         := True ;
    btConsultaVend.Enabled := False ;
    btCOnsultaCli.Enabled  := False ;

    edtCPF.Text       := '' ;
    edtNmCliente.Text := '' ;
    GroupBox4.Enabled := False ;

    btNovo.SetFocus;

end;

procedure TfrmPDV.btSairClick(Sender: TObject);
begin

    if (MessageDlg('Voc� est� com um pedido em aberto. Deseja realmente sair do Pr�-Venda ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
        exit ;

    Close ;
end;

procedure TfrmPDV.cxButton1Click(Sender: TObject);
begin

    if (qryPedido.Active) then
        if (MessageDlg('Deseja realmente fechar o Pr�-Venda ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
            exit ;

    Close ;
end;

procedure TfrmPDV.btNovoClick(Sender: TObject);
begin

    bItemPromocao := false ;

    Mensagem('Iniciando novo pedido...') ;

    qryPedido.Close ;
    qryPedido.Parameters.ParamByName('nPK').Value := 0 ;
    qryPedido.Open  ;

    nCdPedido := 0 ;

    qryPedido.Insert ;

    Mensagem(' ') ;

    btNovo.Enabled      := False ;
    btCondPagto.Enabled := True ;
    btSelCli.Enabled    := True ;
    btDesconto.Enabled  := True ;
    btTroca.Enabled     := True ;
    btEncerra.Enabled   := True ;
    btValePres.Enabled  := True ;
    btValeMerc.Enabled  := True ;
    btConsProd.Enabled  := True ;

    qryCliente.Close ;

    GroupBox1.Enabled   := True ;
    GroupBox4.Enabled   := True ;

    edtVendedor.Enabled := True ;

    edtCPF.Text       := '' ;
    edtNmCliente.Text := '' ;

    btConsultaVend.Enabled := True ;
    btCOnsultaCli.Enabled  := True ;

    cFlgPermDesconto:= 0 ;
    nCdCondpagto    := 0 ;

    edtVendedor.SetFocus;

end;

procedure TfrmPDV.LocalizaProduto(cEAN: String);
var
    cNmProduto:String ;
    nValProduto:Double ;
begin

    If (ComboOperacao.ItemIndex = 0) then
        cAcao := 'V'  ;

    If (ComboOperacao.ItemIndex = 1) then
        cAcao := 'T'  ;

    If (ComboOperacao.ItemIndex = 2) then
        cAcao := 'D'  ;

    { -- se tipo de pedido bloqueia estoque negativo, verifica saldo em estoque -- }
    if (cFlgBloqEstNeg = 1) and (cAcao = 'V') then
        ValidaPosicaoEstoque();

    { *** retirado devido j� ser verificado na procedure SP_PDV_NOVO_PRODUTO ***
    //verificar se o produto � de n�vel 3 --> Olivio
    qryProduto.Close;
    qryProduto.Parameters.ParamByName('nPK').Value := frmMenu.ConvInteiro(edtCodProduto.Text) ;
    qryProduto.Open;

    if not qryProduto.eof then
    begin
        if (qryProdutonCdTabTipoProduto.Value <=2) then
        begin
            MensagemAlerta('Produto informado n�o � do N�vel 3, por favor verifique!') ;
            qryProduto.Close;
            exit ;
        end;
    end;}

    if (frmMenu.ConvInteiro(edtQtde.Text) < 0) then
    begin
        MensagemAlerta('Quantidade Inv�lida.') ;
        exit ;
    end ;

    if (frmMenu.ConvInteiro(edtQtde.Text) = 0) then
    begin

        if qryPedido.Active and (qryPedidonValDesconto.Value > 0) then
        begin
            cAcao := 'V' ;
            ComboOperacao.ItemIndex   := 0 ;

            edtCodProduto.Text := '' ;
            edtQtde.Text       := '1' ;

            MensagemAlerta('Retire o desconto antes de efetuar a exclus�o.') ;
            exit ;
        end ;


        case MessageDlg('Confirma a exclus�o deste item?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo: begin
                edtCodProduto.Text := '' ;
                exit ;
            end ;
        end ;

        cAcao := 'E' ;

    end ;

    edtCodProduto.Text := '' ;

    Mensagem('Consultando banco de dados...') ;

    if (cAcao = 'T') or (cAcao = 'D') then
    begin
        // Consulta para ver se o produto que est� sendo trocado existe no pedido de venda
        if (frmMenu.ConvInteiro(edtNumpedido.Text) > 0) then
        begin

            Mensagem('Consultando o produto no pedido para efetuar a troca...') ;

            try
                qryConsultaProdutoTroca.Close ;
                qryConsultaProdutoTroca.Parameters.ParamByName('nCdPedido').Value  := frmMenu.ConvInteiro(edtNumPedido.Text) ;
                qryConsultaProdutoTroca.Parameters.ParamByName('cCdProduto').Value := cEAN ;
                qryConsultaProdutoTroca.Open ;
            except
                Mensagem('') ;
                raise ;
            end ;

            Mensagem('') ;

            if (qryConsultaProdutoTroca.Eof) then
            begin
                MensagemAlerta('Este produto n�o foi encontrado no pedido de venda n�mero ' + edtNumpedido.Text) ;
                edtNumPedido.SetFocus;
                exit ;
            end ;

            if (frmMenu.ConvInteiro(edtQtde.Text) > qryConsultaProdutoTrocanQtdeExpRec.Value) then
            begin
                MensagemAlerta('A quantidade comprada deste produto no pedido de venda � menor que a quantidade informada para troca.') ;
                edtQtde.SetFocus ;
                exit ;
            end ;

            if (qryConsultaProdutoTrocacFlgTrocado.Value = 1) then
            begin
                MensagemAlerta('J� foi registrada a troca deste produto para o pedido de venda informado.') ;
                edtNumpedido.SetFocus ;
                exit ;
            end ;

        end ;

    end ;

    frmMenu.Connection.BeginTrans;

    try

        SP_PDV_NOVO_PRODUTO.Close ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.nCdLojaAtiva ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiroColab').Value := nCdVendedor ;

        if not qryCliente.Eof then
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value  := qryClientenCdTerceiro.Value
        else SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value := 0 ;

        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value           := edtCPF.Text  ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNmTerceiro').Value        := edtNmCliente.Text ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNrCartao').Value          := edtCartao.Text ;

        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdCondPagto').Value       := nCdCondPagto ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdPedido').Value          := frmMenu.ConvInteiro(IntToStr(nCdPedido)) ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEAN').Value               := cEAN ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cAcao').Value              := cAcao ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nQtde').Value              := frmMenu.ConvInteiro(edtQtde.Text) ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTipoVenda').Value         := 'P' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdVale').Value            := 0 ;

        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdItemPedidoTroca').Value := 0 ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdProdutoTroca').Value    := 0 ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nValUnitarioTroca').Value  := 0 ;

        {-- Informa��es adicionais na autoriza��o de itens sem quantidades em estoque --}
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cOBS').Value               := cOBS;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdUsuarioAutorPDV').Value := nCdUsuarioAutorPDV;


        // informa��es adicionais da troca
        if (trim(edtNumpedido.Text) <> '') and (frmMenu.ConvInteiro(edtNumpedido.Text) > 0) and (not qryConsultaProdutoTroca.Eof) then
        begin
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdItemPedidoTroca').Value := qryConsultaProdutoTrocanCdItemPedido.Value ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdProdutoTroca').Value    := qryConsultaProdutoTrocanCdProduto.Value    ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nValUnitarioTroca').Value  := qryConsultaProdutoTrocanValUnitario.Value  ;
        end ;

        SP_PDV_NOVO_PRODUTO.Open ;

    except
        frmMenu.Connection.RollbackTrans;
        Mensagem(' ') ;
        MensagemErro('Erro no processamento.');
        raise ;
    end ;


    if (SP_PDV_NOVO_PRODUTO.FieldByName('cMsg').Value <> 'OK') then
    begin
        Mensagem(' ') ;
        cAcao := 'V' ;
        frmMenu.Connection.RollbackTrans;
        MensagemAlerta(SP_PDV_NOVO_PRODUTO.FieldList[0].Value) ;
        exit ;
    end ;

    frmMenu.Connection.CommitTrans;

    Mensagem(' ') ;

    cNmProduto        := SP_PDV_NOVO_PRODUTO.FieldByName('cNmproduto').Value ;
    nValProduto       := SP_PDV_NOVO_PRODUTO.FieldByName('nValVenda').Value ;
    nCdPedido         := SP_PDV_NOVO_PRODUTO.FieldByName('nCdPedido').Value ;
    nCdCampanhaPromoc := SP_PDV_NOVO_PRODUTO.FieldByName('nCdCampanhaPromoc').Value ;

    Mensagem('Atualizando pedido...') ;

    qryPedido.Close ;
    qryPedido.Parameters.ParamByName('nPK').Value := nCdPedido ;
    qryPedido.Open ;

    Mensagem(' ') ;

    IncluiLista(cAcao, cEAN, cNmProduto, StrToInt(edtQtde.Text), nValProduto, (nValProduto * StrToInt(edtQtde.Text)), nCdCampanhaPromoc, 0) ;

    cAcao := 'V' ;
    ComboOperacao.ItemIndex   := 0 ;

    edtCodProduto.Text := '' ;
    edtQtde.Text       := '1' ;

    if (edtCondPagto.Text <> '') then
    begin

        edtCondPagto.Text := '' ;
        nCdCondpagto      := 0  ;

        if ((qryPedidonValDescontoCondPagto.Value + qryPedidonValDesconto.Value) > 0) then
            MensagemAlerta('A condi��o de pagamento e o desconto ser�o removidos do pedido.')
        else
            MensagemAlerta('A condi��o de pagamento ser� removida do pedido. Selecione novamente para encerrar.') ;

        qryPedido.Edit ;
        qryPedidonValjurosCondpagto.Value    := 0 ;
        qryPedidonValDescontoCondPagto.Value := 0 ;
        qryPedidonValDesconto.Value          := 0 ;
        qryPedidonValDescontoGrupo.Value     := 0 ;

        qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value ;
        qryPedido.Post ;

    end ;


end;

procedure TfrmPDV.IncluiLista(cAcao:String; cCod:String; cDescricao:String; iQtde:Integer; nValorUnit:double; nValorTotal:double; nCdCampanhaPromoc:integer; cFlgMovEstoqueConsignado:integer) ;
var
    ListItem      : TListItem;
    nValFinal     : Double;
    nPercDesconto : Double;
begin
  // Adicionamos um Item ao dataset

  cdsItens.Insert;
  cdsItenscTipo.Value                    := cAcao;
  cdsItensnCdProduto.Value               := cCod;
  cdsItenscNmProduto.Value               := cDescricao;
  cdsItensnQtde.Value                    := iQtde;
  cdsItensnValUnitario.Value             := nValorUnit;
  cdsItensnValTotal.Value                := nValorTotal;
  cdsItenscFlgPromocional.Value          := 0;
  cdsItenscFlgMovEstoqueConsignado.Value := cFlgMovEstoqueConsignado;

  if (nCdCampanhaPromoc > 0) then
  begin
      cdsItenscFlgPromocional.Value := 1 ;
      bItemPromocao := true ;
  end ;

  cdsItens.Post ;

end ;

procedure TfrmPDV.edtVendedorExit(Sender: TObject);
begin
    { -- limpa nome vendedor -- }
    DBEdit2.Clear;

    if (Trim(edtVendedor.Text) = '') then
        exit ;

    Mensagem('Consultando banco de dados...') ;

    qryVendedor.Close ;
    qryVendedor.Parameters.ParamByName('nPK').Value := frmMenu.ConvInteiro(edtVendedor.Text) ;
    qryVendedor.Open ;

    Mensagem(' ') ;

    if (qryVendedor.eof) then
    begin
        MensagemAlerta('Vendedor n�o cadastrado') ;
        edtVendedor.Clear;
        edtVendedor.SetFocus;
        Exit;
    end;

    qryUsuarioLoja.Close ;
    qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := qryVendedornCdUsuario.Value ;
    qryUsuarioLoja.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
    qryUsuarioLoja.Open  ;

    if qryUsuarioLoja.eof then
    begin
        MensagemAlerta('Este vendedor n�o tem acesso nesta loja.') ;
        edtVendedor.Text := '' ;
        edtVendedor.SetFocus ;
        qryVendedor.Close ;
        exit ;
    end ;

    qryVendedor.First ;

    nCdVendedor := frmMenu.ConvInteiro(edtVendedor.Text) ;

    edtQtde.Enabled       := True ;
    edtCodProduto.Enabled := True ;
    edtQtde.Text          := '1' ;

    edtCPF.SetFocus ;

end;

procedure TfrmPDV.btCondPagtoClick(Sender: TObject);
var
    objFormaPagto : TfrmPdvFormaPagto;
    objCondPagto  : TfrmPdvCondPagto;
begin

    if (qryPedidonValPedido.Value <= 0) then
    begin
        MensagemAlerta('Pedido sem valor.');
        exit;
    end;

    if (qryPedidonValDesconto.Value > 0) then
    begin
        if (MessageDlg('Foi efetuado um desconto manual. Se Voc� alterar a condi��o de pagamento este desconto ser� cancelado. Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
            exit ;

    end ;

    qryPedido.Edit;
    qryPedidonValPedido.Value            := qryPedidonValPedido.Value - qryPedidonValAcrescimo.Value + qryPedidonValDesconto.Value + qryPedidonValDescontoCondPagto.Value - qryPedidonValJurosCondPagto.Value;
    qryPedidonValDesconto.Value          := 0 ;
    qryPedidonValDescontoCondPagto.Value := 0 ;
    qryPedidonValDescontoGrupo.Value     := 0 ;
    qryPedidonValJurosCondPagto.Value    := 0 ;
    qryPedidonCdCondPagto.AsString       := '' ;
    qryPedidonValAcrescimo.Value         := 0 ;
    qryPedido.Post;

    edtCondPagto.Text := '' ;

    objFormaPagto := TfrmPdvFormaPagto.Create( Self );
    objCondPagto  := TfrmPdvCondPagto.Create( Self );

    objFormaPagto.cFlgLiqCrediario := 0 ;

    nCdFormaPagto := objFormaPagto.SelecionaFormaPagto() ;

    objCondPagto.nCdLoja               := frmMenu.nCdLojaAtiva ;
    objCondPagto.nCdFormaPagto         := nCdFormaPagto ;
    objCondPagto.cFlgLiqCrediario      := 0 ;
    objCondPagto.edtValorPagar.Value   := (qryPedidonValprodutos.Value + qryPedidonValAcrescimo.Value - qryPedidonValDesconto.Value);
    objCondPagto.edtSaldo.Value        := (qryPedidonValprodutos.Value + qryPedidonValAcrescimo.Value - qryPedidonValDesconto.Value);
    objCondPagto.cPermDesconto         := 'S' ;
    objCondPagto.edtValorPagar.Enabled := False ;
    objCondPagto.bItemPromocao         := bItemPromocao ;

    nCdCondPagto      := objCondPagto.SelecionaCondPagto() ;

    if (nCdCondPagto = 0) then
        exit ;
        
    edtCondPagto.Text := objCondPagto.cNmCondPagtoSelec;
    cFlgPermDesconto  := objCondPagto.qryCondPagtocFlgPermDesconto.Value ;

    {-- Aplica os juros/desconto da condi��o de pagamento --}

    qryPedido.Edit ;
    qryPedidonCdCondPagto.Value          := nCdCondPagto ;

    {-- s� altera o valor do pedido se n�o for uma troca --}
    if (qryPedidonValProdutos.Value > 0) then
    begin
    
        qryPedidonValjurosCondpagto.Value    := 0 ;
        qryPedidonValDescontoCondPagto.Value := 0 ;
        qryPedidonValDesconto.Value          := 0 ;
        qryPedidonValDescontoGrupo.Value     := 0 ;
        qryPedidonValPedido.Value            := qryPedidonValPedido.Value + qryPedidonValDescontoCondPagto.Value + qryPedidonValDesconto.Value ;

        {if (frmPdvCondpagto.nValorFinal > (qryPedidonValprodutos.Value + qryPedidonValAcrescimo.Value)) then
            qryPedidonValJurosCondPagto.Value := (frmMenu.TBRound(frmPdvCondpagto.nValorFinal,2) - frmMenu.TBRound(qryPedidonValprodutos.Value,2))
        else qryPedidonValDescontoCondPagto.Value := (frmMenu.TBRound(qryPedidonValprodutos.Value,2) - frmMenu.TBRound(frmPdvCondpagto.nValorFinal,2)) ;}

        if (objCondPagto.nValorFinal > (qryPedidonValPedido.Value - qryPedidonValAcrescimo.Value)) then
            qryPedidonValJurosCondPagto.Value := (frmMenu.TBRound(objCondPagto.nValorFinal,2) - frmMenu.TBRound((qryPedidonValPedido.Value - qryPedidonValAcrescimo.Value),2))
        else qryPedidonValDescontoCondPagto.Value := (frmMenu.TBRound((qryPedidonValPedido.Value - qryPedidonValAcrescimo.Value),2) - frmMenu.TBRound(objCondPagto.nValorFinal,2)) ;

        qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value ;

    end ;

    qryPedido.Post ;

    freeAndNil( objFormaPagto ) ;
    freeAndNil( objCondPagto ) ;

    btEncerra.SetFocus;

end;

procedure TfrmPDV.edtVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    case key of
        vk_return : edtVendedorExit(Self);
        vk_f4     : btConsultaVend.Click;
    end ;
    
end;

procedure TfrmPDV.edtCodProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    TrataTeclaFuncao(Key) ;

    case key of
        vk_Return: TrataEnterProduto ;
        vk_F4    : btConsProd.Click;
        vk_ESCAPE: edtcodProduto.Text := '' ;
    end ;

end;

procedure TfrmPDV.btTrocaClick(Sender: TObject);
begin

    if qryPedido.Active and (qryPedidonValDesconto.Value > 0) then
    begin
        MensagemAlerta('Retire o desconto antes de efetuar a troca.') ;
        exit ;
    end ;

    edtNumPedido.Text    := '' ;
    edtNumpedido.Visible := False ;
    lblNumPedido.Visible := False ;

    if (ComboOperacao.ItemIndex = 0) then
    begin
        ComboOperacao.ItemIndex := 1 ;
        ComboOperacao.OnChange(nil) ;
        cAcao := 'T' ;

        if (cPedidoTroca = 'S') then
        begin
            edtNumpedido.Visible := True ;
            lblNumPedido.Visible := True ;
        end ;

        exit ;
    end ;

    if (ComboOperacao.ItemIndex = 1) then
    begin
        ComboOperacao.ItemIndex := 2 ;
        ComboOperacao.OnChange(nil) ;
        cAcao := 'D' ;

        if (cPedidoDefeito = 'S') then
        begin
            edtNumpedido.Visible := True ;
            lblNumPedido.Visible := True ;
        end ;
        exit ;
    end ;

    if (ComboOperacao.ItemIndex = 2) then
    begin
        ComboOperacao.ItemIndex := 0 ;
        ComboOperacao.OnChange(nil) ;
        cAcao := 'V' ;
        exit ;
    end  ;

end;

procedure TfrmPDV.edtQtdeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    TrataTeclaFuncao(Key) ;

    case key of
        vk_Return : begin
                        if (edtCodProduto.Enabled) then
                            edtCodProduto.SetFocus
                        else
                            edtVendedor.SetFocus;
                    end;
        vk_Tab    : begin
                        if (edtCodProduto.Enabled) then
                            edtCodProduto.SetFocus
                        else
                            edtVendedor.SetFocus;
                    end;
    end ;

end;

procedure TfrmPDV.btDescontoClick(Sender: TObject);
var
    objPDVDesconto : TfrmPdvDesconto ;
begin

    if not qryPedido.Active or (qryPedidonCdPedido.Value = 0) then
    begin
        MensagemAlerta('Nenhum pedido ativo.') ;
        exit ;
    end ;

    if ((edtCondpagto.Text = '') and (qryPedidonValPedido.Value > 0)) then
    begin
        MensagemAlerta('Selecione a condi��o de pagamento antes de efetuar o desconto.') ;
        btCondPagto.SetFocus;
        exit ;
    end ;

    if ((cFlgPermDesconto = 0) and (qryPedidonValPedido.Value > 0)) then
    begin
        MensagemAlerta('Condi��o de Pagamento n�o permite desconto manual.') ;
        exit ;
    end ;

    if not AutorizacaoGerente(1) then
    begin
        exit ;
    end ;

    if (nPercMaxDescSupervisor <= 0) then
    begin
        MensagemErro('Voc� n�o tem al�ada para conceder descontos.') ;
        abort ;
    end ;

    objPDVDesconto := TfrmPdvDesconto.Create( Self );

    objPDVDesconto.cFlgArredondaPrecoPDV := qryPedidocFlgArredondaPrecoPDV.Value;
    objPDVDesconto.edtValVenda.Value     := qryPedidonValprodutos.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value;
    objPDVDesconto.nPercMaxDesc          := nPercMaxDescSupervisor ;
    objPDVDesconto.nCdUsuarioVendedor    := qryVendedornCdUsuario.Value;
    objPDVDesconto.ShowModal ;

    qryPedido.Edit ;
    qryPedidonValDesconto.Value      := 0 ;
    qryPedidonValAcrescimo.Value     := 0 ;
    qryPedidonValDescontoGrupo.Value := 0 ;

    if (objPDVDesconto.edtValDesconto.Value > 0) then
        qryPedidonValDesconto.Value :=  frmMenu.TBRound(objPDVDesconto.edtvalDesconto.Value, 2);

    if (objPDVDesconto.edtValAcrescimo.Value > 0) then
        qryPedidonValAcrescimo.Value := frmMenu.TBRound(objPDVDesconto.edtvalAcrescimo.Value, 2);

    freeAndNil( objPDVDesconto ) ;

    qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value;
    qryPedido.Post ;

    btEncerra.SetFocus;

end;

procedure TfrmPDV.btValePresClick(Sender: TObject);
var
    objForm : TfrmPDVValePresente ;
begin

    if not edtCodProduto.Enabled then
    begin
        MensagemAlerta('Informe o vendedor.') ;
        edtVendedor.SetFocus;
        exit ;
    end ;

    objForm := TfrmPDVValePresente.Create( Self ) ;

    objForm.edtValValePresente.Value := 0 ;
    objForm.edtValValePresente.SelectAll;
    objForm.ShowModal ;

    objForm.edtValValePresente.PostEditValue;

    GeraValePresente( objForm.edtValValePresente.Value ) ;

    freeAndNil( objForm ) ;

end;

procedure TfrmPDV.btReabreClick(Sender: TObject);
var
    iPedido            : integer ;
    objPDVReabrePedido : TfrmPdvReabrePedido ;
begin

    if not qryPedido.Eof then
    begin
        MensagemAlerta('Encerre este pedido antes de reabrir.') ;
        exit ;
    end ;

    if (frmMenu.LeParametro('SENREABPED') = 'S') then
    begin

        if not AutorizacaoGerente(2) then
            exit ;

    end ;

    objPDVReabrePedido := TfrmPdvReabrePedido.Create( Self ) ;

    try

        iPedido := objPDVReabrePedido.ReabrePedido() ;

    finally

        freeAndNil( objPDVReabrePedido ) ;

    end ;

    if (iPedido > 0) then
    begin
        bItemPromocao     := False ;

        Mensagem('Consultando banco de dados...') ;

        qryPedido.Close ;
        qryPedido.Parameters.ParamByName('nPK').Value := iPedido ;
        qryPedido.Open ;

        if (qryPedido.Eof) then
        begin
            qryPedido.Close ;
            MensagemAlerta('Pedido n�o encontrado.') ;
            exit ;
        end ;

        if (qryPedidonCdLoja.Value <> frmMenu.nCdLojaAtiva) then
        begin
            qryPedido.Close ;
            MensagemAlerta('Este pedido n�o pertence a essa loja.') ;
            exit ;
        end ;

        if (qryPedidonCdTabStatusPed.Value <> 1) then
        begin
            qryPedido.Close ;
            MensagemAlerta('Este pedido n�o pode ser reaberto.') ;
            exit ;
        end ;

        if (qryPedidonCdTipoPedido.Value <> StrToInt(frmMenu.LeParametro('TIPOPEDPDV'))) then
        begin
            qryPedido.Close ;
            MensagemAlerta('Pedido n�o encontrado.') ;
            exit ;
        end ;

        btNovo.Enabled      := False ;
        btCondPagto.Enabled := True ;
        btSelCli.Enabled    := True ;
        btDesconto.Enabled  := True ;
        btTroca.Enabled     := True ;
        btEncerra.Enabled   := True ;
        btValePres.Enabled  := True ;
        btValeMerc.Enabled  := True ;
        btConsProd.Enabled  := True ;

        qryVendedor.Close ;
        qryVendedor.Parameters.ParamByName('nPK').Value := qryPedidonCdUsuarioVended.Value ;
        qryVendedor.Open ;

        Mensagem(' ') ;

        if (qryVendedor.eof) then
        begin
            MensagemAlerta('Vendedor n�o cadastrado') ;
            exit ;
        end;

        qryVendedor.First ;

        nCdVendedor       := qryVendedornCdUsuario.Value     ;
        edtVendedor.Text  := qryVendedornCdUsuario.asString  ;
        nCdPedido         := qryPedidonCdPedido.Value        ;
        nCdTerceiro       := qryPedidonCdTerceiro.Value      ;
        nCdCondPagto      := qryPedidonCdCondPagto.Value     ;
        edtCondpagto.Text := qryPedidocNmCondPagto.Value     ;
        cFlgPermDesconto  := qryPedidocFlgPermDesconto.Value ;

        if (nCdTerceiro > 0) then
        begin
            qryCliente.Close ;
            qryCliente.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
            qryCliente.Open ;

            if not qryCliente.eof then
            begin

                edtCPF.Text       := qryClientecCNPJCPF.Value    ;
                edtNmCliente.Text := qryClientecNmTerceiro.Value ;

                edtNmCliente.ReadOnly := True      ;

            end ;

        end ;

        GroupBox1.Enabled     := True ;
        edtQtde.Enabled       := True ;
        edtCodProduto.Enabled := True ;
        edtQtde.Text          := '1' ;

        edtCodProduto.SetFocus;

        edtVendedor.Enabled := False ;

        // exibe os itens
        qryItemPedido.Close ;
        qryItemPedido.Parameters.ParamByName('nCdPedido').Value := qryPedidonCdPedido.Value ;
        qryItemPedido.Open ;

        while not qryItemPedido.eof do
        begin

            cAcao := 'V' ;

            if (qryItemPedidonValUnitario.Value < 0) then
                cAcao := 'T' ;

            IncluiLista(cAcao, qryItemPedidocCdProduto.AsString, qryItemPedidocNmItem.Value, StrToInt(qryItemPedidonQtdePed.AsString), qryItemPedidonValUnitario.Value, qryItemPedidonValTotalItem.Value,qryItemPedidocFlgPromocional.Value,qryItemPedidocFlgMovEstoqueConsignado.Value) ;

            qryItemPedido.Next;

        end ;

        qryItemPedido.Close ;


    end ;

    

end;

procedure TfrmPDV.btConsProdClick(Sender: TObject);
var
    objForm : TfrmPdvConsProduto ;
begin

    objForm := TfrmPdvConsProduto.Create( Self ) ;

    edtCodProduto.Text := IntToStr(objForm.ConsultaProduto()) ;

    freeAndNil( objForm ) ;
    
    if (edtCodProduto.Text = '0') then
        edtCodProduto.Text := '' ;
        
    if (edtCodProduto.Enabled) then
        edtCodProduto.SetFocus
    else
        edtVendedor.SetFocus;

end;

procedure TfrmPDV.btValeMercClick(Sender: TObject);
var
    cNmProduto     : String ;
    nValProduto    : Double ;
    objPDVValeMerc : TfrmPdvValeMerc ;
begin

  if not edtCodProduto.Enabled then
  begin
      MensagemAlerta('Informe o vendedor.') ;
      edtVendedor.SetFocus;
      exit ;
  end ;

  if qryPedido.eof and (qryPedidonValPedido.Value <= 0) then
  begin
      MensagemAlerta('Pedido sem valor a pagar, imposs�vel adicionar vale.') ;
      edtCodProduto.SetFocus;
      exit;
  end ;

  if (frmMenu.LeParametro('SENVALEMERC') = 'S') then
  begin
      if not AutorizacaoGerente(3) then
          exit ;
  end ;

  objPDVValeMerc := TfrmPdvValeMerc.Create( Self ) ;

  objPDVValeMerc.qryVale.Close ;

  objPDVValeMerc.edtTipo.Value   := 0 ;
  objPDVValeMerc.edtNumVale.Text := '' ;

  objPDVValeMerc.edtTipo.SelectAll;

  objPDVValeMerc.ShowModal;


  if not objPDVValeMerc.qryVale.Eof and (objPDVValeMerc.edtNumVale.Text <> '') then
  begin

      case MessageDlg('Confirma a utiliza��o do vale ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: exit ;
      end ;


     if (edtCondPagto.Text <> '') then
     begin

        edtCondPagto.Text := '' ;
        nCdCondpagto      := 0  ;

        if ((qryPedidonValDescontoCondPagto.Value + qryPedidonValDesconto.Value) > 0) then
            MensagemAlerta('A condi��o de pagamento e o desconto ser�o removidos do pedido.')
        else
            MensagemAlerta('A condi��o de pagamento ser� removida do pedido. Selecione novamente para encerrar.') ;

        qryPedido.Edit ;
        qryPedidonValjurosCondpagto.Value    := 0 ;
        qryPedidonValDescontoCondPagto.Value := 0 ;
        qryPedidonValDesconto.Value          := 0 ;
        qryPedidonValDescontoGrupo.Value     := 0 ;

        qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value ;
        qryPedido.Post ;

     end ;

      Mensagem('Consultando banco de dados...') ;

      frmMenu.Connection.BeginTrans;

      try

          try

            SP_PDV_NOVO_PRODUTO.Close ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.nCdLojaAtiva ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiroColab').Value := nCdVendedor ;

            if not qryCliente.Eof then
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value  := qryClientenCdTerceiro.Value
            else SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value := 0 ;

            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value      := edtCPF.Text  ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNmTerceiro').Value   := edtNmCliente.Text ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cRG').Value           := '' ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEmail').Value        := '' ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTelefone').Value     := '' ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNrCartao').Value     := '' ;

            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdCondPagto').Value  := nCdCondPagto ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdPedido').Value     := frmMenu.ConvInteiro(IntToStr(nCdPedido)) ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEAN').Value          := ' ' ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cAcao').Value         := cAcao ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nQtde').Value         := 1 ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTipoVenda').Value    := 'M' ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nValVenda').Value     := 0 ;
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdVale').Value       := objPDVValeMerc.qryValenCdVale.Value ;
            SP_PDV_NOVO_PRODUTO.Open ;

          except
            frmMenu.Connection.RollbackTrans;
            Mensagem(' ') ;
            ShowMessage('Erro no processamento.');
            raise ;
          end ;
      finally

          freeAndNil( objPDVValeMerc ) ;

      end ;


      if (SP_PDV_NOVO_PRODUTO.FieldList[0].Value <> 'OK') then
      begin
        Mensagem(' ') ;
        cAcao := 'V' ;
        frmMenu.Connection.RollbackTrans;
        ShowMessage(SP_PDV_NOVO_PRODUTO.FieldList[0].Value) ;
        exit ;
      end ;

      frmMenu.Connection.CommitTrans;

      Mensagem(' ') ;

      cNmProduto  := SP_PDV_NOVO_PRODUTO.FieldList[2].Value ;
      nValProduto := SP_PDV_NOVO_PRODUTO.FieldList[3].Value ;
      nCdPedido   := SP_PDV_NOVO_PRODUTO.FieldList[4].Value ;

      SP_PDV_NOVO_PRODUTO.Close;

      Mensagem('Atualizando pedido...') ;

      qryPedido.Close ;
      qryPedido.Parameters.ParamByName('nPK').Value := nCdPedido ;
      qryPedido.Open ;

      Mensagem(' ') ;

      IncluiLista(cAcao, 'VM', cNmProduto, StrToInt(edtQtde.Text), nValProduto, (nValProduto * StrToInt(edtQtde.Text)), 0, 0) ;

  end ;

  cAcao                   := 'V' ;
  ComboOperacao.ItemIndex := 0 ;

  edtCodProduto.Text := '' ;
  edtQtde.Text       := '1'   ;
  edtCodProduto.SetFocus ;


end;

procedure TfrmPDV.btSelCliClick(Sender: TObject);
var
    objForm : TfrmCaixa_SelCliente ;
begin

    objForm := TfrmCaixa_SelCliente.Create( Self ) ;

    nCdTerceiro := 0 ;
    nCdTerceiro := objForm.SelecionaCliente() ;

    freeAndNil( objForm ) ;

    if (nCdTerceiro > 0) then
    begin
        qryCliente.Close ;
        qryCliente.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
        qryCliente.Parameters.ParamByName('cCNPJCPF').Value    := '' ;
        qryCliente.Open ;

        if not qryCliente.eof then
        begin

            edtCPF.Text       := qryClientecCNPJCPF.Value    ;
            edtNmCliente.Text := qryClientecNmTerceiro.Value ;

            edtNmCliente.ReadOnly := True      ;
        end ;

    end ;

end;

procedure TfrmPDV.btConsultaVendClick(Sender: TObject);
var
  nPK     : integer ;
  objForm : TfrmPDVConsVended ;
begin

    objForm := TfrmPDVConsVended.Create( Self ) ;
    objForm.ConsultaVendedor;

    if (objForm.qryVendedor.Active) then
    begin
        edtVendedor.Text := objForm.qryVendedornCdUsuario.asString ;
        edtVendedor.OnExit(nil) ;
    end ;

    freeAndNil(objForm) ;

end;

procedure TfrmPDV.ImprimirPDV(nCdPedido : integer) ;
var
    iLinha     : integer ;
    nQtdeItens : double  ;
    cCabecalho : string  ;
    iColunaAux : double  ;
    iNumCopias : integer ;
    n          : integer ;
    cCdProduto : String  ;
    nTotalItem : double  ;
    nAcresItem : double  ;
begin

  if (qryAmbientenCdTipoImpressoraPDV.Value <= 1) then
  begin
      MensagemAlerta('Nenhuma impressora configurada para o ambiente deste computador.') ;
      exit ;
  end ;

  // termica bematech
  if (qryAmbientenCdTipoImpressoraPDV.Value = 2) then
  begin

      if (trim(qryAmbientecPortaMatricial.Value) = '') then
      begin
          ShowMessage('Porta de comunica��o da impressora do pdv n�o informada.') ;
          exit ;
      end ;

      if (trim(qryAmbientecNomeDLL.Value) = '') then
      begin
          ShowMessage('Nome da DLL n�o informada para o tipo de impressora ativa.') ;
          exit ;
      end ;

      iNumCopias := strToInt(frmMenu.LeParametro('PDVNUMCOPIA')) ;

      Imprime_Termica_Bematech_AbrePorta(qryAmbientecNomeDLL.Value , trim(qryAmbientecPortaMatricial.Value));   

      for n := 0 to iNumCopias - 1 do
      begin

          nQtdeItens := 0;

          Imprime_Termica_Bematech_Negrito     (PadCenter(frmMenu.cNmEmpresaCupom,24,' ')) ;
          Imprime_Termica_Bematech_Negrito     (padCenter(frmMenu.cNmEmpresaCupom2,24,' ')) ;
          Imprime_Termica_Bematech_ImprimeLinha(padCenter('*** NAO � DOCUMENTO FISCAL ***',48,' '));
          Imprime_Termica_Bematech_ImprimeLinha('------------------------------------------------');

          Imprime_Termica_Bematech_ImprimeLinha('Loja      : ' + frmMenu.cNmLojaAtiva);
          Imprime_Termica_Bematech_ImprimeLinha('No Pedido : ' + qryPedidonCdPedido.AsString);
          Imprime_Termica_Bematech_ImprimeLinha('Data      : ' + qryPedidodDtPedido.AsString);
          Imprime_Termica_Bematech_ImprimeLinha('Vendedor  : ' + qryPedidocNmUsuario.Value);
          Imprime_Termica_Bematech_ImprimeLinha('Cliente   : ' + qryPedidocNmTerceiro.Value);

          qryItemPedido.Close ;
          qryItemPedido.Parameters.ParamByName('nCdPedido').Value := nCdPedido ;
          qryItemPedido.Open ;

          Imprime_Termica_Bematech_ImprimeLinha('                                          ') ;
          Imprime_Termica_Bematech_ImprimeLinha('Descri��o                                       ') ;
          Imprime_Termica_Bematech_ImprimeLinha('C�d                     Qt     Unit.       Total') ;
          Imprime_Termica_Bematech_ImprimeLinha('------------------------------------------------') ;

          while not qryItemPedido.eof do
          begin
              if (qryItemPedidocFlgPromocional.Value = 0) then
              begin
                  if (qryItemPedidonCdProduto.Value > 0) then
                      cCdProduto := qryItemPedidonCdProduto.asString
                  else cCdProduto := qryItemPedidocCdProduto.Value;

                  Imprime_Termica_Bematech_ImprimeLinha(Copy(qryItemPedidocNmItem.Value,1,40));
              end
              else begin
                  if (qryItemPedidonCdProduto.Value > 0) then
                      cCdProduto := qryItemPedidonCdProduto.asString
                  else cCdProduto := qryItemPedidocCdProduto.Value;

                  Imprime_Termica_Bematech_ImprimeLinha('*'+Copy(qryItemPedidocNmItem.Value,1,40));
              end ;

              if (qryPedidonValJurosCondpagto.Value > 0) and (qryItemPedidonCdTipoItemPed.Value <> 11) then
              begin
                  nAcresItem := 0;
                  nTotalItem := 0;

                  nAcresItem := (qryItemPedidonValUnitario.Value * (qryPedidonValJurosCondpagto.Value / (qryPedidonValPedido.Value + qryPedidonValDevoluc.Value - qryPedidonValJurosCondpagto.Value + qryPedidonValValePres.Value)))* qryItemPedidonQtdePed.Value;

                  nTotalItem := nAcresItem + (qryItemPedidonQtdePed.Value * qryItemPedidonValUnitario.Value) ;

                  ShowMessage('Val. Unitario: ' + qryItemPedidonValUnitario.AsString + ' Acrescimo item: ' + FloatToStr(frmMenu.TBRound(nAcresItem,2)) +' Total Item: ' + FloatToStr(frmMenu.TBRound(nTotalItem,2)));

                  Imprime_Termica_Bematech_ImprimeLinha(PadLeft(cCdProduto,24,' ') + PadLeft(qryItemPedidonQtdePed.asString,6,' ') + PadRight(formatFloat('##,##0.00',nTotalItem / qryItemPedidonQtdePed.Value),7,' ') + PadRight(formatFloat('##,##0.00',nTotalItem),10,' '));
              end
              else
              begin
//                ShowMessage('aqui n�o � para entrar');
                  Imprime_Termica_Bematech_ImprimeLinha(PadLeft(cCdProduto,24,' ') + PadLeft(qryItemPedidonQtdePed.asString,6,' ') + PadRight(formatFloat('##,##0.00',qryItemPedidonValUnitario.Value),7,' ') + PadRight(formatFloat('##,##0.00',qryItemPedidonValTotalItem.Value),10,' '));
              end;

              if (qryItemPedidonValTotalItem.Value > 0) then
                  nQtdeItens := nQtdeItens + qryItemPedidonQtdePed.Value ;

              qryItemPedido.Next ;

          end ;

          qryItemPedido.Close ;

          Imprime_Termica_Bematech_ImprimeLinha('------------------------------------------------') ;
          Imprime_Termica_Bematech_ImprimeLinha('Quantidade Itens      : ' + PadRight(formatFloat(',0',nQtdeItens),10,' '));
          Imprime_Termica_Bematech_ImprimeLinha('Total de Produtos     : ' + PadRight(formatFloat('##,##0.00',qryPedidonValProdutos.Value),10,' '));
          Imprime_Termica_Bematech_ImprimeLinha('Total de Trocas       : ' + PadRight(formatFloat('##,##0.00',qryPedidonValDevoluc.Value),10,' '));
          Imprime_Termica_Bematech_ImprimeLinha('Total de Vale-Presente: ' + PadRight(formatFloat('##,##0.00',qryPedidonValValePres.Value),10,' '));


          if ((qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value) > 0) then
              Imprime_Termica_Bematech_ImprimeLinha('**Valor do Desconto   : ' + PadRight(formatFloat('##,##0.00',qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value),10,' '))
          else Imprime_Termica_Bematech_ImprimeLinha('Valor do Desconto     : ' + PadRight(formatFloat('##,##0.00',qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value),10,' '));

          Imprime_Termica_Bematech_ImprimeLinha('Valor do Acrescimo    : ' + PadRight(formatFloat('##,##0.00',qryPedidonValAcrescimo.Value),10,' '));
          Imprime_Termica_Bematech_ImprimeLinha('Total A Pagar         : ' + PadRight(formatFloat('##,##0.00',qryPedidonValPedido.Value),10,' '));
          Imprime_Termica_Bematech_ImprimeLinha('Condi��o de Pagamento : ' + edtCondPagto.Text);

          if ((qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value) > 0) then
          begin
              Imprime_Termica_Bematech_ImprimeLinha('');
              Imprime_Termica_Bematech_ImprimeLinha('** desconto exclusivo para a') ;
              Imprime_Termica_Bematech_ImprimeLinha('condi��o de pagamento impressa.') ;
          end ;

          Imprime_Termica_Bematech_ImprimeLinha('');
          Imprime_Termica_Bematech_ImprimeLinha('');

          //Arrumar impress�o do C�digo de Barras
          Imprime_Termica_Bematech_ImprimeBarra(qryPedidonCdPedido.AsString) ;
          Imprime_Termica_Bematech_ImprimeLinha('') ;
          Imprime_Termica_Bematech_ImprimeLinha('') ;

          Imprime_Termica_Bematech_ImprimeLinha(frmMenu.cMsgCupomFiscal);
          Imprime_Termica_Bematech_ImprimeLinha('') ;
          Imprime_Termica_Bematech_ImprimeLinha('') ;
          Imprime_Termica_Bematech_ImprimeLinha('') ;
          Imprime_Termica_Bematech_ImprimeLinha('') ;
      end;

      Imprime_Termica_Bematech_FechaPorta ;

  end ;

  // epson matricial
  if (qryAmbientenCdTipoImpressoraPDV.Value = 3) then
  begin

      ShowMessage('Prepare a impressora e clique em OK para impress�o do pedido.') ;

      if (qryPedidonValJurosCondpagto.Value > 0) then
      begin
          qryCondPagtoAcres.Close;
          qryCondPagtoAcres.Parameters.ParamByName('nPK').Value := qryPedidonCdCondPagto.Value;
          qryCondPagtoAcres.Open;
      end;

      if (qryAmbientecPortaMatricial.Value <> '') then
          rdPrint1.PortaComunicacao := qryAmbientecPortaMatricial.Value ;

      qryItemPedido.Close ;
      qryItemPedido.Parameters.ParamByName('nCdPedido').Value := nCdPedido ;
      qryItemPedido.Open ;

      rdprint1.TamanhoQteLPP      := seis ;
      rdprint1.FonteTamanhoPadrao := s10cpp;
      RdPrint1.TamanhoQteLinhas   := 35 + (qryItemPedido.RecordCount * 2) ;

      rdPrint1.Abrir ;

      cCabecalho := '*** NAO � DOCUMENTO FISCAL ***' ;

      rdprint1.ImpF(01,StrToint(FloatToStr(int((40-Length(frmMenu.cNmEmpresaCupom))/2))),frmMenu.cNmEmpresaCupom,[Comp12]);
      rdprint1.ImpF(02,StrToint(FloatToStr(int((40-Length(frmMenu.cNmEmpresaCupom2))/2))),frmMenu.cNmEmpresaCupom2,[Comp12]);
      rdprint1.ImpF(03,StrToint(FloatToStr(int((40-Length(cCabecalho))/2))),cCabecalho,[Comp12]);

      rdprint1.ImpF(04,01,'---------------------------------------',[Comp12]);

      rdPrint1.ImpF(05,01,'Loja      : ' + frmMenu.ZeroEsquerda(qryPedidonCdLoja.AsString,3) + ' - ' + frmMenu.cNmLojaAtiva,[Comp12]) ;
      rdPrint1.ImpF(06,01,'No Pedido : ' + qryPedidonCdPedido.AsString,[Comp12]) ;
      rdPrint1.ImpF(07,01,'Data      : ' + qryPedidodDtPedido.AsString,[Comp12]) ;
      rdPrint1.ImpF(08,01,'Vendedor  : ' + Copy(qryPedidocNmUsuario.Value,1,26),[Comp12]) ;
      rdPrint1.ImpF(09,01,'Cliente   : ' + qryPedidonCdTerceiro.asString + ' - ' + Copy(qryPedidocNmTerceiro.Value,1,16),[Comp12]) ;

      rdPrint1.ImpF(11,01,'Descri��o                        ',[Comp12] ) ;
      rdPrint1.ImpF(12,01,'C�d         Qt          Unit.     Total',[Comp12] ) ;
      rdPrint1.ImpF(13,01,'---------------------------------------',[Comp12] ) ;

      iLinha := 14 ;

      nQtdeItens := 0 ;

      while not qryItemPedido.eof do
      begin
          if (qryItemPedidocFlgPromocional.Value = 0) then
              rdPrint1.ImpF(iLinha,01,Copy(qryItemPedidocNmItem.Value,1,40),[Comp12])
          else rdPrint1.ImpF(iLinha,01,'*'+Copy(qryItemPedidocNmItem.Value,1,40),[Comp12]) ;

          iLinha := iLinha + 1 ;

          if (qryItemPedidonCdProduto.Value > 0) then
          begin
              qryProduto.Close;
              qryProduto.Parameters.ParamByName('nPK').Value := qryItemPedidonCdProduto.Value ;
              qryProduto.Open;

              if not qryProduto.eof then
                  rdPrint1.ImpF(iLinha,01,qryProdutocEAN.asString,[Comp12]) ;
          end
          else rdPrint1.ImpF(iLinha,01,qryItemPedidocCdProduto.Value,[Comp12]) ;

          rdPrint1.ImpD(iLinha,14,qryItemPedidonQtdePed.asString,[Comp12]) ;
          
          if (qryPedidonValJurosCondpagto.Value > 0) and (qryItemPedidonCdTipoItemPed.Value <> 11) then
          begin

              if (frmMenu.LeParametro('FORMACREPDV') = 'S') then
              begin
                  //FORMA ERRADA DE CALCULAR O ACRESCIMO
                  nAcresItem := 0;
                  nTotalItem := 0;

                  nAcresItem := (qryItemPedidonValUnitario.Value * qryCondPagtoAcresnPercAcrescimo.Value) / 100;

                  nTotalItem := (qryItemPedidonValUnitario.Value + nAcresItem) * qryItemPedidonQtdePed.Value;

                  rdPrint1.ImpD(iLinha,25,formatFloat('##,##0.00',nTotalItem / qryItemPedidonQtdePed.Value),[Comp12]) ;
                  rdPrint1.ImpD(iLinha,33,formatFloat('##,##0.00',nTotalItem),[Comp12]) ;
              end
              else
              begin
                  //FORMA CERTA DE CALCULAR O ACRESCIMO
                  if (qryItemPedidonValTotalItem.Value > 0) then
                  begin
                      nAcresItem := (qryItemPedidonValUnitario.Value * (qryPedidonValJurosCondpagto.Value / (qryPedidonValPedido.Value + qryPedidonValDevoluc.Value - qryPedidonValJurosCondpagto.Value + qryPedidonValValePres.Value)))* qryItemPedidonQtdePed.Value;

                      nTotalItem := nAcresItem + (qryItemPedidonQtdePed.Value * qryItemPedidonValUnitario.Value) ;

                      rdPrint1.ImpD(iLinha,25,formatFloat('##,##0.00',nTotalItem / qryItemPedidonQtdePed.Value),[Comp12]) ;
                      rdPrint1.ImpD(iLinha,33,formatFloat('##,##0.00',nTotalItem),[Comp12]) ;
                  end
                  else
                  begin
                      rdPrint1.ImpD(iLinha,25,formatFloat('##,##0.00',qryItemPedidonValUnitario.Value),[Comp12]) ;
                      rdPrint1.ImpD(iLinha,33,formatFloat('##,##0.00',qryItemPedidonValTotalItem.Value),[Comp12]) ;
                  end;
              end;
          end
          else
          begin
              rdPrint1.ImpD(iLinha,25,formatFloat('##,##0.00',qryItemPedidonValUnitario.Value),[Comp12]) ;
              rdPrint1.ImpD(iLinha,33,formatFloat('##,##0.00',qryItemPedidonValTotalItem.Value),[Comp12]) ;
          end;

          if (qryItemPedidonValTotalItem.Value > 0) then
              nQtdeItens := nQtdeItens + qryItemPedidonQtdePed.Value ;

          qryProduto.Close;
          qryItemPedido.Next ;

          iLinha := iLinha + 1 ;
      end ;

      qryItemPedido.Close ;

      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Quantidade Itens      : ' + formatFloat(',0',nQtdeItens),[Comp12]) ;
      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Total de Produtos     : ' + formatFloat('##,##0.00',qryPedidonValProdutos.Value + qryPedidonValJurosCondpagto.Value),[Comp12]) ;
      iLinha := iLinha + 1 ;
      
      if (qryPedidonValJurosCondpagto.Value > 0) and (frmMenu.LeParametro('FORMACREPDV') = 'S') then
      begin
          nAcresItem := 0;
          nTotalItem := 0;

          nAcresItem := (qryPedidonValDevoluc.Value * qryCondPagtoAcresnPercAcrescimo.Value) / 100;

          nTotalItem := qryPedidonValDevoluc.Value + nAcresItem;
          rdPrint1.ImpF(iLinha,01,'Valor do Trocas   : ' + formatFloat('##,##0.00',nTotalItem),[Comp12]);
      end
      else
      begin
          rdPrint1.ImpF(iLinha,01,'Total de Trocas       : ' + formatFloat('##,##0.00',qryPedidonValDevoluc.Value),[Comp12]) ;
      end;
      
      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Total de Vale-Presente: ' + formatFloat('##,##0.00',qryPedidonValValePres.Value),[Comp12]) ;
      iLinha := iLinha + 1 ;

      if ((qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value) > 0) then
           rdPrint1.ImpF(iLinha,01,'**Valor do Desconto   : ' + formatFloat('##,##0.00',qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value),[Comp12])
      else rdPrint1.ImpF(iLinha,01,'Valor do Desconto     : ' + formatFloat('##,##0.00',qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value),[Comp12]) ;


      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Valor do Acrescimo    : ' + formatFloat('##,##0.00',qryPedidonValAcrescimo.Value),[Comp12]) ;

      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Total A Pagar         : ' + formatFloat('##,##0.00',qryPedidonValPedido.Value),[Comp12]) ;

      iLinha := iLinha + 1 ;
      rdPrint1.ImpF(iLinha,01,'Condi��o de Pagamento : ' + edtCondPagto.Text,[Comp12]) ;

      if ((qryPedidonValDesconto.Value+qryPedidonValDescontoCondPagto.Value) > 0) then
      begin
          iLinha := iLinha + 1 ;
          rdPrint1.Imp(iLinha,01,'** desconto exclusivo para a') ;
          iLinha := iLinha + 1 ;
          rdPrint1.Imp(iLinha,01,'condi��o de pagamento impressa.') ;
      end ;

      iLinha := iLinha + 2 ;
      rdPrint1.ImpF(iLinha,01,frmMenu.cMsgCupomFiscal,[Comp12]) ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;
      iLinha := iLinha + 1 ;
      rdPrint1.Imp(iLinha,01,' ') ;

      rdPrint1.TitulodoRelatorio := 'ER2SOFT - Pr�-Venda' ;
      rdPrint1.NumerodeCopias    := strToInt(frmMenu.LeParametro('PDVNUMCOPIA')) ;

      if (frmMenu.LeParametro('PDVPEWVIEW') = 'S') then
      begin
          rdprint1.OpcoesPreview.Preview      := true ;
          rdprint1.OpcoesPreview.Remalina     := false ;
          rdprint1.OpcoesPreview.PaginaZebrada:= false ;
      end ;

      rdPrint1.Fechar ;

  end ;

  case MessageDlg('O pedido foi impresso corretamente ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: ImprimirPDV(nCdPedido) ;
  end

end ;

procedure TfrmPDV.btEncerraClick(Sender: TObject);
var
  nValPedido    : Double;
  nValDescGrupo : Double;
  nPercDesGrupo : Double;
label
    SelecionaCondPagto ;
begin

    if not qryPedido.Active or (qryPedidonCdPedido.Value = 0) then
    begin
        MensagemAlerta('Nenhum pedido ativo.') ;
        exit ;
    end ;

    if (qryPedidonValDevoluc.Value <> 0) and (qryCliente.Eof) and (frmMenu.LeParametro('OBRCLITROCPDV') = 'S') then
    begin
        if (trim(edtCPF.Text) = '') or (trim(edtNmCliente.Text) = '') then
        begin
            MensagemAlerta('Para opera��o de troca � obrigat�ria a identifica��o do cliente.') ;
            edtCPF.SetFocus ;
            exit ;
        end ;
    end ;

    if (qryPedidonValDevoluc.Value > 0) then
        if (frmMenu.LeParametro('SENTROCAPDV') = 'S') then
            if not AutorizacaoGerente(4) then
                exit ;

    if ((DBEdit5.Text <> '') and (qryPedidonValDescontoGrupo.Value = 0)) then
    begin
        nPercDesGrupo := 0;

        qryCondPagtoGrupoClienteDesc.Close;
        qryCondPagtoGrupoClienteDesc.Parameters.ParamByName('nCdGrupoClienteDesc').Value := qryClientenCdGrupoClienteDesc.Value;
        qryCondPagtoGrupoClienteDesc.Parameters.ParamByName('nCdCondPagto').Value        := qryPedidonCdCondPagto.Value;
        qryCondPagtoGrupoClienteDesc.Open;

        { -- se houver especifica��o de desc. por condpagto, considera o mesmo -- }
        if (not qryCondPagtoGrupoClienteDesc.IsEmpty) then
            nPercDesGrupo := qryCondPagtoGrupoClienteDescnPercDesconto.Value
        else
            nPercDesGrupo := qryClientenPercDesconto.Value;

        case MessageDlg('O cliente informado participa do grupo ' + qryClientecNmGrupoClienteDesc.Value + ' e possui ' + FormatFloat('#,##0.00', nPercDesGrupo) + '% de desconto em suas compras.' + #13#13 + 'Deseja conceder este desconto ?',mtConfirmation,[mbYes,mbNo],0) of
            mrYes: begin
                nValPedido    := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value;
                nValDescGrupo := frmMenu.TBRound((nValPedido * qryClientenPercDesconto.Value) / 100, 2);

                qryPedido.Edit;
                qryPedidonValDescontoGrupo.Value := nValDescGrupo;
                qryPedidonValDesconto.Value      := qryPedidonValDesconto.Value + nValDescGrupo;
                qryPedidonValPedido.Value        := nValPedido - nValDescGrupo;
                qryPedido.Post;
            end;
        end ;
    end;

    case MessageDlg('Confirma o encerramento deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    frmMenu.Connection.BeginTrans;

    // Atualiza a condi��o de pagamento e cliente

    Mensagem('Atualizando banco de dados...') ;

    try

        SP_PDV_ATU_CLI_CONDPAGTO.Close ;
        SP_PDV_ATU_CLI_CONDPAGTO.Parameters.ParamByName('@nCdPedido').Value    := nCdPedido ;
        SP_PDV_ATU_CLI_CONDPAGTO.Parameters.ParamByName('@nCdCondPagto').Value := nCdCondPagto ;
        SP_PDV_ATU_CLI_CONDPAGTO.Parameters.ParamByName('@nCdTerceiro').Value  := nCdTerceiro ;
        SP_PDV_ATU_CLI_CONDPAGTO.ExecProc;
        SP_PDV_ATU_CLI_CONDPAGTO.Close;

        SP_PDV_ENCERRA_PDV.Close ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@nCdPedido').Value    := nCdPedido ;

        if not qryCliente.Eof then
            SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@nCdTerceiro').Value  := qryClientenCdTerceiro.Value
        else SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@nCdTerceiro').Value := 0 ;

        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cCNPJCPF').Value      := edtCPF.Text  ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cNmTerceiro').Value   := edtNmCliente.Text ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cRG').Value           := '' ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cEmail').Value        := '' ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cTelefone').Value     := '' ;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cNrCartao').Value     := edtCartao.Text ;

        SP_PDV_ENCERRA_PDV.ExecProc;
        SP_PDV_ENCERRA_PDV.Close ;

    except
        frmMenu.Connection.RollbackTrans;
        Mensagem(' ') ;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    Mensagem(' ') ;

    frmMenu.Connection.CommitTrans;

    case MessageDlg('Deseja imprimir o pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes:ImprimirPDV(qryPedidonCdPedido.Value);
    end ;

    if (cOrigemCaixa = 1) then
    begin
        iNrPedidoExterno := qryPedidonCdPedido.Value;
    end ;

    qryPedido.Close ;

    Mensagem(' ') ;

    cdsItens.EmptyDataSet;

    btCondPagto.Enabled := False ;
    btSelCli.Enabled    := False ;
    btDesconto.Enabled  := False ;
    btTroca.Enabled     := False ;
    btEncerra.Enabled   := False ;
    btValePres.Enabled  := False ;
    btValeMerc.Enabled  := False ;
    btConsProd.Enabled  := False ;

    edtVendedor.Enabled := False ;
    edtVendedor.Text    := '' ;
    edtQtde.Text        := '' ;
    edtCondPagto.Text   := '' ;

    qryVendedor.Close ;

    qryCliente.Close ;

    btNovo.Enabled         := True ;
    btConsultaVend.Enabled := False ;
    btCOnsultaCli.Enabled  := False ;

    edtCPF.Text       := '' ;
    edtNmCliente.Text := '' ;
    GroupBox1.Enabled := False ;
    GroupBox4.Enabled := False ;
    bItemPromocao     := False ;

    btNovo.SetFocus;

    if (cOrigemCaixa = 1) then
        Close ;

end;

procedure TfrmPDV.ExcluirItem1Click(Sender: TObject);
var
    bPromocional : Boolean ;
begin

    if not qryPedido.Active or (qryPedidonCdPedido.Value = 0) then
    begin
        MensagemAlerta('Nenhum pedido ativo.') ;
        exit ;
    end ;

    { -- se item � de uma consigna��o, n�o permite exclus�o devido item j� ter sido movimentado estoque anteriormente -- }
    if (cdsItenscFlgMovEstoqueConsignado.Value = 1) then
    begin
        qryBuscaConsignacao.Close;
        qryBuscaConsignacao.Parameters.ParamByName('nCdPedido').Value := qryPedidonCdPedido.Value;
        qryBuscaConsignacao.Open;

        MensagemAlerta('Item consignado n�o pode ser exclu�do devido j� ter sido movimentado anteriormente.' + #13#13 + 'Consigna��o: ' + IntToStr(qryBuscaConsignacaonCdConsignacao.Value) + '.');
        Abort;
    end;

    case MessageDlg('Confirma a exclus�o deste item?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    {-- Aplica os juros/desconto da condi��o de pagamento --}
    if (qryPedidonValDesconto.Value > 0) or (qryPedidonCdCondPagto.Value > 0) then
    begin

        if (qryPedidonValDesconto.Value > 0) then
            if (MessageDlg('A condi��o de pagamento e o desconto ser�o cancelados. Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
                exit 
        else if (qryPedidonValDesconto.Value = 0) then
                if (MessageDlg('Ap�s a exclus�o voc� dever� informar novamente a condi��o de pagamento. Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
                    exit ;

        qryPedido.Edit ;
        qryPedidonValjurosCondpagto.Value    := 0 ;
        qryPedidonValDescontoCondPagto.Value := 0 ;
        qryPedidonValDesconto.Value          := 0 ;
        qryPedidonValDescontoGrupo.Value     := 0 ;
        qryPedidonCdCondPagto.asString       := '';
        nCdCondPagto                         := 0 ;
        edtCondPagto.Text                    := '';

        {--recalcula o total do pedido --}
        qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value ;
        qryPedido.Post ;

    end;

    frmMenu.Connection.BeginTrans;

    try
        SP_PDV_EXCLUIR_ITEM.Close ;
        SP_PDV_EXCLUIR_ITEM.Parameters.ParamByName('@nCdPedido').Value     := qryPedidonCdPedido.Value ;
        SP_PDV_EXCLUIR_ITEM.Parameters.ParamByName('@cCdProduto').Value    := cdsItensnCdProduto.Value ;
        SP_PDV_EXCLUIR_ITEM.Parameters.ParamByName('@nValTotalItem').Value := cdsItensnValTotal.Value  ;
        SP_PDV_EXCLUIR_ITEM.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    bPromocional := (cdsItenscFlgPromocional.Value = 1) ;

    cdsItens.Delete ;

    bItemPromocao := false ;

    {-- Se o item excluido for um item promocional, procura para saber se ficou mais algum item no pedido que tamb�m seja promocional --}
    if (bPromocional) then
    begin
        qryItemPedido.Close ;
        qryItemPedido.Parameters.ParamByName('nCdPedido').Value := qryPedidonCdPedido.Value ;
        qryItemPedido.Open ;

        qryItemPedido.First ;

        while not qryItemPedido.Eof do
        begin
            if (qryItemPedidocFlgPromocional.Value = 1) then
            begin
                bItemPromocao := true ;
                break;
            end ;

            qryItemPedido.Next ;
        end ;

        qryItemPedido.Close ;
    end ;

    qryPedido.Close ;
    qryPedido.Parameters.ParamByName('nPK').Value := nCdPedido ;
    qryPedido.Open ;

end;

procedure TfrmPDV.FormCreate(Sender: TObject);
begin

  Application.OnException      := frmMenu.TrataErros ;
  Screen.OnActiveControlChange := frmMenu.ScreenActiveControlChange;

  qryTipoPedido.Close;
  qryTipoPedido.Parameters.ParamByName('nPK').Value := frmMenu.ConvInteiro(frmMenu.LeParametro('TIPOPEDPDV')) ;
  qryTipoPedido.Open;

  cFlgBloqEstNeg := qryTipoPedidocFlgBloqEstNeg.Value;

end;

procedure TfrmPDV.edtCPFExit(Sender: TObject);
begin
    { -- limpa nome cliente -- }
    edtNmCliente.Clear;

    if ((DBEdit2.Text = '') and (Trim(edtCPF.Text) <> '')) then
    begin
        MensagemAlerta('Informe o vendedor.') ;
        edtVendedor.SetFocus;
        Exit;
    end ;

    if (trim(edtCPF.Text) <> '') then
    begin

        qryCliente.Close ;

        if (frmMenu.TestaCpfCgc(trim(edtCPF.Text)) = '') then
        begin

            edtCPF.Text       := '' ;
            edtNmCliente.Text := '' ;
            edtCPF.SetFocus ;
            abort ;

        end ;

        edtNmCliente.Text := '' ;

        qryCliente.Parameters.ParamByName('nCdTerceiro').Value := 0 ;
        qryCliente.Parameters.ParamByName('cCNPJCPF').Value    := edtCPF.Text ;
        qryCliente.Open ;

        if not qryCliente.Eof then
        begin

            if (qryClientenCdStatus.Value = 2) then
            begin
                frmMenu.MensagemAlerta('Cliente inativo n�o pode ser utilizado, efetue sua ativa��o para que seja liberado o uso do seu cadastro.');
                edtCPF.SetFocus;
                Abort;
            end;

            edtNmCliente.Text := qryClientecNmTerceiro.Value;

            {-- atualiza o terceiro no pedido se for modificado depois de inserir o produto--}
            if (qryPedido.Active) then
            begin
                qryAtualizaTerceiro.Close;
                qryAtualizaTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryClientenCdTerceiro.Value;
                qryAtualizaTerceiro.Parameters.ParamByName('nCdPedido').Value := qryPedidonCdPedido.Value;
                qryAtualizaTerceiro.ExecSQL;
            end;

        end else
        begin
            frmMenu.MensagemAlerta('Cliente n�o cadastrado.') ;
            edtCPF.Text       := '' ;
            edtNmCliente.Text := '' ;
            edtCPF.SetFocus;
        end
    end ;

    if (edtCPF.Text = '') then
        edtNmCliente.Text := '' ;

end;

procedure TfrmPDV.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmPDV.ComboCadastroChange(Sender: TObject);
begin

    edtCPF.Text           := '' ;
    edtNmCliente.Text     := '' ;
    edtCPF.SetFocus ;

end;

procedure TfrmPDV.ComboCadastroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if key = 13 then
  begin
      key := 0 ;
      edtCPF.SetFocus ;
      exit ;
  end;

end;

procedure TfrmPDV.btConsultaCliClick(Sender: TObject);
begin

    btSelCli.Click ;

end;

procedure TfrmPDV.edtCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (key = 13) then
      if (edtCodProduto.Enabled) then
          edtCodProduto.SetFocus
      else
          edtVendedor.SetFocus;

  if (key = VK_F4) then
      btConsultaCli.Click;

end;

procedure TfrmPDV.edtNmClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 13 then
  begin

    key := 0;
    edtCodProduto.SetFocus ;
    exit

  end;

end;

procedure TfrmPDV.ComboOperacaoChange(Sender: TObject);
begin

    edtNumPedido.Text    := '' ;
    edtNumpedido.Visible := False ;
    lblNumPedido.Visible := False ;

    if (edtCodProduto.Enabled) then
        edtCodProduto.SetFocus
    else
        edtVendedor.SetFocus;

    if (ComboOperacao.ItemIndex = 1) then
    begin
        if (cPedidoTroca = 'S') then
        begin
            edtNumpedido.Visible := True ;
            lblNumPedido.Visible := True ;
            edtNumPedido.SetFocus ;
        end ;
    end ;

    if (ComboOperacao.ItemIndex = 2) then
    begin
        if (cPedidoDefeito = 'S') then
        begin
            edtNumpedido.Visible := True ;
            lblNumPedido.Visible := True ;
            edtNumPedido.SetFocus ;
        end ;
    end ;

end;

procedure TfrmPDV.GeraValePresente(nValValePresente : double);
var
    cNmProduto  : string ;
    nValProduto : double ;
begin

  if (nValValePresente > 0) then
  begin

      case MessageDlg('Confirma o Valor do Vale Presente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: begin

              edtCodProduto.Text := ''  ;
              edtQtde.Text       := '1' ;
              edtQtde.SetFocus ;
              exit ;
          end ;
      end ;

      Mensagem('Consultando banco de dados...') ;

      frmMenu.Connection.BeginTrans;

      try

        SP_PDV_NOVO_PRODUTO.Close ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.nCdLojaAtiva ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiroColab').Value := nCdVendedor ;

        if not qryCliente.Eof then
            SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value  := qryClientenCdTerceiro.Value
        else SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value := 0 ;

        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value      := edtCPF.Text  ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNmTerceiro').Value   := edtNmCliente.Text ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cRG').Value           := '' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEmail').Value        := '' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTelefone').Value     := '' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNrCartao').Value     := edtCartao.Text ;

        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdCondPagto').Value     := nCdCondPagto ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdPedido').Value        := frmMenu.ConvInteiro(IntToStr(nCdPedido)) ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEAN').Value             := ' ' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cAcao').Value            := cAcao ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nQtde').Value            := 1 ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTipoVenda').Value       := 'V' ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nValVenda').Value        := nValValePresente ;
        SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdVale').Value          := 0 ;
        SP_PDV_NOVO_PRODUTO.Open ;

      except
        frmMenu.Connection.RollbackTrans;
        Mensagem(' ') ;
        ShowMessage('Erro no processamento.');
        raise ;
      end ;


      if (SP_PDV_NOVO_PRODUTO.FieldList[0].Value <> 'OK') then
      begin
        Mensagem(' ') ;
        cAcao := 'V' ;
        frmMenu.Connection.RollbackTrans;
        ShowMessage(SP_PDV_NOVO_PRODUTO.FieldList[0].Value) ;
        exit ;
      end ;

      frmMenu.Connection.CommitTrans;

      Mensagem(' ') ;

      cNmProduto  := SP_PDV_NOVO_PRODUTO.FieldList[2].Value ;
      nValProduto := SP_PDV_NOVO_PRODUTO.FieldList[3].Value ;
      nCdPedido   := SP_PDV_NOVO_PRODUTO.FieldList[4].Value ;

      SP_PDV_NOVO_PRODUTO.Close;

      Mensagem('Atualizando pedido...') ;

      qryPedido.Close ;
      qryPedido.Parameters.ParamByName('nPK').Value := nCdPedido ;
      qryPedido.Open ;

      Mensagem(' ') ;

      IncluiLista(cAcao, 'VP', cNmProduto, StrToInt(edtQtde.Text), nValProduto, (nValProduto * StrToInt(edtQtde.Text)), 0, 0) ;

      if (edtCondPagto.Text <> '') then
      begin

          edtCondPagto.Text := '' ;
          nCdCondpagto      := 0  ;

          if ((qryPedidonValDescontoCondPagto.Value + qryPedidonValDesconto.Value) > 0) then
              MensagemAlerta('A condi��o de pagamento e o desconto foram removidos do pedido.')
          else
              MensagemAlerta('A condi��o de pagamento foi removida do pedido. Selecione novamente para encerrar.') ;

          qryPedido.Edit ;
          qryPedidonValjurosCondpagto.Value    := 0 ;
          qryPedidonValDescontoCondPagto.Value := 0 ;
          qryPedidonValDesconto.Value          := 0 ;
          qryPedidonValDescontoGrupo.Value     := 0 ;

          qryPedidonValpedido.Value := qryPedidonValProdutos.Value + qryPedidonValAcrescimo.Value + qryPedidonValJurosCondpagto.Value - qryPedidonValDescontoCondpagto.Value - qryPedidonValDesconto.Value ;
          qryPedido.Post ;

      end ;


  end ;

  cAcao                   := 'V' ;
  ComboOperacao.ItemIndex := 0 ;

  edtCodProduto.Text := ''    ;
  edtQtde.Text       := '1'   ;
  edtCodProduto.SetFocus ;
end ;

procedure TfrmPDV.Mensagem(cMensagem: string);
begin
{}
end;

procedure TfrmPDV.btDescontoKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
  begin

    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmPDV.btNovoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;
end;

procedure TfrmPDV.TrataTeclaFuncao(Key: Word);
begin

    case Key of
        VK_F2: if (btNovo.Enabled) then btNovo.Click;
        VK_F3: if (btCondPagto.Enabled) then btCondPagto.Click;
        VK_F5: if (btDesconto.Enabled) then btDesconto.Click;
        VK_F6: if (btTroca.Enabled) then btTroca.Click;
        VK_F7: btEncerra.Click;
        VK_F8: if (btValePres.Enabled) then btValePres.Click;
        VK_F9: if (btValeMerc.Enabled) then btValeMerc.Click;
        VK_F11: if (btReabre.Enabled) then btReabre.Click;
        VK_F12: btSair.Click;
    end ;

end;

procedure TfrmPDV.btCondPagtoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btDescontoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btTrocaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btEncerraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btValePresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btValeMercKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btReabreKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.btSairKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmPDV.ValidaPosicaoEstoque();
var
  nQtdeVenda : Integer;
begin
  nQtdeVenda         := 0;
  nCdUsuarioAutorPDV := 0;
  cOBS               := '';

  qryPosicaoEstoque.Close;
  qryPosicaoEstoque.Parameters.ParamByName('nCdProduto').Value := frmMenu.ConvInteiro(edtCodProduto.Text);
  qryPosicaoEstoque.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryPosicaoEstoque.Open;

  if (qryPosicaoEstoque.IsEmpty) then
  begin
      MensagemAlerta('Posi��o de estoque n�o encontrada para o produto informado.');
      edtCodProduto.SetFocus ;
      Abort;
  end;

  if (qryPosicaoEstoquenCdStatus.Value <> 1) then
  begin
      MensagemAlerta('Produto com status desativado n�o pode ser utilizado no PDV.');
      edtCodProduto.SetFocus ;
      Abort;
  end;

  nQtdeVenda := frmMenu.ConvInteiro(edtQtde.text);

  cdsItens.First;

  while (not cdsItens.Eof) do
  begin
      if ((cdsItensnCdProduto.Value = edtCodProduto.Text) and (cdsItenscTipo.Value = 'V')) then
          nQtdeVenda := nQtdeVenda + cdsItensnQtde.Value;

      cdsItens.Next;
  end;

  if (nQtdeVenda > qryPosicaoEstoquenQtdeDisp.Value) then
  begin
      MensagemAlerta('Produto informado n�o possu� saldo dispon�vel em estoque.');

      case MessageDlg('Deseja liberar a venda deste produto mesmo n�o havendo saldo dispon�vel ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:Abort;
      end ;

      if (not AutorizacaoGerente(5)) then
      begin
          edtCodProduto.SetFocus;
          Abort;
      end;

      frmMenu.InputQuery('Motivo de Libera��o', 'Motivo de libera��o venda: ', cOBS);

      if (Trim(cOBS) = '') then
      begin
          MensagemAlerta('Motivo de autoriza��o n�o informado.');
          Abort;
      end;
  end;
end;

procedure TfrmPDV.TrataEnterProduto;
begin

    if (DBEdit2.Text = '') then
    begin
        MensagemAlerta('Informe o vendedor.') ;
        edtCodProduto.Text := '' ;
        edtVendedor.SetFocus ;
        exit ;
    end ;

    if (edtCPF.Text <> '') and (trim(edtNmCliente.Text) = '') and (not edtNmCliente.ReadOnly) then
    begin
        MensagemAlerta('Informe o nome do cliente ou apague o CPF.') ;
        edtCodProduto.Text := '' ;
        edtNmCliente.SetFocus ;
        exit ;
    end ;

    if (trim(edtCodProduto.Text) <> '') then
    begin

        if (edtNumPedido.Visible) and (ComboOperacao.ItemIndex > 0) and (frmMenu.ConvInteiro(edtNumPedido.Text) <= 0) then
        begin
            MensagemAlerta('Para esta troca � obrigat�rio infomar o n�mero do pedido da venda do produto.') ;
            edtCodProduto.Text := '' ;
            edtNumPedido.SetFocus ;
            abort ;
        end ;

        if (frmMenu.ConvInteiro(edtQtde.Text) < 1) then
        begin
            MensagemAlerta('Quantidade de produtos inv�lida.') ;
            edtQtde.Text       := '1' ;
            edtCodProduto.Text := '' ;
            edtCodProduto.SetFocus ;
            abort ;
        end ;

        LocalizaProduto(Trim(edtCodProduto.Text)) ;

        ComboOperacao.ItemIndex := 0 ;
        edtNumPedido.Text       := '0' ;
        ComboOperacao.OnChange(nil) ;

        edtCodProduto.SetFocus ;

    end
    else if (btCondPagto.Enabled) and (not qryPedido.eof) then
        if (qryPedido.eof) or (qryPedidonValPedido.Value <= 0) then
            btEncerra.SetFocus
        else btCondPagto.SetFocus ;

end;

procedure TfrmPDV.btConsProdDetalheClick(Sender: TObject);
var
    objForm : TfrmPDVConsProdutoDetalhe;
begin

    objForm := TfrmPDVConsProdutoDetalhe.Create( Self );

    objForm.ShowModal;

    freeAndNil( objForm );

    if (DBEdit1.Text <> '') then
        edtCodProduto.SetFocus
    else edtVendedor.SetFocus;

end;

function TfrmPDV.AutorizacaoGerente(nCdTipoLiberacao: integer): boolean;
var
  objCaixa_Recebimento : TfrmCaixa_Recebimento;
begin

  objCaixa_Recebimento := TfrmCaixa_Recebimento.Create( Self );

  Result := objCaixa_Recebimento.AutorizacaoGerente( nCdTipoLiberacao );

  nCdUsuarioAutorPDV     := objCaixa_Recebimento.nCdUsuarioSupervisor;
  nPercMaxDescSupervisor := objCaixa_Recebimento.nPercMaxDescSupervisor;

  freeAndNil( objCaixa_Recebimento );

end;

initialization
    RegisterClass(TfrmPDV) ;

end.
