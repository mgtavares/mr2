inherited rptOperacaoCartao: TrptOperacaoCartao
  Left = 344
  Top = 240
  Width = 753
  Height = 295
  Caption = 'Rel. Opera'#231#227'o com Cart'#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 737
    Height = 233
  end
  object Label1: TLabel [1]
    Left = 7
    Top = 72
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Operadora Cart'#227'o'
  end
  object Label2: TLabel [2]
    Left = 75
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label3: TLabel [3]
    Left = 11
    Top = 96
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Venda'
  end
  object Label4: TLabel [4]
    Left = 6
    Top = 120
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Cr'#233'dito'
  end
  object Label6: TLabel [5]
    Left = 180
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label5: TLabel [6]
    Left = 180
    Top = 120
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 737
    TabOrder = 13
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtOperadora: TMaskEdit [8]
    Left = 104
    Top = 64
    Width = 63
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtOperadoraExit
    OnKeyDown = edtOperadoraKeyDown
  end
  object edtLoja: TMaskEdit [9]
    Left = 104
    Top = 40
    Width = 63
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object RadioGroup1: TRadioGroup [10]
    Left = 16
    Top = 144
    Width = 209
    Height = 49
    Caption = ' Condi'#231#227'o de Pagamento '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'A vista'
      'A prazo')
    TabOrder = 8
  end
  object RadioGroup2: TRadioGroup [11]
    Left = 232
    Top = 144
    Width = 281
    Height = 49
    Caption = ' Situa'#231#227'o da Opera'#231#227'o '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'em aberto'
      'pago pela operadora')
    TabOrder = 9
  end
  object DBEdit1: TDBEdit [12]
    Tag = 1
    Left = 170
    Top = 40
    Width = 559
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 170
    Top = 64
    Width = 559
    Height = 21
    DataField = 'cNmOperadoraCartao'
    DataSource = dsOperadora
    TabOrder = 3
  end
  object RadioGroup3: TRadioGroup [14]
    Left = 520
    Top = 144
    Width = 209
    Height = 49
    Caption = ' Tipo de Opera'#231#227'o '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'cr'#233'dito'
      'd'#233'bito')
    TabOrder = 10
  end
  object edtDtVendaIni: TMaskEdit [15]
    Left = 104
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object edtDtVendaFim: TMaskEdit [16]
    Left = 200
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object edtDtCreditoIni: TMaskEdit [17]
    Left = 104
    Top = 112
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object edtDtCreditoFim: TMaskEdit [18]
    Left = 200
    Top = 112
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object RadioGroup4: TRadioGroup [19]
    Left = 232
    Top = 200
    Width = 281
    Height = 49
    Caption = ' Conciliado Extrato Operadora '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Sim'
      'N'#227'o')
    TabOrder = 12
  end
  object rgOrigemTransacao: TRadioGroup [20]
    Left = 16
    Top = 201
    Width = 209
    Height = 49
    Caption = ' Origem da Transa'#231#227'o '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Interna'
      'Web')
    TabOrder = 11
  end
  inherited ImageList1: TImageList
    Left = 432
    Top = 80
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      
        'AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = :n' +
        'CdUsuario AND UL.nCdLoja = Loja.nCdLoja)')
    Left = 464
    Top = 80
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 464
    Top = 112
  end
  object qryOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperadoraCartao,cNmOperadoraCartao'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 496
    Top = 80
    object qryOperadoranCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryOperadoracNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
  end
  object dsOperadora: TDataSource
    DataSet = qryOperadora
    Left = 496
    Top = 112
  end
end
