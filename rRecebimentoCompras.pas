unit rRecebimentoCompras;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB, comObj, ExcelXP, ER2Excel;

type
  TrptRecebimentoCompras = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label1: TLabel;
    MaskEdit4: TMaskEdit;
    Label4: TLabel;
    MaskEdit5: TMaskEdit;
    Label7: TLabel;
    MaskEdit7: TMaskEdit;
    RadioGroup5: TRadioGroup;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryProduto: TADOQuery;
    qryTipoReceb: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit3: TDBEdit;
    dsProduto: TDataSource;
    qryTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoRecebcNmTipoReceb: TStringField;
    DBEdit4: TDBEdit;
    dsTipoReceb: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironcdterceiro: TIntegerField;
    qryTerceirocCnpjCpf: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsTerceiro: TDataSource;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    dsEmpresa: TDataSource;
    Label2: TLabel;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    DBEdit6: TDBEdit;
    MaskEdit3: TMaskEdit;
    RadioGroup1: TRadioGroup;
    Label8: TLabel;
    ER2Excel: TER2Excel;
    rgExibeItem: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRecebimentoCompras: TrptRecebimentoCompras;

implementation

uses UrelRecebimentoCompras_View, fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TrptRecebimentoCompras.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, Maskedit6.Text) ;
end;

procedure TrptRecebimentoCompras.ToolButton1Click(Sender: TObject);
var objRel : TrptRecebimentoCompras_view;
var
    cNmStatusReceb : String;
    iLinha : integer;
begin
  inherited;

  objRel := TrptRecebimentoCompras_view.Create(nil);

  cNmStatusReceb := 'Finalizados: N�o';

  if (RadioGroup5.ItemIndex = 0) then cNmStatusReceb := 'Finalizados: Sim';

  if (rgExibeItem.ItemIndex = 0) then
  begin
      objRel.QRGroup1.Height := 60;
  end;

  objRel.bPrintItem := (rgExibeItem.ItemIndex = 1);

  Try
      Try
          objRel.usp_Relatorio.Close ;

          objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value    := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value     := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value       := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdProduto').Value     := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTipoReceb').Value   := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdPedido').Value      := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cFlgFinalizado').Value := RadioGroup5.ItemIndex;
          objRel.usp_Relatorio.Parameters.ParamByName('@cFlgExibeItem').Value  := rgExibeItem.ItemIndex;

          objRel.usp_Relatorio.Open;
          
          if (RadioGroup1.ItemIndex = 0) then
          begin 

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              if (Trim(MaskEdit6.Text) <> '') then objRel.lblFiltro1.Caption := 'Terceiro: ' + String(MaskEdit6.Text) + ' ' + DbEdit2.Text
              else objRel.lblFiltro1.Caption := '';

              if (Trim(MaskEdit3.Text) <> '') then objRel.lblFiltro7.Caption := 'Loja: ' + String(MaskEdit3.Text) + ' ' + DbEdit6.Text
              else objRel.lblFiltro7.Caption := '';

              if ((Trim(MaskEdit1.Text) <> '/  /') or (Trim(MaskEdit2.Text) <> '/  /')) then objRel.lblFiltro2.Caption := 'Per�odo : ' + String(MaskEdit1.Text) + ' a ' + String(MaskEdit2.Text)
              else objRel.lblFiltro2.Caption := '';

              if (Trim(MaskEdit4.Text) <> '') then objRel.lblFiltro3.Caption := 'Produto: ' + String(MaskEdit4.Text) + ' ' + DbEdit3.Text
              else objRel.lblFiltro3.Caption := '';

              if (Trim(MaskEdit7.Text) <> '') then objRel.lblFiltro4.Caption := 'Tp.Receb: ' + String(MaskEdit7.Text) + ' ' + DbEdit4.Text
              else objRel.lblFiltro4.Caption := '';

              if (Trim(MaskEdit5.Text) <> '') then objRel.lblFiltro5.Caption := 'Nr.Pedido: ' + String(MaskEdit5.Text)
              else objRel.lblFiltro5.Caption := '';

              objRel.lblFiltro6.Caption := cNmStatusReceb;

              {-- insere informa�oes --}

              objRel.QuickRep1.PreviewModal;
          end
          else
          begin
              frmMenu.mensagemUsuario('Exportando Planilha...');

              ER2Excel.Celula['A1'].Background := RGB(221,221,221);
              ER2Excel.Celula['A1'].Range('W4');

              ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio';
              ER2Excel.Celula['A2'].Text := 'Rel. Recebimento de Mercadorias';
              ER2Excel.Celula['A4'].Text := 'Empresa';
              ER2Excel.Celula['B4'].Text := 'Loja';
              ER2Excel.Celula['C4'].Text := 'Receb.';
              ER2Excel.Celula['D4'].Text := 'Tipo Receb.';
              ER2Excel.Celula['E4'].Text := 'Fornecedor';
              ER2Excel.Celula['F4'].Text := 'Dt. Receb.';
              ER2Excel.Celula['G4'].Text := 'Status';
              ER2Excel.Celula['H4'].Text := 'Inf. Cadastro';
              ER2Excel.Celula['I4'].Text := ''; //usu�rio cad.
              ER2Excel.Celula['J4'].Text := 'Inf. Fechamento';
              ER2Excel.Celula['K4'].Text := ''; //usu�rio fech.
              ER2Excel.Celula['L4'].Text := 'Inf. Autoriza��o Financ.';
              ER2Excel.Celula['M4'].Text := ''; //usu�rio aut. fin.
              ER2Excel.Celula['N4'].Text := 'Nr. Docto';
              ER2Excel.Celula['O4'].Text := 'Dt. Docto';
              ER2Excel.Celula['P4'].Text := 'Chave NF';
              ER2Excel.Celula['Q4'].Text := 'Orig. NF';
              ER2Excel.Celula['R4'].Text := 'Vr. Produto';
              ER2Excel.Celula['S4'].Text := 'Vr. Frete';
              ER2Excel.Celula['T4'].Text := 'Vr. Desconto';
              ER2Excel.Celula['U4'].Text := 'Vr. ICMS';
              ER2Excel.Celula['V4'].Text := 'Vr. ST';
              ER2Excel.Celula['W4'].Text := 'Total NF';

              if (rgExibeItem.ItemIndex = 1) then
              begin
                  ER2Excel.Celula['X1'].Background := RGB(221,221,221);
                  ER2Excel.Celula['X1'].Range('AS4');

                  ER2Excel.Celula['X4'].Text  := 'Produto';
                  ER2Excel.Celula['Y4'].Text  := ''; //desc. prod
                  ER2Excel.Celula['Z4'].Text  := 'Ref.';
                  ER2Excel.Celula['AA4'].Text := 'Departamento';
                  ER2Excel.Celula['AB4'].Text := 'Categoria';
                  ER2Excel.Celula['AC4'].Text := 'Marca';
                  ER2Excel.Celula['AD4'].Text := 'Pedido';
                  ER2Excel.Celula['AE4'].Text := 'Qtde';
                  ER2Excel.Celula['AF4'].Text := 'Vr. Unit';
                  ER2Excel.Celula['AG4'].Text := 'Vr. Venda';
                  ER2Excel.Celula['AH4'].Text := '% Desc.';
                  ER2Excel.Celula['AI4'].Text := 'Vr. Desc.';
                  ER2Excel.Celula['AJ4'].Text := 'Vr. Total';
                  ER2Excel.Celula['AK4'].Text := 'Total s/ Imp.';
                  ER2Excel.Celula['AL4'].Text := '% IPI';
                  ER2Excel.Celula['AM4'].Text := 'Vr. IPI';
                  ER2Excel.Celula['AN4'].Text := '% ST';
                  ER2Excel.Celula['AO4'].Text := 'Vr. ST';
                  ER2Excel.Celula['AP4'].Text := 'Vr. Custo Final';
                  ER2Excel.Celula['AQ4'].Text := 'Vr. Esp.';
                  ER2Excel.Celula['AR4'].Text := 'Vr. Unit. Ped.';
                  ER2Excel.Celula['AS4'].Text := 'Vr. Total Final';
              end;

              ER2Excel.Celula['A1'].Congelar('A5');
              iLinha := 5;

              objRel.usp_relatorio.First;

              while (not objRel.usp_relatorio.Eof) do
              begin
                  ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.usp_relatorionCdEmpresa.Value;
                  ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.usp_relatorionCdLoja.Value;
                  ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.usp_relatorionCdRecebimento.Value;
                  ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmTipoReceb.Value;
                  ER2Excel.Celula['E' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmTerceiro.Value;
                  ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.usp_relatoriodDtReceb.Value;
                  ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmTabStatusReceb.Value;
                  ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.usp_relatoriodDtCad.Value;
                  ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmUsuarioCad.Value;
                  ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.usp_relatoriodDtFech.Value;
                  ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmUsuarioFech.Value;
                  ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.usp_relatoriodDtAutorFinanc.Value;
                  ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmUsuarioAutorFin.Value;
                  ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNrDocto.Value;
                  ER2Excel.Celula['O' + IntToStr(iLinha)].Text := objRel.usp_relatoriodDtDocto.Value;

                  if (Trim(objRel.usp_relatoriocChaveNFe.AsString) <> '') then
                      ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.usp_relatoriocChaveNFe.AsString + '-chave';

                  ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.usp_relatoriocUFOrigemNF.Value;
                  ER2Excel.Celula['R' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValProdutos.Value);
                  ER2Excel.Celula['S' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValFrete.Value);
                  ER2Excel.Celula['T' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValDescontoNF.Value);
                  ER2Excel.Celula['U' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValICMS.Value);
                  ER2Excel.Celula['V' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValICMSSub.Value);
                  ER2Excel.Celula['W' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValTotalNF.Value);

                  if (rgExibeItem.ItemIndex = 1) then
                  begin
                      ER2Excel.Celula['X' + IntToStr(iLinha)].Text := objRel.usp_relatorionCdProduto.Value;
                      ER2Excel.Celula['Y' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmProduto.Value;
                      ER2Excel.Celula['Z' + IntToStr(iLinha)].Text := objRel.usp_relatoriocReferencia.Value;
                      ER2Excel.Celula['AA' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmDepartamento.Value;
                      ER2Excel.Celula['AB' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmCategoria.Value;
                      ER2Excel.Celula['AC' + IntToStr(iLinha)].Text := objRel.usp_relatoriocNmMarca.Value;
                      ER2Excel.Celula['AD' + IntToStr(iLinha)].Text := objRel.usp_relatorionCdPedido.Value;
                      ER2Excel.Celula['AE' + IntToStr(iLinha)].Text := objRel.usp_relatorionQtde.AsInteger;
                      ER2Excel.Celula['AF' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValUnitario.Value);
                      ER2Excel.Celula['AG' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValVenda.Value);
                      ER2Excel.Celula['AH' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00%', objRel.usp_relatorionPercDesconto.Value);
                      ER2Excel.Celula['AI' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValDesconto.Value);
                      ER2Excel.Celula['AJ' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValTotal.Value);
                      ER2Excel.Celula['AK' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', (objRel.usp_relatorionValTotal.Value - objRel.usp_relatorionValDesconto.Value));
                      ER2Excel.Celula['AL' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00%', objRel.usp_relatorionPercIPI.Value);
                      ER2Excel.Celula['AM' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValIPIItem.Value);
                      ER2Excel.Celula['AN' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00%', objRel.usp_relatorionPercICMSSub.Value);
                      ER2Excel.Celula['AO' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValICMSSubItem.Value);
                      ER2Excel.Celula['AP' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValCustoFinal.Value);
                      ER2Excel.Celula['AQ' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValUnitarioEsp.Value);
                      ER2Excel.Celula['AR' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValUnitarioPed.Value);
                      ER2Excel.Celula['AS' + IntToStr(iLinha)].Text := FormatFloat('#,##0.00', objRel.usp_relatorionValTotalItemFinal.Value); //c�lculo do esp.
                  end;

                  Inc(iLinha);

                  objRel.usp_relatorio.Next;
              end;

              { -- exporta planilha e limpa result do componente -- }
              ER2Excel.ExportXLS;
              ER2Excel.CleanupInstance;

              frmMenu.mensagemUsuario('');
          end ;
      except
          MensagemErro('Erro ao criar o relat�rio');
          raise;
      end;
  Finally;
    FreeAndNil(objRel);
  end;
end;

procedure TrptRecebimentoCompras.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRecebimentoCompras.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(76);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRecebimentoCompras.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryProduto.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin
    qryProduto.Parameters.ParamByName('nCdProduto').Value := MaskEdit4.Text ;
    qryProduto.Open ;
  end ;


end;

procedure TrptRecebimentoCompras.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(88);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRecebimentoCompras.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
    qryTipoReceb.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryTipoReceb.Parameters.ParamByName('nCdTipoReceb').Value := MaskEdit7.Text ;
    qryTipoReceb.Open ;
  end ;
end;

procedure TrptRecebimentoCompras.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      MaskEdit3.ReadOnly := True;
      MaskEdit3.Color    := $00E9E4E4;
  end;
end;

procedure TrptRecebimentoCompras.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit3.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;
end;

procedure TrptRecebimentoCompras.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja, Maskedit3.Text) ;
end;

initialization
      RegisterClass(TrptRecebimentoCompras) ;

end.
