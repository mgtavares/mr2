unit rPicking;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPicking = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonCdItemPedido: TIntegerField;
    qryPedidonCdProduto: TIntegerField;
    qryPedidocNmItem: TStringField;
    qryPedidonQtdePed: TBCDField;
    qryPedidonCdTipoItemPed: TIntegerField;
    qryPedidonCdAlaProducao: TIntegerField;
    qryPedidocNmAlaProducao: TStringField;
    qryPedidocComposicao: TStringField;
    QRShape5: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRBand3: TQRBand;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel4: TQRLabel;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRTotalPagina: TQRLabel;
    qryPedidocEnderecoEntrega: TStringField;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel10: TQRLabel;
    qryPedidocNmFantasia: TStringField;
    qryPedidocNmTerceiroRepres: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPicking: TrptPicking;

implementation

{$R *.dfm}

end.
