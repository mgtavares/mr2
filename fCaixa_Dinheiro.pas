unit fCaixa_Dinheiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxTextEdit, cxCurrencyEdit, cxControls, cxContainer, cxEdit,
  cxLabel, cxLookAndFeelPainters, StdCtrls, cxButtons;

type
  TfrmCaixa_Dinheiro = class(TForm)
    cxLabel1: TcxLabel;
    edtDinheiro: TcxCurrencyEdit;
    cxButton1: TcxButton;
    edtSaldo: TcxCurrencyEdit;
    cxLabel2: TcxLabel;
    procedure FormActivate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure edtDinheiroKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_Dinheiro: TfrmCaixa_Dinheiro;

implementation

{$R *.dfm}

procedure TfrmCaixa_Dinheiro.FormActivate(Sender: TObject);
begin

    edtDinheiro.SetFocus;

end;

procedure TfrmCaixa_Dinheiro.cxButton1Click(Sender: TObject);
begin

    Close ;
    
end;

procedure TfrmCaixa_Dinheiro.edtDinheiroKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (key = #13) then
        Close ;

end;

end.
