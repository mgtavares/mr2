unit fInputTEF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, Mask, cxButtons;

type
  TfrmInputTEF = class(TForm)
    btnMenuAnt: TcxButton;
    btnOK: TcxButton;
    edtValor: TMaskEdit;
    lblBuffer: TLabel;
    btnCancelar: TcxButton;
    lblTituloBox: TLabel;
    procedure edtValorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function  ShowInput(cTituloBox, cConteudo : string; iTipoCampo, iTamanhoMin, iTamanhoMax : integer; bExibeBtnPrev : boolean) : boolean;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AjustaTamanhoForm;
    procedure btnMenuAntClick(Sender: TObject);
  private
    { Private declarations }
    bRetorno : boolean;
  public
    { Public declarations }
    bComandoAnt : boolean;
  end;

var
  frmInputTEF: TfrmInputTEF;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmInputTEF.edtValorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = VK_RETURN) then
        btnOK.Click;
end;

function TfrmInputTEF.ShowInput(cTituloBox, cConteudo: string; iTipoCampo, iTamanhoMin, iTamanhoMax : integer; bExibeBtnPrev : boolean) : boolean;
begin
    if (bExibeBtnPrev) then
        btnMenuAnt.Visible := True
    else
        btnMenuAnt.Visible := False;

    edtValor.Text := '';

    //ShowMessage(IntToStr(iTipoCampo));

    case (iTipoCampo) of
        500: begin
                 edtValor.MaxLength := iTamanhoMax;

                 edtValor.PasswordChar := '*';
             end;

        513: edtValor.EditMask := '99/99;1;_';

        515: edtValor.EditMask := '99/99/9999;1;_';

        else
            edtValor.MaxLength := iTamanhoMax;
            
    end;

    lblTituloBox.Caption := cTituloBox;
    lblBuffer.Caption    := cConteudo;

    AjustaTamanhoForm();

    ShowModal;

    edtValor.EditMask := '';

    edtValor.PasswordChar := #0;

    if (bRetorno = False) then
    begin
{
        if (Length(edtValor.Text) < iTamanhoMin) then
        begin
            frmMenu.MensagemAlerta('A quantidade m�nima de caracteres permitido � ' + IntToStr(iTamanhoMin));

            Abort;
        end
}
    end;

    Result := bRetorno;
end;

procedure TfrmInputTEF.btnCancelarClick(Sender: TObject);
begin
    bRetorno := False;

    Close;
end;

procedure TfrmInputTEF.btnOKClick(Sender: TObject);
begin
    bRetorno := True;

    Close;
end;

procedure TfrmInputTEF.FormShow(Sender: TObject);
begin
    bComandoAnt := False;

    edtValor.SetFocus;
end;

procedure TfrmInputTEF.AjustaTamanhoForm;
var
    i, iQtdeLinha : integer;
    s : string;
begin
    iQtdeLinha := 0;

    s := lblBuffer.Caption;

    // Verificando a quantidade de quebras de linha
    for i := 1 to Length(s) do
        if (s[i] = (Char(13))) then
            iQtdeLinha := iQtdeLinha + 1;

    // Verificando se houve um t�tulo enviado
    if (Trim(lblTituloBox.Caption) = '') then
    begin
        lblTituloBox.Visible := False;

        lblBuffer.Top := lblTituloBox.Top;
    end

    else
    begin
        lblTituloBox.Visible := True;

        lblBuffer.Top := 40;
    end;

    //Ajusta dinamicamente o tamanho do form
    if (iQtdeLinha = 0) then
        if (Length(s) > 45) then
            lblBuffer.Height := 34
        else
            lblBuffer.Height := 17
    else
        lblBuffer.Height := iQtdeLinha * 17;

    edtValor.Top := (lblBuffer.Top + lblBuffer.Height + 9);

    btnMenuAnt.Top  := (edtValor.Top + edtValor.Height + 8);
    btnOK.Top       := (edtValor.Top + edtValor.Height + 8);
    btnCancelar.Top := (edtValor.Top + edtValor.Height + 8);

    Height := (btnOK.Top + btnOK.Height + 35);
end;

procedure TfrmInputTEF.btnMenuAntClick(Sender: TObject);
begin
    bComandoAnt := True;

    Close;
end;

end.
