inherited rptAcompanhaGiroProduto: TrptAcompanhaGiroProduto
  Left = 95
  Top = 138
  Height = 665
  Caption = 'Acompanhamento Giro Produto'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 600
  end
  object Label1: TLabel [1]
    Left = 89
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 72
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label3: TLabel [3]
    Left = 62
    Top = 96
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label4: TLabel [4]
    Left = 41
    Top = 120
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
  end
  object Label5: TLabel [5]
    Left = 61
    Top = 144
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label6: TLabel [6]
    Left = 80
    Top = 168
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label7: TLabel [7]
    Left = 39
    Top = 264
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
  end
  object Label8: TLabel [8]
    Left = 47
    Top = 312
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Estoque'
  end
  object Label9: TLabel [9]
    Left = 199
    Top = 312
    Width = 6
    Height = 13
    Caption = 'a'
  end
  object Label10: TLabel [10]
    Left = 57
    Top = 288
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Refer'#234'ncia'
  end
  object Label11: TLabel [11]
    Left = 84
    Top = 192
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label12: TLabel [12]
    Left = 71
    Top = 216
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cole'#231#227'o'
  end
  object Label13: TLabel [13]
    Left = 78
    Top = 240
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label15: TLabel [14]
    Left = 10
    Top = 336
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Estoque Zerado'
  end
  object Label16: TLabel [15]
    Left = 63
    Top = 360
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Giro Ideal'
  end
  object Label17: TLabel [16]
    Left = 142
    Top = 360
    Width = 15
    Height = 13
    Caption = 'x 1'
  end
  object Label14: TLabel [17]
    Left = 196
    Top = 384
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label18: TLabel [18]
    Left = 40
    Top = 384
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 26
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtLoja: TMaskEdit [20]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtDepartamento: TMaskEdit [21]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtDepartamentoExit
    OnKeyDown = edtDepartamentoKeyDown
  end
  object edtCategoria: TMaskEdit [22]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtCategoriaExit
    OnKeyDown = edtCategoriaKeyDown
  end
  object edtSubCategoria: TMaskEdit [23]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = edtSubCategoriaExit
    OnKeyDown = edtSubCategoriaKeyDown
  end
  object edtSegmento: TMaskEdit [24]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = edtSegmentoExit
    OnKeyDown = edtSegmentoKeyDown
  end
  object edtMarca: TMaskEdit [25]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtMarcaExit
    OnKeyDown = edtMarcaKeyDown
  end
  object RadioGroup2: TRadioGroup [26]
    Left = 8
    Top = 408
    Width = 289
    Height = 41
    Caption = 'Agrupamento'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Produto Estruturado'
      'Produto Final')
    TabOrder = 13
  end
  object DBEdit1: TDBEdit [27]
    Tag = 1
    Left = 184
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 16
  end
  object DBEdit2: TDBEdit [28]
    Tag = 1
    Left = 184
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = DataSource2
    TabOrder = 17
  end
  object DBEdit3: TDBEdit [29]
    Tag = 1
    Left = 184
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = DataSource3
    TabOrder = 18
  end
  object DBEdit4: TDBEdit [30]
    Tag = 1
    Left = 184
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = DataSource4
    TabOrder = 19
  end
  object DBEdit5: TDBEdit [31]
    Tag = 1
    Left = 184
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = DataSource5
    TabOrder = 20
  end
  object DBEdit6: TDBEdit [32]
    Tag = 1
    Left = 184
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource6
    TabOrder = 21
  end
  object edtGrupoProduto: TMaskEdit [33]
    Left = 112
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object DBEdit7: TDBEdit [34]
    Tag = 1
    Left = 184
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource7
    TabOrder = 22
  end
  object RadioGroup4: TRadioGroup [35]
    Left = 8
    Top = 456
    Width = 761
    Height = 41
    Caption = 'Ordena'#231#227'o'
    Columns = 7
    ItemIndex = 1
    Items.Strings = (
      'Dias Estoque '
      'Venda Total '
      'Venda Per'#237'odo'
      'Estoque Atual'
      'Giro Total'
      'Giro Per'#237'odo'
      '% Venda')
    TabOrder = 14
  end
  object edtDiasEstoqueIni: TMaskEdit [36]
    Left = 112
    Top = 304
    Width = 75
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 11
    Text = '         '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtDiasEstoqueFim: TMaskEdit [37]
    Left = 213
    Top = 304
    Width = 75
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 12
    Text = '         '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtReferencia: TEdit [38]
    Left = 112
    Top = 280
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 10
  end
  object edtLinha: TMaskEdit [39]
    Left = 112
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtLinhaExit
    OnKeyDown = edtLinhaKeyDown
  end
  object edtColecao: TMaskEdit [40]
    Left = 112
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtColecaoExit
    OnKeyDown = edtColecaoKeyDown
  end
  object edtClasseProduto: TMaskEdit [41]
    Left = 112
    Top = 232
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = edtClasseProdutoExit
    OnKeyDown = edtClasseProdutoKeyDown
  end
  object DBEdit8: TDBEdit [42]
    Tag = 1
    Left = 184
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = DataSource8
    TabOrder = 23
  end
  object DBEdit9: TDBEdit [43]
    Tag = 1
    Left = 184
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmColecao'
    DataSource = DataSource9
    TabOrder = 24
  end
  object DBEdit10: TDBEdit [44]
    Tag = 1
    Left = 184
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = DataSource10
    TabOrder = 25
  end
  object RadioGroup7: TRadioGroup [45]
    Left = 8
    Top = 504
    Width = 185
    Height = 41
    Caption = 'Tipo Ordena'#231#227'o'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Crescente'
      'Decrescente')
    TabOrder = 15
  end
  object RadioGroup5: TRadioGroup [46]
    Left = 325
    Top = 408
    Width = 193
    Height = 41
    Caption = 'Formul'#225'rio Zebrado'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 27
  end
  object edtDiasEstoqueZerado: TMaskEdit [47]
    Left = 112
    Top = 328
    Width = 75
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 28
    Text = '         '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtGiroIdeal: TMaskEdit [48]
    Left = 112
    Top = 352
    Width = 23
    Height = 21
    EditMask = '##;1; '
    MaxLength = 2
    TabOrder = 29
    Text = '  '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtDtFinal: TMaskEdit [49]
    Left = 224
    Top = 376
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 30
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [50]
    Left = 112
    Top = 376
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 31
    Text = '  /  /    '
  end
  object RadioGroup1: TRadioGroup [51]
    Left = 544
    Top = 408
    Width = 225
    Height = 41
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Tela'
      'Relat'#243'rio')
    TabOrder = 32
  end
  inherited ImageList1: TImageList
    Left = 872
    Top = 144
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 664
    Top = 192
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 632
    Top = 296
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmDepartamento, nCdDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 712
    Top = 224
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryDepartamento
    Left = 640
    Top = 304
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategoria, nCdCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 824
    Top = 200
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
  end
  object DataSource3: TDataSource
    DataSet = qryCategoria
    Left = 648
    Top = 312
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSubCategoria, nCdSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 792
    Top = 200
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
  end
  object DataSource4: TDataSource
    DataSet = qrySubCategoria
    Left = 656
    Top = 320
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmSegmento, nCdSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 712
    Top = 264
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
  end
  object DataSource5: TDataSource
    DataSet = qrySegmento
    Left = 664
    Top = 328
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca, nCdMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 752
    Top = 224
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object DataSource6: TDataSource
    DataSet = qryMarca
    Left = 672
    Top = 336
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 816
    Top = 264
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryGrupoProduto
    Left = 680
    Top = 344
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha '
      'WHERE nCdLinha = :nPK')
    Left = 816
    Top = 304
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryLinha
    Left = 624
    Top = 296
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao, cNmColecao'
      'FROM Colecao'
      'WHERE nCdColecao = :nPK')
    Left = 824
    Top = 352
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object DataSource9: TDataSource
    DataSet = qryColecao
    Left = 632
    Top = 304
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdClasseProduto, cNmClasseProduto'
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 800
    Top = 408
    object qryClasseProdutonCdClasseProduto: TIntegerField
      FieldName = 'nCdClasseProduto'
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object DataSource10: TDataSource
    DataSet = qryClasseProduto
    Left = 640
    Top = 312
  end
  object qryCampanhaPromoc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdCampanhaPromoc'
      '      ,cNmCampanhaPromoc '
      '      ,cFlgAtivada'
      '      ,cFlgSuspensa'
      '  FROM CampanhaPromoc'
      ' WHERE nCdEmpresa        = :nCdEmpresa'
      '   AND nCdCampanhaPromoc = :nPK')
    Left = 904
    Top = 264
    object qryCampanhaPromocnCdCampanhaPromoc: TIntegerField
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryCampanhaPromoccNmCampanhaPromoc: TStringField
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryCampanhaPromoccFlgAtivada: TIntegerField
      FieldName = 'cFlgAtivada'
    end
    object qryCampanhaPromoccFlgSuspensa: TIntegerField
      FieldName = 'cFlgSuspensa'
    end
  end
  object DataSource11: TDataSource
    DataSet = qryCampanhaPromoc
    Left = 648
    Top = 320
  end
  object DataSource12: TDataSource
    Left = 624
    Top = 320
  end
end
