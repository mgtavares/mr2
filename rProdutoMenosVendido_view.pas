unit rProdutoMenosVendido_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptProdutoMenosVendido_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape2: TQRShape;
    SPREL_PRODUTO_MENOS_VENDIDO: TADOStoredProc;
    QRDBText30: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    SPREL_PRODUTO_MENOS_VENDIDOcNmDepartamento: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOcNmCategoria: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOcNmSubCategoria: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOcNmSegmento: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOnCdProduto: TIntegerField;
    SPREL_PRODUTO_MENOS_VENDIDOcNmProduto: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOcNmMarca: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOcReferencia: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOnQtdeVenda: TBCDField;
    SPREL_PRODUTO_MENOS_VENDIDOnQtdeEstoque: TBCDField;
    SPREL_PRODUTO_MENOS_VENDIDOnValCusto: TBCDField;
    SPREL_PRODUTO_MENOS_VENDIDOnValVenda: TBCDField;
    SPREL_PRODUTO_MENOS_VENDIDOdDtUltReceb: TStringField;
    SPREL_PRODUTO_MENOS_VENDIDOnCdCampanhaPromoc: TIntegerField;
    SPREL_PRODUTO_MENOS_VENDIDOcChaveCampanha: TStringField;
    QRLabel18: TQRLabel;
    SPREL_PRODUTO_MENOS_VENDIDOnQtdeTransito: TBCDField;
    QRDBText11: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptProdutoMenosVendido_view: TrptProdutoMenosVendido_view;

implementation

{$R *.dfm}

end.
