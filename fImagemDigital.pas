unit fImagemDigital;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, cxPC, cxLookAndFeelPainters,
  DelphiTwain, cxButtons, DBCtrls, Menus, cxContainer, cxGroupBox;

type
  TfrmImagemDigital = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    GroupBox1: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryImagemDigital: TADOQuery;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    qryImagemDigitalnCdImagemDigital: TIntegerField;
    qryImagemDigitalnCdTerceiro: TIntegerField;
    qryImagemDigitalnCdTabTipoImagemDigital: TIntegerField;
    qryImagemDigitaldDtDigitalizacao: TDateTimeField;
    qryImagemDigitalnCdUsuario: TIntegerField;
    qryImagemDigitalnCdLoja: TIntegerField;
    qryImagemDigitalcImagem: TBlobField;
    DBImage1: TDBImage;
    dsImagemDigital: TDataSource;
    cxButton1: TcxButton;
    DelphiTwain1: TDelphiTwain;
    cxButton2: TcxButton;
    qryImagensArmazenadas: TADOQuery;
    qryImagensArmazenadasnCdImagemDigital: TIntegerField;
    qryImagensArmazenadasnCdTabTipoImagemDigital: TIntegerField;
    qryImagensArmazenadascNmTabTipoImagemDigital: TStringField;
    qryImagensArmazenadasdDtDigitalizacao: TDateTimeField;
    qryImagensArmazenadasnCdUsuario: TIntegerField;
    qryImagensArmazenadascNmUsuario: TStringField;
    qryImagensArmazenadasnCdLoja: TIntegerField;
    qryImagensArmazenadascNmLoja: TStringField;
    dsImagensArmazenadas: TDataSource;
    cxGrid1DBTableView1nCdImagemDigital: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTabTipoImagemDigital: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabTipoImagemDigital: TcxGridDBColumn;
    cxGrid1DBTableView1dDtDigitalizacao: TcxGridDBColumn;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    VisualizarImagem1: TMenuItem;
    DBLookupComboBox1: TDBLookupComboBox;
    qryTabTipoImagemDigital: TADOQuery;
    qryTabTipoImagemDigitalnCdTabTipoImagemDigital: TIntegerField;
    qryTabTipoImagemDigitalcNmTabTipoImagemDigital: TStringField;
    dsTabTipoImagemDigital: TDataSource;
    Label1: TLabel;
    cxTabCapturar: TcxTabSheet;
    cxGroupBox1: TcxGroupBox;
    PanelWeb: TPanel;
    cxGroupBox2: TcxGroupBox;
    btnDesligar: TcxButton;
    btnCapturar: TcxButton;
    btnLigar: TcxButton;
    DBLookupComboBox2: TDBLookupComboBox;
    Label2: TLabel;
    cxGroupBox3: TcxGroupBox;
    cxButton3: TcxButton;
    DBImage2: TDBImage;
    btExcluirImagem: TMenuItem;
    qryDeletarImagem: TADOQuery;
    procedure cxButton1Click(Sender: TObject);
    procedure DelphiTwain1TwainAcquire(Sender: TObject;
      const Index: Integer; Image: TBitmap; var Cancel: Boolean);
    procedure DocumentoCliente(nCdTerceiroIn:integer) ;
    procedure cxPageControl1Change(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure VisualizarImagem1Click(Sender: TObject);
    procedure btnLigarClick(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure btnDesligarClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure btExcluirImagemClick(Sender: TObject);
  private
    { Private declarations }
    cFuncao     : string ;
    nCdTerceiro : integer ;
  public
    { Public declarations }
  end;

var
  frmImagemDigital: TfrmImagemDigital;
  HwebCam: HWND;

implementation

uses fMenu, fImagemDigital_Visualizar;

    function capCreateCaptureWindowA(lpszWindowName: pchar; dwStyle: dword; x, y, nWidth, nHeight: word; ParentWin: dword; nId: word): dword; stdcall external 'avicap32.dll';

{$R *.dfm}

procedure TfrmImagemDigital.cxButton1Click(Sender: TObject);
var
  SourceIndex: Integer;
  Source: TTwainSource;
begin

  qryImagemDigital.Close ;
  qryImagemDigital.Open ;

  qryImagemDigital.Insert ;

  //Make sure that the library and Source Manager
  //are loaded
  DelphiTwain1.LibraryLoaded := TRUE;
  DelphiTwain1.SourceManagerLoaded := TRUE;
  //SelectSource method displays a common Twain dialog
  //to allow the user to select one of the avaliable
  //sources and returns it's index or -1 if either
  //the user pressed Cancel or if there were no sources
  SourceIndex := DelphiTwain1.SelectSource();
  if (SourceIndex <> -1) then
  begin
     //Now that we know the index of the source, we'll
     //get the object for this source
     Source := DelphiTwain1.Source[SourceIndex];
     //Load source and acquire image
     Source.Loaded := TRUE;
     Source.Enabled := TRUE;
  end; {if (SourceIndex <> -1)}

  cxButton2.Enabled := True ;

end;

procedure TfrmImagemDigital.DelphiTwain1TwainAcquire(Sender: TObject;
  const Index: Integer; Image: TBitmap; var Cancel: Boolean);
begin
 //Copies the acquired bitmap to the TImage control
  DBImage1.Picture.Assign(Image);
  //Because the component supports multiple images
  //from the source device, Cancel will tell the
  //source that we don't want no more images
  Cancel := TRUE;

end;

procedure TfrmImagemDigital.DocumentoCliente(nCdTerceiroIn: integer);
begin

    cFuncao     := 'CLIVAREJO' ;
    nCdTerceiro := nCdTerceiroIn ;

    qryImagensArmazenadas.SQL.Strings[12] := 'WHERE ImagemDigital.nCdTerceiro = ' + IntToStr(nCdTerceiro) ;

    cxPageControl1.ActivePageIndex := 0 ;

    qryTabTipoImagemDigital.Close ;
    qryTabTipoImagemDigital.Open ;

    qryImagemDigital.Close ;
    qryImagemDigital.Open ;

    qryImagemDigital.Insert ;

    Self.ShowModal;


end;

procedure TfrmImagemDigital.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePageIndex = 2) then
  begin
      qryImagensArmazenadas.Close ;
      qryImagensArmazenadas.Open ;
  end ;

end;

procedure TfrmImagemDigital.cxButton2Click(Sender: TObject);
begin
  inherited;

  if not qryImagemDigital.Active then
  begin
      MensagemAlerta('Nenhuma imagem digitalizada para armazenamento.') ;
      abort ;
  end ;

  case MessageDlg('Confirma o tipo de imagem selecionada ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      if (cFuncao = 'CLIVAREJO') then qryImagemDigitalnCdTerceiro.Value := nCdTerceiro ;

      qryImagemDigitaldDtDigitalizacao.Value        := Now() ;
      qryImagemDigitalnCdUsuario.Value              := frmMenu.nCdUsuarioLogado ;
      qryImagemDigitalnCdLoja.Value                 := frmMenu.nCdLojaAtiva;
      qryImagemDigitalnCdTabTipoImagemDigital.Value := qryTabTipoImagemDigitalnCdTabTipoImagemDigital.Value ;

      if (qryImagemDigital.State = dsInsert) then
          qryImagemDigitalnCdImagemDigital.Value := frmMenu.fnProximoCodigo('IMAGEMDIGITAL');

      qryImagemDigital.Post ;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryImagemDigital.Insert ;

  cxButton1.SetFocus;
  cxButton2.Enabled := False ;

end;

procedure TfrmImagemDigital.VisualizarImagem1Click(Sender: TObject);
var
  objForm : TfrmImagemDigital_Visualizar ;
begin
  inherited;

  if (not qryImagensArmazenadas.Eof) then
  begin

      objForm := TfrmImagemDigital_Visualizar.Create( Self ) ;

      PosicionaQuery(objForm.qryImagemDigital , qryImagensArmazenadasnCdImagemDigital.AsString) ;
      objForm.ShowModal;
      objForm.qryImagemDigital.Close ;

      freeAndNil( objForm ) ;

  end ;

end;

procedure TfrmImagemDigital.btnLigarClick(Sender: TObject);
const
    WM_WEBCAM                 = WM_USER;
    WM_CONECTAR_DRIVER_WEBCAM = WM_WEBCAM + 10;
    WM_SET_PREVIEW            = WM_WEBCAM + 50;
    WM_SET_PREVIEW_RATE       = WM_WEBCAM + 52;
    WM_SET_SCALE              = WM_WEBCAM + 53;
var
    i : integer;
begin
    HwebCam := capCreateCaptureWindowA('CaptureWindow', WS_CHILD + WS_VISIBLE, 0, 0, 640, 480, PanelWeb.Handle, 0);

    for i := 1 to 3 do
       if (SendMessage(HwebCam, WM_CONECTAR_DRIVER_WEBCAM, 0, 0) <> 1) and (i = 3) then
       begin
           HwebCam := 0;

           ShowMessage('N�o foi poss�vel iniciar o dispositivo de captura.');

           Exit;
       end;

    SendMessage(HwebCam, WM_CONECTAR_DRIVER_WEBCAM, 0, 0);
    SendMessage(HwebCam, WM_SET_PREVIEW, -1, 0);
    SendMessage(HwebCam, WM_SET_PREVIEW_RATE, 100, 0);
    SendMessage(HwebCam, WM_SET_SCALE, -1, 0);

    btnLigar.Enabled    := False;
    btnCapturar.Enabled := True;
    btnDesligar.Enabled := True;
end;

procedure TfrmImagemDigital.btnCapturarClick(Sender: TObject);
const
   WM_WEBCAM         = WM_USER;
   WM_SALVAR_CAPTURA = WM_WEBCAM + 25;
begin
   SendMessage(HwebCam, WM_SALVAR_CAPTURA, 0, longint(pchar('C:\temp.bmp')));

   DBImage2.Picture.LoadFromFile('C:\temp.bmp');

   cxButton3.Enabled := True ;
   
//   ShowMessage('Imagem salva com sucesso!');
end;

procedure TfrmImagemDigital.btnDesligarClick(Sender: TObject);
const
   WM_WEBCAM             = WM_USER;
   WM_DESCONECTAR_WEBCAM = WM_WEBCAM + 11;
   WM_FECHAR_WEBCAM      = WM_WEBCAM + 68;
begin
   SendMessage(HwebCam, WM_DESCONECTAR_WEBCAM, 0, 0);
   SendMessage(HwebCam, WM_FECHAR_WEBCAM, 0, 0);

   btnLigar.Enabled    := True;
   btnCapturar.Enabled := False;
   btnDesligar.Enabled := False;
end;

procedure TfrmImagemDigital.cxButton3Click(Sender: TObject);
begin
  inherited;

  if not qryImagemDigital.Active then
  begin
      MensagemAlerta('Nenhuma imagem Capturada para armazenamento.') ;
      abort ;
  end ;

  case MessageDlg('Confirma o tipo de imagem selecionada ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      if (cFuncao = 'CLIVAREJO') then qryImagemDigitalnCdTerceiro.Value := nCdTerceiro ;

      qryImagemDigitaldDtDigitalizacao.Value        := Now() ;
      qryImagemDigitalnCdUsuario.Value              := frmMenu.nCdUsuarioLogado ;
      qryImagemDigitalnCdLoja.Value                 := frmMenu.nCdLojaAtiva;
      qryImagemDigitalnCdTabTipoImagemDigital.Value := qryTabTipoImagemDigitalnCdTabTipoImagemDigital.Value ;

      if (qryImagemDigital.State = dsInsert) then
          qryImagemDigitalnCdImagemDigital.Value := frmMenu.fnProximoCodigo('IMAGEMDIGITAL');

      qryImagemDigital.Post ;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryImagemDigital.Insert ;

  btnDesligar.Click;
  cxButton3.Enabled := False ;
end;

procedure TfrmImagemDigital.btExcluirImagemClick(Sender: TObject);
begin
  inherited;

  if (qryImagensArmazenadas.IsEmpty) then
      Exit;

  case MessageDlg('Confirma exclus�o da imagem selecionada ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;

      qryDeletarImagem.Close;
      qryDeletarImagem.Parameters.ParamByName('nPK').Value := qryImagensArmazenadasnCdImagemDigital.Value;
      qryDeletarImagem.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryImagensArmazenadas.Close;
      qryImagensArmazenadas.Open;

      ShowMessage('Imagem exclu�da com sucesso.');
  except
      MensagemErro('Erro no processamento.');
      Raise;
      frmMenu.Connection.RollbackTrans;
  end;
end;

end.
