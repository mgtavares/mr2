inherited rptVendaDiariaXFormaPagto: TrptVendaDiariaXFormaPagto
  Left = 353
  Top = 279
  Width = 693
  Height = 245
  Caption = 'Rel. Venda Di'#225'ria x Forma Pagto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 677
    Height = 183
  end
  object Label6: TLabel [1]
    Left = 188
    Top = 72
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [2]
    Left = 9
    Top = 72
    Width = 91
    Height = 13
    Caption = 'Per'#237'odo Movimento'
  end
  object Label5: TLabel [3]
    Left = 80
    Top = 48
    Width = 20
    Height = 13
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    Width = 677
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit2: TMaskEdit [5]
    Left = 216
    Top = 64
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit1: TMaskEdit [6]
    Left = 104
    Top = 64
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 172
    Top = 40
    Width = 493
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 4
  end
  object edtLoja: TMaskEdit [8]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  inherited ImageList1: TImageList
    Left = 232
    Top = 168
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 264
    Top = 168
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 264
    Top = 136
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object usp_Relatorio: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SPREL_VENDA_DIARIA_X_FORMA_PAGTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 8
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '03/05/2010'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '03/05/2010'
      end>
    Left = 232
    Top = 136
    object usp_RelatoriocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object usp_RelatoriodDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object usp_RelatorionValOper: TBCDField
      FieldName = 'nValOper'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValDinheiro: TBCDField
      FieldName = 'nValDinheiro'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValCartao: TBCDField
      FieldName = 'nValCartao'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValTEF: TBCDField
      FieldName = 'nValTEF'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValCrediario: TBCDField
      FieldName = 'nValCrediario'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValRecPrest: TBCDField
      FieldName = 'nValRecPrest'
      Precision = 12
      Size = 2
    end
    object usp_RelatorionValDescontoCondPagto: TBCDField
      FieldName = 'nValDescontoCondPagto'
      Precision = 12
      Size = 2
    end
  end
end
