unit fCobranca_VisaoContato_Contato_SelContato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCobranca_VisaoContato_Contato_SelContato = class(TfrmProcesso_Padrao)
    qryListaContato: TADOQuery;
    dsListaContato: TDataSource;
    qryListaContatonCdTerceiro: TIntegerField;
    qryListaContatocNmTipoTelefone: TStringField;
    qryListaContatocTelefone: TStringField;
    qryListaContatocRamal: TStringField;
    qryListaContatocContato: TStringField;
    qryListaContatocDepartamento: TStringField;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCobranca_VisaoContato_Contato_SelContato: TfrmCobranca_VisaoContato_Contato_SelContato;

implementation

{$R *.dfm}

procedure TfrmCobranca_VisaoContato_Contato_SelContato.FormShow(
  Sender: TObject);
begin
  inherited;

  qryListaContato.Close;
  qryListaContato.Open;

  DBGridEh1.SetFocus;
  
end;

procedure TfrmCobranca_VisaoContato_Contato_SelContato.FormKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  {}
end;

procedure TfrmCobranca_VisaoContato_Contato_SelContato.FormKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  {}
end;

procedure TfrmCobranca_VisaoContato_Contato_SelContato.DBGridEh1KeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
      close ;
      
end;

procedure TfrmCobranca_VisaoContato_Contato_SelContato.ToolButton2Click(
  Sender: TObject);
begin
  qryListaContato.Close;
  inherited;

end;

procedure TfrmCobranca_VisaoContato_Contato_SelContato.DBGridEh1DblClick(
  Sender: TObject);
begin
  inherited;

  close ;

end;

end.
