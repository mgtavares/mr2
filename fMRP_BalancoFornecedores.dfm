inherited frmMRP_BalancoFornecedores: TfrmMRP_BalancoFornecedores
  Left = 275
  Top = 256
  Width = 588
  Height = 302
  BorderIcons = [biSystemMenu]
  Caption = 'Balanceamento Fornecedores'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 572
    Height = 237
  end
  inherited ToolBar1: TToolBar
    Width = 572
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 572
    Height = 237
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataSource = dsFornecedores
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nPercentCompra'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNrContrato'
        Footers = <>
        ReadOnly = True
      end>
  end
  object qryFornecedores: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      ''
      'Set @nCdProduto = :nPK'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'Set @nCdProduto = IsNull(@nCdProdutoPai,@nCdProduto)'
      ''
      'SELECT *'
      '  FROM ##Temp_Fornecedores'
      ' WHERE nCdProduto = @nCdProduto')
    Left = 184
    Top = 112
    object qryFornecedoresnCdTerceiro: TIntegerField
      DisplayLabel = 'Fornecedor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryFornecedorescNmTerceiro: TStringField
      DisplayLabel = 'Fornecedor|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryFornecedoresnPercentCompra: TBCDField
      DisplayLabel = '% Compra'
      FieldName = 'nPercentCompra'
      Precision = 12
      Size = 2
    end
    object qryFornecedorescNrContrato: TStringField
      DisplayLabel = 'N'#250'mero|Contrato'
      FieldName = 'cNrContrato'
      Size = 15
    end
  end
  object dsFornecedores: TDataSource
    DataSet = qryFornecedores
    Left = 240
    Top = 120
  end
end
