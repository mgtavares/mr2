unit fLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmLoja = class(TfrmCadastro_Padrao)
    qryMasternCdLoja: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmLoja: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdContaBancariaCofre: TIntegerField;
    qryMasternCdSerieFiscal: TIntegerField;
    qryMastercFlgVenda: TIntegerField;
    qryMastercFlgUsaPDV: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qryContaCofre: TADOQuery;
    qryContaCofrenCdConta: TStringField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    qrySerieFiscal: TADOQuery;
    qrySerieFiscalcModelo: TStringField;
    qrySerieFiscalcSerie: TStringField;
    Label7: TLabel;
    DBEdit10: TDBEdit;
    DataSource4: TDataSource;
    Label8: TLabel;
    DBEdit11: TDBEdit;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdUsuario: TIntegerField;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryUsuarioLojacNmUsuario: TStringField;
    cxPageControl1: TcxPageControl;
    tabUsuario: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsUsuarioLoja: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryMasternCdLocalEstoquePadrao: TIntegerField;
    qryMasternCdCCReceita: TIntegerField;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    Label10: TLabel;
    DBEdit13: TDBEdit;
    qryLocalEstoque: TADOQuery;
    qryCC: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit14: TDBEdit;
    DataSource5: TDataSource;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    DBEdit15: TDBEdit;
    DataSource6: TDataSource;
    qryMastercMsgCupomFiscal: TStringField;
    Label11: TLabel;
    DBEdit16: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    qryMasternCdLojaCobradora: TIntegerField;
    qryMastercFlgLojaCobradora: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    Label12: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    qryLojaCobradora: TADOQuery;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoranCdEmpresa: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    dsLojaCobradora: TDataSource;
    qryLojaCobradoracFlgLojaCobradora: TIntegerField;
    qryUsuarioLojanCdUsuarioLoja: TIntegerField;
    tabCrediarioLoja: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryCrediarioLoja: TADOQuery;
    dsCrediarioLoja: TDataSource;
    qryCrediarioLojanCdCrediarioLoja: TIntegerField;
    qryCrediarioLojanCdLojaVenda: TIntegerField;
    qryCrediarioLojanCdLoja: TIntegerField;
    qryNomeLoja: TADOQuery;
    dsNomeLoja: TDataSource;
    qryCrediarioLojacNmLoja: TStringField;
    qryNomeLojacNmLoja: TStringField;
    tabCentroDistrib: TcxTabSheet;
    gridCD: TDBGridEh;
    qryPrioridadeCentroDistribuicao: TADOQuery;
    qryPrioridadeCentroDistribuicaonCdPrioridadeCentroDistribuicao: TIntegerField;
    qryPrioridadeCentroDistribuicaonCdLoja: TIntegerField;
    qryPrioridadeCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryPrioridadeCentroDistribuicaoiPrioridade: TIntegerField;
    dsPrioridadeCentroDistribuicao: TDataSource;
    qryCentroDistribuicao: TADOQuery;
    qryPrioridadeCentroDistribuicaocNmCentroDistribuicao: TStringField;
    qryValidaCD: TADOQuery;
    qryPrioridadeCentroDistribuicaonCdCriterioSolicitacaoCD: TIntegerField;
    qryCriterioSolicitacaoCD: TADOQuery;
    qryPrioridadeCentroDistribuicaocNmCriterioSolicitacaoCD: TStringField;
    qryValidaPrioridade: TADOQuery;
    tabParamComercial: TcxTabSheet;
    Image2: TImage;
    qryMasternCdTipoPedidoTransfSai: TIntegerField;
    qryMasternCdTipoPedidoTransfEnt: TIntegerField;
    qryMasternCdTipoPedidoRecTransf: TIntegerField;
    qryMasternCdTipoPedidoVendaSai: TIntegerField;
    qryMasternCdTipoPedidoVendaEnt: TIntegerField;
    qryMasternCdTipoPedidoRecVenda: TIntegerField;
    qryMasternCdTipoPedidoDevVendaSai: TIntegerField;
    qryMasternCdTipoPedidoDevVendaEnt: TIntegerField;
    qryTipoPedidoTransfSai: TADOQuery;
    qryTipoPedidoTransfEnt: TADOQuery;
    qryTipoPedidoRecTransf: TADOQuery;
    qryTipoPedidoVendaSai: TADOQuery;
    qryTipoPedidoVendaEnt: TADOQuery;
    qryTipoPedidoRecVenda: TADOQuery;
    qryTipoPedidoDevVendaSai: TADOQuery;
    qryTipoPedidoDevVendaEnt: TADOQuery;
    dsTipoPedidoTransfSai: TDataSource;
    dsTipoPedidoTransfEnt: TDataSource;
    dsTipoPedidoRecTransf: TDataSource;
    dsTipoPedidoVendaSai: TDataSource;
    dsTipoPedidoVendaEnt: TDataSource;
    dsTipoPedidoRecVenda: TDataSource;
    dsTipoPedidoDevVendaSai: TDataSource;
    dsTipoPedidoDevVendaEnt: TDataSource;
    GroupBox1: TGroupBox;
    Label16: TLabel;
    DBEdit25: TDBEdit;
    Label17: TLabel;
    DBEdit26: TDBEdit;
    Label18: TLabel;
    DBEdit27: TDBEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label19: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    Label21: TLabel;
    DBEdit30: TDBEdit;
    Label22: TLabel;
    DBEdit31: TDBEdit;
    Label23: TLabel;
    DBEdit32: TDBEdit;
    qryTipoPedidoTransfSainCdTipoPedido: TIntegerField;
    qryTipoPedidoTransfSaicNmTipoPedido: TStringField;
    qryTipoPedidoTransfEntnCdTipoPedido: TIntegerField;
    qryTipoPedidoTransfEntcNmTipoPedido: TStringField;
    qryTipoPedidoRecTransfnCdTipoReceb: TIntegerField;
    qryTipoPedidoRecTransfcNmTipoReceb: TStringField;
    qryTipoPedidoVendaSainCdTipoPedido: TIntegerField;
    qryTipoPedidoVendaSaicNmTipoPedido: TStringField;
    qryTipoPedidoVendaEntnCdTipoPedido: TIntegerField;
    qryTipoPedidoVendaEntcNmTipoPedido: TStringField;
    qryTipoPedidoRecVendanCdTipoReceb: TIntegerField;
    qryTipoPedidoRecVendacNmTipoReceb: TStringField;
    qryTipoPedidoDevVendaSainCdTipoPedido: TIntegerField;
    qryTipoPedidoDevVendaSaicNmTipoPedido: TStringField;
    qryTipoPedidoDevVendaEntnCdTipoPedido: TIntegerField;
    qryTipoPedidoDevVendaEntcNmTipoPedido: TStringField;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit37: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioLojaCalcFields(DataSet: TDataSet);
    procedure qryUsuarioLojaBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit12Exit(Sender: TObject);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17Exit(Sender: TObject);
    procedure qryCrediarioLojaCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCrediarioLojaBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure gridCDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryPrioridadeCentroDistribuicaoBeforePost(DataSet: TDataSet);
    procedure gridCDEnter(Sender: TObject);
    procedure DBEdit25Exit(Sender: TObject);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit26Exit(Sender: TObject);
    procedure DBEdit26KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit27Exit(Sender: TObject);
    procedure DBEdit27KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit29Exit(Sender: TObject);
    procedure DBEdit29KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit30Exit(Sender: TObject);
    procedure DBEdit30KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit31Exit(Sender: TObject);
    procedure DBEdit31KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit32Exit(Sender: TObject);
    procedure DBEdit32KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    nCdLoja : integer;

  public
    { Public declarations }
  end;

var
  frmLoja: TfrmLoja;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmLoja.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LOJA' ;
  nCdTabelaSistema  := 65 ;
  nCdConsultaPadrao := 147 ;
  bCodigoAutomatico := False ;
  bLimpaAposSalvar  := False;

end;

procedure TfrmLoja.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus ;
end;

procedure TfrmLoja.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;

end;

procedure TfrmLoja.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit4.Text) ;

end;

procedure TfrmLoja.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryContaCofre.Close ;
  PosicionaQuery(qryContaCofre, DbEdit5.Text) ;

end;

procedure TfrmLoja.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qrySerieFiscal.Close ;
  PosicionaQuery(qrySerieFiscal, DBEdit6.Text) ;

end;

procedure TfrmLoja.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryContaCofre.Close ;
  qrySerieFiscal.Close ;
  qryUsuarioLoja.Close ;
  qryCrediarioLoja.Close ;
  qryLocalEstoque.Close ;
  qryCC.Close ;
  qryLojaCobradora.Close;
  qryPrioridadeCentroDistribuicao.Close;

  qryTipoPedidoTransfSai.Close;
  qryTipoPedidoTransfEnt.Close;
  qryTipoPedidoRecTransf.Close;
  qryTipoPedidoVendaSai.Close;
  qryTipoPedidoVendaEnt.Close;
  qryTipoPedidoRecVenda.Close;
  qryTipoPedidoDevVendaSai.Close;
  qryTipoPedidoDevVendaEnt.Close;
end;

procedure TfrmLoja.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryContaCofre.Close ;
  qrySerieFiscal.Close ;
  qryUsuarioLoja.Close ;
  qryCrediarioLoja.Close ;
  qryLocalEstoque.Close ;
  qryCC.Close ;
  qryLojaCobradora.Close;
  qryPrioridadeCentroDistribuicao.Close;
  qryTipoPedidoTransfSai.Close;
  qryTipoPedidoTransfEnt.Close;
  qryTipoPedidoRecTransf.Close;
  qryTipoPedidoVendaSai.Close;
  qryTipoPedidoVendaEnt.Close;
  qryTipoPedidoRecVenda.Close;
  qryTipoPedidoDevVendaSai.Close;
  qryTipoPedidoDevVendaEnt.Close;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryContaCofre, qryMasternCdContaBancariaCofre.AsString) ;
  PosicionaQuery(qrySerieFiscal, qryMasternCdSerieFiscal.AsString) ;
  PosicionaQuery(qryUsuarioLoja, qryMasternCdLoja.asString) ;
  PosicionaQuery(qryCrediarioLoja, qryMasternCdLoja.AsString) ;
  PosicionaQuery(qryCC, qryMasternCdCCReceita.asString) ;
  PosicionaQuery(qryLojaCobradora, qryMasternCdLojaCobradora.asString) ;
  PosicionaQuery(qryPrioridadeCentroDistribuicao, qryMasternCdLoja.AsString);
  PosicionaQuery(qryTipoPedidoTransfSai, qryMasternCdTipoPedidoTransfSai.AsString);
  PosicionaQuery(qryTipoPedidoTransfEnt, qryMasternCdTipoPedidoTransfEnt.AsString);

  if (not qryTipoPedidoTransfEnt.IsEmpty) then
      qryTipoPedidoRecTransf.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidoTransfEntnCdTipoPedido.Value;

  PosicionaQuery(qryTipoPedidoRecTransf, qryMasternCdTipoPedidoRecTransf.AsString);
  PosicionaQuery(qryTipoPedidoVendaSai, qryMasternCdTipoPedidoVendaSai.AsString);
  PosicionaQuery(qryTipoPedidoVendaEnt, qryMasternCdTipoPedidoVendaEnt.AsString);

  if (not qryTipoPedidoVendaEnt.IsEmpty) then
      qryTipoPedidoRecVenda.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidoVendaEntnCdTipoPedido.Value;

  PosicionaQuery(qryTipoPedidoRecVenda, qryMasternCdTipoPedidoRecVenda.AsString);
  PosicionaQuery(qryTipoPedidoDevVendaSai, qryMasternCdTipoPedidoDevVendaSai.AsString);
  PosicionaQuery(qryTipoPedidoDevVendaEnt, qryMasternCdTipoPedidoDevVendaEnt.AsString);

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryLocalEstoque, qryMasterncdLocalEstoquePadrao.AsString) ;

  cxPageControl1.ActivePage := tabUsuario;

  if (qryMaster.IsEmpty) then
      DBEdit1.SetFocus
  else
      DBEdit2.SetFocus;
end;

procedure TfrmLoja.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DbEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o c�digo da loja.') ;
      DbEdit1.SetFocus ;
      abort ;
  end ;

  if (DbEdit7.Text = '') then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (DbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o nome da loja.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro da loja.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgVenda.Value = 1) and (DBEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o centro de custo de receita da loja.') ;
      DBEdit13.SetFocus;
      abort ;
  end ;

  if (DbEdit17.Text <> '') and (DbEdit18.Text = '')then
  begin
      MensagemAlerta('Loja cobradora invalida.') ;
      DbEdit17.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdTipoPedidoTransfSai.Value <> 0) and (DBEdit22.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de sa�da para faturamento de transfer�ncias informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit25.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoTransfEnt.Value <> 0) and (DBEdit23.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de entrada para recebimento de transfer�ncias faturadas informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit26.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoRecTransf.Value <> 0) and (DBEdit24.Text = '')) then
  begin
      MensagemAlerta('Tipo de recebimento para recebimento de transfer�ncias faturadas informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit27.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoVendaSai.Value <> 0) and (DBEdit33.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de venda para faturamento de pedidos informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit28.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoVendaEnt.Value <> 0) and (DBEdit34.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de compra para recebimento de compras informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit29.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoRecVenda.Value <> 0) and (DBEdit36.Text = '')) then
  begin
      MensagemAlerta('Tipo de recebimento de compras informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit30.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoDevVendaSai.Value <> 0) and (DBEdit35.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de devolu��o de compras informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit31.SetFocus;
      Abort;
  end;

  if ((qryMasternCdTipoPedidoDevVendaEnt.Value <> 0) and (DBEdit37.Text = '')) then
  begin
      MensagemAlerta('Tipo de pedido de recebimento de devolu��o de vendas informado inv�lido.');
      cxPageControl1.ActivePage := tabParamComercial;
      DBEdit32.SetFocus;
      Abort;
  end;

  nCdLoja := 0;

  if ((DbEdit17.Text = '') and (qryMaster.State = dsInsert)) then
      nCdLoja := qryMasternCdLoja.Value;
end;

procedure TfrmLoja.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(148);

            If (nPK > 0) then
            begin
                qryMasternCdContaBancariaCofre.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(78);

            If (nPK > 0) then
            begin
                qryMasternCdSerieFiscal.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioLoja.State = dsBrowse) then
             qryUsuarioLoja.Edit ;

        if (qryUsuarioLoja.State = dsInsert) or (qryUsuarioLoja.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioLojanCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.qryUsuarioLojaCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryUsuarioLojanCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioLojacNmUsuario.Value := qryUsuariocNmUsuario.Value ;

end;

procedure TfrmLoja.qryUsuarioLojaBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioLojacNmUsuario.Value = '') then
  begin
      MensagemAlerta('Informe o usu�rio.') ;
      abort ;
  end ;

  qryUsuarioLojanCdLoja.Value := qryMasternCdLoja.Value ;

  if (qryUsuarioLoja.State = dsInsert) then
      qryUsuarioLojanCdUsuarioLoja.Value := frmMenu.fnProximoCodigo('USUARIOLOJA') ;

end;

procedure TfrmLoja.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

    if (qryMaster.State = dsInsert) then
      qryMaster.Post ;

end;

procedure TfrmLoja.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuarioLoja, qryMasternCdLoja.AsString);
  PosicionaQuery(qryCrediarioLoja, qryMasternCdLoja.AsString);

end;

procedure TfrmLoja.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryLocalEstoque, DBEdit12.Text) ;

end;

procedure TfrmLoja.DBEdit13Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit13.Text) ;

end;

procedure TfrmLoja.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMasternCdCCReceita.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(87);

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoquePadrao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit17KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(147,'cFlgLojaCobradora = 1' );

            If (nPK > 0) then
            begin
                qryMasternCdLojaCobradora.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLoja.DBEdit17Exit(Sender: TObject);
begin
  inherited;
  qryLojaCobradora.Close ;
  PosicionaQuery(qryLojaCobradora, DBEdit17.Text) ;
end;

procedure TfrmLoja.qryCrediarioLojaCalcFields(DataSet: TDataSet);
begin
    qryNomeLoja.Close;
    qryNomeLoja.Parameters.ParamByName('nCdLoja').Value      := qryCrediarioLojanCdLoja.Value;
    qryNomeLoja.Parameters.ParamByName('nCdLojaVenda').Value := qryMasternCdLoja.Value;
    qryNomeLoja.Open;

    if not (qryNomeLoja.IsEmpty) then
        qryCrediarioLojacNmLoja.Value := qryNomeLojacNmLoja.Value;
end;

procedure TfrmLoja.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    if (Key = VK_F4) then
    begin
        if (qryCrediarioLoja.State = dsBrowse) then
             qryCrediarioLoja.Edit ;

        if (qryCrediarioLoja.State = dsInsert) or (qryCrediarioLoja.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(147, 'NOT EXISTS (SELECT TOP 1 1 FROM CrediarioLoja CL WHERE CL.nCdLojaVenda = ' + qryMasternCdLoja.AsString + ' AND CL.nCdLoja = Loja.nCdLoja) AND nCdLoja <> ' + qryMasternCdLoja.AsString);

            If (nPK > 0) then
            begin
                qryCrediarioLojanCdLoja.Value := nPK;
            end;

        end;

    end;

end;

procedure TfrmLoja.qryCrediarioLojaBeforePost(DataSet: TDataSet);
begin
  if (qryCrediarioLojacNmLoja.Value = '') then
      if (qryMasternCdLoja.Value = qryCrediarioLojanCdLoja.Value) then
      begin
          MensagemAlerta('N�o � possivel adicionar a loja atual!');
          Abort;
      end

      else
      begin
          MensagemAlerta('Loja n�o encontrada!');
          Abort;
      end;

  qryCrediarioLojanCdLojaVenda.Value := qryMasternCdLoja.Value ;

  if (qryCrediarioLoja.State = dsInsert) then
      qryCrediarioLojanCdCrediarioLoja.Value := frmMenu.fnProximoCodigo('CREDIARIOLOJA') ;

end;

procedure TfrmLoja.FormShow(Sender: TObject);
begin
    if (frmMenu.LeParametro('USAPERMVISUCRED') = 'S') then
        tabCrediarioLoja.TabVisible := True
    else
        tabCrediarioLoja.TabVisible := False;

  cxPageControl1.ActivePage := tabUsuario;
end;

procedure TfrmLoja.gridCDKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    if (Key = VK_F4) then
    begin
        case (gridCD.Col) of

        1: begin
               if (qryPrioridadeCentroDistribuicao.State = dsBrowse) then
                    qryPrioridadeCentroDistribuicao.Edit ;

               if (qryPrioridadeCentroDistribuicao.State = dsInsert) or (qryPrioridadeCentroDistribuicao.State = dsEdit) then
               begin

                   nPK := frmLookup_Padrao.ExecutaConsulta(782);

                   if (nPK > 0) then
                       qryPrioridadeCentroDistribuicaonCdCentroDistribuicao.Value := nPK;
               end;
           end;

        3: begin
               if (qryPrioridadeCentroDistribuicao.State = dsBrowse) then
                    qryPrioridadeCentroDistribuicao.Edit ;

               if (qryPrioridadeCentroDistribuicao.State = dsInsert) or (qryPrioridadeCentroDistribuicao.State = dsEdit) then
               begin

                   nPK := frmLookup_Padrao.ExecutaConsulta(783);

                   if (nPK > 0) then
                       qryPrioridadeCentroDistribuicaonCdCriterioSolicitacaoCD.Value := nPK;
               end;
           end;
        end;
    end;
end;

procedure TfrmLoja.qryPrioridadeCentroDistribuicaoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPrioridadeCentroDistribuicaocNmCentroDistribuicao.IsNull) then
  begin
      MensagemAlerta('Informe o Centro de Distribui��o.');
      Abort;
  end;

  if (qryPrioridadeCentroDistribuicaocNmCriterioSolicitacaoCD.IsNull) then
  begin
      MensagemAlerta('Informe um crit�rio de solicita��o.');
      Abort;
  end;

  if (qryPrioridadeCentroDistribuicaoiPrioridade.IsNull) then
  begin
      MensagemAlerta('Informe a prioridade de Requisi��o para o Centro de Distribui��o');
      Abort;
  end;

  if (qryPrioridadeCentroDistribuicaoiPrioridade.Value <= 0) then
  begin
      MensagemAlerta('A prioridade de Requisi��o deve ser maior que zero.');
      Abort;
  end;

  if (qryPrioridadeCentroDistribuicao.State = dsInsert) then
  begin
      qryValidaCD.Close;
      qryValidaCD.Parameters.ParamByName('nCdLoja').Value := qryMasternCdLoja.Value;
      qryValidaCD.Parameters.ParamByName('nCdCD').Value   := qryPrioridadeCentroDistribuicaonCdCentroDistribuicao.Value;
      qryValidaCD.Open;

      if not (qryValidaCD.IsEmpty) then
      begin
          MensagemAlerta('Centro de Distribui��o j� vinculado a loja.');
          Abort;
      end;

      qryValidaPrioridade.Close;
      qryValidaPrioridade.Parameters.ParamByName('nCdLoja').Value     := qryMasternCdLoja.Value;
      qryValidaPrioridade.Parameters.ParamByName('iPrioridade').Value := qryPrioridadeCentroDistribuicaoiPrioridade.Value;
      qryValidaPrioridade.Open;

      if not (qryValidaPrioridade.IsEmpty) then
      begin
          MensagemAlerta('Prioridade j� atribu�da � outro centro de distribui��o.');
          Abort;
      end;

      qryPrioridadeCentroDistribuicaonCdPrioridadeCentroDistribuicao.Value := frmMenu.fnProximoCodigo('PRIORIDADECENTRODISTRIBUICAO');
      qryPrioridadeCentroDistribuicaonCdLoja.Value                         := qryMasternCdLoja.Value;
  end;
end;

procedure TfrmLoja.gridCDEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post ;
end;

procedure TfrmLoja.DBEdit25Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoTransfSai.Close;
  PosicionaQuery(qryTipoPedidoTransfSai, qryMasternCdTipoPedidoTransfSai.AsString);
end;

procedure TfrmLoja.DBEdit25KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(71, 'cFlgTransf = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoTransfSai.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit26Exit(Sender: TObject);
begin
  inherited;
  
  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoTransfEnt.Close;
  PosicionaQuery(qryTipoPedidoTransfEnt, qryMasternCdTipoPedidoTransfEnt.AsString);
end;

procedure TfrmLoja.DBEdit26KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(98, 'cFlgTransf = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoTransfEnt.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit27Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoRecTransf.Close;

  if (qryTipoPedidoTransfEnt.IsEmpty) then
      Exit;

  qryTipoPedidoRecTransf.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidoTransfEntnCdTipoPedido.Value;
  PosicionaQuery(qryTipoPedidoRecTransf, qryMasternCdTipoPedidoRecTransf.AsString);
end;

procedure TfrmLoja.DBEdit27KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              if (qryTipoPedidoTransfEnt.IsEmpty) then
              begin
                  MensagemAlerta('Informe o tipo de pedido de compra para transfer�ncias faturadas.');
                  DBEdit26.SetFocus;
                  Abort;
              end;

              nPK := frmLookup_Padrao.ExecutaConsulta2(88,'EXISTS(SELECT TOP 1 1 FROM TipoPedidoTipoReceb TPT WHERE TPT.nCdTipoReceb  = TipoReceb.nCdTipoReceb AND TPT.nCdTipoPedido = ' + qryTipoPedidoTransfEntnCdTipoPedido.AsString +')');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoRecTransf.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit28Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoVendaSai.Close;
  PosicionaQuery(qryTipoPedidoVendaSai, qryMasternCdTipoPedidoVendaSai.AsString);
end;

procedure TfrmLoja.DBEdit28KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(71, 'cFlgVenda = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoVendaSai.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit29Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;
      
  qryTipoPedidoVendaEnt.Close;
  PosicionaQuery(qryTipoPedidoVendaEnt, qryMasternCdTipoPedidoVendaEnt.AsString);
end;

procedure TfrmLoja.DBEdit29KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(98, 'cFlgCompra = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoVendaEnt.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit30Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoRecVenda.Close;

  if (qryTipoPedidoVendaEnt.IsEmpty) then
      Exit;

  qryTipoPedidoRecVenda.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidoVendaEntnCdTipoPedido.Value;
  PosicionaQuery(qryTipoPedidoRecVenda, qryMasternCdTipoPedidoRecVenda.AsString);
end;

procedure TfrmLoja.DBEdit30KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              if (qryTipoPedidoVendaEnt.IsEmpty) then
              begin
                  MensagemAlerta('Informe o tipo de pedido de compra para vendas faturadas.');
                  DBEdit29.SetFocus;
                  Abort;
              end;

              nPK := frmLookup_Padrao.ExecutaConsulta2(88,'EXISTS(SELECT TOP 1 1 FROM TipoPedidoTipoReceb TPT WHERE TPT.nCdTipoReceb  = TipoReceb.nCdTipoReceb AND TPT.nCdTipoPedido = ' + qryTipoPedidoVendaEntnCdTipoPedido.AsString +')');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoRecVenda.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit31Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoDevVendaSai.Close;
  PosicionaQuery(qryTipoPedidoDevVendaSai, qryMasternCdTipoPedidoDevVendaSai.AsString);
end;

procedure TfrmLoja.DBEdit31KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(71, 'cFlgDevolucCompra = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoDevVendaSai.Value := nPK;
          end;
      end;
  end;
end;

procedure TfrmLoja.DBEdit32Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTipoPedidoDevVendaEnt.Close;
  PosicionaQuery(qryTipoPedidoDevVendaEnt, qryMasternCdTipoPedidoDevVendaEnt.AsString);
end;

procedure TfrmLoja.DBEdit32KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta2(98, 'cFlgDevolucVenda = 1');

              if (nPK > 0) then
                  qryMasternCdTipoPedidoDevVendaEnt.Value := nPK;
          end;
      end;
  end;
end;

initialization
    RegisterClass(TfrmLoja);

end.
