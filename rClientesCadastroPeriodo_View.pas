unit rClientesCadastroPeriodo_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, jpeg, DB, ADODB;

type
  TrClientesCadastroPeriodoView = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel6: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    uspRelatorio: TADOStoredProc;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRSysData3: TQRSysData;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    uspRelatorionCdLojaTerceiro: TStringField;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatoriocCnpjCpf: TStringField;
    uspRelatoriodDtCadastro: TDateTimeField;
    QRShape6: TQRShape;
    uspRelatoriocEmail: TStringField;
    uspRelatoriocTelefone1: TStringField;
    uspRelatoriocTelefone2: TStringField;
    uspRelatoriocTelefoneMovel: TStringField;
    uspRelatoriocEndereco: TStringField;
    uspRelatorioiNumero: TIntegerField;
    uspRelatoriocBairro: TStringField;
    uspRelatoriocCidade: TStringField;
    uspRelatoriocUF: TStringField;
    uspRelatoriocCep: TStringField;
    uspRelatoriocComplemento: TStringField;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel5: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    uspRelatoriocTipoCadastro: TStringField;
    DataSource1: TDataSource;
    QRLabel14: TQRLabel;
    QRDBText17: TQRDBText;
    uspRelatoriocStatus: TStringField;
    QRLabel13: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel17: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rClientesCadastroPeriodoView: TrClientesCadastroPeriodoView;

implementation

{$R *.dfm}

end.
