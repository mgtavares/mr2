inherited frmManutLimiteClienteManual: TfrmManutLimiteClienteManual
  Left = -8
  Top = -8
  Width = 1040
  Height = 754
  Caption = 'Manuten'#231#227'o de Limite de Cr'#233'dito - Manual'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1024
    Height = 693
  end
  object Label1: TLabel [1]
    Left = 96
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 350
    Top = 38
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Externo'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 65
    Top = 62
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Cliente'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 65
    Top = 86
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero C.P.F'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 368
    Top = 86
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero R.G'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 61
    Top = 110
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Cadastro'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 343
    Top = 110
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Nascimento'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 4
    Top = 182
    Width = 130
    Height = 13
    Alignment = taRightJustify
    Caption = 'Limite Cr'#233'dito Concedido'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 47
    Top = 206
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cr'#233'dito Utilizado'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 244
    Top = 206
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cr'#233'dito Pendente'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 443
    Top = 206
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Credi'#225'rio Atraso'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 640
    Top = 206
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cheque Pendente'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 39
    Top = 254
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = '% Entrada Compra'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [14]
    Left = 6
    Top = 230
    Width = 128
    Height = 13
    Alignment = taRightJustify
    Caption = 'Limite Cr'#233'dito Dispon'#237'vel'
    FocusControl = DBEdit14
  end
  object Label15: TLabel [15]
    Left = 39
    Top = 134
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Situa'#231#227'o Cadastral'
    FocusControl = DBEdit15
  end
  object Label16: TLabel [16]
    Left = 340
    Top = 134
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Negativa'#231#227'o'
    FocusControl = DBEdit16
  end
  inherited ToolBar2: TToolBar
    Width = 1024
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
    inherited ToolButton50: TToolButton
      Left = 465
      Wrap = False
      Visible = False
    end
    inherited btCancelar: TToolButton
      Left = 473
      Top = 0
    end
    inherited ToolButton11: TToolButton
      Left = 562
      Top = 0
    end
    inherited btConsultar: TToolButton
      Left = 570
      Top = 0
    end
    inherited ToolButton8: TToolButton
      Left = 659
      Top = 0
    end
    inherited ToolButton5: TToolButton
      Left = 667
      Top = 0
    end
    inherited ToolButton4: TToolButton
      Left = 756
      Top = 0
    end
    inherited ToolButton9: TToolButton
      Left = 764
      Top = 0
    end
    inherited btnAuditoria: TToolButton
      Left = 772
      Top = 0
    end
  end
  object DBEdit1: TDBEdit [18]
    Tag = 1
    Left = 136
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [19]
    Tag = 1
    Left = 432
    Top = 32
    Width = 81
    Height = 19
    DataField = 'cIDExterno'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [20]
    Tag = 1
    Left = 136
    Top = 56
    Width = 491
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [21]
    Tag = 1
    Left = 136
    Top = 80
    Width = 182
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [22]
    Tag = 1
    Left = 432
    Top = 80
    Width = 195
    Height = 19
    DataField = 'cRG'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [23]
    Tag = 1
    Left = 136
    Top = 104
    Width = 145
    Height = 19
    DataField = 'dDtCadastro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [24]
    Tag = 1
    Left = 432
    Top = 104
    Width = 89
    Height = 19
    DataField = 'dDtNasc'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [25]
    Tag = 1
    Left = 136
    Top = 176
    Width = 100
    Height = 19
    DataField = 'nValLimiteCred'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [26]
    Tag = 1
    Left = 136
    Top = 200
    Width = 100
    Height = 19
    DataField = 'nValCreditoUtil'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [27]
    Tag = 1
    Left = 336
    Top = 200
    Width = 100
    Height = 19
    DataField = 'nValPedidoAberto'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [28]
    Tag = 1
    Left = 529
    Top = 200
    Width = 100
    Height = 19
    DataField = 'nValRecAtraso'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit12: TDBEdit [29]
    Tag = 1
    Left = 735
    Top = 200
    Width = 100
    Height = 19
    DataField = 'nValChequePendComp'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [30]
    Tag = 1
    Left = 136
    Top = 248
    Width = 100
    Height = 19
    DataField = 'nPercEntrada'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit14: TDBEdit [31]
    Tag = 1
    Left = 136
    Top = 224
    Width = 100
    Height = 19
    DataField = 'nSaldoLimiteCredito'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit15: TDBEdit [32]
    Tag = 1
    Left = 136
    Top = 128
    Width = 145
    Height = 19
    DataField = 'cNmTabTipoSituacao'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit16: TDBEdit [33]
    Tag = 1
    Left = 432
    Top = 128
    Width = 89
    Height = 19
    DataField = 'dDtNegativacao'
    DataSource = dsMaster
    TabOrder = 16
    OnChange = DBEdit16Change
  end
  object btCalcular: TcxButton [34]
    Left = 136
    Top = 278
    Width = 145
    Height = 29
    Caption = 'Alterar Limite'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 17
    TabStop = False
    OnClick = btCalcularClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000F6DFCDF6DFCD
      F6DFCDF6DFCDF6DFCDF6DFCDEBDBCD2078B02078B0306890304850D0C4B8F6DF
      CDF6DFCDF6DFCDF6DFCDF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAB0D8F0C0
      F0F040D0FF30C0FF30A8F0304850207090304850CFC3B9F5DCCAF3DBC8E8D7C9
      2078B02078B0306890304050E6D2C2A0C8D0A0C0C090A0A080808070D0F040D0
      FF40B8F0405870F3DBC8F1D9C690B0C0C0F0F040D0FF30C0FF30B0F0103850F1
      D9C6F1D9C6E6D6C71078B01070A0205870203840B4B4B1F1D9C6F0D7C5F0D7C5
      90B0C0A0C0C090A0A0808890E8D3C2F0D7C5F0D7C590B0C0D0FFFF60D8FF40D0
      FF40B8F0305860F0D7C5EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EF
      D6C3EFD6C3E5D3C41068A01070A0205870203840B3B2AFEFD6C3EDD5C2EDD5C2
      EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C280C0D0D0FFFF60D8FF40D0
      FF40B8F0204860EDD5C2D0785090482090482090482080402080402080402080
      3820803820C0B8C00068A01070A02058702030408098A0802020D07850C08870
      C08050B09070D0A890E0C8B0C08060B06830C0505080B8E0C0F0F040D0FF30C0
      FF30A8F0205070802020D08050B09080D0C0B0E0D0C0D0B0A0E0C8C0B08870B0
      7040C07050D0B8A080B0E0A0C0D08098B07080A0D0B890802020D08860E0D0C0
      E0B8A0C08860C08050D0B8A0E0B090B07850B07050E0A880C09890905830A060
      40B0A090C09880802820E09070FFFFFFD09880D08860C08860E0D8D0C09880C0
      8050B07050C09070C0B8B0A05830905830A06030C0C0C0802820E09870E0D0D0
      F0C8B0D09870D08860F0D8D0C09880C08860B08050C09070D0C0B0A06030A060
      30B08870C09080803020E0A080B0A090E0C8C0F0E0E0F0C8B0E0B0A0F0C8B0D0
      A890C09880E0B8A0C09080C0A090D0B8B0B09880A06030803020E0A080A08880
      B09080B0A090D0C0B0FFF0E0FFF0F0F0F0F0F0F0F0F0E0E0F0D8C0C0A890A078
      60905830A05820803820E0A080E0A080E0A080E0A080E09880E09870D09070D0
      8870C08060C07860B07850B07050B06840A06040A05840905030}
    LookAndFeel.NativeStyle = True
  end
  object cxButton1: TcxButton [35]
    Left = 440
    Top = 278
    Width = 145
    Height = 29
    Caption = 'Hist'#243'rico Limite'
    TabOrder = 18
    OnClick = cxButton1Click
    Glyph.Data = {
      6E040000424D6E04000000000000360000002800000013000000120000000100
      1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
      D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
      DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
      D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
      00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
      DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
      D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
      C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
      BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
      0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
      EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
      C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
      FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
      000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
      BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
      BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
      000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
      FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
      D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
      EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
      DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
      FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
      C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
      00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
      0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
      BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
      DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
      D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
      C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
      D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
    LookAndFeel.NativeStyle = True
  end
  object cxButton2: TcxButton [36]
    Left = 288
    Top = 278
    Width = 145
    Height = 29
    Caption = 'Remover Limite'
    TabOrder = 19
    OnClick = cxButton2Click
    Glyph.Data = {
      32040000424D3204000000000000360000002800000013000000110000000100
      180000000000FC03000000000000000000000000000000000000FAFBFFFFFBFC
      FFFAF6FFFDF5FFFFF5FFFFF4FFFFF4FFFDF1FFFDF1FFFFF5FFFFF7FFFAF4FFF8
      F0FFF6E8FFFDEDFFFBEFFFF5EAFFF6F0FFFEFC000000FFFEFFFFFBF7F1E0D7E5
      D0C1EBD1C0F9DCC7EAC9B5F4D1BDFDD8C4EBC5B3E7C3B1F9D5C5FDD9C7FFE2CE
      FFE1CBF3D3C0E3C6B7F0D7CDFFFAF3000000FFFFFBFFFAF1DFC8B9B193809B77
      5FB0886CA17558A97A5EB9896DB7876BB6856BB7866CAD7C62A7785DA0725A9E
      755EB69280E8CBBDFFFBF3000000FFFFF7FFFAEBE8CBB69F7A60623617572705
      743E1B6E34106327037438147437156629076A2F0F743D1E582406623318A97E
      69F6D4C4FFFDF2000000FFF9F0FFFAEAFDDDC6AA826655240467310EC28760AD
      7048692B036A2B05692B05753713B67A56B17551703815643111B4876CFFDDC9
      FFF7E9000000FFFFFBFFFFF4F4D6C5A57C635C2A0C6F3616C88C68FFCCA8CD93
      6F602805592501A67352E6B28EBA7D556529006A3510AA7C5DEAC8B0FFFFF000
      0000FFFFFCFFFFF4F6D5C5A77B635E2A0C6329067A3C18C48962FCC39CB47F5A
      C69470DEB08EB8866285471E75360A733C15B18463FAD8C0FFFDEB000000FFFF
      F9FFFFF1F9D6C2AB7C60612B0A8A4F2871330B6C2C03CA8C63D2976FE2AB84B4
      805B622E066E300784441B6C3510A87A5BFFDFC7FFFDED000000FFFFF7FFFFF0
      FDD7BFAC7D5E632C0775370F7031056E2D01C48559EBAE82E9AF85B68057672F
      0665270070310B6E3515AA7B5FF8D6BFFFFDEE000000FFFFF5FFFFEFFFD7BEAE
      7D5D632C076224007D3C0FC98659E6A578CA8C5EC68B5EFBC295D2986E74350F
      5617007A4225B98B73F0CCBAFFFBEC000000FFFFF5FFFFEFFDD7BFAB7D5E612B
      0882451DC38458FFC397B7794B7C3F136F3407CB9469FFC89FB87B596024066E
      381FAF826DF0CEBEFFFFF4000000FFFFF7FFFFF1F7D6C2A57C635A2B0B6B3512
      B47B54D1966E7A3F17713910632D046E3A12B9845FBC83646E361D5826109E73
      62F8D6C9FFFEF7000000FFFFFBFFFFF4F0D6C69E7B67532A1150210551200067
      3413461300582806744624491C005526075C290F5D2A16532513A47D6FFCDED3
      FFEFE9000000FFFEFEFFFBF8DCC7BFA88D7F9A7969A3806CA78169A98069A981
      68A98168A78268A58268A78169A37865A87B6DA67F71B39086DFC3BCFFF9F400
      0000FFFEFFFFFEFEF5E6E3E0CDC6E3CDC2EAD0C2EBD1C1EDD0C1EDD1C0EDD1C0
      EDD1C0EBD1C1EDD0C1F3D0C3F4D0C6EDCCC3E9CDC6F7E1DCFFFBFA000000FAFA
      FFFFFDFFFFF8F8FFF7F3FFFFF9FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FF
      FFF8FFFFF7FFFEF7FFFEF7FFFEF8FFFAF5FFF8F7FFFDFD000000F7FAFFFDFCFE
      FEFCFCFFFCFBFFFFFCFFFFFCFFFFFBFFFFFBFFFFFBFFFFFCFFFFFCFFFFFCFFFE
      FCFFFCF9FFFDFAFFFEFBFFFEFCFFFDFDFFFAFB000000}
    LookAndFeel.NativeStyle = True
  end
  object DBCheckBox1: TDBCheckBox [37]
    Left = 244
    Top = 176
    Width = 225
    Height = 17
    Caption = 'N'#227'o atualizar limite automaticamente'
    DataField = 'cFlgBloqAtuLimAutom'
    DataSource = dsMaster
    ReadOnly = True
    TabOrder = 20
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxButton3: TcxButton [38]
    Left = 136
    Top = 310
    Width = 241
    Height = 29
    Caption = 'Ativar/Desativar Atualiza'#231#227'o Autom.'
    TabOrder = 21
    OnClick = cxButton3Click
    Glyph.Data = {
      EE030000424DEE03000000000000360000002800000012000000110000000100
      180000000000B803000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000080808000
      0000000000000000808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080000000DEBED4FFFFFFFFFFFF
      000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      000000000000000000000000DEBED4DEBED4000000DEBED4FFFFFF0000000000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF0000009999999999999999
      99000000FFFFFF000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFE6E6E6000000000000FFFFFFDE
      BED4000000DEBED4DEBED4000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFF000000FFFFFFE6E6E6E6E6E6808080000000FFFFFFFFFFFFDEBED4
      0000008080804E7EA6FFFFFF4E7EA6FFFFFFFFFFFFFFFFFF0000FFFFFF000000
      FFFFFFE6E6E6E6E6E60000008080800000000000000000008080804E7EA60000
      000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
      E6E6E6E6E6E6E60000009999990000004E7EA60000004E7EA64E7EA64E7EA600
      00004E7EA6FFFFFF0000FFFFFF000000FFFFFF008000008000E6E6E6E6E6E6E6
      E6E6999999000000FFFFFF0000004E7EA60000004E7EA6000000FFFFFFFFFFFF
      0000FFFFFF000000FFFFFF008000008000008000E6E6E6E6E6E6999999000000
      4E7EA60000004E7EA64E7EA64E7EA60000004E7EA6FFFFFF0000FFFFFF000000
      FFFFFF008000008000E6E6E6E6E6E6E6E6E6999999000000FFFFFF4E7EA60000
      000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
      E6E6E6E6E6E6E6E6E6E6999999000000FFFFFFFFFFFF4E7EA6FFFFFF4E7EA6FF
      FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFF000000000000000000000000000000000000000000FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro '
      '      ,cIDExterno           '
      '      ,cNmTerceiro                                        '
      '      ,cCNPJCPF       '
      '      ,cRG             '
      '      ,dDtCadastro             '
      '      ,dDtNasc'
      '      ,nValLimiteCred                          '
      '      ,nValCreditoUtil                         '
      '      ,nValPedidoAberto                        '
      '      ,nValRecAtraso                           '
      '      ,nValChequePendComp'
      '      ,nPercEntrada'
      '      ,(nValLimiteCred - nValCreditoUtil) as nSaldoLimiteCredito'
      '      ,cNmTabTipoSituacao'
      '      ,dDtNegativacao'
      '      ,cFlgBloqAtuLimAutom'
      '      ,vwClienteVarejo.nCdTabTipoSituacao'
      '  FROM vwClienteVarejo'
      
        '       LEFT JOIN TabTipoSituacao ON TabTipoSituacao.nCdTabTipoSi' +
        'tuacao = vwClienteVarejo.nCdTabTipoSituacao'
      ' WHERE nCdTerceiro = :nPK')
    Left = 688
    Top = 240
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercIDExterno: TStringField
      FieldName = 'cIDExterno'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMastercRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryMasterdDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryMasterdDtNasc: TDateTimeField
      FieldName = 'dDtNasc'
    end
    object qryMasternValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValCreditoUtil: TBCDField
      FieldName = 'nValCreditoUtil'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValPedidoAberto: TBCDField
      FieldName = 'nValPedidoAberto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValRecAtraso: TBCDField
      FieldName = 'nValRecAtraso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValChequePendComp: TBCDField
      FieldName = 'nValChequePendComp'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercEntrada: TBCDField
      FieldName = 'nPercEntrada'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoLimiteCredito: TBCDField
      FieldName = 'nSaldoLimiteCredito'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object qryMastercNmTabTipoSituacao: TStringField
      FieldName = 'cNmTabTipoSituacao'
      Size = 50
    end
    object qryMasterdDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryMastercFlgBloqAtuLimAutom: TIntegerField
      FieldName = 'cFlgBloqAtuLimAutom'
    end
    object qryMasternCdTabTipoSituacao: TIntegerField
      FieldName = 'nCdTabTipoSituacao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 728
    Top = 232
  end
  inherited qryID: TADOQuery
    Left = 864
    Top = 256
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 808
    Top = 224
  end
  inherited qryStat: TADOQuery
    Left = 776
    Top = 232
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 840
    Top = 232
  end
  inherited ImageList1: TImageList
    Top = 296
  end
  object SP_GERA_LIMITE_CREDITO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_LIMITE_CREDITO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValLimite'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nPercEntrada'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@cFlgAutomatico'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 456
    Top = 352
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 544
    Top = 328
  end
end
