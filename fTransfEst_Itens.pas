unit fTransfEst_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, Mask, DBCtrls, DB, DBClient, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmTransfEst_Itens = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit10: TDBEdit;
    DBGridEh1: TDBGridEh;
    cdsProduto: TClientDataSet;
    cdsProdutonQtde: TFloatField;
    cdsProdutocCodigo: TStringField;
    dsClientProduto: TDataSource;
    cdsProdutoiPosicao: TIntegerField;
    edtEAN: TEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure edtEANKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEANExit(Sender: TObject);
  private
    { Private declarations }
    iPosicao : integer ;
  public
    { Public declarations }
  end;

var
  frmTransfEst_Itens: TfrmTransfEst_Itens;

implementation

uses fLookup_Padrao, fTransfEst, fMenu;

{$R *.dfm}

procedure TfrmTransfEst_Itens.FormShow(Sender: TObject);
begin
  inherited;

  cdsProduto.Close ;
  cdsProduto.Open ;


  iPosicao := 1 ;
  {
  cdsProdutonQtde.Value := 1 ;
  }

  edtEAN.MaxLength := StrToint(frmMenu.LeParametro('TMMXBRPRPDV')) ;

  edtEAN.SetFocus ;

end;

procedure TfrmTransfEst_Itens.ToolButton2Click(Sender: TObject);
begin

  if (cdsProduto.State = dsEdit) and (cdsProdutocCodigo.Value <> '') then
      cdsProduto.Post ;

  inherited;
end;

procedure TfrmTransfEst_Itens.edtEANKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Produto2 WHERE Produto2.nCdProdutoPai = Produto.nCdProduto)');

        If (nPK > 0) then
        begin
            cdsProdutocCodigo.Value := IntToStr(nPK) ;
        end ;

    end ;

  end ;
end;

procedure TfrmTransfEst_Itens.edtEANExit(Sender: TObject);
begin
  inherited;

  if (Trim(edtEAN.Text) <> '') then
  begin

      cdsProduto.Insert ;

      cdsProdutocCodigo.Value  := edtEAN.Text ;
      cdsProdutonQtde.Value    := 1 ;
      cdsProdutoiPosicao.Value := iPosicao ;

      cdsProduto.Post ;

      iPosicao := iPosicao + 1;

      edtEAN.Text := '' ;
      edtEAN.SetFocus ;

  end
  else begin
      cdsProduto.Cancel;
      Close ;
  end ;
end;

end.

