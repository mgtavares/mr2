inherited frmItemPedidoCompraAtendido: TfrmItemPedidoCompraAtendido
  Left = 287
  Top = 196
  Width = 694
  Height = 384
  BorderIcons = [biSystemMenu]
  Caption = 'Atendimento de Pedido de Compra'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 678
    Height = 317
  end
  inherited ToolBar1: TToolBar
    Width = 678
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 678
    Height = 317
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdRecebimento'
        Footers = <>
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'cNrDocto'
        Footers = <>
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'cSerieDocto'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'dDtDocto'
        Footers = <>
        Width = 88
      end
      item
        EditButtons = <>
        FieldName = 'dDtAtendimento'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeAtendida'
        Footers = <>
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'iDias'
        Footers = <>
        Width = 36
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 166
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryItem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdItemPedido int'
      ''
      'Set @nCdItemPedido = :nPK'
      ''
      'SELECT Recebimento.nCdRecebimento'
      '      ,Recebimento.cNrDocto'
      '      ,Recebimento.cSerieDocto'
      '      ,Recebimento.dDtDocto'
      '      ,Recebimento.dDtReceb '
      '      ,dDtAtendimento'
      
        '      ,Convert(int,(dbo.fn_OnlyDate(dDtAtendimento)-dbo.fn_OnlyD' +
        'ate(dDtAutor))) as iDias'
      '      ,cNmTerceiro'
      '      ,Sum(ItemPedidoAtendido.nQtdeAtendida) nQtdeAtendida'
      '  FROM ItemPedido'
      
        '       LEFT  JOIN ItemPedidoAtendido ON ItemPedido.nCdItemPedido' +
        '   = ItemPedidoAtendido.nCdItemPedido'
      
        '       INNER JOIN Pedido             ON Pedido.nCdPedido        ' +
        '   = ItemPedido.nCdPedido'
      
        '       INNER JOIN Terceiro           ON Terceiro.nCdTerceiro    ' +
        '   = Pedido.nCdTerceiro'
      
        '       INNER JOIN Recebimento        ON Recebimento.nCdRecebimen' +
        'to = ItemPedidoAtendido.nCdRecebimento'
      ' WHERE ItemPedido.nCdItemPedido = @nCdItemPedido'
      ' GROUP BY Recebimento.nCdRecebimento'
      '      ,Recebimento.cNrDocto'
      '      ,Recebimento.cSerieDocto'
      '      ,Recebimento.dDtDocto'
      '      ,Recebimento.dDtReceb '
      '      ,dDtAtendimento'
      
        '      ,Convert(int,(dbo.fn_OnlyDate(dDtAtendimento)-dbo.fn_OnlyD' +
        'ate(dDtAutor)))'
      '      ,cNmTerceiro'
      'UNION'
      'SELECT Recebimento.nCdRecebimento'
      '      ,Recebimento.cNrDocto'
      '      ,Recebimento.cSerieDocto'
      '      ,Recebimento.dDtDocto'
      '      ,Recebimento.dDtReceb '
      '      ,dDtAtendimento'
      
        '      ,Convert(int,(dbo.fn_OnlyDate(dDtAtendimento)-dbo.fn_OnlyD' +
        'ate(dDtAutor))) as iDias'
      '      ,cNmTerceiro'
      '      ,Sum(ItemPedidoAtendido.nQtdeAtendida) nQtdeAtendida'
      '  FROM ItemPedido'
      
        '       LEFT  JOIN ItemPedidoAtendido ON ItemPedido.nCdItemPedido' +
        '   = ItemPedidoAtendido.nCdItemPedido'
      
        '       INNER JOIN Pedido             ON Pedido.nCdPedido        ' +
        '   = ItemPedido.nCdPedido'
      
        '       INNER JOIN Terceiro           ON Terceiro.nCdTerceiro    ' +
        '   = Pedido.nCdTerceiro'
      
        '       INNER JOIN Recebimento        ON Recebimento.nCdRecebimen' +
        'to = ItemPedidoAtendido.nCdRecebimento'
      ' WHERE ItemPedido.nCdItemPedidoPai = @nCdItemPedido'
      ' GROUP BY Recebimento.nCdRecebimento'
      '      ,Recebimento.cNrDocto'
      '      ,Recebimento.cSerieDocto'
      '      ,Recebimento.dDtDocto'
      '      ,Recebimento.dDtReceb '
      '      ,dDtAtendimento'
      
        '      ,Convert(int,(dbo.fn_OnlyDate(dDtAtendimento)-dbo.fn_OnlyD' +
        'ate(dDtAutor)))'
      '      ,cNmTerceiro'
      '')
    Left = 376
    Top = 160
    object qryItemnCdRecebimento: TIntegerField
      DisplayLabel = 'Recebimento|N'#250'mero'
      FieldName = 'nCdRecebimento'
    end
    object qryItemcNrDocto: TStringField
      DisplayLabel = 'Recebimento|NF'
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryItemcSerieDocto: TStringField
      DisplayLabel = 'Recebimento|S'#233'rie'
      FieldName = 'cSerieDocto'
      FixedChar = True
      Size = 2
    end
    object qryItemdDtDocto: TDateTimeField
      DisplayLabel = 'Recebimento|Emiss'#227'o NF'
      FieldName = 'dDtDocto'
    end
    object qryItemdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryItemdDtAtendimento: TDateTimeField
      DisplayLabel = 'Recebimento|Recebimento'
      FieldName = 'dDtAtendimento'
    end
    object qryItemiDias: TIntegerField
      DisplayLabel = 'Recebimento|Dias'
      FieldName = 'iDias'
      ReadOnly = True
    end
    object qryItemnQtdeAtendida: TBCDField
      DisplayLabel = 'Recebimento|Quantidade'
      FieldName = 'nQtdeAtendida'
      Precision = 12
      Size = 2
    end
    object qryItemcNmTerceiro: TStringField
      DisplayLabel = 'Recebimento|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsItem: TDataSource
    DataSet = qryItem
    Left = 288
    Top = 168
  end
end
